#!/bin/sh
# the next line restarts using wish\
exec wish "$0" "$@" 

if {![info exists vTcl(sourcing)]} {

    package require Tk
    switch $tcl_platform(platform) {
	windows {
            option add *Button.padY 0
	}
	default {
            option add *Scrollbar.width 10
            option add *Scrollbar.highlightThickness 0
            option add *Scrollbar.elementBorderWidth 2
            option add *Scrollbar.borderWidth 2
	}
    }
    
}

#############################################################################
# Visual Tcl v1.60 Project
#


#################################
# VTCL LIBRARY PROCEDURES
#

if {![info exists vTcl(sourcing)]} {
#############################################################################
## Library Procedure:  Window

proc ::Window {args} {
    ## This procedure may be used free of restrictions.
    ##    Exception added by Christian Gavin on 08/08/02.
    ## Other packages and widget toolkits have different licensing requirements.
    ##    Please read their license agreements for details.

    global vTcl
    foreach {cmd name newname} [lrange $args 0 2] {}
    set rest    [lrange $args 3 end]
    if {$name == "" || $cmd == ""} { return }
    if {$newname == ""} { set newname $name }
    if {$name == "."} { wm withdraw $name; return }
    set exists [winfo exists $newname]
    switch $cmd {
        show {
            if {$exists} {
                wm deiconify $newname
            } elseif {[info procs vTclWindow$name] != ""} {
                eval "vTclWindow$name $newname $rest"
            }
            if {[winfo exists $newname] && [wm state $newname] == "normal"} {
                vTcl:FireEvent $newname <<Show>>
            }
        }
        hide    {
            if {$exists} {
                wm withdraw $newname
                vTcl:FireEvent $newname <<Hide>>
                return}
        }
        iconify { if $exists {wm iconify $newname; return} }
        destroy { if $exists {destroy $newname; return} }
    }
}
#############################################################################
## Library Procedure:  vTcl:DefineAlias

proc ::vTcl:DefineAlias {target alias widgetProc top_or_alias cmdalias} {
    ## This procedure may be used free of restrictions.
    ##    Exception added by Christian Gavin on 08/08/02.
    ## Other packages and widget toolkits have different licensing requirements.
    ##    Please read their license agreements for details.

    global widget
    set widget($alias) $target
    set widget(rev,$target) $alias
    if {$cmdalias} {
        interp alias {} $alias {} $widgetProc $target
    }
    if {$top_or_alias != ""} {
        set widget($top_or_alias,$alias) $target
        if {$cmdalias} {
            interp alias {} $top_or_alias.$alias {} $widgetProc $target
        }
    }
}
#############################################################################
## Library Procedure:  vTcl:DoCmdOption

proc ::vTcl:DoCmdOption {target cmd} {
    ## This procedure may be used free of restrictions.
    ##    Exception added by Christian Gavin on 08/08/02.
    ## Other packages and widget toolkits have different licensing requirements.
    ##    Please read their license agreements for details.

    ## menus are considered toplevel windows
    set parent $target
    while {[winfo class $parent] == "Menu"} {
        set parent [winfo parent $parent]
    }

    regsub -all {\%widget} $cmd $target cmd
    regsub -all {\%top} $cmd [winfo toplevel $parent] cmd

    uplevel #0 [list eval $cmd]
}
#############################################################################
## Library Procedure:  vTcl:FireEvent

proc ::vTcl:FireEvent {target event {params {}}} {
    ## This procedure may be used free of restrictions.
    ##    Exception added by Christian Gavin on 08/08/02.
    ## Other packages and widget toolkits have different licensing requirements.
    ##    Please read their license agreements for details.

    ## The window may have disappeared
    if {![winfo exists $target]} return
    ## Process each binding tag, looking for the event
    foreach bindtag [bindtags $target] {
        set tag_events [bind $bindtag]
        set stop_processing 0
        foreach tag_event $tag_events {
            if {$tag_event == $event} {
                set bind_code [bind $bindtag $tag_event]
                foreach rep "\{%W $target\} $params" {
                    regsub -all [lindex $rep 0] $bind_code [lindex $rep 1] bind_code
                }
                set result [catch {uplevel #0 $bind_code} errortext]
                if {$result == 3} {
                    ## break exception, stop processing
                    set stop_processing 1
                } elseif {$result != 0} {
                    bgerror $errortext
                }
                break
            }
        }
        if {$stop_processing} {break}
    }
}
#############################################################################
## Library Procedure:  vTcl:Toplevel:WidgetProc

proc ::vTcl:Toplevel:WidgetProc {w args} {
    ## This procedure may be used free of restrictions.
    ##    Exception added by Christian Gavin on 08/08/02.
    ## Other packages and widget toolkits have different licensing requirements.
    ##    Please read their license agreements for details.

    if {[llength $args] == 0} {
        ## If no arguments, returns the path the alias points to
        return $w
    }
    set command [lindex $args 0]
    set args [lrange $args 1 end]
    switch -- [string tolower $command] {
        "setvar" {
            foreach {varname value} $args {}
            if {$value == ""} {
                return [set ::${w}::${varname}]
            } else {
                return [set ::${w}::${varname} $value]
            }
        }
        "hide" - "show" {
            Window [string tolower $command] $w
        }
        "showmodal" {
            ## modal dialog ends when window is destroyed
            Window show $w; raise $w
            grab $w; tkwait window $w; grab release $w
        }
        "startmodal" {
            ## ends when endmodal called
            Window show $w; raise $w
            set ::${w}::_modal 1
            grab $w; tkwait variable ::${w}::_modal; grab release $w
        }
        "endmodal" {
            ## ends modal dialog started with startmodal, argument is var name
            set ::${w}::_modal 0
            Window hide $w
        }
        default {
            uplevel $w $command $args
        }
    }
}
#############################################################################
## Library Procedure:  vTcl:WidgetProc

proc ::vTcl:WidgetProc {w args} {
    ## This procedure may be used free of restrictions.
    ##    Exception added by Christian Gavin on 08/08/02.
    ## Other packages and widget toolkits have different licensing requirements.
    ##    Please read their license agreements for details.

    if {[llength $args] == 0} {
        ## If no arguments, returns the path the alias points to
        return $w
    }
    ## The first argument is a switch, they must be doing a configure.
    if {[string index $args 0] == "-"} {
        set command configure
        ## There's only one argument, must be a cget.
        if {[llength $args] == 1} {
            set command cget
        }
    } else {
        set command [lindex $args 0]
        set args [lrange $args 1 end]
    }
    uplevel $w $command $args
}
#############################################################################
## Library Procedure:  vTcl:toplevel

proc ::vTcl:toplevel {args} {
    ## This procedure may be used free of restrictions.
    ##    Exception added by Christian Gavin on 08/08/02.
    ## Other packages and widget toolkits have different licensing requirements.
    ##    Please read their license agreements for details.

    uplevel #0 eval toplevel $args
    set target [lindex $args 0]
    namespace eval ::$target {set _modal 0}
}
}


if {[info exists vTcl(sourcing)]} {

proc vTcl:project:info {} {
    set base .top26
    namespace eval ::widgets::$base {
        set set,origin 1
        set set,size 1
        set runvisible 1
    }
    namespace eval ::widgets::$base.tex28 {
        array set save {-background 1 -insertbackground 1 -selectbackground 1}
    }
    namespace eval ::widgets::$base.but30 {
        array set save {-command 1 -disabledforeground 1 -text 1}
    }
    namespace eval ::widgets::$base.ent31 {
        array set save {-background 1 -insertbackground 1 -textvariable 1}
    }
    namespace eval ::widgets::$base.mes37 {
        array set save {-highlightcolor 1 -padx 1 -pady 1 -text 1 -width 1}
    }
    namespace eval ::widgets::$base.cpd38 {
        array set save {-background 1 -insertbackground 1 -textvariable 1}
    }
    namespace eval ::widgets::$base.cpd39 {
        array set save {-command 1 -disabledforeground 1 -text 1}
    }
    namespace eval ::widgets::$base.cpd40 {
        array set save {-highlightcolor 1 -padx 1 -pady 1 -text 1 -width 1}
    }
    namespace eval ::widgets::$base.but41 {
        array set save {-command 1 -disabledforeground 1 -text 1}
    }
    namespace eval ::widgets::$base.cpd42 {
        array set save {-command 1 -disabledforeground 1 -text 1}
    }
    namespace eval ::widgets::$base.cpd44 {
        array set save {-highlightcolor 1 -padx 1 -pady 1 -text 1 -width 1}
    }
    namespace eval ::widgets::$base.cpd45 {
        array set save {-background 1 -insertbackground 1 -textvariable 1}
    }
    namespace eval ::widgets::$base.cpd46 {
        array set save {-command 1 -disabledforeground 1 -text 1}
    }
    namespace eval ::widgets::$base.cpd47 {
        array set save {-command 1 -disabledforeground 1 -text 1}
    }
    namespace eval ::widgets::$base.cpd48 {
        array set save {-background 1 -insertbackground 1 -textvariable 1}
    }
    namespace eval ::widgets::$base.cpd49 {
        array set save {-highlightcolor 1 -padx 1 -pady 1 -text 1 -width 1}
    }
    namespace eval ::widgets::$base.cpd50 {
        array set save {-command 1 -disabledforeground 1 -text 1}
    }
    namespace eval ::widgets::$base.cpd51 {
        array set save {-command 1 -disabledforeground 1 -text 1}
    }
    namespace eval ::widgets::$base.but52 {
        array set save {-command 1 -disabledforeground 1 -text 1}
    }
    namespace eval ::widgets::$base.cpd53 {
        array set save {-command 1 -disabledforeground 1 -text 1}
    }
    namespace eval ::widgets::$base.cpd54 {
        array set save {-highlightcolor 1 -padx 1 -pady 1 -text 1 -width 1}
    }
    namespace eval ::widgets::$base.cpd55 {
        array set save {-background 1 -insertbackground 1 -textvariable 1}
    }
    namespace eval ::widgets::$base.cpd56 {
        array set save {-command 1 -disabledforeground 1 -text 1}
    }
    namespace eval ::widgets::$base.cpd57 {
        array set save {-command 1 -disabledforeground 1 -text 1}
    }
    namespace eval ::widgets::$base.but58 {
        array set save {-command 1 -disabledforeground 1 -text 1}
    }
    namespace eval ::widgets::$base.cpd30 {
        array set save {-background 1 -insertbackground 1 -textvariable 1}
    }
    namespace eval ::widgets::$base.cpd31 {
        array set save {-highlightcolor 1 -padx 1 -pady 1 -text 1 -width 1}
    }
    namespace eval ::widgets::$base.cpd32 {
        array set save {-command 1 -disabledforeground 1 -text 1}
    }
    namespace eval ::widgets::$base.cpd58 {
        array set save {-font 1 -highlightcolor 1 -padx 1 -pady 1 -text 1 -width 1}
    }
    namespace eval ::widgets::$base.cpd59 {
        array set save {-font 1 -highlightcolor 1 -padx 1 -pady 1 -text 1 -width 1}
    }
    namespace eval ::widgets::$base.cpd61 {
        array set save {-font 1 -highlightcolor 1 -padx 1 -pady 1 -text 1 -width 1}
    }
    namespace eval ::widgets::$base.m55 {
        array set save {-tearoff 1}
    }
    namespace eval ::widgets::$base.cpd60 {
        array set save {-command 1 -disabledforeground 1 -text 1}
    }
    namespace eval ::widgets::$base.men55 {
        array set save {-menu 1 -padx 1 -pady 1 -text 1}
    }
    namespace eval ::widgets::$base.men55.m {
        array set save {-tearoff 1}
        namespace eval subOptions {
            array set save {-command 1 -label 1}
        }
    }
    namespace eval ::widgets::$base.men67 {
        array set save {-menu 1 -padx 1 -pady 1 -text 1}
    }
    namespace eval ::widgets::$base.men67.m {
        array set save {-tearoff 1}
        namespace eval subOptions {
            array set save {-command 1 -label 1}
        }
    }
    namespace eval ::widgets::$base.men68 {
        array set save {-menu 1 -padx 1 -pady 1 -text 1}
    }
    namespace eval ::widgets::$base.men68.m {
        array set save {-tearoff 1}
        namespace eval subOptions {
            array set save {-command 1 -label 1}
        }
    }
    namespace eval ::widgets::$base.men69 {
        array set save {-menu 1 -padx 1 -pady 1 -text 1}
    }
    namespace eval ::widgets::$base.men69.m {
        array set save {-tearoff 1}
        namespace eval subOptions {
            array set save {-command 1 -label 1}
        }
    }
    set base .top55
    namespace eval ::widgets::$base {
        set set,origin 1
        set set,size 1
        set runvisible 1
    }
    namespace eval ::widgets::$base.mes56 {
        array set save {-highlightcolor 1 -padx 1 -pady 1 -text 1 -width 1}
    }
    namespace eval ::widgets::$base.ent57 {
        array set save {-background 1 -insertbackground 1 -textvariable 1}
    }
    namespace eval ::widgets::$base.but58 {
        array set save {-command 1 -text 1}
    }
    namespace eval ::widgets::$base.but59 {
        array set save {-command 1 -text 1}
    }
    namespace eval ::widgets::$base.mes60 {
        array set save {-highlightcolor 1 -padx 1 -pady 1 -text 1 -width 1}
    }
    namespace eval ::widgets::$base.ent61 {
        array set save {-background 1 -insertbackground 1 -textvariable 1}
    }
    namespace eval ::widgets::$base.but62 {
        array set save {-command 1 -text 1}
    }
    namespace eval ::widgets::$base.but63 {
        array set save {-command 1 -text 1}
    }
    namespace eval ::widgets::$base.but64 {
        array set save {-command 1 -text 1}
    }
    namespace eval ::widgets::$base.but65 {
        array set save {-command 1 -text 1}
    }
    namespace eval ::widgets::$base.mes66 {
        array set save {-highlightcolor 1 -padx 1 -pady 1 -text 1 -width 1}
    }
    namespace eval ::widgets::$base.ent67 {
        array set save {-background 1 -insertbackground 1 -textvariable 1}
    }
    namespace eval ::widgets::$base.but68 {
        array set save {-command 1 -text 1}
    }
    namespace eval ::widgets::$base.rad56 {
        array set save {-command 1 -cursor 1 -text 1 -value 1 -variable 1}
    }
    namespace eval ::widgets::$base.rad57 {
        array set save {-command 1 -text 1 -value 1 -variable 1}
    }
    namespace eval ::widgets::$base.mes61 {
        array set save {-highlightcolor 1 -padx 1 -pady 1 -text 1 -width 1}
    }
    namespace eval ::widgets_bindings {
        set tagslist _TopLevel
    }
    namespace eval ::vTcl::modules::main {
        set procs {
            init
            main
            print_mess
            efface
        }
        set compounds {
        }
        set projectType single
    }
}
}

#################################
# USER DEFINED PROCEDURES
#
#############################################################################
## Procedure:  main

proc ::main {argc argv} {

}
#############################################################################
## Procedure:  print_mess

proc ::print_mess {alias texte} {
global widget
$widget($alias) insert end $texte
}
#############################################################################
## Procedure:  efface

proc ::efface {alias} {
global widget
$widget($alias) delete 1.0 end
}

#############################################################################
## Initialization Procedure:  init

proc ::init {argc argv} {

}

init $argc $argv

#################################
# VTCL GENERATED GUI PROCEDURES
#

proc vTclWindow. {base} {
    if {$base == ""} {
        set base .
    }
    ###################
    # CREATING WIDGETS
    ###################
    wm focusmodel $top passive
    wm geometry $top 1x1+0+0; update
    wm maxsize $top 1265 994
    wm minsize $top 1 1
    wm overrideredirect $top 0
    wm resizable $top 1 1
    wm withdraw $top
    wm title $top "vtcl.tcl"
    bindtags $top "$top Vtcl.tcl all"
    vTcl:FireEvent $top <<Create>>
    wm protocol $top WM_DELETE_WINDOW "vTcl:FireEvent $top <<DeleteWindow>>"

    ###################
    # SETTING GEOMETRY
    ###################

    vTcl:FireEvent $base <<Ready>>
}

proc vTclWindow.top26 {base} {
    if {$base == ""} {
        set base .top26
    }
    if {[winfo exists $base]} {
        wm deiconify $base; return
    }
    set top $base
    ###################
    # CREATING WIDGETS
    ###################
    vTcl:toplevel $top -class Toplevel \
        -highlightcolor black -menu "$top.m55" 
    wm focusmodel $top passive
    wm geometry $top 698x689+0+0; update
    wm maxsize $top 1265 994
    wm minsize $top 1 1
    wm overrideredirect $top 0
    wm resizable $top 1 1
    wm deiconify $top
    wm title $top "DyALog Front End V.0.1"
    vTcl:DefineAlias "$top" "Toplevel1" vTcl:Toplevel:WidgetProc "" 1
    bindtags $top "$top Toplevel all _TopLevel"
    vTcl:FireEvent $top <<Create>>
    wm protocol $top WM_DELETE_WINDOW "vTcl:FireEvent $top <<DeleteWindow>>"

    text $top.tex28 \
        -background #cccccc -insertbackground black -selectbackground #ffffff 
    vTcl:DefineAlias "$top.tex28" "FENETRE_MESSAGE" vTcl:WidgetProc "Toplevel1" 1
    button $top.but30 \
        \
        -command {set GRAMMAIRE [tk_getOpenFile -defaultextension "tag" -title "S�lectionnez votre grammaire" -initialfile "*.tag" -initialdir $env(HOME)]} \
        -disabledforeground #a7a4a7 -text ... 
    vTcl:DefineAlias "$top.but30" "Button1" vTcl:WidgetProc "Toplevel1" 1
    entry $top.ent31 \
        -background white -insertbackground black -textvariable GRAMMAIRE 
    vTcl:DefineAlias "$top.ent31" "Entry1" vTcl:WidgetProc "Toplevel1" 1
    message $top.mes37 \
        -highlightcolor black -padx 5 -pady 2 -text {Grammaire TAG} \
        -width 110 
    vTcl:DefineAlias "$top.mes37" "Message1" vTcl:WidgetProc "Toplevel1" 1
    entry $top.cpd38 \
        -background white -insertbackground black -textvariable DYACC 
    vTcl:DefineAlias "$top.cpd38" "Entry2" vTcl:WidgetProc "Toplevel1" 1
    button $top.cpd39 \
        \
        -command {set DYACC [tk_getOpenFile -title "Emplacement de dyacc" -initialdir $env(HOME)]} \
        -disabledforeground #a7a4a7 -text ... 
    vTcl:DefineAlias "$top.cpd39" "Button2" vTcl:WidgetProc "Toplevel1" 1
    message $top.cpd40 \
        -highlightcolor black -padx 5 -pady 2 -text {Emplacement de dyacc} \
        -width 160 
    vTcl:DefineAlias "$top.cpd40" "Message2" vTcl:WidgetProc "Toplevel1" 1
    button $top.but41 \
        -command {catch [exec $env(EDITOR) $GRAMMAIRE &]} \
        -disabledforeground #a7a4a7 -text Editer 
    vTcl:DefineAlias "$top.but41" "Button3" vTcl:WidgetProc "Toplevel1" 1
    button $top.cpd42 \
        -command {set DYACC "/usr/local/bin/dyacc"} \
        -disabledforeground #a7a4a7 -text Defaut 
    vTcl:DefineAlias "$top.cpd42" "Button4" vTcl:WidgetProc "Toplevel1" 1
    message $top.cpd44 \
        -highlightcolor black -padx 5 -pady 2 -text Parser -width 80 
    vTcl:DefineAlias "$top.cpd44" "Message3" vTcl:WidgetProc "Toplevel1" 1
    entry $top.cpd45 \
        -background white -insertbackground black -textvariable PARSER 
    vTcl:DefineAlias "$top.cpd45" "Entry3" vTcl:WidgetProc "Toplevel1" 1
    button $top.cpd46 \
        -command {set PARSER [tk_getSaveFile -title "Enregistrer sous"]} \
        -disabledforeground #a7a4a7 -text ... 
    vTcl:DefineAlias "$top.cpd46" "Button5" vTcl:WidgetProc "Toplevel1" 1
    button $top.cpd47 \
        -command {set PARSER "$GRAMMAIRE.parser"} -disabledforeground #a7a4a7 \
        -text Defaut 
    vTcl:DefineAlias "$top.cpd47" "Button6" vTcl:WidgetProc "Toplevel1" 1
    entry $top.cpd48 \
        -background white -insertbackground black -textvariable JOURNAL 
    vTcl:DefineAlias "$top.cpd48" "Entry4" vTcl:WidgetProc "Toplevel1" 1
    message $top.cpd49 \
        -highlightcolor black -padx 5 -pady 2 -text {Fichier journal} \
        -width 96 
    vTcl:DefineAlias "$top.cpd49" "Message4" vTcl:WidgetProc "Toplevel1" 1
    button $top.cpd50 \
        -command {set JOURNAL [tk_getSaveFile -title "Enregistrer sous"]} \
        -disabledforeground #a7a4a7 -text ... 
    vTcl:DefineAlias "$top.cpd50" "Button7" vTcl:WidgetProc "Toplevel1" 1
    button $top.cpd51 \
        \
        -command {#set RESULTATS append $GRAMMAIRE ".parser"
set JOURNAL "$GRAMMAIRE.compile.log"} \
        -disabledforeground #a7a4a7 -text Defaut 
    vTcl:DefineAlias "$top.cpd51" "Button8" vTcl:WidgetProc "Toplevel1" 1
    button $top.but52 \
        \
        -command {set TMP [file dirname $DYACC]
#tk_messageBox -message $TMP
regsub "/bin" $TMP "/include" DP
#tk_messageBox -message $DP
set INCLUDES "$DP/DyALog"
#tk_messageBox -message $INCLUDES
cd [file dirname $GRAMMAIRE]
print_mess FENETRE_MESSAGE "Compilation en cours....(patientez quelques instansts) \n"
print_mess FENETRE_MESSAGE "Execution de la commande : \n $DYACC -parse -verbose_tag $GRAMMAIRE -o $PARSER -I ./ -I $INCLUDES \n"
print_mess FENETRE_MESSAGE [catch {exec $DYACC -parse -verbose_tag $GRAMMAIRE -o $PARSER -I ./ -I $INCLUDES 2> $JOURNAL} VARRETOUR]
print_mess FENETRE_MESSAGE [cat $JOURNAL]
print_mess FENETRE_MESSAGE "\n"} \
        -disabledforeground #a7a4a7 -text {Compiler la grammaire} 
    vTcl:DefineAlias "$top.but52" "Button9" vTcl:WidgetProc "Toplevel1" 1
    button $top.cpd53 \
        \
        -command {print_mess FENETRE_MESSAGE "Analyse syntaxique \n"
print_mess FENETRE_MESSAGE "Execution de la commande : \n echo $PHRASE_A_PARSER | $LEXER -word | $PARSER - -forest \n"
exec echo $PHRASE_A_PARSER | $LEXER -word | $PARSER - -forest > ./parse.resultats
print_mess FENETRE_MESSAGE [exec cat ./parse.resultats]
print_mess FENETRE_MESSAGE "\n"} \
        -disabledforeground #a7a4a7 -text {Analyser la phrase} 
    vTcl:DefineAlias "$top.cpd53" "Button10" vTcl:WidgetProc "Toplevel1" 1
    message $top.cpd54 \
        -highlightcolor black -padx 5 -pady 2 -text Lexer -width 150 
    vTcl:DefineAlias "$top.cpd54" "Message5" vTcl:WidgetProc "Toplevel1" 1
    entry $top.cpd55 \
        -background white -insertbackground black -textvariable LEXER 
    vTcl:DefineAlias "$top.cpd55" "Entry5" vTcl:WidgetProc "Toplevel1" 1
    button $top.cpd56 \
        \
        -command {set LEXER [tk_getOpenFile -title "Choisissez votre lexer" -initialdir .]} \
        -disabledforeground #a7a4a7 -text ... 
    vTcl:DefineAlias "$top.cpd56" "Button11" vTcl:WidgetProc "Toplevel1" 1
    button $top.cpd57 \
        \
        -command {set ZEDIR [file dirname $GRAMMAIRE]
set LEXER "$ZEDIR/lex2db"} \
        -disabledforeground #a7a4a7 -text Defaut 
    vTcl:DefineAlias "$top.cpd57" "Button12" vTcl:WidgetProc "Toplevel1" 1
    button $top.but58 \
        -command {efface FENETRE_MESSAGE} -disabledforeground #a7a4a7 \
        -text Effacer 
    vTcl:DefineAlias "$top.but58" "Button13" vTcl:WidgetProc "Toplevel1" 1
    entry $top.cpd30 \
        -background white -insertbackground black \
        -textvariable PHRASE_A_PARSER 
    vTcl:DefineAlias "$top.cpd30" "Entry7" vTcl:WidgetProc "Toplevel1" 1
    message $top.cpd31 \
        -highlightcolor black -padx 5 -pady 2 -text {Phrase � analyser} \
        -width 131 
    vTcl:DefineAlias "$top.cpd31" "Message7" vTcl:WidgetProc "Toplevel1" 1
    button $top.cpd32 \
        -command {set PHRASE_A_PARSER "Jean mange"} \
        -disabledforeground #a7a4a7 -text Defaut 
    vTcl:DefineAlias "$top.cpd32" "Button15" vTcl:WidgetProc "Toplevel1" 1
    message $top.cpd58 \
        -font {Helvetica -12 {bold italic}} -highlightcolor black -padx 5 \
        -pady 2 -text {Fichiers DyALog} -width 150 
    vTcl:DefineAlias "$top.cpd58" "Message8" vTcl:WidgetProc "Toplevel1" 1
    message $top.cpd59 \
        -font {Helvetica -12 {bold italic}} -highlightcolor black -padx 5 \
        -pady 2 -text {Fichiers r�sultats} -width 150 
    vTcl:DefineAlias "$top.cpd59" "Message9" vTcl:WidgetProc "Toplevel1" 1
    message $top.cpd61 \
        -font {Helvetica -12 {bold italic}} -highlightcolor black -padx 5 \
        -pady 2 -text {Fen�tre des messages} -width 429 
    vTcl:DefineAlias "$top.cpd61" "Message10" vTcl:WidgetProc "Toplevel1" 1
    menu $top.m55 \
        -tearoff 1 
    button $top.cpd60 \
        \
        -command {set GRAMMAIRE ""
set DYACC ""
set LEXER ""
set PARSER ""
set JOURNAL ""
set PHRASE_A_PARSER ""
focus -force .top26.ent31} \
        -disabledforeground #a7a4a7 -text RAZ 
    vTcl:DefineAlias "$top.cpd60" "Button16" vTcl:WidgetProc "Toplevel1" 1
    menubutton $top.men55 \
        -menu "$top.men55.m" -padx 5 -pady 3 -text Fichier 
    vTcl:DefineAlias "$top.men55" "Menubutton1" vTcl:WidgetProc "Toplevel1" 1
    menu $top.men55.m \
        -tearoff 0 
    $top.men55.m add command \
        \
        -command set\ CONFIG_FILE\ \[tk_getOpenFile\ -defaultextension\ \"dya\"\ -title\ \"S�lectionner\ votre\ fichier\ de\ param�tres\ DyALog\"\ -initialfile\ \"*.dya\"\ -initialdir\ \$env(HOME)\]\nif\ \{file\ exists\ \$CONFIG_FILE\ !=\ 1\}\ then\ \{tk_messageBox\ -title\ \"Attention\"\ -message\ \"Fichier\ introuvable\"\}\ else\ \\\n\{set\ HANDLER\ \[open\ \$CONFIG_FILE\ r\]\ \nset\ num_ligne\ 1\nwhile\ \{!eof\ \$HANDLER\}\ \{\nset\ CUR_LINE\ \[gets\ \$HANDLER\]\nif\ \{string\ first\ #\ \$CUR_LINE\ !=\ 1\}\ then\ \{\nset\ FIN_MOT\ \[string\ wordend\ \$CUR_LINE\ 1\]\nset\ MOT\ \[string\ range\ \$CUR_LINE\ 1\ \[expr\ \$FIN_MOT-1\]\]\n\ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \}\n\ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \}\n\} \
        -label {Ouvrir ...} 
    $top.men55.m add command \
        -command {# TODO: Your menu handler here} -label {Enregistrer ...} 
    $top.men55.m add command \
        -command {# TODO: Your menu handler here} \
        -label {Enregistrer sous ...} 
    $top.men55.m add separator \
        
    $top.men55.m add command \
        -command {destroy .top26} -label Quitter 
    menubutton $top.men67 \
        -menu "$top.men67.m" -padx 5 -pady 3 -text Edition 
    vTcl:DefineAlias "$top.men67" "Menubutton2" vTcl:WidgetProc "Toplevel1" 1
    menu $top.men67.m \
        -tearoff 0 
    $top.men67.m add command \
        -command {catch [exec $env(EDITOR) $GRAMMAIRE &]} \
        -label {Editer la grammaire ...} 
    $top.men67.m add command \
        \
        -command {Window show $widget(Toplevel2)
$widget(Radiobutton1) configure -value 4
$widget(Radiobutton2) configure -value 5} \
        -label {Convertir la grammaire ...} 
    menubutton $top.men68 \
        -menu "$top.men68.m" -padx 5 -pady 3 -text Outils 
    vTcl:DefineAlias "$top.men68" "Menubutton3" vTcl:WidgetProc "Toplevel1" 1
    menu $top.men68.m \
        -tearoff 0 
    $top.men68.m add command \
        \
        -command {set TMP [file dirname $DYACC]
#tk_messageBox -message $TMP
regsub "/bin" $TMP "/include" DP
#tk_messageBox -message $DP
set INCLUDES "$DP/DyALog"
#tk_messageBox -message $INCLUDES
cd [file dirname $GRAMMAIRE]
print_mess FENETRE_MESSAGE "Parsing en cours....(patientez quelques instansts) \n"
print_mess FENETRE_MESSAGE "Execution de la commande : \n $DYACC -parse -verbose_tag $GRAMMAIRE -o $PARSER -I ./ -I $INCLUDES \n"
exec $DYACC -parse -verbose_tag $GRAMMAIRE -o $PARSER -I ./ -I $INCLUDES 2> $JOURNAL
print_mess FENETRE_MESSAGE "\n"} \
        -label Compiler 
    $top.men68.m add command \
        \
        -command {print_mess FENETRE_MESSAGE "Analyse syntaxique \n"
print_mess FENETRE_MESSAGE "Execution de la commande : \n echo $PHRASE_A_PARSER | $LEXER -word | $PARSER - -forest \n"
exec echo $PHRASE_A_PARSER | $LEXER -word | $PARSER - -forest > ./parse.resultats
print_mess FENETRE_MESSAGE [exec cat ./parse.resultats]
print_mess FENETRE_MESSAGE "\n"} \
        -label Analyser 
    $top.men68.m add separator \
        
    $top.men68.m add command \
        \
        -command {set GRAMMAIRE ""
set DYACC ""
set LEXER ""
set PARSER ""
set JOURNAL ""
set PHRASE_A_PARSER ""
focus -force .top26.ent31} \
        -label RAZ 
    $top.men68.m add command \
        -command {efface FENETRE_MESSAGE} -label Effacer 
    menubutton $top.men69 \
        -menu "$top.men69.m" -padx 5 -pady 3 -text Aide 
    vTcl:DefineAlias "$top.men69" "Menubutton4" vTcl:WidgetProc "Toplevel1" 1
    menu $top.men69.m \
        -tearoff 0 
    $top.men69.m add command \
        \
        -command {# TODO: Your menu handler here
 tk_messageBox -message "Verifiez bien que votre fichier grammaire contient une requete du type ?-recorded('N',N),tag_phrase(s,0,N)\n et que le fichier header.pl est bien inclu dans votre grammaire\n
Plus d'aide en ligne prochainememnt........\n"} \
        -label {Aide en ligne ...} 
    $top.men69.m add command \
        \
        -command {# TODO: Your menu handler here
tk_messageBox -message "DyAlog (c) Eric Villemonte de La Clergerie INRIA \n http://pauillac.inria.fr/~clerger \n\n DYAFE (c) Djam� Seddah & Yannick Parmentier LORIA\nmail {seddah,parmenti}@loria.fr"} \
        -label {A propos ...} 
    ###################
    # SETTING GEOMETRY
    ###################
    place $top.tex28 \
        -x 5 -y 360 -width 688 -height 325 -anchor nw -bordermode ignore 
    place $top.but30 \
        -x 160 -y 85 -width 31 -height 26 -anchor nw -bordermode ignore 
    place $top.ent31 \
        -x 10 -y 85 -width 148 -height 27 -anchor nw -bordermode ignore 
    place $top.mes37 \
        -x 5 -y 65 -width 110 -height 22 -anchor nw -bordermode ignore 
    place $top.cpd38 \
        -x 10 -y 140 -width 148 -height 27 -anchor nw 
    place $top.cpd39 \
        -x 160 -y 140 -width 31 -height 26 -anchor nw 
    place $top.cpd40 \
        -x 0 -y 120 -width 160 -height 17 -anchor nw 
    place $top.but41 \
        -x 195 -y 85 -anchor nw -bordermode ignore 
    place $top.cpd42 \
        -x 195 -y 140 -width 59 -height 26 -anchor nw 
    place $top.cpd44 \
        -x 355 -y 70 -anchor nw 
    place $top.cpd45 \
        -x 360 -y 90 -width 148 -height 27 -anchor nw 
    place $top.cpd46 \
        -x 510 -y 90 -width 33 -height 26 -anchor nw 
    place $top.cpd47 \
        -x 545 -y 90 -anchor nw 
    place $top.cpd48 \
        -x 360 -y 140 -width 148 -height 27 -anchor nw 
    place $top.cpd49 \
        -x 355 -y 120 -width 96 -height 22 -anchor nw 
    place $top.cpd50 \
        -x 510 -y 140 -width 33 -height 26 -anchor nw 
    place $top.cpd51 \
        -x 545 -y 140 -anchor nw 
    place $top.but52 \
        -x 270 -y 265 -width 161 -height 26 -anchor nw -bordermode ignore 
    place $top.cpd53 \
        -x 270 -y 300 -width 163 -height 26 -anchor nw 
    place $top.cpd54 \
        -x 5 -y 175 -anchor nw 
    place $top.cpd55 \
        -x 10 -y 195 -width 148 -height 27 -anchor nw 
    place $top.cpd56 \
        -x 160 -y 195 -width 31 -height 26 -anchor nw 
    place $top.cpd57 \
        -x 195 -y 195 -width 59 -height 26 -anchor nw 
    place $top.but58 \
        -x 595 -y 330 -width 95 -height 26 -anchor nw -bordermode ignore 
    place $top.cpd30 \
        -x 360 -y 195 -width 173 -height 27 -anchor nw 
    place $top.cpd31 \
        -x 350 -y 175 -width 131 -height 22 -anchor nw 
    place $top.cpd32 \
        -x 540 -y 195 -anchor nw 
    place $top.cpd58 \
        -x 10 -y 40 -anchor nw 
    place $top.cpd59 \
        -x 355 -y 45 -anchor nw 
    place $top.cpd61 \
        -x 135 -y 335 -width 429 -height 22 -anchor nw 
    place $top.cpd60 \
        -x 595 -y 235 -width 90 -height 26 -anchor nw 
    place $top.men55 \
        -x 5 -y 5 -width 61 -height 24 -anchor nw -bordermode ignore 
    place $top.men67 \
        -x 70 -y 5 -width 61 -height 24 -anchor nw -bordermode ignore 
    place $top.men68 \
        -x 130 -y 5 -width 61 -height 24 -anchor nw -bordermode ignore 
    place $top.men69 \
        -x 645 -y 5 -anchor nw -bordermode ignore 

    vTcl:FireEvent $base <<Ready>>
}

proc vTclWindow.top55 {base} {
    if {$base == ""} {
        set base .top55
    }
    if {[winfo exists $base]} {
        wm deiconify $base; return
    }
    set top $base
    ###################
    # CREATING WIDGETS
    ###################
    vTcl:toplevel $top -class Toplevel \
        -highlightcolor black 
    wm withdraw $top
    wm focusmodel $top passive
    wm geometry $top 278x301+542+441; update
    wm maxsize $top 1265 994
    wm minsize $top 1 1
    wm overrideredirect $top 0
    wm resizable $top 1 1
    wm title $top "Conversion de grammaire"
    vTcl:DefineAlias "$top" "Toplevel2" vTcl:Toplevel:WidgetProc "" 1
    bindtags $top "$top Toplevel all _TopLevel"
    vTcl:FireEvent $top <<Create>>
    wm protocol $top WM_DELETE_WINDOW "vTcl:FireEvent $top <<DeleteWindow>>"

    message $top.mes56 \
        -highlightcolor black -padx 5 -pady 2 \
        -text {Emplacement de tagconverter} -width 202 
    vTcl:DefineAlias "$top.mes56" "Message1" vTcl:WidgetProc "Toplevel2" 1
    entry $top.ent57 \
        -background white -insertbackground black -textvariable TAG_CONVERTER 
    vTcl:DefineAlias "$top.ent57" "Entry1" vTcl:WidgetProc "Toplevel2" 1
    button $top.but58 \
        \
        -command {set TAG_CONVERTER [tk_getOpenFile -title "Emplacement du tag_converter" -initialdir $env(HOME)]} \
        -text ... 
    vTcl:DefineAlias "$top.but58" "Button1" vTcl:WidgetProc "Toplevel2" 1
    button $top.but59 \
        \
        -command {set env(PERL5LIB) "/local/led/linux/perl:/local/led/linux/perl/lib:/local/led/linux/perl/lib/5.6.0:/local/led/linux/perl/lib/5.6.0/i686-linux"
set TAG_CONVERTER "/local/led/linux/perl/bin/tag_converter"} \
        -text LORIA 
    vTcl:DefineAlias "$top.but59" "Button2" vTcl:WidgetProc "Toplevel2" 1
    message $top.mes60 \
        -highlightcolor black -padx 5 -pady 2 -text {Grammaire convertie} \
        -width 143 
    vTcl:DefineAlias "$top.mes60" "Message2" vTcl:WidgetProc "Toplevel2" 1
    entry $top.ent61 \
        -background white -insertbackground black \
        -textvariable GRAMMAIRE_SORTIE 
    vTcl:DefineAlias "$top.ent61" "Entry2" vTcl:WidgetProc "Toplevel2" 1
    button $top.but62 \
        \
        -command {set GRAMMAIRE_SORTIE [tk_getSaveFile -title "Enregistrer la grammaire sous"]} \
        -text ... 
    vTcl:DefineAlias "$top.but62" "Button3" vTcl:WidgetProc "Toplevel2" 1
    button $top.but63 \
        \
        -command if\ \{\$VERS_TAG\ ==\ 1\}\ then\ \{\[regsub\ \".xml\"\ \$GRAMMAIRE_ENTREE\ \".tag\"\ GRAMMAIRE_SORTIE\]\}\ elseif\ \\\n\{\$VERS_TAG\ ==\ 0\}\ then\ \{\[regsub\ \".tag\"\ \$GRAMMAIRE_ENTREE\ \".xml\"\ GRAMMAIRE_SORTIE\]\}\ else\ \\\n\{tk_messageBox\ -message\ \"Oups\"\} \
        -text Defaut 
    vTcl:DefineAlias "$top.but63" "Button4" vTcl:WidgetProc "Toplevel2" 1
    button $top.but64 \
        \
        -command upvar\ #0\ GRAMMAIRE\ LA_GRAMMAIRE\ncatch\ \[exec\ \$TAG_CONVERTER\ \$GRAMMAIRE_ENTREE\ -o\ \$GRAMMAIRE_SORTIE\]\ntk_messageBox\ -message\ \$VERS_TAG\nif\ \{\$VERS_TAG\ ==\ 1\}\ then\ \{set\ LA_GRAMMAIRE\ \$GRAMMAIRE_SORTIE\}\ \\\nelseif\ \{\$VERS_TAG\ ==\ 0\}\ then\ \{set\ LA_GRAMMAIRE\ \$GRAMMAIRE_ENTREE\}\ \n#\ else\ \{tk_messageBox\ -message\ \"Veuillez\ choisir\ un\ type\ de\ conversion\"\}\nset\ TAG_CONVERTER\ \"\"\nset\ GRAMMAIRE_ENTREE\ \"\"\nset\ GRAMMAIRE_SORTIE\ \"\"\nset\ VERS_TAG\ \"\"\n\$widget(Radiobutton1)\ configure\ -value\ 4\n\$widget(Radiobutton2)\ configure\ -value\ 5\nfocus\ -force\ .top55.ent57\nWindow\ hide\ \$widget(Toplevel2) \
        -text convertir 
    vTcl:DefineAlias "$top.but64" "Button5" vTcl:WidgetProc "Toplevel2" 1
    button $top.but65 \
        \
        -command {set TAG_CONVERTER ""
set GRAMMAIRE_ENTREE ""
set GRAMMAIRE_SORTIE ""
set VERS_TAG ""
$widget(Radiobutton1) configure -value 4
$widget(Radiobutton2) configure -value 5
focus -force .top55.ent57
wm withdraw $widget(Toplevel2)} \
        -text annuler 
    vTcl:DefineAlias "$top.but65" "Button6" vTcl:WidgetProc "Toplevel2" 1
    message $top.mes66 \
        -highlightcolor black -padx 5 -pady 2 -text {Grammaire en entr�e} \
        -width 142 
    vTcl:DefineAlias "$top.mes66" "Message3" vTcl:WidgetProc "Toplevel2" 1
    entry $top.ent67 \
        -background white -insertbackground black \
        -textvariable GRAMMAIRE_ENTREE 
    vTcl:DefineAlias "$top.ent67" "Entry3" vTcl:WidgetProc "Toplevel2" 1
    button $top.but68 \
        \
        -command if\ \{\$VERS_TAG\ ==\ 1\}\ then\ \{set\ GRAMMAIRE_ENTREE\ \[tk_getOpenFile\ -defaultextension\ \"xml\"\ -title\ \"Emplacement\ de\ la\ grammaire\"\ -initialdir\ \$env(HOME)\]\}\ elseif\ \\\n\{\$VERS_TAG\ ==\ 0\}\ then\ \{set\ GRAMMAIRE_ENTREE\ \[tk_getOpenFile\ -defaultextension\ \"tag\"\ -title\ \"Emplacement\ de\ la\ grammaire\"\ -initialdir\ \$env(HOME)\]\}\ else\ \\\n\{tk_messageBox\ -message\ \"Veuillez\ choisir\ un\ type\ de\ conversion\"\} \
        -text ... 
    vTcl:DefineAlias "$top.but68" "Button7" vTcl:WidgetProc "Toplevel2" 1
    radiobutton $top.rad56 \
        -command {set VERS_TAG 1} -cursor fleur -text {xml (tagML) -> tag} \
        -value 4 -variable FLAG 
    vTcl:DefineAlias "$top.rad56" "Radiobutton1" vTcl:WidgetProc "Toplevel2" 1
    radiobutton $top.rad57 \
        -command {set VERS_TAG 0} -text {tag -> xml(tagML)} -value 5 \
        -variable FLAG 
    vTcl:DefineAlias "$top.rad57" "Radiobutton2" vTcl:WidgetProc "Toplevel2" 1
    message $top.mes61 \
        -highlightcolor black -padx 5 -pady 2 -text {Type de conversion :} \
        -width 127 
    vTcl:DefineAlias "$top.mes61" "Message4" vTcl:WidgetProc "Toplevel2" 1
    ###################
    # SETTING GEOMETRY
    ###################
    place $top.mes56 \
        -x 5 -y 5 -width 202 -height 19 -anchor nw -bordermode ignore 
    place $top.ent57 \
        -x 15 -y 30 -anchor nw -bordermode ignore 
    place $top.but58 \
        -x 170 -y 30 -width 24 -height 21 -anchor nw -bordermode ignore 
    place $top.but59 \
        -x 200 -y 30 -width 59 -height 21 -anchor nw -bordermode ignore 
    place $top.mes60 \
        -x 15 -y 200 -width 143 -height 22 -anchor nw -bordermode ignore 
    place $top.ent61 \
        -x 15 -y 225 -anchor nw -bordermode ignore 
    place $top.but62 \
        -x 170 -y 225 -width 24 -height 21 -anchor nw -bordermode ignore 
    place $top.but63 \
        -x 200 -y 225 -width 59 -height 21 -anchor nw -bordermode ignore 
    place $top.but64 \
        -x 105 -y 265 -anchor nw -bordermode ignore 
    place $top.but65 \
        -x 190 -y 265 -width 74 -height 26 -anchor nw -bordermode ignore 
    place $top.mes66 \
        -x 15 -y 150 -width 142 -height 22 -anchor nw -bordermode ignore 
    place $top.ent67 \
        -x 15 -y 175 -anchor nw -bordermode ignore 
    place $top.but68 \
        -x 170 -y 175 -width 24 -height 21 -anchor nw -bordermode ignore 
    place $top.rad56 \
        -x 20 -y 90 -anchor nw -bordermode ignore 
    place $top.rad57 \
        -x 15 -y 120 -width 148 -height 22 -anchor nw -bordermode ignore 
    place $top.mes61 \
        -x 15 -y 60 -width 127 -height 22 -anchor nw -bordermode ignore 

    vTcl:FireEvent $base <<Ready>>
}

#############################################################################
## Binding tag:  _TopLevel

bind "_TopLevel" <<Create>> {
    if {![info exists _topcount]} {set _topcount 0}; incr _topcount
}
bind "_TopLevel" <<DeleteWindow>> {
    if {[set ::%W::_modal]} {
                vTcl:Toplevel:WidgetProc %W endmodal
            } else {
                destroy %W; if {$_topcount == 0} {exit}
            }
}
bind "_TopLevel" <Destroy> {
    if {[winfo toplevel %W] == "%W"} {incr _topcount -1}
}

Window show .
Window show .top26
Window show .top55

main $argc $argv

