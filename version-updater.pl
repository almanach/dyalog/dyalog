#!/usr/local/bin/perl

use POSIX qw/access/;
use File::Copy cp;
#use Net::SFTP;

my $version=shift;
my $distfile="DyALog-$version.tar.gz";
my $HOME=$ENV{'HOME'};

access($distfile,&POSIX::R_OK) || die "No distribution file $distfile for version $version";


my $MyPackages="$HOME/MyPackages";

print "Copying $distfile to $MyPackages\n";
cp("$distfile", "$MyPackages/$distfile");

#print "Ftp_put $distfile to beaune (don't forget to set ssh-agent)\n";
#my $sftp = Net::SFTP->new('beaune.inria.fr','ssh_args'=> [ port => 22 ] );
#$sftp->put("$distfile","/home/beaune/atoll3/ftp/Eric.Clergerie/DyALog/$distfile");

print "Copying $distfile to ftpdir $HOME/ftpdir/DyALog\n";
cp("$distfile", "$HOME/ftpdir/DyALog/$distfile");

print "Copying DyALog NEWS to $HOME/public_html/NEWS\n";
cp("NEWS", "$HOME/public_html/DyALog/NEWS");

@files_to_update = (
		    "$HOME/public_html/work.html",
		    "$HOME/atoll-www/logiciels.html",
		    "$HOME/atoll-www/logiciels-en.html",
		    );

foreach my $file (@files_to_update) {
    update_html_file($version,$file);
}

sub callback {
    my($sftp, $data, $offset, $size) = @_;
    print "Wrote $offset / $size bytes\r";
}

sub update_html_file {
    my $version = shift;
    my $file = shift;

    print "Updating $file\n";
    open(FILE,"<$file") || die "can't open file $file";
    my $content='';
    $/="";

    while(<FILE>) {
	s/(<SPAN\s+CLASS=\"DyALog-Version\">)\d+\.\d+.\d+/\1$version/og;
	$content .= $_;
    }

    close FILE;

    rename($file,"$file.bak") || die "can't rename $file";
    open(FILE,">$file") || die "can't open $file";
    print FILE $content;

    close FILE;
}

# copy RPM

# update WEB pages

1;
