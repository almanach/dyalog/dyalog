/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 1999, 2004 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  emit.pl -- 
 *
 * ----------------------------------------------------------------
 * Description
 *   Emitting of desc file associated to a TFS hierarchy
 * ----------------------------------------------------------------
 */

:-include 'header.pl'.

emit :-
	current_output(Old_Stream),
        ( recorded( output_file(File) ) ->
            absolute_file_name(File,AF),
            open(AF,write,Stream),
            set_output(Stream)
        ;   
            true
        ),

	emit_header,

	emit_all_types,
	wait(emit_subtypes ),
	wait(emit_escaped_types),
	emit_all_traits,
	wait(emit_intro),
	wait(emit_skel),
	wait(emit_escape),
	wait(emit_unif),
	wait(emit_subs),

	set_output(Old_Stream)
	.

:-prolog [ emit_header,
	   emit_all_types,
	   emit_subtypes,
	   emit_all_traits,
	   emit_intro,
	   emit_skel,
	   emit_unif,
	   emit_subs,
	   emit_escape,
	   emit_escaped_types
	 ].

emit_header :-
        date_time(Year,Month,Day,Hour,Min,Sec),
	format( ';; Tfs Desc file generated from ', [] ),
	every( ( recorded( tfs_file(File) ), format( ' ~w',[File])) ),
	nl,
	format( ';; Date    : ~w ~w ~w at ~w:~w:~w\n\n',[Month,Day,Year,Hour,Min,Sec])
	.

emit_all_types :-
	write( 'TYPES =' ),
	every( (r_tfs_type(T), format(' ~w',[T]) ) ),
	nl
	.

emit_subtypes :-
	r_tfs_type(Type),
	( recorded_answer( closure_subtypes(Type,_) ) ->
	    format( 'SUBTYPES ~w =',[Type]),
	    every( ( recorded_answer( closure_subtypes(Type,SubType))
		   , format( ' ~w', [SubType] )
		   )),
	    nl
	;   true
	)
	.

emit_escaped_types :-
	r_tfs_escape(Type,Escape),
	format( 'XTYPE ~w = ~w\n',[Type,Escape])
	.

emit_all_traits :-
	write( 'FEATURES =' ),
	every( (r_tfs_feature(F), format(' ~w',[F]) ) ),
	nl
	.

emit_intro :-
	r_tfs_intro(Type,F,_),
	\+ (   recorded_answer( closure_subtypes(T,Type ) )
	   ,   r_tfs_intro(T,F,_)
	   ),
	format( 'INTRO ~w = ~w\n', [F,Type] )
	.

emit_skel :-
	r_tfs_type( Type ),
	format( 'SKEL ~w =',[Type]),
	emit_skel_features(Type,1)
	.

:-prolog emit_skel_features/2.

emit_skel_features(Type,I) :-
	( recorded_answer( closure_feature(Type,F,T,I) ) ->
	    format( ' ~w:~w', [F,T] ),
	    J is I+1,
	    emit_skel_features(Type,J)
	;
	    nl
	)
	.

emit_unif :-
	recorded_answer( mgu(TA,TB,T) ),
	TA @=< TB,
	format( 'UNIF ~w ~w = ~w\n', [TA,TB,T]),
	every( ( recorded_answer( closure_feature(T,F,FT,IFT) )
	       , emit_unif_feature(TA,TB,T,F,FT,IFT)
	       ))
	.

:-prolog emit_unif_feature/6.

emit_unif_feature(TA,TB,T,F,FT, IFT) :-
	( recorded_answer( closure_feature(TA,F,FTA,IFTA) )
	, recorded_answer( closure_feature(TB,F,FTB,IFTB) ) ->
	    ( recorded_answer( mgu(FTA,FTB,FT) ) -> OP='DIRECT' ; OP='INDIRECT' ),
	    format( '+\tUNIF ~w LEFT ~w RIGHT ~w ~w ~w\n', [F,IFTA,IFTB,OP,IFT] )
	;   recorded_answer( closure_feature(TA,F,FTA,IFTA) ) ->
	    ( FTA == FT -> OP='DIRECT' ; OP='INDIRECT' ),
	    format( '+\tINHERIT ~w LEFT ~w ~w ~w\n',[F,IFTA,OP,IFT] )
	;   recorded_answer( closure_feature(TB,F,FTB,IFTB) ) ->
	    ( FTB == FT -> OP='DIRECT' ; OP='INDIRECT' ),
	    format( '+\tINHERIT ~w RIGHT ~w ~w ~w\n',[F,IFTB,OP,IFT] )
	;
	    format( '+\tNEW ~w ~w\n', [F,IFT] )
	)
	.
	    
emit_subs :-
	recorded_answer( closure_subtypes(Type,SubType) ),
	format( 'SUBS ~w ~w =\n', [Type,SubType]),
	every( ( recorded_answer( closure_feature(SubType,F,_,IFT) )
	       , recorded_answer( closure_feature(Type,F,_,IFTA) )
	       , format( '+\tCHECK ~w ~w ~w\n', [F,IFTA,IFT] )
	       ))
	.
	
emit_escape :-
	r_tfs_escape(Escape),
	( r_tfs_escape(_,Escape) ->
	    format( 'ESCAPE ~w =',[Escape]),
	    every( ( recorded_answer( closure_escape(Escape,Type) ),
		       format( ' ~w', [Type] ))),
	    nl
	;
	    true
	)
	.

