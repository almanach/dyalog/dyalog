/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 1998 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  reader.pl -- 
 *
 * ----------------------------------------------------------------
 * Description
 *    Read description of a TFS hierarchy and performs prelimary checks
 *    ( essentially syntactic checks )
 * ----------------------------------------------------------------
 */

:-include 'header.pl'.

:-op( 500, xfy, [sub,intro,escape] ).

reader( File ) :-
	( find_file(File,AFile) xor error('File not found ~w',[File]) ),
	( open(AFile,read,S) xor error('Could not open file ~w',[AFile] ) ),
	repeat(
	       (   read_term(S,T,_),
		   analyze(T),
		   T == eof
	       )
	      ),
	close(S)
	.

:-std_prolog analyze/1.

analyze(T) :-
	( T == eof ->
	    true
	;   T = Type sub SubTypes intro Features ->
	    tfs_type(Type),
	    tfs_subtypes(Type,SubTypes),
	    tfs_features(Type,Features)
	;   T = Type sub SubTypes ->
	    tfs_type(Type),
	    tfs_subtypes(Type,SubTypes)
	;   T = Type escape Escape ->
	    tfs_type(Type),
	    tfs_escape(Type,Escape)
	;
	    error( 'Type definition expected ~w',[T] )
	)
	.


:-std_prolog tfs_type/1,check_tfs_type/1.

tfs_type( Type ) :-
	check_tfs_type( Type),
	( r_tfs_type( Type ) ->
	    warning( 'Type ~w already defined', [Type] )
	;   
	    record( r_tfs_type( Type) )
	)
	.
		  

check_tfs_type( Type ) :-
	( atom(Type) ->
	    true
	;   
	    error( 'Symbol expected as type ~w', [Type] )
	)
	.

:-std_prolog tfs_subtypes/2.

tfs_subtypes( Type, SubTypes ) :-
	( SubTypes == [] ->
	    true
	;   SubTypes = [Head|Tail] ->
	    check_tfs_type(Head),  %% do not test existence of Head
	    ( r_tfs_subtype(Type,Head) ->
		warning( 'Subtype ~w of ~w already defined', [Head,Type])
	    ;	
		record( r_tfs_subtype(Type,Head) )
	    ),
	    tfs_subtypes(Type,Tail)
	;   
	    error( 'Subtype list expected ~w',[SubTypes])
	)
	.

:-std_prolog tfs_features/2,check_tfs_feature/1.

tfs_features( Type, Features ) :-
	( Features == [] ->
	    true
	;   Features = [ FT | Tail ] , decompose_feature(FT,F,T) ->
	    check_tfs_feature(F),
	    check_tfs_type(T),
	    ( r_tfs_intro(Type,F,T1) ->
		warning( 'Feature ~w of ~w with type conflict old ~w / new ~w',[F,Type,T1,T])
	    ;	
		record( r_tfs_intro(Type, F, T ) )
	    ),
	    tfs_features( Type, Tail )
	;   
	    error( 'Feature list expected ~w',[Features])
	)
	.

:-std_prolog decompose_feature/3.

decompose_feature(FT,F,T) :- ( FT = F:T xor (FT=F, T='$untyped')).

check_tfs_feature(Feature) :-
	( atom(Feature) ->
	    ( r_tfs_feature(Feature) -> true ; record(r_tfs_feature(Feature)))
	;
	    error( 'Symbol expected as feature ~w', [Feature] )
	)
	.

:-std_prolog tfs_escape/2.

tfs_escape(Type,Escape) :-
	(r_tfs_escape(Escape) xor error( '~w is not an acceptable escape',[Escape])),
	( r_tfs_escape( Type, Escape ) ->
	    warning( 'Type ~w already escaped to ~w',[Type,Escape])
	;   
	    record( r_tfs_escape(Type,Escape) )
	)
	.


