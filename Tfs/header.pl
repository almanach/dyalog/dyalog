/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 1999 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  header.pl -- 
 *
 * ----------------------------------------------------------------
 * Description
 *  Header file to be included by the files of the TFS distribution 
 * ----------------------------------------------------------------
 */

:-prolog
	reader/1,
	checker,
	emit
	.

:-extensional
	r_tfs_type/1,
	r_tfs_subtype/2,
	r_tfs_intro/3,
	r_tfs_feature/1,
	r_tfs_escape/1,
	r_tfs_escape/2
	.

:-require 'format.pl'.


