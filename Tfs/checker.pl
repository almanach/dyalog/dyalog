/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 1998, 2004 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  checker.pl -- 
 *
 * ----------------------------------------------------------------
 * Description
 *    Check full coherence of a TFS hierarchy and compute different
 *    closures ( on subtypes and features )
 * ----------------------------------------------------------------
 */

:-include 'header.pl'.

checker :-
	wait( default_types ),
	wait( closure_subtypes(_,_) ),
	wait( check_types ),
	wait( inherit_feature(_,_,_) ),
	wait( closure_feature(_,_,_,_) ),
	wait( closure_indirect_feature(_,_) ),
	wait( check_features ),
	wait( check_escape ),
	wait( closure_escape(_,_) )
	.

%% Default Types
%% Define type declared by sub or intro but not really defined

default_types :-
	( r_tfs_subtype(_,T) ; r_tfs_intro(_,_,T) ),
	\+ r_tfs_type(T),
	\+ T== '$untyped',
	warning( 'No definition for declared type ~w',[T]),
	record( r_tfs_type(T) )
	.

%% Check Types
%% -----------
%% Errors:
%%    2 No type is its own subtype by transitive closure
%%    3 No pair of types has more than one mgu
%% Warning:
%%    4 Type 'bot' declared
%%    5 Every type but bot is a proper subtype 

%% E2
check_types :-
	closure_subtypes(Type,SubType),
	Type == SubType,
	error( 'Type ~w is circular', [Type] )
	.

%% E3
check_types :-
	mgu(TA,TB,T1),
	mgu(TA,TB,T2),
	T1 \== T2,
	error( 'The pair (~w,~w) accepts mgu ~w and ~w', [TA,TB,T1,T2] )
	.

%% W4
check_types :-
	\+ r_tfs_type( bot ),
	warning( 'No definition for root type bot', [] )
	.

%% W5
check_types :-
	r_tfs_type(Type),
	\+ Type == bot,
	\+ r_tfs_subtype(_,Type),
	warning( 'Type ~w is not a proper subtype',[Type])
	.

closure_subtypes(Type,Subtype) :-
	r_tfs_subtype(Type,T),
	( T=Subtype ; closure_subtypes(T,Subtype) )
	.

mgu(T,T,T) :- r_tfs_type(T).
	
mgu(TA,TB,T) :-
	r_tfs_type(TA),
	r_tfs_type(TB),
	\+ TA == TB,
	( recorded_answer( closure_subtypes(TA,TB) ) ->
	    T=TB
	;   recorded_answer( closure_subtypes(TB,TA) ) ->
	    T=TA
	;   
	    recorded_answer( closure_subtypes(TA,T) ),
	    recorded_answer( closure_subtypes(TB,T) ),
	    iterate( New^(New=[],TT^Old^(New=[TT|Old])),
		     (	 recorded_answer( closure_subtypes(TT,T) )
		     , recorded_answer( closure_subtypes(TA,TT) )
		     , recorded_answer( closure_subtypes(TB,TT) )
		     )
		   ),
	    New = []
	)
	.

%% Check Features
%% --------------
%% Errors:
%%    2 <t0 F1:t1> .. <tn-1 Fn:tn> => t1 \== tn
%%    3 <T1 F:t1> <T2 F:t2> T1 *> T2 => t1 *> t2
%%    4 <T1 F:t1> <T2 F:t2> => Exists <T F:t>, T *> T1  and T *> T2
%% Warnings:
%%    5 <T1 F:t1> <T2 F:t2> T=mgu(T1,T2) <T F:t> => t=mgu(t1,t2)

%% E3
inherit_feature(SubType,F,T1) :-
	r_tfs_subtype(Type,SubType),
	( r_tfs_intro(Type,F,T1) ; inherit_feature(Type,F,T1) ),
	( r_tfs_intro(SubType,F,T) ->
	    \+ (T1==T ; recorded_answer( closure_subtypes(T1,T) )),
	    error( 'Bad restriction to type ~w for feature ~w in type ~w' ,
		    [T,F,SubType])
	;
	    true
	)
	.

closure_feature(Type,F,T,I) :-
	r_tfs_intro(Type,F,T),
	assign_index(Type,F,1,I)
	.

closure_feature(Type,F,T,I) :-
	r_tfs_type(Type),
	group_by( inherit_feature(Type,F,HT)
		, [F]
		, T^HT^( Old^inherited_feature_update(Type,F,Old,HT,T), T=HT )
		),
	assign_index(Type,F,1,I)
	.

closure_indirect_feature(T1,T2) :-
	closure_feature(T1,_,T2,_).

closure_indirect_feature(T1,T3) :-
	closure_feature(T1,_,T2,_),
	closure_indirect_feature(T2,T3)
	.
	
:-prolog feature_update/5.

inherited_feature_update(Type,F,T1,T2,T12) :-
	( T1=T2 ->
	    T12=T1
	;   recorded_answer( mgu(T1,T2,T12) ) ->
	    warning( 'Type for inherited feature ~w in ~w shifted from ~w to ~w',
		     [F,Type,T1,T12] )
	;   
	    error( 'No possible type for inherited feature ~w in ~w', [F,Type] )
	)
	.

:-prolog assign_index/3.

assign_index(Type,F,I,J) :-
	( recorded_answer( closure_feature(Type,F,_,K) ) -> J=K
	; recorded_answer( closure_feature(Type,_,_,I) ) ->
	    K is I+1,
	    assign_index(Type,F,K,J)
	;
	    J=I
	)
	.

%% E2
check_features :-
	closure_indirect_feature(Type,T),
	Type == T,
	error( 'Circularity in type ~w through features',[Type])
	.

%% E4
check_features :-
	r_tfs_intro(T1,F,_),
	r_tfs_intro(T2,F,_),
	T1 \== T2,
	\+ recorded_answer( closure_subtypes(T1,T2) ),
	\+ recorded_answer( closure_subtypes(T2,T1) ),
	\+ (   recorded_answer( closure_subtypes(T,T1) )
	   ,  recorded_answer( closure_subtypes(T,T2) )
	   ,  r_tfs_intro(T,F,_)
	   ),
	error( 'Most general intro type not found for feature ~w',[F] )
	.

%% W5
check_features :-
	closure_feature(T1,F,FT1,_),
	closure_feature(T2,F,FT2,_),
	T1 @< T2,
	mgu(T1,T2,T),
	mgu(FT1,FT2,UFT),
	closure_feature(T,F,FT,_),
	recorded_answer( closure_subtypes(UFT,FT) ),
	warning( 'Weak Unification for feature ~w on types ~w, ~w, and ~w',
		 [F,T1,T2,T])
	.

%% Check Escape
%% --------------
%% Errors:
%%    X1 No more than one escape per escaped type
%%    X2 No subtype for escaped type
%%    X3 No feature for escaped type

%% E X1
check_escape :-
	r_tfs_escape(T,X1),
	r_tfs_escape(T,X2),
	X1 \== X2,
	error( 'two distinct escape ~w and ~w for type ~w',[X1,X2,T])
	.

%% E X2
check_escape :-
	r_tfs_escape(T,_),
	r_tfs_subtype(T,_),
	error( 'escaped type ~w has subtypes',[T])
	.

%% E X3
check_escape :-
	r_tfs_escape(T,_),
	closure_feature(T,_,_,_),
	error( 'escaped type ~w has features', [T])
	.


closure_escape(Escape,Type) :-
	r_tfs_escape(SubType,Escape),
	(   Type=SubType
	;   recorded_answer( closure_subtypes(Type,SubType) )
	)
	.
