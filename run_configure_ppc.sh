#!/bin/sh

# usage

if test "$1" = "-h"; then
    echo "Lance la configuration de DyALog en mode PPC"
    echo "sh run_configure.sh [-tout]"
    echo " -tout : run the whole aclocal, autoheader toolchain"
    echo " Don't modify the optimization flags ! it's not that important anyway"
    exit 0;
fi    

#  lancer avec l'option -tout pour le premier run
# imperatif : derniere version d'autotools installe et dans /usr/local/ si possible sinon modifier les
# chemins plus bas

if test "$1" = "-tout"; then
	/usr/local/bin/aclocal
	/usr/local/bin/autoheader
	/usr/local/bin/glibtoolize -c -f
	/usr/local/bin/automake --add-missing
	/usr/local/bin/autoconf
fi



./configure --prefix=/usr/local --with-libgc-inc=/usr/local/include --with-libgc-lib=/usr/local/lib --program-transform-name=s/libtool/glibtool/ \
"LDFLAGS=-Wl,-read_only_relocs,warning,-L/usr/local/lib,-lgc" \
"CC=gcc -g -O0  -pipe -ffixed-r15 -ffixed-r16 -ffixed-r17 -flat_namespace -fsigned-char -fsigned-bitfields"
