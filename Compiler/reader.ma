;; Compiler: DyALog 1.14.0
;; File "reader.pl"


;;------------------ LOADING ------------

c_code local load
	call_c   Dyam_Loading_Set()
	pl_call  fun67(&seed[97],0)
	pl_call  fun67(&seed[98],0)
	pl_call  fun67(&seed[103],0)
	pl_call  fun67(&seed[33],0)
	pl_call  fun67(&seed[42],0)
	pl_call  fun67(&seed[34],0)
	pl_call  fun67(&seed[99],0)
	pl_call  fun67(&seed[71],0)
	pl_call  fun67(&seed[72],0)
	pl_call  fun67(&seed[49],0)
	pl_call  fun67(&seed[27],0)
	pl_call  fun67(&seed[40],0)
	pl_call  fun67(&seed[39],0)
	pl_call  fun67(&seed[38],0)
	pl_call  fun67(&seed[37],0)
	pl_call  fun67(&seed[76],0)
	pl_call  fun67(&seed[100],0)
	pl_call  fun67(&seed[55],0)
	pl_call  fun67(&seed[54],0)
	pl_call  fun67(&seed[32],0)
	pl_call  fun67(&seed[101],0)
	pl_call  fun67(&seed[35],0)
	pl_call  fun67(&seed[70],0)
	pl_call  fun67(&seed[53],0)
	pl_call  fun67(&seed[21],0)
	pl_call  fun67(&seed[20],0)
	pl_call  fun67(&seed[18],0)
	pl_call  fun67(&seed[30],0)
	pl_call  fun67(&seed[23],0)
	pl_call  fun67(&seed[56],0)
	pl_call  fun67(&seed[68],0)
	pl_call  fun67(&seed[50],0)
	pl_call  fun67(&seed[88],0)
	pl_call  fun67(&seed[73],0)
	pl_call  fun67(&seed[60],0)
	pl_call  fun67(&seed[31],0)
	pl_call  fun67(&seed[66],0)
	pl_call  fun67(&seed[47],0)
	pl_call  fun67(&seed[86],0)
	pl_call  fun67(&seed[69],0)
	pl_call  fun67(&seed[57],0)
	pl_call  fun67(&seed[26],0)
	pl_call  fun67(&seed[77],0)
	pl_call  fun67(&seed[51],0)
	pl_call  fun67(&seed[17],0)
	pl_call  fun67(&seed[19],0)
	pl_call  fun67(&seed[43],0)
	pl_call  fun67(&seed[36],0)
	pl_call  fun67(&seed[29],0)
	pl_call  fun67(&seed[28],0)
	pl_call  fun67(&seed[102],0)
	pl_call  fun67(&seed[10],0)
	pl_call  fun67(&seed[9],0)
	pl_call  fun67(&seed[90],0)
	pl_call  fun67(&seed[89],0)
	pl_call  fun67(&seed[87],0)
	pl_call  fun67(&seed[58],0)
	pl_call  fun67(&seed[59],0)
	pl_call  fun67(&seed[41],0)
	pl_call  fun67(&seed[95],0)
	pl_call  fun67(&seed[65],0)
	pl_call  fun67(&seed[45],0)
	pl_call  fun67(&seed[93],0)
	pl_call  fun67(&seed[67],0)
	pl_call  fun67(&seed[80],0)
	pl_call  fun67(&seed[79],0)
	pl_call  fun67(&seed[85],0)
	pl_call  fun67(&seed[84],0)
	pl_call  fun67(&seed[83],0)
	pl_call  fun67(&seed[82],0)
	pl_call  fun67(&seed[81],0)
	pl_call  fun67(&seed[78],0)
	pl_call  fun67(&seed[61],0)
	pl_call  fun67(&seed[13],0)
	pl_call  fun67(&seed[12],0)
	pl_call  fun67(&seed[11],0)
	pl_call  fun67(&seed[46],0)
	pl_call  fun67(&seed[96],0)
	pl_call  fun67(&seed[94],0)
	pl_call  fun67(&seed[92],0)
	pl_call  fun67(&seed[91],0)
	pl_call  fun67(&seed[25],0)
	pl_call  fun67(&seed[24],0)
	pl_call  fun67(&seed[63],0)
	pl_call  fun67(&seed[16],0)
	pl_call  fun67(&seed[75],0)
	pl_call  fun67(&seed[74],0)
	pl_call  fun67(&seed[64],0)
	pl_call  fun67(&seed[62],0)
	pl_call  fun67(&seed[48],0)
	pl_call  fun67(&seed[14],0)
	pl_call  fun67(&seed[52],0)
	pl_call  fun67(&seed[15],0)
	pl_call  fun67(&seed[22],0)
	pl_call  fun67(&seed[44],0)
	pl_call  fun67(&seed[8],0)
	pl_call  fun67(&seed[6],0)
	pl_call  fun67(&seed[7],0)
	pl_call  fun67(&seed[5],0)
	pl_call  fun67(&seed[4],0)
	pl_call  fun67(&seed[3],0)
	pl_call  fun67(&seed[2],0)
	pl_call  fun67(&seed[1],0)
	pl_call  fun67(&seed[0],0)
	pl_call  fun15(&seed[112],0)
	pl_call  fun15(&seed[111],0)
	pl_call  fun15(&seed[110],0)
	pl_call  fun15(&seed[109],0)
	pl_call  fun15(&seed[108],0)
	pl_call  fun15(&seed[107],0)
	pl_call  fun15(&seed[106],0)
	pl_call  fun15(&seed[105],0)
	pl_call  fun15(&seed[104],0)
	call_c   Dyam_Loading_Reset()
	c_ret

pl_code global pred_check_tag_kind_3
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(2)
	call_c   Dyam_Reg_Unify(4,&ref[657])
	fail_ret
	call_c   Dyam_Choice(fun323)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(1),&ref[648])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(2),&ref[649])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code global pred_install_3
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(3)
	call_c   Dyam_Choice(fun316)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_variable(V(2))
	fail_ret
	call_c   Dyam_Cut()
	pl_call  fun25(&seed[142],1)
	call_c   Dyam_Deallocate()
	pl_ret

pl_code global pred_tag_anchor_analyse_equation_3
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(3)
	call_c   Dyam_Choice(fun302)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_variable(V(1))
	fail_ret
	call_c   Dyam_Cut()
	move     &ref[621], R(0)
	move     0, R(1)
	move     I(0), R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_error_2()

pl_code global pred_extend_compiler_1
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Unify(0,&ref[604])
	fail_ret
	call_c   Dyam_Choice(fun287)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_variable(V(1))
	fail_ret
	call_c   Dyam_Cut()
	move     &ref[600], R(0)
	move     0, R(1)
	move     &ref[110], R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_error_2()

pl_code global pred_read_files_2
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(2)
	call_c   Dyam_Choice(fun281)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[296])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(3))
	call_c   Dyam_Reg_Load(2,V(2))
	pl_call  pred_read_files_2()
	call_c   Dyam_Reg_Load(0,V(4))
	call_c   Dyam_Reg_Load(2,V(2))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_read_files_2()

pl_code global pred_tag_anchor_set_equation_3
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Unify(0,&ref[564])
	fail_ret
	call_c   Dyam_Multi_Reg_Bind(2,4,2)
	call_c   Dyam_Reg_Load(0,V(2))
	move     V(6), R(2)
	move     S(5), R(3)
	move     V(7), R(4)
	move     S(5), R(5)
	pl_call  pred_tag_anchor_analyse_equation_3()
	call_c   Dyam_Choice(fun263)
	call_c   Dyam_Set_Cut()
	pl_call  Object_2(&ref[560],V(11))
	call_c   DYAM_evpred_retract(&ref[560])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun262)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(6),V(8))
	fail_ret
	call_c   Dyam_Unify(V(7),V(9))
	fail_ret
	call_c   Dyam_Cut()
fun261:
	call_c   DYAM_evpred_assert_1(&ref[561])
	call_c   Dyam_Deallocate()
	pl_ret


pl_code global pred_read_analyze_directive_4
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(4)
	call_c   Dyam_Choice(fun249)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[532])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(5))
	call_c   Dyam_Reg_Load(2,V(2))
	call_c   Dyam_Reg_Load(4,V(3))
	move     V(7), R(6)
	move     S(5), R(7)
	pl_call  pred_read_analyze_directive_4()
	call_c   Dyam_Reg_Load(0,V(6))
	call_c   Dyam_Reg_Load(2,V(2))
	call_c   Dyam_Reg_Load(4,V(3))
	call_c   Dyam_Reg_Load(6,V(4))
	call_c   Dyam_Reg_Deallocate(4)
	pl_jump  pred_read_analyze_directive_4()

pl_code global pred_normalize_mode_2
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(2)
	call_c   Dyam_Choice(fun246)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[531])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(2),V(1))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code global pred_check_finite_set_1
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Choice(fun207)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(1),I(0))
	fail_ret
	call_c   Dyam_Cut()
	move     &ref[471], R(0)
	move     0, R(1)
	move     I(0), R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_error_2()

pl_code global pred_read_program_3
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(3)
	call_c   Dyam_Choice(fun150)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(2),&ref[100])
	fail_ret
	call_c   Dyam_Cut()
	call_c   DYAM_Module_Get_1(V(4))
	fail_ret
	call_c   DYAM_Module_Set_1(I(0))
	fail_ret
fun149:
	call_c   Dyam_Choice(fun148)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Foreign_Create_Choice(Lrepeat0_choice_0,0,1)
Lrepeat0_choice_0:
	call_c   Dyam_Foreign_Update_Choice(Lrepeat0_choice_0,0,1)
	call_c   DYAM_Read_Term_3(V(1),V(5),V(6))
	fail_ret
	pl_call  fun25(&seed[115],1)
	call_c   DYAM_sfol_identical(V(7),&ref[310])
	fail_ret
	call_c   Dyam_Cut()
fun147:
	call_c   Dyam_Choice(fun146)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(2),&ref[100])
	fail_ret
	call_c   Dyam_Cut()
	call_c   DYAM_Module_Set_1(V(4))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret



pl_code global pred_tag_nt_modulation_3
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(3)
	call_c   Dyam_Choice(fun142)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(2),V(3))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Deallocate()
	pl_ret

pl_code global pred_spec_in_module_2
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(2)
	call_c   Dyam_Choice(fun124)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[301])
	fail_ret
	call_c   Dyam_Unify(V(2),&ref[302])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(3))
	call_c   Dyam_Reg_Load(2,V(5))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_try_in_module_2()

pl_code global pred_try_in_module_2
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(2)
	call_c   DYAM_Module_Get_1(V(3))
	fail_ret
	call_c   Dyam_Choice(fun109)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(3),I(0))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(1),V(2))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code global pred_install_tag_features_mode_4
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(4)
	call_c   Dyam_Choice(fun97)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_atom(V(1))
	fail_ret
	call_c   Dyam_Cut()
fun96:
	call_c   Dyam_Choice(fun95)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_atom(V(2))
	fail_ret
	pl_call  Domain_2(V(2),&ref[205])
	call_c   Dyam_Cut()
fun94:
	call_c   Dyam_Choice(fun93)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),V(3))
	fail_ret
	call_c   Dyam_Unify(V(1),V(4))
	fail_ret
	call_c   Dyam_Cut()
fun89:
	move     &ref[206], R(0)
	move     S(5), R(1)
	move     V(7), R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Load(4,V(3))
	move     V(8), R(6)
	move     S(5), R(7)
	pl_call  pred_term_current_module_shift_4()
	move     &ref[206], R(0)
	move     S(5), R(1)
	move     V(9), R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Load(4,V(4))
	move     V(10), R(6)
	move     S(5), R(7)
	pl_call  pred_term_current_module_shift_4()
	call_c   Dyam_Unify(V(11),&ref[207])
	fail_ret
	call_c   Dyam_Choice(fun88)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[208])
	call_c   Dyam_Choice(fun87)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(&ref[208],V(11))
	fail_ret
	call_c   Dyam_Cut()
	pl_fail




pl_code global pred_module_directive_4
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(4)
	call_c   Dyam_Choice(fun83)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_atom(V(1))
	fail_ret
	call_c   Dyam_Cut()
fun82:
	call_c   Dyam_Choice(fun81)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[192])
	call_c   Dyam_Choice(fun80)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(5),V(1))
	fail_ret
	call_c   Dyam_Cut()
	pl_fail


pl_code global pred_new_bmg_island_2
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(2)
	call_c   DYAM_Op_3(N(300),&ref[154],V(1))
	fail_ret
	call_c   DYAM_evpred_retract(&ref[155])
	fail_ret
	call_c   DYAM_evpred_assert_1(&ref[155])
	move     &ref[155], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(2))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_install_2()

pl_code global pred_install_tag_features_3
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(3)
	call_c   Dyam_Choice(fun47)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_atom(V(1))
	fail_ret
	call_c   Dyam_Cut()
fun46:
	call_c   Dyam_Choice(fun45)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),V(2))
	fail_ret
	call_c   Dyam_Unify(V(1),V(3))
	fail_ret
	call_c   Dyam_Cut()
fun41:
	move     &ref[111], R(0)
	move     S(5), R(1)
	move     V(6), R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Load(4,V(2))
	move     V(7), R(6)
	move     S(5), R(7)
	pl_call  pred_term_current_module_shift_4()
	move     &ref[111], R(0)
	move     S(5), R(1)
	move     V(8), R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Load(4,V(3))
	move     V(9), R(6)
	move     S(5), R(7)
	pl_call  pred_term_current_module_shift_4()
	call_c   Dyam_Unify(V(10),&ref[112])
	fail_ret
	call_c   Dyam_Choice(fun40)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[113])
	call_c   Dyam_Choice(fun39)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(&ref[113],V(10))
	fail_ret
	call_c   Dyam_Cut()
	pl_fail



pl_code global pred_read_file_2
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(2)
	call_c   Dyam_Choice(fun35)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_Find_File_2(V(1),V(3))
	fail_ret
	call_c   Dyam_Cut()
fun34:
	call_c   Dyam_Choice(fun33)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[104])
	call_c   Dyam_Cut()
	call_c   Dyam_Deallocate()
	pl_ret


pl_code global pred_install_2
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(2)
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(2))
	move     &ref[31], R(4)
	move     0, R(5)
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  pred_install_3()

pl_code global pred_directive_updater_2
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(2)
	call_c   DYAM_evpred_retract(V(1))
	fail_ret
	call_c   DYAM_evpred_assert_1(V(2))
	call_c   Dyam_Deallocate()
	pl_ret

pl_code global pred_register_clause_1
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   DYAM_evpred_assert_1(&ref[10])
	call_c   Dyam_Deallocate()
	pl_ret


;;------------------ VIEWERS ------------

c_code local build_viewers
	c_ret

c_code local build_seed_104
	ret_reg &seed[104]
	call_c   build_ref_0()
	call_c   build_ref_1()
	call_c   Dyam_Seed_Start(&ref[0],&ref[1],&ref[1],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[104]
	c_ret

pl_code local fun0
	pl_ret

;; TERM 1: flag_exclusive_set(lco, lco)
c_code local build_ref_1
	ret_reg &ref[1]
	call_c   build_ref_660()
	call_c   build_ref_661()
	call_c   Dyam_Create_Binary(&ref[660],&ref[661],&ref[661])
	move_ret ref[1]
	c_ret

;; TERM 661: lco
c_code local build_ref_661
	ret_reg &ref[661]
	call_c   Dyam_Create_Atom("lco")
	move_ret ref[661]
	c_ret

;; TERM 660: flag_exclusive_set
c_code local build_ref_660
	ret_reg &ref[660]
	call_c   Dyam_Create_Atom("flag_exclusive_set")
	move_ret ref[660]
	c_ret

;; TERM 0: '*DATABASE*'
c_code local build_ref_0
	ret_reg &ref[0]
	call_c   Dyam_Create_Atom("*DATABASE*")
	move_ret ref[0]
	c_ret

c_code local build_seed_105
	ret_reg &seed[105]
	call_c   build_ref_0()
	call_c   build_ref_2()
	call_c   Dyam_Seed_Start(&ref[0],&ref[2],&ref[2],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[105]
	c_ret

;; TERM 2: flag_exclusive_set(compile, light_tabular)
c_code local build_ref_2
	ret_reg &ref[2]
	call_c   build_ref_660()
	call_c   build_ref_662()
	call_c   build_ref_663()
	call_c   Dyam_Create_Binary(&ref[660],&ref[662],&ref[663])
	move_ret ref[2]
	c_ret

;; TERM 663: light_tabular
c_code local build_ref_663
	ret_reg &ref[663]
	call_c   Dyam_Create_Atom("light_tabular")
	move_ret ref[663]
	c_ret

;; TERM 662: compile
c_code local build_ref_662
	ret_reg &ref[662]
	call_c   Dyam_Create_Atom("compile")
	move_ret ref[662]
	c_ret

c_code local build_seed_106
	ret_reg &seed[106]
	call_c   build_ref_0()
	call_c   build_ref_3()
	call_c   Dyam_Seed_Start(&ref[0],&ref[3],&ref[3],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[106]
	c_ret

;; TERM 3: flag_exclusive_set(compile, std_prolog)
c_code local build_ref_3
	ret_reg &ref[3]
	call_c   build_ref_660()
	call_c   build_ref_662()
	call_c   build_ref_664()
	call_c   Dyam_Create_Binary(&ref[660],&ref[662],&ref[664])
	move_ret ref[3]
	c_ret

;; TERM 664: std_prolog
c_code local build_ref_664
	ret_reg &ref[664]
	call_c   Dyam_Create_Atom("std_prolog")
	move_ret ref[664]
	c_ret

c_code local build_seed_107
	ret_reg &seed[107]
	call_c   build_ref_0()
	call_c   build_ref_4()
	call_c   Dyam_Seed_Start(&ref[0],&ref[4],&ref[4],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[107]
	c_ret

;; TERM 4: flag_exclusive_set(compile, rec_prolog)
c_code local build_ref_4
	ret_reg &ref[4]
	call_c   build_ref_660()
	call_c   build_ref_662()
	call_c   build_ref_665()
	call_c   Dyam_Create_Binary(&ref[660],&ref[662],&ref[665])
	move_ret ref[4]
	c_ret

;; TERM 665: rec_prolog
c_code local build_ref_665
	ret_reg &ref[665]
	call_c   Dyam_Create_Atom("rec_prolog")
	move_ret ref[665]
	c_ret

c_code local build_seed_108
	ret_reg &seed[108]
	call_c   build_ref_0()
	call_c   build_ref_5()
	call_c   Dyam_Seed_Start(&ref[0],&ref[5],&ref[5],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[108]
	c_ret

;; TERM 5: flag_exclusive_set(compile, prolog)
c_code local build_ref_5
	ret_reg &ref[5]
	call_c   build_ref_660()
	call_c   build_ref_662()
	call_c   build_ref_666()
	call_c   Dyam_Create_Binary(&ref[660],&ref[662],&ref[666])
	move_ret ref[5]
	c_ret

;; TERM 666: prolog
c_code local build_ref_666
	ret_reg &ref[666]
	call_c   Dyam_Create_Atom("prolog")
	move_ret ref[666]
	c_ret

c_code local build_seed_109
	ret_reg &seed[109]
	call_c   build_ref_0()
	call_c   build_ref_6()
	call_c   Dyam_Seed_Start(&ref[0],&ref[6],&ref[6],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[109]
	c_ret

;; TERM 6: flag_exclusive_set(compile, extensional)
c_code local build_ref_6
	ret_reg &ref[6]
	call_c   build_ref_660()
	call_c   build_ref_662()
	call_c   build_ref_667()
	call_c   Dyam_Create_Binary(&ref[660],&ref[662],&ref[667])
	move_ret ref[6]
	c_ret

;; TERM 667: extensional
c_code local build_ref_667
	ret_reg &ref[667]
	call_c   Dyam_Create_Atom("extensional")
	move_ret ref[667]
	c_ret

c_code local build_seed_110
	ret_reg &seed[110]
	call_c   build_ref_0()
	call_c   build_ref_7()
	call_c   Dyam_Seed_Start(&ref[0],&ref[7],&ref[7],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[110]
	c_ret

;; TERM 7: cmode_to_pat(bottom, -)
c_code local build_ref_7
	ret_reg &ref[7]
	call_c   build_ref_668()
	call_c   build_ref_669()
	call_c   build_ref_393()
	call_c   Dyam_Create_Binary(&ref[668],&ref[669],&ref[393])
	move_ret ref[7]
	c_ret

;; TERM 393: -
c_code local build_ref_393
	ret_reg &ref[393]
	call_c   Dyam_Create_Atom("-")
	move_ret ref[393]
	c_ret

;; TERM 669: bottom
c_code local build_ref_669
	ret_reg &ref[669]
	call_c   Dyam_Create_Atom("bottom")
	move_ret ref[669]
	c_ret

;; TERM 668: cmode_to_pat
c_code local build_ref_668
	ret_reg &ref[668]
	call_c   Dyam_Create_Atom("cmode_to_pat")
	move_ret ref[668]
	c_ret

c_code local build_seed_111
	ret_reg &seed[111]
	call_c   build_ref_0()
	call_c   build_ref_8()
	call_c   Dyam_Seed_Start(&ref[0],&ref[8],&ref[8],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[111]
	c_ret

;; TERM 8: cmode_to_pat(earley, *)
c_code local build_ref_8
	ret_reg &ref[8]
	call_c   build_ref_668()
	call_c   build_ref_670()
	call_c   build_ref_392()
	call_c   Dyam_Create_Binary(&ref[668],&ref[670],&ref[392])
	move_ret ref[8]
	c_ret

;; TERM 392: *
c_code local build_ref_392
	ret_reg &ref[392]
	call_c   Dyam_Create_Atom("*")
	move_ret ref[392]
	c_ret

;; TERM 670: earley
c_code local build_ref_670
	ret_reg &ref[670]
	call_c   Dyam_Create_Atom("earley")
	move_ret ref[670]
	c_ret

c_code local build_seed_112
	ret_reg &seed[112]
	call_c   build_ref_0()
	call_c   build_ref_9()
	call_c   Dyam_Seed_Start(&ref[0],&ref[9],&ref[9],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[112]
	c_ret

;; TERM 9: cmode_to_pat(old, +)
c_code local build_ref_9
	ret_reg &ref[9]
	call_c   build_ref_668()
	call_c   build_ref_671()
	call_c   build_ref_388()
	call_c   Dyam_Create_Binary(&ref[668],&ref[671],&ref[388])
	move_ret ref[9]
	c_ret

;; TERM 388: +
c_code local build_ref_388
	ret_reg &ref[388]
	call_c   Dyam_Create_Atom("+")
	move_ret ref[388]
	c_ret

;; TERM 671: old
c_code local build_ref_671
	ret_reg &ref[671]
	call_c   Dyam_Create_Atom("old")
	move_ret ref[671]
	c_ret

c_code local build_seed_0
	ret_reg &seed[0]
	call_c   build_ref_11()
	call_c   build_ref_15()
	call_c   Dyam_Seed_Start(&ref[11],&ref[15],I(0),fun0,1)
	call_c   build_ref_14()
	call_c   Dyam_Seed_Add_Comp(&ref[14],fun2,0)
	call_c   Dyam_Seed_End()
	move_ret seed[0]
	c_ret

;; TERM 14: '*GUARD*'(check_ground_list([]))
c_code local build_ref_14
	ret_reg &ref[14]
	call_c   build_ref_12()
	call_c   build_ref_13()
	call_c   Dyam_Create_Unary(&ref[12],&ref[13])
	move_ret ref[14]
	c_ret

;; TERM 13: check_ground_list([])
c_code local build_ref_13
	ret_reg &ref[13]
	call_c   build_ref_672()
	call_c   Dyam_Create_Unary(&ref[672],I(0))
	move_ret ref[13]
	c_ret

;; TERM 672: check_ground_list
c_code local build_ref_672
	ret_reg &ref[672]
	call_c   Dyam_Create_Atom("check_ground_list")
	move_ret ref[672]
	c_ret

;; TERM 12: '*GUARD*'
c_code local build_ref_12
	ret_reg &ref[12]
	call_c   Dyam_Create_Atom("*GUARD*")
	move_ret ref[12]
	c_ret

pl_code local fun2
	call_c   build_ref_14()
	call_c   Dyam_Unify_Item(&ref[14])
	fail_ret
	pl_ret

;; TERM 15: '*GUARD*'(check_ground_list([])) :> []
c_code local build_ref_15
	ret_reg &ref[15]
	call_c   build_ref_14()
	call_c   Dyam_Create_Binary(I(9),&ref[14],I(0))
	move_ret ref[15]
	c_ret

;; TERM 11: '*GUARD_CLAUSE*'
c_code local build_ref_11
	ret_reg &ref[11]
	call_c   Dyam_Create_Atom("*GUARD_CLAUSE*")
	move_ret ref[11]
	c_ret

c_code local build_seed_1
	ret_reg &seed[1]
	call_c   build_ref_11()
	call_c   build_ref_18()
	call_c   Dyam_Seed_Start(&ref[11],&ref[18],I(0),fun0,1)
	call_c   build_ref_17()
	call_c   Dyam_Seed_Add_Comp(&ref[17],fun3,0)
	call_c   Dyam_Seed_End()
	move_ret seed[1]
	c_ret

;; TERM 17: '*GUARD*'(check_mode(*))
c_code local build_ref_17
	ret_reg &ref[17]
	call_c   build_ref_12()
	call_c   build_ref_16()
	call_c   Dyam_Create_Unary(&ref[12],&ref[16])
	move_ret ref[17]
	c_ret

;; TERM 16: check_mode(*)
c_code local build_ref_16
	ret_reg &ref[16]
	call_c   build_ref_673()
	call_c   build_ref_392()
	call_c   Dyam_Create_Unary(&ref[673],&ref[392])
	move_ret ref[16]
	c_ret

;; TERM 673: check_mode
c_code local build_ref_673
	ret_reg &ref[673]
	call_c   Dyam_Create_Atom("check_mode")
	move_ret ref[673]
	c_ret

pl_code local fun3
	call_c   build_ref_17()
	call_c   Dyam_Unify_Item(&ref[17])
	fail_ret
	pl_ret

;; TERM 18: '*GUARD*'(check_mode(*)) :> []
c_code local build_ref_18
	ret_reg &ref[18]
	call_c   build_ref_17()
	call_c   Dyam_Create_Binary(I(9),&ref[17],I(0))
	move_ret ref[18]
	c_ret

c_code local build_seed_2
	ret_reg &seed[2]
	call_c   build_ref_11()
	call_c   build_ref_21()
	call_c   Dyam_Seed_Start(&ref[11],&ref[21],I(0),fun0,1)
	call_c   build_ref_20()
	call_c   Dyam_Seed_Add_Comp(&ref[20],fun4,0)
	call_c   Dyam_Seed_End()
	move_ret seed[2]
	c_ret

;; TERM 20: '*GUARD*'(check_mode(+))
c_code local build_ref_20
	ret_reg &ref[20]
	call_c   build_ref_12()
	call_c   build_ref_19()
	call_c   Dyam_Create_Unary(&ref[12],&ref[19])
	move_ret ref[20]
	c_ret

;; TERM 19: check_mode(+)
c_code local build_ref_19
	ret_reg &ref[19]
	call_c   build_ref_673()
	call_c   build_ref_388()
	call_c   Dyam_Create_Unary(&ref[673],&ref[388])
	move_ret ref[19]
	c_ret

pl_code local fun4
	call_c   build_ref_20()
	call_c   Dyam_Unify_Item(&ref[20])
	fail_ret
	pl_ret

;; TERM 21: '*GUARD*'(check_mode(+)) :> []
c_code local build_ref_21
	ret_reg &ref[21]
	call_c   build_ref_20()
	call_c   Dyam_Create_Binary(I(9),&ref[20],I(0))
	move_ret ref[21]
	c_ret

c_code local build_seed_3
	ret_reg &seed[3]
	call_c   build_ref_11()
	call_c   build_ref_24()
	call_c   Dyam_Seed_Start(&ref[11],&ref[24],I(0),fun0,1)
	call_c   build_ref_23()
	call_c   Dyam_Seed_Add_Comp(&ref[23],fun5,0)
	call_c   Dyam_Seed_End()
	move_ret seed[3]
	c_ret

;; TERM 23: '*GUARD*'(check_mode(-))
c_code local build_ref_23
	ret_reg &ref[23]
	call_c   build_ref_12()
	call_c   build_ref_22()
	call_c   Dyam_Create_Unary(&ref[12],&ref[22])
	move_ret ref[23]
	c_ret

;; TERM 22: check_mode(-)
c_code local build_ref_22
	ret_reg &ref[22]
	call_c   build_ref_673()
	call_c   build_ref_393()
	call_c   Dyam_Create_Unary(&ref[673],&ref[393])
	move_ret ref[22]
	c_ret

pl_code local fun5
	call_c   build_ref_23()
	call_c   Dyam_Unify_Item(&ref[23])
	fail_ret
	pl_ret

;; TERM 24: '*GUARD*'(check_mode(-)) :> []
c_code local build_ref_24
	ret_reg &ref[24]
	call_c   build_ref_23()
	call_c   Dyam_Create_Binary(I(9),&ref[23],I(0))
	move_ret ref[24]
	c_ret

c_code local build_seed_4
	ret_reg &seed[4]
	call_c   build_ref_11()
	call_c   build_ref_27()
	call_c   Dyam_Seed_Start(&ref[11],&ref[27],I(0),fun0,1)
	call_c   build_ref_26()
	call_c   Dyam_Seed_Add_Comp(&ref[26],fun6,0)
	call_c   Dyam_Seed_End()
	move_ret seed[4]
	c_ret

;; TERM 26: '*GUARD*'(check_mode([]))
c_code local build_ref_26
	ret_reg &ref[26]
	call_c   build_ref_12()
	call_c   build_ref_25()
	call_c   Dyam_Create_Unary(&ref[12],&ref[25])
	move_ret ref[26]
	c_ret

;; TERM 25: check_mode([])
c_code local build_ref_25
	ret_reg &ref[25]
	call_c   build_ref_673()
	call_c   Dyam_Create_Unary(&ref[673],I(0))
	move_ret ref[25]
	c_ret

pl_code local fun6
	call_c   build_ref_26()
	call_c   Dyam_Unify_Item(&ref[26])
	fail_ret
	pl_ret

;; TERM 27: '*GUARD*'(check_mode([])) :> []
c_code local build_ref_27
	ret_reg &ref[27]
	call_c   build_ref_26()
	call_c   Dyam_Create_Binary(I(9),&ref[26],I(0))
	move_ret ref[27]
	c_ret

c_code local build_seed_5
	ret_reg &seed[5]
	call_c   build_ref_11()
	call_c   build_ref_30()
	call_c   Dyam_Seed_Start(&ref[11],&ref[30],I(0),fun0,1)
	call_c   build_ref_29()
	call_c   Dyam_Seed_Add_Comp(&ref[29],fun7,0)
	call_c   Dyam_Seed_End()
	move_ret seed[5]
	c_ret

;; TERM 29: '*GUARD*'(tag_anchor_collect_eqvars([], []))
c_code local build_ref_29
	ret_reg &ref[29]
	call_c   build_ref_12()
	call_c   build_ref_28()
	call_c   Dyam_Create_Unary(&ref[12],&ref[28])
	move_ret ref[29]
	c_ret

;; TERM 28: tag_anchor_collect_eqvars([], [])
c_code local build_ref_28
	ret_reg &ref[28]
	call_c   build_ref_674()
	call_c   Dyam_Create_Binary(&ref[674],I(0),I(0))
	move_ret ref[28]
	c_ret

;; TERM 674: tag_anchor_collect_eqvars
c_code local build_ref_674
	ret_reg &ref[674]
	call_c   Dyam_Create_Atom("tag_anchor_collect_eqvars")
	move_ret ref[674]
	c_ret

pl_code local fun7
	call_c   build_ref_29()
	call_c   Dyam_Unify_Item(&ref[29])
	fail_ret
	pl_ret

;; TERM 30: '*GUARD*'(tag_anchor_collect_eqvars([], [])) :> []
c_code local build_ref_30
	ret_reg &ref[30]
	call_c   build_ref_29()
	call_c   Dyam_Create_Binary(I(9),&ref[29],I(0))
	move_ret ref[30]
	c_ret

c_code local build_seed_7
	ret_reg &seed[7]
	call_c   build_ref_11()
	call_c   build_ref_34()
	call_c   Dyam_Seed_Start(&ref[11],&ref[34],I(0),fun0,1)
	call_c   build_ref_33()
	call_c   Dyam_Seed_Add_Comp(&ref[33],fun10,0)
	call_c   Dyam_Seed_End()
	move_ret seed[7]
	c_ret

;; TERM 33: '*GUARD*'(install_list(_B, [], _C))
c_code local build_ref_33
	ret_reg &ref[33]
	call_c   build_ref_12()
	call_c   build_ref_32()
	call_c   Dyam_Create_Unary(&ref[12],&ref[32])
	move_ret ref[33]
	c_ret

;; TERM 32: install_list(_B, [], _C)
c_code local build_ref_32
	ret_reg &ref[32]
	call_c   build_ref_675()
	call_c   Dyam_Term_Start(&ref[675],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_End()
	move_ret ref[32]
	c_ret

;; TERM 675: install_list
c_code local build_ref_675
	ret_reg &ref[675]
	call_c   Dyam_Create_Atom("install_list")
	move_ret ref[675]
	c_ret

pl_code local fun10
	call_c   build_ref_33()
	call_c   Dyam_Unify_Item(&ref[33])
	fail_ret
	pl_ret

;; TERM 34: '*GUARD*'(install_list(_B, [], _C)) :> []
c_code local build_ref_34
	ret_reg &ref[34]
	call_c   build_ref_33()
	call_c   Dyam_Create_Binary(I(9),&ref[33],I(0))
	move_ret ref[34]
	c_ret

c_code local build_seed_6
	ret_reg &seed[6]
	call_c   build_ref_11()
	call_c   build_ref_37()
	call_c   Dyam_Seed_Start(&ref[11],&ref[37],I(0),fun0,1)
	call_c   build_ref_36()
	call_c   Dyam_Seed_Add_Comp(&ref[36],fun11,0)
	call_c   Dyam_Seed_End()
	move_ret seed[6]
	c_ret

;; TERM 36: '*GUARD*'(tag_anchor_set_coanchors([], [], _B, _C))
c_code local build_ref_36
	ret_reg &ref[36]
	call_c   build_ref_12()
	call_c   build_ref_35()
	call_c   Dyam_Create_Unary(&ref[12],&ref[35])
	move_ret ref[36]
	c_ret

;; TERM 35: tag_anchor_set_coanchors([], [], _B, _C)
c_code local build_ref_35
	ret_reg &ref[35]
	call_c   build_ref_676()
	call_c   Dyam_Term_Start(&ref[676],4)
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_End()
	move_ret ref[35]
	c_ret

;; TERM 676: tag_anchor_set_coanchors
c_code local build_ref_676
	ret_reg &ref[676]
	call_c   Dyam_Create_Atom("tag_anchor_set_coanchors")
	move_ret ref[676]
	c_ret

pl_code local fun11
	call_c   build_ref_36()
	call_c   Dyam_Unify_Item(&ref[36])
	fail_ret
	pl_ret

;; TERM 37: '*GUARD*'(tag_anchor_set_coanchors([], [], _B, _C)) :> []
c_code local build_ref_37
	ret_reg &ref[37]
	call_c   build_ref_36()
	call_c   Dyam_Create_Binary(I(9),&ref[36],I(0))
	move_ret ref[37]
	c_ret

c_code local build_seed_8
	ret_reg &seed[8]
	call_c   build_ref_11()
	call_c   build_ref_40()
	call_c   Dyam_Seed_Start(&ref[11],&ref[40],I(0),fun0,1)
	call_c   build_ref_39()
	call_c   Dyam_Seed_Add_Comp(&ref[39],fun12,0)
	call_c   Dyam_Seed_End()
	move_ret seed[8]
	c_ret

;; TERM 39: '*GUARD*'(clause_type('$dyalog_toplevel', '$dyalog_toplevel', _B, _C))
c_code local build_ref_39
	ret_reg &ref[39]
	call_c   build_ref_12()
	call_c   build_ref_38()
	call_c   Dyam_Create_Unary(&ref[12],&ref[38])
	move_ret ref[39]
	c_ret

;; TERM 38: clause_type('$dyalog_toplevel', '$dyalog_toplevel', _B, _C)
c_code local build_ref_38
	ret_reg &ref[38]
	call_c   build_ref_677()
	call_c   build_ref_678()
	call_c   Dyam_Term_Start(&ref[677],4)
	call_c   Dyam_Term_Arg(&ref[678])
	call_c   Dyam_Term_Arg(&ref[678])
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_End()
	move_ret ref[38]
	c_ret

;; TERM 678: '$dyalog_toplevel'
c_code local build_ref_678
	ret_reg &ref[678]
	call_c   Dyam_Create_Atom("$dyalog_toplevel")
	move_ret ref[678]
	c_ret

;; TERM 677: clause_type
c_code local build_ref_677
	ret_reg &ref[677]
	call_c   Dyam_Create_Atom("clause_type")
	move_ret ref[677]
	c_ret

pl_code local fun12
	call_c   build_ref_39()
	call_c   Dyam_Unify_Item(&ref[39])
	fail_ret
	pl_ret

;; TERM 40: '*GUARD*'(clause_type('$dyalog_toplevel', '$dyalog_toplevel', _B, _C)) :> []
c_code local build_ref_40
	ret_reg &ref[40]
	call_c   build_ref_39()
	call_c   Dyam_Create_Binary(I(9),&ref[39],I(0))
	move_ret ref[40]
	c_ret

c_code local build_seed_44
	ret_reg &seed[44]
	call_c   build_ref_11()
	call_c   build_ref_43()
	call_c   Dyam_Seed_Start(&ref[11],&ref[43],I(0),fun0,1)
	call_c   build_ref_42()
	call_c   Dyam_Seed_Add_Comp(&ref[42],fun101,0)
	call_c   Dyam_Seed_End()
	move_ret seed[44]
	c_ret

;; TERM 42: '*GUARD*'(install_elem(_B, '*default*', _C))
c_code local build_ref_42
	ret_reg &ref[42]
	call_c   build_ref_12()
	call_c   build_ref_41()
	call_c   Dyam_Create_Unary(&ref[12],&ref[41])
	move_ret ref[42]
	c_ret

;; TERM 41: install_elem(_B, '*default*', _C)
c_code local build_ref_41
	ret_reg &ref[41]
	call_c   build_ref_679()
	call_c   build_ref_680()
	call_c   Dyam_Term_Start(&ref[679],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(&ref[680])
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_End()
	move_ret ref[41]
	c_ret

;; TERM 680: '*default*'
c_code local build_ref_680
	ret_reg &ref[680]
	call_c   Dyam_Create_Atom("*default*")
	move_ret ref[680]
	c_ret

;; TERM 679: install_elem
c_code local build_ref_679
	ret_reg &ref[679]
	call_c   Dyam_Create_Atom("install_elem")
	move_ret ref[679]
	c_ret

c_code local build_seed_113
	ret_reg &seed[113]
	call_c   build_ref_12()
	call_c   build_ref_45()
	call_c   Dyam_Seed_Start(&ref[12],&ref[45],I(0),fun13,1)
	call_c   build_ref_46()
	call_c   Dyam_Seed_Add_Comp(&ref[46],&ref[45],0)
	call_c   Dyam_Seed_End()
	move_ret seed[113]
	c_ret

pl_code local fun13
	pl_jump  Complete(0,0)

;; TERM 46: '*GUARD*'(installer(_B, '*default*', _C)) :> '$$HOLE$$'
c_code local build_ref_46
	ret_reg &ref[46]
	call_c   build_ref_45()
	call_c   Dyam_Create_Binary(I(9),&ref[45],I(7))
	move_ret ref[46]
	c_ret

;; TERM 45: '*GUARD*'(installer(_B, '*default*', _C))
c_code local build_ref_45
	ret_reg &ref[45]
	call_c   build_ref_12()
	call_c   build_ref_44()
	call_c   Dyam_Create_Unary(&ref[12],&ref[44])
	move_ret ref[45]
	c_ret

;; TERM 44: installer(_B, '*default*', _C)
c_code local build_ref_44
	ret_reg &ref[44]
	call_c   build_ref_681()
	call_c   build_ref_680()
	call_c   Dyam_Term_Start(&ref[681],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(&ref[680])
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_End()
	move_ret ref[44]
	c_ret

;; TERM 681: installer
c_code local build_ref_681
	ret_reg &ref[681]
	call_c   Dyam_Create_Atom("installer")
	move_ret ref[681]
	c_ret

pl_code local fun25
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Light_Object(R(0))
	pl_jump  Schedule_Prolog()

long local pool_fun101[3]=[2,build_ref_42,build_seed_113]

pl_code local fun101
	call_c   Dyam_Pool(pool_fun101)
	call_c   Dyam_Unify_Item(&ref[42])
	fail_ret
	pl_jump  fun25(&seed[113],1)

;; TERM 43: '*GUARD*'(install_elem(_B, '*default*', _C)) :> []
c_code local build_ref_43
	ret_reg &ref[43]
	call_c   build_ref_42()
	call_c   Dyam_Create_Binary(I(9),&ref[42],I(0))
	move_ret ref[43]
	c_ret

c_code local build_seed_22
	ret_reg &seed[22]
	call_c   build_ref_11()
	call_c   build_ref_52()
	call_c   Dyam_Seed_Start(&ref[11],&ref[52],I(0),fun0,1)
	call_c   build_ref_51()
	call_c   Dyam_Seed_Add_Comp(&ref[51],fun26,0)
	call_c   Dyam_Seed_End()
	move_ret seed[22]
	c_ret

;; TERM 51: '*GUARD*'(check_ground_list([_B|_C]))
c_code local build_ref_51
	ret_reg &ref[51]
	call_c   build_ref_12()
	call_c   build_ref_50()
	call_c   Dyam_Create_Unary(&ref[12],&ref[50])
	move_ret ref[51]
	c_ret

;; TERM 50: check_ground_list([_B|_C])
c_code local build_ref_50
	ret_reg &ref[50]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(1),V(2))
	move_ret R(0)
	call_c   build_ref_672()
	call_c   Dyam_Create_Unary(&ref[672],R(0))
	move_ret ref[50]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_114
	ret_reg &seed[114]
	call_c   build_ref_12()
	call_c   build_ref_54()
	call_c   Dyam_Seed_Start(&ref[12],&ref[54],I(0),fun13,1)
	call_c   build_ref_55()
	call_c   Dyam_Seed_Add_Comp(&ref[55],&ref[54],0)
	call_c   Dyam_Seed_End()
	move_ret seed[114]
	c_ret

;; TERM 55: '*GUARD*'(check_ground_list(_C)) :> '$$HOLE$$'
c_code local build_ref_55
	ret_reg &ref[55]
	call_c   build_ref_54()
	call_c   Dyam_Create_Binary(I(9),&ref[54],I(7))
	move_ret ref[55]
	c_ret

;; TERM 54: '*GUARD*'(check_ground_list(_C))
c_code local build_ref_54
	ret_reg &ref[54]
	call_c   build_ref_12()
	call_c   build_ref_53()
	call_c   Dyam_Create_Unary(&ref[12],&ref[53])
	move_ret ref[54]
	c_ret

;; TERM 53: check_ground_list(_C)
c_code local build_ref_53
	ret_reg &ref[53]
	call_c   build_ref_672()
	call_c   Dyam_Create_Unary(&ref[672],V(2))
	move_ret ref[53]
	c_ret

long local pool_fun26[3]=[2,build_ref_51,build_seed_114]

pl_code local fun26
	call_c   Dyam_Pool(pool_fun26)
	call_c   Dyam_Unify_Item(&ref[51])
	fail_ret
	call_c   DYAM_Ground_1(V(1))
	fail_ret
	pl_jump  fun25(&seed[114],1)

;; TERM 52: '*GUARD*'(check_ground_list([_B|_C])) :> []
c_code local build_ref_52
	ret_reg &ref[52]
	call_c   build_ref_51()
	call_c   Dyam_Create_Binary(I(9),&ref[51],I(0))
	move_ret ref[52]
	c_ret

c_code local build_seed_15
	ret_reg &seed[15]
	call_c   build_ref_11()
	call_c   build_ref_58()
	call_c   Dyam_Seed_Start(&ref[11],&ref[58],I(0),fun0,1)
	call_c   build_ref_57()
	call_c   Dyam_Seed_Add_Comp(&ref[57],fun17,0)
	call_c   Dyam_Seed_End()
	move_ret seed[15]
	c_ret

;; TERM 57: '*GUARD*'(directive(no_empty_kleene_body, _B, _C, _D))
c_code local build_ref_57
	ret_reg &ref[57]
	call_c   build_ref_12()
	call_c   build_ref_56()
	call_c   Dyam_Create_Unary(&ref[12],&ref[56])
	move_ret ref[57]
	c_ret

;; TERM 56: directive(no_empty_kleene_body, _B, _C, _D)
c_code local build_ref_56
	ret_reg &ref[56]
	call_c   build_ref_682()
	call_c   build_ref_59()
	call_c   Dyam_Term_Start(&ref[682],4)
	call_c   Dyam_Term_Arg(&ref[59])
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[56]
	c_ret

;; TERM 59: no_empty_kleene_body
c_code local build_ref_59
	ret_reg &ref[59]
	call_c   Dyam_Create_Atom("no_empty_kleene_body")
	move_ret ref[59]
	c_ret

;; TERM 682: directive
c_code local build_ref_682
	ret_reg &ref[682]
	call_c   Dyam_Create_Atom("directive")
	move_ret ref[682]
	c_ret

long local pool_fun17[3]=[2,build_ref_57,build_ref_59]

pl_code local fun17
	call_c   Dyam_Pool(pool_fun17)
	call_c   Dyam_Unify_Item(&ref[57])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[59], R(0)
	move     0, R(1)
	call_c   Dyam_Reg_Deallocate(1)
	pl_jump  pred_record_without_doublon_1()

;; TERM 58: '*GUARD*'(directive(no_empty_kleene_body, _B, _C, _D)) :> []
c_code local build_ref_58
	ret_reg &ref[58]
	call_c   build_ref_57()
	call_c   Dyam_Create_Binary(I(9),&ref[57],I(0))
	move_ret ref[58]
	c_ret

c_code local build_seed_52
	ret_reg &seed[52]
	call_c   build_ref_11()
	call_c   build_ref_62()
	call_c   Dyam_Seed_Start(&ref[11],&ref[62],I(0),fun0,1)
	call_c   build_ref_61()
	call_c   Dyam_Seed_Add_Comp(&ref[61],fun18,0)
	call_c   Dyam_Seed_End()
	move_ret seed[52]
	c_ret

;; TERM 61: '*GUARD*'(directive(autoload, _B, _C, _D))
c_code local build_ref_61
	ret_reg &ref[61]
	call_c   build_ref_12()
	call_c   build_ref_60()
	call_c   Dyam_Create_Unary(&ref[12],&ref[60])
	move_ret ref[61]
	c_ret

;; TERM 60: directive(autoload, _B, _C, _D)
c_code local build_ref_60
	ret_reg &ref[60]
	call_c   build_ref_682()
	call_c   build_ref_63()
	call_c   Dyam_Term_Start(&ref[682],4)
	call_c   Dyam_Term_Arg(&ref[63])
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[60]
	c_ret

;; TERM 63: autoload
c_code local build_ref_63
	ret_reg &ref[63]
	call_c   Dyam_Create_Atom("autoload")
	move_ret ref[63]
	c_ret

long local pool_fun18[3]=[2,build_ref_61,build_ref_63]

pl_code local fun18
	call_c   Dyam_Pool(pool_fun18)
	call_c   Dyam_Unify_Item(&ref[61])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[63], R(0)
	move     0, R(1)
	call_c   Dyam_Reg_Deallocate(1)
	pl_jump  pred_record_without_doublon_1()

;; TERM 62: '*GUARD*'(directive(autoload, _B, _C, _D)) :> []
c_code local build_ref_62
	ret_reg &ref[62]
	call_c   build_ref_61()
	call_c   Dyam_Create_Binary(I(9),&ref[61],I(0))
	move_ret ref[62]
	c_ret

c_code local build_seed_14
	ret_reg &seed[14]
	call_c   build_ref_11()
	call_c   build_ref_49()
	call_c   Dyam_Seed_Start(&ref[11],&ref[49],I(0),fun0,1)
	call_c   build_ref_48()
	call_c   Dyam_Seed_Add_Comp(&ref[48],fun16,0)
	call_c   Dyam_Seed_End()
	move_ret seed[14]
	c_ret

;; TERM 48: '*GUARD*'(clause_type((?- _B), '$query'(_B, _C), _C, _D))
c_code local build_ref_48
	ret_reg &ref[48]
	call_c   build_ref_12()
	call_c   build_ref_47()
	call_c   Dyam_Create_Unary(&ref[12],&ref[47])
	move_ret ref[48]
	c_ret

;; TERM 47: clause_type((?- _B), '$query'(_B, _C), _C, _D)
c_code local build_ref_47
	ret_reg &ref[47]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_683()
	call_c   Dyam_Create_Unary(&ref[683],V(1))
	move_ret R(0)
	call_c   build_ref_684()
	call_c   Dyam_Create_Binary(&ref[684],V(1),V(2))
	move_ret R(1)
	call_c   build_ref_677()
	call_c   Dyam_Term_Start(&ref[677],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[47]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 684: '$query'
c_code local build_ref_684
	ret_reg &ref[684]
	call_c   Dyam_Create_Atom("$query")
	move_ret ref[684]
	c_ret

;; TERM 683: ?-
c_code local build_ref_683
	ret_reg &ref[683]
	call_c   Dyam_Create_Atom("?-")
	move_ret ref[683]
	c_ret

pl_code local fun16
	call_c   build_ref_48()
	call_c   Dyam_Unify_Item(&ref[48])
	fail_ret
	pl_ret

;; TERM 49: '*GUARD*'(clause_type((?- _B), '$query'(_B, _C), _C, _D)) :> []
c_code local build_ref_49
	ret_reg &ref[49]
	call_c   build_ref_48()
	call_c   Dyam_Create_Binary(I(9),&ref[48],I(0))
	move_ret ref[49]
	c_ret

c_code local build_seed_48
	ret_reg &seed[48]
	call_c   build_ref_11()
	call_c   build_ref_66()
	call_c   Dyam_Seed_Start(&ref[11],&ref[66],I(0),fun0,1)
	call_c   build_ref_65()
	call_c   Dyam_Seed_Add_Comp(&ref[65],fun19,0)
	call_c   Dyam_Seed_End()
	move_ret seed[48]
	c_ret

;; TERM 65: '*GUARD*'(directive(debug, _B, _C, _D))
c_code local build_ref_65
	ret_reg &ref[65]
	call_c   build_ref_12()
	call_c   build_ref_64()
	call_c   Dyam_Create_Unary(&ref[12],&ref[64])
	move_ret ref[65]
	c_ret

;; TERM 64: directive(debug, _B, _C, _D)
c_code local build_ref_64
	ret_reg &ref[64]
	call_c   build_ref_682()
	call_c   build_ref_67()
	call_c   Dyam_Term_Start(&ref[682],4)
	call_c   Dyam_Term_Arg(&ref[67])
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[64]
	c_ret

;; TERM 67: debug
c_code local build_ref_67
	ret_reg &ref[67]
	call_c   Dyam_Create_Atom("debug")
	move_ret ref[67]
	c_ret

long local pool_fun19[3]=[2,build_ref_65,build_ref_67]

pl_code local fun19
	call_c   Dyam_Pool(pool_fun19)
	call_c   Dyam_Unify_Item(&ref[65])
	fail_ret
	call_c   DYAM_evpred_retract(&ref[67])
	fail_ret
	call_c   DYAM_evpred_assert_1(&ref[67])
	pl_ret

;; TERM 66: '*GUARD*'(directive(debug, _B, _C, _D)) :> []
c_code local build_ref_66
	ret_reg &ref[66]
	call_c   build_ref_65()
	call_c   Dyam_Create_Binary(I(9),&ref[65],I(0))
	move_ret ref[66]
	c_ret

c_code local build_seed_62
	ret_reg &seed[62]
	call_c   build_ref_11()
	call_c   build_ref_70()
	call_c   Dyam_Seed_Start(&ref[11],&ref[70],I(0),fun0,1)
	call_c   build_ref_69()
	call_c   Dyam_Seed_Add_Comp(&ref[69],fun20,0)
	call_c   Dyam_Seed_End()
	move_ret seed[62]
	c_ret

;; TERM 69: '*GUARD*'(directive((toplevel_clause _B), _C, _D, _E))
c_code local build_ref_69
	ret_reg &ref[69]
	call_c   build_ref_12()
	call_c   build_ref_68()
	call_c   Dyam_Create_Unary(&ref[12],&ref[68])
	move_ret ref[69]
	c_ret

;; TERM 68: directive((toplevel_clause _B), _C, _D, _E)
c_code local build_ref_68
	ret_reg &ref[68]
	call_c   build_ref_682()
	call_c   build_ref_71()
	call_c   Dyam_Term_Start(&ref[682],4)
	call_c   Dyam_Term_Arg(&ref[71])
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[68]
	c_ret

;; TERM 71: toplevel_clause _B
c_code local build_ref_71
	ret_reg &ref[71]
	call_c   build_ref_685()
	call_c   Dyam_Create_Unary(&ref[685],V(1))
	move_ret ref[71]
	c_ret

;; TERM 685: toplevel_clause
c_code local build_ref_685
	ret_reg &ref[685]
	call_c   Dyam_Create_Atom("toplevel_clause")
	move_ret ref[685]
	c_ret

long local pool_fun20[3]=[2,build_ref_69,build_ref_71]

pl_code local fun20
	call_c   Dyam_Pool(pool_fun20)
	call_c   Dyam_Unify_Item(&ref[69])
	fail_ret
	call_c   DYAM_evpred_assert_1(&ref[71])
	pl_ret

;; TERM 70: '*GUARD*'(directive((toplevel_clause _B), _C, _D, _E)) :> []
c_code local build_ref_70
	ret_reg &ref[70]
	call_c   build_ref_69()
	call_c   Dyam_Create_Binary(I(9),&ref[69],I(0))
	move_ret ref[70]
	c_ret

c_code local build_seed_64
	ret_reg &seed[64]
	call_c   build_ref_11()
	call_c   build_ref_74()
	call_c   Dyam_Seed_Start(&ref[11],&ref[74],I(0),fun0,1)
	call_c   build_ref_73()
	call_c   Dyam_Seed_Add_Comp(&ref[73],fun21,0)
	call_c   Dyam_Seed_End()
	move_ret seed[64]
	c_ret

;; TERM 73: '*GUARD*'(directive(subsumption(_B), _C, _D, _E))
c_code local build_ref_73
	ret_reg &ref[73]
	call_c   build_ref_12()
	call_c   build_ref_72()
	call_c   Dyam_Create_Unary(&ref[12],&ref[72])
	move_ret ref[73]
	c_ret

;; TERM 72: directive(subsumption(_B), _C, _D, _E)
c_code local build_ref_72
	ret_reg &ref[72]
	call_c   build_ref_682()
	call_c   build_ref_75()
	call_c   Dyam_Term_Start(&ref[682],4)
	call_c   Dyam_Term_Arg(&ref[75])
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[72]
	c_ret

;; TERM 75: subsumption(_B)
c_code local build_ref_75
	ret_reg &ref[75]
	call_c   build_ref_686()
	call_c   Dyam_Create_Unary(&ref[686],V(1))
	move_ret ref[75]
	c_ret

;; TERM 686: subsumption
c_code local build_ref_686
	ret_reg &ref[686]
	call_c   Dyam_Create_Atom("subsumption")
	move_ret ref[686]
	c_ret

long local pool_fun21[3]=[2,build_ref_73,build_ref_75]

pl_code local fun21
	call_c   Dyam_Pool(pool_fun21)
	call_c   Dyam_Unify_Item(&ref[73])
	fail_ret
	call_c   DYAM_evpred_assert_1(&ref[75])
	pl_ret

;; TERM 74: '*GUARD*'(directive(subsumption(_B), _C, _D, _E)) :> []
c_code local build_ref_74
	ret_reg &ref[74]
	call_c   build_ref_73()
	call_c   Dyam_Create_Binary(I(9),&ref[73],I(0))
	move_ret ref[74]
	c_ret

c_code local build_seed_74
	ret_reg &seed[74]
	call_c   build_ref_11()
	call_c   build_ref_78()
	call_c   Dyam_Seed_Start(&ref[11],&ref[78],I(0),fun0,1)
	call_c   build_ref_77()
	call_c   Dyam_Seed_Add_Comp(&ref[77],fun22,0)
	call_c   Dyam_Seed_End()
	move_ret seed[74]
	c_ret

;; TERM 77: '*GUARD*'(directive(nabla_variance(_B), _C, _D, _E))
c_code local build_ref_77
	ret_reg &ref[77]
	call_c   build_ref_12()
	call_c   build_ref_76()
	call_c   Dyam_Create_Unary(&ref[12],&ref[76])
	move_ret ref[77]
	c_ret

;; TERM 76: directive(nabla_variance(_B), _C, _D, _E)
c_code local build_ref_76
	ret_reg &ref[76]
	call_c   build_ref_682()
	call_c   build_ref_79()
	call_c   Dyam_Term_Start(&ref[682],4)
	call_c   Dyam_Term_Arg(&ref[79])
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[76]
	c_ret

;; TERM 79: nabla_variance(_B)
c_code local build_ref_79
	ret_reg &ref[79]
	call_c   build_ref_687()
	call_c   Dyam_Create_Unary(&ref[687],V(1))
	move_ret ref[79]
	c_ret

;; TERM 687: nabla_variance
c_code local build_ref_687
	ret_reg &ref[687]
	call_c   Dyam_Create_Atom("nabla_variance")
	move_ret ref[687]
	c_ret

long local pool_fun22[3]=[2,build_ref_77,build_ref_79]

pl_code local fun22
	call_c   Dyam_Pool(pool_fun22)
	call_c   Dyam_Unify_Item(&ref[77])
	fail_ret
	call_c   DYAM_evpred_assert_1(&ref[79])
	pl_ret

;; TERM 78: '*GUARD*'(directive(nabla_variance(_B), _C, _D, _E)) :> []
c_code local build_ref_78
	ret_reg &ref[78]
	call_c   build_ref_77()
	call_c   Dyam_Create_Binary(I(9),&ref[77],I(0))
	move_ret ref[78]
	c_ret

c_code local build_seed_75
	ret_reg &seed[75]
	call_c   build_ref_11()
	call_c   build_ref_82()
	call_c   Dyam_Seed_Start(&ref[11],&ref[82],I(0),fun0,1)
	call_c   build_ref_81()
	call_c   Dyam_Seed_Add_Comp(&ref[81],fun23,0)
	call_c   Dyam_Seed_End()
	move_ret seed[75]
	c_ret

;; TERM 81: '*GUARD*'(directive(nabla_subsume(_B), _C, _D, _E))
c_code local build_ref_81
	ret_reg &ref[81]
	call_c   build_ref_12()
	call_c   build_ref_80()
	call_c   Dyam_Create_Unary(&ref[12],&ref[80])
	move_ret ref[81]
	c_ret

;; TERM 80: directive(nabla_subsume(_B), _C, _D, _E)
c_code local build_ref_80
	ret_reg &ref[80]
	call_c   build_ref_682()
	call_c   build_ref_83()
	call_c   Dyam_Term_Start(&ref[682],4)
	call_c   Dyam_Term_Arg(&ref[83])
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[80]
	c_ret

;; TERM 83: nabla_subsume(_B)
c_code local build_ref_83
	ret_reg &ref[83]
	call_c   build_ref_688()
	call_c   Dyam_Create_Unary(&ref[688],V(1))
	move_ret ref[83]
	c_ret

;; TERM 688: nabla_subsume
c_code local build_ref_688
	ret_reg &ref[688]
	call_c   Dyam_Create_Atom("nabla_subsume")
	move_ret ref[688]
	c_ret

long local pool_fun23[3]=[2,build_ref_81,build_ref_83]

pl_code local fun23
	call_c   Dyam_Pool(pool_fun23)
	call_c   Dyam_Unify_Item(&ref[81])
	fail_ret
	call_c   DYAM_evpred_assert_1(&ref[83])
	pl_ret

;; TERM 82: '*GUARD*'(directive(nabla_subsume(_B), _C, _D, _E)) :> []
c_code local build_ref_82
	ret_reg &ref[82]
	call_c   build_ref_81()
	call_c   Dyam_Create_Binary(I(9),&ref[81],I(0))
	move_ret ref[82]
	c_ret

c_code local build_seed_16
	ret_reg &seed[16]
	call_c   build_ref_11()
	call_c   build_ref_86()
	call_c   Dyam_Seed_Start(&ref[11],&ref[86],I(0),fun0,1)
	call_c   build_ref_85()
	call_c   Dyam_Seed_Add_Comp(&ref[85],fun24,0)
	call_c   Dyam_Seed_End()
	move_ret seed[16]
	c_ret

;; TERM 85: '*GUARD*'(installer(import(_B), _C, _D))
c_code local build_ref_85
	ret_reg &ref[85]
	call_c   build_ref_12()
	call_c   build_ref_84()
	call_c   Dyam_Create_Unary(&ref[12],&ref[84])
	move_ret ref[85]
	c_ret

;; TERM 84: installer(import(_B), _C, _D)
c_code local build_ref_84
	ret_reg &ref[84]
	call_c   build_ref_681()
	call_c   build_ref_525()
	call_c   Dyam_Term_Start(&ref[681],3)
	call_c   Dyam_Term_Arg(&ref[525])
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[84]
	c_ret

;; TERM 525: import(_B)
c_code local build_ref_525
	ret_reg &ref[525]
	call_c   build_ref_689()
	call_c   Dyam_Create_Unary(&ref[689],V(1))
	move_ret ref[525]
	c_ret

;; TERM 689: import
c_code local build_ref_689
	ret_reg &ref[689]
	call_c   Dyam_Create_Atom("import")
	move_ret ref[689]
	c_ret

;; TERM 87: module_import(_C, _B)
c_code local build_ref_87
	ret_reg &ref[87]
	call_c   build_ref_690()
	call_c   Dyam_Create_Binary(&ref[690],V(2),V(1))
	move_ret ref[87]
	c_ret

;; TERM 690: module_import
c_code local build_ref_690
	ret_reg &ref[690]
	call_c   Dyam_Create_Atom("module_import")
	move_ret ref[690]
	c_ret

long local pool_fun24[3]=[2,build_ref_85,build_ref_87]

pl_code local fun24
	call_c   Dyam_Pool(pool_fun24)
	call_c   Dyam_Unify_Item(&ref[85])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[87], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Deallocate(1)
	pl_jump  pred_record_without_doublon_1()

;; TERM 86: '*GUARD*'(installer(import(_B), _C, _D)) :> []
c_code local build_ref_86
	ret_reg &ref[86]
	call_c   build_ref_85()
	call_c   Dyam_Create_Binary(I(9),&ref[85],I(0))
	move_ret ref[86]
	c_ret

c_code local build_seed_63
	ret_reg &seed[63]
	call_c   build_ref_11()
	call_c   build_ref_90()
	call_c   Dyam_Seed_Start(&ref[11],&ref[90],I(0),fun0,1)
	call_c   build_ref_89()
	call_c   Dyam_Seed_Add_Comp(&ref[89],fun27,0)
	call_c   Dyam_Seed_End()
	move_ret seed[63]
	c_ret

;; TERM 89: '*GUARD*'(directive((xcompiler _B), _C, _D, _E))
c_code local build_ref_89
	ret_reg &ref[89]
	call_c   build_ref_12()
	call_c   build_ref_88()
	call_c   Dyam_Create_Unary(&ref[12],&ref[88])
	move_ret ref[89]
	c_ret

;; TERM 88: directive((xcompiler _B), _C, _D, _E)
c_code local build_ref_88
	ret_reg &ref[88]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_691()
	call_c   Dyam_Create_Unary(&ref[691],V(1))
	move_ret R(0)
	call_c   build_ref_682()
	call_c   Dyam_Term_Start(&ref[682],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[88]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 691: xcompiler
c_code local build_ref_691
	ret_reg &ref[691]
	call_c   Dyam_Create_Atom("xcompiler")
	move_ret ref[691]
	c_ret

pl_code local fun27
	call_c   build_ref_89()
	call_c   Dyam_Unify_Item(&ref[89])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Deallocate(1)
	pl_jump  pred_extend_compiler_1()

;; TERM 90: '*GUARD*'(directive((xcompiler _B), _C, _D, _E)) :> []
c_code local build_ref_90
	ret_reg &ref[90]
	call_c   build_ref_89()
	call_c   Dyam_Create_Binary(I(9),&ref[89],I(0))
	move_ret ref[90]
	c_ret

c_code local build_seed_24
	ret_reg &seed[24]
	call_c   build_ref_11()
	call_c   build_ref_121()
	call_c   Dyam_Seed_Start(&ref[11],&ref[121],I(0),fun0,1)
	call_c   build_ref_120()
	call_c   Dyam_Seed_Add_Comp(&ref[120],fun49,0)
	call_c   Dyam_Seed_End()
	move_ret seed[24]
	c_ret

;; TERM 120: '*GUARD*'(installer(bmg_island(_B), (_C / _D), _E))
c_code local build_ref_120
	ret_reg &ref[120]
	call_c   build_ref_12()
	call_c   build_ref_119()
	call_c   Dyam_Create_Unary(&ref[12],&ref[119])
	move_ret ref[120]
	c_ret

;; TERM 119: installer(bmg_island(_B), (_C / _D), _E)
c_code local build_ref_119
	ret_reg &ref[119]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_692()
	call_c   Dyam_Create_Binary(&ref[692],V(2),V(3))
	move_ret R(0)
	call_c   build_ref_681()
	call_c   build_ref_155()
	call_c   Dyam_Term_Start(&ref[681],3)
	call_c   Dyam_Term_Arg(&ref[155])
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[119]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 155: bmg_island(_B)
c_code local build_ref_155
	ret_reg &ref[155]
	call_c   build_ref_693()
	call_c   Dyam_Create_Unary(&ref[693],V(1))
	move_ret ref[155]
	c_ret

;; TERM 693: bmg_island
c_code local build_ref_693
	ret_reg &ref[693]
	call_c   Dyam_Create_Atom("bmg_island")
	move_ret ref[693]
	c_ret

;; TERM 692: /
c_code local build_ref_692
	ret_reg &ref[692]
	call_c   Dyam_Create_Atom("/")
	move_ret ref[692]
	c_ret

;; TERM 122: bmg_island(_B, _C)
c_code local build_ref_122
	ret_reg &ref[122]
	call_c   build_ref_693()
	call_c   Dyam_Create_Binary(&ref[693],V(1),V(2))
	move_ret ref[122]
	c_ret

long local pool_fun49[3]=[2,build_ref_120,build_ref_122]

pl_code local fun49
	call_c   Dyam_Pool(pool_fun49)
	call_c   Dyam_Unify_Item(&ref[120])
	fail_ret
	call_c   DYAM_evpred_assert_1(&ref[122])
	pl_ret

;; TERM 121: '*GUARD*'(installer(bmg_island(_B), (_C / _D), _E)) :> []
c_code local build_ref_121
	ret_reg &ref[121]
	call_c   build_ref_120()
	call_c   Dyam_Create_Binary(I(9),&ref[120],I(0))
	move_ret ref[121]
	c_ret

c_code local build_seed_25
	ret_reg &seed[25]
	call_c   build_ref_11()
	call_c   build_ref_125()
	call_c   Dyam_Seed_Start(&ref[11],&ref[125],I(0),fun0,1)
	call_c   build_ref_124()
	call_c   Dyam_Seed_Add_Comp(&ref[124],fun50,0)
	call_c   Dyam_Seed_End()
	move_ret seed[25]
	c_ret

;; TERM 124: '*GUARD*'(installer(bmg_pushable2(_B), (_C / _D), _E))
c_code local build_ref_124
	ret_reg &ref[124]
	call_c   build_ref_12()
	call_c   build_ref_123()
	call_c   Dyam_Create_Unary(&ref[12],&ref[123])
	move_ret ref[124]
	c_ret

;; TERM 123: installer(bmg_pushable2(_B), (_C / _D), _E)
c_code local build_ref_123
	ret_reg &ref[123]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_694()
	call_c   Dyam_Create_Unary(&ref[694],V(1))
	move_ret R(0)
	call_c   build_ref_692()
	call_c   Dyam_Create_Binary(&ref[692],V(2),V(3))
	move_ret R(1)
	call_c   build_ref_681()
	call_c   Dyam_Term_Start(&ref[681],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[123]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 694: bmg_pushable2
c_code local build_ref_694
	ret_reg &ref[694]
	call_c   Dyam_Create_Atom("bmg_pushable2")
	move_ret ref[694]
	c_ret

;; TERM 126: bmg_pushable(_B, _C)
c_code local build_ref_126
	ret_reg &ref[126]
	call_c   build_ref_695()
	call_c   Dyam_Create_Binary(&ref[695],V(1),V(2))
	move_ret ref[126]
	c_ret

;; TERM 695: bmg_pushable
c_code local build_ref_695
	ret_reg &ref[695]
	call_c   Dyam_Create_Atom("bmg_pushable")
	move_ret ref[695]
	c_ret

long local pool_fun50[3]=[2,build_ref_124,build_ref_126]

pl_code local fun50
	call_c   Dyam_Pool(pool_fun50)
	call_c   Dyam_Unify_Item(&ref[124])
	fail_ret
	call_c   DYAM_evpred_assert_1(&ref[126])
	pl_ret

;; TERM 125: '*GUARD*'(installer(bmg_pushable2(_B), (_C / _D), _E)) :> []
c_code local build_ref_125
	ret_reg &ref[125]
	call_c   build_ref_124()
	call_c   Dyam_Create_Binary(I(9),&ref[124],I(0))
	move_ret ref[125]
	c_ret

c_code local build_seed_91
	ret_reg &seed[91]
	call_c   build_ref_11()
	call_c   build_ref_135()
	call_c   Dyam_Seed_Start(&ref[11],&ref[135],I(0),fun0,1)
	call_c   build_ref_134()
	call_c   Dyam_Seed_Add_Comp(&ref[134],fun51,0)
	call_c   Dyam_Seed_End()
	move_ret seed[91]
	c_ret

;; TERM 134: '*GUARD*'(directive((deref_term _B), _C, _D, _E))
c_code local build_ref_134
	ret_reg &ref[134]
	call_c   build_ref_12()
	call_c   build_ref_133()
	call_c   Dyam_Create_Unary(&ref[12],&ref[133])
	move_ret ref[134]
	c_ret

;; TERM 133: directive((deref_term _B), _C, _D, _E)
c_code local build_ref_133
	ret_reg &ref[133]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_136()
	call_c   Dyam_Create_Unary(&ref[136],V(1))
	move_ret R(0)
	call_c   build_ref_682()
	call_c   Dyam_Term_Start(&ref[682],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[133]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 136: deref_term
c_code local build_ref_136
	ret_reg &ref[136]
	call_c   Dyam_Create_Atom("deref_term")
	move_ret ref[136]
	c_ret

long local pool_fun51[3]=[2,build_ref_134,build_ref_136]

pl_code local fun51
	call_c   Dyam_Pool(pool_fun51)
	call_c   Dyam_Unify_Item(&ref[134])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[136], R(0)
	move     0, R(1)
	call_c   Dyam_Reg_Load(2,V(1))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_install_2()

;; TERM 135: '*GUARD*'(directive((deref_term _B), _C, _D, _E)) :> []
c_code local build_ref_135
	ret_reg &ref[135]
	call_c   build_ref_134()
	call_c   Dyam_Create_Binary(I(9),&ref[134],I(0))
	move_ret ref[135]
	c_ret

c_code local build_seed_92
	ret_reg &seed[92]
	call_c   build_ref_11()
	call_c   build_ref_139()
	call_c   Dyam_Seed_Start(&ref[11],&ref[139],I(0),fun0,1)
	call_c   build_ref_138()
	call_c   Dyam_Seed_Add_Comp(&ref[138],fun52,0)
	call_c   Dyam_Seed_End()
	move_ret seed[92]
	c_ret

;; TERM 138: '*GUARD*'(directive((hilog _B), _C, _D, _E))
c_code local build_ref_138
	ret_reg &ref[138]
	call_c   build_ref_12()
	call_c   build_ref_137()
	call_c   Dyam_Create_Unary(&ref[12],&ref[137])
	move_ret ref[138]
	c_ret

;; TERM 137: directive((hilog _B), _C, _D, _E)
c_code local build_ref_137
	ret_reg &ref[137]
	call_c   build_ref_682()
	call_c   build_ref_283()
	call_c   Dyam_Term_Start(&ref[682],4)
	call_c   Dyam_Term_Arg(&ref[283])
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[137]
	c_ret

;; TERM 283: hilog _B
c_code local build_ref_283
	ret_reg &ref[283]
	call_c   build_ref_696()
	call_c   Dyam_Create_Unary(&ref[696],V(1))
	move_ret ref[283]
	c_ret

;; TERM 696: hilog
c_code local build_ref_696
	ret_reg &ref[696]
	call_c   Dyam_Create_Atom("hilog")
	move_ret ref[696]
	c_ret

;; TERM 140: hilog_install
c_code local build_ref_140
	ret_reg &ref[140]
	call_c   Dyam_Create_Atom("hilog_install")
	move_ret ref[140]
	c_ret

long local pool_fun52[3]=[2,build_ref_138,build_ref_140]

pl_code local fun52
	call_c   Dyam_Pool(pool_fun52)
	call_c   Dyam_Unify_Item(&ref[138])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[140], R(0)
	move     0, R(1)
	call_c   Dyam_Reg_Load(2,V(1))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_install_2()

;; TERM 139: '*GUARD*'(directive((hilog _B), _C, _D, _E)) :> []
c_code local build_ref_139
	ret_reg &ref[139]
	call_c   build_ref_138()
	call_c   Dyam_Create_Binary(I(9),&ref[138],I(0))
	move_ret ref[139]
	c_ret

c_code local build_seed_94
	ret_reg &seed[94]
	call_c   build_ref_11()
	call_c   build_ref_143()
	call_c   Dyam_Seed_Start(&ref[11],&ref[143],I(0),fun0,1)
	call_c   build_ref_142()
	call_c   Dyam_Seed_Add_Comp(&ref[142],fun53,0)
	call_c   Dyam_Seed_End()
	move_ret seed[94]
	c_ret

;; TERM 142: '*GUARD*'(directive((resource _B), _C, _D, _E))
c_code local build_ref_142
	ret_reg &ref[142]
	call_c   build_ref_12()
	call_c   build_ref_141()
	call_c   Dyam_Create_Unary(&ref[12],&ref[141])
	move_ret ref[142]
	c_ret

;; TERM 141: directive((resource _B), _C, _D, _E)
c_code local build_ref_141
	ret_reg &ref[141]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_144()
	call_c   Dyam_Create_Unary(&ref[144],V(1))
	move_ret R(0)
	call_c   build_ref_682()
	call_c   Dyam_Term_Start(&ref[682],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[141]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 144: resource
c_code local build_ref_144
	ret_reg &ref[144]
	call_c   Dyam_Create_Atom("resource")
	move_ret ref[144]
	c_ret

long local pool_fun53[3]=[2,build_ref_142,build_ref_144]

pl_code local fun53
	call_c   Dyam_Pool(pool_fun53)
	call_c   Dyam_Unify_Item(&ref[142])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(1))
	move     &ref[144], R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_read_files_2()

;; TERM 143: '*GUARD*'(directive((resource _B), _C, _D, _E)) :> []
c_code local build_ref_143
	ret_reg &ref[143]
	call_c   build_ref_142()
	call_c   Dyam_Create_Binary(I(9),&ref[142],I(0))
	move_ret ref[143]
	c_ret

c_code local build_seed_96
	ret_reg &seed[96]
	call_c   build_ref_11()
	call_c   build_ref_147()
	call_c   Dyam_Seed_Start(&ref[11],&ref[147],I(0),fun0,1)
	call_c   build_ref_146()
	call_c   Dyam_Seed_Add_Comp(&ref[146],fun54,0)
	call_c   Dyam_Seed_End()
	move_ret seed[96]
	c_ret

;; TERM 146: '*GUARD*'(directive((require _B), _C, _D, _E))
c_code local build_ref_146
	ret_reg &ref[146]
	call_c   build_ref_12()
	call_c   build_ref_145()
	call_c   Dyam_Create_Unary(&ref[12],&ref[145])
	move_ret ref[146]
	c_ret

;; TERM 145: directive((require _B), _C, _D, _E)
c_code local build_ref_145
	ret_reg &ref[145]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_100()
	call_c   Dyam_Create_Unary(&ref[100],V(1))
	move_ret R(0)
	call_c   build_ref_682()
	call_c   Dyam_Term_Start(&ref[682],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[145]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 100: require
c_code local build_ref_100
	ret_reg &ref[100]
	call_c   Dyam_Create_Atom("require")
	move_ret ref[100]
	c_ret

long local pool_fun54[3]=[2,build_ref_146,build_ref_100]

pl_code local fun54
	call_c   Dyam_Pool(pool_fun54)
	call_c   Dyam_Unify_Item(&ref[146])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(1))
	move     &ref[100], R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_read_files_2()

;; TERM 147: '*GUARD*'(directive((require _B), _C, _D, _E)) :> []
c_code local build_ref_147
	ret_reg &ref[147]
	call_c   build_ref_146()
	call_c   Dyam_Create_Binary(I(9),&ref[146],I(0))
	move_ret ref[147]
	c_ret

c_code local build_seed_46
	ret_reg &seed[46]
	call_c   build_ref_11()
	call_c   build_ref_129()
	call_c   Dyam_Seed_Start(&ref[11],&ref[129],I(0),fun0,1)
	call_c   build_ref_128()
	call_c   Dyam_Seed_Add_Comp(&ref[128],fun131,0)
	call_c   Dyam_Seed_End()
	move_ret seed[46]
	c_ret

;; TERM 128: '*GUARD*'(install_elem(_B, _C, _D))
c_code local build_ref_128
	ret_reg &ref[128]
	call_c   build_ref_12()
	call_c   build_ref_127()
	call_c   Dyam_Create_Unary(&ref[12],&ref[127])
	move_ret ref[128]
	c_ret

;; TERM 127: install_elem(_B, _C, _D)
c_code local build_ref_127
	ret_reg &ref[127]
	call_c   build_ref_679()
	call_c   Dyam_Term_Start(&ref[679],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[127]
	c_ret

c_code local build_seed_116
	ret_reg &seed[116]
	call_c   build_ref_12()
	call_c   build_ref_131()
	call_c   Dyam_Seed_Start(&ref[12],&ref[131],I(0),fun13,1)
	call_c   build_ref_132()
	call_c   Dyam_Seed_Add_Comp(&ref[132],&ref[131],0)
	call_c   Dyam_Seed_End()
	move_ret seed[116]
	c_ret

;; TERM 132: '*GUARD*'(installer(_B, (_C / 0), _D)) :> '$$HOLE$$'
c_code local build_ref_132
	ret_reg &ref[132]
	call_c   build_ref_131()
	call_c   Dyam_Create_Binary(I(9),&ref[131],I(7))
	move_ret ref[132]
	c_ret

;; TERM 131: '*GUARD*'(installer(_B, (_C / 0), _D))
c_code local build_ref_131
	ret_reg &ref[131]
	call_c   build_ref_12()
	call_c   build_ref_130()
	call_c   Dyam_Create_Unary(&ref[12],&ref[130])
	move_ret ref[131]
	c_ret

;; TERM 130: installer(_B, (_C / 0), _D)
c_code local build_ref_130
	ret_reg &ref[130]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_692()
	call_c   Dyam_Create_Binary(&ref[692],V(2),N(0))
	move_ret R(0)
	call_c   build_ref_681()
	call_c   Dyam_Term_Start(&ref[681],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[130]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun131[3]=[2,build_ref_128,build_seed_116]

pl_code local fun131
	call_c   Dyam_Pool(pool_fun131)
	call_c   Dyam_Unify_Item(&ref[128])
	fail_ret
	call_c   DYAM_evpred_atom(V(2))
	fail_ret
	pl_jump  fun25(&seed[116],1)

;; TERM 129: '*GUARD*'(install_elem(_B, _C, _D)) :> []
c_code local build_ref_129
	ret_reg &ref[129]
	call_c   build_ref_128()
	call_c   Dyam_Create_Binary(I(9),&ref[128],I(0))
	move_ret ref[129]
	c_ret

c_code local build_seed_11
	ret_reg &seed[11]
	call_c   build_ref_11()
	call_c   build_ref_93()
	call_c   Dyam_Seed_Start(&ref[11],&ref[93],I(0),fun0,1)
	call_c   build_ref_92()
	call_c   Dyam_Seed_Add_Comp(&ref[92],fun28,0)
	call_c   Dyam_Seed_End()
	move_ret seed[11]
	c_ret

;; TERM 92: '*GUARD*'(clause_type((spinetree _B), tag_tree{family=> _C, name=> _D, tree=> (spinetree _B)}, _E, _F))
c_code local build_ref_92
	ret_reg &ref[92]
	call_c   build_ref_12()
	call_c   build_ref_91()
	call_c   Dyam_Create_Unary(&ref[12],&ref[91])
	move_ret ref[92]
	c_ret

;; TERM 91: clause_type((spinetree _B), tag_tree{family=> _C, name=> _D, tree=> (spinetree _B)}, _E, _F)
c_code local build_ref_91
	ret_reg &ref[91]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_697()
	call_c   Dyam_Create_Unary(&ref[697],V(1))
	move_ret R(0)
	call_c   build_ref_698()
	call_c   Dyam_Term_Start(&ref[698],3)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_End()
	move_ret R(1)
	call_c   build_ref_677()
	call_c   Dyam_Term_Start(&ref[677],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[91]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 698: tag_tree!'$ft'
c_code local build_ref_698
	ret_reg &ref[698]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_702()
	call_c   Dyam_Create_List(&ref[702],I(0))
	move_ret R(0)
	call_c   build_ref_701()
	call_c   Dyam_Create_List(&ref[701],R(0))
	move_ret R(0)
	call_c   build_ref_700()
	call_c   Dyam_Create_List(&ref[700],R(0))
	move_ret R(0)
	call_c   build_ref_699()
	call_c   Dyam_Create_Atom_Module("$ft",&ref[699])
	move_ret ref[698]
	call_c   DYAM_Feature_2(&ref[699],R(0))
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 699: tag_tree
c_code local build_ref_699
	ret_reg &ref[699]
	call_c   Dyam_Create_Atom("tag_tree")
	move_ret ref[699]
	c_ret

;; TERM 700: family
c_code local build_ref_700
	ret_reg &ref[700]
	call_c   Dyam_Create_Atom("family")
	move_ret ref[700]
	c_ret

;; TERM 701: name
c_code local build_ref_701
	ret_reg &ref[701]
	call_c   Dyam_Create_Atom("name")
	move_ret ref[701]
	c_ret

;; TERM 702: tree
c_code local build_ref_702
	ret_reg &ref[702]
	call_c   Dyam_Create_Atom("tree")
	move_ret ref[702]
	c_ret

;; TERM 697: spinetree
c_code local build_ref_697
	ret_reg &ref[697]
	call_c   Dyam_Create_Atom("spinetree")
	move_ret ref[697]
	c_ret

pl_code local fun28
	call_c   build_ref_92()
	call_c   Dyam_Unify_Item(&ref[92])
	fail_ret
	pl_ret

;; TERM 93: '*GUARD*'(clause_type((spinetree _B), tag_tree{family=> _C, name=> _D, tree=> (spinetree _B)}, _E, _F)) :> []
c_code local build_ref_93
	ret_reg &ref[93]
	call_c   build_ref_92()
	call_c   Dyam_Create_Binary(I(9),&ref[92],I(0))
	move_ret ref[93]
	c_ret

c_code local build_seed_12
	ret_reg &seed[12]
	call_c   build_ref_11()
	call_c   build_ref_96()
	call_c   Dyam_Seed_Start(&ref[11],&ref[96],I(0),fun0,1)
	call_c   build_ref_95()
	call_c   Dyam_Seed_Add_Comp(&ref[95],fun29,0)
	call_c   Dyam_Seed_End()
	move_ret seed[12]
	c_ret

;; TERM 95: '*GUARD*'(clause_type((auxtree _B), tag_tree{family=> _C, name=> _D, tree=> (auxtree _B)}, _E, _F))
c_code local build_ref_95
	ret_reg &ref[95]
	call_c   build_ref_12()
	call_c   build_ref_94()
	call_c   Dyam_Create_Unary(&ref[12],&ref[94])
	move_ret ref[95]
	c_ret

;; TERM 94: clause_type((auxtree _B), tag_tree{family=> _C, name=> _D, tree=> (auxtree _B)}, _E, _F)
c_code local build_ref_94
	ret_reg &ref[94]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_703()
	call_c   Dyam_Create_Unary(&ref[703],V(1))
	move_ret R(0)
	call_c   build_ref_698()
	call_c   Dyam_Term_Start(&ref[698],3)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_End()
	move_ret R(1)
	call_c   build_ref_677()
	call_c   Dyam_Term_Start(&ref[677],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[94]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 703: auxtree
c_code local build_ref_703
	ret_reg &ref[703]
	call_c   Dyam_Create_Atom("auxtree")
	move_ret ref[703]
	c_ret

pl_code local fun29
	call_c   build_ref_95()
	call_c   Dyam_Unify_Item(&ref[95])
	fail_ret
	pl_ret

;; TERM 96: '*GUARD*'(clause_type((auxtree _B), tag_tree{family=> _C, name=> _D, tree=> (auxtree _B)}, _E, _F)) :> []
c_code local build_ref_96
	ret_reg &ref[96]
	call_c   build_ref_95()
	call_c   Dyam_Create_Binary(I(9),&ref[95],I(0))
	move_ret ref[96]
	c_ret

c_code local build_seed_13
	ret_reg &seed[13]
	call_c   build_ref_11()
	call_c   build_ref_99()
	call_c   Dyam_Seed_Start(&ref[11],&ref[99],I(0),fun0,1)
	call_c   build_ref_98()
	call_c   Dyam_Seed_Add_Comp(&ref[98],fun30,0)
	call_c   Dyam_Seed_End()
	move_ret seed[13]
	c_ret

;; TERM 98: '*GUARD*'(clause_type((tree _B), tag_tree{family=> _C, name=> _D, tree=> (tree _B)}, _E, _F))
c_code local build_ref_98
	ret_reg &ref[98]
	call_c   build_ref_12()
	call_c   build_ref_97()
	call_c   Dyam_Create_Unary(&ref[12],&ref[97])
	move_ret ref[98]
	c_ret

;; TERM 97: clause_type((tree _B), tag_tree{family=> _C, name=> _D, tree=> (tree _B)}, _E, _F)
c_code local build_ref_97
	ret_reg &ref[97]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_702()
	call_c   Dyam_Create_Unary(&ref[702],V(1))
	move_ret R(0)
	call_c   build_ref_698()
	call_c   Dyam_Term_Start(&ref[698],3)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_End()
	move_ret R(1)
	call_c   build_ref_677()
	call_c   Dyam_Term_Start(&ref[677],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[97]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

pl_code local fun30
	call_c   build_ref_98()
	call_c   Dyam_Unify_Item(&ref[98])
	fail_ret
	pl_ret

;; TERM 99: '*GUARD*'(clause_type((tree _B), tag_tree{family=> _C, name=> _D, tree=> (tree _B)}, _E, _F)) :> []
c_code local build_ref_99
	ret_reg &ref[99]
	call_c   build_ref_98()
	call_c   Dyam_Create_Binary(I(9),&ref[98],I(0))
	move_ret ref[99]
	c_ret

c_code local build_seed_61
	ret_reg &seed[61]
	call_c   build_ref_11()
	call_c   build_ref_158()
	call_c   Dyam_Seed_Start(&ref[11],&ref[158],I(0),fun0,1)
	call_c   build_ref_157()
	call_c   Dyam_Seed_Add_Comp(&ref[157],fun58,0)
	call_c   Dyam_Seed_End()
	move_ret seed[61]
	c_ret

;; TERM 157: '*GUARD*'(directive(public(_B), _C, _D, _E))
c_code local build_ref_157
	ret_reg &ref[157]
	call_c   build_ref_12()
	call_c   build_ref_156()
	call_c   Dyam_Create_Unary(&ref[12],&ref[156])
	move_ret ref[157]
	c_ret

;; TERM 156: directive(public(_B), _C, _D, _E)
c_code local build_ref_156
	ret_reg &ref[156]
	call_c   build_ref_682()
	call_c   build_ref_203()
	call_c   Dyam_Term_Start(&ref[682],4)
	call_c   Dyam_Term_Arg(&ref[203])
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[156]
	c_ret

;; TERM 203: public(_B)
c_code local build_ref_203
	ret_reg &ref[203]
	call_c   build_ref_704()
	call_c   Dyam_Create_Unary(&ref[704],V(1))
	move_ret ref[203]
	c_ret

;; TERM 704: public
c_code local build_ref_704
	ret_reg &ref[704]
	call_c   Dyam_Create_Atom("public")
	move_ret ref[704]
	c_ret

;; TERM 159: flag(public)
c_code local build_ref_159
	ret_reg &ref[159]
	call_c   build_ref_705()
	call_c   build_ref_704()
	call_c   Dyam_Create_Unary(&ref[705],&ref[704])
	move_ret ref[159]
	c_ret

;; TERM 705: flag
c_code local build_ref_705
	ret_reg &ref[705]
	call_c   Dyam_Create_Atom("flag")
	move_ret ref[705]
	c_ret

long local pool_fun58[3]=[2,build_ref_157,build_ref_159]

pl_code local fun58
	call_c   Dyam_Pool(pool_fun58)
	call_c   Dyam_Unify_Item(&ref[157])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[159], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(1))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_install_2()

;; TERM 158: '*GUARD*'(directive(public(_B), _C, _D, _E)) :> []
c_code local build_ref_158
	ret_reg &ref[158]
	call_c   build_ref_157()
	call_c   Dyam_Create_Binary(I(9),&ref[157],I(0))
	move_ret ref[158]
	c_ret

c_code local build_seed_78
	ret_reg &seed[78]
	call_c   build_ref_11()
	call_c   build_ref_162()
	call_c   Dyam_Seed_Start(&ref[11],&ref[162],I(0),fun0,1)
	call_c   build_ref_161()
	call_c   Dyam_Seed_Add_Comp(&ref[161],fun59,0)
	call_c   Dyam_Seed_End()
	move_ret seed[78]
	c_ret

;; TERM 161: '*GUARD*'(directive((lco _B), _C, _D, _E))
c_code local build_ref_161
	ret_reg &ref[161]
	call_c   build_ref_12()
	call_c   build_ref_160()
	call_c   Dyam_Create_Unary(&ref[12],&ref[160])
	move_ret ref[161]
	c_ret

;; TERM 160: directive((lco _B), _C, _D, _E)
c_code local build_ref_160
	ret_reg &ref[160]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_661()
	call_c   Dyam_Create_Unary(&ref[661],V(1))
	move_ret R(0)
	call_c   build_ref_682()
	call_c   Dyam_Term_Start(&ref[682],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[160]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 163: flag(lco)
c_code local build_ref_163
	ret_reg &ref[163]
	call_c   build_ref_705()
	call_c   build_ref_661()
	call_c   Dyam_Create_Unary(&ref[705],&ref[661])
	move_ret ref[163]
	c_ret

long local pool_fun59[3]=[2,build_ref_161,build_ref_163]

pl_code local fun59
	call_c   Dyam_Pool(pool_fun59)
	call_c   Dyam_Unify_Item(&ref[161])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[163], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(1))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_install_2()

;; TERM 162: '*GUARD*'(directive((lco _B), _C, _D, _E)) :> []
c_code local build_ref_162
	ret_reg &ref[162]
	call_c   build_ref_161()
	call_c   Dyam_Create_Binary(I(9),&ref[161],I(0))
	move_ret ref[162]
	c_ret

c_code local build_seed_81
	ret_reg &seed[81]
	call_c   build_ref_11()
	call_c   build_ref_166()
	call_c   Dyam_Seed_Start(&ref[11],&ref[166],I(0),fun0,1)
	call_c   build_ref_165()
	call_c   Dyam_Seed_Add_Comp(&ref[165],fun60,0)
	call_c   Dyam_Seed_End()
	move_ret seed[81]
	c_ret

;; TERM 165: '*GUARD*'(directive((light_tabular _B), _C, _D, _E))
c_code local build_ref_165
	ret_reg &ref[165]
	call_c   build_ref_12()
	call_c   build_ref_164()
	call_c   Dyam_Create_Unary(&ref[12],&ref[164])
	move_ret ref[165]
	c_ret

;; TERM 164: directive((light_tabular _B), _C, _D, _E)
c_code local build_ref_164
	ret_reg &ref[164]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_663()
	call_c   Dyam_Create_Unary(&ref[663],V(1))
	move_ret R(0)
	call_c   build_ref_682()
	call_c   Dyam_Term_Start(&ref[682],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[164]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 167: flag(light_tabular)
c_code local build_ref_167
	ret_reg &ref[167]
	call_c   build_ref_705()
	call_c   build_ref_663()
	call_c   Dyam_Create_Unary(&ref[705],&ref[663])
	move_ret ref[167]
	c_ret

long local pool_fun60[3]=[2,build_ref_165,build_ref_167]

pl_code local fun60
	call_c   Dyam_Pool(pool_fun60)
	call_c   Dyam_Unify_Item(&ref[165])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[167], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(1))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_install_2()

;; TERM 166: '*GUARD*'(directive((light_tabular _B), _C, _D, _E)) :> []
c_code local build_ref_166
	ret_reg &ref[166]
	call_c   build_ref_165()
	call_c   Dyam_Create_Binary(I(9),&ref[165],I(0))
	move_ret ref[166]
	c_ret

c_code local build_seed_82
	ret_reg &seed[82]
	call_c   build_ref_11()
	call_c   build_ref_170()
	call_c   Dyam_Seed_Start(&ref[11],&ref[170],I(0),fun0,1)
	call_c   build_ref_169()
	call_c   Dyam_Seed_Add_Comp(&ref[169],fun61,0)
	call_c   Dyam_Seed_End()
	move_ret seed[82]
	c_ret

;; TERM 169: '*GUARD*'(directive((extensional _B), _C, _D, _E))
c_code local build_ref_169
	ret_reg &ref[169]
	call_c   build_ref_12()
	call_c   build_ref_168()
	call_c   Dyam_Create_Unary(&ref[12],&ref[168])
	move_ret ref[169]
	c_ret

;; TERM 168: directive((extensional _B), _C, _D, _E)
c_code local build_ref_168
	ret_reg &ref[168]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_667()
	call_c   Dyam_Create_Unary(&ref[667],V(1))
	move_ret R(0)
	call_c   build_ref_682()
	call_c   Dyam_Term_Start(&ref[682],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[168]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 171: flag(extensional)
c_code local build_ref_171
	ret_reg &ref[171]
	call_c   build_ref_705()
	call_c   build_ref_667()
	call_c   Dyam_Create_Unary(&ref[705],&ref[667])
	move_ret ref[171]
	c_ret

long local pool_fun61[3]=[2,build_ref_169,build_ref_171]

pl_code local fun61
	call_c   Dyam_Pool(pool_fun61)
	call_c   Dyam_Unify_Item(&ref[169])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[171], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(1))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_install_2()

;; TERM 170: '*GUARD*'(directive((extensional _B), _C, _D, _E)) :> []
c_code local build_ref_170
	ret_reg &ref[170]
	call_c   build_ref_169()
	call_c   Dyam_Create_Binary(I(9),&ref[169],I(0))
	move_ret ref[170]
	c_ret

c_code local build_seed_83
	ret_reg &seed[83]
	call_c   build_ref_11()
	call_c   build_ref_174()
	call_c   Dyam_Seed_Start(&ref[11],&ref[174],I(0),fun0,1)
	call_c   build_ref_173()
	call_c   Dyam_Seed_Add_Comp(&ref[173],fun62,0)
	call_c   Dyam_Seed_End()
	move_ret seed[83]
	c_ret

;; TERM 173: '*GUARD*'(directive((std_prolog _B), _C, _D, _E))
c_code local build_ref_173
	ret_reg &ref[173]
	call_c   build_ref_12()
	call_c   build_ref_172()
	call_c   Dyam_Create_Unary(&ref[12],&ref[172])
	move_ret ref[173]
	c_ret

;; TERM 172: directive((std_prolog _B), _C, _D, _E)
c_code local build_ref_172
	ret_reg &ref[172]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_664()
	call_c   Dyam_Create_Unary(&ref[664],V(1))
	move_ret R(0)
	call_c   build_ref_682()
	call_c   Dyam_Term_Start(&ref[682],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[172]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 175: flag(std_prolog)
c_code local build_ref_175
	ret_reg &ref[175]
	call_c   build_ref_705()
	call_c   build_ref_664()
	call_c   Dyam_Create_Unary(&ref[705],&ref[664])
	move_ret ref[175]
	c_ret

long local pool_fun62[3]=[2,build_ref_173,build_ref_175]

pl_code local fun62
	call_c   Dyam_Pool(pool_fun62)
	call_c   Dyam_Unify_Item(&ref[173])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[175], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(1))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_install_2()

;; TERM 174: '*GUARD*'(directive((std_prolog _B), _C, _D, _E)) :> []
c_code local build_ref_174
	ret_reg &ref[174]
	call_c   build_ref_173()
	call_c   Dyam_Create_Binary(I(9),&ref[173],I(0))
	move_ret ref[174]
	c_ret

c_code local build_seed_84
	ret_reg &seed[84]
	call_c   build_ref_11()
	call_c   build_ref_178()
	call_c   Dyam_Seed_Start(&ref[11],&ref[178],I(0),fun0,1)
	call_c   build_ref_177()
	call_c   Dyam_Seed_Add_Comp(&ref[177],fun63,0)
	call_c   Dyam_Seed_End()
	move_ret seed[84]
	c_ret

;; TERM 177: '*GUARD*'(directive((rec_prolog _B), _C, _D, _E))
c_code local build_ref_177
	ret_reg &ref[177]
	call_c   build_ref_12()
	call_c   build_ref_176()
	call_c   Dyam_Create_Unary(&ref[12],&ref[176])
	move_ret ref[177]
	c_ret

;; TERM 176: directive((rec_prolog _B), _C, _D, _E)
c_code local build_ref_176
	ret_reg &ref[176]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_665()
	call_c   Dyam_Create_Unary(&ref[665],V(1))
	move_ret R(0)
	call_c   build_ref_682()
	call_c   Dyam_Term_Start(&ref[682],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[176]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 179: flag(rec_prolog)
c_code local build_ref_179
	ret_reg &ref[179]
	call_c   build_ref_705()
	call_c   build_ref_665()
	call_c   Dyam_Create_Unary(&ref[705],&ref[665])
	move_ret ref[179]
	c_ret

long local pool_fun63[3]=[2,build_ref_177,build_ref_179]

pl_code local fun63
	call_c   Dyam_Pool(pool_fun63)
	call_c   Dyam_Unify_Item(&ref[177])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[179], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(1))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_install_2()

;; TERM 178: '*GUARD*'(directive((rec_prolog _B), _C, _D, _E)) :> []
c_code local build_ref_178
	ret_reg &ref[178]
	call_c   build_ref_177()
	call_c   Dyam_Create_Binary(I(9),&ref[177],I(0))
	move_ret ref[178]
	c_ret

c_code local build_seed_85
	ret_reg &seed[85]
	call_c   build_ref_11()
	call_c   build_ref_182()
	call_c   Dyam_Seed_Start(&ref[11],&ref[182],I(0),fun0,1)
	call_c   build_ref_181()
	call_c   Dyam_Seed_Add_Comp(&ref[181],fun64,0)
	call_c   Dyam_Seed_End()
	move_ret seed[85]
	c_ret

;; TERM 181: '*GUARD*'(directive((prolog _B), _C, _D, _E))
c_code local build_ref_181
	ret_reg &ref[181]
	call_c   build_ref_12()
	call_c   build_ref_180()
	call_c   Dyam_Create_Unary(&ref[12],&ref[180])
	move_ret ref[181]
	c_ret

;; TERM 180: directive((prolog _B), _C, _D, _E)
c_code local build_ref_180
	ret_reg &ref[180]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_666()
	call_c   Dyam_Create_Unary(&ref[666],V(1))
	move_ret R(0)
	call_c   build_ref_682()
	call_c   Dyam_Term_Start(&ref[682],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[180]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 183: flag(prolog)
c_code local build_ref_183
	ret_reg &ref[183]
	call_c   build_ref_705()
	call_c   build_ref_666()
	call_c   Dyam_Create_Unary(&ref[705],&ref[666])
	move_ret ref[183]
	c_ret

long local pool_fun64[3]=[2,build_ref_181,build_ref_183]

pl_code local fun64
	call_c   Dyam_Pool(pool_fun64)
	call_c   Dyam_Unify_Item(&ref[181])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[183], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(1))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_install_2()

;; TERM 182: '*GUARD*'(directive((prolog _B), _C, _D, _E)) :> []
c_code local build_ref_182
	ret_reg &ref[182]
	call_c   build_ref_181()
	call_c   Dyam_Create_Binary(I(9),&ref[181],I(0))
	move_ret ref[182]
	c_ret

c_code local build_seed_79
	ret_reg &seed[79]
	call_c   build_ref_11()
	call_c   build_ref_186()
	call_c   Dyam_Seed_Start(&ref[11],&ref[186],I(0),fun0,1)
	call_c   build_ref_185()
	call_c   Dyam_Seed_Add_Comp(&ref[185],fun65,0)
	call_c   Dyam_Seed_End()
	move_ret seed[79]
	c_ret

;; TERM 185: '*GUARD*'(directive(builtin(_B, _C), _D, _E, _F))
c_code local build_ref_185
	ret_reg &ref[185]
	call_c   build_ref_12()
	call_c   build_ref_184()
	call_c   Dyam_Create_Unary(&ref[12],&ref[184])
	move_ret ref[185]
	c_ret

;; TERM 184: directive(builtin(_B, _C), _D, _E, _F)
c_code local build_ref_184
	ret_reg &ref[184]
	call_c   build_ref_682()
	call_c   build_ref_187()
	call_c   Dyam_Term_Start(&ref[682],4)
	call_c   Dyam_Term_Arg(&ref[187])
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[184]
	c_ret

;; TERM 187: builtin(_B, _C)
c_code local build_ref_187
	ret_reg &ref[187]
	call_c   build_ref_706()
	call_c   Dyam_Create_Binary(&ref[706],V(1),V(2))
	move_ret ref[187]
	c_ret

;; TERM 706: builtin
c_code local build_ref_706
	ret_reg &ref[706]
	call_c   Dyam_Create_Atom("builtin")
	move_ret ref[706]
	c_ret

long local pool_fun65[3]=[2,build_ref_185,build_ref_187]

pl_code local fun65
	call_c   Dyam_Pool(pool_fun65)
	call_c   Dyam_Unify_Item(&ref[185])
	fail_ret
	call_c   DYAM_evpred_assert_1(&ref[187])
	pl_ret

;; TERM 186: '*GUARD*'(directive(builtin(_B, _C), _D, _E, _F)) :> []
c_code local build_ref_186
	ret_reg &ref[186]
	call_c   build_ref_185()
	call_c   Dyam_Create_Binary(I(9),&ref[185],I(0))
	move_ret ref[186]
	c_ret

c_code local build_seed_80
	ret_reg &seed[80]
	call_c   build_ref_11()
	call_c   build_ref_190()
	call_c   Dyam_Seed_Start(&ref[11],&ref[190],I(0),fun0,1)
	call_c   build_ref_189()
	call_c   Dyam_Seed_Add_Comp(&ref[189],fun66,0)
	call_c   Dyam_Seed_End()
	move_ret seed[80]
	c_ret

;; TERM 189: '*GUARD*'(directive(foreign(_B, _C), _D, _E, _F))
c_code local build_ref_189
	ret_reg &ref[189]
	call_c   build_ref_12()
	call_c   build_ref_188()
	call_c   Dyam_Create_Unary(&ref[12],&ref[188])
	move_ret ref[189]
	c_ret

;; TERM 188: directive(foreign(_B, _C), _D, _E, _F)
c_code local build_ref_188
	ret_reg &ref[188]
	call_c   build_ref_682()
	call_c   build_ref_191()
	call_c   Dyam_Term_Start(&ref[682],4)
	call_c   Dyam_Term_Arg(&ref[191])
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[188]
	c_ret

;; TERM 191: foreign(_B, _C)
c_code local build_ref_191
	ret_reg &ref[191]
	call_c   build_ref_707()
	call_c   Dyam_Create_Binary(&ref[707],V(1),V(2))
	move_ret ref[191]
	c_ret

;; TERM 707: foreign
c_code local build_ref_707
	ret_reg &ref[707]
	call_c   Dyam_Create_Atom("foreign")
	move_ret ref[707]
	c_ret

long local pool_fun66[3]=[2,build_ref_189,build_ref_191]

pl_code local fun66
	call_c   Dyam_Pool(pool_fun66)
	call_c   Dyam_Unify_Item(&ref[189])
	fail_ret
	call_c   DYAM_evpred_assert_1(&ref[191])
	pl_ret

;; TERM 190: '*GUARD*'(directive(foreign(_B, _C), _D, _E, _F)) :> []
c_code local build_ref_190
	ret_reg &ref[190]
	call_c   build_ref_189()
	call_c   Dyam_Create_Binary(I(9),&ref[189],I(0))
	move_ret ref[190]
	c_ret

c_code local build_seed_67
	ret_reg &seed[67]
	call_c   build_ref_11()
	call_c   build_ref_222()
	call_c   Dyam_Seed_Start(&ref[11],&ref[222],I(0),fun0,1)
	call_c   build_ref_221()
	call_c   Dyam_Seed_Add_Comp(&ref[221],fun99,0)
	call_c   Dyam_Seed_End()
	move_ret seed[67]
	c_ret

;; TERM 221: '*GUARD*'(directive(tag_family(_B), _C, _D, _E))
c_code local build_ref_221
	ret_reg &ref[221]
	call_c   build_ref_12()
	call_c   build_ref_220()
	call_c   Dyam_Create_Unary(&ref[12],&ref[220])
	move_ret ref[221]
	c_ret

;; TERM 220: directive(tag_family(_B), _C, _D, _E)
c_code local build_ref_220
	ret_reg &ref[220]
	call_c   build_ref_682()
	call_c   build_ref_224()
	call_c   Dyam_Term_Start(&ref[682],4)
	call_c   Dyam_Term_Arg(&ref[224])
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[220]
	c_ret

;; TERM 224: tag_family(_B)
c_code local build_ref_224
	ret_reg &ref[224]
	call_c   build_ref_708()
	call_c   Dyam_Create_Unary(&ref[708],V(1))
	move_ret ref[224]
	c_ret

;; TERM 708: tag_family
c_code local build_ref_708
	ret_reg &ref[708]
	call_c   Dyam_Create_Atom("tag_family")
	move_ret ref[708]
	c_ret

;; TERM 223: tag_family(_F)
c_code local build_ref_223
	ret_reg &ref[223]
	call_c   build_ref_708()
	call_c   Dyam_Create_Unary(&ref[708],V(5))
	move_ret ref[223]
	c_ret

long local pool_fun99[4]=[3,build_ref_221,build_ref_223,build_ref_224]

pl_code local fun99
	call_c   Dyam_Pool(pool_fun99)
	call_c   Dyam_Unify_Item(&ref[221])
	fail_ret
	call_c   DYAM_evpred_retract(&ref[223])
	fail_ret
	call_c   DYAM_evpred_assert_1(&ref[224])
	pl_ret

;; TERM 222: '*GUARD*'(directive(tag_family(_B), _C, _D, _E)) :> []
c_code local build_ref_222
	ret_reg &ref[222]
	call_c   build_ref_221()
	call_c   Dyam_Create_Binary(I(9),&ref[221],I(0))
	move_ret ref[222]
	c_ret

c_code local build_seed_93
	ret_reg &seed[93]
	call_c   build_ref_11()
	call_c   build_ref_227()
	call_c   Dyam_Seed_Start(&ref[11],&ref[227],I(0),fun0,1)
	call_c   build_ref_226()
	call_c   Dyam_Seed_Add_Comp(&ref[226],fun100,0)
	call_c   Dyam_Seed_End()
	move_ret seed[93]
	c_ret

;; TERM 226: '*GUARD*'(directive(parse_mode(_B), _C, _D, _E))
c_code local build_ref_226
	ret_reg &ref[226]
	call_c   build_ref_12()
	call_c   build_ref_225()
	call_c   Dyam_Create_Unary(&ref[12],&ref[225])
	move_ret ref[226]
	c_ret

;; TERM 225: directive(parse_mode(_B), _C, _D, _E)
c_code local build_ref_225
	ret_reg &ref[225]
	call_c   build_ref_682()
	call_c   build_ref_229()
	call_c   Dyam_Term_Start(&ref[682],4)
	call_c   Dyam_Term_Arg(&ref[229])
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[225]
	c_ret

;; TERM 229: parse_mode(_B)
c_code local build_ref_229
	ret_reg &ref[229]
	call_c   build_ref_709()
	call_c   Dyam_Create_Unary(&ref[709],V(1))
	move_ret ref[229]
	c_ret

;; TERM 709: parse_mode
c_code local build_ref_709
	ret_reg &ref[709]
	call_c   Dyam_Create_Atom("parse_mode")
	move_ret ref[709]
	c_ret

;; TERM 228: parse_mode(_F)
c_code local build_ref_228
	ret_reg &ref[228]
	call_c   build_ref_709()
	call_c   Dyam_Create_Unary(&ref[709],V(5))
	move_ret ref[228]
	c_ret

long local pool_fun100[4]=[3,build_ref_226,build_ref_228,build_ref_229]

pl_code local fun100
	call_c   Dyam_Pool(pool_fun100)
	call_c   Dyam_Unify_Item(&ref[226])
	fail_ret
	call_c   DYAM_evpred_retract(&ref[228])
	fail_ret
	call_c   DYAM_evpred_assert_1(&ref[229])
	pl_ret

;; TERM 227: '*GUARD*'(directive(parse_mode(_B), _C, _D, _E)) :> []
c_code local build_ref_227
	ret_reg &ref[227]
	call_c   build_ref_226()
	call_c   Dyam_Create_Binary(I(9),&ref[226],I(0))
	move_ret ref[227]
	c_ret

c_code local build_seed_45
	ret_reg &seed[45]
	call_c   build_ref_11()
	call_c   build_ref_129()
	call_c   Dyam_Seed_Start(&ref[11],&ref[129],I(0),fun0,1)
	call_c   build_ref_128()
	call_c   Dyam_Seed_Add_Comp(&ref[128],fun138,0)
	call_c   Dyam_Seed_End()
	move_ret seed[45]
	c_ret

c_code local build_seed_117
	ret_reg &seed[117]
	call_c   build_ref_12()
	call_c   build_ref_218()
	call_c   Dyam_Seed_Start(&ref[12],&ref[218],I(0),fun13,1)
	call_c   build_ref_219()
	call_c   Dyam_Seed_Add_Comp(&ref[219],&ref[218],0)
	call_c   Dyam_Seed_End()
	move_ret seed[117]
	c_ret

;; TERM 219: '*GUARD*'(installer(_B, (_G / _H), _D)) :> '$$HOLE$$'
c_code local build_ref_219
	ret_reg &ref[219]
	call_c   build_ref_218()
	call_c   Dyam_Create_Binary(I(9),&ref[218],I(7))
	move_ret ref[219]
	c_ret

;; TERM 218: '*GUARD*'(installer(_B, (_G / _H), _D))
c_code local build_ref_218
	ret_reg &ref[218]
	call_c   build_ref_12()
	call_c   build_ref_217()
	call_c   Dyam_Create_Unary(&ref[12],&ref[217])
	move_ret ref[218]
	c_ret

;; TERM 217: installer(_B, (_G / _H), _D)
c_code local build_ref_217
	ret_reg &ref[217]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_692()
	call_c   Dyam_Create_Binary(&ref[692],V(6),V(7))
	move_ret R(0)
	call_c   build_ref_681()
	call_c   Dyam_Term_Start(&ref[681],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[217]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun134[2]=[1,build_seed_117]

pl_code local fun135
	call_c   Dyam_Remove_Choice()
fun134:
	call_c   DYAM_evpred_functor(V(2),V(6),V(7))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_jump  fun25(&seed[117],1)


;; TERM 216: _E / _F
c_code local build_ref_216
	ret_reg &ref[216]
	call_c   build_ref_692()
	call_c   Dyam_Create_Binary(&ref[692],V(4),V(5))
	move_ret ref[216]
	c_ret

long local pool_fun136[3]=[65537,build_ref_216,pool_fun134]

pl_code local fun137
	call_c   Dyam_Remove_Choice()
fun136:
	call_c   Dyam_Choice(fun135)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(2),&ref[216])
	fail_ret
	call_c   Dyam_Cut()
	pl_fail


long local pool_fun138[3]=[65537,build_ref_128,pool_fun136]

pl_code local fun138
	call_c   Dyam_Pool(pool_fun138)
	call_c   Dyam_Unify_Item(&ref[128])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun137)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_atom(V(2))
	fail_ret
	call_c   Dyam_Cut()
	pl_fail

c_code local build_seed_65
	ret_reg &seed[65]
	call_c   build_ref_11()
	call_c   build_ref_238()
	call_c   Dyam_Seed_Start(&ref[11],&ref[238],I(0),fun0,1)
	call_c   build_ref_237()
	call_c   Dyam_Seed_Add_Comp(&ref[237],fun105,0)
	call_c   Dyam_Seed_End()
	move_ret seed[65]
	c_ret

;; TERM 237: '*GUARD*'(directive(tag_corrections, _B, _C, _D))
c_code local build_ref_237
	ret_reg &ref[237]
	call_c   build_ref_12()
	call_c   build_ref_236()
	call_c   Dyam_Create_Unary(&ref[12],&ref[236])
	move_ret ref[237]
	c_ret

;; TERM 236: directive(tag_corrections, _B, _C, _D)
c_code local build_ref_236
	ret_reg &ref[236]
	call_c   build_ref_682()
	call_c   build_ref_239()
	call_c   Dyam_Term_Start(&ref[682],4)
	call_c   Dyam_Term_Arg(&ref[239])
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[236]
	c_ret

;; TERM 239: tag_corrections
c_code local build_ref_239
	ret_reg &ref[239]
	call_c   Dyam_Create_Atom("tag_corrections")
	move_ret ref[239]
	c_ret

pl_code local fun104
	call_c   Dyam_Remove_Choice()
fun103:
	call_c   DYAM_evpred_assert_1(&ref[239])
	call_c   Dyam_Deallocate()
	pl_ret


long local pool_fun105[4]=[3,build_ref_237,build_ref_239,build_ref_239]

pl_code local fun105
	call_c   Dyam_Pool(pool_fun105)
	call_c   Dyam_Unify_Item(&ref[237])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun104)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[239])
	call_c   Dyam_Cut()
	pl_fail

;; TERM 238: '*GUARD*'(directive(tag_corrections, _B, _C, _D)) :> []
c_code local build_ref_238
	ret_reg &ref[238]
	call_c   build_ref_237()
	call_c   Dyam_Create_Binary(I(9),&ref[237],I(0))
	move_ret ref[238]
	c_ret

c_code local build_seed_95
	ret_reg &seed[95]
	call_c   build_ref_11()
	call_c   build_ref_242()
	call_c   Dyam_Seed_Start(&ref[11],&ref[242],I(0),fun0,1)
	call_c   build_ref_241()
	call_c   Dyam_Seed_Add_Comp(&ref[241],fun106,0)
	call_c   Dyam_Seed_End()
	move_ret seed[95]
	c_ret

;; TERM 241: '*GUARD*'(directive((include _B), _C, _D, _E))
c_code local build_ref_241
	ret_reg &ref[241]
	call_c   build_ref_12()
	call_c   build_ref_240()
	call_c   Dyam_Create_Unary(&ref[12],&ref[240])
	move_ret ref[241]
	c_ret

;; TERM 240: directive((include _B), _C, _D, _E)
c_code local build_ref_240
	ret_reg &ref[240]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_198()
	call_c   Dyam_Create_Unary(&ref[198],V(1))
	move_ret R(0)
	call_c   build_ref_682()
	call_c   Dyam_Term_Start(&ref[682],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[240]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 198: include
c_code local build_ref_198
	ret_reg &ref[198]
	call_c   Dyam_Create_Atom("include")
	move_ret ref[198]
	c_ret

;; TERM 243: include(_C, _D)
c_code local build_ref_243
	ret_reg &ref[243]
	call_c   build_ref_198()
	call_c   Dyam_Create_Binary(&ref[198],V(2),V(3))
	move_ret ref[243]
	c_ret

long local pool_fun106[3]=[2,build_ref_241,build_ref_243]

pl_code local fun106
	call_c   Dyam_Pool(pool_fun106)
	call_c   Dyam_Unify_Item(&ref[241])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(1))
	move     &ref[243], R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_read_files_2()

;; TERM 242: '*GUARD*'(directive((include _B), _C, _D, _E)) :> []
c_code local build_ref_242
	ret_reg &ref[242]
	call_c   build_ref_241()
	call_c   Dyam_Create_Binary(I(9),&ref[241],I(0))
	move_ret ref[242]
	c_ret

c_code local build_seed_41
	ret_reg &seed[41]
	call_c   build_ref_11()
	call_c   build_ref_246()
	call_c   Dyam_Seed_Start(&ref[11],&ref[246],I(0),fun0,1)
	call_c   build_ref_245()
	call_c   Dyam_Seed_Add_Comp(&ref[245],fun107,0)
	call_c   Dyam_Seed_End()
	move_ret seed[41]
	c_ret

;; TERM 245: '*GUARD*'(installer(op(_B, _C), (_D / _E), _F))
c_code local build_ref_245
	ret_reg &ref[245]
	call_c   build_ref_12()
	call_c   build_ref_244()
	call_c   Dyam_Create_Unary(&ref[12],&ref[244])
	move_ret ref[245]
	c_ret

;; TERM 244: installer(op(_B, _C), (_D / _E), _F)
c_code local build_ref_244
	ret_reg &ref[244]
	call_c   build_ref_681()
	call_c   build_ref_329()
	call_c   build_ref_301()
	call_c   Dyam_Term_Start(&ref[681],3)
	call_c   Dyam_Term_Arg(&ref[329])
	call_c   Dyam_Term_Arg(&ref[301])
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[244]
	c_ret

;; TERM 301: _D / _E
c_code local build_ref_301
	ret_reg &ref[301]
	call_c   build_ref_692()
	call_c   Dyam_Create_Binary(&ref[692],V(3),V(4))
	move_ret ref[301]
	c_ret

;; TERM 329: op(_B, _C)
c_code local build_ref_329
	ret_reg &ref[329]
	call_c   build_ref_710()
	call_c   Dyam_Create_Binary(&ref[710],V(1),V(2))
	move_ret ref[329]
	c_ret

;; TERM 710: op
c_code local build_ref_710
	ret_reg &ref[710]
	call_c   Dyam_Create_Atom("op")
	move_ret ref[710]
	c_ret

pl_code local fun107
	call_c   build_ref_245()
	call_c   Dyam_Unify_Item(&ref[245])
	fail_ret
	call_c   DYAM_Op_3(V(1),V(2),V(3))
	fail_ret
	pl_ret

;; TERM 246: '*GUARD*'(installer(op(_B, _C), (_D / _E), _F)) :> []
c_code local build_ref_246
	ret_reg &ref[246]
	call_c   build_ref_245()
	call_c   Dyam_Create_Binary(I(9),&ref[245],I(0))
	move_ret ref[246]
	c_ret

c_code local build_seed_59
	ret_reg &seed[59]
	call_c   build_ref_11()
	call_c   build_ref_249()
	call_c   Dyam_Seed_Start(&ref[11],&ref[249],I(0),fun0,1)
	call_c   build_ref_248()
	call_c   Dyam_Seed_Add_Comp(&ref[248],fun108,0)
	call_c   Dyam_Seed_End()
	move_ret seed[59]
	c_ret

;; TERM 248: '*GUARD*'(directive(bmg_island(_B, _C), _D, _E, _F))
c_code local build_ref_248
	ret_reg &ref[248]
	call_c   build_ref_12()
	call_c   build_ref_247()
	call_c   Dyam_Create_Unary(&ref[12],&ref[247])
	move_ret ref[248]
	c_ret

;; TERM 247: directive(bmg_island(_B, _C), _D, _E, _F)
c_code local build_ref_247
	ret_reg &ref[247]
	call_c   build_ref_682()
	call_c   build_ref_122()
	call_c   Dyam_Term_Start(&ref[682],4)
	call_c   Dyam_Term_Arg(&ref[122])
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[247]
	c_ret

pl_code local fun108
	call_c   build_ref_248()
	call_c   Dyam_Unify_Item(&ref[248])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(2))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_new_bmg_island_2()

;; TERM 249: '*GUARD*'(directive(bmg_island(_B, _C), _D, _E, _F)) :> []
c_code local build_ref_249
	ret_reg &ref[249]
	call_c   build_ref_248()
	call_c   Dyam_Create_Binary(I(9),&ref[248],I(0))
	move_ret ref[249]
	c_ret

c_code local build_seed_58
	ret_reg &seed[58]
	call_c   build_ref_11()
	call_c   build_ref_252()
	call_c   Dyam_Seed_Start(&ref[11],&ref[252],I(0),fun0,1)
	call_c   build_ref_251()
	call_c   Dyam_Seed_Add_Comp(&ref[251],fun111,0)
	call_c   Dyam_Seed_End()
	move_ret seed[58]
	c_ret

;; TERM 251: '*GUARD*'(directive(bmg_pushable(_B, _C), _D, _E, _F))
c_code local build_ref_251
	ret_reg &ref[251]
	call_c   build_ref_12()
	call_c   build_ref_250()
	call_c   Dyam_Create_Unary(&ref[12],&ref[250])
	move_ret ref[251]
	c_ret

;; TERM 250: directive(bmg_pushable(_B, _C), _D, _E, _F)
c_code local build_ref_250
	ret_reg &ref[250]
	call_c   build_ref_682()
	call_c   build_ref_126()
	call_c   Dyam_Term_Start(&ref[682],4)
	call_c   Dyam_Term_Arg(&ref[126])
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[250]
	c_ret

;; TERM 253: bmg_pushable(_C)
c_code local build_ref_253
	ret_reg &ref[253]
	call_c   build_ref_695()
	call_c   Dyam_Create_Unary(&ref[695],V(2))
	move_ret ref[253]
	c_ret

long local pool_fun111[3]=[2,build_ref_251,build_ref_253]

pl_code local fun111
	call_c   Dyam_Pool(pool_fun111)
	call_c   Dyam_Unify_Item(&ref[251])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[253], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(1))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_install_2()

;; TERM 252: '*GUARD*'(directive(bmg_pushable(_B, _C), _D, _E, _F)) :> []
c_code local build_ref_252
	ret_reg &ref[252]
	call_c   build_ref_251()
	call_c   Dyam_Create_Binary(I(9),&ref[251],I(0))
	move_ret ref[252]
	c_ret

c_code local build_seed_87
	ret_reg &seed[87]
	call_c   build_ref_11()
	call_c   build_ref_256()
	call_c   Dyam_Seed_Start(&ref[11],&ref[256],I(0),fun0,1)
	call_c   build_ref_255()
	call_c   Dyam_Seed_Add_Comp(&ref[255],fun112,0)
	call_c   Dyam_Seed_End()
	move_ret seed[87]
	c_ret

;; TERM 255: '*GUARD*'(directive(subset(_B, _C), _D, _E, _F))
c_code local build_ref_255
	ret_reg &ref[255]
	call_c   build_ref_12()
	call_c   build_ref_254()
	call_c   Dyam_Create_Unary(&ref[12],&ref[254])
	move_ret ref[255]
	c_ret

;; TERM 254: directive(subset(_B, _C), _D, _E, _F)
c_code local build_ref_254
	ret_reg &ref[254]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_711()
	call_c   Dyam_Create_Binary(&ref[711],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_682()
	call_c   Dyam_Term_Start(&ref[682],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[254]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 711: subset
c_code local build_ref_711
	ret_reg &ref[711]
	call_c   Dyam_Create_Atom("subset")
	move_ret ref[711]
	c_ret

;; TERM 257: subset(_C)
c_code local build_ref_257
	ret_reg &ref[257]
	call_c   build_ref_711()
	call_c   Dyam_Create_Unary(&ref[711],V(2))
	move_ret ref[257]
	c_ret

long local pool_fun112[3]=[2,build_ref_255,build_ref_257]

pl_code local fun112
	call_c   Dyam_Pool(pool_fun112)
	call_c   Dyam_Unify_Item(&ref[255])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[257], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(1))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_install_2()

;; TERM 256: '*GUARD*'(directive(subset(_B, _C), _D, _E, _F)) :> []
c_code local build_ref_256
	ret_reg &ref[256]
	call_c   build_ref_255()
	call_c   Dyam_Create_Binary(I(9),&ref[255],I(0))
	move_ret ref[256]
	c_ret

c_code local build_seed_89
	ret_reg &seed[89]
	call_c   build_ref_11()
	call_c   build_ref_260()
	call_c   Dyam_Seed_Start(&ref[11],&ref[260],I(0),fun0,1)
	call_c   build_ref_259()
	call_c   Dyam_Seed_Add_Comp(&ref[259],fun113,0)
	call_c   Dyam_Seed_End()
	move_ret seed[89]
	c_ret

;; TERM 259: '*GUARD*'(directive(feature_intro(_B, _C), _D, _E, _F))
c_code local build_ref_259
	ret_reg &ref[259]
	call_c   build_ref_12()
	call_c   build_ref_258()
	call_c   Dyam_Create_Unary(&ref[12],&ref[258])
	move_ret ref[259]
	c_ret

;; TERM 258: directive(feature_intro(_B, _C), _D, _E, _F)
c_code local build_ref_258
	ret_reg &ref[258]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_712()
	call_c   Dyam_Create_Binary(&ref[712],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_682()
	call_c   Dyam_Term_Start(&ref[682],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[258]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 712: feature_intro
c_code local build_ref_712
	ret_reg &ref[712]
	call_c   Dyam_Create_Atom("feature_intro")
	move_ret ref[712]
	c_ret

;; TERM 261: feature_intro(_C)
c_code local build_ref_261
	ret_reg &ref[261]
	call_c   build_ref_712()
	call_c   Dyam_Create_Unary(&ref[712],V(2))
	move_ret ref[261]
	c_ret

long local pool_fun113[3]=[2,build_ref_259,build_ref_261]

pl_code local fun113
	call_c   Dyam_Pool(pool_fun113)
	call_c   Dyam_Unify_Item(&ref[259])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[261], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(1))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_install_2()

;; TERM 260: '*GUARD*'(directive(feature_intro(_B, _C), _D, _E, _F)) :> []
c_code local build_ref_260
	ret_reg &ref[260]
	call_c   build_ref_259()
	call_c   Dyam_Create_Binary(I(9),&ref[259],I(0))
	move_ret ref[260]
	c_ret

c_code local build_seed_90
	ret_reg &seed[90]
	call_c   build_ref_11()
	call_c   build_ref_264()
	call_c   Dyam_Seed_Start(&ref[11],&ref[264],I(0),fun0,1)
	call_c   build_ref_263()
	call_c   Dyam_Seed_Add_Comp(&ref[263],fun114,0)
	call_c   Dyam_Seed_End()
	move_ret seed[90]
	c_ret

;; TERM 263: '*GUARD*'(directive(features(_B, _C), _D, _E, _F))
c_code local build_ref_263
	ret_reg &ref[263]
	call_c   build_ref_12()
	call_c   build_ref_262()
	call_c   Dyam_Create_Unary(&ref[12],&ref[262])
	move_ret ref[263]
	c_ret

;; TERM 262: directive(features(_B, _C), _D, _E, _F)
c_code local build_ref_262
	ret_reg &ref[262]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_713()
	call_c   Dyam_Create_Binary(&ref[713],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_682()
	call_c   Dyam_Term_Start(&ref[682],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[262]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 713: features
c_code local build_ref_713
	ret_reg &ref[713]
	call_c   Dyam_Create_Atom("features")
	move_ret ref[713]
	c_ret

;; TERM 265: features(_C)
c_code local build_ref_265
	ret_reg &ref[265]
	call_c   build_ref_713()
	call_c   Dyam_Create_Unary(&ref[713],V(2))
	move_ret ref[265]
	c_ret

long local pool_fun114[3]=[2,build_ref_263,build_ref_265]

pl_code local fun114
	call_c   Dyam_Pool(pool_fun114)
	call_c   Dyam_Unify_Item(&ref[263])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[265], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(1))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_install_2()

;; TERM 264: '*GUARD*'(directive(features(_B, _C), _D, _E, _F)) :> []
c_code local build_ref_264
	ret_reg &ref[264]
	call_c   build_ref_263()
	call_c   Dyam_Create_Binary(I(9),&ref[263],I(0))
	move_ret ref[264]
	c_ret

c_code local build_seed_9
	ret_reg &seed[9]
	call_c   build_ref_11()
	call_c   build_ref_150()
	call_c   Dyam_Seed_Start(&ref[11],&ref[150],I(0),fun0,1)
	call_c   build_ref_149()
	call_c   Dyam_Seed_Add_Comp(&ref[149],fun55,0)
	call_c   Dyam_Seed_End()
	move_ret seed[9]
	c_ret

;; TERM 149: '*GUARD*'(clause_type(tag_lemma(_B, _C, _D), tag_lemma(_B, _C, _D), _E, _F))
c_code local build_ref_149
	ret_reg &ref[149]
	call_c   build_ref_12()
	call_c   build_ref_148()
	call_c   Dyam_Create_Unary(&ref[12],&ref[148])
	move_ret ref[149]
	c_ret

;; TERM 148: clause_type(tag_lemma(_B, _C, _D), tag_lemma(_B, _C, _D), _E, _F)
c_code local build_ref_148
	ret_reg &ref[148]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_714()
	call_c   Dyam_Term_Start(&ref[714],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_677()
	call_c   Dyam_Term_Start(&ref[677],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[148]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 714: tag_lemma
c_code local build_ref_714
	ret_reg &ref[714]
	call_c   Dyam_Create_Atom("tag_lemma")
	move_ret ref[714]
	c_ret

pl_code local fun55
	call_c   build_ref_149()
	call_c   Dyam_Unify_Item(&ref[149])
	fail_ret
	pl_ret

;; TERM 150: '*GUARD*'(clause_type(tag_lemma(_B, _C, _D), tag_lemma(_B, _C, _D), _E, _F)) :> []
c_code local build_ref_150
	ret_reg &ref[150]
	call_c   build_ref_149()
	call_c   Dyam_Create_Binary(I(9),&ref[149],I(0))
	move_ret ref[150]
	c_ret

c_code local build_seed_10
	ret_reg &seed[10]
	call_c   build_ref_11()
	call_c   build_ref_153()
	call_c   Dyam_Seed_Start(&ref[11],&ref[153],I(0),fun0,1)
	call_c   build_ref_152()
	call_c   Dyam_Seed_Add_Comp(&ref[152],fun56,0)
	call_c   Dyam_Seed_End()
	move_ret seed[10]
	c_ret

;; TERM 152: '*GUARD*'(clause_type(tag_tree{family=> _B, name=> _C, tree=> _D}, tag_tree{family=> _B, name=> _C, tree=> _D}, _E, _F))
c_code local build_ref_152
	ret_reg &ref[152]
	call_c   build_ref_12()
	call_c   build_ref_151()
	call_c   Dyam_Create_Unary(&ref[12],&ref[151])
	move_ret ref[152]
	c_ret

;; TERM 151: clause_type(tag_tree{family=> _B, name=> _C, tree=> _D}, tag_tree{family=> _B, name=> _C, tree=> _D}, _E, _F)
c_code local build_ref_151
	ret_reg &ref[151]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_698()
	call_c   Dyam_Term_Start(&ref[698],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_677()
	call_c   Dyam_Term_Start(&ref[677],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[151]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

pl_code local fun56
	call_c   build_ref_152()
	call_c   Dyam_Unify_Item(&ref[152])
	fail_ret
	pl_ret

;; TERM 153: '*GUARD*'(clause_type(tag_tree{family=> _B, name=> _C, tree=> _D}, tag_tree{family=> _B, name=> _C, tree=> _D}, _E, _F)) :> []
c_code local build_ref_153
	ret_reg &ref[153]
	call_c   build_ref_152()
	call_c   Dyam_Create_Binary(I(9),&ref[152],I(0))
	move_ret ref[153]
	c_ret

c_code local build_seed_102
	ret_reg &seed[102]
	call_c   build_ref_11()
	call_c   build_ref_232()
	call_c   Dyam_Seed_Start(&ref[11],&ref[232],I(0),fun0,1)
	call_c   build_ref_231()
	call_c   Dyam_Seed_Add_Comp(&ref[231],fun102,0)
	call_c   Dyam_Seed_End()
	move_ret seed[102]
	c_ret

;; TERM 231: '*GUARD*'(clause_type((_B :- _C), (_B :- _C), _D, _E))
c_code local build_ref_231
	ret_reg &ref[231]
	call_c   build_ref_12()
	call_c   build_ref_230()
	call_c   Dyam_Create_Unary(&ref[12],&ref[230])
	move_ret ref[231]
	c_ret

;; TERM 230: clause_type((_B :- _C), (_B :- _C), _D, _E)
c_code local build_ref_230
	ret_reg &ref[230]
	call_c   build_ref_677()
	call_c   build_ref_604()
	call_c   Dyam_Term_Start(&ref[677],4)
	call_c   Dyam_Term_Arg(&ref[604])
	call_c   Dyam_Term_Arg(&ref[604])
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[230]
	c_ret

;; TERM 604: _B :- _C
c_code local build_ref_604
	ret_reg &ref[604]
	call_c   build_ref_715()
	call_c   Dyam_Create_Binary(&ref[715],V(1),V(2))
	move_ret ref[604]
	c_ret

;; TERM 715: :-
c_code local build_ref_715
	ret_reg &ref[715]
	call_c   Dyam_Create_Atom(":-")
	move_ret ref[715]
	c_ret

c_code local build_seed_118
	ret_reg &seed[118]
	call_c   build_ref_12()
	call_c   build_ref_234()
	call_c   Dyam_Seed_Start(&ref[12],&ref[234],I(0),fun13,1)
	call_c   build_ref_235()
	call_c   Dyam_Seed_Add_Comp(&ref[235],&ref[234],0)
	call_c   Dyam_Seed_End()
	move_ret seed[118]
	c_ret

;; TERM 235: '*GUARD*'(register_predicate(clause, _B, _E)) :> '$$HOLE$$'
c_code local build_ref_235
	ret_reg &ref[235]
	call_c   build_ref_234()
	call_c   Dyam_Create_Binary(I(9),&ref[234],I(7))
	move_ret ref[235]
	c_ret

;; TERM 234: '*GUARD*'(register_predicate(clause, _B, _E))
c_code local build_ref_234
	ret_reg &ref[234]
	call_c   build_ref_12()
	call_c   build_ref_233()
	call_c   Dyam_Create_Unary(&ref[12],&ref[233])
	move_ret ref[234]
	c_ret

;; TERM 233: register_predicate(clause, _B, _E)
c_code local build_ref_233
	ret_reg &ref[233]
	call_c   build_ref_716()
	call_c   build_ref_717()
	call_c   Dyam_Term_Start(&ref[716],3)
	call_c   Dyam_Term_Arg(&ref[717])
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[233]
	c_ret

;; TERM 717: clause
c_code local build_ref_717
	ret_reg &ref[717]
	call_c   Dyam_Create_Atom("clause")
	move_ret ref[717]
	c_ret

;; TERM 716: register_predicate
c_code local build_ref_716
	ret_reg &ref[716]
	call_c   Dyam_Create_Atom("register_predicate")
	move_ret ref[716]
	c_ret

long local pool_fun102[3]=[2,build_ref_231,build_seed_118]

pl_code local fun102
	call_c   Dyam_Pool(pool_fun102)
	call_c   Dyam_Unify_Item(&ref[231])
	fail_ret
	pl_jump  fun25(&seed[118],1)

;; TERM 232: '*GUARD*'(clause_type((_B :- _C), (_B :- _C), _D, _E)) :> []
c_code local build_ref_232
	ret_reg &ref[232]
	call_c   build_ref_231()
	call_c   Dyam_Create_Binary(I(9),&ref[231],I(0))
	move_ret ref[232]
	c_ret

c_code local build_seed_28
	ret_reg &seed[28]
	call_c   build_ref_11()
	call_c   build_ref_268()
	call_c   Dyam_Seed_Start(&ref[11],&ref[268],I(0),fun0,1)
	call_c   build_ref_267()
	call_c   Dyam_Seed_Add_Comp(&ref[267],fun115,0)
	call_c   Dyam_Seed_End()
	move_ret seed[28]
	c_ret

;; TERM 267: '*GUARD*'(installer(deref_term, _B, _C))
c_code local build_ref_267
	ret_reg &ref[267]
	call_c   build_ref_12()
	call_c   build_ref_266()
	call_c   Dyam_Create_Unary(&ref[12],&ref[266])
	move_ret ref[267]
	c_ret

;; TERM 266: installer(deref_term, _B, _C)
c_code local build_ref_266
	ret_reg &ref[266]
	call_c   build_ref_681()
	call_c   build_ref_136()
	call_c   Dyam_Term_Start(&ref[681],3)
	call_c   Dyam_Term_Arg(&ref[136])
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_End()
	move_ret ref[266]
	c_ret

;; TERM 269: 'Set Deref ~w\n'
c_code local build_ref_269
	ret_reg &ref[269]
	call_c   Dyam_Create_Atom("Set Deref ~w\n")
	move_ret ref[269]
	c_ret

;; TERM 110: [_B]
c_code local build_ref_110
	ret_reg &ref[110]
	call_c   Dyam_Create_List(V(1),I(0))
	move_ret ref[110]
	c_ret

;; TERM 270: deref_functor(_B)
c_code local build_ref_270
	ret_reg &ref[270]
	call_c   build_ref_718()
	call_c   Dyam_Create_Unary(&ref[718],V(1))
	move_ret ref[270]
	c_ret

;; TERM 718: deref_functor
c_code local build_ref_718
	ret_reg &ref[718]
	call_c   Dyam_Create_Atom("deref_functor")
	move_ret ref[718]
	c_ret

long local pool_fun115[5]=[4,build_ref_267,build_ref_269,build_ref_110,build_ref_270]

pl_code local fun115
	call_c   Dyam_Pool(pool_fun115)
	call_c   Dyam_Unify_Item(&ref[267])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[269], R(0)
	move     0, R(1)
	move     &ref[110], R(2)
	move     S(5), R(3)
	pl_call  pred_format_2()
	call_c   DYAM_evpred_assert_1(&ref[270])
	call_c   DYAM_DerefTermSet_1(V(1))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

;; TERM 268: '*GUARD*'(installer(deref_term, _B, _C)) :> []
c_code local build_ref_268
	ret_reg &ref[268]
	call_c   build_ref_267()
	call_c   Dyam_Create_Binary(I(9),&ref[267],I(0))
	move_ret ref[268]
	c_ret

c_code local build_seed_29
	ret_reg &seed[29]
	call_c   build_ref_11()
	call_c   build_ref_273()
	call_c   Dyam_Seed_Start(&ref[11],&ref[273],I(0),fun0,1)
	call_c   build_ref_272()
	call_c   Dyam_Seed_Add_Comp(&ref[272],fun116,0)
	call_c   Dyam_Seed_End()
	move_ret seed[29]
	c_ret

;; TERM 272: '*GUARD*'(tag_anchor_collect_eqvars([_B ^ _C|_D], _E))
c_code local build_ref_272
	ret_reg &ref[272]
	call_c   build_ref_12()
	call_c   build_ref_271()
	call_c   Dyam_Create_Unary(&ref[12],&ref[271])
	move_ret ref[272]
	c_ret

;; TERM 271: tag_anchor_collect_eqvars([_B ^ _C|_D], _E)
c_code local build_ref_271
	ret_reg &ref[271]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_719()
	call_c   Dyam_Create_Binary(&ref[719],V(1),V(2))
	move_ret R(0)
	call_c   Dyam_Create_List(R(0),V(3))
	move_ret R(0)
	call_c   build_ref_674()
	call_c   Dyam_Create_Binary(&ref[674],R(0),V(4))
	move_ret ref[271]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 719: ^
c_code local build_ref_719
	ret_reg &ref[719]
	call_c   Dyam_Create_Atom("^")
	move_ret ref[719]
	c_ret

c_code local build_seed_119
	ret_reg &seed[119]
	call_c   build_ref_12()
	call_c   build_ref_275()
	call_c   Dyam_Seed_Start(&ref[12],&ref[275],I(0),fun13,1)
	call_c   build_ref_276()
	call_c   Dyam_Seed_Add_Comp(&ref[276],&ref[275],0)
	call_c   Dyam_Seed_End()
	move_ret seed[119]
	c_ret

;; TERM 276: '*GUARD*'(tag_anchor_collect_eqvars(_D, _F)) :> '$$HOLE$$'
c_code local build_ref_276
	ret_reg &ref[276]
	call_c   build_ref_275()
	call_c   Dyam_Create_Binary(I(9),&ref[275],I(7))
	move_ret ref[276]
	c_ret

;; TERM 275: '*GUARD*'(tag_anchor_collect_eqvars(_D, _F))
c_code local build_ref_275
	ret_reg &ref[275]
	call_c   build_ref_12()
	call_c   build_ref_274()
	call_c   Dyam_Create_Unary(&ref[12],&ref[274])
	move_ret ref[275]
	c_ret

;; TERM 274: tag_anchor_collect_eqvars(_D, _F)
c_code local build_ref_274
	ret_reg &ref[274]
	call_c   build_ref_674()
	call_c   Dyam_Create_Binary(&ref[674],V(3),V(5))
	move_ret ref[274]
	c_ret

c_code local build_seed_120
	ret_reg &seed[120]
	call_c   build_ref_12()
	call_c   build_ref_278()
	call_c   Dyam_Seed_Start(&ref[12],&ref[278],I(0),fun13,1)
	call_c   build_ref_279()
	call_c   Dyam_Seed_Add_Comp(&ref[279],&ref[278],0)
	call_c   Dyam_Seed_End()
	move_ret seed[120]
	c_ret

;; TERM 279: '*GUARD*'(append(_B, _F, _E)) :> '$$HOLE$$'
c_code local build_ref_279
	ret_reg &ref[279]
	call_c   build_ref_278()
	call_c   Dyam_Create_Binary(I(9),&ref[278],I(7))
	move_ret ref[279]
	c_ret

;; TERM 278: '*GUARD*'(append(_B, _F, _E))
c_code local build_ref_278
	ret_reg &ref[278]
	call_c   build_ref_12()
	call_c   build_ref_277()
	call_c   Dyam_Create_Unary(&ref[12],&ref[277])
	move_ret ref[278]
	c_ret

;; TERM 277: append(_B, _F, _E)
c_code local build_ref_277
	ret_reg &ref[277]
	call_c   build_ref_720()
	call_c   Dyam_Term_Start(&ref[720],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[277]
	c_ret

;; TERM 720: append
c_code local build_ref_720
	ret_reg &ref[720]
	call_c   Dyam_Create_Atom("append")
	move_ret ref[720]
	c_ret

long local pool_fun116[4]=[3,build_ref_272,build_seed_119,build_seed_120]

pl_code local fun116
	call_c   Dyam_Pool(pool_fun116)
	call_c   Dyam_Unify_Item(&ref[272])
	fail_ret
	call_c   Dyam_Allocate(0)
	pl_call  fun25(&seed[119],1)
	call_c   Dyam_Deallocate()
	pl_jump  fun25(&seed[120],1)

;; TERM 273: '*GUARD*'(tag_anchor_collect_eqvars([_B ^ _C|_D], _E)) :> []
c_code local build_ref_273
	ret_reg &ref[273]
	call_c   build_ref_272()
	call_c   Dyam_Create_Binary(I(9),&ref[272],I(0))
	move_ret ref[273]
	c_ret

c_code local build_seed_36
	ret_reg &seed[36]
	call_c   build_ref_11()
	call_c   build_ref_282()
	call_c   Dyam_Seed_Start(&ref[11],&ref[282],I(0),fun0,1)
	call_c   build_ref_281()
	call_c   Dyam_Seed_Add_Comp(&ref[281],fun117,0)
	call_c   Dyam_Seed_End()
	move_ret seed[36]
	c_ret

;; TERM 281: '*GUARD*'(installer(hilog_install, (_B / _C), _D))
c_code local build_ref_281
	ret_reg &ref[281]
	call_c   build_ref_12()
	call_c   build_ref_280()
	call_c   Dyam_Create_Unary(&ref[12],&ref[280])
	move_ret ref[281]
	c_ret

;; TERM 280: installer(hilog_install, (_B / _C), _D)
c_code local build_ref_280
	ret_reg &ref[280]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_692()
	call_c   Dyam_Create_Binary(&ref[692],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_681()
	call_c   build_ref_140()
	call_c   Dyam_Term_Start(&ref[681],3)
	call_c   Dyam_Term_Arg(&ref[140])
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[280]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun117[3]=[2,build_ref_281,build_ref_283]

pl_code local fun117
	call_c   Dyam_Pool(pool_fun117)
	call_c   Dyam_Unify_Item(&ref[281])
	fail_ret
	call_c   DYAM_evpred_retract(&ref[283])
	fail_ret
	call_c   DYAM_evpred_assert_1(&ref[283])
	call_c   DYAM_Hilog_1(V(1))
	fail_ret
	pl_ret

;; TERM 282: '*GUARD*'(installer(hilog_install, (_B / _C), _D)) :> []
c_code local build_ref_282
	ret_reg &ref[282]
	call_c   build_ref_281()
	call_c   Dyam_Create_Binary(I(9),&ref[281],I(0))
	move_ret ref[282]
	c_ret

c_code local build_seed_43
	ret_reg &seed[43]
	call_c   build_ref_11()
	call_c   build_ref_286()
	call_c   Dyam_Seed_Start(&ref[11],&ref[286],I(0),fun0,1)
	call_c   build_ref_285()
	call_c   Dyam_Seed_Add_Comp(&ref[285],fun118,0)
	call_c   Dyam_Seed_End()
	move_ret seed[43]
	c_ret

;; TERM 285: '*GUARD*'(install_list(_B, [_C|_D], _E))
c_code local build_ref_285
	ret_reg &ref[285]
	call_c   build_ref_12()
	call_c   build_ref_284()
	call_c   Dyam_Create_Unary(&ref[12],&ref[284])
	move_ret ref[285]
	c_ret

;; TERM 284: install_list(_B, [_C|_D], _E)
c_code local build_ref_284
	ret_reg &ref[284]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(2),V(3))
	move_ret R(0)
	call_c   build_ref_675()
	call_c   Dyam_Term_Start(&ref[675],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[284]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_121
	ret_reg &seed[121]
	call_c   build_ref_12()
	call_c   build_ref_288()
	call_c   Dyam_Seed_Start(&ref[12],&ref[288],I(0),fun13,1)
	call_c   build_ref_289()
	call_c   Dyam_Seed_Add_Comp(&ref[289],&ref[288],0)
	call_c   Dyam_Seed_End()
	move_ret seed[121]
	c_ret

;; TERM 289: '*GUARD*'(install_elem(_B, _C, _E)) :> '$$HOLE$$'
c_code local build_ref_289
	ret_reg &ref[289]
	call_c   build_ref_288()
	call_c   Dyam_Create_Binary(I(9),&ref[288],I(7))
	move_ret ref[289]
	c_ret

;; TERM 288: '*GUARD*'(install_elem(_B, _C, _E))
c_code local build_ref_288
	ret_reg &ref[288]
	call_c   build_ref_12()
	call_c   build_ref_287()
	call_c   Dyam_Create_Unary(&ref[12],&ref[287])
	move_ret ref[288]
	c_ret

;; TERM 287: install_elem(_B, _C, _E)
c_code local build_ref_287
	ret_reg &ref[287]
	call_c   build_ref_679()
	call_c   Dyam_Term_Start(&ref[679],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[287]
	c_ret

c_code local build_seed_122
	ret_reg &seed[122]
	call_c   build_ref_12()
	call_c   build_ref_291()
	call_c   Dyam_Seed_Start(&ref[12],&ref[291],I(0),fun13,1)
	call_c   build_ref_292()
	call_c   Dyam_Seed_Add_Comp(&ref[292],&ref[291],0)
	call_c   Dyam_Seed_End()
	move_ret seed[122]
	c_ret

;; TERM 292: '*GUARD*'(install_list(_B, _D, _E)) :> '$$HOLE$$'
c_code local build_ref_292
	ret_reg &ref[292]
	call_c   build_ref_291()
	call_c   Dyam_Create_Binary(I(9),&ref[291],I(7))
	move_ret ref[292]
	c_ret

;; TERM 291: '*GUARD*'(install_list(_B, _D, _E))
c_code local build_ref_291
	ret_reg &ref[291]
	call_c   build_ref_12()
	call_c   build_ref_290()
	call_c   Dyam_Create_Unary(&ref[12],&ref[290])
	move_ret ref[291]
	c_ret

;; TERM 290: install_list(_B, _D, _E)
c_code local build_ref_290
	ret_reg &ref[290]
	call_c   build_ref_675()
	call_c   Dyam_Term_Start(&ref[675],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[290]
	c_ret

long local pool_fun118[4]=[3,build_ref_285,build_seed_121,build_seed_122]

pl_code local fun118
	call_c   Dyam_Pool(pool_fun118)
	call_c   Dyam_Unify_Item(&ref[285])
	fail_ret
	call_c   Dyam_Allocate(0)
	pl_call  fun25(&seed[121],1)
	call_c   Dyam_Deallocate()
	pl_jump  fun25(&seed[122],1)

;; TERM 286: '*GUARD*'(install_list(_B, [_C|_D], _E)) :> []
c_code local build_ref_286
	ret_reg &ref[286]
	call_c   build_ref_285()
	call_c   Dyam_Create_Binary(I(9),&ref[285],I(0))
	move_ret ref[286]
	c_ret

c_code local build_seed_19
	ret_reg &seed[19]
	call_c   build_ref_11()
	call_c   build_ref_295()
	call_c   Dyam_Seed_Start(&ref[11],&ref[295],I(0),fun0,1)
	call_c   build_ref_294()
	call_c   Dyam_Seed_Add_Comp(&ref[294],fun123,0)
	call_c   Dyam_Seed_End()
	move_ret seed[19]
	c_ret

;; TERM 294: '*GUARD*'(register_predicate(dcg, _B, _C))
c_code local build_ref_294
	ret_reg &ref[294]
	call_c   build_ref_12()
	call_c   build_ref_293()
	call_c   Dyam_Create_Unary(&ref[12],&ref[293])
	move_ret ref[294]
	c_ret

;; TERM 293: register_predicate(dcg, _B, _C)
c_code local build_ref_293
	ret_reg &ref[293]
	call_c   build_ref_716()
	call_c   build_ref_579()
	call_c   Dyam_Term_Start(&ref[716],3)
	call_c   Dyam_Term_Arg(&ref[579])
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_End()
	move_ret ref[293]
	c_ret

;; TERM 579: dcg
c_code local build_ref_579
	ret_reg &ref[579]
	call_c   Dyam_Create_Atom("dcg")
	move_ret ref[579]
	c_ret

;; TERM 299: dcg _I / _J
c_code local build_ref_299
	ret_reg &ref[299]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_692()
	call_c   Dyam_Create_Binary(&ref[692],V(8),V(9))
	move_ret R(0)
	call_c   build_ref_579()
	call_c   Dyam_Create_Unary(&ref[579],R(0))
	move_ret ref[299]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 300: predicate(_K, _C)
c_code local build_ref_300
	ret_reg &ref[300]
	call_c   build_ref_721()
	call_c   Dyam_Create_Binary(&ref[721],V(10),V(2))
	move_ret ref[300]
	c_ret

;; TERM 721: predicate
c_code local build_ref_721
	ret_reg &ref[721]
	call_c   Dyam_Create_Atom("predicate")
	move_ret ref[721]
	c_ret

long local pool_fun119[3]=[2,build_ref_299,build_ref_300]

pl_code local fun120
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(6),V(3))
	fail_ret
fun119:
	call_c   DYAM_evpred_functor(V(6),V(8),V(9))
	fail_ret
	move     &ref[299], R(0)
	move     S(5), R(1)
	move     V(10), R(2)
	move     S(5), R(3)
	move     V(11), R(4)
	move     S(5), R(5)
	move     V(12), R(6)
	move     S(5), R(7)
	pl_call  pred_term_current_module_shift_4()
	move     &ref[300], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Deallocate(1)
	pl_jump  pred_record_without_doublon_1()


;; TERM 297: [_F,_G,_H]
c_code local build_ref_297
	ret_reg &ref[297]
	call_c   Dyam_Create_Tupple(5,7,I(0))
	move_ret ref[297]
	c_ret

;; TERM 298: bmg_stack(_F)
c_code local build_ref_298
	ret_reg &ref[298]
	call_c   build_ref_358()
	call_c   Dyam_Create_Unary(&ref[358],V(5))
	move_ret ref[298]
	c_ret

;; TERM 358: bmg_stack
c_code local build_ref_358
	ret_reg &ref[358]
	call_c   Dyam_Create_Atom("bmg_stack")
	move_ret ref[358]
	c_ret

long local pool_fun121[5]=[131074,build_ref_297,build_ref_298,pool_fun119,pool_fun119]

pl_code local fun122
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(3),V(1))
	fail_ret
fun121:
	call_c   Dyam_Choice(fun120)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_univ(V(3),&ref[297])
	fail_ret
	pl_call  Object_1(&ref[298])
	call_c   Dyam_Cut()
	pl_jump  fun119()


;; TERM 296: _D , _E
c_code local build_ref_296
	ret_reg &ref[296]
	call_c   Dyam_Create_Binary(I(4),V(3),V(4))
	move_ret ref[296]
	c_ret

long local pool_fun123[5]=[131074,build_ref_294,build_ref_296,pool_fun121,pool_fun121]

pl_code local fun123
	call_c   Dyam_Pool(pool_fun123)
	call_c   Dyam_Unify_Item(&ref[294])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun122)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[296])
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun121()

;; TERM 295: '*GUARD*'(register_predicate(dcg, _B, _C)) :> []
c_code local build_ref_295
	ret_reg &ref[295]
	call_c   build_ref_294()
	call_c   Dyam_Create_Binary(I(9),&ref[294],I(0))
	move_ret ref[295]
	c_ret

c_code local build_seed_17
	ret_reg &seed[17]
	call_c   build_ref_11()
	call_c   build_ref_305()
	call_c   Dyam_Seed_Start(&ref[11],&ref[305],I(0),fun0,1)
	call_c   build_ref_304()
	call_c   Dyam_Seed_Add_Comp(&ref[304],fun126,0)
	call_c   Dyam_Seed_End()
	move_ret seed[17]
	c_ret

;; TERM 304: '*GUARD*'(installer(public(_B), _C, _D))
c_code local build_ref_304
	ret_reg &ref[304]
	call_c   build_ref_12()
	call_c   build_ref_303()
	call_c   Dyam_Create_Unary(&ref[12],&ref[303])
	move_ret ref[304]
	c_ret

;; TERM 303: installer(public(_B), _C, _D)
c_code local build_ref_303
	ret_reg &ref[303]
	call_c   build_ref_681()
	call_c   build_ref_203()
	call_c   Dyam_Term_Start(&ref[681],3)
	call_c   Dyam_Term_Arg(&ref[203])
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[303]
	c_ret

;; TERM 306: public(_E, _B)
c_code local build_ref_306
	ret_reg &ref[306]
	call_c   build_ref_704()
	call_c   Dyam_Create_Binary(&ref[704],V(4),V(1))
	move_ret ref[306]
	c_ret

long local pool_fun126[3]=[2,build_ref_304,build_ref_306]

pl_code local fun126
	call_c   Dyam_Pool(pool_fun126)
	call_c   Dyam_Unify_Item(&ref[304])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(2))
	move     V(4), R(2)
	move     S(5), R(3)
	pl_call  pred_spec_in_module_2()
	move     &ref[306], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Deallocate(1)
	pl_jump  pred_record_without_doublon_1()

;; TERM 305: '*GUARD*'(installer(public(_B), _C, _D)) :> []
c_code local build_ref_305
	ret_reg &ref[305]
	call_c   build_ref_304()
	call_c   Dyam_Create_Binary(I(9),&ref[304],I(0))
	move_ret ref[305]
	c_ret

c_code local build_seed_51
	ret_reg &seed[51]
	call_c   build_ref_11()
	call_c   build_ref_309()
	call_c   Dyam_Seed_Start(&ref[11],&ref[309],I(0),fun0,1)
	call_c   build_ref_308()
	call_c   Dyam_Seed_Add_Comp(&ref[308],fun129,0)
	call_c   Dyam_Seed_End()
	move_ret seed[51]
	c_ret

;; TERM 308: '*GUARD*'(directive(end_require, _B, _C, _D))
c_code local build_ref_308
	ret_reg &ref[308]
	call_c   build_ref_12()
	call_c   build_ref_307()
	call_c   Dyam_Create_Unary(&ref[12],&ref[307])
	move_ret ref[308]
	c_ret

;; TERM 307: directive(end_require, _B, _C, _D)
c_code local build_ref_307
	ret_reg &ref[307]
	call_c   build_ref_682()
	call_c   build_ref_722()
	call_c   Dyam_Term_Start(&ref[682],4)
	call_c   Dyam_Term_Arg(&ref[722])
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[307]
	c_ret

;; TERM 722: end_require
c_code local build_ref_722
	ret_reg &ref[722]
	call_c   Dyam_Create_Atom("end_require")
	move_ret ref[722]
	c_ret

pl_code local fun128
	call_c   Dyam_Remove_Choice()
fun127:
	call_c   Dyam_Deallocate()
	pl_ret


;; TERM 310: eof
c_code local build_ref_310
	ret_reg &ref[310]
	call_c   Dyam_Create_Atom("eof")
	move_ret ref[310]
	c_ret

long local pool_fun129[4]=[3,build_ref_308,build_ref_100,build_ref_310]

pl_code local fun129
	call_c   Dyam_Pool(pool_fun129)
	call_c   Dyam_Unify_Item(&ref[308])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun128)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(1),&ref[100])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(3),&ref[310])
	fail_ret
	pl_jump  fun127()

;; TERM 309: '*GUARD*'(directive(end_require, _B, _C, _D)) :> []
c_code local build_ref_309
	ret_reg &ref[309]
	call_c   build_ref_308()
	call_c   Dyam_Create_Binary(I(9),&ref[308],I(0))
	move_ret ref[309]
	c_ret

c_code local build_seed_77
	ret_reg &seed[77]
	call_c   build_ref_11()
	call_c   build_ref_313()
	call_c   Dyam_Seed_Start(&ref[11],&ref[313],I(0),fun0,1)
	call_c   build_ref_312()
	call_c   Dyam_Seed_Add_Comp(&ref[312],fun130,0)
	call_c   Dyam_Seed_End()
	move_ret seed[77]
	c_ret

;; TERM 312: '*GUARD*'(directive((lc _B), _C, _D, _E))
c_code local build_ref_312
	ret_reg &ref[312]
	call_c   build_ref_12()
	call_c   build_ref_311()
	call_c   Dyam_Create_Unary(&ref[12],&ref[311])
	move_ret ref[312]
	c_ret

;; TERM 311: directive((lc _B), _C, _D, _E)
c_code local build_ref_311
	ret_reg &ref[311]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_723()
	call_c   Dyam_Create_Unary(&ref[723],V(1))
	move_ret R(0)
	call_c   build_ref_682()
	call_c   Dyam_Term_Start(&ref[682],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[311]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 723: lc
c_code local build_ref_723
	ret_reg &ref[723]
	call_c   Dyam_Create_Atom("lc")
	move_ret ref[723]
	c_ret

;; TERM 314: left_corner
c_code local build_ref_314
	ret_reg &ref[314]
	call_c   Dyam_Create_Atom("left_corner")
	move_ret ref[314]
	c_ret

;; TERM 315: flag(lc)
c_code local build_ref_315
	ret_reg &ref[315]
	call_c   build_ref_705()
	call_c   build_ref_723()
	call_c   Dyam_Create_Unary(&ref[705],&ref[723])
	move_ret ref[315]
	c_ret

long local pool_fun130[4]=[3,build_ref_312,build_ref_314,build_ref_315]

pl_code local fun130
	call_c   Dyam_Pool(pool_fun130)
	call_c   Dyam_Unify_Item(&ref[312])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[314], R(0)
	move     0, R(1)
	pl_call  pred_record_without_doublon_1()
	move     &ref[315], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(1))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_install_2()

;; TERM 313: '*GUARD*'(directive((lc _B), _C, _D, _E)) :> []
c_code local build_ref_313
	ret_reg &ref[313]
	call_c   build_ref_312()
	call_c   Dyam_Create_Binary(I(9),&ref[312],I(0))
	move_ret ref[313]
	c_ret

c_code local build_seed_26
	ret_reg &seed[26]
	call_c   build_ref_11()
	call_c   build_ref_318()
	call_c   Dyam_Seed_Start(&ref[11],&ref[318],I(0),fun0,1)
	call_c   build_ref_317()
	call_c   Dyam_Seed_Add_Comp(&ref[317],fun132,0)
	call_c   Dyam_Seed_End()
	move_ret seed[26]
	c_ret

;; TERM 317: '*GUARD*'(installer(bmg_pushable(_B), _C, _D))
c_code local build_ref_317
	ret_reg &ref[317]
	call_c   build_ref_12()
	call_c   build_ref_316()
	call_c   Dyam_Create_Unary(&ref[12],&ref[316])
	move_ret ref[317]
	c_ret

;; TERM 316: installer(bmg_pushable(_B), _C, _D)
c_code local build_ref_316
	ret_reg &ref[316]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_695()
	call_c   Dyam_Create_Unary(&ref[695],V(1))
	move_ret R(0)
	call_c   build_ref_681()
	call_c   Dyam_Term_Start(&ref[681],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[316]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 319: bmg_pushable2(_E)
c_code local build_ref_319
	ret_reg &ref[319]
	call_c   build_ref_694()
	call_c   Dyam_Create_Unary(&ref[694],V(4))
	move_ret ref[319]
	c_ret

long local pool_fun132[3]=[2,build_ref_317,build_ref_319]

pl_code local fun132
	call_c   Dyam_Pool(pool_fun132)
	call_c   Dyam_Unify_Item(&ref[317])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(2))
	move     V(4), R(2)
	move     S(5), R(3)
	pl_call  pred_spec_in_module_2()
	move     &ref[319], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(1))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_install_2()

;; TERM 318: '*GUARD*'(installer(bmg_pushable(_B), _C, _D)) :> []
c_code local build_ref_318
	ret_reg &ref[318]
	call_c   build_ref_317()
	call_c   Dyam_Create_Binary(I(9),&ref[317],I(0))
	move_ret ref[318]
	c_ret

c_code local build_seed_57
	ret_reg &seed[57]
	call_c   build_ref_11()
	call_c   build_ref_322()
	call_c   Dyam_Seed_Start(&ref[11],&ref[322],I(0),fun0,1)
	call_c   build_ref_321()
	call_c   Dyam_Seed_Add_Comp(&ref[321],fun133,0)
	call_c   Dyam_Seed_End()
	move_ret seed[57]
	c_ret

;; TERM 321: '*GUARD*'(directive(module(_B, _C), _D, _E, _F))
c_code local build_ref_321
	ret_reg &ref[321]
	call_c   build_ref_12()
	call_c   build_ref_320()
	call_c   Dyam_Create_Unary(&ref[12],&ref[320])
	move_ret ref[321]
	c_ret

;; TERM 320: directive(module(_B, _C), _D, _E, _F)
c_code local build_ref_320
	ret_reg &ref[320]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_724()
	call_c   Dyam_Create_Binary(&ref[724],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_682()
	call_c   Dyam_Term_Start(&ref[682],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[320]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 724: module
c_code local build_ref_724
	ret_reg &ref[724]
	call_c   Dyam_Create_Atom("module")
	move_ret ref[724]
	c_ret

pl_code local fun133
	call_c   build_ref_321()
	call_c   Dyam_Unify_Item(&ref[321])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(2))
	call_c   Dyam_Reg_Load(4,V(4))
	call_c   Dyam_Reg_Load(6,V(3))
	call_c   Dyam_Reg_Deallocate(4)
	pl_jump  pred_module_directive_4()

;; TERM 322: '*GUARD*'(directive(module(_B, _C), _D, _E, _F)) :> []
c_code local build_ref_322
	ret_reg &ref[322]
	call_c   build_ref_321()
	call_c   Dyam_Create_Binary(I(9),&ref[321],I(0))
	move_ret ref[322]
	c_ret

c_code local build_seed_69
	ret_reg &seed[69]
	call_c   build_ref_11()
	call_c   build_ref_325()
	call_c   Dyam_Seed_Start(&ref[11],&ref[325],I(0),fun0,1)
	call_c   build_ref_324()
	call_c   Dyam_Seed_Add_Comp(&ref[324],fun139,0)
	call_c   Dyam_Seed_End()
	move_ret seed[69]
	c_ret

;; TERM 324: '*GUARD*'(directive(tag_features(_B, _C, _D), _E, _F, _G))
c_code local build_ref_324
	ret_reg &ref[324]
	call_c   build_ref_12()
	call_c   build_ref_323()
	call_c   Dyam_Create_Unary(&ref[12],&ref[323])
	move_ret ref[324]
	c_ret

;; TERM 323: directive(tag_features(_B, _C, _D), _E, _F, _G)
c_code local build_ref_323
	ret_reg &ref[323]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_725()
	call_c   Dyam_Term_Start(&ref[725],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_682()
	call_c   Dyam_Term_Start(&ref[682],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[323]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 725: tag_features
c_code local build_ref_725
	ret_reg &ref[725]
	call_c   Dyam_Create_Atom("tag_features")
	move_ret ref[725]
	c_ret

pl_code local fun139
	call_c   build_ref_324()
	call_c   Dyam_Unify_Item(&ref[324])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(2))
	call_c   Dyam_Reg_Load(4,V(3))
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  pred_install_tag_features_3()

;; TERM 325: '*GUARD*'(directive(tag_features(_B, _C, _D), _E, _F, _G)) :> []
c_code local build_ref_325
	ret_reg &ref[325]
	call_c   build_ref_324()
	call_c   Dyam_Create_Binary(I(9),&ref[324],I(0))
	move_ret ref[325]
	c_ret

c_code local build_seed_86
	ret_reg &seed[86]
	call_c   build_ref_11()
	call_c   build_ref_328()
	call_c   Dyam_Seed_Start(&ref[11],&ref[328],I(0),fun0,1)
	call_c   build_ref_327()
	call_c   Dyam_Seed_Add_Comp(&ref[327],fun140,0)
	call_c   Dyam_Seed_End()
	move_ret seed[86]
	c_ret

;; TERM 327: '*GUARD*'(directive(op(_B, _C, _D), _E, _F, _G))
c_code local build_ref_327
	ret_reg &ref[327]
	call_c   build_ref_12()
	call_c   build_ref_326()
	call_c   Dyam_Create_Unary(&ref[12],&ref[326])
	move_ret ref[327]
	c_ret

;; TERM 326: directive(op(_B, _C, _D), _E, _F, _G)
c_code local build_ref_326
	ret_reg &ref[326]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_710()
	call_c   Dyam_Term_Start(&ref[710],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_682()
	call_c   Dyam_Term_Start(&ref[682],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[326]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun140[3]=[2,build_ref_327,build_ref_329]

pl_code local fun140
	call_c   Dyam_Pool(pool_fun140)
	call_c   Dyam_Unify_Item(&ref[327])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[329], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(3))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_install_2()

;; TERM 328: '*GUARD*'(directive(op(_B, _C, _D), _E, _F, _G)) :> []
c_code local build_ref_328
	ret_reg &ref[328]
	call_c   build_ref_327()
	call_c   Dyam_Create_Binary(I(9),&ref[327],I(0))
	move_ret ref[328]
	c_ret

c_code local build_seed_47
	ret_reg &seed[47]
	call_c   build_ref_11()
	call_c   build_ref_334()
	call_c   Dyam_Seed_Start(&ref[11],&ref[334],I(0),fun0,1)
	call_c   build_ref_333()
	call_c   Dyam_Seed_Add_Comp(&ref[333],fun144,0)
	call_c   Dyam_Seed_End()
	move_ret seed[47]
	c_ret

;; TERM 333: '*GUARD*'(install_elem(_B, (_C / _D), _E))
c_code local build_ref_333
	ret_reg &ref[333]
	call_c   build_ref_12()
	call_c   build_ref_332()
	call_c   Dyam_Create_Unary(&ref[12],&ref[332])
	move_ret ref[333]
	c_ret

;; TERM 332: install_elem(_B, (_C / _D), _E)
c_code local build_ref_332
	ret_reg &ref[332]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_692()
	call_c   Dyam_Create_Binary(&ref[692],V(2),V(3))
	move_ret R(0)
	call_c   build_ref_679()
	call_c   Dyam_Term_Start(&ref[679],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[332]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_123
	ret_reg &seed[123]
	call_c   build_ref_12()
	call_c   build_ref_336()
	call_c   Dyam_Seed_Start(&ref[12],&ref[336],I(0),fun13,1)
	call_c   build_ref_337()
	call_c   Dyam_Seed_Add_Comp(&ref[337],&ref[336],0)
	call_c   Dyam_Seed_End()
	move_ret seed[123]
	c_ret

;; TERM 337: '*GUARD*'(installer(_B, (_C / _D), _E)) :> '$$HOLE$$'
c_code local build_ref_337
	ret_reg &ref[337]
	call_c   build_ref_336()
	call_c   Dyam_Create_Binary(I(9),&ref[336],I(7))
	move_ret ref[337]
	c_ret

;; TERM 336: '*GUARD*'(installer(_B, (_C / _D), _E))
c_code local build_ref_336
	ret_reg &ref[336]
	call_c   build_ref_12()
	call_c   build_ref_335()
	call_c   Dyam_Create_Unary(&ref[12],&ref[335])
	move_ret ref[336]
	c_ret

;; TERM 335: installer(_B, (_C / _D), _E)
c_code local build_ref_335
	ret_reg &ref[335]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_692()
	call_c   Dyam_Create_Binary(&ref[692],V(2),V(3))
	move_ret R(0)
	call_c   build_ref_681()
	call_c   Dyam_Term_Start(&ref[681],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[335]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun144[3]=[2,build_ref_333,build_seed_123]

pl_code local fun144
	call_c   Dyam_Pool(pool_fun144)
	call_c   Dyam_Unify_Item(&ref[333])
	fail_ret
	call_c   DYAM_evpred_atom(V(2))
	fail_ret
	call_c   DYAM_evpred_integer(V(3))
	fail_ret
	pl_jump  fun25(&seed[123],1)

;; TERM 334: '*GUARD*'(install_elem(_B, (_C / _D), _E)) :> []
c_code local build_ref_334
	ret_reg &ref[334]
	call_c   build_ref_333()
	call_c   Dyam_Create_Binary(I(9),&ref[333],I(0))
	move_ret ref[334]
	c_ret

c_code local build_seed_66
	ret_reg &seed[66]
	call_c   build_ref_11()
	call_c   build_ref_340()
	call_c   Dyam_Seed_Start(&ref[11],&ref[340],I(0),fun0,1)
	call_c   build_ref_339()
	call_c   Dyam_Seed_Add_Comp(&ref[339],fun145,0)
	call_c   Dyam_Seed_End()
	move_ret seed[66]
	c_ret

;; TERM 339: '*GUARD*'(directive(tag_anchor{family=> _B, coanchors=> _C, equations=> _D}, _E, _F, _G))
c_code local build_ref_339
	ret_reg &ref[339]
	call_c   build_ref_12()
	call_c   build_ref_338()
	call_c   Dyam_Create_Unary(&ref[12],&ref[338])
	move_ret ref[339]
	c_ret

;; TERM 338: directive(tag_anchor{family=> _B, coanchors=> _C, equations=> _D}, _E, _F, _G)
c_code local build_ref_338
	ret_reg &ref[338]
	call_c   build_ref_682()
	call_c   build_ref_341()
	call_c   Dyam_Term_Start(&ref[682],4)
	call_c   Dyam_Term_Arg(&ref[341])
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[338]
	c_ret

;; TERM 341: tag_anchor{family=> _B, coanchors=> _C, equations=> _D}
c_code local build_ref_341
	ret_reg &ref[341]
	call_c   build_ref_726()
	call_c   Dyam_Term_Start(&ref[726],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[341]
	c_ret

;; TERM 726: tag_anchor!'$ft'
c_code local build_ref_726
	ret_reg &ref[726]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_729()
	call_c   Dyam_Create_List(&ref[729],I(0))
	move_ret R(0)
	call_c   build_ref_728()
	call_c   Dyam_Create_List(&ref[728],R(0))
	move_ret R(0)
	call_c   build_ref_700()
	call_c   Dyam_Create_List(&ref[700],R(0))
	move_ret R(0)
	call_c   build_ref_727()
	call_c   Dyam_Create_Atom_Module("$ft",&ref[727])
	move_ret ref[726]
	call_c   DYAM_Feature_2(&ref[727],R(0))
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 727: tag_anchor
c_code local build_ref_727
	ret_reg &ref[727]
	call_c   Dyam_Create_Atom("tag_anchor")
	move_ret ref[727]
	c_ret

;; TERM 728: coanchors
c_code local build_ref_728
	ret_reg &ref[728]
	call_c   Dyam_Create_Atom("coanchors")
	move_ret ref[728]
	c_ret

;; TERM 729: equations
c_code local build_ref_729
	ret_reg &ref[729]
	call_c   Dyam_Create_Atom("equations")
	move_ret ref[729]
	c_ret

long local pool_fun145[3]=[2,build_ref_339,build_ref_341]

pl_code local fun145
	call_c   Dyam_Pool(pool_fun145)
	call_c   Dyam_Unify_Item(&ref[339])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[341], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(1))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_install_2()

;; TERM 340: '*GUARD*'(directive(tag_anchor{family=> _B, coanchors=> _C, equations=> _D}, _E, _F, _G)) :> []
c_code local build_ref_340
	ret_reg &ref[340]
	call_c   build_ref_339()
	call_c   Dyam_Create_Binary(I(9),&ref[339],I(0))
	move_ret ref[340]
	c_ret

c_code local build_seed_31
	ret_reg &seed[31]
	call_c   build_ref_11()
	call_c   build_ref_344()
	call_c   Dyam_Seed_Start(&ref[11],&ref[344],I(0),fun0,1)
	call_c   build_ref_343()
	call_c   Dyam_Seed_Add_Comp(&ref[343],fun158,0)
	call_c   Dyam_Seed_End()
	move_ret seed[31]
	c_ret

;; TERM 343: '*GUARD*'(installer(tag_anchor{family=> _B, coanchors=> _C, equations=> _D}, (_E / 0), _F))
c_code local build_ref_343
	ret_reg &ref[343]
	call_c   build_ref_12()
	call_c   build_ref_342()
	call_c   Dyam_Create_Unary(&ref[12],&ref[342])
	move_ret ref[343]
	c_ret

;; TERM 342: installer(tag_anchor{family=> _B, coanchors=> _C, equations=> _D}, (_E / 0), _F)
c_code local build_ref_342
	ret_reg &ref[342]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_692()
	call_c   Dyam_Create_Binary(&ref[692],V(4),N(0))
	move_ret R(0)
	call_c   build_ref_681()
	call_c   build_ref_341()
	call_c   Dyam_Term_Start(&ref[681],3)
	call_c   Dyam_Term_Arg(&ref[341])
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[342]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 352: 'tag_anchor: ground list expected ~w'
c_code local build_ref_352
	ret_reg &ref[352]
	call_c   Dyam_Create_Atom("tag_anchor: ground list expected ~w")
	move_ret ref[352]
	c_ret

;; TERM 214: [_C]
c_code local build_ref_214
	ret_reg &ref[214]
	call_c   Dyam_Create_List(V(2),I(0))
	move_ret ref[214]
	c_ret

c_code local build_seed_124
	ret_reg &seed[124]
	call_c   build_ref_12()
	call_c   build_ref_346()
	call_c   Dyam_Seed_Start(&ref[12],&ref[346],I(0),fun13,1)
	call_c   build_ref_347()
	call_c   Dyam_Seed_Add_Comp(&ref[347],&ref[346],0)
	call_c   Dyam_Seed_End()
	move_ret seed[124]
	c_ret

;; TERM 347: '*GUARD*'(tag_anchor_set_coanchors(_C, _H, _E, _H)) :> '$$HOLE$$'
c_code local build_ref_347
	ret_reg &ref[347]
	call_c   build_ref_346()
	call_c   Dyam_Create_Binary(I(9),&ref[346],I(7))
	move_ret ref[347]
	c_ret

;; TERM 346: '*GUARD*'(tag_anchor_set_coanchors(_C, _H, _E, _H))
c_code local build_ref_346
	ret_reg &ref[346]
	call_c   build_ref_12()
	call_c   build_ref_345()
	call_c   Dyam_Create_Unary(&ref[12],&ref[345])
	move_ret ref[346]
	c_ret

;; TERM 345: tag_anchor_set_coanchors(_C, _H, _E, _H)
c_code local build_ref_345
	ret_reg &ref[345]
	call_c   build_ref_676()
	call_c   Dyam_Term_Start(&ref[676],4)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[345]
	c_ret

c_code local build_seed_125
	ret_reg &seed[125]
	call_c   build_ref_12()
	call_c   build_ref_349()
	call_c   Dyam_Seed_Start(&ref[12],&ref[349],I(0),fun13,1)
	call_c   build_ref_350()
	call_c   Dyam_Seed_Add_Comp(&ref[350],&ref[349],0)
	call_c   Dyam_Seed_End()
	move_ret seed[125]
	c_ret

;; TERM 350: '*GUARD*'(tag_anchor_collect_eqvars(_D, _I)) :> '$$HOLE$$'
c_code local build_ref_350
	ret_reg &ref[350]
	call_c   build_ref_349()
	call_c   Dyam_Create_Binary(I(9),&ref[349],I(7))
	move_ret ref[350]
	c_ret

;; TERM 349: '*GUARD*'(tag_anchor_collect_eqvars(_D, _I))
c_code local build_ref_349
	ret_reg &ref[349]
	call_c   build_ref_12()
	call_c   build_ref_348()
	call_c   Dyam_Create_Unary(&ref[12],&ref[348])
	move_ret ref[349]
	c_ret

;; TERM 348: tag_anchor_collect_eqvars(_D, _I)
c_code local build_ref_348
	ret_reg &ref[348]
	call_c   build_ref_674()
	call_c   Dyam_Create_Binary(&ref[674],V(3),V(8))
	move_ret ref[348]
	c_ret

;; TERM 351: tag_anchor{family=> _E, coanchors=> _H, equations=> _I}
c_code local build_ref_351
	ret_reg &ref[351]
	call_c   build_ref_726()
	call_c   Dyam_Term_Start(&ref[726],3)
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_End()
	move_ret ref[351]
	c_ret

long local pool_fun152[4]=[3,build_seed_124,build_seed_125,build_ref_351]

long local pool_fun153[4]=[65538,build_ref_352,build_ref_214,pool_fun152]

pl_code local fun153
	call_c   Dyam_Remove_Choice()
	move     &ref[352], R(0)
	move     0, R(1)
	move     &ref[214], R(2)
	move     S(5), R(3)
	pl_call  pred_error_2()
fun152:
	call_c   DYAM_evpred_length(V(2),V(6))
	fail_ret
	call_c   DYAM_evpred_length(V(7),V(6))
	fail_ret
	pl_call  fun25(&seed[124],1)
	pl_call  fun25(&seed[125],1)
	call_c   DYAM_evpred_assert_1(&ref[351])
	call_c   Dyam_Choice(fun128)
	pl_call  Domain_2(V(9),V(3))
	call_c   Dyam_Reg_Load(0,V(9))
	call_c   Dyam_Reg_Load(2,V(8))
	call_c   Dyam_Reg_Load(4,V(4))
	pl_call  pred_tag_anchor_set_equation_3()
	pl_fail


long local pool_fun154[4]=[131073,build_seed_114,pool_fun153,pool_fun152]

pl_code local fun155
	call_c   Dyam_Remove_Choice()
fun154:
	call_c   Dyam_Choice(fun153)
	call_c   Dyam_Set_Cut()
	pl_call  fun25(&seed[114],1)
	call_c   Dyam_Cut()
	pl_jump  fun152()


long local pool_fun156[3]=[131072,pool_fun154,pool_fun154]

pl_code local fun157
	call_c   Dyam_Remove_Choice()
fun156:
	call_c   Dyam_Choice(fun155)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(3),I(0))
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun154()


long local pool_fun158[4]=[131073,build_ref_343,pool_fun156,pool_fun156]

pl_code local fun158
	call_c   Dyam_Pool(pool_fun158)
	call_c   Dyam_Unify_Item(&ref[343])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun157)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(2),I(0))
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun156()

;; TERM 344: '*GUARD*'(installer(tag_anchor{family=> _B, coanchors=> _C, equations=> _D}, (_E / 0), _F)) :> []
c_code local build_ref_344
	ret_reg &ref[344]
	call_c   build_ref_343()
	call_c   Dyam_Create_Binary(I(9),&ref[343],I(0))
	move_ret ref[344]
	c_ret

c_code local build_seed_60
	ret_reg &seed[60]
	call_c   build_ref_11()
	call_c   build_ref_355()
	call_c   Dyam_Seed_Start(&ref[11],&ref[355],I(0),fun0,1)
	call_c   build_ref_354()
	call_c   Dyam_Seed_Add_Comp(&ref[354],fun159,0)
	call_c   Dyam_Seed_End()
	move_ret seed[60]
	c_ret

;; TERM 354: '*GUARD*'(directive(bmg_stacks(_B), _C, _D, _E))
c_code local build_ref_354
	ret_reg &ref[354]
	call_c   build_ref_12()
	call_c   build_ref_353()
	call_c   Dyam_Create_Unary(&ref[12],&ref[353])
	move_ret ref[354]
	c_ret

;; TERM 353: directive(bmg_stacks(_B), _C, _D, _E)
c_code local build_ref_353
	ret_reg &ref[353]
	call_c   build_ref_682()
	call_c   build_ref_357()
	call_c   Dyam_Term_Start(&ref[682],4)
	call_c   Dyam_Term_Arg(&ref[357])
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[353]
	c_ret

;; TERM 357: bmg_stacks(_B)
c_code local build_ref_357
	ret_reg &ref[357]
	call_c   build_ref_730()
	call_c   Dyam_Create_Unary(&ref[730],V(1))
	move_ret ref[357]
	c_ret

;; TERM 730: bmg_stacks
c_code local build_ref_730
	ret_reg &ref[730]
	call_c   Dyam_Create_Atom("bmg_stacks")
	move_ret ref[730]
	c_ret

;; TERM 356: bmg_stacks(_F)
c_code local build_ref_356
	ret_reg &ref[356]
	call_c   build_ref_730()
	call_c   Dyam_Create_Unary(&ref[730],V(5))
	move_ret ref[356]
	c_ret

long local pool_fun159[5]=[4,build_ref_354,build_ref_356,build_ref_357,build_ref_358]

pl_code local fun159
	call_c   Dyam_Pool(pool_fun159)
	call_c   Dyam_Unify_Item(&ref[354])
	fail_ret
	call_c   DYAM_evpred_retract(&ref[356])
	fail_ret
	call_c   DYAM_evpred_assert_1(&ref[357])
	call_c   Dyam_Allocate(0)
	move     &ref[358], R(0)
	move     0, R(1)
	call_c   Dyam_Reg_Load(2,V(1))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_install_2()

;; TERM 355: '*GUARD*'(directive(bmg_stacks(_B), _C, _D, _E)) :> []
c_code local build_ref_355
	ret_reg &ref[355]
	call_c   build_ref_354()
	call_c   Dyam_Create_Binary(I(9),&ref[354],I(0))
	move_ret ref[355]
	c_ret

c_code local build_seed_73
	ret_reg &seed[73]
	call_c   build_ref_11()
	call_c   build_ref_361()
	call_c   Dyam_Seed_Start(&ref[11],&ref[361],I(0),fun0,1)
	call_c   build_ref_360()
	call_c   Dyam_Seed_Add_Comp(&ref[360],fun160,0)
	call_c   Dyam_Seed_End()
	move_ret seed[73]
	c_ret

;; TERM 360: '*GUARD*'(directive(cmode(_B), _C, _D, _E))
c_code local build_ref_360
	ret_reg &ref[360]
	call_c   build_ref_12()
	call_c   build_ref_359()
	call_c   Dyam_Create_Unary(&ref[12],&ref[359])
	move_ret ref[360]
	c_ret

;; TERM 359: directive(cmode(_B), _C, _D, _E)
c_code local build_ref_359
	ret_reg &ref[359]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_731()
	call_c   Dyam_Create_Unary(&ref[731],V(1))
	move_ret R(0)
	call_c   build_ref_682()
	call_c   Dyam_Term_Start(&ref[682],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[359]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 731: cmode
c_code local build_ref_731
	ret_reg &ref[731]
	call_c   Dyam_Create_Atom("cmode")
	move_ret ref[731]
	c_ret

;; TERM 362: cmode_to_pat(_B, _F)
c_code local build_ref_362
	ret_reg &ref[362]
	call_c   build_ref_668()
	call_c   Dyam_Create_Binary(&ref[668],V(1),V(5))
	move_ret ref[362]
	c_ret

;; TERM 363: call_pattern('*default*', _G)
c_code local build_ref_363
	ret_reg &ref[363]
	call_c   build_ref_732()
	call_c   build_ref_680()
	call_c   Dyam_Create_Binary(&ref[732],&ref[680],V(6))
	move_ret ref[363]
	c_ret

;; TERM 732: call_pattern
c_code local build_ref_732
	ret_reg &ref[732]
	call_c   Dyam_Create_Atom("call_pattern")
	move_ret ref[732]
	c_ret

;; TERM 364: call_pattern('*default*', _F)
c_code local build_ref_364
	ret_reg &ref[364]
	call_c   build_ref_732()
	call_c   build_ref_680()
	call_c   Dyam_Create_Binary(&ref[732],&ref[680],V(5))
	move_ret ref[364]
	c_ret

long local pool_fun160[5]=[4,build_ref_360,build_ref_362,build_ref_363,build_ref_364]

pl_code local fun160
	call_c   Dyam_Pool(pool_fun160)
	call_c   Dyam_Unify_Item(&ref[360])
	fail_ret
	call_c   Dyam_Allocate(0)
	pl_call  Object_1(&ref[362])
	call_c   DYAM_evpred_retract(&ref[363])
	fail_ret
	call_c   DYAM_evpred_assert_1(&ref[364])
	call_c   Dyam_Deallocate()
	pl_ret

;; TERM 361: '*GUARD*'(directive(cmode(_B), _C, _D, _E)) :> []
c_code local build_ref_361
	ret_reg &ref[361]
	call_c   build_ref_360()
	call_c   Dyam_Create_Binary(I(9),&ref[360],I(0))
	move_ret ref[361]
	c_ret

c_code local build_seed_88
	ret_reg &seed[88]
	call_c   build_ref_11()
	call_c   build_ref_367()
	call_c   Dyam_Seed_Start(&ref[11],&ref[367],I(0),fun0,1)
	call_c   build_ref_366()
	call_c   Dyam_Seed_Add_Comp(&ref[366],fun161,0)
	call_c   Dyam_Seed_End()
	move_ret seed[88]
	c_ret

;; TERM 366: '*GUARD*'(directive(finite_set(_B, _C), _D, _E, _F))
c_code local build_ref_366
	ret_reg &ref[366]
	call_c   build_ref_12()
	call_c   build_ref_365()
	call_c   Dyam_Create_Unary(&ref[12],&ref[365])
	move_ret ref[366]
	c_ret

;; TERM 365: directive(finite_set(_B, _C), _D, _E, _F)
c_code local build_ref_365
	ret_reg &ref[365]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_733()
	call_c   Dyam_Create_Binary(&ref[733],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_682()
	call_c   Dyam_Term_Start(&ref[682],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[365]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 733: finite_set
c_code local build_ref_733
	ret_reg &ref[733]
	call_c   Dyam_Create_Atom("finite_set")
	move_ret ref[733]
	c_ret

;; TERM 368: finite_set(_C)
c_code local build_ref_368
	ret_reg &ref[368]
	call_c   build_ref_733()
	call_c   Dyam_Create_Unary(&ref[733],V(2))
	move_ret ref[368]
	c_ret

long local pool_fun161[3]=[2,build_ref_366,build_ref_368]

pl_code local fun161
	call_c   Dyam_Pool(pool_fun161)
	call_c   Dyam_Unify_Item(&ref[366])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(2))
	pl_call  pred_check_finite_set_1()
	move     &ref[368], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(1))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_install_2()

;; TERM 367: '*GUARD*'(directive(finite_set(_B, _C), _D, _E, _F)) :> []
c_code local build_ref_367
	ret_reg &ref[367]
	call_c   build_ref_366()
	call_c   Dyam_Create_Binary(I(9),&ref[366],I(0))
	move_ret ref[367]
	c_ret

c_code local build_seed_50
	ret_reg &seed[50]
	call_c   build_ref_11()
	call_c   build_ref_371()
	call_c   Dyam_Seed_Start(&ref[11],&ref[371],I(0),fun0,1)
	call_c   build_ref_370()
	call_c   Dyam_Seed_Add_Comp(&ref[370],fun163,0)
	call_c   Dyam_Seed_End()
	move_ret seed[50]
	c_ret

;; TERM 370: '*GUARD*'(directive(include_unless_def(_B), _C, _D, _E))
c_code local build_ref_370
	ret_reg &ref[370]
	call_c   build_ref_12()
	call_c   build_ref_369()
	call_c   Dyam_Create_Unary(&ref[12],&ref[369])
	move_ret ref[370]
	c_ret

;; TERM 369: directive(include_unless_def(_B), _C, _D, _E)
c_code local build_ref_369
	ret_reg &ref[369]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_734()
	call_c   Dyam_Create_Unary(&ref[734],V(1))
	move_ret R(0)
	call_c   build_ref_682()
	call_c   Dyam_Term_Start(&ref[682],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[369]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 734: include_unless_def
c_code local build_ref_734
	ret_reg &ref[734]
	call_c   Dyam_Create_Atom("include_unless_def")
	move_ret ref[734]
	c_ret

pl_code local fun162
	call_c   Dyam_Remove_Choice()
	call_c   DYAM_evpred_assert_1(V(1))
	pl_jump  fun127()

long local pool_fun163[3]=[2,build_ref_370,build_ref_310]

pl_code local fun163
	call_c   Dyam_Pool(pool_fun163)
	call_c   Dyam_Unify_Item(&ref[370])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun162)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(V(1))
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(4),&ref[310])
	fail_ret
	pl_jump  fun127()

;; TERM 371: '*GUARD*'(directive(include_unless_def(_B), _C, _D, _E)) :> []
c_code local build_ref_371
	ret_reg &ref[371]
	call_c   build_ref_370()
	call_c   Dyam_Create_Binary(I(9),&ref[370],I(0))
	move_ret ref[371]
	c_ret

c_code local build_seed_68
	ret_reg &seed[68]
	call_c   build_ref_11()
	call_c   build_ref_374()
	call_c   Dyam_Seed_Start(&ref[11],&ref[374],I(0),fun0,1)
	call_c   build_ref_373()
	call_c   Dyam_Seed_Add_Comp(&ref[373],fun164,0)
	call_c   Dyam_Seed_End()
	move_ret seed[68]
	c_ret

;; TERM 373: '*GUARD*'(directive(tag_features_mode(_B, _C, _D, _E), _F, _G, _H))
c_code local build_ref_373
	ret_reg &ref[373]
	call_c   build_ref_12()
	call_c   build_ref_372()
	call_c   Dyam_Create_Unary(&ref[12],&ref[372])
	move_ret ref[373]
	c_ret

;; TERM 372: directive(tag_features_mode(_B, _C, _D, _E), _F, _G, _H)
c_code local build_ref_372
	ret_reg &ref[372]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_735()
	call_c   Dyam_Term_Start(&ref[735],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_682()
	call_c   Dyam_Term_Start(&ref[682],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[372]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 735: tag_features_mode
c_code local build_ref_735
	ret_reg &ref[735]
	call_c   Dyam_Create_Atom("tag_features_mode")
	move_ret ref[735]
	c_ret

pl_code local fun164
	call_c   build_ref_373()
	call_c   Dyam_Unify_Item(&ref[373])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(2))
	call_c   Dyam_Reg_Load(4,V(3))
	call_c   Dyam_Reg_Load(6,V(4))
	call_c   Dyam_Reg_Deallocate(4)
	pl_jump  pred_install_tag_features_mode_4()

;; TERM 374: '*GUARD*'(directive(tag_features_mode(_B, _C, _D, _E), _F, _G, _H)) :> []
c_code local build_ref_374
	ret_reg &ref[374]
	call_c   build_ref_373()
	call_c   Dyam_Create_Binary(I(9),&ref[373],I(0))
	move_ret ref[374]
	c_ret

c_code local build_seed_56
	ret_reg &seed[56]
	call_c   build_ref_11()
	call_c   build_ref_377()
	call_c   Dyam_Seed_Start(&ref[11],&ref[377],I(0),fun0,1)
	call_c   build_ref_376()
	call_c   Dyam_Seed_Add_Comp(&ref[376],fun173,0)
	call_c   Dyam_Seed_End()
	move_ret seed[56]
	c_ret

;; TERM 376: '*GUARD*'(directive(module(_B), _C, _D, _E))
c_code local build_ref_376
	ret_reg &ref[376]
	call_c   build_ref_12()
	call_c   build_ref_375()
	call_c   Dyam_Create_Unary(&ref[12],&ref[375])
	move_ret ref[376]
	c_ret

;; TERM 375: directive(module(_B), _C, _D, _E)
c_code local build_ref_375
	ret_reg &ref[375]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_724()
	call_c   Dyam_Create_Unary(&ref[724],V(1))
	move_ret R(0)
	call_c   build_ref_682()
	call_c   Dyam_Term_Start(&ref[682],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[375]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 204: '~w is not a valid module name'
c_code local build_ref_204
	ret_reg &ref[204]
	call_c   Dyam_Create_Atom("~w is not a valid module name")
	move_ret ref[204]
	c_ret

;; TERM 380: map_module(_B, _G, _H)
c_code local build_ref_380
	ret_reg &ref[380]
	call_c   build_ref_736()
	call_c   Dyam_Term_Start(&ref[736],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[380]
	c_ret

;; TERM 736: map_module
c_code local build_ref_736
	ret_reg &ref[736]
	call_c   Dyam_Create_Atom("map_module")
	move_ret ref[736]
	c_ret

pl_code local fun166
	call_c   Dyam_Remove_Choice()
fun165:
	call_c   DYAM_evpred_assert_1(&ref[380])
	call_c   Dyam_Deallocate()
	pl_ret


;; TERM 379: map_module(_I, _J, _K)
c_code local build_ref_379
	ret_reg &ref[379]
	call_c   build_ref_736()
	call_c   Dyam_Term_Start(&ref[736],3)
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_End()
	move_ret ref[379]
	c_ret

;; TERM 383: 'File ~w defining modules ~w and ~w'
c_code local build_ref_383
	ret_reg &ref[383]
	call_c   Dyam_Create_Atom("File ~w defining modules ~w and ~w")
	move_ret ref[383]
	c_ret

;; TERM 384: [_G,_B,_I]
c_code local build_ref_384
	ret_reg &ref[384]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(8),I(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(1),R(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(6),R(0))
	move_ret ref[384]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun167[5]=[4,build_ref_380,build_ref_383,build_ref_384,build_ref_380]

pl_code local fun167
	call_c   Dyam_Update_Choice(fun166)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(9),V(6))
	fail_ret
	call_c   Dyam_Cut()
	move     &ref[383], R(0)
	move     0, R(1)
	move     &ref[384], R(2)
	move     S(5), R(3)
	pl_call  pred_warning_2()
	pl_jump  fun165()

;; TERM 381: 'Files ~w and ~w redefining module ~w'
c_code local build_ref_381
	ret_reg &ref[381]
	call_c   Dyam_Create_Atom("Files ~w and ~w redefining module ~w")
	move_ret ref[381]
	c_ret

;; TERM 382: [_G,_J,_B]
c_code local build_ref_382
	ret_reg &ref[382]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_110()
	call_c   Dyam_Create_List(V(9),&ref[110])
	move_ret R(0)
	call_c   Dyam_Create_List(V(6),R(0))
	move_ret ref[382]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun168[5]=[65539,build_ref_381,build_ref_382,build_ref_380,pool_fun167]

pl_code local fun168
	call_c   Dyam_Update_Choice(fun167)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(8),V(1))
	fail_ret
	call_c   Dyam_Cut()
	move     &ref[381], R(0)
	move     0, R(1)
	move     &ref[382], R(2)
	move     S(5), R(3)
	pl_call  pred_warning_2()
	pl_jump  fun165()

long local pool_fun169[5]=[65539,build_ref_380,build_ref_379,build_ref_380,pool_fun168]

pl_code local fun170
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(7),V(3))
	fail_ret
	call_c   Dyam_Unify(V(6),V(3))
	fail_ret
fun169:
	call_c   Dyam_Choice(fun166)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[379])
	call_c   DYAM_evpred_retract(&ref[379])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun168)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(8),V(1))
	fail_ret
	call_c   Dyam_Unify(V(6),V(9))
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun165()


;; TERM 378: file_info(_F, _G, _H)
c_code local build_ref_378
	ret_reg &ref[378]
	call_c   build_ref_737()
	call_c   Dyam_Term_Start(&ref[737],3)
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[378]
	c_ret

;; TERM 737: file_info
c_code local build_ref_737
	ret_reg &ref[737]
	call_c   Dyam_Create_Atom("file_info")
	move_ret ref[737]
	c_ret

long local pool_fun171[4]=[131073,build_ref_378,pool_fun169,pool_fun169]

long local pool_fun172[4]=[65538,build_ref_204,build_ref_110,pool_fun171]

pl_code local fun172
	call_c   Dyam_Remove_Choice()
	move     &ref[204], R(0)
	move     0, R(1)
	move     &ref[110], R(2)
	move     S(5), R(3)
	pl_call  pred_error_2()
fun171:
	call_c   DYAM_Module_Set_1(V(1))
	fail_ret
	call_c   Dyam_Choice(fun170)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(3),&ref[378])
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun169()


long local pool_fun173[4]=[131073,build_ref_376,pool_fun172,pool_fun171]

pl_code local fun173
	call_c   Dyam_Pool(pool_fun173)
	call_c   Dyam_Unify_Item(&ref[376])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun172)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_atom(V(1))
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun171()

;; TERM 377: '*GUARD*'(directive(module(_B), _C, _D, _E)) :> []
c_code local build_ref_377
	ret_reg &ref[377]
	call_c   build_ref_376()
	call_c   Dyam_Create_Binary(I(9),&ref[376],I(0))
	move_ret ref[377]
	c_ret

c_code local build_seed_23
	ret_reg &seed[23]
	call_c   build_ref_11()
	call_c   build_ref_387()
	call_c   Dyam_Seed_Start(&ref[11],&ref[387],I(0),fun0,1)
	call_c   build_ref_386()
	call_c   Dyam_Seed_Add_Comp(&ref[386],fun177,0)
	call_c   Dyam_Seed_End()
	move_ret seed[23]
	c_ret

;; TERM 386: '*GUARD*'(check_mode([_B|_C]))
c_code local build_ref_386
	ret_reg &ref[386]
	call_c   build_ref_12()
	call_c   build_ref_385()
	call_c   Dyam_Create_Unary(&ref[12],&ref[385])
	move_ret ref[386]
	c_ret

;; TERM 385: check_mode([_B|_C])
c_code local build_ref_385
	ret_reg &ref[385]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(1),V(2))
	move_ret R(0)
	call_c   build_ref_673()
	call_c   Dyam_Create_Unary(&ref[673],R(0))
	move_ret ref[385]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_126
	ret_reg &seed[126]
	call_c   build_ref_12()
	call_c   build_ref_390()
	call_c   Dyam_Seed_Start(&ref[12],&ref[390],I(0),fun13,1)
	call_c   build_ref_391()
	call_c   Dyam_Seed_Add_Comp(&ref[391],&ref[390],0)
	call_c   Dyam_Seed_End()
	move_ret seed[126]
	c_ret

;; TERM 391: '*GUARD*'(check_mode(_C)) :> '$$HOLE$$'
c_code local build_ref_391
	ret_reg &ref[391]
	call_c   build_ref_390()
	call_c   Dyam_Create_Binary(I(9),&ref[390],I(7))
	move_ret ref[391]
	c_ret

;; TERM 390: '*GUARD*'(check_mode(_C))
c_code local build_ref_390
	ret_reg &ref[390]
	call_c   build_ref_12()
	call_c   build_ref_389()
	call_c   Dyam_Create_Unary(&ref[12],&ref[389])
	move_ret ref[390]
	c_ret

;; TERM 389: check_mode(_C)
c_code local build_ref_389
	ret_reg &ref[389]
	call_c   build_ref_673()
	call_c   Dyam_Create_Unary(&ref[673],V(2))
	move_ret ref[389]
	c_ret

long local pool_fun174[2]=[1,build_seed_126]

long local pool_fun175[3]=[65537,build_ref_393,pool_fun174]

pl_code local fun175
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(1),&ref[393])
	fail_ret
fun174:
	call_c   Dyam_Deallocate()
	pl_jump  fun25(&seed[126],1)


long local pool_fun176[4]=[131073,build_ref_392,pool_fun175,pool_fun174]

pl_code local fun176
	call_c   Dyam_Update_Choice(fun175)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(1),&ref[392])
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun174()

long local pool_fun177[5]=[131074,build_ref_386,build_ref_388,pool_fun176,pool_fun174]

pl_code local fun177
	call_c   Dyam_Pool(pool_fun177)
	call_c   Dyam_Unify_Item(&ref[386])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun176)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(1),&ref[388])
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun174()

;; TERM 387: '*GUARD*'(check_mode([_B|_C])) :> []
c_code local build_ref_387
	ret_reg &ref[387]
	call_c   build_ref_386()
	call_c   Dyam_Create_Binary(I(9),&ref[386],I(0))
	move_ret ref[387]
	c_ret

c_code local build_seed_30
	ret_reg &seed[30]
	call_c   build_ref_11()
	call_c   build_ref_396()
	call_c   Dyam_Seed_Start(&ref[11],&ref[396],I(0),fun0,1)
	call_c   build_ref_395()
	call_c   Dyam_Seed_Add_Comp(&ref[395],fun178,0)
	call_c   Dyam_Seed_End()
	move_ret seed[30]
	c_ret

;; TERM 395: '*GUARD*'(tag_anchor_set_coanchors([_B|_C], [_D|_E], _F, _G))
c_code local build_ref_395
	ret_reg &ref[395]
	call_c   build_ref_12()
	call_c   build_ref_394()
	call_c   Dyam_Create_Unary(&ref[12],&ref[394])
	move_ret ref[395]
	c_ret

;; TERM 394: tag_anchor_set_coanchors([_B|_C], [_D|_E], _F, _G)
c_code local build_ref_394
	ret_reg &ref[394]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(1),V(2))
	move_ret R(0)
	call_c   build_ref_676()
	call_c   build_ref_531()
	call_c   Dyam_Term_Start(&ref[676],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[531])
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[394]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 531: [_D|_E]
c_code local build_ref_531
	ret_reg &ref[531]
	call_c   Dyam_Create_List(V(3),V(4))
	move_ret ref[531]
	c_ret

;; TERM 397: tag_coanchor(_F, _B, _D, _G)
c_code local build_ref_397
	ret_reg &ref[397]
	call_c   build_ref_738()
	call_c   Dyam_Term_Start(&ref[738],4)
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[397]
	c_ret

;; TERM 738: tag_coanchor
c_code local build_ref_738
	ret_reg &ref[738]
	call_c   Dyam_Create_Atom("tag_coanchor")
	move_ret ref[738]
	c_ret

c_code local build_seed_127
	ret_reg &seed[127]
	call_c   build_ref_12()
	call_c   build_ref_399()
	call_c   Dyam_Seed_Start(&ref[12],&ref[399],I(0),fun13,1)
	call_c   build_ref_400()
	call_c   Dyam_Seed_Add_Comp(&ref[400],&ref[399],0)
	call_c   Dyam_Seed_End()
	move_ret seed[127]
	c_ret

;; TERM 400: '*GUARD*'(tag_anchor_set_coanchors(_C, _E, _F, _G)) :> '$$HOLE$$'
c_code local build_ref_400
	ret_reg &ref[400]
	call_c   build_ref_399()
	call_c   Dyam_Create_Binary(I(9),&ref[399],I(7))
	move_ret ref[400]
	c_ret

;; TERM 399: '*GUARD*'(tag_anchor_set_coanchors(_C, _E, _F, _G))
c_code local build_ref_399
	ret_reg &ref[399]
	call_c   build_ref_12()
	call_c   build_ref_398()
	call_c   Dyam_Create_Unary(&ref[12],&ref[398])
	move_ret ref[399]
	c_ret

;; TERM 398: tag_anchor_set_coanchors(_C, _E, _F, _G)
c_code local build_ref_398
	ret_reg &ref[398]
	call_c   build_ref_676()
	call_c   Dyam_Term_Start(&ref[676],4)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[398]
	c_ret

long local pool_fun178[4]=[3,build_ref_395,build_ref_397,build_seed_127]

pl_code local fun178
	call_c   Dyam_Pool(pool_fun178)
	call_c   Dyam_Unify_Item(&ref[395])
	fail_ret
	call_c   DYAM_evpred_assert_1(&ref[397])
	pl_jump  fun25(&seed[127],1)

;; TERM 396: '*GUARD*'(tag_anchor_set_coanchors([_B|_C], [_D|_E], _F, _G)) :> []
c_code local build_ref_396
	ret_reg &ref[396]
	call_c   build_ref_395()
	call_c   Dyam_Create_Binary(I(9),&ref[395],I(0))
	move_ret ref[396]
	c_ret

c_code local build_seed_18
	ret_reg &seed[18]
	call_c   build_ref_11()
	call_c   build_ref_403()
	call_c   Dyam_Seed_Start(&ref[11],&ref[403],I(0),fun0,1)
	call_c   build_ref_402()
	call_c   Dyam_Seed_Add_Comp(&ref[402],fun181,0)
	call_c   Dyam_Seed_End()
	move_ret seed[18]
	c_ret

;; TERM 402: '*GUARD*'(register_predicate(rcg, _B, _C))
c_code local build_ref_402
	ret_reg &ref[402]
	call_c   build_ref_12()
	call_c   build_ref_401()
	call_c   Dyam_Create_Unary(&ref[12],&ref[401])
	move_ret ref[402]
	c_ret

;; TERM 401: register_predicate(rcg, _B, _C)
c_code local build_ref_401
	ret_reg &ref[401]
	call_c   build_ref_716()
	call_c   build_ref_739()
	call_c   Dyam_Term_Start(&ref[716],3)
	call_c   Dyam_Term_Arg(&ref[739])
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_End()
	move_ret ref[401]
	c_ret

;; TERM 739: rcg
c_code local build_ref_739
	ret_reg &ref[739]
	call_c   Dyam_Create_Atom("rcg")
	move_ret ref[739]
	c_ret

;; TERM 405: 'trying register a non valid rcg predicate ~w'
c_code local build_ref_405
	ret_reg &ref[405]
	call_c   Dyam_Create_Atom("trying register a non valid rcg predicate ~w")
	move_ret ref[405]
	c_ret

;; TERM 404: predicate(_G, _C)
c_code local build_ref_404
	ret_reg &ref[404]
	call_c   build_ref_721()
	call_c   Dyam_Create_Binary(&ref[721],V(6),V(2))
	move_ret ref[404]
	c_ret

long local pool_fun180[4]=[3,build_ref_405,build_ref_110,build_ref_404]

pl_code local fun180
	call_c   Dyam_Remove_Choice()
	move     &ref[405], R(0)
	move     0, R(1)
	move     &ref[110], R(2)
	move     S(5), R(3)
	pl_call  pred_error_2()
fun179:
	call_c   Dyam_Reg_Load(0,V(3))
	move     V(6), R(2)
	move     S(5), R(3)
	move     V(7), R(4)
	move     S(5), R(5)
	move     V(8), R(6)
	move     S(5), R(7)
	pl_call  pred_term_current_module_shift_4()
	move     &ref[404], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Deallocate(1)
	pl_jump  pred_record_without_doublon_1()


long local pool_fun181[4]=[65538,build_ref_402,build_ref_404,pool_fun180]

pl_code local fun181
	call_c   Dyam_Pool(pool_fun181)
	call_c   Dyam_Unify_Item(&ref[402])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun180)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Reg_Load(0,V(1))
	move     V(3), R(2)
	move     S(5), R(3)
	move     V(4), R(4)
	move     S(5), R(5)
	move     V(5), R(6)
	move     S(5), R(7)
	pl_call  pred_rcg_functor_4()
	call_c   Dyam_Cut()
	pl_jump  fun179()

;; TERM 403: '*GUARD*'(register_predicate(rcg, _B, _C)) :> []
c_code local build_ref_403
	ret_reg &ref[403]
	call_c   build_ref_402()
	call_c   Dyam_Create_Binary(I(9),&ref[402],I(0))
	move_ret ref[403]
	c_ret

c_code local build_seed_20
	ret_reg &seed[20]
	call_c   build_ref_11()
	call_c   build_ref_408()
	call_c   Dyam_Seed_Start(&ref[11],&ref[408],I(0),fun0,1)
	call_c   build_ref_407()
	call_c   Dyam_Seed_Add_Comp(&ref[407],fun182,0)
	call_c   Dyam_Seed_End()
	move_ret seed[20]
	c_ret

;; TERM 407: '*GUARD*'(register_predicate(fact, _B, _C))
c_code local build_ref_407
	ret_reg &ref[407]
	call_c   build_ref_12()
	call_c   build_ref_406()
	call_c   Dyam_Create_Unary(&ref[12],&ref[406])
	move_ret ref[407]
	c_ret

;; TERM 406: register_predicate(fact, _B, _C)
c_code local build_ref_406
	ret_reg &ref[406]
	call_c   build_ref_716()
	call_c   build_ref_740()
	call_c   Dyam_Term_Start(&ref[716],3)
	call_c   Dyam_Term_Arg(&ref[740])
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_End()
	move_ret ref[406]
	c_ret

;; TERM 740: fact
c_code local build_ref_740
	ret_reg &ref[740]
	call_c   Dyam_Create_Atom("fact")
	move_ret ref[740]
	c_ret

;; TERM 409: predicate(_F, _C)
c_code local build_ref_409
	ret_reg &ref[409]
	call_c   build_ref_721()
	call_c   Dyam_Create_Binary(&ref[721],V(5),V(2))
	move_ret ref[409]
	c_ret

long local pool_fun182[4]=[3,build_ref_407,build_ref_301,build_ref_409]

pl_code local fun182
	call_c   Dyam_Pool(pool_fun182)
	call_c   Dyam_Unify_Item(&ref[407])
	fail_ret
	call_c   DYAM_evpred_functor(V(1),V(3),V(4))
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[301], R(0)
	move     S(5), R(1)
	move     V(5), R(2)
	move     S(5), R(3)
	move     V(6), R(4)
	move     S(5), R(5)
	move     V(7), R(6)
	move     S(5), R(7)
	pl_call  pred_term_current_module_shift_4()
	move     &ref[409], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Deallocate(1)
	pl_jump  pred_record_without_doublon_1()

;; TERM 408: '*GUARD*'(register_predicate(fact, _B, _C)) :> []
c_code local build_ref_408
	ret_reg &ref[408]
	call_c   build_ref_407()
	call_c   Dyam_Create_Binary(I(9),&ref[407],I(0))
	move_ret ref[408]
	c_ret

c_code local build_seed_21
	ret_reg &seed[21]
	call_c   build_ref_11()
	call_c   build_ref_412()
	call_c   Dyam_Seed_Start(&ref[11],&ref[412],I(0),fun0,1)
	call_c   build_ref_411()
	call_c   Dyam_Seed_Add_Comp(&ref[411],fun183,0)
	call_c   Dyam_Seed_End()
	move_ret seed[21]
	c_ret

;; TERM 411: '*GUARD*'(register_predicate(clause, _B, _C))
c_code local build_ref_411
	ret_reg &ref[411]
	call_c   build_ref_12()
	call_c   build_ref_410()
	call_c   Dyam_Create_Unary(&ref[12],&ref[410])
	move_ret ref[411]
	c_ret

;; TERM 410: register_predicate(clause, _B, _C)
c_code local build_ref_410
	ret_reg &ref[410]
	call_c   build_ref_716()
	call_c   build_ref_717()
	call_c   Dyam_Term_Start(&ref[716],3)
	call_c   Dyam_Term_Arg(&ref[717])
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_End()
	move_ret ref[410]
	c_ret

long local pool_fun183[4]=[3,build_ref_411,build_ref_301,build_ref_409]

pl_code local fun183
	call_c   Dyam_Pool(pool_fun183)
	call_c   Dyam_Unify_Item(&ref[411])
	fail_ret
	call_c   DYAM_evpred_functor(V(1),V(3),V(4))
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[301], R(0)
	move     S(5), R(1)
	move     V(5), R(2)
	move     S(5), R(3)
	move     V(6), R(4)
	move     S(5), R(5)
	move     V(7), R(6)
	move     S(5), R(7)
	pl_call  pred_term_current_module_shift_4()
	move     &ref[409], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Deallocate(1)
	pl_jump  pred_record_without_doublon_1()

;; TERM 412: '*GUARD*'(register_predicate(clause, _B, _C)) :> []
c_code local build_ref_412
	ret_reg &ref[412]
	call_c   build_ref_411()
	call_c   Dyam_Create_Binary(I(9),&ref[411],I(0))
	move_ret ref[412]
	c_ret

c_code local build_seed_53
	ret_reg &seed[53]
	call_c   build_ref_11()
	call_c   build_ref_415()
	call_c   Dyam_Seed_Start(&ref[11],&ref[415],I(0),fun0,1)
	call_c   build_ref_414()
	call_c   Dyam_Seed_Add_Comp(&ref[414],fun184,0)
	call_c   Dyam_Seed_End()
	move_ret seed[53]
	c_ret

;; TERM 414: '*GUARD*'(directive(tagop(_B), _C, _D, _E))
c_code local build_ref_414
	ret_reg &ref[414]
	call_c   build_ref_12()
	call_c   build_ref_413()
	call_c   Dyam_Create_Unary(&ref[12],&ref[413])
	move_ret ref[414]
	c_ret

;; TERM 413: directive(tagop(_B), _C, _D, _E)
c_code local build_ref_413
	ret_reg &ref[413]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_741()
	call_c   Dyam_Create_Unary(&ref[741],V(1))
	move_ret R(0)
	call_c   build_ref_682()
	call_c   Dyam_Term_Start(&ref[682],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[413]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 741: tagop
c_code local build_ref_741
	ret_reg &ref[741]
	call_c   Dyam_Create_Atom("tagop")
	move_ret ref[741]
	c_ret

;; TERM 416: [_B,_G,_H]
c_code local build_ref_416
	ret_reg &ref[416]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Tupple(6,7,I(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(1),R(0))
	move_ret ref[416]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 417: tagop(_I, _J, _K)
c_code local build_ref_417
	ret_reg &ref[417]
	call_c   build_ref_741()
	call_c   Dyam_Term_Start(&ref[741],3)
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_End()
	move_ret ref[417]
	c_ret

;; TERM 418: tagop(_G, _H, _F)
c_code local build_ref_418
	ret_reg &ref[418]
	call_c   build_ref_741()
	call_c   Dyam_Term_Start(&ref[741],3)
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[418]
	c_ret

long local pool_fun184[5]=[4,build_ref_414,build_ref_416,build_ref_417,build_ref_418]

pl_code local fun184
	call_c   Dyam_Pool(pool_fun184)
	call_c   Dyam_Unify_Item(&ref[414])
	fail_ret
	call_c   DYAM_evpred_univ(V(5),&ref[416])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[417], R(0)
	move     S(5), R(1)
	move     &ref[418], R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_directive_updater_2()

;; TERM 415: '*GUARD*'(directive(tagop(_B), _C, _D, _E)) :> []
c_code local build_ref_415
	ret_reg &ref[415]
	call_c   build_ref_414()
	call_c   Dyam_Create_Binary(I(9),&ref[414],I(0))
	move_ret ref[415]
	c_ret

c_code local build_seed_70
	ret_reg &seed[70]
	call_c   build_ref_11()
	call_c   build_ref_421()
	call_c   Dyam_Seed_Start(&ref[11],&ref[421],I(0),fun0,1)
	call_c   build_ref_420()
	call_c   Dyam_Seed_Add_Comp(&ref[420],fun187,0)
	call_c   Dyam_Seed_End()
	move_ret seed[70]
	c_ret

;; TERM 420: '*GUARD*'(directive(tag_node_mode(_B, _C), _D, _E, _F))
c_code local build_ref_420
	ret_reg &ref[420]
	call_c   build_ref_12()
	call_c   build_ref_419()
	call_c   Dyam_Create_Unary(&ref[12],&ref[419])
	move_ret ref[420]
	c_ret

;; TERM 419: directive(tag_node_mode(_B, _C), _D, _E, _F)
c_code local build_ref_419
	ret_reg &ref[419]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_742()
	call_c   Dyam_Create_Binary(&ref[742],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_682()
	call_c   Dyam_Term_Start(&ref[682],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[419]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 742: tag_node_mode
c_code local build_ref_742
	ret_reg &ref[742]
	call_c   Dyam_Create_Atom("tag_node_mode")
	move_ret ref[742]
	c_ret

;; TERM 426: 'Not a valid mode pattern ~w'
c_code local build_ref_426
	ret_reg &ref[426]
	call_c   Dyam_Create_Atom("Not a valid mode pattern ~w")
	move_ret ref[426]
	c_ret

;; TERM 427: [tag_node_mode(_B, _C)]
c_code local build_ref_427
	ret_reg &ref[427]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_742()
	call_c   Dyam_Create_Binary(&ref[742],V(1),V(2))
	move_ret R(0)
	call_c   Dyam_Create_List(R(0),I(0))
	move_ret ref[427]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 425: tag_node_mode(_C)
c_code local build_ref_425
	ret_reg &ref[425]
	call_c   build_ref_742()
	call_c   Dyam_Create_Unary(&ref[742],V(2))
	move_ret ref[425]
	c_ret

long local pool_fun186[4]=[3,build_ref_426,build_ref_427,build_ref_425]

pl_code local fun186
	call_c   Dyam_Remove_Choice()
	move     &ref[426], R(0)
	move     0, R(1)
	move     &ref[427], R(2)
	move     S(5), R(3)
	pl_call  pred_error_2()
fun185:
	move     &ref[425], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(1))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_install_2()


c_code local build_seed_128
	ret_reg &seed[128]
	call_c   build_ref_12()
	call_c   build_ref_423()
	call_c   Dyam_Seed_Start(&ref[12],&ref[423],I(0),fun13,1)
	call_c   build_ref_424()
	call_c   Dyam_Seed_Add_Comp(&ref[424],&ref[423],0)
	call_c   Dyam_Seed_End()
	move_ret seed[128]
	c_ret

;; TERM 424: '*GUARD*'(check_mode([_C])) :> '$$HOLE$$'
c_code local build_ref_424
	ret_reg &ref[424]
	call_c   build_ref_423()
	call_c   Dyam_Create_Binary(I(9),&ref[423],I(7))
	move_ret ref[424]
	c_ret

;; TERM 423: '*GUARD*'(check_mode([_C]))
c_code local build_ref_423
	ret_reg &ref[423]
	call_c   build_ref_12()
	call_c   build_ref_422()
	call_c   Dyam_Create_Unary(&ref[12],&ref[422])
	move_ret ref[423]
	c_ret

;; TERM 422: check_mode([_C])
c_code local build_ref_422
	ret_reg &ref[422]
	call_c   build_ref_673()
	call_c   build_ref_214()
	call_c   Dyam_Create_Unary(&ref[673],&ref[214])
	move_ret ref[422]
	c_ret

long local pool_fun187[5]=[65539,build_ref_420,build_seed_128,build_ref_425,pool_fun186]

pl_code local fun187
	call_c   Dyam_Pool(pool_fun187)
	call_c   Dyam_Unify_Item(&ref[420])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun186)
	call_c   Dyam_Set_Cut()
	pl_call  fun25(&seed[128],1)
	call_c   Dyam_Cut()
	pl_jump  fun185()

;; TERM 421: '*GUARD*'(directive(tag_node_mode(_B, _C), _D, _E, _F)) :> []
c_code local build_ref_421
	ret_reg &ref[421]
	call_c   build_ref_420()
	call_c   Dyam_Create_Binary(I(9),&ref[420],I(0))
	move_ret ref[421]
	c_ret

c_code local build_seed_35
	ret_reg &seed[35]
	call_c   build_ref_11()
	call_c   build_ref_437()
	call_c   Dyam_Seed_Start(&ref[11],&ref[437],I(0),fun0,1)
	call_c   build_ref_436()
	call_c   Dyam_Seed_Add_Comp(&ref[436],fun195,0)
	call_c   Dyam_Seed_End()
	move_ret seed[35]
	c_ret

;; TERM 436: '*GUARD*'(installer(mode(_B), _C, _D))
c_code local build_ref_436
	ret_reg &ref[436]
	call_c   build_ref_12()
	call_c   build_ref_435()
	call_c   Dyam_Create_Unary(&ref[12],&ref[435])
	move_ret ref[436]
	c_ret

;; TERM 435: installer(mode(_B), _C, _D)
c_code local build_ref_435
	ret_reg &ref[435]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_743()
	call_c   Dyam_Create_Unary(&ref[743],V(1))
	move_ret R(0)
	call_c   build_ref_681()
	call_c   Dyam_Term_Start(&ref[681],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[435]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 743: mode
c_code local build_ref_743
	ret_reg &ref[743]
	call_c   Dyam_Create_Atom("mode")
	move_ret ref[743]
	c_ret

;; TERM 442: call_pattern(_G, _B)
c_code local build_ref_442
	ret_reg &ref[442]
	call_c   build_ref_732()
	call_c   Dyam_Create_Binary(&ref[732],V(6),V(1))
	move_ret ref[442]
	c_ret

pl_code local fun192
	call_c   Dyam_Remove_Choice()
fun189:
	call_c   DYAM_evpred_assert_1(&ref[442])
	call_c   Dyam_Deallocate()
	pl_ret


;; TERM 440: call_pattern(_G, _H)
c_code local build_ref_440
	ret_reg &ref[440]
	call_c   build_ref_732()
	call_c   Dyam_Create_Binary(&ref[732],V(6),V(7))
	move_ret ref[440]
	c_ret

;; TERM 441: 'new mode pattern ~w for ~w replaces ~w'
c_code local build_ref_441
	ret_reg &ref[441]
	call_c   Dyam_Create_Atom("new mode pattern ~w for ~w replaces ~w")
	move_ret ref[441]
	c_ret

long local pool_fun190[4]=[3,build_ref_441,build_ref_416,build_ref_442]

pl_code local fun191
	call_c   Dyam_Remove_Choice()
fun190:
	call_c   Dyam_Cut()
	move     &ref[441], R(0)
	move     0, R(1)
	move     &ref[416], R(2)
	move     S(5), R(3)
	pl_call  pred_warning_2()
	pl_jump  fun189()


long local pool_fun193[4]=[65538,build_ref_442,build_ref_440,pool_fun190]

pl_code local fun194
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(6),V(4))
	fail_ret
fun193:
	call_c   Dyam_Choice(fun192)
	call_c   Dyam_Set_Cut()
	pl_call  Object_2(&ref[440],V(8))
	call_c   DYAM_evpred_retract(&ref[440])
	fail_ret
	call_c   Dyam_Choice(fun191)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(7),V(1))
	fail_ret
	call_c   Dyam_Cut()
	pl_fail


;; TERM 438: rcg(_F)
c_code local build_ref_438
	ret_reg &ref[438]
	call_c   build_ref_739()
	call_c   Dyam_Create_Unary(&ref[739],V(5))
	move_ret ref[438]
	c_ret

;; TERM 439: rcg(_E, _F)
c_code local build_ref_439
	ret_reg &ref[439]
	call_c   build_ref_739()
	call_c   Dyam_Create_Binary(&ref[739],V(4),V(5))
	move_ret ref[439]
	c_ret

long local pool_fun195[6]=[131075,build_ref_436,build_ref_438,build_ref_439,pool_fun193,pool_fun193]

pl_code local fun195
	call_c   Dyam_Pool(pool_fun195)
	call_c   Dyam_Unify_Item(&ref[436])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(2))
	move     V(4), R(2)
	move     S(5), R(3)
	pl_call  pred_spec_in_module_2()
	call_c   Dyam_Choice(fun194)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(3),&ref[438])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(6),&ref[439])
	fail_ret
	pl_jump  fun193()

;; TERM 437: '*GUARD*'(installer(mode(_B), _C, _D)) :> []
c_code local build_ref_437
	ret_reg &ref[437]
	call_c   build_ref_436()
	call_c   Dyam_Create_Binary(I(9),&ref[436],I(0))
	move_ret ref[437]
	c_ret

c_code local build_seed_101
	ret_reg &seed[101]
	call_c   build_ref_11()
	call_c   build_ref_430()
	call_c   Dyam_Seed_Start(&ref[11],&ref[430],I(0),fun0,1)
	call_c   build_ref_429()
	call_c   Dyam_Seed_Add_Comp(&ref[429],fun188,0)
	call_c   Dyam_Seed_End()
	move_ret seed[101]
	c_ret

;; TERM 429: '*GUARD*'(clause_type((_B --> _C), grammar_rule(_D, (_B --> _C)), _E, _F))
c_code local build_ref_429
	ret_reg &ref[429]
	call_c   build_ref_12()
	call_c   build_ref_428()
	call_c   Dyam_Create_Unary(&ref[12],&ref[428])
	move_ret ref[429]
	c_ret

;; TERM 428: clause_type((_B --> _C), grammar_rule(_D, (_B --> _C)), _E, _F)
c_code local build_ref_428
	ret_reg &ref[428]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_744()
	call_c   Dyam_Create_Binary(&ref[744],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_745()
	call_c   Dyam_Create_Binary(&ref[745],V(3),R(0))
	move_ret R(1)
	call_c   build_ref_677()
	call_c   Dyam_Term_Start(&ref[677],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[428]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 745: grammar_rule
c_code local build_ref_745
	ret_reg &ref[745]
	call_c   Dyam_Create_Atom("grammar_rule")
	move_ret ref[745]
	c_ret

;; TERM 744: -->
c_code local build_ref_744
	ret_reg &ref[744]
	call_c   Dyam_Create_Atom("-->")
	move_ret ref[744]
	c_ret

;; TERM 431: grammar_kind(_G)
c_code local build_ref_431
	ret_reg &ref[431]
	call_c   build_ref_746()
	call_c   Dyam_Create_Unary(&ref[746],V(6))
	move_ret ref[431]
	c_ret

;; TERM 746: grammar_kind
c_code local build_ref_746
	ret_reg &ref[746]
	call_c   Dyam_Create_Atom("grammar_kind")
	move_ret ref[746]
	c_ret

c_code local build_seed_129
	ret_reg &seed[129]
	call_c   build_ref_12()
	call_c   build_ref_433()
	call_c   Dyam_Seed_Start(&ref[12],&ref[433],I(0),fun13,1)
	call_c   build_ref_434()
	call_c   Dyam_Seed_Add_Comp(&ref[434],&ref[433],0)
	call_c   Dyam_Seed_End()
	move_ret seed[129]
	c_ret

;; TERM 434: '*GUARD*'(register_predicate(_D, _B, _F)) :> '$$HOLE$$'
c_code local build_ref_434
	ret_reg &ref[434]
	call_c   build_ref_433()
	call_c   Dyam_Create_Binary(I(9),&ref[433],I(7))
	move_ret ref[434]
	c_ret

;; TERM 433: '*GUARD*'(register_predicate(_D, _B, _F))
c_code local build_ref_433
	ret_reg &ref[433]
	call_c   build_ref_12()
	call_c   build_ref_432()
	call_c   Dyam_Create_Unary(&ref[12],&ref[432])
	move_ret ref[433]
	c_ret

;; TERM 432: register_predicate(_D, _B, _F)
c_code local build_ref_432
	ret_reg &ref[432]
	call_c   build_ref_716()
	call_c   Dyam_Term_Start(&ref[716],3)
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[432]
	c_ret

long local pool_fun188[4]=[3,build_ref_429,build_ref_431,build_seed_129]

pl_code local fun188
	call_c   Dyam_Pool(pool_fun188)
	call_c   Dyam_Unify_Item(&ref[429])
	fail_ret
	call_c   Dyam_Allocate(0)
	pl_call  Object_1(&ref[431])
	call_c   Dyam_Reg_Load_Ptr(2,V(6))
	fail_ret
	call_c   Dyam_Reg_Load(4,V(3))
	call_c   DyALog_Mutable_Read(R(2),&R(4))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_jump  fun25(&seed[129],1)

;; TERM 430: '*GUARD*'(clause_type((_B --> _C), grammar_rule(_D, (_B --> _C)), _E, _F)) :> []
c_code local build_ref_430
	ret_reg &ref[430]
	call_c   build_ref_429()
	call_c   Dyam_Create_Binary(I(9),&ref[429],I(0))
	move_ret ref[430]
	c_ret

c_code local build_seed_32
	ret_reg &seed[32]
	call_c   build_ref_11()
	call_c   build_ref_445()
	call_c   Dyam_Seed_Start(&ref[11],&ref[445],I(0),fun0,1)
	call_c   build_ref_444()
	call_c   Dyam_Seed_Add_Comp(&ref[444],fun200,0)
	call_c   Dyam_Seed_End()
	move_ret seed[32]
	c_ret

;; TERM 444: '*GUARD*'(installer(tag_node_mode(_B), _C, _D))
c_code local build_ref_444
	ret_reg &ref[444]
	call_c   build_ref_12()
	call_c   build_ref_443()
	call_c   Dyam_Create_Unary(&ref[12],&ref[443])
	move_ret ref[444]
	c_ret

;; TERM 443: installer(tag_node_mode(_B), _C, _D)
c_code local build_ref_443
	ret_reg &ref[443]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_742()
	call_c   Dyam_Create_Unary(&ref[742],V(1))
	move_ret R(0)
	call_c   build_ref_681()
	call_c   Dyam_Term_Start(&ref[681],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[443]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 449: tag_node_modulation(_C, _B)
c_code local build_ref_449
	ret_reg &ref[449]
	call_c   build_ref_747()
	call_c   Dyam_Create_Binary(&ref[747],V(2),V(1))
	move_ret ref[449]
	c_ret

;; TERM 747: tag_node_modulation
c_code local build_ref_747
	ret_reg &ref[747]
	call_c   Dyam_Create_Atom("tag_node_modulation")
	move_ret ref[747]
	c_ret

pl_code local fun199
	call_c   Dyam_Remove_Choice()
fun196:
	call_c   DYAM_evpred_assert_1(&ref[449])
	call_c   Dyam_Deallocate()
	pl_ret


;; TERM 446: tag_node_modulation(_C, _E)
c_code local build_ref_446
	ret_reg &ref[446]
	call_c   build_ref_747()
	call_c   Dyam_Create_Binary(&ref[747],V(2),V(4))
	move_ret ref[446]
	c_ret

;; TERM 447: 'new tag node pattern ~w for ~w replaces ~w'
c_code local build_ref_447
	ret_reg &ref[447]
	call_c   Dyam_Create_Atom("new tag node pattern ~w for ~w replaces ~w")
	move_ret ref[447]
	c_ret

;; TERM 448: [_B,_C,_E]
c_code local build_ref_448
	ret_reg &ref[448]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(4),I(0))
	move_ret R(0)
	call_c   Dyam_Create_Tupple(1,2,R(0))
	move_ret ref[448]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun197[4]=[3,build_ref_447,build_ref_448,build_ref_449]

pl_code local fun198
	call_c   Dyam_Remove_Choice()
fun197:
	call_c   Dyam_Cut()
	move     &ref[447], R(0)
	move     0, R(1)
	move     &ref[448], R(2)
	move     S(5), R(3)
	pl_call  pred_warning_2()
	pl_jump  fun196()


long local pool_fun200[5]=[65539,build_ref_444,build_ref_449,build_ref_446,pool_fun197]

pl_code local fun200
	call_c   Dyam_Pool(pool_fun200)
	call_c   Dyam_Unify_Item(&ref[444])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun199)
	call_c   Dyam_Set_Cut()
	pl_call  Object_2(&ref[446],V(5))
	call_c   DYAM_evpred_retract(&ref[446])
	fail_ret
	call_c   Dyam_Choice(fun198)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(4),V(1))
	fail_ret
	call_c   Dyam_Cut()
	pl_fail

;; TERM 445: '*GUARD*'(installer(tag_node_mode(_B), _C, _D)) :> []
c_code local build_ref_445
	ret_reg &ref[445]
	call_c   build_ref_444()
	call_c   Dyam_Create_Binary(I(9),&ref[444],I(0))
	move_ret ref[445]
	c_ret

c_code local build_seed_54
	ret_reg &seed[54]
	call_c   build_ref_11()
	call_c   build_ref_452()
	call_c   Dyam_Seed_Start(&ref[11],&ref[452],I(0),fun0,1)
	call_c   build_ref_451()
	call_c   Dyam_Seed_Add_Comp(&ref[451],fun201,0)
	call_c   Dyam_Seed_End()
	move_ret seed[54]
	c_ret

;; TERM 451: '*GUARD*'(directive(skipper(_B), _C, _D, _E))
c_code local build_ref_451
	ret_reg &ref[451]
	call_c   build_ref_12()
	call_c   build_ref_450()
	call_c   Dyam_Create_Unary(&ref[12],&ref[450])
	move_ret ref[451]
	c_ret

;; TERM 450: directive(skipper(_B), _C, _D, _E)
c_code local build_ref_450
	ret_reg &ref[450]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_748()
	call_c   Dyam_Create_Unary(&ref[748],V(1))
	move_ret R(0)
	call_c   build_ref_682()
	call_c   Dyam_Term_Start(&ref[682],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[450]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 748: skipper
c_code local build_ref_748
	ret_reg &ref[748]
	call_c   Dyam_Create_Atom("skipper")
	move_ret ref[748]
	c_ret

;; TERM 453: [_B,_G,_H,_I]
c_code local build_ref_453
	ret_reg &ref[453]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Tupple(6,8,I(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(1),R(0))
	move_ret ref[453]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 454: skipper(_J, _K, _L, _M)
c_code local build_ref_454
	ret_reg &ref[454]
	call_c   build_ref_748()
	call_c   Dyam_Term_Start(&ref[748],4)
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_End()
	move_ret ref[454]
	c_ret

;; TERM 455: skipper(_G, _H, _I, _F)
c_code local build_ref_455
	ret_reg &ref[455]
	call_c   build_ref_748()
	call_c   Dyam_Term_Start(&ref[748],4)
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[455]
	c_ret

long local pool_fun201[5]=[4,build_ref_451,build_ref_453,build_ref_454,build_ref_455]

pl_code local fun201
	call_c   Dyam_Pool(pool_fun201)
	call_c   Dyam_Unify_Item(&ref[451])
	fail_ret
	call_c   DYAM_evpred_univ(V(5),&ref[453])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[454], R(0)
	move     S(5), R(1)
	move     &ref[455], R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_directive_updater_2()

;; TERM 452: '*GUARD*'(directive(skipper(_B), _C, _D, _E)) :> []
c_code local build_ref_452
	ret_reg &ref[452]
	call_c   build_ref_451()
	call_c   Dyam_Create_Binary(I(9),&ref[451],I(0))
	move_ret ref[452]
	c_ret

c_code local build_seed_55
	ret_reg &seed[55]
	call_c   build_ref_11()
	call_c   build_ref_458()
	call_c   Dyam_Seed_Start(&ref[11],&ref[458],I(0),fun0,1)
	call_c   build_ref_457()
	call_c   Dyam_Seed_Add_Comp(&ref[457],fun202,0)
	call_c   Dyam_Seed_End()
	move_ret seed[55]
	c_ret

;; TERM 457: '*GUARD*'(directive(scanner(_B), _C, _D, _E))
c_code local build_ref_457
	ret_reg &ref[457]
	call_c   build_ref_12()
	call_c   build_ref_456()
	call_c   Dyam_Create_Unary(&ref[12],&ref[456])
	move_ret ref[457]
	c_ret

;; TERM 456: directive(scanner(_B), _C, _D, _E)
c_code local build_ref_456
	ret_reg &ref[456]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_749()
	call_c   Dyam_Create_Unary(&ref[749],V(1))
	move_ret R(0)
	call_c   build_ref_682()
	call_c   Dyam_Term_Start(&ref[682],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[456]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 749: scanner
c_code local build_ref_749
	ret_reg &ref[749]
	call_c   Dyam_Create_Atom("scanner")
	move_ret ref[749]
	c_ret

;; TERM 459: scanner(_J, _K, _L, _M)
c_code local build_ref_459
	ret_reg &ref[459]
	call_c   build_ref_749()
	call_c   Dyam_Term_Start(&ref[749],4)
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_End()
	move_ret ref[459]
	c_ret

;; TERM 460: scanner(_G, _H, _I, _F)
c_code local build_ref_460
	ret_reg &ref[460]
	call_c   build_ref_749()
	call_c   Dyam_Term_Start(&ref[749],4)
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[460]
	c_ret

long local pool_fun202[5]=[4,build_ref_457,build_ref_453,build_ref_459,build_ref_460]

pl_code local fun202
	call_c   Dyam_Pool(pool_fun202)
	call_c   Dyam_Unify_Item(&ref[457])
	fail_ret
	call_c   DYAM_evpred_univ(V(5),&ref[453])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[459], R(0)
	move     S(5), R(1)
	move     &ref[460], R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_directive_updater_2()

;; TERM 458: '*GUARD*'(directive(scanner(_B), _C, _D, _E)) :> []
c_code local build_ref_458
	ret_reg &ref[458]
	call_c   build_ref_457()
	call_c   Dyam_Create_Binary(I(9),&ref[457],I(0))
	move_ret ref[458]
	c_ret

c_code local build_seed_100
	ret_reg &seed[100]
	call_c   build_ref_11()
	call_c   build_ref_463()
	call_c   Dyam_Seed_Start(&ref[11],&ref[463],I(0),fun0,1)
	call_c   build_ref_462()
	call_c   Dyam_Seed_Add_Comp(&ref[462],fun204,0)
	call_c   Dyam_Seed_End()
	move_ret seed[100]
	c_ret

;; TERM 462: '*GUARD*'(clause_type('$loader'(_B, _C), '$loader'(_B, _D), _E, _F))
c_code local build_ref_462
	ret_reg &ref[462]
	call_c   build_ref_12()
	call_c   build_ref_461()
	call_c   Dyam_Create_Unary(&ref[12],&ref[461])
	move_ret ref[462]
	c_ret

;; TERM 461: clause_type('$loader'(_B, _C), '$loader'(_B, _D), _E, _F)
c_code local build_ref_461
	ret_reg &ref[461]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_750()
	call_c   Dyam_Create_Binary(&ref[750],V(1),V(2))
	move_ret R(0)
	call_c   Dyam_Create_Binary(&ref[750],V(1),V(3))
	move_ret R(1)
	call_c   build_ref_677()
	call_c   Dyam_Term_Start(&ref[677],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[461]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 750: '$loader'
c_code local build_ref_750
	ret_reg &ref[750]
	call_c   Dyam_Create_Atom("$loader")
	move_ret ref[750]
	c_ret

c_code local build_seed_131
	ret_reg &seed[131]
	call_c   build_ref_12()
	call_c   build_ref_468()
	call_c   Dyam_Seed_Start(&ref[12],&ref[468],I(0),fun13,1)
	call_c   build_ref_469()
	call_c   Dyam_Seed_Add_Comp(&ref[469],&ref[468],0)
	call_c   Dyam_Seed_End()
	move_ret seed[131]
	c_ret

;; TERM 469: '*GUARD*'(register_predicate(fact, _C, _F)) :> '$$HOLE$$'
c_code local build_ref_469
	ret_reg &ref[469]
	call_c   build_ref_468()
	call_c   Dyam_Create_Binary(I(9),&ref[468],I(7))
	move_ret ref[469]
	c_ret

;; TERM 468: '*GUARD*'(register_predicate(fact, _C, _F))
c_code local build_ref_468
	ret_reg &ref[468]
	call_c   build_ref_12()
	call_c   build_ref_467()
	call_c   Dyam_Create_Unary(&ref[12],&ref[467])
	move_ret ref[468]
	c_ret

;; TERM 467: register_predicate(fact, _C, _F)
c_code local build_ref_467
	ret_reg &ref[467]
	call_c   build_ref_716()
	call_c   build_ref_740()
	call_c   Dyam_Term_Start(&ref[716],3)
	call_c   Dyam_Term_Arg(&ref[740])
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[467]
	c_ret

;; TERM 470: '$fact'(_C)
c_code local build_ref_470
	ret_reg &ref[470]
	call_c   build_ref_751()
	call_c   Dyam_Create_Unary(&ref[751],V(2))
	move_ret ref[470]
	c_ret

;; TERM 751: '$fact'
c_code local build_ref_751
	ret_reg &ref[751]
	call_c   Dyam_Create_Atom("$fact")
	move_ret ref[751]
	c_ret

long local pool_fun203[3]=[2,build_seed_131,build_ref_470]

pl_code local fun203
	call_c   Dyam_Remove_Choice()
	pl_call  fun25(&seed[131],1)
	call_c   Dyam_Unify(V(3),&ref[470])
	fail_ret
	pl_jump  fun127()

c_code local build_seed_130
	ret_reg &seed[130]
	call_c   build_ref_12()
	call_c   build_ref_465()
	call_c   Dyam_Seed_Start(&ref[12],&ref[465],I(0),fun13,1)
	call_c   build_ref_466()
	call_c   Dyam_Seed_Add_Comp(&ref[466],&ref[465],0)
	call_c   Dyam_Seed_End()
	move_ret seed[130]
	c_ret

;; TERM 466: '*GUARD*'(clause_type(_C, _D, _E, _F)) :> '$$HOLE$$'
c_code local build_ref_466
	ret_reg &ref[466]
	call_c   build_ref_465()
	call_c   Dyam_Create_Binary(I(9),&ref[465],I(7))
	move_ret ref[466]
	c_ret

;; TERM 465: '*GUARD*'(clause_type(_C, _D, _E, _F))
c_code local build_ref_465
	ret_reg &ref[465]
	call_c   build_ref_12()
	call_c   build_ref_464()
	call_c   Dyam_Create_Unary(&ref[12],&ref[464])
	move_ret ref[465]
	c_ret

;; TERM 464: clause_type(_C, _D, _E, _F)
c_code local build_ref_464
	ret_reg &ref[464]
	call_c   build_ref_677()
	call_c   Dyam_Term_Start(&ref[677],4)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[464]
	c_ret

long local pool_fun204[4]=[65538,build_ref_462,build_seed_130,pool_fun203]

pl_code local fun204
	call_c   Dyam_Pool(pool_fun204)
	call_c   Dyam_Unify_Item(&ref[462])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun203)
	call_c   Dyam_Set_Cut()
	pl_call  fun25(&seed[130],1)
	call_c   Dyam_Cut()
	pl_jump  fun127()

;; TERM 463: '*GUARD*'(clause_type('$loader'(_B, _C), '$loader'(_B, _D), _E, _F)) :> []
c_code local build_ref_463
	ret_reg &ref[463]
	call_c   build_ref_462()
	call_c   Dyam_Create_Binary(I(9),&ref[462],I(0))
	move_ret ref[463]
	c_ret

c_code local build_seed_76
	ret_reg &seed[76]
	call_c   build_ref_11()
	call_c   build_ref_479()
	call_c   Dyam_Seed_Start(&ref[11],&ref[479],I(0),fun0,1)
	call_c   build_ref_478()
	call_c   Dyam_Seed_Add_Comp(&ref[478],fun211,0)
	call_c   Dyam_Seed_End()
	move_ret seed[76]
	c_ret

;; TERM 478: '*GUARD*'(directive(mode(_B, _C), _D, _E, _F))
c_code local build_ref_478
	ret_reg &ref[478]
	call_c   build_ref_12()
	call_c   build_ref_477()
	call_c   Dyam_Create_Unary(&ref[12],&ref[477])
	move_ret ref[478]
	c_ret

;; TERM 477: directive(mode(_B, _C), _D, _E, _F)
c_code local build_ref_477
	ret_reg &ref[477]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_743()
	call_c   Dyam_Create_Binary(&ref[743],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_682()
	call_c   Dyam_Term_Start(&ref[682],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[477]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 484: [mode(_B, _C)]
c_code local build_ref_484
	ret_reg &ref[484]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_743()
	call_c   Dyam_Create_Binary(&ref[743],V(1),V(2))
	move_ret R(0)
	call_c   Dyam_Create_List(R(0),I(0))
	move_ret ref[484]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 483: mode(_G)
c_code local build_ref_483
	ret_reg &ref[483]
	call_c   build_ref_743()
	call_c   Dyam_Create_Unary(&ref[743],V(6))
	move_ret ref[483]
	c_ret

long local pool_fun210[4]=[3,build_ref_426,build_ref_484,build_ref_483]

pl_code local fun210
	call_c   Dyam_Remove_Choice()
	move     &ref[426], R(0)
	move     0, R(1)
	move     &ref[484], R(2)
	move     S(5), R(3)
	pl_call  pred_error_2()
fun209:
	move     &ref[483], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(1))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_install_2()


c_code local build_seed_133
	ret_reg &seed[133]
	call_c   build_ref_12()
	call_c   build_ref_481()
	call_c   Dyam_Seed_Start(&ref[12],&ref[481],I(0),fun13,1)
	call_c   build_ref_482()
	call_c   Dyam_Seed_Add_Comp(&ref[482],&ref[481],0)
	call_c   Dyam_Seed_End()
	move_ret seed[133]
	c_ret

;; TERM 482: '*GUARD*'(check_mode(_G)) :> '$$HOLE$$'
c_code local build_ref_482
	ret_reg &ref[482]
	call_c   build_ref_481()
	call_c   Dyam_Create_Binary(I(9),&ref[481],I(7))
	move_ret ref[482]
	c_ret

;; TERM 481: '*GUARD*'(check_mode(_G))
c_code local build_ref_481
	ret_reg &ref[481]
	call_c   build_ref_12()
	call_c   build_ref_480()
	call_c   Dyam_Create_Unary(&ref[12],&ref[480])
	move_ret ref[481]
	c_ret

;; TERM 480: check_mode(_G)
c_code local build_ref_480
	ret_reg &ref[480]
	call_c   build_ref_673()
	call_c   Dyam_Create_Unary(&ref[673],V(6))
	move_ret ref[480]
	c_ret

long local pool_fun211[5]=[65539,build_ref_478,build_seed_133,build_ref_483,pool_fun210]

pl_code local fun211
	call_c   Dyam_Pool(pool_fun211)
	call_c   Dyam_Unify_Item(&ref[478])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun210)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Reg_Load(0,V(2))
	move     V(6), R(2)
	move     S(5), R(3)
	pl_call  pred_normalize_mode_2()
	pl_call  fun25(&seed[133],1)
	call_c   Dyam_Cut()
	pl_jump  fun209()

;; TERM 479: '*GUARD*'(directive(mode(_B, _C), _D, _E, _F)) :> []
c_code local build_ref_479
	ret_reg &ref[479]
	call_c   build_ref_478()
	call_c   Dyam_Create_Binary(I(9),&ref[478],I(0))
	move_ret ref[479]
	c_ret

c_code local build_seed_37
	ret_reg &seed[37]
	call_c   build_ref_11()
	call_c   build_ref_487()
	call_c   Dyam_Seed_Start(&ref[11],&ref[487],I(0),fun0,1)
	call_c   build_ref_486()
	call_c   Dyam_Seed_Add_Comp(&ref[486],fun216,0)
	call_c   Dyam_Seed_End()
	move_ret seed[37]
	c_ret

;; TERM 486: '*GUARD*'(installer(subset(_B), (_C / _D), _E))
c_code local build_ref_486
	ret_reg &ref[486]
	call_c   build_ref_12()
	call_c   build_ref_485()
	call_c   Dyam_Create_Unary(&ref[12],&ref[485])
	move_ret ref[486]
	c_ret

;; TERM 485: installer(subset(_B), (_C / _D), _E)
c_code local build_ref_485
	ret_reg &ref[485]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_711()
	call_c   Dyam_Create_Unary(&ref[711],V(1))
	move_ret R(0)
	call_c   build_ref_692()
	call_c   Dyam_Create_Binary(&ref[692],V(2),V(3))
	move_ret R(1)
	call_c   build_ref_681()
	call_c   Dyam_Term_Start(&ref[681],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[485]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 491: subset(_C, _B)
c_code local build_ref_491
	ret_reg &ref[491]
	call_c   build_ref_711()
	call_c   Dyam_Create_Binary(&ref[711],V(2),V(1))
	move_ret ref[491]
	c_ret

pl_code local fun215
	call_c   Dyam_Remove_Choice()
fun212:
	call_c   DYAM_evpred_assert_1(&ref[491])
	call_c   DYAM_Subset_2(V(2),V(1))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret


;; TERM 488: subset(_C, _F)
c_code local build_ref_488
	ret_reg &ref[488]
	call_c   build_ref_711()
	call_c   Dyam_Create_Binary(&ref[711],V(2),V(5))
	move_ret ref[488]
	c_ret

;; TERM 489: 'new subset ~w for ~w replaces ~w'
c_code local build_ref_489
	ret_reg &ref[489]
	call_c   Dyam_Create_Atom("new subset ~w for ~w replaces ~w")
	move_ret ref[489]
	c_ret

;; TERM 490: [_B,_C,_F]
c_code local build_ref_490
	ret_reg &ref[490]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(5),I(0))
	move_ret R(0)
	call_c   Dyam_Create_Tupple(1,2,R(0))
	move_ret ref[490]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun213[4]=[3,build_ref_489,build_ref_490,build_ref_491]

pl_code local fun214
	call_c   Dyam_Remove_Choice()
fun213:
	call_c   Dyam_Cut()
	move     &ref[489], R(0)
	move     0, R(1)
	move     &ref[490], R(2)
	move     S(5), R(3)
	pl_call  pred_warning_2()
	pl_jump  fun212()


long local pool_fun216[5]=[65539,build_ref_486,build_ref_491,build_ref_488,pool_fun213]

pl_code local fun216
	call_c   Dyam_Pool(pool_fun216)
	call_c   Dyam_Unify_Item(&ref[486])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun215)
	call_c   Dyam_Set_Cut()
	pl_call  Object_2(&ref[488],V(6))
	call_c   DYAM_evpred_retract(&ref[488])
	fail_ret
	call_c   Dyam_Choice(fun214)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(5),V(1))
	fail_ret
	call_c   Dyam_Cut()
	pl_fail

;; TERM 487: '*GUARD*'(installer(subset(_B), (_C / _D), _E)) :> []
c_code local build_ref_487
	ret_reg &ref[487]
	call_c   build_ref_486()
	call_c   Dyam_Create_Binary(I(9),&ref[486],I(0))
	move_ret ref[487]
	c_ret

c_code local build_seed_38
	ret_reg &seed[38]
	call_c   build_ref_11()
	call_c   build_ref_494()
	call_c   Dyam_Seed_Start(&ref[11],&ref[494],I(0),fun0,1)
	call_c   build_ref_493()
	call_c   Dyam_Seed_Add_Comp(&ref[493],fun221,0)
	call_c   Dyam_Seed_End()
	move_ret seed[38]
	c_ret

;; TERM 493: '*GUARD*'(installer(finite_set(_B), (_C / _D), _E))
c_code local build_ref_493
	ret_reg &ref[493]
	call_c   build_ref_12()
	call_c   build_ref_492()
	call_c   Dyam_Create_Unary(&ref[12],&ref[492])
	move_ret ref[493]
	c_ret

;; TERM 492: installer(finite_set(_B), (_C / _D), _E)
c_code local build_ref_492
	ret_reg &ref[492]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_733()
	call_c   Dyam_Create_Unary(&ref[733],V(1))
	move_ret R(0)
	call_c   build_ref_692()
	call_c   Dyam_Create_Binary(&ref[692],V(2),V(3))
	move_ret R(1)
	call_c   build_ref_681()
	call_c   Dyam_Term_Start(&ref[681],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[492]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 497: finite_set(_C, _B)
c_code local build_ref_497
	ret_reg &ref[497]
	call_c   build_ref_733()
	call_c   Dyam_Create_Binary(&ref[733],V(2),V(1))
	move_ret ref[497]
	c_ret

pl_code local fun220
	call_c   Dyam_Remove_Choice()
fun217:
	call_c   DYAM_evpred_assert_1(&ref[497])
	call_c   DYAM_Finite_Set_2(V(2),V(1))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret


;; TERM 495: finite_set(_C, _F)
c_code local build_ref_495
	ret_reg &ref[495]
	call_c   build_ref_733()
	call_c   Dyam_Create_Binary(&ref[733],V(2),V(5))
	move_ret ref[495]
	c_ret

;; TERM 496: 'new element set ~w for ~w replaces ~w'
c_code local build_ref_496
	ret_reg &ref[496]
	call_c   Dyam_Create_Atom("new element set ~w for ~w replaces ~w")
	move_ret ref[496]
	c_ret

long local pool_fun218[4]=[3,build_ref_496,build_ref_490,build_ref_497]

pl_code local fun219
	call_c   Dyam_Remove_Choice()
fun218:
	call_c   Dyam_Cut()
	move     &ref[496], R(0)
	move     0, R(1)
	move     &ref[490], R(2)
	move     S(5), R(3)
	pl_call  pred_warning_2()
	pl_jump  fun217()


long local pool_fun221[5]=[65539,build_ref_493,build_ref_497,build_ref_495,pool_fun218]

pl_code local fun221
	call_c   Dyam_Pool(pool_fun221)
	call_c   Dyam_Unify_Item(&ref[493])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun220)
	call_c   Dyam_Set_Cut()
	pl_call  Object_2(&ref[495],V(6))
	call_c   DYAM_evpred_retract(&ref[495])
	fail_ret
	call_c   Dyam_Choice(fun219)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(5),V(1))
	fail_ret
	call_c   Dyam_Cut()
	pl_fail

;; TERM 494: '*GUARD*'(installer(finite_set(_B), (_C / _D), _E)) :> []
c_code local build_ref_494
	ret_reg &ref[494]
	call_c   build_ref_493()
	call_c   Dyam_Create_Binary(I(9),&ref[493],I(0))
	move_ret ref[494]
	c_ret

c_code local build_seed_39
	ret_reg &seed[39]
	call_c   build_ref_11()
	call_c   build_ref_500()
	call_c   Dyam_Seed_Start(&ref[11],&ref[500],I(0),fun0,1)
	call_c   build_ref_499()
	call_c   Dyam_Seed_Add_Comp(&ref[499],fun226,0)
	call_c   Dyam_Seed_End()
	move_ret seed[39]
	c_ret

;; TERM 499: '*GUARD*'(installer(feature_intro(_B), (_C / _D), _E))
c_code local build_ref_499
	ret_reg &ref[499]
	call_c   build_ref_12()
	call_c   build_ref_498()
	call_c   Dyam_Create_Unary(&ref[12],&ref[498])
	move_ret ref[499]
	c_ret

;; TERM 498: installer(feature_intro(_B), (_C / _D), _E)
c_code local build_ref_498
	ret_reg &ref[498]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_712()
	call_c   Dyam_Create_Unary(&ref[712],V(1))
	move_ret R(0)
	call_c   build_ref_692()
	call_c   Dyam_Create_Binary(&ref[692],V(2),V(3))
	move_ret R(1)
	call_c   build_ref_681()
	call_c   Dyam_Term_Start(&ref[681],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[498]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 503: feature_intro(_C, _B)
c_code local build_ref_503
	ret_reg &ref[503]
	call_c   build_ref_712()
	call_c   Dyam_Create_Binary(&ref[712],V(2),V(1))
	move_ret ref[503]
	c_ret

;; TERM 504: '$ft'
c_code local build_ref_504
	ret_reg &ref[504]
	call_c   Dyam_Create_Atom("$ft")
	move_ret ref[504]
	c_ret

long local pool_fun222[3]=[2,build_ref_503,build_ref_504]

pl_code local fun225
	call_c   Dyam_Remove_Choice()
fun222:
	call_c   DYAM_evpred_assert_1(&ref[503])
	call_c   DYAM_evpred_atom_to_module(V(6),V(1),&ref[504])
	fail_ret
	call_c   DYAM_Intro_2(V(2),V(6))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret


;; TERM 501: feature_intro(_C, _F)
c_code local build_ref_501
	ret_reg &ref[501]
	call_c   build_ref_712()
	call_c   Dyam_Create_Binary(&ref[712],V(2),V(5))
	move_ret ref[501]
	c_ret

;; TERM 502: 'new type ~w introduced by ~w replaces ~w'
c_code local build_ref_502
	ret_reg &ref[502]
	call_c   Dyam_Create_Atom("new type ~w introduced by ~w replaces ~w")
	move_ret ref[502]
	c_ret

long local pool_fun223[4]=[65538,build_ref_502,build_ref_490,pool_fun222]

pl_code local fun224
	call_c   Dyam_Remove_Choice()
fun223:
	call_c   Dyam_Cut()
	move     &ref[502], R(0)
	move     0, R(1)
	move     &ref[490], R(2)
	move     S(5), R(3)
	pl_call  pred_warning_2()
	pl_jump  fun222()


long local pool_fun226[5]=[131074,build_ref_499,build_ref_501,pool_fun222,pool_fun223]

pl_code local fun226
	call_c   Dyam_Pool(pool_fun226)
	call_c   Dyam_Unify_Item(&ref[499])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun225)
	call_c   Dyam_Set_Cut()
	pl_call  Object_2(&ref[501],V(7))
	call_c   DYAM_evpred_retract(&ref[501])
	fail_ret
	call_c   Dyam_Choice(fun224)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(5),V(1))
	fail_ret
	call_c   Dyam_Cut()
	pl_fail

;; TERM 500: '*GUARD*'(installer(feature_intro(_B), (_C / _D), _E)) :> []
c_code local build_ref_500
	ret_reg &ref[500]
	call_c   build_ref_499()
	call_c   Dyam_Create_Binary(I(9),&ref[499],I(0))
	move_ret ref[500]
	c_ret

c_code local build_seed_40
	ret_reg &seed[40]
	call_c   build_ref_11()
	call_c   build_ref_507()
	call_c   Dyam_Seed_Start(&ref[11],&ref[507],I(0),fun0,1)
	call_c   build_ref_506()
	call_c   Dyam_Seed_Add_Comp(&ref[506],fun231,0)
	call_c   Dyam_Seed_End()
	move_ret seed[40]
	c_ret

;; TERM 506: '*GUARD*'(installer(features(_B), (_C / _D), _E))
c_code local build_ref_506
	ret_reg &ref[506]
	call_c   build_ref_12()
	call_c   build_ref_505()
	call_c   Dyam_Create_Unary(&ref[12],&ref[505])
	move_ret ref[506]
	c_ret

;; TERM 505: installer(features(_B), (_C / _D), _E)
c_code local build_ref_505
	ret_reg &ref[505]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_713()
	call_c   Dyam_Create_Unary(&ref[713],V(1))
	move_ret R(0)
	call_c   build_ref_692()
	call_c   Dyam_Create_Binary(&ref[692],V(2),V(3))
	move_ret R(1)
	call_c   build_ref_681()
	call_c   Dyam_Term_Start(&ref[681],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[505]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 510: features(_C, _B)
c_code local build_ref_510
	ret_reg &ref[510]
	call_c   build_ref_713()
	call_c   Dyam_Create_Binary(&ref[713],V(2),V(1))
	move_ret ref[510]
	c_ret

pl_code local fun230
	call_c   Dyam_Remove_Choice()
fun227:
	call_c   DYAM_evpred_assert_1(&ref[510])
	call_c   DYAM_Feature_2(V(2),V(1))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret


;; TERM 508: features(_C, _F)
c_code local build_ref_508
	ret_reg &ref[508]
	call_c   build_ref_713()
	call_c   Dyam_Create_Binary(&ref[713],V(2),V(5))
	move_ret ref[508]
	c_ret

;; TERM 509: 'new feature set ~w for ~w replaces ~w'
c_code local build_ref_509
	ret_reg &ref[509]
	call_c   Dyam_Create_Atom("new feature set ~w for ~w replaces ~w")
	move_ret ref[509]
	c_ret

long local pool_fun228[4]=[3,build_ref_509,build_ref_490,build_ref_510]

pl_code local fun229
	call_c   Dyam_Remove_Choice()
fun228:
	call_c   Dyam_Cut()
	move     &ref[509], R(0)
	move     0, R(1)
	move     &ref[490], R(2)
	move     S(5), R(3)
	pl_call  pred_warning_2()
	pl_jump  fun227()


long local pool_fun231[5]=[65539,build_ref_506,build_ref_510,build_ref_508,pool_fun228]

pl_code local fun231
	call_c   Dyam_Pool(pool_fun231)
	call_c   Dyam_Unify_Item(&ref[506])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun230)
	call_c   Dyam_Set_Cut()
	pl_call  Object_2(&ref[508],V(6))
	call_c   DYAM_evpred_retract(&ref[508])
	fail_ret
	call_c   Dyam_Choice(fun229)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(5),V(1))
	fail_ret
	call_c   Dyam_Cut()
	pl_fail

;; TERM 507: '*GUARD*'(installer(features(_B), (_C / _D), _E)) :> []
c_code local build_ref_507
	ret_reg &ref[507]
	call_c   build_ref_506()
	call_c   Dyam_Create_Binary(I(9),&ref[506],I(0))
	move_ret ref[507]
	c_ret

c_code local build_seed_27
	ret_reg &seed[27]
	call_c   build_ref_11()
	call_c   build_ref_513()
	call_c   Dyam_Seed_Start(&ref[11],&ref[513],I(0),fun0,1)
	call_c   build_ref_512()
	call_c   Dyam_Seed_Add_Comp(&ref[512],fun232,0)
	call_c   Dyam_Seed_End()
	move_ret seed[27]
	c_ret

;; TERM 512: '*GUARD*'(installer(bmg_stack, (_B / _C), _D))
c_code local build_ref_512
	ret_reg &ref[512]
	call_c   build_ref_12()
	call_c   build_ref_511()
	call_c   Dyam_Create_Unary(&ref[12],&ref[511])
	move_ret ref[512]
	c_ret

;; TERM 511: installer(bmg_stack, (_B / _C), _D)
c_code local build_ref_511
	ret_reg &ref[511]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_692()
	call_c   Dyam_Create_Binary(&ref[692],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_681()
	call_c   build_ref_358()
	call_c   Dyam_Term_Start(&ref[681],3)
	call_c   Dyam_Term_Arg(&ref[358])
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[511]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 514: bmg_stack(_B)
c_code local build_ref_514
	ret_reg &ref[514]
	call_c   build_ref_358()
	call_c   Dyam_Create_Unary(&ref[358],V(1))
	move_ret ref[514]
	c_ret

;; TERM 515: xfx
c_code local build_ref_515
	ret_reg &ref[515]
	call_c   Dyam_Create_Atom("xfx")
	move_ret ref[515]
	c_ret

;; TERM 516: 'isl_~w'
c_code local build_ref_516
	ret_reg &ref[516]
	call_c   Dyam_Create_Atom("isl_~w")
	move_ret ref[516]
	c_ret

;; TERM 517: isl
c_code local build_ref_517
	ret_reg &ref[517]
	call_c   Dyam_Create_Atom("isl")
	move_ret ref[517]
	c_ret

long local pool_fun232[7]=[6,build_ref_512,build_ref_514,build_ref_515,build_ref_516,build_ref_517,build_ref_110]

pl_code local fun232
	call_c   Dyam_Pool(pool_fun232)
	call_c   Dyam_Unify_Item(&ref[512])
	fail_ret
	call_c   DYAM_evpred_assert_1(&ref[514])
	call_c   DYAM_Op_3(N(300),&ref[515],V(1))
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[516], R(0)
	move     0, R(1)
	move     &ref[110], R(2)
	move     S(5), R(3)
	move     V(4), R(4)
	move     S(5), R(5)
	pl_call  pred_name_builder_3()
	call_c   Dyam_Reg_Load(0,V(4))
	move     &ref[110], R(2)
	move     S(5), R(3)
	pl_call  pred_new_bmg_island_2()
	move     &ref[517], R(0)
	move     0, R(1)
	move     &ref[110], R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_new_bmg_island_2()

;; TERM 513: '*GUARD*'(installer(bmg_stack, (_B / _C), _D)) :> []
c_code local build_ref_513
	ret_reg &ref[513]
	call_c   build_ref_512()
	call_c   Dyam_Create_Binary(I(9),&ref[512],I(0))
	move_ret ref[513]
	c_ret

c_code local build_seed_49
	ret_reg &seed[49]
	call_c   build_ref_11()
	call_c   build_ref_520()
	call_c   Dyam_Seed_Start(&ref[11],&ref[520],I(0),fun0,1)
	call_c   build_ref_519()
	call_c   Dyam_Seed_Add_Comp(&ref[519],fun243,0)
	call_c   Dyam_Seed_End()
	move_ret seed[49]
	c_ret

;; TERM 519: '*GUARD*'(directive(import{module=> _B, file=> _C, preds=> _D}, _E, _F, _G))
c_code local build_ref_519
	ret_reg &ref[519]
	call_c   build_ref_12()
	call_c   build_ref_518()
	call_c   Dyam_Create_Unary(&ref[12],&ref[518])
	move_ret ref[519]
	c_ret

;; TERM 518: directive(import{module=> _B, file=> _C, preds=> _D}, _E, _F, _G)
c_code local build_ref_518
	ret_reg &ref[518]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_752()
	call_c   Dyam_Term_Start(&ref[752],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_682()
	call_c   Dyam_Term_Start(&ref[682],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[518]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 752: import!'$ft'
c_code local build_ref_752
	ret_reg &ref[752]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_754()
	call_c   Dyam_Create_List(&ref[754],I(0))
	move_ret R(0)
	call_c   build_ref_753()
	call_c   Dyam_Create_List(&ref[753],R(0))
	move_ret R(0)
	call_c   build_ref_724()
	call_c   Dyam_Create_List(&ref[724],R(0))
	move_ret R(0)
	call_c   build_ref_689()
	call_c   Dyam_Create_Atom_Module("$ft",&ref[689])
	move_ret ref[752]
	call_c   DYAM_Feature_2(&ref[689],R(0))
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 753: file
c_code local build_ref_753
	ret_reg &ref[753]
	call_c   Dyam_Create_Atom("file")
	move_ret ref[753]
	c_ret

;; TERM 754: preds
c_code local build_ref_754
	ret_reg &ref[754]
	call_c   Dyam_Create_Atom("preds")
	move_ret ref[754]
	c_ret

;; TERM 529: 'Expected module name in ~w'
c_code local build_ref_529
	ret_reg &ref[529]
	call_c   Dyam_Create_Atom("Expected module name in ~w")
	move_ret ref[529]
	c_ret

;; TERM 530: [import{module=> _B, file=> _C, preds=> _D}]
c_code local build_ref_530
	ret_reg &ref[530]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_752()
	call_c   Dyam_Term_Start(&ref[752],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   Dyam_Create_List(R(0),I(0))
	move_ret ref[530]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 528: '~w.pl'
c_code local build_ref_528
	ret_reg &ref[528]
	call_c   Dyam_Create_Atom("~w.pl")
	move_ret ref[528]
	c_ret

c_code local build_seed_134
	ret_reg &seed[134]
	call_c   build_ref_12()
	call_c   build_ref_522()
	call_c   Dyam_Seed_Start(&ref[12],&ref[522],I(0),fun13,1)
	call_c   build_ref_523()
	call_c   Dyam_Seed_Add_Comp(&ref[523],&ref[522],0)
	call_c   Dyam_Seed_End()
	move_ret seed[134]
	c_ret

;; TERM 523: '*GUARD*'(directive((require _C), _E, _F, _H)) :> '$$HOLE$$'
c_code local build_ref_523
	ret_reg &ref[523]
	call_c   build_ref_522()
	call_c   Dyam_Create_Binary(I(9),&ref[522],I(7))
	move_ret ref[523]
	c_ret

;; TERM 522: '*GUARD*'(directive((require _C), _E, _F, _H))
c_code local build_ref_522
	ret_reg &ref[522]
	call_c   build_ref_12()
	call_c   build_ref_521()
	call_c   Dyam_Create_Unary(&ref[12],&ref[521])
	move_ret ref[522]
	c_ret

;; TERM 521: directive((require _C), _E, _F, _H)
c_code local build_ref_521
	ret_reg &ref[521]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_100()
	call_c   Dyam_Create_Unary(&ref[100],V(2))
	move_ret R(0)
	call_c   build_ref_682()
	call_c   Dyam_Term_Start(&ref[682],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[521]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 526: 'File ~w not defining module ~w'
c_code local build_ref_526
	ret_reg &ref[526]
	call_c   Dyam_Create_Atom("File ~w not defining module ~w")
	move_ret ref[526]
	c_ret

;; TERM 527: [_C,_B]
c_code local build_ref_527
	ret_reg &ref[527]
	call_c   build_ref_110()
	call_c   Dyam_Create_List(V(2),&ref[110])
	move_ret ref[527]
	c_ret

long local pool_fun234[4]=[3,build_ref_526,build_ref_527,build_ref_525]

pl_code local fun234
	call_c   Dyam_Remove_Choice()
	move     &ref[526], R(0)
	move     0, R(1)
	move     &ref[527], R(2)
	move     S(5), R(3)
	pl_call  pred_warning_2()
fun233:
	move     &ref[525], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(3))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_install_2()


long local pool_fun235[3]=[65537,build_ref_525,pool_fun234]

pl_code local fun235
	call_c   Dyam_Update_Choice(fun234)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(1),I(0))
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun233()

;; TERM 524: map_module(_B, _I, _C)
c_code local build_ref_524
	ret_reg &ref[524]
	call_c   build_ref_736()
	call_c   Dyam_Term_Start(&ref[736],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_End()
	move_ret ref[524]
	c_ret

long local pool_fun236[5]=[65539,build_seed_134,build_ref_524,build_ref_525,pool_fun235]

pl_code local fun237
	call_c   Dyam_Remove_Choice()
fun236:
	pl_call  fun25(&seed[134],1)
	call_c   Dyam_Choice(fun235)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[524])
	call_c   Dyam_Cut()
	pl_jump  fun233()


long local pool_fun238[3]=[131072,pool_fun236,pool_fun236]

long local pool_fun239[4]=[65538,build_ref_528,build_ref_110,pool_fun238]

pl_code local fun239
	call_c   Dyam_Remove_Choice()
	move     &ref[528], R(0)
	move     0, R(1)
	move     &ref[110], R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Load(4,V(2))
	pl_call  pred_name_builder_3()
fun238:
	call_c   Dyam_Choice(fun237)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(3),I(0))
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun236()


long local pool_fun240[3]=[131072,pool_fun239,pool_fun238]

long local pool_fun241[4]=[65538,build_ref_529,build_ref_530,pool_fun240]

pl_code local fun241
	call_c   Dyam_Remove_Choice()
	move     &ref[529], R(0)
	move     0, R(1)
	move     &ref[530], R(2)
	move     S(5), R(3)
	pl_call  pred_error_2()
fun240:
	call_c   Dyam_Choice(fun239)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_atom(V(2))
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun238()


long local pool_fun242[3]=[131072,pool_fun241,pool_fun240]

pl_code local fun242
	call_c   Dyam_Update_Choice(fun241)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),I(0))
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun240()

long local pool_fun243[4]=[131073,build_ref_519,pool_fun242,pool_fun240]

pl_code local fun243
	call_c   Dyam_Pool(pool_fun243)
	call_c   Dyam_Unify_Item(&ref[519])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun242)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_atom(V(1))
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun240()

;; TERM 520: '*GUARD*'(directive(import{module=> _B, file=> _C, preds=> _D}, _E, _F, _G)) :> []
c_code local build_ref_520
	ret_reg &ref[520]
	call_c   build_ref_519()
	call_c   Dyam_Create_Binary(I(9),&ref[519],I(0))
	move_ret ref[520]
	c_ret

c_code local build_seed_72
	ret_reg &seed[72]
	call_c   build_ref_11()
	call_c   build_ref_541()
	call_c   Dyam_Seed_Start(&ref[11],&ref[541],I(0),fun0,1)
	call_c   build_ref_540()
	call_c   Dyam_Seed_Add_Comp(&ref[540],fun253,0)
	call_c   Dyam_Seed_End()
	move_ret seed[72]
	c_ret

;; TERM 540: '*GUARD*'(directive(dcg_mode(_B, _C, _D, _E), _F, _G, _H))
c_code local build_ref_540
	ret_reg &ref[540]
	call_c   build_ref_12()
	call_c   build_ref_539()
	call_c   Dyam_Create_Unary(&ref[12],&ref[539])
	move_ret ref[540]
	c_ret

;; TERM 539: directive(dcg_mode(_B, _C, _D, _E), _F, _G, _H)
c_code local build_ref_539
	ret_reg &ref[539]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_755()
	call_c   Dyam_Term_Start(&ref[755],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_682()
	call_c   Dyam_Term_Start(&ref[682],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[539]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 755: dcg_mode
c_code local build_ref_755
	ret_reg &ref[755]
	call_c   Dyam_Create_Atom("dcg_mode")
	move_ret ref[755]
	c_ret

;; TERM 546: [dcg_mode(_B, _C, _D, _E)]
c_code local build_ref_546
	ret_reg &ref[546]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_755()
	call_c   Dyam_Term_Start(&ref[755],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   Dyam_Create_List(R(0),I(0))
	move_ret ref[546]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 545: dcg_mode(_I, _D, _E)
c_code local build_ref_545
	ret_reg &ref[545]
	call_c   build_ref_755()
	call_c   Dyam_Term_Start(&ref[755],3)
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[545]
	c_ret

long local pool_fun252[4]=[3,build_ref_426,build_ref_546,build_ref_545]

pl_code local fun252
	call_c   Dyam_Remove_Choice()
	move     &ref[426], R(0)
	move     0, R(1)
	move     &ref[546], R(2)
	move     S(5), R(3)
	pl_call  pred_error_2()
fun251:
	move     &ref[545], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(1))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_install_2()


c_code local build_seed_136
	ret_reg &seed[136]
	call_c   build_ref_12()
	call_c   build_ref_543()
	call_c   Dyam_Seed_Start(&ref[12],&ref[543],I(0),fun13,1)
	call_c   build_ref_544()
	call_c   Dyam_Seed_Add_Comp(&ref[544],&ref[543],0)
	call_c   Dyam_Seed_End()
	move_ret seed[136]
	c_ret

;; TERM 544: '*GUARD*'(check_mode([_D,_E|_I])) :> '$$HOLE$$'
c_code local build_ref_544
	ret_reg &ref[544]
	call_c   build_ref_543()
	call_c   Dyam_Create_Binary(I(9),&ref[543],I(7))
	move_ret ref[544]
	c_ret

;; TERM 543: '*GUARD*'(check_mode([_D,_E|_I]))
c_code local build_ref_543
	ret_reg &ref[543]
	call_c   build_ref_12()
	call_c   build_ref_542()
	call_c   Dyam_Create_Unary(&ref[12],&ref[542])
	move_ret ref[543]
	c_ret

;; TERM 542: check_mode([_D,_E|_I])
c_code local build_ref_542
	ret_reg &ref[542]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Tupple(3,4,V(8))
	move_ret R(0)
	call_c   build_ref_673()
	call_c   Dyam_Create_Unary(&ref[673],R(0))
	move_ret ref[542]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun253[5]=[65539,build_ref_540,build_seed_136,build_ref_545,pool_fun252]

pl_code local fun253
	call_c   Dyam_Pool(pool_fun253)
	call_c   Dyam_Unify_Item(&ref[540])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun252)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Reg_Load(0,V(2))
	move     V(8), R(2)
	move     S(5), R(3)
	pl_call  pred_normalize_mode_2()
	pl_call  fun25(&seed[136],1)
	call_c   Dyam_Cut()
	pl_jump  fun251()

;; TERM 541: '*GUARD*'(directive(dcg_mode(_B, _C, _D, _E), _F, _G, _H)) :> []
c_code local build_ref_541
	ret_reg &ref[541]
	call_c   build_ref_540()
	call_c   Dyam_Create_Binary(I(9),&ref[540],I(0))
	move_ret ref[541]
	c_ret

c_code local build_seed_71
	ret_reg &seed[71]
	call_c   build_ref_11()
	call_c   build_ref_549()
	call_c   Dyam_Seed_Start(&ref[11],&ref[549],I(0),fun0,1)
	call_c   build_ref_548()
	call_c   Dyam_Seed_Add_Comp(&ref[548],fun256,0)
	call_c   Dyam_Seed_End()
	move_ret seed[71]
	c_ret

;; TERM 548: '*GUARD*'(directive(tag_mode(_B, _C, _D, _E, _F), _G, _H, _I))
c_code local build_ref_548
	ret_reg &ref[548]
	call_c   build_ref_12()
	call_c   build_ref_547()
	call_c   Dyam_Create_Unary(&ref[12],&ref[547])
	move_ret ref[548]
	c_ret

;; TERM 547: directive(tag_mode(_B, _C, _D, _E, _F), _G, _H, _I)
c_code local build_ref_547
	ret_reg &ref[547]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_756()
	call_c   Dyam_Term_Start(&ref[756],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_682()
	call_c   Dyam_Term_Start(&ref[682],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_End()
	move_ret ref[547]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 756: tag_mode
c_code local build_ref_756
	ret_reg &ref[756]
	call_c   Dyam_Create_Atom("tag_mode")
	move_ret ref[756]
	c_ret

;; TERM 554: [tag_mode(_B, _C, _D, _E, _F)]
c_code local build_ref_554
	ret_reg &ref[554]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_756()
	call_c   Dyam_Term_Start(&ref[756],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   Dyam_Create_List(R(0),I(0))
	move_ret ref[554]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 553: tag_mode(_C, _J, _E, _F)
c_code local build_ref_553
	ret_reg &ref[553]
	call_c   build_ref_756()
	call_c   Dyam_Term_Start(&ref[756],4)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[553]
	c_ret

long local pool_fun255[4]=[3,build_ref_426,build_ref_554,build_ref_553]

pl_code local fun255
	call_c   Dyam_Remove_Choice()
	move     &ref[426], R(0)
	move     0, R(1)
	move     &ref[554], R(2)
	move     S(5), R(3)
	pl_call  pred_error_2()
fun254:
	move     &ref[553], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(1))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_install_2()


c_code local build_seed_137
	ret_reg &seed[137]
	call_c   build_ref_12()
	call_c   build_ref_551()
	call_c   Dyam_Seed_Start(&ref[12],&ref[551],I(0),fun13,1)
	call_c   build_ref_552()
	call_c   Dyam_Seed_Add_Comp(&ref[552],&ref[551],0)
	call_c   Dyam_Seed_End()
	move_ret seed[137]
	c_ret

;; TERM 552: '*GUARD*'(check_mode([_E,_F|_J])) :> '$$HOLE$$'
c_code local build_ref_552
	ret_reg &ref[552]
	call_c   build_ref_551()
	call_c   Dyam_Create_Binary(I(9),&ref[551],I(7))
	move_ret ref[552]
	c_ret

;; TERM 551: '*GUARD*'(check_mode([_E,_F|_J]))
c_code local build_ref_551
	ret_reg &ref[551]
	call_c   build_ref_12()
	call_c   build_ref_550()
	call_c   Dyam_Create_Unary(&ref[12],&ref[550])
	move_ret ref[551]
	c_ret

;; TERM 550: check_mode([_E,_F|_J])
c_code local build_ref_550
	ret_reg &ref[550]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Tupple(4,5,V(9))
	move_ret R(0)
	call_c   build_ref_673()
	call_c   Dyam_Create_Unary(&ref[673],R(0))
	move_ret ref[550]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun256[5]=[65539,build_ref_548,build_seed_137,build_ref_553,pool_fun255]

pl_code local fun256
	call_c   Dyam_Pool(pool_fun256)
	call_c   Dyam_Unify_Item(&ref[548])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun255)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Reg_Load(0,V(3))
	move     V(9), R(2)
	move     S(5), R(3)
	pl_call  pred_normalize_mode_2()
	pl_call  fun25(&seed[137],1)
	call_c   Dyam_Cut()
	pl_jump  fun254()

;; TERM 549: '*GUARD*'(directive(tag_mode(_B, _C, _D, _E, _F), _G, _H, _I)) :> []
c_code local build_ref_549
	ret_reg &ref[549]
	call_c   build_ref_548()
	call_c   Dyam_Create_Binary(I(9),&ref[548],I(0))
	move_ret ref[549]
	c_ret

c_code local build_seed_99
	ret_reg &seed[99]
	call_c   build_ref_11()
	call_c   build_ref_557()
	call_c   Dyam_Seed_Start(&ref[11],&ref[557],I(0),fun0,1)
	call_c   build_ref_556()
	call_c   Dyam_Seed_Add_Comp(&ref[556],fun260,0)
	call_c   Dyam_Seed_End()
	move_ret seed[99]
	c_ret

;; TERM 556: '*GUARD*'(read_analyze(_B, toplevel, _C, _D, _E))
c_code local build_ref_556
	ret_reg &ref[556]
	call_c   build_ref_12()
	call_c   build_ref_555()
	call_c   Dyam_Create_Unary(&ref[12],&ref[555])
	move_ret ref[556]
	c_ret

;; TERM 555: read_analyze(_B, toplevel, _C, _D, _E)
c_code local build_ref_555
	ret_reg &ref[555]
	call_c   build_ref_757()
	call_c   build_ref_559()
	call_c   Dyam_Term_Start(&ref[757],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(&ref[559])
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[555]
	c_ret

;; TERM 559: toplevel
c_code local build_ref_559
	ret_reg &ref[559]
	call_c   Dyam_Create_Atom("toplevel")
	move_ret ref[559]
	c_ret

;; TERM 757: read_analyze
c_code local build_ref_757
	ret_reg &ref[757]
	call_c   Dyam_Create_Atom("read_analyze")
	move_ret ref[757]
	c_ret

long local pool_fun257[2]=[1,build_ref_71]

pl_code local fun257
	call_c   Dyam_Remove_Choice()
	call_c   DYAM_evpred_assert_1(&ref[71])
	pl_jump  fun127()

;; TERM 558: :- _F
c_code local build_ref_558
	ret_reg &ref[558]
	call_c   build_ref_715()
	call_c   Dyam_Create_Unary(&ref[715],V(5))
	move_ret ref[558]
	c_ret

long local pool_fun258[4]=[65538,build_ref_558,build_ref_559,pool_fun257]

pl_code local fun258
	call_c   Dyam_Update_Choice(fun257)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[558])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(5))
	move     &ref[559], R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Load(4,V(3))
	call_c   Dyam_Reg_Load(6,V(4))
	pl_call  pred_read_analyze_directive_4()
	pl_jump  fun127()

long local pool_fun259[2]=[65536,pool_fun258]

pl_code local fun259
	call_c   Dyam_Update_Choice(fun258)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_variable(V(1))
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun127()

long local pool_fun260[4]=[65538,build_ref_556,build_ref_310,pool_fun259]

pl_code local fun260
	call_c   Dyam_Pool(pool_fun260)
	call_c   Dyam_Unify_Item(&ref[556])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun259)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(1),&ref[310])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(4),&ref[310])
	fail_ret
	pl_jump  fun127()

;; TERM 557: '*GUARD*'(read_analyze(_B, toplevel, _C, _D, _E)) :> []
c_code local build_ref_557
	ret_reg &ref[557]
	call_c   build_ref_556()
	call_c   Dyam_Create_Binary(I(9),&ref[556],I(0))
	move_ret ref[557]
	c_ret

c_code local build_seed_34
	ret_reg &seed[34]
	call_c   build_ref_11()
	call_c   build_ref_567()
	call_c   Dyam_Seed_Start(&ref[11],&ref[567],I(0),fun0,1)
	call_c   build_ref_566()
	call_c   Dyam_Seed_Add_Comp(&ref[566],fun269,0)
	call_c   Dyam_Seed_End()
	move_ret seed[34]
	c_ret

;; TERM 566: '*GUARD*'(installer(dcg_mode(_B, _C, _D), _E, _F))
c_code local build_ref_566
	ret_reg &ref[566]
	call_c   build_ref_12()
	call_c   build_ref_565()
	call_c   Dyam_Create_Unary(&ref[12],&ref[565])
	move_ret ref[566]
	c_ret

;; TERM 565: installer(dcg_mode(_B, _C, _D), _E, _F)
c_code local build_ref_565
	ret_reg &ref[565]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_755()
	call_c   Dyam_Term_Start(&ref[755],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_681()
	call_c   Dyam_Term_Start(&ref[681],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[565]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 575: call_dcg_pattern(_G, _B, _C, _D)
c_code local build_ref_575
	ret_reg &ref[575]
	call_c   build_ref_758()
	call_c   Dyam_Term_Start(&ref[758],4)
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[575]
	c_ret

;; TERM 758: call_dcg_pattern
c_code local build_ref_758
	ret_reg &ref[758]
	call_c   Dyam_Create_Atom("call_dcg_pattern")
	move_ret ref[758]
	c_ret

pl_code local fun268
	call_c   Dyam_Remove_Choice()
fun265:
	call_c   DYAM_evpred_assert_1(&ref[575])
	call_c   Dyam_Deallocate()
	pl_ret


;; TERM 568: call_dcg_pattern(_G, _H, _I, _J)
c_code local build_ref_568
	ret_reg &ref[568]
	call_c   build_ref_758()
	call_c   Dyam_Term_Start(&ref[758],4)
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret ref[568]
	c_ret

;; TERM 573: 'new dcg mode pattern ~w for ~w replaces ~w'
c_code local build_ref_573
	ret_reg &ref[573]
	call_c   Dyam_Create_Atom("new dcg mode pattern ~w for ~w replaces ~w")
	move_ret ref[573]
	c_ret

;; TERM 574: [_B * _C * _D,_G,_H * _I * _J]
c_code local build_ref_574
	ret_reg &ref[574]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_570()
	call_c   Dyam_Create_List(&ref[570],I(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(6),R(0))
	move_ret R(0)
	call_c   build_ref_572()
	call_c   Dyam_Create_List(&ref[572],R(0))
	move_ret ref[574]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 572: _B * _C * _D
c_code local build_ref_572
	ret_reg &ref[572]
	call_c   build_ref_392()
	call_c   build_ref_571()
	call_c   Dyam_Create_Binary(&ref[392],&ref[571],V(3))
	move_ret ref[572]
	c_ret

;; TERM 571: _B * _C
c_code local build_ref_571
	ret_reg &ref[571]
	call_c   build_ref_392()
	call_c   Dyam_Create_Binary(&ref[392],V(1),V(2))
	move_ret ref[571]
	c_ret

;; TERM 570: _H * _I * _J
c_code local build_ref_570
	ret_reg &ref[570]
	call_c   build_ref_392()
	call_c   build_ref_569()
	call_c   Dyam_Create_Binary(&ref[392],&ref[569],V(9))
	move_ret ref[570]
	c_ret

;; TERM 569: _H * _I
c_code local build_ref_569
	ret_reg &ref[569]
	call_c   build_ref_392()
	call_c   Dyam_Create_Binary(&ref[392],V(7),V(8))
	move_ret ref[569]
	c_ret

long local pool_fun266[4]=[3,build_ref_573,build_ref_574,build_ref_575]

pl_code local fun267
	call_c   Dyam_Remove_Choice()
fun266:
	call_c   Dyam_Cut()
	move     &ref[573], R(0)
	move     0, R(1)
	move     &ref[574], R(2)
	move     S(5), R(3)
	pl_call  pred_warning_2()
	pl_jump  fun265()


long local pool_fun269[7]=[65541,build_ref_566,build_ref_575,build_ref_568,build_ref_572,build_ref_570,pool_fun266]

pl_code local fun269
	call_c   Dyam_Pool(pool_fun269)
	call_c   Dyam_Unify_Item(&ref[566])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(4))
	move     V(6), R(2)
	move     S(5), R(3)
	pl_call  pred_spec_in_module_2()
	call_c   Dyam_Choice(fun268)
	call_c   Dyam_Set_Cut()
	pl_call  Object_2(&ref[568],V(10))
	call_c   DYAM_evpred_retract(&ref[568])
	fail_ret
	call_c   Dyam_Choice(fun267)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(&ref[570],&ref[572])
	fail_ret
	call_c   Dyam_Cut()
	pl_fail

;; TERM 567: '*GUARD*'(installer(dcg_mode(_B, _C, _D), _E, _F)) :> []
c_code local build_ref_567
	ret_reg &ref[567]
	call_c   build_ref_566()
	call_c   Dyam_Create_Binary(I(9),&ref[566],I(0))
	move_ret ref[567]
	c_ret

c_code local build_seed_42
	ret_reg &seed[42]
	call_c   build_ref_11()
	call_c   build_ref_578()
	call_c   Dyam_Seed_Start(&ref[11],&ref[578],I(0),fun0,1)
	call_c   build_ref_577()
	call_c   Dyam_Seed_Add_Comp(&ref[577],fun278,0)
	call_c   Dyam_Seed_End()
	move_ret seed[42]
	c_ret

;; TERM 577: '*GUARD*'(installer(flag(_B), _C, _D))
c_code local build_ref_577
	ret_reg &ref[577]
	call_c   build_ref_12()
	call_c   build_ref_576()
	call_c   Dyam_Create_Unary(&ref[12],&ref[576])
	move_ret ref[577]
	c_ret

;; TERM 576: installer(flag(_B), _C, _D)
c_code local build_ref_576
	ret_reg &ref[576]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_705()
	call_c   Dyam_Create_Unary(&ref[705],V(1))
	move_ret R(0)
	call_c   build_ref_681()
	call_c   Dyam_Term_Start(&ref[681],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[576]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 581: [_B,_F]
c_code local build_ref_581
	ret_reg &ref[581]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(5),I(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(1),R(0))
	move_ret ref[581]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

pl_code local fun273
	call_c   Dyam_Remove_Choice()
fun270:
	call_c   DYAM_evpred_assert_1(V(7))
	call_c   Dyam_Deallocate()
	pl_ret


;; TERM 582: flag_exclusive_set(_I, _B)
c_code local build_ref_582
	ret_reg &ref[582]
	call_c   build_ref_660()
	call_c   Dyam_Create_Binary(&ref[660],V(8),V(1))
	move_ret ref[582]
	c_ret

;; TERM 583: flag_exclusive_set(_I, _J)
c_code local build_ref_583
	ret_reg &ref[583]
	call_c   build_ref_660()
	call_c   Dyam_Create_Binary(&ref[660],V(8),V(9))
	move_ret ref[583]
	c_ret

;; TERM 584: [_J,_F]
c_code local build_ref_584
	ret_reg &ref[584]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(5),I(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(9),R(0))
	move_ret ref[584]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 585: 'new directive ~w for ~w replaces ~w'
c_code local build_ref_585
	ret_reg &ref[585]
	call_c   Dyam_Create_Atom("new directive ~w for ~w replaces ~w")
	move_ret ref[585]
	c_ret

;; TERM 586: [_B,_F,_J]
c_code local build_ref_586
	ret_reg &ref[586]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(9),I(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(5),R(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(1),R(0))
	move_ret ref[586]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun271[4]=[3,build_ref_584,build_ref_585,build_ref_586]

pl_code local fun272
	call_c   Dyam_Remove_Choice()
fun271:
	call_c   DYAM_evpred_univ(V(10),&ref[584])
	fail_ret
	pl_call  Object_2(V(10),V(11))
	call_c   DYAM_evpred_retract(V(10))
	fail_ret
	call_c   Dyam_Cut()
	move     &ref[585], R(0)
	move     0, R(1)
	move     &ref[586], R(2)
	move     S(5), R(3)
	pl_call  pred_warning_2()
	pl_jump  fun270()


long local pool_fun274[5]=[65539,build_ref_581,build_ref_582,build_ref_583,pool_fun271]

pl_code local fun275
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(5),V(4))
	fail_ret
fun274:
	call_c   DYAM_evpred_univ(V(7),&ref[581])
	fail_ret
	call_c   Dyam_Choice(fun273)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[582])
	pl_call  Object_1(&ref[583])
	call_c   Dyam_Choice(fun272)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(9),V(1))
	fail_ret
	call_c   Dyam_Cut()
	pl_fail


;; TERM 589: tag
c_code local build_ref_589
	ret_reg &ref[589]
	call_c   Dyam_Create_Atom("tag")
	move_ret ref[589]
	c_ret

;; TERM 590: tag(_E)
c_code local build_ref_590
	ret_reg &ref[590]
	call_c   build_ref_589()
	call_c   Dyam_Create_Unary(&ref[589],V(4))
	move_ret ref[590]
	c_ret

long local pool_fun276[5]=[131074,build_ref_589,build_ref_590,pool_fun274,pool_fun274]

pl_code local fun276
	call_c   Dyam_Update_Choice(fun275)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(3),&ref[589])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(5),&ref[590])
	fail_ret
	pl_jump  fun274()

;; TERM 587: rcg(_G)
c_code local build_ref_587
	ret_reg &ref[587]
	call_c   build_ref_739()
	call_c   Dyam_Create_Unary(&ref[739],V(6))
	move_ret ref[587]
	c_ret

;; TERM 588: rcg(_E, _G)
c_code local build_ref_588
	ret_reg &ref[588]
	call_c   build_ref_739()
	call_c   Dyam_Create_Binary(&ref[739],V(4),V(6))
	move_ret ref[588]
	c_ret

long local pool_fun277[5]=[131074,build_ref_587,build_ref_588,pool_fun276,pool_fun274]

pl_code local fun277
	call_c   Dyam_Update_Choice(fun276)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(3),&ref[587])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(5),&ref[588])
	fail_ret
	pl_jump  fun274()

;; TERM 580: dcg _E
c_code local build_ref_580
	ret_reg &ref[580]
	call_c   build_ref_579()
	call_c   Dyam_Create_Unary(&ref[579],V(4))
	move_ret ref[580]
	c_ret

long local pool_fun278[6]=[131075,build_ref_577,build_ref_579,build_ref_580,pool_fun277,pool_fun274]

pl_code local fun278
	call_c   Dyam_Pool(pool_fun278)
	call_c   Dyam_Unify_Item(&ref[577])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(2))
	move     V(4), R(2)
	move     S(5), R(3)
	pl_call  pred_spec_in_module_2()
	call_c   Dyam_Choice(fun277)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(3),&ref[579])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(5),&ref[580])
	fail_ret
	pl_jump  fun274()

;; TERM 578: '*GUARD*'(installer(flag(_B), _C, _D)) :> []
c_code local build_ref_578
	ret_reg &ref[578]
	call_c   build_ref_577()
	call_c   Dyam_Create_Binary(I(9),&ref[577],I(0))
	move_ret ref[578]
	c_ret

c_code local build_seed_33
	ret_reg &seed[33]
	call_c   build_ref_11()
	call_c   build_ref_595()
	call_c   Dyam_Seed_Start(&ref[11],&ref[595],I(0),fun0,1)
	call_c   build_ref_594()
	call_c   Dyam_Seed_Add_Comp(&ref[594],fun285,0)
	call_c   Dyam_Seed_End()
	move_ret seed[33]
	c_ret

;; TERM 594: '*GUARD*'(installer(tag_mode(_B, _C, _D, _E), _F, _G))
c_code local build_ref_594
	ret_reg &ref[594]
	call_c   build_ref_12()
	call_c   build_ref_593()
	call_c   Dyam_Create_Unary(&ref[12],&ref[593])
	move_ret ref[594]
	c_ret

;; TERM 593: installer(tag_mode(_B, _C, _D, _E), _F, _G)
c_code local build_ref_593
	ret_reg &ref[593]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_756()
	call_c   Dyam_Term_Start(&ref[756],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_681()
	call_c   Dyam_Term_Start(&ref[681],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[593]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 596: tag_modulation{subst_modulation=> _I, top_modulation=> _J, bot_modulation=> _K}
c_code local build_ref_596
	ret_reg &ref[596]
	call_c   build_ref_759()
	call_c   Dyam_Term_Start(&ref[759],3)
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_End()
	move_ret ref[596]
	c_ret

;; TERM 759: tag_modulation!'$ft'
c_code local build_ref_759
	ret_reg &ref[759]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_763()
	call_c   Dyam_Create_List(&ref[763],I(0))
	move_ret R(0)
	call_c   build_ref_762()
	call_c   Dyam_Create_List(&ref[762],R(0))
	move_ret R(0)
	call_c   build_ref_761()
	call_c   Dyam_Create_List(&ref[761],R(0))
	move_ret R(0)
	call_c   build_ref_760()
	call_c   Dyam_Create_Atom_Module("$ft",&ref[760])
	move_ret ref[759]
	call_c   DYAM_Feature_2(&ref[760],R(0))
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 760: tag_modulation
c_code local build_ref_760
	ret_reg &ref[760]
	call_c   Dyam_Create_Atom("tag_modulation")
	move_ret ref[760]
	c_ret

;; TERM 761: subst_modulation
c_code local build_ref_761
	ret_reg &ref[761]
	call_c   Dyam_Create_Atom("subst_modulation")
	move_ret ref[761]
	c_ret

;; TERM 762: top_modulation
c_code local build_ref_762
	ret_reg &ref[762]
	call_c   Dyam_Create_Atom("top_modulation")
	move_ret ref[762]
	c_ret

;; TERM 763: bot_modulation
c_code local build_ref_763
	ret_reg &ref[763]
	call_c   Dyam_Create_Atom("bot_modulation")
	move_ret ref[763]
	c_ret

;; TERM 597: nt_modulation{nt_pattern=> _C, left_pattern=> _D, right_pattern=> _E}
c_code local build_ref_597
	ret_reg &ref[597]
	call_c   build_ref_764()
	call_c   Dyam_Term_Start(&ref[764],3)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[597]
	c_ret

;; TERM 764: nt_modulation!'$ft'
c_code local build_ref_764
	ret_reg &ref[764]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_768()
	call_c   Dyam_Create_List(&ref[768],I(0))
	move_ret R(0)
	call_c   build_ref_767()
	call_c   Dyam_Create_List(&ref[767],R(0))
	move_ret R(0)
	call_c   build_ref_766()
	call_c   Dyam_Create_List(&ref[766],R(0))
	move_ret R(0)
	call_c   build_ref_765()
	call_c   Dyam_Create_Atom_Module("$ft",&ref[765])
	move_ret ref[764]
	call_c   DYAM_Feature_2(&ref[765],R(0))
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 765: nt_modulation
c_code local build_ref_765
	ret_reg &ref[765]
	call_c   Dyam_Create_Atom("nt_modulation")
	move_ret ref[765]
	c_ret

;; TERM 766: nt_pattern
c_code local build_ref_766
	ret_reg &ref[766]
	call_c   Dyam_Create_Atom("nt_pattern")
	move_ret ref[766]
	c_ret

;; TERM 767: left_pattern
c_code local build_ref_767
	ret_reg &ref[767]
	call_c   Dyam_Create_Atom("left_pattern")
	move_ret ref[767]
	c_ret

;; TERM 768: right_pattern
c_code local build_ref_768
	ret_reg &ref[768]
	call_c   Dyam_Create_Atom("right_pattern")
	move_ret ref[768]
	c_ret

;; TERM 599: tag_modulation(_H, tag_modulation{subst_modulation=> _I, top_modulation=> _J, bot_modulation=> _K})
c_code local build_ref_599
	ret_reg &ref[599]
	call_c   build_ref_760()
	call_c   build_ref_596()
	call_c   Dyam_Create_Binary(&ref[760],V(7),&ref[596])
	move_ret ref[599]
	c_ret

pl_code local fun284
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(11),I(0))
	fail_ret
	call_c   Dyam_Unify(V(12),I(0))
	fail_ret
	call_c   Dyam_Unify(V(13),I(0))
	fail_ret
fun283:
	call_c   Dyam_Reg_Load(0,V(7))
	call_c   Dyam_Reg_Load(2,V(8))
	call_c   Dyam_Reg_Load(4,V(11))
	pl_call  pred_tag_nt_modulation_3()
	call_c   Dyam_Reg_Load(0,V(7))
	call_c   Dyam_Reg_Load(2,V(9))
	call_c   Dyam_Reg_Load(4,V(12))
	pl_call  pred_tag_nt_modulation_3()
	call_c   Dyam_Reg_Load(0,V(7))
	call_c   Dyam_Reg_Load(2,V(10))
	call_c   Dyam_Reg_Load(4,V(13))
	pl_call  pred_tag_nt_modulation_3()
	call_c   DYAM_evpred_assert_1(&ref[599])
	call_c   Dyam_Deallocate()
	pl_ret


;; TERM 598: tag_modulation(_H, tag_modulation{subst_modulation=> _L, top_modulation=> _M, bot_modulation=> _N})
c_code local build_ref_598
	ret_reg &ref[598]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_759()
	call_c   Dyam_Term_Start(&ref[759],3)
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_760()
	call_c   Dyam_Create_Binary(&ref[760],V(7),R(0))
	move_ret ref[598]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun285[7]=[6,build_ref_594,build_ref_596,build_ref_597,build_ref_599,build_ref_598,build_ref_599]

pl_code local fun285
	call_c   Dyam_Pool(pool_fun285)
	call_c   Dyam_Unify_Item(&ref[594])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(5))
	move     V(7), R(2)
	move     S(5), R(3)
	pl_call  pred_spec_in_module_2()
	call_c   Dyam_Reg_Load(0,V(1))
	move     &ref[596], R(2)
	move     S(5), R(3)
	move     &ref[597], R(4)
	move     S(5), R(5)
	pl_call  pred_check_tag_kind_3()
	call_c   Dyam_Choice(fun284)
	call_c   Dyam_Set_Cut()
	pl_call  Object_2(&ref[598],V(14))
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load_Ptr(2,V(14))
	fail_ret
	call_c   object_delete(R(2))
	fail_ret
	pl_jump  fun283()

;; TERM 595: '*GUARD*'(installer(tag_mode(_B, _C, _D, _E), _F, _G)) :> []
c_code local build_ref_595
	ret_reg &ref[595]
	call_c   build_ref_594()
	call_c   Dyam_Create_Binary(I(9),&ref[594],I(0))
	move_ret ref[595]
	c_ret

c_code local build_seed_103
	ret_reg &seed[103]
	call_c   build_ref_11()
	call_c   build_ref_607()
	call_c   Dyam_Seed_Start(&ref[11],&ref[607],I(0),fun0,1)
	call_c   build_ref_606()
	call_c   Dyam_Seed_Add_Comp(&ref[606],fun293,0)
	call_c   Dyam_Seed_End()
	move_ret seed[103]
	c_ret

;; TERM 606: '*GUARD*'(read_analyze(_B, include, _C, _D, _E))
c_code local build_ref_606
	ret_reg &ref[606]
	call_c   build_ref_12()
	call_c   build_ref_605()
	call_c   Dyam_Create_Unary(&ref[12],&ref[605])
	move_ret ref[606]
	c_ret

;; TERM 605: read_analyze(_B, include, _C, _D, _E)
c_code local build_ref_605
	ret_reg &ref[605]
	call_c   build_ref_757()
	call_c   build_ref_198()
	call_c   Dyam_Term_Start(&ref[757],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(&ref[198])
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[605]
	c_ret

c_code local build_seed_139
	ret_reg &seed[139]
	call_c   build_ref_12()
	call_c   build_ref_612()
	call_c   Dyam_Seed_Start(&ref[12],&ref[612],I(0),fun13,1)
	call_c   build_ref_613()
	call_c   Dyam_Seed_Add_Comp(&ref[613],&ref[612],0)
	call_c   Dyam_Seed_End()
	move_ret seed[139]
	c_ret

;; TERM 613: '*GUARD*'(register_predicate(fact, _B, _D)) :> '$$HOLE$$'
c_code local build_ref_613
	ret_reg &ref[613]
	call_c   build_ref_612()
	call_c   Dyam_Create_Binary(I(9),&ref[612],I(7))
	move_ret ref[613]
	c_ret

;; TERM 612: '*GUARD*'(register_predicate(fact, _B, _D))
c_code local build_ref_612
	ret_reg &ref[612]
	call_c   build_ref_12()
	call_c   build_ref_611()
	call_c   Dyam_Create_Unary(&ref[12],&ref[611])
	move_ret ref[612]
	c_ret

;; TERM 611: register_predicate(fact, _B, _D)
c_code local build_ref_611
	ret_reg &ref[611]
	call_c   build_ref_716()
	call_c   build_ref_740()
	call_c   Dyam_Term_Start(&ref[716],3)
	call_c   Dyam_Term_Arg(&ref[740])
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[611]
	c_ret

;; TERM 614: '$fact'(_B)
c_code local build_ref_614
	ret_reg &ref[614]
	call_c   build_ref_751()
	call_c   Dyam_Create_Unary(&ref[751],V(1))
	move_ret ref[614]
	c_ret

long local pool_fun289[3]=[2,build_seed_139,build_ref_614]

pl_code local fun289
	call_c   Dyam_Remove_Choice()
	pl_call  fun25(&seed[139],1)
	move     &ref[614], R(0)
	move     S(5), R(1)
	pl_call  pred_register_clause_1()
	pl_jump  fun127()

c_code local build_seed_138
	ret_reg &seed[138]
	call_c   build_ref_12()
	call_c   build_ref_609()
	call_c   Dyam_Seed_Start(&ref[12],&ref[609],I(0),fun13,1)
	call_c   build_ref_610()
	call_c   Dyam_Seed_Add_Comp(&ref[610],&ref[609],0)
	call_c   Dyam_Seed_End()
	move_ret seed[138]
	c_ret

;; TERM 610: '*GUARD*'(clause_type(_B, _G, _C, _D)) :> '$$HOLE$$'
c_code local build_ref_610
	ret_reg &ref[610]
	call_c   build_ref_609()
	call_c   Dyam_Create_Binary(I(9),&ref[609],I(7))
	move_ret ref[610]
	c_ret

;; TERM 609: '*GUARD*'(clause_type(_B, _G, _C, _D))
c_code local build_ref_609
	ret_reg &ref[609]
	call_c   build_ref_12()
	call_c   build_ref_608()
	call_c   Dyam_Create_Unary(&ref[12],&ref[608])
	move_ret ref[609]
	c_ret

;; TERM 608: clause_type(_B, _G, _C, _D)
c_code local build_ref_608
	ret_reg &ref[608]
	call_c   build_ref_677()
	call_c   Dyam_Term_Start(&ref[677],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[608]
	c_ret

long local pool_fun290[3]=[65537,build_seed_138,pool_fun289]

pl_code local fun290
	call_c   Dyam_Update_Choice(fun289)
	call_c   Dyam_Set_Cut()
	pl_call  fun25(&seed[138],1)
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(6))
	pl_call  pred_register_clause_1()
	pl_jump  fun127()

long local pool_fun291[4]=[65538,build_ref_558,build_ref_198,pool_fun290]

pl_code local fun291
	call_c   Dyam_Update_Choice(fun290)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[558])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(5))
	move     &ref[198], R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Load(4,V(3))
	call_c   Dyam_Reg_Load(6,V(4))
	pl_call  pred_read_analyze_directive_4()
	pl_jump  fun127()

long local pool_fun292[2]=[65536,pool_fun291]

pl_code local fun292
	call_c   Dyam_Update_Choice(fun291)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_variable(V(1))
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun127()

long local pool_fun293[4]=[65538,build_ref_606,build_ref_310,pool_fun292]

pl_code local fun293
	call_c   Dyam_Pool(pool_fun293)
	call_c   Dyam_Unify_Item(&ref[606])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun292)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(1),&ref[310])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(4),&ref[310])
	fail_ret
	pl_jump  fun127()

;; TERM 607: '*GUARD*'(read_analyze(_B, include, _C, _D, _E)) :> []
c_code local build_ref_607
	ret_reg &ref[607]
	call_c   build_ref_606()
	call_c   Dyam_Create_Binary(I(9),&ref[606],I(0))
	move_ret ref[607]
	c_ret

c_code local build_seed_98
	ret_reg &seed[98]
	call_c   build_ref_11()
	call_c   build_ref_617()
	call_c   Dyam_Seed_Start(&ref[11],&ref[617],I(0),fun0,1)
	call_c   build_ref_616()
	call_c   Dyam_Seed_Add_Comp(&ref[616],fun298,0)
	call_c   Dyam_Seed_End()
	move_ret seed[98]
	c_ret

;; TERM 616: '*GUARD*'(read_analyze(_B, resource, _C, _D, _E))
c_code local build_ref_616
	ret_reg &ref[616]
	call_c   build_ref_12()
	call_c   build_ref_615()
	call_c   Dyam_Create_Unary(&ref[12],&ref[615])
	move_ret ref[616]
	c_ret

;; TERM 615: read_analyze(_B, resource, _C, _D, _E)
c_code local build_ref_615
	ret_reg &ref[615]
	call_c   build_ref_757()
	call_c   build_ref_144()
	call_c   Dyam_Term_Start(&ref[757],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(&ref[144])
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[615]
	c_ret

;; TERM 619: _I --> _J
c_code local build_ref_619
	ret_reg &ref[619]
	call_c   build_ref_744()
	call_c   Dyam_Create_Binary(&ref[744],V(8),V(9))
	move_ret ref[619]
	c_ret

;; TERM 620: dcg_compiler_expansion(_B)
c_code local build_ref_620
	ret_reg &ref[620]
	call_c   build_ref_769()
	call_c   Dyam_Create_Unary(&ref[769],V(1))
	move_ret ref[620]
	c_ret

;; TERM 769: dcg_compiler_expansion
c_code local build_ref_769
	ret_reg &ref[769]
	call_c   Dyam_Create_Atom("dcg_compiler_expansion")
	move_ret ref[769]
	c_ret

long local pool_fun294[3]=[2,build_ref_619,build_ref_620]

pl_code local fun294
	call_c   Dyam_Update_Choice(fun128)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[619])
	fail_ret
	call_c   Dyam_Cut()
	call_c   DYAM_evpred_assert_1(&ref[620])
	pl_jump  fun127()

;; TERM 618: _G :- _H
c_code local build_ref_618
	ret_reg &ref[618]
	call_c   build_ref_715()
	call_c   Dyam_Create_Binary(&ref[715],V(6),V(7))
	move_ret ref[618]
	c_ret

long local pool_fun295[3]=[65537,build_ref_618,pool_fun294]

pl_code local fun295
	call_c   Dyam_Update_Choice(fun294)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[618])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(1))
	pl_call  pred_extend_compiler_1()
	pl_jump  fun127()

long local pool_fun296[4]=[65538,build_ref_558,build_ref_144,pool_fun295]

pl_code local fun296
	call_c   Dyam_Update_Choice(fun295)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[558])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(5))
	move     &ref[144], R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Load(4,V(3))
	call_c   Dyam_Reg_Load(6,V(4))
	pl_call  pred_read_analyze_directive_4()
	pl_jump  fun127()

long local pool_fun297[2]=[65536,pool_fun296]

pl_code local fun297
	call_c   Dyam_Update_Choice(fun296)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_variable(V(1))
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun127()

long local pool_fun298[4]=[65538,build_ref_616,build_ref_310,pool_fun297]

pl_code local fun298
	call_c   Dyam_Pool(pool_fun298)
	call_c   Dyam_Unify_Item(&ref[616])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun297)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(1),&ref[310])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(4),&ref[310])
	fail_ret
	pl_jump  fun127()

;; TERM 617: '*GUARD*'(read_analyze(_B, resource, _C, _D, _E)) :> []
c_code local build_ref_617
	ret_reg &ref[617]
	call_c   build_ref_616()
	call_c   Dyam_Create_Binary(I(9),&ref[616],I(0))
	move_ret ref[617]
	c_ret

c_code local build_seed_97
	ret_reg &seed[97]
	call_c   build_ref_11()
	call_c   build_ref_629()
	call_c   Dyam_Seed_Start(&ref[11],&ref[629],I(0),fun0,1)
	call_c   build_ref_628()
	call_c   Dyam_Seed_Add_Comp(&ref[628],fun309,0)
	call_c   Dyam_Seed_End()
	move_ret seed[97]
	c_ret

;; TERM 628: '*GUARD*'(read_analyze(_B, require, _C, _D, _E))
c_code local build_ref_628
	ret_reg &ref[628]
	call_c   build_ref_12()
	call_c   build_ref_627()
	call_c   Dyam_Create_Unary(&ref[12],&ref[627])
	move_ret ref[628]
	c_ret

;; TERM 627: read_analyze(_B, require, _C, _D, _E)
c_code local build_ref_627
	ret_reg &ref[627]
	call_c   build_ref_757()
	call_c   build_ref_100()
	call_c   Dyam_Term_Start(&ref[757],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(&ref[100])
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[627]
	c_ret

long local pool_fun304[2]=[1,build_seed_139]

pl_code local fun304
	call_c   Dyam_Remove_Choice()
	pl_call  fun25(&seed[139],1)
	pl_jump  fun127()

;; TERM 633: _G --> _I
c_code local build_ref_633
	ret_reg &ref[633]
	call_c   build_ref_744()
	call_c   Dyam_Create_Binary(&ref[744],V(6),V(8))
	move_ret ref[633]
	c_ret

;; TERM 634: grammar_kind(_J)
c_code local build_ref_634
	ret_reg &ref[634]
	call_c   build_ref_746()
	call_c   Dyam_Create_Unary(&ref[746],V(9))
	move_ret ref[634]
	c_ret

c_code local build_seed_141
	ret_reg &seed[141]
	call_c   build_ref_12()
	call_c   build_ref_636()
	call_c   Dyam_Seed_Start(&ref[12],&ref[636],I(0),fun13,1)
	call_c   build_ref_637()
	call_c   Dyam_Seed_Add_Comp(&ref[637],&ref[636],0)
	call_c   Dyam_Seed_End()
	move_ret seed[141]
	c_ret

;; TERM 637: '*GUARD*'(register_predicate(_K, _G, _D)) :> '$$HOLE$$'
c_code local build_ref_637
	ret_reg &ref[637]
	call_c   build_ref_636()
	call_c   Dyam_Create_Binary(I(9),&ref[636],I(7))
	move_ret ref[637]
	c_ret

;; TERM 636: '*GUARD*'(register_predicate(_K, _G, _D))
c_code local build_ref_636
	ret_reg &ref[636]
	call_c   build_ref_12()
	call_c   build_ref_635()
	call_c   Dyam_Create_Unary(&ref[12],&ref[635])
	move_ret ref[636]
	c_ret

;; TERM 635: register_predicate(_K, _G, _D)
c_code local build_ref_635
	ret_reg &ref[635]
	call_c   build_ref_716()
	call_c   Dyam_Term_Start(&ref[716],3)
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[635]
	c_ret

long local pool_fun305[5]=[65539,build_ref_633,build_ref_634,build_seed_141,pool_fun304]

pl_code local fun305
	call_c   Dyam_Update_Choice(fun304)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[633])
	fail_ret
	call_c   Dyam_Cut()
	pl_call  Object_1(&ref[634])
	call_c   Dyam_Reg_Load_Ptr(2,V(9))
	fail_ret
	move     V(10), R(4)
	move     S(5), R(5)
	call_c   DyALog_Mutable_Read(R(2),&R(4))
	fail_ret
	pl_call  fun25(&seed[141],1)
	pl_jump  fun127()

c_code local build_seed_140
	ret_reg &seed[140]
	call_c   build_ref_12()
	call_c   build_ref_631()
	call_c   Dyam_Seed_Start(&ref[12],&ref[631],I(0),fun13,1)
	call_c   build_ref_632()
	call_c   Dyam_Seed_Add_Comp(&ref[632],&ref[631],0)
	call_c   Dyam_Seed_End()
	move_ret seed[140]
	c_ret

;; TERM 632: '*GUARD*'(register_predicate(clause, _G, _D)) :> '$$HOLE$$'
c_code local build_ref_632
	ret_reg &ref[632]
	call_c   build_ref_631()
	call_c   Dyam_Create_Binary(I(9),&ref[631],I(7))
	move_ret ref[632]
	c_ret

;; TERM 631: '*GUARD*'(register_predicate(clause, _G, _D))
c_code local build_ref_631
	ret_reg &ref[631]
	call_c   build_ref_12()
	call_c   build_ref_630()
	call_c   Dyam_Create_Unary(&ref[12],&ref[630])
	move_ret ref[631]
	c_ret

;; TERM 630: register_predicate(clause, _G, _D)
c_code local build_ref_630
	ret_reg &ref[630]
	call_c   build_ref_716()
	call_c   build_ref_717()
	call_c   Dyam_Term_Start(&ref[716],3)
	call_c   Dyam_Term_Arg(&ref[717])
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[630]
	c_ret

long local pool_fun306[4]=[65538,build_ref_618,build_seed_140,pool_fun305]

pl_code local fun306
	call_c   Dyam_Update_Choice(fun305)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[618])
	fail_ret
	call_c   Dyam_Cut()
	pl_call  fun25(&seed[140],1)
	pl_jump  fun127()

long local pool_fun307[4]=[65538,build_ref_558,build_ref_100,pool_fun306]

pl_code local fun307
	call_c   Dyam_Update_Choice(fun306)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[558])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(5))
	move     &ref[100], R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Load(4,V(3))
	call_c   Dyam_Reg_Load(6,V(4))
	pl_call  pred_read_analyze_directive_4()
	pl_jump  fun127()

long local pool_fun308[2]=[65536,pool_fun307]

pl_code local fun308
	call_c   Dyam_Update_Choice(fun307)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_variable(V(1))
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun127()

long local pool_fun309[4]=[65538,build_ref_628,build_ref_310,pool_fun308]

pl_code local fun309
	call_c   Dyam_Pool(pool_fun309)
	call_c   Dyam_Unify_Item(&ref[628])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun308)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(1),&ref[310])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(4),&ref[310])
	fail_ret
	pl_jump  fun127()

;; TERM 629: '*GUARD*'(read_analyze(_B, require, _C, _D, _E)) :> []
c_code local build_ref_629
	ret_reg &ref[629]
	call_c   build_ref_628()
	call_c   Dyam_Create_Binary(I(9),&ref[628],I(0))
	move_ret ref[629]
	c_ret

c_code local build_seed_143
	ret_reg &seed[143]
	call_c   build_ref_12()
	call_c   build_ref_641()
	call_c   Dyam_Seed_Start(&ref[12],&ref[641],I(0),fun13,1)
	call_c   build_ref_642()
	call_c   Dyam_Seed_Add_Comp(&ref[642],&ref[641],0)
	call_c   Dyam_Seed_End()
	move_ret seed[143]
	c_ret

;; TERM 642: '*GUARD*'(install_list(_B, _C, _D)) :> '$$HOLE$$'
c_code local build_ref_642
	ret_reg &ref[642]
	call_c   build_ref_641()
	call_c   Dyam_Create_Binary(I(9),&ref[641],I(7))
	move_ret ref[642]
	c_ret

;; TERM 641: '*GUARD*'(install_list(_B, _C, _D))
c_code local build_ref_641
	ret_reg &ref[641]
	call_c   build_ref_12()
	call_c   build_ref_640()
	call_c   Dyam_Create_Unary(&ref[12],&ref[640])
	move_ret ref[641]
	c_ret

;; TERM 640: install_list(_B, _C, _D)
c_code local build_ref_640
	ret_reg &ref[640]
	call_c   build_ref_675()
	call_c   Dyam_Term_Start(&ref[675],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[640]
	c_ret

c_code local build_seed_142
	ret_reg &seed[142]
	call_c   build_ref_12()
	call_c   build_ref_128()
	call_c   Dyam_Seed_Start(&ref[12],&ref[128],I(0),fun13,1)
	call_c   build_ref_638()
	call_c   Dyam_Seed_Add_Comp(&ref[638],&ref[128],0)
	call_c   Dyam_Seed_End()
	move_ret seed[142]
	c_ret

;; TERM 638: '*GUARD*'(install_elem(_B, _C, _D)) :> '$$HOLE$$'
c_code local build_ref_638
	ret_reg &ref[638]
	call_c   build_ref_128()
	call_c   Dyam_Create_Binary(I(9),&ref[128],I(7))
	move_ret ref[638]
	c_ret

c_code local build_seed_135
	ret_reg &seed[135]
	call_c   build_ref_12()
	call_c   build_ref_536()
	call_c   Dyam_Seed_Start(&ref[12],&ref[536],I(0),fun13,1)
	call_c   build_ref_537()
	call_c   Dyam_Seed_Add_Comp(&ref[537],&ref[536],0)
	call_c   Dyam_Seed_End()
	move_ret seed[135]
	c_ret

;; TERM 537: '*GUARD*'(directive(_B, _C, _D, _E)) :> '$$HOLE$$'
c_code local build_ref_537
	ret_reg &ref[537]
	call_c   build_ref_536()
	call_c   Dyam_Create_Binary(I(9),&ref[536],I(7))
	move_ret ref[537]
	c_ret

;; TERM 536: '*GUARD*'(directive(_B, _C, _D, _E))
c_code local build_ref_536
	ret_reg &ref[536]
	call_c   build_ref_12()
	call_c   build_ref_535()
	call_c   Dyam_Create_Unary(&ref[12],&ref[535])
	move_ret ref[536]
	c_ret

;; TERM 535: directive(_B, _C, _D, _E)
c_code local build_ref_535
	ret_reg &ref[535]
	call_c   build_ref_682()
	call_c   Dyam_Term_Start(&ref[682],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[535]
	c_ret

c_code local build_seed_132
	ret_reg &seed[132]
	call_c   build_ref_12()
	call_c   build_ref_474()
	call_c   Dyam_Seed_Start(&ref[12],&ref[474],I(0),fun13,1)
	call_c   build_ref_475()
	call_c   Dyam_Seed_Add_Comp(&ref[475],&ref[474],0)
	call_c   Dyam_Seed_End()
	move_ret seed[132]
	c_ret

;; TERM 475: '*GUARD*'(check_ground_list(_B)) :> '$$HOLE$$'
c_code local build_ref_475
	ret_reg &ref[475]
	call_c   build_ref_474()
	call_c   Dyam_Create_Binary(I(9),&ref[474],I(7))
	move_ret ref[475]
	c_ret

;; TERM 474: '*GUARD*'(check_ground_list(_B))
c_code local build_ref_474
	ret_reg &ref[474]
	call_c   build_ref_12()
	call_c   build_ref_473()
	call_c   Dyam_Create_Unary(&ref[12],&ref[473])
	move_ret ref[474]
	c_ret

;; TERM 473: check_ground_list(_B)
c_code local build_ref_473
	ret_reg &ref[473]
	call_c   build_ref_672()
	call_c   Dyam_Create_Unary(&ref[672],V(1))
	move_ret ref[473]
	c_ret

c_code local build_seed_115
	ret_reg &seed[115]
	call_c   build_ref_12()
	call_c   build_ref_102()
	call_c   Dyam_Seed_Start(&ref[12],&ref[102],I(0),fun13,1)
	call_c   build_ref_103()
	call_c   Dyam_Seed_Add_Comp(&ref[103],&ref[102],0)
	call_c   Dyam_Seed_End()
	move_ret seed[115]
	c_ret

;; TERM 103: '*GUARD*'(read_analyze(_F, _C, _G, _D, _H)) :> '$$HOLE$$'
c_code local build_ref_103
	ret_reg &ref[103]
	call_c   build_ref_102()
	call_c   Dyam_Create_Binary(I(9),&ref[102],I(7))
	move_ret ref[103]
	c_ret

;; TERM 102: '*GUARD*'(read_analyze(_F, _C, _G, _D, _H))
c_code local build_ref_102
	ret_reg &ref[102]
	call_c   build_ref_12()
	call_c   build_ref_101()
	call_c   Dyam_Create_Unary(&ref[12],&ref[101])
	move_ret ref[102]
	c_ret

;; TERM 101: read_analyze(_F, _C, _G, _D, _H)
c_code local build_ref_101
	ret_reg &ref[101]
	call_c   build_ref_757()
	call_c   Dyam_Term_Start(&ref[757],5)
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[101]
	c_ret

;; TERM 649: tag_modulation{subst_modulation=> nt_modulation{nt_pattern=> _D, left_pattern=> _E, right_pattern=> _F}, top_modulation=> _G, bot_modulation=> _H}
c_code local build_ref_649
	ret_reg &ref[649]
	call_c   build_ref_759()
	call_c   build_ref_657()
	call_c   Dyam_Term_Start(&ref[759],3)
	call_c   Dyam_Term_Arg(&ref[657])
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[649]
	c_ret

;; TERM 657: nt_modulation{nt_pattern=> _D, left_pattern=> _E, right_pattern=> _F}
c_code local build_ref_657
	ret_reg &ref[657]
	call_c   build_ref_764()
	call_c   Dyam_Term_Start(&ref[764],3)
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[657]
	c_ret

;; TERM 648: subst
c_code local build_ref_648
	ret_reg &ref[648]
	call_c   Dyam_Create_Atom("subst")
	move_ret ref[648]
	c_ret

;; TERM 651: tag_modulation{subst_modulation=> _I, top_modulation=> nt_modulation{nt_pattern=> _D, left_pattern=> _E, right_pattern=> _F}, bot_modulation=> _J}
c_code local build_ref_651
	ret_reg &ref[651]
	call_c   build_ref_759()
	call_c   build_ref_657()
	call_c   Dyam_Term_Start(&ref[759],3)
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(&ref[657])
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret ref[651]
	c_ret

;; TERM 650: top
c_code local build_ref_650
	ret_reg &ref[650]
	call_c   Dyam_Create_Atom("top")
	move_ret ref[650]
	c_ret

;; TERM 653: tag_modulation{subst_modulation=> _K, top_modulation=> _L, bot_modulation=> nt_modulation{nt_pattern=> _D, left_pattern=> _E, right_pattern=> _F}}
c_code local build_ref_653
	ret_reg &ref[653]
	call_c   build_ref_759()
	call_c   build_ref_657()
	call_c   Dyam_Term_Start(&ref[759],3)
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(&ref[657])
	call_c   Dyam_Term_End()
	move_ret ref[653]
	c_ret

;; TERM 652: bot
c_code local build_ref_652
	ret_reg &ref[652]
	call_c   Dyam_Create_Atom("bot")
	move_ret ref[652]
	c_ret

;; TERM 655: tag_modulation{subst_modulation=> nt_modulation{nt_pattern=> _D, left_pattern=> _E, right_pattern=> _F}, top_modulation=> nt_modulation{nt_pattern=> _D, left_pattern=> _E, right_pattern=> _F}, bot_modulation=> nt_modulation{nt_pattern=> _D, left_pattern=> _E, right_pattern=> _F}}
c_code local build_ref_655
	ret_reg &ref[655]
	call_c   build_ref_759()
	call_c   build_ref_657()
	call_c   Dyam_Term_Start(&ref[759],3)
	call_c   Dyam_Term_Arg(&ref[657])
	call_c   Dyam_Term_Arg(&ref[657])
	call_c   Dyam_Term_Arg(&ref[657])
	call_c   Dyam_Term_End()
	move_ret ref[655]
	c_ret

;; TERM 654: all
c_code local build_ref_654
	ret_reg &ref[654]
	call_c   Dyam_Create_Atom("all")
	move_ret ref[654]
	c_ret

;; TERM 656: [_M]
c_code local build_ref_656
	ret_reg &ref[656]
	call_c   Dyam_Create_List(V(12),I(0))
	move_ret ref[656]
	c_ret

;; TERM 658: [_M|_N]
c_code local build_ref_658
	ret_reg &ref[658]
	call_c   Dyam_Create_List(V(12),V(13))
	move_ret ref[658]
	c_ret

;; TERM 659: 'Not a valid tag modulation kind ~w'
c_code local build_ref_659
	ret_reg &ref[659]
	call_c   Dyam_Create_Atom("Not a valid tag modulation kind ~w")
	move_ret ref[659]
	c_ret

;; TERM 639: [_E|_F]
c_code local build_ref_639
	ret_reg &ref[639]
	call_c   Dyam_Create_List(V(4),V(5))
	move_ret ref[639]
	c_ret

;; TERM 643: _G , _H
c_code local build_ref_643
	ret_reg &ref[643]
	call_c   Dyam_Create_Binary(I(4),V(6),V(7))
	move_ret ref[643]
	c_ret

;; TERM 644: dcg _G
c_code local build_ref_644
	ret_reg &ref[644]
	call_c   build_ref_579()
	call_c   Dyam_Create_Unary(&ref[579],V(6))
	move_ret ref[644]
	c_ret

;; TERM 645: tag(_G)
c_code local build_ref_645
	ret_reg &ref[645]
	call_c   build_ref_589()
	call_c   Dyam_Create_Unary(&ref[589],V(6))
	move_ret ref[645]
	c_ret

;; TERM 647: rcg(_I)
c_code local build_ref_647
	ret_reg &ref[647]
	call_c   build_ref_739()
	call_c   Dyam_Create_Unary(&ref[739],V(8))
	move_ret ref[647]
	c_ret

;; TERM 646: rcg(_G, _I)
c_code local build_ref_646
	ret_reg &ref[646]
	call_c   build_ref_739()
	call_c   Dyam_Create_Binary(&ref[739],V(6),V(8))
	move_ret ref[646]
	c_ret

;; TERM 621: 'tag_anchor: unexpected variables in equation'
c_code local build_ref_621
	ret_reg &ref[621]
	call_c   Dyam_Create_Atom("tag_anchor: unexpected variables in equation")
	move_ret ref[621]
	c_ret

;; TERM 622: _E and _F
c_code local build_ref_622
	ret_reg &ref[622]
	call_c   build_ref_770()
	call_c   Dyam_Create_Binary(&ref[770],V(4),V(5))
	move_ret ref[622]
	c_ret

;; TERM 770: and
c_code local build_ref_770
	ret_reg &ref[770]
	call_c   Dyam_Create_Atom("and")
	move_ret ref[770]
	c_ret

;; TERM 623: top = _G
c_code local build_ref_623
	ret_reg &ref[623]
	call_c   build_ref_771()
	call_c   build_ref_650()
	call_c   Dyam_Create_Binary(&ref[771],&ref[650],V(6))
	move_ret ref[623]
	c_ret

;; TERM 771: =
c_code local build_ref_771
	ret_reg &ref[771]
	call_c   Dyam_Create_Atom("=")
	move_ret ref[771]
	c_ret

;; TERM 624: tag((_H / _I))
c_code local build_ref_624
	ret_reg &ref[624]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_692()
	call_c   Dyam_Create_Binary(&ref[692],V(7),V(8))
	move_ret R(0)
	call_c   build_ref_589()
	call_c   Dyam_Create_Unary(&ref[589],R(0))
	move_ret ref[624]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 625: bot = _K
c_code local build_ref_625
	ret_reg &ref[625]
	call_c   build_ref_771()
	call_c   build_ref_652()
	call_c   Dyam_Create_Binary(&ref[771],&ref[652],V(10))
	move_ret ref[625]
	c_ret

;; TERM 626: 'tag_anchor: unknown equation type ~w'
c_code local build_ref_626
	ret_reg &ref[626]
	call_c   Dyam_Create_Atom("tag_anchor: unknown equation type ~w")
	move_ret ref[626]
	c_ret

;; TERM 600: 'Not a valid compiler extension: ~w'
c_code local build_ref_600
	ret_reg &ref[600]
	call_c   Dyam_Create_Atom("Not a valid compiler extension: ~w")
	move_ret ref[600]
	c_ret

;; TERM 602: [(_B :- _C)|_G]
c_code local build_ref_602
	ret_reg &ref[602]
	call_c   build_ref_604()
	call_c   Dyam_Create_List(&ref[604],V(6))
	move_ret ref[602]
	c_ret

;; TERM 601: compiler_extension((_D / _E), _F)
c_code local build_ref_601
	ret_reg &ref[601]
	call_c   build_ref_772()
	call_c   build_ref_301()
	call_c   Dyam_Create_Binary(&ref[772],&ref[301],V(5))
	move_ret ref[601]
	c_ret

;; TERM 772: compiler_extension
c_code local build_ref_772
	ret_reg &ref[772]
	call_c   Dyam_Create_Atom("compiler_extension")
	move_ret ref[772]
	c_ret

;; TERM 603: [(_B :- _C)]
c_code local build_ref_603
	ret_reg &ref[603]
	call_c   build_ref_604()
	call_c   Dyam_Create_List(&ref[604],I(0))
	move_ret ref[603]
	c_ret

;; TERM 591: stdin
c_code local build_ref_591
	ret_reg &ref[591]
	call_c   Dyam_Create_Atom("stdin")
	move_ret ref[591]
	c_ret

;; TERM 592: 'Not a valid filename or filename list ~w'
c_code local build_ref_592
	ret_reg &ref[592]
	call_c   Dyam_Create_Atom("Not a valid filename or filename list ~w")
	move_ret ref[592]
	c_ret

;; TERM 563: [_D,_F]
c_code local build_ref_563
	ret_reg &ref[563]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(5),I(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(3),R(0))
	move_ret ref[563]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 562: 'incompatible equations for TAG node ~w of family ~w'
c_code local build_ref_562
	ret_reg &ref[562]
	call_c   Dyam_Create_Atom("incompatible equations for TAG node ~w of family ~w")
	move_ret ref[562]
	c_ret

;; TERM 560: tag_equation(_F, _D, _I, _J, _K)
c_code local build_ref_560
	ret_reg &ref[560]
	call_c   build_ref_773()
	call_c   Dyam_Term_Start(&ref[773],5)
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_End()
	move_ret ref[560]
	c_ret

;; TERM 773: tag_equation
c_code local build_ref_773
	ret_reg &ref[773]
	call_c   Dyam_Create_Atom("tag_equation")
	move_ret ref[773]
	c_ret

;; TERM 561: tag_equation(_F, _D, _G, _H, _E)
c_code local build_ref_561
	ret_reg &ref[561]
	call_c   build_ref_773()
	call_c   Dyam_Term_Start(&ref[773],5)
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[561]
	c_ret

;; TERM 564: _B ^ (_C at _D)
c_code local build_ref_564
	ret_reg &ref[564]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_774()
	call_c   Dyam_Create_Binary(&ref[774],V(2),V(3))
	move_ret R(0)
	call_c   build_ref_719()
	call_c   Dyam_Create_Binary(&ref[719],V(1),R(0))
	move_ret ref[564]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 774: at
c_code local build_ref_774
	ret_reg &ref[774]
	call_c   Dyam_Create_Atom("at")
	move_ret ref[774]
	c_ret

;; TERM 538: 'not a valid directive ~w'
c_code local build_ref_538
	ret_reg &ref[538]
	call_c   Dyam_Create_Atom("not a valid directive ~w")
	move_ret ref[538]
	c_ret

;; TERM 534: [_F|_I]
c_code local build_ref_534
	ret_reg &ref[534]
	call_c   Dyam_Create_List(V(5),V(8))
	move_ret ref[534]
	c_ret

;; TERM 533: [_H|_I]
c_code local build_ref_533
	ret_reg &ref[533]
	call_c   Dyam_Create_List(V(7),V(8))
	move_ret ref[533]
	c_ret

;; TERM 532: _F , _G
c_code local build_ref_532
	ret_reg &ref[532]
	call_c   Dyam_Create_Binary(I(4),V(5),V(6))
	move_ret ref[532]
	c_ret

;; TERM 471: 'finite_set: defining empty set'
c_code local build_ref_471
	ret_reg &ref[471]
	call_c   Dyam_Create_Atom("finite_set: defining empty set")
	move_ret ref[471]
	c_ret

;; TERM 472: 'finite_set: defining set with single element ~w'
c_code local build_ref_472
	ret_reg &ref[472]
	call_c   Dyam_Create_Atom("finite_set: defining set with single element ~w")
	move_ret ref[472]
	c_ret

;; TERM 476: 'finite_set: not a ground list ~w'
c_code local build_ref_476
	ret_reg &ref[476]
	call_c   Dyam_Create_Atom("finite_set: not a ground list ~w")
	move_ret ref[476]
	c_ret

;; TERM 331: [_C,_B,_D]
c_code local build_ref_331
	ret_reg &ref[331]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(3),I(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(1),R(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(2),R(0))
	move_ret ref[331]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 330: 'new tag mode pattern ~w for ~w replaces ~w'
c_code local build_ref_330
	ret_reg &ref[330]
	call_c   Dyam_Create_Atom("new tag mode pattern ~w for ~w replaces ~w")
	move_ret ref[330]
	c_ret

;; TERM 302: _F / _E
c_code local build_ref_302
	ret_reg &ref[302]
	call_c   build_ref_692()
	call_c   Dyam_Create_Binary(&ref[692],V(5),V(4))
	move_ret ref[302]
	c_ret

;; TERM 205: [left,right,wrap]
c_code local build_ref_205
	ret_reg &ref[205]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_777()
	call_c   Dyam_Create_List(&ref[777],I(0))
	move_ret R(0)
	call_c   build_ref_776()
	call_c   Dyam_Create_List(&ref[776],R(0))
	move_ret R(0)
	call_c   build_ref_775()
	call_c   Dyam_Create_List(&ref[775],R(0))
	move_ret ref[205]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 775: left
c_code local build_ref_775
	ret_reg &ref[775]
	call_c   Dyam_Create_Atom("left")
	move_ret ref[775]
	c_ret

;; TERM 776: right
c_code local build_ref_776
	ret_reg &ref[776]
	call_c   Dyam_Create_Atom("right")
	move_ret ref[776]
	c_ret

;; TERM 777: wrap
c_code local build_ref_777
	ret_reg &ref[777]
	call_c   Dyam_Create_Atom("wrap")
	move_ret ref[777]
	c_ret

;; TERM 210: [_L]
c_code local build_ref_210
	ret_reg &ref[210]
	call_c   Dyam_Create_List(V(11),I(0))
	move_ret ref[210]
	c_ret

;; TERM 209: 'new tag features mode directive ~w'
c_code local build_ref_209
	ret_reg &ref[209]
	call_c   Dyam_Create_Atom("new tag features mode directive ~w")
	move_ret ref[209]
	c_ret

;; TERM 208: tag_features_mode(_B, _C, _M, _N)
c_code local build_ref_208
	ret_reg &ref[208]
	call_c   build_ref_735()
	call_c   Dyam_Term_Start(&ref[735],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_End()
	move_ret ref[208]
	c_ret

;; TERM 207: tag_features_mode(_B, _C, _I, _K)
c_code local build_ref_207
	ret_reg &ref[207]
	call_c   build_ref_735()
	call_c   Dyam_Term_Start(&ref[735],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_End()
	move_ret ref[207]
	c_ret

;; TERM 206: tag((_F / _G))
c_code local build_ref_206
	ret_reg &ref[206]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_692()
	call_c   Dyam_Create_Binary(&ref[692],V(5),V(6))
	move_ret R(0)
	call_c   build_ref_589()
	call_c   Dyam_Create_Unary(&ref[589],R(0))
	move_ret ref[206]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 212: [_D,_E,_B]
c_code local build_ref_212
	ret_reg &ref[212]
	call_c   build_ref_110()
	call_c   Dyam_Create_Tupple(3,4,&ref[110])
	move_ret ref[212]
	c_ret

;; TERM 211: 'features top ~w or bot ~w not built on same non terminal ~w (directive tag_features_mode)'
c_code local build_ref_211
	ret_reg &ref[211]
	call_c   Dyam_Create_Atom("features top ~w or bot ~w not built on same non terminal ~w (directive tag_features_mode)")
	move_ret ref[211]
	c_ret

;; TERM 213: 'directive tag_features_mode expects a symbol in {left,right,wrap} as second arg: ~w'
c_code local build_ref_213
	ret_reg &ref[213]
	call_c   Dyam_Create_Atom("directive tag_features_mode expects a symbol in {left,right,wrap} as second arg: ~w")
	move_ret ref[213]
	c_ret

;; TERM 215: 'directive tag_features_mode expects a symbol as first arg: ~w'
c_code local build_ref_215
	ret_reg &ref[215]
	call_c   Dyam_Create_Atom("directive tag_features_mode expects a symbol as first arg: ~w")
	move_ret ref[215]
	c_ret

;; TERM 194: [[_B,_F],_D]
c_code local build_ref_194
	ret_reg &ref[194]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(3),I(0))
	move_ret R(0)
	call_c   build_ref_581()
	call_c   Dyam_Create_List(&ref[581],R(0))
	move_ret ref[194]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 193: 'Multiple module names ~w for ~w'
c_code local build_ref_193
	ret_reg &ref[193]
	call_c   Dyam_Create_Atom("Multiple module names ~w for ~w")
	move_ret ref[193]
	c_ret

;; TERM 192: module(_D, _F)
c_code local build_ref_192
	ret_reg &ref[192]
	call_c   build_ref_724()
	call_c   Dyam_Create_Binary(&ref[724],V(3),V(5))
	move_ret ref[192]
	c_ret

;; TERM 197: [[_D,_G],_B]
c_code local build_ref_197
	ret_reg &ref[197]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(6),I(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(3),R(0))
	move_ret R(0)
	call_c   build_ref_110()
	call_c   Dyam_Create_List(R(0),&ref[110])
	move_ret ref[197]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 196: 'Multiple files ~w for module name ~w'
c_code local build_ref_196
	ret_reg &ref[196]
	call_c   Dyam_Create_Atom("Multiple files ~w for module name ~w")
	move_ret ref[196]
	c_ret

;; TERM 195: module(_G, _B)
c_code local build_ref_195
	ret_reg &ref[195]
	call_c   build_ref_724()
	call_c   Dyam_Create_Binary(&ref[724],V(6),V(1))
	move_ret ref[195]
	c_ret

;; TERM 201: [[_F,_B]]
c_code local build_ref_201
	ret_reg &ref[201]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_110()
	call_c   Dyam_Create_List(V(5),&ref[110])
	move_ret R(0)
	call_c   Dyam_Create_List(R(0),I(0))
	move_ret ref[201]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 200: 'multiple modules activated ~w'
c_code local build_ref_200
	ret_reg &ref[200]
	call_c   Dyam_Create_Atom("multiple modules activated ~w")
	move_ret ref[200]
	c_ret

;; TERM 199: current_module(_F)
c_code local build_ref_199
	ret_reg &ref[199]
	call_c   build_ref_778()
	call_c   Dyam_Create_Unary(&ref[778],V(5))
	move_ret ref[199]
	c_ret

;; TERM 778: current_module
c_code local build_ref_778
	ret_reg &ref[778]
	call_c   Dyam_Create_Atom("current_module")
	move_ret ref[778]
	c_ret

;; TERM 202: current_module(_B)
c_code local build_ref_202
	ret_reg &ref[202]
	call_c   build_ref_778()
	call_c   Dyam_Create_Unary(&ref[778],V(1))
	move_ret ref[202]
	c_ret

;; TERM 154: fx
c_code local build_ref_154
	ret_reg &ref[154]
	call_c   Dyam_Create_Atom("fx")
	move_ret ref[154]
	c_ret

;; TERM 115: [_K]
c_code local build_ref_115
	ret_reg &ref[115]
	call_c   Dyam_Create_List(V(10),I(0))
	move_ret ref[115]
	c_ret

;; TERM 114: 'new tag features directive ~w'
c_code local build_ref_114
	ret_reg &ref[114]
	call_c   Dyam_Create_Atom("new tag features directive ~w")
	move_ret ref[114]
	c_ret

;; TERM 113: tag_features(_B, _L, _M)
c_code local build_ref_113
	ret_reg &ref[113]
	call_c   build_ref_725()
	call_c   Dyam_Term_Start(&ref[725],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_End()
	move_ret ref[113]
	c_ret

;; TERM 112: tag_features(_B, _H, _J)
c_code local build_ref_112
	ret_reg &ref[112]
	call_c   build_ref_725()
	call_c   Dyam_Term_Start(&ref[725],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret ref[112]
	c_ret

;; TERM 111: tag((_E / _F))
c_code local build_ref_111
	ret_reg &ref[111]
	call_c   build_ref_589()
	call_c   build_ref_216()
	call_c   Dyam_Create_Unary(&ref[589],&ref[216])
	move_ret ref[111]
	c_ret

;; TERM 117: [_C,_D,_B]
c_code local build_ref_117
	ret_reg &ref[117]
	call_c   build_ref_110()
	call_c   Dyam_Create_Tupple(2,3,&ref[110])
	move_ret ref[117]
	c_ret

;; TERM 116: 'features top ~w or bot ~w not built on same non terminal ~w (directive tag_features)'
c_code local build_ref_116
	ret_reg &ref[116]
	call_c   Dyam_Create_Atom("features top ~w or bot ~w not built on same non terminal ~w (directive tag_features)")
	move_ret ref[116]
	c_ret

;; TERM 118: 'directive tag_features expects a symbol as first arg'
c_code local build_ref_118
	ret_reg &ref[118]
	call_c   Dyam_Create_Atom("directive tag_features expects a symbol as first arg")
	move_ret ref[118]
	c_ret

;; TERM 107: include(_F, file_info(_G, _H, _I))
c_code local build_ref_107
	ret_reg &ref[107]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_737()
	call_c   Dyam_Term_Start(&ref[737],3)
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_198()
	call_c   Dyam_Create_Binary(&ref[198],V(5),R(0))
	move_ret ref[107]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 108: file_info(_G, _D, _B)
c_code local build_ref_108
	ret_reg &ref[108]
	call_c   build_ref_737()
	call_c   Dyam_Term_Start(&ref[737],3)
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_End()
	move_ret ref[108]
	c_ret

;; TERM 106: read
c_code local build_ref_106
	ret_reg &ref[106]
	call_c   Dyam_Create_Atom("read")
	move_ret ref[106]
	c_ret

;; TERM 105: map_file(_B, _D)
c_code local build_ref_105
	ret_reg &ref[105]
	call_c   build_ref_779()
	call_c   Dyam_Create_Binary(&ref[779],V(1),V(3))
	move_ret ref[105]
	c_ret

;; TERM 779: map_file
c_code local build_ref_779
	ret_reg &ref[779]
	call_c   Dyam_Create_Atom("map_file")
	move_ret ref[779]
	c_ret

;; TERM 104: loaded(_D, _C)
c_code local build_ref_104
	ret_reg &ref[104]
	call_c   build_ref_780()
	call_c   Dyam_Create_Binary(&ref[780],V(3),V(2))
	move_ret ref[104]
	c_ret

;; TERM 780: loaded
c_code local build_ref_780
	ret_reg &ref[780]
	call_c   Dyam_Create_Atom("loaded")
	move_ret ref[780]
	c_ret

;; TERM 109: 'File not found ~w'
c_code local build_ref_109
	ret_reg &ref[109]
	call_c   Dyam_Create_Atom("File not found ~w")
	move_ret ref[109]
	c_ret

;; TERM 31: std
c_code local build_ref_31
	ret_reg &ref[31]
	call_c   Dyam_Create_Atom("std")
	move_ret ref[31]
	c_ret

;; TERM 10: '$clause'(_B)
c_code local build_ref_10
	ret_reg &ref[10]
	call_c   build_ref_781()
	call_c   Dyam_Create_Unary(&ref[781],V(1))
	move_ret ref[10]
	c_ret

;; TERM 781: '$clause'
c_code local build_ref_781
	ret_reg &ref[781]
	call_c   Dyam_Create_Atom("$clause")
	move_ret ref[781]
	c_ret

pl_code local fun14
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Object(R(0))
	call_c   Dyam_Tabule()
	pl_ret

pl_code local fun15
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Choice(fun14)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Subsume(R(0))
	fail_ret
	call_c   Dyam_Cut()
	pl_ret

pl_code local fun67
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Object(R(0))
	call_c   Dyam_Tabule()
	pl_ret

pl_code local fun318
	call_c   Dyam_Remove_Choice()
	move     &ref[659], R(0)
	move     0, R(1)
	move     &ref[110], R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_error_2()

pl_code local fun319
	call_c   Dyam_Update_Choice(fun318)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[658])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(12))
	call_c   Dyam_Reg_Load(2,V(2))
	move     &ref[657], R(4)
	move     S(5), R(5)
	pl_call  pred_check_tag_kind_3()
	call_c   Dyam_Reg_Load(0,V(13))
	call_c   Dyam_Reg_Load(2,V(2))
	move     &ref[657], R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  pred_check_tag_kind_3()

pl_code local fun320
	call_c   Dyam_Update_Choice(fun319)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[656])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(12))
	call_c   Dyam_Reg_Load(2,V(2))
	move     &ref[657], R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  pred_check_tag_kind_3()

pl_code local fun321
	call_c   Dyam_Update_Choice(fun320)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(1),&ref[654])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(2),&ref[655])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun322
	call_c   Dyam_Update_Choice(fun321)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(1),&ref[652])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(2),&ref[653])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun323
	call_c   Dyam_Update_Choice(fun322)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(1),&ref[650])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(2),&ref[651])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun310
	call_c   Dyam_Remove_Choice()
	pl_call  fun25(&seed[142],1)
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun311
	call_c   Dyam_Update_Choice(fun310)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(2),&ref[646])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(6))
	move     &ref[647], R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  pred_install_3()

pl_code local fun312
	call_c   Dyam_Update_Choice(fun311)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(2),&ref[645])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(6))
	move     &ref[589], R(4)
	move     0, R(5)
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  pred_install_3()

pl_code local fun313
	call_c   Dyam_Update_Choice(fun312)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(2),&ref[644])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(6))
	move     &ref[579], R(4)
	move     0, R(5)
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  pred_install_3()

pl_code local fun314
	call_c   Dyam_Update_Choice(fun313)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(2),&ref[643])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(6))
	call_c   Dyam_Reg_Load(4,V(3))
	pl_call  pred_install_3()
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(7))
	call_c   Dyam_Reg_Load(4,V(3))
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  pred_install_3()

pl_code local fun315
	call_c   Dyam_Update_Choice(fun314)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(2),&ref[639])
	fail_ret
	call_c   Dyam_Cut()
	pl_call  fun25(&seed[143],1)
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun316
	call_c   Dyam_Update_Choice(fun315)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(2),I(0))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun299
	call_c   Dyam_Remove_Choice()
	move     &ref[626], R(0)
	move     0, R(1)
	move     &ref[110], R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_error_2()

pl_code local fun300
	call_c   Dyam_Update_Choice(fun299)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[625])
	fail_ret
	call_c   Dyam_Cut()
	call_c   DYAM_evpred_functor(V(10),V(7),V(8))
	fail_ret
	move     &ref[624], R(0)
	move     S(5), R(1)
	move     V(11), R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Load(4,V(10))
	call_c   Dyam_Reg_Load(6,V(3))
	call_c   Dyam_Reg_Deallocate(4)
	pl_jump  pred_term_current_module_shift_4()

pl_code local fun301
	call_c   Dyam_Update_Choice(fun300)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[623])
	fail_ret
	call_c   Dyam_Cut()
	call_c   DYAM_evpred_functor(V(6),V(7),V(8))
	fail_ret
	move     &ref[624], R(0)
	move     S(5), R(1)
	move     V(9), R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Load(4,V(6))
	call_c   Dyam_Reg_Load(6,V(2))
	call_c   Dyam_Reg_Deallocate(4)
	pl_jump  pred_term_current_module_shift_4()

pl_code local fun302
	call_c   Dyam_Update_Choice(fun301)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[622])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(4))
	call_c   Dyam_Reg_Load(2,V(2))
	call_c   Dyam_Reg_Load(4,V(3))
	pl_call  pred_tag_anchor_analyse_equation_3()
	call_c   Dyam_Reg_Load(0,V(5))
	call_c   Dyam_Reg_Load(2,V(2))
	call_c   Dyam_Reg_Load(4,V(3))
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  pred_tag_anchor_analyse_equation_3()

pl_code local fun286
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Reg_Load_Ptr(2,V(5))
	fail_ret
	call_c   Dyam_Reg_Load(4,&ref[603])
	call_c   DyALog_Mutable_Write(R(2),&R(4),0)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(5))
	fail_ret
	call_c   DYAM_evpred_assert_1(&ref[601])
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun287
	call_c   Dyam_Remove_Choice()
	call_c   DYAM_evpred_functor(V(1),V(3),V(4))
	fail_ret
	call_c   Dyam_Choice(fun286)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[601])
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load_Ptr(2,V(5))
	fail_ret
	move     V(6), R(4)
	move     S(5), R(5)
	call_c   DyALog_Mutable_Read(R(2),&R(4))
	fail_ret
	call_c   Dyam_Reg_Load_Ptr(2,V(5))
	fail_ret
	call_c   Dyam_Reg_Load(4,&ref[602])
	call_c   DyALog_Mutable_Write(R(2),&R(4),0)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(5))
	fail_ret
	call_c   Dyam_Reg_Deallocate(4)
	pl_ret

pl_code local fun279
	call_c   Dyam_Remove_Choice()
	move     &ref[592], R(0)
	move     0, R(1)
	move     &ref[110], R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_error_2()

pl_code local fun280
	call_c   Dyam_Update_Choice(fun279)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_atom(V(1))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(2))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_read_file_2()

pl_code local fun281
	call_c   Dyam_Update_Choice(fun280)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(1),&ref[393])
	fail_ret
	call_c   Dyam_Cut()
	call_c   DYAM_Current_Input_1(V(5))
	fail_ret
	call_c   Dyam_Reg_Load(0,V(5))
	call_c   Dyam_Reg_Load(2,V(2))
	move     &ref[591], R(4)
	move     0, R(5)
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  pred_read_program_3()

pl_code local fun262
	call_c   Dyam_Remove_Choice()
	move     &ref[562], R(0)
	move     0, R(1)
	move     &ref[563], R(2)
	move     S(5), R(3)
	pl_call  pred_warning_2()
	pl_jump  fun261()

pl_code local fun263
	call_c   Dyam_Remove_Choice()
	pl_jump  fun261()

pl_code local fun248
	call_c   Dyam_Remove_Choice()
	move     &ref[538], R(0)
	move     0, R(1)
	move     &ref[110], R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_warning_2()

pl_code local fun249
	call_c   Dyam_Update_Choice(fun248)
	call_c   Dyam_Set_Cut()
	pl_call  fun25(&seed[135],1)
	call_c   Dyam_Cut()
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun244
	call_c   Dyam_Remove_Choice()
	call_c   DYAM_evpred_univ(V(1),V(2))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun245
	call_c   Dyam_Update_Choice(fun244)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[532])
	fail_ret
	call_c   Dyam_Cut()
	call_c   DYAM_evpred_univ(V(6),&ref[533])
	fail_ret
	call_c   Dyam_Unify(V(2),&ref[534])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun246
	call_c   Dyam_Update_Choice(fun245)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_atom(V(1))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(2),V(1))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun205
	call_c   Dyam_Remove_Choice()
	move     &ref[476], R(0)
	move     0, R(1)
	move     &ref[110], R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_error_2()

pl_code local fun206
	call_c   Dyam_Update_Choice(fun205)
	call_c   Dyam_Set_Cut()
	pl_call  fun25(&seed[132],1)
	call_c   Dyam_Cut()
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun207
	call_c   Dyam_Update_Choice(fun206)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[214])
	fail_ret
	call_c   Dyam_Cut()
	move     &ref[472], R(0)
	move     0, R(1)
	move     &ref[110], R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_warning_2()

pl_code local fun146
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun148
	call_c   Dyam_Remove_Choice()
	pl_fail

pl_code local fun150
	call_c   Dyam_Remove_Choice()
	pl_jump  fun149()

pl_code local fun141
	call_c   Dyam_Remove_Choice()
	move     &ref[330], R(0)
	move     0, R(1)
	move     &ref[331], R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_warning_2()

pl_code local fun142
	call_c   Dyam_Update_Choice(fun141)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(3),I(0))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun124
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(1),V(2))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun109
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(3))
	call_c   Dyam_Reg_Load(4,V(2))
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  pred_deep_module_shift_3()

pl_code local fun87
	call_c   Dyam_Remove_Choice()
fun86:
	call_c   Dyam_Cut()
	call_c   DYAM_evpred_retract(&ref[208])
	fail_ret
	move     &ref[209], R(0)
	move     0, R(1)
	move     &ref[210], R(2)
	move     S(5), R(3)
	pl_call  pred_warning_2()
fun85:
	call_c   DYAM_evpred_assert_1(V(11))
	call_c   Dyam_Deallocate()
	pl_ret



pl_code local fun88
	call_c   Dyam_Remove_Choice()
	pl_jump  fun85()

pl_code local fun91
	call_c   Dyam_Remove_Choice()
	move     &ref[211], R(0)
	move     0, R(1)
	move     &ref[212], R(2)
	move     S(5), R(3)
	pl_call  pred_error_2()
fun90:
	move     &ref[206], R(0)
	move     S(5), R(1)
	move     V(7), R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Load(4,V(3))
	move     V(8), R(6)
	move     S(5), R(7)
	pl_call  pred_term_current_module_shift_4()
	move     &ref[206], R(0)
	move     S(5), R(1)
	move     V(9), R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Load(4,V(4))
	move     V(10), R(6)
	move     S(5), R(7)
	pl_call  pred_term_current_module_shift_4()
	call_c   Dyam_Unify(V(11),&ref[207])
	fail_ret
	call_c   Dyam_Choice(fun88)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[208])
	call_c   Dyam_Choice(fun87)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(&ref[208],V(11))
	fail_ret
	call_c   Dyam_Cut()
	pl_fail


pl_code local fun92
	call_c   Dyam_Update_Choice(fun91)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_functor(V(4),V(5),V(6))
	fail_ret
	call_c   DYAM_evpred_functor(V(3),V(5),V(6))
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun90()

pl_code local fun93
	call_c   Dyam_Update_Choice(fun92)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_functor(V(3),V(5),V(6))
	fail_ret
	call_c   DYAM_evpred_functor(V(4),V(5),V(6))
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun90()

pl_code local fun95
	call_c   Dyam_Remove_Choice()
	move     &ref[213], R(0)
	move     0, R(1)
	move     &ref[214], R(2)
	move     S(5), R(3)
	pl_call  pred_error_2()
	pl_jump  fun94()

pl_code local fun97
	call_c   Dyam_Remove_Choice()
	move     &ref[215], R(0)
	move     0, R(1)
	move     &ref[110], R(2)
	move     S(5), R(3)
	pl_call  pred_error_2()
	pl_jump  fun96()

pl_code local fun80
	call_c   Dyam_Remove_Choice()
fun79:
	call_c   Dyam_Cut()
	move     &ref[193], R(0)
	move     0, R(1)
	move     &ref[194], R(2)
	move     S(5), R(3)
	pl_call  pred_error_2()
fun78:
	call_c   Dyam_Choice(fun77)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[195])
	call_c   Dyam_Choice(fun76)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(6),V(3))
	fail_ret
	call_c   Dyam_Cut()
	pl_fail



pl_code local fun76
	call_c   Dyam_Remove_Choice()
fun75:
	call_c   Dyam_Cut()
	move     &ref[196], R(0)
	move     0, R(1)
	move     &ref[197], R(2)
	move     S(5), R(3)
	pl_call  pred_error_2()
fun74:
	call_c   Dyam_Choice(fun73)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(4),&ref[198])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun72)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[199])
	call_c   Dyam_Choice(fun71)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(5),V(1))
	fail_ret
	call_c   Dyam_Cut()
	pl_fail



pl_code local fun71
	call_c   Dyam_Remove_Choice()
fun70:
	call_c   Dyam_Cut()
	move     &ref[200], R(0)
	move     0, R(1)
	move     &ref[201], R(2)
	move     S(5), R(3)
	pl_call  pred_error_2()
fun69:
	move     &ref[202], R(0)
	move     S(5), R(1)
	pl_call  pred_record_without_doublon_1()
fun68:
	move     &ref[203], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(2))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_install_2()




pl_code local fun72
	call_c   Dyam_Remove_Choice()
	pl_jump  fun69()

pl_code local fun73
	call_c   Dyam_Remove_Choice()
	pl_jump  fun68()

pl_code local fun77
	call_c   Dyam_Remove_Choice()
	pl_jump  fun74()

pl_code local fun81
	call_c   Dyam_Remove_Choice()
	pl_jump  fun78()

pl_code local fun83
	call_c   Dyam_Remove_Choice()
	move     &ref[204], R(0)
	move     0, R(1)
	move     &ref[110], R(2)
	move     S(5), R(3)
	pl_call  pred_error_2()
	pl_jump  fun82()

pl_code local fun39
	call_c   Dyam_Remove_Choice()
fun38:
	call_c   Dyam_Cut()
	call_c   DYAM_evpred_retract(&ref[113])
	fail_ret
	move     &ref[114], R(0)
	move     0, R(1)
	move     &ref[115], R(2)
	move     S(5), R(3)
	pl_call  pred_warning_2()
fun37:
	call_c   DYAM_evpred_assert_1(V(10))
	call_c   Dyam_Deallocate()
	pl_ret



pl_code local fun40
	call_c   Dyam_Remove_Choice()
	pl_jump  fun37()

pl_code local fun43
	call_c   Dyam_Remove_Choice()
	move     &ref[116], R(0)
	move     0, R(1)
	move     &ref[117], R(2)
	move     S(5), R(3)
	pl_call  pred_error_2()
fun42:
	move     &ref[111], R(0)
	move     S(5), R(1)
	move     V(6), R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Load(4,V(2))
	move     V(7), R(6)
	move     S(5), R(7)
	pl_call  pred_term_current_module_shift_4()
	move     &ref[111], R(0)
	move     S(5), R(1)
	move     V(8), R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Load(4,V(3))
	move     V(9), R(6)
	move     S(5), R(7)
	pl_call  pred_term_current_module_shift_4()
	call_c   Dyam_Unify(V(10),&ref[112])
	fail_ret
	call_c   Dyam_Choice(fun40)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[113])
	call_c   Dyam_Choice(fun39)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(&ref[113],V(10))
	fail_ret
	call_c   Dyam_Cut()
	pl_fail


pl_code local fun44
	call_c   Dyam_Update_Choice(fun43)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_functor(V(3),V(4),V(5))
	fail_ret
	call_c   DYAM_evpred_functor(V(2),V(4),V(5))
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun42()

pl_code local fun45
	call_c   Dyam_Update_Choice(fun44)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_functor(V(2),V(4),V(5))
	fail_ret
	call_c   DYAM_evpred_functor(V(3),V(4),V(5))
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun42()

pl_code local fun47
	call_c   Dyam_Remove_Choice()
	move     &ref[118], R(0)
	move     0, R(1)
	move     &ref[110], R(2)
	move     S(5), R(3)
	pl_call  pred_error_2()
	pl_jump  fun46()

pl_code local fun32
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(5),V(2))
	fail_ret
	call_c   Dyam_Unify(V(6),V(3))
	fail_ret
fun31:
	call_c   Dyam_Reg_Load(0,V(4))
	call_c   Dyam_Reg_Load(2,V(5))
	move     &ref[108], R(4)
	move     S(5), R(5)
	pl_call  pred_read_program_3()
	call_c   DYAM_Close_1(V(4))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret


pl_code local fun33
	call_c   Dyam_Remove_Choice()
	call_c   DYAM_evpred_assert_1(&ref[104])
	call_c   DYAM_evpred_assert_1(&ref[105])
	call_c   DYAM_Open_3(V(3),&ref[106],V(4))
	fail_ret
	call_c   Dyam_Choice(fun32)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(2),&ref[107])
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun31()

pl_code local fun35
	call_c   Dyam_Remove_Choice()
	move     &ref[109], R(0)
	move     0, R(1)
	move     &ref[110], R(2)
	move     S(5), R(3)
	pl_call  pred_error_2()
	pl_jump  fun34()


;;------------------ INIT POOL ------------

c_code local build_init_pool
	call_c   build_seed_104()
	call_c   build_seed_105()
	call_c   build_seed_106()
	call_c   build_seed_107()
	call_c   build_seed_108()
	call_c   build_seed_109()
	call_c   build_seed_110()
	call_c   build_seed_111()
	call_c   build_seed_112()
	call_c   build_seed_0()
	call_c   build_seed_1()
	call_c   build_seed_2()
	call_c   build_seed_3()
	call_c   build_seed_4()
	call_c   build_seed_5()
	call_c   build_seed_7()
	call_c   build_seed_6()
	call_c   build_seed_8()
	call_c   build_seed_44()
	call_c   build_seed_22()
	call_c   build_seed_15()
	call_c   build_seed_52()
	call_c   build_seed_14()
	call_c   build_seed_48()
	call_c   build_seed_62()
	call_c   build_seed_64()
	call_c   build_seed_74()
	call_c   build_seed_75()
	call_c   build_seed_16()
	call_c   build_seed_63()
	call_c   build_seed_24()
	call_c   build_seed_25()
	call_c   build_seed_91()
	call_c   build_seed_92()
	call_c   build_seed_94()
	call_c   build_seed_96()
	call_c   build_seed_46()
	call_c   build_seed_11()
	call_c   build_seed_12()
	call_c   build_seed_13()
	call_c   build_seed_61()
	call_c   build_seed_78()
	call_c   build_seed_81()
	call_c   build_seed_82()
	call_c   build_seed_83()
	call_c   build_seed_84()
	call_c   build_seed_85()
	call_c   build_seed_79()
	call_c   build_seed_80()
	call_c   build_seed_67()
	call_c   build_seed_93()
	call_c   build_seed_45()
	call_c   build_seed_65()
	call_c   build_seed_95()
	call_c   build_seed_41()
	call_c   build_seed_59()
	call_c   build_seed_58()
	call_c   build_seed_87()
	call_c   build_seed_89()
	call_c   build_seed_90()
	call_c   build_seed_9()
	call_c   build_seed_10()
	call_c   build_seed_102()
	call_c   build_seed_28()
	call_c   build_seed_29()
	call_c   build_seed_36()
	call_c   build_seed_43()
	call_c   build_seed_19()
	call_c   build_seed_17()
	call_c   build_seed_51()
	call_c   build_seed_77()
	call_c   build_seed_26()
	call_c   build_seed_57()
	call_c   build_seed_69()
	call_c   build_seed_86()
	call_c   build_seed_47()
	call_c   build_seed_66()
	call_c   build_seed_31()
	call_c   build_seed_60()
	call_c   build_seed_73()
	call_c   build_seed_88()
	call_c   build_seed_50()
	call_c   build_seed_68()
	call_c   build_seed_56()
	call_c   build_seed_23()
	call_c   build_seed_30()
	call_c   build_seed_18()
	call_c   build_seed_20()
	call_c   build_seed_21()
	call_c   build_seed_53()
	call_c   build_seed_70()
	call_c   build_seed_35()
	call_c   build_seed_101()
	call_c   build_seed_32()
	call_c   build_seed_54()
	call_c   build_seed_55()
	call_c   build_seed_100()
	call_c   build_seed_76()
	call_c   build_seed_37()
	call_c   build_seed_38()
	call_c   build_seed_39()
	call_c   build_seed_40()
	call_c   build_seed_27()
	call_c   build_seed_49()
	call_c   build_seed_72()
	call_c   build_seed_71()
	call_c   build_seed_99()
	call_c   build_seed_34()
	call_c   build_seed_42()
	call_c   build_seed_33()
	call_c   build_seed_103()
	call_c   build_seed_98()
	call_c   build_seed_97()
	call_c   build_seed_143()
	call_c   build_seed_142()
	call_c   build_seed_135()
	call_c   build_seed_132()
	call_c   build_seed_115()
	call_c   build_ref_649()
	call_c   build_ref_648()
	call_c   build_ref_651()
	call_c   build_ref_650()
	call_c   build_ref_653()
	call_c   build_ref_652()
	call_c   build_ref_655()
	call_c   build_ref_654()
	call_c   build_ref_656()
	call_c   build_ref_658()
	call_c   build_ref_659()
	call_c   build_ref_657()
	call_c   build_ref_639()
	call_c   build_ref_643()
	call_c   build_ref_579()
	call_c   build_ref_644()
	call_c   build_ref_589()
	call_c   build_ref_645()
	call_c   build_ref_647()
	call_c   build_ref_646()
	call_c   build_ref_621()
	call_c   build_ref_622()
	call_c   build_ref_623()
	call_c   build_ref_624()
	call_c   build_ref_625()
	call_c   build_ref_626()
	call_c   build_ref_600()
	call_c   build_ref_602()
	call_c   build_ref_601()
	call_c   build_ref_603()
	call_c   build_ref_604()
	call_c   build_ref_296()
	call_c   build_ref_591()
	call_c   build_ref_393()
	call_c   build_ref_592()
	call_c   build_ref_563()
	call_c   build_ref_562()
	call_c   build_ref_560()
	call_c   build_ref_561()
	call_c   build_ref_564()
	call_c   build_ref_538()
	call_c   build_ref_531()
	call_c   build_ref_534()
	call_c   build_ref_533()
	call_c   build_ref_532()
	call_c   build_ref_471()
	call_c   build_ref_472()
	call_c   build_ref_476()
	call_c   build_ref_100()
	call_c   build_ref_310()
	call_c   build_ref_331()
	call_c   build_ref_330()
	call_c   build_ref_302()
	call_c   build_ref_301()
	call_c   build_ref_205()
	call_c   build_ref_210()
	call_c   build_ref_209()
	call_c   build_ref_208()
	call_c   build_ref_207()
	call_c   build_ref_206()
	call_c   build_ref_212()
	call_c   build_ref_211()
	call_c   build_ref_214()
	call_c   build_ref_213()
	call_c   build_ref_215()
	call_c   build_ref_194()
	call_c   build_ref_193()
	call_c   build_ref_192()
	call_c   build_ref_197()
	call_c   build_ref_196()
	call_c   build_ref_195()
	call_c   build_ref_201()
	call_c   build_ref_200()
	call_c   build_ref_199()
	call_c   build_ref_202()
	call_c   build_ref_198()
	call_c   build_ref_203()
	call_c   build_ref_204()
	call_c   build_ref_155()
	call_c   build_ref_154()
	call_c   build_ref_115()
	call_c   build_ref_114()
	call_c   build_ref_113()
	call_c   build_ref_112()
	call_c   build_ref_111()
	call_c   build_ref_117()
	call_c   build_ref_116()
	call_c   build_ref_118()
	call_c   build_ref_107()
	call_c   build_ref_108()
	call_c   build_ref_106()
	call_c   build_ref_105()
	call_c   build_ref_104()
	call_c   build_ref_110()
	call_c   build_ref_109()
	call_c   build_ref_31()
	call_c   build_ref_10()
	c_ret

long local ref[782]
long local seed[144]

long local _initialization

c_code global initialization_dyalog_reader
	call_c   Dyam_Check_And_Update_Initialization(_initialization)
	fail_c_ret
	call_c   initialization_dyalog_format()
	call_c   build_init_pool()
	call_c   build_viewers()
	call_c   load()
	c_ret

