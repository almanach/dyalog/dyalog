/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 1998, 2003, 2004, 2008, 2011, 2016, 2018 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  application.pl -- Compiling Item x Trans applications
 *
 * ----------------------------------------------------------------
 * Description
 * 
 * ----------------------------------------------------------------
 */

:-include 'header.pl'.

build_closure( Trans, Env, Tupple, Closure::'$CLOSURE'(Fun,Fun_Env), Out_Env ) :-
	unfold( Trans, Env, Tupple, Code, Out_Env),
	label_code(allocate :> Code :> deallocate :> succeed,Fun),
	tupple(Trans, T_Trans),
	inter(Tupple,T_Trans,T1),
	extend_tupple(Env,T1,Fun_Env),

	% format('closure ~w\n',[Fun]),
	% oset_tupple(Fun_Env,XT),
	% oset_tupple(Tupple,XTupple),
	% format('CLOSURE outtupple=~w\n\ttrans=~w\n\tenv=~w\n\ttupple=~w\n',[XT,Trans,Env,XTupple]),

	true
	.

build_args_closure( Trans, callret(CallArgs,Args), Env, Tupple, Closure::'$CLOSURE'(Fun,Fun_Env), Out_Env ) :-
	extend_tupple(Args,Tupple,ArgsTupple),
	unfold( Trans, Env, ArgsTupple, Code_Cont, Out_Env),
	extend_tupple(Env,Tupple,InitTupple),

	tupple(Trans, T_Trans),
	union(T_Trans,InitTupple,U1),
	tupple(Args,T_Args),
	inter(U1,T_Args,U2),
	duplicate_vars(Args,Duplicate_Args),
	union(U2,Duplicate_Args,U3),
	tupple(CallArgs,T_CallArgs),
	delete(U3,T_CallArgs,U4),
	       
	extend_tupple(Args,T_Trans,T_ArgsTrans),
	inter(Tupple,T_ArgsTrans,T1),

	( null_tupple(U4) ->
	  Code = Cont_Cont
	;
	  %% start at 1, because X(0) is used when following closure (Follow_Cont) !
	  std_prolog_unif_args_alt(Args,1,Code,Code_Cont,InitTupple,U4)
	),
	label_code(allocate :> Code :> deallocate :> succeed,Fun),
	extend_tupple(Env,T1,Fun_Env),

	/*
	format('closure ~w\n',[Fun]),
	oset_tupple(Fun_Env,XT),
	oset_tupple(Tupple,XTupple),
	format('ARG CLOSURE outtupple=~w\n\ttrans=~w\n\targs=~w\n\tenv=~w\n\ttupple=~w\n',[XT,Trans,Args,Env,XTupple]),
	*/
	
	true
	.

:-std_prolog check_tag/3.

check_tag(Tag,N,Label) :-
	( Tag == [] ->  Label = N ; Label = [N,Tag] )
	.

init_install(Trans) :-
	null_tupple(Tupple),
	( Trans = '$loader'(Loader) ->
	    unfold(Loader,[],Tupple,Code1,_),
	    label_code(allocate :> Code1 :> deallocate :> succeed, Fun ),
	    Code = call(Fun,[])
	;   ((Trans = '*OBJECT*'(_) ; Trans = '*OBJECT*'(_,_)) ->
		Loader = Trans
	    ;	
		trans_unfold(Trans,Loader)
	    ),
	    unfold(Loader,[],Tupple,Code,_)
	),
	record( load_init(Code) )
	.


/**********************************************************************
 *  Applications
 **********************************************************************/

unfold( '*OBJECT*'(Trans), Env, T, Code, Env ) :-
	( Trans = seed{} ->
	    Seed = Trans
	;
	    Seed = seed{ model=>Trans }
	),
	seed_install( Seed, 0, Code )
	.

unfold( '*OBJECT*'(Trans,AppInfo), Env, T, Code, Env ) :-
	seed_install( seed{ model=>Trans,appinfo=>AppInfo }, 0, Code )
	.

unfold( '*JUMP*'(M,T0), Env, T1, Code :> succeed, Out_Env) :-
	mutable_read(M,V),
	inter(T1,T0,_T),
%	format('HANDLING JUMP M=~w ~w\n',[M,V]),
	( V = JMP::'*JUMP*'(Right) ->
%	    format( 'NEW JUMP ~w T1=~w T0~w _T=~w\n',[Right,XT1,XT0,_XT] ),
	    unfold(Right, Env, _T, Right_Code, Out_Env ),
	    jump_code(Right_Code :> deallocate :> succeed, Code),
	    mutable(M,[V,saved(_T,Code,Env,Out_Env)])
	; V = [JMP|Saved],
	  ( domain(saved(_T1,Code1,Unfold_Env,Out_Env),Saved),
	    inter(_T1,_T,_T),
	    true
	  ->
%	    format( 'SAVED JUMP ~w T1=~w T0~w _T=~w\n',[Right,XT1,XT0,_XT] ),
	    (	Env = Unfold_Env ->
		Code = Code1
	    ;	check_env_renaming(Env,Unfold_Env) ->
		label_term(Env,Env_L),
		label_term(Unfold_Env,Unfold_Env_L),
		Code = unify(Env_L,Unfold_Env_L) :> Code1
	    ;
		internal_error('~w unfolded\n\twith env ~w\n\tbut used with env ~w\n',
			       [Right,Unfold_Env,Env])
	    )
	  ; %format( 'NEW JUMP ~w T1=~w T0~w _T=~w\n',[Right,XT1,XT0,_XT] ),
	    length(Saved,SavedL),
	    ( SavedL > 20 ->
	      %% avoid long saved list
	      %% seems to arise for TAGs with a lot of guards
	      _TT = T0
	    ;
	      _TT = _T
	    ),
	    unfold(Right, Env, _TT, Right_Code, Out_Env ),
	    jump_code(Right_Code :> deallocate :> succeed, Code),
	    mutable(M,[JMP,saved(_TT,Code,Env,Out_Env)|Saved])
	  )
	),
	/*
	( V=[_|_] ->
	  length(V,XL)
	;
	  XL=0
	),
	oset_tupple(_T,_XT),
	format('END JUMP L=~w _T=~w:~w V=~w Code=~w Out_Env=~w\n',[XL,_T,_XT,V,Code,Out_Env]),
	*/
	true
	.



:-std_prolog check_env_renaming/2.

check_env_renaming(Env1,Env2) :-
	(   Env1 == Env2
	xor Env1 = '$VAR'(_), Env2 = '$VAR'(_)
	xor Env1 = [X1|L1],
	    Env2 = [X2|L2],
	    check_env_renaming(X1,X2),
	    check_env_renaming(L1,L2)
	xor Env1 =.. [F|L1], Env2 =.. [F|L2],
	    check_env_renaming(L1,L2)
	)
	.
	
	

unfold( '*ALTERNATIVE*'(R1,R2), Env, T, C, Out_Env1 ) :-
	unfold( R1 , Env, T, C1, Out_Env1 ),
	unfold( R2 :> deallocate :> succeed, Env, T, C2, Out_Env2 ),
	(   Out_Env1 == Out_Env2 xor
	internal_error('Unfolding alternatives do not return same env: ~w and ~w\n\tOut_Env1=~w\n\tOut_Env2=~w\n\tEnv=~w', [R1,R2,Out_Env1,Out_Env2,Env])),
	choice_code(C2, C1, C)
	.

unfold( '*KLEENE-ALTERNATIVE*'(Exit,Loop), Env, T, C, Out_Env1 ) :-
	unfold( Exit , Env, T, C1, Out_Env1 ),
	%% Do not need to check on out_env for Loop part
	%% because we do no exit from there
	unfold( Loop :> deallocate :> succeed, Env, T, C2, _ ),
%%	format('\nKleene Alt:\n\t*** Exit=~K\n\t*** Loop=~K\n',[C1,C2]),
%%	unfold( '*CONT*'(Loop), Env, T, C2, _ ),
	choice_code(C2, C1, C)
	.

unfold( '*CONT*'(Right),Env,T,C,Env) :-
	oset_tupple(T,Tupple),
%%	format('CONT Right=~w Tupple=~w Env=~w\n',[Right,Tupple,Env]),
	seed_install( OO::seed{ model => '*CONT*' :> Right(Env)(T),
				out_env=>[Out_Env],
				schedule => prolog
			      }, 12, C),
%%	format('-->CONT Object=~w\n',[OO]),
	true
	.

unfold( '*ALTERNATIVE-LAYER*'(R1,R2), Env, T, C, Out_Env1 ) :-
	unfold( R1 , Env, T, C1, Out_Env1 ),
	unfold( R2 :> deallocate_layer :> deallocate :> succeed, Env, T, C2, Out_Env2 ),
	(   Out_Env1 == Out_Env2  xor
	internal_error('Unfolding alternatives do not return same env: ~w and ~w\n\tOut_Env1=~w\n\tOut_Env2=~w', [R1,R2,Out_Env1,Out_Env2])),
	choice_code(C2, C1, C)
	.

unfold( R1 :> R2, Env, T, C1 :> C2 , Out_Env2) :-
	unfold( R1, Env, T, C1, Out_Env1 ),
	extend_tupple(R1,T,TT),
	unfold( R2, Out_Env1, TT, C2, Out_Env2 )
	.

unfold( noop,Env, _,noop,Env ).
unfold( succeed, Env, _, succeed, Env ).
unfold( deallocate, Env, _, deallocate, Env).
unfold( deallocate_alt, Env, _, deallocate_alt, Env).
unfold( allocate, Env, _, allocate, Env).
unfold( fail, Env , Cont , fail :> Cont, Env ).

unfold( unify(A,B), Env, _, unify(LA,LB), Env) :-
	label_term(A,LA),
	label_term(B,LB)
	.

unfold( foreign(Term,Fun), Env, _, foreign(Fun,LArgs), Env ) :-
	Term =.. [_|Args],
	label_term_list(Args,LArgs)
	.

unfold( foreign_void_return(Term,Fun), Env, _, foreign_void_return(Fun,LArgs), Env ) :-
	Term =.. [_|Args],
	label_term_list(Args,LArgs)
	.

unfold( builtin(Term,Fun), Env, _, call(Fun,LArgs), Env ) :-
	Term =.. [_|Args],
	label_term_list(Args,LArgs)
	.

unfold( O::failtest(_,_), Env, _,O, Env ).

unfold( '*LAST*'(A), Env, _, Code, Env ) :-
	seed_install( seed{ model=>'*RITEM*'(Env,A)}, 2, Code ).

unfold( '*LASTCALL*'(A), Env, _, Code, Env) :-
	seed_install( seed{ model=>'*CITEM*'(A,Env)}, 2, Code ).

unfold( T::'*ANSWER*'{}, Env, _, Code, Env) :-
	seed_install( seed{ model=>T }, 2, Code ).

unfold( '*NEXT*'{ call=>C, ret=>R, right=>Right, nabla=>N }, Env, T, C1 :> C2, Out_Env) :-	
        seed_install( seed{ model=>'*CITEM*'(C,C) }, 1, C1 ),
	check_tag(N,2,Label),
        seed_install( seed{ model=>'*RITEM*'(C,R) :> Right(Env)(T) ,
			    out_env=>[Out_Env] }, Label, C2 )
	.

% For predicate whose associated nabla should be tabulated
unfold( '*NEXTALT*'(C,R,Right,N), Env, T, C1 :> C2, Out_Env) :-	
        seed_install( seed{ model=>'*CITEM*'(C,C) }, 1, C1 ),
	check_tag(N,2,Label),
	seed_install( seed{ model=>'*RITEM*'(C,R) :> Right(Env)(T) ,
			    out_env=>[Out_Env],
			    subsume => yes
			  }, Label, C2 )
	.

% For predicate whose associated nabla should be tabulated but checked with variance
unfold( '*NEXTALT2*'(C,R,Right,N), Env, T, C1 :> C2, Out_Env) :-	
        seed_install( seed{ model=>'*CITEM*'(C,C) }, 1, C1 ),
	check_tag(N,2,Label),
	seed_install( seed{ model=>'*RITEM*'(C,R) :> Right(Env)(T) ,
			    out_env=>[Out_Env],
			    subsume => variance
			  }, Label, C2 )
	.


% For light tabulated predicates
unfold( '*LIGHTNEXT*'(C,R,Right,N), Env, T, Code, Out_Env) :-	
        seed_install( seed{ model=>'*CITEM*'(C,C), schedule=>prolog}, 1, C1 ),
	check_tag(N,2,Label),
        seed_install( seed{ model=>'*LIGHTRITEM*'(C,R) :> Right(Env)(T) ,
			    tabule => light,
			    schedule => prolog,
			    out_env=>[Out_Env] }, Label, C2 ),
	choice_code(C2 :> deallocate :> succeed, C1, Code)
	.

%% For wrapped light tabulated predicates
%% need a special case because of the alternative and current limitations
unfold( '*LIGHTNEXTALT*'(C,R,Right,N), Env, T, Code, Out_Env) :-	
        seed_install( seed{ model=>'*CITEM*'(C,C), schedule=>prolog}, 1, C1 ),
	check_tag(N,2,Label),
        seed_install( seed{ model=>'*RITEM*'(C,R) :> Right(Env)(T),
			    tabule => light,
			    schedule => prolog,
			    out_env=>[Out_Env] }, Label, C2 ),
	choice_code(C2 :> deallocate_layer :> deallocate :> succeed, C1 :> fail, Code)
%	Code = C1 :> C2
	.

unfold( '*LIGHTLAST*'(A), Env, _, Code, Env ) :-
	seed_install( seed{ model=>'*RITEM*'(Env,A), schedule => no}, 2, Code ).

unfold( '*LIGHTLCLAST*'(A), Env, _, Code, Env ) :-
	seed_install( seed{ model=>'*RITEM*'(Env,A), schedule => prolog }, 2, Code ).

unfold( '*LIGHTLASTCALL*'(A), Env, _, Code, Env) :-
	seed_install( seed{ model=>'*CITEM*'(A,Env), schedule => prolog }, 2, Code ).

unfold( '*PSEUDOLIGHTNEXT*'(C,R,Right,N), Env, T, Code, Out_Env) :-
	check_tag(N,2,Label),
	seed_install( seed{ model=>'*RITEM*'(C,R) :> Right(Env)(T),
			    tabule => light,
			    schedule => prolog,
			    out_env=>[Out_Env] }, Label, C2 ),
	%%	choice_code(C2 :> deallocate :> succeed, fail, Code),
	Code=C2
	.

unfold( '*LIGHTLCNEXT*'(C,R,Right,N,LCInfo), Env, T, Code, Out_Env) :-
	( LCInfo=[] ->
	    AppInfo=[]
	;   LCInfo = C^LC^(TRel,Rel),
	    unfold(Rel,[],T,RelCode,_),
	    unfold(TRel,[],T,TRelCode,_),
	    AppInfo = [key(select,[C,LC],TRelCode),key(lcselect,[C,LC],RelCode)]
	),
        seed_install( seed{ model=>'*CITEM*'(C,C),
			    schedule=>prolog,
			    appinfo=>AppInfo }, 1, C1 ),
	check_tag(N,2,Label),
        seed_install( seed{ model=>'*RITEM*'(C,R) :> Right(Env)(T) ,
			    tabule => light,
			    schedule => prolog,
			    out_env=>[Out_Env] }, Label, C2 ),
	choice_code(C2 :> deallocate_layer :> deallocate :> succeed, C1 :> fail, Code)
	.

unfold( Trans::'*LIGHTLCFIRST*'(C,Right,LCInfo), Env, T, Code, Out_Env ) :-
	( LCInfo=[] ->
	    AppInfo=[]
	;   LCInfo = C^LC^Rel,
	    unfold(Rel,[],T,RelCode,_),
	    AppInfo = [key(lcselect,[C,LC],RelCode)]
	),
	seed_install( Seed::seed{ model=> '*LCFIRST*'(C) :> Right(Env)(T),
				  schedule => prolog,
				  tabule => light,
				  appinfo=>AppInfo,
				  out_env=>[Out_Env]}, 2, Code )
	.

% For Left-Corner predicates
unfold( '*LCNEXT*'(C,R,Right,N,LCInfo), Env, T, C1 :> C2, Out_Env) :-
	( LCInfo=[] ->
	    AppInfo=[]
	;   LCInfo = C^LC^(TRel,Rel),
	    unfold(Rel,[],T,RelCode,_),
	    unfold(TRel,[],T,TRelCode,_),
	    AppInfo = [key(select,[C,LC],TRelCode),key(lcselect,[C,LC],RelCode)]
	),
        seed_install( seed{ model=>'*CITEM*'(C,C), appinfo=>AppInfo}, 1, C1 ),
	check_tag(N,2,Label),
        seed_install( seed{ model=>'*RITEM*'(C,R) :> Right(Env)(T) ,
			    out_env=>[Out_Env] }, Label, C2 )
	.

unfold( Trans::'*LCFIRST*'(C,Right,LCInfo), Env, T, Code, Out_Env ) :-
	( LCInfo=[] ->
	    AppInfo=[]
	;   LCInfo = C^LC^Rel,
	    unfold(Rel,[],T,RelCode,_),
	    AppInfo = [key(lcselect,[C,LC],RelCode)]
	),
	seed_install( Seed::seed{ model=> '*LCFIRST*'(C) :> Right(Env)(T),
			    appinfo=>AppInfo,
			    out_env=>[Out_Env]}, 2, Code )
	.

unfold( '*PSEUDONEXT*'(C,R,Right,N), Env, T, C2, Out_Env) :-
	check_tag(N,2,Label),
	seed_install( seed{ model=>'*RITEM*'(C,R) :> Right(Env)(T),
			    out_env=>[Out_Env] }, Label, C2 )
	.

unfold( '*WAIT*'(T), Env, Tupple, CT, Env) :-
	seed_install( seed{ model=>'*WAIT-CONT*'{ rest=> T,
						  cont=>Env,
						  tupple=> Tupple
						  }}, 12, CT )
	.

unfold( '*CUT*', Env, _, cut, Env).
unfold( '*SETCUT*', Env, _, setcut, Env).

unfold( '*PROLOG-LAST*', Env, _, call('Follow_Cont',[L_Env]), Env) :-
	label_term(Env,L_Env)
	.

unfold( '*PROLOG-LAST*'(A), Env, _, call('Follow_Cont',[L_A]), Env) :-
	label_term(A,L_A)
	.

unfold( '*PROLOG-LAST*'(A,Tag), Env, T, Code, Env ) :-
	check_tag(Tag,4,Label),
	seed_install( seed{ model=>'*PROLOG-ITEM*'{top=>A,cont=>Env}}, Label, Code )
	.

unfold( '*PROLOG*'{ call=> C, right=> Right }, Env, T1, Code, Out_Env) :-
        ( Right = '*PROLOG-LAST*' ->
            seed_install( seed{ model=>'*PROLOG-ITEM*'{top=>C,cont=>Env}}, 12, Code ),
	    Out_Env=Env
        ;
	    extend_tupple(C,T1,T2),
            build_closure(Right,Env,T2,Anchor,Out_Env),
            seed_install( seed{ model=>'*PROLOG-ITEM*'{ top=> C, cont=>Anchor }}
			, 12, Code)
        )
        .

unfold( '*GUARD*'(C), Env, _, Code, Env ) :-
	seed_install( seed{ model=>'*GUARD*'(C) }, 1, Code)
	.

unfold( '*INTERNAL_TAIL*'(Tail), Data, _, unify(V1,V2), Data ) :-
	label_term(Tail,V1),
	label_term(Data,V2)
	.

%% Need a safe exit when one wishes to block trans of last call to jump 
unfold( '*SAFE-EXIT*',Env,_,reg_reset(0) :> deallocate :> succeed,Env).

unfold('*KLEENE*'(Label,T1,Kleene),
       Env,
       T,
       CallCode,
       Out_Env
      ):-
%%	tupple(Kleene,TKleene),
	union(T1,T,TT1), TT1=TT,
%%	inter(TT1,TKLeene,TT),
	oset_tupple(T1,XT1),
	oset_tupple(T,XT),
%%	format('KLEENE label=~w Env=~w T1=~w T=~w\n',[Label,Env,XT1,XT]),
	( recorded( named_function(Fun,Label,local) ) ->
	  ( recorded( kleene(Label,TT,Env,Out_Env) ) ->
	    CallCode = call(Label,[])
	  ; recorded( kleene(Label,TT,_Env,Out_Env) ),
	    check_env_renaming(Env,_Env) ->
	    label_term(Env,Env_L),
	    label_term(_Env,_Env_L),
	    CallCode = unify(Env_L,_Env_L) :> call(Label,[])
	  ; 
	    unfold(Kleene,Env,TT,Kleene_Code,Out_Env),
	    record( kleene(Label,Env,Out_Env)),
	    CallCode = call(Label,[])
	  )
	; 
	  unfold(Kleene,Env,TT,Kleene_Code,Out_Env),
	  %%	jump_code(Kleene_Code,pl_jump(Fun,[])),
	  label_code(allocate :> Kleene_Code :> deallocate :> succeed,Fun),
	  %%	label_code(Kleene_Code,Fun),
	  %%	format('\nKleene: ~w => ~K\n',[Fun,Kleene_Code]),
	  record( kleene(Label,TT,Env,Out_Env) ),
	  record( named_function(Fun,Label,local) ),
	  CallCode = call(Label,[])
	)
	.


unfold('*SIMPLE-KLEENE*'(Label,T1,Kleene),
       Env,
       T,
       CallCode,
       Out_Env
      ):-
	tupple(Kleene,TKleene),
	inter(T,TKleene,TT1),
	union(T1,TT1,TT),
	oset_tupple(T1,XT1),
	oset_tupple(TT,XTT),
	format('KLEENE label=~w Env=~w T1=~w T=~w\n',[Label,Env,XT1,XTT]),
	( recorded( kleene(Label,_TT,Env,Out_Env,XLabel) ),
	  inter(_TT,TT,TT)
	  ->
	  CallCode = call(XLabel,[])
	; recorded( kleene(Label,_TT,_Env,Out_Env,XLabel) ),
	  check_env_renaming(Env,_Env),
	  inter(_TT,TT,TT) ->
	  label_term(Env,Env_L),
	  label_term(_Env,_Env_L),
	  CallCode = unify(Env_L,_Env_L) :> call(XLabel,[])
	; recorded( kleene(Label,_,_,_,_) ) -> 
	  unfold(Kleene,Env,TT,Kleene_Code,Out_Env),
	  label_code(allocate :> Kleene_Code :> deallocate :> succeed,Fun),
	  %%	label_code(Kleene_Code,Fun),
	  %%	format('\nKleene: ~w => ~K\n',[Fun,Kleene_Code]),
	  ( recorded( named_function(Fun,XLabel,local) )
	  xor
	  update_counter(kleene_supp, KleeneSuppId),
	  name_builder('~w_~w',[Label,KleeneSuppId],XLabel),
	  record( named_function(Fun,XLabel,local) )
	  ),
	  record( kleene(Label,TT,Env,Out_Env,XLabel) ),
	  CallCode = call(XLabel,[])
	; 
	  unfold(Kleene,Env,TT,Kleene_Code,Out_Env),
	  %%	jump_code(Kleene_Code,pl_jump(Fun,[])),
	  label_code(allocate :> Kleene_Code :> deallocate :> succeed,Fun),
	  %%	label_code(Kleene_Code,Fun),
	  %%	format('\nKleene: ~w => ~K\n',[Fun,Kleene_Code]),
	  record( kleene(Label,TT,Env,Out_Env,Label) ),
	  record( named_function(Fun,Label,local) ),
	  CallCode = call(Label,[])
	)
	.


unfold('*KLEENE-LAST*'(Label,Args,Params,TLoop,TOut,Right),Env,T,Code,Out_Env) :-
	/** Debug
	oset_tupple(TLoop,XTLoop),
	oset_tupple(TOut,XTOut),
	oset_tupple(T,XT),
	format('KLEENE ~w: args=~w tloop=~w:~w tout=~w t=~w\n',[Label,Args,TLoop,XTLoop,XTOut,XT]),
	**/
	inter(T,TOut,T2),
	union(TLoop,T2,T3),
	oset_tupple(T3,XTupple),
	append(Args,XTupple,AllArgs),
	append(Params,XTupple,AllParams),
	extend_tupple(Env,T,TT),
%%	TT=T,

	length([_|AllArgs],N),
	null_tupple(Void),
	( recorded( no_empty_kleene_body ) ->
	  unfold('*KLEENE-CALL*'(Label,AllParams):>Right,Env,TT,Jump_Code,Out_Env)
	;
	  unfold( '*CONT*'('*KLEENE-CALL*'(Label,AllParams):>Right),Env,TT,Jump_Code,Out_Env)
	),

	std_prolog_unif_args([Env|AllParams],0,Kleene_Call1,Jump_Code,Void),
	std_prolog_load_args([Env|AllArgs],0,Code,
			    reg_deallocate(N)
			    :> allocate_layer
			    :> allocate
			    :> Kleene_Call1
			    :> reg_reset(N)
			    ,
			     TT),
%%	format('\nKleene Last: ~K\n',[Code]),
	%%label_term(AllArgs,L_AllArgs),
	%% label_term(AllParam,L_AllParam),
	%% Loop only if entry param do no subsume output args
	%%   - should avoid the same loop that would avoid a recursive predicate
	%%   - but still missing a trace of the loop in the forest
	%% Code = failtest('DYAM_Subsumes_Chk_2',[L_AllParam,L_AllArgs]) :> Code1,
	true
	.

unfold('*KLEENE-CALL*'(Label,Params),Env,T,call(Label,[]),Env).

%% Just to extend the current tupple when unfolding
%% may be used when starting interleaving (we don't know which interleave branch will be followed)
unfold('*NOOP*'(TT),Env,T,noop,Env).

%% The first interleave alternative to be registered when starting interleaving
%% is actually the exit alternative.
%% To get the correct Out_Env to propagate outside the interleaving part,
%% this exit alternative should be unfolded last
unfold(K::'*IL-START*'(Trans),Env,T,C1 :> C2,Out_Env) :-
	(   Trans = (Exit :> Interleave)
	xor internal_error('Bad interleave transition: ~w\n',[K])
	),
	unfold(Interleave,Env,T,C2,Env2),
	extend_tupple(Interleave,T,TT),
	unfold(Exit,Env2,TT,C1,Out_Env)
	.

unfold('$TUPPLE'(_,Trans),T,Env,C,Out_Env) :-
	unfold(Trans,T,Env,C,Out_Env).

unfold(K::'*IL-ALT*'(Var,Jump,Jump_Args,VLoop),
       Env,
       T,
       unify(Var,L_Closure),
       Out_Env
      ) :-
%%	format('Unfold ~w\n\tEnv=~w\n',[K,Env]),
	/*
	(   Env = Out_Env xor
	internal_error('Sorry, but DyALog does not handle interleave alternatives that modify Env:\n\tIn_Env=~w\n\tOut_Env=~w',[Env,Out_Env])),
        */
	tupple(Jump,T2),
	extend_tupple(Jump_Args,T2,T3),
	inter(T,T3,T4),
	tupple_delete(T4,VLoop,T5),
	extend_tupple(Env,T5,T6),
	unfold('*IL-THREAD*'(Jump,Jump_Args),Env,T,Code_Jump,Out_Env),
	label_code(Code_Jump,Code_Fun),
%%	oset_tupple(T6,XT6),
%%	oset_tupple(T,XT),
%%        format('Thread jargs=~w vloop=~w tupple=~w reftupple=~w env=~w\n',[Jump_Args,VLoop,XT6,XT,Env]),
	Closure = '$CLOSURE'(Code_Fun,T6),
	label_term(Closure,L_Closure)
	.

unfold('*IL-THREAD*'(Jump,Jump_Args),Env,T,Jump_Code,Out_Env) :-
	extend_tupple(Jump_Args,T,T2),
	unfold(allocate :> Jump :> deallocate :> succeed,Env,T2,Jump_Code1,Out_Env),
	std_prolog_unif_args(Jump_Args,1,Jump_Code, Jump_Code1, T),
	oset_tupple(T,XT),
%%	format('IL THREAD jump=~w\n\targs=~w\n\ttupple=~w\n\tenv=~w\n\t=>~w\n',[Jump,Jump_Args,XT,Env,Jump_Code]),
	true
	.

unfold(K::'*IL-LOOP*'(Jump,Jump_Args::[IL_Pos|_]),Env,T,Code,Env) :-
	length(Jump_Args,N),
	M is N+1,
	std_prolog_load_args(Jump_Args,1,Code,
			     call('Follow_Cont',[Jump]) :> reg_reset(M),
			     T)
	.

unfold('*IL-LOOP-WRAP*'(Trans),Env,T,Code,Out_Env) :-
	unfold(Trans,Env,T,Code,Out_Env)
	.


%% A hack to hide some variables from objects when using new_gen_tail
unfold('*HIDE-ARGS*'(_,Trans),Env,T,Code,Out_Env) :-
	unfold(Trans,Env,T,Code,Out_Env).

unfold_std_trans( Trans,
		  item_unify(V) :> allocate :> Rest,
		  Out_Env
		) :-
	(
	 Trans = (Item :> (IndexingKey ^ _Cont(Env)(T1)))
	xor Trans = (Item :> _Cont(Env)(T1))
	xor internal_error('Bad transition format ~w',[Trans])),
	( _Cont = '*HIDE-ARGS*'(_,Cont)
	xor Cont = _Cont
	),
	extend_tupple(Item,T1,T2),
	label_term(Item,V),
	unfold( Cont :> deallocate :> succeed,
		Env,
		T2,
		Rest,
		Out_Env)
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Configuration of DyALog Applications
%%
%% Standard DyALog Applications combines two objects and apply some code
%% The following rules for 'app_model' modelize these applications
%% They are entered in three different modes {item,trans,gen}
%% 
%%        mode init: used to compute the number of possible applications
%%        associated to each kind of object
%%
%%        mode item: used to build the application code for an Item
%%        (and for a given app_model). Item is supposed to be ground.
%%
%%        mode trans: used to build the application code for a Trans
%%        (and for a given app_model). Trans is supposed to be ground.
%%
%%        NOTE: '$VAR'(0) is supposed not used in Item or Trans and may be used
%%        for Env when needed.
%%
%%  There are also a few special applications (WAIT-CONT) that need
%%  only one object (Trans)

app_model( application{ mode=> M,

			key => key(select,Info,InitCode),
			
			item=>   Item::'*CITEM*'(CallX,PopX),
			trans=>  Trans::('*FIRST*'(Call) :> Right),

			item_comp=>   ItemComp::'*CITEM*'(Call,Pop),
    
			code=> ( item_unify(V) :> allocate :> Rest ),

			out_env => Out_Env
		      }
	 ) :-
	( M == trans ->
	    Item=ItemComp,
	    zero_var(Pop),
	    tupple(Call,T),
	    label_term( Item , V), 
	    unfold(Right :> deallocate :> succeed,
		   Pop,
		   T,
		   Rest,
		   Out_Env)
	;   M == item ->
	    (	Info = [] ->
		Item=ItemComp
	    ;	Info = [CallX,Call],
		Pop=Call,
		label_term(CallX,CallX_Ref),
		label_term(Call,Call_Ref),
		label_term(Pop,Pop_Ref),
		Item_Key='$system'(item_k),
		label_code(unify(CallX_Ref,Item_Key,Call_Ref,Item_Key) :> unify(Pop_Ref,Item_Key,Call_Ref,Item_Key) :> succeed,
			   InitCode)
	    ),
	    holes(Trans),
	    Out_Env=[]
	;   
	    true
	)
	.

app_model( application{ mode=> M,

			key => key(lcselect,Info,InitCode),
			
			item=>   Item::'*CITEM*'(CallX,PopX),
			trans=>  Trans::('*LCFIRST*'(Call) :> Closure),

			item_comp=>   ItemComp::CallY,
    
			code=> allocate :> Code :> item_unify(V) :> Rest,

			out_env => Out_Env
		      }
	 ) :-
	recorded(left_corner),
	( M == trans ->
	    (Info = [] ->
		Call=CallY,
		Call=CallX,
		Code=noop
	    ;	
		Info = [Call,CallX],
		CallX=CallY,
		reg_value(Reg,0),
		Code = call(Reg,[]),
		label_term(Call,Call_Ref),
		label_term(CallY,CallY_Ref),
		label_code(unify(Call_Ref,CallY_Ref) :> succeed, InitCode)
	    ),
	    holes(Item),
	    Closure=Cont(Env)(T1),
	    extend_tupple(CallY,T1,T2),
	    label_term(CallY,V),
	    unfold( Cont :> deallocate :> succeed,
		    Env,
		    T2,
		    Rest,
		    Out_Env)
	;   M == item ->
	    ( Info = [] ->
		Call=CallY,
		Call=CallX
	    ;	Info = [CallX,Call],
		CallY=Call,
		label_term(CallX,CallX_Ref),
		label_term(CallY,CallY_Ref),
		Item_Key='$system'(item_k),
		label_code(unify(CallX_Ref,Item_Key,CallY_Ref,Item_Key) :> succeed,
			   InitCode)
	    ),
	    holes(Trans),
	    Out_Env=[]
	;   
	    true
	)
	.

app_model( application{ mode=> M,

			key => key(select,Info,InitCode),
			
			item=>   Item::'*LIGHTCITEM*'(CallX,PopX),
			trans=>  Trans::('*LIGHTFIRST*'(Call) :> Right),

			item_comp=>   ItemComp::'*LIGHTCITEM*'(Call,Pop),
    
			code=> ( item_unify(V) :> allocate :> Rest ),

			out_env => Out_Env
		      }
	 ) :-
	recorded(left_corner),
	( M == trans ->
	    Item=ItemComp,
	    zero_var(Pop),
	    tupple(Call,T),
	    label_term( Item , V), 
	    unfold(Right :> deallocate :> succeed,
		   Pop,
		   T,
		   Rest,
		   Out_Env)
	;   M == item ->
	    (	Info = [] ->
		Item=ItemComp
	    ;	Info = [CallX,Call],
		Pop=Call,
		label_term(CallX,CallX_Ref),
		label_term(Call,Call_Ref),
		label_term(Pop,Pop_Ref),
		Item_Key='$system'(item_k),
		label_code(unify(CallX_Ref,Item_Key,Call_Ref,Item_Key) :> unify(Pop_Ref,Item_Key,Call_Ref,Item_Key) :> succeed,
			   InitCode)
	    ),
	    holes(Trans),
	    Out_Env=[]
	;   
	    true
	)
	.

app_model( application{ mode=> M,

			key => key(lcselect,Info,InitCode),
			
			item=>   Item::'*LIGHTCITEM*'(CallX,PopX),
			trans=>  Trans::('*LIGHTLCFIRST*'(Call) :> Closure),

			item_comp=>   ItemComp::CallY,
    
			code=> allocate :> Code :> item_unify(V) :> Rest,

			out_env => Out_Env
		      }
	 ) :-
	recorded(left_corner),
	( M == trans ->
	    (Info = [] ->
		Call=CallY,
		Call=CallX,
		Code=noop
	    ;	
		Info = [Call,CallX],
		CallX=CallY,
		reg_value(Reg,0),
		Code = call(Reg,[]),
		label_term(Call,Call_Ref),
		label_term(CallY,CallY_Ref),
		label_code(unify(Call_Ref,CallY_Ref) :> succeed, InitCode)
	    ),
	    holes(Item),
	    Closure=Cont(Env)(T1),
	    extend_tupple(CallY,T1,T2),
	    label_term(CallY,V),
	    unfold( Cont :> deallocate :> succeed,
		    Env,
		    T2,
		    Rest,
		    Out_Env)
	;   M == item ->
	    ( Info = [] ->
		Call=CallY,
		Call=CallX
	    ;	Info = [CallX,Call],
		CallY=Call,
		label_term(CallX,CallX_Ref),
		label_term(CallY,CallY_Ref),
		Item_Key='$system'(item_k),
		label_code(unify(CallX_Ref,Item_Key,CallY_Ref,Item_Key) :> succeed,
			   InitCode)
	    ),
	    holes(Trans),
	    Out_Env=[]
	;   
	    true
	)
	.

app_model( application{ mode=> M,

			item=>   Item::'*PROLOG-ITEM*'{top=>Call, cont=>Pop},
			trans=>  Trans::('*PROLOG-FIRST*'(Call) :> Right),

			item_comp=>   Item,
    
			code=> ( item_unify(V) :> allocate :> Rest )

		      ,	restrict=>Restrict,

			out_env => Out_Env

		      }
	 ) :-
	( M == trans ->
	    zero_var(Pop),
	    tupple(Call,T),
	    Restrict = yes,           %%  No need for TRANS to find ITEM
	    label_term( Item , V), 
	    unfold(Right :> deallocate :> succeed,
		   Pop,
		   T,
		   Rest,
		   Out_Env)
	;   M == item -> 
	    holes(Trans),
	    Out_Env=[]
	;   
	    true
	)
	.

app_model( application{ mode=> M,

			item=>  	Item::'*GUARD*'(Call),
			trans=> 	Trans::('*GUARD*'(Call) :> Right),

			item_comp=>   Item,
    
			code=> ( item_unify(V) :> allocate :> Rest )

		      ,	restrict=>Restrict,

			out_env=> Out_Env

		      }
	 ) :-
	( M == trans ->
	    Restrict = yes,           %%  No need for TRANS to find ITEM
	    label_term( Item , V),
	    tupple(Call,T),
	    unfold(Right :> deallocate :> succeed,
		   [],
		   T,
		   Rest,
		   Out_Env)
	;   M == item -> 
	    holes(Trans),
	    Out_Env=[]
	;   
	    true
	)
	.

app_model( application{ mode=> M,
			item=>   	Item::'*RITEM*'(Call,Ret),
			trans=> 	Trans::(Item :> Closure),
			
			item_comp=>    Item,

			code=> Code,

			out_env => Out_Env
		      
		      }
	 ) :-
	( M == trans ->
	    unfold_std_trans(Trans,Code,Out_Env)
	;   M == item ->
	    holes( Trans ),
	    Out_Env = []
	;
	    true
	)
	.

app_model( application{ mode=> M,
			item=>   	Item::'*LIGHTRITEM*'(Call,Ret),
			trans=> 	Trans::(Item :> Closure),
			
			item_comp=>    Item,

			code=> Code,

			out_env => Out_Env
		      
		      }
	 ) :-
	( M == trans ->
	    unfold_std_trans(Trans,Code,Out_Env)
	;   M == item ->
	    holes( Trans ),
	    Out_Env = []
	;
	    true
	)
	.

special_app_model( application{ mode=>M,
				item=>none,
				trans=> Trans::'*WAIT-CONT*'{ rest=>A , cont=>Env, tupple=>T},
				trans_id=>0,
				
				code => C,

				out_env => Out_Env

			      }
		 ) :-
	( M == trans ->
	    unfold(A,Env,T,C,Out_Env)
	;   M == item ->
	    fail,
	    Out_Env=[]
	;
	    true
	)
	.



special_app_model( application{ mode=>M,
				item => none,
				trans => Trans::('*CONT*' :> Cont(Env)(T) ),
				trans_id => 0,
				code => allocate :> Code,
				out_env => Out_Env
			      }
		 ) :-
    ( M == trans ->
	  unfold(Cont :> deallocate :> succeed,Env,T,Code,Out_Env),
	  true
	;   M == item ->
	    fail,
	    Out_Env = []
	;
	    true
	)
	.

