;; Compiler: DyALog 1.14.0
;; File "ma_emit.pl"


;;------------------ LOADING ------------

c_code local load
	call_c   Dyam_Loading_Set()
	call_c   Dyam_Loading_Reset()
	c_ret

pl_code global pred_ma_emit_table_4
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(4)
	call_c   Dyam_Choice(fun23)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(4),I(0))
	fail_ret
	call_c   Dyam_Cut()
	move     &ref[25], R(0)
	move     0, R(1)
	move     &ref[26], R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_format_2()

pl_code global pred_ma_emit_pl_call_2
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(2)
	call_c   Dyam_Choice(fun21)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Reg_Load(0,V(1))
	move     V(3), R(2)
	move     S(5), R(3)
	pl_call  pred_reg_value_2()
	call_c   Dyam_Cut()
	move     &ref[23], R(0)
	move     0, R(1)
	move     &ref[9], R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_format_2()

pl_code global pred_ma_emit_pl_jump_2
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(2)
	call_c   Dyam_Choice(fun19)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Reg_Load(0,V(1))
	move     V(3), R(2)
	move     S(5), R(3)
	pl_call  pred_reg_value_2()
	call_c   Dyam_Cut()
	move     &ref[21], R(0)
	move     0, R(1)
	move     &ref[9], R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_format_2()

pl_code global pred_ma_emit_move_2
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(2)
	move     &ref[20], R(0)
	move     0, R(1)
	move     &ref[14], R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_format_2()

pl_code global pred_ma_emit_call_c_2
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(2)
	move     &ref[19], R(0)
	move     0, R(1)
	move     &ref[14], R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_format_2()

pl_code global pred_ma_emit_cache_or_build_2
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(2)
	move     &ref[18], R(0)
	move     0, R(1)
	move     &ref[14], R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_format_2()

pl_code global pred_ma_emit_jump_reg_2
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(2)
	move     &ref[17], R(0)
	move     0, R(1)
	move     &ref[14], R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_format_2()

pl_code global pred_ma_emit_pl_code_2
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(2)
	move     &ref[16], R(0)
	move     0, R(1)
	move     &ref[14], R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_format_2()

pl_code global pred_ma_emit_code_2
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(2)
	move     &ref[15], R(0)
	move     0, R(1)
	move     &ref[14], R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_format_2()

pl_code global pred_ma_emit_ident_2
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(2)
	move     &ref[13], R(0)
	move     0, R(1)
	move     &ref[14], R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_format_2()

pl_code global pred_ma_emit_label_1
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	move     &ref[12], R(0)
	move     0, R(1)
	move     &ref[9], R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_format_2()

pl_code global pred_ma_emit_jump_1
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	move     &ref[11], R(0)
	move     0, R(1)
	move     &ref[9], R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_format_2()

pl_code global pred_ma_emit_move_ret_1
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	move     &ref[10], R(0)
	move     0, R(1)
	move     &ref[9], R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_format_2()

pl_code global pred_ma_emit_ret_reg_1
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	move     &ref[8], R(0)
	move     0, R(1)
	move     &ref[9], R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_format_2()

pl_code global pred_ma_emit_jump_ret_0
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   DYAM_Write_2(I(0),&ref[7])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code global pred_ma_emit_fail_ret_0
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   DYAM_Write_2(I(0),&ref[6])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code global pred_ma_emit_true_ret_0
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   DYAM_Write_2(I(0),&ref[5])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code global pred_ma_emit_fail_c_ret_0
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   DYAM_Write_2(I(0),&ref[4])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code global pred_ma_emit_true_c_ret_0
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   DYAM_Write_2(I(0),&ref[3])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code global pred_ma_emit_ret_0
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   DYAM_Write_2(I(0),&ref[2])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code global pred_ma_emit_pl_fail_0
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   DYAM_Write_2(I(0),&ref[1])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code global pred_ma_emit_pl_ret_0
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   DYAM_Write_2(I(0),&ref[0])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret


;;------------------ VIEWERS ------------

c_code local build_viewers
	c_ret

;; TERM 26: [_B,_C,_D]
c_code local build_ref_26
	ret_reg &ref[26]
	call_c   Dyam_Create_Tupple(1,3,I(0))
	move_ret ref[26]
	c_ret

;; TERM 25: 'long ~w ~x[~w]\n'
c_code local build_ref_25
	ret_reg &ref[25]
	call_c   Dyam_Create_Atom("long ~w ~x[~w]\n")
	move_ret ref[25]
	c_ret

;; TERM 28: [_B,_C,_D,_E]
c_code local build_ref_28
	ret_reg &ref[28]
	call_c   Dyam_Create_Tupple(1,4,I(0))
	move_ret ref[28]
	c_ret

;; TERM 27: 'long ~w ~x[~w]=[~X]\n'
c_code local build_ref_27
	ret_reg &ref[27]
	call_c   Dyam_Create_Atom("long ~w ~x[~w]=[~X]\n")
	move_ret ref[27]
	c_ret

;; TERM 23: '\tpl_call  ~a\n'
c_code local build_ref_23
	ret_reg &ref[23]
	call_c   Dyam_Create_Atom("\tpl_call  ~a\n")
	move_ret ref[23]
	c_ret

;; TERM 24: '\tpl_call  ~x(~A)\n'
c_code local build_ref_24
	ret_reg &ref[24]
	call_c   Dyam_Create_Atom("\tpl_call  ~x(~A)\n")
	move_ret ref[24]
	c_ret

;; TERM 21: '\tpl_jump  ~a\n'
c_code local build_ref_21
	ret_reg &ref[21]
	call_c   Dyam_Create_Atom("\tpl_jump  ~a\n")
	move_ret ref[21]
	c_ret

;; TERM 22: '\tpl_jump  ~x(~A)\n'
c_code local build_ref_22
	ret_reg &ref[22]
	call_c   Dyam_Create_Atom("\tpl_jump  ~x(~A)\n")
	move_ret ref[22]
	c_ret

;; TERM 20: '\tmove     ~a, ~x\n'
c_code local build_ref_20
	ret_reg &ref[20]
	call_c   Dyam_Create_Atom("\tmove     ~a, ~x\n")
	move_ret ref[20]
	c_ret

;; TERM 19: '\tcall_c   ~x(~A)\n'
c_code local build_ref_19
	ret_reg &ref[19]
	call_c   Dyam_Create_Atom("\tcall_c   ~x(~A)\n")
	move_ret ref[19]
	c_ret

;; TERM 18: '\tcache_or_build   ~a,~x\n'
c_code local build_ref_18
	ret_reg &ref[18]
	call_c   Dyam_Create_Atom("\tcache_or_build   ~a,~x\n")
	move_ret ref[18]
	c_ret

;; TERM 17: '\tjmp_reg ~a,~x\n'
c_code local build_ref_17
	ret_reg &ref[17]
	call_c   Dyam_Create_Atom("\tjmp_reg ~a,~x\n")
	move_ret ref[17]
	c_ret

;; TERM 16: 'pl_code ~w ~x\n'
c_code local build_ref_16
	ret_reg &ref[16]
	call_c   Dyam_Create_Atom("pl_code ~w ~x\n")
	move_ret ref[16]
	c_ret

;; TERM 15: 'c_code ~w ~x\n'
c_code local build_ref_15
	ret_reg &ref[15]
	call_c   Dyam_Create_Atom("c_code ~w ~x\n")
	move_ret ref[15]
	c_ret

;; TERM 14: [_B,_C]
c_code local build_ref_14
	ret_reg &ref[14]
	call_c   Dyam_Create_Tupple(1,2,I(0))
	move_ret ref[14]
	c_ret

;; TERM 13: 'long ~w ~x\n'
c_code local build_ref_13
	ret_reg &ref[13]
	call_c   Dyam_Create_Atom("long ~w ~x\n")
	move_ret ref[13]
	c_ret

;; TERM 12: '~x:\n'
c_code local build_ref_12
	ret_reg &ref[12]
	call_c   Dyam_Create_Atom("~x:\n")
	move_ret ref[12]
	c_ret

;; TERM 11: '\tjump     ~x\n'
c_code local build_ref_11
	ret_reg &ref[11]
	call_c   Dyam_Create_Atom("\tjump     ~x\n")
	move_ret ref[11]
	c_ret

;; TERM 10: '\tmove_ret ~x\n'
c_code local build_ref_10
	ret_reg &ref[10]
	call_c   Dyam_Create_Atom("\tmove_ret ~x\n")
	move_ret ref[10]
	c_ret

;; TERM 9: [_B]
c_code local build_ref_9
	ret_reg &ref[9]
	call_c   Dyam_Create_List(V(1),I(0))
	move_ret ref[9]
	c_ret

;; TERM 8: '\tret_reg ~a\n'
c_code local build_ref_8
	ret_reg &ref[8]
	call_c   Dyam_Create_Atom("\tret_reg ~a\n")
	move_ret ref[8]
	c_ret

;; TERM 7: '\tjump_ret\n'
c_code local build_ref_7
	ret_reg &ref[7]
	call_c   Dyam_Create_Atom("\tjump_ret\n")
	move_ret ref[7]
	c_ret

;; TERM 6: '\tfail_ret\n'
c_code local build_ref_6
	ret_reg &ref[6]
	call_c   Dyam_Create_Atom("\tfail_ret\n")
	move_ret ref[6]
	c_ret

;; TERM 5: '\ttrue_ret\n'
c_code local build_ref_5
	ret_reg &ref[5]
	call_c   Dyam_Create_Atom("\ttrue_ret\n")
	move_ret ref[5]
	c_ret

;; TERM 4: '\tfail_c_ret\n'
c_code local build_ref_4
	ret_reg &ref[4]
	call_c   Dyam_Create_Atom("\tfail_c_ret\n")
	move_ret ref[4]
	c_ret

;; TERM 3: '\ttrue_c_ret\n'
c_code local build_ref_3
	ret_reg &ref[3]
	call_c   Dyam_Create_Atom("\ttrue_c_ret\n")
	move_ret ref[3]
	c_ret

;; TERM 2: '\tc_ret\n'
c_code local build_ref_2
	ret_reg &ref[2]
	call_c   Dyam_Create_Atom("\tc_ret\n")
	move_ret ref[2]
	c_ret

;; TERM 1: '\tpl_fail\n'
c_code local build_ref_1
	ret_reg &ref[1]
	call_c   Dyam_Create_Atom("\tpl_fail\n")
	move_ret ref[1]
	c_ret

;; TERM 0: '\tpl_ret\n'
c_code local build_ref_0
	ret_reg &ref[0]
	call_c   Dyam_Create_Atom("\tpl_ret\n")
	move_ret ref[0]
	c_ret

pl_code local fun23
	call_c   Dyam_Remove_Choice()
	move     &ref[27], R(0)
	move     0, R(1)
	move     &ref[28], R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_format_2()

pl_code local fun21
	call_c   Dyam_Remove_Choice()
	move     &ref[24], R(0)
	move     0, R(1)
	move     &ref[14], R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_format_2()

pl_code local fun19
	call_c   Dyam_Remove_Choice()
	move     &ref[22], R(0)
	move     0, R(1)
	move     &ref[14], R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_format_2()


;;------------------ INIT POOL ------------

c_code local build_init_pool
	call_c   build_ref_26()
	call_c   build_ref_25()
	call_c   build_ref_28()
	call_c   build_ref_27()
	call_c   build_ref_23()
	call_c   build_ref_24()
	call_c   build_ref_21()
	call_c   build_ref_22()
	call_c   build_ref_20()
	call_c   build_ref_19()
	call_c   build_ref_18()
	call_c   build_ref_17()
	call_c   build_ref_16()
	call_c   build_ref_15()
	call_c   build_ref_14()
	call_c   build_ref_13()
	call_c   build_ref_12()
	call_c   build_ref_11()
	call_c   build_ref_10()
	call_c   build_ref_9()
	call_c   build_ref_8()
	call_c   build_ref_7()
	call_c   build_ref_6()
	call_c   build_ref_5()
	call_c   build_ref_4()
	call_c   build_ref_3()
	call_c   build_ref_2()
	call_c   build_ref_1()
	call_c   build_ref_0()
	c_ret

long local ref[29]
long local seed[0]

long local _initialization

c_code global initialization_dyalog_ma_5Femit
	call_c   Dyam_Check_And_Update_Initialization(_initialization)
	fail_c_ret
	call_c   initialization_dyalog_format()
	call_c   build_init_pool()
	call_c   build_viewers()
	call_c   load()
	c_ret

