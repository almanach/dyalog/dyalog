/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2002 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  lc.pl -- Left Corner Relation Computation
 *
 * ----------------------------------------------------------------
 * Description
 *  Use the results of the PDA pass of the compiler to compute
 *  a left corner relation
 * ----------------------------------------------------------------
 */

:-include 'header.pl'.

:-xcompiler
prolog_group_by( Generator, Group, X^Y^(Acc^Updater,Init) ) :-
    gensym(Id),
    every( (Generator,
           ( recorded( toto(Id,Group,Acc) ) ->
             (   erase( toto(Id,Group,Acc) ),
                 Updater,
                 record( toto(Id,Group,X) ) )
             ;
             ( Init,
                 record( toto(Id,Group,X) ) )
           ))),
    recorded( toto(Id,Group,X) )
    .


check_analyzer_mode(lc) :-
	record( stop_compilation_at(pda) )
	.

emit_at_pda :-
	recorded( compiler_analyzer(lc) ),

	current_output(Old_Stream),
        ( recorded( output_file(File) ) ->
            absolute_file_name(File,AF),
            open(AF,write,Stream),
            set_output(Stream)
        ;
            true
        ),
	
	wait( (
		  (   recorded('*WRAPPER*'( Pred,_,'*LCNEXT*'(C,_,_,_,_) )) ;
		      recorded('*WRAPPER*'( Pred,_,'*LIGHTLCNEXT*'(C,_,_,_,_) ))),
		  value_counter(Pred,V),
		  V>0,
		  copy_numbervars(C,XC),
		  record_without_doublon(tmp_start(XC))
	      )),
	wait((compute_lc(C,LC))),
	lc_emit_header,
	format( '\n%----------------- LC ---------------------\n',[]),
	/*
	every( (
		   exists(compute_lc(C,LC)),
		   format( 'internal_lc(~k,~k).\n', [C,LC] )
	       )),
        */
	(
	    prolog_group_by( exists(compute_lc(C,LC)),
		      [C],
		      All^LC^(Old^(All=[LC|Old]),
			      All=[LC])
		    ),
	    format( 'internal_lc(~k,~k).\n', [C,All] ),
	    fail
	;
	    true
	),
	format( '\n%-------------- REVERSE LC ----------------\n',[]),
/*
	every((
	      exists(compute_lc(C,LC)),
	      format( 'internal_reverse_lc(~k,~k).\n', [LC,C] )
	     )),
*/
	(   
	    prolog_group_by( exists(compute_lc(C,LC)),
			     [LC],
			     All^C^(Old^(All=[C|Old]),
				    All=[C])
			   ),
	    format( 'internal_reverse_lc(~k,~k).\n', [LC,All] ),
	    fail
	;   
	    true
	),
	format( '\n%-------------- TERMINAL LC ---------------\n',[]),
	/*
	every((
	      exists(compute_lc(C,LC)),
	      recorded(tmp_terminal(LC)),
	      format( 'internal_terminal_lc(~k,~k).\n', [C,LC] )
	     )),
	*/
	(   
	    prolog_group_by( (exists(compute_lc(C,LC)),recorded(tmp_terminal(LC))),
			     [C],
			     All^LC^(Old^(All=[LC|Old]),
				     All=[LC])
			   ),
	    format( 'internal_terminal_lc(~k,~k).\n', [C,All] ),
	    fail
	;   
	    true
	),
	nl,
	set_output(Old_Stream)
	.

:-mode(compute_lc/2,+(+,-)).

compute_lc(C,C) :- recorded( tmp_start(C) ). 
compute_lc(C,LC) :- compute_lc(C,LC1), recorded( tmp_lc(LC1,LC) ).

analyze_at_pda( '*OBJECT*'( seed{ model => '*RITEM*'(LC,_) :> '*LCFIRST*'(C,_,_)(_)(_)} ) ) :-
	recorded( compiler_analyzer(lc) ),
	copy_numbervars(tmp_lc(C,LC),U::tmp_lc(XC,XLC)),
	record_without_doublon(U)
	.

:-std_prolog lc_emit_header.

lc_emit_header :-
        date_time(Year,Month,Day,Hour,Min,Sec),
        compiler_info( compiler_info{ name=>Name, version=>Version }),
        format( '%% Compiler Analyzer Left Corner: ~w ~w\n', [Name,Version]),
        format( '%% Date    : ~w ~w ~w at ~w:~w:~w\n',[Month,Day,Year,Hour,Min,Sec]),
        every(  ( recorded( main_file(File,_) )
                , File \== '-'
                , absolute_file_name(File,F)
                , format('%% File ~q\n',[F])
                )
             ),
        ( recorded( main_file('-',_) ) -> format(';; File stdin\n',[]) ; true ),
        nl
        .


%% Part of analyze_at_pda is actually done in dcg.pl

check_lc(Pred) :-
	recorded(lc(Pred)) xor
	(   Pred = dcg(_)  ->
	    recorded(lc(dcg('*default*')))
	;   
	    recorded(lc('*default*'))
	)
	.

