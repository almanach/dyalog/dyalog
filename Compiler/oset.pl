/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2000, 2004 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  oset.pl -- Ordered Set of Number
 *
 * ----------------------------------------------------------------
 * Description
 *  Compact representation for Ordered Number Sets
 * ----------------------------------------------------------------
 */

%%:-require 'format.pl'.

:-std_prolog number_to_oset/2, oset_list/2, oset_void/1, oset_union/3, oset_inter/3.
:-std_prolog oset_delete/3.

number_to_oset(N,T) :- ('$interface'( oset_insert(N:int, 0:ptr), [return(_T:ptr)] ) xor _T=0),T=_T.

oset_list(T,L) :- '$interface'( oset_list(T:ptr), [return(L:term)] ).

oset_void(0).

oset_union(T1,T2,T3) :- ('$interface'( oset_union(T1:ptr,T2:ptr), [return(_T3:ptr)]) xor _T3=0),_T3=T3.
oset_inter(T1,T2,T3) :- ('$interface'( oset_inter(T1:ptr,T2:ptr), [return(_T3:ptr)]) xor _T3=0),_T3=T3.
oset_delete(T1,T2,T3) :- ('$interface'( oset_delete(T1:ptr,T2:ptr), [return(_T3:ptr)]) xor _T3=0),_T3=T3.

:-std_prolog oset_register/2.

oset_register(T1,T2) :- ('$interface'( oset_register(T1:ptr), [return(_T2:ptr)]) xor _T2 = 0),_T2=T2.

:-std_prolog oset_insert/3.

oset_insert(N,T1,T2) :- '$interface'( oset_insert(N:int,T1:ptr),[return(T2:ptr)]).

:-std_prolog oset_list_alt/2.

oset_list_alt(T,L) :- '$interface'( oset_list_alt(T:ptr), [return(L:term)] ).
