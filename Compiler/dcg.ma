;; Compiler: DyALog 1.14.0
;; File "dcg.pl"


;;------------------ LOADING ------------

c_code local load
	call_c   Dyam_Loading_Set()
	pl_call  fun2(&seed[106],0)
	pl_call  fun2(&seed[61],0)
	pl_call  fun2(&seed[55],0)
	pl_call  fun15(&seed[19],0)
	pl_call  fun15(&seed[11],0)
	pl_call  fun15(&seed[51],0)
	pl_call  fun15(&seed[28],0)
	pl_call  fun15(&seed[9],0)
	pl_call  fun15(&seed[26],0)
	pl_call  fun15(&seed[12],0)
	pl_call  fun15(&seed[21],0)
	pl_call  fun15(&seed[13],0)
	pl_call  fun15(&seed[17],0)
	pl_call  fun15(&seed[7],0)
	pl_call  fun15(&seed[10],0)
	pl_call  fun15(&seed[23],0)
	pl_call  fun15(&seed[8],0)
	pl_call  fun15(&seed[53],0)
	pl_call  fun15(&seed[4],0)
	pl_call  fun15(&seed[48],0)
	pl_call  fun15(&seed[46],0)
	pl_call  fun15(&seed[45],0)
	pl_call  fun15(&seed[16],0)
	pl_call  fun15(&seed[49],0)
	pl_call  fun15(&seed[15],0)
	pl_call  fun15(&seed[2],0)
	pl_call  fun15(&seed[14],0)
	pl_call  fun15(&seed[5],0)
	pl_call  fun15(&seed[38],0)
	pl_call  fun15(&seed[3],0)
	pl_call  fun15(&seed[44],0)
	pl_call  fun15(&seed[42],0)
	pl_call  fun15(&seed[35],0)
	pl_call  fun15(&seed[34],0)
	pl_call  fun15(&seed[20],0)
	pl_call  fun15(&seed[27],0)
	pl_call  fun15(&seed[36],0)
	pl_call  fun15(&seed[22],0)
	pl_call  fun15(&seed[32],0)
	pl_call  fun15(&seed[31],0)
	pl_call  fun15(&seed[43],0)
	pl_call  fun15(&seed[47],0)
	pl_call  fun15(&seed[30],0)
	pl_call  fun15(&seed[29],0)
	pl_call  fun15(&seed[41],0)
	pl_call  fun15(&seed[50],0)
	pl_call  fun15(&seed[40],0)
	pl_call  fun15(&seed[39],0)
	pl_call  fun15(&seed[37],0)
	pl_call  fun15(&seed[52],0)
	pl_call  fun15(&seed[0],0)
	pl_call  fun15(&seed[33],0)
	pl_call  fun15(&seed[24],0)
	pl_call  fun15(&seed[6],0)
	pl_call  fun15(&seed[1],0)
	pl_call  fun15(&seed[18],0)
	pl_call  fun15(&seed[25],0)
	call_c   Dyam_Loading_Reset()
	c_ret

pl_code global pred_dcg_autoloader_2
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(2)
	call_c   Dyam_Choice(fun224)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_univ(V(1),&ref[663])
	fail_ret
	pl_call  Domain_2(V(3),&ref[664])
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(4))
	move     V(6), R(2)
	move     S(5), R(3)
	pl_call  pred_dcg_autoloader_2()
	call_c   Dyam_Reg_Load(0,V(5))
	move     V(7), R(2)
	move     S(5), R(3)
	pl_call  pred_dcg_autoloader_2()
	move     &ref[665], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(2))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_autoloader_simplify_2()

pl_code global pred_kleene_normalize_2
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Unify(0,&ref[315])
	fail_ret
	call_c   Dyam_Reg_Unify(2,&ref[316])
	fail_ret
	call_c   Dyam_Choice(fun109)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_variable(V(2))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(10),I(0))
	fail_ret
fun106:
	call_c   Dyam_Choice(fun105)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_variable(V(5))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(11),I(0))
	fail_ret
fun102:
	call_c   Dyam_Choice(fun101)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_variable(V(7))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(13),V(11))
	fail_ret
fun98:
	call_c   Dyam_Choice(fun97)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_variable(V(6))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(12),V(11))
	fail_ret
fun94:
	call_c   Dyam_Choice(fun93)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_variable(V(8))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(14),V(12))
	fail_ret
fun90:
	call_c   Dyam_Choice(fun89)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_variable(V(9))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(15),&ref[186])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret






pl_code global pred_dcg_il_register_to_lpda_12
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(9)
	call_c   Dyam_Reg_Unify(18,&ref[302])
	fail_ret
	call_c   Dyam_Multi_Reg_Bind(20,13,2)
	call_c   Dyam_Reg_Load(0,V(10))
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(15), R(4)
	move     S(5), R(5)
	pl_call  pred_my_numbervars_3()
	move     &ref[300], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Load(4,V(5))
	call_c   Dyam_Reg_Load(6,V(3))
	call_c   Dyam_Reg_Load(8,V(4))
	call_c   Dyam_Reg_Load(10,V(11))
	call_c   Dyam_Reg_Deallocate(6)
fun77:
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Reg_Bind(2,V(6))
	call_c   Dyam_Multi_Reg_Bind(4,2,4)
	call_c   Dyam_Choice(fun76)
	pl_call  fun13(&seed[88],1)
	pl_fail


pl_code global pred_dcg_il_start_to_lpda_11
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(8)
	call_c   Dyam_Reg_Unify(16,&ref[294])
	fail_ret
	call_c   Dyam_Multi_Reg_Bind(18,12,2)
	call_c   Dyam_Reg_Load(0,V(9))
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(14), R(4)
	move     S(5), R(5)
	pl_call  pred_my_numbervars_3()
	move     &ref[292], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Load(4,V(5))
	call_c   Dyam_Reg_Load(6,V(3))
	call_c   Dyam_Reg_Load(8,V(4))
	call_c   Dyam_Reg_Load(10,V(10))
	call_c   Dyam_Reg_Deallocate(6)
	pl_jump  fun77()

pl_code global pred_dcg_il_loop_to_lpda_6
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(4)
	call_c   Dyam_Reg_Unify(8,&ref[286])
	fail_ret
	call_c   Dyam_Reg_Bind(10,V(6))
	move     &ref[271], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(9), R(4)
	move     S(5), R(5)
	pl_call  pred_my_numbervars_3()
	move     &ref[285], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Load(4,V(7))
	call_c   Dyam_Reg_Load(6,V(2))
	call_c   Dyam_Reg_Load(8,V(3))
	move     V(10), R(10)
	move     S(5), R(11)
	call_c   Dyam_Reg_Deallocate(6)
	pl_jump  fun77()

pl_code global pred_autoloader_simplify_2
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(2)
	call_c   Dyam_Choice(fun55)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[183])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Deallocate()
	pl_ret

pl_code global pred_dcg_il_body_to_lpda_11
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(11)
	call_c   Dyam_Choice(fun50)
	call_c   Dyam_Set_Cut()
	pl_call  fun7(&seed[69],1)
	call_c   Dyam_Reg_Load(0,V(12))
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(13), R(4)
	move     S(5), R(5)
	pl_call  pred_my_numbervars_3()
	call_c   Dyam_Cut()
fun49:
	call_c   Dyam_Choice(fun48)
	call_c   Dyam_Set_Cut()
	pl_call  fun7(&seed[70],1)
	call_c   Dyam_Cut()
	call_c   Dyam_Deallocate()
	pl_ret


pl_code global pred_dcg_body_to_lpda_9
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(9)
	call_c   Dyam_Choice(fun35)
	call_c   Dyam_Set_Cut()
	pl_call  fun7(&seed[64],1)
	call_c   Dyam_Reg_Load(0,V(10))
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(11), R(4)
	move     S(5), R(5)
	pl_call  pred_my_numbervars_3()
	call_c   Dyam_Cut()
fun34:
	call_c   Dyam_Choice(fun33)
	call_c   Dyam_Set_Cut()
	pl_call  fun7(&seed[65],1)
	call_c   Dyam_Cut()
	call_c   Dyam_Deallocate()
	pl_ret


pl_code global pred_bmg_null_step_7
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind(2,2,6)
	call_c   Dyam_Choice(fun19)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(5),V(4))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(8),V(6))
	fail_ret
fun18:
	call_c   Dyam_Choice(fun17)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(2),V(3))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(7),V(8))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret


pl_code global pred_dcg_null_step_5
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind(2,2,4)
	call_c   Dyam_Choice(fun9)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(2),V(3))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(5),V(4))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code global pred_il_label_2
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(2)
	move     &ref[47], R(0)
	move     0, R(1)
	move     V(3), R(2)
	move     S(5), R(3)
	pl_call  pred_update_counter_2()
	move     &ref[48], R(0)
	move     0, R(1)
	move     &ref[49], R(2)
	move     S(5), R(3)
	move     V(4), R(4)
	move     S(5), R(5)
	pl_call  pred_name_builder_3()
	call_c   DYAM_evpred_atom_to_module(V(2),V(1),V(4))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret


;;------------------ VIEWERS ------------

c_code local build_viewers
	call_c   Dyam_Load_Viewer(&ref[34],&ref[35])
	call_c   Dyam_Load_Viewer(&ref[31],&ref[30])
	call_c   Dyam_Load_Viewer(&ref[29],&ref[28])
	call_c   Dyam_Load_Viewer(&ref[26],&ref[27])
	call_c   Dyam_Load_Viewer(&ref[23],&ref[22])
	call_c   Dyam_Load_Viewer(&ref[21],&ref[20])
	call_c   Dyam_Load_Viewer(&ref[18],&ref[19])
	call_c   Dyam_Load_Viewer(&ref[15],&ref[13])
	call_c   Dyam_Load_Viewer(&ref[11],&ref[12])
	call_c   Dyam_Load_Viewer(&ref[7],&ref[8])
	call_c   Dyam_Load_Viewer(&ref[3],&ref[4])
	c_ret

c_code local build_seed_25
	ret_reg &seed[25]
	call_c   build_ref_36()
	call_c   build_ref_38()
	call_c   Dyam_Seed_Start(&ref[36],&ref[38],I(0),fun4,1)
	call_c   build_ref_40()
	call_c   Dyam_Seed_Add_Comp(&ref[40],fun3,0)
	call_c   Dyam_Seed_End()
	move_ret seed[25]
	c_ret

;; TERM 40: '*CITEM*'(interleave_initialize, _A)
c_code local build_ref_40
	ret_reg &ref[40]
	call_c   build_ref_39()
	call_c   build_ref_22()
	call_c   Dyam_Create_Binary(&ref[39],&ref[22],V(0))
	move_ret ref[40]
	c_ret

;; TERM 22: interleave_initialize
c_code local build_ref_22
	ret_reg &ref[22]
	call_c   Dyam_Create_Atom("interleave_initialize")
	move_ret ref[22]
	c_ret

;; TERM 39: '*CITEM*'
c_code local build_ref_39
	ret_reg &ref[39]
	call_c   Dyam_Create_Atom("*CITEM*")
	move_ret ref[39]
	c_ret

;; TERM 41: 'interleave.pl'
c_code local build_ref_41
	ret_reg &ref[41]
	call_c   Dyam_Create_Atom("interleave.pl")
	move_ret ref[41]
	c_ret

;; TERM 42: require
c_code local build_ref_42
	ret_reg &ref[42]
	call_c   Dyam_Create_Atom("require")
	move_ret ref[42]
	c_ret

c_code local build_seed_54
	ret_reg &seed[54]
	call_c   build_ref_0()
	call_c   build_ref_43()
	call_c   Dyam_Seed_Start(&ref[0],&ref[43],&ref[43],fun0,1)
	call_c   build_ref_44()
	call_c   Dyam_Seed_Add_Comp(&ref[44],&ref[43],0)
	call_c   Dyam_Seed_End()
	move_ret seed[54]
	c_ret

pl_code local fun0
	pl_jump  Complete(0,0)

;; TERM 44: '*RITEM*'(_A, voidret) :> '$$HOLE$$'
c_code local build_ref_44
	ret_reg &ref[44]
	call_c   build_ref_43()
	call_c   Dyam_Create_Binary(I(9),&ref[43],I(7))
	move_ret ref[44]
	c_ret

;; TERM 43: '*RITEM*'(_A, voidret)
c_code local build_ref_43
	ret_reg &ref[43]
	call_c   build_ref_0()
	call_c   build_ref_14()
	call_c   Dyam_Create_Binary(&ref[0],V(0),&ref[14])
	move_ret ref[43]
	c_ret

;; TERM 14: voidret
c_code local build_ref_14
	ret_reg &ref[14]
	call_c   Dyam_Create_Atom("voidret")
	move_ret ref[14]
	c_ret

;; TERM 0: '*RITEM*'
c_code local build_ref_0
	ret_reg &ref[0]
	call_c   Dyam_Create_Atom("*RITEM*")
	move_ret ref[0]
	c_ret

pl_code local fun2
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Choice(fun1)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Subsume(R(0))
	fail_ret
	call_c   Dyam_Cut()
	pl_ret

long local pool_fun3[5]=[4,build_ref_40,build_ref_41,build_ref_42,build_seed_54]

pl_code local fun3
	call_c   Dyam_Pool(pool_fun3)
	call_c   Dyam_Unify_Item(&ref[40])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[41], R(0)
	move     0, R(1)
	move     &ref[42], R(2)
	move     0, R(3)
	pl_call  pred_read_files_2()
	call_c   Dyam_Deallocate()
	pl_jump  fun2(&seed[54],2)

pl_code local fun4
	pl_jump  Apply(0,0)

;; TERM 38: '*FIRST*'(interleave_initialize) :> []
c_code local build_ref_38
	ret_reg &ref[38]
	call_c   build_ref_37()
	call_c   Dyam_Create_Binary(I(9),&ref[37],I(0))
	move_ret ref[38]
	c_ret

;; TERM 37: '*FIRST*'(interleave_initialize)
c_code local build_ref_37
	ret_reg &ref[37]
	call_c   build_ref_36()
	call_c   build_ref_22()
	call_c   Dyam_Create_Unary(&ref[36],&ref[22])
	move_ret ref[37]
	c_ret

;; TERM 36: '*FIRST*'
c_code local build_ref_36
	ret_reg &ref[36]
	call_c   Dyam_Create_Atom("*FIRST*")
	move_ret ref[36]
	c_ret

c_code local build_seed_18
	ret_reg &seed[18]
	call_c   build_ref_50()
	call_c   build_ref_54()
	call_c   Dyam_Seed_Start(&ref[50],&ref[54],I(0),fun5,1)
	call_c   build_ref_53()
	call_c   Dyam_Seed_Add_Comp(&ref[53],fun8,0)
	call_c   Dyam_Seed_End()
	move_ret seed[18]
	c_ret

;; TERM 53: '*GUARD*'(position_and_stacks(_B, _C, _D))
c_code local build_ref_53
	ret_reg &ref[53]
	call_c   build_ref_51()
	call_c   build_ref_52()
	call_c   Dyam_Create_Unary(&ref[51],&ref[52])
	move_ret ref[53]
	c_ret

;; TERM 52: position_and_stacks(_B, _C, _D)
c_code local build_ref_52
	ret_reg &ref[52]
	call_c   build_ref_826()
	call_c   Dyam_Term_Start(&ref[826],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[52]
	c_ret

;; TERM 826: position_and_stacks
c_code local build_ref_826
	ret_reg &ref[826]
	call_c   Dyam_Create_Atom("position_and_stacks")
	move_ret ref[826]
	c_ret

;; TERM 51: '*GUARD*'
c_code local build_ref_51
	ret_reg &ref[51]
	call_c   Dyam_Create_Atom("*GUARD*")
	move_ret ref[51]
	c_ret

c_code local build_seed_56
	ret_reg &seed[56]
	call_c   build_ref_51()
	call_c   build_ref_56()
	call_c   Dyam_Seed_Start(&ref[51],&ref[56],I(0),fun0,1)
	call_c   build_ref_57()
	call_c   Dyam_Seed_Add_Comp(&ref[57],&ref[56],0)
	call_c   Dyam_Seed_End()
	move_ret seed[56]
	c_ret

;; TERM 57: '*GUARD*'(bmg_get_stacks(_D, _B)) :> '$$HOLE$$'
c_code local build_ref_57
	ret_reg &ref[57]
	call_c   build_ref_56()
	call_c   Dyam_Create_Binary(I(9),&ref[56],I(7))
	move_ret ref[57]
	c_ret

;; TERM 56: '*GUARD*'(bmg_get_stacks(_D, _B))
c_code local build_ref_56
	ret_reg &ref[56]
	call_c   build_ref_51()
	call_c   build_ref_55()
	call_c   Dyam_Create_Unary(&ref[51],&ref[55])
	move_ret ref[56]
	c_ret

;; TERM 55: bmg_get_stacks(_D, _B)
c_code local build_ref_55
	ret_reg &ref[55]
	call_c   build_ref_827()
	call_c   Dyam_Create_Binary(&ref[827],V(3),V(1))
	move_ret ref[55]
	c_ret

;; TERM 827: bmg_get_stacks
c_code local build_ref_827
	ret_reg &ref[827]
	call_c   Dyam_Create_Atom("bmg_get_stacks")
	move_ret ref[827]
	c_ret

pl_code local fun7
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Light_Object(R(0))
	pl_jump  Schedule_Prolog()

long local pool_fun8[3]=[2,build_ref_53,build_seed_56]

pl_code local fun8
	call_c   Dyam_Pool(pool_fun8)
	call_c   Dyam_Unify_Item(&ref[53])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(2))
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(4), R(4)
	move     S(5), R(5)
	pl_call  pred_my_numbervars_3()
	call_c   Dyam_Deallocate()
	pl_jump  fun7(&seed[56],1)

pl_code local fun5
	pl_ret

;; TERM 54: '*GUARD*'(position_and_stacks(_B, _C, _D)) :> []
c_code local build_ref_54
	ret_reg &ref[54]
	call_c   build_ref_53()
	call_c   Dyam_Create_Binary(I(9),&ref[53],I(0))
	move_ret ref[54]
	c_ret

;; TERM 50: '*GUARD_CLAUSE*'
c_code local build_ref_50
	ret_reg &ref[50]
	call_c   Dyam_Create_Atom("*GUARD_CLAUSE*")
	move_ret ref[50]
	c_ret

c_code local build_seed_1
	ret_reg &seed[1]
	call_c   build_ref_50()
	call_c   build_ref_62()
	call_c   Dyam_Seed_Start(&ref[50],&ref[62],I(0),fun5,1)
	call_c   build_ref_61()
	call_c   Dyam_Seed_Add_Comp(&ref[61],fun11,0)
	call_c   Dyam_Seed_End()
	move_ret seed[1]
	c_ret

;; TERM 61: '*GUARD*'(dcg_body_to_lpda_handler(_B, fail, _C, _D, _E, _F, _G, (fail :> _G), _H))
c_code local build_ref_61
	ret_reg &ref[61]
	call_c   build_ref_51()
	call_c   build_ref_60()
	call_c   Dyam_Create_Unary(&ref[51],&ref[60])
	move_ret ref[61]
	c_ret

;; TERM 60: dcg_body_to_lpda_handler(_B, fail, _C, _D, _E, _F, _G, (fail :> _G), _H)
c_code local build_ref_60
	ret_reg &ref[60]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_829()
	call_c   Dyam_Create_Binary(I(9),&ref[829],V(6))
	move_ret R(0)
	call_c   build_ref_828()
	call_c   Dyam_Term_Start(&ref[828],9)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(&ref[829])
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[60]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 828: dcg_body_to_lpda_handler
c_code local build_ref_828
	ret_reg &ref[828]
	call_c   Dyam_Create_Atom("dcg_body_to_lpda_handler")
	move_ret ref[828]
	c_ret

;; TERM 829: fail
c_code local build_ref_829
	ret_reg &ref[829]
	call_c   Dyam_Create_Atom("fail")
	move_ret ref[829]
	c_ret

pl_code local fun11
	call_c   build_ref_61()
	call_c   Dyam_Unify_Item(&ref[61])
	fail_ret
	pl_ret

;; TERM 62: '*GUARD*'(dcg_body_to_lpda_handler(_B, fail, _C, _D, _E, _F, _G, (fail :> _G), _H)) :> []
c_code local build_ref_62
	ret_reg &ref[62]
	call_c   build_ref_61()
	call_c   Dyam_Create_Binary(I(9),&ref[61],I(0))
	move_ret ref[62]
	c_ret

c_code local build_seed_6
	ret_reg &seed[6]
	call_c   build_ref_50()
	call_c   build_ref_77()
	call_c   Dyam_Seed_Start(&ref[50],&ref[77],I(0),fun5,1)
	call_c   build_ref_76()
	call_c   Dyam_Seed_Add_Comp(&ref[76],fun16,0)
	call_c   Dyam_Seed_End()
	move_ret seed[6]
	c_ret

;; TERM 76: '*GUARD*'(token_trail_gen([_B], _C, _D, record(_E), _F))
c_code local build_ref_76
	ret_reg &ref[76]
	call_c   build_ref_51()
	call_c   build_ref_75()
	call_c   Dyam_Create_Unary(&ref[51],&ref[75])
	move_ret ref[76]
	c_ret

;; TERM 75: token_trail_gen([_B], _C, _D, record(_E), _F)
c_code local build_ref_75
	ret_reg &ref[75]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   Dyam_Create_List(V(1),I(0))
	move_ret R(0)
	call_c   build_ref_831()
	call_c   Dyam_Create_Unary(&ref[831],V(4))
	move_ret R(1)
	call_c   build_ref_830()
	call_c   Dyam_Term_Start(&ref[830],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[75]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 830: token_trail_gen
c_code local build_ref_830
	ret_reg &ref[830]
	call_c   Dyam_Create_Atom("token_trail_gen")
	move_ret ref[830]
	c_ret

;; TERM 831: record
c_code local build_ref_831
	ret_reg &ref[831]
	call_c   Dyam_Create_Atom("record")
	move_ret ref[831]
	c_ret

c_code local build_seed_59
	ret_reg &seed[59]
	call_c   build_ref_51()
	call_c   build_ref_79()
	call_c   Dyam_Seed_Start(&ref[51],&ref[79],I(0),fun0,1)
	call_c   build_ref_80()
	call_c   Dyam_Seed_Add_Comp(&ref[80],&ref[79],0)
	call_c   Dyam_Seed_End()
	move_ret seed[59]
	c_ret

;; TERM 80: '*GUARD*'(generalized_scanner(_F, _C, _B, _D, _E, skip)) :> '$$HOLE$$'
c_code local build_ref_80
	ret_reg &ref[80]
	call_c   build_ref_79()
	call_c   Dyam_Create_Binary(I(9),&ref[79],I(7))
	move_ret ref[80]
	c_ret

;; TERM 79: '*GUARD*'(generalized_scanner(_F, _C, _B, _D, _E, skip))
c_code local build_ref_79
	ret_reg &ref[79]
	call_c   build_ref_51()
	call_c   build_ref_78()
	call_c   Dyam_Create_Unary(&ref[51],&ref[78])
	move_ret ref[79]
	c_ret

;; TERM 78: generalized_scanner(_F, _C, _B, _D, _E, skip)
c_code local build_ref_78
	ret_reg &ref[78]
	call_c   build_ref_832()
	call_c   build_ref_389()
	call_c   Dyam_Term_Start(&ref[832],6)
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(&ref[389])
	call_c   Dyam_Term_End()
	move_ret ref[78]
	c_ret

;; TERM 389: skip
c_code local build_ref_389
	ret_reg &ref[389]
	call_c   Dyam_Create_Atom("skip")
	move_ret ref[389]
	c_ret

;; TERM 832: generalized_scanner
c_code local build_ref_832
	ret_reg &ref[832]
	call_c   Dyam_Create_Atom("generalized_scanner")
	move_ret ref[832]
	c_ret

long local pool_fun16[3]=[2,build_ref_76,build_seed_59]

pl_code local fun16
	call_c   Dyam_Pool(pool_fun16)
	call_c   Dyam_Unify_Item(&ref[76])
	fail_ret
	pl_jump  fun7(&seed[59],1)

;; TERM 77: '*GUARD*'(token_trail_gen([_B], _C, _D, record(_E), _F)) :> []
c_code local build_ref_77
	ret_reg &ref[77]
	call_c   build_ref_76()
	call_c   Dyam_Create_Binary(I(9),&ref[76],I(0))
	move_ret ref[77]
	c_ret

c_code local build_seed_24
	ret_reg &seed[24]
	call_c   build_ref_36()
	call_c   build_ref_65()
	call_c   Dyam_Seed_Start(&ref[36],&ref[65],I(0),fun4,1)
	call_c   build_ref_66()
	call_c   Dyam_Seed_Add_Comp(&ref[66],fun25,0)
	call_c   Dyam_Seed_End()
	move_ret seed[24]
	c_ret

;; TERM 66: '*CITEM*'(dcg_il_args(_B, _C, _D, [_B,_C|_D]), _A)
c_code local build_ref_66
	ret_reg &ref[66]
	call_c   build_ref_39()
	call_c   build_ref_63()
	call_c   Dyam_Create_Binary(&ref[39],&ref[63],V(0))
	move_ret ref[66]
	c_ret

;; TERM 63: dcg_il_args(_B, _C, _D, [_B,_C|_D])
c_code local build_ref_63
	ret_reg &ref[63]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Tupple(1,2,V(3))
	move_ret R(0)
	call_c   build_ref_833()
	call_c   Dyam_Term_Start(&ref[833],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_End()
	move_ret ref[63]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 833: dcg_il_args
c_code local build_ref_833
	ret_reg &ref[833]
	call_c   Dyam_Create_Atom("dcg_il_args")
	move_ret ref[833]
	c_ret

;; TERM 88: '$CLOSURE'('$fun'(24, 0, 1146576196), '$TUPPLE'(35074870873604))
c_code local build_ref_88
	ret_reg &ref[88]
	call_c   build_ref_87()
	call_c   Dyam_Closure_Aux(fun24,&ref[87])
	move_ret ref[88]
	c_ret

long local pool_fun24[2]=[1,build_seed_54]

pl_code local fun24
	call_c   Dyam_Pool(pool_fun24)
	pl_jump  fun2(&seed[54],2)

;; TERM 87: '$TUPPLE'(35074870873604)
c_code local build_ref_87
	ret_reg &ref[87]
	call_c   Dyam_Create_Simple_Tupple(0,268435456)
	move_ret ref[87]
	c_ret

long local pool_fun25[3]=[2,build_ref_66,build_ref_88]

pl_code local fun25
	call_c   Dyam_Pool(pool_fun25)
	call_c   Dyam_Unify_Item(&ref[66])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[88], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Load(4,V(3))
	call_c   Dyam_Reg_Deallocate(3)
fun23:
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Reg_Bind(2,V(3))
	call_c   Dyam_Reg_Bind(4,V(2))
	call_c   Dyam_Choice(fun22)
	pl_call  fun13(&seed[57],1)
	pl_fail


;; TERM 65: '*FIRST*'(dcg_il_args(_B, _C, _D, [_B,_C|_D])) :> []
c_code local build_ref_65
	ret_reg &ref[65]
	call_c   build_ref_64()
	call_c   Dyam_Create_Binary(I(9),&ref[64],I(0))
	move_ret ref[65]
	c_ret

;; TERM 64: '*FIRST*'(dcg_il_args(_B, _C, _D, [_B,_C|_D]))
c_code local build_ref_64
	ret_reg &ref[64]
	call_c   build_ref_36()
	call_c   build_ref_63()
	call_c   Dyam_Create_Unary(&ref[36],&ref[63])
	move_ret ref[64]
	c_ret

c_code local build_seed_33
	ret_reg &seed[33]
	call_c   build_ref_50()
	call_c   build_ref_94()
	call_c   Dyam_Seed_Start(&ref[50],&ref[94],I(0),fun5,1)
	call_c   build_ref_93()
	call_c   Dyam_Seed_Add_Comp(&ref[93],fun27,0)
	call_c   Dyam_Seed_End()
	move_ret seed[33]
	c_ret

;; TERM 93: '*GUARD*'(dcg_body_to_lpda_handler(_B, wait(_C), _D, _E, _F, _G, _H, _I, _J))
c_code local build_ref_93
	ret_reg &ref[93]
	call_c   build_ref_51()
	call_c   build_ref_92()
	call_c   Dyam_Create_Unary(&ref[51],&ref[92])
	move_ret ref[93]
	c_ret

;; TERM 92: dcg_body_to_lpda_handler(_B, wait(_C), _D, _E, _F, _G, _H, _I, _J)
c_code local build_ref_92
	ret_reg &ref[92]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_834()
	call_c   Dyam_Create_Unary(&ref[834],V(2))
	move_ret R(0)
	call_c   build_ref_828()
	call_c   Dyam_Term_Start(&ref[828],9)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret ref[92]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 834: wait
c_code local build_ref_834
	ret_reg &ref[834]
	call_c   Dyam_Create_Atom("wait")
	move_ret ref[834]
	c_ret

;; TERM 95: 'Not yet implemented in DCG body ~w'
c_code local build_ref_95
	ret_reg &ref[95]
	call_c   Dyam_Create_Atom("Not yet implemented in DCG body ~w")
	move_ret ref[95]
	c_ret

;; TERM 96: [wait(_C)]
c_code local build_ref_96
	ret_reg &ref[96]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_834()
	call_c   Dyam_Create_Unary(&ref[834],V(2))
	move_ret R(0)
	call_c   Dyam_Create_List(R(0),I(0))
	move_ret ref[96]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun27[4]=[3,build_ref_93,build_ref_95,build_ref_96]

pl_code local fun27
	call_c   Dyam_Pool(pool_fun27)
	call_c   Dyam_Unify_Item(&ref[93])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[95], R(0)
	move     0, R(1)
	move     &ref[96], R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_error_2()

;; TERM 94: '*GUARD*'(dcg_body_to_lpda_handler(_B, wait(_C), _D, _E, _F, _G, _H, _I, _J)) :> []
c_code local build_ref_94
	ret_reg &ref[94]
	call_c   build_ref_93()
	call_c   Dyam_Create_Binary(I(9),&ref[93],I(0))
	move_ret ref[94]
	c_ret

c_code local build_seed_0
	ret_reg &seed[0]
	call_c   build_ref_50()
	call_c   build_ref_91()
	call_c   Dyam_Seed_Start(&ref[50],&ref[91],I(0),fun5,1)
	call_c   build_ref_90()
	call_c   Dyam_Seed_Add_Comp(&ref[90],fun26,0)
	call_c   Dyam_Seed_End()
	move_ret seed[0]
	c_ret

;; TERM 90: '*GUARD*'(dcg_il_body_to_lpda_handler(_B, [], _C, _D, _E, _F, _G, _H, (unify(_C, _D) :> _G), _I, _J))
c_code local build_ref_90
	ret_reg &ref[90]
	call_c   build_ref_51()
	call_c   build_ref_89()
	call_c   Dyam_Create_Unary(&ref[51],&ref[89])
	move_ret ref[90]
	c_ret

;; TERM 89: dcg_il_body_to_lpda_handler(_B, [], _C, _D, _E, _F, _G, _H, (unify(_C, _D) :> _G), _I, _J)
c_code local build_ref_89
	ret_reg &ref[89]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_58()
	call_c   Dyam_Create_Binary(I(9),&ref[58],V(6))
	move_ret R(0)
	call_c   build_ref_835()
	call_c   Dyam_Term_Start(&ref[835],11)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret ref[89]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 835: dcg_il_body_to_lpda_handler
c_code local build_ref_835
	ret_reg &ref[835]
	call_c   Dyam_Create_Atom("dcg_il_body_to_lpda_handler")
	move_ret ref[835]
	c_ret

;; TERM 58: unify(_C, _D)
c_code local build_ref_58
	ret_reg &ref[58]
	call_c   build_ref_836()
	call_c   Dyam_Create_Binary(&ref[836],V(2),V(3))
	move_ret ref[58]
	c_ret

;; TERM 836: unify
c_code local build_ref_836
	ret_reg &ref[836]
	call_c   Dyam_Create_Atom("unify")
	move_ret ref[836]
	c_ret

pl_code local fun26
	call_c   build_ref_90()
	call_c   Dyam_Unify_Item(&ref[90])
	fail_ret
	pl_ret

;; TERM 91: '*GUARD*'(dcg_il_body_to_lpda_handler(_B, [], _C, _D, _E, _F, _G, _H, (unify(_C, _D) :> _G), _I, _J)) :> []
c_code local build_ref_91
	ret_reg &ref[91]
	call_c   build_ref_90()
	call_c   Dyam_Create_Binary(I(9),&ref[90],I(0))
	move_ret ref[91]
	c_ret

c_code local build_seed_52
	ret_reg &seed[52]
	call_c   build_ref_50()
	call_c   build_ref_100()
	call_c   Dyam_Seed_Start(&ref[50],&ref[100],I(0),fun5,1)
	call_c   build_ref_99()
	call_c   Dyam_Seed_Add_Comp(&ref[99],fun32,0)
	call_c   Dyam_Seed_End()
	move_ret seed[52]
	c_ret

;; TERM 99: '*GUARD*'(pgm_to_lpda(register_predicate((dcg _B / _C)), ('*PROLOG-FIRST*'(_D) :> _E)))
c_code local build_ref_99
	ret_reg &ref[99]
	call_c   build_ref_51()
	call_c   build_ref_98()
	call_c   Dyam_Create_Unary(&ref[51],&ref[98])
	move_ret ref[99]
	c_ret

;; TERM 98: pgm_to_lpda(register_predicate((dcg _B / _C)), ('*PROLOG-FIRST*'(_D) :> _E))
c_code local build_ref_98
	ret_reg &ref[98]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_840()
	call_c   Dyam_Create_Binary(&ref[840],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_839()
	call_c   Dyam_Create_Unary(&ref[839],R(0))
	move_ret R(0)
	call_c   build_ref_838()
	call_c   Dyam_Create_Unary(&ref[838],R(0))
	move_ret R(0)
	call_c   build_ref_476()
	call_c   Dyam_Create_Unary(&ref[476],V(3))
	move_ret R(1)
	call_c   Dyam_Create_Binary(I(9),R(1),V(4))
	move_ret R(1)
	call_c   build_ref_837()
	call_c   Dyam_Create_Binary(&ref[837],R(0),R(1))
	move_ret ref[98]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 837: pgm_to_lpda
c_code local build_ref_837
	ret_reg &ref[837]
	call_c   Dyam_Create_Atom("pgm_to_lpda")
	move_ret ref[837]
	c_ret

;; TERM 476: '*PROLOG-FIRST*'
c_code local build_ref_476
	ret_reg &ref[476]
	call_c   Dyam_Create_Atom("*PROLOG-FIRST*")
	move_ret ref[476]
	c_ret

;; TERM 838: register_predicate
c_code local build_ref_838
	ret_reg &ref[838]
	call_c   Dyam_Create_Atom("register_predicate")
	move_ret ref[838]
	c_ret

;; TERM 839: dcg
c_code local build_ref_839
	ret_reg &ref[839]
	call_c   Dyam_Create_Atom("dcg")
	move_ret ref[839]
	c_ret

;; TERM 840: /
c_code local build_ref_840
	ret_reg &ref[840]
	call_c   Dyam_Create_Atom("/")
	move_ret ref[840]
	c_ret

;; TERM 112: public((dcg '*default*'))
c_code local build_ref_112
	ret_reg &ref[112]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_839()
	call_c   build_ref_842()
	call_c   Dyam_Create_Unary(&ref[839],&ref[842])
	move_ret R(0)
	call_c   build_ref_841()
	call_c   Dyam_Create_Unary(&ref[841],R(0))
	move_ret ref[112]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 841: public
c_code local build_ref_841
	ret_reg &ref[841]
	call_c   Dyam_Create_Atom("public")
	move_ret ref[841]
	c_ret

;; TERM 842: '*default*'
c_code local build_ref_842
	ret_reg &ref[842]
	call_c   Dyam_Create_Atom("*default*")
	move_ret ref[842]
	c_ret

c_code local build_seed_62
	ret_reg &seed[62]
	call_c   build_ref_51()
	call_c   build_ref_103()
	call_c   Dyam_Seed_Start(&ref[51],&ref[103],I(0),fun0,1)
	call_c   build_ref_104()
	call_c   Dyam_Seed_Add_Comp(&ref[104],&ref[103],0)
	call_c   Dyam_Seed_End()
	move_ret seed[62]
	c_ret

;; TERM 104: '*GUARD*'(position_and_stacks(_G, _I, _J)) :> '$$HOLE$$'
c_code local build_ref_104
	ret_reg &ref[104]
	call_c   build_ref_103()
	call_c   Dyam_Create_Binary(I(9),&ref[103],I(7))
	move_ret ref[104]
	c_ret

;; TERM 103: '*GUARD*'(position_and_stacks(_G, _I, _J))
c_code local build_ref_103
	ret_reg &ref[103]
	call_c   build_ref_51()
	call_c   build_ref_102()
	call_c   Dyam_Create_Unary(&ref[51],&ref[102])
	move_ret ref[103]
	c_ret

;; TERM 102: position_and_stacks(_G, _I, _J)
c_code local build_ref_102
	ret_reg &ref[102]
	call_c   build_ref_826()
	call_c   Dyam_Term_Start(&ref[826],3)
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret ref[102]
	c_ret

c_code local build_seed_63
	ret_reg &seed[63]
	call_c   build_ref_51()
	call_c   build_ref_106()
	call_c   Dyam_Seed_Start(&ref[51],&ref[106],I(0),fun0,1)
	call_c   build_ref_107()
	call_c   Dyam_Seed_Add_Comp(&ref[107],&ref[106],0)
	call_c   Dyam_Seed_End()
	move_ret seed[63]
	c_ret

;; TERM 107: '*GUARD*'(position_and_stacks(_G, _K, _L)) :> '$$HOLE$$'
c_code local build_ref_107
	ret_reg &ref[107]
	call_c   build_ref_106()
	call_c   Dyam_Create_Binary(I(9),&ref[106],I(7))
	move_ret ref[107]
	c_ret

;; TERM 106: '*GUARD*'(position_and_stacks(_G, _K, _L))
c_code local build_ref_106
	ret_reg &ref[106]
	call_c   build_ref_51()
	call_c   build_ref_105()
	call_c   Dyam_Create_Unary(&ref[51],&ref[105])
	move_ret ref[106]
	c_ret

;; TERM 105: position_and_stacks(_G, _K, _L)
c_code local build_ref_105
	ret_reg &ref[105]
	call_c   build_ref_826()
	call_c   Dyam_Term_Start(&ref[826],3)
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_End()
	move_ret ref[105]
	c_ret

;; TERM 111: '$answers'(_F)
c_code local build_ref_111
	ret_reg &ref[111]
	call_c   build_ref_843()
	call_c   Dyam_Create_Unary(&ref[843],V(5))
	move_ret ref[111]
	c_ret

;; TERM 843: '$answers'
c_code local build_ref_843
	ret_reg &ref[843]
	call_c   Dyam_Create_Atom("$answers")
	move_ret ref[843]
	c_ret

;; TERM 108: '$bmgcall'(_M, _I, _K, _J, _L)
c_code local build_ref_108
	ret_reg &ref[108]
	call_c   build_ref_844()
	call_c   Dyam_Term_Start(&ref[844],5)
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_End()
	move_ret ref[108]
	c_ret

;; TERM 844: '$bmgcall'
c_code local build_ref_844
	ret_reg &ref[844]
	call_c   Dyam_Create_Atom("$bmgcall")
	move_ret ref[844]
	c_ret

;; TERM 109: '*PROLOG-LAST*'
c_code local build_ref_109
	ret_reg &ref[109]
	call_c   Dyam_Create_Atom("*PROLOG-LAST*")
	move_ret ref[109]
	c_ret

;; TERM 110: prolog
c_code local build_ref_110
	ret_reg &ref[110]
	call_c   Dyam_Create_Atom("prolog")
	move_ret ref[110]
	c_ret

long local pool_fun28[4]=[3,build_ref_108,build_ref_109,build_ref_110]

long local pool_fun29[3]=[65537,build_ref_111,pool_fun28]

pl_code local fun29
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(12),&ref[111])
	fail_ret
fun28:
	call_c   Dyam_Unify(V(3),&ref[108])
	fail_ret
	call_c   Dyam_Reg_Load(0,V(6))
	call_c   Dyam_Reg_Load(2,V(12))
	call_c   Dyam_Reg_Load(4,V(8))
	call_c   Dyam_Reg_Load(6,V(10))
	call_c   Dyam_Reg_Load(8,V(9))
	call_c   Dyam_Reg_Load(10,V(11))
	move     &ref[109], R(12)
	move     0, R(13)
	call_c   Dyam_Reg_Load(14,V(4))
	move     &ref[110], R(16)
	move     0, R(17)
	call_c   Dyam_Reg_Deallocate(9)
	pl_jump  pred_dcg_body_to_lpda_9()


long local pool_fun30[5]=[131074,build_seed_62,build_seed_63,pool_fun29,pool_fun28]

long local pool_fun31[3]=[65537,build_ref_112,pool_fun30]

pl_code local fun31
	call_c   Dyam_Remove_Choice()
	pl_call  Object_1(&ref[112])
fun30:
	call_c   DYAM_evpred_functor(V(5),V(1),V(2))
	fail_ret
	call_c   Dyam_Reg_Load(0,V(5))
	move     V(6), R(2)
	move     S(5), R(3)
	move     V(7), R(4)
	move     S(5), R(5)
	pl_call  pred_my_numbervars_3()
	pl_call  fun7(&seed[62],1)
	pl_call  fun7(&seed[63],1)
	call_c   Dyam_Choice(fun29)
	call_c   Dyam_Unify(V(12),V(5))
	fail_ret
	pl_jump  fun28()


;; TERM 101: public((dcg _B / _C))
c_code local build_ref_101
	ret_reg &ref[101]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_840()
	call_c   Dyam_Create_Binary(&ref[840],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_839()
	call_c   Dyam_Create_Unary(&ref[839],R(0))
	move_ret R(0)
	call_c   build_ref_841()
	call_c   Dyam_Create_Unary(&ref[841],R(0))
	move_ret ref[101]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun32[5]=[131074,build_ref_99,build_ref_101,pool_fun31,pool_fun30]

pl_code local fun32
	call_c   Dyam_Pool(pool_fun32)
	call_c   Dyam_Unify_Item(&ref[99])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun31)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[101])
	call_c   Dyam_Cut()
	pl_jump  fun30()

;; TERM 100: '*GUARD*'(pgm_to_lpda(register_predicate((dcg _B / _C)), ('*PROLOG-FIRST*'(_D) :> _E))) :> []
c_code local build_ref_100
	ret_reg &ref[100]
	call_c   build_ref_99()
	call_c   Dyam_Create_Binary(I(9),&ref[99],I(0))
	move_ret ref[100]
	c_ret

c_code local build_seed_37
	ret_reg &seed[37]
	call_c   build_ref_50()
	call_c   build_ref_124()
	call_c   Dyam_Seed_Start(&ref[50],&ref[124],I(0),fun5,1)
	call_c   build_ref_123()
	call_c   Dyam_Seed_Add_Comp(&ref[123],fun37,0)
	call_c   Dyam_Seed_End()
	move_ret seed[37]
	c_ret

;; TERM 123: '*GUARD*'(dcg_body_to_lpda_handler(_B, [], _C, _D, _E, _F, _G, _H, _I))
c_code local build_ref_123
	ret_reg &ref[123]
	call_c   build_ref_51()
	call_c   build_ref_122()
	call_c   Dyam_Create_Unary(&ref[51],&ref[122])
	move_ret ref[123]
	c_ret

;; TERM 122: dcg_body_to_lpda_handler(_B, [], _C, _D, _E, _F, _G, _H, _I)
c_code local build_ref_122
	ret_reg &ref[122]
	call_c   build_ref_828()
	call_c   Dyam_Term_Start(&ref[828],9)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_End()
	move_ret ref[122]
	c_ret

pl_code local fun37
	call_c   build_ref_123()
	call_c   Dyam_Unify_Item(&ref[123])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(2))
	call_c   Dyam_Reg_Load(4,V(3))
	call_c   Dyam_Reg_Load(6,V(4))
	call_c   Dyam_Reg_Load(8,V(5))
	call_c   Dyam_Reg_Load(10,V(6))
	call_c   Dyam_Reg_Load(12,V(7))
	call_c   Dyam_Reg_Deallocate(7)
	pl_jump  pred_bmg_null_step_7()

;; TERM 124: '*GUARD*'(dcg_body_to_lpda_handler(_B, [], _C, _D, _E, _F, _G, _H, _I)) :> []
c_code local build_ref_124
	ret_reg &ref[124]
	call_c   build_ref_123()
	call_c   Dyam_Create_Binary(I(9),&ref[123],I(0))
	move_ret ref[124]
	c_ret

c_code local build_seed_39
	ret_reg &seed[39]
	call_c   build_ref_50()
	call_c   build_ref_127()
	call_c   Dyam_Seed_Start(&ref[50],&ref[127],I(0),fun5,1)
	call_c   build_ref_126()
	call_c   Dyam_Seed_Add_Comp(&ref[126],fun38,0)
	call_c   Dyam_Seed_End()
	move_ret seed[39]
	c_ret

;; TERM 126: '*GUARD*'(dcg_body_to_lpda_handler(_B, true, _C, _D, _E, _F, _G, _H, _I))
c_code local build_ref_126
	ret_reg &ref[126]
	call_c   build_ref_51()
	call_c   build_ref_125()
	call_c   Dyam_Create_Unary(&ref[51],&ref[125])
	move_ret ref[126]
	c_ret

;; TERM 125: dcg_body_to_lpda_handler(_B, true, _C, _D, _E, _F, _G, _H, _I)
c_code local build_ref_125
	ret_reg &ref[125]
	call_c   build_ref_828()
	call_c   build_ref_186()
	call_c   Dyam_Term_Start(&ref[828],9)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(&ref[186])
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_End()
	move_ret ref[125]
	c_ret

;; TERM 186: true
c_code local build_ref_186
	ret_reg &ref[186]
	call_c   Dyam_Create_Atom("true")
	move_ret ref[186]
	c_ret

pl_code local fun38
	call_c   build_ref_126()
	call_c   Dyam_Unify_Item(&ref[126])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(2))
	call_c   Dyam_Reg_Load(4,V(3))
	call_c   Dyam_Reg_Load(6,V(4))
	call_c   Dyam_Reg_Load(8,V(5))
	call_c   Dyam_Reg_Load(10,V(6))
	call_c   Dyam_Reg_Load(12,V(7))
	call_c   Dyam_Reg_Deallocate(7)
	pl_jump  pred_bmg_null_step_7()

;; TERM 127: '*GUARD*'(dcg_body_to_lpda_handler(_B, true, _C, _D, _E, _F, _G, _H, _I)) :> []
c_code local build_ref_127
	ret_reg &ref[127]
	call_c   build_ref_126()
	call_c   Dyam_Create_Binary(I(9),&ref[126],I(0))
	move_ret ref[127]
	c_ret

c_code local build_seed_40
	ret_reg &seed[40]
	call_c   build_ref_50()
	call_c   build_ref_130()
	call_c   Dyam_Seed_Start(&ref[50],&ref[130],I(0),fun5,1)
	call_c   build_ref_129()
	call_c   Dyam_Seed_Add_Comp(&ref[129],fun39,0)
	call_c   Dyam_Seed_End()
	move_ret seed[40]
	c_ret

;; TERM 129: '*GUARD*'(dcg_body_to_lpda_handler(_B, (_C @?), _D, _E, _F, _G, _H, _I, _J))
c_code local build_ref_129
	ret_reg &ref[129]
	call_c   build_ref_51()
	call_c   build_ref_128()
	call_c   Dyam_Create_Unary(&ref[51],&ref[128])
	move_ret ref[129]
	c_ret

;; TERM 128: dcg_body_to_lpda_handler(_B, (_C @?), _D, _E, _F, _G, _H, _I, _J)
c_code local build_ref_128
	ret_reg &ref[128]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_845()
	call_c   Dyam_Create_Unary(&ref[845],V(2))
	move_ret R(0)
	call_c   build_ref_828()
	call_c   Dyam_Term_Start(&ref[828],9)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret ref[128]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 845: @?
c_code local build_ref_845
	ret_reg &ref[845]
	call_c   Dyam_Create_Atom("@?")
	move_ret ref[845]
	c_ret

c_code local build_seed_67
	ret_reg &seed[67]
	call_c   build_ref_51()
	call_c   build_ref_132()
	call_c   Dyam_Seed_Start(&ref[51],&ref[132],I(0),fun0,1)
	call_c   build_ref_133()
	call_c   Dyam_Seed_Add_Comp(&ref[133],&ref[132],0)
	call_c   Dyam_Seed_End()
	move_ret seed[67]
	c_ret

;; TERM 133: '*GUARD*'(dcg_body_to_lpda_handler(_B, (true ; _C), _D, _E, _F, _G, _H, _I, _J)) :> '$$HOLE$$'
c_code local build_ref_133
	ret_reg &ref[133]
	call_c   build_ref_132()
	call_c   Dyam_Create_Binary(I(9),&ref[132],I(7))
	move_ret ref[133]
	c_ret

;; TERM 132: '*GUARD*'(dcg_body_to_lpda_handler(_B, (true ; _C), _D, _E, _F, _G, _H, _I, _J))
c_code local build_ref_132
	ret_reg &ref[132]
	call_c   build_ref_51()
	call_c   build_ref_131()
	call_c   Dyam_Create_Unary(&ref[51],&ref[131])
	move_ret ref[132]
	c_ret

;; TERM 131: dcg_body_to_lpda_handler(_B, (true ; _C), _D, _E, _F, _G, _H, _I, _J)
c_code local build_ref_131
	ret_reg &ref[131]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_186()
	call_c   Dyam_Create_Binary(I(5),&ref[186],V(2))
	move_ret R(0)
	call_c   build_ref_828()
	call_c   Dyam_Term_Start(&ref[828],9)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret ref[131]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun39[3]=[2,build_ref_129,build_seed_67]

pl_code local fun39
	call_c   Dyam_Pool(pool_fun39)
	call_c   Dyam_Unify_Item(&ref[129])
	fail_ret
	pl_jump  fun7(&seed[67],1)

;; TERM 130: '*GUARD*'(dcg_body_to_lpda_handler(_B, (_C @?), _D, _E, _F, _G, _H, _I, _J)) :> []
c_code local build_ref_130
	ret_reg &ref[130]
	call_c   build_ref_129()
	call_c   Dyam_Create_Binary(I(9),&ref[129],I(0))
	move_ret ref[130]
	c_ret

c_code local build_seed_50
	ret_reg &seed[50]
	call_c   build_ref_50()
	call_c   build_ref_136()
	call_c   Dyam_Seed_Start(&ref[50],&ref[136],I(0),fun5,1)
	call_c   build_ref_135()
	call_c   Dyam_Seed_Add_Comp(&ref[135],fun40,0)
	call_c   Dyam_Seed_End()
	move_ret seed[50]
	c_ret

;; TERM 135: '*GUARD*'(dcg_body_to_lpda_handler(_B, _Q44062592, _D, _E, _F, _G, _H, _I, _J))
c_code local build_ref_135
	ret_reg &ref[135]
	call_c   build_ref_51()
	call_c   build_ref_134()
	call_c   Dyam_Create_Unary(&ref[51],&ref[134])
	move_ret ref[135]
	c_ret

;; TERM 134: dcg_body_to_lpda_handler(_B, _Q44062592, _D, _E, _F, _G, _H, _I, _J)
c_code local build_ref_134
	ret_reg &ref[134]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Unary(I(6),V(2))
	move_ret R(0)
	call_c   build_ref_828()
	call_c   Dyam_Term_Start(&ref[828],9)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret ref[134]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_68
	ret_reg &seed[68]
	call_c   build_ref_51()
	call_c   build_ref_138()
	call_c   Dyam_Seed_Start(&ref[51],&ref[138],I(0),fun0,1)
	call_c   build_ref_139()
	call_c   Dyam_Seed_Add_Comp(&ref[139],&ref[138],0)
	call_c   Dyam_Seed_End()
	move_ret seed[68]
	c_ret

;; TERM 139: '*GUARD*'(dcg_body_to_lpda_handler(_B, '$call'(_Q44062592), _D, _E, _F, _G, _H, _I, _J)) :> '$$HOLE$$'
c_code local build_ref_139
	ret_reg &ref[139]
	call_c   build_ref_138()
	call_c   Dyam_Create_Binary(I(9),&ref[138],I(7))
	move_ret ref[139]
	c_ret

;; TERM 138: '*GUARD*'(dcg_body_to_lpda_handler(_B, '$call'(_Q44062592), _D, _E, _F, _G, _H, _I, _J))
c_code local build_ref_138
	ret_reg &ref[138]
	call_c   build_ref_51()
	call_c   build_ref_137()
	call_c   Dyam_Create_Unary(&ref[51],&ref[137])
	move_ret ref[138]
	c_ret

;; TERM 137: dcg_body_to_lpda_handler(_B, '$call'(_Q44062592), _D, _E, _F, _G, _H, _I, _J)
c_code local build_ref_137
	ret_reg &ref[137]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Unary(I(6),V(2))
	move_ret R(0)
	call_c   build_ref_846()
	call_c   Dyam_Create_Unary(&ref[846],R(0))
	move_ret R(0)
	call_c   build_ref_828()
	call_c   Dyam_Term_Start(&ref[828],9)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret ref[137]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 846: '$call'
c_code local build_ref_846
	ret_reg &ref[846]
	call_c   Dyam_Create_Atom("$call")
	move_ret ref[846]
	c_ret

long local pool_fun40[3]=[2,build_ref_135,build_seed_68]

pl_code local fun40
	call_c   Dyam_Pool(pool_fun40)
	call_c   Dyam_Unify_Item(&ref[135])
	fail_ret
	pl_jump  fun7(&seed[68],1)

;; TERM 136: '*GUARD*'(dcg_body_to_lpda_handler(_B, _Q44062592, _D, _E, _F, _G, _H, _I, _J)) :> []
c_code local build_ref_136
	ret_reg &ref[136]
	call_c   build_ref_135()
	call_c   Dyam_Create_Binary(I(9),&ref[135],I(0))
	move_ret ref[136]
	c_ret

c_code local build_seed_41
	ret_reg &seed[41]
	call_c   build_ref_50()
	call_c   build_ref_161()
	call_c   Dyam_Seed_Start(&ref[50],&ref[161],I(0),fun5,1)
	call_c   build_ref_160()
	call_c   Dyam_Seed_Add_Comp(&ref[160],fun43,0)
	call_c   Dyam_Seed_End()
	move_ret seed[41]
	c_ret

;; TERM 160: '*GUARD*'(dcg_body_to_lpda_handler(_B, (_C ; _D), _E, _F, _G, _H, _I, _J, _K))
c_code local build_ref_160
	ret_reg &ref[160]
	call_c   build_ref_51()
	call_c   build_ref_159()
	call_c   Dyam_Create_Unary(&ref[51],&ref[159])
	move_ret ref[160]
	c_ret

;; TERM 159: dcg_body_to_lpda_handler(_B, (_C ; _D), _E, _F, _G, _H, _I, _J, _K)
c_code local build_ref_159
	ret_reg &ref[159]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Binary(I(5),V(2),V(3))
	move_ret R(0)
	call_c   build_ref_828()
	call_c   Dyam_Term_Start(&ref[828],9)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_End()
	move_ret ref[159]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_73
	ret_reg &seed[73]
	call_c   build_ref_51()
	call_c   build_ref_164()
	call_c   Dyam_Seed_Start(&ref[51],&ref[164],I(0),fun0,1)
	call_c   build_ref_165()
	call_c   Dyam_Seed_Add_Comp(&ref[165],&ref[164],0)
	call_c   Dyam_Seed_End()
	move_ret seed[73]
	c_ret

;; TERM 165: '*GUARD*'(position_and_stacks(_B, _E, _G)) :> '$$HOLE$$'
c_code local build_ref_165
	ret_reg &ref[165]
	call_c   build_ref_164()
	call_c   Dyam_Create_Binary(I(9),&ref[164],I(7))
	move_ret ref[165]
	c_ret

;; TERM 164: '*GUARD*'(position_and_stacks(_B, _E, _G))
c_code local build_ref_164
	ret_reg &ref[164]
	call_c   build_ref_51()
	call_c   build_ref_163()
	call_c   Dyam_Create_Unary(&ref[51],&ref[163])
	move_ret ref[164]
	c_ret

;; TERM 163: position_and_stacks(_B, _E, _G)
c_code local build_ref_163
	ret_reg &ref[163]
	call_c   build_ref_826()
	call_c   Dyam_Term_Start(&ref[826],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[163]
	c_ret

long local pool_fun41[2]=[1,build_seed_73]

pl_code local fun42
	call_c   Dyam_Remove_Choice()
fun41:
	call_c   Dyam_Reg_Load(0,V(8))
	call_c   Dyam_Reg_Load(2,V(9))
	move     V(13), R(4)
	move     S(5), R(5)
	move     V(14), R(6)
	move     S(5), R(7)
	move     V(15), R(8)
	move     S(5), R(9)
	pl_call  pred_handle_choice_5()
	pl_call  fun7(&seed[73],1)
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(2))
	call_c   Dyam_Reg_Load(4,V(4))
	call_c   Dyam_Reg_Load(6,V(5))
	call_c   Dyam_Reg_Load(8,V(6))
	call_c   Dyam_Reg_Load(10,V(7))
	call_c   Dyam_Reg_Load(12,V(13))
	call_c   Dyam_Reg_Load(14,V(14))
	call_c   Dyam_Reg_Load(16,V(10))
	pl_call  pred_dcg_body_to_lpda_9()
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(3))
	call_c   Dyam_Reg_Load(4,V(4))
	call_c   Dyam_Reg_Load(6,V(5))
	call_c   Dyam_Reg_Load(8,V(6))
	call_c   Dyam_Reg_Load(10,V(7))
	call_c   Dyam_Reg_Load(12,V(13))
	call_c   Dyam_Reg_Load(14,V(15))
	call_c   Dyam_Reg_Load(16,V(10))
	call_c   Dyam_Reg_Deallocate(9)
	pl_jump  pred_dcg_body_to_lpda_9()


;; TERM 162: _L -> _M
c_code local build_ref_162
	ret_reg &ref[162]
	call_c   build_ref_847()
	call_c   Dyam_Create_Binary(&ref[847],V(11),V(12))
	move_ret ref[162]
	c_ret

;; TERM 847: ->
c_code local build_ref_847
	ret_reg &ref[847]
	call_c   Dyam_Create_Atom("->")
	move_ret ref[847]
	c_ret

long local pool_fun43[4]=[65538,build_ref_160,build_ref_162,pool_fun41]

pl_code local fun43
	call_c   Dyam_Pool(pool_fun43)
	call_c   Dyam_Unify_Item(&ref[160])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun42)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(2),&ref[162])
	fail_ret
	call_c   Dyam_Cut()
	pl_fail

;; TERM 161: '*GUARD*'(dcg_body_to_lpda_handler(_B, (_C ; _D), _E, _F, _G, _H, _I, _J, _K)) :> []
c_code local build_ref_161
	ret_reg &ref[161]
	call_c   build_ref_160()
	call_c   Dyam_Create_Binary(I(9),&ref[160],I(0))
	move_ret ref[161]
	c_ret

c_code local build_seed_29
	ret_reg &seed[29]
	call_c   build_ref_50()
	call_c   build_ref_148()
	call_c   Dyam_Seed_Start(&ref[50],&ref[148],I(0),fun5,1)
	call_c   build_ref_147()
	call_c   Dyam_Seed_Add_Comp(&ref[147],fun86,0)
	call_c   Dyam_Seed_End()
	move_ret seed[29]
	c_ret

;; TERM 147: '*GUARD*'(dcg_body_to_lpda_handler(_B, (_C @+), _D, _E, _F, _G, _H, _I, _J))
c_code local build_ref_147
	ret_reg &ref[147]
	call_c   build_ref_51()
	call_c   build_ref_146()
	call_c   Dyam_Create_Unary(&ref[51],&ref[146])
	move_ret ref[147]
	c_ret

;; TERM 146: dcg_body_to_lpda_handler(_B, (_C @+), _D, _E, _F, _G, _H, _I, _J)
c_code local build_ref_146
	ret_reg &ref[146]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_848()
	call_c   Dyam_Create_Unary(&ref[848],V(2))
	move_ret R(0)
	call_c   build_ref_828()
	call_c   Dyam_Term_Start(&ref[828],9)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret ref[146]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 848: @+
c_code local build_ref_848
	ret_reg &ref[848]
	call_c   Dyam_Create_Atom("@+")
	move_ret ref[848]
	c_ret

c_code local build_seed_71
	ret_reg &seed[71]
	call_c   build_ref_51()
	call_c   build_ref_151()
	call_c   Dyam_Seed_Start(&ref[51],&ref[151],I(0),fun0,1)
	call_c   build_ref_152()
	call_c   Dyam_Seed_Add_Comp(&ref[152],&ref[151],0)
	call_c   Dyam_Seed_End()
	move_ret seed[71]
	c_ret

;; TERM 152: '*GUARD*'(dcg_body_to_lpda_handler(_B, @*{goal=> _L, vars=> _K, from=> 1, to=> _M, collect_first=> _N, collect_last=> _O, collect_loop=> _P, collect_next=> _Q, collect_pred=> _R}, _D, _E, _F, _G, _H, _I, _J)) :> '$$HOLE$$'
c_code local build_ref_152
	ret_reg &ref[152]
	call_c   build_ref_151()
	call_c   Dyam_Create_Binary(I(9),&ref[151],I(7))
	move_ret ref[152]
	c_ret

;; TERM 151: '*GUARD*'(dcg_body_to_lpda_handler(_B, @*{goal=> _L, vars=> _K, from=> 1, to=> _M, collect_first=> _N, collect_last=> _O, collect_loop=> _P, collect_next=> _Q, collect_pred=> _R}, _D, _E, _F, _G, _H, _I, _J))
c_code local build_ref_151
	ret_reg &ref[151]
	call_c   build_ref_51()
	call_c   build_ref_150()
	call_c   Dyam_Create_Unary(&ref[51],&ref[150])
	move_ret ref[151]
	c_ret

;; TERM 150: dcg_body_to_lpda_handler(_B, @*{goal=> _L, vars=> _K, from=> 1, to=> _M, collect_first=> _N, collect_last=> _O, collect_loop=> _P, collect_next=> _Q, collect_pred=> _R}, _D, _E, _F, _G, _H, _I, _J)
c_code local build_ref_150
	ret_reg &ref[150]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_849()
	call_c   Dyam_Term_Start(&ref[849],9)
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(N(1))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_828()
	call_c   Dyam_Term_Start(&ref[828],9)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret ref[150]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 849: @*!'$ft'
c_code local build_ref_849
	ret_reg &ref[849]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_859()
	call_c   Dyam_Create_List(&ref[859],I(0))
	move_ret R(0)
	call_c   build_ref_858()
	call_c   Dyam_Create_List(&ref[858],R(0))
	move_ret R(0)
	call_c   build_ref_857()
	call_c   Dyam_Create_List(&ref[857],R(0))
	move_ret R(0)
	call_c   build_ref_856()
	call_c   Dyam_Create_List(&ref[856],R(0))
	move_ret R(0)
	call_c   build_ref_855()
	call_c   Dyam_Create_List(&ref[855],R(0))
	move_ret R(0)
	call_c   build_ref_854()
	call_c   Dyam_Create_List(&ref[854],R(0))
	move_ret R(0)
	call_c   build_ref_853()
	call_c   Dyam_Create_List(&ref[853],R(0))
	move_ret R(0)
	call_c   build_ref_852()
	call_c   Dyam_Create_List(&ref[852],R(0))
	move_ret R(0)
	call_c   build_ref_851()
	call_c   Dyam_Create_List(&ref[851],R(0))
	move_ret R(0)
	call_c   build_ref_850()
	call_c   Dyam_Create_Atom_Module("$ft",&ref[850])
	move_ret ref[849]
	call_c   DYAM_Feature_2(&ref[850],R(0))
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 850: @*
c_code local build_ref_850
	ret_reg &ref[850]
	call_c   Dyam_Create_Atom("@*")
	move_ret ref[850]
	c_ret

;; TERM 851: goal
c_code local build_ref_851
	ret_reg &ref[851]
	call_c   Dyam_Create_Atom("goal")
	move_ret ref[851]
	c_ret

;; TERM 852: vars
c_code local build_ref_852
	ret_reg &ref[852]
	call_c   Dyam_Create_Atom("vars")
	move_ret ref[852]
	c_ret

;; TERM 853: from
c_code local build_ref_853
	ret_reg &ref[853]
	call_c   Dyam_Create_Atom("from")
	move_ret ref[853]
	c_ret

;; TERM 854: to
c_code local build_ref_854
	ret_reg &ref[854]
	call_c   Dyam_Create_Atom("to")
	move_ret ref[854]
	c_ret

;; TERM 855: collect_first
c_code local build_ref_855
	ret_reg &ref[855]
	call_c   Dyam_Create_Atom("collect_first")
	move_ret ref[855]
	c_ret

;; TERM 856: collect_last
c_code local build_ref_856
	ret_reg &ref[856]
	call_c   Dyam_Create_Atom("collect_last")
	move_ret ref[856]
	c_ret

;; TERM 857: collect_loop
c_code local build_ref_857
	ret_reg &ref[857]
	call_c   Dyam_Create_Atom("collect_loop")
	move_ret ref[857]
	c_ret

;; TERM 858: collect_next
c_code local build_ref_858
	ret_reg &ref[858]
	call_c   Dyam_Create_Atom("collect_next")
	move_ret ref[858]
	c_ret

;; TERM 859: collect_pred
c_code local build_ref_859
	ret_reg &ref[859]
	call_c   Dyam_Create_Atom("collect_pred")
	move_ret ref[859]
	c_ret

long local pool_fun84[2]=[1,build_seed_71]

pl_code local fun85
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(2),V(11))
	fail_ret
fun84:
	call_c   Dyam_Deallocate()
	pl_jump  fun7(&seed[71],1)


;; TERM 149: _K ^ _L
c_code local build_ref_149
	ret_reg &ref[149]
	call_c   build_ref_860()
	call_c   Dyam_Create_Binary(&ref[860],V(10),V(11))
	move_ret ref[149]
	c_ret

;; TERM 860: ^
c_code local build_ref_860
	ret_reg &ref[860]
	call_c   Dyam_Create_Atom("^")
	move_ret ref[860]
	c_ret

long local pool_fun86[5]=[131074,build_ref_147,build_ref_149,pool_fun84,pool_fun84]

pl_code local fun86
	call_c   Dyam_Pool(pool_fun86)
	call_c   Dyam_Unify_Item(&ref[147])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun85)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(2),&ref[149])
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun84()

;; TERM 148: '*GUARD*'(dcg_body_to_lpda_handler(_B, (_C @+), _D, _E, _F, _G, _H, _I, _J)) :> []
c_code local build_ref_148
	ret_reg &ref[148]
	call_c   build_ref_147()
	call_c   Dyam_Create_Binary(I(9),&ref[147],I(0))
	move_ret ref[148]
	c_ret

c_code local build_seed_30
	ret_reg &seed[30]
	call_c   build_ref_50()
	call_c   build_ref_155()
	call_c   Dyam_Seed_Start(&ref[50],&ref[155],I(0),fun5,1)
	call_c   build_ref_154()
	call_c   Dyam_Seed_Add_Comp(&ref[154],fun113,0)
	call_c   Dyam_Seed_End()
	move_ret seed[30]
	c_ret

;; TERM 154: '*GUARD*'(dcg_body_to_lpda_handler(_B, (_C @*), _D, _E, _F, _G, _H, _I, _J))
c_code local build_ref_154
	ret_reg &ref[154]
	call_c   build_ref_51()
	call_c   build_ref_153()
	call_c   Dyam_Create_Unary(&ref[51],&ref[153])
	move_ret ref[154]
	c_ret

;; TERM 153: dcg_body_to_lpda_handler(_B, (_C @*), _D, _E, _F, _G, _H, _I, _J)
c_code local build_ref_153
	ret_reg &ref[153]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_850()
	call_c   Dyam_Create_Unary(&ref[850],V(2))
	move_ret R(0)
	call_c   build_ref_828()
	call_c   Dyam_Term_Start(&ref[828],9)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret ref[153]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_72
	ret_reg &seed[72]
	call_c   build_ref_51()
	call_c   build_ref_157()
	call_c   Dyam_Seed_Start(&ref[51],&ref[157],I(0),fun0,1)
	call_c   build_ref_158()
	call_c   Dyam_Seed_Add_Comp(&ref[158],&ref[157],0)
	call_c   Dyam_Seed_End()
	move_ret seed[72]
	c_ret

;; TERM 158: '*GUARD*'(dcg_body_to_lpda_handler(_B, @*{goal=> _L, vars=> _K, from=> _M, to=> _N, collect_first=> _O, collect_last=> _P, collect_loop=> _Q, collect_next=> _R, collect_pred=> _S}, _D, _E, _F, _G, _H, _I, _J)) :> '$$HOLE$$'
c_code local build_ref_158
	ret_reg &ref[158]
	call_c   build_ref_157()
	call_c   Dyam_Create_Binary(I(9),&ref[157],I(7))
	move_ret ref[158]
	c_ret

;; TERM 157: '*GUARD*'(dcg_body_to_lpda_handler(_B, @*{goal=> _L, vars=> _K, from=> _M, to=> _N, collect_first=> _O, collect_last=> _P, collect_loop=> _Q, collect_next=> _R, collect_pred=> _S}, _D, _E, _F, _G, _H, _I, _J))
c_code local build_ref_157
	ret_reg &ref[157]
	call_c   build_ref_51()
	call_c   build_ref_156()
	call_c   Dyam_Create_Unary(&ref[51],&ref[156])
	move_ret ref[157]
	c_ret

;; TERM 156: dcg_body_to_lpda_handler(_B, @*{goal=> _L, vars=> _K, from=> _M, to=> _N, collect_first=> _O, collect_last=> _P, collect_loop=> _Q, collect_next=> _R, collect_pred=> _S}, _D, _E, _F, _G, _H, _I, _J)
c_code local build_ref_156
	ret_reg &ref[156]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_849()
	call_c   Dyam_Term_Start(&ref[849],9)
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_828()
	call_c   Dyam_Term_Start(&ref[828],9)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret ref[156]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun111[2]=[1,build_seed_72]

pl_code local fun112
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(2),V(11))
	fail_ret
fun111:
	call_c   Dyam_Deallocate()
	pl_jump  fun7(&seed[72],1)


long local pool_fun113[5]=[131074,build_ref_154,build_ref_149,pool_fun111,pool_fun111]

pl_code local fun113
	call_c   Dyam_Pool(pool_fun113)
	call_c   Dyam_Unify_Item(&ref[154])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun112)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(2),&ref[149])
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun111()

;; TERM 155: '*GUARD*'(dcg_body_to_lpda_handler(_B, (_C @*), _D, _E, _F, _G, _H, _I, _J)) :> []
c_code local build_ref_155
	ret_reg &ref[155]
	call_c   build_ref_154()
	call_c   Dyam_Create_Binary(I(9),&ref[154],I(0))
	move_ret ref[155]
	c_ret

c_code local build_seed_47
	ret_reg &seed[47]
	call_c   build_ref_50()
	call_c   build_ref_168()
	call_c   Dyam_Seed_Start(&ref[50],&ref[168],I(0),fun5,1)
	call_c   build_ref_167()
	call_c   Dyam_Seed_Add_Comp(&ref[167],fun44,0)
	call_c   Dyam_Seed_End()
	move_ret seed[47]
	c_ret

;; TERM 167: '*GUARD*'(dcg_body_to_lpda_handler(_B, (_C +> _D), _E, _F, _G, _H, _I, _J, _K))
c_code local build_ref_167
	ret_reg &ref[167]
	call_c   build_ref_51()
	call_c   build_ref_166()
	call_c   Dyam_Create_Unary(&ref[51],&ref[166])
	move_ret ref[167]
	c_ret

;; TERM 166: dcg_body_to_lpda_handler(_B, (_C +> _D), _E, _F, _G, _H, _I, _J, _K)
c_code local build_ref_166
	ret_reg &ref[166]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_861()
	call_c   Dyam_Create_Binary(&ref[861],V(2),V(3))
	move_ret R(0)
	call_c   build_ref_828()
	call_c   Dyam_Term_Start(&ref[828],9)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_End()
	move_ret ref[166]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 861: +>
c_code local build_ref_861
	ret_reg &ref[861]
	call_c   Dyam_Create_Atom("+>")
	move_ret ref[861]
	c_ret

c_code local build_seed_74
	ret_reg &seed[74]
	call_c   build_ref_51()
	call_c   build_ref_170()
	call_c   Dyam_Seed_Start(&ref[51],&ref[170],I(0),fun0,1)
	call_c   build_ref_171()
	call_c   Dyam_Seed_Add_Comp(&ref[171],&ref[170],0)
	call_c   Dyam_Seed_End()
	move_ret seed[74]
	c_ret

;; TERM 171: '*GUARD*'(dcg_body_to_lpda_handler(_B, (_C , _D), _E, _F, _G, _H, _I, _J, _K)) :> '$$HOLE$$'
c_code local build_ref_171
	ret_reg &ref[171]
	call_c   build_ref_170()
	call_c   Dyam_Create_Binary(I(9),&ref[170],I(7))
	move_ret ref[171]
	c_ret

;; TERM 170: '*GUARD*'(dcg_body_to_lpda_handler(_B, (_C , _D), _E, _F, _G, _H, _I, _J, _K))
c_code local build_ref_170
	ret_reg &ref[170]
	call_c   build_ref_51()
	call_c   build_ref_169()
	call_c   Dyam_Create_Unary(&ref[51],&ref[169])
	move_ret ref[170]
	c_ret

;; TERM 169: dcg_body_to_lpda_handler(_B, (_C , _D), _E, _F, _G, _H, _I, _J, _K)
c_code local build_ref_169
	ret_reg &ref[169]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Binary(I(4),V(2),V(3))
	move_ret R(0)
	call_c   build_ref_828()
	call_c   Dyam_Term_Start(&ref[828],9)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_End()
	move_ret ref[169]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun44[3]=[2,build_ref_167,build_seed_74]

pl_code local fun44
	call_c   Dyam_Pool(pool_fun44)
	call_c   Dyam_Unify_Item(&ref[167])
	fail_ret
	pl_jump  fun7(&seed[74],1)

;; TERM 168: '*GUARD*'(dcg_body_to_lpda_handler(_B, (_C +> _D), _E, _F, _G, _H, _I, _J, _K)) :> []
c_code local build_ref_168
	ret_reg &ref[168]
	call_c   build_ref_167()
	call_c   Dyam_Create_Binary(I(9),&ref[167],I(0))
	move_ret ref[168]
	c_ret

c_code local build_seed_43
	ret_reg &seed[43]
	call_c   build_ref_50()
	call_c   build_ref_190()
	call_c   Dyam_Seed_Start(&ref[50],&ref[190],I(0),fun5,1)
	call_c   build_ref_189()
	call_c   Dyam_Seed_Add_Comp(&ref[189],fun57,0)
	call_c   Dyam_Seed_End()
	move_ret seed[43]
	c_ret

;; TERM 189: '*GUARD*'(dcg_body_to_lpda_handler(_B, (_C -> _D), _E, _F, _G, _H, _I, _J, _K))
c_code local build_ref_189
	ret_reg &ref[189]
	call_c   build_ref_51()
	call_c   build_ref_188()
	call_c   Dyam_Create_Unary(&ref[51],&ref[188])
	move_ret ref[189]
	c_ret

;; TERM 188: dcg_body_to_lpda_handler(_B, (_C -> _D), _E, _F, _G, _H, _I, _J, _K)
c_code local build_ref_188
	ret_reg &ref[188]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_847()
	call_c   Dyam_Create_Binary(&ref[847],V(2),V(3))
	move_ret R(0)
	call_c   build_ref_828()
	call_c   Dyam_Term_Start(&ref[828],9)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_End()
	move_ret ref[188]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_77
	ret_reg &seed[77]
	call_c   build_ref_51()
	call_c   build_ref_192()
	call_c   Dyam_Seed_Start(&ref[51],&ref[192],I(0),fun0,1)
	call_c   build_ref_193()
	call_c   Dyam_Seed_Add_Comp(&ref[193],&ref[192],0)
	call_c   Dyam_Seed_End()
	move_ret seed[77]
	c_ret

;; TERM 193: '*GUARD*'(dcg_body_to_lpda_handler(_B, (_C -> _D ; fail), _E, _F, _G, _H, _I, _J, _K)) :> '$$HOLE$$'
c_code local build_ref_193
	ret_reg &ref[193]
	call_c   build_ref_192()
	call_c   Dyam_Create_Binary(I(9),&ref[192],I(7))
	move_ret ref[193]
	c_ret

;; TERM 192: '*GUARD*'(dcg_body_to_lpda_handler(_B, (_C -> _D ; fail), _E, _F, _G, _H, _I, _J, _K))
c_code local build_ref_192
	ret_reg &ref[192]
	call_c   build_ref_51()
	call_c   build_ref_191()
	call_c   Dyam_Create_Unary(&ref[51],&ref[191])
	move_ret ref[192]
	c_ret

;; TERM 191: dcg_body_to_lpda_handler(_B, (_C -> _D ; fail), _E, _F, _G, _H, _I, _J, _K)
c_code local build_ref_191
	ret_reg &ref[191]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_847()
	call_c   Dyam_Create_Binary(&ref[847],V(2),V(3))
	move_ret R(0)
	call_c   build_ref_829()
	call_c   Dyam_Create_Binary(I(5),R(0),&ref[829])
	move_ret R(0)
	call_c   build_ref_828()
	call_c   Dyam_Term_Start(&ref[828],9)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_End()
	move_ret ref[191]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun57[3]=[2,build_ref_189,build_seed_77]

pl_code local fun57
	call_c   Dyam_Pool(pool_fun57)
	call_c   Dyam_Unify_Item(&ref[189])
	fail_ret
	pl_jump  fun7(&seed[77],1)

;; TERM 190: '*GUARD*'(dcg_body_to_lpda_handler(_B, (_C -> _D), _E, _F, _G, _H, _I, _J, _K)) :> []
c_code local build_ref_190
	ret_reg &ref[190]
	call_c   build_ref_189()
	call_c   Dyam_Create_Binary(I(9),&ref[189],I(0))
	move_ret ref[190]
	c_ret

c_code local build_seed_31
	ret_reg &seed[31]
	call_c   build_ref_50()
	call_c   build_ref_196()
	call_c   Dyam_Seed_Start(&ref[50],&ref[196],I(0),fun5,1)
	call_c   build_ref_195()
	call_c   Dyam_Seed_Add_Comp(&ref[195],fun58,0)
	call_c   Dyam_Seed_End()
	move_ret seed[31]
	c_ret

;; TERM 195: '*GUARD*'(dcg_body_to_lpda_handler(_B, '$block'(_C), _D, _E, _F, _G, _H, _I, _J))
c_code local build_ref_195
	ret_reg &ref[195]
	call_c   build_ref_51()
	call_c   build_ref_194()
	call_c   Dyam_Create_Unary(&ref[51],&ref[194])
	move_ret ref[195]
	c_ret

;; TERM 194: dcg_body_to_lpda_handler(_B, '$block'(_C), _D, _E, _F, _G, _H, _I, _J)
c_code local build_ref_194
	ret_reg &ref[194]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_862()
	call_c   Dyam_Create_Unary(&ref[862],V(2))
	move_ret R(0)
	call_c   build_ref_828()
	call_c   Dyam_Term_Start(&ref[828],9)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret ref[194]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 862: '$block'
c_code local build_ref_862
	ret_reg &ref[862]
	call_c   Dyam_Create_Atom("$block")
	move_ret ref[862]
	c_ret

pl_code local fun58
	call_c   build_ref_195()
	call_c   Dyam_Unify_Item(&ref[195])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(2))
	call_c   Dyam_Reg_Load(4,V(3))
	call_c   Dyam_Reg_Load(6,V(4))
	call_c   Dyam_Reg_Load(8,V(5))
	call_c   Dyam_Reg_Load(10,V(6))
	call_c   Dyam_Reg_Load(12,V(7))
	call_c   Dyam_Reg_Load(14,V(8))
	call_c   Dyam_Reg_Load(16,V(9))
	call_c   Dyam_Reg_Deallocate(9)
	pl_jump  pred_dcg_body_to_lpda_9()

;; TERM 196: '*GUARD*'(dcg_body_to_lpda_handler(_B, '$block'(_C), _D, _E, _F, _G, _H, _I, _J)) :> []
c_code local build_ref_196
	ret_reg &ref[196]
	call_c   build_ref_195()
	call_c   Dyam_Create_Binary(I(9),&ref[195],I(0))
	move_ret ref[196]
	c_ret

c_code local build_seed_32
	ret_reg &seed[32]
	call_c   build_ref_50()
	call_c   build_ref_211()
	call_c   Dyam_Seed_Start(&ref[50],&ref[211],I(0),fun5,1)
	call_c   build_ref_210()
	call_c   Dyam_Seed_Add_Comp(&ref[210],fun59,0)
	call_c   Dyam_Seed_End()
	move_ret seed[32]
	c_ret

;; TERM 210: '*GUARD*'(dcg_body_to_lpda_handler(_B, '$answers'(_C), _D, _E, _F, _G, _H, _I, _J))
c_code local build_ref_210
	ret_reg &ref[210]
	call_c   build_ref_51()
	call_c   build_ref_209()
	call_c   Dyam_Create_Unary(&ref[51],&ref[209])
	move_ret ref[210]
	c_ret

;; TERM 209: dcg_body_to_lpda_handler(_B, '$answers'(_C), _D, _E, _F, _G, _H, _I, _J)
c_code local build_ref_209
	ret_reg &ref[209]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_843()
	call_c   Dyam_Create_Unary(&ref[843],V(2))
	move_ret R(0)
	call_c   build_ref_828()
	call_c   Dyam_Term_Start(&ref[828],9)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret ref[209]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 212: answer(_J)
c_code local build_ref_212
	ret_reg &ref[212]
	call_c   build_ref_863()
	call_c   Dyam_Create_Unary(&ref[863],V(9))
	move_ret ref[212]
	c_ret

;; TERM 863: answer
c_code local build_ref_863
	ret_reg &ref[863]
	call_c   Dyam_Create_Atom("answer")
	move_ret ref[863]
	c_ret

long local pool_fun59[3]=[2,build_ref_210,build_ref_212]

pl_code local fun59
	call_c   Dyam_Pool(pool_fun59)
	call_c   Dyam_Unify_Item(&ref[210])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(2))
	call_c   Dyam_Reg_Load(4,V(3))
	call_c   Dyam_Reg_Load(6,V(4))
	call_c   Dyam_Reg_Load(8,V(5))
	call_c   Dyam_Reg_Load(10,V(6))
	call_c   Dyam_Reg_Load(12,V(7))
	call_c   Dyam_Reg_Load(14,V(8))
	move     &ref[212], R(16)
	move     S(5), R(17)
	call_c   Dyam_Reg_Deallocate(9)
	pl_jump  pred_dcg_body_to_lpda_9()

;; TERM 211: '*GUARD*'(dcg_body_to_lpda_handler(_B, '$answers'(_C), _D, _E, _F, _G, _H, _I, _J)) :> []
c_code local build_ref_211
	ret_reg &ref[211]
	call_c   build_ref_210()
	call_c   Dyam_Create_Binary(I(9),&ref[210],I(0))
	move_ret ref[211]
	c_ret

c_code local build_seed_22
	ret_reg &seed[22]
	call_c   build_ref_50()
	call_c   build_ref_222()
	call_c   Dyam_Seed_Start(&ref[50],&ref[222],I(0),fun5,1)
	call_c   build_ref_221()
	call_c   Dyam_Seed_Add_Comp(&ref[221],fun62,0)
	call_c   Dyam_Seed_End()
	move_ret seed[22]
	c_ret

;; TERM 221: '*GUARD*'(dcg_il_body_to_lpda_handler(_B, (_C ; _D), _E, _F, _G, _H, _I, _J, _K, _L, _M))
c_code local build_ref_221
	ret_reg &ref[221]
	call_c   build_ref_51()
	call_c   build_ref_220()
	call_c   Dyam_Create_Unary(&ref[51],&ref[220])
	move_ret ref[221]
	c_ret

;; TERM 220: dcg_il_body_to_lpda_handler(_B, (_C ; _D), _E, _F, _G, _H, _I, _J, _K, _L, _M)
c_code local build_ref_220
	ret_reg &ref[220]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Binary(I(5),V(2),V(3))
	move_ret R(0)
	call_c   build_ref_835()
	call_c   Dyam_Term_Start(&ref[835],11)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_End()
	move_ret ref[220]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

pl_code local fun61
	call_c   Dyam_Remove_Choice()
fun60:
	call_c   Dyam_Reg_Load(0,V(9))
	call_c   Dyam_Reg_Load(2,V(10))
	move     V(15), R(4)
	move     S(5), R(5)
	move     V(16), R(6)
	move     S(5), R(7)
	move     V(17), R(8)
	move     S(5), R(9)
	pl_call  pred_handle_choice_5()
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(2))
	call_c   Dyam_Reg_Load(4,V(4))
	call_c   Dyam_Reg_Load(6,V(5))
	call_c   Dyam_Reg_Load(8,V(6))
	call_c   Dyam_Reg_Load(10,V(7))
	call_c   Dyam_Reg_Load(12,V(8))
	call_c   Dyam_Reg_Load(14,V(15))
	call_c   Dyam_Reg_Load(16,V(16))
	call_c   Dyam_Reg_Load(18,V(11))
	call_c   Dyam_Reg_Load(20,V(12))
	pl_call  pred_dcg_il_body_to_lpda_11()
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(3))
	call_c   Dyam_Reg_Load(4,V(4))
	call_c   Dyam_Reg_Load(6,V(5))
	call_c   Dyam_Reg_Load(8,V(6))
	call_c   Dyam_Reg_Load(10,V(7))
	call_c   Dyam_Reg_Load(12,V(8))
	call_c   Dyam_Reg_Load(14,V(15))
	call_c   Dyam_Reg_Load(16,V(17))
	call_c   Dyam_Reg_Load(18,V(11))
	call_c   Dyam_Reg_Load(20,V(12))
	call_c   Dyam_Reg_Deallocate(11)
	pl_jump  pred_dcg_il_body_to_lpda_11()


;; TERM 223: _N -> _O
c_code local build_ref_223
	ret_reg &ref[223]
	call_c   build_ref_847()
	call_c   Dyam_Create_Binary(&ref[847],V(13),V(14))
	move_ret ref[223]
	c_ret

long local pool_fun62[3]=[2,build_ref_221,build_ref_223]

pl_code local fun62
	call_c   Dyam_Pool(pool_fun62)
	call_c   Dyam_Unify_Item(&ref[221])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun61)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(2),&ref[223])
	fail_ret
	call_c   Dyam_Cut()
	pl_fail

;; TERM 222: '*GUARD*'(dcg_il_body_to_lpda_handler(_B, (_C ; _D), _E, _F, _G, _H, _I, _J, _K, _L, _M)) :> []
c_code local build_ref_222
	ret_reg &ref[222]
	call_c   build_ref_221()
	call_c   Dyam_Create_Binary(I(9),&ref[221],I(0))
	move_ret ref[222]
	c_ret

c_code local build_seed_36
	ret_reg &seed[36]
	call_c   build_ref_50()
	call_c   build_ref_226()
	call_c   Dyam_Seed_Start(&ref[50],&ref[226],I(0),fun5,1)
	call_c   build_ref_225()
	call_c   Dyam_Seed_Add_Comp(&ref[225],fun63,0)
	call_c   Dyam_Seed_End()
	move_ret seed[36]
	c_ret

;; TERM 225: '*GUARD*'(dcg_body_to_lpda_handler(_B, [_C|_D], _E, _F, _G, _H, _I, _J, _K))
c_code local build_ref_225
	ret_reg &ref[225]
	call_c   build_ref_51()
	call_c   build_ref_224()
	call_c   Dyam_Create_Unary(&ref[51],&ref[224])
	move_ret ref[225]
	c_ret

;; TERM 224: dcg_body_to_lpda_handler(_B, [_C|_D], _E, _F, _G, _H, _I, _J, _K)
c_code local build_ref_224
	ret_reg &ref[224]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(2),V(3))
	move_ret R(0)
	call_c   build_ref_828()
	call_c   Dyam_Term_Start(&ref[828],9)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_End()
	move_ret ref[224]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_82
	ret_reg &seed[82]
	call_c   build_ref_51()
	call_c   build_ref_228()
	call_c   Dyam_Seed_Start(&ref[51],&ref[228],I(0),fun0,1)
	call_c   build_ref_229()
	call_c   Dyam_Seed_Add_Comp(&ref[229],&ref[228],0)
	call_c   Dyam_Seed_End()
	move_ret seed[82]
	c_ret

;; TERM 229: '*GUARD*'(bmg_equations(_G, _H, _I, _L)) :> '$$HOLE$$'
c_code local build_ref_229
	ret_reg &ref[229]
	call_c   build_ref_228()
	call_c   Dyam_Create_Binary(I(9),&ref[228],I(7))
	move_ret ref[229]
	c_ret

;; TERM 228: '*GUARD*'(bmg_equations(_G, _H, _I, _L))
c_code local build_ref_228
	ret_reg &ref[228]
	call_c   build_ref_51()
	call_c   build_ref_227()
	call_c   Dyam_Create_Unary(&ref[51],&ref[227])
	move_ret ref[228]
	c_ret

;; TERM 227: bmg_equations(_G, _H, _I, _L)
c_code local build_ref_227
	ret_reg &ref[227]
	call_c   build_ref_864()
	call_c   Dyam_Term_Start(&ref[864],4)
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_End()
	move_ret ref[227]
	c_ret

;; TERM 864: bmg_equations
c_code local build_ref_864
	ret_reg &ref[864]
	call_c   Dyam_Create_Atom("bmg_equations")
	move_ret ref[864]
	c_ret

c_code local build_seed_83
	ret_reg &seed[83]
	call_c   build_ref_51()
	call_c   build_ref_231()
	call_c   Dyam_Seed_Start(&ref[51],&ref[231],I(0),fun0,1)
	call_c   build_ref_232()
	call_c   Dyam_Seed_Add_Comp(&ref[232],&ref[231],0)
	call_c   Dyam_Seed_End()
	move_ret seed[83]
	c_ret

;; TERM 232: '*GUARD*'(scan_to_lpda(_B, [_C|_D], _E, _F, _L, _J, _K)) :> '$$HOLE$$'
c_code local build_ref_232
	ret_reg &ref[232]
	call_c   build_ref_231()
	call_c   Dyam_Create_Binary(I(9),&ref[231],I(7))
	move_ret ref[232]
	c_ret

;; TERM 231: '*GUARD*'(scan_to_lpda(_B, [_C|_D], _E, _F, _L, _J, _K))
c_code local build_ref_231
	ret_reg &ref[231]
	call_c   build_ref_51()
	call_c   build_ref_230()
	call_c   Dyam_Create_Unary(&ref[51],&ref[230])
	move_ret ref[231]
	c_ret

;; TERM 230: scan_to_lpda(_B, [_C|_D], _E, _F, _L, _J, _K)
c_code local build_ref_230
	ret_reg &ref[230]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(2),V(3))
	move_ret R(0)
	call_c   build_ref_865()
	call_c   Dyam_Term_Start(&ref[865],7)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_End()
	move_ret ref[230]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 865: scan_to_lpda
c_code local build_ref_865
	ret_reg &ref[865]
	call_c   Dyam_Create_Atom("scan_to_lpda")
	move_ret ref[865]
	c_ret

long local pool_fun63[4]=[3,build_ref_225,build_seed_82,build_seed_83]

pl_code local fun63
	call_c   Dyam_Pool(pool_fun63)
	call_c   Dyam_Unify_Item(&ref[225])
	fail_ret
	call_c   Dyam_Allocate(0)
	pl_call  fun7(&seed[82],1)
	call_c   Dyam_Deallocate()
	pl_jump  fun7(&seed[83],1)

;; TERM 226: '*GUARD*'(dcg_body_to_lpda_handler(_B, [_C|_D], _E, _F, _G, _H, _I, _J, _K)) :> []
c_code local build_ref_226
	ret_reg &ref[226]
	call_c   build_ref_225()
	call_c   Dyam_Create_Binary(I(9),&ref[225],I(0))
	move_ret ref[226]
	c_ret

c_code local build_seed_27
	ret_reg &seed[27]
	call_c   build_ref_36()
	call_c   build_ref_198()
	call_c   Dyam_Seed_Start(&ref[36],&ref[198],I(0),fun4,1)
	call_c   build_ref_199()
	call_c   Dyam_Seed_Add_Comp(&ref[199],fun119,0)
	call_c   Dyam_Seed_End()
	move_ret seed[27]
	c_ret

;; TERM 199: '*CITEM*'('call_dcg_kleene_args/7', _A)
c_code local build_ref_199
	ret_reg &ref[199]
	call_c   build_ref_39()
	call_c   build_ref_24()
	call_c   Dyam_Create_Binary(&ref[39],&ref[24],V(0))
	move_ret ref[199]
	c_ret

;; TERM 24: 'call_dcg_kleene_args/7'
c_code local build_ref_24
	ret_reg &ref[24]
	call_c   Dyam_Create_Atom("call_dcg_kleene_args/7")
	move_ret ref[24]
	c_ret

;; TERM 343: '$CLOSURE'('$fun'(118, 0, 1147512588), '$TUPPLE'(35074870877108))
c_code local build_ref_343
	ret_reg &ref[343]
	call_c   build_ref_342()
	call_c   Dyam_Closure_Aux(fun118,&ref[342])
	move_ret ref[343]
	c_ret

;; TERM 340: '$CLOSURE'('$fun'(117, 0, 1147502048), '$TUPPLE'(35074870877048))
c_code local build_ref_340
	ret_reg &ref[340]
	call_c   build_ref_339()
	call_c   Dyam_Closure_Aux(fun117,&ref[339])
	move_ret ref[340]
	c_ret

;; TERM 337: '$CLOSURE'('$fun'(116, 0, 1147492436), '$TUPPLE'(35074870876988))
c_code local build_ref_337
	ret_reg &ref[337]
	call_c   build_ref_336()
	call_c   Dyam_Closure_Aux(fun116,&ref[336])
	move_ret ref[337]
	c_ret

c_code local build_seed_78
	ret_reg &seed[78]
	call_c   build_ref_51()
	call_c   build_ref_201()
	call_c   Dyam_Seed_Start(&ref[51],&ref[201],I(0),fun0,1)
	call_c   build_ref_202()
	call_c   Dyam_Seed_Add_Comp(&ref[202],&ref[201],0)
	call_c   Dyam_Seed_End()
	move_ret seed[78]
	c_ret

;; TERM 202: '*GUARD*'(append(_F, _G, _I)) :> '$$HOLE$$'
c_code local build_ref_202
	ret_reg &ref[202]
	call_c   build_ref_201()
	call_c   Dyam_Create_Binary(I(9),&ref[201],I(7))
	move_ret ref[202]
	c_ret

;; TERM 201: '*GUARD*'(append(_F, _G, _I))
c_code local build_ref_201
	ret_reg &ref[201]
	call_c   build_ref_51()
	call_c   build_ref_200()
	call_c   Dyam_Create_Unary(&ref[51],&ref[200])
	move_ret ref[201]
	c_ret

;; TERM 200: append(_F, _G, _I)
c_code local build_ref_200
	ret_reg &ref[200]
	call_c   build_ref_866()
	call_c   Dyam_Term_Start(&ref[866],3)
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_End()
	move_ret ref[200]
	c_ret

;; TERM 866: append
c_code local build_ref_866
	ret_reg &ref[866]
	call_c   Dyam_Create_Atom("append")
	move_ret ref[866]
	c_ret

c_code local build_seed_79
	ret_reg &seed[79]
	call_c   build_ref_51()
	call_c   build_ref_204()
	call_c   Dyam_Seed_Start(&ref[51],&ref[204],I(0),fun0,1)
	call_c   build_ref_205()
	call_c   Dyam_Seed_Add_Comp(&ref[205],&ref[204],0)
	call_c   Dyam_Seed_End()
	move_ret seed[79]
	c_ret

;; TERM 205: '*GUARD*'(append(_E, _I, _H)) :> '$$HOLE$$'
c_code local build_ref_205
	ret_reg &ref[205]
	call_c   build_ref_204()
	call_c   Dyam_Create_Binary(I(9),&ref[204],I(7))
	move_ret ref[205]
	c_ret

;; TERM 204: '*GUARD*'(append(_E, _I, _H))
c_code local build_ref_204
	ret_reg &ref[204]
	call_c   build_ref_51()
	call_c   build_ref_203()
	call_c   Dyam_Create_Unary(&ref[51],&ref[203])
	move_ret ref[204]
	c_ret

;; TERM 203: append(_E, _I, _H)
c_code local build_ref_203
	ret_reg &ref[203]
	call_c   build_ref_866()
	call_c   Dyam_Term_Start(&ref[866],3)
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[203]
	c_ret

c_code local build_seed_80
	ret_reg &seed[80]
	call_c   build_ref_0()
	call_c   build_ref_207()
	call_c   Dyam_Seed_Start(&ref[0],&ref[207],&ref[207],fun0,1)
	call_c   build_ref_208()
	call_c   Dyam_Seed_Add_Comp(&ref[208],&ref[207],0)
	call_c   Dyam_Seed_End()
	move_ret seed[80]
	c_ret

;; TERM 208: '*RITEM*'(_A, return(_B, _C, _D, _E, _F, _G, [_B,_C,_D|_H])) :> '$$HOLE$$'
c_code local build_ref_208
	ret_reg &ref[208]
	call_c   build_ref_207()
	call_c   Dyam_Create_Binary(I(9),&ref[207],I(7))
	move_ret ref[208]
	c_ret

;; TERM 207: '*RITEM*'(_A, return(_B, _C, _D, _E, _F, _G, [_B,_C,_D|_H]))
c_code local build_ref_207
	ret_reg &ref[207]
	call_c   build_ref_0()
	call_c   build_ref_206()
	call_c   Dyam_Create_Binary(&ref[0],V(0),&ref[206])
	move_ret ref[207]
	c_ret

;; TERM 206: return(_B, _C, _D, _E, _F, _G, [_B,_C,_D|_H])
c_code local build_ref_206
	ret_reg &ref[206]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Tupple(1,3,V(7))
	move_ret R(0)
	call_c   build_ref_867()
	call_c   Dyam_Term_Start(&ref[867],7)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_End()
	move_ret ref[206]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 867: return
c_code local build_ref_867
	ret_reg &ref[867]
	call_c   Dyam_Create_Atom("return")
	move_ret ref[867]
	c_ret

long local pool_fun116[4]=[3,build_seed_78,build_seed_79,build_seed_80]

pl_code local fun116
	call_c   Dyam_Pool(pool_fun116)
	call_c   Dyam_Allocate(0)
	pl_call  fun7(&seed[78],1)
	pl_call  fun7(&seed[79],1)
	call_c   Dyam_Deallocate()
	pl_jump  fun2(&seed[80],2)

;; TERM 336: '$TUPPLE'(35074870876988)
c_code local build_ref_336
	ret_reg &ref[336]
	call_c   Dyam_Create_Simple_Tupple(0,297795584)
	move_ret ref[336]
	c_ret

long local pool_fun117[2]=[1,build_ref_337]

pl_code local fun117
	call_c   Dyam_Pool(pool_fun117)
	call_c   Dyam_Allocate(0)
	move     &ref[337], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	move     V(6), R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  fun23()

;; TERM 339: '$TUPPLE'(35074870877048)
c_code local build_ref_339
	ret_reg &ref[339]
	call_c   Dyam_Create_Simple_Tupple(0,293601280)
	move_ret ref[339]
	c_ret

long local pool_fun118[2]=[1,build_ref_340]

pl_code local fun118
	call_c   Dyam_Pool(pool_fun118)
	call_c   Dyam_Allocate(0)
	move     &ref[340], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	move     V(5), R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  fun23()

;; TERM 342: '$TUPPLE'(35074870877108)
c_code local build_ref_342
	ret_reg &ref[342]
	call_c   Dyam_Create_Simple_Tupple(0,285212672)
	move_ret ref[342]
	c_ret

long local pool_fun119[3]=[2,build_ref_199,build_ref_343]

pl_code local fun119
	call_c   Dyam_Pool(pool_fun119)
	call_c   Dyam_Unify_Item(&ref[199])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[343], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	move     V(4), R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  fun23()

;; TERM 198: '*FIRST*'('call_dcg_kleene_args/7') :> []
c_code local build_ref_198
	ret_reg &ref[198]
	call_c   build_ref_197()
	call_c   Dyam_Create_Binary(I(9),&ref[197],I(0))
	move_ret ref[198]
	c_ret

;; TERM 197: '*FIRST*'('call_dcg_kleene_args/7')
c_code local build_ref_197
	ret_reg &ref[197]
	call_c   build_ref_36()
	call_c   build_ref_24()
	call_c   Dyam_Create_Unary(&ref[36],&ref[24])
	move_ret ref[197]
	c_ret

c_code local build_seed_20
	ret_reg &seed[20]
	call_c   build_ref_50()
	call_c   build_ref_215()
	call_c   Dyam_Seed_Start(&ref[50],&ref[215],I(0),fun5,1)
	call_c   build_ref_214()
	call_c   Dyam_Seed_Add_Comp(&ref[214],fun122,0)
	call_c   Dyam_Seed_End()
	move_ret seed[20]
	c_ret

;; TERM 214: '*GUARD*'(dcg_il_body_to_lpda_handler(_B, (_C @*), _D, _E, _F, _G, _H, _I, _J, _K, _L))
c_code local build_ref_214
	ret_reg &ref[214]
	call_c   build_ref_51()
	call_c   build_ref_213()
	call_c   Dyam_Create_Unary(&ref[51],&ref[213])
	move_ret ref[214]
	c_ret

;; TERM 213: dcg_il_body_to_lpda_handler(_B, (_C @*), _D, _E, _F, _G, _H, _I, _J, _K, _L)
c_code local build_ref_213
	ret_reg &ref[213]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_850()
	call_c   Dyam_Create_Unary(&ref[850],V(2))
	move_ret R(0)
	call_c   build_ref_835()
	call_c   Dyam_Term_Start(&ref[835],11)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_End()
	move_ret ref[213]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_81
	ret_reg &seed[81]
	call_c   build_ref_51()
	call_c   build_ref_218()
	call_c   Dyam_Seed_Start(&ref[51],&ref[218],I(0),fun0,1)
	call_c   build_ref_219()
	call_c   Dyam_Seed_Add_Comp(&ref[219],&ref[218],0)
	call_c   Dyam_Seed_End()
	move_ret seed[81]
	c_ret

;; TERM 219: '*GUARD*'(dcg_il_body_to_lpda_handler(_B, @*{goal=> _N, vars=> _M, from=> _O, to=> _P, collect_first=> _Q, collect_last=> _R, collect_loop=> _S, collect_next=> _T, collect_pred=> _U}, _D, _E, _F, _G, _H, _I, _J, _K, _L)) :> '$$HOLE$$'
c_code local build_ref_219
	ret_reg &ref[219]
	call_c   build_ref_218()
	call_c   Dyam_Create_Binary(I(9),&ref[218],I(7))
	move_ret ref[219]
	c_ret

;; TERM 218: '*GUARD*'(dcg_il_body_to_lpda_handler(_B, @*{goal=> _N, vars=> _M, from=> _O, to=> _P, collect_first=> _Q, collect_last=> _R, collect_loop=> _S, collect_next=> _T, collect_pred=> _U}, _D, _E, _F, _G, _H, _I, _J, _K, _L))
c_code local build_ref_218
	ret_reg &ref[218]
	call_c   build_ref_51()
	call_c   build_ref_217()
	call_c   Dyam_Create_Unary(&ref[51],&ref[217])
	move_ret ref[218]
	c_ret

;; TERM 217: dcg_il_body_to_lpda_handler(_B, @*{goal=> _N, vars=> _M, from=> _O, to=> _P, collect_first=> _Q, collect_last=> _R, collect_loop=> _S, collect_next=> _T, collect_pred=> _U}, _D, _E, _F, _G, _H, _I, _J, _K, _L)
c_code local build_ref_217
	ret_reg &ref[217]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_849()
	call_c   Dyam_Term_Start(&ref[849],9)
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_835()
	call_c   Dyam_Term_Start(&ref[835],11)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_End()
	move_ret ref[217]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun120[2]=[1,build_seed_81]

pl_code local fun121
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(2),V(13))
	fail_ret
fun120:
	call_c   Dyam_Deallocate()
	pl_jump  fun7(&seed[81],1)


;; TERM 216: _M ^ _N
c_code local build_ref_216
	ret_reg &ref[216]
	call_c   build_ref_860()
	call_c   Dyam_Create_Binary(&ref[860],V(12),V(13))
	move_ret ref[216]
	c_ret

long local pool_fun122[5]=[131074,build_ref_214,build_ref_216,pool_fun120,pool_fun120]

pl_code local fun122
	call_c   Dyam_Pool(pool_fun122)
	call_c   Dyam_Unify_Item(&ref[214])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun121)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(2),&ref[216])
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun120()

;; TERM 215: '*GUARD*'(dcg_il_body_to_lpda_handler(_B, (_C @*), _D, _E, _F, _G, _H, _I, _J, _K, _L)) :> []
c_code local build_ref_215
	ret_reg &ref[215]
	call_c   build_ref_214()
	call_c   Dyam_Create_Binary(I(9),&ref[214],I(0))
	move_ret ref[215]
	c_ret

c_code local build_seed_34
	ret_reg &seed[34]
	call_c   build_ref_50()
	call_c   build_ref_235()
	call_c   Dyam_Seed_Start(&ref[50],&ref[235],I(0),fun5,1)
	call_c   build_ref_234()
	call_c   Dyam_Seed_Add_Comp(&ref[234],fun64,0)
	call_c   Dyam_Seed_End()
	move_ret seed[34]
	c_ret

;; TERM 234: '*GUARD*'(dcg_body_to_lpda_handler(_B, '$dcg_pos'(_C), _D, _E, _F, _G, _H, _I, _J))
c_code local build_ref_234
	ret_reg &ref[234]
	call_c   build_ref_51()
	call_c   build_ref_233()
	call_c   Dyam_Create_Unary(&ref[51],&ref[233])
	move_ret ref[234]
	c_ret

;; TERM 233: dcg_body_to_lpda_handler(_B, '$dcg_pos'(_C), _D, _E, _F, _G, _H, _I, _J)
c_code local build_ref_233
	ret_reg &ref[233]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_868()
	call_c   Dyam_Create_Unary(&ref[868],V(2))
	move_ret R(0)
	call_c   build_ref_828()
	call_c   Dyam_Term_Start(&ref[828],9)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret ref[233]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 868: '$dcg_pos'
c_code local build_ref_868
	ret_reg &ref[868]
	call_c   Dyam_Create_Atom("$dcg_pos")
	move_ret ref[868]
	c_ret

;; TERM 236: {_D = _C}
c_code local build_ref_236
	ret_reg &ref[236]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_869()
	call_c   Dyam_Create_Binary(&ref[869],V(3),V(2))
	move_ret R(0)
	call_c   Dyam_Create_Unary(I(2),R(0))
	move_ret ref[236]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 869: =
c_code local build_ref_869
	ret_reg &ref[869]
	call_c   Dyam_Create_Atom("=")
	move_ret ref[869]
	c_ret

long local pool_fun64[3]=[2,build_ref_234,build_ref_236]

pl_code local fun64
	call_c   Dyam_Pool(pool_fun64)
	call_c   Dyam_Unify_Item(&ref[234])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(1))
	move     &ref[236], R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Load(4,V(3))
	call_c   Dyam_Reg_Load(6,V(4))
	call_c   Dyam_Reg_Load(8,V(5))
	call_c   Dyam_Reg_Load(10,V(6))
	call_c   Dyam_Reg_Load(12,V(7))
	call_c   Dyam_Reg_Load(14,V(8))
	call_c   Dyam_Reg_Load(16,V(9))
	call_c   Dyam_Reg_Deallocate(9)
	pl_jump  pred_dcg_body_to_lpda_9()

;; TERM 235: '*GUARD*'(dcg_body_to_lpda_handler(_B, '$dcg_pos'(_C), _D, _E, _F, _G, _H, _I, _J)) :> []
c_code local build_ref_235
	ret_reg &ref[235]
	call_c   build_ref_234()
	call_c   Dyam_Create_Binary(I(9),&ref[234],I(0))
	move_ret ref[235]
	c_ret

c_code local build_seed_35
	ret_reg &seed[35]
	call_c   build_ref_50()
	call_c   build_ref_239()
	call_c   Dyam_Seed_Start(&ref[50],&ref[239],I(0),fun5,1)
	call_c   build_ref_238()
	call_c   Dyam_Seed_Add_Comp(&ref[238],fun65,0)
	call_c   Dyam_Seed_End()
	move_ret seed[35]
	c_ret

;; TERM 238: '*GUARD*'(dcg_body_to_lpda_handler(_B, '$noskip'([_C|_D]), _E, _F, _G, _H, _I, _J, _K))
c_code local build_ref_238
	ret_reg &ref[238]
	call_c   build_ref_51()
	call_c   build_ref_237()
	call_c   Dyam_Create_Unary(&ref[51],&ref[237])
	move_ret ref[238]
	c_ret

;; TERM 237: dcg_body_to_lpda_handler(_B, '$noskip'([_C|_D]), _E, _F, _G, _H, _I, _J, _K)
c_code local build_ref_237
	ret_reg &ref[237]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(2),V(3))
	move_ret R(0)
	call_c   build_ref_870()
	call_c   Dyam_Create_Unary(&ref[870],R(0))
	move_ret R(0)
	call_c   build_ref_828()
	call_c   Dyam_Term_Start(&ref[828],9)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_End()
	move_ret ref[237]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 870: '$noskip'
c_code local build_ref_870
	ret_reg &ref[870]
	call_c   Dyam_Create_Atom("$noskip")
	move_ret ref[870]
	c_ret

c_code local build_seed_84
	ret_reg &seed[84]
	call_c   build_ref_51()
	call_c   build_ref_241()
	call_c   Dyam_Seed_Start(&ref[51],&ref[241],I(0),fun0,1)
	call_c   build_ref_242()
	call_c   Dyam_Seed_Add_Comp(&ref[242],&ref[241],0)
	call_c   Dyam_Seed_End()
	move_ret seed[84]
	c_ret

;; TERM 242: '*GUARD*'(scan_to_lpda(_B, '$noskip'([_C|_D]), _E, _F, _L, _J, _K)) :> '$$HOLE$$'
c_code local build_ref_242
	ret_reg &ref[242]
	call_c   build_ref_241()
	call_c   Dyam_Create_Binary(I(9),&ref[241],I(7))
	move_ret ref[242]
	c_ret

;; TERM 241: '*GUARD*'(scan_to_lpda(_B, '$noskip'([_C|_D]), _E, _F, _L, _J, _K))
c_code local build_ref_241
	ret_reg &ref[241]
	call_c   build_ref_51()
	call_c   build_ref_240()
	call_c   Dyam_Create_Unary(&ref[51],&ref[240])
	move_ret ref[241]
	c_ret

;; TERM 240: scan_to_lpda(_B, '$noskip'([_C|_D]), _E, _F, _L, _J, _K)
c_code local build_ref_240
	ret_reg &ref[240]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(2),V(3))
	move_ret R(0)
	call_c   build_ref_870()
	call_c   Dyam_Create_Unary(&ref[870],R(0))
	move_ret R(0)
	call_c   build_ref_865()
	call_c   Dyam_Term_Start(&ref[865],7)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_End()
	move_ret ref[240]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun65[4]=[3,build_ref_238,build_seed_82,build_seed_84]

pl_code local fun65
	call_c   Dyam_Pool(pool_fun65)
	call_c   Dyam_Unify_Item(&ref[238])
	fail_ret
	call_c   Dyam_Allocate(0)
	pl_call  fun7(&seed[82],1)
	call_c   Dyam_Deallocate()
	pl_jump  fun7(&seed[84],1)

;; TERM 239: '*GUARD*'(dcg_body_to_lpda_handler(_B, '$noskip'([_C|_D]), _E, _F, _G, _H, _I, _J, _K)) :> []
c_code local build_ref_239
	ret_reg &ref[239]
	call_c   build_ref_238()
	call_c   Dyam_Create_Binary(I(9),&ref[238],I(0))
	move_ret ref[239]
	c_ret

c_code local build_seed_42
	ret_reg &seed[42]
	call_c   build_ref_50()
	call_c   build_ref_245()
	call_c   Dyam_Seed_Start(&ref[50],&ref[245],I(0),fun5,1)
	call_c   build_ref_244()
	call_c   Dyam_Seed_Add_Comp(&ref[244],fun66,0)
	call_c   Dyam_Seed_End()
	move_ret seed[42]
	c_ret

;; TERM 244: '*GUARD*'(dcg_body_to_lpda_handler(_B, '*COMMIT*'(_C), _D, _E, _F, _G, _H, ('*SETCUT*' :> _I), _J))
c_code local build_ref_244
	ret_reg &ref[244]
	call_c   build_ref_51()
	call_c   build_ref_243()
	call_c   Dyam_Create_Unary(&ref[51],&ref[243])
	move_ret ref[244]
	c_ret

;; TERM 243: dcg_body_to_lpda_handler(_B, '*COMMIT*'(_C), _D, _E, _F, _G, _H, ('*SETCUT*' :> _I), _J)
c_code local build_ref_243
	ret_reg &ref[243]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_871()
	call_c   Dyam_Create_Unary(&ref[871],V(2))
	move_ret R(0)
	call_c   build_ref_872()
	call_c   Dyam_Create_Binary(I(9),&ref[872],V(8))
	move_ret R(1)
	call_c   build_ref_828()
	call_c   Dyam_Term_Start(&ref[828],9)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret ref[243]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 872: '*SETCUT*'
c_code local build_ref_872
	ret_reg &ref[872]
	call_c   Dyam_Create_Atom("*SETCUT*")
	move_ret ref[872]
	c_ret

;; TERM 871: '*COMMIT*'
c_code local build_ref_871
	ret_reg &ref[871]
	call_c   Dyam_Create_Atom("*COMMIT*")
	move_ret ref[871]
	c_ret

;; TERM 247: '*CUT*' :> _H
c_code local build_ref_247
	ret_reg &ref[247]
	call_c   build_ref_246()
	call_c   Dyam_Create_Binary(I(9),&ref[246],V(7))
	move_ret ref[247]
	c_ret

;; TERM 246: '*CUT*'
c_code local build_ref_246
	ret_reg &ref[246]
	call_c   Dyam_Create_Atom("*CUT*")
	move_ret ref[246]
	c_ret

;; TERM 248: rec_prolog
c_code local build_ref_248
	ret_reg &ref[248]
	call_c   Dyam_Create_Atom("rec_prolog")
	move_ret ref[248]
	c_ret

long local pool_fun66[4]=[3,build_ref_244,build_ref_247,build_ref_248]

pl_code local fun66
	call_c   Dyam_Pool(pool_fun66)
	call_c   Dyam_Unify_Item(&ref[244])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(2))
	call_c   Dyam_Reg_Load(4,V(3))
	call_c   Dyam_Reg_Load(6,V(4))
	call_c   Dyam_Reg_Load(8,V(5))
	call_c   Dyam_Reg_Load(10,V(6))
	move     &ref[247], R(12)
	move     S(5), R(13)
	call_c   Dyam_Reg_Load(14,V(8))
	move     &ref[248], R(16)
	move     0, R(17)
	call_c   Dyam_Reg_Deallocate(9)
	pl_jump  pred_dcg_body_to_lpda_9()

;; TERM 245: '*GUARD*'(dcg_body_to_lpda_handler(_B, '*COMMIT*'(_C), _D, _E, _F, _G, _H, ('*SETCUT*' :> _I), _J)) :> []
c_code local build_ref_245
	ret_reg &ref[245]
	call_c   build_ref_244()
	call_c   Dyam_Create_Binary(I(9),&ref[244],I(0))
	move_ret ref[245]
	c_ret

c_code local build_seed_44
	ret_reg &seed[44]
	call_c   build_ref_50()
	call_c   build_ref_251()
	call_c   Dyam_Seed_Start(&ref[50],&ref[251],I(0),fun5,1)
	call_c   build_ref_250()
	call_c   Dyam_Seed_Add_Comp(&ref[250],fun67,0)
	call_c   Dyam_Seed_End()
	move_ret seed[44]
	c_ret

;; TERM 250: '*GUARD*'(dcg_body_to_lpda_handler(_B, (_C -> _D ; _E), _F, _G, _H, _I, _J, _K, _L))
c_code local build_ref_250
	ret_reg &ref[250]
	call_c   build_ref_51()
	call_c   build_ref_249()
	call_c   Dyam_Create_Unary(&ref[51],&ref[249])
	move_ret ref[250]
	c_ret

;; TERM 249: dcg_body_to_lpda_handler(_B, (_C -> _D ; _E), _F, _G, _H, _I, _J, _K, _L)
c_code local build_ref_249
	ret_reg &ref[249]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_847()
	call_c   Dyam_Create_Binary(&ref[847],V(2),V(3))
	move_ret R(0)
	call_c   Dyam_Create_Binary(I(5),R(0),V(4))
	move_ret R(0)
	call_c   build_ref_828()
	call_c   Dyam_Term_Start(&ref[828],9)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_End()
	move_ret ref[249]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_85
	ret_reg &seed[85]
	call_c   build_ref_51()
	call_c   build_ref_253()
	call_c   Dyam_Seed_Start(&ref[51],&ref[253],I(0),fun0,1)
	call_c   build_ref_254()
	call_c   Dyam_Seed_Add_Comp(&ref[254],&ref[253],0)
	call_c   Dyam_Seed_End()
	move_ret seed[85]
	c_ret

;; TERM 254: '*GUARD*'(dcg_body_to_lpda_handler(_B, ('*COMMIT*'(_C) , _D ; _E), _F, _G, _H, _I, _J, _K, _L)) :> '$$HOLE$$'
c_code local build_ref_254
	ret_reg &ref[254]
	call_c   build_ref_253()
	call_c   Dyam_Create_Binary(I(9),&ref[253],I(7))
	move_ret ref[254]
	c_ret

;; TERM 253: '*GUARD*'(dcg_body_to_lpda_handler(_B, ('*COMMIT*'(_C) , _D ; _E), _F, _G, _H, _I, _J, _K, _L))
c_code local build_ref_253
	ret_reg &ref[253]
	call_c   build_ref_51()
	call_c   build_ref_252()
	call_c   Dyam_Create_Unary(&ref[51],&ref[252])
	move_ret ref[253]
	c_ret

;; TERM 252: dcg_body_to_lpda_handler(_B, ('*COMMIT*'(_C) , _D ; _E), _F, _G, _H, _I, _J, _K, _L)
c_code local build_ref_252
	ret_reg &ref[252]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_871()
	call_c   Dyam_Create_Unary(&ref[871],V(2))
	move_ret R(0)
	call_c   Dyam_Create_Binary(I(4),R(0),V(3))
	move_ret R(0)
	call_c   Dyam_Create_Binary(I(5),R(0),V(4))
	move_ret R(0)
	call_c   build_ref_828()
	call_c   Dyam_Term_Start(&ref[828],9)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_End()
	move_ret ref[252]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun67[3]=[2,build_ref_250,build_seed_85]

pl_code local fun67
	call_c   Dyam_Pool(pool_fun67)
	call_c   Dyam_Unify_Item(&ref[250])
	fail_ret
	pl_jump  fun7(&seed[85],1)

;; TERM 251: '*GUARD*'(dcg_body_to_lpda_handler(_B, (_C -> _D ; _E), _F, _G, _H, _I, _J, _K, _L)) :> []
c_code local build_ref_251
	ret_reg &ref[251]
	call_c   build_ref_250()
	call_c   Dyam_Create_Binary(I(9),&ref[250],I(0))
	move_ret ref[251]
	c_ret

c_code local build_seed_3
	ret_reg &seed[3]
	call_c   build_ref_36()
	call_c   build_ref_257()
	call_c   Dyam_Seed_Start(&ref[36],&ref[257],I(0),fun4,1)
	call_c   build_ref_258()
	call_c   Dyam_Seed_Add_Comp(&ref[258],fun73,0)
	call_c   Dyam_Seed_End()
	move_ret seed[3]
	c_ret

;; TERM 258: '*CITEM*'('call_term_module_shift/5'(_B, (dcg _C / _D)), _A)
c_code local build_ref_258
	ret_reg &ref[258]
	call_c   build_ref_39()
	call_c   build_ref_255()
	call_c   Dyam_Create_Binary(&ref[39],&ref[255],V(0))
	move_ret ref[258]
	c_ret

;; TERM 255: 'call_term_module_shift/5'(_B, (dcg _C / _D))
c_code local build_ref_255
	ret_reg &ref[255]
	call_c   build_ref_873()
	call_c   build_ref_674()
	call_c   Dyam_Create_Binary(&ref[873],V(1),&ref[674])
	move_ret ref[255]
	c_ret

;; TERM 674: dcg _C / _D
c_code local build_ref_674
	ret_reg &ref[674]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_840()
	call_c   Dyam_Create_Binary(&ref[840],V(2),V(3))
	move_ret R(0)
	call_c   build_ref_839()
	call_c   Dyam_Create_Unary(&ref[839],R(0))
	move_ret ref[674]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 873: 'call_term_module_shift/5'
c_code local build_ref_873
	ret_reg &ref[873]
	call_c   Dyam_Create_Atom("call_term_module_shift/5")
	move_ret ref[873]
	c_ret

;; TERM 260: [_C|_J]
c_code local build_ref_260
	ret_reg &ref[260]
	call_c   Dyam_Create_List(V(2),V(9))
	move_ret ref[260]
	c_ret

;; TERM 261: [_E|_J]
c_code local build_ref_261
	ret_reg &ref[261]
	call_c   Dyam_Create_List(V(4),V(9))
	move_ret ref[261]
	c_ret

c_code local build_seed_86
	ret_reg &seed[86]
	call_c   build_ref_0()
	call_c   build_ref_263()
	call_c   Dyam_Seed_Start(&ref[0],&ref[263],&ref[263],fun0,1)
	call_c   build_ref_264()
	call_c   Dyam_Seed_Add_Comp(&ref[264],&ref[263],0)
	call_c   Dyam_Seed_End()
	move_ret seed[86]
	c_ret

;; TERM 264: '*RITEM*'(_A, return((dcg _E / _D), _F, _G)) :> '$$HOLE$$'
c_code local build_ref_264
	ret_reg &ref[264]
	call_c   build_ref_263()
	call_c   Dyam_Create_Binary(I(9),&ref[263],I(7))
	move_ret ref[264]
	c_ret

;; TERM 263: '*RITEM*'(_A, return((dcg _E / _D), _F, _G))
c_code local build_ref_263
	ret_reg &ref[263]
	call_c   build_ref_0()
	call_c   build_ref_262()
	call_c   Dyam_Create_Binary(&ref[0],V(0),&ref[262])
	move_ret ref[263]
	c_ret

;; TERM 262: return((dcg _E / _D), _F, _G)
c_code local build_ref_262
	ret_reg &ref[262]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_840()
	call_c   Dyam_Create_Binary(&ref[840],V(4),V(3))
	move_ret R(0)
	call_c   build_ref_839()
	call_c   Dyam_Create_Unary(&ref[839],R(0))
	move_ret R(0)
	call_c   build_ref_867()
	call_c   Dyam_Term_Start(&ref[867],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[262]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun68[4]=[3,build_ref_260,build_ref_261,build_seed_86]

pl_code local fun71
	call_c   Dyam_Remove_Choice()
fun70:
	call_c   DYAM_evpred_atom_to_module(V(2),I(0),V(8))
	fail_ret
	call_c   Dyam_Unify(V(7),V(1))
	fail_ret
fun68:
	call_c   DYAM_evpred_atom_to_module(V(4),V(7),V(2))
	fail_ret
	call_c   DYAM_evpred_length(V(9),V(3))
	fail_ret
	call_c   DYAM_evpred_univ(V(5),&ref[260])
	fail_ret
	call_c   DYAM_evpred_univ(V(6),&ref[261])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_jump  fun2(&seed[86],2)



pl_code local fun72
	call_c   Dyam_Update_Choice(fun71)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(1),I(0))
	fail_ret
	call_c   Dyam_Cut()
	pl_fail

;; TERM 259: module_import((dcg _C / _D), _H)
c_code local build_ref_259
	ret_reg &ref[259]
	call_c   build_ref_874()
	call_c   build_ref_674()
	call_c   Dyam_Create_Binary(&ref[874],&ref[674],V(7))
	move_ret ref[259]
	c_ret

;; TERM 874: module_import
c_code local build_ref_874
	ret_reg &ref[874]
	call_c   Dyam_Create_Atom("module_import")
	move_ret ref[874]
	c_ret

pl_code local fun69
	call_c   Dyam_Remove_Choice()
	pl_jump  fun68()

long local pool_fun73[5]=[131074,build_ref_258,build_ref_259,pool_fun68,pool_fun68]

pl_code local fun73
	call_c   Dyam_Pool(pool_fun73)
	call_c   Dyam_Unify_Item(&ref[258])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun72)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[259])
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun69)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(7),I(0))
	fail_ret
	call_c   Dyam_Cut()
	pl_fail

;; TERM 257: '*FIRST*'('call_term_module_shift/5'(_B, (dcg _C / _D))) :> []
c_code local build_ref_257
	ret_reg &ref[257]
	call_c   build_ref_256()
	call_c   Dyam_Create_Binary(I(9),&ref[256],I(0))
	move_ret ref[257]
	c_ret

;; TERM 256: '*FIRST*'('call_term_module_shift/5'(_B, (dcg _C / _D)))
c_code local build_ref_256
	ret_reg &ref[256]
	call_c   build_ref_36()
	call_c   build_ref_255()
	call_c   Dyam_Create_Unary(&ref[36],&ref[255])
	move_ret ref[256]
	c_ret

c_code local build_seed_38
	ret_reg &seed[38]
	call_c   build_ref_50()
	call_c   build_ref_267()
	call_c   Dyam_Seed_Start(&ref[50],&ref[267],I(0),fun5,1)
	call_c   build_ref_266()
	call_c   Dyam_Seed_Add_Comp(&ref[266],fun74,0)
	call_c   Dyam_Seed_End()
	move_ret seed[38]
	c_ret

;; TERM 266: '*GUARD*'(dcg_body_to_lpda_handler(_B, {_C}, _D, _E, _F, _G, _H, _I, _J))
c_code local build_ref_266
	ret_reg &ref[266]
	call_c   build_ref_51()
	call_c   build_ref_265()
	call_c   Dyam_Create_Unary(&ref[51],&ref[265])
	move_ret ref[266]
	c_ret

;; TERM 265: dcg_body_to_lpda_handler(_B, {_C}, _D, _E, _F, _G, _H, _I, _J)
c_code local build_ref_265
	ret_reg &ref[265]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Unary(I(2),V(2))
	move_ret R(0)
	call_c   build_ref_828()
	call_c   Dyam_Term_Start(&ref[828],9)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret ref[265]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_87
	ret_reg &seed[87]
	call_c   build_ref_51()
	call_c   build_ref_269()
	call_c   Dyam_Seed_Start(&ref[51],&ref[269],I(0),fun0,1)
	call_c   build_ref_270()
	call_c   Dyam_Seed_Add_Comp(&ref[270],&ref[269],0)
	call_c   Dyam_Seed_End()
	move_ret seed[87]
	c_ret

;; TERM 270: '*GUARD*'(body_to_lpda(_B, _C, _H, _K, _J)) :> '$$HOLE$$'
c_code local build_ref_270
	ret_reg &ref[270]
	call_c   build_ref_269()
	call_c   Dyam_Create_Binary(I(9),&ref[269],I(7))
	move_ret ref[270]
	c_ret

;; TERM 269: '*GUARD*'(body_to_lpda(_B, _C, _H, _K, _J))
c_code local build_ref_269
	ret_reg &ref[269]
	call_c   build_ref_51()
	call_c   build_ref_268()
	call_c   Dyam_Create_Unary(&ref[51],&ref[268])
	move_ret ref[269]
	c_ret

;; TERM 268: body_to_lpda(_B, _C, _H, _K, _J)
c_code local build_ref_268
	ret_reg &ref[268]
	call_c   build_ref_875()
	call_c   Dyam_Term_Start(&ref[875],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret ref[268]
	c_ret

;; TERM 875: body_to_lpda
c_code local build_ref_875
	ret_reg &ref[875]
	call_c   Dyam_Create_Atom("body_to_lpda")
	move_ret ref[875]
	c_ret

long local pool_fun74[3]=[2,build_ref_266,build_seed_87]

pl_code local fun74
	call_c   Dyam_Pool(pool_fun74)
	call_c   Dyam_Unify_Item(&ref[266])
	fail_ret
	call_c   Dyam_Allocate(0)
	pl_call  fun7(&seed[87],1)
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(3))
	call_c   Dyam_Reg_Load(4,V(4))
	call_c   Dyam_Reg_Load(6,V(5))
	call_c   Dyam_Reg_Load(8,V(6))
	call_c   Dyam_Reg_Load(10,V(10))
	call_c   Dyam_Reg_Load(12,V(8))
	call_c   Dyam_Reg_Deallocate(7)
	pl_jump  pred_bmg_null_step_7()

;; TERM 267: '*GUARD*'(dcg_body_to_lpda_handler(_B, {_C}, _D, _E, _F, _G, _H, _I, _J)) :> []
c_code local build_ref_267
	ret_reg &ref[267]
	call_c   build_ref_266()
	call_c   Dyam_Create_Binary(I(9),&ref[266],I(0))
	move_ret ref[267]
	c_ret

c_code local build_seed_5
	ret_reg &seed[5]
	call_c   build_ref_50()
	call_c   build_ref_319()
	call_c   Dyam_Seed_Start(&ref[50],&ref[319],I(0),fun5,1)
	call_c   build_ref_318()
	call_c   Dyam_Seed_Add_Comp(&ref[318],fun114,0)
	call_c   Dyam_Seed_End()
	move_ret seed[5]
	c_ret

;; TERM 318: '*GUARD*'(token_trail_gen([_B,_C|_D], _E, _F, (gensym(_G) , record(_H) , _I), _J))
c_code local build_ref_318
	ret_reg &ref[318]
	call_c   build_ref_51()
	call_c   build_ref_317()
	call_c   Dyam_Create_Unary(&ref[51],&ref[317])
	move_ret ref[318]
	c_ret

;; TERM 317: token_trail_gen([_B,_C|_D], _E, _F, (gensym(_G) , record(_H) , _I), _J)
c_code local build_ref_317
	ret_reg &ref[317]
	call_c   Dyam_Pseudo_Choice(3)
	call_c   Dyam_Create_Tupple(1,2,V(3))
	move_ret R(0)
	call_c   build_ref_876()
	call_c   Dyam_Create_Unary(&ref[876],V(6))
	move_ret R(1)
	call_c   build_ref_831()
	call_c   Dyam_Create_Unary(&ref[831],V(7))
	move_ret R(2)
	call_c   Dyam_Create_Binary(I(4),R(2),V(8))
	move_ret R(2)
	call_c   Dyam_Create_Binary(I(4),R(1),R(2))
	move_ret R(2)
	call_c   build_ref_830()
	call_c   Dyam_Term_Start(&ref[830],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(R(2))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret ref[317]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 876: gensym
c_code local build_ref_876
	ret_reg &ref[876]
	call_c   Dyam_Create_Atom("gensym")
	move_ret ref[876]
	c_ret

c_code local build_seed_93
	ret_reg &seed[93]
	call_c   build_ref_51()
	call_c   build_ref_321()
	call_c   Dyam_Seed_Start(&ref[51],&ref[321],I(0),fun0,1)
	call_c   build_ref_322()
	call_c   Dyam_Seed_Add_Comp(&ref[322],&ref[321],0)
	call_c   Dyam_Seed_End()
	move_ret seed[93]
	c_ret

;; TERM 322: '*GUARD*'(generalized_scanner(_J, _E, _B, added(_G), _H, skip)) :> '$$HOLE$$'
c_code local build_ref_322
	ret_reg &ref[322]
	call_c   build_ref_321()
	call_c   Dyam_Create_Binary(I(9),&ref[321],I(7))
	move_ret ref[322]
	c_ret

;; TERM 321: '*GUARD*'(generalized_scanner(_J, _E, _B, added(_G), _H, skip))
c_code local build_ref_321
	ret_reg &ref[321]
	call_c   build_ref_51()
	call_c   build_ref_320()
	call_c   Dyam_Create_Unary(&ref[51],&ref[320])
	move_ret ref[321]
	c_ret

;; TERM 320: generalized_scanner(_J, _E, _B, added(_G), _H, skip)
c_code local build_ref_320
	ret_reg &ref[320]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_877()
	call_c   Dyam_Create_Unary(&ref[877],V(6))
	move_ret R(0)
	call_c   build_ref_832()
	call_c   build_ref_389()
	call_c   Dyam_Term_Start(&ref[832],6)
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(&ref[389])
	call_c   Dyam_Term_End()
	move_ret ref[320]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 877: added
c_code local build_ref_877
	ret_reg &ref[877]
	call_c   Dyam_Create_Atom("added")
	move_ret ref[877]
	c_ret

c_code local build_seed_94
	ret_reg &seed[94]
	call_c   build_ref_51()
	call_c   build_ref_324()
	call_c   Dyam_Seed_Start(&ref[51],&ref[324],I(0),fun0,1)
	call_c   build_ref_325()
	call_c   Dyam_Seed_Add_Comp(&ref[325],&ref[324],0)
	call_c   Dyam_Seed_End()
	move_ret seed[94]
	c_ret

;; TERM 325: '*GUARD*'(token_trail_gen([_C|_D], added(_G), _F, _I, _J)) :> '$$HOLE$$'
c_code local build_ref_325
	ret_reg &ref[325]
	call_c   build_ref_324()
	call_c   Dyam_Create_Binary(I(9),&ref[324],I(7))
	move_ret ref[325]
	c_ret

;; TERM 324: '*GUARD*'(token_trail_gen([_C|_D], added(_G), _F, _I, _J))
c_code local build_ref_324
	ret_reg &ref[324]
	call_c   build_ref_51()
	call_c   build_ref_323()
	call_c   Dyam_Create_Unary(&ref[51],&ref[323])
	move_ret ref[324]
	c_ret

;; TERM 323: token_trail_gen([_C|_D], added(_G), _F, _I, _J)
c_code local build_ref_323
	ret_reg &ref[323]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   Dyam_Create_List(V(2),V(3))
	move_ret R(0)
	call_c   build_ref_877()
	call_c   Dyam_Create_Unary(&ref[877],V(6))
	move_ret R(1)
	call_c   build_ref_830()
	call_c   Dyam_Term_Start(&ref[830],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret ref[323]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun114[4]=[3,build_ref_318,build_seed_93,build_seed_94]

pl_code local fun114
	call_c   Dyam_Pool(pool_fun114)
	call_c   Dyam_Unify_Item(&ref[318])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(6))
	call_c   Dyam_Reg_Load(2,V(9))
	move     V(10), R(4)
	move     S(5), R(5)
	pl_call  pred_my_numbervars_3()
	pl_call  fun7(&seed[93],1)
	call_c   Dyam_Deallocate()
	pl_jump  fun7(&seed[94],1)

;; TERM 319: '*GUARD*'(token_trail_gen([_B,_C|_D], _E, _F, (gensym(_G) , record(_H) , _I), _J)) :> []
c_code local build_ref_319
	ret_reg &ref[319]
	call_c   build_ref_318()
	call_c   Dyam_Create_Binary(I(9),&ref[318],I(0))
	move_ret ref[319]
	c_ret

c_code local build_seed_14
	ret_reg &seed[14]
	call_c   build_ref_50()
	call_c   build_ref_328()
	call_c   Dyam_Seed_Start(&ref[50],&ref[328],I(0),fun5,1)
	call_c   build_ref_327()
	call_c   Dyam_Seed_Add_Comp(&ref[327],fun115,0)
	call_c   Dyam_Seed_End()
	move_ret seed[14]
	c_ret

;; TERM 327: '*GUARD*'(token_scan_to_lpda(_B, (_C ; _D), _E, _F, _G, _H, _I, _J))
c_code local build_ref_327
	ret_reg &ref[327]
	call_c   build_ref_51()
	call_c   build_ref_326()
	call_c   Dyam_Create_Unary(&ref[51],&ref[326])
	move_ret ref[327]
	c_ret

;; TERM 326: token_scan_to_lpda(_B, (_C ; _D), _E, _F, _G, _H, _I, _J)
c_code local build_ref_326
	ret_reg &ref[326]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Binary(I(5),V(2),V(3))
	move_ret R(0)
	call_c   build_ref_878()
	call_c   Dyam_Term_Start(&ref[878],8)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret ref[326]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 878: token_scan_to_lpda
c_code local build_ref_878
	ret_reg &ref[878]
	call_c   Dyam_Create_Atom("token_scan_to_lpda")
	move_ret ref[878]
	c_ret

c_code local build_seed_95
	ret_reg &seed[95]
	call_c   build_ref_51()
	call_c   build_ref_330()
	call_c   Dyam_Seed_Start(&ref[51],&ref[330],I(0),fun0,1)
	call_c   build_ref_331()
	call_c   Dyam_Seed_Add_Comp(&ref[331],&ref[330],0)
	call_c   Dyam_Seed_End()
	move_ret seed[95]
	c_ret

;; TERM 331: '*GUARD*'(token_scan_to_lpda(_B, _C, _E, _F, _K, _L, _I, _J)) :> '$$HOLE$$'
c_code local build_ref_331
	ret_reg &ref[331]
	call_c   build_ref_330()
	call_c   Dyam_Create_Binary(I(9),&ref[330],I(7))
	move_ret ref[331]
	c_ret

;; TERM 330: '*GUARD*'(token_scan_to_lpda(_B, _C, _E, _F, _K, _L, _I, _J))
c_code local build_ref_330
	ret_reg &ref[330]
	call_c   build_ref_51()
	call_c   build_ref_329()
	call_c   Dyam_Create_Unary(&ref[51],&ref[329])
	move_ret ref[330]
	c_ret

;; TERM 329: token_scan_to_lpda(_B, _C, _E, _F, _K, _L, _I, _J)
c_code local build_ref_329
	ret_reg &ref[329]
	call_c   build_ref_878()
	call_c   Dyam_Term_Start(&ref[878],8)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret ref[329]
	c_ret

c_code local build_seed_96
	ret_reg &seed[96]
	call_c   build_ref_51()
	call_c   build_ref_333()
	call_c   Dyam_Seed_Start(&ref[51],&ref[333],I(0),fun0,1)
	call_c   build_ref_334()
	call_c   Dyam_Seed_Add_Comp(&ref[334],&ref[333],0)
	call_c   Dyam_Seed_End()
	move_ret seed[96]
	c_ret

;; TERM 334: '*GUARD*'(token_scan_to_lpda(_B, _D, _E, _F, _K, _M, _I, _J)) :> '$$HOLE$$'
c_code local build_ref_334
	ret_reg &ref[334]
	call_c   build_ref_333()
	call_c   Dyam_Create_Binary(I(9),&ref[333],I(7))
	move_ret ref[334]
	c_ret

;; TERM 333: '*GUARD*'(token_scan_to_lpda(_B, _D, _E, _F, _K, _M, _I, _J))
c_code local build_ref_333
	ret_reg &ref[333]
	call_c   build_ref_51()
	call_c   build_ref_332()
	call_c   Dyam_Create_Unary(&ref[51],&ref[332])
	move_ret ref[333]
	c_ret

;; TERM 332: token_scan_to_lpda(_B, _D, _E, _F, _K, _M, _I, _J)
c_code local build_ref_332
	ret_reg &ref[332]
	call_c   build_ref_878()
	call_c   Dyam_Term_Start(&ref[878],8)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret ref[332]
	c_ret

long local pool_fun115[4]=[3,build_ref_327,build_seed_95,build_seed_96]

pl_code local fun115
	call_c   Dyam_Pool(pool_fun115)
	call_c   Dyam_Unify_Item(&ref[327])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(6))
	call_c   Dyam_Reg_Load(2,V(7))
	move     V(10), R(4)
	move     S(5), R(5)
	move     V(11), R(6)
	move     S(5), R(7)
	move     V(12), R(8)
	move     S(5), R(9)
	pl_call  pred_handle_choice_5()
	pl_call  fun7(&seed[95],1)
	call_c   Dyam_Deallocate()
	pl_jump  fun7(&seed[96],1)

;; TERM 328: '*GUARD*'(token_scan_to_lpda(_B, (_C ; _D), _E, _F, _G, _H, _I, _J)) :> []
c_code local build_ref_328
	ret_reg &ref[328]
	call_c   build_ref_327()
	call_c   Dyam_Create_Binary(I(9),&ref[327],I(0))
	move_ret ref[328]
	c_ret

c_code local build_seed_2
	ret_reg &seed[2]
	call_c   build_ref_50()
	call_c   build_ref_346()
	call_c   Dyam_Seed_Start(&ref[50],&ref[346],I(0),fun5,1)
	call_c   build_ref_345()
	call_c   Dyam_Seed_Add_Comp(&ref[345],fun131,0)
	call_c   Dyam_Seed_End()
	move_ret seed[2]
	c_ret

;; TERM 345: '*GUARD*'(try_install_loader(_B, (_C ^ _D), _E, _F))
c_code local build_ref_345
	ret_reg &ref[345]
	call_c   build_ref_51()
	call_c   build_ref_344()
	call_c   Dyam_Create_Unary(&ref[51],&ref[344])
	move_ret ref[345]
	c_ret

;; TERM 344: try_install_loader(_B, (_C ^ _D), _E, _F)
c_code local build_ref_344
	ret_reg &ref[344]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_860()
	call_c   Dyam_Create_Binary(&ref[860],V(2),V(3))
	move_ret R(0)
	call_c   build_ref_879()
	call_c   Dyam_Term_Start(&ref[879],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[344]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 879: try_install_loader
c_code local build_ref_879
	ret_reg &ref[879]
	call_c   Dyam_Create_Atom("try_install_loader")
	move_ret ref[879]
	c_ret

c_code local build_seed_97
	ret_reg &seed[97]
	call_c   build_ref_51()
	call_c   build_ref_351()
	call_c   Dyam_Seed_Start(&ref[51],&ref[351],I(0),fun0,1)
	call_c   build_ref_352()
	call_c   Dyam_Seed_Add_Comp(&ref[352],&ref[351],0)
	call_c   Dyam_Seed_End()
	move_ret seed[97]
	c_ret

;; TERM 352: '*GUARD*'(build_cond_loader(_B, (_H -> true ; fail), _E, _F)) :> '$$HOLE$$'
c_code local build_ref_352
	ret_reg &ref[352]
	call_c   build_ref_351()
	call_c   Dyam_Create_Binary(I(9),&ref[351],I(7))
	move_ret ref[352]
	c_ret

;; TERM 351: '*GUARD*'(build_cond_loader(_B, (_H -> true ; fail), _E, _F))
c_code local build_ref_351
	ret_reg &ref[351]
	call_c   build_ref_51()
	call_c   build_ref_350()
	call_c   Dyam_Create_Unary(&ref[51],&ref[350])
	move_ret ref[351]
	c_ret

;; TERM 350: build_cond_loader(_B, (_H -> true ; fail), _E, _F)
c_code local build_ref_350
	ret_reg &ref[350]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_847()
	call_c   build_ref_186()
	call_c   Dyam_Create_Binary(&ref[847],V(7),&ref[186])
	move_ret R(0)
	call_c   build_ref_829()
	call_c   Dyam_Create_Binary(I(5),R(0),&ref[829])
	move_ret R(0)
	call_c   build_ref_880()
	call_c   Dyam_Term_Start(&ref[880],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[350]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 880: build_cond_loader
c_code local build_ref_880
	ret_reg &ref[880]
	call_c   Dyam_Create_Atom("build_cond_loader")
	move_ret ref[880]
	c_ret

long local pool_fun128[2]=[1,build_seed_97]

pl_code local fun128
	call_c   Dyam_Remove_Choice()
	pl_call  fun7(&seed[97],1)
fun123:
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Deallocate(1)
	pl_jump  pred_numbervars_delete_1()


pl_code local fun125
	call_c   Dyam_Remove_Choice()
fun124:
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(5),V(4))
	fail_ret
	pl_jump  fun123()


;; TERM 349: autoload
c_code local build_ref_349
	ret_reg &ref[349]
	call_c   Dyam_Create_Atom("autoload")
	move_ret ref[349]
	c_ret

long local pool_fun126[2]=[1,build_ref_349]

pl_code local fun126
	call_c   Dyam_Update_Choice(fun125)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[349])
	call_c   Dyam_Cut()
	pl_fail

;; TERM 348: parse_mode(list)
c_code local build_ref_348
	ret_reg &ref[348]
	call_c   build_ref_881()
	call_c   build_ref_560()
	call_c   Dyam_Create_Unary(&ref[881],&ref[560])
	move_ret ref[348]
	c_ret

;; TERM 560: list
c_code local build_ref_560
	ret_reg &ref[560]
	call_c   Dyam_Create_Atom("list")
	move_ret ref[560]
	c_ret

;; TERM 881: parse_mode
c_code local build_ref_881
	ret_reg &ref[881]
	call_c   Dyam_Create_Atom("parse_mode")
	move_ret ref[881]
	c_ret

long local pool_fun127[3]=[65537,build_ref_348,pool_fun126]

pl_code local fun127
	call_c   Dyam_Update_Choice(fun126)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[348])
	call_c   Dyam_Cut()
	pl_jump  fun124()

long local pool_fun129[4]=[131073,build_ref_186,pool_fun128,pool_fun127]

pl_code local fun130
	call_c   Dyam_Remove_Choice()
fun129:
	call_c   Dyam_Reg_Load_Ptr(2,V(6))
	fail_ret
	call_c   Dyam_Reg_Load(4,V(7))
	call_c   DyALog_Mutable_Read(R(2),&R(4))
	fail_ret
	call_c   Dyam_Choice(fun128)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Choice(fun127)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(7),&ref[186])
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun124()


;; TERM 347: _H , _C
c_code local build_ref_347
	ret_reg &ref[347]
	call_c   Dyam_Create_Binary(I(4),V(7),V(2))
	move_ret ref[347]
	c_ret

long local pool_fun131[5]=[65539,build_ref_345,build_ref_186,build_ref_347,pool_fun129]

pl_code local fun131
	call_c   Dyam_Pool(pool_fun131)
	call_c   Dyam_Unify_Item(&ref[345])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load_Ptr(2,V(6))
	fail_ret
	move     &ref[186], R(4)
	move     0, R(5)
	call_c   DyALog_Mutable_Write(R(2),&R(4),0)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(6))
	fail_ret
	call_c   Dyam_Choice(fun130)
	pl_call  Object_2(V(3),V(8))
	call_c   DYAM_evpred_retract(V(3))
	fail_ret
	call_c   Dyam_Reg_Load_Ptr(2,V(6))
	fail_ret
	move     V(7), R(4)
	move     S(5), R(5)
	call_c   DyALog_Mutable_Read(R(2),&R(4))
	fail_ret
	call_c   Dyam_Reg_Load_Ptr(2,V(6))
	fail_ret
	call_c   Dyam_Reg_Load(4,&ref[347])
	call_c   DyALog_Mutable_Write(R(2),&R(4),0)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(6))
	fail_ret
	pl_fail

;; TERM 346: '*GUARD*'(try_install_loader(_B, (_C ^ _D), _E, _F)) :> []
c_code local build_ref_346
	ret_reg &ref[346]
	call_c   build_ref_345()
	call_c   Dyam_Create_Binary(I(9),&ref[345],I(0))
	move_ret ref[346]
	c_ret

c_code local build_seed_15
	ret_reg &seed[15]
	call_c   build_ref_50()
	call_c   build_ref_355()
	call_c   Dyam_Seed_Start(&ref[50],&ref[355],I(0),fun5,1)
	call_c   build_ref_354()
	call_c   Dyam_Seed_Add_Comp(&ref[354],fun134,0)
	call_c   Dyam_Seed_End()
	move_ret seed[15]
	c_ret

;; TERM 354: '*GUARD*'(token_scan_to_lpda(_B, [_C], _D, _E, _F, _G, _H, _I))
c_code local build_ref_354
	ret_reg &ref[354]
	call_c   build_ref_51()
	call_c   build_ref_353()
	call_c   Dyam_Create_Unary(&ref[51],&ref[353])
	move_ret ref[354]
	c_ret

;; TERM 353: token_scan_to_lpda(_B, [_C], _D, _E, _F, _G, _H, _I)
c_code local build_ref_353
	ret_reg &ref[353]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(2),I(0))
	move_ret R(0)
	call_c   build_ref_878()
	call_c   Dyam_Term_Start(&ref[878],8)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_End()
	move_ret ref[353]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_99
	ret_reg &seed[99]
	call_c   build_ref_51()
	call_c   build_ref_361()
	call_c   Dyam_Seed_Start(&ref[51],&ref[361],I(0),fun0,1)
	call_c   build_ref_362()
	call_c   Dyam_Seed_Add_Comp(&ref[362],&ref[361],0)
	call_c   Dyam_Seed_End()
	move_ret seed[99]
	c_ret

;; TERM 362: '*GUARD*'(generalized_scanner(_B, _D, _C, _E, _L, _I)) :> '$$HOLE$$'
c_code local build_ref_362
	ret_reg &ref[362]
	call_c   build_ref_361()
	call_c   Dyam_Create_Binary(I(9),&ref[361],I(7))
	move_ret ref[362]
	c_ret

;; TERM 361: '*GUARD*'(generalized_scanner(_B, _D, _C, _E, _L, _I))
c_code local build_ref_361
	ret_reg &ref[361]
	call_c   build_ref_51()
	call_c   build_ref_360()
	call_c   Dyam_Create_Unary(&ref[51],&ref[360])
	move_ret ref[361]
	c_ret

;; TERM 360: generalized_scanner(_B, _D, _C, _E, _L, _I)
c_code local build_ref_360
	ret_reg &ref[360]
	call_c   build_ref_832()
	call_c   Dyam_Term_Start(&ref[832],6)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_End()
	move_ret ref[360]
	c_ret

c_code local build_seed_100
	ret_reg &seed[100]
	call_c   build_ref_51()
	call_c   build_ref_364()
	call_c   Dyam_Seed_Start(&ref[51],&ref[364],I(0),fun0,1)
	call_c   build_ref_365()
	call_c   Dyam_Seed_Add_Comp(&ref[365],&ref[364],0)
	call_c   Dyam_Seed_End()
	move_ret seed[100]
	c_ret

;; TERM 365: '*GUARD*'(body_to_lpda(_B, _L, _F, _G, _H)) :> '$$HOLE$$'
c_code local build_ref_365
	ret_reg &ref[365]
	call_c   build_ref_364()
	call_c   Dyam_Create_Binary(I(9),&ref[364],I(7))
	move_ret ref[365]
	c_ret

;; TERM 364: '*GUARD*'(body_to_lpda(_B, _L, _F, _G, _H))
c_code local build_ref_364
	ret_reg &ref[364]
	call_c   build_ref_51()
	call_c   build_ref_363()
	call_c   Dyam_Create_Unary(&ref[51],&ref[363])
	move_ret ref[364]
	c_ret

;; TERM 363: body_to_lpda(_B, _L, _F, _G, _H)
c_code local build_ref_363
	ret_reg &ref[363]
	call_c   build_ref_875()
	call_c   Dyam_Term_Start(&ref[875],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[363]
	c_ret

long local pool_fun133[3]=[2,build_seed_99,build_seed_100]

pl_code local fun133
	call_c   Dyam_Remove_Choice()
	pl_call  fun7(&seed[99],1)
	pl_call  fun7(&seed[100],1)
fun132:
	call_c   Dyam_Deallocate()
	pl_ret


;; TERM 356: _J ; _K
c_code local build_ref_356
	ret_reg &ref[356]
	call_c   Dyam_Create_Binary(I(5),V(9),V(10))
	move_ret ref[356]
	c_ret

c_code local build_seed_98
	ret_reg &seed[98]
	call_c   build_ref_51()
	call_c   build_ref_358()
	call_c   Dyam_Seed_Start(&ref[51],&ref[358],I(0),fun0,1)
	call_c   build_ref_359()
	call_c   Dyam_Seed_Add_Comp(&ref[359],&ref[358],0)
	call_c   Dyam_Seed_End()
	move_ret seed[98]
	c_ret

;; TERM 359: '*GUARD*'(token_scan_to_lpda(_B, _C, _D, _E, _F, _G, _H, _I)) :> '$$HOLE$$'
c_code local build_ref_359
	ret_reg &ref[359]
	call_c   build_ref_358()
	call_c   Dyam_Create_Binary(I(9),&ref[358],I(7))
	move_ret ref[359]
	c_ret

;; TERM 358: '*GUARD*'(token_scan_to_lpda(_B, _C, _D, _E, _F, _G, _H, _I))
c_code local build_ref_358
	ret_reg &ref[358]
	call_c   build_ref_51()
	call_c   build_ref_357()
	call_c   Dyam_Create_Unary(&ref[51],&ref[357])
	move_ret ref[358]
	c_ret

;; TERM 357: token_scan_to_lpda(_B, _C, _D, _E, _F, _G, _H, _I)
c_code local build_ref_357
	ret_reg &ref[357]
	call_c   build_ref_878()
	call_c   Dyam_Term_Start(&ref[878],8)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_End()
	move_ret ref[357]
	c_ret

long local pool_fun134[5]=[65539,build_ref_354,build_ref_356,build_seed_98,pool_fun133]

pl_code local fun134
	call_c   Dyam_Pool(pool_fun134)
	call_c   Dyam_Unify_Item(&ref[354])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun133)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(2),&ref[356])
	fail_ret
	call_c   Dyam_Cut()
	pl_call  fun7(&seed[98],1)
	pl_jump  fun132()

;; TERM 355: '*GUARD*'(token_scan_to_lpda(_B, [_C], _D, _E, _F, _G, _H, _I)) :> []
c_code local build_ref_355
	ret_reg &ref[355]
	call_c   build_ref_354()
	call_c   Dyam_Create_Binary(I(9),&ref[354],I(0))
	move_ret ref[355]
	c_ret

c_code local build_seed_49
	ret_reg &seed[49]
	call_c   build_ref_50()
	call_c   build_ref_368()
	call_c   Dyam_Seed_Start(&ref[50],&ref[368],I(0),fun5,1)
	call_c   build_ref_367()
	call_c   Dyam_Seed_Add_Comp(&ref[367],fun140,0)
	call_c   Dyam_Seed_End()
	move_ret seed[49]
	c_ret

;; TERM 367: '*GUARD*'(dcg_body_to_lpda_handler(_B, '$call'(_C), _D, _E, _F, _G, _H, _I, _J))
c_code local build_ref_367
	ret_reg &ref[367]
	call_c   build_ref_51()
	call_c   build_ref_366()
	call_c   Dyam_Create_Unary(&ref[51],&ref[366])
	move_ret ref[367]
	c_ret

;; TERM 366: dcg_body_to_lpda_handler(_B, '$call'(_C), _D, _E, _F, _G, _H, _I, _J)
c_code local build_ref_366
	ret_reg &ref[366]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_846()
	call_c   Dyam_Create_Unary(&ref[846],V(2))
	move_ret R(0)
	call_c   build_ref_828()
	call_c   Dyam_Term_Start(&ref[828],9)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret ref[366]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 383: '$CLOSURE'('$fun'(139, 0, 1145401088), '$TUPPLE'(35074870873368))
c_code local build_ref_383
	ret_reg &ref[383]
	call_c   build_ref_382()
	call_c   Dyam_Closure_Aux(fun139,&ref[382])
	move_ret ref[383]
	c_ret

c_code local build_seed_104
	ret_reg &seed[104]
	call_c   build_ref_51()
	call_c   build_ref_379()
	call_c   Dyam_Seed_Start(&ref[51],&ref[379],I(0),fun0,1)
	call_c   build_ref_380()
	call_c   Dyam_Seed_Add_Comp(&ref[380],&ref[379],0)
	call_c   Dyam_Seed_End()
	move_ret seed[104]
	c_ret

;; TERM 380: '*GUARD*'(body_to_lpda(_B, call!bmgcall(_C, _D, _E, _F, _G), _H, _I, _J)) :> '$$HOLE$$'
c_code local build_ref_380
	ret_reg &ref[380]
	call_c   build_ref_379()
	call_c   Dyam_Create_Binary(I(9),&ref[379],I(7))
	move_ret ref[380]
	c_ret

;; TERM 379: '*GUARD*'(body_to_lpda(_B, call!bmgcall(_C, _D, _E, _F, _G), _H, _I, _J))
c_code local build_ref_379
	ret_reg &ref[379]
	call_c   build_ref_51()
	call_c   build_ref_378()
	call_c   Dyam_Create_Unary(&ref[51],&ref[378])
	move_ret ref[379]
	c_ret

;; TERM 378: body_to_lpda(_B, call!bmgcall(_C, _D, _E, _F, _G), _H, _I, _J)
c_code local build_ref_378
	ret_reg &ref[378]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_882()
	call_c   Dyam_Term_Start(&ref[882],5)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_875()
	call_c   Dyam_Term_Start(&ref[875],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret ref[378]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 882: call!bmgcall
c_code local build_ref_882
	ret_reg &ref[882]
	call_c   build_ref_883()
	call_c   Dyam_Create_Atom_Module("bmgcall",&ref[883])
	move_ret ref[882]
	c_ret

;; TERM 883: call
c_code local build_ref_883
	ret_reg &ref[883]
	call_c   Dyam_Create_Atom("call")
	move_ret ref[883]
	c_ret

long local pool_fun138[2]=[1,build_seed_104]

pl_code local fun138
	call_c   Dyam_Remove_Choice()
	pl_call  fun7(&seed[104],1)
	pl_jump  fun132()

;; TERM 374: answer(_K)
c_code local build_ref_374
	ret_reg &ref[374]
	call_c   build_ref_863()
	call_c   Dyam_Create_Unary(&ref[863],V(10))
	move_ret ref[374]
	c_ret

c_code local build_seed_103
	ret_reg &seed[103]
	call_c   build_ref_51()
	call_c   build_ref_376()
	call_c   Dyam_Seed_Start(&ref[51],&ref[376],I(0),fun0,1)
	call_c   build_ref_377()
	call_c   Dyam_Seed_Add_Comp(&ref[377],&ref[376],0)
	call_c   Dyam_Seed_End()
	move_ret seed[103]
	c_ret

;; TERM 377: '*GUARD*'(body_to_lpda(_B, call!bmgcall('$answers'(_C), _D, _E, _F, _G), _H, _I, _K)) :> '$$HOLE$$'
c_code local build_ref_377
	ret_reg &ref[377]
	call_c   build_ref_376()
	call_c   Dyam_Create_Binary(I(9),&ref[376],I(7))
	move_ret ref[377]
	c_ret

;; TERM 376: '*GUARD*'(body_to_lpda(_B, call!bmgcall('$answers'(_C), _D, _E, _F, _G), _H, _I, _K))
c_code local build_ref_376
	ret_reg &ref[376]
	call_c   build_ref_51()
	call_c   build_ref_375()
	call_c   Dyam_Create_Unary(&ref[51],&ref[375])
	move_ret ref[376]
	c_ret

;; TERM 375: body_to_lpda(_B, call!bmgcall('$answers'(_C), _D, _E, _F, _G), _H, _I, _K)
c_code local build_ref_375
	ret_reg &ref[375]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_843()
	call_c   Dyam_Create_Unary(&ref[843],V(2))
	move_ret R(0)
	call_c   build_ref_882()
	call_c   Dyam_Term_Start(&ref[882],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_875()
	call_c   Dyam_Term_Start(&ref[875],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_End()
	move_ret ref[375]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun139[4]=[65538,build_ref_374,build_seed_103,pool_fun138]

pl_code local fun139
	call_c   Dyam_Pool(pool_fun139)
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun138)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(9),&ref[374])
	fail_ret
	call_c   Dyam_Cut()
	pl_call  fun7(&seed[103],1)
	pl_jump  fun132()

;; TERM 382: '$TUPPLE'(35074870873368)
c_code local build_ref_382
	ret_reg &ref[382]
	call_c   Dyam_Create_Simple_Tupple(0,267911168)
	move_ret ref[382]
	c_ret

long local pool_fun140[3]=[2,build_ref_367,build_ref_383]

pl_code local fun140
	call_c   Dyam_Pool(pool_fun140)
	call_c   Dyam_Unify_Item(&ref[367])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[383], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Deallocate(2)
fun137:
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(2)
	call_c   Dyam_Choice(fun136)
	pl_call  fun13(&seed[101],1)
	pl_fail


;; TERM 368: '*GUARD*'(dcg_body_to_lpda_handler(_B, '$call'(_C), _D, _E, _F, _G, _H, _I, _J)) :> []
c_code local build_ref_368
	ret_reg &ref[368]
	call_c   build_ref_367()
	call_c   Dyam_Create_Binary(I(9),&ref[367],I(0))
	move_ret ref[368]
	c_ret

c_code local build_seed_16
	ret_reg &seed[16]
	call_c   build_ref_50()
	call_c   build_ref_386()
	call_c   Dyam_Seed_Start(&ref[50],&ref[386],I(0),fun5,1)
	call_c   build_ref_385()
	call_c   Dyam_Seed_Add_Comp(&ref[385],fun142,0)
	call_c   Dyam_Seed_End()
	move_ret seed[16]
	c_ret

;; TERM 385: '*GUARD*'(generalized_scanner(_B, _C, _D, _E, _F, _G))
c_code local build_ref_385
	ret_reg &ref[385]
	call_c   build_ref_51()
	call_c   build_ref_384()
	call_c   Dyam_Create_Unary(&ref[51],&ref[384])
	move_ret ref[385]
	c_ret

;; TERM 384: generalized_scanner(_B, _C, _D, _E, _F, _G)
c_code local build_ref_384
	ret_reg &ref[384]
	call_c   build_ref_832()
	call_c   Dyam_Term_Start(&ref[832],6)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[384]
	c_ret

;; TERM 387: scanner(_H, _D, _E, _I)
c_code local build_ref_387
	ret_reg &ref[387]
	call_c   build_ref_884()
	call_c   Dyam_Term_Start(&ref[884],4)
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_End()
	move_ret ref[387]
	c_ret

;; TERM 884: scanner
c_code local build_ref_884
	ret_reg &ref[884]
	call_c   Dyam_Create_Atom("scanner")
	move_ret ref[884]
	c_ret

pl_code local fun141
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(2),V(7))
	fail_ret
	call_c   Dyam_Unify(V(5),V(8))
	fail_ret
	pl_jump  fun132()

;; TERM 388: skipper(_C, _J, _H, _K)
c_code local build_ref_388
	ret_reg &ref[388]
	call_c   build_ref_885()
	call_c   Dyam_Term_Start(&ref[885],4)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_End()
	move_ret ref[388]
	c_ret

;; TERM 885: skipper
c_code local build_ref_885
	ret_reg &ref[885]
	call_c   Dyam_Create_Atom("skipper")
	move_ret ref[885]
	c_ret

;; TERM 390: [_J,_H]
c_code local build_ref_390
	ret_reg &ref[390]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(7),I(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(9),R(0))
	move_ret ref[390]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 391: (_K ; _H = _C) , _I
c_code local build_ref_391
	ret_reg &ref[391]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_869()
	call_c   Dyam_Create_Binary(&ref[869],V(7),V(2))
	move_ret R(0)
	call_c   Dyam_Create_Binary(I(5),V(10),R(0))
	move_ret R(0)
	call_c   Dyam_Create_Binary(I(4),R(0),V(8))
	move_ret ref[391]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun142[7]=[6,build_ref_385,build_ref_387,build_ref_388,build_ref_389,build_ref_390,build_ref_391]

pl_code local fun142
	call_c   Dyam_Pool(pool_fun142)
	call_c   Dyam_Unify_Item(&ref[385])
	fail_ret
	call_c   Dyam_Allocate(0)
	pl_call  Object_1(&ref[387])
	call_c   Dyam_Choice(fun141)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[388])
	call_c   Dyam_Unify(V(6),&ref[389])
	fail_ret
	call_c   Dyam_Cut()
	move     &ref[390], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(11), R(4)
	move     S(5), R(5)
	pl_call  pred_my_numbervars_3()
	call_c   Dyam_Unify(V(5),&ref[391])
	fail_ret
	pl_jump  fun132()

;; TERM 386: '*GUARD*'(generalized_scanner(_B, _C, _D, _E, _F, _G)) :> []
c_code local build_ref_386
	ret_reg &ref[386]
	call_c   build_ref_385()
	call_c   Dyam_Create_Binary(I(9),&ref[385],I(0))
	move_ret ref[386]
	c_ret

c_code local build_seed_45
	ret_reg &seed[45]
	call_c   build_ref_50()
	call_c   build_ref_394()
	call_c   Dyam_Seed_Start(&ref[50],&ref[394],I(0),fun5,1)
	call_c   build_ref_393()
	call_c   Dyam_Seed_Add_Comp(&ref[393],fun143,0)
	call_c   Dyam_Seed_End()
	move_ret seed[45]
	c_ret

;; TERM 393: '*GUARD*'(dcg_body_to_lpda_handler(_B, (_C & _D), _E, _F, _G, _H, _I, _J, _K))
c_code local build_ref_393
	ret_reg &ref[393]
	call_c   build_ref_51()
	call_c   build_ref_392()
	call_c   Dyam_Create_Unary(&ref[51],&ref[392])
	move_ret ref[393]
	c_ret

;; TERM 392: dcg_body_to_lpda_handler(_B, (_C & _D), _E, _F, _G, _H, _I, _J, _K)
c_code local build_ref_392
	ret_reg &ref[392]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_886()
	call_c   Dyam_Create_Binary(&ref[886],V(2),V(3))
	move_ret R(0)
	call_c   build_ref_828()
	call_c   Dyam_Term_Start(&ref[828],9)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_End()
	move_ret ref[392]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 886: &
c_code local build_ref_886
	ret_reg &ref[886]
	call_c   Dyam_Create_Atom("&")
	move_ret ref[886]
	c_ret

long local pool_fun143[3]=[2,build_ref_393,build_seed_73]

pl_code local fun143
	call_c   Dyam_Pool(pool_fun143)
	call_c   Dyam_Unify_Item(&ref[393])
	fail_ret
	call_c   Dyam_Allocate(0)
	pl_call  fun7(&seed[73],1)
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(3))
	call_c   Dyam_Reg_Load(4,V(4))
	call_c   Dyam_Reg_Load(6,V(5))
	call_c   Dyam_Reg_Load(8,V(6))
	call_c   Dyam_Reg_Load(10,V(7))
	call_c   Dyam_Reg_Load(12,V(8))
	move     V(11), R(14)
	move     S(5), R(15)
	call_c   Dyam_Reg_Load(16,V(10))
	pl_call  pred_dcg_body_to_lpda_9()
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(2))
	call_c   Dyam_Reg_Load(4,V(4))
	call_c   Dyam_Reg_Load(6,V(5))
	call_c   Dyam_Reg_Load(8,V(6))
	call_c   Dyam_Reg_Load(10,V(7))
	call_c   Dyam_Reg_Load(12,V(11))
	call_c   Dyam_Reg_Load(14,V(9))
	call_c   Dyam_Reg_Load(16,V(10))
	call_c   Dyam_Reg_Deallocate(9)
	pl_jump  pred_dcg_body_to_lpda_9()

;; TERM 394: '*GUARD*'(dcg_body_to_lpda_handler(_B, (_C & _D), _E, _F, _G, _H, _I, _J, _K)) :> []
c_code local build_ref_394
	ret_reg &ref[394]
	call_c   build_ref_393()
	call_c   Dyam_Create_Binary(I(9),&ref[393],I(0))
	move_ret ref[394]
	c_ret

c_code local build_seed_46
	ret_reg &seed[46]
	call_c   build_ref_50()
	call_c   build_ref_397()
	call_c   Dyam_Seed_Start(&ref[50],&ref[397],I(0),fun5,1)
	call_c   build_ref_396()
	call_c   Dyam_Seed_Add_Comp(&ref[396],fun144,0)
	call_c   Dyam_Seed_End()
	move_ret seed[46]
	c_ret

;; TERM 396: '*GUARD*'(dcg_body_to_lpda_handler(_B, (_C <+ _D), _E, _F, _G, _H, _I, _J, _K))
c_code local build_ref_396
	ret_reg &ref[396]
	call_c   build_ref_51()
	call_c   build_ref_395()
	call_c   Dyam_Create_Unary(&ref[51],&ref[395])
	move_ret ref[396]
	c_ret

;; TERM 395: dcg_body_to_lpda_handler(_B, (_C <+ _D), _E, _F, _G, _H, _I, _J, _K)
c_code local build_ref_395
	ret_reg &ref[395]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_887()
	call_c   Dyam_Create_Binary(&ref[887],V(2),V(3))
	move_ret R(0)
	call_c   build_ref_828()
	call_c   Dyam_Term_Start(&ref[828],9)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_End()
	move_ret ref[395]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 887: <+
c_code local build_ref_887
	ret_reg &ref[887]
	call_c   Dyam_Create_Atom("<+")
	move_ret ref[887]
	c_ret

c_code local build_seed_105
	ret_reg &seed[105]
	call_c   build_ref_51()
	call_c   build_ref_399()
	call_c   Dyam_Seed_Start(&ref[51],&ref[399],I(0),fun0,1)
	call_c   build_ref_400()
	call_c   Dyam_Seed_Add_Comp(&ref[400],&ref[399],0)
	call_c   Dyam_Seed_End()
	move_ret seed[105]
	c_ret

;; TERM 400: '*GUARD*'(position_and_stacks(_B, _L, _M)) :> '$$HOLE$$'
c_code local build_ref_400
	ret_reg &ref[400]
	call_c   build_ref_399()
	call_c   Dyam_Create_Binary(I(9),&ref[399],I(7))
	move_ret ref[400]
	c_ret

;; TERM 399: '*GUARD*'(position_and_stacks(_B, _L, _M))
c_code local build_ref_399
	ret_reg &ref[399]
	call_c   build_ref_51()
	call_c   build_ref_398()
	call_c   Dyam_Create_Unary(&ref[51],&ref[398])
	move_ret ref[399]
	c_ret

;; TERM 398: position_and_stacks(_B, _L, _M)
c_code local build_ref_398
	ret_reg &ref[398]
	call_c   build_ref_826()
	call_c   Dyam_Term_Start(&ref[826],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_End()
	move_ret ref[398]
	c_ret

long local pool_fun144[3]=[2,build_ref_396,build_seed_105]

pl_code local fun144
	call_c   Dyam_Pool(pool_fun144)
	call_c   Dyam_Unify_Item(&ref[396])
	fail_ret
	call_c   Dyam_Allocate(0)
	pl_call  fun7(&seed[105],1)
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(2))
	call_c   Dyam_Reg_Load(4,V(4))
	call_c   Dyam_Reg_Load(6,V(11))
	call_c   Dyam_Reg_Load(8,V(6))
	call_c   Dyam_Reg_Load(10,V(12))
	call_c   Dyam_Reg_Load(12,V(8))
	move     V(13), R(14)
	move     S(5), R(15)
	call_c   Dyam_Reg_Load(16,V(10))
	pl_call  pred_dcg_body_to_lpda_9()
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(3))
	call_c   Dyam_Reg_Load(4,V(11))
	call_c   Dyam_Reg_Load(6,V(5))
	call_c   Dyam_Reg_Load(8,V(12))
	call_c   Dyam_Reg_Load(10,V(7))
	call_c   Dyam_Reg_Load(12,V(13))
	call_c   Dyam_Reg_Load(14,V(9))
	call_c   Dyam_Reg_Load(16,V(10))
	call_c   Dyam_Reg_Deallocate(9)
	pl_jump  pred_dcg_body_to_lpda_9()

;; TERM 397: '*GUARD*'(dcg_body_to_lpda_handler(_B, (_C <+ _D), _E, _F, _G, _H, _I, _J, _K)) :> []
c_code local build_ref_397
	ret_reg &ref[397]
	call_c   build_ref_396()
	call_c   Dyam_Create_Binary(I(9),&ref[396],I(0))
	move_ret ref[397]
	c_ret

c_code local build_seed_48
	ret_reg &seed[48]
	call_c   build_ref_50()
	call_c   build_ref_401()
	call_c   Dyam_Seed_Start(&ref[50],&ref[401],I(0),fun5,1)
	call_c   build_ref_170()
	call_c   Dyam_Seed_Add_Comp(&ref[170],fun145,0)
	call_c   Dyam_Seed_End()
	move_ret seed[48]
	c_ret

long local pool_fun145[3]=[2,build_ref_170,build_seed_105]

pl_code local fun145
	call_c   Dyam_Pool(pool_fun145)
	call_c   Dyam_Unify_Item(&ref[170])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(3))
	move     V(11), R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Load(6,V(5))
	move     V(12), R(8)
	move     S(5), R(9)
	call_c   Dyam_Reg_Load(10,V(7))
	call_c   Dyam_Reg_Load(12,V(8))
	move     V(13), R(14)
	move     S(5), R(15)
	call_c   Dyam_Reg_Load(16,V(10))
	pl_call  pred_dcg_body_to_lpda_9()
	pl_call  fun7(&seed[105],1)
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(2))
	call_c   Dyam_Reg_Load(4,V(4))
	call_c   Dyam_Reg_Load(6,V(11))
	call_c   Dyam_Reg_Load(8,V(6))
	call_c   Dyam_Reg_Load(10,V(12))
	call_c   Dyam_Reg_Load(12,V(13))
	call_c   Dyam_Reg_Load(14,V(9))
	call_c   Dyam_Reg_Load(16,V(10))
	call_c   Dyam_Reg_Deallocate(9)
	pl_jump  pred_dcg_body_to_lpda_9()

;; TERM 401: '*GUARD*'(dcg_body_to_lpda_handler(_B, (_C , _D), _E, _F, _G, _H, _I, _J, _K)) :> []
c_code local build_ref_401
	ret_reg &ref[401]
	call_c   build_ref_170()
	call_c   Dyam_Create_Binary(I(9),&ref[170],I(0))
	move_ret ref[401]
	c_ret

c_code local build_seed_4
	ret_reg &seed[4]
	call_c   build_ref_36()
	call_c   build_ref_405()
	call_c   Dyam_Seed_Start(&ref[36],&ref[405],I(0),fun4,1)
	call_c   build_ref_406()
	call_c   Dyam_Seed_Add_Comp(&ref[406],fun152,0)
	call_c   Dyam_Seed_End()
	move_ret seed[4]
	c_ret

;; TERM 406: '*CITEM*'('call_dcg_prolog_make/7'((dcg _B / _C)), _A)
c_code local build_ref_406
	ret_reg &ref[406]
	call_c   build_ref_39()
	call_c   build_ref_403()
	call_c   Dyam_Create_Binary(&ref[39],&ref[403],V(0))
	move_ret ref[406]
	c_ret

;; TERM 403: 'call_dcg_prolog_make/7'((dcg _B / _C))
c_code local build_ref_403
	ret_reg &ref[403]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_840()
	call_c   Dyam_Create_Binary(&ref[840],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_839()
	call_c   Dyam_Create_Unary(&ref[839],R(0))
	move_ret R(0)
	call_c   build_ref_888()
	call_c   Dyam_Create_Unary(&ref[888],R(0))
	move_ret ref[403]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 888: 'call_dcg_prolog_make/7'
c_code local build_ref_888
	ret_reg &ref[888]
	call_c   Dyam_Create_Atom("call_dcg_prolog_make/7")
	move_ret ref[888]
	c_ret

;; TERM 407: [_B|_J]
c_code local build_ref_407
	ret_reg &ref[407]
	call_c   Dyam_Create_List(V(1),V(9))
	move_ret ref[407]
	c_ret

;; TERM 426: '$CLOSURE'('$fun'(151, 0, 1148031060), '$TUPPLE'(35074870617576))
c_code local build_ref_426
	ret_reg &ref[426]
	call_c   build_ref_425()
	call_c   Dyam_Closure_Aux(fun151,&ref[425])
	move_ret ref[426]
	c_ret

;; TERM 417: [_B|_K]
c_code local build_ref_417
	ret_reg &ref[417]
	call_c   Dyam_Create_List(V(1),V(10))
	move_ret ref[417]
	c_ret

c_code local build_seed_109
	ret_reg &seed[109]
	call_c   build_ref_0()
	call_c   build_ref_422()
	call_c   Dyam_Seed_Start(&ref[0],&ref[422],&ref[422],fun0,1)
	call_c   build_ref_423()
	call_c   Dyam_Seed_Add_Comp(&ref[423],&ref[422],0)
	call_c   Dyam_Seed_End()
	move_ret seed[109]
	c_ret

;; TERM 423: '*RITEM*'(_A, return(_D, _E, _F, _G, _H, _I)) :> '$$HOLE$$'
c_code local build_ref_423
	ret_reg &ref[423]
	call_c   build_ref_422()
	call_c   Dyam_Create_Binary(I(9),&ref[422],I(7))
	move_ret ref[423]
	c_ret

;; TERM 422: '*RITEM*'(_A, return(_D, _E, _F, _G, _H, _I))
c_code local build_ref_422
	ret_reg &ref[422]
	call_c   build_ref_0()
	call_c   build_ref_421()
	call_c   Dyam_Create_Binary(&ref[0],V(0),&ref[421])
	move_ret ref[422]
	c_ret

;; TERM 421: return(_D, _E, _F, _G, _H, _I)
c_code local build_ref_421
	ret_reg &ref[421]
	call_c   build_ref_867()
	call_c   Dyam_Term_Start(&ref[867],6)
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_End()
	move_ret ref[421]
	c_ret

long local pool_fun149[2]=[1,build_seed_109]

pl_code local fun150
	call_c   Dyam_Remove_Choice()
fun149:
	call_c   Dyam_Deallocate()
	pl_jump  fun2(&seed[109],2)


;; TERM 418: '*PROLOG-ITEM*'{top=> _I, cont=> _M}
c_code local build_ref_418
	ret_reg &ref[418]
	call_c   build_ref_889()
	call_c   Dyam_Create_Binary(&ref[889],V(8),V(12))
	move_ret ref[418]
	c_ret

;; TERM 889: '*PROLOG-ITEM*'!'$ft'
c_code local build_ref_889
	ret_reg &ref[889]
	call_c   build_ref_890()
	call_c   Dyam_Create_Atom_Module("$ft",&ref[890])
	move_ret ref[889]
	c_ret

;; TERM 890: '*PROLOG-ITEM*'
c_code local build_ref_890
	ret_reg &ref[890]
	call_c   Dyam_Create_Atom("*PROLOG-ITEM*")
	move_ret ref[890]
	c_ret

;; TERM 419: _D(_E,_F)
c_code local build_ref_419
	ret_reg &ref[419]
	call_c   Dyam_Term_Start(I(3),3)
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[419]
	c_ret

;; TERM 420: viewer(_N, _O)
c_code local build_ref_420
	ret_reg &ref[420]
	call_c   build_ref_891()
	call_c   Dyam_Create_Binary(&ref[891],V(13),V(14))
	move_ret ref[420]
	c_ret

;; TERM 891: viewer
c_code local build_ref_891
	ret_reg &ref[891]
	call_c   Dyam_Create_Atom("viewer")
	move_ret ref[891]
	c_ret

long local pool_fun151[6]=[65540,build_ref_417,build_ref_418,build_ref_419,build_ref_420,pool_fun149]

pl_code local fun151
	call_c   Dyam_Pool(pool_fun151)
	call_c   DYAM_evpred_univ(V(8),&ref[417])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun150)
	call_c   Dyam_Unify(V(11),&ref[418])
	fail_ret
	call_c   DYAM_Numbervars_3(V(11),N(0),V(15))
	fail_ret
	call_c   Dyam_Reg_Load(0,V(11))
	move     V(13), R(2)
	move     S(5), R(3)
	pl_call  pred_label_term_2()
	move     &ref[419], R(0)
	move     S(5), R(1)
	move     V(14), R(2)
	move     S(5), R(3)
	pl_call  pred_label_term_2()
	call_c   DYAM_evpred_assert_1(&ref[420])
	pl_fail

;; TERM 425: '$TUPPLE'(35074870617576)
c_code local build_ref_425
	ret_reg &ref[425]
	call_c   Dyam_Create_Simple_Tupple(0,467927040)
	move_ret ref[425]
	c_ret

;; TERM 427: bmg_args(_E, _F, _G, _H, _J, _K)
c_code local build_ref_427
	ret_reg &ref[427]
	call_c   build_ref_892()
	call_c   Dyam_Term_Start(&ref[892],6)
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_End()
	move_ret ref[427]
	c_ret

;; TERM 892: bmg_args
c_code local build_ref_892
	ret_reg &ref[892]
	call_c   Dyam_Create_Atom("bmg_args")
	move_ret ref[892]
	c_ret

long local pool_fun152[5]=[4,build_ref_406,build_ref_407,build_ref_426,build_ref_427]

pl_code local fun152
	call_c   Dyam_Pool(pool_fun152)
	call_c   Dyam_Unify_Item(&ref[406])
	fail_ret
	call_c   DYAM_evpred_functor(V(3),V(1),V(2))
	fail_ret
	call_c   DYAM_evpred_univ(V(3),&ref[407])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[426], R(0)
	move     S(5), R(1)
	move     &ref[427], R(2)
	move     S(5), R(3)
	move     I(0), R(4)
	move     0, R(5)
	call_c   Dyam_Reg_Deallocate(3)
fun148:
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Reg_Unify(2,&ref[416])
	fail_ret
	call_c   Dyam_Reg_Bind(4,V(8))
	call_c   Dyam_Choice(fun147)
	pl_call  fun13(&seed[107],1)
	pl_fail


;; TERM 405: '*FIRST*'('call_dcg_prolog_make/7'((dcg _B / _C))) :> []
c_code local build_ref_405
	ret_reg &ref[405]
	call_c   build_ref_404()
	call_c   Dyam_Create_Binary(I(9),&ref[404],I(0))
	move_ret ref[405]
	c_ret

;; TERM 404: '*FIRST*'('call_dcg_prolog_make/7'((dcg _B / _C)))
c_code local build_ref_404
	ret_reg &ref[404]
	call_c   build_ref_36()
	call_c   build_ref_403()
	call_c   Dyam_Create_Unary(&ref[36],&ref[403])
	move_ret ref[404]
	c_ret

c_code local build_seed_53
	ret_reg &seed[53]
	call_c   build_ref_50()
	call_c   build_ref_430()
	call_c   Dyam_Seed_Start(&ref[50],&ref[430],I(0),fun5,1)
	call_c   build_ref_429()
	call_c   Dyam_Seed_Add_Comp(&ref[429],fun183,0)
	call_c   Dyam_Seed_End()
	move_ret seed[53]
	c_ret

;; TERM 429: '*GUARD*'(pgm_to_lpda(grammar_rule(dcg, (_B --> _C)), _D))
c_code local build_ref_429
	ret_reg &ref[429]
	call_c   build_ref_51()
	call_c   build_ref_428()
	call_c   Dyam_Create_Unary(&ref[51],&ref[428])
	move_ret ref[429]
	c_ret

;; TERM 428: pgm_to_lpda(grammar_rule(dcg, (_B --> _C)), _D)
c_code local build_ref_428
	ret_reg &ref[428]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_893()
	call_c   build_ref_839()
	call_c   build_ref_431()
	call_c   Dyam_Create_Binary(&ref[893],&ref[839],&ref[431])
	move_ret R(0)
	call_c   build_ref_837()
	call_c   Dyam_Create_Binary(&ref[837],R(0),V(3))
	move_ret ref[428]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 431: _B --> _C
c_code local build_ref_431
	ret_reg &ref[431]
	call_c   build_ref_894()
	call_c   Dyam_Create_Binary(&ref[894],V(1),V(2))
	move_ret ref[431]
	c_ret

;; TERM 894: -->
c_code local build_ref_894
	ret_reg &ref[894]
	call_c   Dyam_Create_Atom("-->")
	move_ret ref[894]
	c_ret

;; TERM 893: grammar_rule
c_code local build_ref_893
	ret_reg &ref[893]
	call_c   Dyam_Create_Atom("grammar_rule")
	move_ret ref[893]
	c_ret

;; TERM 509: '$CLOSURE'('$fun'(177, 0, 1148287432), '$TUPPLE'(35074870878528))
c_code local build_ref_509
	ret_reg &ref[509]
	call_c   build_ref_508()
	call_c   Dyam_Closure_Aux(fun177,&ref[508])
	move_ret ref[509]
	c_ret

;; TERM 445: dcg _B1 / _C1
c_code local build_ref_445
	ret_reg &ref[445]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_840()
	call_c   Dyam_Create_Binary(&ref[840],V(27),V(28))
	move_ret R(0)
	call_c   build_ref_839()
	call_c   Dyam_Create_Unary(&ref[839],R(0))
	move_ret ref[445]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 506: '$CLOSURE'('$fun'(174, 0, 1148276540), '$TUPPLE'(35074870877724))
c_code local build_ref_506
	ret_reg &ref[506]
	call_c   build_ref_473()
	call_c   Dyam_Closure_Aux(fun174,&ref[473])
	move_ret ref[506]
	c_ret

c_code local build_seed_118
	ret_reg &seed[118]
	call_c   build_ref_51()
	call_c   build_ref_487()
	call_c   Dyam_Seed_Start(&ref[51],&ref[487],I(0),fun0,1)
	call_c   build_ref_488()
	call_c   Dyam_Seed_Add_Comp(&ref[488],&ref[487],0)
	call_c   Dyam_Seed_End()
	move_ret seed[118]
	c_ret

;; TERM 488: '*GUARD*'(make_dcg_callret(_E1, _G, _I, _V, _W, _I1, _J1)) :> '$$HOLE$$'
c_code local build_ref_488
	ret_reg &ref[488]
	call_c   build_ref_487()
	call_c   Dyam_Create_Binary(I(9),&ref[487],I(7))
	move_ret ref[488]
	c_ret

;; TERM 487: '*GUARD*'(make_dcg_callret(_E1, _G, _I, _V, _W, _I1, _J1))
c_code local build_ref_487
	ret_reg &ref[487]
	call_c   build_ref_51()
	call_c   build_ref_486()
	call_c   Dyam_Create_Unary(&ref[51],&ref[486])
	move_ret ref[487]
	c_ret

;; TERM 486: make_dcg_callret(_E1, _G, _I, _V, _W, _I1, _J1)
c_code local build_ref_486
	ret_reg &ref[486]
	call_c   build_ref_895()
	call_c   Dyam_Term_Start(&ref[895],7)
	call_c   Dyam_Term_Arg(V(30))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(21))
	call_c   Dyam_Term_Arg(V(22))
	call_c   Dyam_Term_Arg(V(34))
	call_c   Dyam_Term_Arg(V(35))
	call_c   Dyam_Term_End()
	move_ret ref[486]
	c_ret

;; TERM 895: make_dcg_callret
c_code local build_ref_895
	ret_reg &ref[895]
	call_c   Dyam_Create_Atom("make_dcg_callret")
	move_ret ref[895]
	c_ret

;; TERM 489: '*FIRST*'(_I1)
c_code local build_ref_489
	ret_reg &ref[489]
	call_c   build_ref_36()
	call_c   Dyam_Create_Unary(&ref[36],V(34))
	move_ret ref[489]
	c_ret

;; TERM 503: '*LAST*'(_J1)
c_code local build_ref_503
	ret_reg &ref[503]
	call_c   build_ref_896()
	call_c   Dyam_Create_Unary(&ref[896],V(35))
	move_ret ref[503]
	c_ret

;; TERM 896: '*LAST*'
c_code local build_ref_896
	ret_reg &ref[896]
	call_c   Dyam_Create_Atom("*LAST*")
	move_ret ref[896]
	c_ret

;; TERM 504: dyalog
c_code local build_ref_504
	ret_reg &ref[504]
	call_c   Dyam_Create_Atom("dyalog")
	move_ret ref[504]
	c_ret

c_code local build_seed_119
	ret_reg &seed[119]
	call_c   build_ref_51()
	call_c   build_ref_493()
	call_c   Dyam_Seed_Start(&ref[51],&ref[493],I(0),fun0,1)
	call_c   build_ref_494()
	call_c   Dyam_Seed_Add_Comp(&ref[494],&ref[493],0)
	call_c   Dyam_Seed_End()
	move_ret seed[119]
	c_ret

;; TERM 494: '*GUARD*'(dcg_trail_to_lpda(_E, _L, _I, _M, _L1, _H1, _M1)) :> '$$HOLE$$'
c_code local build_ref_494
	ret_reg &ref[494]
	call_c   build_ref_493()
	call_c   Dyam_Create_Binary(I(9),&ref[493],I(7))
	move_ret ref[494]
	c_ret

;; TERM 493: '*GUARD*'(dcg_trail_to_lpda(_E, _L, _I, _M, _L1, _H1, _M1))
c_code local build_ref_493
	ret_reg &ref[493]
	call_c   build_ref_51()
	call_c   build_ref_492()
	call_c   Dyam_Create_Unary(&ref[51],&ref[492])
	move_ret ref[493]
	c_ret

;; TERM 492: dcg_trail_to_lpda(_E, _L, _I, _M, _L1, _H1, _M1)
c_code local build_ref_492
	ret_reg &ref[492]
	call_c   build_ref_897()
	call_c   Dyam_Term_Start(&ref[897],7)
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(37))
	call_c   Dyam_Term_Arg(V(33))
	call_c   Dyam_Term_Arg(V(38))
	call_c   Dyam_Term_End()
	move_ret ref[492]
	c_ret

;; TERM 897: dcg_trail_to_lpda
c_code local build_ref_897
	ret_reg &ref[897]
	call_c   Dyam_Create_Atom("dcg_trail_to_lpda")
	move_ret ref[897]
	c_ret

c_code local build_seed_120
	ret_reg &seed[120]
	call_c   build_ref_51()
	call_c   build_ref_496()
	call_c   Dyam_Seed_Start(&ref[51],&ref[496],I(0),fun0,1)
	call_c   build_ref_497()
	call_c   Dyam_Seed_Add_Comp(&ref[497],&ref[496],0)
	call_c   Dyam_Seed_End()
	move_ret seed[120]
	c_ret

;; TERM 497: '*GUARD*'(dcg_try_lc_transform(_E, (_D1 / _C1), _K1, _G1, _I1, _N1)) :> '$$HOLE$$'
c_code local build_ref_497
	ret_reg &ref[497]
	call_c   build_ref_496()
	call_c   Dyam_Create_Binary(I(9),&ref[496],I(7))
	move_ret ref[497]
	c_ret

;; TERM 496: '*GUARD*'(dcg_try_lc_transform(_E, (_D1 / _C1), _K1, _G1, _I1, _N1))
c_code local build_ref_496
	ret_reg &ref[496]
	call_c   build_ref_51()
	call_c   build_ref_495()
	call_c   Dyam_Create_Unary(&ref[51],&ref[495])
	move_ret ref[496]
	c_ret

;; TERM 495: dcg_try_lc_transform(_E, (_D1 / _C1), _K1, _G1, _I1, _N1)
c_code local build_ref_495
	ret_reg &ref[495]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_840()
	call_c   Dyam_Create_Binary(&ref[840],V(29),V(28))
	move_ret R(0)
	call_c   build_ref_898()
	call_c   Dyam_Term_Start(&ref[898],6)
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(36))
	call_c   Dyam_Term_Arg(V(32))
	call_c   Dyam_Term_Arg(V(34))
	call_c   Dyam_Term_Arg(V(39))
	call_c   Dyam_Term_End()
	move_ret ref[495]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 898: dcg_try_lc_transform
c_code local build_ref_898
	ret_reg &ref[898]
	call_c   Dyam_Create_Atom("dcg_try_lc_transform")
	move_ret ref[898]
	c_ret

pl_code local fun169
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(3),V(39))
	fail_ret
fun166:
	call_c   Dyam_Reg_Load(0,V(4))
	pl_call  pred_numbervars_delete_1()
	pl_jump  fun132()


;; TERM 498: parse_mode(token)
c_code local build_ref_498
	ret_reg &ref[498]
	call_c   build_ref_881()
	call_c   build_ref_553()
	call_c   Dyam_Create_Unary(&ref[881],&ref[553])
	move_ret ref[498]
	c_ret

;; TERM 553: token
c_code local build_ref_553
	ret_reg &ref[553]
	call_c   Dyam_Create_Atom("token")
	move_ret ref[553]
	c_ret

c_code local build_seed_121
	ret_reg &seed[121]
	call_c   build_ref_51()
	call_c   build_ref_500()
	call_c   Dyam_Seed_Start(&ref[51],&ref[500],I(0),fun0,1)
	call_c   build_ref_501()
	call_c   Dyam_Seed_Add_Comp(&ref[501],&ref[500],0)
	call_c   Dyam_Seed_End()
	move_ret seed[121]
	c_ret

;; TERM 501: '*GUARD*'(build_cond_loader(_E, (_O1 -> true ; fail), _N1, _D)) :> '$$HOLE$$'
c_code local build_ref_501
	ret_reg &ref[501]
	call_c   build_ref_500()
	call_c   Dyam_Create_Binary(I(9),&ref[500],I(7))
	move_ret ref[501]
	c_ret

;; TERM 500: '*GUARD*'(build_cond_loader(_E, (_O1 -> true ; fail), _N1, _D))
c_code local build_ref_500
	ret_reg &ref[500]
	call_c   build_ref_51()
	call_c   build_ref_499()
	call_c   Dyam_Create_Unary(&ref[51],&ref[499])
	move_ret ref[500]
	c_ret

;; TERM 499: build_cond_loader(_E, (_O1 -> true ; fail), _N1, _D)
c_code local build_ref_499
	ret_reg &ref[499]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_847()
	call_c   build_ref_186()
	call_c   Dyam_Create_Binary(&ref[847],V(40),&ref[186])
	move_ret R(0)
	call_c   build_ref_829()
	call_c   Dyam_Create_Binary(I(5),R(0),&ref[829])
	move_ret R(0)
	call_c   build_ref_880()
	call_c   Dyam_Term_Start(&ref[880],4)
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(39))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[499]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun167[2]=[1,build_seed_121]

pl_code local fun168
	call_c   Dyam_Remove_Choice()
fun167:
	call_c   Dyam_Cut()
	pl_call  fun7(&seed[121],1)
	pl_jump  fun166()


long local pool_fun170[7]=[65541,build_seed_119,build_seed_120,build_ref_349,build_ref_498,build_ref_186,pool_fun167]

long local pool_fun173[4]=[65538,build_ref_503,build_ref_504,pool_fun170]

pl_code local fun173
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(37),&ref[503])
	fail_ret
	call_c   Dyam_Unify(V(38),&ref[504])
	fail_ret
fun170:
	pl_call  fun7(&seed[119],1)
	call_c   Dyam_Reg_Load(0,V(4))
	call_c   Dyam_Reg_Load(2,V(2))
	call_c   Dyam_Reg_Load(4,V(6))
	call_c   Dyam_Reg_Load(6,V(12))
	call_c   Dyam_Reg_Load(8,V(23))
	call_c   Dyam_Reg_Load(10,V(24))
	call_c   Dyam_Reg_Load(12,V(33))
	move     V(32), R(14)
	move     S(5), R(15)
	call_c   Dyam_Reg_Load(16,V(38))
	pl_call  pred_dcg_body_to_lpda_9()
	pl_call  fun7(&seed[120],1)
	call_c   Dyam_Choice(fun169)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[349])
	pl_call  Object_1(&ref[498])
	call_c   Dyam_Reg_Load(0,V(2))
	move     V(40), R(2)
	move     S(5), R(3)
	pl_call  pred_dcg_autoloader_2()
	call_c   Dyam_Choice(fun168)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(40),&ref[186])
	fail_ret
	call_c   Dyam_Cut()
	pl_fail


;; TERM 490: light_tabular dcg _D1 / _C1
c_code local build_ref_490
	ret_reg &ref[490]
	call_c   build_ref_899()
	call_c   build_ref_446()
	call_c   Dyam_Create_Unary(&ref[899],&ref[446])
	move_ret ref[490]
	c_ret

;; TERM 446: dcg _D1 / _C1
c_code local build_ref_446
	ret_reg &ref[446]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_840()
	call_c   Dyam_Create_Binary(&ref[840],V(29),V(28))
	move_ret R(0)
	call_c   build_ref_839()
	call_c   Dyam_Create_Unary(&ref[839],R(0))
	move_ret ref[446]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 899: light_tabular
c_code local build_ref_899
	ret_reg &ref[899]
	call_c   Dyam_Create_Atom("light_tabular")
	move_ret ref[899]
	c_ret

;; TERM 502: '*LIGHTLAST*'(_J1)
c_code local build_ref_502
	ret_reg &ref[502]
	call_c   build_ref_900()
	call_c   Dyam_Create_Unary(&ref[900],V(35))
	move_ret ref[502]
	c_ret

;; TERM 900: '*LIGHTLAST*'
c_code local build_ref_900
	ret_reg &ref[900]
	call_c   Dyam_Create_Atom("*LIGHTLAST*")
	move_ret ref[900]
	c_ret

long local pool_fun171[3]=[65537,build_ref_248,pool_fun170]

long local pool_fun172[3]=[65537,build_ref_502,pool_fun171]

pl_code local fun172
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(37),&ref[502])
	fail_ret
fun171:
	call_c   Dyam_Unify(V(38),&ref[248])
	fail_ret
	pl_jump  fun170()


;; TERM 491: '*LIGHTLCLAST*'(_J1)
c_code local build_ref_491
	ret_reg &ref[491]
	call_c   build_ref_901()
	call_c   Dyam_Create_Unary(&ref[901],V(35))
	move_ret ref[491]
	c_ret

;; TERM 901: '*LIGHTLCLAST*'
c_code local build_ref_901
	ret_reg &ref[901]
	call_c   Dyam_Create_Atom("*LIGHTLCLAST*")
	move_ret ref[901]
	c_ret

long local pool_fun174[9]=[196613,build_seed_118,build_ref_489,build_ref_490,build_ref_446,build_ref_491,pool_fun173,pool_fun172,pool_fun171]

pl_code local fun174
	call_c   Dyam_Pool(pool_fun174)
	call_c   Dyam_Allocate(0)
	pl_call  fun7(&seed[118],1)
	call_c   Dyam_Unify(V(36),&ref[489])
	fail_ret
	call_c   Dyam_Choice(fun173)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[490])
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun172)
	call_c   Dyam_Set_Cut()
	move     &ref[446], R(0)
	move     S(5), R(1)
	pl_call  pred_check_lc_1()
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(37),&ref[491])
	fail_ret
	pl_jump  fun171()

;; TERM 473: '$TUPPLE'(35074870877724)
c_code local build_ref_473
	ret_reg &ref[473]
	call_c   Dyam_Start_Tupple(0,122880241)
	call_c   Dyam_Almost_End_Tupple(29,402653184)
	move_ret ref[473]
	c_ret

long local pool_fun175[3]=[2,build_ref_506,build_ref_446]

pl_code local fun175
	call_c   Dyam_Remove_Choice()
	move     &ref[506], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	move     &ref[446], R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Deallocate(3)
fun158:
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Reg_Bind(2,V(3))
	call_c   Dyam_Reg_Bind(4,V(2))
	call_c   Dyam_Choice(fun157)
	pl_call  fun13(&seed[112],1)
	pl_fail


;; TERM 475: prolog dcg _D1 / _C1
c_code local build_ref_475
	ret_reg &ref[475]
	call_c   build_ref_110()
	call_c   build_ref_446()
	call_c   Dyam_Create_Unary(&ref[110],&ref[446])
	move_ret ref[475]
	c_ret

;; TERM 485: '$CLOSURE'('$fun'(165, 0, 1148190852), '$TUPPLE'(35074870877724))
c_code local build_ref_485
	ret_reg &ref[485]
	call_c   build_ref_473()
	call_c   Dyam_Closure_Aux(fun165,&ref[473])
	move_ret ref[485]
	c_ret

;; TERM 483: '$CLOSURE'('$fun'(164, 0, 1148191488), '$TUPPLE'(35074870877628))
c_code local build_ref_483
	ret_reg &ref[483]
	call_c   build_ref_469()
	call_c   Dyam_Closure_Aux(fun164,&ref[469])
	move_ret ref[483]
	c_ret

;; TERM 478: '*PROLOG-FIRST*'(_F1) :> _G1
c_code local build_ref_478
	ret_reg &ref[478]
	call_c   build_ref_477()
	call_c   Dyam_Create_Binary(I(9),&ref[477],V(32))
	move_ret ref[478]
	c_ret

;; TERM 477: '*PROLOG-FIRST*'(_F1)
c_code local build_ref_477
	ret_reg &ref[477]
	call_c   build_ref_476()
	call_c   Dyam_Create_Unary(&ref[476],V(31))
	move_ret ref[477]
	c_ret

c_code local build_seed_117
	ret_reg &seed[117]
	call_c   build_ref_51()
	call_c   build_ref_480()
	call_c   Dyam_Seed_Start(&ref[51],&ref[480],I(0),fun0,1)
	call_c   build_ref_481()
	call_c   Dyam_Seed_Add_Comp(&ref[481],&ref[480],0)
	call_c   Dyam_Seed_End()
	move_ret seed[117]
	c_ret

;; TERM 481: '*GUARD*'(dcg_trail_to_lpda(_E, _L, _I, _M, '*PROLOG-LAST*', _H1, prolog)) :> '$$HOLE$$'
c_code local build_ref_481
	ret_reg &ref[481]
	call_c   build_ref_480()
	call_c   Dyam_Create_Binary(I(9),&ref[480],I(7))
	move_ret ref[481]
	c_ret

;; TERM 480: '*GUARD*'(dcg_trail_to_lpda(_E, _L, _I, _M, '*PROLOG-LAST*', _H1, prolog))
c_code local build_ref_480
	ret_reg &ref[480]
	call_c   build_ref_51()
	call_c   build_ref_479()
	call_c   Dyam_Create_Unary(&ref[51],&ref[479])
	move_ret ref[480]
	c_ret

;; TERM 479: dcg_trail_to_lpda(_E, _L, _I, _M, '*PROLOG-LAST*', _H1, prolog)
c_code local build_ref_479
	ret_reg &ref[479]
	call_c   build_ref_897()
	call_c   build_ref_109()
	call_c   build_ref_110()
	call_c   Dyam_Term_Start(&ref[897],7)
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(&ref[109])
	call_c   Dyam_Term_Arg(V(33))
	call_c   Dyam_Term_Arg(&ref[110])
	call_c   Dyam_Term_End()
	move_ret ref[479]
	c_ret

long local pool_fun164[4]=[3,build_ref_478,build_seed_117,build_ref_110]

pl_code local fun164
	call_c   Dyam_Pool(pool_fun164)
	call_c   Dyam_Unify(V(3),&ref[478])
	fail_ret
	call_c   Dyam_Allocate(0)
	pl_call  fun7(&seed[117],1)
	call_c   Dyam_Reg_Load(0,V(4))
	call_c   Dyam_Reg_Load(2,V(2))
	call_c   Dyam_Reg_Load(4,V(6))
	call_c   Dyam_Reg_Load(6,V(12))
	call_c   Dyam_Reg_Load(8,V(23))
	call_c   Dyam_Reg_Load(10,V(24))
	call_c   Dyam_Reg_Load(12,V(33))
	call_c   Dyam_Reg_Load(14,V(32))
	move     &ref[110], R(16)
	move     0, R(17)
	pl_call  pred_dcg_body_to_lpda_9()
	pl_jump  fun132()

;; TERM 469: '$TUPPLE'(35074870877628)
c_code local build_ref_469
	ret_reg &ref[469]
	call_c   Dyam_Start_Tupple(0,122880048)
	call_c   Dyam_Almost_End_Tupple(29,67108864)
	move_ret ref[469]
	c_ret

;; TERM 471: dcg_prolog_make((dcg _D1 / _C1), _E1, _G, _I, _V, _W, _F1)
c_code local build_ref_471
	ret_reg &ref[471]
	call_c   build_ref_902()
	call_c   build_ref_446()
	call_c   Dyam_Term_Start(&ref[902],7)
	call_c   Dyam_Term_Arg(&ref[446])
	call_c   Dyam_Term_Arg(V(30))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(21))
	call_c   Dyam_Term_Arg(V(22))
	call_c   Dyam_Term_Arg(V(31))
	call_c   Dyam_Term_End()
	move_ret ref[471]
	c_ret

;; TERM 902: dcg_prolog_make
c_code local build_ref_902
	ret_reg &ref[902]
	call_c   Dyam_Create_Atom("dcg_prolog_make")
	move_ret ref[902]
	c_ret

long local pool_fun165[3]=[2,build_ref_483,build_ref_471]

pl_code local fun165
	call_c   Dyam_Pool(pool_fun165)
	call_c   Dyam_Allocate(0)
	move     &ref[483], R(0)
	move     S(5), R(1)
	move     &ref[471], R(2)
	move     S(5), R(3)
	move     I(0), R(4)
	move     0, R(5)
	call_c   Dyam_Reg_Deallocate(3)
fun161:
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Reg_Unify(2,&ref[462])
	fail_ret
	call_c   Dyam_Reg_Bind(4,V(9))
	call_c   Dyam_Choice(fun160)
	pl_call  fun13(&seed[114],1)
	pl_fail


long local pool_fun176[5]=[65539,build_ref_475,build_ref_485,build_ref_446,pool_fun175]

pl_code local fun176
	call_c   Dyam_Update_Choice(fun175)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[475])
	call_c   Dyam_Cut()
	move     &ref[485], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	move     &ref[446], R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  fun158()

;; TERM 447: rec_prolog dcg _D1 / _C1
c_code local build_ref_447
	ret_reg &ref[447]
	call_c   build_ref_248()
	call_c   build_ref_446()
	call_c   Dyam_Create_Unary(&ref[248],&ref[446])
	move_ret ref[447]
	c_ret

;; TERM 474: '$CLOSURE'('$fun'(163, 0, 1148168660), '$TUPPLE'(35074870877724))
c_code local build_ref_474
	ret_reg &ref[474]
	call_c   build_ref_473()
	call_c   Dyam_Closure_Aux(fun163,&ref[473])
	move_ret ref[474]
	c_ret

;; TERM 470: '$CLOSURE'('$fun'(162, 0, 1148161768), '$TUPPLE'(35074870877628))
c_code local build_ref_470
	ret_reg &ref[470]
	call_c   build_ref_469()
	call_c   Dyam_Closure_Aux(fun162,&ref[469])
	move_ret ref[470]
	c_ret

;; TERM 464: '*GUARD*'(_F1) :> _G1
c_code local build_ref_464
	ret_reg &ref[464]
	call_c   build_ref_463()
	call_c   Dyam_Create_Binary(I(9),&ref[463],V(32))
	move_ret ref[464]
	c_ret

;; TERM 463: '*GUARD*'(_F1)
c_code local build_ref_463
	ret_reg &ref[463]
	call_c   build_ref_51()
	call_c   Dyam_Create_Unary(&ref[51],V(31))
	move_ret ref[463]
	c_ret

c_code local build_seed_116
	ret_reg &seed[116]
	call_c   build_ref_51()
	call_c   build_ref_466()
	call_c   Dyam_Seed_Start(&ref[51],&ref[466],I(0),fun0,1)
	call_c   build_ref_467()
	call_c   Dyam_Seed_Add_Comp(&ref[467],&ref[466],0)
	call_c   Dyam_Seed_End()
	move_ret seed[116]
	c_ret

;; TERM 467: '*GUARD*'(dcg_trail_to_lpda(_E, _L, _I, _M, noop, _H1, rec_prolog)) :> '$$HOLE$$'
c_code local build_ref_467
	ret_reg &ref[467]
	call_c   build_ref_466()
	call_c   Dyam_Create_Binary(I(9),&ref[466],I(7))
	move_ret ref[467]
	c_ret

;; TERM 466: '*GUARD*'(dcg_trail_to_lpda(_E, _L, _I, _M, noop, _H1, rec_prolog))
c_code local build_ref_466
	ret_reg &ref[466]
	call_c   build_ref_51()
	call_c   build_ref_465()
	call_c   Dyam_Create_Unary(&ref[51],&ref[465])
	move_ret ref[466]
	c_ret

;; TERM 465: dcg_trail_to_lpda(_E, _L, _I, _M, noop, _H1, rec_prolog)
c_code local build_ref_465
	ret_reg &ref[465]
	call_c   build_ref_897()
	call_c   build_ref_179()
	call_c   build_ref_248()
	call_c   Dyam_Term_Start(&ref[897],7)
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(&ref[179])
	call_c   Dyam_Term_Arg(V(33))
	call_c   Dyam_Term_Arg(&ref[248])
	call_c   Dyam_Term_End()
	move_ret ref[465]
	c_ret

;; TERM 179: noop
c_code local build_ref_179
	ret_reg &ref[179]
	call_c   Dyam_Create_Atom("noop")
	move_ret ref[179]
	c_ret

long local pool_fun162[4]=[3,build_ref_464,build_seed_116,build_ref_248]

pl_code local fun162
	call_c   Dyam_Pool(pool_fun162)
	call_c   Dyam_Unify(V(3),&ref[464])
	fail_ret
	call_c   Dyam_Allocate(0)
	pl_call  fun7(&seed[116],1)
	call_c   Dyam_Reg_Load(0,V(4))
	call_c   Dyam_Reg_Load(2,V(2))
	call_c   Dyam_Reg_Load(4,V(6))
	call_c   Dyam_Reg_Load(6,V(12))
	call_c   Dyam_Reg_Load(8,V(23))
	call_c   Dyam_Reg_Load(10,V(24))
	call_c   Dyam_Reg_Load(12,V(33))
	call_c   Dyam_Reg_Load(14,V(32))
	move     &ref[248], R(16)
	move     0, R(17)
	pl_call  pred_dcg_body_to_lpda_9()
	pl_jump  fun132()

long local pool_fun163[3]=[2,build_ref_470,build_ref_471]

pl_code local fun163
	call_c   Dyam_Pool(pool_fun163)
	call_c   Dyam_Allocate(0)
	move     &ref[470], R(0)
	move     S(5), R(1)
	move     &ref[471], R(2)
	move     S(5), R(3)
	move     I(0), R(4)
	move     0, R(5)
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  fun161()

long local pool_fun177[6]=[65540,build_ref_445,build_ref_447,build_ref_474,build_ref_446,pool_fun176]

pl_code local fun177
	call_c   Dyam_Pool(pool_fun177)
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(21))
	call_c   Dyam_Reg_Load(2,V(4))
	move     V(25), R(4)
	move     S(5), R(5)
	pl_call  pred_my_numbervars_3()
	call_c   Dyam_Reg_Load(0,V(22))
	call_c   Dyam_Reg_Load(2,V(4))
	move     V(26), R(4)
	move     S(5), R(5)
	pl_call  pred_my_numbervars_3()
	call_c   DYAM_evpred_functor(V(15),V(27),V(28))
	fail_ret
	move     &ref[445], R(0)
	move     S(5), R(1)
	move     &ref[446], R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Load(4,V(15))
	move     V(30), R(6)
	move     S(5), R(7)
	pl_call  pred_term_current_module_shift_4()
	call_c   Dyam_Choice(fun176)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[447])
	call_c   Dyam_Cut()
	move     &ref[474], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	move     &ref[446], R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  fun158()

;; TERM 508: '$TUPPLE'(35074870878528)
c_code local build_ref_508
	ret_reg &ref[508]
	call_c   Dyam_Create_Simple_Tupple(0,122888432)
	move_ret ref[508]
	c_ret

;; TERM 510: bmg_head_stacks(_O, _U, _V, _W, _X, _Y)
c_code local build_ref_510
	ret_reg &ref[510]
	call_c   build_ref_903()
	call_c   Dyam_Term_Start(&ref[903],6)
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_Arg(V(21))
	call_c   Dyam_Term_Arg(V(22))
	call_c   Dyam_Term_Arg(V(23))
	call_c   Dyam_Term_Arg(V(24))
	call_c   Dyam_Term_End()
	move_ret ref[510]
	c_ret

;; TERM 903: bmg_head_stacks
c_code local build_ref_903
	ret_reg &ref[903]
	call_c   Dyam_Create_Atom("bmg_head_stacks")
	move_ret ref[903]
	c_ret

long local pool_fun178[3]=[2,build_ref_509,build_ref_510]

pl_code local fun179
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(15),V(10))
	fail_ret
	call_c   Dyam_Unify(V(14),I(0))
	fail_ret
	call_c   Dyam_Unify(V(20),I(0))
	fail_ret
fun178:
	move     &ref[509], R(0)
	move     S(5), R(1)
	move     &ref[510], R(2)
	move     S(5), R(3)
	move     I(0), R(4)
	move     0, R(5)
	call_c   Dyam_Reg_Deallocate(3)
fun155:
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Reg_Unify(2,&ref[444])
	fail_ret
	call_c   Dyam_Reg_Bind(4,V(8))
	call_c   Dyam_Choice(fun154)
	pl_call  fun13(&seed[110],1)
	pl_fail



;; TERM 433: [_O,_P,_Q]
c_code local build_ref_433
	ret_reg &ref[433]
	call_c   Dyam_Create_Tupple(14,16,I(0))
	move_ret ref[433]
	c_ret

;; TERM 434: bmg_stack(_O)
c_code local build_ref_434
	ret_reg &ref[434]
	call_c   build_ref_904()
	call_c   Dyam_Create_Unary(&ref[904],V(14))
	move_ret ref[434]
	c_ret

;; TERM 904: bmg_stack
c_code local build_ref_904
	ret_reg &ref[904]
	call_c   Dyam_Create_Atom("bmg_stack")
	move_ret ref[904]
	c_ret

;; TERM 435: dcg _R / _S
c_code local build_ref_435
	ret_reg &ref[435]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_840()
	call_c   Dyam_Create_Binary(&ref[840],V(17),V(18))
	move_ret R(0)
	call_c   build_ref_839()
	call_c   Dyam_Create_Unary(&ref[839],R(0))
	move_ret ref[435]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun180[6]=[131075,build_ref_433,build_ref_434,build_ref_435,pool_fun178,pool_fun178]

pl_code local fun182
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(10),V(1))
	fail_ret
	call_c   Dyam_Unify(V(12),V(8))
	fail_ret
	call_c   Dyam_Unify(V(11),I(0))
	fail_ret
fun180:
	call_c   Dyam_Choice(fun179)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_univ(V(10),&ref[433])
	fail_ret
	pl_call  Object_1(&ref[434])
	call_c   Dyam_Cut()
	call_c   DYAM_evpred_functor(V(16),V(17),V(18))
	fail_ret
	move     &ref[435], R(0)
	move     S(5), R(1)
	move     V(19), R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Load(4,V(16))
	move     V(20), R(6)
	move     S(5), R(7)
	pl_call  pred_term_current_module_shift_4()
	pl_jump  fun178()


;; TERM 432: _K , _L
c_code local build_ref_432
	ret_reg &ref[432]
	call_c   Dyam_Create_Binary(I(4),V(10),V(11))
	move_ret ref[432]
	c_ret

pl_code local fun181
	call_c   Dyam_Remove_Choice()
	move     V(12), R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(4))
	move     V(13), R(4)
	move     S(5), R(5)
	pl_call  pred_my_numbervars_3()
	pl_jump  fun180()

long local pool_fun183[7]=[196611,build_ref_429,build_ref_431,build_ref_432,pool_fun180,pool_fun180,pool_fun180]

pl_code local fun183
	call_c   Dyam_Pool(pool_fun183)
	call_c   Dyam_Unify_Item(&ref[429])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[431], R(0)
	move     S(5), R(1)
	move     V(4), R(2)
	move     S(5), R(3)
	move     V(5), R(4)
	move     S(5), R(5)
	pl_call  pred_my_numbervars_3()
	move     V(6), R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(4))
	move     V(7), R(4)
	move     S(5), R(5)
	pl_call  pred_my_numbervars_3()
	move     V(8), R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(4))
	move     V(9), R(4)
	move     S(5), R(5)
	pl_call  pred_my_numbervars_3()
	call_c   Dyam_Choice(fun182)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[432])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun181)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(11),I(0))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(12),V(8))
	fail_ret
	pl_jump  fun180()

;; TERM 430: '*GUARD*'(pgm_to_lpda(grammar_rule(dcg, (_B --> _C)), _D)) :> []
c_code local build_ref_430
	ret_reg &ref[430]
	call_c   build_ref_429()
	call_c   Dyam_Create_Binary(I(9),&ref[429],I(0))
	move_ret ref[430]
	c_ret

c_code local build_seed_8
	ret_reg &seed[8]
	call_c   build_ref_36()
	call_c   build_ref_513()
	call_c   Dyam_Seed_Start(&ref[36],&ref[513],I(0),fun4,1)
	call_c   build_ref_514()
	call_c   Dyam_Seed_Add_Comp(&ref[514],fun187,0)
	call_c   Dyam_Seed_End()
	move_ret seed[8]
	c_ret

;; TERM 514: '*CITEM*'(wrapping_predicate(dcg_prolog((_B / _C))), _A)
c_code local build_ref_514
	ret_reg &ref[514]
	call_c   build_ref_39()
	call_c   build_ref_511()
	call_c   Dyam_Create_Binary(&ref[39],&ref[511],V(0))
	move_ret ref[514]
	c_ret

;; TERM 511: wrapping_predicate(dcg_prolog((_B / _C)))
c_code local build_ref_511
	ret_reg &ref[511]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_840()
	call_c   Dyam_Create_Binary(&ref[840],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_906()
	call_c   Dyam_Create_Unary(&ref[906],R(0))
	move_ret R(0)
	call_c   build_ref_905()
	call_c   Dyam_Create_Unary(&ref[905],R(0))
	move_ret ref[511]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 905: wrapping_predicate
c_code local build_ref_905
	ret_reg &ref[905]
	call_c   Dyam_Create_Atom("wrapping_predicate")
	move_ret ref[905]
	c_ret

;; TERM 906: dcg_prolog
c_code local build_ref_906
	ret_reg &ref[906]
	call_c   Dyam_Create_Atom("dcg_prolog")
	move_ret ref[906]
	c_ret

;; TERM 527: '$CLOSURE'('$fun'(186, 0, 1148361208), '$TUPPLE'(35074870618184))
c_code local build_ref_527
	ret_reg &ref[527]
	call_c   build_ref_526()
	call_c   Dyam_Closure_Aux(fun186,&ref[526])
	move_ret ref[527]
	c_ret

;; TERM 524: '$CLOSURE'('$fun'(185, 0, 1148354860), '$TUPPLE'(35074870618132))
c_code local build_ref_524
	ret_reg &ref[524]
	call_c   build_ref_523()
	call_c   Dyam_Closure_Aux(fun185,&ref[523])
	move_ret ref[524]
	c_ret

;; TERM 520: '$CLOSURE'('$fun'(184, 0, 1148346716), '$TUPPLE'(35074870618080))
c_code local build_ref_520
	ret_reg &ref[520]
	call_c   build_ref_519()
	call_c   Dyam_Closure_Aux(fun184,&ref[519])
	move_ret ref[520]
	c_ret

;; TERM 515: bmg_wrap_args(_F, _G, _H, _J, _D, _E, _K)
c_code local build_ref_515
	ret_reg &ref[515]
	call_c   build_ref_907()
	call_c   Dyam_Term_Start(&ref[907],7)
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_End()
	move_ret ref[515]
	c_ret

;; TERM 907: bmg_wrap_args
c_code local build_ref_907
	ret_reg &ref[907]
	call_c   Dyam_Create_Atom("bmg_wrap_args")
	move_ret ref[907]
	c_ret

;; TERM 516: [_L,_G,_H,_J,_D,_E,_F]
c_code local build_ref_516
	ret_reg &ref[516]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_663()
	call_c   Dyam_Create_List(V(9),&ref[663])
	move_ret R(0)
	call_c   Dyam_Create_Tupple(6,7,R(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(11),R(0))
	move_ret ref[516]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 663: [_D,_E,_F]
c_code local build_ref_663
	ret_reg &ref[663]
	call_c   Dyam_Create_Tupple(3,5,I(0))
	move_ret ref[663]
	c_ret

;; TERM 517: '*WRAPPER*'(dcg_prolog((_B / _C)), [_L|_K], '*PROLOG-LAST*'(_I, _J))
c_code local build_ref_517
	ret_reg &ref[517]
	call_c   Dyam_Pseudo_Choice(3)
	call_c   build_ref_840()
	call_c   Dyam_Create_Binary(&ref[840],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_906()
	call_c   Dyam_Create_Unary(&ref[906],R(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(11),V(10))
	move_ret R(1)
	call_c   build_ref_109()
	call_c   Dyam_Create_Binary(&ref[109],V(8),V(9))
	move_ret R(2)
	call_c   build_ref_908()
	call_c   Dyam_Term_Start(&ref[908],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(R(2))
	call_c   Dyam_Term_End()
	move_ret ref[517]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 908: '*WRAPPER*'
c_code local build_ref_908
	ret_reg &ref[908]
	call_c   Dyam_Create_Atom("*WRAPPER*")
	move_ret ref[908]
	c_ret

long local pool_fun184[5]=[4,build_ref_515,build_ref_516,build_ref_517,build_seed_54]

pl_code local fun184
	call_c   Dyam_Pool(pool_fun184)
	call_c   Dyam_Allocate(0)
	pl_call  Object_1(&ref[515])
	move     &ref[516], R(0)
	move     S(5), R(1)
	move     V(12), R(2)
	move     S(5), R(3)
	move     V(13), R(4)
	move     S(5), R(5)
	pl_call  pred_my_numbervars_3()
	call_c   DYAM_evpred_assert_1(&ref[517])
	call_c   Dyam_Deallocate()
	pl_jump  fun2(&seed[54],2)

;; TERM 519: '$TUPPLE'(35074870618080)
c_code local build_ref_519
	ret_reg &ref[519]
	call_c   Dyam_Create_Simple_Tupple(0,535822336)
	move_ret ref[519]
	c_ret

;; TERM 521: dcg_prolog_make((dcg _B / _C), _F, _G, _H, _D, _E, _I)
c_code local build_ref_521
	ret_reg &ref[521]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_840()
	call_c   Dyam_Create_Binary(&ref[840],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_839()
	call_c   Dyam_Create_Unary(&ref[839],R(0))
	move_ret R(0)
	call_c   build_ref_902()
	call_c   Dyam_Term_Start(&ref[902],7)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_End()
	move_ret ref[521]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun185[3]=[2,build_ref_520,build_ref_521]

pl_code local fun185
	call_c   Dyam_Pool(pool_fun185)
	call_c   DYAM_evpred_functor(V(5),V(1),V(2))
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[520], R(0)
	move     S(5), R(1)
	move     &ref[521], R(2)
	move     S(5), R(3)
	move     I(0), R(4)
	move     0, R(5)
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  fun161()

;; TERM 523: '$TUPPLE'(35074870618132)
c_code local build_ref_523
	ret_reg &ref[523]
	call_c   Dyam_Create_Simple_Tupple(0,520093696)
	move_ret ref[523]
	c_ret

long local pool_fun186[2]=[1,build_ref_524]

pl_code local fun186
	call_c   Dyam_Pool(pool_fun186)
	call_c   Dyam_Allocate(0)
	move     &ref[524], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	move     V(4), R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  fun23()

;; TERM 526: '$TUPPLE'(35074870618184)
c_code local build_ref_526
	ret_reg &ref[526]
	call_c   Dyam_Create_Simple_Tupple(0,503316480)
	move_ret ref[526]
	c_ret

long local pool_fun187[3]=[2,build_ref_514,build_ref_527]

pl_code local fun187
	call_c   Dyam_Pool(pool_fun187)
	call_c   Dyam_Unify_Item(&ref[514])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[527], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	move     V(3), R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  fun23()

;; TERM 513: '*FIRST*'(wrapping_predicate(dcg_prolog((_B / _C)))) :> []
c_code local build_ref_513
	ret_reg &ref[513]
	call_c   build_ref_512()
	call_c   Dyam_Create_Binary(I(9),&ref[512],I(0))
	move_ret ref[513]
	c_ret

;; TERM 512: '*FIRST*'(wrapping_predicate(dcg_prolog((_B / _C))))
c_code local build_ref_512
	ret_reg &ref[512]
	call_c   build_ref_36()
	call_c   build_ref_511()
	call_c   Dyam_Create_Unary(&ref[36],&ref[511])
	move_ret ref[512]
	c_ret

c_code local build_seed_23
	ret_reg &seed[23]
	call_c   build_ref_50()
	call_c   build_ref_530()
	call_c   Dyam_Seed_Start(&ref[50],&ref[530],I(0),fun5,1)
	call_c   build_ref_529()
	call_c   Dyam_Seed_Add_Comp(&ref[529],fun188,0)
	call_c   Dyam_Seed_End()
	move_ret seed[23]
	c_ret

;; TERM 529: '*GUARD*'(dcg_il_body_to_lpda_handler(_B, (_C , _D), _E, _F, _G, _H, _I, _J, _K, _L, _M))
c_code local build_ref_529
	ret_reg &ref[529]
	call_c   build_ref_51()
	call_c   build_ref_528()
	call_c   Dyam_Create_Unary(&ref[51],&ref[528])
	move_ret ref[529]
	c_ret

;; TERM 528: dcg_il_body_to_lpda_handler(_B, (_C , _D), _E, _F, _G, _H, _I, _J, _K, _L, _M)
c_code local build_ref_528
	ret_reg &ref[528]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Binary(I(4),V(2),V(3))
	move_ret R(0)
	call_c   build_ref_835()
	call_c   Dyam_Term_Start(&ref[835],11)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_End()
	move_ret ref[528]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 531: [_N,_O]
c_code local build_ref_531
	ret_reg &ref[531]
	call_c   Dyam_Create_Tupple(13,14,I(0))
	move_ret ref[531]
	c_ret

long local pool_fun188[4]=[3,build_ref_529,build_ref_531,build_ref_179]

pl_code local fun188
	call_c   Dyam_Pool(pool_fun188)
	call_c   Dyam_Unify_Item(&ref[529])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[531], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(15), R(4)
	move     S(5), R(5)
	pl_call  pred_my_numbervars_3()
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(3))
	call_c   Dyam_Reg_Load(4,V(13))
	call_c   Dyam_Reg_Load(6,V(14))
	call_c   Dyam_Reg_Load(8,V(6))
	call_c   Dyam_Reg_Load(10,V(7))
	call_c   Dyam_Reg_Load(12,V(8))
	move     &ref[179], R(14)
	move     0, R(15)
	move     V(16), R(16)
	move     S(5), R(17)
	call_c   Dyam_Reg_Load(18,V(11))
	call_c   Dyam_Reg_Load(20,V(12))
	pl_call  pred_dcg_il_body_to_lpda_11()
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(2))
	call_c   Dyam_Reg_Load(4,V(4))
	call_c   Dyam_Reg_Load(6,V(5))
	call_c   Dyam_Reg_Load(8,V(13))
	call_c   Dyam_Reg_Load(10,V(14))
	call_c   Dyam_Reg_Load(12,V(16))
	call_c   Dyam_Reg_Load(14,V(9))
	call_c   Dyam_Reg_Load(16,V(10))
	call_c   Dyam_Reg_Load(18,V(11))
	call_c   Dyam_Reg_Load(20,V(12))
	call_c   Dyam_Reg_Deallocate(11)
	pl_jump  pred_dcg_il_body_to_lpda_11()

;; TERM 530: '*GUARD*'(dcg_il_body_to_lpda_handler(_B, (_C , _D), _E, _F, _G, _H, _I, _J, _K, _L, _M)) :> []
c_code local build_ref_530
	ret_reg &ref[530]
	call_c   build_ref_529()
	call_c   Dyam_Create_Binary(I(9),&ref[529],I(0))
	move_ret ref[530]
	c_ret

c_code local build_seed_10
	ret_reg &seed[10]
	call_c   build_ref_36()
	call_c   build_ref_534()
	call_c   Dyam_Seed_Start(&ref[36],&ref[534],I(0),fun4,1)
	call_c   build_ref_535()
	call_c   Dyam_Seed_Add_Comp(&ref[535],fun193,0)
	call_c   Dyam_Seed_End()
	move_ret seed[10]
	c_ret

;; TERM 535: '*CITEM*'(wrapping_predicate((dcg _B / _C)), _A)
c_code local build_ref_535
	ret_reg &ref[535]
	call_c   build_ref_39()
	call_c   build_ref_532()
	call_c   Dyam_Create_Binary(&ref[39],&ref[532],V(0))
	move_ret ref[535]
	c_ret

;; TERM 532: wrapping_predicate((dcg _B / _C))
c_code local build_ref_532
	ret_reg &ref[532]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_840()
	call_c   Dyam_Create_Binary(&ref[840],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_839()
	call_c   Dyam_Create_Unary(&ref[839],R(0))
	move_ret R(0)
	call_c   build_ref_905()
	call_c   Dyam_Create_Unary(&ref[905],R(0))
	move_ret ref[532]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 548: '$CLOSURE'('$fun'(192, 0, 1148418804), '$TUPPLE'(35074870618184))
c_code local build_ref_548
	ret_reg &ref[548]
	call_c   build_ref_526()
	call_c   Dyam_Closure_Aux(fun192,&ref[526])
	move_ret ref[548]
	c_ret

;; TERM 546: '$CLOSURE'('$fun'(191, 0, 1148407228), '$TUPPLE'(35074870618132))
c_code local build_ref_546
	ret_reg &ref[546]
	call_c   build_ref_523()
	call_c   Dyam_Closure_Aux(fun191,&ref[523])
	move_ret ref[546]
	c_ret

c_code local build_seed_122
	ret_reg &seed[122]
	call_c   build_ref_51()
	call_c   build_ref_537()
	call_c   Dyam_Seed_Start(&ref[51],&ref[537],I(0),fun0,1)
	call_c   build_ref_538()
	call_c   Dyam_Seed_Add_Comp(&ref[538],&ref[537],0)
	call_c   Dyam_Seed_End()
	move_ret seed[122]
	c_ret

;; TERM 538: '*GUARD*'(make_dcg_callret(_F, _G, _H, _D, _E, _I, _J)) :> '$$HOLE$$'
c_code local build_ref_538
	ret_reg &ref[538]
	call_c   build_ref_537()
	call_c   Dyam_Create_Binary(I(9),&ref[537],I(7))
	move_ret ref[538]
	c_ret

;; TERM 537: '*GUARD*'(make_dcg_callret(_F, _G, _H, _D, _E, _I, _J))
c_code local build_ref_537
	ret_reg &ref[537]
	call_c   build_ref_51()
	call_c   build_ref_536()
	call_c   Dyam_Create_Unary(&ref[51],&ref[536])
	move_ret ref[537]
	c_ret

;; TERM 536: make_dcg_callret(_F, _G, _H, _D, _E, _I, _J)
c_code local build_ref_536
	ret_reg &ref[536]
	call_c   build_ref_895()
	call_c   Dyam_Term_Start(&ref[895],7)
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret ref[536]
	c_ret

;; TERM 539: bmg_wrap_args(_F, _G, _H, _K, _D, _E, _L)
c_code local build_ref_539
	ret_reg &ref[539]
	call_c   build_ref_907()
	call_c   Dyam_Term_Start(&ref[907],7)
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_End()
	move_ret ref[539]
	c_ret

;; TERM 540: [_M,_G,_H,_K,_D,_E,_F]
c_code local build_ref_540
	ret_reg &ref[540]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_663()
	call_c   Dyam_Create_List(V(10),&ref[663])
	move_ret R(0)
	call_c   Dyam_Create_Tupple(6,7,R(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(12),R(0))
	move_ret ref[540]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 544: '*NEXT*'{nabla=> _K, call=> _I, ret=> _J, right=> '*PROLOG-LAST*'}
c_code local build_ref_544
	ret_reg &ref[544]
	call_c   build_ref_909()
	call_c   build_ref_109()
	call_c   Dyam_Term_Start(&ref[909],4)
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(&ref[109])
	call_c   Dyam_Term_End()
	move_ret ref[544]
	c_ret

;; TERM 909: '*NEXT*'!'$ft'
c_code local build_ref_909
	ret_reg &ref[909]
	call_c   build_ref_910()
	call_c   Dyam_Create_Atom_Module("$ft",&ref[910])
	move_ret ref[909]
	c_ret

;; TERM 910: '*NEXT*'
c_code local build_ref_910
	ret_reg &ref[910]
	call_c   Dyam_Create_Atom("*NEXT*")
	move_ret ref[910]
	c_ret

;; TERM 543: '*WRAPPER*'((dcg _B / _C), [_M|_L], _P)
c_code local build_ref_543
	ret_reg &ref[543]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_840()
	call_c   Dyam_Create_Binary(&ref[840],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_839()
	call_c   Dyam_Create_Unary(&ref[839],R(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(12),V(11))
	move_ret R(1)
	call_c   build_ref_908()
	call_c   Dyam_Term_Start(&ref[908],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_End()
	move_ret ref[543]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun189[3]=[2,build_ref_543,build_seed_54]

long local pool_fun190[3]=[65537,build_ref_544,pool_fun189]

pl_code local fun190
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(15),&ref[544])
	fail_ret
fun189:
	call_c   DYAM_evpred_assert_1(&ref[543])
	call_c   Dyam_Deallocate()
	pl_jump  fun2(&seed[54],2)


;; TERM 541: light_tabular dcg _B / _C
c_code local build_ref_541
	ret_reg &ref[541]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_840()
	call_c   Dyam_Create_Binary(&ref[840],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_839()
	call_c   Dyam_Create_Unary(&ref[839],R(0))
	move_ret R(0)
	call_c   build_ref_899()
	call_c   Dyam_Create_Unary(&ref[899],R(0))
	move_ret ref[541]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 542: '*LIGHTNEXTALT*'(_I, _J, '*PROLOG-LAST*', _K)
c_code local build_ref_542
	ret_reg &ref[542]
	call_c   build_ref_911()
	call_c   build_ref_109()
	call_c   Dyam_Term_Start(&ref[911],4)
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(&ref[109])
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_End()
	move_ret ref[542]
	c_ret

;; TERM 911: '*LIGHTNEXTALT*'
c_code local build_ref_911
	ret_reg &ref[911]
	call_c   Dyam_Create_Atom("*LIGHTNEXTALT*")
	move_ret ref[911]
	c_ret

long local pool_fun191[8]=[131077,build_seed_122,build_ref_539,build_ref_540,build_ref_541,build_ref_542,pool_fun190,pool_fun189]

pl_code local fun191
	call_c   Dyam_Pool(pool_fun191)
	call_c   DYAM_evpred_functor(V(5),V(1),V(2))
	fail_ret
	call_c   Dyam_Allocate(0)
	pl_call  fun7(&seed[122],1)
	pl_call  Object_1(&ref[539])
	move     &ref[540], R(0)
	move     S(5), R(1)
	move     V(13), R(2)
	move     S(5), R(3)
	move     V(14), R(4)
	move     S(5), R(5)
	pl_call  pred_my_numbervars_3()
	call_c   Dyam_Choice(fun190)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[541])
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(15),&ref[542])
	fail_ret
	pl_jump  fun189()

long local pool_fun192[2]=[1,build_ref_546]

pl_code local fun192
	call_c   Dyam_Pool(pool_fun192)
	call_c   Dyam_Allocate(0)
	move     &ref[546], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	move     V(4), R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  fun23()

long local pool_fun193[3]=[2,build_ref_535,build_ref_548]

pl_code local fun193
	call_c   Dyam_Pool(pool_fun193)
	call_c   Dyam_Unify_Item(&ref[535])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[548], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	move     V(3), R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  fun23()

;; TERM 534: '*FIRST*'(wrapping_predicate((dcg _B / _C))) :> []
c_code local build_ref_534
	ret_reg &ref[534]
	call_c   build_ref_533()
	call_c   Dyam_Create_Binary(I(9),&ref[533],I(0))
	move_ret ref[534]
	c_ret

;; TERM 533: '*FIRST*'(wrapping_predicate((dcg _B / _C)))
c_code local build_ref_533
	ret_reg &ref[533]
	call_c   build_ref_36()
	call_c   build_ref_532()
	call_c   Dyam_Create_Unary(&ref[36],&ref[532])
	move_ret ref[533]
	c_ret

c_code local build_seed_7
	ret_reg &seed[7]
	call_c   build_ref_50()
	call_c   build_ref_551()
	call_c   Dyam_Seed_Start(&ref[50],&ref[551],I(0),fun5,1)
	call_c   build_ref_550()
	call_c   Dyam_Seed_Add_Comp(&ref[550],fun197,0)
	call_c   Dyam_Seed_End()
	move_ret seed[7]
	c_ret

;; TERM 550: '*GUARD*'(dcg_trail_to_lpda(_B, _C, _D, _E, _F, _G, _H))
c_code local build_ref_550
	ret_reg &ref[550]
	call_c   build_ref_51()
	call_c   build_ref_549()
	call_c   Dyam_Create_Unary(&ref[51],&ref[549])
	move_ret ref[550]
	c_ret

;; TERM 549: dcg_trail_to_lpda(_B, _C, _D, _E, _F, _G, _H)
c_code local build_ref_549
	ret_reg &ref[549]
	call_c   build_ref_897()
	call_c   Dyam_Term_Start(&ref[897],7)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[549]
	c_ret

;; TERM 552: parse_mode(_I)
c_code local build_ref_552
	ret_reg &ref[552]
	call_c   build_ref_881()
	call_c   Dyam_Create_Unary(&ref[881],V(8))
	move_ret ref[552]
	c_ret

;; TERM 566: 'Not a valid parse mode ~w'
c_code local build_ref_566
	ret_reg &ref[566]
	call_c   Dyam_Create_Atom("Not a valid parse mode ~w")
	move_ret ref[566]
	c_ret

long local pool_fun194[2]=[1,build_ref_566]

pl_code local fun194
	call_c   Dyam_Remove_Choice()
	move     &ref[566], R(0)
	move     0, R(1)
	call_c   Dyam_Reg_Load(2,V(8))
	pl_call  pred_error_2()
	pl_jump  fun132()

;; TERM 562: unify(_D, _M) :> _F
c_code local build_ref_562
	ret_reg &ref[562]
	call_c   build_ref_561()
	call_c   Dyam_Create_Binary(I(9),&ref[561],V(5))
	move_ret ref[562]
	c_ret

;; TERM 561: unify(_D, _M)
c_code local build_ref_561
	ret_reg &ref[561]
	call_c   build_ref_836()
	call_c   Dyam_Create_Binary(&ref[836],V(3),V(12))
	move_ret ref[561]
	c_ret

c_code local build_seed_125
	ret_reg &seed[125]
	call_c   build_ref_51()
	call_c   build_ref_564()
	call_c   Dyam_Seed_Start(&ref[51],&ref[564],I(0),fun0,1)
	call_c   build_ref_565()
	call_c   Dyam_Seed_Add_Comp(&ref[565],&ref[564],0)
	call_c   Dyam_Seed_End()
	move_ret seed[125]
	c_ret

;; TERM 565: '*GUARD*'(append(_C, _E, _M)) :> '$$HOLE$$'
c_code local build_ref_565
	ret_reg &ref[565]
	call_c   build_ref_564()
	call_c   Dyam_Create_Binary(I(9),&ref[564],I(7))
	move_ret ref[565]
	c_ret

;; TERM 564: '*GUARD*'(append(_C, _E, _M))
c_code local build_ref_564
	ret_reg &ref[564]
	call_c   build_ref_51()
	call_c   build_ref_563()
	call_c   Dyam_Create_Unary(&ref[51],&ref[563])
	move_ret ref[564]
	c_ret

;; TERM 563: append(_C, _E, _M)
c_code local build_ref_563
	ret_reg &ref[563]
	call_c   build_ref_866()
	call_c   Dyam_Term_Start(&ref[866],3)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_End()
	move_ret ref[563]
	c_ret

long local pool_fun195[5]=[65539,build_ref_560,build_ref_562,build_seed_125,pool_fun194]

pl_code local fun195
	call_c   Dyam_Update_Choice(fun194)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(8),&ref[560])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(6),&ref[562])
	fail_ret
	pl_call  fun7(&seed[125],1)
	pl_jump  fun132()

c_code local build_seed_123
	ret_reg &seed[123]
	call_c   build_ref_51()
	call_c   build_ref_555()
	call_c   Dyam_Seed_Start(&ref[51],&ref[555],I(0),fun0,1)
	call_c   build_ref_556()
	call_c   Dyam_Seed_Add_Comp(&ref[556],&ref[555],0)
	call_c   Dyam_Seed_End()
	move_ret seed[123]
	c_ret

;; TERM 556: '*GUARD*'(token_trail_gen(_C, _D, _E, _L, _B)) :> '$$HOLE$$'
c_code local build_ref_556
	ret_reg &ref[556]
	call_c   build_ref_555()
	call_c   Dyam_Create_Binary(I(9),&ref[555],I(7))
	move_ret ref[556]
	c_ret

;; TERM 555: '*GUARD*'(token_trail_gen(_C, _D, _E, _L, _B))
c_code local build_ref_555
	ret_reg &ref[555]
	call_c   build_ref_51()
	call_c   build_ref_554()
	call_c   Dyam_Create_Unary(&ref[51],&ref[554])
	move_ret ref[555]
	c_ret

;; TERM 554: token_trail_gen(_C, _D, _E, _L, _B)
c_code local build_ref_554
	ret_reg &ref[554]
	call_c   build_ref_830()
	call_c   Dyam_Term_Start(&ref[830],5)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_End()
	move_ret ref[554]
	c_ret

c_code local build_seed_124
	ret_reg &seed[124]
	call_c   build_ref_51()
	call_c   build_ref_558()
	call_c   Dyam_Seed_Start(&ref[51],&ref[558],I(0),fun0,1)
	call_c   build_ref_559()
	call_c   Dyam_Seed_Add_Comp(&ref[559],&ref[558],0)
	call_c   Dyam_Seed_End()
	move_ret seed[124]
	c_ret

;; TERM 559: '*GUARD*'(body_to_lpda(_B, (gensym(_J) , _D = added(_J) , _L), _F, _G, _H)) :> '$$HOLE$$'
c_code local build_ref_559
	ret_reg &ref[559]
	call_c   build_ref_558()
	call_c   Dyam_Create_Binary(I(9),&ref[558],I(7))
	move_ret ref[559]
	c_ret

;; TERM 558: '*GUARD*'(body_to_lpda(_B, (gensym(_J) , _D = added(_J) , _L), _F, _G, _H))
c_code local build_ref_558
	ret_reg &ref[558]
	call_c   build_ref_51()
	call_c   build_ref_557()
	call_c   Dyam_Create_Unary(&ref[51],&ref[557])
	move_ret ref[558]
	c_ret

;; TERM 557: body_to_lpda(_B, (gensym(_J) , _D = added(_J) , _L), _F, _G, _H)
c_code local build_ref_557
	ret_reg &ref[557]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_876()
	call_c   Dyam_Create_Unary(&ref[876],V(9))
	move_ret R(0)
	call_c   build_ref_877()
	call_c   Dyam_Create_Unary(&ref[877],V(9))
	move_ret R(1)
	call_c   build_ref_869()
	call_c   Dyam_Create_Binary(&ref[869],V(3),R(1))
	move_ret R(1)
	call_c   Dyam_Create_Binary(I(4),R(1),V(11))
	move_ret R(1)
	call_c   Dyam_Create_Binary(I(4),R(0),R(1))
	move_ret R(1)
	call_c   build_ref_875()
	call_c   Dyam_Term_Start(&ref[875],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[557]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun196[5]=[65539,build_ref_553,build_seed_123,build_seed_124,pool_fun195]

pl_code local fun196
	call_c   Dyam_Update_Choice(fun195)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(8),&ref[553])
	fail_ret
	call_c   Dyam_Cut()
	move     V(9), R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(10), R(4)
	move     S(5), R(5)
	pl_call  pred_my_numbervars_3()
	pl_call  fun7(&seed[123],1)
	pl_call  fun7(&seed[124],1)
	pl_jump  fun132()

long local pool_fun197[4]=[65538,build_ref_550,build_ref_552,pool_fun196]

pl_code local fun197
	call_c   Dyam_Pool(pool_fun197)
	call_c   Dyam_Unify_Item(&ref[550])
	fail_ret
	call_c   Dyam_Allocate(0)
	pl_call  Object_1(&ref[552])
	call_c   Dyam_Choice(fun196)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(2),I(0))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(6),V(5))
	fail_ret
	pl_jump  fun132()

;; TERM 551: '*GUARD*'(dcg_trail_to_lpda(_B, _C, _D, _E, _F, _G, _H)) :> []
c_code local build_ref_551
	ret_reg &ref[551]
	call_c   build_ref_550()
	call_c   Dyam_Create_Binary(I(9),&ref[550],I(0))
	move_ret ref[551]
	c_ret

c_code local build_seed_17
	ret_reg &seed[17]
	call_c   build_ref_50()
	call_c   build_ref_569()
	call_c   Dyam_Seed_Start(&ref[50],&ref[569],I(0),fun5,1)
	call_c   build_ref_568()
	call_c   Dyam_Seed_Add_Comp(&ref[568],fun203,0)
	call_c   Dyam_Seed_End()
	move_ret seed[17]
	c_ret

;; TERM 568: '*GUARD*'(scan_to_lpda(_B, _C, _D, _E, _F, _G, _H))
c_code local build_ref_568
	ret_reg &ref[568]
	call_c   build_ref_51()
	call_c   build_ref_567()
	call_c   Dyam_Create_Unary(&ref[51],&ref[567])
	move_ret ref[568]
	c_ret

;; TERM 567: scan_to_lpda(_B, _C, _D, _E, _F, _G, _H)
c_code local build_ref_567
	ret_reg &ref[567]
	call_c   build_ref_865()
	call_c   Dyam_Term_Start(&ref[865],7)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[567]
	c_ret

;; TERM 570: parse_mode(_J)
c_code local build_ref_570
	ret_reg &ref[570]
	call_c   build_ref_881()
	call_c   Dyam_Create_Unary(&ref[881],V(9))
	move_ret ref[570]
	c_ret

long local pool_fun201[2]=[1,build_ref_566]

pl_code local fun201
	call_c   Dyam_Remove_Choice()
	move     &ref[566], R(0)
	move     0, R(1)
	call_c   Dyam_Reg_Load(2,V(9))
	pl_call  pred_error_2()
	pl_jump  fun132()

;; TERM 579: unify(_D, _L) :> _F
c_code local build_ref_579
	ret_reg &ref[579]
	call_c   build_ref_578()
	call_c   Dyam_Create_Binary(I(9),&ref[578],V(5))
	move_ret ref[579]
	c_ret

;; TERM 578: unify(_D, _L)
c_code local build_ref_578
	ret_reg &ref[578]
	call_c   build_ref_836()
	call_c   Dyam_Create_Binary(&ref[836],V(3),V(11))
	move_ret ref[578]
	c_ret

c_code local build_seed_128
	ret_reg &seed[128]
	call_c   build_ref_51()
	call_c   build_ref_581()
	call_c   Dyam_Seed_Start(&ref[51],&ref[581],I(0),fun0,1)
	call_c   build_ref_582()
	call_c   Dyam_Seed_Add_Comp(&ref[582],&ref[581],0)
	call_c   Dyam_Seed_End()
	move_ret seed[128]
	c_ret

;; TERM 582: '*GUARD*'(append(_K, _E, _L)) :> '$$HOLE$$'
c_code local build_ref_582
	ret_reg &ref[582]
	call_c   build_ref_581()
	call_c   Dyam_Create_Binary(I(9),&ref[581],I(7))
	move_ret ref[582]
	c_ret

;; TERM 581: '*GUARD*'(append(_K, _E, _L))
c_code local build_ref_581
	ret_reg &ref[581]
	call_c   build_ref_51()
	call_c   build_ref_580()
	call_c   Dyam_Create_Unary(&ref[51],&ref[580])
	move_ret ref[581]
	c_ret

;; TERM 580: append(_K, _E, _L)
c_code local build_ref_580
	ret_reg &ref[580]
	call_c   build_ref_866()
	call_c   Dyam_Term_Start(&ref[866],3)
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_End()
	move_ret ref[580]
	c_ret

long local pool_fun199[2]=[1,build_seed_128]

pl_code local fun200
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(2),V(10))
	fail_ret
fun199:
	pl_call  fun7(&seed[128],1)
	pl_jump  fun132()


;; TERM 571: '$noskip'(_K)
c_code local build_ref_571
	ret_reg &ref[571]
	call_c   build_ref_870()
	call_c   Dyam_Create_Unary(&ref[870],V(10))
	move_ret ref[571]
	c_ret

long local pool_fun202[7]=[196611,build_ref_560,build_ref_579,build_ref_571,pool_fun201,pool_fun199,pool_fun199]

pl_code local fun202
	call_c   Dyam_Update_Choice(fun201)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(9),&ref[560])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(6),&ref[579])
	fail_ret
	call_c   Dyam_Choice(fun200)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(2),&ref[571])
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun199()

c_code local build_seed_127
	ret_reg &seed[127]
	call_c   build_ref_51()
	call_c   build_ref_576()
	call_c   Dyam_Seed_Start(&ref[51],&ref[576],I(0),fun0,1)
	call_c   build_ref_577()
	call_c   Dyam_Seed_Add_Comp(&ref[577],&ref[576],0)
	call_c   Dyam_Seed_End()
	move_ret seed[127]
	c_ret

;; TERM 577: '*GUARD*'(token_scan_to_lpda(_B, _C, _D, _E, _F, _G, _H, skip)) :> '$$HOLE$$'
c_code local build_ref_577
	ret_reg &ref[577]
	call_c   build_ref_576()
	call_c   Dyam_Create_Binary(I(9),&ref[576],I(7))
	move_ret ref[577]
	c_ret

;; TERM 576: '*GUARD*'(token_scan_to_lpda(_B, _C, _D, _E, _F, _G, _H, skip))
c_code local build_ref_576
	ret_reg &ref[576]
	call_c   build_ref_51()
	call_c   build_ref_575()
	call_c   Dyam_Create_Unary(&ref[51],&ref[575])
	move_ret ref[576]
	c_ret

;; TERM 575: token_scan_to_lpda(_B, _C, _D, _E, _F, _G, _H, skip)
c_code local build_ref_575
	ret_reg &ref[575]
	call_c   build_ref_878()
	call_c   build_ref_389()
	call_c   Dyam_Term_Start(&ref[878],8)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(&ref[389])
	call_c   Dyam_Term_End()
	move_ret ref[575]
	c_ret

long local pool_fun198[2]=[1,build_seed_127]

pl_code local fun198
	call_c   Dyam_Remove_Choice()
	pl_call  fun7(&seed[127],1)
	pl_jump  fun132()

c_code local build_seed_126
	ret_reg &seed[126]
	call_c   build_ref_51()
	call_c   build_ref_573()
	call_c   Dyam_Seed_Start(&ref[51],&ref[573],I(0),fun0,1)
	call_c   build_ref_574()
	call_c   Dyam_Seed_Add_Comp(&ref[574],&ref[573],0)
	call_c   Dyam_Seed_End()
	move_ret seed[126]
	c_ret

;; TERM 574: '*GUARD*'(token_scan_to_lpda(_B, _K, _D, _E, _F, _G, _H, noksip)) :> '$$HOLE$$'
c_code local build_ref_574
	ret_reg &ref[574]
	call_c   build_ref_573()
	call_c   Dyam_Create_Binary(I(9),&ref[573],I(7))
	move_ret ref[574]
	c_ret

;; TERM 573: '*GUARD*'(token_scan_to_lpda(_B, _K, _D, _E, _F, _G, _H, noksip))
c_code local build_ref_573
	ret_reg &ref[573]
	call_c   build_ref_51()
	call_c   build_ref_572()
	call_c   Dyam_Create_Unary(&ref[51],&ref[572])
	move_ret ref[573]
	c_ret

;; TERM 572: token_scan_to_lpda(_B, _K, _D, _E, _F, _G, _H, noksip)
c_code local build_ref_572
	ret_reg &ref[572]
	call_c   build_ref_878()
	call_c   build_ref_912()
	call_c   Dyam_Term_Start(&ref[878],8)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(&ref[912])
	call_c   Dyam_Term_End()
	move_ret ref[572]
	c_ret

;; TERM 912: noksip
c_code local build_ref_912
	ret_reg &ref[912]
	call_c   Dyam_Create_Atom("noksip")
	move_ret ref[912]
	c_ret

long local pool_fun203[8]=[131077,build_ref_568,build_ref_570,build_ref_553,build_ref_571,build_seed_126,pool_fun202,pool_fun198]

pl_code local fun203
	call_c   Dyam_Pool(pool_fun203)
	call_c   Dyam_Unify_Item(&ref[568])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(4))
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(8), R(4)
	move     S(5), R(5)
	pl_call  pred_my_numbervars_3()
	pl_call  Object_1(&ref[570])
	call_c   Dyam_Choice(fun202)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(9),&ref[553])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun198)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(2),&ref[571])
	fail_ret
	call_c   Dyam_Cut()
	pl_call  fun7(&seed[126],1)
	pl_jump  fun132()

;; TERM 569: '*GUARD*'(scan_to_lpda(_B, _C, _D, _E, _F, _G, _H)) :> []
c_code local build_ref_569
	ret_reg &ref[569]
	call_c   build_ref_568()
	call_c   Dyam_Create_Binary(I(9),&ref[568],I(0))
	move_ret ref[569]
	c_ret

c_code local build_seed_13
	ret_reg &seed[13]
	call_c   build_ref_50()
	call_c   build_ref_585()
	call_c   Dyam_Seed_Start(&ref[50],&ref[585],I(0),fun5,1)
	call_c   build_ref_584()
	call_c   Dyam_Seed_Add_Comp(&ref[584],fun205,0)
	call_c   Dyam_Seed_End()
	move_ret seed[13]
	c_ret

;; TERM 584: '*GUARD*'(token_scan_to_lpda(_B, [_C|_D], _E, _F, _G, _H, _I, _J))
c_code local build_ref_584
	ret_reg &ref[584]
	call_c   build_ref_51()
	call_c   build_ref_583()
	call_c   Dyam_Create_Unary(&ref[51],&ref[583])
	move_ret ref[584]
	c_ret

;; TERM 583: token_scan_to_lpda(_B, [_C|_D], _E, _F, _G, _H, _I, _J)
c_code local build_ref_583
	ret_reg &ref[583]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(2),V(3))
	move_ret R(0)
	call_c   build_ref_878()
	call_c   Dyam_Term_Start(&ref[878],8)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret ref[583]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 586: [_K|_L]
c_code local build_ref_586
	ret_reg &ref[586]
	call_c   Dyam_Create_List(V(10),V(11))
	move_ret ref[586]
	c_ret

c_code local build_seed_129
	ret_reg &seed[129]
	call_c   build_ref_51()
	call_c   build_ref_588()
	call_c   Dyam_Seed_Start(&ref[51],&ref[588],I(0),fun0,1)
	call_c   build_ref_589()
	call_c   Dyam_Seed_Add_Comp(&ref[589],&ref[588],0)
	call_c   Dyam_Seed_End()
	move_ret seed[129]
	c_ret

;; TERM 589: '*GUARD*'(token_scan_to_lpda(_B, _D, _M, _F, _G, _O, _I, _J)) :> '$$HOLE$$'
c_code local build_ref_589
	ret_reg &ref[589]
	call_c   build_ref_588()
	call_c   Dyam_Create_Binary(I(9),&ref[588],I(7))
	move_ret ref[589]
	c_ret

;; TERM 588: '*GUARD*'(token_scan_to_lpda(_B, _D, _M, _F, _G, _O, _I, _J))
c_code local build_ref_588
	ret_reg &ref[588]
	call_c   build_ref_51()
	call_c   build_ref_587()
	call_c   Dyam_Create_Unary(&ref[51],&ref[587])
	move_ret ref[588]
	c_ret

;; TERM 587: token_scan_to_lpda(_B, _D, _M, _F, _G, _O, _I, _J)
c_code local build_ref_587
	ret_reg &ref[587]
	call_c   build_ref_878()
	call_c   Dyam_Term_Start(&ref[878],8)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret ref[587]
	c_ret

c_code local build_seed_132
	ret_reg &seed[132]
	call_c   build_ref_51()
	call_c   build_ref_598()
	call_c   Dyam_Seed_Start(&ref[51],&ref[598],I(0),fun0,1)
	call_c   build_ref_599()
	call_c   Dyam_Seed_Add_Comp(&ref[599],&ref[598],0)
	call_c   Dyam_Seed_End()
	move_ret seed[132]
	c_ret

;; TERM 599: '*GUARD*'(generalized_scanner(_B, _E, _C, _M, _U, _J)) :> '$$HOLE$$'
c_code local build_ref_599
	ret_reg &ref[599]
	call_c   build_ref_598()
	call_c   Dyam_Create_Binary(I(9),&ref[598],I(7))
	move_ret ref[599]
	c_ret

;; TERM 598: '*GUARD*'(generalized_scanner(_B, _E, _C, _M, _U, _J))
c_code local build_ref_598
	ret_reg &ref[598]
	call_c   build_ref_51()
	call_c   build_ref_597()
	call_c   Dyam_Create_Unary(&ref[51],&ref[597])
	move_ret ref[598]
	c_ret

;; TERM 597: generalized_scanner(_B, _E, _C, _M, _U, _J)
c_code local build_ref_597
	ret_reg &ref[597]
	call_c   build_ref_832()
	call_c   Dyam_Term_Start(&ref[832],6)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret ref[597]
	c_ret

c_code local build_seed_133
	ret_reg &seed[133]
	call_c   build_ref_51()
	call_c   build_ref_601()
	call_c   Dyam_Seed_Start(&ref[51],&ref[601],I(0),fun0,1)
	call_c   build_ref_602()
	call_c   Dyam_Seed_Add_Comp(&ref[602],&ref[601],0)
	call_c   Dyam_Seed_End()
	move_ret seed[133]
	c_ret

;; TERM 602: '*GUARD*'(body_to_lpda(_B, _U, _O, _H, _I)) :> '$$HOLE$$'
c_code local build_ref_602
	ret_reg &ref[602]
	call_c   build_ref_601()
	call_c   Dyam_Create_Binary(I(9),&ref[601],I(7))
	move_ret ref[602]
	c_ret

;; TERM 601: '*GUARD*'(body_to_lpda(_B, _U, _O, _H, _I))
c_code local build_ref_601
	ret_reg &ref[601]
	call_c   build_ref_51()
	call_c   build_ref_600()
	call_c   Dyam_Create_Unary(&ref[51],&ref[600])
	move_ret ref[601]
	c_ret

;; TERM 600: body_to_lpda(_B, _U, _O, _H, _I)
c_code local build_ref_600
	ret_reg &ref[600]
	call_c   build_ref_875()
	call_c   Dyam_Term_Start(&ref[875],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_End()
	move_ret ref[600]
	c_ret

long local pool_fun204[3]=[2,build_seed_132,build_seed_133]

pl_code local fun204
	call_c   Dyam_Remove_Choice()
	pl_call  fun7(&seed[132],1)
	pl_call  fun7(&seed[133],1)
	pl_jump  fun132()

;; TERM 590: _P ; _Q
c_code local build_ref_590
	ret_reg &ref[590]
	call_c   Dyam_Create_Binary(I(5),V(15),V(16))
	move_ret ref[590]
	c_ret

c_code local build_seed_130
	ret_reg &seed[130]
	call_c   build_ref_51()
	call_c   build_ref_592()
	call_c   Dyam_Seed_Start(&ref[51],&ref[592],I(0),fun0,1)
	call_c   build_ref_593()
	call_c   Dyam_Seed_Add_Comp(&ref[593],&ref[592],0)
	call_c   Dyam_Seed_End()
	move_ret seed[130]
	c_ret

;; TERM 593: '*GUARD*'(token_scan_to_lpda(_B, _P, _E, _M, _R, _S, _I, _J)) :> '$$HOLE$$'
c_code local build_ref_593
	ret_reg &ref[593]
	call_c   build_ref_592()
	call_c   Dyam_Create_Binary(I(9),&ref[592],I(7))
	move_ret ref[593]
	c_ret

;; TERM 592: '*GUARD*'(token_scan_to_lpda(_B, _P, _E, _M, _R, _S, _I, _J))
c_code local build_ref_592
	ret_reg &ref[592]
	call_c   build_ref_51()
	call_c   build_ref_591()
	call_c   Dyam_Create_Unary(&ref[51],&ref[591])
	move_ret ref[592]
	c_ret

;; TERM 591: token_scan_to_lpda(_B, _P, _E, _M, _R, _S, _I, _J)
c_code local build_ref_591
	ret_reg &ref[591]
	call_c   build_ref_878()
	call_c   Dyam_Term_Start(&ref[878],8)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret ref[591]
	c_ret

c_code local build_seed_131
	ret_reg &seed[131]
	call_c   build_ref_51()
	call_c   build_ref_595()
	call_c   Dyam_Seed_Start(&ref[51],&ref[595],I(0),fun0,1)
	call_c   build_ref_596()
	call_c   Dyam_Seed_Add_Comp(&ref[596],&ref[595],0)
	call_c   Dyam_Seed_End()
	move_ret seed[131]
	c_ret

;; TERM 596: '*GUARD*'(token_scan_to_lpda(_B, _Q, _E, _M, _R, _T, _I, _J)) :> '$$HOLE$$'
c_code local build_ref_596
	ret_reg &ref[596]
	call_c   build_ref_595()
	call_c   Dyam_Create_Binary(I(9),&ref[595],I(7))
	move_ret ref[596]
	c_ret

;; TERM 595: '*GUARD*'(token_scan_to_lpda(_B, _Q, _E, _M, _R, _T, _I, _J))
c_code local build_ref_595
	ret_reg &ref[595]
	call_c   build_ref_51()
	call_c   build_ref_594()
	call_c   Dyam_Create_Unary(&ref[51],&ref[594])
	move_ret ref[595]
	c_ret

;; TERM 594: token_scan_to_lpda(_B, _Q, _E, _M, _R, _T, _I, _J)
c_code local build_ref_594
	ret_reg &ref[594]
	call_c   build_ref_878()
	call_c   Dyam_Term_Start(&ref[878],8)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret ref[594]
	c_ret

long local pool_fun205[8]=[65542,build_ref_584,build_ref_586,build_seed_129,build_ref_590,build_seed_130,build_seed_131,pool_fun204]

pl_code local fun205
	call_c   Dyam_Pool(pool_fun205)
	call_c   Dyam_Unify_Item(&ref[584])
	fail_ret
	call_c   Dyam_Unify(V(3),&ref[586])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     V(12), R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(13), R(4)
	move     S(5), R(5)
	pl_call  pred_my_numbervars_3()
	pl_call  fun7(&seed[129],1)
	call_c   Dyam_Choice(fun204)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(2),&ref[590])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(14))
	call_c   Dyam_Reg_Load(2,V(7))
	move     V(17), R(4)
	move     S(5), R(5)
	move     V(18), R(6)
	move     S(5), R(7)
	move     V(19), R(8)
	move     S(5), R(9)
	pl_call  pred_handle_choice_5()
	pl_call  fun7(&seed[130],1)
	pl_call  fun7(&seed[131],1)
	pl_jump  fun132()

;; TERM 585: '*GUARD*'(token_scan_to_lpda(_B, [_C|_D], _E, _F, _G, _H, _I, _J)) :> []
c_code local build_ref_585
	ret_reg &ref[585]
	call_c   build_ref_584()
	call_c   Dyam_Create_Binary(I(9),&ref[584],I(0))
	move_ret ref[585]
	c_ret

c_code local build_seed_21
	ret_reg &seed[21]
	call_c   build_ref_50()
	call_c   build_ref_605()
	call_c   Dyam_Seed_Start(&ref[50],&ref[605],I(0),fun5,1)
	call_c   build_ref_604()
	call_c   Dyam_Seed_Add_Comp(&ref[604],fun208,0)
	call_c   Dyam_Seed_End()
	move_ret seed[21]
	c_ret

;; TERM 604: '*GUARD*'(dcg_il_body_to_lpda_handler(_B, (_C ## _D), _E, _F, _G, _H, _I, _J, _K, _L, _M))
c_code local build_ref_604
	ret_reg &ref[604]
	call_c   build_ref_51()
	call_c   build_ref_603()
	call_c   Dyam_Create_Unary(&ref[51],&ref[603])
	move_ret ref[604]
	c_ret

;; TERM 603: dcg_il_body_to_lpda_handler(_B, (_C ## _D), _E, _F, _G, _H, _I, _J, _K, _L, _M)
c_code local build_ref_603
	ret_reg &ref[603]
	call_c   build_ref_835()
	call_c   build_ref_638()
	call_c   Dyam_Term_Start(&ref[835],11)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(&ref[638])
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_End()
	move_ret ref[603]
	c_ret

;; TERM 638: _C ## _D
c_code local build_ref_638
	ret_reg &ref[638]
	call_c   build_ref_913()
	call_c   Dyam_Create_Binary(&ref[913],V(2),V(3))
	move_ret ref[638]
	c_ret

;; TERM 913: ##
c_code local build_ref_913
	ret_reg &ref[913]
	call_c   Dyam_Create_Atom("##")
	move_ret ref[913]
	c_ret

c_code local build_seed_134
	ret_reg &seed[134]
	call_c   build_ref_51()
	call_c   build_ref_607()
	call_c   Dyam_Seed_Start(&ref[51],&ref[607],I(0),fun0,1)
	call_c   build_ref_608()
	call_c   Dyam_Seed_Add_Comp(&ref[608],&ref[607],0)
	call_c   Dyam_Seed_End()
	move_ret seed[134]
	c_ret

;; TERM 608: '*GUARD*'(position_and_stacks(_B, _R, _S)) :> '$$HOLE$$'
c_code local build_ref_608
	ret_reg &ref[608]
	call_c   build_ref_607()
	call_c   Dyam_Create_Binary(I(9),&ref[607],I(7))
	move_ret ref[608]
	c_ret

;; TERM 607: '*GUARD*'(position_and_stacks(_B, _R, _S))
c_code local build_ref_607
	ret_reg &ref[607]
	call_c   build_ref_51()
	call_c   build_ref_606()
	call_c   Dyam_Create_Unary(&ref[51],&ref[606])
	move_ret ref[607]
	c_ret

;; TERM 606: position_and_stacks(_B, _R, _S)
c_code local build_ref_606
	ret_reg &ref[606]
	call_c   build_ref_826()
	call_c   Dyam_Term_Start(&ref[826],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_End()
	move_ret ref[606]
	c_ret

;; TERM 609: [_O,_P]
c_code local build_ref_609
	ret_reg &ref[609]
	call_c   Dyam_Create_Tupple(14,15,I(0))
	move_ret ref[609]
	c_ret

;; TERM 610: _I :> _U
c_code local build_ref_610
	ret_reg &ref[610]
	call_c   Dyam_Create_Binary(I(9),V(8),V(20))
	move_ret ref[610]
	c_ret

long local pool_fun207[5]=[4,build_seed_134,build_ref_609,build_ref_610,build_ref_179]

pl_code local fun207
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Reg_Load(0,V(12))
	move     V(13), R(2)
	move     S(5), R(3)
	pl_call  pred_il_label_2()
	pl_call  fun7(&seed[134],1)
	move     &ref[609], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(19), R(4)
	move     S(5), R(5)
	pl_call  pred_my_numbervars_3()
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(17))
	call_c   Dyam_Reg_Load(4,V(18))
	call_c   Dyam_Reg_Load(6,V(7))
	move     V(20), R(8)
	move     S(5), R(9)
	call_c   Dyam_Reg_Load(10,V(11))
	pl_call  pred_dcg_il_loop_to_lpda_6()
	call_c   Dyam_Reg_Load(0,V(1))
	move     &ref[610], R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Load(4,V(17))
	call_c   Dyam_Reg_Load(6,V(18))
	call_c   Dyam_Reg_Load(8,V(6))
	call_c   Dyam_Reg_Load(10,V(4))
	call_c   Dyam_Reg_Load(12,V(14))
	move     V(16), R(14)
	move     S(5), R(15)
	call_c   Dyam_Reg_Load(16,V(10))
	call_c   Dyam_Reg_Load(18,V(11))
	call_c   Dyam_Reg_Load(20,V(13))
	pl_call  pred_dcg_il_start_to_lpda_11()
fun206:
	move     V(21), R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(22), R(4)
	move     S(5), R(5)
	pl_call  pred_my_numbervars_3()
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(3))
	call_c   Dyam_Reg_Load(4,V(21))
	call_c   Dyam_Reg_Load(6,V(5))
	move     V(23), R(8)
	move     S(5), R(9)
	move     V(23), R(10)
	move     S(5), R(11)
	move     &ref[179], R(12)
	move     0, R(13)
	call_c   Dyam_Reg_Load(14,V(9))
	move     V(24), R(16)
	move     S(5), R(17)
	call_c   Dyam_Reg_Load(18,V(11))
	call_c   Dyam_Reg_Load(20,V(13))
	pl_call  pred_dcg_il_body_to_lpda_11()
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(2))
	call_c   Dyam_Reg_Load(4,V(14))
	call_c   Dyam_Reg_Load(6,V(21))
	move     V(25), R(8)
	move     S(5), R(9)
	move     V(25), R(10)
	move     S(5), R(11)
	move     &ref[179], R(12)
	move     0, R(13)
	call_c   Dyam_Reg_Load(14,V(24))
	call_c   Dyam_Reg_Load(16,V(16))
	call_c   Dyam_Reg_Load(18,V(11))
	call_c   Dyam_Reg_Load(20,V(13))
	call_c   Dyam_Reg_Deallocate(11)
	pl_jump  pred_dcg_il_body_to_lpda_11()


long local pool_fun208[5]=[65539,build_ref_604,build_ref_179,build_ref_179,pool_fun207]

pl_code local fun208
	call_c   Dyam_Pool(pool_fun208)
	call_c   Dyam_Unify_Item(&ref[604])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun207)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(8),&ref[179])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(13),V(12))
	fail_ret
	call_c   Dyam_Unify(V(14),V(4))
	fail_ret
	call_c   Dyam_Unify(V(15),V(5))
	fail_ret
	call_c   Dyam_Unify(V(10),V(16))
	fail_ret
	pl_jump  fun206()

;; TERM 605: '*GUARD*'(dcg_il_body_to_lpda_handler(_B, (_C ## _D), _E, _F, _G, _H, _I, _J, _K, _L, _M)) :> []
c_code local build_ref_605
	ret_reg &ref[605]
	call_c   build_ref_604()
	call_c   Dyam_Create_Binary(I(9),&ref[604],I(0))
	move_ret ref[605]
	c_ret

c_code local build_seed_12
	ret_reg &seed[12]
	call_c   build_ref_50()
	call_c   build_ref_613()
	call_c   Dyam_Seed_Start(&ref[50],&ref[613],I(0),fun5,1)
	call_c   build_ref_612()
	call_c   Dyam_Seed_Add_Comp(&ref[612],fun210,0)
	call_c   Dyam_Seed_End()
	move_ret seed[12]
	c_ret

;; TERM 612: '*GUARD*'(bmg_non_terminal_to_lpda(_B, _C, _D, _E, _F, _G, _H, _I, _J))
c_code local build_ref_612
	ret_reg &ref[612]
	call_c   build_ref_51()
	call_c   build_ref_611()
	call_c   Dyam_Create_Unary(&ref[51],&ref[611])
	move_ret ref[612]
	c_ret

;; TERM 611: bmg_non_terminal_to_lpda(_B, _C, _D, _E, _F, _G, _H, _I, _J)
c_code local build_ref_611
	ret_reg &ref[611]
	call_c   build_ref_914()
	call_c   Dyam_Term_Start(&ref[914],9)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret ref[611]
	c_ret

;; TERM 914: bmg_non_terminal_to_lpda
c_code local build_ref_914
	ret_reg &ref[914]
	call_c   Dyam_Create_Atom("bmg_non_terminal_to_lpda")
	move_ret ref[914]
	c_ret

c_code local build_seed_135
	ret_reg &seed[135]
	call_c   build_ref_51()
	call_c   build_ref_615()
	call_c   Dyam_Seed_Start(&ref[51],&ref[615],I(0),fun0,1)
	call_c   build_ref_616()
	call_c   Dyam_Seed_Add_Comp(&ref[616],&ref[615],0)
	call_c   Dyam_Seed_End()
	move_ret seed[135]
	c_ret

;; TERM 616: '*GUARD*'(bmg_inside_island(_C, _K, _F, _G, _L, _M, _N, _O)) :> '$$HOLE$$'
c_code local build_ref_616
	ret_reg &ref[616]
	call_c   build_ref_615()
	call_c   Dyam_Create_Binary(I(9),&ref[615],I(7))
	move_ret ref[616]
	c_ret

;; TERM 615: '*GUARD*'(bmg_inside_island(_C, _K, _F, _G, _L, _M, _N, _O))
c_code local build_ref_615
	ret_reg &ref[615]
	call_c   build_ref_51()
	call_c   build_ref_614()
	call_c   Dyam_Create_Unary(&ref[51],&ref[614])
	move_ret ref[615]
	c_ret

;; TERM 614: bmg_inside_island(_C, _K, _F, _G, _L, _M, _N, _O)
c_code local build_ref_614
	ret_reg &ref[614]
	call_c   build_ref_915()
	call_c   Dyam_Term_Start(&ref[915],8)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_End()
	move_ret ref[614]
	c_ret

;; TERM 915: bmg_inside_island
c_code local build_ref_915
	ret_reg &ref[915]
	call_c   Dyam_Create_Atom("bmg_inside_island")
	move_ret ref[915]
	c_ret

;; TERM 617: dcg _P / _Q
c_code local build_ref_617
	ret_reg &ref[617]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_840()
	call_c   Dyam_Create_Binary(&ref[840],V(15),V(16))
	move_ret R(0)
	call_c   build_ref_839()
	call_c   Dyam_Create_Unary(&ref[839],R(0))
	move_ret ref[617]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_136
	ret_reg &seed[136]
	call_c   build_ref_51()
	call_c   build_ref_619()
	call_c   Dyam_Seed_Start(&ref[51],&ref[619],I(0),fun0,1)
	call_c   build_ref_620()
	call_c   Dyam_Seed_Add_Comp(&ref[620],&ref[619],0)
	call_c   Dyam_Seed_End()
	move_ret seed[136]
	c_ret

;; TERM 620: '*GUARD*'(bmg_pop(_S, _F, _G, _T)) :> '$$HOLE$$'
c_code local build_ref_620
	ret_reg &ref[620]
	call_c   build_ref_619()
	call_c   Dyam_Create_Binary(I(9),&ref[619],I(7))
	move_ret ref[620]
	c_ret

;; TERM 619: '*GUARD*'(bmg_pop(_S, _F, _G, _T))
c_code local build_ref_619
	ret_reg &ref[619]
	call_c   build_ref_51()
	call_c   build_ref_618()
	call_c   Dyam_Create_Unary(&ref[51],&ref[618])
	move_ret ref[619]
	c_ret

;; TERM 618: bmg_pop(_S, _F, _G, _T)
c_code local build_ref_618
	ret_reg &ref[618]
	call_c   build_ref_916()
	call_c   Dyam_Term_Start(&ref[916],4)
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_End()
	move_ret ref[618]
	c_ret

;; TERM 916: bmg_pop
c_code local build_ref_916
	ret_reg &ref[916]
	call_c   Dyam_Create_Atom("bmg_pop")
	move_ret ref[916]
	c_ret

c_code local build_seed_138
	ret_reg &seed[138]
	call_c   build_ref_51()
	call_c   build_ref_625()
	call_c   Dyam_Seed_Start(&ref[51],&ref[625],I(0),fun0,1)
	call_c   build_ref_626()
	call_c   Dyam_Seed_Add_Comp(&ref[626],&ref[625],0)
	call_c   Dyam_Seed_End()
	move_ret seed[138]
	c_ret

;; TERM 626: '*GUARD*'(bmg_pop_code(_T, _S, _F, _G, _D, _E, _N, _U)) :> '$$HOLE$$'
c_code local build_ref_626
	ret_reg &ref[626]
	call_c   build_ref_625()
	call_c   Dyam_Create_Binary(I(9),&ref[625],I(7))
	move_ret ref[626]
	c_ret

;; TERM 625: '*GUARD*'(bmg_pop_code(_T, _S, _F, _G, _D, _E, _N, _U))
c_code local build_ref_625
	ret_reg &ref[625]
	call_c   build_ref_51()
	call_c   build_ref_624()
	call_c   Dyam_Create_Unary(&ref[51],&ref[624])
	move_ret ref[625]
	c_ret

;; TERM 624: bmg_pop_code(_T, _S, _F, _G, _D, _E, _N, _U)
c_code local build_ref_624
	ret_reg &ref[624]
	call_c   build_ref_917()
	call_c   Dyam_Term_Start(&ref[917],8)
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_End()
	move_ret ref[624]
	c_ret

;; TERM 917: bmg_pop_code
c_code local build_ref_917
	ret_reg &ref[917]
	call_c   Dyam_Create_Atom("bmg_pop_code")
	move_ret ref[917]
	c_ret

c_code local build_seed_139
	ret_reg &seed[139]
	call_c   build_ref_51()
	call_c   build_ref_628()
	call_c   Dyam_Seed_Start(&ref[51],&ref[628],I(0),fun0,1)
	call_c   build_ref_629()
	call_c   Dyam_Seed_Add_Comp(&ref[629],&ref[628],0)
	call_c   Dyam_Seed_End()
	move_ret seed[139]
	c_ret

;; TERM 629: '*GUARD*'(non_terminal_to_lpda(_B, _S, _D, _E, _L, _M, _O, _V, prolog)) :> '$$HOLE$$'
c_code local build_ref_629
	ret_reg &ref[629]
	call_c   build_ref_628()
	call_c   Dyam_Create_Binary(I(9),&ref[628],I(7))
	move_ret ref[629]
	c_ret

;; TERM 628: '*GUARD*'(non_terminal_to_lpda(_B, _S, _D, _E, _L, _M, _O, _V, prolog))
c_code local build_ref_628
	ret_reg &ref[628]
	call_c   build_ref_51()
	call_c   build_ref_627()
	call_c   Dyam_Create_Unary(&ref[51],&ref[627])
	move_ret ref[628]
	c_ret

;; TERM 627: non_terminal_to_lpda(_B, _S, _D, _E, _L, _M, _O, _V, prolog)
c_code local build_ref_627
	ret_reg &ref[627]
	call_c   build_ref_918()
	call_c   build_ref_110()
	call_c   Dyam_Term_Start(&ref[918],9)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(21))
	call_c   Dyam_Term_Arg(&ref[110])
	call_c   Dyam_Term_End()
	move_ret ref[627]
	c_ret

;; TERM 918: non_terminal_to_lpda
c_code local build_ref_918
	ret_reg &ref[918]
	call_c   Dyam_Create_Atom("non_terminal_to_lpda")
	move_ret ref[918]
	c_ret

long local pool_fun209[3]=[2,build_seed_138,build_seed_139]

pl_code local fun209
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Reg_Load(0,V(7))
	call_c   Dyam_Reg_Load(2,V(8))
	call_c   Dyam_Reg_Load(4,V(13))
	move     V(20), R(6)
	move     S(5), R(7)
	move     V(21), R(8)
	move     S(5), R(9)
	pl_call  pred_handle_choice_5()
	pl_call  fun7(&seed[138],1)
	pl_call  fun7(&seed[139],1)
	pl_jump  fun132()

c_code local build_seed_137
	ret_reg &seed[137]
	call_c   build_ref_51()
	call_c   build_ref_622()
	call_c   Dyam_Seed_Start(&ref[51],&ref[622],I(0),fun0,1)
	call_c   build_ref_623()
	call_c   Dyam_Seed_Add_Comp(&ref[623],&ref[622],0)
	call_c   Dyam_Seed_End()
	move_ret seed[137]
	c_ret

;; TERM 623: '*GUARD*'(non_terminal_to_lpda(_B, _S, _D, _E, _L, _M, _O, _I, _J)) :> '$$HOLE$$'
c_code local build_ref_623
	ret_reg &ref[623]
	call_c   build_ref_622()
	call_c   Dyam_Create_Binary(I(9),&ref[622],I(7))
	move_ret ref[623]
	c_ret

;; TERM 622: '*GUARD*'(non_terminal_to_lpda(_B, _S, _D, _E, _L, _M, _O, _I, _J))
c_code local build_ref_622
	ret_reg &ref[622]
	call_c   build_ref_51()
	call_c   build_ref_621()
	call_c   Dyam_Create_Unary(&ref[51],&ref[621])
	move_ret ref[622]
	c_ret

;; TERM 621: non_terminal_to_lpda(_B, _S, _D, _E, _L, _M, _O, _I, _J)
c_code local build_ref_621
	ret_reg &ref[621]
	call_c   build_ref_918()
	call_c   Dyam_Term_Start(&ref[918],9)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret ref[621]
	c_ret

long local pool_fun210[7]=[65541,build_ref_612,build_seed_135,build_ref_617,build_seed_136,build_seed_137,pool_fun209]

pl_code local fun210
	call_c   Dyam_Pool(pool_fun210)
	call_c   Dyam_Unify_Item(&ref[612])
	fail_ret
	call_c   Dyam_Allocate(0)
	pl_call  fun7(&seed[135],1)
	call_c   DYAM_evpred_functor(V(10),V(15),V(16))
	fail_ret
	move     &ref[617], R(0)
	move     S(5), R(1)
	move     V(17), R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Load(4,V(10))
	move     V(18), R(6)
	move     S(5), R(7)
	pl_call  pred_term_current_module_shift_4()
	pl_call  fun7(&seed[136],1)
	call_c   Dyam_Choice(fun209)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(19),I(0))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(13),V(7))
	fail_ret
	pl_call  fun7(&seed[137],1)
	pl_jump  fun132()

;; TERM 613: '*GUARD*'(bmg_non_terminal_to_lpda(_B, _C, _D, _E, _F, _G, _H, _I, _J)) :> []
c_code local build_ref_613
	ret_reg &ref[613]
	call_c   build_ref_612()
	call_c   Dyam_Create_Binary(I(9),&ref[612],I(0))
	move_ret ref[613]
	c_ret

c_code local build_seed_26
	ret_reg &seed[26]
	call_c   build_ref_50()
	call_c   build_ref_632()
	call_c   Dyam_Seed_Start(&ref[50],&ref[632],I(0),fun5,1)
	call_c   build_ref_631()
	call_c   Dyam_Seed_Add_Comp(&ref[631],fun215,0)
	call_c   Dyam_Seed_End()
	move_ret seed[26]
	c_ret

;; TERM 631: '*GUARD*'(dcg_body_to_lpda_handler(_B, (_C ## _D), _E, _F, _G, _H, _I, _J, _K))
c_code local build_ref_631
	ret_reg &ref[631]
	call_c   build_ref_51()
	call_c   build_ref_630()
	call_c   Dyam_Create_Unary(&ref[51],&ref[630])
	move_ret ref[631]
	c_ret

;; TERM 630: dcg_body_to_lpda_handler(_B, (_C ## _D), _E, _F, _G, _H, _I, _J, _K)
c_code local build_ref_630
	ret_reg &ref[630]
	call_c   build_ref_828()
	call_c   build_ref_638()
	call_c   Dyam_Term_Start(&ref[828],9)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(&ref[638])
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_End()
	move_ret ref[630]
	c_ret

;; TERM 644: '$CLOSURE'('$fun'(214, 0, 1148677336), '$TUPPLE'(35074870873908))
c_code local build_ref_644
	ret_reg &ref[644]
	call_c   build_ref_643()
	call_c   Dyam_Closure_Aux(fun214,&ref[643])
	move_ret ref[644]
	c_ret

;; TERM 637: [_M,_N]
c_code local build_ref_637
	ret_reg &ref[637]
	call_c   Dyam_Create_Tupple(12,13,I(0))
	move_ret ref[637]
	c_ret

;; TERM 641: '*NOOP*'(_P) :> '*IL-START*'(_T)
c_code local build_ref_641
	ret_reg &ref[641]
	call_c   build_ref_639()
	call_c   build_ref_640()
	call_c   Dyam_Create_Binary(I(9),&ref[639],&ref[640])
	move_ret ref[641]
	c_ret

;; TERM 640: '*IL-START*'(_T)
c_code local build_ref_640
	ret_reg &ref[640]
	call_c   build_ref_919()
	call_c   Dyam_Create_Unary(&ref[919],V(19))
	move_ret ref[640]
	c_ret

;; TERM 919: '*IL-START*'
c_code local build_ref_919
	ret_reg &ref[919]
	call_c   Dyam_Create_Atom("*IL-START*")
	move_ret ref[919]
	c_ret

;; TERM 639: '*NOOP*'(_P)
c_code local build_ref_639
	ret_reg &ref[639]
	call_c   build_ref_920()
	call_c   Dyam_Create_Unary(&ref[920],V(15))
	move_ret ref[639]
	c_ret

;; TERM 920: '*NOOP*'
c_code local build_ref_920
	ret_reg &ref[920]
	call_c   Dyam_Create_Atom("*NOOP*")
	move_ret ref[920]
	c_ret

long local pool_fun214[6]=[5,build_ref_637,build_seed_73,build_ref_638,build_ref_179,build_ref_641]

pl_code local fun214
	call_c   Dyam_Pool(pool_fun214)
	call_c   Dyam_Allocate(0)
	move     I(0), R(0)
	move     0, R(1)
	move     V(11), R(2)
	move     S(5), R(3)
	pl_call  pred_il_label_2()
	move     &ref[637], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(14), R(4)
	move     S(5), R(5)
	pl_call  pred_my_numbervars_3()
	move     &ref[638], R(0)
	move     S(5), R(1)
	move     V(15), R(2)
	move     S(5), R(3)
	pl_call  pred_il_tupple_2()
	pl_call  fun7(&seed[73],1)
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(4))
	call_c   Dyam_Reg_Load(4,V(6))
	call_c   Dyam_Reg_Load(6,V(13))
	move     V(16), R(8)
	move     S(5), R(9)
	call_c   Dyam_Reg_Load(10,V(10))
	pl_call  pred_dcg_il_loop_to_lpda_6()
	call_c   Dyam_Reg_Load(0,V(1))
	move     &ref[638], R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Load(4,V(12))
	call_c   Dyam_Reg_Load(6,V(13))
	move     V(17), R(8)
	move     S(5), R(9)
	move     V(17), R(10)
	move     S(5), R(11)
	move     &ref[179], R(12)
	move     0, R(13)
	call_c   Dyam_Reg_Load(14,V(16))
	move     V(18), R(16)
	move     S(5), R(17)
	call_c   Dyam_Reg_Load(18,V(10))
	call_c   Dyam_Reg_Load(20,V(11))
	pl_call  pred_dcg_il_body_to_lpda_11()
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(8))
	call_c   Dyam_Reg_Load(4,V(5))
	call_c   Dyam_Reg_Load(6,V(7))
	move     I(0), R(8)
	move     0, R(9)
	move     I(0), R(10)
	move     0, R(11)
	call_c   Dyam_Reg_Load(12,V(12))
	call_c   Dyam_Reg_Load(14,V(18))
	move     V(19), R(16)
	move     S(5), R(17)
	call_c   Dyam_Reg_Load(18,V(10))
	call_c   Dyam_Reg_Load(20,V(11))
	pl_call  pred_dcg_il_start_to_lpda_11()
	call_c   Dyam_Unify(V(9),&ref[641])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

;; TERM 643: '$TUPPLE'(35074870873908)
c_code local build_ref_643
	ret_reg &ref[643]
	call_c   Dyam_Create_Simple_Tupple(0,268173312)
	move_ret ref[643]
	c_ret

long local pool_fun215[3]=[2,build_ref_631,build_ref_644]

pl_code local fun215
	call_c   Dyam_Pool(pool_fun215)
	call_c   Dyam_Unify_Item(&ref[631])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[644], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Deallocate(2)
fun213:
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(2)
	call_c   Dyam_Choice(fun212)
	pl_call  fun13(&seed[140],1)
	pl_fail


;; TERM 632: '*GUARD*'(dcg_body_to_lpda_handler(_B, (_C ## _D), _E, _F, _G, _H, _I, _J, _K)) :> []
c_code local build_ref_632
	ret_reg &ref[632]
	call_c   build_ref_631()
	call_c   Dyam_Create_Binary(I(9),&ref[631],I(0))
	move_ret ref[632]
	c_ret

c_code local build_seed_9
	ret_reg &seed[9]
	call_c   build_ref_36()
	call_c   build_ref_647()
	call_c   Dyam_Seed_Start(&ref[36],&ref[647],I(0),fun4,1)
	call_c   build_ref_648()
	call_c   Dyam_Seed_Add_Comp(&ref[648],fun220,0)
	call_c   Dyam_Seed_End()
	move_ret seed[9]
	c_ret

;; TERM 648: '*CITEM*'(wrapping_predicate(lc_dcg((_B / _C))), _A)
c_code local build_ref_648
	ret_reg &ref[648]
	call_c   build_ref_39()
	call_c   build_ref_645()
	call_c   Dyam_Create_Binary(&ref[39],&ref[645],V(0))
	move_ret ref[648]
	c_ret

;; TERM 645: wrapping_predicate(lc_dcg((_B / _C)))
c_code local build_ref_645
	ret_reg &ref[645]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_840()
	call_c   Dyam_Create_Binary(&ref[840],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_921()
	call_c   Dyam_Create_Unary(&ref[921],R(0))
	move_ret R(0)
	call_c   build_ref_905()
	call_c   Dyam_Create_Unary(&ref[905],R(0))
	move_ret ref[645]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 921: lc_dcg
c_code local build_ref_921
	ret_reg &ref[921]
	call_c   Dyam_Create_Atom("lc_dcg")
	move_ret ref[921]
	c_ret

;; TERM 662: '$CLOSURE'('$fun'(219, 0, 1148737472), '$TUPPLE'(35074870618184))
c_code local build_ref_662
	ret_reg &ref[662]
	call_c   build_ref_526()
	call_c   Dyam_Closure_Aux(fun219,&ref[526])
	move_ret ref[662]
	c_ret

;; TERM 660: '$CLOSURE'('$fun'(218, 0, 1148725584), '$TUPPLE'(35074870618132))
c_code local build_ref_660
	ret_reg &ref[660]
	call_c   build_ref_523()
	call_c   Dyam_Closure_Aux(fun218,&ref[523])
	move_ret ref[660]
	c_ret

;; TERM 649: [_P,_Q]
c_code local build_ref_649
	ret_reg &ref[649]
	call_c   Dyam_Create_Tupple(15,16,I(0))
	move_ret ref[649]
	c_ret

c_code local build_seed_142
	ret_reg &seed[142]
	call_c   build_ref_51()
	call_c   build_ref_651()
	call_c   Dyam_Seed_Start(&ref[51],&ref[651],I(0),fun0,1)
	call_c   build_ref_652()
	call_c   Dyam_Seed_Add_Comp(&ref[652],&ref[651],0)
	call_c   Dyam_Seed_End()
	move_ret seed[142]
	c_ret

;; TERM 652: '*GUARD*'(body_to_lpda(_N, (internal_terminal_lc(_I, _Q) , domain(_P, _Q)), noop, _S, rec_prolog)) :> '$$HOLE$$'
c_code local build_ref_652
	ret_reg &ref[652]
	call_c   build_ref_651()
	call_c   Dyam_Create_Binary(I(9),&ref[651],I(7))
	move_ret ref[652]
	c_ret

;; TERM 651: '*GUARD*'(body_to_lpda(_N, (internal_terminal_lc(_I, _Q) , domain(_P, _Q)), noop, _S, rec_prolog))
c_code local build_ref_651
	ret_reg &ref[651]
	call_c   build_ref_51()
	call_c   build_ref_650()
	call_c   Dyam_Create_Unary(&ref[51],&ref[650])
	move_ret ref[651]
	c_ret

;; TERM 650: body_to_lpda(_N, (internal_terminal_lc(_I, _Q) , domain(_P, _Q)), noop, _S, rec_prolog)
c_code local build_ref_650
	ret_reg &ref[650]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_922()
	call_c   Dyam_Create_Binary(&ref[922],V(8),V(16))
	move_ret R(0)
	call_c   build_ref_923()
	call_c   Dyam_Create_Binary(&ref[923],V(15),V(16))
	move_ret R(1)
	call_c   Dyam_Create_Binary(I(4),R(0),R(1))
	move_ret R(1)
	call_c   build_ref_875()
	call_c   build_ref_179()
	call_c   build_ref_248()
	call_c   Dyam_Term_Start(&ref[875],5)
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(&ref[179])
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(&ref[248])
	call_c   Dyam_Term_End()
	move_ret ref[650]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 923: domain
c_code local build_ref_923
	ret_reg &ref[923]
	call_c   Dyam_Create_Atom("domain")
	move_ret ref[923]
	c_ret

;; TERM 922: internal_terminal_lc
c_code local build_ref_922
	ret_reg &ref[922]
	call_c   Dyam_Create_Atom("internal_terminal_lc")
	move_ret ref[922]
	c_ret

c_code local build_seed_143
	ret_reg &seed[143]
	call_c   build_ref_51()
	call_c   build_ref_654()
	call_c   Dyam_Seed_Start(&ref[51],&ref[654],I(0),fun0,1)
	call_c   build_ref_655()
	call_c   Dyam_Seed_Add_Comp(&ref[655],&ref[654],0)
	call_c   Dyam_Seed_End()
	move_ret seed[143]
	c_ret

;; TERM 655: '*GUARD*'(body_to_lpda(_N, (internal_lc(_I, _Q) , domain(_P, _Q)), noop, _T, rec_prolog)) :> '$$HOLE$$'
c_code local build_ref_655
	ret_reg &ref[655]
	call_c   build_ref_654()
	call_c   Dyam_Create_Binary(I(9),&ref[654],I(7))
	move_ret ref[655]
	c_ret

;; TERM 654: '*GUARD*'(body_to_lpda(_N, (internal_lc(_I, _Q) , domain(_P, _Q)), noop, _T, rec_prolog))
c_code local build_ref_654
	ret_reg &ref[654]
	call_c   build_ref_51()
	call_c   build_ref_653()
	call_c   Dyam_Create_Unary(&ref[51],&ref[653])
	move_ret ref[654]
	c_ret

;; TERM 653: body_to_lpda(_N, (internal_lc(_I, _Q) , domain(_P, _Q)), noop, _T, rec_prolog)
c_code local build_ref_653
	ret_reg &ref[653]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_924()
	call_c   Dyam_Create_Binary(&ref[924],V(8),V(16))
	move_ret R(0)
	call_c   build_ref_923()
	call_c   Dyam_Create_Binary(&ref[923],V(15),V(16))
	move_ret R(1)
	call_c   Dyam_Create_Binary(I(4),R(0),R(1))
	move_ret R(1)
	call_c   build_ref_875()
	call_c   build_ref_179()
	call_c   build_ref_248()
	call_c   Dyam_Term_Start(&ref[875],5)
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(&ref[179])
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_Arg(&ref[248])
	call_c   Dyam_Term_End()
	move_ret ref[653]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 924: internal_lc
c_code local build_ref_924
	ret_reg &ref[924]
	call_c   Dyam_Create_Atom("internal_lc")
	move_ret ref[924]
	c_ret

;; TERM 658: '*LCNEXT*'(_I, _J, '*PROLOG-LAST*', _K, (_I ^ _P ^ (_S , _T)))
c_code local build_ref_658
	ret_reg &ref[658]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Binary(I(4),V(18),V(19))
	move_ret R(0)
	call_c   build_ref_860()
	call_c   Dyam_Create_Binary(&ref[860],V(15),R(0))
	move_ret R(0)
	call_c   Dyam_Create_Binary(&ref[860],V(8),R(0))
	move_ret R(0)
	call_c   build_ref_925()
	call_c   build_ref_109()
	call_c   Dyam_Term_Start(&ref[925],5)
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(&ref[109])
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_End()
	move_ret ref[658]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 925: '*LCNEXT*'
c_code local build_ref_925
	ret_reg &ref[925]
	call_c   Dyam_Create_Atom("*LCNEXT*")
	move_ret ref[925]
	c_ret

;; TERM 657: '*WRAPPER*'(lc_dcg((_B / _C)), [_M|_L], _U)
c_code local build_ref_657
	ret_reg &ref[657]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_840()
	call_c   Dyam_Create_Binary(&ref[840],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_921()
	call_c   Dyam_Create_Unary(&ref[921],R(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(12),V(11))
	move_ret R(1)
	call_c   build_ref_908()
	call_c   Dyam_Term_Start(&ref[908],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_End()
	move_ret ref[657]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun216[3]=[2,build_ref_657,build_seed_54]

long local pool_fun217[3]=[65537,build_ref_658,pool_fun216]

pl_code local fun217
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(20),&ref[658])
	fail_ret
fun216:
	call_c   DYAM_evpred_assert_1(&ref[657])
	call_c   Dyam_Deallocate()
	pl_jump  fun2(&seed[54],2)


;; TERM 656: '*LIGHTLCNEXT*'(_I, _J, '*PROLOG-LAST*', _K, (_I ^ _P ^ (_S , _T)))
c_code local build_ref_656
	ret_reg &ref[656]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Binary(I(4),V(18),V(19))
	move_ret R(0)
	call_c   build_ref_860()
	call_c   Dyam_Create_Binary(&ref[860],V(15),R(0))
	move_ret R(0)
	call_c   Dyam_Create_Binary(&ref[860],V(8),R(0))
	move_ret R(0)
	call_c   build_ref_926()
	call_c   build_ref_109()
	call_c   Dyam_Term_Start(&ref[926],5)
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(&ref[109])
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_End()
	move_ret ref[656]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 926: '*LIGHTLCNEXT*'
c_code local build_ref_926
	ret_reg &ref[926]
	call_c   Dyam_Create_Atom("*LIGHTLCNEXT*")
	move_ret ref[926]
	c_ret

long local pool_fun218[11]=[131080,build_seed_122,build_ref_539,build_ref_540,build_ref_649,build_seed_142,build_seed_143,build_ref_541,build_ref_656,pool_fun217,pool_fun216]

pl_code local fun218
	call_c   Dyam_Pool(pool_fun218)
	call_c   DYAM_evpred_functor(V(5),V(1),V(2))
	fail_ret
	call_c   Dyam_Allocate(0)
	pl_call  fun7(&seed[122],1)
	pl_call  Object_1(&ref[539])
	move     &ref[540], R(0)
	move     S(5), R(1)
	move     V(13), R(2)
	move     S(5), R(3)
	move     V(14), R(4)
	move     S(5), R(5)
	pl_call  pred_my_numbervars_3()
	move     &ref[649], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(13))
	move     V(17), R(4)
	move     S(5), R(5)
	pl_call  pred_my_numbervars_3()
	pl_call  fun7(&seed[142],1)
	pl_call  fun7(&seed[143],1)
	call_c   Dyam_Choice(fun217)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[541])
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(20),&ref[656])
	fail_ret
	pl_jump  fun216()

long local pool_fun219[2]=[1,build_ref_660]

pl_code local fun219
	call_c   Dyam_Pool(pool_fun219)
	call_c   Dyam_Allocate(0)
	move     &ref[660], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	move     V(4), R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  fun23()

long local pool_fun220[3]=[2,build_ref_648,build_ref_662]

pl_code local fun220
	call_c   Dyam_Pool(pool_fun220)
	call_c   Dyam_Unify_Item(&ref[648])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[662], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	move     V(3), R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  fun23()

;; TERM 647: '*FIRST*'(wrapping_predicate(lc_dcg((_B / _C)))) :> []
c_code local build_ref_647
	ret_reg &ref[647]
	call_c   build_ref_646()
	call_c   Dyam_Create_Binary(I(9),&ref[646],I(0))
	move_ret ref[647]
	c_ret

;; TERM 646: '*FIRST*'(wrapping_predicate(lc_dcg((_B / _C))))
c_code local build_ref_646
	ret_reg &ref[646]
	call_c   build_ref_36()
	call_c   build_ref_645()
	call_c   Dyam_Create_Unary(&ref[36],&ref[645])
	move_ret ref[646]
	c_ret

c_code local build_seed_28
	ret_reg &seed[28]
	call_c   build_ref_50()
	call_c   build_ref_706()
	call_c   Dyam_Seed_Start(&ref[50],&ref[706],I(0),fun5,1)
	call_c   build_ref_705()
	call_c   Dyam_Seed_Add_Comp(&ref[705],fun231,0)
	call_c   Dyam_Seed_End()
	move_ret seed[28]
	c_ret

;; TERM 705: '*GUARD*'(dcg_body_to_lpda_handler(_B, _C, _D, _E, _F, _G, _H, _I, _J))
c_code local build_ref_705
	ret_reg &ref[705]
	call_c   build_ref_51()
	call_c   build_ref_704()
	call_c   Dyam_Create_Unary(&ref[51],&ref[704])
	move_ret ref[705]
	c_ret

;; TERM 704: dcg_body_to_lpda_handler(_B, _C, _D, _E, _F, _G, _H, _I, _J)
c_code local build_ref_704
	ret_reg &ref[704]
	call_c   build_ref_828()
	call_c   Dyam_Term_Start(&ref[828],9)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret ref[704]
	c_ret

;; TERM 707: @*{goal=> _K, vars=> _L, from=> _M, to=> _N, collect_first=> _O, collect_last=> _P, collect_loop=> _Q, collect_next=> _R, collect_pred=> _S}
c_code local build_ref_707
	ret_reg &ref[707]
	call_c   build_ref_849()
	call_c   Dyam_Term_Start(&ref[849],9)
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_End()
	move_ret ref[707]
	c_ret

;; TERM 708: kleene
c_code local build_ref_708
	ret_reg &ref[708]
	call_c   Dyam_Create_Atom("kleene")
	move_ret ref[708]
	c_ret

;; TERM 709: '_kleene~w'
c_code local build_ref_709
	ret_reg &ref[709]
	call_c   Dyam_Create_Atom("_kleene~w")
	move_ret ref[709]
	c_ret

;; TERM 710: [_T]
c_code local build_ref_710
	ret_reg &ref[710]
	call_c   Dyam_Create_List(V(19),I(0))
	move_ret ref[710]
	c_ret

;; TERM 711: kleene_conditions([_M,_N], [_V,_W], _X, _Y, _Z)
c_code local build_ref_711
	ret_reg &ref[711]
	call_c   build_ref_927()
	call_c   build_ref_637()
	call_c   build_ref_680()
	call_c   Dyam_Term_Start(&ref[927],5)
	call_c   Dyam_Term_Arg(&ref[637])
	call_c   Dyam_Term_Arg(&ref[680])
	call_c   Dyam_Term_Arg(V(23))
	call_c   Dyam_Term_Arg(V(24))
	call_c   Dyam_Term_Arg(V(25))
	call_c   Dyam_Term_End()
	move_ret ref[711]
	c_ret

;; TERM 680: [_V,_W]
c_code local build_ref_680
	ret_reg &ref[680]
	call_c   Dyam_Create_Tupple(21,22,I(0))
	move_ret ref[680]
	c_ret

;; TERM 927: kleene_conditions
c_code local build_ref_927
	ret_reg &ref[927]
	call_c   Dyam_Create_Atom("kleene_conditions")
	move_ret ref[927]
	c_ret

c_code local build_seed_147
	ret_reg &seed[147]
	call_c   build_ref_51()
	call_c   build_ref_713()
	call_c   Dyam_Seed_Start(&ref[51],&ref[713],I(0),fun0,1)
	call_c   build_ref_714()
	call_c   Dyam_Seed_Add_Comp(&ref[714],&ref[713],0)
	call_c   Dyam_Seed_End()
	move_ret seed[147]
	c_ret

;; TERM 714: '*GUARD*'(position_and_stacks(_B, _D, _F)) :> '$$HOLE$$'
c_code local build_ref_714
	ret_reg &ref[714]
	call_c   build_ref_713()
	call_c   Dyam_Create_Binary(I(9),&ref[713],I(7))
	move_ret ref[714]
	c_ret

;; TERM 713: '*GUARD*'(position_and_stacks(_B, _D, _F))
c_code local build_ref_713
	ret_reg &ref[713]
	call_c   build_ref_51()
	call_c   build_ref_712()
	call_c   Dyam_Create_Unary(&ref[51],&ref[712])
	move_ret ref[713]
	c_ret

;; TERM 712: position_and_stacks(_B, _D, _F)
c_code local build_ref_712
	ret_reg &ref[712]
	call_c   build_ref_826()
	call_c   Dyam_Term_Start(&ref[826],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[712]
	c_ret

c_code local build_seed_148
	ret_reg &seed[148]
	call_c   build_ref_51()
	call_c   build_ref_716()
	call_c   Dyam_Seed_Start(&ref[51],&ref[716],I(0),fun0,1)
	call_c   build_ref_717()
	call_c   Dyam_Seed_Add_Comp(&ref[717],&ref[716],0)
	call_c   Dyam_Seed_End()
	move_ret seed[148]
	c_ret

;; TERM 717: '*GUARD*'(position_and_stacks(_B, _A1, _B1)) :> '$$HOLE$$'
c_code local build_ref_717
	ret_reg &ref[717]
	call_c   build_ref_716()
	call_c   Dyam_Create_Binary(I(9),&ref[716],I(7))
	move_ret ref[717]
	c_ret

;; TERM 716: '*GUARD*'(position_and_stacks(_B, _A1, _B1))
c_code local build_ref_716
	ret_reg &ref[716]
	call_c   build_ref_51()
	call_c   build_ref_715()
	call_c   Dyam_Create_Unary(&ref[51],&ref[715])
	move_ret ref[716]
	c_ret

;; TERM 715: position_and_stacks(_B, _A1, _B1)
c_code local build_ref_715
	ret_reg &ref[715]
	call_c   build_ref_826()
	call_c   Dyam_Term_Start(&ref[826],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(26))
	call_c   Dyam_Term_Arg(V(27))
	call_c   Dyam_Term_End()
	move_ret ref[715]
	c_ret

c_code local build_seed_149
	ret_reg &seed[149]
	call_c   build_ref_51()
	call_c   build_ref_719()
	call_c   Dyam_Seed_Start(&ref[51],&ref[719],I(0),fun0,1)
	call_c   build_ref_720()
	call_c   Dyam_Seed_Add_Comp(&ref[720],&ref[719],0)
	call_c   Dyam_Seed_End()
	move_ret seed[149]
	c_ret

;; TERM 720: '*GUARD*'(position_and_stacks(_B, _C1, _D1)) :> '$$HOLE$$'
c_code local build_ref_720
	ret_reg &ref[720]
	call_c   build_ref_719()
	call_c   Dyam_Create_Binary(I(9),&ref[719],I(7))
	move_ret ref[720]
	c_ret

;; TERM 719: '*GUARD*'(position_and_stacks(_B, _C1, _D1))
c_code local build_ref_719
	ret_reg &ref[719]
	call_c   build_ref_51()
	call_c   build_ref_718()
	call_c   Dyam_Create_Unary(&ref[51],&ref[718])
	move_ret ref[719]
	c_ret

;; TERM 718: position_and_stacks(_B, _C1, _D1)
c_code local build_ref_718
	ret_reg &ref[718]
	call_c   build_ref_826()
	call_c   Dyam_Term_Start(&ref[826],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(28))
	call_c   Dyam_Term_Arg(V(29))
	call_c   Dyam_Term_End()
	move_ret ref[718]
	c_ret

;; TERM 721: [_V,_W,_X,_Y,_Z,@*{goal=> _K, vars=> _L, from=> _M, to=> _N, collect_first=> _O, collect_last=> _P, collect_loop=> _Q, collect_next=> _R, collect_pred=> _S}]
c_code local build_ref_721
	ret_reg &ref[721]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_707()
	call_c   Dyam_Create_List(&ref[707],I(0))
	move_ret R(0)
	call_c   Dyam_Create_Tupple(21,25,R(0))
	move_ret ref[721]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 722: [_V,_C1|_D1]
c_code local build_ref_722
	ret_reg &ref[722]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(28),V(29))
	move_ret R(0)
	call_c   Dyam_Create_List(V(21),R(0))
	move_ret ref[722]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 723: [_W,_A1|_B1]
c_code local build_ref_723
	ret_reg &ref[723]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(26),V(27))
	move_ret R(0)
	call_c   Dyam_Create_List(V(22),R(0))
	move_ret ref[723]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_150
	ret_reg &seed[150]
	call_c   build_ref_51()
	call_c   build_ref_725()
	call_c   Dyam_Seed_Start(&ref[51],&ref[725],I(0),fun0,1)
	call_c   build_ref_726()
	call_c   Dyam_Seed_Add_Comp(&ref[726],&ref[725],0)
	call_c   Dyam_Seed_End()
	move_ret seed[150]
	c_ret

;; TERM 726: '*GUARD*'(append(_F1, _Q, _H1)) :> '$$HOLE$$'
c_code local build_ref_726
	ret_reg &ref[726]
	call_c   build_ref_725()
	call_c   Dyam_Create_Binary(I(9),&ref[725],I(7))
	move_ret ref[726]
	c_ret

;; TERM 725: '*GUARD*'(append(_F1, _Q, _H1))
c_code local build_ref_725
	ret_reg &ref[725]
	call_c   build_ref_51()
	call_c   build_ref_724()
	call_c   Dyam_Create_Unary(&ref[51],&ref[724])
	move_ret ref[725]
	c_ret

;; TERM 724: append(_F1, _Q, _H1)
c_code local build_ref_724
	ret_reg &ref[724]
	call_c   build_ref_866()
	call_c   Dyam_Term_Start(&ref[866],3)
	call_c   Dyam_Term_Arg(V(31))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(33))
	call_c   Dyam_Term_End()
	move_ret ref[724]
	c_ret

c_code local build_seed_151
	ret_reg &seed[151]
	call_c   build_ref_51()
	call_c   build_ref_728()
	call_c   Dyam_Seed_Start(&ref[51],&ref[728],I(0),fun0,1)
	call_c   build_ref_729()
	call_c   Dyam_Seed_Add_Comp(&ref[729],&ref[728],0)
	call_c   Dyam_Seed_End()
	move_ret seed[151]
	c_ret

;; TERM 729: '*GUARD*'(append(_G1, _R, _I1)) :> '$$HOLE$$'
c_code local build_ref_729
	ret_reg &ref[729]
	call_c   build_ref_728()
	call_c   Dyam_Create_Binary(I(9),&ref[728],I(7))
	move_ret ref[729]
	c_ret

;; TERM 728: '*GUARD*'(append(_G1, _R, _I1))
c_code local build_ref_728
	ret_reg &ref[728]
	call_c   build_ref_51()
	call_c   build_ref_727()
	call_c   Dyam_Create_Unary(&ref[51],&ref[727])
	move_ret ref[728]
	c_ret

;; TERM 727: append(_G1, _R, _I1)
c_code local build_ref_727
	ret_reg &ref[727]
	call_c   build_ref_866()
	call_c   Dyam_Term_Start(&ref[866],3)
	call_c   Dyam_Term_Arg(V(32))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(34))
	call_c   Dyam_Term_End()
	move_ret ref[727]
	c_ret

;; TERM 730: [_E,_G,_P,[_M,_N]]
c_code local build_ref_730
	ret_reg &ref[730]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_637()
	call_c   Dyam_Create_List(&ref[637],I(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(15),R(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(6),R(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(4),R(0))
	move_ret ref[730]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_152
	ret_reg &seed[152]
	call_c   build_ref_51()
	call_c   build_ref_732()
	call_c   Dyam_Seed_Start(&ref[51],&ref[732],I(0),fun0,1)
	call_c   build_ref_733()
	call_c   Dyam_Seed_Add_Comp(&ref[733],&ref[732],0)
	call_c   Dyam_Seed_End()
	move_ret seed[152]
	c_ret

;; TERM 733: '*GUARD*'(dcg_body_to_lpda_handler(_B, {_Y , _P = _Q}, _C1, _E, _D1, _G, _H, _M1, _J)) :> '$$HOLE$$'
c_code local build_ref_733
	ret_reg &ref[733]
	call_c   build_ref_732()
	call_c   Dyam_Create_Binary(I(9),&ref[732],I(7))
	move_ret ref[733]
	c_ret

;; TERM 732: '*GUARD*'(dcg_body_to_lpda_handler(_B, {_Y , _P = _Q}, _C1, _E, _D1, _G, _H, _M1, _J))
c_code local build_ref_732
	ret_reg &ref[732]
	call_c   build_ref_51()
	call_c   build_ref_731()
	call_c   Dyam_Create_Unary(&ref[51],&ref[731])
	move_ret ref[732]
	c_ret

;; TERM 731: dcg_body_to_lpda_handler(_B, {_Y , _P = _Q}, _C1, _E, _D1, _G, _H, _M1, _J)
c_code local build_ref_731
	ret_reg &ref[731]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_869()
	call_c   Dyam_Create_Binary(&ref[869],V(15),V(16))
	move_ret R(0)
	call_c   Dyam_Create_Binary(I(4),V(24),R(0))
	move_ret R(0)
	call_c   Dyam_Create_Unary(I(2),R(0))
	move_ret R(0)
	call_c   build_ref_828()
	call_c   Dyam_Term_Start(&ref[828],9)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(28))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(29))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(38))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret ref[731]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_153
	ret_reg &seed[153]
	call_c   build_ref_51()
	call_c   build_ref_735()
	call_c   Dyam_Seed_Start(&ref[51],&ref[735],I(0),fun0,1)
	call_c   build_ref_736()
	call_c   Dyam_Seed_Add_Comp(&ref[736],&ref[735],0)
	call_c   Dyam_Seed_End()
	move_ret seed[153]
	c_ret

;; TERM 736: '*GUARD*'(dcg_body_to_lpda_handler(_B, ({_Z} , _K), _C1, _A1, _D1, _B1, '*KLEENE-LAST*'(_U, _I1, _H1, _J1, _L1, noop), _N1, _J)) :> '$$HOLE$$'
c_code local build_ref_736
	ret_reg &ref[736]
	call_c   build_ref_735()
	call_c   Dyam_Create_Binary(I(9),&ref[735],I(7))
	move_ret ref[736]
	c_ret

;; TERM 735: '*GUARD*'(dcg_body_to_lpda_handler(_B, ({_Z} , _K), _C1, _A1, _D1, _B1, '*KLEENE-LAST*'(_U, _I1, _H1, _J1, _L1, noop), _N1, _J))
c_code local build_ref_735
	ret_reg &ref[735]
	call_c   build_ref_51()
	call_c   build_ref_734()
	call_c   Dyam_Create_Unary(&ref[51],&ref[734])
	move_ret ref[735]
	c_ret

;; TERM 734: dcg_body_to_lpda_handler(_B, ({_Z} , _K), _C1, _A1, _D1, _B1, '*KLEENE-LAST*'(_U, _I1, _H1, _J1, _L1, noop), _N1, _J)
c_code local build_ref_734
	ret_reg &ref[734]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   Dyam_Create_Unary(I(2),V(25))
	move_ret R(0)
	call_c   Dyam_Create_Binary(I(4),R(0),V(10))
	move_ret R(0)
	call_c   build_ref_928()
	call_c   build_ref_179()
	call_c   Dyam_Term_Start(&ref[928],6)
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_Arg(V(34))
	call_c   Dyam_Term_Arg(V(33))
	call_c   Dyam_Term_Arg(V(35))
	call_c   Dyam_Term_Arg(V(37))
	call_c   Dyam_Term_Arg(&ref[179])
	call_c   Dyam_Term_End()
	move_ret R(1)
	call_c   build_ref_828()
	call_c   Dyam_Term_Start(&ref[828],9)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(28))
	call_c   Dyam_Term_Arg(V(26))
	call_c   Dyam_Term_Arg(V(29))
	call_c   Dyam_Term_Arg(V(27))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(39))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret ref[734]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 928: '*KLEENE-LAST*'
c_code local build_ref_928
	ret_reg &ref[928]
	call_c   Dyam_Create_Atom("*KLEENE-LAST*")
	move_ret ref[928]
	c_ret

;; TERM 737: '*KLEENE*'(_U, _J1, '*ALTERNATIVE*'(_M1, _N1))
c_code local build_ref_737
	ret_reg &ref[737]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_930()
	call_c   Dyam_Create_Binary(&ref[930],V(38),V(39))
	move_ret R(0)
	call_c   build_ref_929()
	call_c   Dyam_Term_Start(&ref[929],3)
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_Arg(V(35))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_End()
	move_ret ref[737]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 929: '*KLEENE*'
c_code local build_ref_929
	ret_reg &ref[929]
	call_c   Dyam_Create_Atom("*KLEENE*")
	move_ret ref[929]
	c_ret

;; TERM 930: '*ALTERNATIVE*'
c_code local build_ref_930
	ret_reg &ref[930]
	call_c   Dyam_Create_Atom("*ALTERNATIVE*")
	move_ret ref[930]
	c_ret

c_code local build_seed_154
	ret_reg &seed[154]
	call_c   build_ref_51()
	call_c   build_ref_739()
	call_c   Dyam_Seed_Start(&ref[51],&ref[739],I(0),fun0,1)
	call_c   build_ref_740()
	call_c   Dyam_Seed_Add_Comp(&ref[740],&ref[739],0)
	call_c   Dyam_Seed_End()
	move_ret seed[154]
	c_ret

;; TERM 740: '*GUARD*'(dcg_body_to_lpda_handler(_B, {_X , _O = _Q}, _D, _C1, _F, _D1, _O1, _I, _J)) :> '$$HOLE$$'
c_code local build_ref_740
	ret_reg &ref[740]
	call_c   build_ref_739()
	call_c   Dyam_Create_Binary(I(9),&ref[739],I(7))
	move_ret ref[740]
	c_ret

;; TERM 739: '*GUARD*'(dcg_body_to_lpda_handler(_B, {_X , _O = _Q}, _D, _C1, _F, _D1, _O1, _I, _J))
c_code local build_ref_739
	ret_reg &ref[739]
	call_c   build_ref_51()
	call_c   build_ref_738()
	call_c   Dyam_Create_Unary(&ref[51],&ref[738])
	move_ret ref[739]
	c_ret

;; TERM 738: dcg_body_to_lpda_handler(_B, {_X , _O = _Q}, _D, _C1, _F, _D1, _O1, _I, _J)
c_code local build_ref_738
	ret_reg &ref[738]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_869()
	call_c   Dyam_Create_Binary(&ref[869],V(14),V(16))
	move_ret R(0)
	call_c   Dyam_Create_Binary(I(4),V(23),R(0))
	move_ret R(0)
	call_c   Dyam_Create_Unary(I(2),R(0))
	move_ret R(0)
	call_c   build_ref_828()
	call_c   Dyam_Term_Start(&ref[828],9)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(28))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(29))
	call_c   Dyam_Term_Arg(V(40))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret ref[738]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun231[20]=[19,build_ref_705,build_ref_707,build_ref_708,build_ref_709,build_ref_710,build_ref_711,build_seed_147,build_seed_148,build_seed_149,build_ref_721,build_ref_722,build_ref_723,build_seed_150,build_seed_151,build_ref_730,build_seed_152,build_seed_153,build_ref_737,build_seed_154]

pl_code local fun231
	call_c   Dyam_Pool(pool_fun231)
	call_c   Dyam_Unify_Item(&ref[705])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(2))
	move     &ref[707], R(2)
	move     S(5), R(3)
	pl_call  pred_kleene_normalize_2()
	move     &ref[708], R(0)
	move     0, R(1)
	move     V(19), R(2)
	move     S(5), R(3)
	pl_call  pred_update_counter_2()
	move     &ref[709], R(0)
	move     0, R(1)
	move     &ref[710], R(2)
	move     S(5), R(3)
	move     V(20), R(4)
	move     S(5), R(5)
	pl_call  pred_name_builder_3()
	pl_call  Object_1(&ref[711])
	pl_call  fun7(&seed[147],1)
	pl_call  fun7(&seed[148],1)
	pl_call  fun7(&seed[149],1)
	move     &ref[721], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(30), R(4)
	move     S(5), R(5)
	pl_call  pred_my_numbervars_3()
	call_c   Dyam_Unify(V(31),&ref[722])
	fail_ret
	call_c   Dyam_Unify(V(32),&ref[723])
	fail_ret
	pl_call  fun7(&seed[150],1)
	pl_call  fun7(&seed[151],1)
	call_c   Dyam_Reg_Load(0,V(11))
	move     V(35), R(2)
	move     S(5), R(3)
	pl_call  pred_tupple_2()
	call_c   Dyam_Reg_Load(0,V(7))
	move     V(36), R(2)
	move     S(5), R(3)
	pl_call  pred_tupple_2()
	move     &ref[730], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(36))
	move     V(37), R(4)
	move     S(5), R(5)
	pl_call  pred_extend_tupple_3()
	pl_call  fun7(&seed[152],1)
	pl_call  fun7(&seed[153],1)
	call_c   Dyam_Unify(V(40),&ref[737])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_jump  fun7(&seed[154],1)

;; TERM 706: '*GUARD*'(dcg_body_to_lpda_handler(_B, _C, _D, _E, _F, _G, _H, _I, _J)) :> []
c_code local build_ref_706
	ret_reg &ref[706]
	call_c   build_ref_705()
	call_c   Dyam_Create_Binary(I(9),&ref[705],I(0))
	move_ret ref[706]
	c_ret

c_code local build_seed_51
	ret_reg &seed[51]
	call_c   build_ref_50()
	call_c   build_ref_673()
	call_c   Dyam_Seed_Start(&ref[50],&ref[673],I(0),fun5,1)
	call_c   build_ref_672()
	call_c   Dyam_Seed_Add_Comp(&ref[672],fun230,0)
	call_c   Dyam_Seed_End()
	move_ret seed[51]
	c_ret

;; TERM 672: '*GUARD*'(dcg_try_lc_transform(_B, (_C / _D), _E, _F, _G, _H))
c_code local build_ref_672
	ret_reg &ref[672]
	call_c   build_ref_51()
	call_c   build_ref_671()
	call_c   Dyam_Create_Unary(&ref[51],&ref[671])
	move_ret ref[672]
	c_ret

;; TERM 671: dcg_try_lc_transform(_B, (_C / _D), _E, _F, _G, _H)
c_code local build_ref_671
	ret_reg &ref[671]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_840()
	call_c   Dyam_Create_Binary(&ref[840],V(2),V(3))
	move_ret R(0)
	call_c   build_ref_898()
	call_c   Dyam_Term_Start(&ref[898],6)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[671]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 700: _E :> _F
c_code local build_ref_700
	ret_reg &ref[700]
	call_c   Dyam_Create_Binary(I(9),V(4),V(5))
	move_ret ref[700]
	c_ret

pl_code local fun228
	call_c   Dyam_Remove_Choice()
	pl_jump  fun132()

;; TERM 701: compiler_analyzer(lc)
c_code local build_ref_701
	ret_reg &ref[701]
	call_c   build_ref_931()
	call_c   build_ref_932()
	call_c   Dyam_Create_Unary(&ref[931],&ref[932])
	move_ret ref[701]
	c_ret

;; TERM 932: lc
c_code local build_ref_932
	ret_reg &ref[932]
	call_c   Dyam_Create_Atom("lc")
	move_ret ref[932]
	c_ret

;; TERM 931: compiler_analyzer
c_code local build_ref_931
	ret_reg &ref[931]
	call_c   Dyam_Create_Atom("compiler_analyzer")
	move_ret ref[931]
	c_ret

;; TERM 702: tmp_terminal(_R1)
c_code local build_ref_702
	ret_reg &ref[702]
	call_c   build_ref_933()
	call_c   Dyam_Create_Unary(&ref[933],V(43))
	move_ret ref[702]
	c_ret

;; TERM 933: tmp_terminal
c_code local build_ref_933
	ret_reg &ref[933]
	call_c   Dyam_Create_Atom("tmp_terminal")
	move_ret ref[933]
	c_ret

;; TERM 703: tmp_lc(_R1, _R1)
c_code local build_ref_703
	ret_reg &ref[703]
	call_c   build_ref_934()
	call_c   Dyam_Create_Binary(&ref[934],V(43),V(43))
	move_ret ref[703]
	c_ret

;; TERM 934: tmp_lc
c_code local build_ref_934
	ret_reg &ref[934]
	call_c   Dyam_Create_Atom("tmp_lc")
	move_ret ref[934]
	c_ret

long local pool_fun229[6]=[5,build_ref_700,build_ref_674,build_ref_701,build_ref_702,build_ref_703]

pl_code local fun229
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(7),&ref[700])
	fail_ret
	call_c   Dyam_Choice(fun228)
	call_c   Dyam_Set_Cut()
	move     &ref[674], R(0)
	move     S(5), R(1)
	pl_call  pred_check_lc_1()
	pl_call  Object_1(&ref[701])
	call_c   Dyam_Cut()
	call_c   DYAM_Copy_Numbervars_2(V(6),V(43))
	fail_ret
	move     &ref[702], R(0)
	move     S(5), R(1)
	pl_call  pred_record_without_doublon_1()
	move     &ref[703], R(0)
	move     S(5), R(1)
	pl_call  pred_record_without_doublon_1()
	pl_jump  fun132()

;; TERM 675: '*WRAPPER-CALL*'(lc_dcg((_I / _J)), _K, _L)
c_code local build_ref_675
	ret_reg &ref[675]
	call_c   build_ref_935()
	call_c   build_ref_692()
	call_c   Dyam_Term_Start(&ref[935],3)
	call_c   Dyam_Term_Arg(&ref[692])
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_End()
	move_ret ref[675]
	c_ret

;; TERM 692: lc_dcg((_I / _J))
c_code local build_ref_692
	ret_reg &ref[692]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_840()
	call_c   Dyam_Create_Binary(&ref[840],V(8),V(9))
	move_ret R(0)
	call_c   build_ref_921()
	call_c   Dyam_Create_Unary(&ref[921],R(0))
	move_ret ref[692]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 935: '*WRAPPER-CALL*'
c_code local build_ref_935
	ret_reg &ref[935]
	call_c   Dyam_Create_Atom("*WRAPPER-CALL*")
	move_ret ref[935]
	c_ret

;; TERM 699: '$CLOSURE'('$fun'(227, 0, 1148829344), '$TUPPLE'(35074877686696))
c_code local build_ref_699
	ret_reg &ref[699]
	call_c   build_ref_698()
	call_c   Dyam_Closure_Aux(fun227,&ref[698])
	move_ret ref[699]
	c_ret

;; TERM 696: '$CLOSURE'('$fun'(226, 0, 1148818564), '$TUPPLE'(35074877686584))
c_code local build_ref_696
	ret_reg &ref[696]
	call_c   build_ref_695()
	call_c   Dyam_Closure_Aux(fun226,&ref[695])
	move_ret ref[696]
	c_ret

c_code local build_seed_144
	ret_reg &seed[144]
	call_c   build_ref_51()
	call_c   build_ref_677()
	call_c   Dyam_Seed_Start(&ref[51],&ref[677],I(0),fun0,1)
	call_c   build_ref_678()
	call_c   Dyam_Seed_Add_Comp(&ref[678],&ref[677],0)
	call_c   Dyam_Seed_End()
	move_ret seed[144]
	c_ret

;; TERM 678: '*GUARD*'(make_dcg_callret(_O, _P, _Q, _M, _N, _R, _S)) :> '$$HOLE$$'
c_code local build_ref_678
	ret_reg &ref[678]
	call_c   build_ref_677()
	call_c   Dyam_Create_Binary(I(9),&ref[677],I(7))
	move_ret ref[678]
	c_ret

;; TERM 677: '*GUARD*'(make_dcg_callret(_O, _P, _Q, _M, _N, _R, _S))
c_code local build_ref_677
	ret_reg &ref[677]
	call_c   build_ref_51()
	call_c   build_ref_676()
	call_c   Dyam_Create_Unary(&ref[51],&ref[676])
	move_ret ref[677]
	c_ret

;; TERM 676: make_dcg_callret(_O, _P, _Q, _M, _N, _R, _S)
c_code local build_ref_676
	ret_reg &ref[676]
	call_c   build_ref_895()
	call_c   Dyam_Term_Start(&ref[895],7)
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_End()
	move_ret ref[676]
	c_ret

;; TERM 679: bmg_wrap_args(_O, _P, _Q, _T, _M, _N, _K)
c_code local build_ref_679
	ret_reg &ref[679]
	call_c   build_ref_907()
	call_c   Dyam_Term_Start(&ref[907],7)
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_End()
	move_ret ref[679]
	c_ret

c_code local build_seed_145
	ret_reg &seed[145]
	call_c   build_ref_51()
	call_c   build_ref_682()
	call_c   Dyam_Seed_Start(&ref[51],&ref[682],I(0),fun0,1)
	call_c   build_ref_683()
	call_c   Dyam_Seed_Add_Comp(&ref[683],&ref[682],0)
	call_c   Dyam_Seed_End()
	move_ret seed[145]
	c_ret

;; TERM 683: '*GUARD*'(body_to_lpda(_B, (internal_reverse_lc(_G, _V) , domain(_W, _V)), noop, _Y, rec_prolog)) :> '$$HOLE$$'
c_code local build_ref_683
	ret_reg &ref[683]
	call_c   build_ref_682()
	call_c   Dyam_Create_Binary(I(9),&ref[682],I(7))
	move_ret ref[683]
	c_ret

;; TERM 682: '*GUARD*'(body_to_lpda(_B, (internal_reverse_lc(_G, _V) , domain(_W, _V)), noop, _Y, rec_prolog))
c_code local build_ref_682
	ret_reg &ref[682]
	call_c   build_ref_51()
	call_c   build_ref_681()
	call_c   Dyam_Create_Unary(&ref[51],&ref[681])
	move_ret ref[682]
	c_ret

;; TERM 681: body_to_lpda(_B, (internal_reverse_lc(_G, _V) , domain(_W, _V)), noop, _Y, rec_prolog)
c_code local build_ref_681
	ret_reg &ref[681]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_936()
	call_c   Dyam_Create_Binary(&ref[936],V(6),V(21))
	move_ret R(0)
	call_c   build_ref_923()
	call_c   Dyam_Create_Binary(&ref[923],V(22),V(21))
	move_ret R(1)
	call_c   Dyam_Create_Binary(I(4),R(0),R(1))
	move_ret R(1)
	call_c   build_ref_875()
	call_c   build_ref_179()
	call_c   build_ref_248()
	call_c   Dyam_Term_Start(&ref[875],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(&ref[179])
	call_c   Dyam_Term_Arg(V(24))
	call_c   Dyam_Term_Arg(&ref[248])
	call_c   Dyam_Term_End()
	move_ret ref[681]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 936: internal_reverse_lc
c_code local build_ref_936
	ret_reg &ref[936]
	call_c   Dyam_Create_Atom("internal_reverse_lc")
	move_ret ref[936]
	c_ret

;; TERM 684: internal_lc_trigger(_B, _P, _Q, _S, _R, _G)
c_code local build_ref_684
	ret_reg &ref[684]
	call_c   build_ref_937()
	call_c   Dyam_Term_Start(&ref[937],6)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[684]
	c_ret

;; TERM 937: internal_lc_trigger
c_code local build_ref_937
	ret_reg &ref[937]
	call_c   Dyam_Create_Atom("internal_lc_trigger")
	move_ret ref[937]
	c_ret

c_code local build_seed_146
	ret_reg &seed[146]
	call_c   build_ref_51()
	call_c   build_ref_686()
	call_c   Dyam_Seed_Start(&ref[51],&ref[686],I(0),fun0,1)
	call_c   build_ref_687()
	call_c   Dyam_Seed_Add_Comp(&ref[687],&ref[686],0)
	call_c   Dyam_Seed_End()
	move_ret seed[146]
	c_ret

;; TERM 687: '*GUARD*'(body_to_lpda(_B, (recorded(_Z) -> fail ; record(_Z)), _L, _A1, rec_prolog)) :> '$$HOLE$$'
c_code local build_ref_687
	ret_reg &ref[687]
	call_c   build_ref_686()
	call_c   Dyam_Create_Binary(I(9),&ref[686],I(7))
	move_ret ref[687]
	c_ret

;; TERM 686: '*GUARD*'(body_to_lpda(_B, (recorded(_Z) -> fail ; record(_Z)), _L, _A1, rec_prolog))
c_code local build_ref_686
	ret_reg &ref[686]
	call_c   build_ref_51()
	call_c   build_ref_685()
	call_c   Dyam_Create_Unary(&ref[51],&ref[685])
	move_ret ref[686]
	c_ret

;; TERM 685: body_to_lpda(_B, (recorded(_Z) -> fail ; record(_Z)), _L, _A1, rec_prolog)
c_code local build_ref_685
	ret_reg &ref[685]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_938()
	call_c   Dyam_Create_Unary(&ref[938],V(25))
	move_ret R(0)
	call_c   build_ref_847()
	call_c   build_ref_829()
	call_c   Dyam_Create_Binary(&ref[847],R(0),&ref[829])
	move_ret R(0)
	call_c   build_ref_831()
	call_c   Dyam_Create_Unary(&ref[831],V(25))
	move_ret R(1)
	call_c   Dyam_Create_Binary(I(5),R(0),R(1))
	move_ret R(1)
	call_c   build_ref_875()
	call_c   build_ref_248()
	call_c   Dyam_Term_Start(&ref[875],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(26))
	call_c   Dyam_Term_Arg(&ref[248])
	call_c   Dyam_Term_End()
	move_ret ref[685]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 938: recorded
c_code local build_ref_938
	ret_reg &ref[938]
	call_c   Dyam_Create_Atom("recorded")
	move_ret ref[938]
	c_ret

;; TERM 690: '*RITEM*'(_R, _S) :> '*LCFIRST*'(_G, _A1, (_G ^ _W ^ _Y))(_G)(_U)
c_code local build_ref_690
	ret_reg &ref[690]
	call_c   build_ref_688()
	call_c   build_ref_689()
	call_c   Dyam_Create_Binary(I(9),&ref[688],&ref[689])
	move_ret ref[690]
	c_ret

;; TERM 689: '*LCFIRST*'(_G, _A1, (_G ^ _W ^ _Y))(_G)(_U)
c_code local build_ref_689
	ret_reg &ref[689]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_860()
	call_c   Dyam_Create_Binary(&ref[860],V(22),V(24))
	move_ret R(0)
	call_c   Dyam_Create_Binary(&ref[860],V(6),R(0))
	move_ret R(0)
	call_c   build_ref_939()
	call_c   Dyam_Term_Start(&ref[939],3)
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(26))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   Dyam_Create_Binary(I(3),R(0),V(6))
	move_ret R(0)
	call_c   Dyam_Create_Binary(I(3),R(0),V(20))
	move_ret ref[689]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 939: '*LCFIRST*'
c_code local build_ref_939
	ret_reg &ref[939]
	call_c   Dyam_Create_Atom("*LCFIRST*")
	move_ret ref[939]
	c_ret

;; TERM 688: '*RITEM*'(_R, _S)
c_code local build_ref_688
	ret_reg &ref[688]
	call_c   build_ref_0()
	call_c   Dyam_Create_Binary(&ref[0],V(17),V(18))
	move_ret ref[688]
	c_ret

;; TERM 691: '*OBJECT*'(seed{model=> _B1, id=> _C1, anchor=> _D1, subs_comp=> _E1, code=> _F1, go=> _G1, tabule=> yes, schedule=> no, subsume=> _H1, model_ref=> _I1, subs_comp_ref=> _J1, c_type=> _K1, c_type_ref=> _L1, out_env=> _M1, appinfo=> _N1, tail_flag=> _O1})
c_code local build_ref_691
	ret_reg &ref[691]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_941()
	call_c   build_ref_942()
	call_c   build_ref_943()
	call_c   Dyam_Term_Start(&ref[941],16)
	call_c   Dyam_Term_Arg(V(27))
	call_c   Dyam_Term_Arg(V(28))
	call_c   Dyam_Term_Arg(V(29))
	call_c   Dyam_Term_Arg(V(30))
	call_c   Dyam_Term_Arg(V(31))
	call_c   Dyam_Term_Arg(V(32))
	call_c   Dyam_Term_Arg(&ref[942])
	call_c   Dyam_Term_Arg(&ref[943])
	call_c   Dyam_Term_Arg(V(33))
	call_c   Dyam_Term_Arg(V(34))
	call_c   Dyam_Term_Arg(V(35))
	call_c   Dyam_Term_Arg(V(36))
	call_c   Dyam_Term_Arg(V(37))
	call_c   Dyam_Term_Arg(V(38))
	call_c   Dyam_Term_Arg(V(39))
	call_c   Dyam_Term_Arg(V(40))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_940()
	call_c   Dyam_Create_Unary(&ref[940],R(0))
	move_ret ref[691]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 940: '*OBJECT*'
c_code local build_ref_940
	ret_reg &ref[940]
	call_c   Dyam_Create_Atom("*OBJECT*")
	move_ret ref[940]
	c_ret

;; TERM 943: no
c_code local build_ref_943
	ret_reg &ref[943]
	call_c   Dyam_Create_Atom("no")
	move_ret ref[943]
	c_ret

;; TERM 942: yes
c_code local build_ref_942
	ret_reg &ref[942]
	call_c   Dyam_Create_Atom("yes")
	move_ret ref[942]
	c_ret

;; TERM 941: seed!'$ft'
c_code local build_ref_941
	ret_reg &ref[941]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_960()
	call_c   Dyam_Create_List(&ref[960],I(0))
	move_ret R(0)
	call_c   build_ref_959()
	call_c   Dyam_Create_List(&ref[959],R(0))
	move_ret R(0)
	call_c   build_ref_958()
	call_c   Dyam_Create_List(&ref[958],R(0))
	move_ret R(0)
	call_c   build_ref_957()
	call_c   Dyam_Create_List(&ref[957],R(0))
	move_ret R(0)
	call_c   build_ref_956()
	call_c   Dyam_Create_List(&ref[956],R(0))
	move_ret R(0)
	call_c   build_ref_955()
	call_c   Dyam_Create_List(&ref[955],R(0))
	move_ret R(0)
	call_c   build_ref_954()
	call_c   Dyam_Create_List(&ref[954],R(0))
	move_ret R(0)
	call_c   build_ref_953()
	call_c   Dyam_Create_List(&ref[953],R(0))
	move_ret R(0)
	call_c   build_ref_952()
	call_c   Dyam_Create_List(&ref[952],R(0))
	move_ret R(0)
	call_c   build_ref_951()
	call_c   Dyam_Create_List(&ref[951],R(0))
	move_ret R(0)
	call_c   build_ref_950()
	call_c   Dyam_Create_List(&ref[950],R(0))
	move_ret R(0)
	call_c   build_ref_949()
	call_c   Dyam_Create_List(&ref[949],R(0))
	move_ret R(0)
	call_c   build_ref_948()
	call_c   Dyam_Create_List(&ref[948],R(0))
	move_ret R(0)
	call_c   build_ref_947()
	call_c   Dyam_Create_List(&ref[947],R(0))
	move_ret R(0)
	call_c   build_ref_946()
	call_c   Dyam_Create_List(&ref[946],R(0))
	move_ret R(0)
	call_c   build_ref_945()
	call_c   Dyam_Create_List(&ref[945],R(0))
	move_ret R(0)
	call_c   build_ref_944()
	call_c   Dyam_Create_Atom_Module("$ft",&ref[944])
	move_ret ref[941]
	call_c   DYAM_Feature_2(&ref[944],R(0))
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 944: seed
c_code local build_ref_944
	ret_reg &ref[944]
	call_c   Dyam_Create_Atom("seed")
	move_ret ref[944]
	c_ret

;; TERM 945: model
c_code local build_ref_945
	ret_reg &ref[945]
	call_c   Dyam_Create_Atom("model")
	move_ret ref[945]
	c_ret

;; TERM 946: id
c_code local build_ref_946
	ret_reg &ref[946]
	call_c   Dyam_Create_Atom("id")
	move_ret ref[946]
	c_ret

;; TERM 947: anchor
c_code local build_ref_947
	ret_reg &ref[947]
	call_c   Dyam_Create_Atom("anchor")
	move_ret ref[947]
	c_ret

;; TERM 948: subs_comp
c_code local build_ref_948
	ret_reg &ref[948]
	call_c   Dyam_Create_Atom("subs_comp")
	move_ret ref[948]
	c_ret

;; TERM 949: code
c_code local build_ref_949
	ret_reg &ref[949]
	call_c   Dyam_Create_Atom("code")
	move_ret ref[949]
	c_ret

;; TERM 950: go
c_code local build_ref_950
	ret_reg &ref[950]
	call_c   Dyam_Create_Atom("go")
	move_ret ref[950]
	c_ret

;; TERM 951: tabule
c_code local build_ref_951
	ret_reg &ref[951]
	call_c   Dyam_Create_Atom("tabule")
	move_ret ref[951]
	c_ret

;; TERM 952: schedule
c_code local build_ref_952
	ret_reg &ref[952]
	call_c   Dyam_Create_Atom("schedule")
	move_ret ref[952]
	c_ret

;; TERM 953: subsume
c_code local build_ref_953
	ret_reg &ref[953]
	call_c   Dyam_Create_Atom("subsume")
	move_ret ref[953]
	c_ret

;; TERM 954: model_ref
c_code local build_ref_954
	ret_reg &ref[954]
	call_c   Dyam_Create_Atom("model_ref")
	move_ret ref[954]
	c_ret

;; TERM 955: subs_comp_ref
c_code local build_ref_955
	ret_reg &ref[955]
	call_c   Dyam_Create_Atom("subs_comp_ref")
	move_ret ref[955]
	c_ret

;; TERM 956: c_type
c_code local build_ref_956
	ret_reg &ref[956]
	call_c   Dyam_Create_Atom("c_type")
	move_ret ref[956]
	c_ret

;; TERM 957: c_type_ref
c_code local build_ref_957
	ret_reg &ref[957]
	call_c   Dyam_Create_Atom("c_type_ref")
	move_ret ref[957]
	c_ret

;; TERM 958: out_env
c_code local build_ref_958
	ret_reg &ref[958]
	call_c   Dyam_Create_Atom("out_env")
	move_ret ref[958]
	c_ret

;; TERM 959: appinfo
c_code local build_ref_959
	ret_reg &ref[959]
	call_c   Dyam_Create_Atom("appinfo")
	move_ret ref[959]
	c_ret

;; TERM 960: tail_flag
c_code local build_ref_960
	ret_reg &ref[960]
	call_c   Dyam_Create_Atom("tail_flag")
	move_ret ref[960]
	c_ret

;; TERM 693: _P1 - 1
c_code local build_ref_693
	ret_reg &ref[693]
	call_c   build_ref_961()
	call_c   Dyam_Create_Binary(&ref[961],V(41),N(1))
	move_ret ref[693]
	c_ret

;; TERM 961: -
c_code local build_ref_961
	ret_reg &ref[961]
	call_c   Dyam_Create_Atom("-")
	move_ret ref[961]
	c_ret

long local pool_fun226[11]=[10,build_seed_144,build_ref_679,build_ref_680,build_seed_145,build_ref_684,build_seed_146,build_ref_690,build_ref_691,build_ref_693,build_ref_692]

pl_code local fun226
	call_c   Dyam_Pool(pool_fun226)
	call_c   DYAM_evpred_functor(V(14),V(8),V(9))
	fail_ret
	call_c   Dyam_Allocate(0)
	pl_call  fun7(&seed[144],1)
	pl_call  Object_1(&ref[679])
	call_c   Dyam_Reg_Load(0,V(10))
	move     V(20), R(2)
	move     S(5), R(3)
	pl_call  pred_tupple_2()
	move     &ref[680], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(23), R(4)
	move     S(5), R(5)
	pl_call  pred_my_numbervars_3()
	pl_call  fun7(&seed[145],1)
	call_c   Dyam_Unify(V(25),&ref[684])
	fail_ret
	pl_call  fun7(&seed[146],1)
	call_c   Dyam_Unify(V(27),&ref[690])
	fail_ret
	call_c   Dyam_Unify(V(7),&ref[691])
	fail_ret
	move     &ref[692], R(0)
	move     S(5), R(1)
	move     V(41), R(2)
	move     S(5), R(3)
	pl_call  pred_value_counter_2()
	call_c   DYAM_evpred_is(V(42),&ref[693])
	fail_ret
	move     &ref[692], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(42))
	pl_call  pred_reset_counter_2()
	pl_jump  fun132()

;; TERM 695: '$TUPPLE'(35074877686584)
c_code local build_ref_695
	ret_reg &ref[695]
	call_c   Dyam_Create_Simple_Tupple(0,142573568)
	move_ret ref[695]
	c_ret

long local pool_fun227[2]=[1,build_ref_696]

pl_code local fun227
	call_c   Dyam_Pool(pool_fun227)
	call_c   Dyam_Allocate(0)
	move     &ref[696], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	move     V(13), R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  fun23()

;; TERM 698: '$TUPPLE'(35074877686696)
c_code local build_ref_698
	ret_reg &ref[698]
	call_c   Dyam_Create_Simple_Tupple(0,142540800)
	move_ret ref[698]
	c_ret

long local pool_fun230[6]=[65540,build_ref_672,build_ref_674,build_ref_675,build_ref_699,pool_fun229]

pl_code local fun230
	call_c   Dyam_Pool(pool_fun230)
	call_c   Dyam_Unify_Item(&ref[672])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun229)
	call_c   Dyam_Set_Cut()
	move     &ref[674], R(0)
	move     S(5), R(1)
	pl_call  pred_check_lc_1()
	call_c   Dyam_Unify(V(5),&ref[675])
	fail_ret
	call_c   Dyam_Cut()
	move     &ref[699], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	move     V(12), R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  fun23()

;; TERM 673: '*GUARD*'(dcg_try_lc_transform(_B, (_C / _D), _E, _F, _G, _H)) :> []
c_code local build_ref_673
	ret_reg &ref[673]
	call_c   build_ref_672()
	call_c   Dyam_Create_Binary(I(9),&ref[672],I(0))
	move_ret ref[673]
	c_ret

c_code local build_seed_11
	ret_reg &seed[11]
	call_c   build_ref_50()
	call_c   build_ref_743()
	call_c   Dyam_Seed_Start(&ref[50],&ref[743],I(0),fun5,1)
	call_c   build_ref_742()
	call_c   Dyam_Seed_Add_Comp(&ref[742],fun254,0)
	call_c   Dyam_Seed_End()
	move_ret seed[11]
	c_ret

;; TERM 742: '*GUARD*'(non_terminal_to_lpda(_B, _C, _D, _E, _F, _G, _H, _I, _J))
c_code local build_ref_742
	ret_reg &ref[742]
	call_c   build_ref_51()
	call_c   build_ref_741()
	call_c   Dyam_Create_Unary(&ref[51],&ref[741])
	move_ret ref[742]
	c_ret

;; TERM 741: non_terminal_to_lpda(_B, _C, _D, _E, _F, _G, _H, _I, _J)
c_code local build_ref_741
	ret_reg &ref[741]
	call_c   build_ref_918()
	call_c   Dyam_Term_Start(&ref[918],9)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret ref[741]
	c_ret

;; TERM 744: dcg _M / _N
c_code local build_ref_744
	ret_reg &ref[744]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_840()
	call_c   Dyam_Create_Binary(&ref[840],V(12),V(13))
	move_ret R(0)
	call_c   build_ref_839()
	call_c   Dyam_Create_Unary(&ref[839],R(0))
	move_ret ref[744]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_158
	ret_reg &seed[158]
	call_c   build_ref_51()
	call_c   build_ref_778()
	call_c   Dyam_Seed_Start(&ref[51],&ref[778],I(0),fun0,1)
	call_c   build_ref_779()
	call_c   Dyam_Seed_Add_Comp(&ref[779],&ref[778],0)
	call_c   Dyam_Seed_End()
	move_ret seed[158]
	c_ret

;; TERM 779: '*GUARD*'(make_dcg_callret(_L, _D, _E, _F, _G, _R, _S)) :> '$$HOLE$$'
c_code local build_ref_779
	ret_reg &ref[779]
	call_c   build_ref_778()
	call_c   Dyam_Create_Binary(I(9),&ref[778],I(7))
	move_ret ref[779]
	c_ret

;; TERM 778: '*GUARD*'(make_dcg_callret(_L, _D, _E, _F, _G, _R, _S))
c_code local build_ref_778
	ret_reg &ref[778]
	call_c   build_ref_51()
	call_c   build_ref_777()
	call_c   Dyam_Create_Unary(&ref[51],&ref[777])
	move_ret ref[778]
	c_ret

;; TERM 777: make_dcg_callret(_L, _D, _E, _F, _G, _R, _S)
c_code local build_ref_777
	ret_reg &ref[777]
	call_c   build_ref_895()
	call_c   Dyam_Term_Start(&ref[895],7)
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_End()
	move_ret ref[777]
	c_ret

;; TERM 753: bmg_wrap_args(_L, _D, _E, _K, _F, _G, _P)
c_code local build_ref_753
	ret_reg &ref[753]
	call_c   build_ref_907()
	call_c   Dyam_Term_Start(&ref[907],7)
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_End()
	move_ret ref[753]
	c_ret

;; TERM 787: '$CLOSURE'('$fun'(245, 0, 1149045288), '$TUPPLE'(35074870873440))
c_code local build_ref_787
	ret_reg &ref[787]
	call_c   build_ref_786()
	call_c   Dyam_Closure_Aux(fun245,&ref[786])
	move_ret ref[787]
	c_ret

;; TERM 784: '*WRAPPER-CALL*'(_T, _P, _H)
c_code local build_ref_784
	ret_reg &ref[784]
	call_c   build_ref_935()
	call_c   Dyam_Term_Start(&ref[935],3)
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[784]
	c_ret

long local pool_fun245[2]=[1,build_ref_784]

pl_code local fun245
	call_c   Dyam_Pool(pool_fun245)
	call_c   Dyam_Unify(V(8),&ref[784])
	fail_ret
	call_c   Dyam_Allocate(0)
	pl_jump  fun132()

;; TERM 786: '$TUPPLE'(35074870873440)
c_code local build_ref_786
	ret_reg &ref[786]
	call_c   Dyam_Create_Simple_Tupple(0,3154432)
	move_ret ref[786]
	c_ret

long local pool_fun246[2]=[1,build_ref_787]

long local pool_fun247[3]=[65537,build_ref_744,pool_fun246]

pl_code local fun247
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(19),&ref[744])
	fail_ret
fun246:
	move     &ref[787], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Load(4,V(19))
	call_c   Dyam_Reg_Deallocate(3)
fun235:
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Reg_Bind(2,V(3))
	call_c   Dyam_Reg_Bind(4,V(2))
	call_c   Dyam_Choice(fun234)
	pl_call  fun13(&seed[155],1)
	pl_fail



;; TERM 783: lc_dcg((_M / _N))
c_code local build_ref_783
	ret_reg &ref[783]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_840()
	call_c   Dyam_Create_Binary(&ref[840],V(12),V(13))
	move_ret R(0)
	call_c   build_ref_921()
	call_c   Dyam_Create_Unary(&ref[921],R(0))
	move_ret ref[783]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun248[6]=[131075,build_ref_753,build_ref_744,build_ref_783,pool_fun247,pool_fun246]

pl_code local fun248
	call_c   Dyam_Remove_Choice()
	pl_call  Object_1(&ref[753])
	call_c   Dyam_Choice(fun247)
	call_c   Dyam_Set_Cut()
	move     &ref[744], R(0)
	move     S(5), R(1)
	pl_call  pred_check_lc_1()
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(19),&ref[783])
	fail_ret
	call_c   Dyam_Reg_Load(0,V(19))
	move     V(20), R(2)
	move     S(5), R(3)
	pl_call  pred_update_counter_2()
	pl_jump  fun246()

long local pool_fun249[4]=[65538,build_seed_158,build_ref_744,pool_fun248]

pl_code local fun249
	call_c   Dyam_Remove_Choice()
	pl_call  fun7(&seed[158],1)
	call_c   Dyam_Choice(fun248)
	call_c   Dyam_Set_Cut()
	move     &ref[744], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(10))
	call_c   Dyam_Reg_Load(4,V(17))
	call_c   Dyam_Reg_Load(6,V(18))
	call_c   Dyam_Reg_Load(8,V(7))
	call_c   Dyam_Reg_Load(10,V(8))
	pl_call  pred_try_lco_6()
	call_c   Dyam_Cut()
	pl_jump  fun132()

;; TERM 776: answer(_Q)
c_code local build_ref_776
	ret_reg &ref[776]
	call_c   build_ref_863()
	call_c   Dyam_Create_Unary(&ref[863],V(16))
	move_ret ref[776]
	c_ret

;; TERM 782: '*PSEUDONEXT*'(_R, _S, _H, _K)
c_code local build_ref_782
	ret_reg &ref[782]
	call_c   build_ref_962()
	call_c   Dyam_Term_Start(&ref[962],4)
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_End()
	move_ret ref[782]
	c_ret

;; TERM 962: '*PSEUDONEXT*'
c_code local build_ref_962
	ret_reg &ref[962]
	call_c   Dyam_Create_Atom("*PSEUDONEXT*")
	move_ret ref[962]
	c_ret

long local pool_fun244[2]=[1,build_ref_782]

pl_code local fun244
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(8),&ref[782])
	fail_ret
	pl_jump  fun132()

;; TERM 775: light_tabular dcg _M / _N
c_code local build_ref_775
	ret_reg &ref[775]
	call_c   build_ref_899()
	call_c   build_ref_744()
	call_c   Dyam_Create_Unary(&ref[899],&ref[744])
	move_ret ref[775]
	c_ret

;; TERM 781: '*PSEUDOLIGHTNEXT*'(_R, _S, (deallocate :> succeed), _K) :> _H
c_code local build_ref_781
	ret_reg &ref[781]
	call_c   build_ref_780()
	call_c   Dyam_Create_Binary(I(9),&ref[780],V(7))
	move_ret ref[781]
	c_ret

;; TERM 780: '*PSEUDOLIGHTNEXT*'(_R, _S, (deallocate :> succeed), _K)
c_code local build_ref_780
	ret_reg &ref[780]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_964()
	call_c   build_ref_965()
	call_c   Dyam_Create_Binary(I(9),&ref[964],&ref[965])
	move_ret R(0)
	call_c   build_ref_963()
	call_c   Dyam_Term_Start(&ref[963],4)
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_End()
	move_ret ref[780]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 963: '*PSEUDOLIGHTNEXT*'
c_code local build_ref_963
	ret_reg &ref[963]
	call_c   Dyam_Create_Atom("*PSEUDOLIGHTNEXT*")
	move_ret ref[963]
	c_ret

;; TERM 965: succeed
c_code local build_ref_965
	ret_reg &ref[965]
	call_c   Dyam_Create_Atom("succeed")
	move_ret ref[965]
	c_ret

;; TERM 964: deallocate
c_code local build_ref_964
	ret_reg &ref[964]
	call_c   Dyam_Create_Atom("deallocate")
	move_ret ref[964]
	c_ret

long local pool_fun250[8]=[131077,build_ref_776,build_ref_744,build_seed_158,build_ref_775,build_ref_781,pool_fun249,pool_fun244]

pl_code local fun250
	call_c   Dyam_Update_Choice(fun249)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(9),&ref[776])
	fail_ret
	call_c   Dyam_Cut()
	move     &ref[744], R(0)
	move     S(5), R(1)
	pl_call  pred_registered_predicate_1()
	pl_call  fun7(&seed[158],1)
	call_c   Dyam_Choice(fun244)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[775])
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(8),&ref[781])
	fail_ret
	pl_jump  fun132()

;; TERM 766: 'Not a recursive prolog dcg predicate ~w'
c_code local build_ref_766
	ret_reg &ref[766]
	call_c   Dyam_Create_Atom("Not a recursive prolog dcg predicate ~w")
	move_ret ref[766]
	c_ret

;; TERM 767: [_L]
c_code local build_ref_767
	ret_reg &ref[767]
	call_c   Dyam_Create_List(V(11),I(0))
	move_ret ref[767]
	c_ret

long local pool_fun242[3]=[2,build_ref_766,build_ref_767]

pl_code local fun243
	call_c   Dyam_Remove_Choice()
fun242:
	call_c   Dyam_Cut()
	move     &ref[766], R(0)
	move     0, R(1)
	move     &ref[767], R(2)
	move     S(5), R(3)
	pl_call  pred_error_2()
	pl_jump  fun132()


long local pool_fun251[5]=[131074,build_ref_248,build_ref_775,pool_fun250,pool_fun242]

pl_code local fun251
	call_c   Dyam_Update_Choice(fun250)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(9),&ref[248])
	fail_ret
	call_c   Dyam_Choice(fun243)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[775])
	call_c   Dyam_Cut()
	pl_fail

;; TERM 768: extensional dcg _M / _N
c_code local build_ref_768
	ret_reg &ref[768]
	call_c   build_ref_966()
	call_c   build_ref_744()
	call_c   Dyam_Create_Unary(&ref[966],&ref[744])
	move_ret ref[768]
	c_ret

;; TERM 966: extensional
c_code local build_ref_966
	ret_reg &ref[966]
	call_c   Dyam_Create_Atom("extensional")
	move_ret ref[966]
	c_ret

;; TERM 774: '$CLOSURE'('$fun'(241, 0, 1149014964), '$TUPPLE'(35074877687408))
c_code local build_ref_774
	ret_reg &ref[774]
	call_c   build_ref_773()
	call_c   Dyam_Closure_Aux(fun241,&ref[773])
	move_ret ref[774]
	c_ret

c_code local build_seed_157
	ret_reg &seed[157]
	call_c   build_ref_51()
	call_c   build_ref_770()
	call_c   Dyam_Seed_Start(&ref[51],&ref[770],I(0),fun0,1)
	call_c   build_ref_771()
	call_c   Dyam_Seed_Add_Comp(&ref[771],&ref[770],0)
	call_c   Dyam_Seed_End()
	move_ret seed[157]
	c_ret

;; TERM 771: '*GUARD*'(litteral_to_lpda(_B, recorded(_O), _H, _I, _J)) :> '$$HOLE$$'
c_code local build_ref_771
	ret_reg &ref[771]
	call_c   build_ref_770()
	call_c   Dyam_Create_Binary(I(9),&ref[770],I(7))
	move_ret ref[771]
	c_ret

;; TERM 770: '*GUARD*'(litteral_to_lpda(_B, recorded(_O), _H, _I, _J))
c_code local build_ref_770
	ret_reg &ref[770]
	call_c   build_ref_51()
	call_c   build_ref_769()
	call_c   Dyam_Create_Unary(&ref[51],&ref[769])
	move_ret ref[770]
	c_ret

;; TERM 769: litteral_to_lpda(_B, recorded(_O), _H, _I, _J)
c_code local build_ref_769
	ret_reg &ref[769]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_938()
	call_c   Dyam_Create_Unary(&ref[938],V(14))
	move_ret R(0)
	call_c   build_ref_967()
	call_c   Dyam_Term_Start(&ref[967],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret ref[769]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 967: litteral_to_lpda
c_code local build_ref_967
	ret_reg &ref[967]
	call_c   Dyam_Create_Atom("litteral_to_lpda")
	move_ret ref[967]
	c_ret

long local pool_fun241[2]=[1,build_seed_157]

pl_code local fun241
	call_c   Dyam_Pool(pool_fun241)
	call_c   Dyam_Allocate(0)
	pl_call  fun7(&seed[157],1)
	pl_jump  fun132()

;; TERM 773: '$TUPPLE'(35074877687408)
c_code local build_ref_773
	ret_reg &ref[773]
	call_c   Dyam_Create_Simple_Tupple(0,137904128)
	move_ret ref[773]
	c_ret

;; TERM 751: dcg_prolog_make((dcg _M / _N), _L, _D, _E, _F, _G, _O)
c_code local build_ref_751
	ret_reg &ref[751]
	call_c   build_ref_902()
	call_c   build_ref_744()
	call_c   Dyam_Term_Start(&ref[902],7)
	call_c   Dyam_Term_Arg(&ref[744])
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_End()
	move_ret ref[751]
	c_ret

long local pool_fun252[5]=[65539,build_ref_768,build_ref_774,build_ref_751,pool_fun251]

pl_code local fun252
	call_c   Dyam_Update_Choice(fun251)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[768])
	call_c   Dyam_Cut()
	move     &ref[774], R(0)
	move     S(5), R(1)
	move     &ref[751], R(2)
	move     S(5), R(3)
	move     I(0), R(4)
	move     0, R(5)
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  fun161()

;; TERM 752: prolog dcg _M / _N
c_code local build_ref_752
	ret_reg &ref[752]
	call_c   build_ref_110()
	call_c   build_ref_744()
	call_c   Dyam_Create_Unary(&ref[110],&ref[744])
	move_ret ref[752]
	c_ret

;; TERM 764: '$CLOSURE'('$fun'(236, 0, 1148975644), '$TUPPLE'(35074877687308))
c_code local build_ref_764
	ret_reg &ref[764]
	call_c   build_ref_763()
	call_c   Dyam_Closure_Aux(fun236,&ref[763])
	move_ret ref[764]
	c_ret

;; TERM 761: '*WRAPPER-CALL*'(dcg_prolog((_M / _N)), _P, _H)
c_code local build_ref_761
	ret_reg &ref[761]
	call_c   build_ref_935()
	call_c   build_ref_765()
	call_c   Dyam_Term_Start(&ref[935],3)
	call_c   Dyam_Term_Arg(&ref[765])
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[761]
	c_ret

;; TERM 765: dcg_prolog((_M / _N))
c_code local build_ref_765
	ret_reg &ref[765]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_840()
	call_c   Dyam_Create_Binary(&ref[840],V(12),V(13))
	move_ret R(0)
	call_c   build_ref_906()
	call_c   Dyam_Create_Unary(&ref[906],R(0))
	move_ret ref[765]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun236[2]=[1,build_ref_761]

pl_code local fun236
	call_c   Dyam_Pool(pool_fun236)
	call_c   Dyam_Unify(V(8),&ref[761])
	fail_ret
	call_c   Dyam_Allocate(0)
	pl_jump  fun132()

;; TERM 763: '$TUPPLE'(35074877687308)
c_code local build_ref_763
	ret_reg &ref[763]
	call_c   Dyam_Create_Simple_Tupple(0,3252224)
	move_ret ref[763]
	c_ret

long local pool_fun237[4]=[3,build_ref_753,build_ref_764,build_ref_765]

long local pool_fun240[4]=[65538,build_ref_766,build_ref_767,pool_fun237]

pl_code local fun240
	call_c   Dyam_Remove_Choice()
	move     &ref[766], R(0)
	move     0, R(1)
	move     &ref[767], R(2)
	move     S(5), R(3)
	pl_call  pred_error_2()
fun237:
	pl_call  Object_1(&ref[753])
	move     &ref[764], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	move     &ref[765], R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  fun235()


pl_code local fun239
	call_c   Dyam_Remove_Choice()
fun238:
	call_c   Dyam_Cut()
	pl_jump  fun237()


long local pool_fun253[6]=[196610,build_ref_752,build_ref_248,pool_fun252,pool_fun240,pool_fun237]

pl_code local fun253
	call_c   Dyam_Update_Choice(fun252)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[752])
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun240)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Choice(fun239)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(9),&ref[248])
	fail_ret
	call_c   Dyam_Cut()
	pl_fail

;; TERM 745: rec_prolog dcg _M / _N
c_code local build_ref_745
	ret_reg &ref[745]
	call_c   build_ref_248()
	call_c   build_ref_744()
	call_c   Dyam_Create_Unary(&ref[248],&ref[744])
	move_ret ref[745]
	c_ret

;; TERM 750: '$CLOSURE'('$fun'(232, 0, 1148937960), '$TUPPLE'(35074877687140))
c_code local build_ref_750
	ret_reg &ref[750]
	call_c   build_ref_749()
	call_c   Dyam_Closure_Aux(fun232,&ref[749])
	move_ret ref[750]
	c_ret

;; TERM 747: '*GUARD*'(_O) :> _H
c_code local build_ref_747
	ret_reg &ref[747]
	call_c   build_ref_746()
	call_c   Dyam_Create_Binary(I(9),&ref[746],V(7))
	move_ret ref[747]
	c_ret

;; TERM 746: '*GUARD*'(_O)
c_code local build_ref_746
	ret_reg &ref[746]
	call_c   build_ref_51()
	call_c   Dyam_Create_Unary(&ref[51],V(14))
	move_ret ref[746]
	c_ret

long local pool_fun232[2]=[1,build_ref_747]

pl_code local fun232
	call_c   Dyam_Pool(pool_fun232)
	call_c   Dyam_Unify(V(8),&ref[747])
	fail_ret
	call_c   Dyam_Allocate(0)
	pl_jump  fun132()

;; TERM 749: '$TUPPLE'(35074877687140)
c_code local build_ref_749
	ret_reg &ref[749]
	call_c   Dyam_Create_Simple_Tupple(0,3162112)
	move_ret ref[749]
	c_ret

long local pool_fun254[7]=[65541,build_ref_742,build_ref_744,build_ref_745,build_ref_750,build_ref_751,pool_fun253]

pl_code local fun254
	call_c   Dyam_Pool(pool_fun254)
	call_c   Dyam_Unify_Item(&ref[742])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     V(10), R(0)
	move     S(5), R(1)
	move     V(11), R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Load(4,V(2))
	pl_call  pred_xtagop_3()
	call_c   DYAM_evpred_functor(V(11),V(12),V(13))
	fail_ret
	move     &ref[744], R(0)
	move     S(5), R(1)
	pl_call  pred_registered_predicate_1()
	call_c   Dyam_Choice(fun253)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[745])
	call_c   Dyam_Cut()
	move     &ref[750], R(0)
	move     S(5), R(1)
	move     &ref[751], R(2)
	move     S(5), R(3)
	move     I(0), R(4)
	move     0, R(5)
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  fun161()

;; TERM 743: '*GUARD*'(non_terminal_to_lpda(_B, _C, _D, _E, _F, _G, _H, _I, _J)) :> []
c_code local build_ref_743
	ret_reg &ref[743]
	call_c   build_ref_742()
	call_c   Dyam_Create_Binary(I(9),&ref[742],I(0))
	move_ret ref[743]
	c_ret

c_code local build_seed_19
	ret_reg &seed[19]
	call_c   build_ref_50()
	call_c   build_ref_790()
	call_c   Dyam_Seed_Start(&ref[50],&ref[790],I(0),fun5,1)
	call_c   build_ref_789()
	call_c   Dyam_Seed_Add_Comp(&ref[789],fun256,0)
	call_c   Dyam_Seed_End()
	move_ret seed[19]
	c_ret

;; TERM 789: '*GUARD*'(dcg_il_body_to_lpda_handler(_B, _C, _D, _E, _F, _G, _H, _I, _J, _K, _L))
c_code local build_ref_789
	ret_reg &ref[789]
	call_c   build_ref_51()
	call_c   build_ref_788()
	call_c   Dyam_Create_Unary(&ref[51],&ref[788])
	move_ret ref[789]
	c_ret

;; TERM 788: dcg_il_body_to_lpda_handler(_B, _C, _D, _E, _F, _G, _H, _I, _J, _K, _L)
c_code local build_ref_788
	ret_reg &ref[788]
	call_c   build_ref_835()
	call_c   Dyam_Term_Start(&ref[835],11)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_End()
	move_ret ref[788]
	c_ret

;; TERM 791: @*{goal=> _M, vars=> _N, from=> _O, to=> _P, collect_first=> _Q, collect_last=> _R, collect_loop=> _S, collect_next=> _T, collect_pred=> _U}
c_code local build_ref_791
	ret_reg &ref[791]
	call_c   build_ref_849()
	call_c   Dyam_Term_Start(&ref[849],9)
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_End()
	move_ret ref[791]
	c_ret

;; TERM 792: [_V]
c_code local build_ref_792
	ret_reg &ref[792]
	call_c   Dyam_Create_List(V(21),I(0))
	move_ret ref[792]
	c_ret

;; TERM 793: kleene_conditions([_O,_P], [_X,_Y], _Z, _A1, _B1)
c_code local build_ref_793
	ret_reg &ref[793]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Tupple(23,24,I(0))
	move_ret R(0)
	call_c   build_ref_927()
	call_c   build_ref_609()
	call_c   Dyam_Term_Start(&ref[927],5)
	call_c   Dyam_Term_Arg(&ref[609])
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(25))
	call_c   Dyam_Term_Arg(V(26))
	call_c   Dyam_Term_Arg(V(27))
	call_c   Dyam_Term_End()
	move_ret ref[793]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 794: [_X,_Y,_Z,_A1,_B1,@*{goal=> _M, vars=> _N, from=> _O, to=> _P, collect_first=> _Q, collect_last=> _R, collect_loop=> _S, collect_next=> _T, collect_pred=> _U}]
c_code local build_ref_794
	ret_reg &ref[794]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_791()
	call_c   Dyam_Create_List(&ref[791],I(0))
	move_ret R(0)
	call_c   Dyam_Create_Tupple(23,27,R(0))
	move_ret ref[794]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 795: [_G,_F,_D1,_E1,_F1]
c_code local build_ref_795
	ret_reg &ref[795]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Tupple(29,31,I(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(5),R(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(6),R(0))
	move_ret ref[795]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 825: '$CLOSURE'('$fun'(255, 0, 1149311216), '$TUPPLE'(35074877687864))
c_code local build_ref_825
	ret_reg &ref[825]
	call_c   build_ref_824()
	call_c   Dyam_Closure_Aux(fun255,&ref[824])
	move_ret ref[825]
	c_ret

c_code local build_seed_159
	ret_reg &seed[159]
	call_c   build_ref_51()
	call_c   build_ref_797()
	call_c   Dyam_Seed_Start(&ref[51],&ref[797],I(0),fun0,1)
	call_c   build_ref_798()
	call_c   Dyam_Seed_Add_Comp(&ref[798],&ref[797],0)
	call_c   Dyam_Seed_End()
	move_ret seed[159]
	c_ret

;; TERM 798: '*GUARD*'(position_and_stacks(_B, _I1, _J1)) :> '$$HOLE$$'
c_code local build_ref_798
	ret_reg &ref[798]
	call_c   build_ref_797()
	call_c   Dyam_Create_Binary(I(9),&ref[797],I(7))
	move_ret ref[798]
	c_ret

;; TERM 797: '*GUARD*'(position_and_stacks(_B, _I1, _J1))
c_code local build_ref_797
	ret_reg &ref[797]
	call_c   build_ref_51()
	call_c   build_ref_796()
	call_c   Dyam_Create_Unary(&ref[51],&ref[796])
	move_ret ref[797]
	c_ret

;; TERM 796: position_and_stacks(_B, _I1, _J1)
c_code local build_ref_796
	ret_reg &ref[796]
	call_c   build_ref_826()
	call_c   Dyam_Term_Start(&ref[826],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(34))
	call_c   Dyam_Term_Arg(V(35))
	call_c   Dyam_Term_End()
	move_ret ref[796]
	c_ret

;; TERM 799: [_X,_D1,_L1|_H1]
c_code local build_ref_799
	ret_reg &ref[799]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(37),V(33))
	move_ret R(0)
	call_c   Dyam_Create_List(V(29),R(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(23),R(0))
	move_ret ref[799]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 800: [_Y,_F1,_I1|_J1]
c_code local build_ref_800
	ret_reg &ref[800]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(34),V(35))
	move_ret R(0)
	call_c   Dyam_Create_List(V(31),R(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(24),R(0))
	move_ret ref[800]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_160
	ret_reg &seed[160]
	call_c   build_ref_51()
	call_c   build_ref_802()
	call_c   Dyam_Seed_Start(&ref[51],&ref[802],I(0),fun0,1)
	call_c   build_ref_803()
	call_c   Dyam_Seed_Add_Comp(&ref[803],&ref[802],0)
	call_c   Dyam_Seed_End()
	move_ret seed[160]
	c_ret

;; TERM 803: '*GUARD*'(append(_K1, _S, _N1)) :> '$$HOLE$$'
c_code local build_ref_803
	ret_reg &ref[803]
	call_c   build_ref_802()
	call_c   Dyam_Create_Binary(I(9),&ref[802],I(7))
	move_ret ref[803]
	c_ret

;; TERM 802: '*GUARD*'(append(_K1, _S, _N1))
c_code local build_ref_802
	ret_reg &ref[802]
	call_c   build_ref_51()
	call_c   build_ref_801()
	call_c   Dyam_Create_Unary(&ref[51],&ref[801])
	move_ret ref[802]
	c_ret

;; TERM 801: append(_K1, _S, _N1)
c_code local build_ref_801
	ret_reg &ref[801]
	call_c   build_ref_866()
	call_c   Dyam_Term_Start(&ref[866],3)
	call_c   Dyam_Term_Arg(V(36))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(39))
	call_c   Dyam_Term_End()
	move_ret ref[801]
	c_ret

c_code local build_seed_161
	ret_reg &seed[161]
	call_c   build_ref_51()
	call_c   build_ref_805()
	call_c   Dyam_Seed_Start(&ref[51],&ref[805],I(0),fun0,1)
	call_c   build_ref_806()
	call_c   Dyam_Seed_Add_Comp(&ref[806],&ref[805],0)
	call_c   Dyam_Seed_End()
	move_ret seed[161]
	c_ret

;; TERM 806: '*GUARD*'(append(_M1, _T, _O1)) :> '$$HOLE$$'
c_code local build_ref_806
	ret_reg &ref[806]
	call_c   build_ref_805()
	call_c   Dyam_Create_Binary(I(9),&ref[805],I(7))
	move_ret ref[806]
	c_ret

;; TERM 805: '*GUARD*'(append(_M1, _T, _O1))
c_code local build_ref_805
	ret_reg &ref[805]
	call_c   build_ref_51()
	call_c   build_ref_804()
	call_c   Dyam_Create_Unary(&ref[51],&ref[804])
	move_ret ref[805]
	c_ret

;; TERM 804: append(_M1, _T, _O1)
c_code local build_ref_804
	ret_reg &ref[804]
	call_c   build_ref_866()
	call_c   Dyam_Term_Start(&ref[866],3)
	call_c   Dyam_Term_Arg(V(38))
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_Arg(V(40))
	call_c   Dyam_Term_End()
	move_ret ref[804]
	c_ret

;; TERM 807: [_F,_G,_E,_R,[_O,_P]]
c_code local build_ref_807
	ret_reg &ref[807]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_609()
	call_c   Dyam_Create_List(&ref[609],I(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(17),R(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(4),R(0))
	move_ret R(0)
	call_c   Dyam_Create_Tupple(5,6,R(0))
	move_ret ref[807]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_162
	ret_reg &seed[162]
	call_c   build_ref_51()
	call_c   build_ref_809()
	call_c   Dyam_Seed_Start(&ref[51],&ref[809],I(0),fun0,1)
	call_c   build_ref_810()
	call_c   Dyam_Seed_Add_Comp(&ref[810],&ref[809],0)
	call_c   Dyam_Seed_End()
	move_ret seed[162]
	c_ret

;; TERM 810: '*GUARD*'(body_to_lpda(_B, (_A1 , _R = _S , _D1 = _F , _E1 = _G), _H, _S1, _K)) :> '$$HOLE$$'
c_code local build_ref_810
	ret_reg &ref[810]
	call_c   build_ref_809()
	call_c   Dyam_Create_Binary(I(9),&ref[809],I(7))
	move_ret ref[810]
	c_ret

;; TERM 809: '*GUARD*'(body_to_lpda(_B, (_A1 , _R = _S , _D1 = _F , _E1 = _G), _H, _S1, _K))
c_code local build_ref_809
	ret_reg &ref[809]
	call_c   build_ref_51()
	call_c   build_ref_808()
	call_c   Dyam_Create_Unary(&ref[51],&ref[808])
	move_ret ref[809]
	c_ret

;; TERM 808: body_to_lpda(_B, (_A1 , _R = _S , _D1 = _F , _E1 = _G), _H, _S1, _K)
c_code local build_ref_808
	ret_reg &ref[808]
	call_c   Dyam_Pseudo_Choice(3)
	call_c   build_ref_869()
	call_c   Dyam_Create_Binary(&ref[869],V(17),V(18))
	move_ret R(0)
	call_c   Dyam_Create_Binary(&ref[869],V(29),V(5))
	move_ret R(1)
	call_c   Dyam_Create_Binary(&ref[869],V(30),V(6))
	move_ret R(2)
	call_c   Dyam_Create_Binary(I(4),R(1),R(2))
	move_ret R(2)
	call_c   Dyam_Create_Binary(I(4),R(0),R(2))
	move_ret R(2)
	call_c   Dyam_Create_Binary(I(4),V(26),R(2))
	move_ret R(2)
	call_c   build_ref_875()
	call_c   Dyam_Term_Start(&ref[875],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(2))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(44))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_End()
	move_ret ref[808]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 811: [_D1,_E1,_F1,_E,_D,_G,_F]
c_code local build_ref_811
	ret_reg &ref[811]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(5),I(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(6),R(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(3),R(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(4),R(0))
	move_ret R(0)
	call_c   Dyam_Create_Tupple(29,31,R(0))
	move_ret ref[811]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 812: [_T1 ^ _V1,[_I1|_J1],[_L1|_H1]] ^ '*KLEENE-LAST*'(_W, _O1, _N1, _P1, _R1, _V1)
c_code local build_ref_812
	ret_reg &ref[812]
	call_c   Dyam_Pseudo_Choice(3)
	call_c   build_ref_860()
	call_c   Dyam_Create_Binary(&ref[860],V(45),V(47))
	move_ret R(0)
	call_c   Dyam_Create_List(V(34),V(35))
	move_ret R(1)
	call_c   Dyam_Create_List(V(37),V(33))
	move_ret R(2)
	call_c   Dyam_Create_List(R(2),I(0))
	move_ret R(2)
	call_c   Dyam_Create_List(R(1),R(2))
	move_ret R(2)
	call_c   Dyam_Create_List(R(0),R(2))
	move_ret R(2)
	call_c   build_ref_928()
	call_c   Dyam_Term_Start(&ref[928],6)
	call_c   Dyam_Term_Arg(V(22))
	call_c   Dyam_Term_Arg(V(40))
	call_c   Dyam_Term_Arg(V(39))
	call_c   Dyam_Term_Arg(V(41))
	call_c   Dyam_Term_Arg(V(43))
	call_c   Dyam_Term_Arg(V(47))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   Dyam_Create_Binary(&ref[860],R(2),R(0))
	move_ret ref[812]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_163
	ret_reg &seed[163]
	call_c   build_ref_51()
	call_c   build_ref_814()
	call_c   Dyam_Seed_Start(&ref[51],&ref[814],I(0),fun0,1)
	call_c   build_ref_815()
	call_c   Dyam_Seed_Add_Comp(&ref[815],&ref[814],0)
	call_c   Dyam_Seed_End()
	move_ret seed[163]
	c_ret

;; TERM 815: '*GUARD*'(body_to_lpda(_B, _B1, _W1, _X1, _K)) :> '$$HOLE$$'
c_code local build_ref_815
	ret_reg &ref[815]
	call_c   build_ref_814()
	call_c   Dyam_Create_Binary(I(9),&ref[814],I(7))
	move_ret ref[815]
	c_ret

;; TERM 814: '*GUARD*'(body_to_lpda(_B, _B1, _W1, _X1, _K))
c_code local build_ref_814
	ret_reg &ref[814]
	call_c   build_ref_51()
	call_c   build_ref_813()
	call_c   Dyam_Create_Unary(&ref[51],&ref[813])
	move_ret ref[814]
	c_ret

;; TERM 813: body_to_lpda(_B, _B1, _W1, _X1, _K)
c_code local build_ref_813
	ret_reg &ref[813]
	call_c   build_ref_875()
	call_c   Dyam_Term_Start(&ref[875],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(27))
	call_c   Dyam_Term_Arg(V(48))
	call_c   Dyam_Term_Arg(V(49))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_End()
	move_ret ref[813]
	c_ret

;; TERM 816: '*KLEENE*'(_W, _P1, '*ALTERNATIVE*'(_S1, _X1))
c_code local build_ref_816
	ret_reg &ref[816]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_930()
	call_c   Dyam_Create_Binary(&ref[930],V(44),V(49))
	move_ret R(0)
	call_c   build_ref_929()
	call_c   Dyam_Term_Start(&ref[929],3)
	call_c   Dyam_Term_Arg(V(22))
	call_c   Dyam_Term_Arg(V(41))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_End()
	move_ret ref[816]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_164
	ret_reg &seed[164]
	call_c   build_ref_51()
	call_c   build_ref_818()
	call_c   Dyam_Seed_Start(&ref[51],&ref[818],I(0),fun0,1)
	call_c   build_ref_819()
	call_c   Dyam_Seed_Add_Comp(&ref[819],&ref[818],0)
	call_c   Dyam_Seed_End()
	move_ret seed[164]
	c_ret

;; TERM 819: '*GUARD*'(body_to_lpda(_B, (_E = _E1), _I, _Z1, _K)) :> '$$HOLE$$'
c_code local build_ref_819
	ret_reg &ref[819]
	call_c   build_ref_818()
	call_c   Dyam_Create_Binary(I(9),&ref[818],I(7))
	move_ret ref[819]
	c_ret

;; TERM 818: '*GUARD*'(body_to_lpda(_B, (_E = _E1), _I, _Z1, _K))
c_code local build_ref_818
	ret_reg &ref[818]
	call_c   build_ref_51()
	call_c   build_ref_817()
	call_c   Dyam_Create_Unary(&ref[51],&ref[817])
	move_ret ref[818]
	c_ret

;; TERM 817: body_to_lpda(_B, (_E = _E1), _I, _Z1, _K)
c_code local build_ref_817
	ret_reg &ref[817]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_869()
	call_c   Dyam_Create_Binary(&ref[869],V(4),V(30))
	move_ret R(0)
	call_c   build_ref_875()
	call_c   Dyam_Term_Start(&ref[875],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(51))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_End()
	move_ret ref[817]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_165
	ret_reg &seed[165]
	call_c   build_ref_51()
	call_c   build_ref_821()
	call_c   Dyam_Seed_Start(&ref[51],&ref[821],I(0),fun0,1)
	call_c   build_ref_822()
	call_c   Dyam_Seed_Add_Comp(&ref[822],&ref[821],0)
	call_c   Dyam_Seed_End()
	move_ret seed[165]
	c_ret

;; TERM 822: '*GUARD*'(body_to_lpda(_B, (_Z , _Q = _S , _D = _D1), (_Y1 :> _Z1), _J, _K)) :> '$$HOLE$$'
c_code local build_ref_822
	ret_reg &ref[822]
	call_c   build_ref_821()
	call_c   Dyam_Create_Binary(I(9),&ref[821],I(7))
	move_ret ref[822]
	c_ret

;; TERM 821: '*GUARD*'(body_to_lpda(_B, (_Z , _Q = _S , _D = _D1), (_Y1 :> _Z1), _J, _K))
c_code local build_ref_821
	ret_reg &ref[821]
	call_c   build_ref_51()
	call_c   build_ref_820()
	call_c   Dyam_Create_Unary(&ref[51],&ref[820])
	move_ret ref[821]
	c_ret

;; TERM 820: body_to_lpda(_B, (_Z , _Q = _S , _D = _D1), (_Y1 :> _Z1), _J, _K)
c_code local build_ref_820
	ret_reg &ref[820]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_869()
	call_c   Dyam_Create_Binary(&ref[869],V(16),V(18))
	move_ret R(0)
	call_c   Dyam_Create_Binary(&ref[869],V(3),V(29))
	move_ret R(1)
	call_c   Dyam_Create_Binary(I(4),R(0),R(1))
	move_ret R(1)
	call_c   Dyam_Create_Binary(I(4),V(25),R(1))
	move_ret R(1)
	call_c   Dyam_Create_Binary(I(9),V(50),V(51))
	move_ret R(0)
	call_c   build_ref_875()
	call_c   Dyam_Term_Start(&ref[875],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_End()
	move_ret ref[820]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun255[15]=[14,build_seed_159,build_ref_799,build_ref_800,build_seed_160,build_seed_161,build_ref_807,build_seed_162,build_ref_811,build_ref_812,build_ref_179,build_seed_163,build_ref_816,build_seed_164,build_seed_165]

pl_code local fun255
	call_c   Dyam_Pool(pool_fun255)
	call_c   Dyam_Allocate(0)
	pl_call  fun7(&seed[159],1)
	call_c   Dyam_Unify(V(36),&ref[799])
	fail_ret
	call_c   Dyam_Unify(V(38),&ref[800])
	fail_ret
	pl_call  fun7(&seed[160],1)
	pl_call  fun7(&seed[161],1)
	call_c   Dyam_Reg_Load(0,V(13))
	move     V(41), R(2)
	move     S(5), R(3)
	pl_call  pred_tupple_2()
	call_c   Dyam_Reg_Load(0,V(7))
	move     V(42), R(2)
	move     S(5), R(3)
	pl_call  pred_tupple_2()
	move     &ref[807], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(42))
	move     V(43), R(4)
	move     S(5), R(5)
	pl_call  pred_extend_tupple_3()
	pl_call  fun7(&seed[162],1)
	call_c   Dyam_Unify(V(45),&ref[811])
	fail_ret
	call_c   Dyam_Unify(V(46),&ref[812])
	fail_ret
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(12))
	call_c   Dyam_Reg_Load(4,V(29))
	call_c   Dyam_Reg_Load(6,V(30))
	call_c   Dyam_Reg_Load(8,V(31))
	call_c   Dyam_Reg_Load(10,V(30))
	call_c   Dyam_Reg_Load(12,V(46))
	move     &ref[179], R(14)
	move     0, R(15)
	move     V(48), R(16)
	move     S(5), R(17)
	call_c   Dyam_Reg_Load(18,V(10))
	call_c   Dyam_Reg_Load(20,V(11))
	pl_call  pred_dcg_il_body_to_lpda_11()
	pl_call  fun7(&seed[163],1)
	call_c   Dyam_Unify(V(50),&ref[816])
	fail_ret
	pl_call  fun7(&seed[164],1)
	call_c   Dyam_Deallocate()
	pl_jump  fun7(&seed[165],1)

;; TERM 824: '$TUPPLE'(35074877687864)
c_code local build_ref_824
	ret_reg &ref[824]
	call_c   Dyam_Start_Tupple(0,201326206)
	call_c   Dyam_Almost_End_Tupple(29,486539264)
	move_ret ref[824]
	c_ret

long local pool_fun256[10]=[9,build_ref_789,build_ref_791,build_ref_708,build_ref_709,build_ref_792,build_ref_793,build_ref_794,build_ref_795,build_ref_825]

pl_code local fun256
	call_c   Dyam_Pool(pool_fun256)
	call_c   Dyam_Unify_Item(&ref[789])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(2))
	move     &ref[791], R(2)
	move     S(5), R(3)
	pl_call  pred_kleene_normalize_2()
	move     &ref[708], R(0)
	move     0, R(1)
	move     V(21), R(2)
	move     S(5), R(3)
	pl_call  pred_update_counter_2()
	move     &ref[709], R(0)
	move     0, R(1)
	move     &ref[792], R(2)
	move     S(5), R(3)
	move     V(22), R(4)
	move     S(5), R(5)
	pl_call  pred_name_builder_3()
	pl_call  Object_1(&ref[793])
	move     &ref[794], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(28), R(4)
	move     S(5), R(5)
	pl_call  pred_my_numbervars_3()
	move     &ref[795], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(32), R(4)
	move     S(5), R(5)
	pl_call  pred_my_numbervars_3()
	move     &ref[825], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	move     V(33), R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  fun23()

;; TERM 790: '*GUARD*'(dcg_il_body_to_lpda_handler(_B, _C, _D, _E, _F, _G, _H, _I, _J, _K, _L)) :> []
c_code local build_ref_790
	ret_reg &ref[790]
	call_c   build_ref_789()
	call_c   Dyam_Create_Binary(I(9),&ref[789],I(0))
	move_ret ref[790]
	c_ret

c_code local build_seed_55
	ret_reg &seed[55]
	call_c   build_ref_45()
	call_c   build_ref_46()
	call_c   Dyam_Seed_Start(&ref[45],&ref[46],&ref[46],fun5,0)
	call_c   Dyam_Seed_End()
	move_ret seed[55]
	c_ret

;; TERM 46: bmg_wrap_args(_B, _C, _D, _E, [], [], [_B,_C,_D,_E])
c_code local build_ref_46
	ret_reg &ref[46]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Tupple(1,4,I(0))
	move_ret R(0)
	call_c   build_ref_907()
	call_c   Dyam_Term_Start(&ref[907],7)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_End()
	move_ret ref[46]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 45: '*DATABASE*'
c_code local build_ref_45
	ret_reg &ref[45]
	call_c   Dyam_Create_Atom("*DATABASE*")
	move_ret ref[45]
	c_ret

c_code local build_seed_61
	ret_reg &seed[61]
	call_c   build_ref_45()
	call_c   build_ref_97()
	call_c   Dyam_Seed_Start(&ref[45],&ref[97],&ref[97],fun5,0)
	call_c   Dyam_Seed_End()
	move_ret seed[61]
	c_ret

;; TERM 97: bmg_wrap_args(_B, _C, _D, _E, [_F|_G], _H, [_B,_C,_D,_E,[_F|_G],_H])
c_code local build_ref_97
	ret_reg &ref[97]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   Dyam_Create_List(V(5),V(6))
	move_ret R(0)
	call_c   Dyam_Create_List(V(7),I(0))
	move_ret R(1)
	call_c   Dyam_Create_List(R(0),R(1))
	move_ret R(1)
	call_c   Dyam_Create_Tupple(1,4,R(1))
	move_ret R(1)
	call_c   build_ref_907()
	call_c   Dyam_Term_Start(&ref[907],7)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_End()
	move_ret ref[97]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_106
	ret_reg &seed[106]
	call_c   build_ref_45()
	call_c   build_ref_402()
	call_c   Dyam_Seed_Start(&ref[45],&ref[402],&ref[402],fun5,0)
	call_c   Dyam_Seed_End()
	move_ret seed[106]
	c_ret

;; TERM 402: kleene_conditions([_B,_C], [_D,_E], (_D = 0 , (_B = 0 xor true) , (_C = inf xor true) , true), (_D >= _B , true), (_C == inf -> (_D =< _B -> _E is _D + 1 ; _D = _E) ; _D < _C , _E is _D + 1))
c_code local build_ref_402
	ret_reg &ref[402]
	call_c   Dyam_Pseudo_Choice(8)
	call_c   Dyam_Create_Tupple(1,2,I(0))
	move_ret R(0)
	call_c   Dyam_Create_Tupple(3,4,I(0))
	move_ret R(1)
	call_c   build_ref_869()
	call_c   Dyam_Create_Binary(&ref[869],V(3),N(0))
	move_ret R(2)
	call_c   Dyam_Create_Binary(&ref[869],V(1),N(0))
	move_ret R(3)
	call_c   build_ref_968()
	call_c   build_ref_186()
	call_c   Dyam_Create_Binary(&ref[968],R(3),&ref[186])
	move_ret R(3)
	call_c   build_ref_969()
	call_c   Dyam_Create_Binary(&ref[869],V(2),&ref[969])
	move_ret R(4)
	call_c   Dyam_Create_Binary(&ref[968],R(4),&ref[186])
	move_ret R(4)
	call_c   Dyam_Create_Binary(I(4),R(4),&ref[186])
	move_ret R(4)
	call_c   Dyam_Create_Binary(I(4),R(3),R(4))
	move_ret R(4)
	call_c   Dyam_Create_Binary(I(4),R(2),R(4))
	move_ret R(4)
	call_c   build_ref_970()
	call_c   Dyam_Create_Binary(&ref[970],V(3),V(1))
	move_ret R(2)
	call_c   Dyam_Create_Binary(I(4),R(2),&ref[186])
	move_ret R(2)
	call_c   build_ref_971()
	call_c   Dyam_Create_Binary(&ref[971],V(2),&ref[969])
	move_ret R(3)
	call_c   build_ref_972()
	call_c   Dyam_Create_Binary(&ref[972],V(3),V(1))
	move_ret R(5)
	call_c   build_ref_974()
	call_c   Dyam_Create_Binary(&ref[974],V(3),N(1))
	move_ret R(6)
	call_c   build_ref_973()
	call_c   Dyam_Create_Binary(&ref[973],V(4),R(6))
	move_ret R(6)
	call_c   build_ref_847()
	call_c   Dyam_Create_Binary(&ref[847],R(5),R(6))
	move_ret R(5)
	call_c   Dyam_Create_Binary(&ref[869],V(3),V(4))
	move_ret R(7)
	call_c   Dyam_Create_Binary(I(5),R(5),R(7))
	move_ret R(7)
	call_c   Dyam_Create_Binary(&ref[847],R(3),R(7))
	move_ret R(7)
	call_c   build_ref_975()
	call_c   Dyam_Create_Binary(&ref[975],V(3),V(2))
	move_ret R(3)
	call_c   Dyam_Create_Binary(I(4),R(3),R(6))
	move_ret R(6)
	call_c   Dyam_Create_Binary(I(5),R(7),R(6))
	move_ret R(6)
	call_c   build_ref_927()
	call_c   Dyam_Term_Start(&ref[927],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(R(4))
	call_c   Dyam_Term_Arg(R(2))
	call_c   Dyam_Term_Arg(R(6))
	call_c   Dyam_Term_End()
	move_ret ref[402]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 975: <
c_code local build_ref_975
	ret_reg &ref[975]
	call_c   Dyam_Create_Atom("<")
	move_ret ref[975]
	c_ret

;; TERM 973: is
c_code local build_ref_973
	ret_reg &ref[973]
	call_c   Dyam_Create_Atom("is")
	move_ret ref[973]
	c_ret

;; TERM 974: +
c_code local build_ref_974
	ret_reg &ref[974]
	call_c   Dyam_Create_Atom("+")
	move_ret ref[974]
	c_ret

;; TERM 972: =<
c_code local build_ref_972
	ret_reg &ref[972]
	call_c   Dyam_Create_Atom("=<")
	move_ret ref[972]
	c_ret

;; TERM 971: ==
c_code local build_ref_971
	ret_reg &ref[971]
	call_c   Dyam_Create_Atom("==")
	move_ret ref[971]
	c_ret

;; TERM 970: >=
c_code local build_ref_970
	ret_reg &ref[970]
	call_c   Dyam_Create_Atom(">=")
	move_ret ref[970]
	c_ret

;; TERM 969: inf
c_code local build_ref_969
	ret_reg &ref[969]
	call_c   Dyam_Create_Atom("inf")
	move_ret ref[969]
	c_ret

;; TERM 968: xor
c_code local build_ref_968
	ret_reg &ref[968]
	call_c   Dyam_Create_Atom("xor")
	move_ret ref[968]
	c_ret

c_code local build_seed_155
	ret_reg &seed[155]
	call_c   build_ref_39()
	call_c   build_ref_755()
	call_c   Dyam_Seed_Start(&ref[39],&ref[755],&ref[755],fun0,1)
	call_c   build_ref_757()
	call_c   Dyam_Seed_Add_Comp(&ref[757],&ref[755],0)
	call_c   Dyam_Seed_End()
	move_ret seed[155]
	c_ret

;; TERM 757: '*FIRST*'(wrapping_predicate(_C)) :> '$$HOLE$$'
c_code local build_ref_757
	ret_reg &ref[757]
	call_c   build_ref_756()
	call_c   Dyam_Create_Binary(I(9),&ref[756],I(7))
	move_ret ref[757]
	c_ret

;; TERM 756: '*FIRST*'(wrapping_predicate(_C))
c_code local build_ref_756
	ret_reg &ref[756]
	call_c   build_ref_36()
	call_c   build_ref_754()
	call_c   Dyam_Create_Unary(&ref[36],&ref[754])
	move_ret ref[756]
	c_ret

;; TERM 754: wrapping_predicate(_C)
c_code local build_ref_754
	ret_reg &ref[754]
	call_c   build_ref_905()
	call_c   Dyam_Create_Unary(&ref[905],V(2))
	move_ret ref[754]
	c_ret

;; TERM 755: '*CITEM*'(wrapping_predicate(_C), wrapping_predicate(_C))
c_code local build_ref_755
	ret_reg &ref[755]
	call_c   build_ref_39()
	call_c   build_ref_754()
	call_c   Dyam_Create_Binary(&ref[39],&ref[754],&ref[754])
	move_ret ref[755]
	c_ret

c_code local build_seed_156
	ret_reg &seed[156]
	call_c   build_ref_70()
	call_c   build_ref_760()
	call_c   Dyam_Seed_Start(&ref[70],&ref[760],I(0),fun4,1)
	call_c   build_ref_758()
	call_c   Dyam_Seed_Add_Comp(&ref[758],fun233,0)
	call_c   Dyam_Seed_End()
	move_ret seed[156]
	c_ret

;; TERM 758: '*RITEM*'(wrapping_predicate(_C), voidret)
c_code local build_ref_758
	ret_reg &ref[758]
	call_c   build_ref_0()
	call_c   build_ref_754()
	call_c   build_ref_14()
	call_c   Dyam_Create_Binary(&ref[0],&ref[754],&ref[14])
	move_ret ref[758]
	c_ret

pl_code local fun233
	call_c   build_ref_758()
	call_c   Dyam_Unify_Item(&ref[758])
	fail_ret
	pl_jump  Follow_Cont(V(1))

;; TERM 760: '*RITEM*'(wrapping_predicate(_C), voidret) :> dcg(8, '$TUPPLE'(35074870873324))
c_code local build_ref_760
	ret_reg &ref[760]
	call_c   build_ref_758()
	call_c   build_ref_759()
	call_c   Dyam_Create_Binary(I(9),&ref[758],&ref[759])
	move_ret ref[760]
	c_ret

;; TERM 759: dcg(8, '$TUPPLE'(35074870873324))
c_code local build_ref_759
	ret_reg &ref[759]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,134217728)
	move_ret R(0)
	call_c   build_ref_839()
	call_c   Dyam_Create_Binary(&ref[839],N(8),R(0))
	move_ret ref[759]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 70: '*CURNEXT*'
c_code local build_ref_70
	ret_reg &ref[70]
	call_c   Dyam_Create_Atom("*CURNEXT*")
	move_ret ref[70]
	c_ret

c_code local build_seed_140
	ret_reg &seed[140]
	call_c   build_ref_39()
	call_c   build_ref_633()
	call_c   Dyam_Seed_Start(&ref[39],&ref[633],&ref[633],fun0,1)
	call_c   build_ref_634()
	call_c   Dyam_Seed_Add_Comp(&ref[634],&ref[633],0)
	call_c   Dyam_Seed_End()
	move_ret seed[140]
	c_ret

;; TERM 634: '*FIRST*'(interleave_initialize) :> '$$HOLE$$'
c_code local build_ref_634
	ret_reg &ref[634]
	call_c   build_ref_37()
	call_c   Dyam_Create_Binary(I(9),&ref[37],I(7))
	move_ret ref[634]
	c_ret

;; TERM 633: '*CITEM*'(interleave_initialize, interleave_initialize)
c_code local build_ref_633
	ret_reg &ref[633]
	call_c   build_ref_39()
	call_c   build_ref_22()
	call_c   Dyam_Create_Binary(&ref[39],&ref[22],&ref[22])
	move_ret ref[633]
	c_ret

c_code local build_seed_141
	ret_reg &seed[141]
	call_c   build_ref_70()
	call_c   build_ref_636()
	call_c   Dyam_Seed_Start(&ref[70],&ref[636],I(0),fun4,1)
	call_c   build_ref_23()
	call_c   Dyam_Seed_Add_Comp(&ref[23],fun211,0)
	call_c   Dyam_Seed_End()
	move_ret seed[141]
	c_ret

;; TERM 23: '*RITEM*'(interleave_initialize, voidret)
c_code local build_ref_23
	ret_reg &ref[23]
	call_c   build_ref_0()
	call_c   build_ref_22()
	call_c   build_ref_14()
	call_c   Dyam_Create_Binary(&ref[0],&ref[22],&ref[14])
	move_ret ref[23]
	c_ret

pl_code local fun211
	call_c   build_ref_23()
	call_c   Dyam_Unify_Item(&ref[23])
	fail_ret
	pl_jump  Follow_Cont(V(1))

;; TERM 636: '*RITEM*'(interleave_initialize, voidret) :> dcg(7, '$TUPPLE'(35074870873324))
c_code local build_ref_636
	ret_reg &ref[636]
	call_c   build_ref_23()
	call_c   build_ref_635()
	call_c   Dyam_Create_Binary(I(9),&ref[23],&ref[635])
	move_ret ref[636]
	c_ret

;; TERM 635: dcg(7, '$TUPPLE'(35074870873324))
c_code local build_ref_635
	ret_reg &ref[635]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,134217728)
	move_ret R(0)
	call_c   build_ref_839()
	call_c   Dyam_Create_Binary(&ref[839],N(7),R(0))
	move_ret ref[635]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_114
	ret_reg &seed[114]
	call_c   build_ref_39()
	call_c   build_ref_456()
	call_c   Dyam_Seed_Start(&ref[39],&ref[456],&ref[456],fun0,1)
	call_c   build_ref_458()
	call_c   Dyam_Seed_Add_Comp(&ref[458],&ref[456],0)
	call_c   Dyam_Seed_End()
	move_ret seed[114]
	c_ret

;; TERM 458: '*FIRST*'('call_dcg_prolog_make/7'(_C)) :> '$$HOLE$$'
c_code local build_ref_458
	ret_reg &ref[458]
	call_c   build_ref_457()
	call_c   Dyam_Create_Binary(I(9),&ref[457],I(7))
	move_ret ref[458]
	c_ret

;; TERM 457: '*FIRST*'('call_dcg_prolog_make/7'(_C))
c_code local build_ref_457
	ret_reg &ref[457]
	call_c   build_ref_36()
	call_c   build_ref_455()
	call_c   Dyam_Create_Unary(&ref[36],&ref[455])
	move_ret ref[457]
	c_ret

;; TERM 455: 'call_dcg_prolog_make/7'(_C)
c_code local build_ref_455
	ret_reg &ref[455]
	call_c   build_ref_888()
	call_c   Dyam_Create_Unary(&ref[888],V(2))
	move_ret ref[455]
	c_ret

;; TERM 456: '*CITEM*'('call_dcg_prolog_make/7'(_C), 'call_dcg_prolog_make/7'(_C))
c_code local build_ref_456
	ret_reg &ref[456]
	call_c   build_ref_39()
	call_c   build_ref_455()
	call_c   Dyam_Create_Binary(&ref[39],&ref[455],&ref[455])
	move_ret ref[456]
	c_ret

c_code local build_seed_115
	ret_reg &seed[115]
	call_c   build_ref_70()
	call_c   build_ref_461()
	call_c   Dyam_Seed_Start(&ref[70],&ref[461],I(0),fun4,1)
	call_c   build_ref_459()
	call_c   Dyam_Seed_Add_Comp(&ref[459],fun159,0)
	call_c   Dyam_Seed_End()
	move_ret seed[115]
	c_ret

;; TERM 459: '*RITEM*'('call_dcg_prolog_make/7'(_C), return(_D, _E, _F, _G, _H, _I))
c_code local build_ref_459
	ret_reg &ref[459]
	call_c   build_ref_0()
	call_c   build_ref_455()
	call_c   build_ref_421()
	call_c   Dyam_Create_Binary(&ref[0],&ref[455],&ref[421])
	move_ret ref[459]
	c_ret

pl_code local fun159
	call_c   build_ref_459()
	call_c   Dyam_Unify_Item(&ref[459])
	fail_ret
	pl_jump  Follow_Cont(V(1))

;; TERM 461: '*RITEM*'('call_dcg_prolog_make/7'(_C), return(_D, _E, _F, _G, _H, _I)) :> dcg(6, '$TUPPLE'(35074870873324))
c_code local build_ref_461
	ret_reg &ref[461]
	call_c   build_ref_459()
	call_c   build_ref_460()
	call_c   Dyam_Create_Binary(I(9),&ref[459],&ref[460])
	move_ret ref[461]
	c_ret

;; TERM 460: dcg(6, '$TUPPLE'(35074870873324))
c_code local build_ref_460
	ret_reg &ref[460]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,134217728)
	move_ret R(0)
	call_c   build_ref_839()
	call_c   Dyam_Create_Binary(&ref[839],N(6),R(0))
	move_ret ref[460]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_112
	ret_reg &seed[112]
	call_c   build_ref_39()
	call_c   build_ref_449()
	call_c   Dyam_Seed_Start(&ref[39],&ref[449],&ref[449],fun0,1)
	call_c   build_ref_451()
	call_c   Dyam_Seed_Add_Comp(&ref[451],&ref[449],0)
	call_c   Dyam_Seed_End()
	move_ret seed[112]
	c_ret

;; TERM 451: '*FIRST*'(register_predicate(_C)) :> '$$HOLE$$'
c_code local build_ref_451
	ret_reg &ref[451]
	call_c   build_ref_450()
	call_c   Dyam_Create_Binary(I(9),&ref[450],I(7))
	move_ret ref[451]
	c_ret

;; TERM 450: '*FIRST*'(register_predicate(_C))
c_code local build_ref_450
	ret_reg &ref[450]
	call_c   build_ref_36()
	call_c   build_ref_448()
	call_c   Dyam_Create_Unary(&ref[36],&ref[448])
	move_ret ref[450]
	c_ret

;; TERM 448: register_predicate(_C)
c_code local build_ref_448
	ret_reg &ref[448]
	call_c   build_ref_838()
	call_c   Dyam_Create_Unary(&ref[838],V(2))
	move_ret ref[448]
	c_ret

;; TERM 449: '*CITEM*'(register_predicate(_C), register_predicate(_C))
c_code local build_ref_449
	ret_reg &ref[449]
	call_c   build_ref_39()
	call_c   build_ref_448()
	call_c   Dyam_Create_Binary(&ref[39],&ref[448],&ref[448])
	move_ret ref[449]
	c_ret

c_code local build_seed_113
	ret_reg &seed[113]
	call_c   build_ref_70()
	call_c   build_ref_454()
	call_c   Dyam_Seed_Start(&ref[70],&ref[454],I(0),fun4,1)
	call_c   build_ref_452()
	call_c   Dyam_Seed_Add_Comp(&ref[452],fun156,0)
	call_c   Dyam_Seed_End()
	move_ret seed[113]
	c_ret

;; TERM 452: '*RITEM*'(register_predicate(_C), voidret)
c_code local build_ref_452
	ret_reg &ref[452]
	call_c   build_ref_0()
	call_c   build_ref_448()
	call_c   build_ref_14()
	call_c   Dyam_Create_Binary(&ref[0],&ref[448],&ref[14])
	move_ret ref[452]
	c_ret

pl_code local fun156
	call_c   build_ref_452()
	call_c   Dyam_Unify_Item(&ref[452])
	fail_ret
	pl_jump  Follow_Cont(V(1))

;; TERM 454: '*RITEM*'(register_predicate(_C), voidret) :> dcg(5, '$TUPPLE'(35074870873324))
c_code local build_ref_454
	ret_reg &ref[454]
	call_c   build_ref_452()
	call_c   build_ref_453()
	call_c   Dyam_Create_Binary(I(9),&ref[452],&ref[453])
	move_ret ref[454]
	c_ret

;; TERM 453: dcg(5, '$TUPPLE'(35074870873324))
c_code local build_ref_453
	ret_reg &ref[453]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,134217728)
	move_ret R(0)
	call_c   build_ref_839()
	call_c   Dyam_Create_Binary(&ref[839],N(5),R(0))
	move_ret ref[453]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_110
	ret_reg &seed[110]
	call_c   build_ref_39()
	call_c   build_ref_437()
	call_c   Dyam_Seed_Start(&ref[39],&ref[437],&ref[437],fun0,1)
	call_c   build_ref_439()
	call_c   Dyam_Seed_Add_Comp(&ref[439],&ref[437],0)
	call_c   Dyam_Seed_End()
	move_ret seed[110]
	c_ret

;; TERM 439: '*FIRST*'('call_bmg_head_stacks/6'(_C)) :> '$$HOLE$$'
c_code local build_ref_439
	ret_reg &ref[439]
	call_c   build_ref_438()
	call_c   Dyam_Create_Binary(I(9),&ref[438],I(7))
	move_ret ref[439]
	c_ret

;; TERM 438: '*FIRST*'('call_bmg_head_stacks/6'(_C))
c_code local build_ref_438
	ret_reg &ref[438]
	call_c   build_ref_36()
	call_c   build_ref_436()
	call_c   Dyam_Create_Unary(&ref[36],&ref[436])
	move_ret ref[438]
	c_ret

;; TERM 436: 'call_bmg_head_stacks/6'(_C)
c_code local build_ref_436
	ret_reg &ref[436]
	call_c   build_ref_976()
	call_c   Dyam_Create_Unary(&ref[976],V(2))
	move_ret ref[436]
	c_ret

;; TERM 976: 'call_bmg_head_stacks/6'
c_code local build_ref_976
	ret_reg &ref[976]
	call_c   Dyam_Create_Atom("call_bmg_head_stacks/6")
	move_ret ref[976]
	c_ret

;; TERM 437: '*CITEM*'('call_bmg_head_stacks/6'(_C), 'call_bmg_head_stacks/6'(_C))
c_code local build_ref_437
	ret_reg &ref[437]
	call_c   build_ref_39()
	call_c   build_ref_436()
	call_c   Dyam_Create_Binary(&ref[39],&ref[436],&ref[436])
	move_ret ref[437]
	c_ret

c_code local build_seed_111
	ret_reg &seed[111]
	call_c   build_ref_70()
	call_c   build_ref_443()
	call_c   Dyam_Seed_Start(&ref[70],&ref[443],I(0),fun4,1)
	call_c   build_ref_441()
	call_c   Dyam_Seed_Add_Comp(&ref[441],fun153,0)
	call_c   Dyam_Seed_End()
	move_ret seed[111]
	c_ret

;; TERM 441: '*RITEM*'('call_bmg_head_stacks/6'(_C), return(_D, _E, _F, _G, _H))
c_code local build_ref_441
	ret_reg &ref[441]
	call_c   build_ref_0()
	call_c   build_ref_436()
	call_c   build_ref_440()
	call_c   Dyam_Create_Binary(&ref[0],&ref[436],&ref[440])
	move_ret ref[441]
	c_ret

;; TERM 440: return(_D, _E, _F, _G, _H)
c_code local build_ref_440
	ret_reg &ref[440]
	call_c   build_ref_867()
	call_c   Dyam_Term_Start(&ref[867],5)
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[440]
	c_ret

pl_code local fun153
	call_c   build_ref_441()
	call_c   Dyam_Unify_Item(&ref[441])
	fail_ret
	pl_jump  Follow_Cont(V(1))

;; TERM 443: '*RITEM*'('call_bmg_head_stacks/6'(_C), return(_D, _E, _F, _G, _H)) :> dcg(4, '$TUPPLE'(35074870873324))
c_code local build_ref_443
	ret_reg &ref[443]
	call_c   build_ref_441()
	call_c   build_ref_442()
	call_c   Dyam_Create_Binary(I(9),&ref[441],&ref[442])
	move_ret ref[443]
	c_ret

;; TERM 442: dcg(4, '$TUPPLE'(35074870873324))
c_code local build_ref_442
	ret_reg &ref[442]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,134217728)
	move_ret R(0)
	call_c   build_ref_839()
	call_c   Dyam_Create_Binary(&ref[839],N(4),R(0))
	move_ret ref[442]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_107
	ret_reg &seed[107]
	call_c   build_ref_39()
	call_c   build_ref_409()
	call_c   Dyam_Seed_Start(&ref[39],&ref[409],&ref[409],fun0,1)
	call_c   build_ref_411()
	call_c   Dyam_Seed_Add_Comp(&ref[411],&ref[409],0)
	call_c   Dyam_Seed_End()
	move_ret seed[107]
	c_ret

;; TERM 411: '*FIRST*'('call_bmg_args/6'(_E, _F)) :> '$$HOLE$$'
c_code local build_ref_411
	ret_reg &ref[411]
	call_c   build_ref_410()
	call_c   Dyam_Create_Binary(I(9),&ref[410],I(7))
	move_ret ref[411]
	c_ret

;; TERM 410: '*FIRST*'('call_bmg_args/6'(_E, _F))
c_code local build_ref_410
	ret_reg &ref[410]
	call_c   build_ref_36()
	call_c   build_ref_408()
	call_c   Dyam_Create_Unary(&ref[36],&ref[408])
	move_ret ref[410]
	c_ret

;; TERM 408: 'call_bmg_args/6'(_E, _F)
c_code local build_ref_408
	ret_reg &ref[408]
	call_c   build_ref_977()
	call_c   Dyam_Create_Binary(&ref[977],V(4),V(5))
	move_ret ref[408]
	c_ret

;; TERM 977: 'call_bmg_args/6'
c_code local build_ref_977
	ret_reg &ref[977]
	call_c   Dyam_Create_Atom("call_bmg_args/6")
	move_ret ref[977]
	c_ret

;; TERM 409: '*CITEM*'('call_bmg_args/6'(_E, _F), 'call_bmg_args/6'(_E, _F))
c_code local build_ref_409
	ret_reg &ref[409]
	call_c   build_ref_39()
	call_c   build_ref_408()
	call_c   Dyam_Create_Binary(&ref[39],&ref[408],&ref[408])
	move_ret ref[409]
	c_ret

c_code local build_seed_108
	ret_reg &seed[108]
	call_c   build_ref_70()
	call_c   build_ref_415()
	call_c   Dyam_Seed_Start(&ref[70],&ref[415],I(0),fun4,1)
	call_c   build_ref_413()
	call_c   Dyam_Seed_Add_Comp(&ref[413],fun146,0)
	call_c   Dyam_Seed_End()
	move_ret seed[108]
	c_ret

;; TERM 413: '*RITEM*'('call_bmg_args/6'(_E, _F), return(_C, _D, _G, _H))
c_code local build_ref_413
	ret_reg &ref[413]
	call_c   build_ref_0()
	call_c   build_ref_408()
	call_c   build_ref_412()
	call_c   Dyam_Create_Binary(&ref[0],&ref[408],&ref[412])
	move_ret ref[413]
	c_ret

;; TERM 412: return(_C, _D, _G, _H)
c_code local build_ref_412
	ret_reg &ref[412]
	call_c   build_ref_867()
	call_c   Dyam_Term_Start(&ref[867],4)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[412]
	c_ret

pl_code local fun146
	call_c   build_ref_413()
	call_c   Dyam_Unify_Item(&ref[413])
	fail_ret
	pl_jump  Follow_Cont(V(1))

;; TERM 415: '*RITEM*'('call_bmg_args/6'(_E, _F), return(_C, _D, _G, _H)) :> dcg(3, '$TUPPLE'(35074870873324))
c_code local build_ref_415
	ret_reg &ref[415]
	call_c   build_ref_413()
	call_c   build_ref_414()
	call_c   Dyam_Create_Binary(I(9),&ref[413],&ref[414])
	move_ret ref[415]
	c_ret

;; TERM 414: dcg(3, '$TUPPLE'(35074870873324))
c_code local build_ref_414
	ret_reg &ref[414]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,134217728)
	move_ret R(0)
	call_c   build_ref_839()
	call_c   Dyam_Create_Binary(&ref[839],N(3),R(0))
	move_ret ref[414]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_101
	ret_reg &seed[101]
	call_c   build_ref_39()
	call_c   build_ref_369()
	call_c   Dyam_Seed_Start(&ref[39],&ref[369],&ref[369],fun0,1)
	call_c   build_ref_371()
	call_c   Dyam_Seed_Add_Comp(&ref[371],&ref[369],0)
	call_c   Dyam_Seed_End()
	move_ret seed[101]
	c_ret

;; TERM 371: '*FIRST*'(metacall_initialize) :> '$$HOLE$$'
c_code local build_ref_371
	ret_reg &ref[371]
	call_c   build_ref_370()
	call_c   Dyam_Create_Binary(I(9),&ref[370],I(7))
	move_ret ref[371]
	c_ret

;; TERM 370: '*FIRST*'(metacall_initialize)
c_code local build_ref_370
	ret_reg &ref[370]
	call_c   build_ref_36()
	call_c   build_ref_28()
	call_c   Dyam_Create_Unary(&ref[36],&ref[28])
	move_ret ref[370]
	c_ret

;; TERM 28: metacall_initialize
c_code local build_ref_28
	ret_reg &ref[28]
	call_c   Dyam_Create_Atom("metacall_initialize")
	move_ret ref[28]
	c_ret

;; TERM 369: '*CITEM*'(metacall_initialize, metacall_initialize)
c_code local build_ref_369
	ret_reg &ref[369]
	call_c   build_ref_39()
	call_c   build_ref_28()
	call_c   Dyam_Create_Binary(&ref[39],&ref[28],&ref[28])
	move_ret ref[369]
	c_ret

c_code local build_seed_102
	ret_reg &seed[102]
	call_c   build_ref_70()
	call_c   build_ref_373()
	call_c   Dyam_Seed_Start(&ref[70],&ref[373],I(0),fun4,1)
	call_c   build_ref_29()
	call_c   Dyam_Seed_Add_Comp(&ref[29],fun135,0)
	call_c   Dyam_Seed_End()
	move_ret seed[102]
	c_ret

;; TERM 29: '*RITEM*'(metacall_initialize, voidret)
c_code local build_ref_29
	ret_reg &ref[29]
	call_c   build_ref_0()
	call_c   build_ref_28()
	call_c   build_ref_14()
	call_c   Dyam_Create_Binary(&ref[0],&ref[28],&ref[14])
	move_ret ref[29]
	c_ret

pl_code local fun135
	call_c   build_ref_29()
	call_c   Dyam_Unify_Item(&ref[29])
	fail_ret
	pl_jump  Follow_Cont(V(1))

;; TERM 373: '*RITEM*'(metacall_initialize, voidret) :> dcg(2, '$TUPPLE'(35074870873324))
c_code local build_ref_373
	ret_reg &ref[373]
	call_c   build_ref_29()
	call_c   build_ref_372()
	call_c   Dyam_Create_Binary(I(9),&ref[29],&ref[372])
	move_ret ref[373]
	c_ret

;; TERM 372: dcg(2, '$TUPPLE'(35074870873324))
c_code local build_ref_372
	ret_reg &ref[372]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,134217728)
	move_ret R(0)
	call_c   build_ref_839()
	call_c   Dyam_Create_Binary(&ref[839],N(2),R(0))
	move_ret ref[372]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_88
	ret_reg &seed[88]
	call_c   build_ref_39()
	call_c   build_ref_273()
	call_c   Dyam_Seed_Start(&ref[39],&ref[273],&ref[273],fun0,1)
	call_c   build_ref_275()
	call_c   Dyam_Seed_Add_Comp(&ref[275],&ref[273],0)
	call_c   Dyam_Seed_End()
	move_ret seed[88]
	c_ret

;; TERM 275: '*FIRST*'(dcg_il_args(_C, _D, _E, _F)) :> '$$HOLE$$'
c_code local build_ref_275
	ret_reg &ref[275]
	call_c   build_ref_274()
	call_c   Dyam_Create_Binary(I(9),&ref[274],I(7))
	move_ret ref[275]
	c_ret

;; TERM 274: '*FIRST*'(dcg_il_args(_C, _D, _E, _F))
c_code local build_ref_274
	ret_reg &ref[274]
	call_c   build_ref_36()
	call_c   build_ref_272()
	call_c   Dyam_Create_Unary(&ref[36],&ref[272])
	move_ret ref[274]
	c_ret

;; TERM 272: dcg_il_args(_C, _D, _E, _F)
c_code local build_ref_272
	ret_reg &ref[272]
	call_c   build_ref_833()
	call_c   Dyam_Term_Start(&ref[833],4)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[272]
	c_ret

;; TERM 273: '*CITEM*'(dcg_il_args(_C, _D, _E, _F), dcg_il_args(_C, _D, _E, _F))
c_code local build_ref_273
	ret_reg &ref[273]
	call_c   build_ref_39()
	call_c   build_ref_272()
	call_c   Dyam_Create_Binary(&ref[39],&ref[272],&ref[272])
	move_ret ref[273]
	c_ret

c_code local build_seed_89
	ret_reg &seed[89]
	call_c   build_ref_70()
	call_c   build_ref_278()
	call_c   Dyam_Seed_Start(&ref[70],&ref[278],I(0),fun4,1)
	call_c   build_ref_276()
	call_c   Dyam_Seed_Add_Comp(&ref[276],fun75,0)
	call_c   Dyam_Seed_End()
	move_ret seed[89]
	c_ret

;; TERM 276: '*RITEM*'(dcg_il_args(_C, _D, _E, _F), voidret)
c_code local build_ref_276
	ret_reg &ref[276]
	call_c   build_ref_0()
	call_c   build_ref_272()
	call_c   build_ref_14()
	call_c   Dyam_Create_Binary(&ref[0],&ref[272],&ref[14])
	move_ret ref[276]
	c_ret

pl_code local fun75
	call_c   build_ref_276()
	call_c   Dyam_Unify_Item(&ref[276])
	fail_ret
	pl_jump  Follow_Cont(V(1))

;; TERM 278: '*RITEM*'(dcg_il_args(_C, _D, _E, _F), voidret) :> dcg(1, '$TUPPLE'(35074870873324))
c_code local build_ref_278
	ret_reg &ref[278]
	call_c   build_ref_276()
	call_c   build_ref_277()
	call_c   Dyam_Create_Binary(I(9),&ref[276],&ref[277])
	move_ret ref[278]
	c_ret

;; TERM 277: dcg(1, '$TUPPLE'(35074870873324))
c_code local build_ref_277
	ret_reg &ref[277]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,134217728)
	move_ret R(0)
	call_c   build_ref_839()
	call_c   Dyam_Create_Binary(&ref[839],N(1),R(0))
	move_ret ref[277]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_69
	ret_reg &seed[69]
	call_c   build_ref_51()
	call_c   build_ref_141()
	call_c   Dyam_Seed_Start(&ref[51],&ref[141],I(0),fun0,1)
	call_c   build_ref_142()
	call_c   Dyam_Seed_Add_Comp(&ref[142],&ref[141],0)
	call_c   Dyam_Seed_End()
	move_ret seed[69]
	c_ret

;; TERM 142: '*GUARD*'(toplevel(dcg_term_expand(_C, _M))) :> '$$HOLE$$'
c_code local build_ref_142
	ret_reg &ref[142]
	call_c   build_ref_141()
	call_c   Dyam_Create_Binary(I(9),&ref[141],I(7))
	move_ret ref[142]
	c_ret

;; TERM 141: '*GUARD*'(toplevel(dcg_term_expand(_C, _M)))
c_code local build_ref_141
	ret_reg &ref[141]
	call_c   build_ref_51()
	call_c   build_ref_140()
	call_c   Dyam_Create_Unary(&ref[51],&ref[140])
	move_ret ref[141]
	c_ret

;; TERM 140: toplevel(dcg_term_expand(_C, _M))
c_code local build_ref_140
	ret_reg &ref[140]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_979()
	call_c   Dyam_Create_Binary(&ref[979],V(2),V(12))
	move_ret R(0)
	call_c   build_ref_978()
	call_c   Dyam_Create_Unary(&ref[978],R(0))
	move_ret ref[140]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 978: toplevel
c_code local build_ref_978
	ret_reg &ref[978]
	call_c   Dyam_Create_Atom("toplevel")
	move_ret ref[978]
	c_ret

;; TERM 979: dcg_term_expand
c_code local build_ref_979
	ret_reg &ref[979]
	call_c   Dyam_Create_Atom("dcg_term_expand")
	move_ret ref[979]
	c_ret

c_code local build_seed_70
	ret_reg &seed[70]
	call_c   build_ref_51()
	call_c   build_ref_144()
	call_c   Dyam_Seed_Start(&ref[51],&ref[144],I(0),fun0,1)
	call_c   build_ref_145()
	call_c   Dyam_Seed_Add_Comp(&ref[145],&ref[144],0)
	call_c   Dyam_Seed_End()
	move_ret seed[70]
	c_ret

;; TERM 145: '*GUARD*'(dcg_il_body_to_lpda_handler(_B, _M, _D, _E, _F, _G, _H, _I, _J, _K, _L)) :> '$$HOLE$$'
c_code local build_ref_145
	ret_reg &ref[145]
	call_c   build_ref_144()
	call_c   Dyam_Create_Binary(I(9),&ref[144],I(7))
	move_ret ref[145]
	c_ret

;; TERM 144: '*GUARD*'(dcg_il_body_to_lpda_handler(_B, _M, _D, _E, _F, _G, _H, _I, _J, _K, _L))
c_code local build_ref_144
	ret_reg &ref[144]
	call_c   build_ref_51()
	call_c   build_ref_143()
	call_c   Dyam_Create_Unary(&ref[51],&ref[143])
	move_ret ref[144]
	c_ret

;; TERM 143: dcg_il_body_to_lpda_handler(_B, _M, _D, _E, _F, _G, _H, _I, _J, _K, _L)
c_code local build_ref_143
	ret_reg &ref[143]
	call_c   build_ref_835()
	call_c   Dyam_Term_Start(&ref[835],11)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_End()
	move_ret ref[143]
	c_ret

c_code local build_seed_76
	ret_reg &seed[76]
	call_c   build_ref_51()
	call_c   build_ref_176()
	call_c   Dyam_Seed_Start(&ref[51],&ref[176],I(0),fun0,1)
	call_c   build_ref_177()
	call_c   Dyam_Seed_Add_Comp(&ref[177],&ref[176],0)
	call_c   Dyam_Seed_End()
	move_ret seed[76]
	c_ret

;; TERM 177: '*GUARD*'(position_and_stacks(_B, _Q, _R)) :> '$$HOLE$$'
c_code local build_ref_177
	ret_reg &ref[177]
	call_c   build_ref_176()
	call_c   Dyam_Create_Binary(I(9),&ref[176],I(7))
	move_ret ref[177]
	c_ret

;; TERM 176: '*GUARD*'(position_and_stacks(_B, _Q, _R))
c_code local build_ref_176
	ret_reg &ref[176]
	call_c   build_ref_51()
	call_c   build_ref_175()
	call_c   Dyam_Create_Unary(&ref[51],&ref[175])
	move_ret ref[176]
	c_ret

;; TERM 175: position_and_stacks(_B, _Q, _R)
c_code local build_ref_175
	ret_reg &ref[175]
	call_c   build_ref_826()
	call_c   Dyam_Term_Start(&ref[826],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_End()
	move_ret ref[175]
	c_ret

c_code local build_seed_75
	ret_reg &seed[75]
	call_c   build_ref_51()
	call_c   build_ref_173()
	call_c   Dyam_Seed_Start(&ref[51],&ref[173],I(0),fun0,1)
	call_c   build_ref_174()
	call_c   Dyam_Seed_Add_Comp(&ref[174],&ref[173],0)
	call_c   Dyam_Seed_End()
	move_ret seed[75]
	c_ret

;; TERM 174: '*GUARD*'(position_and_stacks(_B, _O, _P)) :> '$$HOLE$$'
c_code local build_ref_174
	ret_reg &ref[174]
	call_c   build_ref_173()
	call_c   Dyam_Create_Binary(I(9),&ref[173],I(7))
	move_ret ref[174]
	c_ret

;; TERM 173: '*GUARD*'(position_and_stacks(_B, _O, _P))
c_code local build_ref_173
	ret_reg &ref[173]
	call_c   build_ref_51()
	call_c   build_ref_172()
	call_c   Dyam_Create_Unary(&ref[51],&ref[172])
	move_ret ref[173]
	c_ret

;; TERM 172: position_and_stacks(_B, _O, _P)
c_code local build_ref_172
	ret_reg &ref[172]
	call_c   build_ref_826()
	call_c   Dyam_Term_Start(&ref[826],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_End()
	move_ret ref[172]
	c_ret

c_code local build_seed_64
	ret_reg &seed[64]
	call_c   build_ref_51()
	call_c   build_ref_114()
	call_c   Dyam_Seed_Start(&ref[51],&ref[114],I(0),fun0,1)
	call_c   build_ref_115()
	call_c   Dyam_Seed_Add_Comp(&ref[115],&ref[114],0)
	call_c   Dyam_Seed_End()
	move_ret seed[64]
	c_ret

;; TERM 115: '*GUARD*'(toplevel(dcg_term_expand(_C, _K))) :> '$$HOLE$$'
c_code local build_ref_115
	ret_reg &ref[115]
	call_c   build_ref_114()
	call_c   Dyam_Create_Binary(I(9),&ref[114],I(7))
	move_ret ref[115]
	c_ret

;; TERM 114: '*GUARD*'(toplevel(dcg_term_expand(_C, _K)))
c_code local build_ref_114
	ret_reg &ref[114]
	call_c   build_ref_51()
	call_c   build_ref_113()
	call_c   Dyam_Create_Unary(&ref[51],&ref[113])
	move_ret ref[114]
	c_ret

;; TERM 113: toplevel(dcg_term_expand(_C, _K))
c_code local build_ref_113
	ret_reg &ref[113]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_979()
	call_c   Dyam_Create_Binary(&ref[979],V(2),V(10))
	move_ret R(0)
	call_c   build_ref_978()
	call_c   Dyam_Create_Unary(&ref[978],R(0))
	move_ret ref[113]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_65
	ret_reg &seed[65]
	call_c   build_ref_51()
	call_c   build_ref_117()
	call_c   Dyam_Seed_Start(&ref[51],&ref[117],I(0),fun0,1)
	call_c   build_ref_118()
	call_c   Dyam_Seed_Add_Comp(&ref[118],&ref[117],0)
	call_c   Dyam_Seed_End()
	move_ret seed[65]
	c_ret

;; TERM 118: '*GUARD*'(dcg_body_to_lpda_handler(_B, _K, _D, _E, _F, _G, _H, _I, _J)) :> '$$HOLE$$'
c_code local build_ref_118
	ret_reg &ref[118]
	call_c   build_ref_117()
	call_c   Dyam_Create_Binary(I(9),&ref[117],I(7))
	move_ret ref[118]
	c_ret

;; TERM 117: '*GUARD*'(dcg_body_to_lpda_handler(_B, _K, _D, _E, _F, _G, _H, _I, _J))
c_code local build_ref_117
	ret_reg &ref[117]
	call_c   build_ref_51()
	call_c   build_ref_116()
	call_c   Dyam_Create_Unary(&ref[51],&ref[116])
	move_ret ref[117]
	c_ret

;; TERM 116: dcg_body_to_lpda_handler(_B, _K, _D, _E, _F, _G, _H, _I, _J)
c_code local build_ref_116
	ret_reg &ref[116]
	call_c   build_ref_828()
	call_c   Dyam_Term_Start(&ref[828],9)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret ref[116]
	c_ret

c_code local build_seed_66
	ret_reg &seed[66]
	call_c   build_ref_51()
	call_c   build_ref_120()
	call_c   Dyam_Seed_Start(&ref[51],&ref[120],I(0),fun0,1)
	call_c   build_ref_121()
	call_c   Dyam_Seed_Add_Comp(&ref[121],&ref[120],0)
	call_c   Dyam_Seed_End()
	move_ret seed[66]
	c_ret

;; TERM 121: '*GUARD*'(bmg_non_terminal_to_lpda(_B, _K, _D, _E, _F, _G, _H, _I, _J)) :> '$$HOLE$$'
c_code local build_ref_121
	ret_reg &ref[121]
	call_c   build_ref_120()
	call_c   Dyam_Create_Binary(I(9),&ref[120],I(7))
	move_ret ref[121]
	c_ret

;; TERM 120: '*GUARD*'(bmg_non_terminal_to_lpda(_B, _K, _D, _E, _F, _G, _H, _I, _J))
c_code local build_ref_120
	ret_reg &ref[120]
	call_c   build_ref_51()
	call_c   build_ref_119()
	call_c   Dyam_Create_Unary(&ref[51],&ref[119])
	move_ret ref[120]
	c_ret

;; TERM 119: bmg_non_terminal_to_lpda(_B, _K, _D, _E, _F, _G, _H, _I, _J)
c_code local build_ref_119
	ret_reg &ref[119]
	call_c   build_ref_914()
	call_c   Dyam_Term_Start(&ref[914],9)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret ref[119]
	c_ret

c_code local build_seed_57
	ret_reg &seed[57]
	call_c   build_ref_39()
	call_c   build_ref_67()
	call_c   Dyam_Seed_Start(&ref[39],&ref[67],&ref[67],fun0,1)
	call_c   build_ref_69()
	call_c   Dyam_Seed_Add_Comp(&ref[69],&ref[67],0)
	call_c   Dyam_Seed_End()
	move_ret seed[57]
	c_ret

;; TERM 69: '*FIRST*'('call_bmg_get_stacks/1') :> '$$HOLE$$'
c_code local build_ref_69
	ret_reg &ref[69]
	call_c   build_ref_68()
	call_c   Dyam_Create_Binary(I(9),&ref[68],I(7))
	move_ret ref[69]
	c_ret

;; TERM 68: '*FIRST*'('call_bmg_get_stacks/1')
c_code local build_ref_68
	ret_reg &ref[68]
	call_c   build_ref_36()
	call_c   build_ref_16()
	call_c   Dyam_Create_Unary(&ref[36],&ref[16])
	move_ret ref[68]
	c_ret

;; TERM 16: 'call_bmg_get_stacks/1'
c_code local build_ref_16
	ret_reg &ref[16]
	call_c   Dyam_Create_Atom("call_bmg_get_stacks/1")
	move_ret ref[16]
	c_ret

;; TERM 67: '*CITEM*'('call_bmg_get_stacks/1', 'call_bmg_get_stacks/1')
c_code local build_ref_67
	ret_reg &ref[67]
	call_c   build_ref_39()
	call_c   build_ref_16()
	call_c   Dyam_Create_Binary(&ref[39],&ref[16],&ref[16])
	move_ret ref[67]
	c_ret

c_code local build_seed_58
	ret_reg &seed[58]
	call_c   build_ref_70()
	call_c   build_ref_74()
	call_c   Dyam_Seed_Start(&ref[70],&ref[74],I(0),fun4,1)
	call_c   build_ref_72()
	call_c   Dyam_Seed_Add_Comp(&ref[72],fun14,0)
	call_c   Dyam_Seed_End()
	move_ret seed[58]
	c_ret

;; TERM 72: '*RITEM*'('call_bmg_get_stacks/1', return(_C))
c_code local build_ref_72
	ret_reg &ref[72]
	call_c   build_ref_0()
	call_c   build_ref_16()
	call_c   build_ref_71()
	call_c   Dyam_Create_Binary(&ref[0],&ref[16],&ref[71])
	move_ret ref[72]
	c_ret

;; TERM 71: return(_C)
c_code local build_ref_71
	ret_reg &ref[71]
	call_c   build_ref_867()
	call_c   Dyam_Create_Unary(&ref[867],V(2))
	move_ret ref[71]
	c_ret

pl_code local fun14
	call_c   build_ref_72()
	call_c   Dyam_Unify_Item(&ref[72])
	fail_ret
	pl_jump  Follow_Cont(V(1))

;; TERM 74: '*RITEM*'('call_bmg_get_stacks/1', return(_C)) :> dcg(0, '$TUPPLE'(35074870873324))
c_code local build_ref_74
	ret_reg &ref[74]
	call_c   build_ref_72()
	call_c   build_ref_73()
	call_c   Dyam_Create_Binary(I(9),&ref[72],&ref[73])
	move_ret ref[74]
	c_ret

;; TERM 73: dcg(0, '$TUPPLE'(35074870873324))
c_code local build_ref_73
	ret_reg &ref[73]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,134217728)
	move_ret R(0)
	call_c   build_ref_839()
	call_c   Dyam_Create_Binary(&ref[839],N(0),R(0))
	move_ret ref[73]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_60
	ret_reg &seed[60]
	call_c   build_ref_51()
	call_c   build_ref_83()
	call_c   Dyam_Seed_Start(&ref[51],&ref[83],I(0),fun0,1)
	call_c   build_ref_84()
	call_c   Dyam_Seed_Add_Comp(&ref[84],&ref[83],0)
	call_c   Dyam_Seed_End()
	move_ret seed[60]
	c_ret

;; TERM 84: '*GUARD*'(bmg_equations(_F, _E, _G, _I)) :> '$$HOLE$$'
c_code local build_ref_84
	ret_reg &ref[84]
	call_c   build_ref_83()
	call_c   Dyam_Create_Binary(I(9),&ref[83],I(7))
	move_ret ref[84]
	c_ret

;; TERM 83: '*GUARD*'(bmg_equations(_F, _E, _G, _I))
c_code local build_ref_83
	ret_reg &ref[83]
	call_c   build_ref_51()
	call_c   build_ref_82()
	call_c   Dyam_Create_Unary(&ref[51],&ref[82])
	move_ret ref[83]
	c_ret

;; TERM 82: bmg_equations(_F, _E, _G, _I)
c_code local build_ref_82
	ret_reg &ref[82]
	call_c   build_ref_864()
	call_c   Dyam_Term_Start(&ref[864],4)
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_End()
	move_ret ref[82]
	c_ret

;; TERM 4: term_module_shift(_A, _B, _C, _D, _E)
c_code local build_ref_4
	ret_reg &ref[4]
	call_c   build_ref_980()
	call_c   Dyam_Term_Start(&ref[980],5)
	call_c   Dyam_Term_Arg(V(0))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[4]
	c_ret

;; TERM 980: term_module_shift
c_code local build_ref_980
	ret_reg &ref[980]
	call_c   Dyam_Create_Atom("term_module_shift")
	move_ret ref[980]
	c_ret

;; TERM 3: '*RITEM*'('call_term_module_shift/5'(_A, _B), return(_C, _D, _E))
c_code local build_ref_3
	ret_reg &ref[3]
	call_c   build_ref_0()
	call_c   build_ref_1()
	call_c   build_ref_2()
	call_c   Dyam_Create_Binary(&ref[0],&ref[1],&ref[2])
	move_ret ref[3]
	c_ret

;; TERM 2: return(_C, _D, _E)
c_code local build_ref_2
	ret_reg &ref[2]
	call_c   build_ref_867()
	call_c   Dyam_Term_Start(&ref[867],3)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[2]
	c_ret

;; TERM 1: 'call_term_module_shift/5'(_A, _B)
c_code local build_ref_1
	ret_reg &ref[1]
	call_c   build_ref_873()
	call_c   Dyam_Create_Binary(&ref[873],V(0),V(1))
	move_ret ref[1]
	c_ret

;; TERM 8: dcg_prolog_make(_A, _B, _C, _D, _E, _F, _G)
c_code local build_ref_8
	ret_reg &ref[8]
	call_c   build_ref_902()
	call_c   Dyam_Term_Start(&ref[902],7)
	call_c   Dyam_Term_Arg(V(0))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[8]
	c_ret

;; TERM 7: '*RITEM*'('call_dcg_prolog_make/7'(_A), return(_B, _C, _D, _E, _F, _G))
c_code local build_ref_7
	ret_reg &ref[7]
	call_c   build_ref_0()
	call_c   build_ref_5()
	call_c   build_ref_6()
	call_c   Dyam_Create_Binary(&ref[0],&ref[5],&ref[6])
	move_ret ref[7]
	c_ret

;; TERM 6: return(_B, _C, _D, _E, _F, _G)
c_code local build_ref_6
	ret_reg &ref[6]
	call_c   build_ref_867()
	call_c   Dyam_Term_Start(&ref[867],6)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[6]
	c_ret

;; TERM 5: 'call_dcg_prolog_make/7'(_A)
c_code local build_ref_5
	ret_reg &ref[5]
	call_c   build_ref_888()
	call_c   Dyam_Create_Unary(&ref[888],V(0))
	move_ret ref[5]
	c_ret

;; TERM 12: bmg_args(_A, _B, _C, _D, _E, _F)
c_code local build_ref_12
	ret_reg &ref[12]
	call_c   build_ref_892()
	call_c   Dyam_Term_Start(&ref[892],6)
	call_c   Dyam_Term_Arg(V(0))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[12]
	c_ret

;; TERM 11: '*RITEM*'('call_bmg_args/6'(_C, _D), return(_A, _B, _E, _F))
c_code local build_ref_11
	ret_reg &ref[11]
	call_c   build_ref_0()
	call_c   build_ref_9()
	call_c   build_ref_10()
	call_c   Dyam_Create_Binary(&ref[0],&ref[9],&ref[10])
	move_ret ref[11]
	c_ret

;; TERM 10: return(_A, _B, _E, _F)
c_code local build_ref_10
	ret_reg &ref[10]
	call_c   build_ref_867()
	call_c   Dyam_Term_Start(&ref[867],4)
	call_c   Dyam_Term_Arg(V(0))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[10]
	c_ret

;; TERM 9: 'call_bmg_args/6'(_C, _D)
c_code local build_ref_9
	ret_reg &ref[9]
	call_c   build_ref_977()
	call_c   Dyam_Create_Binary(&ref[977],V(2),V(3))
	move_ret ref[9]
	c_ret

;; TERM 13: wrapping_predicate(_A)
c_code local build_ref_13
	ret_reg &ref[13]
	call_c   build_ref_905()
	call_c   Dyam_Create_Unary(&ref[905],V(0))
	move_ret ref[13]
	c_ret

;; TERM 15: '*RITEM*'(wrapping_predicate(_A), voidret)
c_code local build_ref_15
	ret_reg &ref[15]
	call_c   build_ref_0()
	call_c   build_ref_13()
	call_c   build_ref_14()
	call_c   Dyam_Create_Binary(&ref[0],&ref[13],&ref[14])
	move_ret ref[15]
	c_ret

;; TERM 19: bmg_get_stacks(_A)
c_code local build_ref_19
	ret_reg &ref[19]
	call_c   build_ref_827()
	call_c   Dyam_Create_Unary(&ref[827],V(0))
	move_ret ref[19]
	c_ret

;; TERM 18: '*RITEM*'('call_bmg_get_stacks/1', return(_A))
c_code local build_ref_18
	ret_reg &ref[18]
	call_c   build_ref_0()
	call_c   build_ref_16()
	call_c   build_ref_17()
	call_c   Dyam_Create_Binary(&ref[0],&ref[16],&ref[17])
	move_ret ref[18]
	c_ret

;; TERM 17: return(_A)
c_code local build_ref_17
	ret_reg &ref[17]
	call_c   build_ref_867()
	call_c   Dyam_Create_Unary(&ref[867],V(0))
	move_ret ref[17]
	c_ret

;; TERM 20: dcg_il_args(_A, _B, _C, _D)
c_code local build_ref_20
	ret_reg &ref[20]
	call_c   build_ref_833()
	call_c   Dyam_Term_Start(&ref[833],4)
	call_c   Dyam_Term_Arg(V(0))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[20]
	c_ret

;; TERM 21: '*RITEM*'(dcg_il_args(_A, _B, _C, _D), voidret)
c_code local build_ref_21
	ret_reg &ref[21]
	call_c   build_ref_0()
	call_c   build_ref_20()
	call_c   build_ref_14()
	call_c   Dyam_Create_Binary(&ref[0],&ref[20],&ref[14])
	move_ret ref[21]
	c_ret

;; TERM 27: dcg_kleene_args(_A, _B, _C, _D, _E, _F, _G)
c_code local build_ref_27
	ret_reg &ref[27]
	call_c   build_ref_981()
	call_c   Dyam_Term_Start(&ref[981],7)
	call_c   Dyam_Term_Arg(V(0))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[27]
	c_ret

;; TERM 981: dcg_kleene_args
c_code local build_ref_981
	ret_reg &ref[981]
	call_c   Dyam_Create_Atom("dcg_kleene_args")
	move_ret ref[981]
	c_ret

;; TERM 26: '*RITEM*'('call_dcg_kleene_args/7', return(_A, _B, _C, _D, _E, _F, _G))
c_code local build_ref_26
	ret_reg &ref[26]
	call_c   build_ref_0()
	call_c   build_ref_24()
	call_c   build_ref_25()
	call_c   Dyam_Create_Binary(&ref[0],&ref[24],&ref[25])
	move_ret ref[26]
	c_ret

;; TERM 25: return(_A, _B, _C, _D, _E, _F, _G)
c_code local build_ref_25
	ret_reg &ref[25]
	call_c   build_ref_867()
	call_c   Dyam_Term_Start(&ref[867],7)
	call_c   Dyam_Term_Arg(V(0))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[25]
	c_ret

;; TERM 30: register_predicate(_A)
c_code local build_ref_30
	ret_reg &ref[30]
	call_c   build_ref_838()
	call_c   Dyam_Create_Unary(&ref[838],V(0))
	move_ret ref[30]
	c_ret

;; TERM 31: '*RITEM*'(register_predicate(_A), voidret)
c_code local build_ref_31
	ret_reg &ref[31]
	call_c   build_ref_0()
	call_c   build_ref_30()
	call_c   build_ref_14()
	call_c   Dyam_Create_Binary(&ref[0],&ref[30],&ref[14])
	move_ret ref[31]
	c_ret

;; TERM 35: bmg_head_stacks(_A, _B, _C, _D, _E, _F)
c_code local build_ref_35
	ret_reg &ref[35]
	call_c   build_ref_903()
	call_c   Dyam_Term_Start(&ref[903],6)
	call_c   Dyam_Term_Arg(V(0))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[35]
	c_ret

;; TERM 34: '*RITEM*'('call_bmg_head_stacks/6'(_A), return(_B, _C, _D, _E, _F))
c_code local build_ref_34
	ret_reg &ref[34]
	call_c   build_ref_0()
	call_c   build_ref_32()
	call_c   build_ref_33()
	call_c   Dyam_Create_Binary(&ref[0],&ref[32],&ref[33])
	move_ret ref[34]
	c_ret

;; TERM 33: return(_B, _C, _D, _E, _F)
c_code local build_ref_33
	ret_reg &ref[33]
	call_c   build_ref_867()
	call_c   Dyam_Term_Start(&ref[867],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[33]
	c_ret

;; TERM 32: 'call_bmg_head_stacks/6'(_A)
c_code local build_ref_32
	ret_reg &ref[32]
	call_c   build_ref_976()
	call_c   Dyam_Create_Unary(&ref[976],V(0))
	move_ret ref[32]
	c_ret

;; TERM 664: [',',+>,<+,&]
c_code local build_ref_664
	ret_reg &ref[664]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_886()
	call_c   Dyam_Create_List(&ref[886],I(0))
	move_ret R(0)
	call_c   build_ref_887()
	call_c   Dyam_Create_List(&ref[887],R(0))
	move_ret R(0)
	call_c   build_ref_861()
	call_c   Dyam_Create_List(&ref[861],R(0))
	move_ret R(0)
	call_c   Dyam_Create_List(I(4),R(0))
	move_ret ref[664]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 665: _G , _H
c_code local build_ref_665
	ret_reg &ref[665]
	call_c   Dyam_Create_Binary(I(4),V(6),V(7))
	move_ret ref[665]
	c_ret

;; TERM 666: _E -> _F
c_code local build_ref_666
	ret_reg &ref[666]
	call_c   build_ref_847()
	call_c   Dyam_Create_Binary(&ref[847],V(4),V(5))
	move_ret ref[666]
	c_ret

;; TERM 668: _E , _F
c_code local build_ref_668
	ret_reg &ref[668]
	call_c   Dyam_Create_Binary(I(4),V(4),V(5))
	move_ret ref[668]
	c_ret

;; TERM 667: _E -> _F ; _I
c_code local build_ref_667
	ret_reg &ref[667]
	call_c   build_ref_666()
	call_c   Dyam_Create_Binary(I(5),&ref[666],V(8))
	move_ret ref[667]
	c_ret

;; TERM 669: _G xor _H
c_code local build_ref_669
	ret_reg &ref[669]
	call_c   build_ref_968()
	call_c   Dyam_Create_Binary(&ref[968],V(6),V(7))
	move_ret ref[669]
	c_ret

;; TERM 670: _E ; _F
c_code local build_ref_670
	ret_reg &ref[670]
	call_c   Dyam_Create_Binary(I(5),V(4),V(5))
	move_ret ref[670]
	c_ret

;; TERM 462: dcg_prolog_make(_C, _D, _E, _F, _G, _H, _I)
c_code local build_ref_462
	ret_reg &ref[462]
	call_c   build_ref_902()
	call_c   Dyam_Term_Start(&ref[902],7)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_End()
	move_ret ref[462]
	c_ret

;; TERM 444: bmg_head_stacks(_C, _D, _E, _F, _G, _H)
c_code local build_ref_444
	ret_reg &ref[444]
	call_c   build_ref_903()
	call_c   Dyam_Term_Start(&ref[903],6)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[444]
	c_ret

;; TERM 416: bmg_args(_C, _D, _E, _F, _G, _H)
c_code local build_ref_416
	ret_reg &ref[416]
	call_c   build_ref_892()
	call_c   Dyam_Term_Start(&ref[892],6)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[416]
	c_ret

;; TERM 313: _E44070094
c_code local build_ref_313
	ret_reg &ref[313]
	call_c   Dyam_Create_Unary(I(6),V(21))
	move_ret ref[313]
	c_ret

;; TERM 314: '$protect'(_K)
c_code local build_ref_314
	ret_reg &ref[314]
	call_c   build_ref_982()
	call_c   Dyam_Create_Unary(&ref[982],V(10))
	move_ret ref[314]
	c_ret

;; TERM 982: '$protect'
c_code local build_ref_982
	ret_reg &ref[982]
	call_c   Dyam_Create_Atom("$protect")
	move_ret ref[982]
	c_ret

;; TERM 311: _Q44068160
c_code local build_ref_311
	ret_reg &ref[311]
	call_c   Dyam_Create_Unary(I(6),V(20))
	move_ret ref[311]
	c_ret

;; TERM 312: '$protect'(_L)
c_code local build_ref_312
	ret_reg &ref[312]
	call_c   build_ref_982()
	call_c   Dyam_Create_Unary(&ref[982],V(11))
	move_ret ref[312]
	c_ret

;; TERM 309: _G44068161
c_code local build_ref_309
	ret_reg &ref[309]
	call_c   Dyam_Create_Unary(I(6),V(19))
	move_ret ref[309]
	c_ret

;; TERM 310: '$protect'(_N)
c_code local build_ref_310
	ret_reg &ref[310]
	call_c   build_ref_982()
	call_c   Dyam_Create_Unary(&ref[982],V(13))
	move_ret ref[310]
	c_ret

;; TERM 307: _W44068161
c_code local build_ref_307
	ret_reg &ref[307]
	call_c   Dyam_Create_Unary(I(6),V(18))
	move_ret ref[307]
	c_ret

;; TERM 308: '$protect'(_M)
c_code local build_ref_308
	ret_reg &ref[308]
	call_c   build_ref_982()
	call_c   Dyam_Create_Unary(&ref[982],V(12))
	move_ret ref[308]
	c_ret

;; TERM 305: _M44068162
c_code local build_ref_305
	ret_reg &ref[305]
	call_c   Dyam_Create_Unary(I(6),V(17))
	move_ret ref[305]
	c_ret

;; TERM 306: '$protect'(_O)
c_code local build_ref_306
	ret_reg &ref[306]
	call_c   build_ref_982()
	call_c   Dyam_Create_Unary(&ref[982],V(14))
	move_ret ref[306]
	c_ret

;; TERM 303: _C44068163
c_code local build_ref_303
	ret_reg &ref[303]
	call_c   Dyam_Create_Unary(I(6),V(16))
	move_ret ref[303]
	c_ret

;; TERM 304: '$protect'(_P)
c_code local build_ref_304
	ret_reg &ref[304]
	call_c   build_ref_982()
	call_c   Dyam_Create_Unary(&ref[982],V(15))
	move_ret ref[304]
	c_ret

;; TERM 316: @*{goal=> _B, vars=> _K, from=> _D, to=> _E, collect_first=> _L, collect_last=> _M, collect_loop=> _N, collect_next=> _O, collect_pred=> _P}
c_code local build_ref_316
	ret_reg &ref[316]
	call_c   build_ref_849()
	call_c   Dyam_Term_Start(&ref[849],9)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_End()
	move_ret ref[316]
	c_ret

;; TERM 315: @*{goal=> _B, vars=> _C, from=> _D, to=> _E, collect_first=> _F, collect_last=> _G, collect_loop=> _H, collect_next=> _I, collect_pred=> _J}
c_code local build_ref_315
	ret_reg &ref[315]
	call_c   build_ref_849()
	call_c   Dyam_Term_Start(&ref[849],9)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret ref[315]
	c_ret

;; TERM 300: '$CLOSURE'('$fun'(82, 0, 1147254624), '$TUPPLE'(35074870876128))
c_code local build_ref_300
	ret_reg &ref[300]
	call_c   build_ref_299()
	call_c   Dyam_Closure_Aux(fun82,&ref[299])
	move_ret ref[300]
	c_ret

c_code local build_seed_92
	ret_reg &seed[92]
	call_c   build_ref_51()
	call_c   build_ref_296()
	call_c   Dyam_Seed_Start(&ref[51],&ref[296],I(0),fun0,1)
	call_c   build_ref_297()
	call_c   Dyam_Seed_Add_Comp(&ref[297],&ref[296],0)
	call_c   Dyam_Seed_End()
	move_ret seed[92]
	c_ret

;; TERM 297: '*GUARD*'(body_to_lpda(_B, interleave_register(_O, _K, _G, _H), _I, _M, _N)) :> '$$HOLE$$'
c_code local build_ref_297
	ret_reg &ref[297]
	call_c   build_ref_296()
	call_c   Dyam_Create_Binary(I(9),&ref[296],I(7))
	move_ret ref[297]
	c_ret

;; TERM 296: '*GUARD*'(body_to_lpda(_B, interleave_register(_O, _K, _G, _H), _I, _M, _N))
c_code local build_ref_296
	ret_reg &ref[296]
	call_c   build_ref_51()
	call_c   build_ref_295()
	call_c   Dyam_Create_Unary(&ref[51],&ref[295])
	move_ret ref[296]
	c_ret

;; TERM 295: body_to_lpda(_B, interleave_register(_O, _K, _G, _H), _I, _M, _N)
c_code local build_ref_295
	ret_reg &ref[295]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_983()
	call_c   Dyam_Term_Start(&ref[983],4)
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_875()
	call_c   Dyam_Term_Start(&ref[875],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_End()
	move_ret ref[295]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 983: interleave_register
c_code local build_ref_983
	ret_reg &ref[983]
	call_c   Dyam_Create_Atom("interleave_register")
	move_ret ref[983]
	c_ret

long local pool_fun82[2]=[1,build_seed_92]

pl_code local fun82
	call_c   Dyam_Pool(pool_fun82)
	call_c   Dyam_Allocate(0)
	pl_call  fun7(&seed[92],1)
	call_c   Dyam_Deallocate()
	pl_ret

;; TERM 299: '$TUPPLE'(35074870876128)
c_code local build_ref_299
	ret_reg &ref[299]
	call_c   Dyam_Create_Simple_Tupple(0,141934592)
	move_ret ref[299]
	c_ret

;; TERM 302: '*IL-ALT*'(_K, _C, _L, _J) :> _M
c_code local build_ref_302
	ret_reg &ref[302]
	call_c   build_ref_301()
	call_c   Dyam_Create_Binary(I(9),&ref[301],V(12))
	move_ret ref[302]
	c_ret

;; TERM 301: '*IL-ALT*'(_K, _C, _L, _J)
c_code local build_ref_301
	ret_reg &ref[301]
	call_c   build_ref_984()
	call_c   Dyam_Term_Start(&ref[984],4)
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret ref[301]
	c_ret

;; TERM 984: '*IL-ALT*'
c_code local build_ref_984
	ret_reg &ref[984]
	call_c   Dyam_Create_Atom("*IL-ALT*")
	move_ret ref[984]
	c_ret

;; TERM 292: '$CLOSURE'('$fun'(80, 0, 1147224976), '$TUPPLE'(35074870875824))
c_code local build_ref_292
	ret_reg &ref[292]
	call_c   build_ref_291()
	call_c   Dyam_Closure_Aux(fun80,&ref[291])
	move_ret ref[292]
	c_ret

c_code local build_seed_91
	ret_reg &seed[91]
	call_c   build_ref_51()
	call_c   build_ref_288()
	call_c   Dyam_Seed_Start(&ref[51],&ref[288],I(0),fun0,1)
	call_c   build_ref_289()
	call_c   Dyam_Seed_Add_Comp(&ref[289],&ref[288],0)
	call_c   Dyam_Seed_End()
	move_ret seed[91]
	c_ret

;; TERM 289: '*GUARD*'(body_to_lpda(_B, interleave_start(_N, _J, _G, _H), _I, _L, _M)) :> '$$HOLE$$'
c_code local build_ref_289
	ret_reg &ref[289]
	call_c   build_ref_288()
	call_c   Dyam_Create_Binary(I(9),&ref[288],I(7))
	move_ret ref[289]
	c_ret

;; TERM 288: '*GUARD*'(body_to_lpda(_B, interleave_start(_N, _J, _G, _H), _I, _L, _M))
c_code local build_ref_288
	ret_reg &ref[288]
	call_c   build_ref_51()
	call_c   build_ref_287()
	call_c   Dyam_Create_Unary(&ref[51],&ref[287])
	move_ret ref[288]
	c_ret

;; TERM 287: body_to_lpda(_B, interleave_start(_N, _J, _G, _H), _I, _L, _M)
c_code local build_ref_287
	ret_reg &ref[287]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_985()
	call_c   Dyam_Term_Start(&ref[985],4)
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_875()
	call_c   Dyam_Term_Start(&ref[875],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_End()
	move_ret ref[287]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 985: interleave_start
c_code local build_ref_985
	ret_reg &ref[985]
	call_c   Dyam_Create_Atom("interleave_start")
	move_ret ref[985]
	c_ret

long local pool_fun80[2]=[1,build_seed_91]

pl_code local fun80
	call_c   Dyam_Pool(pool_fun80)
	call_c   Dyam_Allocate(0)
	pl_call  fun7(&seed[91],1)
	call_c   Dyam_Deallocate()
	pl_ret

;; TERM 291: '$TUPPLE'(35074870875824)
c_code local build_ref_291
	ret_reg &ref[291]
	call_c   Dyam_Create_Simple_Tupple(0,142311424)
	move_ret ref[291]
	c_ret

;; TERM 294: '*IL-ALT*'(_J, _C, _K, []) :> _L
c_code local build_ref_294
	ret_reg &ref[294]
	call_c   build_ref_293()
	call_c   Dyam_Create_Binary(I(9),&ref[293],V(11))
	move_ret ref[294]
	c_ret

;; TERM 293: '*IL-ALT*'(_J, _C, _K, [])
c_code local build_ref_293
	ret_reg &ref[293]
	call_c   build_ref_984()
	call_c   Dyam_Term_Start(&ref[984],4)
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_End()
	move_ret ref[293]
	c_ret

;; TERM 285: '$CLOSURE'('$fun'(78, 0, 1147194064), '$TUPPLE'(35074870875556))
c_code local build_ref_285
	ret_reg &ref[285]
	call_c   build_ref_284()
	call_c   Dyam_Closure_Aux(fun78,&ref[284])
	move_ret ref[285]
	c_ret

;; TERM 279: il_choice
c_code local build_ref_279
	ret_reg &ref[279]
	call_c   Dyam_Create_Atom("il_choice")
	move_ret ref[279]
	c_ret

c_code local build_seed_90
	ret_reg &seed[90]
	call_c   build_ref_51()
	call_c   build_ref_281()
	call_c   Dyam_Seed_Start(&ref[51],&ref[281],I(0),fun0,1)
	call_c   build_ref_282()
	call_c   Dyam_Seed_Add_Comp(&ref[282],&ref[281],0)
	call_c   Dyam_Seed_End()
	move_ret seed[90]
	c_ret

;; TERM 282: '*GUARD*'(body_to_lpda(_B, interleave_choose(_L, _E, _H, _I), '*IL-LOOP*'(_I, _K), _F, _G)) :> '$$HOLE$$'
c_code local build_ref_282
	ret_reg &ref[282]
	call_c   build_ref_281()
	call_c   Dyam_Create_Binary(I(9),&ref[281],I(7))
	move_ret ref[282]
	c_ret

;; TERM 281: '*GUARD*'(body_to_lpda(_B, interleave_choose(_L, _E, _H, _I), '*IL-LOOP*'(_I, _K), _F, _G))
c_code local build_ref_281
	ret_reg &ref[281]
	call_c   build_ref_51()
	call_c   build_ref_280()
	call_c   Dyam_Create_Unary(&ref[51],&ref[280])
	move_ret ref[281]
	c_ret

;; TERM 280: body_to_lpda(_B, interleave_choose(_L, _E, _H, _I), '*IL-LOOP*'(_I, _K), _F, _G)
c_code local build_ref_280
	ret_reg &ref[280]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_986()
	call_c   Dyam_Term_Start(&ref[986],4)
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_987()
	call_c   Dyam_Create_Binary(&ref[987],V(8),V(10))
	move_ret R(1)
	call_c   build_ref_875()
	call_c   Dyam_Term_Start(&ref[875],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[280]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 987: '*IL-LOOP*'
c_code local build_ref_987
	ret_reg &ref[987]
	call_c   Dyam_Create_Atom("*IL-LOOP*")
	move_ret ref[987]
	c_ret

;; TERM 986: interleave_choose
c_code local build_ref_986
	ret_reg &ref[986]
	call_c   Dyam_Create_Atom("interleave_choose")
	move_ret ref[986]
	c_ret

long local pool_fun78[3]=[2,build_ref_279,build_seed_90]

pl_code local fun78
	call_c   Dyam_Pool(pool_fun78)
	call_c   Dyam_Allocate(0)
	move     &ref[279], R(0)
	move     0, R(1)
	move     V(11), R(2)
	move     S(5), R(3)
	pl_call  pred_update_counter_2()
	pl_call  fun7(&seed[90],1)
	call_c   Dyam_Deallocate()
	pl_ret

;; TERM 284: '$TUPPLE'(35074870875556)
c_code local build_ref_284
	ret_reg &ref[284]
	call_c   Dyam_Create_Simple_Tupple(0,166985728)
	move_ret ref[284]
	c_ret

;; TERM 271: [_H,_I]
c_code local build_ref_271
	ret_reg &ref[271]
	call_c   Dyam_Create_Tupple(7,8,I(0))
	move_ret ref[271]
	c_ret

;; TERM 286: '*IL-LOOP-WRAP*'(_F)
c_code local build_ref_286
	ret_reg &ref[286]
	call_c   build_ref_988()
	call_c   Dyam_Create_Unary(&ref[988],V(5))
	move_ret ref[286]
	c_ret

;; TERM 988: '*IL-LOOP-WRAP*'
c_code local build_ref_988
	ret_reg &ref[988]
	call_c   Dyam_Create_Atom("*IL-LOOP-WRAP*")
	move_ret ref[988]
	c_ret

;; TERM 183: true , _C
c_code local build_ref_183
	ret_reg &ref[183]
	call_c   build_ref_186()
	call_c   Dyam_Create_Binary(I(4),&ref[186],V(2))
	move_ret ref[183]
	c_ret

;; TERM 184: _C , true
c_code local build_ref_184
	ret_reg &ref[184]
	call_c   build_ref_186()
	call_c   Dyam_Create_Binary(I(4),V(2),&ref[186])
	move_ret ref[184]
	c_ret

;; TERM 185: true xor _D
c_code local build_ref_185
	ret_reg &ref[185]
	call_c   build_ref_968()
	call_c   build_ref_186()
	call_c   Dyam_Create_Binary(&ref[968],&ref[186],V(3))
	move_ret ref[185]
	c_ret

;; TERM 187: _E xor true
c_code local build_ref_187
	ret_reg &ref[187]
	call_c   build_ref_968()
	call_c   build_ref_186()
	call_c   Dyam_Create_Binary(&ref[968],V(4),&ref[186])
	move_ret ref[187]
	c_ret

;; TERM 181: [_X ^ _T,[_V|_W],[_Q|_R]] ^ _Y
c_code local build_ref_181
	ret_reg &ref[181]
	call_c   Dyam_Pseudo_Choice(3)
	call_c   build_ref_860()
	call_c   Dyam_Create_Binary(&ref[860],V(23),V(19))
	move_ret R(0)
	call_c   Dyam_Create_List(V(21),V(22))
	move_ret R(1)
	call_c   Dyam_Create_List(V(16),V(17))
	move_ret R(2)
	call_c   Dyam_Create_List(R(2),I(0))
	move_ret R(2)
	call_c   Dyam_Create_List(R(1),R(2))
	move_ret R(2)
	call_c   Dyam_Create_List(R(0),R(2))
	move_ret R(2)
	call_c   Dyam_Create_Binary(&ref[860],R(2),V(24))
	move_ret ref[181]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 180: [_T|_X]
c_code local build_ref_180
	ret_reg &ref[180]
	call_c   Dyam_Create_List(V(19),V(23))
	move_ret ref[180]
	c_ret

;; TERM 182: _H :> _T
c_code local build_ref_182
	ret_reg &ref[182]
	call_c   Dyam_Create_Binary(I(9),V(7),V(19))
	move_ret ref[182]
	c_ret

;; TERM 178: [_F,_G]
c_code local build_ref_178
	ret_reg &ref[178]
	call_c   Dyam_Create_Tupple(5,6,I(0))
	move_ret ref[178]
	c_ret

;; TERM 81: unify(_C, _D) :> _I
c_code local build_ref_81
	ret_reg &ref[81]
	call_c   build_ref_58()
	call_c   Dyam_Create_Binary(I(9),&ref[58],V(8))
	move_ret ref[81]
	c_ret

;; TERM 59: unify(_C, _D) :> _E
c_code local build_ref_59
	ret_reg &ref[59]
	call_c   build_ref_58()
	call_c   Dyam_Create_Binary(I(9),&ref[58],V(4))
	move_ret ref[59]
	c_ret

;; TERM 49: [_D]
c_code local build_ref_49
	ret_reg &ref[49]
	call_c   Dyam_Create_List(V(3),I(0))
	move_ret ref[49]
	c_ret

;; TERM 48: '_interleave~w'
c_code local build_ref_48
	ret_reg &ref[48]
	call_c   Dyam_Create_Atom("_interleave~w")
	move_ret ref[48]
	c_ret

;; TERM 47: interleave
c_code local build_ref_47
	ret_reg &ref[47]
	call_c   Dyam_Create_Atom("interleave")
	move_ret ref[47]
	c_ret

pl_code local fun15
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Object(R(0))
	call_c   Dyam_Tabule()
	pl_ret

pl_code local fun1
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Object(R(0))
	call_c   Dyam_Tabule()
	pl_ret

pl_code local fun234
	call_c   Dyam_Remove_Choice()
	pl_call  fun21(&seed[156],2,V(3))
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun221
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(2),&ref[186])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun222
	call_c   Dyam_Update_Choice(fun221)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[670])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(4))
	move     V(6), R(2)
	move     S(5), R(3)
	pl_call  pred_dcg_autoloader_2()
	call_c   Dyam_Reg_Load(0,V(5))
	move     V(7), R(2)
	move     S(5), R(3)
	pl_call  pred_dcg_autoloader_2()
	move     &ref[669], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(2))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_autoloader_simplify_2()

pl_code local fun223
	call_c   Dyam_Update_Choice(fun222)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[667])
	fail_ret
	call_c   Dyam_Cut()
	move     &ref[668], R(0)
	move     S(5), R(1)
	move     V(6), R(2)
	move     S(5), R(3)
	pl_call  pred_dcg_autoloader_2()
	call_c   Dyam_Reg_Load(0,V(8))
	move     V(7), R(2)
	move     S(5), R(3)
	pl_call  pred_dcg_autoloader_2()
	move     &ref[669], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(2))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_autoloader_simplify_2()

pl_code local fun224
	call_c   Dyam_Update_Choice(fun223)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[666])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(4))
	move     V(6), R(2)
	move     S(5), R(3)
	pl_call  pred_dcg_autoloader_2()
	call_c   Dyam_Reg_Load(0,V(5))
	move     V(7), R(2)
	move     S(5), R(3)
	pl_call  pred_dcg_autoloader_2()
	move     &ref[665], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(2))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_autoloader_simplify_2()

pl_code local fun212
	call_c   Dyam_Remove_Choice()
	pl_call  fun21(&seed[141],2,V(2))
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun160
	call_c   Dyam_Remove_Choice()
	pl_call  fun21(&seed[115],2,V(9))
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun157
	call_c   Dyam_Remove_Choice()
	pl_call  fun21(&seed[113],2,V(3))
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun154
	call_c   Dyam_Remove_Choice()
	pl_call  fun21(&seed[111],2,V(8))
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun147
	call_c   Dyam_Remove_Choice()
	pl_call  fun21(&seed[108],2,V(8))
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun136
	call_c   Dyam_Remove_Choice()
	pl_call  fun21(&seed[102],2,V(2))
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun87
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(15),V(9))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun88
	call_c   Dyam_Update_Choice(fun87)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(9),&ref[304])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun89
	call_c   Dyam_Update_Choice(fun88)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(9),&ref[303])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(15),&ref[186])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun91
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(14),V(8))
	fail_ret
	pl_jump  fun90()

pl_code local fun92
	call_c   Dyam_Update_Choice(fun91)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(8),&ref[306])
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun90()

pl_code local fun93
	call_c   Dyam_Update_Choice(fun92)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(8),&ref[305])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(14),V(12))
	fail_ret
	pl_jump  fun90()

pl_code local fun95
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(12),V(6))
	fail_ret
	pl_jump  fun94()

pl_code local fun96
	call_c   Dyam_Update_Choice(fun95)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(6),&ref[308])
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun94()

pl_code local fun97
	call_c   Dyam_Update_Choice(fun96)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(6),&ref[307])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(12),V(11))
	fail_ret
	pl_jump  fun94()

pl_code local fun99
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(13),V(7))
	fail_ret
	pl_jump  fun98()

pl_code local fun100
	call_c   Dyam_Update_Choice(fun99)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(7),&ref[310])
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun98()

pl_code local fun101
	call_c   Dyam_Update_Choice(fun100)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(7),&ref[309])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(13),V(11))
	fail_ret
	pl_jump  fun98()

pl_code local fun103
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(11),V(5))
	fail_ret
	pl_jump  fun102()

pl_code local fun104
	call_c   Dyam_Update_Choice(fun103)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(5),&ref[312])
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun102()

pl_code local fun105
	call_c   Dyam_Update_Choice(fun104)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(5),&ref[311])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(11),I(0))
	fail_ret
	pl_jump  fun102()

pl_code local fun107
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(10),V(2))
	fail_ret
	pl_jump  fun106()

pl_code local fun108
	call_c   Dyam_Update_Choice(fun107)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(2),&ref[314])
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun106()

pl_code local fun109
	call_c   Dyam_Update_Choice(fun108)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(2),&ref[313])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(10),I(0))
	fail_ret
	pl_jump  fun106()

pl_code local fun76
	call_c   Dyam_Remove_Choice()
	pl_call  fun21(&seed[89],2,V(6))
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun52
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(1),V(2))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun53
	call_c   Dyam_Update_Choice(fun52)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[187])
	fail_ret
	call_c   Dyam_Unify(V(2),&ref[186])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun54
	call_c   Dyam_Update_Choice(fun53)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[185])
	fail_ret
	call_c   Dyam_Unify(V(2),&ref[186])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun55
	call_c   Dyam_Update_Choice(fun54)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[184])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun46
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(20),&ref[182])
	fail_ret
	call_c   Dyam_Unify(V(16),V(21))
	fail_ret
	call_c   Dyam_Unify(V(17),V(22))
	fail_ret
	call_c   Dyam_Unify(V(23),I(0))
	fail_ret
fun45:
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(12))
	call_c   Dyam_Reg_Load(4,V(14))
	call_c   Dyam_Reg_Load(6,V(21))
	call_c   Dyam_Reg_Load(8,V(15))
	call_c   Dyam_Reg_Load(10,V(22))
	call_c   Dyam_Reg_Load(12,V(20))
	move     V(25), R(14)
	move     S(5), R(15)
	call_c   Dyam_Reg_Load(16,V(10))
	pl_call  pred_dcg_body_to_lpda_9()
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(25))
	call_c   Dyam_Reg_Load(4,V(14))
	call_c   Dyam_Reg_Load(6,V(15))
	call_c   Dyam_Reg_Load(8,V(5))
	call_c   Dyam_Reg_Load(10,V(3))
	call_c   Dyam_Reg_Load(12,V(4))
	call_c   Dyam_Reg_Load(14,V(8))
	move     &ref[180], R(16)
	move     S(5), R(17)
	call_c   Dyam_Reg_Load(18,V(9))
	call_c   Dyam_Reg_Load(20,V(10))
	call_c   Dyam_Reg_Load(22,V(11))
	call_c   Dyam_Reg_Deallocate(12)
	pl_jump  pred_dcg_il_register_to_lpda_12()


pl_code local fun47
	call_c   Dyam_Update_Choice(fun46)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(7),&ref[181])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(20),V(24))
	fail_ret
	pl_jump  fun45()

pl_code local fun48
	call_c   Dyam_Remove_Choice()
	pl_call  fun7(&seed[75],1)
	pl_call  fun7(&seed[76],1)
	move     &ref[178], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(18), R(4)
	move     S(5), R(5)
	pl_call  pred_my_numbervars_3()
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(16))
	call_c   Dyam_Reg_Load(4,V(17))
	call_c   Dyam_Reg_Load(6,V(6))
	move     V(19), R(8)
	move     S(5), R(9)
	call_c   Dyam_Reg_Load(10,V(10))
	pl_call  pred_dcg_il_loop_to_lpda_6()
	call_c   Dyam_Choice(fun47)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(7),&ref[179])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(20),V(19))
	fail_ret
	call_c   Dyam_Unify(V(16),V(21))
	fail_ret
	call_c   Dyam_Unify(V(17),V(22))
	fail_ret
	call_c   Dyam_Unify(V(23),I(0))
	fail_ret
	pl_jump  fun45()

pl_code local fun50
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(2),V(12))
	fail_ret
	pl_jump  fun49()

pl_code local fun33
	call_c   Dyam_Remove_Choice()
	pl_call  fun7(&seed[66],1)
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun35
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(2),V(10))
	fail_ret
	pl_jump  fun34()

pl_code local fun12
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Object(R(0))
	call_c   Dyam_Tabule()
	pl_jump  Schedule_Prolog()

pl_code local fun13
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Choice(fun12)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Subsume(R(0))
	fail_ret
	call_c   Dyam_Cut()
	pl_ret

pl_code local fun21
	call_c   Dyam_Backptr_Trace(R(1),R(2))
	call_c   Dyam_Light_Object(R(0))
	pl_jump  Schedule_Prolog()

pl_code local fun22
	call_c   Dyam_Remove_Choice()
	pl_call  fun21(&seed[58],2,V(3))
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun17
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(7),&ref[81])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun19
	call_c   Dyam_Remove_Choice()
	pl_call  fun7(&seed[60],1)
	pl_jump  fun18()

pl_code local fun9
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(5),&ref[59])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret


;;------------------ INIT POOL ------------

c_code local build_init_pool
	call_c   build_seed_25()
	call_c   build_seed_18()
	call_c   build_seed_1()
	call_c   build_seed_6()
	call_c   build_seed_24()
	call_c   build_seed_33()
	call_c   build_seed_0()
	call_c   build_seed_52()
	call_c   build_seed_37()
	call_c   build_seed_39()
	call_c   build_seed_40()
	call_c   build_seed_50()
	call_c   build_seed_41()
	call_c   build_seed_29()
	call_c   build_seed_30()
	call_c   build_seed_47()
	call_c   build_seed_43()
	call_c   build_seed_31()
	call_c   build_seed_32()
	call_c   build_seed_22()
	call_c   build_seed_36()
	call_c   build_seed_27()
	call_c   build_seed_20()
	call_c   build_seed_34()
	call_c   build_seed_35()
	call_c   build_seed_42()
	call_c   build_seed_44()
	call_c   build_seed_3()
	call_c   build_seed_38()
	call_c   build_seed_5()
	call_c   build_seed_14()
	call_c   build_seed_2()
	call_c   build_seed_15()
	call_c   build_seed_49()
	call_c   build_seed_16()
	call_c   build_seed_45()
	call_c   build_seed_46()
	call_c   build_seed_48()
	call_c   build_seed_4()
	call_c   build_seed_53()
	call_c   build_seed_8()
	call_c   build_seed_23()
	call_c   build_seed_10()
	call_c   build_seed_7()
	call_c   build_seed_17()
	call_c   build_seed_13()
	call_c   build_seed_21()
	call_c   build_seed_12()
	call_c   build_seed_26()
	call_c   build_seed_9()
	call_c   build_seed_28()
	call_c   build_seed_51()
	call_c   build_seed_11()
	call_c   build_seed_19()
	call_c   build_seed_55()
	call_c   build_seed_61()
	call_c   build_seed_106()
	call_c   build_seed_155()
	call_c   build_seed_156()
	call_c   build_seed_140()
	call_c   build_seed_141()
	call_c   build_seed_114()
	call_c   build_seed_115()
	call_c   build_seed_112()
	call_c   build_seed_113()
	call_c   build_seed_110()
	call_c   build_seed_111()
	call_c   build_seed_107()
	call_c   build_seed_108()
	call_c   build_seed_101()
	call_c   build_seed_102()
	call_c   build_seed_88()
	call_c   build_seed_89()
	call_c   build_seed_69()
	call_c   build_seed_70()
	call_c   build_seed_76()
	call_c   build_seed_75()
	call_c   build_seed_64()
	call_c   build_seed_65()
	call_c   build_seed_66()
	call_c   build_seed_57()
	call_c   build_seed_58()
	call_c   build_seed_60()
	call_c   build_ref_4()
	call_c   build_ref_3()
	call_c   build_ref_8()
	call_c   build_ref_7()
	call_c   build_ref_12()
	call_c   build_ref_11()
	call_c   build_ref_13()
	call_c   build_ref_15()
	call_c   build_ref_19()
	call_c   build_ref_18()
	call_c   build_ref_20()
	call_c   build_ref_21()
	call_c   build_ref_22()
	call_c   build_ref_23()
	call_c   build_ref_27()
	call_c   build_ref_26()
	call_c   build_ref_28()
	call_c   build_ref_29()
	call_c   build_ref_30()
	call_c   build_ref_31()
	call_c   build_ref_35()
	call_c   build_ref_34()
	call_c   build_ref_664()
	call_c   build_ref_663()
	call_c   build_ref_665()
	call_c   build_ref_666()
	call_c   build_ref_668()
	call_c   build_ref_667()
	call_c   build_ref_669()
	call_c   build_ref_670()
	call_c   build_ref_462()
	call_c   build_ref_444()
	call_c   build_ref_416()
	call_c   build_ref_313()
	call_c   build_ref_314()
	call_c   build_ref_311()
	call_c   build_ref_312()
	call_c   build_ref_309()
	call_c   build_ref_310()
	call_c   build_ref_307()
	call_c   build_ref_308()
	call_c   build_ref_305()
	call_c   build_ref_306()
	call_c   build_ref_303()
	call_c   build_ref_304()
	call_c   build_ref_316()
	call_c   build_ref_315()
	call_c   build_ref_300()
	call_c   build_ref_302()
	call_c   build_ref_292()
	call_c   build_ref_294()
	call_c   build_ref_285()
	call_c   build_ref_271()
	call_c   build_ref_286()
	call_c   build_ref_183()
	call_c   build_ref_184()
	call_c   build_ref_185()
	call_c   build_ref_186()
	call_c   build_ref_187()
	call_c   build_ref_179()
	call_c   build_ref_181()
	call_c   build_ref_180()
	call_c   build_ref_182()
	call_c   build_ref_178()
	call_c   build_ref_81()
	call_c   build_ref_59()
	call_c   build_ref_49()
	call_c   build_ref_48()
	call_c   build_ref_47()
	c_ret

long local ref[989]
long local seed[166]

long local _initialization

c_code global initialization_dyalog_dcg
	call_c   Dyam_Check_And_Update_Initialization(_initialization)
	fail_c_ret
	call_c   initialization_dyalog_format()
	call_c   build_init_pool()
	call_c   build_viewers()
	call_c   load()
	c_ret

