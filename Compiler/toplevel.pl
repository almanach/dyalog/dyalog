/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2002, 2003, 2004, 2008, 2010, 2011 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  toplevel.pl -- Toplevel within DyALog compiler
 *
 * ----------------------------------------------------------------
 * Description
 * 
 * ----------------------------------------------------------------
 */

:-include 'header.pl'.

%% Trigger the insertion of a top level in DyALog compiler
'$dyalog_toplevel'.

get_clause(Clause) :-
	recorded( Clause::'$dyalog_toplevel' ).

pgm_to_lpda( '$dyalog_toplevel', Trans ) :-
	(   foreign(F/N,_) ; builtin(F/N,_) ; registered_toplevel(F/N)), 
	functor(T,F,N),
%%	format('Toplevel: register ~w\n',[T]),
	pgm_to_lpda((toplevel(T) :- T),Trans)
		.
		
toplevel((A,B)) :-
	toplevel(A),
	toplevel(B).

toplevel((A;B)) :-
	\+ A = (_ -> _),
	( toplevel(A)
	;
	  toplevel(B)
	).

toplevel((A->B)) :-
	toplevel((A->B;fail)).
	
toplevel((A->B;C)) :-
	toplevel(A) -> toplevel(B) ; toplevel(C).

toplevel(X=X).

toplevel(true).

toplevel((A xor B)) :-
	( toplevel(A) -> true ; toplevel(B)).

toplevel(\+ A) :-
	( toplevel(A) -> fail ; true )
	.

toplevel(A) :-
	\+ A=(_ xor _),
	\+ A=(\+ _),
%	\+ A=(_ -> _),
	functor(A,F,N),
	compiler_extension(Pred::F/N,M_L),
	mutable_read(M_L,L),
	(   domain((A:-Body),L) ->
	    toplevel(Body)
	;
	    error('No compiler expansion for ~w',[A])
	)
	.

:-extensional toplevel_clause/1.

toplevel(A) :-
	(   toplevel_clause((A)) ;
	    toplevel_clause((A :- Body)),toplevel(Body)
	)
	.

:-extensional registered_toplevel/1.
:-extensional usage_info/2.

registered_toplevel(format/3).
registered_toplevel(format/2).
registered_toplevel(toplevel_loop/0).
registered_toplevel(usage_info/2).

:-std_prolog toplevel_read_eval/2.

:-rec_prolog read_analyze/5.

toplevel_read_eval(T,V) :-
	( T==quit ->
	    format('Leaving toplevel. Good bye!\n',[])
	;   T = (?- G) ->
	    toplevel( G ),
	    ( V = [] ->
		format('\ttrue\n',[])
	    ;	domain(X,V),
		format('\t~w\n',[X])
	    ),
	    fail
	;   
	    read_analyze(T,toplevel,_,_,_),
	    fail
	).

toplevel_loop :-
	format('Entering DyALog toplevel\n',[]),
	repeat( ( read_term([],T,V),
		    toplevel_read_eval(T,V),
		    T == quit
		))
	.



