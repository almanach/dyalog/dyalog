;; Compiler: DyALog 1.14.0
;; File "code.pl"


;;------------------ LOADING ------------

c_code local load
	call_c   Dyam_Loading_Set()
	pl_call  fun3(&seed[3],0)
	pl_call  fun3(&seed[2],0)
	pl_call  fun3(&seed[1],0)
	pl_call  fun3(&seed[0],0)
	call_c   Dyam_Loading_Reset()
	c_ret

pl_code global pred_label_code_2
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(2)
	call_c   Dyam_Reg_Load(0,V(1))
	move     V(3), R(2)
	move     S(5), R(3)
	move     V(4), R(4)
	move     S(5), R(5)
	pl_call  pred_code_optimize_3()
	call_c   Dyam_Choice(fun17)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(3),&ref[15])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(5))
	pl_call  pred_jump_decr_1()
	call_c   Dyam_Unify(V(2),V(3))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code global pred_choice_code_3
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(2)
	call_c   Dyam_Reg_Unify(4,&ref[20])
	fail_ret
	move     &ref[17], R(0)
	move     S(5), R(1)
	move     V(4), R(2)
	move     S(5), R(3)
	move     V(5), R(4)
	move     S(5), R(5)
	pl_call  pred_code_optimize_3()
	call_c   Dyam_Choice(fun14)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(4),&ref[18])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(3))
	call_c   Dyam_Reg_Deallocate(1)
	pl_jump  pred_jump_decr_1()

pl_code global pred_jump_code_2
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(2)
	call_c   Dyam_Reg_Load(0,V(1))
	move     V(3), R(2)
	move     S(5), R(3)
	move     V(4), R(4)
	move     S(5), R(5)
	pl_call  pred_code_optimize_3()
	call_c   Dyam_Choice(fun12)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[14])
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(2),V(3))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code global pred_jump_decr_1
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Choice(fun4)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[12])
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load_Ptr(2,V(2))
	fail_ret
	move     V(3), R(4)
	move     S(5), R(5)
	call_c   DyALog_Mutable_Read(R(2),&R(4))
	fail_ret
	call_c   DYAM_evpred_is(V(4),&ref[13])
	fail_ret
	call_c   Dyam_Reg_Load_Ptr(2,V(2))
	fail_ret
	call_c   Dyam_Reg_Load(4,V(4))
	call_c   DyALog_Mutable_Write(R(2),&R(4),0)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(2))
	fail_ret
	call_c   Dyam_Reg_Deallocate(4)
	pl_ret

pl_code global pred_label_code2_3
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(3)
	call_c   Dyam_Choice(fun9)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_variable(V(3))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(3),&ref[7])
	fail_ret
fun8:
	call_c   Dyam_Choice(fun7)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_lt(V(2),N(20))
	fail_ret
	pl_call  Object_1(&ref[8])
	call_c   Dyam_Cut()
	call_c   Dyam_Deallocate()
	pl_ret


pl_code global pred_jump_inc_1
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Choice(fun4)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[6])
	fail_ret
	call_c   Dyam_Cut()
	move     &ref[5], R(0)
	move     S(5), R(1)
	move     V(4), R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_update_counter_2()

pl_code global pred_jump_value_2
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(2)
	move     &ref[5], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(2))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_value_counter_2()


;;------------------ VIEWERS ------------

c_code local build_viewers
	c_ret

c_code local build_seed_0
	ret_reg &seed[0]
	call_c   build_ref_0()
	call_c   build_ref_1()
	call_c   Dyam_Seed_Start(&ref[0],&ref[1],&ref[1],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[0]
	c_ret

pl_code local fun0
	pl_ret

;; TERM 1: redirect_jump(succeed)
c_code local build_ref_1
	ret_reg &ref[1]
	call_c   build_ref_22()
	call_c   build_ref_23()
	call_c   Dyam_Create_Unary(&ref[22],&ref[23])
	move_ret ref[1]
	c_ret

;; TERM 23: succeed
c_code local build_ref_23
	ret_reg &ref[23]
	call_c   Dyam_Create_Atom("succeed")
	move_ret ref[23]
	c_ret

;; TERM 22: redirect_jump
c_code local build_ref_22
	ret_reg &ref[22]
	call_c   Dyam_Create_Atom("redirect_jump")
	move_ret ref[22]
	c_ret

;; TERM 0: '*DATABASE*'
c_code local build_ref_0
	ret_reg &ref[0]
	call_c   Dyam_Create_Atom("*DATABASE*")
	move_ret ref[0]
	c_ret

c_code local build_seed_1
	ret_reg &seed[1]
	call_c   build_ref_0()
	call_c   build_ref_2()
	call_c   Dyam_Seed_Start(&ref[0],&ref[2],&ref[2],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[1]
	c_ret

;; TERM 2: redirect_jump(fail)
c_code local build_ref_2
	ret_reg &ref[2]
	call_c   build_ref_22()
	call_c   build_ref_24()
	call_c   Dyam_Create_Unary(&ref[22],&ref[24])
	move_ret ref[2]
	c_ret

;; TERM 24: fail
c_code local build_ref_24
	ret_reg &ref[24]
	call_c   Dyam_Create_Atom("fail")
	move_ret ref[24]
	c_ret

c_code local build_seed_2
	ret_reg &seed[2]
	call_c   build_ref_0()
	call_c   build_ref_3()
	call_c   Dyam_Seed_Start(&ref[0],&ref[3],&ref[3],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[2]
	c_ret

;; TERM 3: redirect_jump(noop)
c_code local build_ref_3
	ret_reg &ref[3]
	call_c   build_ref_22()
	call_c   build_ref_21()
	call_c   Dyam_Create_Unary(&ref[22],&ref[21])
	move_ret ref[3]
	c_ret

;; TERM 21: noop
c_code local build_ref_21
	ret_reg &ref[21]
	call_c   Dyam_Create_Atom("noop")
	move_ret ref[21]
	c_ret

c_code local build_seed_3
	ret_reg &seed[3]
	call_c   build_ref_0()
	call_c   build_ref_4()
	call_c   Dyam_Seed_Start(&ref[0],&ref[4],&ref[4],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[3]
	c_ret

;; TERM 4: redirect_jump(pl_jump(_B, _C))
c_code local build_ref_4
	ret_reg &ref[4]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_25()
	call_c   Dyam_Create_Binary(&ref[25],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_22()
	call_c   Dyam_Create_Unary(&ref[22],R(0))
	move_ret ref[4]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 25: pl_jump
c_code local build_ref_25
	ret_reg &ref[25]
	call_c   Dyam_Create_Atom("pl_jump")
	move_ret ref[25]
	c_ret

;; TERM 18: pl_jump(_D, [])
c_code local build_ref_18
	ret_reg &ref[18]
	call_c   build_ref_25()
	call_c   Dyam_Create_Binary(&ref[25],V(3),I(0))
	move_ret ref[18]
	c_ret

;; TERM 17: remove_choice :> _B
c_code local build_ref_17
	ret_reg &ref[17]
	call_c   build_ref_16()
	call_c   Dyam_Create_Binary(I(9),&ref[16],V(1))
	move_ret ref[17]
	c_ret

;; TERM 16: remove_choice
c_code local build_ref_16
	ret_reg &ref[16]
	call_c   Dyam_Create_Atom("remove_choice")
	move_ret ref[16]
	c_ret

;; TERM 20: choice(_D) :> _C
c_code local build_ref_20
	ret_reg &ref[20]
	call_c   build_ref_19()
	call_c   Dyam_Create_Binary(I(9),&ref[19],V(2))
	move_ret ref[20]
	c_ret

;; TERM 19: choice(_D)
c_code local build_ref_19
	ret_reg &ref[19]
	call_c   build_ref_26()
	call_c   Dyam_Create_Unary(&ref[26],V(3))
	move_ret ref[19]
	c_ret

;; TERM 26: choice
c_code local build_ref_26
	ret_reg &ref[26]
	call_c   Dyam_Create_Atom("choice")
	move_ret ref[26]
	c_ret

;; TERM 14: redirect_jump(_D)
c_code local build_ref_14
	ret_reg &ref[14]
	call_c   build_ref_22()
	call_c   Dyam_Create_Unary(&ref[22],V(3))
	move_ret ref[14]
	c_ret

;; TERM 15: pl_jump(_F, [])
c_code local build_ref_15
	ret_reg &ref[15]
	call_c   build_ref_25()
	call_c   Dyam_Create_Binary(&ref[25],V(5),I(0))
	move_ret ref[15]
	c_ret

;; TERM 13: _D - 1
c_code local build_ref_13
	ret_reg &ref[13]
	call_c   build_ref_27()
	call_c   Dyam_Create_Binary(&ref[27],V(3),N(1))
	move_ret ref[13]
	c_ret

;; TERM 27: -
c_code local build_ref_27
	ret_reg &ref[27]
	call_c   Dyam_Create_Atom("-")
	move_ret ref[27]
	c_ret

;; TERM 12: counter(jump(_B), _C)
c_code local build_ref_12
	ret_reg &ref[12]
	call_c   build_ref_28()
	call_c   build_ref_5()
	call_c   Dyam_Create_Binary(&ref[28],&ref[5],V(2))
	move_ret ref[12]
	c_ret

;; TERM 5: jump(_B)
c_code local build_ref_5
	ret_reg &ref[5]
	call_c   build_ref_29()
	call_c   Dyam_Create_Unary(&ref[29],V(1))
	move_ret ref[5]
	c_ret

;; TERM 29: jump
c_code local build_ref_29
	ret_reg &ref[29]
	call_c   Dyam_Create_Atom("jump")
	move_ret ref[29]
	c_ret

;; TERM 28: counter
c_code local build_ref_28
	ret_reg &ref[28]
	call_c   Dyam_Create_Atom("counter")
	move_ret ref[28]
	c_ret

;; TERM 7: '$fun'(_E, 0, _F)
c_code local build_ref_7
	ret_reg &ref[7]
	call_c   build_ref_30()
	call_c   Dyam_Term_Start(&ref[30],3)
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(N(0))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[7]
	c_ret

;; TERM 30: '$fun'
c_code local build_ref_30
	ret_reg &ref[30]
	call_c   Dyam_Create_Atom("$fun")
	move_ret ref[30]
	c_ret

;; TERM 8: code(_C, _B, _D)
c_code local build_ref_8
	ret_reg &ref[8]
	call_c   build_ref_9()
	call_c   Dyam_Term_Start(&ref[9],3)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[8]
	c_ret

;; TERM 9: code
c_code local build_ref_9
	ret_reg &ref[9]
	call_c   Dyam_Create_Atom("code")
	move_ret ref[9]
	c_ret

;; TERM 10: '$fun_info'{code=> _B, refs=> _H, ins=> _I}
c_code local build_ref_10
	ret_reg &ref[10]
	call_c   build_ref_31()
	call_c   Dyam_Term_Start(&ref[31],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_End()
	move_ret ref[10]
	c_ret

;; TERM 31: '$fun_info'!'$ft'
c_code local build_ref_31
	ret_reg &ref[31]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_34()
	call_c   Dyam_Create_List(&ref[34],I(0))
	move_ret R(0)
	call_c   build_ref_33()
	call_c   Dyam_Create_List(&ref[33],R(0))
	move_ret R(0)
	call_c   build_ref_9()
	call_c   Dyam_Create_List(&ref[9],R(0))
	move_ret R(0)
	call_c   build_ref_32()
	call_c   Dyam_Create_Atom_Module("$ft",&ref[32])
	move_ret ref[31]
	call_c   DYAM_Feature_2(&ref[32],R(0))
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 32: '$fun_info'
c_code local build_ref_32
	ret_reg &ref[32]
	call_c   Dyam_Create_Atom("$fun_info")
	move_ret ref[32]
	c_ret

;; TERM 33: refs
c_code local build_ref_33
	ret_reg &ref[33]
	call_c   Dyam_Create_Atom("refs")
	move_ret ref[33]
	c_ret

;; TERM 34: ins
c_code local build_ref_34
	ret_reg &ref[34]
	call_c   Dyam_Create_Atom("ins")
	move_ret ref[34]
	c_ret

;; TERM 11: '$fun'(_E, _G, _F)
c_code local build_ref_11
	ret_reg &ref[11]
	call_c   build_ref_30()
	call_c   Dyam_Term_Start(&ref[30],3)
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[11]
	c_ret

;; TERM 6: '$fun'(_C, 0, _D)
c_code local build_ref_6
	ret_reg &ref[6]
	call_c   build_ref_30()
	call_c   Dyam_Term_Start(&ref[30],3)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(N(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[6]
	c_ret

pl_code local fun2
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Object(R(0))
	call_c   Dyam_Tabule()
	pl_ret

pl_code local fun3
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Choice(fun2)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Subsume(R(0))
	fail_ret
	call_c   Dyam_Cut()
	pl_ret

pl_code local fun16
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Reg_Load(0,V(3))
	call_c   Dyam_Reg_Load(2,V(4))
	call_c   Dyam_Reg_Load(4,V(2))
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  pred_label_code2_3()

pl_code local fun17
	call_c   Dyam_Update_Choice(fun16)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(3),&ref[21])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(2),&ref[21])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun14
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Reg_Load(0,V(4))
	call_c   Dyam_Reg_Load(2,V(5))
	call_c   Dyam_Reg_Load(4,V(3))
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  pred_label_code2_3()

pl_code local fun12
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Reg_Load(0,V(3))
	call_c   Dyam_Reg_Load(2,V(4))
	move     V(5), R(4)
	move     S(5), R(5)
	pl_call  pred_label_code2_3()
	call_c   Dyam_Unify(V(2),&ref[15])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun6
	call_c   Dyam_Remove_Choice()
	call_c   DYAM_evpred_assert_1(&ref[8])
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun7
	call_c   Dyam_Remove_Choice()
	move     &ref[9], R(0)
	move     0, R(1)
	call_c   Dyam_Reg_Load(2,V(4))
	pl_call  pred_update_counter_2()
	call_c   Dyam_Reg_Load(0,V(1))
	move     V(7), R(2)
	move     S(5), R(3)
	move     V(8), R(4)
	move     S(5), R(5)
	pl_call  pred_code_refs_3()
	call_c   Dyam_Reg_Load_Ptr(2,V(5))
	fail_ret
	call_c   Dyam_Reg_Load(4,&ref[10])
	call_c   DyALog_Mutable_Write(R(2),&R(4),0)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(5))
	fail_ret
	call_c   Dyam_Choice(fun6)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_ge(V(2),N(20))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun9
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(3),&ref[11])
	fail_ret
	pl_jump  fun8()

pl_code local fun4
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Deallocate()
	pl_ret


;;------------------ INIT POOL ------------

c_code local build_init_pool
	call_c   build_seed_0()
	call_c   build_seed_1()
	call_c   build_seed_2()
	call_c   build_seed_3()
	call_c   build_ref_21()
	call_c   build_ref_18()
	call_c   build_ref_17()
	call_c   build_ref_20()
	call_c   build_ref_14()
	call_c   build_ref_15()
	call_c   build_ref_13()
	call_c   build_ref_12()
	call_c   build_ref_7()
	call_c   build_ref_8()
	call_c   build_ref_10()
	call_c   build_ref_9()
	call_c   build_ref_11()
	call_c   build_ref_6()
	call_c   build_ref_5()
	c_ret

long local ref[35]
long local seed[4]

long local _initialization

c_code global initialization_dyalog_code
	call_c   Dyam_Check_And_Update_Initialization(_initialization)
	fail_c_ret
	call_c   initialization_dyalog_format()
	call_c   build_init_pool()
	call_c   build_viewers()
	call_c   load()
	c_ret

