;; Compiler: DyALog 1.14.0
;; File "rcg.pl"


;;------------------ LOADING ------------

c_code local load
	call_c   Dyam_Loading_Set()
	pl_call  fun2(&seed[10],0)
	pl_call  fun2(&seed[9],0)
	pl_call  fun2(&seed[11],0)
	pl_call  fun2(&seed[8],0)
	pl_call  fun2(&seed[7],0)
	pl_call  fun2(&seed[5],0)
	pl_call  fun2(&seed[1],0)
	pl_call  fun2(&seed[6],0)
	pl_call  fun2(&seed[4],0)
	pl_call  fun2(&seed[2],0)
	pl_call  fun2(&seed[3],0)
	pl_call  fun2(&seed[0],0)
	call_c   Dyam_Loading_Reset()
	c_ret

pl_code global pred_rcg_str_expand_6
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(6)
	call_c   Dyam_Choice(fun68)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(2),&ref[198])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(5),V(6))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code global pred_rcg_functor_4
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Reg_Unify(2,&ref[114])
	fail_ret
	call_c   Dyam_Multi_Reg_Bind(4,5,2)
	call_c   Dyam_Choice(fun40)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_variable(V(1))
	fail_ret
	call_c   Dyam_Cut()
	call_c   DYAM_evpred_atom(V(2))
	fail_ret
	call_c   DYAM_evpred_number(V(3))
	fail_ret
	call_c   DYAM_evpred_number(V(4))
	fail_ret
	call_c   DYAM_evpred_functor(V(5),V(2),V(3))
	fail_ret
	call_c   DYAM_evpred_length(V(6),V(4))
	fail_ret
	call_c   DYAM_evpred_univ(V(1),&ref[113])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code global pred_rcg_terminals_expand_6
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(6)
	call_c   Dyam_Unify(V(1),&ref[61])
	fail_ret
	call_c   Dyam_Unify(V(5),&ref[62])
	fail_ret
	call_c   Dyam_Choice(fun20)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(2),&ref[63])
	fail_ret
	call_c   Dyam_Cut()
	pl_call  Object_1(&ref[64])
fun19:
	call_c   Dyam_Choice(fun18)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(8),I(0))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(10),V(6))
	fail_ret
	call_c   Dyam_Unify(V(11),V(4))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret



;;------------------ VIEWERS ------------

c_code local build_viewers
	call_c   Dyam_Load_Viewer(&ref[16],&ref[15])
	call_c   Dyam_Load_Viewer(&ref[13],&ref[14])
	call_c   Dyam_Load_Viewer(&ref[11],&ref[9])
	call_c   Dyam_Load_Viewer(&ref[7],&ref[8])
	call_c   Dyam_Load_Viewer(&ref[3],&ref[4])
	c_ret

c_code local build_seed_0
	ret_reg &seed[0]
	call_c   build_ref_17()
	call_c   build_ref_21()
	call_c   Dyam_Seed_Start(&ref[17],&ref[21],I(0),fun1,1)
	call_c   build_ref_20()
	call_c   Dyam_Seed_Add_Comp(&ref[20],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[0]
	c_ret

;; TERM 20: '*GUARD*'(rcg_args_expand(_B, [], [], _C, _C))
c_code local build_ref_20
	ret_reg &ref[20]
	call_c   build_ref_18()
	call_c   build_ref_19()
	call_c   Dyam_Create_Unary(&ref[18],&ref[19])
	move_ret ref[20]
	c_ret

;; TERM 19: rcg_args_expand(_B, [], [], _C, _C)
c_code local build_ref_19
	ret_reg &ref[19]
	call_c   build_ref_302()
	call_c   Dyam_Term_Start(&ref[302],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_End()
	move_ret ref[19]
	c_ret

;; TERM 302: rcg_args_expand
c_code local build_ref_302
	ret_reg &ref[302]
	call_c   Dyam_Create_Atom("rcg_args_expand")
	move_ret ref[302]
	c_ret

;; TERM 18: '*GUARD*'
c_code local build_ref_18
	ret_reg &ref[18]
	call_c   Dyam_Create_Atom("*GUARD*")
	move_ret ref[18]
	c_ret

pl_code local fun0
	call_c   build_ref_20()
	call_c   Dyam_Unify_Item(&ref[20])
	fail_ret
	pl_ret

pl_code local fun1
	pl_ret

;; TERM 21: '*GUARD*'(rcg_args_expand(_B, [], [], _C, _C)) :> []
c_code local build_ref_21
	ret_reg &ref[21]
	call_c   build_ref_20()
	call_c   Dyam_Create_Binary(I(9),&ref[20],I(0))
	move_ret ref[21]
	c_ret

;; TERM 17: '*GUARD_CLAUSE*'
c_code local build_ref_17
	ret_reg &ref[17]
	call_c   Dyam_Create_Atom("*GUARD_CLAUSE*")
	move_ret ref[17]
	c_ret

c_code local build_seed_3
	ret_reg &seed[3]
	call_c   build_ref_17()
	call_c   build_ref_24()
	call_c   Dyam_Seed_Start(&ref[17],&ref[24],I(0),fun1,1)
	call_c   build_ref_23()
	call_c   Dyam_Seed_Add_Comp(&ref[23],fun5,0)
	call_c   Dyam_Seed_End()
	move_ret seed[3]
	c_ret

;; TERM 23: '*GUARD*'(rcg_prolog_make(rcg((_B / _C), _D), _E, _F, _G))
c_code local build_ref_23
	ret_reg &ref[23]
	call_c   build_ref_18()
	call_c   build_ref_22()
	call_c   Dyam_Create_Unary(&ref[18],&ref[22])
	move_ret ref[23]
	c_ret

;; TERM 22: rcg_prolog_make(rcg((_B / _C), _D), _E, _F, _G)
c_code local build_ref_22
	ret_reg &ref[22]
	call_c   build_ref_303()
	call_c   build_ref_51()
	call_c   Dyam_Term_Start(&ref[303],4)
	call_c   Dyam_Term_Arg(&ref[51])
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[22]
	c_ret

;; TERM 51: rcg((_B / _C), _D)
c_code local build_ref_51
	ret_reg &ref[51]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_305()
	call_c   Dyam_Create_Binary(&ref[305],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_304()
	call_c   Dyam_Create_Binary(&ref[304],R(0),V(3))
	move_ret ref[51]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 304: rcg
c_code local build_ref_304
	ret_reg &ref[304]
	call_c   Dyam_Create_Atom("rcg")
	move_ret ref[304]
	c_ret

;; TERM 305: /
c_code local build_ref_305
	ret_reg &ref[305]
	call_c   Dyam_Create_Atom("/")
	move_ret ref[305]
	c_ret

;; TERM 303: rcg_prolog_make
c_code local build_ref_303
	ret_reg &ref[303]
	call_c   Dyam_Create_Atom("rcg_prolog_make")
	move_ret ref[303]
	c_ret

;; TERM 25: [_B|_H]
c_code local build_ref_25
	ret_reg &ref[25]
	call_c   Dyam_Create_List(V(1),V(7))
	move_ret ref[25]
	c_ret

c_code local build_seed_12
	ret_reg &seed[12]
	call_c   build_ref_18()
	call_c   build_ref_27()
	call_c   Dyam_Seed_Start(&ref[18],&ref[27],I(0),fun3,1)
	call_c   build_ref_28()
	call_c   Dyam_Seed_Add_Comp(&ref[28],&ref[27],0)
	call_c   Dyam_Seed_End()
	move_ret seed[12]
	c_ret

pl_code local fun3
	pl_jump  Complete(0,0)

;; TERM 28: '*GUARD*'(append(_H, _F, _I)) :> '$$HOLE$$'
c_code local build_ref_28
	ret_reg &ref[28]
	call_c   build_ref_27()
	call_c   Dyam_Create_Binary(I(9),&ref[27],I(7))
	move_ret ref[28]
	c_ret

;; TERM 27: '*GUARD*'(append(_H, _F, _I))
c_code local build_ref_27
	ret_reg &ref[27]
	call_c   build_ref_18()
	call_c   build_ref_26()
	call_c   Dyam_Create_Unary(&ref[18],&ref[26])
	move_ret ref[27]
	c_ret

;; TERM 26: append(_H, _F, _I)
c_code local build_ref_26
	ret_reg &ref[26]
	call_c   build_ref_306()
	call_c   Dyam_Term_Start(&ref[306],3)
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_End()
	move_ret ref[26]
	c_ret

;; TERM 306: append
c_code local build_ref_306
	ret_reg &ref[306]
	call_c   Dyam_Create_Atom("append")
	move_ret ref[306]
	c_ret

pl_code local fun4
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Light_Object(R(0))
	pl_jump  Schedule_Prolog()

;; TERM 29: [_B|_I]
c_code local build_ref_29
	ret_reg &ref[29]
	call_c   Dyam_Create_List(V(1),V(8))
	move_ret ref[29]
	c_ret

long local pool_fun5[5]=[4,build_ref_23,build_ref_25,build_seed_12,build_ref_29]

pl_code local fun5
	call_c   Dyam_Pool(pool_fun5)
	call_c   Dyam_Unify_Item(&ref[23])
	fail_ret
	call_c   DYAM_evpred_univ(V(4),&ref[25])
	fail_ret
	call_c   Dyam_Allocate(0)
	pl_call  fun4(&seed[12],1)
	call_c   DYAM_evpred_univ(V(6),&ref[29])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

;; TERM 24: '*GUARD*'(rcg_prolog_make(rcg((_B / _C), _D), _E, _F, _G)) :> []
c_code local build_ref_24
	ret_reg &ref[24]
	call_c   build_ref_23()
	call_c   Dyam_Create_Binary(I(9),&ref[23],I(0))
	move_ret ref[24]
	c_ret

c_code local build_seed_2
	ret_reg &seed[2]
	call_c   build_ref_30()
	call_c   build_ref_33()
	call_c   Dyam_Seed_Start(&ref[30],&ref[33],I(0),fun17,1)
	call_c   build_ref_35()
	call_c   Dyam_Seed_Add_Comp(&ref[35],fun16,0)
	call_c   Dyam_Seed_End()
	move_ret seed[2]
	c_ret

;; TERM 35: '*CITEM*'('call_rcg_make_callret/5'(rcg((_B / _C), _D)), _A)
c_code local build_ref_35
	ret_reg &ref[35]
	call_c   build_ref_34()
	call_c   build_ref_31()
	call_c   Dyam_Create_Binary(&ref[34],&ref[31],V(0))
	move_ret ref[35]
	c_ret

;; TERM 31: 'call_rcg_make_callret/5'(rcg((_B / _C), _D))
c_code local build_ref_31
	ret_reg &ref[31]
	call_c   build_ref_307()
	call_c   build_ref_51()
	call_c   Dyam_Create_Unary(&ref[307],&ref[51])
	move_ret ref[31]
	c_ret

;; TERM 307: 'call_rcg_make_callret/5'
c_code local build_ref_307
	ret_reg &ref[307]
	call_c   Dyam_Create_Atom("call_rcg_make_callret/5")
	move_ret ref[307]
	c_ret

;; TERM 34: '*CITEM*'
c_code local build_ref_34
	ret_reg &ref[34]
	call_c   Dyam_Create_Atom("*CITEM*")
	move_ret ref[34]
	c_ret

;; TERM 39: +
c_code local build_ref_39
	ret_reg &ref[39]
	call_c   Dyam_Create_Atom("+")
	move_ret ref[39]
	c_ret

;; TERM 38: _D * 2
c_code local build_ref_38
	ret_reg &ref[38]
	call_c   build_ref_37()
	call_c   Dyam_Create_Binary(&ref[37],V(3),N(2))
	move_ret ref[38]
	c_ret

;; TERM 37: *
c_code local build_ref_37
	ret_reg &ref[37]
	call_c   Dyam_Create_Atom("*")
	move_ret ref[37]
	c_ret

c_code local build_seed_13
	ret_reg &seed[13]
	call_c   build_ref_18()
	call_c   build_ref_42()
	call_c   Dyam_Seed_Start(&ref[18],&ref[42],I(0),fun3,1)
	call_c   build_ref_43()
	call_c   Dyam_Seed_Add_Comp(&ref[43],&ref[42],0)
	call_c   Dyam_Seed_End()
	move_ret seed[13]
	c_ret

;; TERM 43: '*GUARD*'(append(_L, _N, _O)) :> '$$HOLE$$'
c_code local build_ref_43
	ret_reg &ref[43]
	call_c   build_ref_42()
	call_c   Dyam_Create_Binary(I(9),&ref[42],I(7))
	move_ret ref[43]
	c_ret

;; TERM 42: '*GUARD*'(append(_L, _N, _O))
c_code local build_ref_42
	ret_reg &ref[42]
	call_c   build_ref_18()
	call_c   build_ref_41()
	call_c   Dyam_Create_Unary(&ref[18],&ref[41])
	move_ret ref[42]
	c_ret

;; TERM 41: append(_L, _N, _O)
c_code local build_ref_41
	ret_reg &ref[41]
	call_c   build_ref_306()
	call_c   Dyam_Term_Start(&ref[306],3)
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_End()
	move_ret ref[41]
	c_ret

;; TERM 44: [_M|_O]
c_code local build_ref_44
	ret_reg &ref[44]
	call_c   Dyam_Create_List(V(12),V(14))
	move_ret ref[44]
	c_ret

c_code local build_seed_14
	ret_reg &seed[14]
	call_c   build_ref_18()
	call_c   build_ref_46()
	call_c   Dyam_Seed_Start(&ref[18],&ref[46],I(0),fun3,1)
	call_c   build_ref_47()
	call_c   Dyam_Seed_Add_Comp(&ref[47],&ref[46],0)
	call_c   Dyam_Seed_End()
	move_ret seed[14]
	c_ret

;; TERM 47: '*GUARD*'(rcg_prolog_make(rcg((_B / _C), _D), _E, _F, _Q)) :> '$$HOLE$$'
c_code local build_ref_47
	ret_reg &ref[47]
	call_c   build_ref_46()
	call_c   Dyam_Create_Binary(I(9),&ref[46],I(7))
	move_ret ref[47]
	c_ret

;; TERM 46: '*GUARD*'(rcg_prolog_make(rcg((_B / _C), _D), _E, _F, _Q))
c_code local build_ref_46
	ret_reg &ref[46]
	call_c   build_ref_18()
	call_c   build_ref_45()
	call_c   Dyam_Create_Unary(&ref[18],&ref[45])
	move_ret ref[46]
	c_ret

;; TERM 45: rcg_prolog_make(rcg((_B / _C), _D), _E, _F, _Q)
c_code local build_ref_45
	ret_reg &ref[45]
	call_c   build_ref_303()
	call_c   build_ref_51()
	call_c   Dyam_Term_Start(&ref[303],4)
	call_c   Dyam_Term_Arg(&ref[51])
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_End()
	move_ret ref[45]
	c_ret

c_code local build_seed_15
	ret_reg &seed[15]
	call_c   build_ref_18()
	call_c   build_ref_49()
	call_c   Dyam_Seed_Start(&ref[18],&ref[49],I(0),fun3,1)
	call_c   build_ref_50()
	call_c   Dyam_Seed_Add_Comp(&ref[50],&ref[49],0)
	call_c   Dyam_Seed_End()
	move_ret seed[15]
	c_ret

;; TERM 50: '*GUARD*'(make_callret_with_pat(_P, _Q, _G, _H)) :> '$$HOLE$$'
c_code local build_ref_50
	ret_reg &ref[50]
	call_c   build_ref_49()
	call_c   Dyam_Create_Binary(I(9),&ref[49],I(7))
	move_ret ref[50]
	c_ret

;; TERM 49: '*GUARD*'(make_callret_with_pat(_P, _Q, _G, _H))
c_code local build_ref_49
	ret_reg &ref[49]
	call_c   build_ref_18()
	call_c   build_ref_48()
	call_c   Dyam_Create_Unary(&ref[18],&ref[48])
	move_ret ref[49]
	c_ret

;; TERM 48: make_callret_with_pat(_P, _Q, _G, _H)
c_code local build_ref_48
	ret_reg &ref[48]
	call_c   build_ref_308()
	call_c   Dyam_Term_Start(&ref[308],4)
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[48]
	c_ret

;; TERM 308: make_callret_with_pat
c_code local build_ref_308
	ret_reg &ref[308]
	call_c   Dyam_Create_Atom("make_callret_with_pat")
	move_ret ref[308]
	c_ret

c_code local build_seed_17
	ret_reg &seed[17]
	call_c   build_ref_0()
	call_c   build_ref_58()
	call_c   Dyam_Seed_Start(&ref[0],&ref[58],&ref[58],fun3,1)
	call_c   build_ref_59()
	call_c   Dyam_Seed_Add_Comp(&ref[59],&ref[58],0)
	call_c   Dyam_Seed_End()
	move_ret seed[17]
	c_ret

;; TERM 59: '*RITEM*'(_A, return(_E, _F, _G, _H)) :> '$$HOLE$$'
c_code local build_ref_59
	ret_reg &ref[59]
	call_c   build_ref_58()
	call_c   Dyam_Create_Binary(I(9),&ref[58],I(7))
	move_ret ref[59]
	c_ret

;; TERM 58: '*RITEM*'(_A, return(_E, _F, _G, _H))
c_code local build_ref_58
	ret_reg &ref[58]
	call_c   build_ref_0()
	call_c   build_ref_57()
	call_c   Dyam_Create_Binary(&ref[0],V(0),&ref[57])
	move_ret ref[58]
	c_ret

;; TERM 57: return(_E, _F, _G, _H)
c_code local build_ref_57
	ret_reg &ref[57]
	call_c   build_ref_309()
	call_c   Dyam_Term_Start(&ref[309],4)
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[57]
	c_ret

;; TERM 309: return
c_code local build_ref_309
	ret_reg &ref[309]
	call_c   Dyam_Create_Atom("return")
	move_ret ref[309]
	c_ret

;; TERM 0: '*RITEM*'
c_code local build_ref_0
	ret_reg &ref[0]
	call_c   Dyam_Create_Atom("*RITEM*")
	move_ret ref[0]
	c_ret

pl_code local fun6
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Object(R(0))
	call_c   Dyam_Tabule()
	pl_ret

pl_code local fun7
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Choice(fun6)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Subsume(R(0))
	fail_ret
	call_c   Dyam_Cut()
	pl_ret

long local pool_fun8[2]=[1,build_seed_17]

pl_code local fun9
	call_c   Dyam_Remove_Choice()
fun8:
	call_c   Dyam_Deallocate()
	pl_jump  fun7(&seed[17],2)


c_code local build_seed_16
	ret_reg &seed[16]
	call_c   build_ref_18()
	call_c   build_ref_53()
	call_c   Dyam_Seed_Start(&ref[18],&ref[53],I(0),fun3,1)
	call_c   build_ref_54()
	call_c   Dyam_Seed_Add_Comp(&ref[54],&ref[53],0)
	call_c   Dyam_Seed_End()
	move_ret seed[16]
	c_ret

;; TERM 54: '*GUARD*'(rcg_args_expand(_T, _S, _F, _U, true)) :> '$$HOLE$$'
c_code local build_ref_54
	ret_reg &ref[54]
	call_c   build_ref_53()
	call_c   Dyam_Create_Binary(I(9),&ref[53],I(7))
	move_ret ref[54]
	c_ret

;; TERM 53: '*GUARD*'(rcg_args_expand(_T, _S, _F, _U, true))
c_code local build_ref_53
	ret_reg &ref[53]
	call_c   build_ref_18()
	call_c   build_ref_52()
	call_c   Dyam_Create_Unary(&ref[18],&ref[52])
	move_ret ref[53]
	c_ret

;; TERM 52: rcg_args_expand(_T, _S, _F, _U, true)
c_code local build_ref_52
	ret_reg &ref[52]
	call_c   build_ref_302()
	call_c   build_ref_293()
	call_c   Dyam_Term_Start(&ref[302],5)
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_Arg(&ref[293])
	call_c   Dyam_Term_End()
	move_ret ref[52]
	c_ret

;; TERM 293: true
c_code local build_ref_293
	ret_reg &ref[293]
	call_c   Dyam_Create_Atom("true")
	move_ret ref[293]
	c_ret

;; TERM 55: '*RITEM*'(_G, _H)
c_code local build_ref_55
	ret_reg &ref[55]
	call_c   build_ref_0()
	call_c   Dyam_Create_Binary(&ref[0],V(6),V(7))
	move_ret ref[55]
	c_ret

;; TERM 56: viewer(_W, _X)
c_code local build_ref_56
	ret_reg &ref[56]
	call_c   build_ref_310()
	call_c   Dyam_Create_Binary(&ref[310],V(22),V(23))
	move_ret ref[56]
	c_ret

;; TERM 310: viewer
c_code local build_ref_310
	ret_reg &ref[310]
	call_c   Dyam_Create_Atom("viewer")
	move_ret ref[310]
	c_ret

long local pool_fun10[10]=[65544,build_seed_13,build_ref_44,build_seed_14,build_seed_15,build_ref_51,build_seed_16,build_ref_55,build_ref_56,pool_fun8]

pl_code local fun11
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(12),V(8))
	fail_ret
	call_c   Dyam_Unify(V(13),V(8))
	fail_ret
fun10:
	pl_call  fun4(&seed[13],1)
	call_c   Dyam_Unify(V(15),&ref[44])
	fail_ret
	pl_call  fun4(&seed[14],1)
	pl_call  fun4(&seed[15],1)
	call_c   Dyam_Choice(fun9)
	move     V(17), R(0)
	move     S(5), R(1)
	move     &ref[51], R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Load(4,V(4))
	move     V(18), R(6)
	move     S(5), R(7)
	pl_call  pred_rcg_functor_4()
	pl_call  fun4(&seed[16],1)
	call_c   DYAM_Numbervars_3(V(17),N(0),V(24))
	fail_ret
	call_c   Dyam_Unify(V(21),&ref[55])
	fail_ret
	call_c   Dyam_Reg_Load(0,V(21))
	move     V(22), R(2)
	move     S(5), R(3)
	pl_call  pred_label_term_2()
	call_c   Dyam_Reg_Load(0,V(17))
	move     V(23), R(2)
	move     S(5), R(3)
	pl_call  pred_label_term_2()
	call_c   DYAM_evpred_assert_1(&ref[56])
	pl_fail


;; TERM 40: [_M|_N]
c_code local build_ref_40
	ret_reg &ref[40]
	call_c   Dyam_Create_List(V(12),V(13))
	move_ret ref[40]
	c_ret

long local pool_fun12[4]=[131073,build_ref_40,pool_fun10,pool_fun10]

pl_code local fun12
	call_c   Dyam_Update_Choice(fun11)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_compound(V(8))
	fail_ret
	call_c   Dyam_Cut()
	call_c   DYAM_evpred_univ(V(8),&ref[40])
	fail_ret
	pl_jump  fun10()

long local pool_fun13[6]=[131075,build_ref_38,build_ref_39,build_ref_40,pool_fun12,pool_fun10]

long local pool_fun14[3]=[65537,build_ref_39,pool_fun13]

pl_code local fun14
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(8),&ref[39])
	fail_ret
fun13:
	call_c   DYAM_evpred_functor(V(4),V(1),V(2))
	fail_ret
	call_c   DYAM_evpred_is(V(10),&ref[38])
	fail_ret
	call_c   DYAM_evpred_length(V(5),V(10))
	fail_ret
	call_c   DYAM_Make_List(V(11),V(10),&ref[39])
	fail_ret
	call_c   Dyam_Choice(fun12)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(8),&ref[40])
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun10()


;; TERM 60: call_pattern(rcg('*default*', _J), _I)
c_code local build_ref_60
	ret_reg &ref[60]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_304()
	call_c   build_ref_312()
	call_c   Dyam_Create_Binary(&ref[304],&ref[312],V(9))
	move_ret R(0)
	call_c   build_ref_311()
	call_c   Dyam_Create_Binary(&ref[311],R(0),V(8))
	move_ret ref[60]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 311: call_pattern
c_code local build_ref_311
	ret_reg &ref[311]
	call_c   Dyam_Create_Atom("call_pattern")
	move_ret ref[311]
	c_ret

;; TERM 312: '*default*'
c_code local build_ref_312
	ret_reg &ref[312]
	call_c   Dyam_Create_Atom("*default*")
	move_ret ref[312]
	c_ret

long local pool_fun15[4]=[131073,build_ref_60,pool_fun14,pool_fun13]

pl_code local fun15
	call_c   Dyam_Update_Choice(fun14)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[60])
	call_c   Dyam_Cut()
	pl_jump  fun13()

;; TERM 36: call_pattern(rcg((_B / _C), _D), _I)
c_code local build_ref_36
	ret_reg &ref[36]
	call_c   build_ref_311()
	call_c   build_ref_51()
	call_c   Dyam_Create_Binary(&ref[311],&ref[51],V(8))
	move_ret ref[36]
	c_ret

long local pool_fun16[5]=[131074,build_ref_35,build_ref_36,pool_fun15,pool_fun13]

pl_code local fun16
	call_c   Dyam_Pool(pool_fun16)
	call_c   Dyam_Unify_Item(&ref[35])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun15)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[36])
	call_c   Dyam_Cut()
	pl_jump  fun13()

pl_code local fun17
	pl_jump  Apply(0,0)

;; TERM 33: '*FIRST*'('call_rcg_make_callret/5'(rcg((_B / _C), _D))) :> []
c_code local build_ref_33
	ret_reg &ref[33]
	call_c   build_ref_32()
	call_c   Dyam_Create_Binary(I(9),&ref[32],I(0))
	move_ret ref[33]
	c_ret

;; TERM 32: '*FIRST*'('call_rcg_make_callret/5'(rcg((_B / _C), _D)))
c_code local build_ref_32
	ret_reg &ref[32]
	call_c   build_ref_30()
	call_c   build_ref_31()
	call_c   Dyam_Create_Unary(&ref[30],&ref[31])
	move_ret ref[32]
	c_ret

;; TERM 30: '*FIRST*'
c_code local build_ref_30
	ret_reg &ref[30]
	call_c   Dyam_Create_Atom("*FIRST*")
	move_ret ref[30]
	c_ret

c_code local build_seed_4
	ret_reg &seed[4]
	call_c   build_ref_17()
	call_c   build_ref_68()
	call_c   Dyam_Seed_Start(&ref[17],&ref[68],I(0),fun1,1)
	call_c   build_ref_67()
	call_c   Dyam_Seed_Add_Comp(&ref[67],fun22,0)
	call_c   Dyam_Seed_End()
	move_ret seed[4]
	c_ret

;; TERM 67: '*GUARD*'(rcg_args_expand(_B, [_C|_D], [_E,_F|_G], _H, _I))
c_code local build_ref_67
	ret_reg &ref[67]
	call_c   build_ref_18()
	call_c   build_ref_66()
	call_c   Dyam_Create_Unary(&ref[18],&ref[66])
	move_ret ref[67]
	c_ret

;; TERM 66: rcg_args_expand(_B, [_C|_D], [_E,_F|_G], _H, _I)
c_code local build_ref_66
	ret_reg &ref[66]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   Dyam_Create_List(V(2),V(3))
	move_ret R(0)
	call_c   Dyam_Create_Tupple(4,5,V(6))
	move_ret R(1)
	call_c   build_ref_302()
	call_c   Dyam_Term_Start(&ref[302],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_End()
	move_ret ref[66]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_18
	ret_reg &seed[18]
	call_c   build_ref_18()
	call_c   build_ref_70()
	call_c   Dyam_Seed_Start(&ref[18],&ref[70],I(0),fun3,1)
	call_c   build_ref_71()
	call_c   Dyam_Seed_Add_Comp(&ref[71],&ref[70],0)
	call_c   Dyam_Seed_End()
	move_ret seed[18]
	c_ret

;; TERM 71: '*GUARD*'(rcg_args_expand(_B, _D, _G, _J, _I)) :> '$$HOLE$$'
c_code local build_ref_71
	ret_reg &ref[71]
	call_c   build_ref_70()
	call_c   Dyam_Create_Binary(I(9),&ref[70],I(7))
	move_ret ref[71]
	c_ret

;; TERM 70: '*GUARD*'(rcg_args_expand(_B, _D, _G, _J, _I))
c_code local build_ref_70
	ret_reg &ref[70]
	call_c   build_ref_18()
	call_c   build_ref_69()
	call_c   Dyam_Create_Unary(&ref[18],&ref[69])
	move_ret ref[70]
	c_ret

;; TERM 69: rcg_args_expand(_B, _D, _G, _J, _I)
c_code local build_ref_69
	ret_reg &ref[69]
	call_c   build_ref_302()
	call_c   Dyam_Term_Start(&ref[302],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_End()
	move_ret ref[69]
	c_ret

long local pool_fun22[3]=[2,build_ref_67,build_seed_18]

pl_code local fun22
	call_c   Dyam_Pool(pool_fun22)
	call_c   Dyam_Unify_Item(&ref[67])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(2))
	call_c   Dyam_Reg_Load(4,V(4))
	call_c   Dyam_Reg_Load(6,V(5))
	call_c   Dyam_Reg_Load(8,V(7))
	move     V(9), R(10)
	move     S(5), R(11)
	pl_call  pred_rcg_str_expand_6()
	call_c   Dyam_Deallocate()
	pl_jump  fun4(&seed[18],1)

;; TERM 68: '*GUARD*'(rcg_args_expand(_B, [_C|_D], [_E,_F|_G], _H, _I)) :> []
c_code local build_ref_68
	ret_reg &ref[68]
	call_c   build_ref_67()
	call_c   Dyam_Create_Binary(I(9),&ref[67],I(0))
	move_ret ref[68]
	c_ret

c_code local build_seed_6
	ret_reg &seed[6]
	call_c   build_ref_30()
	call_c   build_ref_74()
	call_c   Dyam_Seed_Start(&ref[30],&ref[74],I(0),fun17,1)
	call_c   build_ref_75()
	call_c   Dyam_Seed_Add_Comp(&ref[75],fun23,0)
	call_c   Dyam_Seed_End()
	move_ret seed[6]
	c_ret

;; TERM 75: '*CITEM*'('call_rcg_wrap_args/5'(rcg((_B / _C), _D)), _A)
c_code local build_ref_75
	ret_reg &ref[75]
	call_c   build_ref_34()
	call_c   build_ref_72()
	call_c   Dyam_Create_Binary(&ref[34],&ref[72],V(0))
	move_ret ref[75]
	c_ret

;; TERM 72: 'call_rcg_wrap_args/5'(rcg((_B / _C), _D))
c_code local build_ref_72
	ret_reg &ref[72]
	call_c   build_ref_313()
	call_c   build_ref_51()
	call_c   Dyam_Create_Unary(&ref[313],&ref[51])
	move_ret ref[72]
	c_ret

;; TERM 313: 'call_rcg_wrap_args/5'
c_code local build_ref_313
	ret_reg &ref[313]
	call_c   Dyam_Create_Atom("call_rcg_wrap_args/5")
	move_ret ref[313]
	c_ret

;; TERM 76: [_I|_J]
c_code local build_ref_76
	ret_reg &ref[76]
	call_c   Dyam_Create_List(V(8),V(9))
	move_ret ref[76]
	c_ret

;; TERM 77: 2 * _D
c_code local build_ref_77
	ret_reg &ref[77]
	call_c   build_ref_37()
	call_c   Dyam_Create_Binary(&ref[37],N(2),V(3))
	move_ret ref[77]
	c_ret

c_code local build_seed_19
	ret_reg &seed[19]
	call_c   build_ref_18()
	call_c   build_ref_79()
	call_c   Dyam_Seed_Start(&ref[18],&ref[79],I(0),fun3,1)
	call_c   build_ref_80()
	call_c   Dyam_Seed_Add_Comp(&ref[80],&ref[79],0)
	call_c   Dyam_Seed_End()
	move_ret seed[19]
	c_ret

;; TERM 80: '*GUARD*'(append(_J, [_F|_G], _H)) :> '$$HOLE$$'
c_code local build_ref_80
	ret_reg &ref[80]
	call_c   build_ref_79()
	call_c   Dyam_Create_Binary(I(9),&ref[79],I(7))
	move_ret ref[80]
	c_ret

;; TERM 79: '*GUARD*'(append(_J, [_F|_G], _H))
c_code local build_ref_79
	ret_reg &ref[79]
	call_c   build_ref_18()
	call_c   build_ref_78()
	call_c   Dyam_Create_Unary(&ref[18],&ref[78])
	move_ret ref[79]
	c_ret

;; TERM 78: append(_J, [_F|_G], _H)
c_code local build_ref_78
	ret_reg &ref[78]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(5),V(6))
	move_ret R(0)
	call_c   build_ref_306()
	call_c   Dyam_Term_Start(&ref[306],3)
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[78]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun23[6]=[5,build_ref_75,build_ref_76,build_ref_77,build_seed_19,build_seed_17]

pl_code local fun23
	call_c   Dyam_Pool(pool_fun23)
	call_c   Dyam_Unify_Item(&ref[75])
	fail_ret
	call_c   DYAM_evpred_functor(V(4),V(1),V(2))
	fail_ret
	call_c   DYAM_evpred_univ(V(4),&ref[76])
	fail_ret
	call_c   DYAM_evpred_is(V(10),&ref[77])
	fail_ret
	call_c   DYAM_evpred_length(V(6),V(10))
	fail_ret
	call_c   Dyam_Allocate(0)
	pl_call  fun4(&seed[19],1)
	call_c   Dyam_Deallocate()
	pl_jump  fun7(&seed[17],2)

;; TERM 74: '*FIRST*'('call_rcg_wrap_args/5'(rcg((_B / _C), _D))) :> []
c_code local build_ref_74
	ret_reg &ref[74]
	call_c   build_ref_73()
	call_c   Dyam_Create_Binary(I(9),&ref[73],I(0))
	move_ret ref[74]
	c_ret

;; TERM 73: '*FIRST*'('call_rcg_wrap_args/5'(rcg((_B / _C), _D)))
c_code local build_ref_73
	ret_reg &ref[73]
	call_c   build_ref_30()
	call_c   build_ref_72()
	call_c   Dyam_Create_Unary(&ref[30],&ref[72])
	move_ret ref[73]
	c_ret

c_code local build_seed_1
	ret_reg &seed[1]
	call_c   build_ref_30()
	call_c   build_ref_83()
	call_c   Dyam_Seed_Start(&ref[30],&ref[83],I(0),fun17,1)
	call_c   build_ref_84()
	call_c   Dyam_Seed_Add_Comp(&ref[84],fun29,0)
	call_c   Dyam_Seed_End()
	move_ret seed[1]
	c_ret

;; TERM 84: '*CITEM*'('call_term_module_shift/5'(_B, rcg((_C / _D), _E)), _A)
c_code local build_ref_84
	ret_reg &ref[84]
	call_c   build_ref_34()
	call_c   build_ref_81()
	call_c   Dyam_Create_Binary(&ref[34],&ref[81],V(0))
	move_ret ref[84]
	c_ret

;; TERM 81: 'call_term_module_shift/5'(_B, rcg((_C / _D), _E))
c_code local build_ref_81
	ret_reg &ref[81]
	call_c   build_ref_314()
	call_c   build_ref_114()
	call_c   Dyam_Create_Binary(&ref[314],V(1),&ref[114])
	move_ret ref[81]
	c_ret

;; TERM 114: rcg((_C / _D), _E)
c_code local build_ref_114
	ret_reg &ref[114]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_305()
	call_c   Dyam_Create_Binary(&ref[305],V(2),V(3))
	move_ret R(0)
	call_c   build_ref_304()
	call_c   Dyam_Create_Binary(&ref[304],R(0),V(4))
	move_ret ref[114]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 314: 'call_term_module_shift/5'
c_code local build_ref_314
	ret_reg &ref[314]
	call_c   Dyam_Create_Atom("call_term_module_shift/5")
	move_ret ref[314]
	c_ret

;; TERM 86: [_C|_K]
c_code local build_ref_86
	ret_reg &ref[86]
	call_c   Dyam_Create_List(V(2),V(10))
	move_ret ref[86]
	c_ret

;; TERM 87: [_F|_K]
c_code local build_ref_87
	ret_reg &ref[87]
	call_c   Dyam_Create_List(V(5),V(10))
	move_ret ref[87]
	c_ret

c_code local build_seed_20
	ret_reg &seed[20]
	call_c   build_ref_0()
	call_c   build_ref_89()
	call_c   Dyam_Seed_Start(&ref[0],&ref[89],&ref[89],fun3,1)
	call_c   build_ref_90()
	call_c   Dyam_Seed_Add_Comp(&ref[90],&ref[89],0)
	call_c   Dyam_Seed_End()
	move_ret seed[20]
	c_ret

;; TERM 90: '*RITEM*'(_A, return(rcg((_F / _D), _E), _G, _H)) :> '$$HOLE$$'
c_code local build_ref_90
	ret_reg &ref[90]
	call_c   build_ref_89()
	call_c   Dyam_Create_Binary(I(9),&ref[89],I(7))
	move_ret ref[90]
	c_ret

;; TERM 89: '*RITEM*'(_A, return(rcg((_F / _D), _E), _G, _H))
c_code local build_ref_89
	ret_reg &ref[89]
	call_c   build_ref_0()
	call_c   build_ref_88()
	call_c   Dyam_Create_Binary(&ref[0],V(0),&ref[88])
	move_ret ref[89]
	c_ret

;; TERM 88: return(rcg((_F / _D), _E), _G, _H)
c_code local build_ref_88
	ret_reg &ref[88]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_305()
	call_c   Dyam_Create_Binary(&ref[305],V(5),V(3))
	move_ret R(0)
	call_c   build_ref_304()
	call_c   Dyam_Create_Binary(&ref[304],R(0),V(4))
	move_ret R(0)
	call_c   build_ref_309()
	call_c   Dyam_Term_Start(&ref[309],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[88]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun24[4]=[3,build_ref_86,build_ref_87,build_seed_20]

pl_code local fun27
	call_c   Dyam_Remove_Choice()
fun26:
	call_c   DYAM_evpred_atom_to_module(V(2),I(0),V(9))
	fail_ret
	call_c   Dyam_Unify(V(8),V(1))
	fail_ret
fun24:
	call_c   DYAM_evpred_atom_to_module(V(5),V(8),V(2))
	fail_ret
	call_c   DYAM_evpred_length(V(10),V(3))
	fail_ret
	call_c   DYAM_evpred_univ(V(6),&ref[86])
	fail_ret
	call_c   DYAM_evpred_univ(V(7),&ref[87])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_jump  fun7(&seed[20],2)



pl_code local fun28
	call_c   Dyam_Update_Choice(fun27)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(1),I(0))
	fail_ret
	call_c   Dyam_Cut()
	pl_fail

;; TERM 85: module_import(rcg((_C / _D), _E), _I)
c_code local build_ref_85
	ret_reg &ref[85]
	call_c   build_ref_315()
	call_c   build_ref_114()
	call_c   Dyam_Create_Binary(&ref[315],&ref[114],V(8))
	move_ret ref[85]
	c_ret

;; TERM 315: module_import
c_code local build_ref_315
	ret_reg &ref[315]
	call_c   Dyam_Create_Atom("module_import")
	move_ret ref[315]
	c_ret

pl_code local fun25
	call_c   Dyam_Remove_Choice()
	pl_jump  fun24()

long local pool_fun29[5]=[131074,build_ref_84,build_ref_85,pool_fun24,pool_fun24]

pl_code local fun29
	call_c   Dyam_Pool(pool_fun29)
	call_c   Dyam_Unify_Item(&ref[84])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun28)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[85])
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun25)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(8),I(0))
	fail_ret
	call_c   Dyam_Cut()
	pl_fail

;; TERM 83: '*FIRST*'('call_term_module_shift/5'(_B, rcg((_C / _D), _E))) :> []
c_code local build_ref_83
	ret_reg &ref[83]
	call_c   build_ref_82()
	call_c   Dyam_Create_Binary(I(9),&ref[82],I(0))
	move_ret ref[83]
	c_ret

;; TERM 82: '*FIRST*'('call_term_module_shift/5'(_B, rcg((_C / _D), _E)))
c_code local build_ref_82
	ret_reg &ref[82]
	call_c   build_ref_30()
	call_c   build_ref_81()
	call_c   Dyam_Create_Unary(&ref[30],&ref[81])
	move_ret ref[82]
	c_ret

c_code local build_seed_5
	ret_reg &seed[5]
	call_c   build_ref_17()
	call_c   build_ref_93()
	call_c   Dyam_Seed_Start(&ref[17],&ref[93],I(0),fun1,1)
	call_c   build_ref_92()
	call_c   Dyam_Seed_Add_Comp(&ref[92],fun39,0)
	call_c   Dyam_Seed_End()
	move_ret seed[5]
	c_ret

;; TERM 92: '*GUARD*'(rcg_expand(_B, _C, rcg((_D / _E), _F), _G, _H, _I))
c_code local build_ref_92
	ret_reg &ref[92]
	call_c   build_ref_18()
	call_c   build_ref_91()
	call_c   Dyam_Create_Unary(&ref[18],&ref[91])
	move_ret ref[92]
	c_ret

;; TERM 91: rcg_expand(_B, _C, rcg((_D / _E), _F), _G, _H, _I)
c_code local build_ref_91
	ret_reg &ref[91]
	call_c   build_ref_316()
	call_c   build_ref_95()
	call_c   Dyam_Term_Start(&ref[316],6)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(&ref[95])
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_End()
	move_ret ref[91]
	c_ret

;; TERM 95: rcg((_D / _E), _F)
c_code local build_ref_95
	ret_reg &ref[95]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_305()
	call_c   Dyam_Create_Binary(&ref[305],V(3),V(4))
	move_ret R(0)
	call_c   build_ref_304()
	call_c   Dyam_Create_Binary(&ref[304],R(0),V(5))
	move_ret ref[95]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 316: rcg_expand
c_code local build_ref_316
	ret_reg &ref[316]
	call_c   Dyam_Create_Atom("rcg_expand")
	move_ret ref[316]
	c_ret

;; TERM 111: 'rcg litteral expected ~w\n'
c_code local build_ref_111
	ret_reg &ref[111]
	call_c   Dyam_Create_Atom("rcg litteral expected ~w\n")
	move_ret ref[111]
	c_ret

;; TERM 112: [_K]
c_code local build_ref_112
	ret_reg &ref[112]
	call_c   Dyam_Create_List(V(10),I(0))
	move_ret ref[112]
	c_ret

;; TERM 94: rcg((_J / _E), _F)
c_code local build_ref_94
	ret_reg &ref[94]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_305()
	call_c   Dyam_Create_Binary(&ref[305],V(9),V(4))
	move_ret R(0)
	call_c   build_ref_304()
	call_c   Dyam_Create_Binary(&ref[304],R(0),V(5))
	move_ret ref[94]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 110: '$CLOSURE'('$fun'(36, 0, 1087687716), '$TUPPLE'(35080023054144))
c_code local build_ref_110
	ret_reg &ref[110]
	call_c   build_ref_109()
	call_c   Dyam_Closure_Aux(fun36,&ref[109])
	move_ret ref[110]
	c_ret

c_code local build_seed_23
	ret_reg &seed[23]
	call_c   build_ref_18()
	call_c   build_ref_105()
	call_c   Dyam_Seed_Start(&ref[18],&ref[105],I(0),fun3,1)
	call_c   build_ref_106()
	call_c   Dyam_Seed_Add_Comp(&ref[106],&ref[105],0)
	call_c   Dyam_Seed_End()
	move_ret seed[23]
	c_ret

;; TERM 106: '*GUARD*'(rcg_args_expand(_B, _L, _H, _I, true)) :> '$$HOLE$$'
c_code local build_ref_106
	ret_reg &ref[106]
	call_c   build_ref_105()
	call_c   Dyam_Create_Binary(I(9),&ref[105],I(7))
	move_ret ref[106]
	c_ret

;; TERM 105: '*GUARD*'(rcg_args_expand(_B, _L, _H, _I, true))
c_code local build_ref_105
	ret_reg &ref[105]
	call_c   build_ref_18()
	call_c   build_ref_104()
	call_c   Dyam_Create_Unary(&ref[18],&ref[104])
	move_ret ref[105]
	c_ret

;; TERM 104: rcg_args_expand(_B, _L, _H, _I, true)
c_code local build_ref_104
	ret_reg &ref[104]
	call_c   build_ref_302()
	call_c   build_ref_293()
	call_c   Dyam_Term_Start(&ref[302],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(&ref[293])
	call_c   Dyam_Term_End()
	move_ret ref[104]
	c_ret

long local pool_fun36[2]=[1,build_seed_23]

pl_code local fun36
	call_c   Dyam_Pool(pool_fun36)
	call_c   Dyam_Allocate(0)
	pl_call  fun4(&seed[23],1)
	call_c   Dyam_Reg_Load(0,V(6))
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(12), R(4)
	move     S(5), R(5)
	pl_call  pred_my_numbervars_3()
	call_c   Dyam_Reg_Load(0,V(7))
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(13), R(4)
	move     S(5), R(5)
	pl_call  pred_my_numbervars_3()
	call_c   Dyam_Reg_Load(0,V(8))
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(14), R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  pred_my_numbervars_3()

;; TERM 109: '$TUPPLE'(35080023054144)
c_code local build_ref_109
	ret_reg &ref[109]
	call_c   Dyam_Create_Simple_Tupple(0,141688832)
	move_ret ref[109]
	c_ret

long local pool_fun37[4]=[3,build_ref_94,build_ref_110,build_ref_95]

long local pool_fun38[4]=[65538,build_ref_111,build_ref_112,pool_fun37]

pl_code local fun38
	call_c   Dyam_Remove_Choice()
	move     &ref[111], R(0)
	move     0, R(1)
	move     &ref[112], R(2)
	move     S(5), R(3)
	pl_call  pred_error_2()
fun37:
	move     &ref[94], R(0)
	move     S(5), R(1)
	move     &ref[95], R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Load(4,V(10))
	call_c   Dyam_Reg_Load(6,V(6))
	pl_call  pred_term_current_module_shift_4()
	move     &ref[110], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	move     &ref[95], R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Deallocate(3)
fun35:
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Reg_Bind(2,V(3))
	call_c   Dyam_Reg_Bind(4,V(2))
	call_c   Dyam_Choice(fun34)
	pl_call  fun31(&seed[21],1)
	pl_fail



long local pool_fun39[5]=[131074,build_ref_92,build_ref_94,pool_fun38,pool_fun37]

pl_code local fun39
	call_c   Dyam_Pool(pool_fun39)
	call_c   Dyam_Unify_Item(&ref[92])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun38)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Reg_Load(0,V(2))
	move     &ref[94], R(2)
	move     S(5), R(3)
	move     V(10), R(4)
	move     S(5), R(5)
	move     V(11), R(6)
	move     S(5), R(7)
	pl_call  pred_rcg_functor_4()
	call_c   Dyam_Cut()
	pl_jump  fun37()

;; TERM 93: '*GUARD*'(rcg_expand(_B, _C, rcg((_D / _E), _F), _G, _H, _I)) :> []
c_code local build_ref_93
	ret_reg &ref[93]
	call_c   build_ref_92()
	call_c   Dyam_Create_Binary(I(9),&ref[92],I(0))
	move_ret ref[93]
	c_ret

c_code local build_seed_7
	ret_reg &seed[7]
	call_c   build_ref_30()
	call_c   build_ref_117()
	call_c   Dyam_Seed_Start(&ref[30],&ref[117],I(0),fun17,1)
	call_c   build_ref_118()
	call_c   Dyam_Seed_Add_Comp(&ref[118],fun46,0)
	call_c   Dyam_Seed_End()
	move_ret seed[7]
	c_ret

;; TERM 118: '*CITEM*'(wrapping_predicate(rcg_prolog(rcg((_B / _C), _D))), _A)
c_code local build_ref_118
	ret_reg &ref[118]
	call_c   build_ref_34()
	call_c   build_ref_115()
	call_c   Dyam_Create_Binary(&ref[34],&ref[115],V(0))
	move_ret ref[118]
	c_ret

;; TERM 115: wrapping_predicate(rcg_prolog(rcg((_B / _C), _D)))
c_code local build_ref_115
	ret_reg &ref[115]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_318()
	call_c   build_ref_51()
	call_c   Dyam_Create_Unary(&ref[318],&ref[51])
	move_ret R(0)
	call_c   build_ref_317()
	call_c   Dyam_Create_Unary(&ref[317],R(0))
	move_ret ref[115]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 317: wrapping_predicate
c_code local build_ref_317
	ret_reg &ref[317]
	call_c   Dyam_Create_Atom("wrapping_predicate")
	move_ret ref[317]
	c_ret

;; TERM 318: rcg_prolog
c_code local build_ref_318
	ret_reg &ref[318]
	call_c   Dyam_Create_Atom("rcg_prolog")
	move_ret ref[318]
	c_ret

c_code local build_seed_24
	ret_reg &seed[24]
	call_c   build_ref_18()
	call_c   build_ref_120()
	call_c   Dyam_Seed_Start(&ref[18],&ref[120],I(0),fun3,1)
	call_c   build_ref_121()
	call_c   Dyam_Seed_Add_Comp(&ref[121],&ref[120],0)
	call_c   Dyam_Seed_End()
	move_ret seed[24]
	c_ret

;; TERM 121: '*GUARD*'(rcg_args_expand(_H, _G, _I, _J, true)) :> '$$HOLE$$'
c_code local build_ref_121
	ret_reg &ref[121]
	call_c   build_ref_120()
	call_c   Dyam_Create_Binary(I(9),&ref[120],I(7))
	move_ret ref[121]
	c_ret

;; TERM 120: '*GUARD*'(rcg_args_expand(_H, _G, _I, _J, true))
c_code local build_ref_120
	ret_reg &ref[120]
	call_c   build_ref_18()
	call_c   build_ref_119()
	call_c   Dyam_Create_Unary(&ref[18],&ref[119])
	move_ret ref[120]
	c_ret

;; TERM 119: rcg_args_expand(_H, _G, _I, _J, true)
c_code local build_ref_119
	ret_reg &ref[119]
	call_c   build_ref_302()
	call_c   build_ref_293()
	call_c   Dyam_Term_Start(&ref[302],5)
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(&ref[293])
	call_c   Dyam_Term_End()
	move_ret ref[119]
	c_ret

c_code local build_seed_25
	ret_reg &seed[25]
	call_c   build_ref_18()
	call_c   build_ref_123()
	call_c   Dyam_Seed_Start(&ref[18],&ref[123],I(0),fun3,1)
	call_c   build_ref_124()
	call_c   Dyam_Seed_Add_Comp(&ref[124],&ref[123],0)
	call_c   Dyam_Seed_End()
	move_ret seed[25]
	c_ret

;; TERM 124: '*GUARD*'(rcg_prolog_make(rcg((_B / _C), _D), _F, _I, _K)) :> '$$HOLE$$'
c_code local build_ref_124
	ret_reg &ref[124]
	call_c   build_ref_123()
	call_c   Dyam_Create_Binary(I(9),&ref[123],I(7))
	move_ret ref[124]
	c_ret

;; TERM 123: '*GUARD*'(rcg_prolog_make(rcg((_B / _C), _D), _F, _I, _K))
c_code local build_ref_123
	ret_reg &ref[123]
	call_c   build_ref_18()
	call_c   build_ref_122()
	call_c   Dyam_Create_Unary(&ref[18],&ref[122])
	move_ret ref[123]
	c_ret

;; TERM 122: rcg_prolog_make(rcg((_B / _C), _D), _F, _I, _K)
c_code local build_ref_122
	ret_reg &ref[122]
	call_c   build_ref_303()
	call_c   build_ref_51()
	call_c   Dyam_Term_Start(&ref[303],4)
	call_c   Dyam_Term_Arg(&ref[51])
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_End()
	move_ret ref[122]
	c_ret

;; TERM 139: '$CLOSURE'('$fun'(45, 0, 1087845880), '$TUPPLE'(35080023054544))
c_code local build_ref_139
	ret_reg &ref[139]
	call_c   build_ref_138()
	call_c   Dyam_Closure_Aux(fun45,&ref[138])
	move_ret ref[139]
	c_ret

;; TERM 133: [_N,_F,_L,_I]
c_code local build_ref_133
	ret_reg &ref[133]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(8),I(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(11),R(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(5),R(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(13),R(0))
	move_ret ref[133]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 134: '*WRAPPER*'(rcg_prolog(rcg((_B / _C), _D)), [_N|_M], '*PROLOG-LAST*'(_K, _L))
c_code local build_ref_134
	ret_reg &ref[134]
	call_c   Dyam_Pseudo_Choice(3)
	call_c   build_ref_318()
	call_c   build_ref_51()
	call_c   Dyam_Create_Unary(&ref[318],&ref[51])
	move_ret R(0)
	call_c   Dyam_Create_List(V(13),V(12))
	move_ret R(1)
	call_c   build_ref_188()
	call_c   Dyam_Create_Binary(&ref[188],V(10),V(11))
	move_ret R(2)
	call_c   build_ref_319()
	call_c   Dyam_Term_Start(&ref[319],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(R(2))
	call_c   Dyam_Term_End()
	move_ret ref[134]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 319: '*WRAPPER*'
c_code local build_ref_319
	ret_reg &ref[319]
	call_c   Dyam_Create_Atom("*WRAPPER*")
	move_ret ref[319]
	c_ret

;; TERM 188: '*PROLOG-LAST*'
c_code local build_ref_188
	ret_reg &ref[188]
	call_c   Dyam_Create_Atom("*PROLOG-LAST*")
	move_ret ref[188]
	c_ret

c_code local build_seed_28
	ret_reg &seed[28]
	call_c   build_ref_0()
	call_c   build_ref_135()
	call_c   Dyam_Seed_Start(&ref[0],&ref[135],&ref[135],fun3,1)
	call_c   build_ref_136()
	call_c   Dyam_Seed_Add_Comp(&ref[136],&ref[135],0)
	call_c   Dyam_Seed_End()
	move_ret seed[28]
	c_ret

;; TERM 136: '*RITEM*'(_A, voidret) :> '$$HOLE$$'
c_code local build_ref_136
	ret_reg &ref[136]
	call_c   build_ref_135()
	call_c   Dyam_Create_Binary(I(9),&ref[135],I(7))
	move_ret ref[136]
	c_ret

;; TERM 135: '*RITEM*'(_A, voidret)
c_code local build_ref_135
	ret_reg &ref[135]
	call_c   build_ref_0()
	call_c   build_ref_10()
	call_c   Dyam_Create_Binary(&ref[0],V(0),&ref[10])
	move_ret ref[135]
	c_ret

;; TERM 10: voidret
c_code local build_ref_10
	ret_reg &ref[10]
	call_c   Dyam_Create_Atom("voidret")
	move_ret ref[10]
	c_ret

long local pool_fun45[4]=[3,build_ref_133,build_ref_134,build_seed_28]

pl_code local fun45
	call_c   Dyam_Pool(pool_fun45)
	call_c   Dyam_Allocate(0)
	move     &ref[133], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(7))
	move     V(14), R(4)
	move     S(5), R(5)
	pl_call  pred_my_numbervars_3()
	call_c   DYAM_evpred_assert_1(&ref[134])
	call_c   Dyam_Deallocate()
	pl_jump  fun7(&seed[28],2)

;; TERM 138: '$TUPPLE'(35080023054544)
c_code local build_ref_138
	ret_reg &ref[138]
	call_c   Dyam_Create_Simple_Tupple(0,515309568)
	move_ret ref[138]
	c_ret

long local pool_fun46[6]=[5,build_ref_118,build_seed_24,build_seed_25,build_ref_139,build_ref_51]

pl_code local fun46
	call_c   Dyam_Pool(pool_fun46)
	call_c   Dyam_Unify_Item(&ref[118])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     V(4), R(0)
	move     S(5), R(1)
	move     &ref[51], R(2)
	move     S(5), R(3)
	move     V(5), R(4)
	move     S(5), R(5)
	move     V(6), R(6)
	move     S(5), R(7)
	pl_call  pred_rcg_functor_4()
	pl_call  fun4(&seed[24],1)
	pl_call  fun4(&seed[25],1)
	move     &ref[139], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	move     &ref[51], R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Load(6,V(5))
	move     V(11), R(8)
	move     S(5), R(9)
	call_c   Dyam_Reg_Load(10,V(8))
	move     V(12), R(12)
	move     S(5), R(13)
	call_c   Dyam_Reg_Deallocate(7)
fun44:
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Reg_Bind(2,V(7))
	call_c   Dyam_Multi_Reg_Bind(4,2,5)
	call_c   Dyam_Choice(fun43)
	pl_call  fun31(&seed[26],1)
	pl_fail


;; TERM 117: '*FIRST*'(wrapping_predicate(rcg_prolog(rcg((_B / _C), _D)))) :> []
c_code local build_ref_117
	ret_reg &ref[117]
	call_c   build_ref_116()
	call_c   Dyam_Create_Binary(I(9),&ref[116],I(0))
	move_ret ref[117]
	c_ret

;; TERM 116: '*FIRST*'(wrapping_predicate(rcg_prolog(rcg((_B / _C), _D))))
c_code local build_ref_116
	ret_reg &ref[116]
	call_c   build_ref_30()
	call_c   build_ref_115()
	call_c   Dyam_Create_Unary(&ref[30],&ref[115])
	move_ret ref[116]
	c_ret

c_code local build_seed_8
	ret_reg &seed[8]
	call_c   build_ref_30()
	call_c   build_ref_142()
	call_c   Dyam_Seed_Start(&ref[30],&ref[142],I(0),fun17,1)
	call_c   build_ref_143()
	call_c   Dyam_Seed_Add_Comp(&ref[143],fun54,0)
	call_c   Dyam_Seed_End()
	move_ret seed[8]
	c_ret

;; TERM 143: '*CITEM*'(wrapping_predicate(rcg((_B / _C), _D)), _A)
c_code local build_ref_143
	ret_reg &ref[143]
	call_c   build_ref_34()
	call_c   build_ref_140()
	call_c   Dyam_Create_Binary(&ref[34],&ref[140],V(0))
	move_ret ref[143]
	c_ret

;; TERM 140: wrapping_predicate(rcg((_B / _C), _D))
c_code local build_ref_140
	ret_reg &ref[140]
	call_c   build_ref_317()
	call_c   build_ref_51()
	call_c   Dyam_Create_Unary(&ref[317],&ref[51])
	move_ret ref[140]
	c_ret

;; TERM 161: '$CLOSURE'('$fun'(53, 0, 1087917620), '$TUPPLE'(35080023055072))
c_code local build_ref_161
	ret_reg &ref[161]
	call_c   build_ref_160()
	call_c   Dyam_Closure_Aux(fun53,&ref[160])
	move_ret ref[161]
	c_ret

;; TERM 158: '$CLOSURE'('$fun'(52, 0, 1087905104), '$TUPPLE'(35080023054916))
c_code local build_ref_158
	ret_reg &ref[158]
	call_c   build_ref_157()
	call_c   Dyam_Closure_Aux(fun52,&ref[157])
	move_ret ref[158]
	c_ret

;; TERM 151: [_O,_F,_M,_I]
c_code local build_ref_151
	ret_reg &ref[151]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(8),I(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(12),R(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(5),R(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(14),R(0))
	move_ret ref[151]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 155: '*NEXT*'{nabla=> _M, call=> _K, ret=> _L, right=> '*PROLOG-LAST*'}
c_code local build_ref_155
	ret_reg &ref[155]
	call_c   build_ref_320()
	call_c   build_ref_188()
	call_c   Dyam_Term_Start(&ref[320],4)
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(&ref[188])
	call_c   Dyam_Term_End()
	move_ret ref[155]
	c_ret

;; TERM 320: '*NEXT*'!'$ft'
c_code local build_ref_320
	ret_reg &ref[320]
	call_c   build_ref_321()
	call_c   Dyam_Create_Atom_Module("$ft",&ref[321])
	move_ret ref[320]
	c_ret

;; TERM 321: '*NEXT*'
c_code local build_ref_321
	ret_reg &ref[321]
	call_c   Dyam_Create_Atom("*NEXT*")
	move_ret ref[321]
	c_ret

;; TERM 154: '*WRAPPER*'(rcg((_B / _C), _D), [_O|_N], _Q)
c_code local build_ref_154
	ret_reg &ref[154]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(14),V(13))
	move_ret R(0)
	call_c   build_ref_319()
	call_c   build_ref_51()
	call_c   Dyam_Term_Start(&ref[319],3)
	call_c   Dyam_Term_Arg(&ref[51])
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_End()
	move_ret ref[154]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun50[3]=[2,build_ref_154,build_seed_28]

long local pool_fun51[3]=[65537,build_ref_155,pool_fun50]

pl_code local fun51
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(16),&ref[155])
	fail_ret
fun50:
	call_c   DYAM_evpred_assert_1(&ref[154])
	call_c   Dyam_Deallocate()
	pl_jump  fun7(&seed[28],2)


;; TERM 152: light_tabular rcg((_B / _C), _D)
c_code local build_ref_152
	ret_reg &ref[152]
	call_c   build_ref_322()
	call_c   build_ref_51()
	call_c   Dyam_Create_Unary(&ref[322],&ref[51])
	move_ret ref[152]
	c_ret

;; TERM 322: light_tabular
c_code local build_ref_322
	ret_reg &ref[322]
	call_c   Dyam_Create_Atom("light_tabular")
	move_ret ref[322]
	c_ret

;; TERM 153: '*LIGHTNEXTALT*'(_K, _L, '*PROLOG-LAST*', _M)
c_code local build_ref_153
	ret_reg &ref[153]
	call_c   build_ref_323()
	call_c   build_ref_188()
	call_c   Dyam_Term_Start(&ref[323],4)
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(&ref[188])
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_End()
	move_ret ref[153]
	c_ret

;; TERM 323: '*LIGHTNEXTALT*'
c_code local build_ref_323
	ret_reg &ref[323]
	call_c   Dyam_Create_Atom("*LIGHTNEXTALT*")
	move_ret ref[323]
	c_ret

long local pool_fun52[6]=[131075,build_ref_151,build_ref_152,build_ref_153,pool_fun51,pool_fun50]

pl_code local fun52
	call_c   Dyam_Pool(pool_fun52)
	call_c   Dyam_Allocate(0)
	move     &ref[151], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(7))
	move     V(15), R(4)
	move     S(5), R(5)
	pl_call  pred_my_numbervars_3()
	call_c   Dyam_Choice(fun51)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[152])
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(16),&ref[153])
	fail_ret
	pl_jump  fun50()

;; TERM 157: '$TUPPLE'(35080023054916)
c_code local build_ref_157
	ret_reg &ref[157]
	call_c   Dyam_Create_Simple_Tupple(0,515342336)
	move_ret ref[157]
	c_ret

long local pool_fun53[3]=[2,build_ref_158,build_ref_51]

pl_code local fun53
	call_c   Dyam_Pool(pool_fun53)
	call_c   Dyam_Allocate(0)
	move     &ref[158], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	move     &ref[51], R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Load(6,V(5))
	move     V(12), R(8)
	move     S(5), R(9)
	call_c   Dyam_Reg_Load(10,V(8))
	move     V(13), R(12)
	move     S(5), R(13)
	call_c   Dyam_Reg_Deallocate(7)
	pl_jump  fun44()

;; TERM 160: '$TUPPLE'(35080023055072)
c_code local build_ref_160
	ret_reg &ref[160]
	call_c   Dyam_Create_Simple_Tupple(0,515244032)
	move_ret ref[160]
	c_ret

long local pool_fun54[5]=[4,build_ref_143,build_seed_24,build_ref_161,build_ref_51]

pl_code local fun54
	call_c   Dyam_Pool(pool_fun54)
	call_c   Dyam_Unify_Item(&ref[143])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     V(4), R(0)
	move     S(5), R(1)
	move     &ref[51], R(2)
	move     S(5), R(3)
	move     V(5), R(4)
	move     S(5), R(5)
	move     V(6), R(6)
	move     S(5), R(7)
	pl_call  pred_rcg_functor_4()
	pl_call  fun4(&seed[24],1)
	move     &ref[161], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	move     &ref[51], R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Load(6,V(5))
	call_c   Dyam_Reg_Load(8,V(8))
	move     V(10), R(10)
	move     S(5), R(11)
	move     V(11), R(12)
	move     S(5), R(13)
	call_c   Dyam_Reg_Deallocate(7)
fun49:
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Reg_Bind(2,V(7))
	call_c   Dyam_Multi_Reg_Bind(4,2,5)
	call_c   Dyam_Choice(fun48)
	pl_call  fun31(&seed[29],1)
	pl_fail


;; TERM 142: '*FIRST*'(wrapping_predicate(rcg((_B / _C), _D))) :> []
c_code local build_ref_142
	ret_reg &ref[142]
	call_c   build_ref_141()
	call_c   Dyam_Create_Binary(I(9),&ref[141],I(0))
	move_ret ref[142]
	c_ret

;; TERM 141: '*FIRST*'(wrapping_predicate(rcg((_B / _C), _D)))
c_code local build_ref_141
	ret_reg &ref[141]
	call_c   build_ref_30()
	call_c   build_ref_140()
	call_c   Dyam_Create_Unary(&ref[30],&ref[140])
	move_ret ref[141]
	c_ret

c_code local build_seed_11
	ret_reg &seed[11]
	call_c   build_ref_17()
	call_c   build_ref_164()
	call_c   Dyam_Seed_Start(&ref[17],&ref[164],I(0),fun1,1)
	call_c   build_ref_163()
	call_c   Dyam_Seed_Add_Comp(&ref[163],fun60,0)
	call_c   Dyam_Seed_End()
	move_ret seed[11]
	c_ret

;; TERM 163: '*GUARD*'(pgm_to_lpda(grammar_rule(rcg, (_B --> _C)), _D))
c_code local build_ref_163
	ret_reg &ref[163]
	call_c   build_ref_18()
	call_c   build_ref_162()
	call_c   Dyam_Create_Unary(&ref[18],&ref[162])
	move_ret ref[163]
	c_ret

;; TERM 162: pgm_to_lpda(grammar_rule(rcg, (_B --> _C)), _D)
c_code local build_ref_162
	ret_reg &ref[162]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_326()
	call_c   Dyam_Create_Binary(&ref[326],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_325()
	call_c   build_ref_304()
	call_c   Dyam_Create_Binary(&ref[325],&ref[304],R(0))
	move_ret R(0)
	call_c   build_ref_324()
	call_c   Dyam_Create_Binary(&ref[324],R(0),V(3))
	move_ret ref[162]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 324: pgm_to_lpda
c_code local build_ref_324
	ret_reg &ref[324]
	call_c   Dyam_Create_Atom("pgm_to_lpda")
	move_ret ref[324]
	c_ret

;; TERM 325: grammar_rule
c_code local build_ref_325
	ret_reg &ref[325]
	call_c   Dyam_Create_Atom("grammar_rule")
	move_ret ref[325]
	c_ret

;; TERM 326: -->
c_code local build_ref_326
	ret_reg &ref[326]
	call_c   Dyam_Create_Atom("-->")
	move_ret ref[326]
	c_ret

c_code local build_seed_31
	ret_reg &seed[31]
	call_c   build_ref_18()
	call_c   build_ref_166()
	call_c   Dyam_Seed_Start(&ref[18],&ref[166],I(0),fun3,1)
	call_c   build_ref_167()
	call_c   Dyam_Seed_Add_Comp(&ref[167],&ref[166],0)
	call_c   Dyam_Seed_End()
	move_ret seed[31]
	c_ret

;; TERM 167: '*GUARD*'(rcg_expand(_E, _B, _F, _G, _H, _I)) :> '$$HOLE$$'
c_code local build_ref_167
	ret_reg &ref[167]
	call_c   build_ref_166()
	call_c   Dyam_Create_Binary(I(9),&ref[166],I(7))
	move_ret ref[167]
	c_ret

;; TERM 166: '*GUARD*'(rcg_expand(_E, _B, _F, _G, _H, _I))
c_code local build_ref_166
	ret_reg &ref[166]
	call_c   build_ref_18()
	call_c   build_ref_165()
	call_c   Dyam_Create_Unary(&ref[18],&ref[165])
	move_ret ref[166]
	c_ret

;; TERM 165: rcg_expand(_E, _B, _F, _G, _H, _I)
c_code local build_ref_165
	ret_reg &ref[165]
	call_c   build_ref_316()
	call_c   Dyam_Term_Start(&ref[316],6)
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_End()
	move_ret ref[165]
	c_ret

;; TERM 197: '$CLOSURE'('$fun'(57, 0, 1087989324), '$TUPPLE'(35080023574004))
c_code local build_ref_197
	ret_reg &ref[197]
	call_c   build_ref_196()
	call_c   Dyam_Closure_Aux(fun57,&ref[196])
	move_ret ref[197]
	c_ret

;; TERM 190: '*FIRST*'(_N) :> _K
c_code local build_ref_190
	ret_reg &ref[190]
	call_c   build_ref_189()
	call_c   Dyam_Create_Binary(I(9),&ref[189],V(10))
	move_ret ref[190]
	c_ret

;; TERM 189: '*FIRST*'(_N)
c_code local build_ref_189
	ret_reg &ref[189]
	call_c   build_ref_30()
	call_c   Dyam_Create_Unary(&ref[30],V(13))
	move_ret ref[189]
	c_ret

;; TERM 193: dyalog
c_code local build_ref_193
	ret_reg &ref[193]
	call_c   Dyam_Create_Atom("dyalog")
	move_ret ref[193]
	c_ret

;; TERM 194: '*LAST*'(_O)
c_code local build_ref_194
	ret_reg &ref[194]
	call_c   build_ref_327()
	call_c   Dyam_Create_Unary(&ref[327],V(14))
	move_ret ref[194]
	c_ret

;; TERM 327: '*LAST*'
c_code local build_ref_327
	ret_reg &ref[327]
	call_c   Dyam_Create_Atom("*LAST*")
	move_ret ref[327]
	c_ret

c_code local build_seed_33
	ret_reg &seed[33]
	call_c   build_ref_18()
	call_c   build_ref_177()
	call_c   Dyam_Seed_Start(&ref[18],&ref[177],I(0),fun3,1)
	call_c   build_ref_178()
	call_c   Dyam_Seed_Add_Comp(&ref[178],&ref[177],0)
	call_c   Dyam_Seed_End()
	move_ret seed[33]
	c_ret

;; TERM 178: '*GUARD*'(rcg_body_to_lpda(_E, _C, _M, _P, _L)) :> '$$HOLE$$'
c_code local build_ref_178
	ret_reg &ref[178]
	call_c   build_ref_177()
	call_c   Dyam_Create_Binary(I(9),&ref[177],I(7))
	move_ret ref[178]
	c_ret

;; TERM 177: '*GUARD*'(rcg_body_to_lpda(_E, _C, _M, _P, _L))
c_code local build_ref_177
	ret_reg &ref[177]
	call_c   build_ref_18()
	call_c   build_ref_176()
	call_c   Dyam_Create_Unary(&ref[18],&ref[176])
	move_ret ref[177]
	c_ret

;; TERM 176: rcg_body_to_lpda(_E, _C, _M, _P, _L)
c_code local build_ref_176
	ret_reg &ref[176]
	call_c   build_ref_328()
	call_c   Dyam_Term_Start(&ref[328],5)
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_End()
	move_ret ref[176]
	c_ret

;; TERM 328: rcg_body_to_lpda
c_code local build_ref_328
	ret_reg &ref[328]
	call_c   Dyam_Create_Atom("rcg_body_to_lpda")
	move_ret ref[328]
	c_ret

c_code local build_seed_34
	ret_reg &seed[34]
	call_c   build_ref_18()
	call_c   build_ref_180()
	call_c   Dyam_Seed_Start(&ref[18],&ref[180],I(0),fun3,1)
	call_c   build_ref_181()
	call_c   Dyam_Seed_Add_Comp(&ref[181],&ref[180],0)
	call_c   Dyam_Seed_End()
	move_ret seed[34]
	c_ret

;; TERM 181: '*GUARD*'(body_to_lpda(_E, _I, _P, _K, _L)) :> '$$HOLE$$'
c_code local build_ref_181
	ret_reg &ref[181]
	call_c   build_ref_180()
	call_c   Dyam_Create_Binary(I(9),&ref[180],I(7))
	move_ret ref[181]
	c_ret

;; TERM 180: '*GUARD*'(body_to_lpda(_E, _I, _P, _K, _L))
c_code local build_ref_180
	ret_reg &ref[180]
	call_c   build_ref_18()
	call_c   build_ref_179()
	call_c   Dyam_Create_Unary(&ref[18],&ref[179])
	move_ret ref[180]
	c_ret

;; TERM 179: body_to_lpda(_E, _I, _P, _K, _L)
c_code local build_ref_179
	ret_reg &ref[179]
	call_c   build_ref_329()
	call_c   Dyam_Term_Start(&ref[329],5)
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_End()
	move_ret ref[179]
	c_ret

;; TERM 329: body_to_lpda
c_code local build_ref_329
	ret_reg &ref[329]
	call_c   Dyam_Create_Atom("body_to_lpda")
	move_ret ref[329]
	c_ret

;; TERM 182: rcg_assign(_E, _Q, _R, _S)
c_code local build_ref_182
	ret_reg &ref[182]
	call_c   build_ref_330()
	call_c   Dyam_Term_Start(&ref[330],4)
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_End()
	move_ret ref[182]
	c_ret

;; TERM 330: rcg_assign
c_code local build_ref_330
	ret_reg &ref[330]
	call_c   Dyam_Create_Atom("rcg_assign")
	move_ret ref[330]
	c_ret

long local pool_fun55[4]=[3,build_seed_33,build_seed_34,build_ref_182]

long local pool_fun56[4]=[65538,build_ref_193,build_ref_194,pool_fun55]

pl_code local fun56
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(11),&ref[193])
	fail_ret
	call_c   Dyam_Unify(V(12),&ref[194])
	fail_ret
fun55:
	pl_call  fun4(&seed[33],1)
	pl_call  fun4(&seed[34],1)
	call_c   DYAM_evpred_retract(&ref[182])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret


;; TERM 191: light_tabular _F
c_code local build_ref_191
	ret_reg &ref[191]
	call_c   build_ref_322()
	call_c   Dyam_Create_Unary(&ref[322],V(5))
	move_ret ref[191]
	c_ret

;; TERM 174: rec_prolog
c_code local build_ref_174
	ret_reg &ref[174]
	call_c   Dyam_Create_Atom("rec_prolog")
	move_ret ref[174]
	c_ret

;; TERM 192: '*LIGHTLAST*'(_O)
c_code local build_ref_192
	ret_reg &ref[192]
	call_c   build_ref_331()
	call_c   Dyam_Create_Unary(&ref[331],V(14))
	move_ret ref[192]
	c_ret

;; TERM 331: '*LIGHTLAST*'
c_code local build_ref_331
	ret_reg &ref[331]
	call_c   Dyam_Create_Atom("*LIGHTLAST*")
	move_ret ref[331]
	c_ret

long local pool_fun57[7]=[131076,build_ref_190,build_ref_191,build_ref_174,build_ref_192,pool_fun56,pool_fun55]

pl_code local fun57
	call_c   Dyam_Pool(pool_fun57)
	call_c   Dyam_Unify(V(3),&ref[190])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun56)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[191])
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(11),&ref[174])
	fail_ret
	call_c   Dyam_Unify(V(12),&ref[192])
	fail_ret
	pl_jump  fun55()

;; TERM 196: '$TUPPLE'(35080023574004)
c_code local build_ref_196
	ret_reg &ref[196]
	call_c   Dyam_Create_Simple_Tupple(0,126926848)
	move_ret ref[196]
	c_ret

long local pool_fun58[2]=[1,build_ref_197]

pl_code local fun58
	call_c   Dyam_Remove_Choice()
	move     &ref[197], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Load(4,V(5))
	call_c   Dyam_Reg_Load(6,V(6))
	call_c   Dyam_Reg_Load(8,V(7))
	move     V(13), R(10)
	move     S(5), R(11)
	move     V(14), R(12)
	move     S(5), R(13)
	call_c   Dyam_Reg_Deallocate(7)
	pl_jump  fun49()

;; TERM 183: prolog _F
c_code local build_ref_183
	ret_reg &ref[183]
	call_c   build_ref_187()
	call_c   Dyam_Create_Unary(&ref[187],V(5))
	move_ret ref[183]
	c_ret

;; TERM 187: prolog
c_code local build_ref_187
	ret_reg &ref[187]
	call_c   Dyam_Create_Atom("prolog")
	move_ret ref[187]
	c_ret

c_code local build_seed_32
	ret_reg &seed[32]
	call_c   build_ref_18()
	call_c   build_ref_170()
	call_c   Dyam_Seed_Start(&ref[18],&ref[170],I(0),fun3,1)
	call_c   build_ref_171()
	call_c   Dyam_Seed_Add_Comp(&ref[171],&ref[170],0)
	call_c   Dyam_Seed_End()
	move_ret seed[32]
	c_ret

;; TERM 171: '*GUARD*'(rcg_prolog_make(_F, _G, _H, _J)) :> '$$HOLE$$'
c_code local build_ref_171
	ret_reg &ref[171]
	call_c   build_ref_170()
	call_c   Dyam_Create_Binary(I(9),&ref[170],I(7))
	move_ret ref[171]
	c_ret

;; TERM 170: '*GUARD*'(rcg_prolog_make(_F, _G, _H, _J))
c_code local build_ref_170
	ret_reg &ref[170]
	call_c   build_ref_18()
	call_c   build_ref_169()
	call_c   Dyam_Create_Unary(&ref[18],&ref[169])
	move_ret ref[170]
	c_ret

;; TERM 169: rcg_prolog_make(_F, _G, _H, _J)
c_code local build_ref_169
	ret_reg &ref[169]
	call_c   build_ref_303()
	call_c   Dyam_Term_Start(&ref[303],4)
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret ref[169]
	c_ret

;; TERM 186: '*PROLOG-FIRST*'(_J) :> _K
c_code local build_ref_186
	ret_reg &ref[186]
	call_c   build_ref_185()
	call_c   Dyam_Create_Binary(I(9),&ref[185],V(10))
	move_ret ref[186]
	c_ret

;; TERM 185: '*PROLOG-FIRST*'(_J)
c_code local build_ref_185
	ret_reg &ref[185]
	call_c   build_ref_184()
	call_c   Dyam_Create_Unary(&ref[184],V(9))
	move_ret ref[185]
	c_ret

;; TERM 184: '*PROLOG-FIRST*'
c_code local build_ref_184
	ret_reg &ref[184]
	call_c   Dyam_Create_Atom("*PROLOG-FIRST*")
	move_ret ref[184]
	c_ret

long local pool_fun59[8]=[131077,build_ref_183,build_seed_32,build_ref_186,build_ref_187,build_ref_188,pool_fun58,pool_fun55]

pl_code local fun59
	call_c   Dyam_Update_Choice(fun58)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[183])
	call_c   Dyam_Cut()
	pl_call  fun4(&seed[32],1)
	call_c   Dyam_Unify(V(3),&ref[186])
	fail_ret
	call_c   Dyam_Unify(V(11),&ref[187])
	fail_ret
	call_c   Dyam_Unify(V(12),&ref[188])
	fail_ret
	pl_jump  fun55()

;; TERM 168: rec_prolog _F
c_code local build_ref_168
	ret_reg &ref[168]
	call_c   build_ref_174()
	call_c   Dyam_Create_Unary(&ref[174],V(5))
	move_ret ref[168]
	c_ret

;; TERM 173: '*GUARD*'(_J) :> _K
c_code local build_ref_173
	ret_reg &ref[173]
	call_c   build_ref_172()
	call_c   Dyam_Create_Binary(I(9),&ref[172],V(10))
	move_ret ref[173]
	c_ret

;; TERM 172: '*GUARD*'(_J)
c_code local build_ref_172
	ret_reg &ref[172]
	call_c   build_ref_18()
	call_c   Dyam_Create_Unary(&ref[18],V(9))
	move_ret ref[172]
	c_ret

;; TERM 175: noop
c_code local build_ref_175
	ret_reg &ref[175]
	call_c   Dyam_Create_Atom("noop")
	move_ret ref[175]
	c_ret

long local pool_fun60[10]=[131079,build_ref_163,build_seed_31,build_ref_168,build_seed_32,build_ref_173,build_ref_174,build_ref_175,pool_fun59,pool_fun55]

pl_code local fun60
	call_c   Dyam_Pool(pool_fun60)
	call_c   Dyam_Unify_Item(&ref[163])
	fail_ret
	call_c   Dyam_Allocate(0)
	pl_call  fun4(&seed[31],1)
	call_c   Dyam_Choice(fun59)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[168])
	call_c   Dyam_Cut()
	pl_call  fun4(&seed[32],1)
	call_c   Dyam_Unify(V(3),&ref[173])
	fail_ret
	call_c   Dyam_Unify(V(11),&ref[174])
	fail_ret
	call_c   Dyam_Unify(V(12),&ref[175])
	fail_ret
	pl_jump  fun55()

;; TERM 164: '*GUARD*'(pgm_to_lpda(grammar_rule(rcg, (_B --> _C)), _D)) :> []
c_code local build_ref_164
	ret_reg &ref[164]
	call_c   build_ref_163()
	call_c   Dyam_Create_Binary(I(9),&ref[163],I(0))
	move_ret ref[164]
	c_ret

c_code local build_seed_9
	ret_reg &seed[9]
	call_c   build_ref_17()
	call_c   build_ref_212()
	call_c   Dyam_Seed_Start(&ref[17],&ref[212],I(0),fun1,1)
	call_c   build_ref_211()
	call_c   Dyam_Seed_Add_Comp(&ref[211],fun90,0)
	call_c   Dyam_Seed_End()
	move_ret seed[9]
	c_ret

;; TERM 211: '*GUARD*'(rcg_non_terminal_to_lpda(_B, _C, _D, _E, _F))
c_code local build_ref_211
	ret_reg &ref[211]
	call_c   build_ref_18()
	call_c   build_ref_210()
	call_c   Dyam_Create_Unary(&ref[18],&ref[210])
	move_ret ref[211]
	c_ret

;; TERM 210: rcg_non_terminal_to_lpda(_B, _C, _D, _E, _F)
c_code local build_ref_210
	ret_reg &ref[210]
	call_c   build_ref_332()
	call_c   Dyam_Term_Start(&ref[332],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[210]
	c_ret

;; TERM 332: rcg_non_terminal_to_lpda
c_code local build_ref_332
	ret_reg &ref[332]
	call_c   Dyam_Create_Atom("rcg_non_terminal_to_lpda")
	move_ret ref[332]
	c_ret

c_code local build_seed_35
	ret_reg &seed[35]
	call_c   build_ref_18()
	call_c   build_ref_214()
	call_c   Dyam_Seed_Start(&ref[18],&ref[214],I(0),fun3,1)
	call_c   build_ref_215()
	call_c   Dyam_Seed_Add_Comp(&ref[215],&ref[214],0)
	call_c   Dyam_Seed_End()
	move_ret seed[35]
	c_ret

;; TERM 215: '*GUARD*'(rcg_expand(_B, _H, _I, _J, _K, _L)) :> '$$HOLE$$'
c_code local build_ref_215
	ret_reg &ref[215]
	call_c   build_ref_214()
	call_c   Dyam_Create_Binary(I(9),&ref[214],I(7))
	move_ret ref[215]
	c_ret

;; TERM 214: '*GUARD*'(rcg_expand(_B, _H, _I, _J, _K, _L))
c_code local build_ref_214
	ret_reg &ref[214]
	call_c   build_ref_18()
	call_c   build_ref_213()
	call_c   Dyam_Create_Unary(&ref[18],&ref[213])
	move_ret ref[214]
	c_ret

;; TERM 213: rcg_expand(_B, _H, _I, _J, _K, _L)
c_code local build_ref_213
	ret_reg &ref[213]
	call_c   build_ref_316()
	call_c   Dyam_Term_Start(&ref[316],6)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_End()
	move_ret ref[213]
	c_ret

;; TERM 257: '$CLOSURE'('$fun'(85, 0, 1088206100), '$TUPPLE'(35080023575276))
c_code local build_ref_257
	ret_reg &ref[257]
	call_c   build_ref_256()
	call_c   Dyam_Closure_Aux(fun85,&ref[256])
	move_ret ref[257]
	c_ret

;; TERM 254: '$CLOSURE'('$fun'(83, 0, 1088206688), '$TUPPLE'(35080023574784))
c_code local build_ref_254
	ret_reg &ref[254]
	call_c   build_ref_235()
	call_c   Dyam_Closure_Aux(fun83,&ref[235])
	move_ret ref[254]
	c_ret

;; TERM 252: '$CLOSURE'('$fun'(82, 0, 1088196088), '$TUPPLE'(35080023574784))
c_code local build_ref_252
	ret_reg &ref[252]
	call_c   build_ref_235()
	call_c   Dyam_Closure_Aux(fun82,&ref[235])
	move_ret ref[252]
	c_ret

;; TERM 250: '*WRAPPER-CALL*'(_I, _O, _D)
c_code local build_ref_250
	ret_reg &ref[250]
	call_c   build_ref_333()
	call_c   Dyam_Term_Start(&ref[333],3)
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[250]
	c_ret

;; TERM 333: '*WRAPPER-CALL*'
c_code local build_ref_333
	ret_reg &ref[333]
	call_c   Dyam_Create_Atom("*WRAPPER-CALL*")
	move_ret ref[333]
	c_ret

c_code local build_seed_37
	ret_reg &seed[37]
	call_c   build_ref_18()
	call_c   build_ref_223()
	call_c   Dyam_Seed_Start(&ref[18],&ref[223],I(0),fun3,1)
	call_c   build_ref_224()
	call_c   Dyam_Seed_Add_Comp(&ref[224],&ref[223],0)
	call_c   Dyam_Seed_End()
	move_ret seed[37]
	c_ret

;; TERM 224: '*GUARD*'(body_to_lpda(_B, _L, _N, _E, dyalog)) :> '$$HOLE$$'
c_code local build_ref_224
	ret_reg &ref[224]
	call_c   build_ref_223()
	call_c   Dyam_Create_Binary(I(9),&ref[223],I(7))
	move_ret ref[224]
	c_ret

;; TERM 223: '*GUARD*'(body_to_lpda(_B, _L, _N, _E, dyalog))
c_code local build_ref_223
	ret_reg &ref[223]
	call_c   build_ref_18()
	call_c   build_ref_222()
	call_c   Dyam_Create_Unary(&ref[18],&ref[222])
	move_ret ref[223]
	c_ret

;; TERM 222: body_to_lpda(_B, _L, _N, _E, dyalog)
c_code local build_ref_222
	ret_reg &ref[222]
	call_c   build_ref_329()
	call_c   build_ref_193()
	call_c   Dyam_Term_Start(&ref[329],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(&ref[193])
	call_c   Dyam_Term_End()
	move_ret ref[222]
	c_ret

long local pool_fun70[2]=[1,build_seed_37]

long local pool_fun82[3]=[65537,build_ref_250,pool_fun70]

pl_code local fun82
	call_c   Dyam_Pool(pool_fun82)
	call_c   Dyam_Unify(V(13),&ref[250])
	fail_ret
	call_c   Dyam_Allocate(0)
fun70:
	call_c   Dyam_Deallocate()
	pl_jump  fun4(&seed[37],1)


;; TERM 235: '$TUPPLE'(35080023574784)
c_code local build_ref_235
	ret_reg &ref[235]
	call_c   Dyam_Create_Simple_Tupple(0,185745408)
	move_ret ref[235]
	c_ret

long local pool_fun83[2]=[1,build_ref_252]

pl_code local fun83
	call_c   Dyam_Pool(pool_fun83)
	call_c   Dyam_Allocate(0)
	move     &ref[252], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Load(4,V(8))
	call_c   Dyam_Reg_Deallocate(3)
fun73:
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Reg_Bind(2,V(3))
	call_c   Dyam_Reg_Bind(4,V(2))
	call_c   Dyam_Choice(fun72)
	pl_call  fun31(&seed[38],1)
	pl_fail


long local pool_fun84[2]=[1,build_ref_254]

pl_code local fun84
	call_c   Dyam_Remove_Choice()
	move     &ref[254], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Load(4,V(8))
	call_c   Dyam_Reg_Load(6,V(9))
	call_c   Dyam_Reg_Load(8,V(6))
	call_c   Dyam_Reg_Load(10,V(10))
	move     V(14), R(12)
	move     S(5), R(13)
	call_c   Dyam_Reg_Deallocate(7)
	pl_jump  fun44()

;; TERM 247: lco _I
c_code local build_ref_247
	ret_reg &ref[247]
	call_c   build_ref_334()
	call_c   Dyam_Create_Unary(&ref[334],V(8))
	move_ret ref[247]
	c_ret

;; TERM 334: lco
c_code local build_ref_334
	ret_reg &ref[334]
	call_c   Dyam_Create_Atom("lco")
	move_ret ref[334]
	c_ret

;; TERM 248: '*LAST*'(_Q)
c_code local build_ref_248
	ret_reg &ref[248]
	call_c   build_ref_327()
	call_c   Dyam_Create_Unary(&ref[327],V(16))
	move_ret ref[248]
	c_ret

;; TERM 249: '*LASTCALL*'(_P)
c_code local build_ref_249
	ret_reg &ref[249]
	call_c   build_ref_335()
	call_c   Dyam_Create_Unary(&ref[335],V(15))
	move_ret ref[249]
	c_ret

;; TERM 335: '*LASTCALL*'
c_code local build_ref_335
	ret_reg &ref[335]
	call_c   Dyam_Create_Atom("*LASTCALL*")
	move_ret ref[335]
	c_ret

long local pool_fun85[6]=[131075,build_ref_247,build_ref_248,build_ref_249,pool_fun84,pool_fun70]

pl_code local fun85
	call_c   Dyam_Pool(pool_fun85)
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun84)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[247])
	call_c   DYAM_sfol_identical(&ref[248],V(3))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(13),&ref[249])
	fail_ret
	pl_jump  fun70()

;; TERM 256: '$TUPPLE'(35080023575276)
c_code local build_ref_256
	ret_reg &ref[256]
	call_c   Dyam_Create_Simple_Tupple(0,190722048)
	move_ret ref[256]
	c_ret

long local pool_fun86[2]=[1,build_ref_257]

pl_code local fun86
	call_c   Dyam_Remove_Choice()
	move     &ref[257], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Load(4,V(8))
	call_c   Dyam_Reg_Load(6,V(9))
	call_c   Dyam_Reg_Load(8,V(10))
	move     V(15), R(10)
	move     S(5), R(11)
	move     V(16), R(12)
	move     S(5), R(13)
	call_c   Dyam_Reg_Deallocate(7)
	pl_jump  fun49()

;; TERM 240: 'Not a recursive prolog rcg predicate ~w'
c_code local build_ref_240
	ret_reg &ref[240]
	call_c   Dyam_Create_Atom("Not a recursive prolog rcg predicate ~w")
	move_ret ref[240]
	c_ret

;; TERM 241: [_H]
c_code local build_ref_241
	ret_reg &ref[241]
	call_c   Dyam_Create_List(V(7),I(0))
	move_ret ref[241]
	c_ret

long local pool_fun80[4]=[65538,build_ref_240,build_ref_241,pool_fun70]

pl_code local fun81
	call_c   Dyam_Remove_Choice()
fun80:
	call_c   Dyam_Cut()
	move     &ref[240], R(0)
	move     0, R(1)
	move     &ref[241], R(2)
	move     S(5), R(3)
	pl_call  pred_error_2()
	pl_jump  fun70()


;; TERM 246: light_tabular _I
c_code local build_ref_246
	ret_reg &ref[246]
	call_c   build_ref_322()
	call_c   Dyam_Create_Unary(&ref[322],V(8))
	move_ret ref[246]
	c_ret

long local pool_fun87[5]=[131074,build_ref_174,build_ref_246,pool_fun86,pool_fun80]

pl_code local fun87
	call_c   Dyam_Update_Choice(fun86)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(5),&ref[174])
	fail_ret
	call_c   Dyam_Choice(fun81)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[246])
	call_c   Dyam_Cut()
	pl_fail

;; TERM 242: extensional _I
c_code local build_ref_242
	ret_reg &ref[242]
	call_c   build_ref_336()
	call_c   Dyam_Create_Unary(&ref[336],V(8))
	move_ret ref[242]
	c_ret

;; TERM 336: extensional
c_code local build_ref_336
	ret_reg &ref[336]
	call_c   Dyam_Create_Atom("extensional")
	move_ret ref[336]
	c_ret

c_code local build_seed_36
	ret_reg &seed[36]
	call_c   build_ref_18()
	call_c   build_ref_218()
	call_c   Dyam_Seed_Start(&ref[18],&ref[218],I(0),fun3,1)
	call_c   build_ref_219()
	call_c   Dyam_Seed_Add_Comp(&ref[219],&ref[218],0)
	call_c   Dyam_Seed_End()
	move_ret seed[36]
	c_ret

;; TERM 219: '*GUARD*'(rcg_prolog_make(_I, _J, _K, _M)) :> '$$HOLE$$'
c_code local build_ref_219
	ret_reg &ref[219]
	call_c   build_ref_218()
	call_c   Dyam_Create_Binary(I(9),&ref[218],I(7))
	move_ret ref[219]
	c_ret

;; TERM 218: '*GUARD*'(rcg_prolog_make(_I, _J, _K, _M))
c_code local build_ref_218
	ret_reg &ref[218]
	call_c   build_ref_18()
	call_c   build_ref_217()
	call_c   Dyam_Create_Unary(&ref[18],&ref[217])
	move_ret ref[218]
	c_ret

;; TERM 217: rcg_prolog_make(_I, _J, _K, _M)
c_code local build_ref_217
	ret_reg &ref[217]
	call_c   build_ref_303()
	call_c   Dyam_Term_Start(&ref[303],4)
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_End()
	move_ret ref[217]
	c_ret

c_code local build_seed_40
	ret_reg &seed[40]
	call_c   build_ref_18()
	call_c   build_ref_244()
	call_c   Dyam_Seed_Start(&ref[18],&ref[244],I(0),fun3,1)
	call_c   build_ref_245()
	call_c   Dyam_Seed_Add_Comp(&ref[245],&ref[244],0)
	call_c   Dyam_Seed_End()
	move_ret seed[40]
	c_ret

;; TERM 245: '*GUARD*'(litteral_to_lpda(_B, recorded(_M), _D, _N, _F)) :> '$$HOLE$$'
c_code local build_ref_245
	ret_reg &ref[245]
	call_c   build_ref_244()
	call_c   Dyam_Create_Binary(I(9),&ref[244],I(7))
	move_ret ref[245]
	c_ret

;; TERM 244: '*GUARD*'(litteral_to_lpda(_B, recorded(_M), _D, _N, _F))
c_code local build_ref_244
	ret_reg &ref[244]
	call_c   build_ref_18()
	call_c   build_ref_243()
	call_c   Dyam_Create_Unary(&ref[18],&ref[243])
	move_ret ref[244]
	c_ret

;; TERM 243: litteral_to_lpda(_B, recorded(_M), _D, _N, _F)
c_code local build_ref_243
	ret_reg &ref[243]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_338()
	call_c   Dyam_Create_Unary(&ref[338],V(12))
	move_ret R(0)
	call_c   build_ref_337()
	call_c   Dyam_Term_Start(&ref[337],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[243]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 337: litteral_to_lpda
c_code local build_ref_337
	ret_reg &ref[337]
	call_c   Dyam_Create_Atom("litteral_to_lpda")
	move_ret ref[337]
	c_ret

;; TERM 338: recorded
c_code local build_ref_338
	ret_reg &ref[338]
	call_c   Dyam_Create_Atom("recorded")
	move_ret ref[338]
	c_ret

long local pool_fun88[6]=[131075,build_ref_242,build_seed_36,build_seed_40,pool_fun87,pool_fun70]

pl_code local fun88
	call_c   Dyam_Update_Choice(fun87)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[242])
	call_c   Dyam_Cut()
	pl_call  fun4(&seed[36],1)
	pl_call  fun4(&seed[40],1)
	pl_jump  fun70()

;; TERM 225: prolog _I
c_code local build_ref_225
	ret_reg &ref[225]
	call_c   build_ref_187()
	call_c   Dyam_Create_Unary(&ref[187],V(8))
	move_ret ref[225]
	c_ret

;; TERM 239: '$CLOSURE'('$fun'(75, 0, 1088146816), '$TUPPLE'(35080023574784))
c_code local build_ref_239
	ret_reg &ref[239]
	call_c   build_ref_235()
	call_c   Dyam_Closure_Aux(fun75,&ref[235])
	move_ret ref[239]
	c_ret

;; TERM 236: '$CLOSURE'('$fun'(74, 0, 1088139684), '$TUPPLE'(35080023574784))
c_code local build_ref_236
	ret_reg &ref[236]
	call_c   build_ref_235()
	call_c   Dyam_Closure_Aux(fun74,&ref[235])
	move_ret ref[236]
	c_ret

;; TERM 233: '*WRAPPER-CALL*'(rcg_prolog(_I), _O, _D)
c_code local build_ref_233
	ret_reg &ref[233]
	call_c   build_ref_333()
	call_c   build_ref_237()
	call_c   Dyam_Term_Start(&ref[333],3)
	call_c   Dyam_Term_Arg(&ref[237])
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[233]
	c_ret

;; TERM 237: rcg_prolog(_I)
c_code local build_ref_237
	ret_reg &ref[237]
	call_c   build_ref_318()
	call_c   Dyam_Create_Unary(&ref[318],V(8))
	move_ret ref[237]
	c_ret

long local pool_fun74[3]=[65537,build_ref_233,pool_fun70]

pl_code local fun74
	call_c   Dyam_Pool(pool_fun74)
	call_c   Dyam_Unify(V(13),&ref[233])
	fail_ret
	call_c   Dyam_Allocate(0)
	pl_jump  fun70()

long local pool_fun75[3]=[2,build_ref_236,build_ref_237]

pl_code local fun75
	call_c   Dyam_Pool(pool_fun75)
	call_c   Dyam_Allocate(0)
	move     &ref[236], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	move     &ref[237], R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  fun73()

long local pool_fun76[2]=[1,build_ref_239]

long local pool_fun79[4]=[65538,build_ref_240,build_ref_241,pool_fun76]

pl_code local fun79
	call_c   Dyam_Remove_Choice()
	move     &ref[240], R(0)
	move     0, R(1)
	move     &ref[241], R(2)
	move     S(5), R(3)
	pl_call  pred_error_2()
fun76:
	move     &ref[239], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Load(4,V(8))
	call_c   Dyam_Reg_Load(6,V(9))
	call_c   Dyam_Reg_Load(8,V(6))
	call_c   Dyam_Reg_Load(10,V(10))
	move     V(14), R(12)
	move     S(5), R(13)
	call_c   Dyam_Reg_Deallocate(7)
	pl_jump  fun44()


pl_code local fun78
	call_c   Dyam_Remove_Choice()
fun77:
	call_c   Dyam_Cut()
	pl_jump  fun76()


long local pool_fun89[6]=[196610,build_ref_225,build_ref_174,pool_fun88,pool_fun79,pool_fun76]

pl_code local fun89
	call_c   Dyam_Update_Choice(fun88)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[225])
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun79)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Choice(fun78)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(5),&ref[174])
	fail_ret
	call_c   Dyam_Cut()
	pl_fail

;; TERM 216: rec_prolog _I
c_code local build_ref_216
	ret_reg &ref[216]
	call_c   build_ref_174()
	call_c   Dyam_Create_Unary(&ref[174],V(8))
	move_ret ref[216]
	c_ret

;; TERM 221: '*GUARD*'(_M) :> _D
c_code local build_ref_221
	ret_reg &ref[221]
	call_c   build_ref_220()
	call_c   Dyam_Create_Binary(I(9),&ref[220],V(3))
	move_ret ref[221]
	c_ret

;; TERM 220: '*GUARD*'(_M)
c_code local build_ref_220
	ret_reg &ref[220]
	call_c   build_ref_18()
	call_c   Dyam_Create_Unary(&ref[18],V(12))
	move_ret ref[220]
	c_ret

long local pool_fun90[8]=[131077,build_ref_211,build_seed_35,build_ref_216,build_seed_36,build_ref_221,pool_fun89,pool_fun70]

pl_code local fun90
	call_c   Dyam_Pool(pool_fun90)
	call_c   Dyam_Unify_Item(&ref[211])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     V(6), R(0)
	move     S(5), R(1)
	move     V(7), R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Load(4,V(2))
	pl_call  pred_xtagop_3()
	pl_call  fun4(&seed[35],1)
	call_c   Dyam_Choice(fun89)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[216])
	call_c   Dyam_Cut()
	pl_call  fun4(&seed[36],1)
	call_c   Dyam_Unify(V(13),&ref[221])
	fail_ret
	pl_jump  fun70()

;; TERM 212: '*GUARD*'(rcg_non_terminal_to_lpda(_B, _C, _D, _E, _F)) :> []
c_code local build_ref_212
	ret_reg &ref[212]
	call_c   build_ref_211()
	call_c   Dyam_Create_Binary(I(9),&ref[211],I(0))
	move_ret ref[212]
	c_ret

c_code local build_seed_10
	ret_reg &seed[10]
	call_c   build_ref_17()
	call_c   build_ref_260()
	call_c   Dyam_Seed_Start(&ref[17],&ref[260],I(0),fun1,1)
	call_c   build_ref_259()
	call_c   Dyam_Seed_Add_Comp(&ref[259],fun104,0)
	call_c   Dyam_Seed_End()
	move_ret seed[10]
	c_ret

;; TERM 259: '*GUARD*'(rcg_body_to_lpda(_B, _C, _D, _E, _F))
c_code local build_ref_259
	ret_reg &ref[259]
	call_c   build_ref_18()
	call_c   build_ref_258()
	call_c   Dyam_Create_Unary(&ref[18],&ref[258])
	move_ret ref[259]
	c_ret

;; TERM 258: rcg_body_to_lpda(_B, _C, _D, _E, _F)
c_code local build_ref_258
	ret_reg &ref[258]
	call_c   build_ref_328()
	call_c   Dyam_Term_Start(&ref[328],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[258]
	c_ret

c_code local build_seed_49
	ret_reg &seed[49]
	call_c   build_ref_18()
	call_c   build_ref_211()
	call_c   Dyam_Seed_Start(&ref[18],&ref[211],I(0),fun3,1)
	call_c   build_ref_301()
	call_c   Dyam_Seed_Add_Comp(&ref[301],&ref[211],0)
	call_c   Dyam_Seed_End()
	move_ret seed[49]
	c_ret

;; TERM 301: '*GUARD*'(rcg_non_terminal_to_lpda(_B, _C, _D, _E, _F)) :> '$$HOLE$$'
c_code local build_ref_301
	ret_reg &ref[301]
	call_c   build_ref_211()
	call_c   Dyam_Create_Binary(I(9),&ref[211],I(7))
	move_ret ref[301]
	c_ret

long local pool_fun94[2]=[1,build_seed_49]

pl_code local fun94
	call_c   Dyam_Remove_Choice()
	pl_call  fun4(&seed[49],1)
fun91:
	call_c   Dyam_Deallocate()
	pl_ret


;; TERM 300: '$answers'(_H)
c_code local build_ref_300
	ret_reg &ref[300]
	call_c   build_ref_339()
	call_c   Dyam_Create_Unary(&ref[339],V(7))
	move_ret ref[300]
	c_ret

;; TERM 339: '$answers'
c_code local build_ref_339
	ret_reg &ref[339]
	call_c   Dyam_Create_Atom("$answers")
	move_ret ref[339]
	c_ret

;; TERM 299: 'Not yet implemented in RCG body ~w'
c_code local build_ref_299
	ret_reg &ref[299]
	call_c   Dyam_Create_Atom("Not yet implemented in RCG body ~w")
	move_ret ref[299]
	c_ret

;; TERM 209: [_C]
c_code local build_ref_209
	ret_reg &ref[209]
	call_c   Dyam_Create_List(V(2),I(0))
	move_ret ref[209]
	c_ret

long local pool_fun92[3]=[2,build_ref_299,build_ref_209]

long local pool_fun93[3]=[65537,build_ref_300,pool_fun92]

pl_code local fun93
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(2),&ref[300])
	fail_ret
fun92:
	call_c   Dyam_Cut()
	move     &ref[299], R(0)
	move     0, R(1)
	move     &ref[209], R(2)
	move     S(5), R(3)
	pl_call  pred_error_2()
	pl_jump  fun91()


;; TERM 298: wait(_P)
c_code local build_ref_298
	ret_reg &ref[298]
	call_c   build_ref_340()
	call_c   Dyam_Create_Unary(&ref[340],V(15))
	move_ret ref[298]
	c_ret

;; TERM 340: wait
c_code local build_ref_340
	ret_reg &ref[340]
	call_c   Dyam_Create_Atom("wait")
	move_ret ref[340]
	c_ret

long local pool_fun95[5]=[196609,build_ref_298,pool_fun94,pool_fun93,pool_fun92]

pl_code local fun95
	call_c   Dyam_Update_Choice(fun94)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Choice(fun93)
	call_c   Dyam_Unify(V(2),&ref[298])
	fail_ret
	pl_jump  fun92()

;; TERM 294: {_N}
c_code local build_ref_294
	ret_reg &ref[294]
	call_c   Dyam_Create_Unary(I(2),V(13))
	move_ret ref[294]
	c_ret

c_code local build_seed_48
	ret_reg &seed[48]
	call_c   build_ref_18()
	call_c   build_ref_296()
	call_c   Dyam_Seed_Start(&ref[18],&ref[296],I(0),fun3,1)
	call_c   build_ref_297()
	call_c   Dyam_Seed_Add_Comp(&ref[297],&ref[296],0)
	call_c   Dyam_Seed_End()
	move_ret seed[48]
	c_ret

;; TERM 297: '*GUARD*'(body_to_lpda(_B, _N, _D, _E, _F)) :> '$$HOLE$$'
c_code local build_ref_297
	ret_reg &ref[297]
	call_c   build_ref_296()
	call_c   Dyam_Create_Binary(I(9),&ref[296],I(7))
	move_ret ref[297]
	c_ret

;; TERM 296: '*GUARD*'(body_to_lpda(_B, _N, _D, _E, _F))
c_code local build_ref_296
	ret_reg &ref[296]
	call_c   build_ref_18()
	call_c   build_ref_295()
	call_c   Dyam_Create_Unary(&ref[18],&ref[295])
	move_ret ref[296]
	c_ret

;; TERM 295: body_to_lpda(_B, _N, _D, _E, _F)
c_code local build_ref_295
	ret_reg &ref[295]
	call_c   build_ref_329()
	call_c   Dyam_Term_Start(&ref[329],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[295]
	c_ret

long local pool_fun96[4]=[65538,build_ref_294,build_seed_48,pool_fun95]

pl_code local fun96
	call_c   Dyam_Update_Choice(fun95)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(2),&ref[294])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(13))
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(14), R(4)
	move     S(5), R(5)
	pl_call  pred_my_numbervars_3()
	pl_call  fun4(&seed[48],1)
	pl_jump  fun91()

long local pool_fun97[3]=[65537,build_ref_293,pool_fun96]

pl_code local fun97
	call_c   Dyam_Update_Choice(fun96)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(2),&ref[293])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(4),V(3))
	fail_ret
	pl_jump  fun91()

;; TERM 291: fail
c_code local build_ref_291
	ret_reg &ref[291]
	call_c   Dyam_Create_Atom("fail")
	move_ret ref[291]
	c_ret

;; TERM 292: fail :> _D
c_code local build_ref_292
	ret_reg &ref[292]
	call_c   build_ref_291()
	call_c   Dyam_Create_Binary(I(9),&ref[291],V(3))
	move_ret ref[292]
	c_ret

long local pool_fun98[4]=[65538,build_ref_291,build_ref_292,pool_fun97]

pl_code local fun98
	call_c   Dyam_Update_Choice(fun97)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(2),&ref[291])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(4),&ref[292])
	fail_ret
	pl_jump  fun91()

;; TERM 285: '*COMMIT*'(_H)
c_code local build_ref_285
	ret_reg &ref[285]
	call_c   build_ref_341()
	call_c   Dyam_Create_Unary(&ref[341],V(7))
	move_ret ref[285]
	c_ret

;; TERM 341: '*COMMIT*'
c_code local build_ref_341
	ret_reg &ref[341]
	call_c   Dyam_Create_Atom("*COMMIT*")
	move_ret ref[341]
	c_ret

c_code local build_seed_47
	ret_reg &seed[47]
	call_c   build_ref_18()
	call_c   build_ref_287()
	call_c   Dyam_Seed_Start(&ref[18],&ref[287],I(0),fun3,1)
	call_c   build_ref_288()
	call_c   Dyam_Seed_Add_Comp(&ref[288],&ref[287],0)
	call_c   Dyam_Seed_End()
	move_ret seed[47]
	c_ret

;; TERM 288: '*GUARD*'(rcg_body_to_lpda(_B, _H, ('*CUT*' :> _D), _M, rec_prolog)) :> '$$HOLE$$'
c_code local build_ref_288
	ret_reg &ref[288]
	call_c   build_ref_287()
	call_c   Dyam_Create_Binary(I(9),&ref[287],I(7))
	move_ret ref[288]
	c_ret

;; TERM 287: '*GUARD*'(rcg_body_to_lpda(_B, _H, ('*CUT*' :> _D), _M, rec_prolog))
c_code local build_ref_287
	ret_reg &ref[287]
	call_c   build_ref_18()
	call_c   build_ref_286()
	call_c   Dyam_Create_Unary(&ref[18],&ref[286])
	move_ret ref[287]
	c_ret

;; TERM 286: rcg_body_to_lpda(_B, _H, ('*CUT*' :> _D), _M, rec_prolog)
c_code local build_ref_286
	ret_reg &ref[286]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_342()
	call_c   Dyam_Create_Binary(I(9),&ref[342],V(3))
	move_ret R(0)
	call_c   build_ref_328()
	call_c   build_ref_174()
	call_c   Dyam_Term_Start(&ref[328],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(&ref[174])
	call_c   Dyam_Term_End()
	move_ret ref[286]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 342: '*CUT*'
c_code local build_ref_342
	ret_reg &ref[342]
	call_c   Dyam_Create_Atom("*CUT*")
	move_ret ref[342]
	c_ret

;; TERM 290: '*SETCUT*' :> _M
c_code local build_ref_290
	ret_reg &ref[290]
	call_c   build_ref_289()
	call_c   Dyam_Create_Binary(I(9),&ref[289],V(12))
	move_ret ref[290]
	c_ret

;; TERM 289: '*SETCUT*'
c_code local build_ref_289
	ret_reg &ref[289]
	call_c   Dyam_Create_Atom("*SETCUT*")
	move_ret ref[289]
	c_ret

long local pool_fun99[5]=[65539,build_ref_285,build_seed_47,build_ref_290,pool_fun98]

pl_code local fun99
	call_c   Dyam_Update_Choice(fun98)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(2),&ref[285])
	fail_ret
	call_c   Dyam_Cut()
	pl_call  fun4(&seed[47],1)
	call_c   Dyam_Unify(V(4),&ref[290])
	fail_ret
	pl_jump  fun91()

;; TERM 278: _H ; _I
c_code local build_ref_278
	ret_reg &ref[278]
	call_c   Dyam_Create_Binary(I(5),V(7),V(8))
	move_ret ref[278]
	c_ret

c_code local build_seed_45
	ret_reg &seed[45]
	call_c   build_ref_18()
	call_c   build_ref_280()
	call_c   Dyam_Seed_Start(&ref[18],&ref[280],I(0),fun3,1)
	call_c   build_ref_281()
	call_c   Dyam_Seed_Add_Comp(&ref[281],&ref[280],0)
	call_c   Dyam_Seed_End()
	move_ret seed[45]
	c_ret

;; TERM 281: '*GUARD*'(rcg_body_to_lpda(_B, _H, _L, _M, _F)) :> '$$HOLE$$'
c_code local build_ref_281
	ret_reg &ref[281]
	call_c   build_ref_280()
	call_c   Dyam_Create_Binary(I(9),&ref[280],I(7))
	move_ret ref[281]
	c_ret

;; TERM 280: '*GUARD*'(rcg_body_to_lpda(_B, _H, _L, _M, _F))
c_code local build_ref_280
	ret_reg &ref[280]
	call_c   build_ref_18()
	call_c   build_ref_279()
	call_c   Dyam_Create_Unary(&ref[18],&ref[279])
	move_ret ref[280]
	c_ret

;; TERM 279: rcg_body_to_lpda(_B, _H, _L, _M, _F)
c_code local build_ref_279
	ret_reg &ref[279]
	call_c   build_ref_328()
	call_c   Dyam_Term_Start(&ref[328],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[279]
	c_ret

c_code local build_seed_46
	ret_reg &seed[46]
	call_c   build_ref_18()
	call_c   build_ref_283()
	call_c   Dyam_Seed_Start(&ref[18],&ref[283],I(0),fun3,1)
	call_c   build_ref_284()
	call_c   Dyam_Seed_Add_Comp(&ref[284],&ref[283],0)
	call_c   Dyam_Seed_End()
	move_ret seed[46]
	c_ret

;; TERM 284: '*GUARD*'(rcg_body_to_lpda(_B, _I, _L, _J, _F)) :> '$$HOLE$$'
c_code local build_ref_284
	ret_reg &ref[284]
	call_c   build_ref_283()
	call_c   Dyam_Create_Binary(I(9),&ref[283],I(7))
	move_ret ref[284]
	c_ret

;; TERM 283: '*GUARD*'(rcg_body_to_lpda(_B, _I, _L, _J, _F))
c_code local build_ref_283
	ret_reg &ref[283]
	call_c   build_ref_18()
	call_c   build_ref_282()
	call_c   Dyam_Create_Unary(&ref[18],&ref[282])
	move_ret ref[283]
	c_ret

;; TERM 282: rcg_body_to_lpda(_B, _I, _L, _J, _F)
c_code local build_ref_282
	ret_reg &ref[282]
	call_c   build_ref_328()
	call_c   Dyam_Term_Start(&ref[328],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[282]
	c_ret

long local pool_fun100[5]=[65539,build_ref_278,build_seed_45,build_seed_46,pool_fun99]

pl_code local fun100
	call_c   Dyam_Update_Choice(fun99)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(2),&ref[278])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(3))
	call_c   Dyam_Reg_Load(2,V(4))
	move     V(11), R(4)
	move     S(5), R(5)
	move     V(12), R(6)
	move     S(5), R(7)
	move     V(9), R(8)
	move     S(5), R(9)
	pl_call  pred_handle_choice_5()
	pl_call  fun4(&seed[45],1)
	pl_call  fun4(&seed[46],1)
	pl_jump  fun91()

;; TERM 274: _H -> _I
c_code local build_ref_274
	ret_reg &ref[274]
	call_c   build_ref_343()
	call_c   Dyam_Create_Binary(&ref[343],V(7),V(8))
	move_ret ref[274]
	c_ret

;; TERM 343: ->
c_code local build_ref_343
	ret_reg &ref[343]
	call_c   Dyam_Create_Atom("->")
	move_ret ref[343]
	c_ret

c_code local build_seed_44
	ret_reg &seed[44]
	call_c   build_ref_18()
	call_c   build_ref_276()
	call_c   Dyam_Seed_Start(&ref[18],&ref[276],I(0),fun3,1)
	call_c   build_ref_277()
	call_c   Dyam_Seed_Add_Comp(&ref[277],&ref[276],0)
	call_c   Dyam_Seed_End()
	move_ret seed[44]
	c_ret

;; TERM 277: '*GUARD*'(rcg_body_to_lpda(_B, (_H -> _I ; fail), _D, _E, _F)) :> '$$HOLE$$'
c_code local build_ref_277
	ret_reg &ref[277]
	call_c   build_ref_276()
	call_c   Dyam_Create_Binary(I(9),&ref[276],I(7))
	move_ret ref[277]
	c_ret

;; TERM 276: '*GUARD*'(rcg_body_to_lpda(_B, (_H -> _I ; fail), _D, _E, _F))
c_code local build_ref_276
	ret_reg &ref[276]
	call_c   build_ref_18()
	call_c   build_ref_275()
	call_c   Dyam_Create_Unary(&ref[18],&ref[275])
	move_ret ref[276]
	c_ret

;; TERM 275: rcg_body_to_lpda(_B, (_H -> _I ; fail), _D, _E, _F)
c_code local build_ref_275
	ret_reg &ref[275]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_274()
	call_c   build_ref_291()
	call_c   Dyam_Create_Binary(I(5),&ref[274],&ref[291])
	move_ret R(0)
	call_c   build_ref_328()
	call_c   Dyam_Term_Start(&ref[328],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[275]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun101[4]=[65538,build_ref_274,build_seed_44,pool_fun100]

pl_code local fun101
	call_c   Dyam_Update_Choice(fun100)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(2),&ref[274])
	fail_ret
	call_c   Dyam_Cut()
	pl_call  fun4(&seed[44],1)
	pl_jump  fun91()

;; TERM 270: _H -> _I ; _K
c_code local build_ref_270
	ret_reg &ref[270]
	call_c   build_ref_274()
	call_c   Dyam_Create_Binary(I(5),&ref[274],V(10))
	move_ret ref[270]
	c_ret

c_code local build_seed_43
	ret_reg &seed[43]
	call_c   build_ref_18()
	call_c   build_ref_272()
	call_c   Dyam_Seed_Start(&ref[18],&ref[272],I(0),fun3,1)
	call_c   build_ref_273()
	call_c   Dyam_Seed_Add_Comp(&ref[273],&ref[272],0)
	call_c   Dyam_Seed_End()
	move_ret seed[43]
	c_ret

;; TERM 273: '*GUARD*'(rcg_body_to_lpda(_B, ('*COMMIT*'(_H) , _I ; _K), _D, _E, _F)) :> '$$HOLE$$'
c_code local build_ref_273
	ret_reg &ref[273]
	call_c   build_ref_272()
	call_c   Dyam_Create_Binary(I(9),&ref[272],I(7))
	move_ret ref[273]
	c_ret

;; TERM 272: '*GUARD*'(rcg_body_to_lpda(_B, ('*COMMIT*'(_H) , _I ; _K), _D, _E, _F))
c_code local build_ref_272
	ret_reg &ref[272]
	call_c   build_ref_18()
	call_c   build_ref_271()
	call_c   Dyam_Create_Unary(&ref[18],&ref[271])
	move_ret ref[272]
	c_ret

;; TERM 271: rcg_body_to_lpda(_B, ('*COMMIT*'(_H) , _I ; _K), _D, _E, _F)
c_code local build_ref_271
	ret_reg &ref[271]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_285()
	call_c   Dyam_Create_Binary(I(4),&ref[285],V(8))
	move_ret R(0)
	call_c   Dyam_Create_Binary(I(5),R(0),V(10))
	move_ret R(0)
	call_c   build_ref_328()
	call_c   Dyam_Term_Start(&ref[328],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[271]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun102[4]=[65538,build_ref_270,build_seed_43,pool_fun101]

pl_code local fun102
	call_c   Dyam_Update_Choice(fun101)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(2),&ref[270])
	fail_ret
	call_c   Dyam_Cut()
	pl_call  fun4(&seed[43],1)
	pl_jump  fun91()

;; TERM 263: _H , _I
c_code local build_ref_263
	ret_reg &ref[263]
	call_c   Dyam_Create_Binary(I(4),V(7),V(8))
	move_ret ref[263]
	c_ret

c_code local build_seed_41
	ret_reg &seed[41]
	call_c   build_ref_18()
	call_c   build_ref_265()
	call_c   Dyam_Seed_Start(&ref[18],&ref[265],I(0),fun3,1)
	call_c   build_ref_266()
	call_c   Dyam_Seed_Add_Comp(&ref[266],&ref[265],0)
	call_c   Dyam_Seed_End()
	move_ret seed[41]
	c_ret

;; TERM 266: '*GUARD*'(rcg_body_to_lpda(_B, _I, _D, _J, _F)) :> '$$HOLE$$'
c_code local build_ref_266
	ret_reg &ref[266]
	call_c   build_ref_265()
	call_c   Dyam_Create_Binary(I(9),&ref[265],I(7))
	move_ret ref[266]
	c_ret

;; TERM 265: '*GUARD*'(rcg_body_to_lpda(_B, _I, _D, _J, _F))
c_code local build_ref_265
	ret_reg &ref[265]
	call_c   build_ref_18()
	call_c   build_ref_264()
	call_c   Dyam_Create_Unary(&ref[18],&ref[264])
	move_ret ref[265]
	c_ret

;; TERM 264: rcg_body_to_lpda(_B, _I, _D, _J, _F)
c_code local build_ref_264
	ret_reg &ref[264]
	call_c   build_ref_328()
	call_c   Dyam_Term_Start(&ref[328],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[264]
	c_ret

c_code local build_seed_42
	ret_reg &seed[42]
	call_c   build_ref_18()
	call_c   build_ref_268()
	call_c   Dyam_Seed_Start(&ref[18],&ref[268],I(0),fun3,1)
	call_c   build_ref_269()
	call_c   Dyam_Seed_Add_Comp(&ref[269],&ref[268],0)
	call_c   Dyam_Seed_End()
	move_ret seed[42]
	c_ret

;; TERM 269: '*GUARD*'(rcg_body_to_lpda(_B, _H, _J, _E, _F)) :> '$$HOLE$$'
c_code local build_ref_269
	ret_reg &ref[269]
	call_c   build_ref_268()
	call_c   Dyam_Create_Binary(I(9),&ref[268],I(7))
	move_ret ref[269]
	c_ret

;; TERM 268: '*GUARD*'(rcg_body_to_lpda(_B, _H, _J, _E, _F))
c_code local build_ref_268
	ret_reg &ref[268]
	call_c   build_ref_18()
	call_c   build_ref_267()
	call_c   Dyam_Create_Unary(&ref[18],&ref[267])
	move_ret ref[268]
	c_ret

;; TERM 267: rcg_body_to_lpda(_B, _H, _J, _E, _F)
c_code local build_ref_267
	ret_reg &ref[267]
	call_c   build_ref_328()
	call_c   Dyam_Term_Start(&ref[328],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[267]
	c_ret

long local pool_fun103[5]=[65539,build_ref_263,build_seed_41,build_seed_42,pool_fun102]

pl_code local fun103
	call_c   Dyam_Update_Choice(fun102)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(2),&ref[263])
	fail_ret
	call_c   Dyam_Cut()
	pl_call  fun4(&seed[41],1)
	pl_call  fun4(&seed[42],1)
	pl_jump  fun91()

;; TERM 261: _W41778057
c_code local build_ref_261
	ret_reg &ref[261]
	call_c   Dyam_Create_Unary(I(6),V(6))
	move_ret ref[261]
	c_ret

;; TERM 262: 'Not yet implemented ~w'
c_code local build_ref_262
	ret_reg &ref[262]
	call_c   Dyam_Create_Atom("Not yet implemented ~w")
	move_ret ref[262]
	c_ret

long local pool_fun104[6]=[65540,build_ref_259,build_ref_261,build_ref_262,build_ref_209,pool_fun103]

pl_code local fun104
	call_c   Dyam_Pool(pool_fun104)
	call_c   Dyam_Unify_Item(&ref[259])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun103)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(2),&ref[261])
	fail_ret
	call_c   Dyam_Cut()
	move     &ref[262], R(0)
	move     0, R(1)
	move     &ref[209], R(2)
	move     S(5), R(3)
	pl_call  pred_error_2()
	pl_jump  fun91()

;; TERM 260: '*GUARD*'(rcg_body_to_lpda(_B, _C, _D, _E, _F)) :> []
c_code local build_ref_260
	ret_reg &ref[260]
	call_c   build_ref_259()
	call_c   Dyam_Create_Binary(I(9),&ref[259],I(0))
	move_ret ref[260]
	c_ret

c_code local build_seed_38
	ret_reg &seed[38]
	call_c   build_ref_34()
	call_c   build_ref_227()
	call_c   Dyam_Seed_Start(&ref[34],&ref[227],&ref[227],fun3,1)
	call_c   build_ref_229()
	call_c   Dyam_Seed_Add_Comp(&ref[229],&ref[227],0)
	call_c   Dyam_Seed_End()
	move_ret seed[38]
	c_ret

;; TERM 229: '*FIRST*'(wrapping_predicate(_C)) :> '$$HOLE$$'
c_code local build_ref_229
	ret_reg &ref[229]
	call_c   build_ref_228()
	call_c   Dyam_Create_Binary(I(9),&ref[228],I(7))
	move_ret ref[229]
	c_ret

;; TERM 228: '*FIRST*'(wrapping_predicate(_C))
c_code local build_ref_228
	ret_reg &ref[228]
	call_c   build_ref_30()
	call_c   build_ref_226()
	call_c   Dyam_Create_Unary(&ref[30],&ref[226])
	move_ret ref[228]
	c_ret

;; TERM 226: wrapping_predicate(_C)
c_code local build_ref_226
	ret_reg &ref[226]
	call_c   build_ref_317()
	call_c   Dyam_Create_Unary(&ref[317],V(2))
	move_ret ref[226]
	c_ret

;; TERM 227: '*CITEM*'(wrapping_predicate(_C), wrapping_predicate(_C))
c_code local build_ref_227
	ret_reg &ref[227]
	call_c   build_ref_34()
	call_c   build_ref_226()
	call_c   Dyam_Create_Binary(&ref[34],&ref[226],&ref[226])
	move_ret ref[227]
	c_ret

c_code local build_seed_39
	ret_reg &seed[39]
	call_c   build_ref_100()
	call_c   build_ref_232()
	call_c   Dyam_Seed_Start(&ref[100],&ref[232],I(0),fun17,1)
	call_c   build_ref_230()
	call_c   Dyam_Seed_Add_Comp(&ref[230],fun71,0)
	call_c   Dyam_Seed_End()
	move_ret seed[39]
	c_ret

;; TERM 230: '*RITEM*'(wrapping_predicate(_C), voidret)
c_code local build_ref_230
	ret_reg &ref[230]
	call_c   build_ref_0()
	call_c   build_ref_226()
	call_c   build_ref_10()
	call_c   Dyam_Create_Binary(&ref[0],&ref[226],&ref[10])
	move_ret ref[230]
	c_ret

pl_code local fun71
	call_c   build_ref_230()
	call_c   Dyam_Unify_Item(&ref[230])
	fail_ret
	pl_jump  Follow_Cont(V(1))

;; TERM 232: '*RITEM*'(wrapping_predicate(_C), voidret) :> rcg(3, '$TUPPLE'(35080023573592))
c_code local build_ref_232
	ret_reg &ref[232]
	call_c   build_ref_230()
	call_c   build_ref_231()
	call_c   Dyam_Create_Binary(I(9),&ref[230],&ref[231])
	move_ret ref[232]
	c_ret

;; TERM 231: rcg(3, '$TUPPLE'(35080023573592))
c_code local build_ref_231
	ret_reg &ref[231]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,134217728)
	move_ret R(0)
	call_c   build_ref_304()
	call_c   Dyam_Create_Binary(&ref[304],N(3),R(0))
	move_ret ref[231]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 100: '*CURNEXT*'
c_code local build_ref_100
	ret_reg &ref[100]
	call_c   Dyam_Create_Atom("*CURNEXT*")
	move_ret ref[100]
	c_ret

c_code local build_seed_29
	ret_reg &seed[29]
	call_c   build_ref_34()
	call_c   build_ref_145()
	call_c   Dyam_Seed_Start(&ref[34],&ref[145],&ref[145],fun3,1)
	call_c   build_ref_147()
	call_c   Dyam_Seed_Add_Comp(&ref[147],&ref[145],0)
	call_c   Dyam_Seed_End()
	move_ret seed[29]
	c_ret

;; TERM 147: '*FIRST*'('call_rcg_make_callret/5'(_C)) :> '$$HOLE$$'
c_code local build_ref_147
	ret_reg &ref[147]
	call_c   build_ref_146()
	call_c   Dyam_Create_Binary(I(9),&ref[146],I(7))
	move_ret ref[147]
	c_ret

;; TERM 146: '*FIRST*'('call_rcg_make_callret/5'(_C))
c_code local build_ref_146
	ret_reg &ref[146]
	call_c   build_ref_30()
	call_c   build_ref_144()
	call_c   Dyam_Create_Unary(&ref[30],&ref[144])
	move_ret ref[146]
	c_ret

;; TERM 144: 'call_rcg_make_callret/5'(_C)
c_code local build_ref_144
	ret_reg &ref[144]
	call_c   build_ref_307()
	call_c   Dyam_Create_Unary(&ref[307],V(2))
	move_ret ref[144]
	c_ret

;; TERM 145: '*CITEM*'('call_rcg_make_callret/5'(_C), 'call_rcg_make_callret/5'(_C))
c_code local build_ref_145
	ret_reg &ref[145]
	call_c   build_ref_34()
	call_c   build_ref_144()
	call_c   Dyam_Create_Binary(&ref[34],&ref[144],&ref[144])
	move_ret ref[145]
	c_ret

c_code local build_seed_30
	ret_reg &seed[30]
	call_c   build_ref_100()
	call_c   build_ref_150()
	call_c   Dyam_Seed_Start(&ref[100],&ref[150],I(0),fun17,1)
	call_c   build_ref_148()
	call_c   Dyam_Seed_Add_Comp(&ref[148],fun47,0)
	call_c   Dyam_Seed_End()
	move_ret seed[30]
	c_ret

;; TERM 148: '*RITEM*'('call_rcg_make_callret/5'(_C), return(_D, _E, _F, _G))
c_code local build_ref_148
	ret_reg &ref[148]
	call_c   build_ref_0()
	call_c   build_ref_144()
	call_c   build_ref_129()
	call_c   Dyam_Create_Binary(&ref[0],&ref[144],&ref[129])
	move_ret ref[148]
	c_ret

;; TERM 129: return(_D, _E, _F, _G)
c_code local build_ref_129
	ret_reg &ref[129]
	call_c   build_ref_309()
	call_c   Dyam_Term_Start(&ref[309],4)
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[129]
	c_ret

pl_code local fun47
	call_c   build_ref_148()
	call_c   Dyam_Unify_Item(&ref[148])
	fail_ret
	pl_jump  Follow_Cont(V(1))

;; TERM 150: '*RITEM*'('call_rcg_make_callret/5'(_C), return(_D, _E, _F, _G)) :> rcg(2, '$TUPPLE'(35080023573592))
c_code local build_ref_150
	ret_reg &ref[150]
	call_c   build_ref_148()
	call_c   build_ref_149()
	call_c   Dyam_Create_Binary(I(9),&ref[148],&ref[149])
	move_ret ref[150]
	c_ret

;; TERM 149: rcg(2, '$TUPPLE'(35080023573592))
c_code local build_ref_149
	ret_reg &ref[149]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,134217728)
	move_ret R(0)
	call_c   build_ref_304()
	call_c   Dyam_Create_Binary(&ref[304],N(2),R(0))
	move_ret ref[149]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_26
	ret_reg &seed[26]
	call_c   build_ref_34()
	call_c   build_ref_126()
	call_c   Dyam_Seed_Start(&ref[34],&ref[126],&ref[126],fun3,1)
	call_c   build_ref_128()
	call_c   Dyam_Seed_Add_Comp(&ref[128],&ref[126],0)
	call_c   Dyam_Seed_End()
	move_ret seed[26]
	c_ret

;; TERM 128: '*FIRST*'('call_rcg_wrap_args/5'(_C)) :> '$$HOLE$$'
c_code local build_ref_128
	ret_reg &ref[128]
	call_c   build_ref_127()
	call_c   Dyam_Create_Binary(I(9),&ref[127],I(7))
	move_ret ref[128]
	c_ret

;; TERM 127: '*FIRST*'('call_rcg_wrap_args/5'(_C))
c_code local build_ref_127
	ret_reg &ref[127]
	call_c   build_ref_30()
	call_c   build_ref_125()
	call_c   Dyam_Create_Unary(&ref[30],&ref[125])
	move_ret ref[127]
	c_ret

;; TERM 125: 'call_rcg_wrap_args/5'(_C)
c_code local build_ref_125
	ret_reg &ref[125]
	call_c   build_ref_313()
	call_c   Dyam_Create_Unary(&ref[313],V(2))
	move_ret ref[125]
	c_ret

;; TERM 126: '*CITEM*'('call_rcg_wrap_args/5'(_C), 'call_rcg_wrap_args/5'(_C))
c_code local build_ref_126
	ret_reg &ref[126]
	call_c   build_ref_34()
	call_c   build_ref_125()
	call_c   Dyam_Create_Binary(&ref[34],&ref[125],&ref[125])
	move_ret ref[126]
	c_ret

c_code local build_seed_27
	ret_reg &seed[27]
	call_c   build_ref_100()
	call_c   build_ref_132()
	call_c   Dyam_Seed_Start(&ref[100],&ref[132],I(0),fun17,1)
	call_c   build_ref_130()
	call_c   Dyam_Seed_Add_Comp(&ref[130],fun42,0)
	call_c   Dyam_Seed_End()
	move_ret seed[27]
	c_ret

;; TERM 130: '*RITEM*'('call_rcg_wrap_args/5'(_C), return(_D, _E, _F, _G))
c_code local build_ref_130
	ret_reg &ref[130]
	call_c   build_ref_0()
	call_c   build_ref_125()
	call_c   build_ref_129()
	call_c   Dyam_Create_Binary(&ref[0],&ref[125],&ref[129])
	move_ret ref[130]
	c_ret

pl_code local fun42
	call_c   build_ref_130()
	call_c   Dyam_Unify_Item(&ref[130])
	fail_ret
	pl_jump  Follow_Cont(V(1))

;; TERM 132: '*RITEM*'('call_rcg_wrap_args/5'(_C), return(_D, _E, _F, _G)) :> rcg(1, '$TUPPLE'(35080023573592))
c_code local build_ref_132
	ret_reg &ref[132]
	call_c   build_ref_130()
	call_c   build_ref_131()
	call_c   Dyam_Create_Binary(I(9),&ref[130],&ref[131])
	move_ret ref[132]
	c_ret

;; TERM 131: rcg(1, '$TUPPLE'(35080023573592))
c_code local build_ref_131
	ret_reg &ref[131]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,134217728)
	move_ret R(0)
	call_c   build_ref_304()
	call_c   Dyam_Create_Binary(&ref[304],N(1),R(0))
	move_ret ref[131]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_21
	ret_reg &seed[21]
	call_c   build_ref_34()
	call_c   build_ref_97()
	call_c   Dyam_Seed_Start(&ref[34],&ref[97],&ref[97],fun3,1)
	call_c   build_ref_99()
	call_c   Dyam_Seed_Add_Comp(&ref[99],&ref[97],0)
	call_c   Dyam_Seed_End()
	move_ret seed[21]
	c_ret

;; TERM 99: '*FIRST*'(register_predicate(_C)) :> '$$HOLE$$'
c_code local build_ref_99
	ret_reg &ref[99]
	call_c   build_ref_98()
	call_c   Dyam_Create_Binary(I(9),&ref[98],I(7))
	move_ret ref[99]
	c_ret

;; TERM 98: '*FIRST*'(register_predicate(_C))
c_code local build_ref_98
	ret_reg &ref[98]
	call_c   build_ref_30()
	call_c   build_ref_96()
	call_c   Dyam_Create_Unary(&ref[30],&ref[96])
	move_ret ref[98]
	c_ret

;; TERM 96: register_predicate(_C)
c_code local build_ref_96
	ret_reg &ref[96]
	call_c   build_ref_344()
	call_c   Dyam_Create_Unary(&ref[344],V(2))
	move_ret ref[96]
	c_ret

;; TERM 344: register_predicate
c_code local build_ref_344
	ret_reg &ref[344]
	call_c   Dyam_Create_Atom("register_predicate")
	move_ret ref[344]
	c_ret

;; TERM 97: '*CITEM*'(register_predicate(_C), register_predicate(_C))
c_code local build_ref_97
	ret_reg &ref[97]
	call_c   build_ref_34()
	call_c   build_ref_96()
	call_c   Dyam_Create_Binary(&ref[34],&ref[96],&ref[96])
	move_ret ref[97]
	c_ret

c_code local build_seed_22
	ret_reg &seed[22]
	call_c   build_ref_100()
	call_c   build_ref_103()
	call_c   Dyam_Seed_Start(&ref[100],&ref[103],I(0),fun17,1)
	call_c   build_ref_101()
	call_c   Dyam_Seed_Add_Comp(&ref[101],fun32,0)
	call_c   Dyam_Seed_End()
	move_ret seed[22]
	c_ret

;; TERM 101: '*RITEM*'(register_predicate(_C), voidret)
c_code local build_ref_101
	ret_reg &ref[101]
	call_c   build_ref_0()
	call_c   build_ref_96()
	call_c   build_ref_10()
	call_c   Dyam_Create_Binary(&ref[0],&ref[96],&ref[10])
	move_ret ref[101]
	c_ret

pl_code local fun32
	call_c   build_ref_101()
	call_c   Dyam_Unify_Item(&ref[101])
	fail_ret
	pl_jump  Follow_Cont(V(1))

;; TERM 103: '*RITEM*'(register_predicate(_C), voidret) :> rcg(0, '$TUPPLE'(35080023573592))
c_code local build_ref_103
	ret_reg &ref[103]
	call_c   build_ref_101()
	call_c   build_ref_102()
	call_c   Dyam_Create_Binary(I(9),&ref[101],&ref[102])
	move_ret ref[103]
	c_ret

;; TERM 102: rcg(0, '$TUPPLE'(35080023573592))
c_code local build_ref_102
	ret_reg &ref[102]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,134217728)
	move_ret R(0)
	call_c   build_ref_304()
	call_c   Dyam_Create_Binary(&ref[304],N(0),R(0))
	move_ret ref[102]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 4: term_module_shift(_A, _B, _C, _D, _E)
c_code local build_ref_4
	ret_reg &ref[4]
	call_c   build_ref_345()
	call_c   Dyam_Term_Start(&ref[345],5)
	call_c   Dyam_Term_Arg(V(0))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[4]
	c_ret

;; TERM 345: term_module_shift
c_code local build_ref_345
	ret_reg &ref[345]
	call_c   Dyam_Create_Atom("term_module_shift")
	move_ret ref[345]
	c_ret

;; TERM 3: '*RITEM*'('call_term_module_shift/5'(_A, _B), return(_C, _D, _E))
c_code local build_ref_3
	ret_reg &ref[3]
	call_c   build_ref_0()
	call_c   build_ref_1()
	call_c   build_ref_2()
	call_c   Dyam_Create_Binary(&ref[0],&ref[1],&ref[2])
	move_ret ref[3]
	c_ret

;; TERM 2: return(_C, _D, _E)
c_code local build_ref_2
	ret_reg &ref[2]
	call_c   build_ref_309()
	call_c   Dyam_Term_Start(&ref[309],3)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[2]
	c_ret

;; TERM 1: 'call_term_module_shift/5'(_A, _B)
c_code local build_ref_1
	ret_reg &ref[1]
	call_c   build_ref_314()
	call_c   Dyam_Create_Binary(&ref[314],V(0),V(1))
	move_ret ref[1]
	c_ret

;; TERM 8: rcg_make_callret(_A, _B, _C, _D, _E)
c_code local build_ref_8
	ret_reg &ref[8]
	call_c   build_ref_346()
	call_c   Dyam_Term_Start(&ref[346],5)
	call_c   Dyam_Term_Arg(V(0))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[8]
	c_ret

;; TERM 346: rcg_make_callret
c_code local build_ref_346
	ret_reg &ref[346]
	call_c   Dyam_Create_Atom("rcg_make_callret")
	move_ret ref[346]
	c_ret

;; TERM 7: '*RITEM*'('call_rcg_make_callret/5'(_A), return(_B, _C, _D, _E))
c_code local build_ref_7
	ret_reg &ref[7]
	call_c   build_ref_0()
	call_c   build_ref_5()
	call_c   build_ref_6()
	call_c   Dyam_Create_Binary(&ref[0],&ref[5],&ref[6])
	move_ret ref[7]
	c_ret

;; TERM 6: return(_B, _C, _D, _E)
c_code local build_ref_6
	ret_reg &ref[6]
	call_c   build_ref_309()
	call_c   Dyam_Term_Start(&ref[309],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[6]
	c_ret

;; TERM 5: 'call_rcg_make_callret/5'(_A)
c_code local build_ref_5
	ret_reg &ref[5]
	call_c   build_ref_307()
	call_c   Dyam_Create_Unary(&ref[307],V(0))
	move_ret ref[5]
	c_ret

;; TERM 9: register_predicate(_A)
c_code local build_ref_9
	ret_reg &ref[9]
	call_c   build_ref_344()
	call_c   Dyam_Create_Unary(&ref[344],V(0))
	move_ret ref[9]
	c_ret

;; TERM 11: '*RITEM*'(register_predicate(_A), voidret)
c_code local build_ref_11
	ret_reg &ref[11]
	call_c   build_ref_0()
	call_c   build_ref_9()
	call_c   build_ref_10()
	call_c   Dyam_Create_Binary(&ref[0],&ref[9],&ref[10])
	move_ret ref[11]
	c_ret

;; TERM 14: rcg_wrap_args(_A, _B, _C, _D, _E)
c_code local build_ref_14
	ret_reg &ref[14]
	call_c   build_ref_347()
	call_c   Dyam_Term_Start(&ref[347],5)
	call_c   Dyam_Term_Arg(V(0))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[14]
	c_ret

;; TERM 347: rcg_wrap_args
c_code local build_ref_347
	ret_reg &ref[347]
	call_c   Dyam_Create_Atom("rcg_wrap_args")
	move_ret ref[347]
	c_ret

;; TERM 13: '*RITEM*'('call_rcg_wrap_args/5'(_A), return(_B, _C, _D, _E))
c_code local build_ref_13
	ret_reg &ref[13]
	call_c   build_ref_0()
	call_c   build_ref_12()
	call_c   build_ref_6()
	call_c   Dyam_Create_Binary(&ref[0],&ref[12],&ref[6])
	move_ret ref[13]
	c_ret

;; TERM 12: 'call_rcg_wrap_args/5'(_A)
c_code local build_ref_12
	ret_reg &ref[12]
	call_c   build_ref_313()
	call_c   Dyam_Create_Unary(&ref[313],V(0))
	move_ret ref[12]
	c_ret

;; TERM 15: wrapping_predicate(_A)
c_code local build_ref_15
	ret_reg &ref[15]
	call_c   build_ref_317()
	call_c   Dyam_Create_Unary(&ref[317],V(0))
	move_ret ref[15]
	c_ret

;; TERM 16: '*RITEM*'(wrapping_predicate(_A), voidret)
c_code local build_ref_16
	ret_reg &ref[16]
	call_c   build_ref_0()
	call_c   build_ref_15()
	call_c   build_ref_10()
	call_c   Dyam_Create_Binary(&ref[0],&ref[15],&ref[10])
	move_ret ref[16]
	c_ret

;; TERM 198: _D : _E
c_code local build_ref_198
	ret_reg &ref[198]
	call_c   build_ref_348()
	call_c   Dyam_Create_Binary(&ref[348],V(3),V(4))
	move_ret ref[198]
	c_ret

;; TERM 348: :
c_code local build_ref_348
	ret_reg &ref[348]
	call_c   Dyam_Create_Atom(":")
	move_ret ref[348]
	c_ret

;; TERM 200: _D = _H , _E = _I , _G
c_code local build_ref_200
	ret_reg &ref[200]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_349()
	call_c   Dyam_Create_Binary(&ref[349],V(3),V(7))
	move_ret R(0)
	call_c   Dyam_Create_Binary(&ref[349],V(4),V(8))
	move_ret R(1)
	call_c   Dyam_Create_Binary(I(4),R(1),V(6))
	move_ret R(1)
	call_c   Dyam_Create_Binary(I(4),R(0),R(1))
	move_ret ref[200]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 349: =
c_code local build_ref_349
	ret_reg &ref[349]
	call_c   Dyam_Create_Atom("=")
	move_ret ref[349]
	c_ret

;; TERM 199: rcg_assign(_B, _C, _H, _I)
c_code local build_ref_199
	ret_reg &ref[199]
	call_c   build_ref_330()
	call_c   Dyam_Term_Start(&ref[330],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_End()
	move_ret ref[199]
	c_ret

;; TERM 202: _C = _D : _E , _G
c_code local build_ref_202
	ret_reg &ref[202]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_349()
	call_c   build_ref_198()
	call_c   Dyam_Create_Binary(&ref[349],V(2),&ref[198])
	move_ret R(0)
	call_c   Dyam_Create_Binary(I(4),R(0),V(6))
	move_ret ref[202]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 201: rcg_assign(_B, _C, _D, _E)
c_code local build_ref_201
	ret_reg &ref[201]
	call_c   build_ref_330()
	call_c   Dyam_Term_Start(&ref[330],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[201]
	c_ret

;; TERM 203: _D = _E , _G
c_code local build_ref_203
	ret_reg &ref[203]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_349()
	call_c   Dyam_Create_Binary(&ref[349],V(3),V(4))
	move_ret R(0)
	call_c   Dyam_Create_Binary(I(4),R(0),V(6))
	move_ret ref[203]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 205: parse_mode(_N)
c_code local build_ref_205
	ret_reg &ref[205]
	call_c   build_ref_350()
	call_c   Dyam_Create_Unary(&ref[350],V(13))
	move_ret ref[205]
	c_ret

;; TERM 350: parse_mode
c_code local build_ref_350
	ret_reg &ref[350]
	call_c   Dyam_Create_Atom("parse_mode")
	move_ret ref[350]
	c_ret

;; TERM 204: [_L|_M]
c_code local build_ref_204
	ret_reg &ref[204]
	call_c   Dyam_Create_List(V(11),V(12))
	move_ret ref[204]
	c_ret

;; TERM 207: _Q = _R , _S
c_code local build_ref_207
	ret_reg &ref[207]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_349()
	call_c   Dyam_Create_Binary(&ref[349],V(16),V(17))
	move_ret R(0)
	call_c   Dyam_Create_Binary(I(4),R(0),V(18))
	move_ret ref[207]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 206: _O @ _P
c_code local build_ref_206
	ret_reg &ref[206]
	call_c   build_ref_351()
	call_c   Dyam_Create_Binary(&ref[351],V(14),V(15))
	move_ret ref[206]
	c_ret

;; TERM 351: @
c_code local build_ref_351
	ret_reg &ref[351]
	call_c   Dyam_Create_Atom("@")
	move_ret ref[351]
	c_ret

;; TERM 208: 'not a valid rcg string ~w'
c_code local build_ref_208
	ret_reg &ref[208]
	call_c   Dyam_Create_Atom("not a valid rcg string ~w")
	move_ret ref[208]
	c_ret

;; TERM 113: [apply,_F|_G]
c_code local build_ref_113
	ret_reg &ref[113]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(5),V(6))
	move_ret R(0)
	call_c   Dyam_Create_List(I(3),R(0))
	move_ret ref[113]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 64: scanner(_D, _H, _L, _J)
c_code local build_ref_64
	ret_reg &ref[64]
	call_c   build_ref_352()
	call_c   Dyam_Term_Start(&ref[352],4)
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret ref[64]
	c_ret

;; TERM 352: scanner
c_code local build_ref_352
	ret_reg &ref[352]
	call_c   Dyam_Create_Atom("scanner")
	move_ret ref[352]
	c_ret

;; TERM 63: token
c_code local build_ref_63
	ret_reg &ref[63]
	call_c   Dyam_Create_Atom("token")
	move_ret ref[63]
	c_ret

;; TERM 65: _D = [_H|_L]
c_code local build_ref_65
	ret_reg &ref[65]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(7),V(11))
	move_ret R(0)
	call_c   build_ref_349()
	call_c   Dyam_Create_Binary(&ref[349],V(3),R(0))
	move_ret ref[65]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 62: _J , _K
c_code local build_ref_62
	ret_reg &ref[62]
	call_c   Dyam_Create_Binary(I(4),V(9),V(10))
	move_ret ref[62]
	c_ret

;; TERM 61: [_H|_I]
c_code local build_ref_61
	ret_reg &ref[61]
	call_c   Dyam_Create_List(V(7),V(8))
	move_ret ref[61]
	c_ret

pl_code local fun2
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Object(R(0))
	call_c   Dyam_Tabule()
	pl_ret

pl_code local fun72
	call_c   Dyam_Remove_Choice()
	pl_call  fun33(&seed[39],2,V(3))
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun61
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Reg_Load(0,V(3))
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(9), R(4)
	move     S(5), R(5)
	pl_call  pred_my_numbervars_3()
	call_c   Dyam_Reg_Load(0,V(4))
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(10), R(4)
	move     S(5), R(5)
	pl_call  pred_my_numbervars_3()
	call_c   DYAM_evpred_assert_1(&ref[201])
	call_c   Dyam_Unify(V(5),&ref[202])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun63
	call_c   Dyam_Remove_Choice()
fun62:
	call_c   Dyam_Choice(fun61)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[199])
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(5),&ref[200])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret


pl_code local fun64
	call_c   Dyam_Remove_Choice()
	move     &ref[208], R(0)
	move     0, R(1)
	move     &ref[209], R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_error_2()

pl_code local fun65
	call_c   Dyam_Update_Choice(fun64)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(2),&ref[206])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(5),&ref[207])
	fail_ret
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(14))
	call_c   Dyam_Reg_Load(4,V(3))
	call_c   Dyam_Reg_Load(6,V(16))
	call_c   Dyam_Reg_Load(8,V(18))
	move     V(19), R(10)
	move     S(5), R(11)
	pl_call  pred_rcg_str_expand_6()
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(15))
	call_c   Dyam_Reg_Load(4,V(17))
	call_c   Dyam_Reg_Load(6,V(4))
	call_c   Dyam_Reg_Load(8,V(19))
	call_c   Dyam_Reg_Load(10,V(6))
	call_c   Dyam_Reg_Deallocate(6)
	pl_jump  pred_rcg_str_expand_6()

pl_code local fun66
	call_c   Dyam_Update_Choice(fun65)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(2),&ref[204])
	fail_ret
	call_c   Dyam_Cut()
	pl_call  Object_1(&ref[205])
	call_c   Dyam_Reg_Load(0,V(2))
	call_c   Dyam_Reg_Load(2,V(13))
	call_c   Dyam_Reg_Load(4,V(3))
	call_c   Dyam_Reg_Load(6,V(4))
	call_c   Dyam_Reg_Load(8,V(5))
	call_c   Dyam_Reg_Load(10,V(6))
	call_c   Dyam_Reg_Deallocate(6)
	pl_jump  pred_rcg_terminals_expand_6()

pl_code local fun67
	call_c   Dyam_Update_Choice(fun66)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(2),I(0))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(5),&ref[203])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun68
	call_c   Dyam_Update_Choice(fun67)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Reg_Load(0,V(2))
	pl_call  pred_check_var_1()
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun63)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_variable(V(1))
	fail_ret
	call_c   Dyam_Cut()
	call_c   DyALog_Gensym()
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Number(0,V(1))
	fail_ret
	pl_jump  fun62()

pl_code local fun48
	call_c   Dyam_Remove_Choice()
	pl_call  fun33(&seed[30],2,V(7))
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun43
	call_c   Dyam_Remove_Choice()
	pl_call  fun33(&seed[27],2,V(7))
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun40
	call_c   Dyam_Remove_Choice()
	call_c   DYAM_evpred_univ(V(1),&ref[113])
	fail_ret
	call_c   DYAM_evpred_length(V(6),V(4))
	fail_ret
	call_c   DYAM_evpred_functor(V(5),V(2),V(3))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun30
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Object(R(0))
	call_c   Dyam_Tabule()
	pl_jump  Schedule_Prolog()

pl_code local fun31
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Choice(fun30)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Subsume(R(0))
	fail_ret
	call_c   Dyam_Cut()
	pl_ret

pl_code local fun33
	call_c   Dyam_Backptr_Trace(R(1),R(2))
	call_c   Dyam_Light_Object(R(0))
	pl_jump  Schedule_Prolog()

pl_code local fun34
	call_c   Dyam_Remove_Choice()
	pl_call  fun33(&seed[22],2,V(3))
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun18
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Reg_Load(0,V(8))
	call_c   Dyam_Reg_Load(2,V(2))
	call_c   Dyam_Reg_Load(4,V(11))
	call_c   Dyam_Reg_Load(6,V(4))
	call_c   Dyam_Reg_Load(8,V(10))
	call_c   Dyam_Reg_Load(10,V(6))
	call_c   Dyam_Reg_Deallocate(6)
	pl_jump  pred_rcg_terminals_expand_6()

pl_code local fun20
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(9),&ref[65])
	fail_ret
	pl_jump  fun19()


;;------------------ INIT POOL ------------

c_code local build_init_pool
	call_c   build_seed_0()
	call_c   build_seed_3()
	call_c   build_seed_2()
	call_c   build_seed_4()
	call_c   build_seed_6()
	call_c   build_seed_1()
	call_c   build_seed_5()
	call_c   build_seed_7()
	call_c   build_seed_8()
	call_c   build_seed_11()
	call_c   build_seed_9()
	call_c   build_seed_10()
	call_c   build_seed_38()
	call_c   build_seed_39()
	call_c   build_seed_29()
	call_c   build_seed_30()
	call_c   build_seed_26()
	call_c   build_seed_27()
	call_c   build_seed_21()
	call_c   build_seed_22()
	call_c   build_ref_4()
	call_c   build_ref_3()
	call_c   build_ref_8()
	call_c   build_ref_7()
	call_c   build_ref_9()
	call_c   build_ref_11()
	call_c   build_ref_14()
	call_c   build_ref_13()
	call_c   build_ref_15()
	call_c   build_ref_16()
	call_c   build_ref_198()
	call_c   build_ref_200()
	call_c   build_ref_199()
	call_c   build_ref_202()
	call_c   build_ref_201()
	call_c   build_ref_203()
	call_c   build_ref_205()
	call_c   build_ref_204()
	call_c   build_ref_207()
	call_c   build_ref_206()
	call_c   build_ref_209()
	call_c   build_ref_208()
	call_c   build_ref_113()
	call_c   build_ref_114()
	call_c   build_ref_64()
	call_c   build_ref_63()
	call_c   build_ref_65()
	call_c   build_ref_62()
	call_c   build_ref_61()
	c_ret

long local ref[353]
long local seed[50]

long local _initialization

c_code global initialization_dyalog_rcg
	call_c   Dyam_Check_And_Update_Initialization(_initialization)
	fail_c_ret
	call_c   initialization_dyalog_maker()
	call_c   initialization_dyalog_format()
	call_c   build_init_pool()
	call_c   build_viewers()
	call_c   load()
	c_ret

