;; Compiler: DyALog 1.14.0
;; File "tag_maker.pl"


;;------------------ LOADING ------------

c_code local load
	call_c   Dyam_Loading_Set()
	pl_call  fun6(&seed[5],0)
	pl_call  fun6(&seed[0],0)
	pl_call  fun6(&seed[1],0)
	pl_call  fun6(&seed[4],0)
	pl_call  fun6(&seed[3],0)
	pl_call  fun6(&seed[2],0)
	call_c   Dyam_Loading_Reset()
	c_ret


;;------------------ VIEWERS ------------

c_code local build_viewers
	call_c   Dyam_Load_Viewer(&ref[3],&ref[1])
	c_ret

c_code local build_seed_2
	ret_reg &seed[2]
	call_c   build_ref_4()
	call_c   build_ref_8()
	call_c   Dyam_Seed_Start(&ref[4],&ref[8],I(0),fun3,1)
	call_c   build_ref_7()
	call_c   Dyam_Seed_Add_Comp(&ref[7],fun2,0)
	call_c   Dyam_Seed_End()
	move_ret seed[2]
	c_ret

;; TERM 7: '*GUARD*'(make_tag_bot_callret(_B, _C, _D, _E, _F))
c_code local build_ref_7
	ret_reg &ref[7]
	call_c   build_ref_5()
	call_c   build_ref_6()
	call_c   Dyam_Create_Unary(&ref[5],&ref[6])
	move_ret ref[7]
	c_ret

;; TERM 6: make_tag_bot_callret(_B, _C, _D, _E, _F)
c_code local build_ref_6
	ret_reg &ref[6]
	call_c   build_ref_79()
	call_c   Dyam_Term_Start(&ref[79],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[6]
	c_ret

;; TERM 79: make_tag_bot_callret
c_code local build_ref_79
	ret_reg &ref[79]
	call_c   Dyam_Create_Atom("make_tag_bot_callret")
	move_ret ref[79]
	c_ret

;; TERM 5: '*GUARD*'
c_code local build_ref_5
	ret_reg &ref[5]
	call_c   Dyam_Create_Atom("*GUARD*")
	move_ret ref[5]
	c_ret

c_code local build_seed_6
	ret_reg &seed[6]
	call_c   build_ref_5()
	call_c   build_ref_10()
	call_c   Dyam_Seed_Start(&ref[5],&ref[10],I(0),fun0,1)
	call_c   build_ref_11()
	call_c   Dyam_Seed_Add_Comp(&ref[11],&ref[10],0)
	call_c   Dyam_Seed_End()
	move_ret seed[6]
	c_ret

pl_code local fun0
	pl_jump  Complete(0,0)

;; TERM 11: '*GUARD*'(make_tag_callret(_B, _C, _D, _E, _F, (bot * tag_modulation{subst_modulation=> _G, top_modulation=> _H, bot_modulation=> _I}), _I)) :> '$$HOLE$$'
c_code local build_ref_11
	ret_reg &ref[11]
	call_c   build_ref_10()
	call_c   Dyam_Create_Binary(I(9),&ref[10],I(7))
	move_ret ref[11]
	c_ret

;; TERM 10: '*GUARD*'(make_tag_callret(_B, _C, _D, _E, _F, (bot * tag_modulation{subst_modulation=> _G, top_modulation=> _H, bot_modulation=> _I}), _I))
c_code local build_ref_10
	ret_reg &ref[10]
	call_c   build_ref_5()
	call_c   build_ref_9()
	call_c   Dyam_Create_Unary(&ref[5],&ref[9])
	move_ret ref[10]
	c_ret

;; TERM 9: make_tag_callret(_B, _C, _D, _E, _F, (bot * tag_modulation{subst_modulation=> _G, top_modulation=> _H, bot_modulation=> _I}), _I)
c_code local build_ref_9
	ret_reg &ref[9]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_82()
	call_c   Dyam_Term_Start(&ref[82],3)
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_69()
	call_c   build_ref_81()
	call_c   Dyam_Create_Binary(&ref[69],&ref[81],R(0))
	move_ret R(0)
	call_c   build_ref_80()
	call_c   Dyam_Term_Start(&ref[80],7)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_End()
	move_ret ref[9]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 80: make_tag_callret
c_code local build_ref_80
	ret_reg &ref[80]
	call_c   Dyam_Create_Atom("make_tag_callret")
	move_ret ref[80]
	c_ret

;; TERM 81: bot
c_code local build_ref_81
	ret_reg &ref[81]
	call_c   Dyam_Create_Atom("bot")
	move_ret ref[81]
	c_ret

;; TERM 69: *
c_code local build_ref_69
	ret_reg &ref[69]
	call_c   Dyam_Create_Atom("*")
	move_ret ref[69]
	c_ret

;; TERM 82: tag_modulation!'$ft'
c_code local build_ref_82
	ret_reg &ref[82]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_86()
	call_c   Dyam_Create_List(&ref[86],I(0))
	move_ret R(0)
	call_c   build_ref_85()
	call_c   Dyam_Create_List(&ref[85],R(0))
	move_ret R(0)
	call_c   build_ref_84()
	call_c   Dyam_Create_List(&ref[84],R(0))
	move_ret R(0)
	call_c   build_ref_83()
	call_c   Dyam_Create_Atom_Module("$ft",&ref[83])
	move_ret ref[82]
	call_c   DYAM_Feature_2(&ref[83],R(0))
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 83: tag_modulation
c_code local build_ref_83
	ret_reg &ref[83]
	call_c   Dyam_Create_Atom("tag_modulation")
	move_ret ref[83]
	c_ret

;; TERM 84: subst_modulation
c_code local build_ref_84
	ret_reg &ref[84]
	call_c   Dyam_Create_Atom("subst_modulation")
	move_ret ref[84]
	c_ret

;; TERM 85: top_modulation
c_code local build_ref_85
	ret_reg &ref[85]
	call_c   Dyam_Create_Atom("top_modulation")
	move_ret ref[85]
	c_ret

;; TERM 86: bot_modulation
c_code local build_ref_86
	ret_reg &ref[86]
	call_c   Dyam_Create_Atom("bot_modulation")
	move_ret ref[86]
	c_ret

pl_code local fun1
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Light_Object(R(0))
	pl_jump  Schedule_Prolog()

long local pool_fun2[3]=[2,build_ref_7,build_seed_6]

pl_code local fun2
	call_c   Dyam_Pool(pool_fun2)
	call_c   Dyam_Unify_Item(&ref[7])
	fail_ret
	pl_jump  fun1(&seed[6],1)

pl_code local fun3
	pl_ret

;; TERM 8: '*GUARD*'(make_tag_bot_callret(_B, _C, _D, _E, _F)) :> []
c_code local build_ref_8
	ret_reg &ref[8]
	call_c   build_ref_7()
	call_c   Dyam_Create_Binary(I(9),&ref[7],I(0))
	move_ret ref[8]
	c_ret

;; TERM 4: '*GUARD_CLAUSE*'
c_code local build_ref_4
	ret_reg &ref[4]
	call_c   Dyam_Create_Atom("*GUARD_CLAUSE*")
	move_ret ref[4]
	c_ret

c_code local build_seed_3
	ret_reg &seed[3]
	call_c   build_ref_4()
	call_c   build_ref_14()
	call_c   Dyam_Seed_Start(&ref[4],&ref[14],I(0),fun3,1)
	call_c   build_ref_13()
	call_c   Dyam_Seed_Add_Comp(&ref[13],fun4,0)
	call_c   Dyam_Seed_End()
	move_ret seed[3]
	c_ret

;; TERM 13: '*GUARD*'(make_tag_top_callret(_B, _C, _D, _E, _F))
c_code local build_ref_13
	ret_reg &ref[13]
	call_c   build_ref_5()
	call_c   build_ref_12()
	call_c   Dyam_Create_Unary(&ref[5],&ref[12])
	move_ret ref[13]
	c_ret

;; TERM 12: make_tag_top_callret(_B, _C, _D, _E, _F)
c_code local build_ref_12
	ret_reg &ref[12]
	call_c   build_ref_87()
	call_c   Dyam_Term_Start(&ref[87],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[12]
	c_ret

;; TERM 87: make_tag_top_callret
c_code local build_ref_87
	ret_reg &ref[87]
	call_c   Dyam_Create_Atom("make_tag_top_callret")
	move_ret ref[87]
	c_ret

c_code local build_seed_7
	ret_reg &seed[7]
	call_c   build_ref_5()
	call_c   build_ref_16()
	call_c   Dyam_Seed_Start(&ref[5],&ref[16],I(0),fun0,1)
	call_c   build_ref_17()
	call_c   Dyam_Seed_Add_Comp(&ref[17],&ref[16],0)
	call_c   Dyam_Seed_End()
	move_ret seed[7]
	c_ret

;; TERM 17: '*GUARD*'(make_tag_callret(_B, _C, _D, _E, _F, (top * tag_modulation{subst_modulation=> _G, top_modulation=> _H, bot_modulation=> _I}), _H)) :> '$$HOLE$$'
c_code local build_ref_17
	ret_reg &ref[17]
	call_c   build_ref_16()
	call_c   Dyam_Create_Binary(I(9),&ref[16],I(7))
	move_ret ref[17]
	c_ret

;; TERM 16: '*GUARD*'(make_tag_callret(_B, _C, _D, _E, _F, (top * tag_modulation{subst_modulation=> _G, top_modulation=> _H, bot_modulation=> _I}), _H))
c_code local build_ref_16
	ret_reg &ref[16]
	call_c   build_ref_5()
	call_c   build_ref_15()
	call_c   Dyam_Create_Unary(&ref[5],&ref[15])
	move_ret ref[16]
	c_ret

;; TERM 15: make_tag_callret(_B, _C, _D, _E, _F, (top * tag_modulation{subst_modulation=> _G, top_modulation=> _H, bot_modulation=> _I}), _H)
c_code local build_ref_15
	ret_reg &ref[15]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_82()
	call_c   Dyam_Term_Start(&ref[82],3)
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_69()
	call_c   build_ref_88()
	call_c   Dyam_Create_Binary(&ref[69],&ref[88],R(0))
	move_ret R(0)
	call_c   build_ref_80()
	call_c   Dyam_Term_Start(&ref[80],7)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[15]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 88: top
c_code local build_ref_88
	ret_reg &ref[88]
	call_c   Dyam_Create_Atom("top")
	move_ret ref[88]
	c_ret

long local pool_fun4[3]=[2,build_ref_13,build_seed_7]

pl_code local fun4
	call_c   Dyam_Pool(pool_fun4)
	call_c   Dyam_Unify_Item(&ref[13])
	fail_ret
	pl_jump  fun1(&seed[7],1)

;; TERM 14: '*GUARD*'(make_tag_top_callret(_B, _C, _D, _E, _F)) :> []
c_code local build_ref_14
	ret_reg &ref[14]
	call_c   build_ref_13()
	call_c   Dyam_Create_Binary(I(9),&ref[13],I(0))
	move_ret ref[14]
	c_ret

c_code local build_seed_4
	ret_reg &seed[4]
	call_c   build_ref_4()
	call_c   build_ref_20()
	call_c   Dyam_Seed_Start(&ref[4],&ref[20],I(0),fun3,1)
	call_c   build_ref_19()
	call_c   Dyam_Seed_Add_Comp(&ref[19],fun5,0)
	call_c   Dyam_Seed_End()
	move_ret seed[4]
	c_ret

;; TERM 19: '*GUARD*'(make_tag_subst_callret(_B, _C, _D, _E, _F))
c_code local build_ref_19
	ret_reg &ref[19]
	call_c   build_ref_5()
	call_c   build_ref_18()
	call_c   Dyam_Create_Unary(&ref[5],&ref[18])
	move_ret ref[19]
	c_ret

;; TERM 18: make_tag_subst_callret(_B, _C, _D, _E, _F)
c_code local build_ref_18
	ret_reg &ref[18]
	call_c   build_ref_89()
	call_c   Dyam_Term_Start(&ref[89],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[18]
	c_ret

;; TERM 89: make_tag_subst_callret
c_code local build_ref_89
	ret_reg &ref[89]
	call_c   Dyam_Create_Atom("make_tag_subst_callret")
	move_ret ref[89]
	c_ret

c_code local build_seed_8
	ret_reg &seed[8]
	call_c   build_ref_5()
	call_c   build_ref_22()
	call_c   Dyam_Seed_Start(&ref[5],&ref[22],I(0),fun0,1)
	call_c   build_ref_23()
	call_c   Dyam_Seed_Add_Comp(&ref[23],&ref[22],0)
	call_c   Dyam_Seed_End()
	move_ret seed[8]
	c_ret

;; TERM 23: '*GUARD*'(make_tag_callret(_B, _C, _D, _E, _F, (subst * tag_modulation{subst_modulation=> _G, top_modulation=> _H, bot_modulation=> _I}), _G)) :> '$$HOLE$$'
c_code local build_ref_23
	ret_reg &ref[23]
	call_c   build_ref_22()
	call_c   Dyam_Create_Binary(I(9),&ref[22],I(7))
	move_ret ref[23]
	c_ret

;; TERM 22: '*GUARD*'(make_tag_callret(_B, _C, _D, _E, _F, (subst * tag_modulation{subst_modulation=> _G, top_modulation=> _H, bot_modulation=> _I}), _G))
c_code local build_ref_22
	ret_reg &ref[22]
	call_c   build_ref_5()
	call_c   build_ref_21()
	call_c   Dyam_Create_Unary(&ref[5],&ref[21])
	move_ret ref[22]
	c_ret

;; TERM 21: make_tag_callret(_B, _C, _D, _E, _F, (subst * tag_modulation{subst_modulation=> _G, top_modulation=> _H, bot_modulation=> _I}), _G)
c_code local build_ref_21
	ret_reg &ref[21]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_82()
	call_c   Dyam_Term_Start(&ref[82],3)
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_69()
	call_c   build_ref_90()
	call_c   Dyam_Create_Binary(&ref[69],&ref[90],R(0))
	move_ret R(0)
	call_c   build_ref_80()
	call_c   Dyam_Term_Start(&ref[80],7)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[21]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 90: subst
c_code local build_ref_90
	ret_reg &ref[90]
	call_c   Dyam_Create_Atom("subst")
	move_ret ref[90]
	c_ret

long local pool_fun5[3]=[2,build_ref_19,build_seed_8]

pl_code local fun5
	call_c   Dyam_Pool(pool_fun5)
	call_c   Dyam_Unify_Item(&ref[19])
	fail_ret
	pl_jump  fun1(&seed[8],1)

;; TERM 20: '*GUARD*'(make_tag_subst_callret(_B, _C, _D, _E, _F)) :> []
c_code local build_ref_20
	ret_reg &ref[20]
	call_c   build_ref_19()
	call_c   Dyam_Create_Binary(I(9),&ref[19],I(0))
	move_ret ref[20]
	c_ret

c_code local build_seed_1
	ret_reg &seed[1]
	call_c   build_ref_4()
	call_c   build_ref_26()
	call_c   Dyam_Seed_Start(&ref[4],&ref[26],I(0),fun3,1)
	call_c   build_ref_25()
	call_c   Dyam_Seed_Add_Comp(&ref[25],fun26,0)
	call_c   Dyam_Seed_End()
	move_ret seed[1]
	c_ret

;; TERM 25: '*GUARD*'(make_tag_callret(_B, _C, _D, _E, _F, _G, _H))
c_code local build_ref_25
	ret_reg &ref[25]
	call_c   build_ref_5()
	call_c   build_ref_24()
	call_c   Dyam_Create_Unary(&ref[5],&ref[24])
	move_ret ref[25]
	c_ret

;; TERM 24: make_tag_callret(_B, _C, _D, _E, _F, _G, _H)
c_code local build_ref_24
	ret_reg &ref[24]
	call_c   build_ref_80()
	call_c   Dyam_Term_Start(&ref[80],7)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[24]
	c_ret

;; TERM 61: '$CLOSURE'('$fun'(25, 0, 1126089800), '$TUPPLE'(35100048754612))
c_code local build_ref_61
	ret_reg &ref[61]
	call_c   build_ref_60()
	call_c   Dyam_Closure_Aux(fun25,&ref[60])
	move_ret ref[61]
	c_ret

pl_code local fun25
	call_c   Dyam_Unify(V(1),V(10))
	fail_ret
	call_c   Dyam_Unify(V(2),V(11))
	fail_ret
	call_c   Dyam_Unify(V(3),V(12))
	fail_ret
	pl_ret

;; TERM 60: '$TUPPLE'(35100048754612)
c_code local build_ref_60
	ret_reg &ref[60]
	call_c   Dyam_Create_Simple_Tupple(0,235339776)
	move_ret ref[60]
	c_ret

;; TERM 62: make_true_tag_callret(_K, _L, _M, _E, _F, _G, _H)
c_code local build_ref_62
	ret_reg &ref[62]
	call_c   build_ref_91()
	call_c   Dyam_Term_Start(&ref[91],7)
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[62]
	c_ret

;; TERM 91: make_true_tag_callret
c_code local build_ref_91
	ret_reg &ref[91]
	call_c   Dyam_Create_Atom("make_true_tag_callret")
	move_ret ref[91]
	c_ret

long local pool_fun26[4]=[3,build_ref_25,build_ref_61,build_ref_62]

pl_code local fun26
	call_c   Dyam_Pool(pool_fun26)
	call_c   Dyam_Unify_Item(&ref[25])
	fail_ret
	call_c   DYAM_evpred_functor(V(1),V(8),V(9))
	fail_ret
	call_c   DYAM_evpred_functor(V(10),V(8),V(9))
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[61], R(0)
	move     S(5), R(1)
	move     &ref[62], R(2)
	move     S(5), R(3)
	move     I(0), R(4)
	move     0, R(5)
	call_c   Dyam_Reg_Deallocate(3)
fun24:
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Reg_Unify(2,&ref[28])
	fail_ret
	call_c   Dyam_Reg_Bind(4,V(9))
	call_c   Dyam_Choice(fun23)
	pl_call  fun20(&seed[9],1)
	pl_fail


;; TERM 26: '*GUARD*'(make_tag_callret(_B, _C, _D, _E, _F, _G, _H)) :> []
c_code local build_ref_26
	ret_reg &ref[26]
	call_c   build_ref_25()
	call_c   Dyam_Create_Binary(I(9),&ref[25],I(0))
	move_ret ref[26]
	c_ret

c_code local build_seed_0
	ret_reg &seed[0]
	call_c   build_ref_30()
	call_c   build_ref_35()
	call_c   Dyam_Seed_Start(&ref[30],&ref[35],I(0),fun18,1)
	call_c   build_ref_36()
	call_c   Dyam_Seed_Add_Comp(&ref[36],fun17,0)
	call_c   Dyam_Seed_End()
	move_ret seed[0]
	c_ret

;; TERM 36: '*CITEM*'(make_true_tag_callret(_B, _C, _D, _E, _F, (_G * _H), nt_modulation{nt_pattern=> _I, left_pattern=> _J, right_pattern=> _K}), _A)
c_code local build_ref_36
	ret_reg &ref[36]
	call_c   build_ref_27()
	call_c   build_ref_33()
	call_c   Dyam_Create_Binary(&ref[27],&ref[33],V(0))
	move_ret ref[36]
	c_ret

;; TERM 33: make_true_tag_callret(_B, _C, _D, _E, _F, (_G * _H), nt_modulation{nt_pattern=> _I, left_pattern=> _J, right_pattern=> _K})
c_code local build_ref_33
	ret_reg &ref[33]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_69()
	call_c   Dyam_Create_Binary(&ref[69],V(6),V(7))
	move_ret R(0)
	call_c   build_ref_92()
	call_c   Dyam_Term_Start(&ref[92],3)
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_End()
	move_ret R(1)
	call_c   build_ref_91()
	call_c   Dyam_Term_Start(&ref[91],7)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_End()
	move_ret ref[33]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 92: nt_modulation!'$ft'
c_code local build_ref_92
	ret_reg &ref[92]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_96()
	call_c   Dyam_Create_List(&ref[96],I(0))
	move_ret R(0)
	call_c   build_ref_95()
	call_c   Dyam_Create_List(&ref[95],R(0))
	move_ret R(0)
	call_c   build_ref_94()
	call_c   Dyam_Create_List(&ref[94],R(0))
	move_ret R(0)
	call_c   build_ref_93()
	call_c   Dyam_Create_Atom_Module("$ft",&ref[93])
	move_ret ref[92]
	call_c   DYAM_Feature_2(&ref[93],R(0))
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 93: nt_modulation
c_code local build_ref_93
	ret_reg &ref[93]
	call_c   Dyam_Create_Atom("nt_modulation")
	move_ret ref[93]
	c_ret

;; TERM 94: nt_pattern
c_code local build_ref_94
	ret_reg &ref[94]
	call_c   Dyam_Create_Atom("nt_pattern")
	move_ret ref[94]
	c_ret

;; TERM 95: left_pattern
c_code local build_ref_95
	ret_reg &ref[95]
	call_c   Dyam_Create_Atom("left_pattern")
	move_ret ref[95]
	c_ret

;; TERM 96: right_pattern
c_code local build_ref_96
	ret_reg &ref[96]
	call_c   Dyam_Create_Atom("right_pattern")
	move_ret ref[96]
	c_ret

;; TERM 27: '*CITEM*'
c_code local build_ref_27
	ret_reg &ref[27]
	call_c   Dyam_Create_Atom("*CITEM*")
	move_ret ref[27]
	c_ret

;; TERM 52: +
c_code local build_ref_52
	ret_reg &ref[52]
	call_c   Dyam_Create_Atom("+")
	move_ret ref[52]
	c_ret

;; TERM 53: -
c_code local build_ref_53
	ret_reg &ref[53]
	call_c   Dyam_Create_Atom("-")
	move_ret ref[53]
	c_ret

;; TERM 50: [_I,_J,_K|_I]
c_code local build_ref_50
	ret_reg &ref[50]
	call_c   Dyam_Create_Tupple(8,10,V(8))
	move_ret ref[50]
	c_ret

;; TERM 40: [_Q|_R]
c_code local build_ref_40
	ret_reg &ref[40]
	call_c   Dyam_Create_List(V(16),V(17))
	move_ret ref[40]
	c_ret

;; TERM 41: [_Q,_C,_D|_R]
c_code local build_ref_41
	ret_reg &ref[41]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Tupple(2,3,V(17))
	move_ret R(0)
	call_c   Dyam_Create_List(V(16),R(0))
	move_ret ref[41]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_10
	ret_reg &seed[10]
	call_c   build_ref_5()
	call_c   build_ref_43()
	call_c   Dyam_Seed_Start(&ref[5],&ref[43],I(0),fun0,1)
	call_c   build_ref_44()
	call_c   Dyam_Seed_Add_Comp(&ref[44],&ref[43],0)
	call_c   Dyam_Seed_End()
	move_ret seed[10]
	c_ret

;; TERM 44: '*GUARD*'(make_callret_with_pat(_P, _S, _E, _F)) :> '$$HOLE$$'
c_code local build_ref_44
	ret_reg &ref[44]
	call_c   build_ref_43()
	call_c   Dyam_Create_Binary(I(9),&ref[43],I(7))
	move_ret ref[44]
	c_ret

;; TERM 43: '*GUARD*'(make_callret_with_pat(_P, _S, _E, _F))
c_code local build_ref_43
	ret_reg &ref[43]
	call_c   build_ref_5()
	call_c   build_ref_42()
	call_c   Dyam_Create_Unary(&ref[5],&ref[42])
	move_ret ref[43]
	c_ret

;; TERM 42: make_callret_with_pat(_P, _S, _E, _F)
c_code local build_ref_42
	ret_reg &ref[42]
	call_c   build_ref_97()
	call_c   Dyam_Term_Start(&ref[97],4)
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[42]
	c_ret

;; TERM 97: make_callret_with_pat
c_code local build_ref_97
	ret_reg &ref[97]
	call_c   Dyam_Create_Atom("make_callret_with_pat")
	move_ret ref[97]
	c_ret

c_code local build_seed_12
	ret_reg &seed[12]
	call_c   build_ref_0()
	call_c   build_ref_48()
	call_c   Dyam_Seed_Start(&ref[0],&ref[48],&ref[48],fun0,1)
	call_c   build_ref_49()
	call_c   Dyam_Seed_Add_Comp(&ref[49],&ref[48],0)
	call_c   Dyam_Seed_End()
	move_ret seed[12]
	c_ret

;; TERM 49: '*RITEM*'(_A, voidret) :> '$$HOLE$$'
c_code local build_ref_49
	ret_reg &ref[49]
	call_c   build_ref_48()
	call_c   Dyam_Create_Binary(I(9),&ref[48],I(7))
	move_ret ref[49]
	c_ret

;; TERM 48: '*RITEM*'(_A, voidret)
c_code local build_ref_48
	ret_reg &ref[48]
	call_c   build_ref_0()
	call_c   build_ref_2()
	call_c   Dyam_Create_Binary(&ref[0],V(0),&ref[2])
	move_ret ref[48]
	c_ret

;; TERM 2: voidret
c_code local build_ref_2
	ret_reg &ref[2]
	call_c   Dyam_Create_Atom("voidret")
	move_ret ref[2]
	c_ret

;; TERM 0: '*RITEM*'
c_code local build_ref_0
	ret_reg &ref[0]
	call_c   Dyam_Create_Atom("*RITEM*")
	move_ret ref[0]
	c_ret

pl_code local fun7
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Object(R(0))
	call_c   Dyam_Tabule()
	pl_ret

pl_code local fun8
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Choice(fun7)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Subsume(R(0))
	fail_ret
	call_c   Dyam_Cut()
	pl_ret

long local pool_fun9[2]=[1,build_seed_12]

pl_code local fun10
	call_c   Dyam_Remove_Choice()
fun9:
	call_c   Dyam_Deallocate()
	pl_jump  fun8(&seed[12],2)


c_code local build_seed_11
	ret_reg &seed[11]
	call_c   build_ref_5()
	call_c   build_ref_46()
	call_c   Dyam_Seed_Start(&ref[5],&ref[46],I(0),fun0,1)
	call_c   build_ref_47()
	call_c   Dyam_Seed_Add_Comp(&ref[47],&ref[46],0)
	call_c   Dyam_Seed_End()
	move_ret seed[11]
	c_ret

;; TERM 47: '*GUARD*'(tag_viewer(_G, _B, _C, _D, _E, _F)) :> '$$HOLE$$'
c_code local build_ref_47
	ret_reg &ref[47]
	call_c   build_ref_46()
	call_c   Dyam_Create_Binary(I(9),&ref[46],I(7))
	move_ret ref[47]
	c_ret

;; TERM 46: '*GUARD*'(tag_viewer(_G, _B, _C, _D, _E, _F))
c_code local build_ref_46
	ret_reg &ref[46]
	call_c   build_ref_5()
	call_c   build_ref_45()
	call_c   Dyam_Create_Unary(&ref[5],&ref[45])
	move_ret ref[46]
	c_ret

;; TERM 45: tag_viewer(_G, _B, _C, _D, _E, _F)
c_code local build_ref_45
	ret_reg &ref[45]
	call_c   build_ref_98()
	call_c   Dyam_Term_Start(&ref[98],6)
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[45]
	c_ret

;; TERM 98: tag_viewer
c_code local build_ref_98
	ret_reg &ref[98]
	call_c   Dyam_Create_Atom("tag_viewer")
	move_ret ref[98]
	c_ret

long local pool_fun11[6]=[65540,build_ref_40,build_ref_41,build_seed_10,build_seed_11,pool_fun9]

long local pool_fun12[3]=[65537,build_ref_50,pool_fun11]

pl_code local fun12
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(15),&ref[50])
	fail_ret
fun11:
	call_c   DYAM_evpred_univ(V(1),&ref[40])
	fail_ret
	call_c   DYAM_evpred_univ(V(18),&ref[41])
	fail_ret
	pl_call  fun1(&seed[10],1)
	call_c   Dyam_Choice(fun10)
	pl_call  fun1(&seed[11],1)
	pl_fail


;; TERM 38: [_N|_O]
c_code local build_ref_38
	ret_reg &ref[38]
	call_c   Dyam_Create_List(V(13),V(14))
	move_ret ref[38]
	c_ret

;; TERM 39: [_N,_J,_K|_O]
c_code local build_ref_39
	ret_reg &ref[39]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Tupple(9,10,V(14))
	move_ret R(0)
	call_c   Dyam_Create_List(V(13),R(0))
	move_ret ref[39]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun13[5]=[131074,build_ref_38,build_ref_39,pool_fun12,pool_fun11]

pl_code local fun13
	call_c   Dyam_Update_Choice(fun12)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_compound(V(8))
	fail_ret
	call_c   Dyam_Cut()
	call_c   DYAM_evpred_univ(V(8),&ref[38])
	fail_ret
	call_c   Dyam_Unify(V(15),&ref[39])
	fail_ret
	pl_jump  fun11()

long local pool_fun14[5]=[131074,build_ref_38,build_ref_39,pool_fun13,pool_fun11]

long local pool_fun15[4]=[65538,build_ref_52,build_ref_53,pool_fun14]

pl_code local fun15
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(8),&ref[52])
	fail_ret
	call_c   Dyam_Unify(V(9),&ref[52])
	fail_ret
	call_c   Dyam_Unify(V(10),&ref[53])
	fail_ret
fun14:
	call_c   Dyam_Choice(fun13)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(8),&ref[38])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(15),&ref[39])
	fail_ret
	pl_jump  fun11()


;; TERM 51: tag_modulation('*default*', _H)
c_code local build_ref_51
	ret_reg &ref[51]
	call_c   build_ref_83()
	call_c   build_ref_99()
	call_c   Dyam_Create_Binary(&ref[83],&ref[99],V(7))
	move_ret ref[51]
	c_ret

;; TERM 99: '*default*'
c_code local build_ref_99
	ret_reg &ref[99]
	call_c   Dyam_Create_Atom("*default*")
	move_ret ref[99]
	c_ret

long local pool_fun16[4]=[131073,build_ref_51,pool_fun15,pool_fun14]

pl_code local fun16
	call_c   Dyam_Update_Choice(fun15)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[51])
	call_c   Dyam_Cut()
	pl_jump  fun14()

;; TERM 37: tag_modulation((_L / _M), _H)
c_code local build_ref_37
	ret_reg &ref[37]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_100()
	call_c   Dyam_Create_Binary(&ref[100],V(11),V(12))
	move_ret R(0)
	call_c   build_ref_83()
	call_c   Dyam_Create_Binary(&ref[83],R(0),V(7))
	move_ret ref[37]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 100: /
c_code local build_ref_100
	ret_reg &ref[100]
	call_c   Dyam_Create_Atom("/")
	move_ret ref[100]
	c_ret

long local pool_fun17[5]=[131074,build_ref_36,build_ref_37,pool_fun16,pool_fun14]

pl_code local fun17
	call_c   Dyam_Pool(pool_fun17)
	call_c   Dyam_Unify_Item(&ref[36])
	fail_ret
	call_c   DYAM_evpred_functor(V(1),V(11),V(12))
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun16)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[37])
	call_c   Dyam_Cut()
	pl_jump  fun14()

pl_code local fun18
	pl_jump  Apply(0,0)

;; TERM 35: '*FIRST*'(make_true_tag_callret(_B, _C, _D, _E, _F, (_G * _H), nt_modulation{nt_pattern=> _I, left_pattern=> _J, right_pattern=> _K})) :> []
c_code local build_ref_35
	ret_reg &ref[35]
	call_c   build_ref_34()
	call_c   Dyam_Create_Binary(I(9),&ref[34],I(0))
	move_ret ref[35]
	c_ret

;; TERM 34: '*FIRST*'(make_true_tag_callret(_B, _C, _D, _E, _F, (_G * _H), nt_modulation{nt_pattern=> _I, left_pattern=> _J, right_pattern=> _K}))
c_code local build_ref_34
	ret_reg &ref[34]
	call_c   build_ref_30()
	call_c   build_ref_33()
	call_c   Dyam_Create_Unary(&ref[30],&ref[33])
	move_ret ref[34]
	c_ret

;; TERM 30: '*FIRST*'
c_code local build_ref_30
	ret_reg &ref[30]
	call_c   Dyam_Create_Atom("*FIRST*")
	move_ret ref[30]
	c_ret

c_code local build_seed_5
	ret_reg &seed[5]
	call_c   build_ref_4()
	call_c   build_ref_65()
	call_c   Dyam_Seed_Start(&ref[4],&ref[65],I(0),fun3,1)
	call_c   build_ref_64()
	call_c   Dyam_Seed_Add_Comp(&ref[64],fun37,0)
	call_c   Dyam_Seed_End()
	move_ret seed[5]
	c_ret

;; TERM 64: '*GUARD*'(make_tag_node_callret(_B, tag_node{id=> _C, label=> _D, children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}, _O, _P, _C))
c_code local build_ref_64
	ret_reg &ref[64]
	call_c   build_ref_5()
	call_c   build_ref_63()
	call_c   Dyam_Create_Unary(&ref[5],&ref[63])
	move_ret ref[64]
	c_ret

;; TERM 63: make_tag_node_callret(_B, tag_node{id=> _C, label=> _D, children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}, _O, _P, _C)
c_code local build_ref_63
	ret_reg &ref[63]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_102()
	call_c   Dyam_Term_Start(&ref[102],12)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_101()
	call_c   Dyam_Term_Start(&ref[101],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_End()
	move_ret ref[63]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 101: make_tag_node_callret
c_code local build_ref_101
	ret_reg &ref[101]
	call_c   Dyam_Create_Atom("make_tag_node_callret")
	move_ret ref[101]
	c_ret

;; TERM 102: tag_node!'$ft'
c_code local build_ref_102
	ret_reg &ref[102]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_113()
	call_c   Dyam_Create_List(&ref[113],I(0))
	move_ret R(0)
	call_c   build_ref_112()
	call_c   Dyam_Create_List(&ref[112],R(0))
	move_ret R(0)
	call_c   build_ref_111()
	call_c   Dyam_Create_List(&ref[111],R(0))
	move_ret R(0)
	call_c   build_ref_110()
	call_c   Dyam_Create_List(&ref[110],R(0))
	move_ret R(0)
	call_c   build_ref_81()
	call_c   Dyam_Create_List(&ref[81],R(0))
	move_ret R(0)
	call_c   build_ref_88()
	call_c   Dyam_Create_List(&ref[88],R(0))
	move_ret R(0)
	call_c   build_ref_109()
	call_c   Dyam_Create_List(&ref[109],R(0))
	move_ret R(0)
	call_c   build_ref_108()
	call_c   Dyam_Create_List(&ref[108],R(0))
	move_ret R(0)
	call_c   build_ref_107()
	call_c   Dyam_Create_List(&ref[107],R(0))
	move_ret R(0)
	call_c   build_ref_106()
	call_c   Dyam_Create_List(&ref[106],R(0))
	move_ret R(0)
	call_c   build_ref_105()
	call_c   Dyam_Create_List(&ref[105],R(0))
	move_ret R(0)
	call_c   build_ref_104()
	call_c   Dyam_Create_List(&ref[104],R(0))
	move_ret R(0)
	call_c   build_ref_103()
	call_c   Dyam_Create_Atom_Module("$ft",&ref[103])
	move_ret ref[102]
	call_c   DYAM_Feature_2(&ref[103],R(0))
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 103: tag_node
c_code local build_ref_103
	ret_reg &ref[103]
	call_c   Dyam_Create_Atom("tag_node")
	move_ret ref[103]
	c_ret

;; TERM 104: id
c_code local build_ref_104
	ret_reg &ref[104]
	call_c   Dyam_Create_Atom("id")
	move_ret ref[104]
	c_ret

;; TERM 105: label
c_code local build_ref_105
	ret_reg &ref[105]
	call_c   Dyam_Create_Atom("label")
	move_ret ref[105]
	c_ret

;; TERM 106: children
c_code local build_ref_106
	ret_reg &ref[106]
	call_c   Dyam_Create_Atom("children")
	move_ret ref[106]
	c_ret

;; TERM 107: kind
c_code local build_ref_107
	ret_reg &ref[107]
	call_c   Dyam_Create_Atom("kind")
	move_ret ref[107]
	c_ret

;; TERM 108: adj
c_code local build_ref_108
	ret_reg &ref[108]
	call_c   Dyam_Create_Atom("adj")
	move_ret ref[108]
	c_ret

;; TERM 109: spine
c_code local build_ref_109
	ret_reg &ref[109]
	call_c   Dyam_Create_Atom("spine")
	move_ret ref[109]
	c_ret

;; TERM 110: token
c_code local build_ref_110
	ret_reg &ref[110]
	call_c   Dyam_Create_Atom("token")
	move_ret ref[110]
	c_ret

;; TERM 111: adjleft
c_code local build_ref_111
	ret_reg &ref[111]
	call_c   Dyam_Create_Atom("adjleft")
	move_ret ref[111]
	c_ret

;; TERM 112: adjright
c_code local build_ref_112
	ret_reg &ref[112]
	call_c   Dyam_Create_Atom("adjright")
	move_ret ref[112]
	c_ret

;; TERM 113: adjwrap
c_code local build_ref_113
	ret_reg &ref[113]
	call_c   Dyam_Create_Atom("adjwrap")
	move_ret ref[113]
	c_ret

;; TERM 78: '~w:~w'
c_code local build_ref_78
	ret_reg &ref[78]
	call_c   Dyam_Create_Atom("~w:~w")
	move_ret ref[78]
	c_ret

;; TERM 67: [_B,_C]
c_code local build_ref_67
	ret_reg &ref[67]
	call_c   Dyam_Create_Tupple(1,2,I(0))
	move_ret ref[67]
	c_ret

;; TERM 70: _J * _E
c_code local build_ref_70
	ret_reg &ref[70]
	call_c   build_ref_69()
	call_c   Dyam_Create_Binary(&ref[69],V(9),V(4))
	move_ret ref[70]
	c_ret

;; TERM 71: [_Q,_S]
c_code local build_ref_71
	ret_reg &ref[71]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(18),I(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(16),R(0))
	move_ret ref[71]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

pl_code local fun28
	call_c   Dyam_Remove_Choice()
fun27:
	call_c   Dyam_Deallocate()
	pl_ret


c_code local build_seed_14
	ret_reg &seed[14]
	call_c   build_ref_5()
	call_c   build_ref_74()
	call_c   Dyam_Seed_Start(&ref[5],&ref[74],I(0),fun0,1)
	call_c   build_ref_75()
	call_c   Dyam_Seed_Add_Comp(&ref[75],&ref[74],0)
	call_c   Dyam_Seed_End()
	move_ret seed[14]
	c_ret

;; TERM 75: '*GUARD*'(tag_node_viewer(tag_node{id=> _C, label=> _D, children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}, _Q, _O, _P)) :> '$$HOLE$$'
c_code local build_ref_75
	ret_reg &ref[75]
	call_c   build_ref_74()
	call_c   Dyam_Create_Binary(I(9),&ref[74],I(7))
	move_ret ref[75]
	c_ret

;; TERM 74: '*GUARD*'(tag_node_viewer(tag_node{id=> _C, label=> _D, children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}, _Q, _O, _P))
c_code local build_ref_74
	ret_reg &ref[74]
	call_c   build_ref_5()
	call_c   build_ref_73()
	call_c   Dyam_Create_Unary(&ref[5],&ref[73])
	move_ret ref[74]
	c_ret

;; TERM 73: tag_node_viewer(tag_node{id=> _C, label=> _D, children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}, _Q, _O, _P)
c_code local build_ref_73
	ret_reg &ref[73]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_102()
	call_c   Dyam_Term_Start(&ref[102],12)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_114()
	call_c   Dyam_Term_Start(&ref[114],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_End()
	move_ret ref[73]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 114: tag_node_viewer
c_code local build_ref_114
	ret_reg &ref[114]
	call_c   Dyam_Create_Atom("tag_node_viewer")
	move_ret ref[114]
	c_ret

long local pool_fun29[2]=[1,build_seed_14]

long local pool_fun30[3]=[65537,build_ref_71,pool_fun29]

pl_code local fun30
	call_c   Dyam_Remove_Choice()
	call_c   DYAM_evpred_univ(V(14),&ref[71])
	fail_ret
	call_c   DYAM_evpred_univ(V(15),&ref[71])
	fail_ret
fun29:
	call_c   Dyam_Choice(fun28)
	pl_call  fun1(&seed[14],1)
	pl_fail


;; TERM 76: footcall
c_code local build_ref_76
	ret_reg &ref[76]
	call_c   Dyam_Create_Atom("footcall")
	move_ret ref[76]
	c_ret

long local pool_fun31[6]=[131075,build_ref_53,build_ref_76,build_ref_71,pool_fun30,pool_fun29]

pl_code local fun31
	call_c   Dyam_Update_Choice(fun30)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(17),&ref[53])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(14),&ref[76])
	fail_ret
	call_c   DYAM_evpred_univ(V(15),&ref[71])
	fail_ret
	pl_jump  fun29()

;; TERM 72: footret
c_code local build_ref_72
	ret_reg &ref[72]
	call_c   Dyam_Create_Atom("footret")
	move_ret ref[72]
	c_ret

long local pool_fun32[7]=[131076,build_ref_70,build_ref_52,build_ref_71,build_ref_72,pool_fun31,pool_fun29]

long local pool_fun33[3]=[65537,build_ref_52,pool_fun32]

pl_code local fun33
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(17),&ref[52])
	fail_ret
fun32:
	move     &ref[70], R(0)
	move     S(5), R(1)
	move     V(18), R(2)
	move     S(5), R(3)
	pl_call  pred_tupple_2()
	call_c   Dyam_Choice(fun31)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(17),&ref[52])
	fail_ret
	call_c   Dyam_Cut()
	call_c   DYAM_evpred_univ(V(14),&ref[71])
	fail_ret
	call_c   Dyam_Unify(V(15),&ref[72])
	fail_ret
	pl_jump  fun29()


;; TERM 77: tag_node_modulation('*default*', _R)
c_code local build_ref_77
	ret_reg &ref[77]
	call_c   build_ref_115()
	call_c   build_ref_99()
	call_c   Dyam_Create_Binary(&ref[115],&ref[99],V(17))
	move_ret ref[77]
	c_ret

;; TERM 115: tag_node_modulation
c_code local build_ref_115
	ret_reg &ref[115]
	call_c   Dyam_Create_Atom("tag_node_modulation")
	move_ret ref[115]
	c_ret

long local pool_fun34[4]=[131073,build_ref_77,pool_fun33,pool_fun32]

pl_code local fun34
	call_c   Dyam_Update_Choice(fun33)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[77])
	call_c   Dyam_Cut()
	pl_jump  fun32()

;; TERM 68: tag_node_modulation((_D / 0), _R)
c_code local build_ref_68
	ret_reg &ref[68]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_100()
	call_c   Dyam_Create_Binary(&ref[100],V(3),N(0))
	move_ret R(0)
	call_c   build_ref_115()
	call_c   Dyam_Create_Binary(&ref[115],R(0),V(17))
	move_ret ref[68]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun35[4]=[131073,build_ref_68,pool_fun34,pool_fun32]

long local pool_fun36[4]=[65538,build_ref_78,build_ref_67,pool_fun35]

pl_code local fun36
	call_c   Dyam_Remove_Choice()
	move     &ref[78], R(0)
	move     0, R(1)
	move     &ref[67], R(2)
	move     S(5), R(3)
	move     V(16), R(4)
	move     S(5), R(5)
	pl_call  pred_name_builder_3()
fun35:
	call_c   Dyam_Choice(fun34)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[68])
	call_c   Dyam_Cut()
	pl_jump  fun32()


;; TERM 66: '~w:#~w'
c_code local build_ref_66
	ret_reg &ref[66]
	call_c   Dyam_Create_Atom("~w:#~w")
	move_ret ref[66]
	c_ret

long local pool_fun37[6]=[131075,build_ref_64,build_ref_66,build_ref_67,pool_fun36,pool_fun35]

pl_code local fun37
	call_c   Dyam_Pool(pool_fun37)
	call_c   Dyam_Unify_Item(&ref[64])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun36)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_number(V(2))
	fail_ret
	call_c   Dyam_Cut()
	move     &ref[66], R(0)
	move     0, R(1)
	move     &ref[67], R(2)
	move     S(5), R(3)
	move     V(16), R(4)
	move     S(5), R(5)
	pl_call  pred_name_builder_3()
	pl_jump  fun35()

;; TERM 65: '*GUARD*'(make_tag_node_callret(_B, tag_node{id=> _C, label=> _D, children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}, _O, _P, _C)) :> []
c_code local build_ref_65
	ret_reg &ref[65]
	call_c   build_ref_64()
	call_c   Dyam_Create_Binary(I(9),&ref[64],I(0))
	move_ret ref[65]
	c_ret

c_code local build_seed_9
	ret_reg &seed[9]
	call_c   build_ref_27()
	call_c   build_ref_29()
	call_c   Dyam_Seed_Start(&ref[27],&ref[29],&ref[29],fun0,1)
	call_c   build_ref_32()
	call_c   Dyam_Seed_Add_Comp(&ref[32],&ref[29],0)
	call_c   Dyam_Seed_End()
	move_ret seed[9]
	c_ret

;; TERM 32: '*FIRST*'(make_true_tag_callret(_C, _D, _E, _F, _G, _H, _I)) :> '$$HOLE$$'
c_code local build_ref_32
	ret_reg &ref[32]
	call_c   build_ref_31()
	call_c   Dyam_Create_Binary(I(9),&ref[31],I(7))
	move_ret ref[32]
	c_ret

;; TERM 31: '*FIRST*'(make_true_tag_callret(_C, _D, _E, _F, _G, _H, _I))
c_code local build_ref_31
	ret_reg &ref[31]
	call_c   build_ref_30()
	call_c   build_ref_28()
	call_c   Dyam_Create_Unary(&ref[30],&ref[28])
	move_ret ref[31]
	c_ret

;; TERM 28: make_true_tag_callret(_C, _D, _E, _F, _G, _H, _I)
c_code local build_ref_28
	ret_reg &ref[28]
	call_c   build_ref_91()
	call_c   Dyam_Term_Start(&ref[91],7)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_End()
	move_ret ref[28]
	c_ret

;; TERM 29: '*CITEM*'(make_true_tag_callret(_C, _D, _E, _F, _G, _H, _I), make_true_tag_callret(_C, _D, _E, _F, _G, _H, _I))
c_code local build_ref_29
	ret_reg &ref[29]
	call_c   build_ref_27()
	call_c   build_ref_28()
	call_c   Dyam_Create_Binary(&ref[27],&ref[28],&ref[28])
	move_ret ref[29]
	c_ret

c_code local build_seed_13
	ret_reg &seed[13]
	call_c   build_ref_54()
	call_c   build_ref_57()
	call_c   Dyam_Seed_Start(&ref[54],&ref[57],I(0),fun18,1)
	call_c   build_ref_55()
	call_c   Dyam_Seed_Add_Comp(&ref[55],fun21,0)
	call_c   Dyam_Seed_End()
	move_ret seed[13]
	c_ret

;; TERM 55: '*RITEM*'(make_true_tag_callret(_C, _D, _E, _F, _G, _H, _I), voidret)
c_code local build_ref_55
	ret_reg &ref[55]
	call_c   build_ref_0()
	call_c   build_ref_28()
	call_c   build_ref_2()
	call_c   Dyam_Create_Binary(&ref[0],&ref[28],&ref[2])
	move_ret ref[55]
	c_ret

pl_code local fun21
	call_c   build_ref_55()
	call_c   Dyam_Unify_Item(&ref[55])
	fail_ret
	pl_jump  Follow_Cont(V(1))

;; TERM 57: '*RITEM*'(make_true_tag_callret(_C, _D, _E, _F, _G, _H, _I), voidret) :> tag_maker(0, '$TUPPLE'(35100049541364))
c_code local build_ref_57
	ret_reg &ref[57]
	call_c   build_ref_55()
	call_c   build_ref_56()
	call_c   Dyam_Create_Binary(I(9),&ref[55],&ref[56])
	move_ret ref[57]
	c_ret

;; TERM 56: tag_maker(0, '$TUPPLE'(35100049541364))
c_code local build_ref_56
	ret_reg &ref[56]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,134217728)
	move_ret R(0)
	call_c   build_ref_116()
	call_c   Dyam_Create_Binary(&ref[116],N(0),R(0))
	move_ret ref[56]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 116: tag_maker
c_code local build_ref_116
	ret_reg &ref[116]
	call_c   Dyam_Create_Atom("tag_maker")
	move_ret ref[116]
	c_ret

;; TERM 54: '*CURNEXT*'
c_code local build_ref_54
	ret_reg &ref[54]
	call_c   Dyam_Create_Atom("*CURNEXT*")
	move_ret ref[54]
	c_ret

;; TERM 1: make_true_tag_callret(_A, _B, _C, _D, _E, _F, _G)
c_code local build_ref_1
	ret_reg &ref[1]
	call_c   build_ref_91()
	call_c   Dyam_Term_Start(&ref[91],7)
	call_c   Dyam_Term_Arg(V(0))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[1]
	c_ret

;; TERM 3: '*RITEM*'(make_true_tag_callret(_A, _B, _C, _D, _E, _F, _G), voidret)
c_code local build_ref_3
	ret_reg &ref[3]
	call_c   build_ref_0()
	call_c   build_ref_1()
	call_c   build_ref_2()
	call_c   Dyam_Create_Binary(&ref[0],&ref[1],&ref[2])
	move_ret ref[3]
	c_ret

pl_code local fun6
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Object(R(0))
	call_c   Dyam_Tabule()
	pl_ret

pl_code local fun19
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Object(R(0))
	call_c   Dyam_Tabule()
	pl_jump  Schedule_Prolog()

pl_code local fun20
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Choice(fun19)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Subsume(R(0))
	fail_ret
	call_c   Dyam_Cut()
	pl_ret

pl_code local fun22
	call_c   Dyam_Backptr_Trace(R(1),R(2))
	call_c   Dyam_Light_Object(R(0))
	pl_jump  Schedule_Prolog()

pl_code local fun23
	call_c   Dyam_Remove_Choice()
	pl_call  fun22(&seed[13],2,V(9))
	call_c   Dyam_Deallocate()
	pl_ret


;;------------------ INIT POOL ------------

c_code local build_init_pool
	call_c   build_seed_2()
	call_c   build_seed_3()
	call_c   build_seed_4()
	call_c   build_seed_1()
	call_c   build_seed_0()
	call_c   build_seed_5()
	call_c   build_seed_9()
	call_c   build_seed_13()
	call_c   build_ref_1()
	call_c   build_ref_3()
	call_c   build_ref_28()
	c_ret

long local ref[117]
long local seed[15]

long local _initialization

c_code global initialization_dyalog_tag_5Fmaker
	call_c   Dyam_Check_And_Update_Initialization(_initialization)
	fail_c_ret
	call_c   initialization_dyalog_maker()
	call_c   initialization_dyalog_format()
	call_c   build_init_pool()
	call_c   build_viewers()
	call_c   load()
	c_ret

