;; Compiler: DyALog 1.14.0
;; File "tools.pl"


;;------------------ LOADING ------------

c_code local load
	call_c   Dyam_Loading_Set()
	pl_call  fun56(&seed[2],0)
	pl_call  fun52(&seed[0],0)
	pl_call  fun52(&seed[1],0)
	call_c   Dyam_Loading_Reset()
	c_ret

pl_code global pred_duplicate_vars_2
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(2)
	move     V(3), R(0)
	move     S(5), R(1)
	pl_call  pred_null_tupple_1()
	call_c   Dyam_Reg_Load_Ptr(2,V(4))
	fail_ret
	call_c   Dyam_Reg_Load(4,V(3))
	call_c   DyALog_Mutable_Write(R(2),&R(4),0)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(4))
	fail_ret
	call_c   Dyam_Reg_Load_Ptr(2,V(5))
	fail_ret
	call_c   Dyam_Reg_Load(4,V(3))
	call_c   DyALog_Mutable_Write(R(2),&R(4),0)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(5))
	fail_ret
	call_c   Dyam_Choice(fun91)
	pl_call  Domain_2(V(6),V(1))
	call_c   Dyam_Reg_Load(0,V(6))
	move     V(7), R(2)
	move     S(5), R(3)
	pl_call  pred_tupple_2()
	call_c   Dyam_Reg_Load_Ptr(2,V(4))
	fail_ret
	move     V(8), R(4)
	move     S(5), R(5)
	call_c   DyALog_Mutable_Read(R(2),&R(4))
	fail_ret
	call_c   Dyam_Reg_Load(0,V(7))
	call_c   Dyam_Reg_Load(2,V(8))
	move     V(9), R(4)
	move     S(5), R(5)
	pl_call  pred_inter_3()
	call_c   Dyam_Reg_Load_Ptr(2,V(5))
	fail_ret
	move     V(10), R(4)
	move     S(5), R(5)
	call_c   DyALog_Mutable_Read(R(2),&R(4))
	fail_ret
	call_c   Dyam_Reg_Load(0,V(9))
	call_c   Dyam_Reg_Load(2,V(10))
	move     V(11), R(4)
	move     S(5), R(5)
	pl_call  pred_union_3()
	call_c   Dyam_Reg_Load_Ptr(2,V(5))
	fail_ret
	call_c   Dyam_Reg_Load(4,V(11))
	call_c   DyALog_Mutable_Write(R(2),&R(4),0)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(5))
	fail_ret
	call_c   Dyam_Reg_Load(0,V(8))
	call_c   Dyam_Reg_Load(2,V(7))
	move     V(12), R(4)
	move     S(5), R(5)
	pl_call  pred_union_3()
	call_c   Dyam_Reg_Load_Ptr(2,V(4))
	fail_ret
	call_c   Dyam_Reg_Load(4,V(12))
	call_c   DyALog_Mutable_Write(R(2),&R(4),0)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(4))
	fail_ret
	pl_fail

pl_code global pred_add_ordered_table_3
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(3)
	call_c   Dyam_Choice(fun88)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[75])
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load_Ptr(2,V(6))
	fail_ret
	move     V(7), R(4)
	move     S(5), R(5)
	call_c   DyALog_Mutable_Read(R(2),&R(4))
	fail_ret
	call_c   Dyam_Choice(fun87)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Reg_Load_Ptr(2,V(7))
	fail_ret
	call_c   Dyam_Reg_Load(4,&ref[76])
	call_c   DyALog_Mutable_Read(R(2),&R(4))
	fail_ret
	call_c   Dyam_Cut()
fun86:
	call_c   Dyam_Reg_Load_Ptr(2,V(4))
	fail_ret
	call_c   DyALog_Mutable_Inc(R(2),&R(4))
	fail_ret
	call_c   Dyam_Reg_Unify_C_Number(4,V(3))
	fail_ret
	call_c   DYAM_evpred_assert(V(2),V(11))
	fail_ret
	call_c   Dyam_Reg_Load_Ptr(2,V(12))
	fail_ret
	move     I(0), R(4)
	move     0, R(5)
	call_c   DyALog_Mutable_Write(R(2),&R(4),0)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(12))
	fail_ret
	call_c   Dyam_Reg_Load_Ptr(2,V(10))
	fail_ret
	call_c   Dyam_Reg_Load(4,&ref[77])
	call_c   DyALog_Mutable_Write(R(2),&R(4),0)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(10))
	fail_ret
	call_c   Dyam_Reg_Load_Ptr(2,V(5))
	fail_ret
	move     V(13), R(4)
	move     S(5), R(5)
	call_c   DyALog_Mutable_Read(R(2),&R(4))
	fail_ret
	call_c   Dyam_Reg_Load_Ptr(2,V(6))
	fail_ret
	call_c   Dyam_Reg_Load(4,V(10))
	call_c   DyALog_Mutable_Write(R(2),&R(4),0)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(6))
	fail_ret
	call_c   Dyam_Reg_Deallocate(4)
	pl_ret


pl_code global pred_consume_ordered_table_2
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(2)
	call_c   Dyam_Choice(fun53)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[29])
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load_Ptr(2,V(4))
	fail_ret
	move     V(6), R(4)
	move     S(5), R(5)
	call_c   DyALog_Mutable_Read(R(2),&R(4))
	fail_ret
	call_c   Dyam_Reg_Load_Ptr(2,V(3))
	fail_ret
	move     N(0), R(4)
	move     0, R(5)
	call_c   DyALog_Mutable_Write(R(2),&R(4),0)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(3))
	fail_ret
	call_c   Dyam_Reg_Load_Ptr(2,V(7))
	fail_ret
	move     I(0), R(4)
	move     0, R(5)
	call_c   DyALog_Mutable_Write(R(2),&R(4),0)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(7))
	fail_ret
	call_c   Dyam_Reg_Load_Ptr(2,V(8))
	fail_ret
	move     I(0), R(4)
	move     0, R(5)
	call_c   DyALog_Mutable_Write(R(2),&R(4),0)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(8))
	fail_ret
	call_c   Dyam_Reg_Load_Ptr(2,V(4))
	fail_ret
	call_c   Dyam_Reg_Load(4,V(8))
	call_c   DyALog_Mutable_Write(R(2),&R(4),0)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(4))
	fail_ret
	call_c   Dyam_Reg_Load_Ptr(2,V(5))
	fail_ret
	call_c   Dyam_Reg_Load(4,V(8))
	call_c   DyALog_Mutable_Write(R(2),&R(4),0)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(5))
	fail_ret
	call_c   Dyam_Reg_Load(0,V(6))
	call_c   Dyam_Reg_Load(2,V(2))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_follow_ordered_table_2()

pl_code global pred_reset_ordered_table_1
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Choice(fun29)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[74])
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load_Ptr(2,V(2))
	fail_ret
	move     N(0), R(4)
	move     0, R(5)
	call_c   DyALog_Mutable_Write(R(2),&R(4),0)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(2))
	fail_ret
	call_c   Dyam_Reg_Load_Ptr(2,V(5))
	fail_ret
	move     I(0), R(4)
	move     0, R(5)
	call_c   DyALog_Mutable_Write(R(2),&R(4),0)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(5))
	fail_ret
	call_c   Dyam_Reg_Load_Ptr(2,V(6))
	fail_ret
	move     I(0), R(4)
	move     0, R(5)
	call_c   DyALog_Mutable_Write(R(2),&R(4),0)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(6))
	fail_ret
	call_c   Dyam_Reg_Load_Ptr(2,V(3))
	fail_ret
	call_c   Dyam_Reg_Load(4,V(6))
	call_c   DyALog_Mutable_Write(R(2),&R(4),0)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(3))
	fail_ret
	call_c   Dyam_Reg_Load_Ptr(2,V(4))
	fail_ret
	call_c   Dyam_Reg_Load(4,V(6))
	call_c   DyALog_Mutable_Write(R(2),&R(4),0)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(4))
	fail_ret
	call_c   Dyam_Reg_Deallocate(4)
	pl_ret

pl_code global pred_my_numbervars_3
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(3)
	call_c   Dyam_Choice(fun82)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_variable(V(2))
	fail_ret
	call_c   Dyam_Cut()
	call_c   DyALog_Gensym()
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Number(0,V(2))
	fail_ret
	call_c   DYAM_Numbervars_3(V(1),N(1),V(3))
	fail_ret
	call_c   Dyam_Reg_Load_Ptr(2,V(4))
	fail_ret
	call_c   Dyam_Reg_Load(4,V(3))
	call_c   DyALog_Mutable_Write(R(2),&R(4),0)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(4))
	fail_ret
	call_c   DYAM_evpred_assert_1(&ref[72])
	call_c   Dyam_Deallocate()
	pl_ret

pl_code global pred_gen_tail_2
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Unify(0,&ref[71])
	fail_ret
	call_c   Dyam_Reg_Bind(2,V(3))
	call_c   DYAM_evpred_univ(V(1),&ref[68])
	fail_ret
	call_c   Dyam_Reg_Load(0,V(4))
	move     V(6), R(2)
	move     S(5), R(3)
	pl_call  pred_tupple_2()
	call_c   Dyam_Reg_Load(0,V(2))
	call_c   Dyam_Reg_Load(2,V(6))
	move     V(7), R(4)
	move     S(5), R(5)
	pl_call  pred_inter_3()
	call_c   Dyam_Reg_Load(0,V(5))
	move     V(8), R(2)
	move     S(5), R(3)
	pl_call  pred_tupple_2()
	call_c   Dyam_Reg_Load(0,V(8))
	call_c   Dyam_Reg_Load(2,V(7))
	move     V(9), R(4)
	move     S(5), R(5)
	pl_call  pred_union_3()
	move     &ref[43], R(0)
	move     0, R(1)
	move     V(10), R(2)
	move     S(5), R(3)
	pl_call  pred_update_counter_2()
	pl_call  Object_1(&ref[69])
	call_c   DYAM_evpred_univ(V(3),&ref[70])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code global pred_deep_module_shift_3
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(3)
	call_c   DYAM_evpred_atom_to_module(V(1),V(4),V(5))
	fail_ret
	call_c   Dyam_Choice(fun79)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(4),I(0))
	fail_ret
	call_c   Dyam_Cut()
	call_c   DYAM_evpred_atom_to_module(V(3),V(2),V(1))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code global pred_reverse_ordered_table_1
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Choice(fun76)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[67])
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load_Ptr(2,V(2))
	fail_ret
	move     V(4), R(4)
	move     S(5), R(5)
	call_c   DyALog_Mutable_Read(R(2),&R(4))
	fail_ret
	call_c   Dyam_Reg_Load(0,V(4))
	move     I(0), R(2)
	move     0, R(3)
	move     V(5), R(4)
	move     S(5), R(5)
	pl_call  pred_reverse_3()
fun75:
	call_c   Dyam_Reg_Load_Ptr(2,V(2))
	fail_ret
	call_c   Dyam_Reg_Load(4,V(5))
	call_c   DyALog_Mutable_Write(R(2),&R(4),0)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(2))
	fail_ret
	call_c   Dyam_Reg_Deallocate(4)
	pl_ret


pl_code global pred_term_current_module_shift_4
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(4)
	call_c   DYAM_Module_Get_1(V(5))
	fail_ret
	call_c   Dyam_Choice(fun73)
	call_c   Dyam_Set_Cut()
	move     &ref[66], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Load(4,V(5))
	call_c   Dyam_Reg_Load(6,V(1))
	call_c   Dyam_Reg_Load(8,V(2))
	call_c   Dyam_Reg_Load(10,V(3))
	call_c   Dyam_Reg_Load(12,V(4))
	call_c   Dyam_Reg_Deallocate(7)
fun71:
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Reg_Bind(2,V(7))
	call_c   Dyam_Multi_Reg_Bind(4,2,5)
	call_c   Dyam_Choice(fun70)
	pl_call  fun62(&seed[4],1)
	pl_fail


pl_code global pred_reset_counter_2
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(2)
	call_c   Dyam_Choice(fun67)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[23])
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load_Ptr(2,V(3))
	fail_ret
	call_c   Dyam_Reg_Load(4,V(2))
	call_c   DyALog_Mutable_Write(R(2),&R(4),0)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(3))
	fail_ret
	call_c   Dyam_Reg_Deallocate(4)
	pl_ret

pl_code global pred_update_counter_2
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(2)
	call_c   Dyam_Choice(fun65)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[23])
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load_Ptr(2,V(3))
	fail_ret
	call_c   DyALog_Mutable_Inc(R(2),&R(4))
	fail_ret
	call_c   Dyam_Reg_Unify_C_Number(4,V(2))
	fail_ret
	call_c   Dyam_Reg_Deallocate(3)
	pl_ret

pl_code global pred_union_3
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(3)
	call_c   Dyam_Reg_Load(0,V(1))
	move     V(4), R(2)
	move     S(5), R(3)
	pl_call  pred_check_light_tupple_2()
	call_c   Dyam_Reg_Load(0,V(2))
	move     V(5), R(2)
	move     S(5), R(3)
	pl_call  pred_check_light_tupple_2()
	call_c   Dyam_Reg_Load(0,V(3))
	move     V(6), R(2)
	move     S(5), R(3)
	pl_call  pred_check_light_tupple_2()
	call_c   Dyam_Reg_Load(0,V(4))
	call_c   Dyam_Reg_Load(2,V(5))
	call_c   Dyam_Reg_Load(4,V(6))
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  pred_oset_union_3()

pl_code global pred_inter_3
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(3)
	call_c   Dyam_Reg_Load(0,V(1))
	move     V(4), R(2)
	move     S(5), R(3)
	pl_call  pred_check_light_tupple_2()
	call_c   Dyam_Reg_Load(0,V(2))
	move     V(5), R(2)
	move     S(5), R(3)
	pl_call  pred_check_light_tupple_2()
	call_c   Dyam_Reg_Load(0,V(3))
	move     V(6), R(2)
	move     S(5), R(3)
	pl_call  pred_check_light_tupple_2()
	call_c   Dyam_Reg_Load(0,V(4))
	call_c   Dyam_Reg_Load(2,V(5))
	call_c   Dyam_Reg_Load(4,V(6))
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  pred_oset_inter_3()

pl_code global pred_delete_3
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(3)
	call_c   Dyam_Reg_Load(0,V(1))
	move     V(4), R(2)
	move     S(5), R(3)
	pl_call  pred_check_light_tupple_2()
	call_c   Dyam_Reg_Load(0,V(2))
	move     V(5), R(2)
	move     S(5), R(3)
	pl_call  pred_check_light_tupple_2()
	call_c   Dyam_Reg_Load(0,V(3))
	move     V(6), R(2)
	move     S(5), R(3)
	pl_call  pred_check_light_tupple_2()
	call_c   Dyam_Reg_Load(0,V(4))
	call_c   Dyam_Reg_Load(2,V(5))
	call_c   Dyam_Reg_Load(4,V(6))
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  pred_oset_delete_3()

pl_code global pred_name_builder_3
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(3)
	call_c   Dyam_Reg_Load_String(2,V(4))
	fail_ret
	call_c   DyALog_Open_String_Stream(R(2))
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Number(0,V(5))
	fail_ret
	call_c   Dyam_Reg_Load(0,V(5))
	call_c   Dyam_Reg_Load(2,V(1))
	call_c   Dyam_Reg_Load(4,V(2))
	pl_call  pred_format_3()
	call_c   Dyam_Reg_Load_Output(2,V(5))
	fail_ret
	call_c   DyALog_Flush_String_Stream(R(2))
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_String(0,V(3))
	fail_ret
	call_c   DYAM_Close_1(V(5))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code global pred_ordered_table_2
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(2)
	call_c   Dyam_Choice(fun53)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[29])
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load_Ptr(2,V(4))
	fail_ret
	move     V(6), R(4)
	move     S(5), R(5)
	call_c   DyALog_Mutable_Read(R(2),&R(4))
	fail_ret
	call_c   Dyam_Reg_Load(0,V(6))
	call_c   Dyam_Reg_Load(2,V(2))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_follow_ordered_table_2()

pl_code global pred_new_gen_tail_4
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(4)
	call_c   Dyam_Choice(fun50)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[40])
	fail_ret
	call_c   Dyam_Cut()
fun49:
	call_c   DYAM_evpred_univ(V(6),&ref[41])
	fail_ret
	call_c   Dyam_Reg_Load(0,V(8))
	move     V(10), R(2)
	move     S(5), R(3)
	pl_call  pred_tupple_2()
	call_c   Dyam_Reg_Load(0,V(7))
	call_c   Dyam_Reg_Load(2,V(10))
	move     V(11), R(4)
	move     S(5), R(5)
	pl_call  pred_inter_3()
	call_c   Dyam_Reg_Load(0,V(9))
	move     V(12), R(2)
	move     S(5), R(3)
	pl_call  pred_tupple_2()
	call_c   Dyam_Reg_Load(0,V(12))
	call_c   Dyam_Reg_Load(2,V(11))
	move     V(13), R(4)
	move     S(5), R(5)
	pl_call  pred_union_3()
	call_c   Dyam_Reg_Load(0,V(2))
	move     V(14), R(2)
	move     S(5), R(3)
	pl_call  pred_tupple_2()
	call_c   Dyam_Reg_Load(0,V(13))
	call_c   Dyam_Reg_Load(2,V(14))
	move     V(15), R(4)
	move     S(5), R(5)
	pl_call  pred_delete_3()
	call_c   Dyam_Choice(fun48)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(8),&ref[42])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(16))
	move     V(18), R(2)
	move     S(5), R(3)
	pl_call  pred_tupple_2()
	call_c   Dyam_Reg_Load(0,V(15))
	call_c   Dyam_Reg_Load(2,V(18))
	move     V(19), R(4)
	move     S(5), R(5)
	pl_call  pred_delete_3()
fun47:
	move     &ref[43], R(0)
	move     0, R(1)
	move     V(20), R(2)
	move     S(5), R(3)
	pl_call  pred_update_counter_2()
	pl_call  Object_1(&ref[44])
	call_c   Dyam_Choice(fun46)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(5),I(0))
	fail_ret
	call_c   DYAM_sfol_identical(V(4),&ref[45])
	fail_ret
	call_c   Dyam_Cut()
	move     &ref[46], R(0)
	move     0, R(1)
	move     &ref[47], R(2)
	move     S(5), R(3)
	move     V(26), R(4)
	move     S(5), R(5)
	pl_call  pred_name_builder_3()
	call_c   Dyam_Choice(fun44)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Reg_Load(0,V(19))
	pl_call  pred_null_tupple_1()
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(3),V(26))
	fail_ret
fun43:
	call_c   Dyam_Reg_Load(0,V(19))
	move     V(27), R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_oset_tupple_2()




pl_code global pred_reverse_3
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(3)
	call_c   Dyam_Choice(fun41)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[38])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(5))
	move     &ref[39], R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Load(4,V(3))
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  pred_reverse_3()

pl_code global pred_extend_tupple_3
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Reg_Unify(2,&ref[14])
	fail_ret
	call_c   Dyam_Reg_Unify(4,&ref[37])
	fail_ret
	call_c   Dyam_Choice(fun38)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Reg_Load(2,V(1))
	call_c   Dyam_Reg_Load_Ptr(4,V(2))
	fail_ret
	call_c   Tupple_Extend(&R(2),R(4))
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(4))
	fail_ret
	call_c   Dyam_Cut()
fun37:
	call_c   Dyam_Unify(V(3),V(4))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret


pl_code global pred_tupple_delete_3
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Unify(0,&ref[6])
	fail_ret
	call_c   Dyam_Reg_Bind(2,V(2))
	call_c   Dyam_Reg_Unify(4,&ref[37])
	fail_ret
	call_c   Dyam_Choice(fun38)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Reg_Load(2,V(2))
	call_c   Dyam_Reg_Load_Ptr(4,V(1))
	fail_ret
	call_c   Tupple_Delete(&R(2),R(4))
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(4))
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun37()

pl_code global pred_follow_ordered_table_2
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(2)
	call_c   Dyam_Reg_Load_Ptr(2,V(1))
	fail_ret
	call_c   Dyam_Reg_Load(4,&ref[36])
	call_c   DyALog_Mutable_Read(R(2),&R(4))
	fail_ret
	call_c   Dyam_Choice(fun33)
	pl_call  Object_2(V(2),V(3))
	call_c   Dyam_Deallocate()
	pl_ret

pl_code global pred_length_ordered_table_2
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(2)
	call_c   Dyam_Choice(fun26)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[29])
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load_Ptr(2,V(3))
	fail_ret
	call_c   Dyam_Reg_Load(4,V(2))
	call_c   DyALog_Mutable_Read(R(2),&R(4))
	fail_ret
	call_c   Dyam_Reg_Deallocate(3)
	pl_ret

pl_code global pred_warning_2
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(2)
	call_c   Dyam_Choice(fun29)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[27])
	call_c   Dyam_Cut()
	call_c   DYAM_Write_2(N(2),&ref[28])
	fail_ret
	move     N(2), R(0)
	move     0, R(1)
	call_c   Dyam_Reg_Load(2,V(1))
	call_c   Dyam_Reg_Load(4,V(2))
	pl_call  pred_format_3()
	call_c   DYAM_Nl_1(N(2))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code global pred_default_module_2
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(2)
	pl_call  Object_1(&ref[24])
	move     &ref[25], R(0)
	move     0, R(1)
	move     &ref[26], R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Load(4,V(2))
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  pred_name_builder_3()

pl_code global pred_value_counter_2
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(2)
	call_c   Dyam_Choice(fun26)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[23])
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load_Ptr(2,V(3))
	fail_ret
	call_c   Dyam_Reg_Load(4,V(2))
	call_c   DyALog_Mutable_Read(R(2),&R(4))
	fail_ret
	call_c   Dyam_Reg_Deallocate(3)
	pl_ret

pl_code global pred_tupple_2
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Reg_Unify(2,&ref[14])
	fail_ret
	call_c   Dyam_Choice(fun24)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Reg_Load(2,V(1))
	call_c   Numbervar_Tupple_And_Register(&R(2))
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(3))
	fail_ret
	call_c   Dyam_Cut()
fun23:
	call_c   Dyam_Unify(V(3),V(2))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret


pl_code global pred_term_module_get_2
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(2)
	call_c   Dyam_Choice(fun21)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_functor(V(1),V(3),V(4))
	fail_ret
	call_c   DYAM_evpred_atom_to_module(V(3),V(2),V(5))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Deallocate()
	pl_ret

pl_code global pred_error_2
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(2)
	call_c   DYAM_Write_2(N(2),&ref[22])
	fail_ret
	move     N(2), R(0)
	move     0, R(1)
	call_c   Dyam_Reg_Load(2,V(1))
	call_c   Dyam_Reg_Load(4,V(2))
	pl_call  pred_format_3()
	call_c   DYAM_Nl_1(N(2))
	fail_ret
	call_c   DYAM_Exit_1(N(1))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code global pred_internal_error_2
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(2)
	call_c   DYAM_Write_2(N(2),&ref[21])
	fail_ret
	move     N(2), R(0)
	move     0, R(1)
	call_c   Dyam_Reg_Load(2,V(1))
	call_c   Dyam_Reg_Load(4,V(2))
	pl_call  pred_format_3()
	call_c   DYAM_Nl_1(N(2))
	fail_ret
	call_c   DYAM_Exit_1(N(1))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code global pred_check_light_tupple_2
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(2)
	call_c   Dyam_Choice(fun17)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[14])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Deallocate()
	pl_ret

pl_code global pred_oset_tupple_2
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(2)
	call_c   Dyam_Reg_Load(0,V(1))
	move     V(3), R(2)
	move     S(5), R(3)
	pl_call  pred_check_light_tupple_2()
	call_c   Dyam_Reg_Load_Ptr(2,V(3))
	fail_ret
	call_c   oset_numbervarlist(R(2))
	move_ret R(0)
	call_c   Dyam_Reg_Unify(0,V(2))
	fail_ret
	call_c   Dyam_Reg_Deallocate(2)
	pl_ret

pl_code global pred_foreign_arg_3
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(3)
	call_c   Dyam_Reg_Load(2,V(1))
	call_c   Dyam_Reg_Load(6,V(3))
	call_c   Dyam_Foreign_Create_Choice(LDyALog_Arg3_choice_1,3,2)
LDyALog_Arg3_choice_1:
	call_c   Dyam_Foreign_Update_Choice(LDyALog_Arg3_choice_1,3,2)
	call_c   DyALog_Arg(&R(2),&R(4),&R(6))
	fail_ret
	call_c   Dyam_Reg_Unify_C_Number(4,V(2))
	fail_ret
	call_c   Dyam_Reg_Deallocate(4)
	pl_ret

pl_code global pred_enumerate_3
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(3)
	call_c   Dyam_Reg_Load_Number(2,V(1))
	fail_ret
	call_c   Dyam_Reg_Load_Number(4,V(2))
	fail_ret
	call_c   Dyam_Foreign_Create_Choice(LDyALog_Count3_choice_0,3,0)
LDyALog_Count3_choice_0:
	call_c   Dyam_Foreign_Update_Choice(LDyALog_Count3_choice_0,3,0)
	call_c   DyALog_Count(R(2),R(4),&R(6))
	fail_ret
	call_c   Dyam_Reg_Unify_C_Number(6,V(3))
	fail_ret
	call_c   Dyam_Reg_Deallocate(4)
	pl_ret

pl_code global pred_record_without_doublon_1
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Choice(fun12)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(V(1))
	call_c   Dyam_Cut()
	call_c   Dyam_Deallocate()
	pl_ret

pl_code global pred_check_fun_tupple_1
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Unify(0,&ref[16])
	fail_ret
	call_c   DYAM_evpred_atom(V(1))
	fail_ret
	call_c   Dyam_Reg_Load(0,V(2))
	call_c   Dyam_Reg_Deallocate(1)
	pl_jump  pred_check_tupple_1()

pl_code global pred_abolish_1
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Unify(0,&ref[15])
	fail_ret
	call_c   Dyam_Reg_Load(2,V(1))
	call_c   Dyam_Reg_Load_Number(4,V(2))
	fail_ret
	call_c   Abolish(&R(2),R(4))
	call_c   Dyam_Reg_Deallocate(3)
	pl_ret

pl_code global pred_var_in_tupple_2
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Unify(0,&ref[7])
	fail_ret
	call_c   Dyam_Reg_Unify(2,&ref[14])
	fail_ret
	call_c   Dyam_Reg_Load_Number(2,V(1))
	fail_ret
	call_c   Dyam_Reg_Load_Ptr(4,V(2))
	fail_ret
	call_c   oset_member(R(2),R(4))
	fail_ret
	call_c   Dyam_Reg_Deallocate(3)
	pl_ret

pl_code global pred_check_deref_var_1
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Unify(0,&ref[8])
	fail_ret
	call_c   DYAM_evpred_number(V(1))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code global pred_holes_1
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   DYAM_Binder_2(V(1),I(7))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code global pred_check_var_1
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Unify(0,&ref[7])
	fail_ret
	call_c   DYAM_evpred_number(V(1))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code global pred_check_tupple_1
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Unify(0,&ref[6])
	fail_ret
	call_c   DYAM_evpred_number(V(1))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code global pred_zero_var_1
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Unify(0,&ref[7])
	fail_ret
	call_c   Dyam_Unify(V(1),N(0))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code global pred_null_tupple_1
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Unify(0,&ref[6])
	fail_ret
	call_c   Dyam_Unify(V(1),N(0))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code global pred_reg_value_2
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Unify(0,&ref[5])
	fail_ret
	call_c   Dyam_Reg_Unify(2,V(1))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret


;;------------------ VIEWERS ------------

c_code local build_viewers
	call_c   Dyam_Load_Viewer(&ref[3],&ref[4])
	c_ret

c_code local build_seed_1
	ret_reg &seed[1]
	call_c   build_ref_9()
	call_c   build_ref_13()
	call_c   Dyam_Seed_Start(&ref[9],&ref[13],I(0),fun8,1)
	call_c   build_ref_12()
	call_c   Dyam_Seed_Add_Comp(&ref[12],fun7,0)
	call_c   Dyam_Seed_End()
	move_ret seed[1]
	c_ret

;; TERM 12: '*GUARD*'(append([], _B, _B))
c_code local build_ref_12
	ret_reg &ref[12]
	call_c   build_ref_10()
	call_c   build_ref_11()
	call_c   Dyam_Create_Unary(&ref[10],&ref[11])
	move_ret ref[12]
	c_ret

;; TERM 11: append([], _B, _B)
c_code local build_ref_11
	ret_reg &ref[11]
	call_c   build_ref_78()
	call_c   Dyam_Term_Start(&ref[78],3)
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_End()
	move_ret ref[11]
	c_ret

;; TERM 78: append
c_code local build_ref_78
	ret_reg &ref[78]
	call_c   Dyam_Create_Atom("append")
	move_ret ref[78]
	c_ret

;; TERM 10: '*GUARD*'
c_code local build_ref_10
	ret_reg &ref[10]
	call_c   Dyam_Create_Atom("*GUARD*")
	move_ret ref[10]
	c_ret

pl_code local fun7
	call_c   build_ref_12()
	call_c   Dyam_Unify_Item(&ref[12])
	fail_ret
	pl_ret

pl_code local fun8
	pl_ret

;; TERM 13: '*GUARD*'(append([], _B, _B)) :> []
c_code local build_ref_13
	ret_reg &ref[13]
	call_c   build_ref_12()
	call_c   Dyam_Create_Binary(I(9),&ref[12],I(0))
	move_ret ref[13]
	c_ret

;; TERM 9: '*GUARD_CLAUSE*'
c_code local build_ref_9
	ret_reg &ref[9]
	call_c   Dyam_Create_Atom("*GUARD_CLAUSE*")
	move_ret ref[9]
	c_ret

c_code local build_seed_0
	ret_reg &seed[0]
	call_c   build_ref_9()
	call_c   build_ref_32()
	call_c   Dyam_Seed_Start(&ref[9],&ref[32],I(0),fun8,1)
	call_c   build_ref_31()
	call_c   Dyam_Seed_Add_Comp(&ref[31],fun36,0)
	call_c   Dyam_Seed_End()
	move_ret seed[0]
	c_ret

;; TERM 31: '*GUARD*'(append([_B|_C], _D, [_B|_E]))
c_code local build_ref_31
	ret_reg &ref[31]
	call_c   build_ref_10()
	call_c   build_ref_30()
	call_c   Dyam_Create_Unary(&ref[10],&ref[30])
	move_ret ref[31]
	c_ret

;; TERM 30: append([_B|_C], _D, [_B|_E])
c_code local build_ref_30
	ret_reg &ref[30]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   Dyam_Create_List(V(1),V(2))
	move_ret R(0)
	call_c   Dyam_Create_List(V(1),V(4))
	move_ret R(1)
	call_c   build_ref_78()
	call_c   Dyam_Term_Start(&ref[78],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_End()
	move_ret ref[30]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_3
	ret_reg &seed[3]
	call_c   build_ref_10()
	call_c   build_ref_34()
	call_c   Dyam_Seed_Start(&ref[10],&ref[34],I(0),fun32,1)
	call_c   build_ref_35()
	call_c   Dyam_Seed_Add_Comp(&ref[35],&ref[34],0)
	call_c   Dyam_Seed_End()
	move_ret seed[3]
	c_ret

pl_code local fun32
	pl_jump  Complete(0,0)

;; TERM 35: '*GUARD*'(append(_C, _D, _E)) :> '$$HOLE$$'
c_code local build_ref_35
	ret_reg &ref[35]
	call_c   build_ref_34()
	call_c   Dyam_Create_Binary(I(9),&ref[34],I(7))
	move_ret ref[35]
	c_ret

;; TERM 34: '*GUARD*'(append(_C, _D, _E))
c_code local build_ref_34
	ret_reg &ref[34]
	call_c   build_ref_10()
	call_c   build_ref_33()
	call_c   Dyam_Create_Unary(&ref[10],&ref[33])
	move_ret ref[34]
	c_ret

;; TERM 33: append(_C, _D, _E)
c_code local build_ref_33
	ret_reg &ref[33]
	call_c   build_ref_78()
	call_c   Dyam_Term_Start(&ref[78],3)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[33]
	c_ret

pl_code local fun35
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Light_Object(R(0))
	pl_jump  Schedule_Prolog()

long local pool_fun36[3]=[2,build_ref_31,build_seed_3]

pl_code local fun36
	call_c   Dyam_Pool(pool_fun36)
	call_c   Dyam_Unify_Item(&ref[31])
	fail_ret
	pl_jump  fun35(&seed[3],1)

;; TERM 32: '*GUARD*'(append([_B|_C], _D, [_B|_E])) :> []
c_code local build_ref_32
	ret_reg &ref[32]
	call_c   build_ref_31()
	call_c   Dyam_Create_Binary(I(9),&ref[31],I(0))
	move_ret ref[32]
	c_ret

c_code local build_seed_2
	ret_reg &seed[2]
	call_c   build_ref_17()
	call_c   build_ref_18()
	call_c   Dyam_Seed_Start(&ref[17],&ref[18],&ref[18],fun8,0)
	call_c   Dyam_Seed_End()
	move_ret seed[2]
	c_ret

;; TERM 18: tupple_delete_wrapper('$TUPPLE'(_B), _C, '$TUPPLE'(_B, _C))
c_code local build_ref_18
	ret_reg &ref[18]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Binary(I(10),V(1),V(2))
	move_ret R(0)
	call_c   build_ref_79()
	call_c   build_ref_6()
	call_c   Dyam_Term_Start(&ref[79],3)
	call_c   Dyam_Term_Arg(&ref[6])
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_End()
	move_ret ref[18]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 6: '$TUPPLE'(_B)
c_code local build_ref_6
	ret_reg &ref[6]
	call_c   Dyam_Create_Unary(I(10),V(1))
	move_ret ref[6]
	c_ret

;; TERM 79: tupple_delete_wrapper
c_code local build_ref_79
	ret_reg &ref[79]
	call_c   Dyam_Create_Atom("tupple_delete_wrapper")
	move_ret ref[79]
	c_ret

;; TERM 17: '*DATABASE*'
c_code local build_ref_17
	ret_reg &ref[17]
	call_c   Dyam_Create_Atom("*DATABASE*")
	move_ret ref[17]
	c_ret

c_code local build_seed_4
	ret_reg &seed[4]
	call_c   build_ref_52()
	call_c   build_ref_54()
	call_c   Dyam_Seed_Start(&ref[52],&ref[54],&ref[54],fun32,1)
	call_c   build_ref_57()
	call_c   Dyam_Seed_Add_Comp(&ref[57],&ref[54],0)
	call_c   Dyam_Seed_End()
	move_ret seed[4]
	c_ret

;; TERM 57: '*FIRST*'('call_term_module_shift/5'(_C, _D)) :> '$$HOLE$$'
c_code local build_ref_57
	ret_reg &ref[57]
	call_c   build_ref_56()
	call_c   Dyam_Create_Binary(I(9),&ref[56],I(7))
	move_ret ref[57]
	c_ret

;; TERM 56: '*FIRST*'('call_term_module_shift/5'(_C, _D))
c_code local build_ref_56
	ret_reg &ref[56]
	call_c   build_ref_55()
	call_c   build_ref_53()
	call_c   Dyam_Create_Unary(&ref[55],&ref[53])
	move_ret ref[56]
	c_ret

;; TERM 53: 'call_term_module_shift/5'(_C, _D)
c_code local build_ref_53
	ret_reg &ref[53]
	call_c   build_ref_80()
	call_c   Dyam_Create_Binary(&ref[80],V(2),V(3))
	move_ret ref[53]
	c_ret

;; TERM 80: 'call_term_module_shift/5'
c_code local build_ref_80
	ret_reg &ref[80]
	call_c   Dyam_Create_Atom("call_term_module_shift/5")
	move_ret ref[80]
	c_ret

;; TERM 55: '*FIRST*'
c_code local build_ref_55
	ret_reg &ref[55]
	call_c   Dyam_Create_Atom("*FIRST*")
	move_ret ref[55]
	c_ret

;; TERM 54: '*CITEM*'('call_term_module_shift/5'(_C, _D), 'call_term_module_shift/5'(_C, _D))
c_code local build_ref_54
	ret_reg &ref[54]
	call_c   build_ref_52()
	call_c   build_ref_53()
	call_c   Dyam_Create_Binary(&ref[52],&ref[53],&ref[53])
	move_ret ref[54]
	c_ret

;; TERM 52: '*CITEM*'
c_code local build_ref_52
	ret_reg &ref[52]
	call_c   Dyam_Create_Atom("*CITEM*")
	move_ret ref[52]
	c_ret

c_code local build_seed_5
	ret_reg &seed[5]
	call_c   build_ref_58()
	call_c   build_ref_62()
	call_c   Dyam_Seed_Start(&ref[58],&ref[62],I(0),fun64,1)
	call_c   build_ref_60()
	call_c   Dyam_Seed_Add_Comp(&ref[60],fun63,0)
	call_c   Dyam_Seed_End()
	move_ret seed[5]
	c_ret

;; TERM 60: '*RITEM*'('call_term_module_shift/5'(_C, _D), return(_E, _F, _G))
c_code local build_ref_60
	ret_reg &ref[60]
	call_c   build_ref_0()
	call_c   build_ref_53()
	call_c   build_ref_59()
	call_c   Dyam_Create_Binary(&ref[0],&ref[53],&ref[59])
	move_ret ref[60]
	c_ret

;; TERM 59: return(_E, _F, _G)
c_code local build_ref_59
	ret_reg &ref[59]
	call_c   build_ref_81()
	call_c   Dyam_Term_Start(&ref[81],3)
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[59]
	c_ret

;; TERM 81: return
c_code local build_ref_81
	ret_reg &ref[81]
	call_c   Dyam_Create_Atom("return")
	move_ret ref[81]
	c_ret

;; TERM 0: '*RITEM*'
c_code local build_ref_0
	ret_reg &ref[0]
	call_c   Dyam_Create_Atom("*RITEM*")
	move_ret ref[0]
	c_ret

pl_code local fun63
	call_c   build_ref_60()
	call_c   Dyam_Unify_Item(&ref[60])
	fail_ret
	pl_jump  Follow_Cont(V(1))

pl_code local fun64
	pl_jump  Apply(0,0)

;; TERM 62: '*RITEM*'('call_term_module_shift/5'(_C, _D), return(_E, _F, _G)) :> tools(0, '$TUPPLE'(35108576038180))
c_code local build_ref_62
	ret_reg &ref[62]
	call_c   build_ref_60()
	call_c   build_ref_61()
	call_c   Dyam_Create_Binary(I(9),&ref[60],&ref[61])
	move_ret ref[62]
	c_ret

;; TERM 61: tools(0, '$TUPPLE'(35108576038180))
c_code local build_ref_61
	ret_reg &ref[61]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,134217728)
	move_ret R(0)
	call_c   build_ref_82()
	call_c   Dyam_Create_Binary(&ref[82],N(0),R(0))
	move_ret ref[61]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 82: tools
c_code local build_ref_82
	ret_reg &ref[82]
	call_c   Dyam_Create_Atom("tools")
	move_ret ref[82]
	c_ret

;; TERM 58: '*CURNEXT*'
c_code local build_ref_58
	ret_reg &ref[58]
	call_c   Dyam_Create_Atom("*CURNEXT*")
	move_ret ref[58]
	c_ret

;; TERM 4: term_module_shift(_A, _B, _C, _D, _E)
c_code local build_ref_4
	ret_reg &ref[4]
	call_c   build_ref_83()
	call_c   Dyam_Term_Start(&ref[83],5)
	call_c   Dyam_Term_Arg(V(0))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[4]
	c_ret

;; TERM 83: term_module_shift
c_code local build_ref_83
	ret_reg &ref[83]
	call_c   Dyam_Create_Atom("term_module_shift")
	move_ret ref[83]
	c_ret

;; TERM 3: '*RITEM*'('call_term_module_shift/5'(_A, _B), return(_C, _D, _E))
c_code local build_ref_3
	ret_reg &ref[3]
	call_c   build_ref_0()
	call_c   build_ref_1()
	call_c   build_ref_2()
	call_c   Dyam_Create_Binary(&ref[0],&ref[1],&ref[2])
	move_ret ref[3]
	c_ret

;; TERM 2: return(_C, _D, _E)
c_code local build_ref_2
	ret_reg &ref[2]
	call_c   build_ref_81()
	call_c   Dyam_Term_Start(&ref[81],3)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[2]
	c_ret

;; TERM 1: 'call_term_module_shift/5'(_A, _B)
c_code local build_ref_1
	ret_reg &ref[1]
	call_c   build_ref_80()
	call_c   Dyam_Create_Binary(&ref[80],V(0),V(1))
	move_ret ref[1]
	c_ret

;; TERM 76: elem(_I, _J, _K)
c_code local build_ref_76
	ret_reg &ref[76]
	call_c   build_ref_84()
	call_c   Dyam_Term_Start(&ref[84],3)
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_End()
	move_ret ref[76]
	c_ret

;; TERM 84: elem
c_code local build_ref_84
	ret_reg &ref[84]
	call_c   Dyam_Create_Atom("elem")
	move_ret ref[84]
	c_ret

;; TERM 77: elem(_L, _H, _M)
c_code local build_ref_77
	ret_reg &ref[77]
	call_c   build_ref_84()
	call_c   Dyam_Term_Start(&ref[84],3)
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_End()
	move_ret ref[77]
	c_ret

;; TERM 75: ordered_table(_B, _E, _F, _G)
c_code local build_ref_75
	ret_reg &ref[75]
	call_c   build_ref_85()
	call_c   Dyam_Term_Start(&ref[85],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[75]
	c_ret

;; TERM 85: ordered_table
c_code local build_ref_85
	ret_reg &ref[85]
	call_c   Dyam_Create_Atom("ordered_table")
	move_ret ref[85]
	c_ret

;; TERM 74: ordered_table(_B, _C, _D, _E)
c_code local build_ref_74
	ret_reg &ref[74]
	call_c   build_ref_85()
	call_c   Dyam_Term_Start(&ref[85],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[74]
	c_ret

;; TERM 72: numbervars(_C, _E)
c_code local build_ref_72
	ret_reg &ref[72]
	call_c   build_ref_86()
	call_c   Dyam_Create_Binary(&ref[86],V(2),V(4))
	move_ret ref[72]
	c_ret

;; TERM 86: numbervars
c_code local build_ref_86
	ret_reg &ref[86]
	call_c   Dyam_Create_Atom("numbervars")
	move_ret ref[86]
	c_ret

;; TERM 73: numbervars(_C, _F)
c_code local build_ref_73
	ret_reg &ref[73]
	call_c   build_ref_86()
	call_c   Dyam_Create_Binary(&ref[86],V(2),V(5))
	move_ret ref[73]
	c_ret

;; TERM 70: [_N,_K,_J]
c_code local build_ref_70
	ret_reg &ref[70]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(9),I(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(10),R(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(13),R(0))
	move_ret ref[70]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 69: main_file(_L, info(_M, _N, _O, _P))
c_code local build_ref_69
	ret_reg &ref[69]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_88()
	call_c   Dyam_Term_Start(&ref[88],4)
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_87()
	call_c   Dyam_Create_Binary(&ref[87],V(11),R(0))
	move_ret ref[69]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 87: main_file
c_code local build_ref_87
	ret_reg &ref[87]
	call_c   Dyam_Create_Atom("main_file")
	move_ret ref[87]
	c_ret

;; TERM 88: info
c_code local build_ref_88
	ret_reg &ref[88]
	call_c   Dyam_Create_Atom("info")
	move_ret ref[88]
	c_ret

;; TERM 68: [apply,_E|_F]
c_code local build_ref_68
	ret_reg &ref[68]
	call_c   build_ref_38()
	call_c   Dyam_Create_List(I(3),&ref[38])
	move_ret ref[68]
	c_ret

;; TERM 38: [_E|_F]
c_code local build_ref_38
	ret_reg &ref[38]
	call_c   Dyam_Create_List(V(4),V(5))
	move_ret ref[38]
	c_ret

;; TERM 71: _B(_C)
c_code local build_ref_71
	ret_reg &ref[71]
	call_c   Dyam_Create_Binary(I(3),V(1),V(2))
	move_ret ref[71]
	c_ret

;; TERM 67: ordered_table(_B, _C, _D)
c_code local build_ref_67
	ret_reg &ref[67]
	call_c   build_ref_85()
	call_c   Dyam_Term_Start(&ref[85],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[67]
	c_ret

;; TERM 66: '$CLOSURE'('$fun'(72, 0, 1121005760), '$TUPPLE'(0))
c_code local build_ref_66
	ret_reg &ref[66]
	call_c   Dyam_Closure_Aux(fun72,I(0))
	move_ret ref[66]
	c_ret

pl_code local fun72
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Cut()
	call_c   Dyam_Deallocate()
	pl_ret

;; TERM 40: _F ^ _G(_H)
c_code local build_ref_40
	ret_reg &ref[40]
	call_c   build_ref_89()
	call_c   build_ref_51()
	call_c   Dyam_Create_Binary(&ref[89],V(5),&ref[51])
	move_ret ref[40]
	c_ret

;; TERM 51: _G(_H)
c_code local build_ref_51
	ret_reg &ref[51]
	call_c   Dyam_Create_Binary(I(3),V(6),V(7))
	move_ret ref[51]
	c_ret

;; TERM 89: ^
c_code local build_ref_89
	ret_reg &ref[89]
	call_c   Dyam_Create_Atom("^")
	move_ret ref[89]
	c_ret

;; TERM 42: '*HIDE-ARGS*'(_Q, _R)
c_code local build_ref_42
	ret_reg &ref[42]
	call_c   build_ref_90()
	call_c   Dyam_Create_Binary(&ref[90],V(16),V(17))
	move_ret ref[42]
	c_ret

;; TERM 90: '*HIDE-ARGS*'
c_code local build_ref_90
	ret_reg &ref[90]
	call_c   Dyam_Create_Atom("*HIDE-ARGS*")
	move_ret ref[90]
	c_ret

;; TERM 48: '$FUNTUPPLE'(_A1, _T)
c_code local build_ref_48
	ret_reg &ref[48]
	call_c   build_ref_91()
	call_c   Dyam_Create_Binary(&ref[91],V(26),V(19))
	move_ret ref[48]
	c_ret

;; TERM 91: '$FUNTUPPLE'
c_code local build_ref_91
	ret_reg &ref[91]
	call_c   Dyam_Create_Atom("$FUNTUPPLE")
	move_ret ref[91]
	c_ret

;; TERM 47: [_X,_U]
c_code local build_ref_47
	ret_reg &ref[47]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(20),I(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(23),R(0))
	move_ret ref[47]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 46: '~w_~w'
c_code local build_ref_46
	ret_reg &ref[46]
	call_c   Dyam_Create_Atom("~w_~w")
	move_ret ref[46]
	c_ret

;; TERM 45: flat
c_code local build_ref_45
	ret_reg &ref[45]
	call_c   Dyam_Create_Atom("flat")
	move_ret ref[45]
	c_ret

;; TERM 49: [_X,_U,_T]
c_code local build_ref_49
	ret_reg &ref[49]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(19),I(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(20),R(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(23),R(0))
	move_ret ref[49]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 50: [_X,_U,_F,_T]
c_code local build_ref_50
	ret_reg &ref[50]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(19),I(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(5),R(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(20),R(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(23),R(0))
	move_ret ref[50]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 44: main_file(_V, info(_W, _X, _Y, _Z))
c_code local build_ref_44
	ret_reg &ref[44]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_88()
	call_c   Dyam_Term_Start(&ref[88],4)
	call_c   Dyam_Term_Arg(V(22))
	call_c   Dyam_Term_Arg(V(23))
	call_c   Dyam_Term_Arg(V(24))
	call_c   Dyam_Term_Arg(V(25))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_87()
	call_c   Dyam_Create_Binary(&ref[87],V(21),R(0))
	move_ret ref[44]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 43: nabla
c_code local build_ref_43
	ret_reg &ref[43]
	call_c   Dyam_Create_Atom("nabla")
	move_ret ref[43]
	c_ret

;; TERM 41: [apply,_I|_J]
c_code local build_ref_41
	ret_reg &ref[41]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(8),V(9))
	move_ret R(0)
	call_c   Dyam_Create_List(I(3),R(0))
	move_ret ref[41]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 39: [_E|_C]
c_code local build_ref_39
	ret_reg &ref[39]
	call_c   Dyam_Create_List(V(4),V(2))
	move_ret ref[39]
	c_ret

;; TERM 37: '$TUPPLE'(_D)
c_code local build_ref_37
	ret_reg &ref[37]
	call_c   Dyam_Create_Unary(I(10),V(3))
	move_ret ref[37]
	c_ret

;; TERM 36: elem(_D, _E, _F)
c_code local build_ref_36
	ret_reg &ref[36]
	call_c   build_ref_84()
	call_c   Dyam_Term_Start(&ref[84],3)
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[36]
	c_ret

;; TERM 29: ordered_table(_B, _D, _E, _F)
c_code local build_ref_29
	ret_reg &ref[29]
	call_c   build_ref_85()
	call_c   Dyam_Term_Start(&ref[85],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[29]
	c_ret

;; TERM 28: 'dyalog: warning : '
c_code local build_ref_28
	ret_reg &ref[28]
	call_c   Dyam_Create_Atom("dyalog: warning : ")
	move_ret ref[28]
	c_ret

;; TERM 27: warning
c_code local build_ref_27
	ret_reg &ref[27]
	call_c   Dyam_Create_Atom("warning")
	move_ret ref[27]
	c_ret

;; TERM 26: [_H,_B]
c_code local build_ref_26
	ret_reg &ref[26]
	call_c   build_ref_20()
	call_c   Dyam_Create_List(V(7),&ref[20])
	move_ret ref[26]
	c_ret

;; TERM 20: [_B]
c_code local build_ref_20
	ret_reg &ref[20]
	call_c   Dyam_Create_List(V(1),I(0))
	move_ret ref[20]
	c_ret

;; TERM 25: '~w~w'
c_code local build_ref_25
	ret_reg &ref[25]
	call_c   Dyam_Create_Atom("~w~w")
	move_ret ref[25]
	c_ret

;; TERM 24: main_file(_D, info(_E, _F, _G, _H))
c_code local build_ref_24
	ret_reg &ref[24]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_88()
	call_c   Dyam_Term_Start(&ref[88],4)
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_87()
	call_c   Dyam_Create_Binary(&ref[87],V(3),R(0))
	move_ret ref[24]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 23: counter(_B, _D)
c_code local build_ref_23
	ret_reg &ref[23]
	call_c   build_ref_92()
	call_c   Dyam_Create_Binary(&ref[92],V(1),V(3))
	move_ret ref[23]
	c_ret

;; TERM 92: counter
c_code local build_ref_92
	ret_reg &ref[92]
	call_c   Dyam_Create_Atom("counter")
	move_ret ref[92]
	c_ret

;; TERM 22: 'dyalog: '
c_code local build_ref_22
	ret_reg &ref[22]
	call_c   Dyam_Create_Atom("dyalog: ")
	move_ret ref[22]
	c_ret

;; TERM 21: 'dyalog: internal error : '
c_code local build_ref_21
	ret_reg &ref[21]
	call_c   Dyam_Create_Atom("dyalog: internal error : ")
	move_ret ref[21]
	c_ret

;; TERM 19: 'Tupple expected in compact form: ~w'
c_code local build_ref_19
	ret_reg &ref[19]
	call_c   Dyam_Create_Atom("Tupple expected in compact form: ~w")
	move_ret ref[19]
	c_ret

;; TERM 16: '$FUNTUPPLE'(_B, _C)
c_code local build_ref_16
	ret_reg &ref[16]
	call_c   build_ref_91()
	call_c   Dyam_Create_Binary(&ref[91],V(1),V(2))
	move_ret ref[16]
	c_ret

;; TERM 15: _B / _C
c_code local build_ref_15
	ret_reg &ref[15]
	call_c   build_ref_93()
	call_c   Dyam_Create_Binary(&ref[93],V(1),V(2))
	move_ret ref[15]
	c_ret

;; TERM 93: /
c_code local build_ref_93
	ret_reg &ref[93]
	call_c   Dyam_Create_Atom("/")
	move_ret ref[93]
	c_ret

;; TERM 14: '$TUPPLE'(_C)
c_code local build_ref_14
	ret_reg &ref[14]
	call_c   Dyam_Create_Unary(I(10),V(2))
	move_ret ref[14]
	c_ret

;; TERM 8: '$VAR'(_Y43080004, _C)
c_code local build_ref_8
	ret_reg &ref[8]
	call_c   build_ref_7()
	call_c   Dyam_Create_Binary(I(6),&ref[7],V(2))
	move_ret ref[8]
	c_ret

;; TERM 7: _Y43080004
c_code local build_ref_7
	ret_reg &ref[7]
	call_c   Dyam_Create_Unary(I(6),V(1))
	move_ret ref[7]
	c_ret

;; TERM 5: '&reg'(_B)
c_code local build_ref_5
	ret_reg &ref[5]
	call_c   build_ref_94()
	call_c   Dyam_Create_Unary(&ref[94],V(1))
	move_ret ref[5]
	c_ret

;; TERM 94: '&reg'
c_code local build_ref_94
	ret_reg &ref[94]
	call_c   Dyam_Create_Atom("&reg")
	move_ret ref[94]
	c_ret

pl_code local fun52
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Object(R(0))
	call_c   Dyam_Tabule()
	pl_ret

pl_code local fun55
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Object(R(0))
	call_c   Dyam_Tabule()
	pl_ret

pl_code local fun56
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Choice(fun55)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Subsume(R(0))
	fail_ret
	call_c   Dyam_Cut()
	pl_ret

pl_code local fun91
	call_c   Dyam_Remove_Choice()
fun90:
	call_c   Dyam_Reg_Load_Ptr(2,V(5))
	fail_ret
	call_c   Dyam_Reg_Load(4,V(2))
	call_c   DyALog_Mutable_Read(R(2),&R(4))
	fail_ret
	call_c   Dyam_Reg_Deallocate(3)
	pl_ret


pl_code local fun87
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(7),V(10))
	fail_ret
	pl_jump  fun86()

pl_code local fun88
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Reg_Load_Ptr(2,V(4))
	fail_ret
	move     N(0), R(4)
	move     0, R(5)
	call_c   DyALog_Mutable_Write(R(2),&R(4),0)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(4))
	fail_ret
	call_c   Dyam_Reg_Load_Ptr(2,V(7))
	fail_ret
	move     I(0), R(4)
	move     0, R(5)
	call_c   DyALog_Mutable_Write(R(2),&R(4),0)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(7))
	fail_ret
	call_c   Dyam_Reg_Load_Ptr(2,V(10))
	fail_ret
	move     I(0), R(4)
	move     0, R(5)
	call_c   DyALog_Mutable_Write(R(2),&R(4),0)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(10))
	fail_ret
	call_c   Dyam_Reg_Load_Ptr(2,V(5))
	fail_ret
	call_c   Dyam_Reg_Load(4,V(10))
	call_c   DyALog_Mutable_Write(R(2),&R(4),0)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(5))
	fail_ret
	call_c   Dyam_Reg_Load_Ptr(2,V(6))
	fail_ret
	call_c   Dyam_Reg_Load(4,V(10))
	call_c   DyALog_Mutable_Write(R(2),&R(4),0)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(6))
	fail_ret
	call_c   DYAM_evpred_assert_1(&ref[75])
	pl_jump  fun86()

pl_code local fun82
	call_c   Dyam_Remove_Choice()
	pl_call  Object_1(&ref[73])
	call_c   Dyam_Reg_Load_Ptr(2,V(5))
	fail_ret
	move     V(6), R(4)
	move     S(5), R(5)
	call_c   DyALog_Mutable_Read(R(2),&R(4))
	fail_ret
	call_c   DYAM_Numbervars_3(V(1),V(6),V(3))
	fail_ret
	call_c   Dyam_Reg_Load_Ptr(2,V(5))
	fail_ret
	call_c   Dyam_Reg_Load(4,V(3))
	call_c   DyALog_Mutable_Write(R(2),&R(4),0)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(5))
	fail_ret
	call_c   Dyam_Reg_Deallocate(4)
	pl_ret

pl_code local fun78
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Reg_Load(0,V(4))
	call_c   Dyam_Reg_Load(2,V(2))
	move     V(6), R(4)
	move     S(5), R(5)
	pl_call  pred_deep_module_shift_3()
	call_c   DYAM_evpred_atom_to_module(V(3),V(6),V(5))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun79
	call_c   Dyam_Update_Choice(fun78)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(2),V(4))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(3),V(1))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun76
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(6),N(0))
	fail_ret
	call_c   Dyam_Reg_Load_Ptr(2,V(3))
	fail_ret
	call_c   Dyam_Reg_Load(4,V(6))
	call_c   DyALog_Mutable_Write(R(2),&R(4),0)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(3))
	fail_ret
	call_c   Dyam_Unify(V(5),I(0))
	fail_ret
	pl_jump  fun75()

pl_code local fun73
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(3),V(4))
	fail_ret
	call_c   Dyam_Unify(V(1),V(2))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun61
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Object(R(0))
	call_c   Dyam_Tabule()
	pl_jump  Schedule_Prolog()

pl_code local fun62
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Choice(fun61)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Subsume(R(0))
	fail_ret
	call_c   Dyam_Cut()
	pl_ret

pl_code local fun69
	call_c   Dyam_Backptr_Trace(R(1),R(2))
	call_c   Dyam_Light_Object(R(0))
	pl_jump  Schedule_Prolog()

pl_code local fun70
	call_c   Dyam_Remove_Choice()
	pl_call  fun69(&seed[5],2,V(7))
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun67
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Reg_Load_Ptr(2,V(3))
	fail_ret
	call_c   Dyam_Reg_Load(4,V(2))
	call_c   DyALog_Mutable_Write(R(2),&R(4),0)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(3))
	fail_ret
	call_c   DYAM_evpred_assert_1(&ref[23])
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun65
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(2),N(0))
	fail_ret
	call_c   Dyam_Reg_Load_Ptr(2,V(3))
	fail_ret
	move     N(1), R(4)
	move     0, R(5)
	call_c   DyALog_Mutable_Write(R(2),&R(4),0)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(3))
	fail_ret
	call_c   DYAM_evpred_assert_1(&ref[23])
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun53
	call_c   Dyam_Remove_Choice()
	pl_fail

pl_code local fun44
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(3),&ref[48])
	fail_ret
	pl_jump  fun43()

pl_code local fun45
	call_c   Dyam_Remove_Choice()
	call_c   DYAM_evpred_univ(V(3),&ref[50])
	fail_ret
	pl_jump  fun43()

pl_code local fun46
	call_c   Dyam_Update_Choice(fun45)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(5),I(0))
	fail_ret
	call_c   Dyam_Cut()
	call_c   DYAM_evpred_univ(V(3),&ref[49])
	fail_ret
	pl_jump  fun43()

pl_code local fun48
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(19),V(15))
	fail_ret
	pl_jump  fun47()

pl_code local fun50
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(1),&ref[51])
	fail_ret
	call_c   Dyam_Unify(V(5),I(0))
	fail_ret
	pl_jump  fun49()

pl_code local fun41
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(3),V(2))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun38
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(4),N(0))
	fail_ret
	pl_jump  fun37()

pl_code local fun33
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Reg_Load(0,V(5))
	call_c   Dyam_Reg_Load(2,V(2))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_follow_ordered_table_2()

pl_code local fun29
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun26
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(2),N(0))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun24
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(3),N(0))
	fail_ret
	pl_jump  fun23()

pl_code local fun21
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(2),I(0))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun17
	call_c   Dyam_Remove_Choice()
	move     &ref[19], R(0)
	move     0, R(1)
	move     &ref[20], R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_error_2()

pl_code local fun12
	call_c   Dyam_Remove_Choice()
	call_c   DYAM_evpred_assert_1(V(1))
	call_c   Dyam_Deallocate()
	pl_ret


;;------------------ INIT POOL ------------

c_code local build_init_pool
	call_c   build_seed_1()
	call_c   build_seed_0()
	call_c   build_seed_2()
	call_c   build_seed_4()
	call_c   build_seed_5()
	call_c   build_ref_4()
	call_c   build_ref_3()
	call_c   build_ref_76()
	call_c   build_ref_77()
	call_c   build_ref_75()
	call_c   build_ref_74()
	call_c   build_ref_72()
	call_c   build_ref_73()
	call_c   build_ref_70()
	call_c   build_ref_69()
	call_c   build_ref_68()
	call_c   build_ref_71()
	call_c   build_ref_67()
	call_c   build_ref_66()
	call_c   build_ref_40()
	call_c   build_ref_42()
	call_c   build_ref_48()
	call_c   build_ref_47()
	call_c   build_ref_46()
	call_c   build_ref_45()
	call_c   build_ref_49()
	call_c   build_ref_50()
	call_c   build_ref_44()
	call_c   build_ref_43()
	call_c   build_ref_41()
	call_c   build_ref_51()
	call_c   build_ref_39()
	call_c   build_ref_38()
	call_c   build_ref_37()
	call_c   build_ref_36()
	call_c   build_ref_29()
	call_c   build_ref_28()
	call_c   build_ref_27()
	call_c   build_ref_26()
	call_c   build_ref_25()
	call_c   build_ref_24()
	call_c   build_ref_23()
	call_c   build_ref_22()
	call_c   build_ref_21()
	call_c   build_ref_20()
	call_c   build_ref_19()
	call_c   build_ref_16()
	call_c   build_ref_15()
	call_c   build_ref_14()
	call_c   build_ref_8()
	call_c   build_ref_7()
	call_c   build_ref_6()
	call_c   build_ref_5()
	c_ret

long local ref[95]
long local seed[6]

long local _initialization

c_code global initialization_dyalog_tools
	call_c   Dyam_Check_And_Update_Initialization(_initialization)
	fail_c_ret
	call_c   initialization_dyalog_oset()
	call_c   initialization_dyalog_format()
	call_c   build_init_pool()
	call_c   build_viewers()
	call_c   load()
	c_ret

