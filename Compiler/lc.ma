;; Compiler: DyALog 1.14.0
;; File "lc.pl"


;;------------------ LOADING ------------

c_code local load
	call_c   Dyam_Loading_Set()
	pl_call  fun10(&seed[0],0)
	pl_call  fun10(&seed[3],0)
	pl_call  fun10(&seed[1],0)
	pl_call  fun10(&seed[2],0)
	pl_call  fun10(&seed[4],0)
	call_c   Dyam_Loading_Reset()
	c_ret

pl_code global pred_lc_emit_header_0
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   DYAM_Date_Time_6(V(1),V(2),V(3),V(4),V(5),V(6))
	fail_ret
	call_c   DYAM_Compiler_Info_1(&ref[72])
	fail_ret
	move     &ref[73], R(0)
	move     0, R(1)
	move     &ref[74], R(2)
	move     S(5), R(3)
	pl_call  pred_format_2()
	move     &ref[75], R(0)
	move     0, R(1)
	move     &ref[76], R(2)
	move     S(5), R(3)
	pl_call  pred_format_2()
	call_c   Dyam_Choice(fun40)
	pl_call  Object_1(&ref[77])
	call_c   Dyam_Choice(fun39)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(11),&ref[78])
	fail_ret
	call_c   Dyam_Cut()
	pl_fail

pl_code global pred_check_lc_1
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Choice(fun4)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[24])
	call_c   Dyam_Cut()
	call_c   Dyam_Deallocate()
	pl_ret


;;------------------ VIEWERS ------------

c_code local build_viewers
	call_c   Dyam_Load_Viewer(&ref[3],&ref[4])
	c_ret

c_code local build_seed_4
	ret_reg &seed[4]
	call_c   build_ref_5()
	call_c   build_ref_9()
	call_c   Dyam_Seed_Start(&ref[5],&ref[9],I(0),fun1,1)
	call_c   build_ref_8()
	call_c   Dyam_Seed_Add_Comp(&ref[8],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[4]
	c_ret

;; TERM 8: '*GUARD*'(check_analyzer_mode(lc))
c_code local build_ref_8
	ret_reg &ref[8]
	call_c   build_ref_6()
	call_c   build_ref_7()
	call_c   Dyam_Create_Unary(&ref[6],&ref[7])
	move_ret ref[8]
	c_ret

;; TERM 7: check_analyzer_mode(lc)
c_code local build_ref_7
	ret_reg &ref[7]
	call_c   build_ref_95()
	call_c   build_ref_96()
	call_c   Dyam_Create_Unary(&ref[95],&ref[96])
	move_ret ref[7]
	c_ret

;; TERM 96: lc
c_code local build_ref_96
	ret_reg &ref[96]
	call_c   Dyam_Create_Atom("lc")
	move_ret ref[96]
	c_ret

;; TERM 95: check_analyzer_mode
c_code local build_ref_95
	ret_reg &ref[95]
	call_c   Dyam_Create_Atom("check_analyzer_mode")
	move_ret ref[95]
	c_ret

;; TERM 6: '*GUARD*'
c_code local build_ref_6
	ret_reg &ref[6]
	call_c   Dyam_Create_Atom("*GUARD*")
	move_ret ref[6]
	c_ret

;; TERM 10: stop_compilation_at(pda)
c_code local build_ref_10
	ret_reg &ref[10]
	call_c   build_ref_97()
	call_c   build_ref_98()
	call_c   Dyam_Create_Unary(&ref[97],&ref[98])
	move_ret ref[10]
	c_ret

;; TERM 98: pda
c_code local build_ref_98
	ret_reg &ref[98]
	call_c   Dyam_Create_Atom("pda")
	move_ret ref[98]
	c_ret

;; TERM 97: stop_compilation_at
c_code local build_ref_97
	ret_reg &ref[97]
	call_c   Dyam_Create_Atom("stop_compilation_at")
	move_ret ref[97]
	c_ret

long local pool_fun0[3]=[2,build_ref_8,build_ref_10]

pl_code local fun0
	call_c   Dyam_Pool(pool_fun0)
	call_c   Dyam_Unify_Item(&ref[8])
	fail_ret
	call_c   DYAM_evpred_assert_1(&ref[10])
	pl_ret

pl_code local fun1
	pl_ret

;; TERM 9: '*GUARD*'(check_analyzer_mode(lc)) :> []
c_code local build_ref_9
	ret_reg &ref[9]
	call_c   build_ref_8()
	call_c   Dyam_Create_Binary(I(9),&ref[8],I(0))
	move_ret ref[9]
	c_ret

;; TERM 5: '*GUARD_CLAUSE*'
c_code local build_ref_5
	ret_reg &ref[5]
	call_c   Dyam_Create_Atom("*GUARD_CLAUSE*")
	move_ret ref[5]
	c_ret

c_code local build_seed_2
	ret_reg &seed[2]
	call_c   build_ref_11()
	call_c   build_ref_14()
	call_c   Dyam_Seed_Start(&ref[11],&ref[14],I(0),fun9,1)
	call_c   build_ref_16()
	call_c   Dyam_Seed_Add_Comp(&ref[16],fun8,0)
	call_c   Dyam_Seed_End()
	move_ret seed[2]
	c_ret

;; TERM 16: '*CITEM*'('call_compute_lc/2'(_B), _A)
c_code local build_ref_16
	ret_reg &ref[16]
	call_c   build_ref_15()
	call_c   build_ref_12()
	call_c   Dyam_Create_Binary(&ref[15],&ref[12],V(0))
	move_ret ref[16]
	c_ret

;; TERM 12: 'call_compute_lc/2'(_B)
c_code local build_ref_12
	ret_reg &ref[12]
	call_c   build_ref_99()
	call_c   Dyam_Create_Unary(&ref[99],V(1))
	move_ret ref[12]
	c_ret

;; TERM 99: 'call_compute_lc/2'
c_code local build_ref_99
	ret_reg &ref[99]
	call_c   Dyam_Create_Atom("call_compute_lc/2")
	move_ret ref[99]
	c_ret

;; TERM 15: '*CITEM*'
c_code local build_ref_15
	ret_reg &ref[15]
	call_c   Dyam_Create_Atom("*CITEM*")
	move_ret ref[15]
	c_ret

;; TERM 17: tmp_start(_B)
c_code local build_ref_17
	ret_reg &ref[17]
	call_c   build_ref_100()
	call_c   Dyam_Create_Unary(&ref[100],V(1))
	move_ret ref[17]
	c_ret

;; TERM 100: tmp_start
c_code local build_ref_100
	ret_reg &ref[100]
	call_c   Dyam_Create_Atom("tmp_start")
	move_ret ref[100]
	c_ret

c_code local build_seed_5
	ret_reg &seed[5]
	call_c   build_ref_0()
	call_c   build_ref_18()
	call_c   Dyam_Seed_Start(&ref[0],&ref[18],&ref[18],fun2,1)
	call_c   build_ref_19()
	call_c   Dyam_Seed_Add_Comp(&ref[19],&ref[18],0)
	call_c   Dyam_Seed_End()
	move_ret seed[5]
	c_ret

pl_code local fun2
	pl_jump  Complete(0,0)

;; TERM 19: '*RITEM*'(_A, return(_B)) :> '$$HOLE$$'
c_code local build_ref_19
	ret_reg &ref[19]
	call_c   build_ref_18()
	call_c   Dyam_Create_Binary(I(9),&ref[18],I(7))
	move_ret ref[19]
	c_ret

;; TERM 18: '*RITEM*'(_A, return(_B))
c_code local build_ref_18
	ret_reg &ref[18]
	call_c   build_ref_0()
	call_c   build_ref_2()
	call_c   Dyam_Create_Binary(&ref[0],V(0),&ref[2])
	move_ret ref[18]
	c_ret

;; TERM 2: return(_B)
c_code local build_ref_2
	ret_reg &ref[2]
	call_c   build_ref_101()
	call_c   Dyam_Create_Unary(&ref[101],V(1))
	move_ret ref[2]
	c_ret

;; TERM 101: return
c_code local build_ref_101
	ret_reg &ref[101]
	call_c   Dyam_Create_Atom("return")
	move_ret ref[101]
	c_ret

;; TERM 0: '*RITEM*'
c_code local build_ref_0
	ret_reg &ref[0]
	call_c   Dyam_Create_Atom("*RITEM*")
	move_ret ref[0]
	c_ret

pl_code local fun7
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Choice(fun6)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Subsume(R(0))
	fail_ret
	call_c   Dyam_Cut()
	pl_ret

long local pool_fun8[4]=[3,build_ref_16,build_ref_17,build_seed_5]

pl_code local fun8
	call_c   Dyam_Pool(pool_fun8)
	call_c   Dyam_Unify_Item(&ref[16])
	fail_ret
	call_c   Dyam_Allocate(0)
	pl_call  Object_1(&ref[17])
	call_c   Dyam_Deallocate()
	pl_jump  fun7(&seed[5],2)

pl_code local fun9
	pl_jump  Apply(0,0)

;; TERM 14: '*FIRST*'('call_compute_lc/2'(_B)) :> []
c_code local build_ref_14
	ret_reg &ref[14]
	call_c   build_ref_13()
	call_c   Dyam_Create_Binary(I(9),&ref[13],I(0))
	move_ret ref[14]
	c_ret

;; TERM 13: '*FIRST*'('call_compute_lc/2'(_B))
c_code local build_ref_13
	ret_reg &ref[13]
	call_c   build_ref_11()
	call_c   build_ref_12()
	call_c   Dyam_Create_Unary(&ref[11],&ref[12])
	move_ret ref[13]
	c_ret

;; TERM 11: '*FIRST*'
c_code local build_ref_11
	ret_reg &ref[11]
	call_c   Dyam_Create_Atom("*FIRST*")
	move_ret ref[11]
	c_ret

c_code local build_seed_1
	ret_reg &seed[1]
	call_c   build_ref_11()
	call_c   build_ref_14()
	call_c   Dyam_Seed_Start(&ref[11],&ref[14],I(0),fun9,1)
	call_c   build_ref_16()
	call_c   Dyam_Seed_Add_Comp(&ref[16],fun15,0)
	call_c   Dyam_Seed_End()
	move_ret seed[1]
	c_ret

;; TERM 40: '$CLOSURE'('$fun'(14, 0, 1182333060), '$TUPPLE'(35086250281752))
c_code local build_ref_40
	ret_reg &ref[40]
	call_c   build_ref_39()
	call_c   Dyam_Closure_Aux(fun14,&ref[39])
	move_ret ref[40]
	c_ret

;; TERM 33: tmp_lc(_D, _C)
c_code local build_ref_33
	ret_reg &ref[33]
	call_c   build_ref_102()
	call_c   Dyam_Create_Binary(&ref[102],V(3),V(2))
	move_ret ref[33]
	c_ret

;; TERM 102: tmp_lc
c_code local build_ref_102
	ret_reg &ref[102]
	call_c   Dyam_Create_Atom("tmp_lc")
	move_ret ref[102]
	c_ret

c_code local build_seed_8
	ret_reg &seed[8]
	call_c   build_ref_0()
	call_c   build_ref_35()
	call_c   Dyam_Seed_Start(&ref[0],&ref[35],&ref[35],fun2,1)
	call_c   build_ref_36()
	call_c   Dyam_Seed_Add_Comp(&ref[36],&ref[35],0)
	call_c   Dyam_Seed_End()
	move_ret seed[8]
	c_ret

;; TERM 36: '*RITEM*'(_A, return(_C)) :> '$$HOLE$$'
c_code local build_ref_36
	ret_reg &ref[36]
	call_c   build_ref_35()
	call_c   Dyam_Create_Binary(I(9),&ref[35],I(7))
	move_ret ref[36]
	c_ret

;; TERM 35: '*RITEM*'(_A, return(_C))
c_code local build_ref_35
	ret_reg &ref[35]
	call_c   build_ref_0()
	call_c   build_ref_34()
	call_c   Dyam_Create_Binary(&ref[0],V(0),&ref[34])
	move_ret ref[35]
	c_ret

;; TERM 34: return(_C)
c_code local build_ref_34
	ret_reg &ref[34]
	call_c   build_ref_101()
	call_c   Dyam_Create_Unary(&ref[101],V(2))
	move_ret ref[34]
	c_ret

long local pool_fun14[3]=[2,build_ref_33,build_seed_8]

pl_code local fun14
	call_c   Dyam_Pool(pool_fun14)
	call_c   Dyam_Allocate(0)
	pl_call  Object_1(&ref[33])
	call_c   Dyam_Deallocate()
	pl_jump  fun7(&seed[8],2)

;; TERM 39: '$TUPPLE'(35086250281752)
c_code local build_ref_39
	ret_reg &ref[39]
	call_c   Dyam_Create_Simple_Tupple(0,301989888)
	move_ret ref[39]
	c_ret

long local pool_fun15[3]=[2,build_ref_16,build_ref_40]

pl_code local fun15
	call_c   Dyam_Pool(pool_fun15)
	call_c   Dyam_Unify_Item(&ref[16])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[40], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Load(4,V(1))
	move     V(3), R(6)
	move     S(5), R(7)
	call_c   Dyam_Reg_Deallocate(4)
fun13:
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Reg_Bind(2,V(4))
	call_c   Dyam_Multi_Reg_Bind(4,2,2)
	pl_call  fun7(&seed[6],1)
	pl_call  fun12(&seed[7],2,V(4))
	call_c   Dyam_Deallocate()
	pl_ret


c_code local build_seed_3
	ret_reg &seed[3]
	call_c   build_ref_41()
	call_c   build_ref_45()
	call_c   Dyam_Seed_Start(&ref[41],&ref[45],I(0),fun1,1)
	call_c   build_ref_46()
	call_c   Dyam_Seed_Add_Comp(&ref[46],fun51,0)
	call_c   Dyam_Seed_End()
	move_ret seed[3]
	c_ret

;; TERM 46: '*PROLOG-ITEM*'{top=> emit_at_pda, cont=> _A}
c_code local build_ref_46
	ret_reg &ref[46]
	call_c   build_ref_103()
	call_c   build_ref_43()
	call_c   Dyam_Create_Binary(&ref[103],&ref[43],V(0))
	move_ret ref[46]
	c_ret

;; TERM 43: emit_at_pda
c_code local build_ref_43
	ret_reg &ref[43]
	call_c   Dyam_Create_Atom("emit_at_pda")
	move_ret ref[43]
	c_ret

;; TERM 103: '*PROLOG-ITEM*'!'$ft'
c_code local build_ref_103
	ret_reg &ref[103]
	call_c   build_ref_104()
	call_c   Dyam_Create_Atom_Module("$ft",&ref[104])
	move_ret ref[103]
	c_ret

;; TERM 104: '*PROLOG-ITEM*'
c_code local build_ref_104
	ret_reg &ref[104]
	call_c   Dyam_Create_Atom("*PROLOG-ITEM*")
	move_ret ref[104]
	c_ret

;; TERM 47: compiler_analyzer(lc)
c_code local build_ref_47
	ret_reg &ref[47]
	call_c   build_ref_105()
	call_c   build_ref_96()
	call_c   Dyam_Create_Unary(&ref[105],&ref[96])
	move_ret ref[47]
	c_ret

;; TERM 105: compiler_analyzer
c_code local build_ref_105
	ret_reg &ref[105]
	call_c   Dyam_Create_Atom("compiler_analyzer")
	move_ret ref[105]
	c_ret

c_code local build_seed_9
	ret_reg &seed[9]
	call_c   build_ref_51()
	call_c   build_ref_50()
	call_c   Dyam_Seed_Start(&ref[51],&ref[50],I(0),fun46,0)
	call_c   Dyam_Seed_End()
	move_ret seed[9]
	c_ret

c_code local build_seed_10
	ret_reg &seed[10]
	call_c   build_ref_51()
	call_c   build_ref_50()
	call_c   Dyam_Seed_Start(&ref[51],&ref[50],I(0),fun43,0)
	call_c   Dyam_Seed_End()
	move_ret seed[10]
	c_ret

;; TERM 52: '\n%----------------- LC ---------------------\n'
c_code local build_ref_52
	ret_reg &ref[52]
	call_c   Dyam_Create_Atom("\n%----------------- LC ---------------------\n")
	move_ret ref[52]
	c_ret

;; TERM 59: '\n%-------------- REVERSE LC ----------------\n'
c_code local build_ref_59
	ret_reg &ref[59]
	call_c   Dyam_Create_Atom("\n%-------------- REVERSE LC ----------------\n")
	move_ret ref[59]
	c_ret

;; TERM 65: '\n%-------------- TERMINAL LC ---------------\n'
c_code local build_ref_65
	ret_reg &ref[65]
	call_c   Dyam_Create_Atom("\n%-------------- TERMINAL LC ---------------\n")
	move_ret ref[65]
	c_ret

pl_code local fun22
	call_c   Dyam_Remove_Choice()
fun16:
	call_c   DYAM_Nl_0()
	fail_ret
	call_c   DYAM_Set_Output_1(V(1))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_jump  Follow_Cont(V(0))


;; TERM 68: toto(_W, [_H], _U)
c_code local build_ref_68
	ret_reg &ref[68]
	call_c   build_ref_106()
	call_c   build_ref_71()
	call_c   Dyam_Term_Start(&ref[106],3)
	call_c   Dyam_Term_Arg(V(22))
	call_c   Dyam_Term_Arg(&ref[71])
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_End()
	move_ret ref[68]
	c_ret

;; TERM 71: [_H]
c_code local build_ref_71
	ret_reg &ref[71]
	call_c   Dyam_Create_List(V(7),I(0))
	move_ret ref[71]
	c_ret

;; TERM 106: toto
c_code local build_ref_106
	ret_reg &ref[106]
	call_c   Dyam_Create_Atom("toto")
	move_ret ref[106]
	c_ret

;; TERM 69: 'internal_terminal_lc(~k,~k).\n'
c_code local build_ref_69
	ret_reg &ref[69]
	call_c   Dyam_Create_Atom("internal_terminal_lc(~k,~k).\n")
	move_ret ref[69]
	c_ret

;; TERM 58: [_H,_U]
c_code local build_ref_58
	ret_reg &ref[58]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(20),I(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(7),R(0))
	move_ret ref[58]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun17[4]=[3,build_ref_68,build_ref_69,build_ref_58]

pl_code local fun21
	call_c   Dyam_Remove_Choice()
fun17:
	pl_call  Object_1(&ref[68])
	move     &ref[69], R(0)
	move     0, R(1)
	move     &ref[58], R(2)
	move     S(5), R(3)
	pl_call  pred_format_2()
	pl_fail


pl_code local fun20
	call_c   Dyam_Remove_Choice()
	pl_fail

;; TERM 53: compute_lc(_H, _T)
c_code local build_ref_53
	ret_reg &ref[53]
	call_c   build_ref_107()
	call_c   Dyam_Create_Binary(&ref[107],V(7),V(19))
	move_ret ref[53]
	c_ret

;; TERM 107: compute_lc
c_code local build_ref_107
	ret_reg &ref[107]
	call_c   Dyam_Create_Atom("compute_lc")
	move_ret ref[107]
	c_ret

;; TERM 66: tmp_terminal(_T)
c_code local build_ref_66
	ret_reg &ref[66]
	call_c   build_ref_108()
	call_c   Dyam_Create_Unary(&ref[108],V(19))
	move_ret ref[66]
	c_ret

;; TERM 108: tmp_terminal
c_code local build_ref_108
	ret_reg &ref[108]
	call_c   Dyam_Create_Atom("tmp_terminal")
	move_ret ref[108]
	c_ret

;; TERM 70: [_T]
c_code local build_ref_70
	ret_reg &ref[70]
	call_c   Dyam_Create_List(V(19),I(0))
	move_ret ref[70]
	c_ret

long local pool_fun18[3]=[2,build_ref_70,build_ref_68]

pl_code local fun18
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(20),&ref[70])
	fail_ret
	call_c   DYAM_evpred_assert_1(&ref[68])
	pl_fail

;; TERM 67: toto(_W, [_H], _V)
c_code local build_ref_67
	ret_reg &ref[67]
	call_c   build_ref_106()
	call_c   build_ref_71()
	call_c   Dyam_Term_Start(&ref[106],3)
	call_c   Dyam_Term_Arg(V(22))
	call_c   Dyam_Term_Arg(&ref[71])
	call_c   Dyam_Term_Arg(V(21))
	call_c   Dyam_Term_End()
	move_ret ref[67]
	c_ret

;; TERM 55: [_T|_V]
c_code local build_ref_55
	ret_reg &ref[55]
	call_c   Dyam_Create_List(V(19),V(21))
	move_ret ref[55]
	c_ret

long local pool_fun19[6]=[65540,build_ref_66,build_ref_67,build_ref_55,build_ref_68,pool_fun18]

long local pool_fun23[5]=[131074,build_ref_65,build_ref_53,pool_fun17,pool_fun19]

pl_code local fun28
	call_c   Dyam_Remove_Choice()
fun23:
	move     &ref[65], R(0)
	move     0, R(1)
	move     I(0), R(2)
	move     0, R(3)
	pl_call  pred_format_2()
	call_c   Dyam_Choice(fun22)
	call_c   DyALog_Gensym()
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Number(0,V(22))
	fail_ret
	call_c   Dyam_Choice(fun21)
	call_c   Dyam_Choice(fun20)
	call_c   Dyam_Set_Cut()
	pl_call  Callret_2(V(23),&ref[53])
	call_c   Dyam_Cut()
fun19:
	pl_call  Object_1(V(23))
	pl_call  Object_1(&ref[66])
	call_c   Dyam_Choice(fun18)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[67])
	call_c   Dyam_Cut()
	call_c   DYAM_evpred_retract(&ref[67])
	fail_ret
	call_c   Dyam_Unify(V(20),&ref[55])
	fail_ret
	call_c   DYAM_evpred_assert_1(&ref[68])
	pl_fail



;; TERM 62: toto(_Y, [_T], _U)
c_code local build_ref_62
	ret_reg &ref[62]
	call_c   build_ref_106()
	call_c   build_ref_70()
	call_c   Dyam_Term_Start(&ref[106],3)
	call_c   Dyam_Term_Arg(V(24))
	call_c   Dyam_Term_Arg(&ref[70])
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_End()
	move_ret ref[62]
	c_ret

;; TERM 63: 'internal_reverse_lc(~k,~k).\n'
c_code local build_ref_63
	ret_reg &ref[63]
	call_c   Dyam_Create_Atom("internal_reverse_lc(~k,~k).\n")
	move_ret ref[63]
	c_ret

;; TERM 64: [_T,_U]
c_code local build_ref_64
	ret_reg &ref[64]
	call_c   Dyam_Create_Tupple(19,20,I(0))
	move_ret ref[64]
	c_ret

long local pool_fun24[4]=[3,build_ref_62,build_ref_63,build_ref_64]

pl_code local fun27
	call_c   Dyam_Remove_Choice()
fun24:
	pl_call  Object_1(&ref[62])
	move     &ref[63], R(0)
	move     0, R(1)
	move     &ref[64], R(2)
	move     S(5), R(3)
	pl_call  pred_format_2()
	pl_fail


long local pool_fun25[3]=[2,build_ref_71,build_ref_62]

pl_code local fun25
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(20),&ref[71])
	fail_ret
	call_c   DYAM_evpred_assert_1(&ref[62])
	pl_fail

;; TERM 60: toto(_Y, [_T], _V)
c_code local build_ref_60
	ret_reg &ref[60]
	call_c   build_ref_106()
	call_c   build_ref_70()
	call_c   Dyam_Term_Start(&ref[106],3)
	call_c   Dyam_Term_Arg(V(24))
	call_c   Dyam_Term_Arg(&ref[70])
	call_c   Dyam_Term_Arg(V(21))
	call_c   Dyam_Term_End()
	move_ret ref[60]
	c_ret

;; TERM 61: [_H|_V]
c_code local build_ref_61
	ret_reg &ref[61]
	call_c   Dyam_Create_List(V(7),V(21))
	move_ret ref[61]
	c_ret

long local pool_fun26[5]=[65539,build_ref_60,build_ref_61,build_ref_62,pool_fun25]

long local pool_fun29[6]=[196610,build_ref_59,build_ref_53,pool_fun23,pool_fun24,pool_fun26]

pl_code local fun34
	call_c   Dyam_Remove_Choice()
fun29:
	move     &ref[59], R(0)
	move     0, R(1)
	move     I(0), R(2)
	move     0, R(3)
	pl_call  pred_format_2()
	call_c   Dyam_Choice(fun28)
	call_c   DyALog_Gensym()
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Number(0,V(24))
	fail_ret
	call_c   Dyam_Choice(fun27)
	call_c   Dyam_Choice(fun20)
	call_c   Dyam_Set_Cut()
	pl_call  Callret_2(V(25),&ref[53])
	call_c   Dyam_Cut()
fun26:
	pl_call  Object_1(V(25))
	call_c   Dyam_Choice(fun25)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[60])
	call_c   Dyam_Cut()
	call_c   DYAM_evpred_retract(&ref[60])
	fail_ret
	call_c   Dyam_Unify(V(20),&ref[61])
	fail_ret
	call_c   DYAM_evpred_assert_1(&ref[62])
	pl_fail



;; TERM 56: toto(_A1, [_H], _U)
c_code local build_ref_56
	ret_reg &ref[56]
	call_c   build_ref_106()
	call_c   build_ref_71()
	call_c   Dyam_Term_Start(&ref[106],3)
	call_c   Dyam_Term_Arg(V(26))
	call_c   Dyam_Term_Arg(&ref[71])
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_End()
	move_ret ref[56]
	c_ret

;; TERM 57: 'internal_lc(~k,~k).\n'
c_code local build_ref_57
	ret_reg &ref[57]
	call_c   Dyam_Create_Atom("internal_lc(~k,~k).\n")
	move_ret ref[57]
	c_ret

long local pool_fun30[4]=[3,build_ref_56,build_ref_57,build_ref_58]

pl_code local fun33
	call_c   Dyam_Remove_Choice()
fun30:
	pl_call  Object_1(&ref[56])
	move     &ref[57], R(0)
	move     0, R(1)
	move     &ref[58], R(2)
	move     S(5), R(3)
	pl_call  pred_format_2()
	pl_fail


long local pool_fun31[3]=[2,build_ref_70,build_ref_56]

pl_code local fun31
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(20),&ref[70])
	fail_ret
	call_c   DYAM_evpred_assert_1(&ref[56])
	pl_fail

;; TERM 54: toto(_A1, [_H], _V)
c_code local build_ref_54
	ret_reg &ref[54]
	call_c   build_ref_106()
	call_c   build_ref_71()
	call_c   Dyam_Term_Start(&ref[106],3)
	call_c   Dyam_Term_Arg(V(26))
	call_c   Dyam_Term_Arg(&ref[71])
	call_c   Dyam_Term_Arg(V(21))
	call_c   Dyam_Term_End()
	move_ret ref[54]
	c_ret

long local pool_fun32[5]=[65539,build_ref_54,build_ref_55,build_ref_56,pool_fun31]

long local pool_fun43[6]=[196610,build_ref_52,build_ref_53,pool_fun29,pool_fun30,pool_fun32]

pl_code local fun43
	call_c   Dyam_Pool(pool_fun43)
	call_c   Dyam_Allocate(0)
	pl_call  pred_lc_emit_header_0()
	move     &ref[52], R(0)
	move     0, R(1)
	move     I(0), R(2)
	move     0, R(3)
	pl_call  pred_format_2()
	call_c   Dyam_Choice(fun34)
	call_c   DyALog_Gensym()
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Number(0,V(26))
	fail_ret
	call_c   Dyam_Choice(fun33)
	call_c   Dyam_Choice(fun20)
	call_c   Dyam_Set_Cut()
	pl_call  Callret_2(V(27),&ref[53])
	call_c   Dyam_Cut()
fun32:
	pl_call  Object_1(V(27))
	call_c   Dyam_Choice(fun31)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[54])
	call_c   Dyam_Cut()
	call_c   DYAM_evpred_retract(&ref[54])
	fail_ret
	call_c   Dyam_Unify(V(20),&ref[55])
	fail_ret
	call_c   DYAM_evpred_assert_1(&ref[56])
	pl_fail


;; TERM 50: cont('$TUPPLE'(35086250281880))
c_code local build_ref_50
	ret_reg &ref[50]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,404751358)
	move_ret R(0)
	call_c   build_ref_109()
	call_c   Dyam_Create_Unary(&ref[109],R(0))
	move_ret ref[50]
	call_c   Dyam_Set_Not_Copyable(&ref[50])
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 109: cont
c_code local build_ref_109
	ret_reg &ref[109]
	call_c   Dyam_Create_Atom("cont")
	move_ret ref[109]
	c_ret

;; TERM 51: '*WAIT_CONT*'
c_code local build_ref_51
	ret_reg &ref[51]
	call_c   Dyam_Create_Atom("*WAIT_CONT*")
	move_ret ref[51]
	c_ret

pl_code local fun44
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Object(R(0))
	call_c   Dyam_Schedule_Last()
	pl_ret

;; TERM 91: '$CLOSURE'('$fun'(45, 0, 1182537460), '$TUPPLE'(35086250281764))
c_code local build_ref_91
	ret_reg &ref[91]
	call_c   build_ref_90()
	call_c   Dyam_Closure_Aux(fun45,&ref[90])
	move_ret ref[91]
	c_ret

pl_code local fun45
	pl_fail

;; TERM 90: '$TUPPLE'(35086250281764)
c_code local build_ref_90
	ret_reg &ref[90]
	call_c   Dyam_Create_Simple_Tupple(0,268435456)
	move_ret ref[90]
	c_ret

long local pool_fun46[3]=[2,build_seed_10,build_ref_91]

pl_code local fun46
	call_c   Dyam_Pool(pool_fun46)
	call_c   Dyam_Allocate(0)
	pl_call  fun44(&seed[10],12)
	move     &ref[91], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Load(4,V(7))
	call_c   Dyam_Reg_Load(6,V(19))
	call_c   Dyam_Reg_Deallocate(4)
	pl_jump  fun13()

;; TERM 94: '*WRAPPER*'(_F, _M, '*LIGHTLCNEXT*'(_H, _N, _O, _P, _Q))
c_code local build_ref_94
	ret_reg &ref[94]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_111()
	call_c   Dyam_Term_Start(&ref[111],5)
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_110()
	call_c   Dyam_Term_Start(&ref[110],3)
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_End()
	move_ret ref[94]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 110: '*WRAPPER*'
c_code local build_ref_110
	ret_reg &ref[110]
	call_c   Dyam_Create_Atom("*WRAPPER*")
	move_ret ref[110]
	c_ret

;; TERM 111: '*LIGHTLCNEXT*'
c_code local build_ref_111
	ret_reg &ref[111]
	call_c   Dyam_Create_Atom("*LIGHTLCNEXT*")
	move_ret ref[111]
	c_ret

;; TERM 93: tmp_start(_S)
c_code local build_ref_93
	ret_reg &ref[93]
	call_c   build_ref_100()
	call_c   Dyam_Create_Unary(&ref[100],V(18))
	move_ret ref[93]
	c_ret

long local pool_fun48[3]=[2,build_ref_94,build_ref_93]

pl_code local fun48
	call_c   Dyam_Remove_Choice()
	pl_call  Object_1(&ref[94])
fun47:
	call_c   Dyam_Reg_Load(0,V(5))
	move     V(17), R(2)
	move     S(5), R(3)
	pl_call  pred_value_counter_2()
	call_c   DYAM_evpred_gt(V(17),N(0))
	fail_ret
	call_c   DYAM_Copy_Numbervars_2(V(7),V(18))
	fail_ret
	move     &ref[93], R(0)
	move     S(5), R(1)
	pl_call  pred_record_without_doublon_1()
	pl_fail


;; TERM 92: '*WRAPPER*'(_F, _G, '*LCNEXT*'(_H, _I, _J, _K, _L))
c_code local build_ref_92
	ret_reg &ref[92]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_112()
	call_c   Dyam_Term_Start(&ref[112],5)
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_110()
	call_c   Dyam_Term_Start(&ref[110],3)
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_End()
	move_ret ref[92]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 112: '*LCNEXT*'
c_code local build_ref_112
	ret_reg &ref[112]
	call_c   Dyam_Create_Atom("*LCNEXT*")
	move_ret ref[112]
	c_ret

long local pool_fun49[5]=[65539,build_seed_9,build_ref_92,build_ref_93,pool_fun48]

pl_code local fun50
	call_c   Dyam_Remove_Choice()
fun49:
	pl_call  fun44(&seed[9],12)
	call_c   Dyam_Choice(fun48)
	pl_call  Object_1(&ref[92])
	pl_jump  fun47()


;; TERM 48: output_file(_C)
c_code local build_ref_48
	ret_reg &ref[48]
	call_c   build_ref_113()
	call_c   Dyam_Create_Unary(&ref[113],V(2))
	move_ret ref[48]
	c_ret

;; TERM 113: output_file
c_code local build_ref_113
	ret_reg &ref[113]
	call_c   Dyam_Create_Atom("output_file")
	move_ret ref[113]
	c_ret

;; TERM 49: write
c_code local build_ref_49
	ret_reg &ref[49]
	call_c   Dyam_Create_Atom("write")
	move_ret ref[49]
	c_ret

long local pool_fun51[7]=[131076,build_ref_46,build_ref_47,build_ref_48,build_ref_49,pool_fun49,pool_fun49]

pl_code local fun51
	call_c   Dyam_Pool(pool_fun51)
	call_c   Dyam_Unify_Item(&ref[46])
	fail_ret
	call_c   Dyam_Allocate(0)
	pl_call  Object_1(&ref[47])
	call_c   DYAM_Current_Output_1(V(1))
	fail_ret
	call_c   Dyam_Choice(fun50)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[48])
	call_c   Dyam_Cut()
	call_c   DYAM_Absolute_File_Name(V(2),V(3))
	fail_ret
	call_c   DYAM_Open_3(V(3),&ref[49],V(4))
	fail_ret
	call_c   DYAM_Set_Output_1(V(4))
	fail_ret
	pl_jump  fun49()

;; TERM 45: '*PROLOG-FIRST*'(emit_at_pda) :> []
c_code local build_ref_45
	ret_reg &ref[45]
	call_c   build_ref_44()
	call_c   Dyam_Create_Binary(I(9),&ref[44],I(0))
	move_ret ref[45]
	c_ret

;; TERM 44: '*PROLOG-FIRST*'(emit_at_pda)
c_code local build_ref_44
	ret_reg &ref[44]
	call_c   build_ref_42()
	call_c   build_ref_43()
	call_c   Dyam_Create_Unary(&ref[42],&ref[43])
	move_ret ref[44]
	c_ret

;; TERM 42: '*PROLOG-FIRST*'
c_code local build_ref_42
	ret_reg &ref[42]
	call_c   Dyam_Create_Atom("*PROLOG-FIRST*")
	move_ret ref[42]
	c_ret

;; TERM 41: '*PROLOG_FIRST*'
c_code local build_ref_41
	ret_reg &ref[41]
	call_c   Dyam_Create_Atom("*PROLOG_FIRST*")
	move_ret ref[41]
	c_ret

c_code local build_seed_0
	ret_reg &seed[0]
	call_c   build_ref_41()
	call_c   build_ref_85()
	call_c   Dyam_Seed_Start(&ref[41],&ref[85],I(0),fun1,1)
	call_c   build_ref_86()
	call_c   Dyam_Seed_Add_Comp(&ref[86],fun42,0)
	call_c   Dyam_Seed_End()
	move_ret seed[0]
	c_ret

;; TERM 86: '*PROLOG-ITEM*'{top=> analyze_at_pda('*OBJECT*'(seed{model=> ('*RITEM*'(_B, _C) :> '*LCFIRST*'(_D, _E, _F)(_G)(_H)), id=> _I, anchor=> _J, subs_comp=> _K, code=> _L, go=> _M, tabule=> _N, schedule=> _O, subsume=> _P, model_ref=> _Q, subs_comp_ref=> _R, c_type=> _S, c_type_ref=> _T, out_env=> _U, appinfo=> _V, tail_flag=> _W})), cont=> _A}
c_code local build_ref_86
	ret_reg &ref[86]
	call_c   build_ref_103()
	call_c   build_ref_83()
	call_c   Dyam_Create_Binary(&ref[103],&ref[83],V(0))
	move_ret ref[86]
	c_ret

;; TERM 83: analyze_at_pda('*OBJECT*'(seed{model=> ('*RITEM*'(_B, _C) :> '*LCFIRST*'(_D, _E, _F)(_G)(_H)), id=> _I, anchor=> _J, subs_comp=> _K, code=> _L, go=> _M, tabule=> _N, schedule=> _O, subsume=> _P, model_ref=> _Q, subs_comp_ref=> _R, c_type=> _S, c_type_ref=> _T, out_env=> _U, appinfo=> _V, tail_flag=> _W}))
c_code local build_ref_83
	ret_reg &ref[83]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_0()
	call_c   Dyam_Create_Binary(&ref[0],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_117()
	call_c   Dyam_Term_Start(&ref[117],3)
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret R(1)
	call_c   Dyam_Create_Binary(I(3),R(1),V(6))
	move_ret R(1)
	call_c   Dyam_Create_Binary(I(3),R(1),V(7))
	move_ret R(1)
	call_c   Dyam_Create_Binary(I(9),R(0),R(1))
	move_ret R(1)
	call_c   build_ref_116()
	call_c   Dyam_Term_Start(&ref[116],16)
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_Arg(V(21))
	call_c   Dyam_Term_Arg(V(22))
	call_c   Dyam_Term_End()
	move_ret R(1)
	call_c   build_ref_115()
	call_c   Dyam_Create_Unary(&ref[115],R(1))
	move_ret R(1)
	call_c   build_ref_114()
	call_c   Dyam_Create_Unary(&ref[114],R(1))
	move_ret ref[83]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 114: analyze_at_pda
c_code local build_ref_114
	ret_reg &ref[114]
	call_c   Dyam_Create_Atom("analyze_at_pda")
	move_ret ref[114]
	c_ret

;; TERM 115: '*OBJECT*'
c_code local build_ref_115
	ret_reg &ref[115]
	call_c   Dyam_Create_Atom("*OBJECT*")
	move_ret ref[115]
	c_ret

;; TERM 116: seed!'$ft'
c_code local build_ref_116
	ret_reg &ref[116]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_134()
	call_c   Dyam_Create_List(&ref[134],I(0))
	move_ret R(0)
	call_c   build_ref_133()
	call_c   Dyam_Create_List(&ref[133],R(0))
	move_ret R(0)
	call_c   build_ref_132()
	call_c   Dyam_Create_List(&ref[132],R(0))
	move_ret R(0)
	call_c   build_ref_131()
	call_c   Dyam_Create_List(&ref[131],R(0))
	move_ret R(0)
	call_c   build_ref_130()
	call_c   Dyam_Create_List(&ref[130],R(0))
	move_ret R(0)
	call_c   build_ref_129()
	call_c   Dyam_Create_List(&ref[129],R(0))
	move_ret R(0)
	call_c   build_ref_128()
	call_c   Dyam_Create_List(&ref[128],R(0))
	move_ret R(0)
	call_c   build_ref_127()
	call_c   Dyam_Create_List(&ref[127],R(0))
	move_ret R(0)
	call_c   build_ref_126()
	call_c   Dyam_Create_List(&ref[126],R(0))
	move_ret R(0)
	call_c   build_ref_125()
	call_c   Dyam_Create_List(&ref[125],R(0))
	move_ret R(0)
	call_c   build_ref_124()
	call_c   Dyam_Create_List(&ref[124],R(0))
	move_ret R(0)
	call_c   build_ref_123()
	call_c   Dyam_Create_List(&ref[123],R(0))
	move_ret R(0)
	call_c   build_ref_122()
	call_c   Dyam_Create_List(&ref[122],R(0))
	move_ret R(0)
	call_c   build_ref_121()
	call_c   Dyam_Create_List(&ref[121],R(0))
	move_ret R(0)
	call_c   build_ref_120()
	call_c   Dyam_Create_List(&ref[120],R(0))
	move_ret R(0)
	call_c   build_ref_119()
	call_c   Dyam_Create_List(&ref[119],R(0))
	move_ret R(0)
	call_c   build_ref_118()
	call_c   Dyam_Create_Atom_Module("$ft",&ref[118])
	move_ret ref[116]
	call_c   DYAM_Feature_2(&ref[118],R(0))
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 118: seed
c_code local build_ref_118
	ret_reg &ref[118]
	call_c   Dyam_Create_Atom("seed")
	move_ret ref[118]
	c_ret

;; TERM 119: model
c_code local build_ref_119
	ret_reg &ref[119]
	call_c   Dyam_Create_Atom("model")
	move_ret ref[119]
	c_ret

;; TERM 120: id
c_code local build_ref_120
	ret_reg &ref[120]
	call_c   Dyam_Create_Atom("id")
	move_ret ref[120]
	c_ret

;; TERM 121: anchor
c_code local build_ref_121
	ret_reg &ref[121]
	call_c   Dyam_Create_Atom("anchor")
	move_ret ref[121]
	c_ret

;; TERM 122: subs_comp
c_code local build_ref_122
	ret_reg &ref[122]
	call_c   Dyam_Create_Atom("subs_comp")
	move_ret ref[122]
	c_ret

;; TERM 123: code
c_code local build_ref_123
	ret_reg &ref[123]
	call_c   Dyam_Create_Atom("code")
	move_ret ref[123]
	c_ret

;; TERM 124: go
c_code local build_ref_124
	ret_reg &ref[124]
	call_c   Dyam_Create_Atom("go")
	move_ret ref[124]
	c_ret

;; TERM 125: tabule
c_code local build_ref_125
	ret_reg &ref[125]
	call_c   Dyam_Create_Atom("tabule")
	move_ret ref[125]
	c_ret

;; TERM 126: schedule
c_code local build_ref_126
	ret_reg &ref[126]
	call_c   Dyam_Create_Atom("schedule")
	move_ret ref[126]
	c_ret

;; TERM 127: subsume
c_code local build_ref_127
	ret_reg &ref[127]
	call_c   Dyam_Create_Atom("subsume")
	move_ret ref[127]
	c_ret

;; TERM 128: model_ref
c_code local build_ref_128
	ret_reg &ref[128]
	call_c   Dyam_Create_Atom("model_ref")
	move_ret ref[128]
	c_ret

;; TERM 129: subs_comp_ref
c_code local build_ref_129
	ret_reg &ref[129]
	call_c   Dyam_Create_Atom("subs_comp_ref")
	move_ret ref[129]
	c_ret

;; TERM 130: c_type
c_code local build_ref_130
	ret_reg &ref[130]
	call_c   Dyam_Create_Atom("c_type")
	move_ret ref[130]
	c_ret

;; TERM 131: c_type_ref
c_code local build_ref_131
	ret_reg &ref[131]
	call_c   Dyam_Create_Atom("c_type_ref")
	move_ret ref[131]
	c_ret

;; TERM 132: out_env
c_code local build_ref_132
	ret_reg &ref[132]
	call_c   Dyam_Create_Atom("out_env")
	move_ret ref[132]
	c_ret

;; TERM 133: appinfo
c_code local build_ref_133
	ret_reg &ref[133]
	call_c   Dyam_Create_Atom("appinfo")
	move_ret ref[133]
	c_ret

;; TERM 134: tail_flag
c_code local build_ref_134
	ret_reg &ref[134]
	call_c   Dyam_Create_Atom("tail_flag")
	move_ret ref[134]
	c_ret

;; TERM 117: '*LCFIRST*'
c_code local build_ref_117
	ret_reg &ref[117]
	call_c   Dyam_Create_Atom("*LCFIRST*")
	move_ret ref[117]
	c_ret

;; TERM 87: tmp_lc(_D, _B)
c_code local build_ref_87
	ret_reg &ref[87]
	call_c   build_ref_102()
	call_c   Dyam_Create_Binary(&ref[102],V(3),V(1))
	move_ret ref[87]
	c_ret

;; TERM 88: tmp_lc(_X, _Y)
c_code local build_ref_88
	ret_reg &ref[88]
	call_c   build_ref_102()
	call_c   Dyam_Create_Binary(&ref[102],V(23),V(24))
	move_ret ref[88]
	c_ret

long local pool_fun42[5]=[4,build_ref_86,build_ref_47,build_ref_87,build_ref_88]

pl_code local fun42
	call_c   Dyam_Pool(pool_fun42)
	call_c   Dyam_Unify_Item(&ref[86])
	fail_ret
	call_c   Dyam_Allocate(0)
	pl_call  Object_1(&ref[47])
	call_c   DYAM_Copy_Numbervars_2(&ref[87],&ref[88])
	fail_ret
	move     &ref[88], R(0)
	move     S(5), R(1)
	pl_call  pred_record_without_doublon_1()
	call_c   Dyam_Deallocate()
	pl_jump  Follow_Cont(V(0))

;; TERM 85: '*PROLOG-FIRST*'(analyze_at_pda('*OBJECT*'(seed{model=> ('*RITEM*'(_B, _C) :> '*LCFIRST*'(_D, _E, _F)(_G)(_H)), id=> _I, anchor=> _J, subs_comp=> _K, code=> _L, go=> _M, tabule=> _N, schedule=> _O, subsume=> _P, model_ref=> _Q, subs_comp_ref=> _R, c_type=> _S, c_type_ref=> _T, out_env=> _U, appinfo=> _V, tail_flag=> _W}))) :> []
c_code local build_ref_85
	ret_reg &ref[85]
	call_c   build_ref_84()
	call_c   Dyam_Create_Binary(I(9),&ref[84],I(0))
	move_ret ref[85]
	c_ret

;; TERM 84: '*PROLOG-FIRST*'(analyze_at_pda('*OBJECT*'(seed{model=> ('*RITEM*'(_B, _C) :> '*LCFIRST*'(_D, _E, _F)(_G)(_H)), id=> _I, anchor=> _J, subs_comp=> _K, code=> _L, go=> _M, tabule=> _N, schedule=> _O, subsume=> _P, model_ref=> _Q, subs_comp_ref=> _R, c_type=> _S, c_type_ref=> _T, out_env=> _U, appinfo=> _V, tail_flag=> _W})))
c_code local build_ref_84
	ret_reg &ref[84]
	call_c   build_ref_42()
	call_c   build_ref_83()
	call_c   Dyam_Create_Unary(&ref[42],&ref[83])
	move_ret ref[84]
	c_ret

c_code local build_seed_7
	ret_reg &seed[7]
	call_c   build_ref_28()
	call_c   build_ref_32()
	call_c   Dyam_Seed_Start(&ref[28],&ref[32],I(0),fun9,1)
	call_c   build_ref_30()
	call_c   Dyam_Seed_Add_Comp(&ref[30],fun11,0)
	call_c   Dyam_Seed_End()
	move_ret seed[7]
	c_ret

;; TERM 30: '*RITEM*'('call_compute_lc/2'(_C), return(_D))
c_code local build_ref_30
	ret_reg &ref[30]
	call_c   build_ref_0()
	call_c   build_ref_20()
	call_c   build_ref_29()
	call_c   Dyam_Create_Binary(&ref[0],&ref[20],&ref[29])
	move_ret ref[30]
	c_ret

;; TERM 29: return(_D)
c_code local build_ref_29
	ret_reg &ref[29]
	call_c   build_ref_101()
	call_c   Dyam_Create_Unary(&ref[101],V(3))
	move_ret ref[29]
	c_ret

;; TERM 20: 'call_compute_lc/2'(_C)
c_code local build_ref_20
	ret_reg &ref[20]
	call_c   build_ref_99()
	call_c   Dyam_Create_Unary(&ref[99],V(2))
	move_ret ref[20]
	c_ret

pl_code local fun11
	call_c   build_ref_30()
	call_c   Dyam_Unify_Item(&ref[30])
	fail_ret
	pl_jump  Follow_Cont(V(1))

;; TERM 32: '*RITEM*'('call_compute_lc/2'(_C), return(_D)) :> lc(0, '$TUPPLE'(35086250281344))
c_code local build_ref_32
	ret_reg &ref[32]
	call_c   build_ref_30()
	call_c   build_ref_31()
	call_c   Dyam_Create_Binary(I(9),&ref[30],&ref[31])
	move_ret ref[32]
	c_ret

;; TERM 31: lc(0, '$TUPPLE'(35086250281344))
c_code local build_ref_31
	ret_reg &ref[31]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,134217728)
	move_ret R(0)
	call_c   build_ref_96()
	call_c   Dyam_Create_Binary(&ref[96],N(0),R(0))
	move_ret ref[31]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 28: '*CURNEXT*'
c_code local build_ref_28
	ret_reg &ref[28]
	call_c   Dyam_Create_Atom("*CURNEXT*")
	move_ret ref[28]
	c_ret

c_code local build_seed_6
	ret_reg &seed[6]
	call_c   build_ref_15()
	call_c   build_ref_21()
	call_c   Dyam_Seed_Start(&ref[15],&ref[21],&ref[21],fun2,1)
	call_c   build_ref_23()
	call_c   Dyam_Seed_Add_Comp(&ref[23],&ref[21],0)
	call_c   Dyam_Seed_End()
	move_ret seed[6]
	c_ret

;; TERM 23: '*FIRST*'('call_compute_lc/2'(_C)) :> '$$HOLE$$'
c_code local build_ref_23
	ret_reg &ref[23]
	call_c   build_ref_22()
	call_c   Dyam_Create_Binary(I(9),&ref[22],I(7))
	move_ret ref[23]
	c_ret

;; TERM 22: '*FIRST*'('call_compute_lc/2'(_C))
c_code local build_ref_22
	ret_reg &ref[22]
	call_c   build_ref_11()
	call_c   build_ref_20()
	call_c   Dyam_Create_Unary(&ref[11],&ref[20])
	move_ret ref[22]
	c_ret

;; TERM 21: '*CITEM*'('call_compute_lc/2'(_C), 'call_compute_lc/2'(_C))
c_code local build_ref_21
	ret_reg &ref[21]
	call_c   build_ref_15()
	call_c   build_ref_20()
	call_c   Dyam_Create_Binary(&ref[15],&ref[20],&ref[20])
	move_ret ref[21]
	c_ret

;; TERM 4: compute_lc(_A, _B)
c_code local build_ref_4
	ret_reg &ref[4]
	call_c   build_ref_107()
	call_c   Dyam_Create_Binary(&ref[107],V(0),V(1))
	move_ret ref[4]
	c_ret

;; TERM 3: '*RITEM*'('call_compute_lc/2'(_A), return(_B))
c_code local build_ref_3
	ret_reg &ref[3]
	call_c   build_ref_0()
	call_c   build_ref_1()
	call_c   build_ref_2()
	call_c   Dyam_Create_Binary(&ref[0],&ref[1],&ref[2])
	move_ret ref[3]
	c_ret

;; TERM 1: 'call_compute_lc/2'(_A)
c_code local build_ref_1
	ret_reg &ref[1]
	call_c   build_ref_99()
	call_c   Dyam_Create_Unary(&ref[99],V(0))
	move_ret ref[1]
	c_ret

;; TERM 78: -
c_code local build_ref_78
	ret_reg &ref[78]
	call_c   Dyam_Create_Atom("-")
	move_ret ref[78]
	c_ret

;; TERM 80: [_N]
c_code local build_ref_80
	ret_reg &ref[80]
	call_c   Dyam_Create_List(V(13),I(0))
	move_ret ref[80]
	c_ret

;; TERM 79: '%% File ~q\n'
c_code local build_ref_79
	ret_reg &ref[79]
	call_c   Dyam_Create_Atom("%% File ~q\n")
	move_ret ref[79]
	c_ret

;; TERM 77: main_file(_L, _M)
c_code local build_ref_77
	ret_reg &ref[77]
	call_c   build_ref_135()
	call_c   Dyam_Create_Binary(&ref[135],V(11),V(12))
	move_ret ref[77]
	c_ret

;; TERM 135: main_file
c_code local build_ref_135
	ret_reg &ref[135]
	call_c   Dyam_Create_Atom("main_file")
	move_ret ref[135]
	c_ret

;; TERM 82: ';; File stdin\n'
c_code local build_ref_82
	ret_reg &ref[82]
	call_c   Dyam_Create_Atom(";; File stdin\n")
	move_ret ref[82]
	c_ret

;; TERM 81: main_file(-, _O)
c_code local build_ref_81
	ret_reg &ref[81]
	call_c   build_ref_135()
	call_c   build_ref_78()
	call_c   Dyam_Create_Binary(&ref[135],&ref[78],V(14))
	move_ret ref[81]
	c_ret

;; TERM 76: [_C,_D,_B,_E,_F,_G]
c_code local build_ref_76
	ret_reg &ref[76]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Tupple(4,6,I(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(1),R(0))
	move_ret R(0)
	call_c   Dyam_Create_Tupple(2,3,R(0))
	move_ret ref[76]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 75: '%% Date    : ~w ~w ~w at ~w:~w:~w\n'
c_code local build_ref_75
	ret_reg &ref[75]
	call_c   Dyam_Create_Atom("%% Date    : ~w ~w ~w at ~w:~w:~w\n")
	move_ret ref[75]
	c_ret

;; TERM 74: [_H,_I]
c_code local build_ref_74
	ret_reg &ref[74]
	call_c   Dyam_Create_Tupple(7,8,I(0))
	move_ret ref[74]
	c_ret

;; TERM 73: '%% Compiler Analyzer Left Corner: ~w ~w\n'
c_code local build_ref_73
	ret_reg &ref[73]
	call_c   Dyam_Create_Atom("%% Compiler Analyzer Left Corner: ~w ~w\n")
	move_ret ref[73]
	c_ret

;; TERM 72: compiler_info{name=> _H, version=> _I, author=> _J, email=> _K}
c_code local build_ref_72
	ret_reg &ref[72]
	call_c   build_ref_136()
	call_c   Dyam_Term_Start(&ref[136],4)
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_End()
	move_ret ref[72]
	c_ret

;; TERM 136: compiler_info!'$ft'
c_code local build_ref_136
	ret_reg &ref[136]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_141()
	call_c   Dyam_Create_List(&ref[141],I(0))
	move_ret R(0)
	call_c   build_ref_140()
	call_c   Dyam_Create_List(&ref[140],R(0))
	move_ret R(0)
	call_c   build_ref_139()
	call_c   Dyam_Create_List(&ref[139],R(0))
	move_ret R(0)
	call_c   build_ref_138()
	call_c   Dyam_Create_List(&ref[138],R(0))
	move_ret R(0)
	call_c   build_ref_137()
	call_c   Dyam_Create_Atom_Module("$ft",&ref[137])
	move_ret ref[136]
	call_c   DYAM_Feature_2(&ref[137],R(0))
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 137: compiler_info
c_code local build_ref_137
	ret_reg &ref[137]
	call_c   Dyam_Create_Atom("compiler_info")
	move_ret ref[137]
	c_ret

;; TERM 138: name
c_code local build_ref_138
	ret_reg &ref[138]
	call_c   Dyam_Create_Atom("name")
	move_ret ref[138]
	c_ret

;; TERM 139: version
c_code local build_ref_139
	ret_reg &ref[139]
	call_c   Dyam_Create_Atom("version")
	move_ret ref[139]
	c_ret

;; TERM 140: author
c_code local build_ref_140
	ret_reg &ref[140]
	call_c   Dyam_Create_Atom("author")
	move_ret ref[140]
	c_ret

;; TERM 141: email
c_code local build_ref_141
	ret_reg &ref[141]
	call_c   Dyam_Create_Atom("email")
	move_ret ref[141]
	c_ret

;; TERM 24: lc _B
c_code local build_ref_24
	ret_reg &ref[24]
	call_c   build_ref_96()
	call_c   Dyam_Create_Unary(&ref[96],V(1))
	move_ret ref[24]
	c_ret

;; TERM 26: lc dcg '*default*'
c_code local build_ref_26
	ret_reg &ref[26]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_142()
	call_c   build_ref_143()
	call_c   Dyam_Create_Unary(&ref[142],&ref[143])
	move_ret R(0)
	call_c   build_ref_96()
	call_c   Dyam_Create_Unary(&ref[96],R(0))
	move_ret ref[26]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 143: '*default*'
c_code local build_ref_143
	ret_reg &ref[143]
	call_c   Dyam_Create_Atom("*default*")
	move_ret ref[143]
	c_ret

;; TERM 142: dcg
c_code local build_ref_142
	ret_reg &ref[142]
	call_c   Dyam_Create_Atom("dcg")
	move_ret ref[142]
	c_ret

;; TERM 25: dcg _C
c_code local build_ref_25
	ret_reg &ref[25]
	call_c   build_ref_142()
	call_c   Dyam_Create_Unary(&ref[142],V(2))
	move_ret ref[25]
	c_ret

;; TERM 27: lc '*default*'
c_code local build_ref_27
	ret_reg &ref[27]
	call_c   build_ref_96()
	call_c   build_ref_143()
	call_c   Dyam_Create_Unary(&ref[96],&ref[143])
	move_ret ref[27]
	c_ret

pl_code local fun10
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Object(R(0))
	call_c   Dyam_Tabule()
	pl_ret

pl_code local fun39
	call_c   Dyam_Remove_Choice()
fun38:
	call_c   DYAM_Absolute_File_Name(V(11),V(13))
	fail_ret
	move     &ref[79], R(0)
	move     0, R(1)
	move     &ref[80], R(2)
	move     S(5), R(3)
	pl_call  pred_format_2()
	pl_fail


pl_code local fun36
	call_c   Dyam_Remove_Choice()
fun35:
	call_c   DYAM_Nl_0()
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret


pl_code local fun40
	call_c   Dyam_Remove_Choice()
fun37:
	call_c   Dyam_Choice(fun36)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[81])
	call_c   Dyam_Cut()
	move     &ref[82], R(0)
	move     0, R(1)
	move     I(0), R(2)
	move     0, R(3)
	pl_call  pred_format_2()
	pl_jump  fun35()


pl_code local fun12
	call_c   Dyam_Backptr_Trace(R(1),R(2))
	call_c   Dyam_Object(R(0))
	call_c   Dyam_Tabule()
	call_c   Dyam_Now()
	pl_ret

pl_code local fun6
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Object(R(0))
	call_c   Dyam_Tabule()
	call_c   Dyam_Schedule()
	pl_ret

pl_code local fun3
	call_c   Dyam_Remove_Choice()
	pl_call  Object_1(&ref[27])
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun4
	call_c   Dyam_Update_Choice(fun3)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[25])
	fail_ret
	call_c   Dyam_Cut()
	pl_call  Object_1(&ref[26])
	call_c   Dyam_Deallocate()
	pl_ret


;;------------------ INIT POOL ------------

c_code local build_init_pool
	call_c   build_seed_4()
	call_c   build_seed_2()
	call_c   build_seed_1()
	call_c   build_seed_3()
	call_c   build_seed_0()
	call_c   build_seed_7()
	call_c   build_seed_6()
	call_c   build_ref_4()
	call_c   build_ref_3()
	call_c   build_ref_78()
	call_c   build_ref_80()
	call_c   build_ref_79()
	call_c   build_ref_77()
	call_c   build_ref_82()
	call_c   build_ref_81()
	call_c   build_ref_76()
	call_c   build_ref_75()
	call_c   build_ref_74()
	call_c   build_ref_73()
	call_c   build_ref_72()
	call_c   build_ref_24()
	call_c   build_ref_26()
	call_c   build_ref_25()
	call_c   build_ref_27()
	c_ret

long local ref[144]
long local seed[11]

long local _initialization

c_code global initialization_dyalog_lc
	call_c   Dyam_Check_And_Update_Initialization(_initialization)
	fail_c_ret
	call_c   initialization_dyalog_format()
	call_c   build_init_pool()
	call_c   build_viewers()
	call_c   load()
	c_ret

