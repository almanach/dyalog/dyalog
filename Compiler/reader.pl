/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 1998, 2002, 2003, 2004, 2005, 2007, 2008, 2010, 2011, 2012, 2016, 2018 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  reader.pl -- Program Reader
 *
 * ----------------------------------------------------------------
 * Description
 * 
 * ----------------------------------------------------------------
 */

:-include 'header.pl'.

:-rec_prolog read_analyze/5.
:-std_prolog
	read_program/3,
	read_analyze_directive/4.

:-std_prolog foreign_handle/2.

/**********************************************************************
 *                    Reader
 **********************************************************************/

:-std_prolog read_files/2.

read_files(Files,Mode) :-
	( Files = (F,FF) ->
	    read_files(F,Mode),
	    read_files(FF,Mode)
	;   Files == '-' ->
	    current_input(S),
	    read_program( S, Mode, stdin )
	;   atom(Files) ->
	    read_file(Files,Mode)
	;   
	    error( 'Not a valid filename or filename list ~w', [Files])
	)
	.

read_file( F, Mode ) :-
	(   find_file(F,AF) xor error( 'File not found ~w', [F] )),
%	format('File ~w\n',[AF]),
	(   
	    recorded( loaded(AF,Mode) ) 
	xor record( loaded(AF,Mode) ),
	    record( map_file(F,AF) ),
	    open(AF,read,S),
	    (	Mode = include(ReadMode,file_info(Master_File,_,_))
	    xor (ReadMode = Mode, Master_File=AF) ),
	    read_program( S, ReadMode, file_info(Master_File,AF,F) ),
	    close( S )
	)
	.

read_program( S, Mode, File ) :-
	( Mode == require ->
	    module_get(Save_Module),
	    module_set([])
	;   
	    true
	),
	repeat( (read_term(S,T,V),
%%		    format('Read Term ~w\n',[T]),
		    read_analyze(T, Mode,V, File,Status),
		    Status == eof
		)
	      ),
	( Mode == require ->
	    module_set(Save_Module)
	;   
	    true
	)
	.

:-std_prolog register_clause/1.

register_clause(Clause) :- record( '$clause'(Clause) ).

read_analyze(T,include,V,File,Status) :-
	%% *WARNING* : V has only non anonymous vars of T
	( T == eof -> Status=eof
	;   var(T) -> true
	;   T = (:- Directive)  ->  read_analyze_directive( Directive, include, File, Status )
	;   clause_type(T,Clause,V,File) ->
	    register_clause(Clause)
	;   
	    register_predicate(fact,T,File),
	    register_clause( '$fact'( T ) )
	).

:-rec_prolog clause_type/4.

clause_type( T::(Head :- _), T, _, File) :-
	register_predicate(clause,Head,File)
	.

clause_type( T::(Head --> _), grammar_rule(Kind,T), _, File) :-
	recorded(grammar_kind(MKind)),
	mutable_read(MKind,Kind),
	register_predicate(Kind,Head,File)
	.

clause_type( (?- G), '$query'(G,V), V, File).
clause_type( T::tree(_), tag_tree{ tree=>T }, _, _).
clause_type( T::auxtree(_), tag_tree{ tree=>T }, _, _).
clause_type( T::spinetree(_), tag_tree{ tree=>T }, _, _).
clause_type( T::tag_tree{}, T, _, _).
clause_type( T::tag_lemma(_,_,_), T, _, _).
clause_type( '$loader'(Cond,T),
	     '$loader'(Cond,Clause),
	     V,
	     File
	   ) :-
	(   clause_type(T,Clause,V,File) ->
	    true
	;   % T is a fact
	    register_predicate(fact,T,File),
	    Clause='$fact'(T)
	)
	.

%% To trigger the insertin of a toplevel inside DyALog compiler
%% see file top_level.pl
clause_type( T::'$dyalog_toplevel',T,_,_).

read_analyze(T,toplevel,_,File,Status) :-
	( T == eof -> Status=eof
	;   var(T) -> true
	;   T = (:- Directive)  ->  read_analyze_directive( Directive, toplevel , File, Status )
	;   record( toplevel_clause(T) )
	).


read_analyze(T,resource,_,File,Status) :-
	( T == eof -> Status=eof
	;   var(T) -> true
	;   T = (:- Directive)  ->  read_analyze_directive( Directive, resource , File, Status )
	;   T = (_ :- _) -> extend_compiler( T )
	;   T = (_ --> _) -> record( dcg_compiler_expansion(T) )
	;   true
	).

read_analyze(T,require,_,File,Status) :-
	( T == eof -> Status=eof
	;   var(T) -> true
	;   T = (:- Directive)  ->
	    read_analyze_directive( Directive, require, File,Status )
	;   T = (Head :- _) ->
	    register_predicate(clause,Head,File)
	;   T = (Head --> _) ->
	    recorded(grammar_kind(MKind)),
	    mutable_read(MKind,Kind),
	    register_predicate(Kind,Head,File)
	;
	    register_predicate(fact,T,File)
	).

read_analyze_directive( D, ReadMode, File, Status ) :-
	(
	    D= (A , B) ->
	    read_analyze_directive(A,ReadMode,File, _),
	    read_analyze_directive(B,ReadMode,File, Status)
	;   directive(D,ReadMode,File,Status) ->   true
	;   warning( 'not a valid directive ~w', [D] )
	)
	.

:-rec_prolog directive/4.

directive(D::require( Files ),ReadMode,_,_) :-
	(read_files( Files, require )).

directive(D::include(Files), ReadMode, File,_) :-
	read_files( Files, include(ReadMode,File) ).

directive(D::resource( Files ),_,_,_) :-
	read_files( Files, resource).

directive(D::parse_mode(Mode),_,_,_) :-
	erase( parse_mode(_) ),
	record( parse_mode(Mode) )
	.

directive(D::hilog( Spec ),_,_,_) :-
	install(hilog_install,Spec).

directive(D::deref_term( Spec),_,_,_) :-
	install( deref_term, Spec).

directive(D::features( Spec, Features ),_,_,_) :-
	install(features(Features),Spec).

directive(D::feature_intro(Spec,Type),_,_,_) :-
	install(feature_intro(Type),Spec).

directive(D::finite_set( Spec, Elements ),_,_,_) :-
	check_finite_set(Elements),
	install(finite_set(Elements),Spec)
	.

directive(D::subset(Spec,Set),_,_,_) :-
	install(subset(Set),Spec).

directive(D::op( Prec, Mode, Spec ),_,_,_) :-
	install(op(Prec,Mode),Spec ).

directive(D::prolog( Spec ),_,_,_) :-
	install( flag(prolog), Spec).

directive(D::rec_prolog( Spec ),_,_,_) :-
	install( flag(rec_prolog),Spec).

directive(D::std_prolog( Spec ),_,_,_) :-
	install( flag(std_prolog),Spec).

directive(D::extensional( Spec ),_,_,_) :-
	install( flag(extensional),Spec).

directive(D::light_tabular( Spec ),_,_,_) :-
	install( flag(light_tabular),Spec).

directive(D::foreign( Template, Options ),_,_,_) :-
	record( D ).

directive(D::builtin( Spec, Fun ),_,_,_) :-
	record( D ).

directive(D::lco( Spec ),_,_,_) :-
	install( flag(lco),Spec ).

directive(D::lc( Spec ),_,_,_) :-
	record_without_doublon(left_corner),
	install( flag(lc), Spec ).

directive(D::mode( Spec, Pattern ),_,_,_) :-
	(   normalize_mode(Pattern,LPattern),
	    check_mode(LPattern)
	xor error('Not a valid mode pattern ~w',[D])),
	install( mode(LPattern), Spec)
	.

directive(D::nabla_subsume(Spec),_,_,_) :-
    record(D)
	.

directive(D::nabla_variance(Spec),_,_,_) :-
    record(D)
	.

directive(D::cmode( Kind ),_,_,_) :-
	cmode_to_pat(Kind,Pattern),
	erase('call_pattern'('*default*',_)),
	record('call_pattern'('*default*',Pattern))
	.

directive(D::dcg_mode(Spec, Pattern, Left, Right ),_,_,_) :-
	(   normalize_mode(Pattern,LPattern),
	    check_mode([Left,Right|LPattern])
	xor error('Not a valid mode pattern ~w',[D])),
	install( dcg_mode(LPattern,Left,Right), Spec)
	.

directive(D::tag_mode(Spec, Kind, Pattern,Left,Right),_,_,_) :-
	(   normalize_mode(Pattern,LPattern),
	    check_mode(X::[Left,Right|LPattern])
	xor error('Not a valid mode pattern ~w',[D])),
	install( tag_mode(Kind,LPattern,Left,Right), Spec )
	.

directive(D::tag_node_mode(Spec,Pattern),_,_,_) :-
	(   check_mode([Pattern])
	xor error('Not a valid mode pattern ~w',[D])),
	install( tag_node_mode(Pattern), Spec )
	.

directive(D::tag_features(Name,Top,Bot),_,_,_) :-
	install_tag_features(Name,Top,Bot)
	.

directive(D::tag_features_mode(Name,Mode,Top,Bot),_,_,_) :-
	install_tag_features_mode(Name,Mode,Top,Bot)
	.

directive(D::tag_family( Family ),_,_,_) :-
	erase( tag_family(_) ),
	record( D )
	.

directive(D::tag_anchor{ family => Spec },_,_,_) :-
	install( D, Spec )
	.

directive(D::tag_corrections,_,_,_) :-
	\+ recorded(D),
	record(D)
	.

directive(D::subsumption(_),_,_,_) :-
	record(D)
	.

directive(D::xcompiler( Extension ),_,_,_) :-
	extend_compiler( Extension )
	.

directive(D::toplevel_clause( Clause ),_,_,_) :-
	record( toplevel_clause(Clause) )
	.

directive(D::public(Spec),_,_,_) :-
	install( flag(public), Spec)
	.

directive(D::bmg_stacks(Stacks),_,_,_) :-
	erase( bmg_stacks(_) ),
	record( bmg_stacks(Stacks)),
	install( bmg_stack, Stacks)
	.

directive(D::bmg_island(Name,Stacks),_,_,_) :-
	new_bmg_island(Name,Stacks)
	.

directive(D::bmg_pushable(Spec,Stacks),_,_,_) :-
	install( bmg_pushable(Stacks), Spec)
	.

directive(D::module( Module, Spec ),ReadMode,File,_) :-
	module_directive(Module,Spec,File,ReadMode)
	.

directive(D::module( Module ), ReadMode,File,_) :-
	(   atom(Module) xor error('~w is not a valid module name',[Module])),
	%%	format('Switch to module ~w\n',[Module]),
	module_set(Module),
	(   File = file_info(_,Long_File,Short_File)
	xor Short_File=File, Long_File=File
	),
	( recorded( Info::map_module(M,F,S) ), erase(Info) ->
	    ( M=Module, Long_File=F -> true
	    ;	M=Module ->
		warning('Files ~w and ~w redefining module ~w',[Long_File,F,Module])
	    ;	F=Long_File ->
		warning('File ~w defining modules ~w and ~w',[Long_File,Module,M])
	    ;	true
	    )
	;   
	    true
	),
	%%format( 'Recording ~w\n',[map_module(Module,File)]),
	record( map_module(Module,Long_File,Short_File))
	.

directive(D::scanner( Scanner ),_,_,_) :-
	Scan_Atom =.. [Scanner,Left,Token,Right],
	directive_updater( scanner(_,_,_,_),
			   scanner(Left,Token,Right, Scan_Atom) )
	.

directive(D::skipper( Skipper ),_,_,_) :-
	Skip_Atom =.. [Skipper,Left,Token,Right],
	directive_updater( skipper(_,_,_,_),
			   skipper(Left,Token,Right, Skip_Atom) )
	.

directive(D::tagop( Op ),_,_,_) :-
	TagOp =.. [Op,Tag,A],
	directive_updater( tagop(_,_,_), tagop(Tag,A,TagOp) )
	.

directive(D::autoload,_,_,_) :-
	record_without_doublon( autoload ).


directive(end_require,ReadMode,_,Status) :-
	(ReadMode==require -> Status=eof ; true).

directive(include_unless_def(Cond),_,_,Status) :-
	( recorded(Cond) ->
	  Status=eof
	; record(Cond)
	)
	.

directive(D::import{module=>M,file=>F,preds=>Spec},ReadMode,File,_) :-
	(   atom(M) xor M=[] xor error('Expected module name in ~w',[D]) ),
	(   atom(F) xor name_builder('~w.pl',[M],F) ),
	(   Spec = [] xor true ),
	(   %% ReadMode==require
	    %% xor
	    directive(require(F),ReadMode,File,_),
	    (	recorded( map_module(M,LF,F) )
	    xor M==[]
	    xor warning('File ~w not defining module ~w',[F,M])),
	    install( import(M), Spec )
	)
	.

directive(D::debug,_,_,_) :-
	erase( debug ),
	record( debug )
	.


:-std_prolog directive_updater/2.

directive_updater(Old,New) :-
	erase(Old),
	record(New)
	.
		
/**********************************************************************
 * Generic Installer
 **********************************************************************/

:-std_prolog install/2,install/3.
:-rec_prolog install_list/3,install_elem/3,installer/3.

install(Installer,Spec) :- install(Installer,Spec,std).

install(Installer,Spec,Kind) :-
	( var(Spec) ->
	    install_elem(Installer,Spec,Kind)
	;   Spec == [] ->
	    true
	;   Spec = [_|_] ->
	    install_list( Installer, Spec,Kind )
	;   Spec = (A,B) ->
	    install(Installer,A,Kind),
	    install(Installer,B,Kind)
	;   Spec = dcg(A) ->
	    install(Installer,A,dcg)
	;   Spec = tag(A) ->
	    install(Installer,A,tag)
	;   Spec = rcg(A,M) ->
	    install(Installer,A,rcg(M))
	;   
	    install_elem( Installer, Spec, Kind )
	).

install_elem(Installer,Pred/N,Kind ) :-
	atom(Pred),
	integer(N),
	installer(Installer, Pred/N, Kind).

install_elem(Installer,Pred,Kind) :- 
	atom(Pred),
	installer(Installer,Pred/0,Kind).

install_elem(Installer,Term,Kind) :-
	\+ atom(Term),
	\+ Term = _/_,
	functor(Term,F,N),
	installer(Installer,F/N,Kind)
	.
	
install_elem(Installer,Pred::'*default*',Kind) :-
	installer(Installer,Pred,Kind).

install_list(_ , [], _ ).
install_list(Installer, [A|L], Kind )  :-
	install_elem(Installer,A, Kind ),
	install_list(Installer,L, Kind ).

/**********************************************************************
 * Specific Installers
 **********************************************************************/


:-std_prolog try_in_module/2.

try_in_module(Name1,Name2) :-
	module_get(Module),
	( Module == [] ->
	    Name1=Name2
	;   
	    deep_module_shift(Name1,Module,Name2)
	)
	.

:-std_prolog spec_in_module/2.

spec_in_module(Spec1,Spec2) :-
	( Spec1=Name1/N, Spec2=Name2/N ->
	    try_in_module(Name1,Name2)
	;   
	    Spec1=Spec2
	)
	.

installer(flag(F),Spec1, Kind)         :-
	spec_in_module(Spec1,Spec),
	( Kind == dcg ->
	    TSpec = dcg(Spec)
	;   Kind = rcg(M) ->
	    TSpec = rcg(Spec,M)
	;   Kind = tag ->
	    TSpec = tag(Spec)
	;   
	    TSpec = Spec ),
	T =.. [F,TSpec],
%%	( recorded( predicate(TSpec,_) ) ->
%%	    warning( 'directive ~w after definition for ~w',[F,TSpec])
%%	;   true
%%	),
	(   flag_exclusive_set(Set,F),
	    flag_exclusive_set(Set,FF),
	    FF \== F,
	    TT =.. [FF,TSpec],
	    erase_recorded( TT )
	    ->
	    warning( 'new directive ~w for ~w replaces ~w',[F,TSpec,FF])
	;
	    true
	),
	record(T).

installer(op(Prec,Mode),P/_,_)          :-    op(Prec,Mode,P).

installer(features(Features),P/_,_)     :-
	(   erase_recorded( features(P,Old_Features) ),
	    Old_Features \== Features
	->
	    warning( 'new feature set ~w for ~w replaces ~w',
		     [Features,P,Old_Features])
	;
	    true
	),
	record(features(P,Features)),
%%	atom_module(PF,P,feature),
	feature(P,Features)
	.

installer(feature_intro(Type),Feature/_,_) :-
	( erase_recorded( feature_intro(Feature,Old_Type) ),
	    Old_Type \== Type
	->
	    warning('new type ~w introduced by ~w replaces ~w',
		    [Type,Feature,Old_Type])
	;
	    true
	),
	record(feature_intro(Feature,Type)),
	atom_module(XType,Type,'$ft'),
	feature_intro(Feature,XType),
	true
	.

installer(finite_set(Elements),P/_,_)  :-
	(   erase_recorded( finite_set(P,Old_Elements) ),
	    Old_Elements \== Elements
	->
	    warning( 'new element set ~w for ~w replaces ~w',
		     [Elements,P,Old_Elements])
	;
	    true
	),
	record(finite_set(P,Elements)),
	finite_set(P,Elements)
	.

installer(subset(Set),P/_,_)     :-
	(   erase_recorded( subset(P,Old_Set) ),
	    Old_Set \== Set
	->
	    warning( 'new subset ~w for ~w replaces ~w',
		     [Set,P,Old_Set])
	;
	    true
	),
	record(subset(P,Set)),
	subset(P,Set),
	true
	.


installer(hilog_install,P/_,_)          :-
	erase( hilog(P) ),
	record( hilog(P) ),
	hilog(P).

installer(mode(Pattern),Spec1,Kind)         :-
	spec_in_module(Spec1,Spec),
	( Kind = rcg(M) ->
	    TSpec = rcg(Spec,M)
	;   
	    TSpec = Spec
	),
	(   erase_recorded( 'call_pattern'(TSpec,Old_Pattern) ),
	    Old_Pattern \== Pattern
	->  warning('new mode pattern ~w for ~w replaces ~w',
		    [Pattern,TSpec,Old_Pattern])
	;   
	    true
	),
	record('call_pattern'(TSpec,Pattern)).

installer(dcg_mode(Pattern,Left,Right),Spec1,_) :-
	spec_in_module(Spec1,Spec),
	(   erase_recorded( 'call_dcg_pattern'(Spec,Old_Pattern,OL,OR) ),
	    Old_Pattern*OL*OR \== Pattern*Left*Right
	->  warning('new dcg mode pattern ~w for ~w replaces ~w',
		    [Pattern*Left*Right,Spec,Old_Pattern*OL*OR])
	;
	    true
	),
	record('call_dcg_pattern'(Spec,Pattern,Left,Right))
	.

installer(tag_mode(Kind,Pattern,Left,Right),Spec1,_) :-
	spec_in_module(Spec1,Spec),
	check_tag_kind(Kind,
		   Filter::tag_modulation{ subst_modulation=>Subst,
					   top_modulation=>Top,
					   bot_modulation=>Bot },
		   Modulation:: nt_modulation{ nt_pattern=>Pattern,
					       left_pattern=>Left,
					       right_pattern=>Right }
		  ),
	(   recorded( 'tag_modulation'(Spec,
				       OFilter::tag_modulation{ subst_modulation=>OSubst,
								top_modulation=>OTop,
								bot_modulation=>OBot }),
		      Old_Add
		    ) ->
	    delete_address( Old_Add )
	;
	    OSubst=[], OTop=[],OBot=[]
	),
	tag_nt_modulation(Spec,Subst,OSubst),
	tag_nt_modulation(Spec,Top,OTop),
	tag_nt_modulation(Spec,Bot,OBot),
	record( 'tag_modulation'(Spec,Filter) )
	.
	    
:-std_prolog tag_nt_modulation/3,check_tag_kind/3.

tag_nt_modulation(Spec,New_Modulation,Old_Modulation) :-
	( New_Modulation = Old_Modulation -> true
	;   Old_Modulation == [] -> true
	;   warning('new tag mode pattern ~w for ~w replaces ~w',
		    [New_Modulation,Spec,Old_Modulation])
	)
	.

check_tag_kind(Kind,Filter,Modulation::nt_modulation{}) :-
	( Kind == subst -> Filter = tag_modulation{ subst_modulation => Modulation }
	;   Kind == top -> Filter = tag_modulation{ top_modulation => Modulation }
	;   Kind == bot -> Filter = tag_modulation{ bot_modulation => Modulation }
	;   Kind == all -> Filter = tag_modulation{ subst_modulation => Modulation,
						    top_modulation => Modulation,
						    bot_modulation => Modulation
						  }
	;   Kind = [K] -> check_tag_kind(K,Filter,Modulation)
	;   Kind = [K|LK] ->
	    check_tag_kind(K,Filter,Modulation),
	    check_tag_kind(LK,Filter,Modulation)
	;   error('Not a valid tag modulation kind ~w',[Kind])
	)
	.

installer(tag_node_mode(Pattern),NT,_) :-
	(   erase_recorded( 'tag_node_modulation'(NT,OPat) ),
	    OPat \== Pattern
	->  warning( 'new tag node pattern ~w for ~w replaces ~w',[Pattern,NT,OPat])
	;   true ),
	record( 'tag_node_modulation'(NT,Pattern) )
	.

installer( tag_anchor{ coanchors=> Coanchors, equations => Equations },
	   Family/0,
	   _
	 ) :-
	Coanchors ?= [],
	Equations ?= [],
	(   check_ground_list(Coanchors) xor error( 'tag_anchor: ground list expected ~w',
						    [Coanchors] )),
	length( Coanchors, N),
	length( VarCoanchors, N),
	tag_anchor_set_coanchors( Coanchors, VarCoanchors, Family, VarCoanchors),
	tag_anchor_collect_eqvars( Equations, EqVars ),
	record( O::tag_anchor{ family=>Family,
			       coanchors=>VarCoanchors,
			       equations=>EqVars
			     } ),
	%%	format( '~w\n', [O] ),
	(   domain( Eq, Equations ),
	    tag_anchor_set_equation( Eq, EqVars, Family ),
	    fail
	;   
	    true
	)
	.

:-rec_prolog tag_anchor_set_coanchors/4.

tag_anchor_set_coanchors([],[],_,_).
tag_anchor_set_coanchors([Node|Coanchors],[Token|VarCoanchors],Family,Vars) :-
	record( O::tag_coanchor(Family,Node,Token,Vars) ),
%%	format( '~w\n', [O] ),
	tag_anchor_set_coanchors(Coanchors,VarCoanchors,Family,Vars)
	.

:-rec_prolog tag_anchor_collect_eqvars/2.

tag_anchor_collect_eqvars([],[]).
tag_anchor_collect_eqvars([EVars^(_)|Equations],Vars) :-
	tag_anchor_collect_eqvars(Equations,Vars2),
	append(EVars,Vars2,Vars)
	.

:-std_prolog tag_anchor_set_equation/3.

tag_anchor_set_equation( _^(E at Node), EqVars, Family ) :-
	tag_anchor_analyse_equation(E,Top,Bot),
	( erase_recorded(tag_equation(Family,Node,_Top,_Bot,_)) ->
	    (	Top = _Top, Bot = _Bot xor
	    warning('incompatible equations for TAG node ~w of family ~w',[Node,Family])
	    )
	;
	    true
	),
	record( O::tag_equation(Family,Node,Top,Bot,EqVars) )
	.
		
:-std_prolog tag_anchor_analyse_equation/3.

tag_anchor_analyse_equation(E,Top,Bot) :-
	( var(E) -> error( 'tag_anchor: unexpected variables in equation', [])
	;   E = (E1 and E2 ) ->
	    tag_anchor_analyse_equation(E1,Top,Bot),
	    tag_anchor_analyse_equation(E2,Top,Bot)
	;   E = (top=Top1) ->
	    functor(Top1,F1,N),
	    term_current_module_shift(tag(F1/N),_,Top1,Top)
	;   E = (bot=Bot1) ->
	    functor(Bot1,F1,N),
	    term_current_module_shift(tag(F1/N),_,Bot1,Bot)
	;   error( 'tag_anchor: unknown equation type ~w',[E])
	)
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

installer(deref_term,Spec,_) :-
	format('Set Deref ~w\n',[Spec]),
	record( deref_functor(Spec) ),
	deref_term(Spec)
	.

%% Should test redefinition of bmg_stack
installer( bmg_stack, S/_, _ ) :-
	record( bmg_stack(S) ),
	op(300,xfx,S),
	name_builder('isl_~w',[S],I),
	new_bmg_island(I,[S]),
	new_bmg_island(isl,[S])  %% isl collect all stacks
	.

%% Should test redefinition of bmg_pushable
installer( bmg_pushable(Stacks), Spec1, _ ) :-
	spec_in_module(Spec1,Spec),
	install( bmg_pushable2(Spec), Stacks ).

installer( bmg_pushable2(Spec), S/_, _ ) :-
	record( bmg_pushable(Spec,S) ).

%% Should test redefinition of bmg_island
installer( bmg_island(Name), S/_, _ ) :-
	record( bmg_island(Name,S) ).

:-std_prolog new_bmg_island/2.

new_bmg_island(Name,Stacks) :-
	op(300,fx,Name),
	erase( bmg_island(Name) ),
	record( bmg_island(Name) ),
	install( bmg_island(Name), Stacks )
	.

:-std_prolog extend_compiler/1.

extend_compiler( (Head :- Body) ) :-
	( var(Head) ->
	    error('Not a valid compiler extension: ~w',[Head])
	;
	    functor(Head,F,N),
	    ( recorded( compiler_extension(F/N,M_L) ) ->
		mutable_read(M_L,L),
		mutable(M_L,[(Head :- Body)|L])
	    ;	
		mutable(M_L,[(Head :- Body)]),
		record( compiler_extension(F/N,M_L) )
	    )
	)
	.

:-std_prolog install_tag_features/3.

install_tag_features(Label,Top,Bot) :-
	(   atom(Label) xor error( 'directive tag_features expects a symbol as first arg', [Label])),
	(   Label=Top, Label=Bot
	xor functor(Top,Label1,N),functor(Bot,Label1,N)
	xor functor(Bot,Label1,N),functor(Top,Label1,N)
	xor error('features top ~w or bot ~w not built on same non terminal ~w (directive tag_features)',
		  [Top,Bot,Label])
	),
	term_current_module_shift(tag(Label1/N),_,Top,MTop),
	term_current_module_shift(tag(Label1/N),_,Bot,MBot),
	New = tag_features(Label,MTop,MBot),
	(   recorded( Old::tag_features(Label,_,_) ),
	    Old \== New
	->  erase(Old),
	    warning( 'new tag features directive ~w', [New] )
	;   true ),
	record( New )
	.

:-std_prolog install_tag_features_mode/4.

install_tag_features_mode(Label,Mode,Top,Bot) :-
	(   atom(Label) xor error( 'directive tag_features_mode expects a symbol as first arg: ~w', [Label])),
	( atom(Mode), domain(Mode,[left,right,wrap])
	xor error( 'directive tag_features_mode expects a symbol in {left,right,wrap} as second arg: ~w', [Mode])),
	(   Label=Top, Label=Bot
	xor functor(Top,Label1,N),functor(Bot,Label1,N)
	xor functor(Bot,Label1,N),functor(Top,Label1,N)
	xor error('features top ~w or bot ~w not built on same non terminal ~w (directive tag_features_mode)',
		  [Top,Bot,Label])
	),
	term_current_module_shift(tag(Label1/N),_,Top,MTop),
	term_current_module_shift(tag(Label1/N),_,Bot,MBot),
	New = tag_features_mode(Label,Mode,MTop,MBot),
	(   recorded( Old::tag_features_mode(Label,Mode,_,_) ),
	    Old \== New
	->  erase(Old),
	    warning( 'new tag features mode directive ~w', [New] )
	;   true ),
	record( New )
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

:-std_prolog normalize_mode/2.

normalize_mode(Pattern,LPattern) :-
	( Pattern = [_|_] -> LPattern = Pattern
	;   atom(Pattern) -> LPattern = Pattern
	; Pattern = (PredPat,FSPat) ->
	  %% to be used for feature term as in mode(f{},(+,f{ a=> + }))
	  FSPat =.. [_|LFSPattern],
	  LPattern = [PredPat|LFSPattern]
	;
	    Pattern =.. LPattern
	).

:-rec_prolog check_mode/1.

check_mode([]).
check_mode([U|L]) :- (U==(+) xor U==(*) xor U=(-)),check_mode(L).

check_mode(-).
check_mode(+).
check_mode(*).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

:-std_prolog check_finite_set/1.

check_finite_set(Elements) :-
	( Elements == [] -> error('finite_set: defining empty set',[])
	;   Elements = [_] -> warning('finite_set: defining set with single element ~w',
				      [Elements])
	;   ( check_ground_list(Elements)
	    xor error('finite_set: not a ground list ~w', [Elements]))
	)
	.

:-rec_prolog check_ground_list/1.

check_ground_list([]).
check_ground_list([A|L]) :-
	ground(A),
	check_ground_list(L)
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

:-extensional cmode_to_pat/2.

cmode_to_pat(old,+).
cmode_to_pat(earley,*).
cmode_to_pat(bottom,-).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Used to register definition of predicates
%% Should test coherence wrt declarations (prolog, rec_prolog, extensional)

%% To work properly, the declarations should precede the definitions !
%% but predicate uses may precede the definitions !

:-rec_prolog register_predicate/3.

register_predicate(clause,Head,File) :-
	functor(Head,F1,N1),
	term_current_module_shift(Pred1::F1/N1,Pred,_,_),
	record_without_doublon( predicate(Pred,File) )
	.

register_predicate(fact,Head,File) :-
	functor(Head,F1,N1),
	term_current_module_shift(Pred1::F1/N1,Pred,_,_),
	record_without_doublon( predicate(Pred,File) )
	.

register_predicate(dcg,THead,File) :-
	( THead = (BMG_Head,_) xor BMG_Head=THead ),
	(   (BMG_Head =.. [Stack,Head,_], bmg_stack(Stack))
	xor Head=BMG_Head ),
	functor(Head,F1,N1),
	term_current_module_shift(Pred1::dcg(F1/N1),Pred,_,_),
	record_without_doublon( predicate(Pred,File) )
	.

register_predicate(rcg,Head,File) :-
	(   rcg_functor(Head,Key1,_,_) xor
	error('trying register a non valid rcg predicate ~w',[Head])),
	term_current_module_shift(Key1,Key,_,_),
	record_without_doublon( predicate(Key,File) )
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Define exclusive set of declaration for predicates
%% Flags are grouped by sets of exclusive values

:-extensional flag_exclusive_set/2.

%% Set compile
flag_exclusive_set(compile,extensional).
flag_exclusive_set(compile,prolog).
flag_exclusive_set(compile,rec_prolog).
flag_exclusive_set(compile,std_prolog).
flag_exclusive_set(compile,light_tabular).

%% Set lco
flag_exclusive_set(lco,lco).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  Directive module

:-std_prolog module_directive/4.

module_directive(Module,Spec,File,Mode) :-
	( atom(Module)
          xor error('~w is not a valid module name',[Module])),
	(   recorded( module(File,M) ),
	    M \== Module ->
	error( 'Multiple module names ~w for ~w',[[Module,M],File] )
	;   true
	),
	(   recorded( module(F,Module) ),
	    F \== File ->
	error( 'Multiple files ~w for module name ~w',[[File,F],Module])
	;   true
	),
	(   Mode == include ->
	    (	recorded( current_module(M) ),
		M \== Module ->
	    error( 'multiple modules activated ~w',[[M,Module]])
	    ;	true
	    ),
	    record_without_doublon( current_module(Module) )
	;
	    true
	),
	install( public(Module), Spec )
	.

installer( public(Module), Spec1, _ ) :-
	spec_in_module(Spec1,Spec),
	record_without_doublon( public(Spec,Module) )
	.
	

installer( import(Module), Spec, _ ) :-
	%%	format(';; set import ~w ~w\n',[Spec,Module]),
	record_without_doublon( D::module_import(Spec,Module) )
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% About Kleene


%% This directive should be computable automatically
%% To be used when no kleene star body can scan an empty string
directive(D::no_empty_kleene_body,_,_,_) :-
	record_without_doublon( D )
	.
