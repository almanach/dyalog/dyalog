/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 1998, 2002, 2005, 2008, 2009, 2011 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  emit.pl -- File Emitting
 *
 * ----------------------------------------------------------------
 * Description
 * 
 * ----------------------------------------------------------------
 */

:-include 'header.pl'.

emit :-
	current_output(Old_Stream),
	( recorded( output_file(File) ) ->
	    absolute_file_name(File,AF),
	    open(AF,write,Stream),
	    set_output(Stream)
	;
	    true
	),

	%% Some initialization
	mutable(M,0),
	record(term_handling(M)),

	%% Emitting
	emit_header,
	emit_loading,
	emit_named_functions,
	emit_viewers,
	emit_propagated_directives,
	emit_init_pool,
	emit_initializer,
	set_output(Old_Stream)
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Emit Header, Modules and Main Info

:-std_prolog emit_header.

emit_header :-
%%	date_time(Year,Month,Day,Hour,Min,Sec),
	compiler_info( compiler_info{ name=>Name, version=>Version }),
	format( ';; Compiler: ~w ~w\n', [Name,Version]),
%%	format( ';; Date    : ~w ~w ~w at ~w:~w:~w\n',[Month,Day,Year,Hour,Min,Sec]),
	every(	( recorded( main_file(File,_) )
		, File \== '-'
%%		, absolute_file_name(File,F)
%%		, format(';; File ~q\n',[F])
		, format(';; File ~q\n',[File])
		)
	     ),
	( recorded( main_file('-',_) ) -> format(';; File stdin\n',[]) ; true ),
	nl
	.


emit_initializer :-
	value_counter(term,N_Terms),
	ma_emit_table(local,ref,N_Terms,[]),
	value_counter(seeds,N_Seeds),
	ma_emit_table(local,seed,N_Seeds,[]),
	nl,
	( recorded( main_module ) ->
	    ma_emit_code(global,main_initialization),
	    emit_initializer_aux
	;   recorded( main_file(File,info(_,Module,_,_)) ) ->
	    safe_c_normalize(Module,CModule),
	    format('long local _initialization\n\n',[]),
	    ma_emit_code(global,initialization_dyalog_+CModule),
	    ma_emit_call_c('Dyam_Check_And_Update_Initialization',[c('_initialization')]),
	    ma_emit_fail_c_ret,
	    emit_initializer_aux
	;   
	    true
	)
	.

:-std_prolog emit_initializer/0, emit_initializer_aux/0.

emit_initializer_aux :-
	every( (   recorded( loaded(File,require) ),
		   decompose_file_name(File,_,Module,_),
		   safe_c_normalize(Module,CModule),
		   ma_emit_call_c(initialization_dyalog_+CModule,[])
	       )),
	(recorded( main_module ) ->
	 %% init_pool done in directive initializer for main modules
	 true
	;
	 ma_emit_call_c(build_init_pool,[])
	),
	ma_emit_call_c(build_viewers,[]),
%%	ma_emit_call_c(build_propagated_directives,[]),
	ma_emit_call_c(load,[]),
	ma_emit_ret,
	nl
	.

:-light_tabular c_normalize/2.
:-mode(c_normalise/2,+(+,-)).

c_normalize(Name,CName) :-
	'$interface'( 'C_Normalize'(Name: string), [return(CName: string)]),
	true
	.

:-std_prolog safe_c_normalize/2.
	
safe_c_normalize(Name,CName) :-
	c_normalize(Name,CName),
	(Name == CName
	xor warning( 'May have unexpected name clashes because of C normalization of ~w into ~w' , [Name,CName])
	)
	.
	


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Emit Terms

:-rec_prolog emit_lterm/4.
:-std_prolog emit_lterm_post/2.
:-std_prolog emit_term/1.

emit_term(TermId:: '$term'(Id,TermM)) :-
	mutable_read(TermM,Term),
	mutable(TermM,[]),	% => emitted
	format(';; TERM ~w: ~k\n',[Id,Term]),
	ma_emit_code(local,'build_ref_'+Id),
	ma_emit_ret_reg(TermId),
	recorded( term_handling(M) ),
	mutable(M,TermId),
	label_local_term(Term,'$lterm'(_,Reg,_)),
	mutable(Reg,TermId),
	reset_registers,
	every( ( ordered_table(lterm,LT),
		   LT=lterm(_,T,LevelM,_,Live),
		   mutable_read(LevelM,Level),
		   T='$lterm'(_,TReg,_),
		   (   Arg='$lterm'(_,_,_),
		       domain(Arg,Live),
		       free_arg(Arg,Level)
		   ;
		       mutable_read(TReg,[]), % if not global
		       first_register(RegV), % assign a tmp reg
		       mutable(TReg,RegV) 
		   )
	       )),
%%	format(';; IN TERM ~w\n',[Id]),
	register_info(Max,_),
	( Max == -1 xor
%%            format(';; Registers to save ~w\n',[Max]),
%%	    update_counter(choice_label,Label),
	    Save is Max+1,
	    ma_emit_call_c('Dyam_Pseudo_Choice',[c(Save)])
	),
	every( ( ordered_table(lterm,LT),
		   LT=lterm(_,T,LevelM,Inst,Live),
		   T='$lterm'(L,R,_),
		   mutable_read(L,LV),
		   mutable_read(R,RV),
		   emit_lterm(Inst,Live,T,TermId),
		   mutable(LevelM,[])
	       )),
	(   Max == -1
	xor ma_emit_call_c('Dyam_Remove_Pseudo_Choice',[])
	),
	ma_emit_ret,
	nl,
%	erase( ordered_table(lterm,_,_,_) )
	reset_ordered_table(lterm)
	.

emit_lterm_post(T::'$lterm'(_,Reg,_),Where) :-
	mutable_read(Reg,Where),
	ma_emit_move_ret(Where),
	(   \+ recorded( no_copyable(Where) ) xor
	ma_emit_call_c('Dyam_Set_Not_Copyable',[Where]) )
	.

emit_lterm( '$CLOSURE'(F), [Env], T, InGlobal ) :-
	emit_globals(Env,InGlobal),
	emit_globals(F,InGlobal),
	ma_emit_call_c( 'Dyam_Closure_Aux', [F,Env] ),
	emit_lterm_post(T,_)
	.

emit_lterm( '$charlist'(S), [], T , _) :-
	ma_emit_call_c('Dyam_Create_Char_List',['$smb'(S)]),
	emit_lterm_post(T,_)
	.

emit_lterm( '$smb'(S), [], T, _) :-
	ma_emit_call_c('Dyam_Create_Atom',['$smb'(S)]),
	emit_lterm_post(T,Where),
	every( ( recorded( deref_functor( S/N ) ),
		   format(';; deref functor ~w/~w\n',[S,N]),
		   ma_emit_call_c('Dyam_Set_Deref_Functor',[Where,N])
	       ))
	.

emit_lterm( '$smb'(S), X::[Module|Rest], T, InGlobal) :-
	emit_globals(X,InGlobal),
	ma_emit_call_c('Dyam_Create_Atom_Module',['$smb'(S),Module]),
	emit_lterm_post(T,Where),
	( Rest = [Features] ->
	    ma_emit_call_c('DYAM_Feature_2',[Module,Features])
	;   
	    true
	),
	every( ( recorded( deref_functor( S/N ) ),
		   format(';; deref functor ~w/~w\n',[S,N]),
		   ma_emit_call_c('Dyam_Set_Deref_Functor',[Where,N])
	       ))
	.

emit_lterm( '$list', Args, T, InGlobal) :-
	emit_globals(Args,InGlobal),
	ma_emit_call_c('Dyam_Create_List',Args),
	emit_lterm_post(T,_)
	.

emit_lterm( '$tupple'(N,M), [A], T, InGlobal) :-
	emit_globals(A,InGlobal),
	ma_emit_call_c('Dyam_Create_Tupple',[c(N),c(M),A]),
	emit_lterm_post(T,_)
	.

emit_lterm( '$alt_tupple'(Base,Mask), [A], T, InGlobal) :-
	emit_globals(A,InGlobal),
	ma_emit_call_c('Dyam_Create_Alt_Tupple',[c(Base),c(Mask),A]),
	emit_lterm_post(T,_)
	.

emit_lterm( '$xtupple'([Base,Mask|List]),[],T,InGlobal) :-
	( List = [] ->
	  ma_emit_call_c('Dyam_Create_Simple_Tupple',[c(Base),c(Mask)])
	;
	  ma_emit_call_c('Dyam_Start_Tupple',[c(Base),c(Mask)]),
	  emit_xtupple(List)
	),
	emit_lterm_post(T,_)
	.

:-std_prolog emit_xtupple/1.

emit_xtupple(L) :-
	( L = [Base,Mask] ->
	  ma_emit_call_c('Dyam_Almost_End_Tupple',[c(Base),c(Mask)])
	; L = [Base,Mask|Tail],
	  ma_emit_call_c('Dyam_Write_Tupple',[c(Base),c(Mask)]),
	  emit_xtupple(Tail)
	)
	.

emit_lterm( '$fun_tupple'([Base,Mask|List]),  F, T, InGlobal ) :-
%%	format(';; fun_tupple ~w ~w\n',[F,XList]),
	emit_globals(F,InGlobal),
	( List = []->
	  ma_emit_call_c('Dyam_Create_Fun_With_Tupple',[F,c(Base),c(Mask)])
	;
	  ma_emit_call_c('Dyam_Start_Fun_With_Tupple',[c(Base),c(Mask)]),
	  emit_fun_tupple(List,F)
	),
	emit_lterm_post(T,_)
	.

:-std_prolog emit_fun_tupple/2.

emit_fun_tupple(L,F) :-
	( L = [] ->
	  ma_emit_call_c('Dyam_End_Fun_With_Tupple',[F])
	; L=[Base,Mask|Tail],
	  ma_emit_call_c('Dyam_Write_Fun_With_Tupple',[c(Base),c(Mask)]),
	  emit_fun_tupple(Tail,F)
	).

emit_lterm( '$unary', X, T, InGlobal ) :-
	emit_globals(X,InGlobal),
	ma_emit_call_c('Dyam_Create_Unary',X),
	emit_lterm_post(T,_)
	.

emit_lterm( '$binary', X, T, InGlobal ) :-
	emit_globals(X,InGlobal),
	ma_emit_call_c('Dyam_Create_Binary',X),
	emit_lterm_post(T,_)
	.

emit_lterm( '$term'(Arity), X::[F|Args], T, InGlobal ) :-
	emit_globals(X,InGlobal),
	ma_emit_call_c('Dyam_Term_Start',[F,c(Arity)]),
	(   domain(A,Args),
	    ma_emit_call_c('Dyam_Term_Arg',[A]),
	    fail
	;   
	    ma_emit_call_c('Dyam_Term_End',[]),
	    emit_lterm_post(T,_)
	)
	.

:-std_prolog free_arg/2.
free_arg('$lterm'(ALev,AReg,_),Level) :-
	mutable_read(AReg,ARegV),
	( ARegV='$term'(_,_) ->	% Global: no register to free
	    true
	;   mutable_read(ALev,ALevV),
	    Level >= ALevV ->
				% the arg may be freed
	    recorded( registers(M) ),
	    mutable_read(M,N:L),
	    ( domain(ARegV,L) ->
		true
	    ;
		mutable(M,N:[ARegV|L])
	    )
	;   
	    %% format(';; Try free alpha\n',[]),
	    true
	)
	.

:-xcompiler			% Inlining
reset_registers :-
	(   recorded( registers(M) )
	xor mutable(M,[]),
	    record( registers(M) )
	),
	mutable(M, -1 : [])
	.


:-std_prolog register_info/2.

register_info(I,L) :-
	recorded( registers(M) ),
	mutable_read(M,I:L)
	.
	

:-std_prolog first_register/1.
first_register(Id) :-
	%% format(';; Obj ~w\n',[Obj]),
	recorded( registers(M) ),
	mutable_read(M,N:L),
	%% format(';; Obj ~w from state ~w ~w\n',[Obj,N,L]),
	%% \+ recorded( in_register(Obj,_) ),
	%% format(';; no in register\n',[]),
	( L = [Id|LL] ->
	    mutable(M,N:LL)
	;   
	    I is N+1,
	    Id = '$reg'(I),
	    mutable(M,I:[])
	),
	%%	format(';; Register ~w from state ~w ~w\n',[Id,N,L]),
	true
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Emit Prototype and Code of Functions


:-std_prolog emit_named_functions/0.

emit_named_functions :-
	every(( recorded( named_function(L,_,_) ),
		data_emit(L)))
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Emit Seeds

:-std_prolog emit_seed/1.

emit_seed(SeedId::'$seed'(Id,SeedM)) :-
	mutable_read(SeedM,
		     Seed::seed{ id => SeedId, 
				 model => M ,
				 anchor => A,
				 model_ref => M_Id, 
				 subs_comp_ref => Subs_Id, 
				 code => Code,
				 go=>Go,
				 c_type_ref => Type_Ref
			       }),
	mutable(SeedM,[]),	% Emitted
	functor(Code,_,N),
	Code =.. [_|Components],
	ma_emit_code(local,'build_seed_'+Id),
	ma_emit_ret_reg(SeedId),
	emit_globals([Type_Ref,M_Id,Subs_Id,Go,c(N)],SeedId),
	ma_emit_call_c('Dyam_Seed_Start',[Type_Ref,M_Id,Subs_Id,Go,c(N)]),
	(   domain(component(_A,_B,_C),Components),
	    emit_globals([_A,_B,_C],SeedId),
	    ma_emit_call_c('Dyam_Seed_Add_Comp',[_A,_B,_C]),
	    fail
	;   
	    ma_emit_call_c('Dyam_Seed_End',[]),
	    ma_emit_move_ret(SeedId),
	    ma_emit_ret,
	    nl
	)
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Emit Viewers

:-std_prolog emit_viewers.

emit_viewers :-
	writeln( '\n;;------------------ VIEWERS ------------\n'),
	ma_emit_code(local,build_viewers),
	every( (   recorded( viewer(CI,CT) ),
		   emit_globals_in_init_pool([CI,CT]),
		   ma_emit_call_c('Dyam_Load_Viewer',[CI,CT])
	       )),
	ma_emit_ret,
	nl
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Emit Propagated Directives

:-std_prolog emit_propagated_directives.

emit_propagated_directives :-
	( recorded( main_module ) ->
	  writeln( '\n;;------------------ DIRECTIVES ------------\n'),
	  ma_emit_code(global,main_directive_initialization),
	  %% main module inits pool just before directives
	  ma_emit_call_c(build_init_pool,[]),
	  every((   recorded( finite_set(FS,Elts) ),
		    label_term(Elts,LElts),
		    label_term(FS,LFS),
		    emit_globals_in_init_pool([LFS,LElts]),
		    ma_emit_call_c('DYAM_Finite_Set_2',[LFS,LElts])
		)),
	  every(( recorded( features(F,Features) ),
		  label_term(F,LF),
		  label_term(Features,LFeatures),
		  emit_globals_in_init_pool([LF,LFeatures]),
		  ma_emit_call_c('DYAM_Feature_2',[LF,LFeatures])
		)),
	  ( recorded(dyalog_ender(_,EnderFunId)) ->
	    ma_emit_call_c('DyALog_Register_Ender',[EnderFunId]),
	    true
	  ;
	    true
	  ),
	  ma_emit_ret,
	  nl
	;
	  true
	)
	.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Emit Initial Objects Loading

:-std_prolog emit_loading.

emit_loading :-
	writeln( '\n;;------------------ LOADING ------------\n'),
	ma_emit_code(local,load),
	ma_emit_call_c('Dyam_Loading_Set',[]),
	every( (   recorded( load_init(Code) ),
		   emit_init_code(Code)
	       )),
	ma_emit_call_c('Dyam_Loading_Reset',[]),
	ma_emit_ret,
	nl
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Emit Data Builder functions

:-std_prolog clean_globals/1.

clean_globals(Fun) :-
	%% clean_globals acts in a complex way !
	%% each data_emit built globals/2 and its own ordered_table globals
	%% which are destroyed and consumed by the clean_globals/1 at the end of
	%% data_emit
	%% => there may be more than one global ordered_table followed at some point
	every(( erase_recorded(global(Fun,A)),
		data_emit(A)
	      )),
	value_counter( emitted_terms, V ),
	(   V < 3000 xor
	format(';; reset lterms\n',[]),
	    reset_counter(emitted_terms,0),
	    abolish( lterm/5 ))
	.

:-rec_prolog data_emit/1.

data_emit(L::'$term'(_,TermM)) :-
	\+ mutable_read(TermM,[]),
	update_counter( emitted_terms, _ ),
	emit_term(L),
	clean_globals(L),
	true
	.

data_emit(L::'$seed'(_,SeedM)) :-
	\+ mutable_read(SeedM,[]),
	emit_seed(L),
	clean_globals(L)
	.

data_emit(L::'$fun'(_,_,CodeM)) :-
	\+ mutable_read(CodeM,[]),
	emit_function(L)
	.

:-std_prolog emit_init_pool/0.

emit_init_pool :-
	every(( recorded( init_pool(A) ), data_emit(A) )),
	writeln( '\n;;------------------ INIT POOL ------------\n'),
	ma_emit_code(local,'build_init_pool'),
	every((   erase_recorded( init_pool(A) ),
		  ( A = '$seed'(Id,_) ->
		      ma_emit_call_c('build_seed_' + Id,[])
		  ;   A = '$term'(Id,_) ->
		      ma_emit_call_c('build_ref_' + Id,[])
		  ;
		      true
		  )
	      )),
	ma_emit_ret,
	nl
	.




