;; Compiler: DyALog 1.14.0
;; File "tag_features.pl"


;;------------------ LOADING ------------

c_code local load
	call_c   Dyam_Loading_Set()
	pl_call  fun9(&seed[1],0)
	pl_call  fun9(&seed[2],0)
	pl_call  fun9(&seed[4],0)
	pl_call  fun9(&seed[5],0)
	pl_call  fun9(&seed[0],0)
	pl_call  fun9(&seed[3],0)
	call_c   Dyam_Loading_Reset()
	c_ret

pl_code global pred_tag_features_skel_node_4
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(4)
	call_c   Dyam_Choice(fun114)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[149])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun84)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Choice(fun83)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(10),&ref[150])
	fail_ret
	call_c   Dyam_Cut()
fun82:
	call_c   Dyam_Cut()
	move     &ref[151], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(2))
	call_c   Dyam_Reg_Load(4,V(3))
	call_c   Dyam_Reg_Load(6,V(4))
	call_c   Dyam_Reg_Deallocate(4)
	pl_jump  pred_tag_features_skel_node_4()


pl_code global pred_tag_features_apply_aux_1
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Choice(fun80)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[145])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun78)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[146])
	call_c   Dyam_Cut()
fun77:
	call_c   Dyam_Reg_Load(0,V(5))
	call_c   Dyam_Reg_Deallocate(1)
	pl_jump  pred_tag_features_apply_aux_1()


pl_code global pred_tag_features_emit_header_0
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   DYAM_Date_Time_6(V(1),V(2),V(3),V(4),V(5),V(6))
	fail_ret
	call_c   DYAM_Compiler_Info_1(&ref[134])
	fail_ret
	move     &ref[135], R(0)
	move     0, R(1)
	move     &ref[136], R(2)
	move     S(5), R(3)
	pl_call  pred_format_2()
	move     &ref[137], R(0)
	move     0, R(1)
	move     &ref[138], R(2)
	move     S(5), R(3)
	pl_call  pred_format_2()
	call_c   Dyam_Choice(fun75)
	pl_call  Object_1(&ref[139])
	call_c   Dyam_Choice(fun74)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(11),&ref[140])
	fail_ret
	call_c   Dyam_Cut()
	pl_fail

pl_code global pred_tag_features_combine_6
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(6)
	call_c   DYAM_evpred_univ(V(1),&ref[119])
	fail_ret
	call_c   DYAM_evpred_univ(V(2),&ref[120])
	fail_ret
	call_c   DYAM_evpred_univ(V(3),&ref[121])
	fail_ret
	call_c   DYAM_evpred_univ(V(4),&ref[122])
	fail_ret
	pl_call  fun23(&seed[13],1)
	call_c   DYAM_evpred_univ(V(5),&ref[126])
	fail_ret
	call_c   DYAM_evpred_univ(V(6),&ref[127])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code global pred_tag_features_fixpoint_0
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	move     &ref[102], R(0)
	move     0, R(1)
	move     I(0), R(2)
	move     0, R(3)
	pl_call  pred_format_2()
	call_c   Dyam_Choice(fun64)
	pl_call  fun23(&seed[12],2)
	call_c   Dyam_Reg_Load(0,V(4))
	call_c   Dyam_Reg_Load(2,V(3))
	call_c   Dyam_Reg_Load(4,V(5))
	call_c   Dyam_Reg_Load(6,V(6))
	pl_call  pred_tag_features_apply_4()
	call_c   Dyam_Reg_Load(0,V(4))
	call_c   Dyam_Reg_Load(2,V(5))
	call_c   Dyam_Reg_Load(4,V(6))
	pl_call  pred_tag_features_aggregate_3()
	call_c   Dyam_Reg_Load(0,V(4))
	call_c   Dyam_Reg_Load(2,V(7))
	call_c   Dyam_Reg_Load(4,V(5))
	call_c   Dyam_Reg_Load(6,V(6))
	pl_call  pred_tag_features_mode_aggregate_4()
	pl_fail

pl_code global pred_tag_features_mode_aggregate_4
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(4)
	call_c   Dyam_Choice(fun11)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[23])
	call_c   Dyam_Cut()
fun10:
	call_c   Dyam_Reg_Load(0,V(5))
	call_c   Dyam_Reg_Load(2,V(6))
	call_c   Dyam_Reg_Load(4,V(3))
	call_c   Dyam_Reg_Load(6,V(4))
	move     V(7), R(8)
	move     S(5), R(9)
	move     V(8), R(10)
	move     S(5), R(11)
	pl_call  pred_tag_features_combine_6()
	call_c   DYAM_evpred_retract(&ref[24])
	fail_ret
	call_c   DYAM_evpred_assert_1(&ref[25])
	call_c   Dyam_Deallocate()
	pl_ret


pl_code global pred_tag_features_aggregate_3
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(3)
	call_c   Dyam_Choice(fun7)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[20])
	call_c   Dyam_Cut()
fun6:
	call_c   Dyam_Reg_Load(0,V(4))
	call_c   Dyam_Reg_Load(2,V(5))
	call_c   Dyam_Reg_Load(4,V(2))
	call_c   Dyam_Reg_Load(6,V(3))
	move     V(6), R(8)
	move     S(5), R(9)
	move     V(7), R(10)
	move     S(5), R(11)
	pl_call  pred_tag_features_combine_6()
	call_c   DYAM_evpred_retract(&ref[21])
	fail_ret
	call_c   DYAM_evpred_assert_1(&ref[22])
	call_c   Dyam_Deallocate()
	pl_ret


pl_code global pred_tag_features_subsumes_4
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(4)
	call_c   Dyam_Choice(fun4)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_Subsumes_Chk_2(&ref[18],&ref[19])
	fail_ret
	call_c   Dyam_Cut()
	pl_fail

pl_code global pred_tag_features_apply_4
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(2,V(2))
	call_c   Dyam_Reg_Load(0,V(2))
	call_c   Dyam_Reg_Deallocate(1)
	pl_jump  pred_tag_features_apply_aux_1()


;;------------------ VIEWERS ------------

c_code local build_viewers
	call_c   Dyam_Load_Viewer(&ref[7],&ref[8])
	call_c   Dyam_Load_Viewer(&ref[3],&ref[4])
	c_ret

c_code local build_seed_3
	ret_reg &seed[3]
	call_c   build_ref_9()
	call_c   build_ref_13()
	call_c   Dyam_Seed_Start(&ref[9],&ref[13],I(0),fun1,1)
	call_c   build_ref_12()
	call_c   Dyam_Seed_Add_Comp(&ref[12],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[3]
	c_ret

;; TERM 12: '*GUARD*'(check_analyzer_mode(tag_features))
c_code local build_ref_12
	ret_reg &ref[12]
	call_c   build_ref_10()
	call_c   build_ref_11()
	call_c   Dyam_Create_Unary(&ref[10],&ref[11])
	move_ret ref[12]
	c_ret

;; TERM 11: check_analyzer_mode(tag_features)
c_code local build_ref_11
	ret_reg &ref[11]
	call_c   build_ref_178()
	call_c   build_ref_179()
	call_c   Dyam_Create_Unary(&ref[178],&ref[179])
	move_ret ref[11]
	c_ret

;; TERM 179: tag_features
c_code local build_ref_179
	ret_reg &ref[179]
	call_c   Dyam_Create_Atom("tag_features")
	move_ret ref[179]
	c_ret

;; TERM 178: check_analyzer_mode
c_code local build_ref_178
	ret_reg &ref[178]
	call_c   Dyam_Create_Atom("check_analyzer_mode")
	move_ret ref[178]
	c_ret

;; TERM 10: '*GUARD*'
c_code local build_ref_10
	ret_reg &ref[10]
	call_c   Dyam_Create_Atom("*GUARD*")
	move_ret ref[10]
	c_ret

;; TERM 14: stop_compilation_at(pgm)
c_code local build_ref_14
	ret_reg &ref[14]
	call_c   build_ref_180()
	call_c   build_ref_181()
	call_c   Dyam_Create_Unary(&ref[180],&ref[181])
	move_ret ref[14]
	c_ret

;; TERM 181: pgm
c_code local build_ref_181
	ret_reg &ref[181]
	call_c   Dyam_Create_Atom("pgm")
	move_ret ref[181]
	c_ret

;; TERM 180: stop_compilation_at
c_code local build_ref_180
	ret_reg &ref[180]
	call_c   Dyam_Create_Atom("stop_compilation_at")
	move_ret ref[180]
	c_ret

long local pool_fun0[3]=[2,build_ref_12,build_ref_14]

pl_code local fun0
	call_c   Dyam_Pool(pool_fun0)
	call_c   Dyam_Unify_Item(&ref[12])
	fail_ret
	call_c   DYAM_evpred_assert_1(&ref[14])
	pl_ret

pl_code local fun1
	pl_ret

;; TERM 13: '*GUARD*'(check_analyzer_mode(tag_features)) :> []
c_code local build_ref_13
	ret_reg &ref[13]
	call_c   build_ref_12()
	call_c   Dyam_Create_Binary(I(9),&ref[12],I(0))
	move_ret ref[13]
	c_ret

;; TERM 9: '*GUARD_CLAUSE*'
c_code local build_ref_9
	ret_reg &ref[9]
	call_c   Dyam_Create_Atom("*GUARD_CLAUSE*")
	move_ret ref[9]
	c_ret

c_code local build_seed_0
	ret_reg &seed[0]
	call_c   build_ref_9()
	call_c   build_ref_17()
	call_c   Dyam_Seed_Start(&ref[9],&ref[17],I(0),fun1,1)
	call_c   build_ref_16()
	call_c   Dyam_Seed_Add_Comp(&ref[16],fun3,0)
	call_c   Dyam_Seed_End()
	move_ret seed[0]
	c_ret

;; TERM 16: '*GUARD*'(tag_features_combine_aux([], [], [], [], [], []))
c_code local build_ref_16
	ret_reg &ref[16]
	call_c   build_ref_10()
	call_c   build_ref_15()
	call_c   Dyam_Create_Unary(&ref[10],&ref[15])
	move_ret ref[16]
	c_ret

;; TERM 15: tag_features_combine_aux([], [], [], [], [], [])
c_code local build_ref_15
	ret_reg &ref[15]
	call_c   build_ref_182()
	call_c   Dyam_Term_Start(&ref[182],6)
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_End()
	move_ret ref[15]
	c_ret

;; TERM 182: tag_features_combine_aux
c_code local build_ref_182
	ret_reg &ref[182]
	call_c   Dyam_Create_Atom("tag_features_combine_aux")
	move_ret ref[182]
	c_ret

pl_code local fun3
	call_c   build_ref_16()
	call_c   Dyam_Unify_Item(&ref[16])
	fail_ret
	pl_ret

;; TERM 17: '*GUARD*'(tag_features_combine_aux([], [], [], [], [], [])) :> []
c_code local build_ref_17
	ret_reg &ref[17]
	call_c   build_ref_16()
	call_c   Dyam_Create_Binary(I(9),&ref[16],I(0))
	move_ret ref[17]
	c_ret

c_code local build_seed_5
	ret_reg &seed[5]
	call_c   build_ref_26()
	call_c   build_ref_30()
	call_c   Dyam_Seed_Start(&ref[26],&ref[30],I(0),fun1,1)
	call_c   build_ref_31()
	call_c   Dyam_Seed_Add_Comp(&ref[31],fun21,0)
	call_c   Dyam_Seed_End()
	move_ret seed[5]
	c_ret

;; TERM 31: '*PROLOG-ITEM*'{top=> analyze_at_clause(tag_tree{family=> _B, name=> _C, tree=> (auxtree _D)}), cont=> _A}
c_code local build_ref_31
	ret_reg &ref[31]
	call_c   build_ref_183()
	call_c   build_ref_28()
	call_c   Dyam_Create_Binary(&ref[183],&ref[28],V(0))
	move_ret ref[31]
	c_ret

;; TERM 28: analyze_at_clause(tag_tree{family=> _B, name=> _C, tree=> (auxtree _D)})
c_code local build_ref_28
	ret_reg &ref[28]
	call_c   build_ref_184()
	call_c   build_ref_44()
	call_c   Dyam_Create_Unary(&ref[184],&ref[44])
	move_ret ref[28]
	c_ret

;; TERM 44: tag_tree{family=> _B, name=> _C, tree=> (auxtree _D)}
c_code local build_ref_44
	ret_reg &ref[44]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_186()
	call_c   Dyam_Create_Unary(&ref[186],V(3))
	move_ret R(0)
	call_c   build_ref_185()
	call_c   Dyam_Term_Start(&ref[185],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_End()
	move_ret ref[44]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 185: tag_tree!'$ft'
c_code local build_ref_185
	ret_reg &ref[185]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_190()
	call_c   Dyam_Create_List(&ref[190],I(0))
	move_ret R(0)
	call_c   build_ref_189()
	call_c   Dyam_Create_List(&ref[189],R(0))
	move_ret R(0)
	call_c   build_ref_188()
	call_c   Dyam_Create_List(&ref[188],R(0))
	move_ret R(0)
	call_c   build_ref_187()
	call_c   Dyam_Create_Atom_Module("$ft",&ref[187])
	move_ret ref[185]
	call_c   DYAM_Feature_2(&ref[187],R(0))
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 187: tag_tree
c_code local build_ref_187
	ret_reg &ref[187]
	call_c   Dyam_Create_Atom("tag_tree")
	move_ret ref[187]
	c_ret

;; TERM 188: family
c_code local build_ref_188
	ret_reg &ref[188]
	call_c   Dyam_Create_Atom("family")
	move_ret ref[188]
	c_ret

;; TERM 189: name
c_code local build_ref_189
	ret_reg &ref[189]
	call_c   Dyam_Create_Atom("name")
	move_ret ref[189]
	c_ret

;; TERM 190: tree
c_code local build_ref_190
	ret_reg &ref[190]
	call_c   Dyam_Create_Atom("tree")
	move_ret ref[190]
	c_ret

;; TERM 186: auxtree
c_code local build_ref_186
	ret_reg &ref[186]
	call_c   Dyam_Create_Atom("auxtree")
	move_ret ref[186]
	c_ret

;; TERM 184: analyze_at_clause
c_code local build_ref_184
	ret_reg &ref[184]
	call_c   Dyam_Create_Atom("analyze_at_clause")
	move_ret ref[184]
	c_ret

;; TERM 183: '*PROLOG-ITEM*'!'$ft'
c_code local build_ref_183
	ret_reg &ref[183]
	call_c   build_ref_191()
	call_c   Dyam_Create_Atom_Module("$ft",&ref[191])
	move_ret ref[183]
	c_ret

;; TERM 191: '*PROLOG-ITEM*'
c_code local build_ref_191
	ret_reg &ref[191]
	call_c   Dyam_Create_Atom("*PROLOG-ITEM*")
	move_ret ref[191]
	c_ret

;; TERM 32: compiler_analyzer(tag_features)
c_code local build_ref_32
	ret_reg &ref[32]
	call_c   build_ref_192()
	call_c   build_ref_179()
	call_c   Dyam_Create_Unary(&ref[192],&ref[179])
	move_ret ref[32]
	c_ret

;; TERM 192: compiler_analyzer
c_code local build_ref_192
	ret_reg &ref[192]
	call_c   Dyam_Create_Atom("compiler_analyzer")
	move_ret ref[192]
	c_ret

long local pool_fun21[4]=[3,build_ref_31,build_ref_32,build_ref_44]

pl_code local fun21
	call_c   Dyam_Pool(pool_fun21)
	call_c   Dyam_Unify_Item(&ref[31])
	fail_ret
	call_c   Dyam_Allocate(0)
	pl_call  Object_1(&ref[32])
	move     V(0), R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	move     &ref[44], R(4)
	move     S(5), R(5)
	move     0, R(6)
	call_c   Dyam_Reg_Deallocate(4)
fun20:
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Reg_Bind(2,V(4))
	call_c   Dyam_Multi_Reg_Bind(4,2,2)
	call_c   Dyam_Choice(fun19)
	pl_call  fun15(&seed[6],1)
	pl_fail


;; TERM 30: '*PROLOG-FIRST*'(analyze_at_clause(tag_tree{family=> _B, name=> _C, tree=> (auxtree _D)})) :> []
c_code local build_ref_30
	ret_reg &ref[30]
	call_c   build_ref_29()
	call_c   Dyam_Create_Binary(I(9),&ref[29],I(0))
	move_ret ref[30]
	c_ret

;; TERM 29: '*PROLOG-FIRST*'(analyze_at_clause(tag_tree{family=> _B, name=> _C, tree=> (auxtree _D)}))
c_code local build_ref_29
	ret_reg &ref[29]
	call_c   build_ref_27()
	call_c   build_ref_28()
	call_c   Dyam_Create_Unary(&ref[27],&ref[28])
	move_ret ref[29]
	c_ret

;; TERM 27: '*PROLOG-FIRST*'
c_code local build_ref_27
	ret_reg &ref[27]
	call_c   Dyam_Create_Atom("*PROLOG-FIRST*")
	move_ret ref[27]
	c_ret

;; TERM 26: '*PROLOG_FIRST*'
c_code local build_ref_26
	ret_reg &ref[26]
	call_c   Dyam_Create_Atom("*PROLOG_FIRST*")
	move_ret ref[26]
	c_ret

c_code local build_seed_4
	ret_reg &seed[4]
	call_c   build_ref_26()
	call_c   build_ref_47()
	call_c   Dyam_Seed_Start(&ref[26],&ref[47],I(0),fun1,1)
	call_c   build_ref_48()
	call_c   Dyam_Seed_Add_Comp(&ref[48],fun40,0)
	call_c   Dyam_Seed_End()
	move_ret seed[4]
	c_ret

;; TERM 48: '*PROLOG-ITEM*'{top=> emit_at_pda, cont=> _A}
c_code local build_ref_48
	ret_reg &ref[48]
	call_c   build_ref_183()
	call_c   build_ref_45()
	call_c   Dyam_Create_Binary(&ref[183],&ref[45],V(0))
	move_ret ref[48]
	c_ret

;; TERM 45: emit_at_pda
c_code local build_ref_45
	ret_reg &ref[45]
	call_c   Dyam_Create_Atom("emit_at_pda")
	move_ret ref[45]
	c_ret

;; TERM 75: '\n\n%% Feature tag mode\n\n'
c_code local build_ref_75
	ret_reg &ref[75]
	call_c   Dyam_Create_Atom("\n\n%% Feature tag mode\n\n")
	move_ret ref[75]
	c_ret

pl_code local fun28
	call_c   Dyam_Remove_Choice()
fun27:
	call_c   DYAM_Nl_0()
	fail_ret
	call_c   DYAM_Set_Output_1(V(1))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_jump  Follow_Cont(V(0))


;; TERM 76: tag_features_mode_tmp(_J, _Q, _G, _H)
c_code local build_ref_76
	ret_reg &ref[76]
	call_c   build_ref_193()
	call_c   Dyam_Term_Start(&ref[193],4)
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[76]
	c_ret

;; TERM 193: tag_features_mode_tmp
c_code local build_ref_193
	ret_reg &ref[193]
	call_c   Dyam_Create_Atom("tag_features_mode_tmp")
	move_ret ref[193]
	c_ret

;; TERM 77: tag_features_tmp(_J, _R, _S)
c_code local build_ref_77
	ret_reg &ref[77]
	call_c   build_ref_194()
	call_c   Dyam_Term_Start(&ref[194],3)
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_End()
	move_ret ref[77]
	c_ret

;; TERM 194: tag_features_tmp
c_code local build_ref_194
	ret_reg &ref[194]
	call_c   Dyam_Create_Atom("tag_features_tmp")
	move_ret ref[194]
	c_ret

;; TERM 71: [_G,_H]
c_code local build_ref_71
	ret_reg &ref[71]
	call_c   Dyam_Create_Tupple(6,7,I(0))
	move_ret ref[71]
	c_ret

;; TERM 78: ':-tag_features_mode(~k,~k,~k,~k).\n'
c_code local build_ref_78
	ret_reg &ref[78]
	call_c   Dyam_Create_Atom(":-tag_features_mode(~k,~k,~k,~k).\n")
	move_ret ref[78]
	c_ret

;; TERM 79: [_J,_Q,_G,_H]
c_code local build_ref_79
	ret_reg &ref[79]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_71()
	call_c   Dyam_Create_List(V(16),&ref[71])
	move_ret R(0)
	call_c   Dyam_Create_List(V(9),R(0))
	move_ret ref[79]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun29[7]=[6,build_ref_75,build_ref_76,build_ref_77,build_ref_71,build_ref_78,build_ref_79]

pl_code local fun32
	call_c   Dyam_Remove_Choice()
fun29:
	move     &ref[75], R(0)
	move     0, R(1)
	move     I(0), R(2)
	move     0, R(3)
	pl_call  pred_format_2()
	call_c   Dyam_Choice(fun28)
	pl_call  Object_1(&ref[76])
	pl_call  Object_1(&ref[77])
	call_c   Dyam_Reg_Load(0,V(17))
	call_c   Dyam_Reg_Load(2,V(18))
	call_c   Dyam_Reg_Load(4,V(6))
	call_c   Dyam_Reg_Load(6,V(7))
	pl_call  pred_tag_features_subsumes_4()
	call_c   Dyam_Reg_Load(2,&ref[71])
	call_c   DyALog_Assign_Display_Info(&R(2))
	move     &ref[78], R(0)
	move     0, R(1)
	move     &ref[79], R(2)
	move     S(5), R(3)
	pl_call  pred_format_2()
	pl_fail


;; TERM 70: tag_features_tmp(_J, _G, _H)
c_code local build_ref_70
	ret_reg &ref[70]
	call_c   build_ref_194()
	call_c   Dyam_Term_Start(&ref[194],3)
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[70]
	c_ret

;; TERM 73: ':-tag_features(~k,~k,~k).\n'
c_code local build_ref_73
	ret_reg &ref[73]
	call_c   Dyam_Create_Atom(":-tag_features(~k,~k,~k).\n")
	move_ret ref[73]
	c_ret

;; TERM 74: [_J,_G,_H]
c_code local build_ref_74
	ret_reg &ref[74]
	call_c   build_ref_71()
	call_c   Dyam_Create_List(V(9),&ref[71])
	move_ret ref[74]
	c_ret

long local pool_fun30[3]=[2,build_ref_73,build_ref_74]

pl_code local fun31
	call_c   Dyam_Remove_Choice()
fun30:
	move     &ref[73], R(0)
	move     0, R(1)
	move     &ref[74], R(2)
	move     S(5), R(3)
	pl_call  pred_format_2()
	pl_fail


;; TERM 72: tag_features(_J, _R, _S)
c_code local build_ref_72
	ret_reg &ref[72]
	call_c   build_ref_179()
	call_c   Dyam_Term_Start(&ref[179],3)
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_End()
	move_ret ref[72]
	c_ret

long local pool_fun33[7]=[196611,build_ref_70,build_ref_71,build_ref_72,pool_fun29,pool_fun30,pool_fun30]

pl_code local fun35
	call_c   Dyam_Remove_Choice()
fun33:
	pl_call  pred_tag_features_fixpoint_0()
	call_c   Dyam_Choice(fun32)
	pl_call  Object_1(&ref[70])
	call_c   Dyam_Reg_Load(2,&ref[71])
	call_c   DyALog_Assign_Display_Info(&R(2))
	call_c   Dyam_Choice(fun31)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[72])
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(17))
	call_c   Dyam_Reg_Load(2,V(18))
	call_c   Dyam_Reg_Load(4,V(6))
	call_c   Dyam_Reg_Load(6,V(7))
	pl_call  pred_tag_features_subsumes_4()
	pl_jump  fun30()


c_code local build_seed_8
	ret_reg &seed[8]
	call_c   build_ref_39()
	call_c   build_ref_58()
	call_c   Dyam_Seed_Start(&ref[39],&ref[58],I(0),fun17,1)
	call_c   build_ref_56()
	call_c   Dyam_Seed_Add_Comp(&ref[56],fun22,0)
	call_c   Dyam_Seed_End()
	move_ret seed[8]
	c_ret

;; TERM 56: '*RITEM*'('call_normalize/2'(tag_tree{family=> _K, name=> _L, tree=> (auxtree _M)}), return(_F))
c_code local build_ref_56
	ret_reg &ref[56]
	call_c   build_ref_0()
	call_c   build_ref_54()
	call_c   build_ref_55()
	call_c   Dyam_Create_Binary(&ref[0],&ref[54],&ref[55])
	move_ret ref[56]
	c_ret

;; TERM 55: return(_F)
c_code local build_ref_55
	ret_reg &ref[55]
	call_c   build_ref_195()
	call_c   Dyam_Create_Unary(&ref[195],V(5))
	move_ret ref[55]
	c_ret

;; TERM 195: return
c_code local build_ref_195
	ret_reg &ref[195]
	call_c   Dyam_Create_Atom("return")
	move_ret ref[195]
	c_ret

;; TERM 54: 'call_normalize/2'(tag_tree{family=> _K, name=> _L, tree=> (auxtree _M)})
c_code local build_ref_54
	ret_reg &ref[54]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_186()
	call_c   Dyam_Create_Unary(&ref[186],V(12))
	move_ret R(0)
	call_c   build_ref_185()
	call_c   Dyam_Term_Start(&ref[185],3)
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_196()
	call_c   Dyam_Create_Unary(&ref[196],R(0))
	move_ret ref[54]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 196: 'call_normalize/2'
c_code local build_ref_196
	ret_reg &ref[196]
	call_c   Dyam_Create_Atom("call_normalize/2")
	move_ret ref[196]
	c_ret

;; TERM 0: '*RITEM*'
c_code local build_ref_0
	ret_reg &ref[0]
	call_c   Dyam_Create_Atom("*RITEM*")
	move_ret ref[0]
	c_ret

pl_code local fun22
	call_c   build_ref_56()
	call_c   Dyam_Unify_Item(&ref[56])
	fail_ret
	pl_ret

pl_code local fun17
	pl_jump  Apply(0,0)

;; TERM 58: '*RITEM*'('call_normalize/2'(tag_tree{family=> _K, name=> _L, tree=> (auxtree _M)}), return(_F)) :> tag_features(1, '$TUPPLE'(35063484914696))
c_code local build_ref_58
	ret_reg &ref[58]
	call_c   build_ref_56()
	call_c   build_ref_57()
	call_c   Dyam_Create_Binary(I(9),&ref[56],&ref[57])
	move_ret ref[58]
	c_ret

;; TERM 57: tag_features(1, '$TUPPLE'(35063484914696))
c_code local build_ref_57
	ret_reg &ref[57]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,268435456)
	move_ret R(0)
	call_c   build_ref_179()
	call_c   Dyam_Create_Binary(&ref[179],N(1),R(0))
	move_ret ref[57]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 39: '*CURNEXT*'
c_code local build_ref_39
	ret_reg &ref[39]
	call_c   Dyam_Create_Atom("*CURNEXT*")
	move_ret ref[39]
	c_ret

pl_code local fun23
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Light_Object(R(0))
	pl_jump  Schedule_Prolog()

;; TERM 83: '$CLOSURE'('$fun'(34, 0, 1113441084), '$TUPPLE'(35063484915248))
c_code local build_ref_83
	ret_reg &ref[83]
	call_c   build_ref_82()
	call_c   Dyam_Closure_Aux(fun34,&ref[82])
	move_ret ref[83]
	c_ret

;; TERM 68: '%% Skel ~w: N=~w Root=~w Foot=~w Skel=~w\n'
c_code local build_ref_68
	ret_reg &ref[68]
	call_c   Dyam_Create_Atom("%% Skel ~w: N=~w Root=~w Foot=~w Skel=~w\n")
	move_ret ref[68]
	c_ret

;; TERM 69: [_L,_J,_O,_P,_N]
c_code local build_ref_69
	ret_reg &ref[69]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_142()
	call_c   Dyam_Create_Tupple(14,15,&ref[142])
	move_ret R(0)
	call_c   Dyam_Create_List(V(9),R(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(11),R(0))
	move_ret ref[69]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 142: [_N]
c_code local build_ref_142
	ret_reg &ref[142]
	call_c   Dyam_Create_List(V(13),I(0))
	move_ret ref[142]
	c_ret

long local pool_fun34[3]=[2,build_ref_68,build_ref_69]

pl_code local fun34
	call_c   Dyam_Pool(pool_fun34)
	call_c   Dyam_Allocate(0)
	move     &ref[68], R(0)
	move     0, R(1)
	move     &ref[69], R(2)
	move     S(5), R(3)
	pl_call  pred_format_2()
	pl_fail

;; TERM 82: '$TUPPLE'(35063484915248)
c_code local build_ref_82
	ret_reg &ref[82]
	call_c   Dyam_Create_Simple_Tupple(0,409661440)
	move_ret ref[82]
	c_ret

;; TERM 84: tag_features_skel(_L, _F, _N, _J, _O, _P, _Q)
c_code local build_ref_84
	ret_reg &ref[84]
	call_c   build_ref_197()
	call_c   Dyam_Term_Start(&ref[197],7)
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_End()
	move_ret ref[84]
	c_ret

;; TERM 197: tag_features_skel
c_code local build_ref_197
	ret_reg &ref[197]
	call_c   Dyam_Create_Atom("tag_features_skel")
	move_ret ref[197]
	c_ret

long local pool_fun36[5]=[65539,build_seed_8,build_ref_83,build_ref_84,pool_fun33]

pl_code local fun37
	call_c   Dyam_Remove_Choice()
fun36:
	call_c   Dyam_Choice(fun35)
	pl_call  fun23(&seed[8],2)
	move     &ref[83], R(0)
	move     S(5), R(1)
	move     &ref[84], R(2)
	move     S(5), R(3)
	move     I(0), R(4)
	move     0, R(5)
	call_c   Dyam_Reg_Deallocate(3)
fun26:
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Reg_Unify(2,&ref[67])
	fail_ret
	call_c   Dyam_Reg_Bind(4,V(9))
	call_c   Dyam_Choice(fun25)
	pl_call  fun15(&seed[9],1)
	pl_fail



;; TERM 51: tag_features(_F, _G, _H)
c_code local build_ref_51
	ret_reg &ref[51]
	call_c   build_ref_179()
	call_c   Dyam_Term_Start(&ref[179],3)
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[51]
	c_ret

;; TERM 52: tag_nt(_F, (_I / _J))
c_code local build_ref_52
	ret_reg &ref[52]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_199()
	call_c   Dyam_Create_Binary(&ref[199],V(8),V(9))
	move_ret R(0)
	call_c   build_ref_198()
	call_c   Dyam_Create_Binary(&ref[198],V(5),R(0))
	move_ret ref[52]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 198: tag_nt
c_code local build_ref_198
	ret_reg &ref[198]
	call_c   Dyam_Create_Atom("tag_nt")
	move_ret ref[198]
	c_ret

;; TERM 199: /
c_code local build_ref_199
	ret_reg &ref[199]
	call_c   Dyam_Create_Atom("/")
	move_ret ref[199]
	c_ret

;; TERM 53: tag_features_tmp(_F, _G, _H)
c_code local build_ref_53
	ret_reg &ref[53]
	call_c   build_ref_194()
	call_c   Dyam_Term_Start(&ref[194],3)
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[53]
	c_ret

long local pool_fun38[5]=[65539,build_ref_51,build_ref_52,build_ref_53,pool_fun36]

pl_code local fun39
	call_c   Dyam_Remove_Choice()
fun38:
	pl_call  pred_tag_features_emit_header_0()
	call_c   Dyam_Choice(fun37)
	pl_call  Object_1(&ref[51])
	call_c   Dyam_Unify(V(6),V(7))
	fail_ret
	call_c   DYAM_evpred_functor(V(6),V(8),V(9))
	fail_ret
	move     &ref[52], R(0)
	move     S(5), R(1)
	pl_call  pred_record_without_doublon_1()
	call_c   DYAM_evpred_assert_1(&ref[53])
	pl_fail


;; TERM 49: output_file(_C)
c_code local build_ref_49
	ret_reg &ref[49]
	call_c   build_ref_200()
	call_c   Dyam_Create_Unary(&ref[200],V(2))
	move_ret ref[49]
	c_ret

;; TERM 200: output_file
c_code local build_ref_200
	ret_reg &ref[200]
	call_c   Dyam_Create_Atom("output_file")
	move_ret ref[200]
	c_ret

;; TERM 50: write
c_code local build_ref_50
	ret_reg &ref[50]
	call_c   Dyam_Create_Atom("write")
	move_ret ref[50]
	c_ret

long local pool_fun40[7]=[131076,build_ref_48,build_ref_32,build_ref_49,build_ref_50,pool_fun38,pool_fun38]

pl_code local fun40
	call_c   Dyam_Pool(pool_fun40)
	call_c   Dyam_Unify_Item(&ref[48])
	fail_ret
	call_c   Dyam_Allocate(0)
	pl_call  Object_1(&ref[32])
	call_c   DYAM_Current_Output_1(V(1))
	fail_ret
	call_c   Dyam_Choice(fun39)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[49])
	call_c   Dyam_Cut()
	call_c   DYAM_Absolute_File_Name(V(2),V(3))
	fail_ret
	call_c   DYAM_Open_3(V(3),&ref[50],V(4))
	fail_ret
	call_c   DYAM_Set_Output_1(V(4))
	fail_ret
	pl_jump  fun38()

;; TERM 47: '*PROLOG-FIRST*'(emit_at_pda) :> []
c_code local build_ref_47
	ret_reg &ref[47]
	call_c   build_ref_46()
	call_c   Dyam_Create_Binary(I(9),&ref[46],I(0))
	move_ret ref[47]
	c_ret

;; TERM 46: '*PROLOG-FIRST*'(emit_at_pda)
c_code local build_ref_46
	ret_reg &ref[46]
	call_c   build_ref_27()
	call_c   build_ref_45()
	call_c   Dyam_Create_Unary(&ref[27],&ref[45])
	move_ret ref[46]
	c_ret

c_code local build_seed_2
	ret_reg &seed[2]
	call_c   build_ref_36()
	call_c   build_ref_87()
	call_c   Dyam_Seed_Start(&ref[36],&ref[87],I(0),fun17,1)
	call_c   build_ref_88()
	call_c   Dyam_Seed_Add_Comp(&ref[88],fun53,0)
	call_c   Dyam_Seed_End()
	move_ret seed[2]
	c_ret

;; TERM 88: '*CITEM*'('call_tag_features_skel/7'(_B, _C), _A)
c_code local build_ref_88
	ret_reg &ref[88]
	call_c   build_ref_33()
	call_c   build_ref_85()
	call_c   Dyam_Create_Binary(&ref[33],&ref[85],V(0))
	move_ret ref[88]
	c_ret

;; TERM 85: 'call_tag_features_skel/7'(_B, _C)
c_code local build_ref_85
	ret_reg &ref[85]
	call_c   build_ref_201()
	call_c   Dyam_Create_Binary(&ref[201],V(1),V(2))
	move_ret ref[85]
	c_ret

;; TERM 201: 'call_tag_features_skel/7'
c_code local build_ref_201
	ret_reg &ref[201]
	call_c   Dyam_Create_Atom("call_tag_features_skel/7")
	move_ret ref[201]
	c_ret

;; TERM 33: '*CITEM*'
c_code local build_ref_33
	ret_reg &ref[33]
	call_c   Dyam_Create_Atom("*CITEM*")
	move_ret ref[33]
	c_ret

;; TERM 101: guard{goal=> tag_node{id=> _I, label=> _E, children=> _J, kind=> _K, adj=> _L, spine=> _M, top=> _F, bot=> _N, token=> _O, adjleft=> _P, adjright=> _Q, adjwrap=> _R}, plus=> _S, minus=> _T}
c_code local build_ref_101
	ret_reg &ref[101]
	call_c   build_ref_202()
	call_c   build_ref_89()
	call_c   Dyam_Term_Start(&ref[202],3)
	call_c   Dyam_Term_Arg(&ref[89])
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_End()
	move_ret ref[101]
	c_ret

;; TERM 89: tag_node{id=> _I, label=> _E, children=> _J, kind=> _K, adj=> _L, spine=> _M, top=> _F, bot=> _N, token=> _O, adjleft=> _P, adjright=> _Q, adjwrap=> _R}
c_code local build_ref_89
	ret_reg &ref[89]
	call_c   build_ref_203()
	call_c   Dyam_Term_Start(&ref[203],12)
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_End()
	move_ret ref[89]
	c_ret

;; TERM 203: tag_node!'$ft'
c_code local build_ref_203
	ret_reg &ref[203]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_216()
	call_c   Dyam_Create_List(&ref[216],I(0))
	move_ret R(0)
	call_c   build_ref_215()
	call_c   Dyam_Create_List(&ref[215],R(0))
	move_ret R(0)
	call_c   build_ref_214()
	call_c   Dyam_Create_List(&ref[214],R(0))
	move_ret R(0)
	call_c   build_ref_213()
	call_c   Dyam_Create_List(&ref[213],R(0))
	move_ret R(0)
	call_c   build_ref_212()
	call_c   Dyam_Create_List(&ref[212],R(0))
	move_ret R(0)
	call_c   build_ref_211()
	call_c   Dyam_Create_List(&ref[211],R(0))
	move_ret R(0)
	call_c   build_ref_210()
	call_c   Dyam_Create_List(&ref[210],R(0))
	move_ret R(0)
	call_c   build_ref_209()
	call_c   Dyam_Create_List(&ref[209],R(0))
	move_ret R(0)
	call_c   build_ref_208()
	call_c   Dyam_Create_List(&ref[208],R(0))
	move_ret R(0)
	call_c   build_ref_207()
	call_c   Dyam_Create_List(&ref[207],R(0))
	move_ret R(0)
	call_c   build_ref_206()
	call_c   Dyam_Create_List(&ref[206],R(0))
	move_ret R(0)
	call_c   build_ref_205()
	call_c   Dyam_Create_List(&ref[205],R(0))
	move_ret R(0)
	call_c   build_ref_204()
	call_c   Dyam_Create_Atom_Module("$ft",&ref[204])
	move_ret ref[203]
	call_c   DYAM_Feature_2(&ref[204],R(0))
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 204: tag_node
c_code local build_ref_204
	ret_reg &ref[204]
	call_c   Dyam_Create_Atom("tag_node")
	move_ret ref[204]
	c_ret

;; TERM 205: id
c_code local build_ref_205
	ret_reg &ref[205]
	call_c   Dyam_Create_Atom("id")
	move_ret ref[205]
	c_ret

;; TERM 206: label
c_code local build_ref_206
	ret_reg &ref[206]
	call_c   Dyam_Create_Atom("label")
	move_ret ref[206]
	c_ret

;; TERM 207: children
c_code local build_ref_207
	ret_reg &ref[207]
	call_c   Dyam_Create_Atom("children")
	move_ret ref[207]
	c_ret

;; TERM 208: kind
c_code local build_ref_208
	ret_reg &ref[208]
	call_c   Dyam_Create_Atom("kind")
	move_ret ref[208]
	c_ret

;; TERM 209: adj
c_code local build_ref_209
	ret_reg &ref[209]
	call_c   Dyam_Create_Atom("adj")
	move_ret ref[209]
	c_ret

;; TERM 210: spine
c_code local build_ref_210
	ret_reg &ref[210]
	call_c   Dyam_Create_Atom("spine")
	move_ret ref[210]
	c_ret

;; TERM 211: top
c_code local build_ref_211
	ret_reg &ref[211]
	call_c   Dyam_Create_Atom("top")
	move_ret ref[211]
	c_ret

;; TERM 212: bot
c_code local build_ref_212
	ret_reg &ref[212]
	call_c   Dyam_Create_Atom("bot")
	move_ret ref[212]
	c_ret

;; TERM 213: token
c_code local build_ref_213
	ret_reg &ref[213]
	call_c   Dyam_Create_Atom("token")
	move_ret ref[213]
	c_ret

;; TERM 214: adjleft
c_code local build_ref_214
	ret_reg &ref[214]
	call_c   Dyam_Create_Atom("adjleft")
	move_ret ref[214]
	c_ret

;; TERM 215: adjright
c_code local build_ref_215
	ret_reg &ref[215]
	call_c   Dyam_Create_Atom("adjright")
	move_ret ref[215]
	c_ret

;; TERM 216: adjwrap
c_code local build_ref_216
	ret_reg &ref[216]
	call_c   Dyam_Create_Atom("adjwrap")
	move_ret ref[216]
	c_ret

;; TERM 202: guard!'$ft'
c_code local build_ref_202
	ret_reg &ref[202]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_220()
	call_c   Dyam_Create_List(&ref[220],I(0))
	move_ret R(0)
	call_c   build_ref_219()
	call_c   Dyam_Create_List(&ref[219],R(0))
	move_ret R(0)
	call_c   build_ref_218()
	call_c   Dyam_Create_List(&ref[218],R(0))
	move_ret R(0)
	call_c   build_ref_217()
	call_c   Dyam_Create_Atom_Module("$ft",&ref[217])
	move_ret ref[202]
	call_c   DYAM_Feature_2(&ref[217],R(0))
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 217: guard
c_code local build_ref_217
	ret_reg &ref[217]
	call_c   Dyam_Create_Atom("guard")
	move_ret ref[217]
	c_ret

;; TERM 218: goal
c_code local build_ref_218
	ret_reg &ref[218]
	call_c   Dyam_Create_Atom("goal")
	move_ret ref[218]
	c_ret

;; TERM 219: plus
c_code local build_ref_219
	ret_reg &ref[219]
	call_c   Dyam_Create_Atom("plus")
	move_ret ref[219]
	c_ret

;; TERM 220: minus
c_code local build_ref_220
	ret_reg &ref[220]
	call_c   Dyam_Create_Atom("minus")
	move_ret ref[220]
	c_ret

;; TERM 100: wrap
c_code local build_ref_100
	ret_reg &ref[100]
	call_c   Dyam_Create_Atom("wrap")
	move_ret ref[100]
	c_ret

;; TERM 91: tag_nt(_E, (_U / _V))
c_code local build_ref_91
	ret_reg &ref[91]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_199()
	call_c   Dyam_Create_Binary(&ref[199],V(20),V(21))
	move_ret R(0)
	call_c   build_ref_198()
	call_c   Dyam_Create_Binary(&ref[198],V(4),R(0))
	move_ret ref[91]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 99: tag_features_tmp(_E, _Y, _Y)
c_code local build_ref_99
	ret_reg &ref[99]
	call_c   build_ref_194()
	call_c   Dyam_Term_Start(&ref[194],3)
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(24))
	call_c   Dyam_Term_Arg(V(24))
	call_c   Dyam_Term_End()
	move_ret ref[99]
	c_ret

;; TERM 98: tag_features_mode_tmp(_E, _H, _Y, _Y)
c_code local build_ref_98
	ret_reg &ref[98]
	call_c   build_ref_193()
	call_c   Dyam_Term_Start(&ref[193],4)
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(24))
	call_c   Dyam_Term_Arg(V(24))
	call_c   Dyam_Term_End()
	move_ret ref[98]
	c_ret

c_code local build_seed_11
	ret_reg &seed[11]
	call_c   build_ref_0()
	call_c   build_ref_96()
	call_c   Dyam_Seed_Start(&ref[0],&ref[96],&ref[96],fun13,1)
	call_c   build_ref_97()
	call_c   Dyam_Seed_Add_Comp(&ref[97],&ref[96],0)
	call_c   Dyam_Seed_End()
	move_ret seed[11]
	c_ret

pl_code local fun13
	pl_jump  Complete(0,0)

;; TERM 97: '*RITEM*'(_A, return(_D, _E, _F, _G, _H)) :> '$$HOLE$$'
c_code local build_ref_97
	ret_reg &ref[97]
	call_c   build_ref_96()
	call_c   Dyam_Create_Binary(I(9),&ref[96],I(7))
	move_ret ref[97]
	c_ret

;; TERM 96: '*RITEM*'(_A, return(_D, _E, _F, _G, _H))
c_code local build_ref_96
	ret_reg &ref[96]
	call_c   build_ref_0()
	call_c   build_ref_95()
	call_c   Dyam_Create_Binary(&ref[0],V(0),&ref[95])
	move_ret ref[96]
	c_ret

;; TERM 95: return(_D, _E, _F, _G, _H)
c_code local build_ref_95
	ret_reg &ref[95]
	call_c   build_ref_195()
	call_c   Dyam_Term_Start(&ref[195],5)
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[95]
	c_ret

pl_code local fun41
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Object(R(0))
	call_c   Dyam_Tabule()
	pl_ret

pl_code local fun42
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Choice(fun41)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Subsume(R(0))
	fail_ret
	call_c   Dyam_Cut()
	pl_ret

long local pool_fun43[2]=[1,build_seed_11]

pl_code local fun44
	call_c   Dyam_Remove_Choice()
fun43:
	call_c   Dyam_Deallocate()
	pl_jump  fun42(&seed[11],2)


;; TERM 94: 'Foot node not reached in ~w\n\ttree=~w'
c_code local build_ref_94
	ret_reg &ref[94]
	call_c   Dyam_Create_Atom("Foot node not reached in ~w\n\ttree=~w")
	move_ret ref[94]
	c_ret

;; TERM 19: [_B,_C]
c_code local build_ref_19
	ret_reg &ref[19]
	call_c   Dyam_Create_Tupple(1,2,I(0))
	move_ret ref[19]
	c_ret

long local pool_fun45[6]=[131075,build_ref_89,build_ref_94,build_ref_19,pool_fun43,pool_fun43]

long local pool_fun46[3]=[65537,build_ref_98,pool_fun45]

pl_code local fun46
	call_c   Dyam_Remove_Choice()
	call_c   DYAM_evpred_functor(V(5),V(20),V(21))
	fail_ret
	call_c   DYAM_evpred_functor(V(24),V(20),V(21))
	fail_ret
	call_c   DYAM_evpred_assert_1(&ref[98])
fun45:
	move     &ref[89], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	move     V(3), R(4)
	move     S(5), R(5)
	move     V(6), R(6)
	move     S(5), R(7)
	pl_call  pred_tag_features_skel_node_4()
	call_c   Dyam_Choice(fun44)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_variable(V(6))
	fail_ret
	call_c   Dyam_Cut()
	move     &ref[94], R(0)
	move     0, R(1)
	move     &ref[19], R(2)
	move     S(5), R(3)
	pl_call  pred_error_2()
	pl_jump  fun43()


;; TERM 93: tag_features_mode_tmp(_E, _H, _Z, _A1)
c_code local build_ref_93
	ret_reg &ref[93]
	call_c   build_ref_193()
	call_c   Dyam_Term_Start(&ref[193],4)
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(25))
	call_c   Dyam_Term_Arg(V(26))
	call_c   Dyam_Term_End()
	move_ret ref[93]
	c_ret

long local pool_fun47[4]=[131073,build_ref_93,pool_fun46,pool_fun45]

long local pool_fun48[3]=[65537,build_ref_99,pool_fun47]

pl_code local fun48
	call_c   Dyam_Remove_Choice()
	call_c   DYAM_evpred_functor(V(5),V(20),V(21))
	fail_ret
	call_c   DYAM_evpred_functor(V(24),V(20),V(21))
	fail_ret
	call_c   DYAM_evpred_assert_1(&ref[99])
fun47:
	call_c   Dyam_Choice(fun46)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[93])
	call_c   Dyam_Cut()
	pl_jump  fun45()


;; TERM 92: tag_features_tmp(_E, _W, _X)
c_code local build_ref_92
	ret_reg &ref[92]
	call_c   build_ref_194()
	call_c   Dyam_Term_Start(&ref[194],3)
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(22))
	call_c   Dyam_Term_Arg(V(23))
	call_c   Dyam_Term_End()
	move_ret ref[92]
	c_ret

long local pool_fun49[5]=[131074,build_ref_91,build_ref_92,pool_fun48,pool_fun47]

long local pool_fun50[3]=[65537,build_ref_100,pool_fun49]

pl_code local fun50
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(7),&ref[100])
	fail_ret
fun49:
	call_c   DYAM_evpred_functor(V(5),V(20),V(21))
	fail_ret
	move     &ref[91], R(0)
	move     S(5), R(1)
	pl_call  pred_record_without_doublon_1()
	call_c   Dyam_Choice(fun48)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[92])
	call_c   Dyam_Cut()
	pl_jump  fun47()


;; TERM 90: tig(_B, _H)
c_code local build_ref_90
	ret_reg &ref[90]
	call_c   build_ref_221()
	call_c   Dyam_Create_Binary(&ref[221],V(1),V(7))
	move_ret ref[90]
	c_ret

;; TERM 221: tig
c_code local build_ref_221
	ret_reg &ref[221]
	call_c   Dyam_Create_Atom("tig")
	move_ret ref[221]
	c_ret

long local pool_fun51[4]=[131073,build_ref_90,pool_fun50,pool_fun49]

long local pool_fun52[3]=[65537,build_ref_101,pool_fun51]

pl_code local fun52
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(2),&ref[101])
	fail_ret
fun51:
	call_c   Dyam_Choice(fun50)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[90])
	call_c   Dyam_Cut()
	pl_jump  fun49()


long local pool_fun53[5]=[131074,build_ref_88,build_ref_89,pool_fun52,pool_fun51]

pl_code local fun53
	call_c   Dyam_Pool(pool_fun53)
	call_c   Dyam_Unify_Item(&ref[88])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun52)
	call_c   Dyam_Unify(V(2),&ref[89])
	fail_ret
	pl_jump  fun51()

;; TERM 87: '*FIRST*'('call_tag_features_skel/7'(_B, _C)) :> []
c_code local build_ref_87
	ret_reg &ref[87]
	call_c   build_ref_86()
	call_c   Dyam_Create_Binary(I(9),&ref[86],I(0))
	move_ret ref[87]
	c_ret

;; TERM 86: '*FIRST*'('call_tag_features_skel/7'(_B, _C))
c_code local build_ref_86
	ret_reg &ref[86]
	call_c   build_ref_36()
	call_c   build_ref_85()
	call_c   Dyam_Create_Unary(&ref[36],&ref[85])
	move_ret ref[86]
	c_ret

;; TERM 36: '*FIRST*'
c_code local build_ref_36
	ret_reg &ref[36]
	call_c   Dyam_Create_Atom("*FIRST*")
	move_ret ref[36]
	c_ret

c_code local build_seed_1
	ret_reg &seed[1]
	call_c   build_ref_9()
	call_c   build_ref_130()
	call_c   Dyam_Seed_Start(&ref[9],&ref[130],I(0),fun1,1)
	call_c   build_ref_129()
	call_c   Dyam_Seed_Add_Comp(&ref[129],fun69,0)
	call_c   Dyam_Seed_End()
	move_ret seed[1]
	c_ret

;; TERM 129: '*GUARD*'(tag_features_combine_aux([_B|_C], [_D|_E], [_F|_G], [_H|_I], [_J|_K], [_L|_M]))
c_code local build_ref_129
	ret_reg &ref[129]
	call_c   build_ref_10()
	call_c   build_ref_128()
	call_c   Dyam_Create_Unary(&ref[10],&ref[128])
	move_ret ref[129]
	c_ret

;; TERM 128: tag_features_combine_aux([_B|_C], [_D|_E], [_F|_G], [_H|_I], [_J|_K], [_L|_M])
c_code local build_ref_128
	ret_reg &ref[128]
	call_c   Dyam_Pseudo_Choice(5)
	call_c   Dyam_Create_List(V(1),V(2))
	move_ret R(0)
	call_c   Dyam_Create_List(V(3),V(4))
	move_ret R(1)
	call_c   Dyam_Create_List(V(5),V(6))
	move_ret R(2)
	call_c   Dyam_Create_List(V(9),V(10))
	move_ret R(3)
	call_c   Dyam_Create_List(V(11),V(12))
	move_ret R(4)
	call_c   build_ref_182()
	call_c   build_ref_119()
	call_c   Dyam_Term_Start(&ref[182],6)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(R(2))
	call_c   Dyam_Term_Arg(&ref[119])
	call_c   Dyam_Term_Arg(R(3))
	call_c   Dyam_Term_Arg(R(4))
	call_c   Dyam_Term_End()
	move_ret ref[128]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 119: [_H|_I]
c_code local build_ref_119
	ret_reg &ref[119]
	call_c   Dyam_Create_List(V(7),V(8))
	move_ret ref[119]
	c_ret

c_code local build_seed_14
	ret_reg &seed[14]
	call_c   build_ref_10()
	call_c   build_ref_132()
	call_c   Dyam_Seed_Start(&ref[10],&ref[132],I(0),fun13,1)
	call_c   build_ref_133()
	call_c   Dyam_Seed_Add_Comp(&ref[133],&ref[132],0)
	call_c   Dyam_Seed_End()
	move_ret seed[14]
	c_ret

;; TERM 133: '*GUARD*'(tag_features_combine_aux(_C, _E, _G, _I, _K, _M)) :> '$$HOLE$$'
c_code local build_ref_133
	ret_reg &ref[133]
	call_c   build_ref_132()
	call_c   Dyam_Create_Binary(I(9),&ref[132],I(7))
	move_ret ref[133]
	c_ret

;; TERM 132: '*GUARD*'(tag_features_combine_aux(_C, _E, _G, _I, _K, _M))
c_code local build_ref_132
	ret_reg &ref[132]
	call_c   build_ref_10()
	call_c   build_ref_131()
	call_c   Dyam_Create_Unary(&ref[10],&ref[131])
	move_ret ref[132]
	c_ret

;; TERM 131: tag_features_combine_aux(_C, _E, _G, _I, _K, _M)
c_code local build_ref_131
	ret_reg &ref[131]
	call_c   build_ref_182()
	call_c   Dyam_Term_Start(&ref[182],6)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_End()
	move_ret ref[131]
	c_ret

long local pool_fun67[2]=[1,build_seed_14]

pl_code local fun68
	call_c   Dyam_Remove_Choice()
fun67:
	call_c   Dyam_Deallocate()
	pl_jump  fun23(&seed[14],1)


long local pool_fun69[4]=[131073,build_ref_129,pool_fun67,pool_fun67]

pl_code local fun69
	call_c   Dyam_Pool(pool_fun69)
	call_c   Dyam_Unify_Item(&ref[129])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun68)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(1),V(3))
	fail_ret
	call_c   DYAM_sfol_identical(V(5),V(7))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(9),V(11))
	fail_ret
	pl_jump  fun67()

;; TERM 130: '*GUARD*'(tag_features_combine_aux([_B|_C], [_D|_E], [_F|_G], [_H|_I], [_J|_K], [_L|_M])) :> []
c_code local build_ref_130
	ret_reg &ref[130]
	call_c   build_ref_129()
	call_c   Dyam_Create_Binary(I(9),&ref[129],I(0))
	move_ret ref[130]
	c_ret

c_code local build_seed_13
	ret_reg &seed[13]
	call_c   build_ref_10()
	call_c   build_ref_124()
	call_c   Dyam_Seed_Start(&ref[10],&ref[124],I(0),fun13,1)
	call_c   build_ref_125()
	call_c   Dyam_Seed_Add_Comp(&ref[125],&ref[124],0)
	call_c   Dyam_Seed_End()
	move_ret seed[13]
	c_ret

;; TERM 125: '*GUARD*'(tag_features_combine_aux(_I, _J, _K, _L, _M, _N)) :> '$$HOLE$$'
c_code local build_ref_125
	ret_reg &ref[125]
	call_c   build_ref_124()
	call_c   Dyam_Create_Binary(I(9),&ref[124],I(7))
	move_ret ref[125]
	c_ret

;; TERM 124: '*GUARD*'(tag_features_combine_aux(_I, _J, _K, _L, _M, _N))
c_code local build_ref_124
	ret_reg &ref[124]
	call_c   build_ref_10()
	call_c   build_ref_123()
	call_c   Dyam_Create_Unary(&ref[10],&ref[123])
	move_ret ref[124]
	c_ret

;; TERM 123: tag_features_combine_aux(_I, _J, _K, _L, _M, _N)
c_code local build_ref_123
	ret_reg &ref[123]
	call_c   build_ref_182()
	call_c   Dyam_Term_Start(&ref[182],6)
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_End()
	move_ret ref[123]
	c_ret

c_code local build_seed_12
	ret_reg &seed[12]
	call_c   build_ref_39()
	call_c   build_ref_105()
	call_c   Dyam_Seed_Start(&ref[39],&ref[105],I(0),fun17,1)
	call_c   build_ref_103()
	call_c   Dyam_Seed_Add_Comp(&ref[103],fun54,0)
	call_c   Dyam_Seed_End()
	move_ret seed[12]
	c_ret

;; TERM 103: '*RITEM*'('call_tag_features_skel/7'(_B, _C), return(_D, _E, _F, _G, _H))
c_code local build_ref_103
	ret_reg &ref[103]
	call_c   build_ref_0()
	call_c   build_ref_85()
	call_c   build_ref_95()
	call_c   Dyam_Create_Binary(&ref[0],&ref[85],&ref[95])
	move_ret ref[103]
	c_ret

pl_code local fun54
	call_c   build_ref_103()
	call_c   Dyam_Unify_Item(&ref[103])
	fail_ret
	pl_ret

;; TERM 105: '*RITEM*'('call_tag_features_skel/7'(_B, _C), return(_D, _E, _F, _G, _H)) :> tag_features(3, '$TUPPLE'(0))
c_code local build_ref_105
	ret_reg &ref[105]
	call_c   build_ref_103()
	call_c   build_ref_104()
	call_c   Dyam_Create_Binary(I(9),&ref[103],&ref[104])
	move_ret ref[105]
	c_ret

;; TERM 104: tag_features(3, '$TUPPLE'(0))
c_code local build_ref_104
	ret_reg &ref[104]
	call_c   build_ref_179()
	call_c   Dyam_Create_Binary(&ref[179],N(3),I(0))
	move_ret ref[104]
	c_ret

c_code local build_seed_9
	ret_reg &seed[9]
	call_c   build_ref_33()
	call_c   build_ref_60()
	call_c   Dyam_Seed_Start(&ref[33],&ref[60],&ref[60],fun13,1)
	call_c   build_ref_62()
	call_c   Dyam_Seed_Add_Comp(&ref[62],&ref[60],0)
	call_c   Dyam_Seed_End()
	move_ret seed[9]
	c_ret

;; TERM 62: '*FIRST*'('call_tag_features_skel/7'(_C, _D)) :> '$$HOLE$$'
c_code local build_ref_62
	ret_reg &ref[62]
	call_c   build_ref_61()
	call_c   Dyam_Create_Binary(I(9),&ref[61],I(7))
	move_ret ref[62]
	c_ret

;; TERM 61: '*FIRST*'('call_tag_features_skel/7'(_C, _D))
c_code local build_ref_61
	ret_reg &ref[61]
	call_c   build_ref_36()
	call_c   build_ref_59()
	call_c   Dyam_Create_Unary(&ref[36],&ref[59])
	move_ret ref[61]
	c_ret

;; TERM 59: 'call_tag_features_skel/7'(_C, _D)
c_code local build_ref_59
	ret_reg &ref[59]
	call_c   build_ref_201()
	call_c   Dyam_Create_Binary(&ref[201],V(2),V(3))
	move_ret ref[59]
	c_ret

;; TERM 60: '*CITEM*'('call_tag_features_skel/7'(_C, _D), 'call_tag_features_skel/7'(_C, _D))
c_code local build_ref_60
	ret_reg &ref[60]
	call_c   build_ref_33()
	call_c   build_ref_59()
	call_c   Dyam_Create_Binary(&ref[33],&ref[59],&ref[59])
	move_ret ref[60]
	c_ret

c_code local build_seed_10
	ret_reg &seed[10]
	call_c   build_ref_39()
	call_c   build_ref_66()
	call_c   Dyam_Seed_Start(&ref[39],&ref[66],I(0),fun17,1)
	call_c   build_ref_64()
	call_c   Dyam_Seed_Add_Comp(&ref[64],fun24,0)
	call_c   Dyam_Seed_End()
	move_ret seed[10]
	c_ret

;; TERM 64: '*RITEM*'('call_tag_features_skel/7'(_C, _D), return(_E, _F, _G, _H, _I))
c_code local build_ref_64
	ret_reg &ref[64]
	call_c   build_ref_0()
	call_c   build_ref_59()
	call_c   build_ref_63()
	call_c   Dyam_Create_Binary(&ref[0],&ref[59],&ref[63])
	move_ret ref[64]
	c_ret

;; TERM 63: return(_E, _F, _G, _H, _I)
c_code local build_ref_63
	ret_reg &ref[63]
	call_c   build_ref_195()
	call_c   Dyam_Term_Start(&ref[195],5)
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_End()
	move_ret ref[63]
	c_ret

pl_code local fun24
	call_c   build_ref_64()
	call_c   Dyam_Unify_Item(&ref[64])
	fail_ret
	pl_jump  Follow_Cont(V(1))

;; TERM 66: '*RITEM*'('call_tag_features_skel/7'(_C, _D), return(_E, _F, _G, _H, _I)) :> tag_features(2, '$TUPPLE'(35063484913968))
c_code local build_ref_66
	ret_reg &ref[66]
	call_c   build_ref_64()
	call_c   build_ref_65()
	call_c   Dyam_Create_Binary(I(9),&ref[64],&ref[65])
	move_ret ref[66]
	c_ret

;; TERM 65: tag_features(2, '$TUPPLE'(35063484913968))
c_code local build_ref_65
	ret_reg &ref[65]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,134217728)
	move_ret R(0)
	call_c   build_ref_179()
	call_c   Dyam_Create_Binary(&ref[179],N(2),R(0))
	move_ret ref[65]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_6
	ret_reg &seed[6]
	call_c   build_ref_33()
	call_c   build_ref_35()
	call_c   Dyam_Seed_Start(&ref[33],&ref[35],&ref[35],fun13,1)
	call_c   build_ref_38()
	call_c   Dyam_Seed_Add_Comp(&ref[38],&ref[35],0)
	call_c   Dyam_Seed_End()
	move_ret seed[6]
	c_ret

;; TERM 38: '*FIRST*'('call_normalize/2'(_C)) :> '$$HOLE$$'
c_code local build_ref_38
	ret_reg &ref[38]
	call_c   build_ref_37()
	call_c   Dyam_Create_Binary(I(9),&ref[37],I(7))
	move_ret ref[38]
	c_ret

;; TERM 37: '*FIRST*'('call_normalize/2'(_C))
c_code local build_ref_37
	ret_reg &ref[37]
	call_c   build_ref_36()
	call_c   build_ref_34()
	call_c   Dyam_Create_Unary(&ref[36],&ref[34])
	move_ret ref[37]
	c_ret

;; TERM 34: 'call_normalize/2'(_C)
c_code local build_ref_34
	ret_reg &ref[34]
	call_c   build_ref_196()
	call_c   Dyam_Create_Unary(&ref[196],V(2))
	move_ret ref[34]
	c_ret

;; TERM 35: '*CITEM*'('call_normalize/2'(_C), 'call_normalize/2'(_C))
c_code local build_ref_35
	ret_reg &ref[35]
	call_c   build_ref_33()
	call_c   build_ref_34()
	call_c   Dyam_Create_Binary(&ref[33],&ref[34],&ref[34])
	move_ret ref[35]
	c_ret

c_code local build_seed_7
	ret_reg &seed[7]
	call_c   build_ref_39()
	call_c   build_ref_43()
	call_c   Dyam_Seed_Start(&ref[39],&ref[43],I(0),fun17,1)
	call_c   build_ref_41()
	call_c   Dyam_Seed_Add_Comp(&ref[41],fun16,0)
	call_c   Dyam_Seed_End()
	move_ret seed[7]
	c_ret

;; TERM 41: '*RITEM*'('call_normalize/2'(_C), return(_D))
c_code local build_ref_41
	ret_reg &ref[41]
	call_c   build_ref_0()
	call_c   build_ref_34()
	call_c   build_ref_40()
	call_c   Dyam_Create_Binary(&ref[0],&ref[34],&ref[40])
	move_ret ref[41]
	c_ret

;; TERM 40: return(_D)
c_code local build_ref_40
	ret_reg &ref[40]
	call_c   build_ref_195()
	call_c   Dyam_Create_Unary(&ref[195],V(3))
	move_ret ref[40]
	c_ret

pl_code local fun16
	call_c   build_ref_41()
	call_c   Dyam_Unify_Item(&ref[41])
	fail_ret
	pl_jump  Follow_Cont(V(1))

;; TERM 43: '*RITEM*'('call_normalize/2'(_C), return(_D)) :> tag_features(0, '$TUPPLE'(35063484913968))
c_code local build_ref_43
	ret_reg &ref[43]
	call_c   build_ref_41()
	call_c   build_ref_42()
	call_c   Dyam_Create_Binary(I(9),&ref[41],&ref[42])
	move_ret ref[43]
	c_ret

;; TERM 42: tag_features(0, '$TUPPLE'(35063484913968))
c_code local build_ref_42
	ret_reg &ref[42]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,134217728)
	move_ret R(0)
	call_c   build_ref_179()
	call_c   Dyam_Create_Binary(&ref[179],N(0),R(0))
	move_ret ref[42]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 4: tag_features_skel(_A, _B, _C, _D, _E, _F, _G)
c_code local build_ref_4
	ret_reg &ref[4]
	call_c   build_ref_197()
	call_c   Dyam_Term_Start(&ref[197],7)
	call_c   Dyam_Term_Arg(V(0))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[4]
	c_ret

;; TERM 3: '*RITEM*'('call_tag_features_skel/7'(_A, _B), return(_C, _D, _E, _F, _G))
c_code local build_ref_3
	ret_reg &ref[3]
	call_c   build_ref_0()
	call_c   build_ref_1()
	call_c   build_ref_2()
	call_c   Dyam_Create_Binary(&ref[0],&ref[1],&ref[2])
	move_ret ref[3]
	c_ret

;; TERM 2: return(_C, _D, _E, _F, _G)
c_code local build_ref_2
	ret_reg &ref[2]
	call_c   build_ref_195()
	call_c   Dyam_Term_Start(&ref[195],5)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[2]
	c_ret

;; TERM 1: 'call_tag_features_skel/7'(_A, _B)
c_code local build_ref_1
	ret_reg &ref[1]
	call_c   build_ref_201()
	call_c   Dyam_Create_Binary(&ref[201],V(0),V(1))
	move_ret ref[1]
	c_ret

;; TERM 8: normalize(_A, _B)
c_code local build_ref_8
	ret_reg &ref[8]
	call_c   build_ref_222()
	call_c   Dyam_Create_Binary(&ref[222],V(0),V(1))
	move_ret ref[8]
	c_ret

;; TERM 222: normalize
c_code local build_ref_222
	ret_reg &ref[222]
	call_c   Dyam_Create_Atom("normalize")
	move_ret ref[222]
	c_ret

;; TERM 7: '*RITEM*'('call_normalize/2'(_A), return(_B))
c_code local build_ref_7
	ret_reg &ref[7]
	call_c   build_ref_0()
	call_c   build_ref_5()
	call_c   build_ref_6()
	call_c   Dyam_Create_Binary(&ref[0],&ref[5],&ref[6])
	move_ret ref[7]
	c_ret

;; TERM 6: return(_B)
c_code local build_ref_6
	ret_reg &ref[6]
	call_c   build_ref_195()
	call_c   Dyam_Create_Unary(&ref[195],V(1))
	move_ret ref[6]
	c_ret

;; TERM 5: 'call_normalize/2'(_A)
c_code local build_ref_5
	ret_reg &ref[5]
	call_c   build_ref_196()
	call_c   Dyam_Create_Unary(&ref[196],V(0))
	move_ret ref[5]
	c_ret

;; TERM 150: yes
c_code local build_ref_150
	ret_reg &ref[150]
	call_c   Dyam_Create_Atom("yes")
	move_ret ref[150]
	c_ret

;; TERM 152: fail
c_code local build_ref_152
	ret_reg &ref[152]
	call_c   Dyam_Create_Atom("fail")
	move_ret ref[152]
	c_ret

;; TERM 149: guard{goal=> tag_node{id=> _F, label=> _G, children=> _H, kind=> _I, adj=> _J, spine=> _K, top=> _L, bot=> _M, token=> _N, adjleft=> _O, adjright=> _P, adjwrap=> _Q}, plus=> _R, minus=> _S}
c_code local build_ref_149
	ret_reg &ref[149]
	call_c   build_ref_202()
	call_c   build_ref_151()
	call_c   Dyam_Term_Start(&ref[202],3)
	call_c   Dyam_Term_Arg(&ref[151])
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_End()
	move_ret ref[149]
	c_ret

;; TERM 151: tag_node{id=> _F, label=> _G, children=> _H, kind=> _I, adj=> _J, spine=> _K, top=> _L, bot=> _M, token=> _N, adjleft=> _O, adjright=> _P, adjwrap=> _Q}
c_code local build_ref_151
	ret_reg &ref[151]
	call_c   build_ref_203()
	call_c   Dyam_Term_Start(&ref[203],12)
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_End()
	move_ret ref[151]
	c_ret

;; TERM 153: guard{goal=> tag_node{id=> _F, label=> _G, children=> _H, kind=> _I, adj=> _J, spine=> _K, top=> _L, bot=> _M, token=> _N, adjleft=> _O, adjright=> _P, adjwrap=> _Q}, plus=> _T, minus=> fail}
c_code local build_ref_153
	ret_reg &ref[153]
	call_c   build_ref_202()
	call_c   build_ref_151()
	call_c   build_ref_152()
	call_c   Dyam_Term_Start(&ref[202],3)
	call_c   Dyam_Term_Arg(&ref[151])
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_Arg(&ref[152])
	call_c   Dyam_Term_End()
	move_ret ref[153]
	c_ret

;; TERM 154: [_U|_V]
c_code local build_ref_154
	ret_reg &ref[154]
	call_c   Dyam_Create_List(V(20),V(21))
	move_ret ref[154]
	c_ret

;; TERM 155: _U , _V
c_code local build_ref_155
	ret_reg &ref[155]
	call_c   Dyam_Create_Binary(I(4),V(20),V(21))
	move_ret ref[155]
	c_ret

;; TERM 156: _U ## _V
c_code local build_ref_156
	ret_reg &ref[156]
	call_c   build_ref_223()
	call_c   Dyam_Create_Binary(&ref[223],V(20),V(21))
	move_ret ref[156]
	c_ret

;; TERM 223: ##
c_code local build_ref_223
	ret_reg &ref[223]
	call_c   Dyam_Create_Atom("##")
	move_ret ref[223]
	c_ret

;; TERM 158: [scan,escape]
c_code local build_ref_158
	ret_reg &ref[158]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_225()
	call_c   Dyam_Create_List(&ref[225],I(0))
	move_ret R(0)
	call_c   build_ref_224()
	call_c   Dyam_Create_List(&ref[224],R(0))
	move_ret ref[158]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 224: scan
c_code local build_ref_224
	ret_reg &ref[224]
	call_c   Dyam_Create_Atom("scan")
	move_ret ref[224]
	c_ret

;; TERM 225: escape
c_code local build_ref_225
	ret_reg &ref[225]
	call_c   Dyam_Create_Atom("escape")
	move_ret ref[225]
	c_ret

;; TERM 159: foot
c_code local build_ref_159
	ret_reg &ref[159]
	call_c   Dyam_Create_Atom("foot")
	move_ret ref[159]
	c_ret

;; TERM 160: no
c_code local build_ref_160
	ret_reg &ref[160]
	call_c   Dyam_Create_Atom("no")
	move_ret ref[160]
	c_ret

;; TERM 161: tag_features_tmp(_Y, _J1, _K1)
c_code local build_ref_161
	ret_reg &ref[161]
	call_c   build_ref_194()
	call_c   Dyam_Term_Start(&ref[194],3)
	call_c   Dyam_Term_Arg(V(24))
	call_c   Dyam_Term_Arg(V(35))
	call_c   Dyam_Term_Arg(V(36))
	call_c   Dyam_Term_End()
	move_ret ref[161]
	c_ret

;; TERM 164: tag_features_mode_tmp(_Y, left, _P1, _Q1)
c_code local build_ref_164
	ret_reg &ref[164]
	call_c   build_ref_193()
	call_c   build_ref_226()
	call_c   Dyam_Term_Start(&ref[193],4)
	call_c   Dyam_Term_Arg(V(24))
	call_c   Dyam_Term_Arg(&ref[226])
	call_c   Dyam_Term_Arg(V(41))
	call_c   Dyam_Term_Arg(V(42))
	call_c   Dyam_Term_End()
	move_ret ref[164]
	c_ret

;; TERM 226: left
c_code local build_ref_226
	ret_reg &ref[226]
	call_c   Dyam_Create_Atom("left")
	move_ret ref[226]
	c_ret

;; TERM 165: [[left,_Y,_D1,_R1]|_C]
c_code local build_ref_165
	ret_reg &ref[165]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(43),I(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(29),R(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(24),R(0))
	move_ret R(0)
	call_c   build_ref_226()
	call_c   Dyam_Create_List(&ref[226],R(0))
	move_ret R(0)
	call_c   Dyam_Create_List(R(0),V(2))
	move_ret ref[165]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 176: tag_features_mode_tmp(_Y, left, _N1, _N1)
c_code local build_ref_176
	ret_reg &ref[176]
	call_c   build_ref_193()
	call_c   build_ref_226()
	call_c   Dyam_Term_Start(&ref[193],4)
	call_c   Dyam_Term_Arg(V(24))
	call_c   Dyam_Term_Arg(&ref[226])
	call_c   Dyam_Term_Arg(V(39))
	call_c   Dyam_Term_Arg(V(39))
	call_c   Dyam_Term_End()
	move_ret ref[176]
	c_ret

;; TERM 163: '$VAR'(_O1, ['$SET$',adj(no, yes, strict, atmostone, one, sync),42])
c_code local build_ref_163
	ret_reg &ref[163]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_209()
	call_c   build_ref_160()
	call_c   build_ref_150()
	call_c   build_ref_227()
	call_c   build_ref_228()
	call_c   build_ref_229()
	call_c   build_ref_230()
	call_c   Dyam_Term_Start(&ref[209],6)
	call_c   Dyam_Term_Arg(&ref[160])
	call_c   Dyam_Term_Arg(&ref[150])
	call_c   Dyam_Term_Arg(&ref[227])
	call_c   Dyam_Term_Arg(&ref[228])
	call_c   Dyam_Term_Arg(&ref[229])
	call_c   Dyam_Term_Arg(&ref[230])
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   Dyam_Term_Start(I(8),3)
	call_c   Dyam_Term_Arg(V(40))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(N(42))
	call_c   Dyam_Term_End()
	move_ret ref[163]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 230: sync
c_code local build_ref_230
	ret_reg &ref[230]
	call_c   Dyam_Create_Atom("sync")
	move_ret ref[230]
	c_ret

;; TERM 229: one
c_code local build_ref_229
	ret_reg &ref[229]
	call_c   Dyam_Create_Atom("one")
	move_ret ref[229]
	c_ret

;; TERM 228: atmostone
c_code local build_ref_228
	ret_reg &ref[228]
	call_c   Dyam_Create_Atom("atmostone")
	move_ret ref[228]
	c_ret

;; TERM 227: strict
c_code local build_ref_227
	ret_reg &ref[227]
	call_c   Dyam_Create_Atom("strict")
	move_ret ref[227]
	c_ret

;; TERM 166: adjkind(_Y, wrap)
c_code local build_ref_166
	ret_reg &ref[166]
	call_c   build_ref_231()
	call_c   build_ref_100()
	call_c   Dyam_Create_Binary(&ref[231],V(24),&ref[100])
	move_ret ref[166]
	c_ret

;; TERM 231: adjkind
c_code local build_ref_231
	ret_reg &ref[231]
	call_c   Dyam_Create_Atom("adjkind")
	move_ret ref[231]
	c_ret

;; TERM 162: adjkind(_Y, left)
c_code local build_ref_162
	ret_reg &ref[162]
	call_c   build_ref_231()
	call_c   build_ref_226()
	call_c   Dyam_Create_Binary(&ref[231],V(24),&ref[226])
	move_ret ref[162]
	c_ret

;; TERM 167: tag_features_mode_tmp(_Y, wrap, _T1, _U1)
c_code local build_ref_167
	ret_reg &ref[167]
	call_c   build_ref_193()
	call_c   build_ref_100()
	call_c   Dyam_Term_Start(&ref[193],4)
	call_c   Dyam_Term_Arg(V(24))
	call_c   Dyam_Term_Arg(&ref[100])
	call_c   Dyam_Term_Arg(V(45))
	call_c   Dyam_Term_Arg(V(46))
	call_c   Dyam_Term_End()
	move_ret ref[167]
	c_ret

;; TERM 168: [[wrap,_Y,_R1,_V1]|_S1]
c_code local build_ref_168
	ret_reg &ref[168]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(47),I(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(43),R(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(24),R(0))
	move_ret R(0)
	call_c   build_ref_100()
	call_c   Dyam_Create_List(&ref[100],R(0))
	move_ret R(0)
	call_c   Dyam_Create_List(R(0),V(44))
	move_ret ref[168]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 175: tag_features_mode_tmp(_Y, wrap, _N1, _N1)
c_code local build_ref_175
	ret_reg &ref[175]
	call_c   build_ref_193()
	call_c   build_ref_100()
	call_c   Dyam_Term_Start(&ref[193],4)
	call_c   Dyam_Term_Arg(V(24))
	call_c   Dyam_Term_Arg(&ref[100])
	call_c   Dyam_Term_Arg(V(39))
	call_c   Dyam_Term_Arg(V(39))
	call_c   Dyam_Term_End()
	move_ret ref[175]
	c_ret

;; TERM 171: tag_features_mode_tmp(_Y, right, _Y1, _Z1)
c_code local build_ref_171
	ret_reg &ref[171]
	call_c   build_ref_193()
	call_c   build_ref_232()
	call_c   Dyam_Term_Start(&ref[193],4)
	call_c   Dyam_Term_Arg(V(24))
	call_c   Dyam_Term_Arg(&ref[232])
	call_c   Dyam_Term_Arg(V(50))
	call_c   Dyam_Term_Arg(V(51))
	call_c   Dyam_Term_End()
	move_ret ref[171]
	c_ret

;; TERM 232: right
c_code local build_ref_232
	ret_reg &ref[232]
	call_c   Dyam_Create_Atom("right")
	move_ret ref[232]
	c_ret

;; TERM 172: [[right,_Y,_V1,_E1]|_W1]
c_code local build_ref_172
	ret_reg &ref[172]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(30),I(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(47),R(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(24),R(0))
	move_ret R(0)
	call_c   build_ref_232()
	call_c   Dyam_Create_List(&ref[232],R(0))
	move_ret R(0)
	call_c   Dyam_Create_List(R(0),V(48))
	move_ret ref[172]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 174: tag_features_mode_tmp(_Y, right, _N1, _N1)
c_code local build_ref_174
	ret_reg &ref[174]
	call_c   build_ref_193()
	call_c   build_ref_232()
	call_c   Dyam_Term_Start(&ref[193],4)
	call_c   Dyam_Term_Arg(V(24))
	call_c   Dyam_Term_Arg(&ref[232])
	call_c   Dyam_Term_Arg(V(39))
	call_c   Dyam_Term_Arg(V(39))
	call_c   Dyam_Term_End()
	move_ret ref[174]
	c_ret

;; TERM 170: '$VAR'(_X1, ['$SET$',adj(no, yes, strict, atmostone, one, sync),42])
c_code local build_ref_170
	ret_reg &ref[170]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_209()
	call_c   build_ref_160()
	call_c   build_ref_150()
	call_c   build_ref_227()
	call_c   build_ref_228()
	call_c   build_ref_229()
	call_c   build_ref_230()
	call_c   Dyam_Term_Start(&ref[209],6)
	call_c   Dyam_Term_Arg(&ref[160])
	call_c   Dyam_Term_Arg(&ref[150])
	call_c   Dyam_Term_Arg(&ref[227])
	call_c   Dyam_Term_Arg(&ref[228])
	call_c   Dyam_Term_Arg(&ref[229])
	call_c   Dyam_Term_Arg(&ref[230])
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   Dyam_Term_Start(I(8),3)
	call_c   Dyam_Term_Arg(V(49))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(N(42))
	call_c   Dyam_Term_End()
	move_ret ref[170]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 169: adjkind(_Y, right)
c_code local build_ref_169
	ret_reg &ref[169]
	call_c   build_ref_231()
	call_c   build_ref_232()
	call_c   Dyam_Create_Binary(&ref[231],V(24),&ref[232])
	move_ret ref[169]
	c_ret

;; TERM 173: [[_Y,_D1,_E1]|_A2]
c_code local build_ref_173
	ret_reg &ref[173]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Tupple(29,30,I(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(24),R(0))
	move_ret R(0)
	call_c   Dyam_Create_List(R(0),V(52))
	move_ret ref[173]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 177: tag_features_tmp(_Y, _N1, _N1)
c_code local build_ref_177
	ret_reg &ref[177]
	call_c   build_ref_194()
	call_c   Dyam_Term_Start(&ref[194],3)
	call_c   Dyam_Term_Arg(V(24))
	call_c   Dyam_Term_Arg(V(39))
	call_c   Dyam_Term_Arg(V(39))
	call_c   Dyam_Term_End()
	move_ret ref[177]
	c_ret

;; TERM 157: tag_node{id=> _X, label=> _Y, children=> _Z, kind=> _A1, adj=> _B1, spine=> _C1, top=> _D1, bot=> _E1, token=> _F1, adjleft=> _G1, adjright=> _H1, adjwrap=> _I1}
c_code local build_ref_157
	ret_reg &ref[157]
	call_c   build_ref_203()
	call_c   Dyam_Term_Start(&ref[203],12)
	call_c   Dyam_Term_Arg(V(23))
	call_c   Dyam_Term_Arg(V(24))
	call_c   Dyam_Term_Arg(V(25))
	call_c   Dyam_Term_Arg(V(26))
	call_c   Dyam_Term_Arg(V(27))
	call_c   Dyam_Term_Arg(V(28))
	call_c   Dyam_Term_Arg(V(29))
	call_c   Dyam_Term_Arg(V(30))
	call_c   Dyam_Term_Arg(V(31))
	call_c   Dyam_Term_Arg(V(32))
	call_c   Dyam_Term_Arg(V(33))
	call_c   Dyam_Term_Arg(V(34))
	call_c   Dyam_Term_End()
	move_ret ref[157]
	c_ret

;; TERM 146: tag_features_tmp(_C, _D, _E)
c_code local build_ref_146
	ret_reg &ref[146]
	call_c   build_ref_194()
	call_c   Dyam_Term_Start(&ref[194],3)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[146]
	c_ret

;; TERM 145: [[_C,_D,_E]|_F]
c_code local build_ref_145
	ret_reg &ref[145]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Tupple(2,4,I(0))
	move_ret R(0)
	call_c   Dyam_Create_List(R(0),V(5))
	move_ret ref[145]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 148: tag_features_mode_tmp(_C, _G, _D, _E)
c_code local build_ref_148
	ret_reg &ref[148]
	call_c   build_ref_193()
	call_c   Dyam_Term_Start(&ref[193],4)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[148]
	c_ret

;; TERM 147: [[_G,_C,_D,_E]|_F]
c_code local build_ref_147
	ret_reg &ref[147]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Tupple(2,4,I(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(6),R(0))
	move_ret R(0)
	call_c   Dyam_Create_List(R(0),V(5))
	move_ret ref[147]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 140: -
c_code local build_ref_140
	ret_reg &ref[140]
	call_c   Dyam_Create_Atom("-")
	move_ret ref[140]
	c_ret

;; TERM 141: '%% File ~q\n'
c_code local build_ref_141
	ret_reg &ref[141]
	call_c   Dyam_Create_Atom("%% File ~q\n")
	move_ret ref[141]
	c_ret

;; TERM 139: main_file(_L, _M)
c_code local build_ref_139
	ret_reg &ref[139]
	call_c   build_ref_233()
	call_c   Dyam_Create_Binary(&ref[233],V(11),V(12))
	move_ret ref[139]
	c_ret

;; TERM 233: main_file
c_code local build_ref_233
	ret_reg &ref[233]
	call_c   Dyam_Create_Atom("main_file")
	move_ret ref[233]
	c_ret

;; TERM 144: ';; File stdin\n'
c_code local build_ref_144
	ret_reg &ref[144]
	call_c   Dyam_Create_Atom(";; File stdin\n")
	move_ret ref[144]
	c_ret

;; TERM 143: main_file(-, _O)
c_code local build_ref_143
	ret_reg &ref[143]
	call_c   build_ref_233()
	call_c   build_ref_140()
	call_c   Dyam_Create_Binary(&ref[233],&ref[140],V(14))
	move_ret ref[143]
	c_ret

;; TERM 138: [_C,_D,_B,_E,_F,_G]
c_code local build_ref_138
	ret_reg &ref[138]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Tupple(4,6,I(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(1),R(0))
	move_ret R(0)
	call_c   Dyam_Create_Tupple(2,3,R(0))
	move_ret ref[138]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 137: '%% Date    : ~w ~w ~w at ~w:~w:~w\n'
c_code local build_ref_137
	ret_reg &ref[137]
	call_c   Dyam_Create_Atom("%% Date    : ~w ~w ~w at ~w:~w:~w\n")
	move_ret ref[137]
	c_ret

;; TERM 136: [_H,_I]
c_code local build_ref_136
	ret_reg &ref[136]
	call_c   Dyam_Create_Tupple(7,8,I(0))
	move_ret ref[136]
	c_ret

;; TERM 135: '%% Compiler Analyzer tag_features: ~w ~w\n'
c_code local build_ref_135
	ret_reg &ref[135]
	call_c   Dyam_Create_Atom("%% Compiler Analyzer tag_features: ~w ~w\n")
	move_ret ref[135]
	c_ret

;; TERM 134: compiler_info{name=> _H, version=> _I, author=> _J, email=> _K}
c_code local build_ref_134
	ret_reg &ref[134]
	call_c   build_ref_234()
	call_c   Dyam_Term_Start(&ref[234],4)
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_End()
	move_ret ref[134]
	c_ret

;; TERM 234: compiler_info!'$ft'
c_code local build_ref_234
	ret_reg &ref[234]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_238()
	call_c   Dyam_Create_List(&ref[238],I(0))
	move_ret R(0)
	call_c   build_ref_237()
	call_c   Dyam_Create_List(&ref[237],R(0))
	move_ret R(0)
	call_c   build_ref_236()
	call_c   Dyam_Create_List(&ref[236],R(0))
	move_ret R(0)
	call_c   build_ref_189()
	call_c   Dyam_Create_List(&ref[189],R(0))
	move_ret R(0)
	call_c   build_ref_235()
	call_c   Dyam_Create_Atom_Module("$ft",&ref[235])
	move_ret ref[234]
	call_c   DYAM_Feature_2(&ref[235],R(0))
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 235: compiler_info
c_code local build_ref_235
	ret_reg &ref[235]
	call_c   Dyam_Create_Atom("compiler_info")
	move_ret ref[235]
	c_ret

;; TERM 236: version
c_code local build_ref_236
	ret_reg &ref[236]
	call_c   Dyam_Create_Atom("version")
	move_ret ref[236]
	c_ret

;; TERM 237: author
c_code local build_ref_237
	ret_reg &ref[237]
	call_c   Dyam_Create_Atom("author")
	move_ret ref[237]
	c_ret

;; TERM 238: email
c_code local build_ref_238
	ret_reg &ref[238]
	call_c   Dyam_Create_Atom("email")
	move_ret ref[238]
	c_ret

;; TERM 127: [_H|_N]
c_code local build_ref_127
	ret_reg &ref[127]
	call_c   Dyam_Create_List(V(7),V(13))
	move_ret ref[127]
	c_ret

;; TERM 126: [_H|_M]
c_code local build_ref_126
	ret_reg &ref[126]
	call_c   Dyam_Create_List(V(7),V(12))
	move_ret ref[126]
	c_ret

;; TERM 122: [_H|_L]
c_code local build_ref_122
	ret_reg &ref[122]
	call_c   Dyam_Create_List(V(7),V(11))
	move_ret ref[122]
	c_ret

;; TERM 121: [_H|_K]
c_code local build_ref_121
	ret_reg &ref[121]
	call_c   Dyam_Create_List(V(7),V(10))
	move_ret ref[121]
	c_ret

;; TERM 120: [_H|_J]
c_code local build_ref_120
	ret_reg &ref[120]
	call_c   Dyam_Create_List(V(7),V(9))
	move_ret ref[120]
	c_ret

;; TERM 111: tag_features_tmp(_E, _J, _K)
c_code local build_ref_111
	ret_reg &ref[111]
	call_c   build_ref_194()
	call_c   Dyam_Term_Start(&ref[194],3)
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_End()
	move_ret ref[111]
	c_ret

;; TERM 110: tag_features_tmp(_E, _P, _Q)
c_code local build_ref_110
	ret_reg &ref[110]
	call_c   build_ref_194()
	call_c   Dyam_Term_Start(&ref[194],3)
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_End()
	move_ret ref[110]
	c_ret

;; TERM 109: tag_features_tmp(_E, _N, _O)
c_code local build_ref_109
	ret_reg &ref[109]
	call_c   build_ref_194()
	call_c   Dyam_Term_Start(&ref[194],3)
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_End()
	move_ret ref[109]
	c_ret

;; TERM 108: tag_features_current(_E, _L, _M)
c_code local build_ref_108
	ret_reg &ref[108]
	call_c   build_ref_239()
	call_c   Dyam_Term_Start(&ref[239],3)
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_End()
	move_ret ref[108]
	c_ret

;; TERM 239: tag_features_current
c_code local build_ref_239
	ret_reg &ref[239]
	call_c   Dyam_Create_Atom("tag_features_current")
	move_ret ref[239]
	c_ret

;; TERM 107: tag_features_current(_E, _J, _K)
c_code local build_ref_107
	ret_reg &ref[107]
	call_c   build_ref_239()
	call_c   Dyam_Term_Start(&ref[239],3)
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_End()
	move_ret ref[107]
	c_ret

;; TERM 106: tag_nt(_E, _I)
c_code local build_ref_106
	ret_reg &ref[106]
	call_c   build_ref_198()
	call_c   Dyam_Create_Binary(&ref[198],V(4),V(8))
	move_ret ref[106]
	c_ret

;; TERM 118: tag_features_mode_tmp(_E, _H, _J, _K)
c_code local build_ref_118
	ret_reg &ref[118]
	call_c   build_ref_193()
	call_c   Dyam_Term_Start(&ref[193],4)
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_End()
	move_ret ref[118]
	c_ret

;; TERM 117: tag_features_mode_tmp(_E, _H, _U, _V)
c_code local build_ref_117
	ret_reg &ref[117]
	call_c   build_ref_193()
	call_c   Dyam_Term_Start(&ref[193],4)
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_Arg(V(21))
	call_c   Dyam_Term_End()
	move_ret ref[117]
	c_ret

;; TERM 116: tag_features_mode_tmp(_E, _H, _N, _O)
c_code local build_ref_116
	ret_reg &ref[116]
	call_c   build_ref_193()
	call_c   Dyam_Term_Start(&ref[193],4)
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_End()
	move_ret ref[116]
	c_ret

;; TERM 115: tag_features_mode_current(_E, _H, _S, _T)
c_code local build_ref_115
	ret_reg &ref[115]
	call_c   build_ref_240()
	call_c   Dyam_Term_Start(&ref[240],4)
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_End()
	move_ret ref[115]
	c_ret

;; TERM 240: tag_features_mode_current
c_code local build_ref_240
	ret_reg &ref[240]
	call_c   Dyam_Create_Atom("tag_features_mode_current")
	move_ret ref[240]
	c_ret

;; TERM 114: tag_features_mode_current(_E, _H, _J, _K)
c_code local build_ref_114
	ret_reg &ref[114]
	call_c   build_ref_240()
	call_c   Dyam_Term_Start(&ref[240],4)
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_End()
	move_ret ref[114]
	c_ret

;; TERM 113: tag_nt(_E, _R)
c_code local build_ref_113
	ret_reg &ref[113]
	call_c   build_ref_198()
	call_c   Dyam_Create_Binary(&ref[198],V(4),V(17))
	move_ret ref[113]
	c_ret

;; TERM 112: tag_features_new_round
c_code local build_ref_112
	ret_reg &ref[112]
	call_c   Dyam_Create_Atom("tag_features_new_round")
	move_ret ref[112]
	c_ret

;; TERM 102: '%% New round\n'
c_code local build_ref_102
	ret_reg &ref[102]
	call_c   Dyam_Create_Atom("%% New round\n")
	move_ret ref[102]
	c_ret

;; TERM 67: tag_features_skel(_C, _D, _E, _F, _G, _H, _I)
c_code local build_ref_67
	ret_reg &ref[67]
	call_c   build_ref_197()
	call_c   Dyam_Term_Start(&ref[197],7)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_End()
	move_ret ref[67]
	c_ret

;; TERM 23: tag_features_mode_current(_B, _C, _F, _G)
c_code local build_ref_23
	ret_reg &ref[23]
	call_c   build_ref_240()
	call_c   Dyam_Term_Start(&ref[240],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[23]
	c_ret

;; TERM 25: tag_features_mode_current(_B, _C, _H, _I)
c_code local build_ref_25
	ret_reg &ref[25]
	call_c   build_ref_240()
	call_c   Dyam_Term_Start(&ref[240],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_End()
	move_ret ref[25]
	c_ret

;; TERM 24: tag_features_mode_current(_B, _C, _J, _K)
c_code local build_ref_24
	ret_reg &ref[24]
	call_c   build_ref_240()
	call_c   Dyam_Term_Start(&ref[240],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_End()
	move_ret ref[24]
	c_ret

;; TERM 20: tag_features_current(_B, _E, _F)
c_code local build_ref_20
	ret_reg &ref[20]
	call_c   build_ref_239()
	call_c   Dyam_Term_Start(&ref[239],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[20]
	c_ret

;; TERM 22: tag_features_current(_B, _G, _H)
c_code local build_ref_22
	ret_reg &ref[22]
	call_c   build_ref_239()
	call_c   Dyam_Term_Start(&ref[239],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[22]
	c_ret

;; TERM 21: tag_features_current(_B, _I, _J)
c_code local build_ref_21
	ret_reg &ref[21]
	call_c   build_ref_239()
	call_c   Dyam_Term_Start(&ref[239],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret ref[21]
	c_ret

;; TERM 18: [_D,_E]
c_code local build_ref_18
	ret_reg &ref[18]
	call_c   Dyam_Create_Tupple(3,4,I(0))
	move_ret ref[18]
	c_ret

pl_code local fun9
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Object(R(0))
	call_c   Dyam_Tabule()
	pl_ret

pl_code local fun83
	call_c   Dyam_Remove_Choice()
	call_c   DYAM_sfol_identical(V(18),&ref[152])
	fail_ret
	pl_jump  fun82()

pl_code local fun100
	call_c   Dyam_Remove_Choice()
	call_c   DYAM_evpred_functor(V(29),V(37),V(38))
	fail_ret
	call_c   DYAM_evpred_functor(V(39),V(37),V(38))
	fail_ret
	call_c   DYAM_evpred_assert_1(&ref[176])
fun99:
	call_c   DYAM_evpred_functor(V(30),V(37),V(38))
	fail_ret
	call_c   DYAM_evpred_functor(V(43),V(37),V(38))
	fail_ret
	call_c   Dyam_Unify(V(44),&ref[165])
	fail_ret
fun98:
	call_c   Dyam_Choice(fun97)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Choice(fun96)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[166])
	call_c   Dyam_Cut()
fun92:
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun91)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[167])
	call_c   Dyam_Cut()
fun90:
	call_c   DYAM_evpred_functor(V(30),V(37),V(38))
	fail_ret
	call_c   DYAM_evpred_functor(V(47),V(37),V(38))
	fail_ret
	call_c   Dyam_Unify(V(48),&ref[168])
	fail_ret
fun89:
	call_c   Dyam_Choice(fun88)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[169])
	call_c   Dyam_Unify(V(33),&ref[170])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun87)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[171])
	call_c   Dyam_Cut()
fun86:
	call_c   Dyam_Unify(V(52),&ref[172])
	fail_ret
fun85:
	call_c   Dyam_Reg_Load(0,V(25))
	move     &ref[173], R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Load(4,V(3))
	call_c   Dyam_Reg_Load(6,V(4))
	call_c   Dyam_Reg_Deallocate(4)
	pl_jump  pred_tag_features_skel_node_4()








pl_code local fun91
	call_c   Dyam_Remove_Choice()
	call_c   DYAM_evpred_functor(V(29),V(37),V(38))
	fail_ret
	call_c   DYAM_evpred_functor(V(39),V(37),V(38))
	fail_ret
	call_c   DYAM_evpred_assert_1(&ref[175])
	pl_jump  fun90()

pl_code local fun93
	call_c   Dyam_Remove_Choice()
	pl_jump  fun92()

pl_code local fun95
	call_c   Dyam_Remove_Choice()
fun94:
	call_c   Dyam_Choice(fun93)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[169])
	call_c   Dyam_Cut()
	pl_fail


pl_code local fun96
	call_c   Dyam_Update_Choice(fun95)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[162])
	call_c   Dyam_Cut()
	pl_fail

pl_code local fun87
	call_c   Dyam_Remove_Choice()
	call_c   DYAM_evpred_functor(V(29),V(37),V(38))
	fail_ret
	call_c   DYAM_evpred_functor(V(39),V(37),V(38))
	fail_ret
	call_c   DYAM_evpred_assert_1(&ref[174])
	pl_jump  fun86()

pl_code local fun88
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(47),V(30))
	fail_ret
	call_c   Dyam_Unify(V(52),V(48))
	fail_ret
	pl_jump  fun85()

pl_code local fun97
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(48),V(44))
	fail_ret
	call_c   Dyam_Unify(V(43),V(47))
	fail_ret
	pl_jump  fun89()

pl_code local fun101
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(44),V(2))
	fail_ret
	call_c   Dyam_Unify(V(43),V(29))
	fail_ret
	pl_jump  fun98()

pl_code local fun103
	call_c   Dyam_Remove_Choice()
	call_c   DYAM_evpred_functor(V(29),V(37),V(38))
	fail_ret
	call_c   DYAM_evpred_functor(V(39),V(37),V(38))
	fail_ret
	call_c   DYAM_evpred_assert_1(&ref[177])
fun102:
	call_c   Dyam_Choice(fun101)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[162])
	call_c   Dyam_Unify(V(32),&ref[163])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun100)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[164])
	call_c   Dyam_Cut()
	pl_jump  fun99()


pl_code local fun104
	call_c   Dyam_Update_Choice(fun103)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[161])
	call_c   Dyam_Cut()
	pl_jump  fun102()

pl_code local fun106
	call_c   Dyam_Remove_Choice()
fun105:
	call_c   Dyam_Choice(fun104)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(27),&ref[160])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(25))
	call_c   Dyam_Reg_Load(2,V(2))
	call_c   Dyam_Reg_Load(4,V(3))
	call_c   Dyam_Reg_Load(6,V(4))
	call_c   Dyam_Reg_Deallocate(4)
	pl_jump  pred_tag_features_skel_node_4()


pl_code local fun108
	call_c   Dyam_Remove_Choice()
fun107:
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun106)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(26),&ref[159])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(4),V(30))
	fail_ret
	pl_jump  fun105()


pl_code local fun84
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(2),V(3))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun109
	call_c   Dyam_Update_Choice(fun84)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[157])
	fail_ret
	call_c   Dyam_Choice(fun108)
	call_c   Dyam_Set_Cut()
	pl_call  Domain_2(V(26),&ref[158])
	call_c   Dyam_Cut()
	pl_fail

pl_code local fun110
	call_c   Dyam_Update_Choice(fun109)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[156])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(20))
	call_c   Dyam_Reg_Load(2,V(2))
	move     V(22), R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Load(6,V(4))
	pl_call  pred_tag_features_skel_node_4()
	call_c   Dyam_Reg_Load(0,V(21))
	call_c   Dyam_Reg_Load(2,V(22))
	call_c   Dyam_Reg_Load(4,V(3))
	call_c   Dyam_Reg_Load(6,V(4))
	call_c   Dyam_Reg_Deallocate(4)
	pl_jump  pred_tag_features_skel_node_4()

pl_code local fun111
	call_c   Dyam_Update_Choice(fun110)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[155])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(20))
	call_c   Dyam_Reg_Load(2,V(2))
	move     V(22), R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Load(6,V(4))
	pl_call  pred_tag_features_skel_node_4()
	call_c   Dyam_Reg_Load(0,V(21))
	call_c   Dyam_Reg_Load(2,V(22))
	call_c   Dyam_Reg_Load(4,V(3))
	call_c   Dyam_Reg_Load(6,V(4))
	call_c   Dyam_Reg_Deallocate(4)
	pl_jump  pred_tag_features_skel_node_4()

pl_code local fun112
	call_c   Dyam_Update_Choice(fun111)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[154])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(20))
	call_c   Dyam_Reg_Load(2,V(2))
	move     V(22), R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Load(6,V(4))
	pl_call  pred_tag_features_skel_node_4()
	call_c   Dyam_Reg_Load(0,V(21))
	call_c   Dyam_Reg_Load(2,V(22))
	call_c   Dyam_Reg_Load(4,V(3))
	call_c   Dyam_Reg_Load(6,V(4))
	call_c   Dyam_Reg_Deallocate(4)
	pl_jump  pred_tag_features_skel_node_4()

pl_code local fun113
	call_c   Dyam_Update_Choice(fun112)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),I(0))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(2),V(3))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun114
	call_c   Dyam_Update_Choice(fun113)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[153])
	fail_ret
	call_c   Dyam_Cut()
	move     &ref[151], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(2))
	call_c   Dyam_Reg_Load(4,V(3))
	call_c   Dyam_Reg_Load(6,V(4))
	call_c   Dyam_Reg_Deallocate(4)
	pl_jump  pred_tag_features_skel_node_4()

pl_code local fun78
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Reg_Load(0,V(2))
	call_c   Dyam_Reg_Load(2,V(3))
	call_c   Dyam_Reg_Load(4,V(4))
	pl_call  pred_tag_features_aggregate_3()
	pl_jump  fun77()

pl_code local fun79
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Reg_Load(0,V(2))
	call_c   Dyam_Reg_Load(2,V(6))
	call_c   Dyam_Reg_Load(4,V(3))
	call_c   Dyam_Reg_Load(6,V(4))
	pl_call  pred_tag_features_mode_aggregate_4()
	pl_jump  fun77()

pl_code local fun80
	call_c   Dyam_Update_Choice(fun4)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[147])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun79)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[148])
	call_c   Dyam_Cut()
	pl_jump  fun77()

pl_code local fun74
	call_c   Dyam_Remove_Choice()
fun73:
	call_c   DYAM_Absolute_File_Name(V(11),V(13))
	fail_ret
	move     &ref[141], R(0)
	move     0, R(1)
	move     &ref[142], R(2)
	move     S(5), R(3)
	pl_call  pred_format_2()
	pl_fail


pl_code local fun71
	call_c   Dyam_Remove_Choice()
fun70:
	call_c   DYAM_Nl_0()
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret


pl_code local fun75
	call_c   Dyam_Remove_Choice()
fun72:
	call_c   Dyam_Choice(fun71)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[143])
	call_c   Dyam_Cut()
	move     &ref[144], R(0)
	move     0, R(1)
	move     I(0), R(2)
	move     0, R(3)
	pl_call  pred_format_2()
	pl_jump  fun70()


pl_code local fun59
	call_c   Dyam_Remove_Choice()
	pl_fail

pl_code local fun56
	call_c   Dyam_Remove_Choice()
fun55:
	call_c   Dyam_Cut()
	call_c   Dyam_Deallocate()
	pl_ret


pl_code local fun57
	call_c   Dyam_Remove_Choice()
	call_c   DYAM_evpred_retract(&ref[112])
	fail_ret
	call_c   Dyam_Reg_Deallocate(0)
	pl_jump  pred_tag_features_fixpoint_0()

pl_code local fun60
	call_c   Dyam_Remove_Choice()
fun58:
	call_c   Dyam_Choice(fun57)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Choice(fun56)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[112])
	call_c   Dyam_Cut()
	pl_fail


pl_code local fun62
	call_c   Dyam_Remove_Choice()
fun61:
	call_c   Dyam_Choice(fun60)
	pl_call  Object_1(&ref[113])
	pl_call  Object_1(&ref[114])
	call_c   DYAM_evpred_retract(&ref[115])
	fail_ret
	pl_call  Object_1(&ref[116])
	call_c   Dyam_Choice(fun59)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Reg_Load(0,V(9))
	call_c   Dyam_Reg_Load(2,V(10))
	call_c   Dyam_Reg_Load(4,V(13))
	call_c   Dyam_Reg_Load(6,V(14))
	pl_call  pred_tag_features_subsumes_4()
	call_c   Dyam_Cut()
	call_c   DYAM_evpred_retract(&ref[117])
	fail_ret
	call_c   DYAM_evpred_assert_1(&ref[118])
	move     &ref[112], R(0)
	move     0, R(1)
	pl_call  pred_record_without_doublon_1()
	pl_fail


pl_code local fun64
	call_c   Dyam_Remove_Choice()
fun63:
	call_c   Dyam_Choice(fun62)
	pl_call  Object_1(&ref[106])
	pl_call  Object_1(&ref[107])
	call_c   DYAM_evpred_retract(&ref[108])
	fail_ret
	pl_call  Object_1(&ref[109])
	call_c   Dyam_Choice(fun59)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Reg_Load(0,V(9))
	call_c   Dyam_Reg_Load(2,V(10))
	call_c   Dyam_Reg_Load(4,V(13))
	call_c   Dyam_Reg_Load(6,V(14))
	pl_call  pred_tag_features_subsumes_4()
	call_c   Dyam_Cut()
	call_c   DYAM_evpred_retract(&ref[110])
	fail_ret
	call_c   DYAM_evpred_assert_1(&ref[111])
	move     &ref[112], R(0)
	move     0, R(1)
	pl_call  pred_record_without_doublon_1()
	pl_fail


pl_code local fun25
	call_c   Dyam_Remove_Choice()
	pl_call  fun18(&seed[10],2,V(9))
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun14
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Object(R(0))
	call_c   Dyam_Tabule()
	pl_jump  Schedule_Prolog()

pl_code local fun15
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Choice(fun14)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Subsume(R(0))
	fail_ret
	call_c   Dyam_Cut()
	pl_ret

pl_code local fun18
	call_c   Dyam_Backptr_Trace(R(1),R(2))
	call_c   Dyam_Light_Object(R(0))
	pl_jump  Schedule_Prolog()

pl_code local fun19
	call_c   Dyam_Remove_Choice()
	pl_call  fun18(&seed[7],2,V(4))
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun11
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(5),V(6))
	fail_ret
	call_c   Dyam_Unify(V(5),V(3))
	fail_ret
	pl_jump  fun10()

pl_code local fun7
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(4),V(5))
	fail_ret
	call_c   Dyam_Unify(V(4),V(2))
	fail_ret
	pl_jump  fun6()

pl_code local fun4
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Deallocate()
	pl_ret


;;------------------ INIT POOL ------------

c_code local build_init_pool
	call_c   build_seed_3()
	call_c   build_seed_0()
	call_c   build_seed_5()
	call_c   build_seed_4()
	call_c   build_seed_2()
	call_c   build_seed_1()
	call_c   build_seed_13()
	call_c   build_seed_12()
	call_c   build_seed_9()
	call_c   build_seed_10()
	call_c   build_seed_6()
	call_c   build_seed_7()
	call_c   build_ref_4()
	call_c   build_ref_3()
	call_c   build_ref_8()
	call_c   build_ref_7()
	call_c   build_ref_150()
	call_c   build_ref_152()
	call_c   build_ref_149()
	call_c   build_ref_151()
	call_c   build_ref_153()
	call_c   build_ref_154()
	call_c   build_ref_155()
	call_c   build_ref_156()
	call_c   build_ref_158()
	call_c   build_ref_159()
	call_c   build_ref_160()
	call_c   build_ref_161()
	call_c   build_ref_164()
	call_c   build_ref_165()
	call_c   build_ref_176()
	call_c   build_ref_163()
	call_c   build_ref_166()
	call_c   build_ref_162()
	call_c   build_ref_167()
	call_c   build_ref_168()
	call_c   build_ref_175()
	call_c   build_ref_171()
	call_c   build_ref_172()
	call_c   build_ref_174()
	call_c   build_ref_170()
	call_c   build_ref_169()
	call_c   build_ref_173()
	call_c   build_ref_177()
	call_c   build_ref_157()
	call_c   build_ref_146()
	call_c   build_ref_145()
	call_c   build_ref_148()
	call_c   build_ref_147()
	call_c   build_ref_140()
	call_c   build_ref_142()
	call_c   build_ref_141()
	call_c   build_ref_139()
	call_c   build_ref_144()
	call_c   build_ref_143()
	call_c   build_ref_138()
	call_c   build_ref_137()
	call_c   build_ref_136()
	call_c   build_ref_135()
	call_c   build_ref_134()
	call_c   build_ref_127()
	call_c   build_ref_126()
	call_c   build_ref_122()
	call_c   build_ref_121()
	call_c   build_ref_120()
	call_c   build_ref_119()
	call_c   build_ref_111()
	call_c   build_ref_110()
	call_c   build_ref_109()
	call_c   build_ref_108()
	call_c   build_ref_107()
	call_c   build_ref_106()
	call_c   build_ref_118()
	call_c   build_ref_117()
	call_c   build_ref_116()
	call_c   build_ref_115()
	call_c   build_ref_114()
	call_c   build_ref_113()
	call_c   build_ref_112()
	call_c   build_ref_102()
	call_c   build_ref_67()
	call_c   build_ref_23()
	call_c   build_ref_25()
	call_c   build_ref_24()
	call_c   build_ref_20()
	call_c   build_ref_22()
	call_c   build_ref_21()
	call_c   build_ref_18()
	call_c   build_ref_19()
	c_ret

long local ref[241]
long local seed[15]

long local _initialization

c_code global initialization_dyalog_tag_5Ffeatures
	call_c   Dyam_Check_And_Update_Initialization(_initialization)
	fail_c_ret
	call_c   initialization_dyalog_tag_5Fnormalize()
	call_c   initialization_dyalog_reader()
	call_c   initialization_dyalog_format()
	call_c   build_init_pool()
	call_c   build_viewers()
	call_c   load()
	c_ret

