;; Compiler: DyALog 1.14.0
;; File "format.pl"


;;------------------ LOADING ------------

c_code local load
	call_c   Dyam_Loading_Set()
	pl_call  fun20(&seed[19],0)
	pl_call  fun20(&seed[13],0)
	pl_call  fun20(&seed[15],0)
	pl_call  fun20(&seed[7],0)
	pl_call  fun20(&seed[5],0)
	pl_call  fun20(&seed[0],0)
	pl_call  fun20(&seed[14],0)
	pl_call  fun20(&seed[4],0)
	pl_call  fun20(&seed[17],0)
	pl_call  fun20(&seed[16],0)
	pl_call  fun20(&seed[11],0)
	pl_call  fun20(&seed[10],0)
	pl_call  fun20(&seed[9],0)
	pl_call  fun20(&seed[1],0)
	pl_call  fun20(&seed[6],0)
	pl_call  fun20(&seed[12],0)
	pl_call  fun20(&seed[18],0)
	pl_call  fun20(&seed[3],0)
	pl_call  fun20(&seed[2],0)
	pl_call  fun20(&seed[8],0)
	pl_call  fun15(&seed[26],0)
	pl_call  fun15(&seed[25],0)
	pl_call  fun15(&seed[24],0)
	pl_call  fun15(&seed[23],0)
	pl_call  fun15(&seed[22],0)
	pl_call  fun15(&seed[21],0)
	pl_call  fun15(&seed[20],0)
	call_c   Dyam_Loading_Reset()
	c_ret

pl_code global pred_format_3
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(3)
	call_c   Dyam_Choice(fun58)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Reg_Load(2,V(1))
	call_c   Dyam_Reg_Load_Ptr(4,V(2))
	fail_ret
	call_c   DyALog_Format(&R(2),R(4),126,&R(8))
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Char(8,V(4))
	fail_ret
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(5))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun57)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(4),C(119))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(3),&ref[107])
	fail_ret
	call_c   DYAM_Write_2(V(1),V(6))
	fail_ret
fun46:
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(5))
	call_c   Dyam_Reg_Load(4,V(7))
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  pred_format_3()


pl_code global pred_display_arg_2
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(2)
	call_c   Dyam_Choice(fun44)
	call_c   Dyam_Set_Cut()
	pl_call  fun30(&seed[28],1)
	call_c   Dyam_Cut()
	call_c   Dyam_Deallocate()
	pl_ret

pl_code global pred_display_id_2
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(2)
	call_c   Dyam_Choice(fun36)
	call_c   Dyam_Set_Cut()
	pl_call  fun30(&seed[27],1)
	call_c   Dyam_Cut()
	call_c   Dyam_Deallocate()
	pl_ret

pl_code global pred_format_2
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(2)
	move     I(0), R(0)
	move     0, R(1)
	call_c   Dyam_Reg_Load(2,V(1))
	call_c   Dyam_Reg_Load(4,V(2))
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  pred_format_3()


;;------------------ VIEWERS ------------

c_code local build_viewers
	c_ret

c_code local build_seed_20
	ret_reg &seed[20]
	call_c   build_ref_0()
	call_c   build_ref_1()
	call_c   Dyam_Seed_Start(&ref[0],&ref[1],&ref[1],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[20]
	c_ret

pl_code local fun0
	pl_ret

;; TERM 1: display_arg_base_alt('$freg'(_B), _B, 'T')
c_code local build_ref_1
	ret_reg &ref[1]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_112()
	call_c   Dyam_Create_Unary(&ref[112],V(1))
	move_ret R(0)
	call_c   build_ref_111()
	call_c   build_ref_113()
	call_c   Dyam_Term_Start(&ref[111],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(&ref[113])
	call_c   Dyam_Term_End()
	move_ret ref[1]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 113: 'T'
c_code local build_ref_113
	ret_reg &ref[113]
	call_c   Dyam_Create_Atom("T")
	move_ret ref[113]
	c_ret

;; TERM 111: display_arg_base_alt
c_code local build_ref_111
	ret_reg &ref[111]
	call_c   Dyam_Create_Atom("display_arg_base_alt")
	move_ret ref[111]
	c_ret

;; TERM 112: '$freg'
c_code local build_ref_112
	ret_reg &ref[112]
	call_c   Dyam_Create_Atom("$freg")
	move_ret ref[112]
	c_ret

;; TERM 0: '*DATABASE*'
c_code local build_ref_0
	ret_reg &ref[0]
	call_c   Dyam_Create_Atom("*DATABASE*")
	move_ret ref[0]
	c_ret

c_code local build_seed_21
	ret_reg &seed[21]
	call_c   build_ref_0()
	call_c   build_ref_2()
	call_c   Dyam_Seed_Start(&ref[0],&ref[2],&ref[2],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[21]
	c_ret

;; TERM 2: display_arg_base_alt('$reg'(_B), _B, 'R')
c_code local build_ref_2
	ret_reg &ref[2]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_114()
	call_c   Dyam_Create_Unary(&ref[114],V(1))
	move_ret R(0)
	call_c   build_ref_111()
	call_c   build_ref_115()
	call_c   Dyam_Term_Start(&ref[111],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(&ref[115])
	call_c   Dyam_Term_End()
	move_ret ref[2]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 115: 'R'
c_code local build_ref_115
	ret_reg &ref[115]
	call_c   Dyam_Create_Atom("R")
	move_ret ref[115]
	c_ret

;; TERM 114: '$reg'
c_code local build_ref_114
	ret_reg &ref[114]
	call_c   Dyam_Create_Atom("$reg")
	move_ret ref[114]
	c_ret

c_code local build_seed_22
	ret_reg &seed[22]
	call_c   build_ref_0()
	call_c   build_ref_3()
	call_c   Dyam_Seed_Start(&ref[0],&ref[3],&ref[3],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[22]
	c_ret

;; TERM 3: display_arg_base_alt('$internal'(_B), _B, 'I')
c_code local build_ref_3
	ret_reg &ref[3]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_116()
	call_c   Dyam_Create_Unary(&ref[116],V(1))
	move_ret R(0)
	call_c   build_ref_111()
	call_c   build_ref_117()
	call_c   Dyam_Term_Start(&ref[111],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(&ref[117])
	call_c   Dyam_Term_End()
	move_ret ref[3]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 117: 'I'
c_code local build_ref_117
	ret_reg &ref[117]
	call_c   Dyam_Create_Atom("I")
	move_ret ref[117]
	c_ret

;; TERM 116: '$internal'
c_code local build_ref_116
	ret_reg &ref[116]
	call_c   Dyam_Create_Atom("$internal")
	move_ret ref[116]
	c_ret

c_code local build_seed_23
	ret_reg &seed[23]
	call_c   build_ref_0()
	call_c   build_ref_4()
	call_c   Dyam_Seed_Start(&ref[0],&ref[4],&ref[4],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[23]
	c_ret

;; TERM 4: display_arg_base_alt(_Q41997608, _B, 'V')
c_code local build_ref_4
	ret_reg &ref[4]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Unary(I(6),V(1))
	move_ret R(0)
	call_c   build_ref_111()
	call_c   build_ref_118()
	call_c   Dyam_Term_Start(&ref[111],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(&ref[118])
	call_c   Dyam_Term_End()
	move_ret ref[4]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 118: 'V'
c_code local build_ref_118
	ret_reg &ref[118]
	call_c   Dyam_Create_Atom("V")
	move_ret ref[118]
	c_ret

c_code local build_seed_24
	ret_reg &seed[24]
	call_c   build_ref_0()
	call_c   build_ref_5()
	call_c   Dyam_Seed_Start(&ref[0],&ref[5],&ref[5],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[24]
	c_ret

;; TERM 5: display_arg_base('&mem'(_B), _B, foreign_bkt_buffer)
c_code local build_ref_5
	ret_reg &ref[5]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_120()
	call_c   Dyam_Create_Unary(&ref[120],V(1))
	move_ret R(0)
	call_c   build_ref_119()
	call_c   build_ref_121()
	call_c   Dyam_Term_Start(&ref[119],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(&ref[121])
	call_c   Dyam_Term_End()
	move_ret ref[5]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 121: foreign_bkt_buffer
c_code local build_ref_121
	ret_reg &ref[121]
	call_c   Dyam_Create_Atom("foreign_bkt_buffer")
	move_ret ref[121]
	c_ret

;; TERM 119: display_arg_base
c_code local build_ref_119
	ret_reg &ref[119]
	call_c   Dyam_Create_Atom("display_arg_base")
	move_ret ref[119]
	c_ret

;; TERM 120: '&mem'
c_code local build_ref_120
	ret_reg &ref[120]
	call_c   Dyam_Create_Atom("&mem")
	move_ret ref[120]
	c_ret

c_code local build_seed_25
	ret_reg &seed[25]
	call_c   build_ref_0()
	call_c   build_ref_6()
	call_c   Dyam_Seed_Start(&ref[0],&ref[6],&ref[6],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[25]
	c_ret

;; TERM 6: display_arg_base('$seed'(_B, _C), _B, seed)
c_code local build_ref_6
	ret_reg &ref[6]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_122()
	call_c   Dyam_Create_Binary(&ref[122],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_119()
	call_c   build_ref_123()
	call_c   Dyam_Term_Start(&ref[119],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(&ref[123])
	call_c   Dyam_Term_End()
	move_ret ref[6]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 123: seed
c_code local build_ref_123
	ret_reg &ref[123]
	call_c   Dyam_Create_Atom("seed")
	move_ret ref[123]
	c_ret

;; TERM 122: '$seed'
c_code local build_ref_122
	ret_reg &ref[122]
	call_c   Dyam_Create_Atom("$seed")
	move_ret ref[122]
	c_ret

c_code local build_seed_26
	ret_reg &seed[26]
	call_c   build_ref_0()
	call_c   build_ref_7()
	call_c   Dyam_Seed_Start(&ref[0],&ref[7],&ref[7],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[26]
	c_ret

;; TERM 7: display_arg_base('$term'(_B, _C), _B, ref)
c_code local build_ref_7
	ret_reg &ref[7]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_124()
	call_c   Dyam_Create_Binary(&ref[124],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_119()
	call_c   build_ref_125()
	call_c   Dyam_Term_Start(&ref[119],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(&ref[125])
	call_c   Dyam_Term_End()
	move_ret ref[7]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 125: ref
c_code local build_ref_125
	ret_reg &ref[125]
	call_c   Dyam_Create_Atom("ref")
	move_ret ref[125]
	c_ret

;; TERM 124: '$term'
c_code local build_ref_124
	ret_reg &ref[124]
	call_c   Dyam_Create_Atom("$term")
	move_ret ref[124]
	c_ret

c_code local build_seed_8
	ret_reg &seed[8]
	call_c   build_ref_8()
	call_c   build_ref_12()
	call_c   Dyam_Seed_Start(&ref[8],&ref[12],I(0),fun0,1)
	call_c   build_ref_11()
	call_c   Dyam_Seed_Add_Comp(&ref[11],fun2,0)
	call_c   Dyam_Seed_End()
	move_ret seed[8]
	c_ret

;; TERM 11: '*GUARD*'(display_arg_aux(noop, _B))
c_code local build_ref_11
	ret_reg &ref[11]
	call_c   build_ref_9()
	call_c   build_ref_10()
	call_c   Dyam_Create_Unary(&ref[9],&ref[10])
	move_ret ref[11]
	c_ret

;; TERM 10: display_arg_aux(noop, _B)
c_code local build_ref_10
	ret_reg &ref[10]
	call_c   build_ref_126()
	call_c   build_ref_127()
	call_c   Dyam_Create_Binary(&ref[126],&ref[127],V(1))
	move_ret ref[10]
	c_ret

;; TERM 127: noop
c_code local build_ref_127
	ret_reg &ref[127]
	call_c   Dyam_Create_Atom("noop")
	move_ret ref[127]
	c_ret

;; TERM 126: display_arg_aux
c_code local build_ref_126
	ret_reg &ref[126]
	call_c   Dyam_Create_Atom("display_arg_aux")
	move_ret ref[126]
	c_ret

;; TERM 9: '*GUARD*'
c_code local build_ref_9
	ret_reg &ref[9]
	call_c   Dyam_Create_Atom("*GUARD*")
	move_ret ref[9]
	c_ret

;; TERM 13: '0'
c_code local build_ref_13
	ret_reg &ref[13]
	call_c   Dyam_Create_Atom("0")
	move_ret ref[13]
	c_ret

long local pool_fun2[3]=[2,build_ref_11,build_ref_13]

pl_code local fun2
	call_c   Dyam_Pool(pool_fun2)
	call_c   Dyam_Unify_Item(&ref[11])
	fail_ret
	call_c   DYAM_Write_2(V(1),&ref[13])
	fail_ret
	pl_ret

;; TERM 12: '*GUARD*'(display_arg_aux(noop, _B)) :> []
c_code local build_ref_12
	ret_reg &ref[12]
	call_c   build_ref_11()
	call_c   Dyam_Create_Binary(I(9),&ref[11],I(0))
	move_ret ref[12]
	c_ret

;; TERM 8: '*GUARD_CLAUSE*'
c_code local build_ref_8
	ret_reg &ref[8]
	call_c   Dyam_Create_Atom("*GUARD_CLAUSE*")
	move_ret ref[8]
	c_ret

c_code local build_seed_2
	ret_reg &seed[2]
	call_c   build_ref_8()
	call_c   build_ref_16()
	call_c   Dyam_Seed_Start(&ref[8],&ref[16],I(0),fun0,1)
	call_c   build_ref_15()
	call_c   Dyam_Seed_Add_Comp(&ref[15],fun3,0)
	call_c   Dyam_Seed_End()
	move_ret seed[2]
	c_ret

;; TERM 15: '*GUARD*'(display_arg_aux(c(_B), _C))
c_code local build_ref_15
	ret_reg &ref[15]
	call_c   build_ref_9()
	call_c   build_ref_14()
	call_c   Dyam_Create_Unary(&ref[9],&ref[14])
	move_ret ref[15]
	c_ret

;; TERM 14: display_arg_aux(c(_B), _C)
c_code local build_ref_14
	ret_reg &ref[14]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_128()
	call_c   Dyam_Create_Unary(&ref[128],V(1))
	move_ret R(0)
	call_c   build_ref_126()
	call_c   Dyam_Create_Binary(&ref[126],R(0),V(2))
	move_ret ref[14]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 128: c
c_code local build_ref_128
	ret_reg &ref[128]
	call_c   Dyam_Create_Atom("c")
	move_ret ref[128]
	c_ret

pl_code local fun3
	call_c   build_ref_15()
	call_c   Dyam_Unify_Item(&ref[15])
	fail_ret
	call_c   DYAM_Write_2(V(2),V(1))
	fail_ret
	pl_ret

;; TERM 16: '*GUARD*'(display_arg_aux(c(_B), _C)) :> []
c_code local build_ref_16
	ret_reg &ref[16]
	call_c   build_ref_15()
	call_c   Dyam_Create_Binary(I(9),&ref[15],I(0))
	move_ret ref[16]
	c_ret

c_code local build_seed_3
	ret_reg &seed[3]
	call_c   build_ref_8()
	call_c   build_ref_19()
	call_c   Dyam_Seed_Start(&ref[8],&ref[19],I(0),fun0,1)
	call_c   build_ref_18()
	call_c   Dyam_Seed_Add_Comp(&ref[18],fun4,0)
	call_c   Dyam_Seed_End()
	move_ret seed[3]
	c_ret

;; TERM 18: '*GUARD*'(display_arg_aux('$smb'(_B), _C))
c_code local build_ref_18
	ret_reg &ref[18]
	call_c   build_ref_9()
	call_c   build_ref_17()
	call_c   Dyam_Create_Unary(&ref[9],&ref[17])
	move_ret ref[18]
	c_ret

;; TERM 17: display_arg_aux('$smb'(_B), _C)
c_code local build_ref_17
	ret_reg &ref[17]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_129()
	call_c   Dyam_Create_Unary(&ref[129],V(1))
	move_ret R(0)
	call_c   build_ref_126()
	call_c   Dyam_Create_Binary(&ref[126],R(0),V(2))
	move_ret ref[17]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 129: '$smb'
c_code local build_ref_129
	ret_reg &ref[129]
	call_c   Dyam_Create_Atom("$smb")
	move_ret ref[129]
	c_ret

pl_code local fun4
	call_c   build_ref_18()
	call_c   Dyam_Unify_Item(&ref[18])
	fail_ret
	call_c   DYAM_Write_C_Atom_2(V(2),V(1))
	fail_ret
	pl_ret

;; TERM 19: '*GUARD*'(display_arg_aux('$smb'(_B), _C)) :> []
c_code local build_ref_19
	ret_reg &ref[19]
	call_c   build_ref_18()
	call_c   Dyam_Create_Binary(I(9),&ref[18],I(0))
	move_ret ref[19]
	c_ret

c_code local build_seed_18
	ret_reg &seed[18]
	call_c   build_ref_8()
	call_c   build_ref_22()
	call_c   Dyam_Seed_Start(&ref[8],&ref[22],I(0),fun0,1)
	call_c   build_ref_21()
	call_c   Dyam_Seed_Add_Comp(&ref[21],fun5,0)
	call_c   Dyam_Seed_End()
	move_ret seed[18]
	c_ret

;; TERM 21: '*GUARD*'(display_id_aux('$label'(_B), _C))
c_code local build_ref_21
	ret_reg &ref[21]
	call_c   build_ref_9()
	call_c   build_ref_20()
	call_c   Dyam_Create_Unary(&ref[9],&ref[20])
	move_ret ref[21]
	c_ret

;; TERM 20: display_id_aux('$label'(_B), _C)
c_code local build_ref_20
	ret_reg &ref[20]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_131()
	call_c   Dyam_Create_Unary(&ref[131],V(1))
	move_ret R(0)
	call_c   build_ref_130()
	call_c   Dyam_Create_Binary(&ref[130],R(0),V(2))
	move_ret ref[20]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 130: display_id_aux
c_code local build_ref_130
	ret_reg &ref[130]
	call_c   Dyam_Create_Atom("display_id_aux")
	move_ret ref[130]
	c_ret

;; TERM 131: '$label'
c_code local build_ref_131
	ret_reg &ref[131]
	call_c   Dyam_Create_Atom("$label")
	move_ret ref[131]
	c_ret

pl_code local fun5
	call_c   build_ref_21()
	call_c   Dyam_Unify_Item(&ref[21])
	fail_ret
	call_c   DYAM_Write_2(V(2),V(1))
	fail_ret
	pl_ret

;; TERM 22: '*GUARD*'(display_id_aux('$label'(_B), _C)) :> []
c_code local build_ref_22
	ret_reg &ref[22]
	call_c   build_ref_21()
	call_c   Dyam_Create_Binary(I(9),&ref[21],I(0))
	move_ret ref[22]
	c_ret

c_code local build_seed_12
	ret_reg &seed[12]
	call_c   build_ref_8()
	call_c   build_ref_25()
	call_c   Dyam_Seed_Start(&ref[8],&ref[25],I(0),fun0,1)
	call_c   build_ref_24()
	call_c   Dyam_Seed_Add_Comp(&ref[24],fun6,0)
	call_c   Dyam_Seed_End()
	move_ret seed[12]
	c_ret

;; TERM 24: '*GUARD*'(display_arg_aux('$label'(_B), _C))
c_code local build_ref_24
	ret_reg &ref[24]
	call_c   build_ref_9()
	call_c   build_ref_23()
	call_c   Dyam_Create_Unary(&ref[9],&ref[23])
	move_ret ref[24]
	c_ret

;; TERM 23: display_arg_aux('$label'(_B), _C)
c_code local build_ref_23
	ret_reg &ref[23]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_131()
	call_c   Dyam_Create_Unary(&ref[131],V(1))
	move_ret R(0)
	call_c   build_ref_126()
	call_c   Dyam_Create_Binary(&ref[126],R(0),V(2))
	move_ret ref[23]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

pl_code local fun6
	call_c   build_ref_24()
	call_c   Dyam_Unify_Item(&ref[24])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(2))
	call_c   Dyam_Reg_Load(2,V(1))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_display_id_2()

;; TERM 25: '*GUARD*'(display_arg_aux('$label'(_B), _C)) :> []
c_code local build_ref_25
	ret_reg &ref[25]
	call_c   build_ref_24()
	call_c   Dyam_Create_Binary(I(9),&ref[24],I(0))
	move_ret ref[25]
	c_ret

c_code local build_seed_6
	ret_reg &seed[6]
	call_c   build_ref_8()
	call_c   build_ref_28()
	call_c   Dyam_Seed_Start(&ref[8],&ref[28],I(0),fun0,1)
	call_c   build_ref_27()
	call_c   Dyam_Seed_Add_Comp(&ref[27],fun7,0)
	call_c   Dyam_Seed_End()
	move_ret seed[6]
	c_ret

;; TERM 27: '*GUARD*'(display_arg_aux('$tupple'(_B, _C), _D))
c_code local build_ref_27
	ret_reg &ref[27]
	call_c   build_ref_9()
	call_c   build_ref_26()
	call_c   Dyam_Create_Unary(&ref[9],&ref[26])
	move_ret ref[27]
	c_ret

;; TERM 26: display_arg_aux('$tupple'(_B, _C), _D)
c_code local build_ref_26
	ret_reg &ref[26]
	call_c   build_ref_126()
	call_c   build_ref_29()
	call_c   Dyam_Create_Binary(&ref[126],&ref[29],V(3))
	move_ret ref[26]
	c_ret

;; TERM 29: '$tupple'(_B, _C)
c_code local build_ref_29
	ret_reg &ref[29]
	call_c   build_ref_132()
	call_c   Dyam_Create_Binary(&ref[132],V(1),V(2))
	move_ret ref[29]
	c_ret

;; TERM 132: '$tupple'
c_code local build_ref_132
	ret_reg &ref[132]
	call_c   Dyam_Create_Atom("$tupple")
	move_ret ref[132]
	c_ret

long local pool_fun7[3]=[2,build_ref_27,build_ref_29]

pl_code local fun7
	call_c   Dyam_Pool(pool_fun7)
	call_c   Dyam_Unify_Item(&ref[27])
	fail_ret
	call_c   DYAM_Write_2(V(3),&ref[29])
	fail_ret
	pl_ret

;; TERM 28: '*GUARD*'(display_arg_aux('$tupple'(_B, _C), _D)) :> []
c_code local build_ref_28
	ret_reg &ref[28]
	call_c   build_ref_27()
	call_c   Dyam_Create_Binary(I(9),&ref[27],I(0))
	move_ret ref[28]
	c_ret

c_code local build_seed_1
	ret_reg &seed[1]
	call_c   build_ref_8()
	call_c   build_ref_32()
	call_c   Dyam_Seed_Start(&ref[8],&ref[32],I(0),fun0,1)
	call_c   build_ref_31()
	call_c   Dyam_Seed_Add_Comp(&ref[31],fun8,0)
	call_c   Dyam_Seed_End()
	move_ret seed[1]
	c_ret

;; TERM 31: '*GUARD*'(display_arg_aux('$arg'(_B), _C))
c_code local build_ref_31
	ret_reg &ref[31]
	call_c   build_ref_9()
	call_c   build_ref_30()
	call_c   Dyam_Create_Unary(&ref[9],&ref[30])
	move_ret ref[31]
	c_ret

;; TERM 30: display_arg_aux('$arg'(_B), _C)
c_code local build_ref_30
	ret_reg &ref[30]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_133()
	call_c   Dyam_Create_Unary(&ref[133],V(1))
	move_ret R(0)
	call_c   build_ref_126()
	call_c   Dyam_Create_Binary(&ref[126],R(0),V(2))
	move_ret ref[30]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 133: '$arg'
c_code local build_ref_133
	ret_reg &ref[133]
	call_c   Dyam_Create_Atom("$arg")
	move_ret ref[133]
	c_ret

;; TERM 33: 'R(~w)'
c_code local build_ref_33
	ret_reg &ref[33]
	call_c   Dyam_Create_Atom("R(~w)")
	move_ret ref[33]
	c_ret

;; TERM 34: [_B]
c_code local build_ref_34
	ret_reg &ref[34]
	call_c   Dyam_Create_List(V(1),I(0))
	move_ret ref[34]
	c_ret

long local pool_fun8[4]=[3,build_ref_31,build_ref_33,build_ref_34]

pl_code local fun8
	call_c   Dyam_Pool(pool_fun8)
	call_c   Dyam_Unify_Item(&ref[31])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(2))
	move     &ref[33], R(2)
	move     0, R(3)
	move     &ref[34], R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  pred_format_3()

;; TERM 32: '*GUARD*'(display_arg_aux('$arg'(_B), _C)) :> []
c_code local build_ref_32
	ret_reg &ref[32]
	call_c   build_ref_31()
	call_c   Dyam_Create_Binary(I(9),&ref[31],I(0))
	move_ret ref[32]
	c_ret

c_code local build_seed_9
	ret_reg &seed[9]
	call_c   build_ref_8()
	call_c   build_ref_37()
	call_c   Dyam_Seed_Start(&ref[8],&ref[37],I(0),fun0,1)
	call_c   build_ref_36()
	call_c   Dyam_Seed_Add_Comp(&ref[36],fun9,0)
	call_c   Dyam_Seed_End()
	move_ret seed[9]
	c_ret

;; TERM 36: '*GUARD*'(display_arg_aux('$mem'(_B), _C))
c_code local build_ref_36
	ret_reg &ref[36]
	call_c   build_ref_9()
	call_c   build_ref_35()
	call_c   Dyam_Create_Unary(&ref[9],&ref[35])
	move_ret ref[36]
	c_ret

;; TERM 35: display_arg_aux('$mem'(_B), _C)
c_code local build_ref_35
	ret_reg &ref[35]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_134()
	call_c   Dyam_Create_Unary(&ref[134],V(1))
	move_ret R(0)
	call_c   build_ref_126()
	call_c   Dyam_Create_Binary(&ref[126],R(0),V(2))
	move_ret ref[35]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 134: '$mem'
c_code local build_ref_134
	ret_reg &ref[134]
	call_c   Dyam_Create_Atom("$mem")
	move_ret ref[134]
	c_ret

;; TERM 38: 'foreign_bkt_buffer[~w]'
c_code local build_ref_38
	ret_reg &ref[38]
	call_c   Dyam_Create_Atom("foreign_bkt_buffer[~w]")
	move_ret ref[38]
	c_ret

long local pool_fun9[4]=[3,build_ref_36,build_ref_38,build_ref_34]

pl_code local fun9
	call_c   Dyam_Pool(pool_fun9)
	call_c   Dyam_Unify_Item(&ref[36])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(2))
	move     &ref[38], R(2)
	move     0, R(3)
	move     &ref[34], R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  pred_format_3()

;; TERM 37: '*GUARD*'(display_arg_aux('$mem'(_B), _C)) :> []
c_code local build_ref_37
	ret_reg &ref[37]
	call_c   build_ref_36()
	call_c   Dyam_Create_Binary(I(9),&ref[36],I(0))
	move_ret ref[37]
	c_ret

c_code local build_seed_10
	ret_reg &seed[10]
	call_c   build_ref_8()
	call_c   build_ref_41()
	call_c   Dyam_Seed_Start(&ref[8],&ref[41],I(0),fun0,1)
	call_c   build_ref_40()
	call_c   Dyam_Seed_Add_Comp(&ref[40],fun10,0)
	call_c   Dyam_Seed_End()
	move_ret seed[10]
	c_ret

;; TERM 40: '*GUARD*'(display_arg_aux('&freg'(_B), _C))
c_code local build_ref_40
	ret_reg &ref[40]
	call_c   build_ref_9()
	call_c   build_ref_39()
	call_c   Dyam_Create_Unary(&ref[9],&ref[39])
	move_ret ref[40]
	c_ret

;; TERM 39: display_arg_aux('&freg'(_B), _C)
c_code local build_ref_39
	ret_reg &ref[39]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_135()
	call_c   Dyam_Create_Unary(&ref[135],V(1))
	move_ret R(0)
	call_c   build_ref_126()
	call_c   Dyam_Create_Binary(&ref[126],R(0),V(2))
	move_ret ref[39]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 135: '&freg'
c_code local build_ref_135
	ret_reg &ref[135]
	call_c   Dyam_Create_Atom("&freg")
	move_ret ref[135]
	c_ret

;; TERM 42: '&T(~w)'
c_code local build_ref_42
	ret_reg &ref[42]
	call_c   Dyam_Create_Atom("&T(~w)")
	move_ret ref[42]
	c_ret

long local pool_fun10[4]=[3,build_ref_40,build_ref_42,build_ref_34]

pl_code local fun10
	call_c   Dyam_Pool(pool_fun10)
	call_c   Dyam_Unify_Item(&ref[40])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(2))
	move     &ref[42], R(2)
	move     0, R(3)
	move     &ref[34], R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  pred_format_3()

;; TERM 41: '*GUARD*'(display_arg_aux('&freg'(_B), _C)) :> []
c_code local build_ref_41
	ret_reg &ref[41]
	call_c   build_ref_40()
	call_c   Dyam_Create_Binary(I(9),&ref[40],I(0))
	move_ret ref[41]
	c_ret

c_code local build_seed_11
	ret_reg &seed[11]
	call_c   build_ref_8()
	call_c   build_ref_45()
	call_c   Dyam_Seed_Start(&ref[8],&ref[45],I(0),fun0,1)
	call_c   build_ref_44()
	call_c   Dyam_Seed_Add_Comp(&ref[44],fun11,0)
	call_c   Dyam_Seed_End()
	move_ret seed[11]
	c_ret

;; TERM 44: '*GUARD*'(display_arg_aux('&reg'(_B), _C))
c_code local build_ref_44
	ret_reg &ref[44]
	call_c   build_ref_9()
	call_c   build_ref_43()
	call_c   Dyam_Create_Unary(&ref[9],&ref[43])
	move_ret ref[44]
	c_ret

;; TERM 43: display_arg_aux('&reg'(_B), _C)
c_code local build_ref_43
	ret_reg &ref[43]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_136()
	call_c   Dyam_Create_Unary(&ref[136],V(1))
	move_ret R(0)
	call_c   build_ref_126()
	call_c   Dyam_Create_Binary(&ref[126],R(0),V(2))
	move_ret ref[43]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 136: '&reg'
c_code local build_ref_136
	ret_reg &ref[136]
	call_c   Dyam_Create_Atom("&reg")
	move_ret ref[136]
	c_ret

;; TERM 46: '&R(~w)'
c_code local build_ref_46
	ret_reg &ref[46]
	call_c   Dyam_Create_Atom("&R(~w)")
	move_ret ref[46]
	c_ret

long local pool_fun11[4]=[3,build_ref_44,build_ref_46,build_ref_34]

pl_code local fun11
	call_c   Dyam_Pool(pool_fun11)
	call_c   Dyam_Unify_Item(&ref[44])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(2))
	move     &ref[46], R(2)
	move     0, R(3)
	move     &ref[34], R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  pred_format_3()

;; TERM 45: '*GUARD*'(display_arg_aux('&reg'(_B), _C)) :> []
c_code local build_ref_45
	ret_reg &ref[45]
	call_c   build_ref_44()
	call_c   Dyam_Create_Binary(I(9),&ref[44],I(0))
	move_ret ref[45]
	c_ret

c_code local build_seed_16
	ret_reg &seed[16]
	call_c   build_ref_8()
	call_c   build_ref_49()
	call_c   Dyam_Seed_Start(&ref[8],&ref[49],I(0),fun0,1)
	call_c   build_ref_48()
	call_c   Dyam_Seed_Add_Comp(&ref[48],fun12,0)
	call_c   Dyam_Seed_End()
	move_ret seed[16]
	c_ret

;; TERM 48: '*GUARD*'(display_id_aux('$freg'(_B), _C))
c_code local build_ref_48
	ret_reg &ref[48]
	call_c   build_ref_9()
	call_c   build_ref_47()
	call_c   Dyam_Create_Unary(&ref[9],&ref[47])
	move_ret ref[48]
	c_ret

;; TERM 47: display_id_aux('$freg'(_B), _C)
c_code local build_ref_47
	ret_reg &ref[47]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_112()
	call_c   Dyam_Create_Unary(&ref[112],V(1))
	move_ret R(0)
	call_c   build_ref_130()
	call_c   Dyam_Create_Binary(&ref[130],R(0),V(2))
	move_ret ref[47]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 50: 'T(~w)'
c_code local build_ref_50
	ret_reg &ref[50]
	call_c   Dyam_Create_Atom("T(~w)")
	move_ret ref[50]
	c_ret

long local pool_fun12[4]=[3,build_ref_48,build_ref_50,build_ref_34]

pl_code local fun12
	call_c   Dyam_Pool(pool_fun12)
	call_c   Dyam_Unify_Item(&ref[48])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(2))
	move     &ref[50], R(2)
	move     0, R(3)
	move     &ref[34], R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  pred_format_3()

;; TERM 49: '*GUARD*'(display_id_aux('$freg'(_B), _C)) :> []
c_code local build_ref_49
	ret_reg &ref[49]
	call_c   build_ref_48()
	call_c   Dyam_Create_Binary(I(9),&ref[48],I(0))
	move_ret ref[49]
	c_ret

c_code local build_seed_17
	ret_reg &seed[17]
	call_c   build_ref_8()
	call_c   build_ref_53()
	call_c   Dyam_Seed_Start(&ref[8],&ref[53],I(0),fun0,1)
	call_c   build_ref_52()
	call_c   Dyam_Seed_Add_Comp(&ref[52],fun13,0)
	call_c   Dyam_Seed_End()
	move_ret seed[17]
	c_ret

;; TERM 52: '*GUARD*'(display_id_aux('$reg'(_B), _C))
c_code local build_ref_52
	ret_reg &ref[52]
	call_c   build_ref_9()
	call_c   build_ref_51()
	call_c   Dyam_Create_Unary(&ref[9],&ref[51])
	move_ret ref[52]
	c_ret

;; TERM 51: display_id_aux('$reg'(_B), _C)
c_code local build_ref_51
	ret_reg &ref[51]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_114()
	call_c   Dyam_Create_Unary(&ref[114],V(1))
	move_ret R(0)
	call_c   build_ref_130()
	call_c   Dyam_Create_Binary(&ref[130],R(0),V(2))
	move_ret ref[51]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun13[4]=[3,build_ref_52,build_ref_33,build_ref_34]

pl_code local fun13
	call_c   Dyam_Pool(pool_fun13)
	call_c   Dyam_Unify_Item(&ref[52])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(2))
	move     &ref[33], R(2)
	move     0, R(3)
	move     &ref[34], R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  pred_format_3()

;; TERM 53: '*GUARD*'(display_id_aux('$reg'(_B), _C)) :> []
c_code local build_ref_53
	ret_reg &ref[53]
	call_c   build_ref_52()
	call_c   Dyam_Create_Binary(I(9),&ref[52],I(0))
	move_ret ref[53]
	c_ret

c_code local build_seed_4
	ret_reg &seed[4]
	call_c   build_ref_8()
	call_c   build_ref_56()
	call_c   Dyam_Seed_Start(&ref[8],&ref[56],I(0),fun0,1)
	call_c   build_ref_55()
	call_c   Dyam_Seed_Add_Comp(&ref[55],fun16,0)
	call_c   Dyam_Seed_End()
	move_ret seed[4]
	c_ret

;; TERM 55: '*GUARD*'(display_arg_aux(component(_B, _C), _D))
c_code local build_ref_55
	ret_reg &ref[55]
	call_c   build_ref_9()
	call_c   build_ref_54()
	call_c   Dyam_Create_Unary(&ref[9],&ref[54])
	move_ret ref[55]
	c_ret

;; TERM 54: display_arg_aux(component(_B, _C), _D)
c_code local build_ref_54
	ret_reg &ref[54]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_137()
	call_c   Dyam_Create_Binary(&ref[137],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_126()
	call_c   Dyam_Create_Binary(&ref[126],R(0),V(3))
	move_ret ref[54]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 137: component
c_code local build_ref_137
	ret_reg &ref[137]
	call_c   Dyam_Create_Atom("component")
	move_ret ref[137]
	c_ret

;; TERM 57: 'comp ~a ~a'
c_code local build_ref_57
	ret_reg &ref[57]
	call_c   Dyam_Create_Atom("comp ~a ~a")
	move_ret ref[57]
	c_ret

;; TERM 58: [_B,_C]
c_code local build_ref_58
	ret_reg &ref[58]
	call_c   Dyam_Create_Tupple(1,2,I(0))
	move_ret ref[58]
	c_ret

long local pool_fun16[4]=[3,build_ref_55,build_ref_57,build_ref_58]

pl_code local fun16
	call_c   Dyam_Pool(pool_fun16)
	call_c   Dyam_Unify_Item(&ref[55])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(3))
	move     &ref[57], R(2)
	move     0, R(3)
	move     &ref[58], R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  pred_format_3()

;; TERM 56: '*GUARD*'(display_arg_aux(component(_B, _C), _D)) :> []
c_code local build_ref_56
	ret_reg &ref[56]
	call_c   build_ref_55()
	call_c   Dyam_Create_Binary(I(9),&ref[55],I(0))
	move_ret ref[56]
	c_ret

c_code local build_seed_14
	ret_reg &seed[14]
	call_c   build_ref_8()
	call_c   build_ref_61()
	call_c   Dyam_Seed_Start(&ref[8],&ref[61],I(0),fun0,1)
	call_c   build_ref_60()
	call_c   Dyam_Seed_Add_Comp(&ref[60],fun17,0)
	call_c   Dyam_Seed_End()
	move_ret seed[14]
	c_ret

;; TERM 60: '*GUARD*'(display_id_aux((_B + _C), _D))
c_code local build_ref_60
	ret_reg &ref[60]
	call_c   build_ref_9()
	call_c   build_ref_59()
	call_c   Dyam_Create_Unary(&ref[9],&ref[59])
	move_ret ref[60]
	c_ret

;; TERM 59: display_id_aux((_B + _C), _D)
c_code local build_ref_59
	ret_reg &ref[59]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_138()
	call_c   Dyam_Create_Binary(&ref[138],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_130()
	call_c   Dyam_Create_Binary(&ref[130],R(0),V(3))
	move_ret ref[59]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 138: +
c_code local build_ref_138
	ret_reg &ref[138]
	call_c   Dyam_Create_Atom("+")
	move_ret ref[138]
	c_ret

pl_code local fun17
	call_c   build_ref_60()
	call_c   Dyam_Unify_Item(&ref[60])
	fail_ret
	call_c   DYAM_Write_2(V(3),V(1))
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(3))
	call_c   Dyam_Reg_Load(2,V(2))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_display_id_2()

;; TERM 61: '*GUARD*'(display_id_aux((_B + _C), _D)) :> []
c_code local build_ref_61
	ret_reg &ref[61]
	call_c   build_ref_60()
	call_c   Dyam_Create_Binary(I(9),&ref[60],I(0))
	move_ret ref[61]
	c_ret

c_code local build_seed_0
	ret_reg &seed[0]
	call_c   build_ref_8()
	call_c   build_ref_64()
	call_c   Dyam_Seed_Start(&ref[8],&ref[64],I(0),fun0,1)
	call_c   build_ref_63()
	call_c   Dyam_Seed_Add_Comp(&ref[63],fun18,0)
	call_c   Dyam_Seed_End()
	move_ret seed[0]
	c_ret

;; TERM 63: '*GUARD*'(display_arg_aux((_B + _C), _D))
c_code local build_ref_63
	ret_reg &ref[63]
	call_c   build_ref_9()
	call_c   build_ref_62()
	call_c   Dyam_Create_Unary(&ref[9],&ref[62])
	move_ret ref[63]
	c_ret

;; TERM 62: display_arg_aux((_B + _C), _D)
c_code local build_ref_62
	ret_reg &ref[62]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_138()
	call_c   Dyam_Create_Binary(&ref[138],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_126()
	call_c   Dyam_Create_Binary(&ref[126],R(0),V(3))
	move_ret ref[62]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

pl_code local fun18
	call_c   build_ref_63()
	call_c   Dyam_Unify_Item(&ref[63])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(3))
	call_c   Dyam_Reg_Load(2,V(1))
	pl_call  pred_display_arg_2()
	call_c   Dyam_Reg_Load(0,V(3))
	call_c   Dyam_Reg_Load(2,V(2))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_display_id_2()

;; TERM 64: '*GUARD*'(display_arg_aux((_B + _C), _D)) :> []
c_code local build_ref_64
	ret_reg &ref[64]
	call_c   build_ref_63()
	call_c   Dyam_Create_Binary(I(9),&ref[63],I(0))
	move_ret ref[64]
	c_ret

c_code local build_seed_5
	ret_reg &seed[5]
	call_c   build_ref_8()
	call_c   build_ref_67()
	call_c   Dyam_Seed_Start(&ref[8],&ref[67],I(0),fun0,1)
	call_c   build_ref_66()
	call_c   Dyam_Seed_Add_Comp(&ref[66],fun19,0)
	call_c   Dyam_Seed_End()
	move_ret seed[5]
	c_ret

;; TERM 66: '*GUARD*'(display_arg_aux('$lterm'(_B, _C, _D), _E))
c_code local build_ref_66
	ret_reg &ref[66]
	call_c   build_ref_9()
	call_c   build_ref_65()
	call_c   Dyam_Create_Unary(&ref[9],&ref[65])
	move_ret ref[66]
	c_ret

;; TERM 65: display_arg_aux('$lterm'(_B, _C, _D), _E)
c_code local build_ref_65
	ret_reg &ref[65]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_139()
	call_c   Dyam_Term_Start(&ref[139],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_126()
	call_c   Dyam_Create_Binary(&ref[126],R(0),V(4))
	move_ret ref[65]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 139: '$lterm'
c_code local build_ref_139
	ret_reg &ref[139]
	call_c   Dyam_Create_Atom("$lterm")
	move_ret ref[139]
	c_ret

pl_code local fun19
	call_c   build_ref_66()
	call_c   Dyam_Unify_Item(&ref[66])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load_Ptr(2,V(2))
	fail_ret
	move     V(5), R(4)
	move     S(5), R(5)
	call_c   DyALog_Mutable_Read(R(2),&R(4))
	fail_ret
	call_c   Dyam_Reg_Load(0,V(4))
	call_c   Dyam_Reg_Load(2,V(5))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_display_arg_2()

;; TERM 67: '*GUARD*'(display_arg_aux('$lterm'(_B, _C, _D), _E)) :> []
c_code local build_ref_67
	ret_reg &ref[67]
	call_c   build_ref_66()
	call_c   Dyam_Create_Binary(I(9),&ref[66],I(0))
	move_ret ref[67]
	c_ret

c_code local build_seed_7
	ret_reg &seed[7]
	call_c   build_ref_8()
	call_c   build_ref_70()
	call_c   Dyam_Seed_Start(&ref[8],&ref[70],I(0),fun0,1)
	call_c   build_ref_69()
	call_c   Dyam_Seed_Add_Comp(&ref[69],fun23,0)
	call_c   Dyam_Seed_End()
	move_ret seed[7]
	c_ret

;; TERM 69: '*GUARD*'(display_arg_aux('$system'(_B), _C))
c_code local build_ref_69
	ret_reg &ref[69]
	call_c   build_ref_9()
	call_c   build_ref_68()
	call_c   Dyam_Create_Unary(&ref[9],&ref[68])
	move_ret ref[69]
	c_ret

;; TERM 68: display_arg_aux('$system'(_B), _C)
c_code local build_ref_68
	ret_reg &ref[68]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_140()
	call_c   Dyam_Create_Unary(&ref[140],V(1))
	move_ret R(0)
	call_c   build_ref_126()
	call_c   Dyam_Create_Binary(&ref[126],R(0),V(2))
	move_ret ref[68]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 140: '$system'
c_code local build_ref_140
	ret_reg &ref[140]
	call_c   Dyam_Create_Atom("$system")
	move_ret ref[140]
	c_ret

;; TERM 74: 'Not a valid system register ~w'
c_code local build_ref_74
	ret_reg &ref[74]
	call_c   Dyam_Create_Atom("Not a valid system register ~w")
	move_ret ref[74]
	c_ret

;; TERM 75: ['$system'(_B)]
c_code local build_ref_75
	ret_reg &ref[75]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_140()
	call_c   Dyam_Create_Unary(&ref[140],V(1))
	move_ret R(0)
	call_c   Dyam_Create_List(R(0),I(0))
	move_ret ref[75]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 72: 'S(~w)'
c_code local build_ref_72
	ret_reg &ref[72]
	call_c   Dyam_Create_Atom("S(~w)")
	move_ret ref[72]
	c_ret

;; TERM 73: [_D]
c_code local build_ref_73
	ret_reg &ref[73]
	call_c   Dyam_Create_List(V(3),I(0))
	move_ret ref[73]
	c_ret

long local pool_fun21[3]=[2,build_ref_72,build_ref_73]

long local pool_fun22[4]=[65538,build_ref_74,build_ref_75,pool_fun21]

pl_code local fun22
	call_c   Dyam_Remove_Choice()
	move     &ref[74], R(0)
	move     0, R(1)
	move     &ref[75], R(2)
	move     S(5), R(3)
	pl_call  pred_internal_error_2()
fun21:
	move     &ref[72], R(0)
	move     0, R(1)
	move     &ref[73], R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_format_2()


;; TERM 71: system_register(_B, _D)
c_code local build_ref_71
	ret_reg &ref[71]
	call_c   build_ref_141()
	call_c   Dyam_Create_Binary(&ref[141],V(1),V(3))
	move_ret ref[71]
	c_ret

;; TERM 141: system_register
c_code local build_ref_141
	ret_reg &ref[141]
	call_c   Dyam_Create_Atom("system_register")
	move_ret ref[141]
	c_ret

long local pool_fun23[5]=[131074,build_ref_69,build_ref_71,pool_fun22,pool_fun21]

pl_code local fun23
	call_c   Dyam_Pool(pool_fun23)
	call_c   Dyam_Unify_Item(&ref[69])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun22)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[71])
	call_c   Dyam_Cut()
	pl_jump  fun21()

;; TERM 70: '*GUARD*'(display_arg_aux('$system'(_B), _C)) :> []
c_code local build_ref_70
	ret_reg &ref[70]
	call_c   build_ref_69()
	call_c   Dyam_Create_Binary(I(9),&ref[69],I(0))
	move_ret ref[70]
	c_ret

c_code local build_seed_15
	ret_reg &seed[15]
	call_c   build_ref_8()
	call_c   build_ref_78()
	call_c   Dyam_Seed_Start(&ref[8],&ref[78],I(0),fun0,1)
	call_c   build_ref_77()
	call_c   Dyam_Seed_Add_Comp(&ref[77],fun24,0)
	call_c   Dyam_Seed_End()
	move_ret seed[15]
	c_ret

;; TERM 77: '*GUARD*'(display_id_aux('$system'(_B), _C))
c_code local build_ref_77
	ret_reg &ref[77]
	call_c   build_ref_9()
	call_c   build_ref_76()
	call_c   Dyam_Create_Unary(&ref[9],&ref[76])
	move_ret ref[77]
	c_ret

;; TERM 76: display_id_aux('$system'(_B), _C)
c_code local build_ref_76
	ret_reg &ref[76]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_140()
	call_c   Dyam_Create_Unary(&ref[140],V(1))
	move_ret R(0)
	call_c   build_ref_130()
	call_c   Dyam_Create_Binary(&ref[130],R(0),V(2))
	move_ret ref[76]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun24[5]=[131074,build_ref_77,build_ref_71,pool_fun22,pool_fun21]

pl_code local fun24
	call_c   Dyam_Pool(pool_fun24)
	call_c   Dyam_Unify_Item(&ref[77])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun22)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[71])
	call_c   Dyam_Cut()
	pl_jump  fun21()

;; TERM 78: '*GUARD*'(display_id_aux('$system'(_B), _C)) :> []
c_code local build_ref_78
	ret_reg &ref[78]
	call_c   build_ref_77()
	call_c   Dyam_Create_Binary(I(9),&ref[77],I(0))
	move_ret ref[78]
	c_ret

c_code local build_seed_13
	ret_reg &seed[13]
	call_c   build_ref_8()
	call_c   build_ref_81()
	call_c   Dyam_Seed_Start(&ref[8],&ref[81],I(0),fun0,1)
	call_c   build_ref_80()
	call_c   Dyam_Seed_Add_Comp(&ref[80],fun27,0)
	call_c   Dyam_Seed_End()
	move_ret seed[13]
	c_ret

;; TERM 80: '*GUARD*'(display_arg_aux('$fun'(_B, _C, _D), _E))
c_code local build_ref_80
	ret_reg &ref[80]
	call_c   build_ref_9()
	call_c   build_ref_79()
	call_c   Dyam_Create_Unary(&ref[9],&ref[79])
	move_ret ref[80]
	c_ret

;; TERM 79: display_arg_aux('$fun'(_B, _C, _D), _E)
c_code local build_ref_79
	ret_reg &ref[79]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_142()
	call_c   Dyam_Term_Start(&ref[142],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_126()
	call_c   Dyam_Create_Binary(&ref[126],R(0),V(4))
	move_ret ref[79]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 142: '$fun'
c_code local build_ref_142
	ret_reg &ref[142]
	call_c   Dyam_Create_Atom("$fun")
	move_ret ref[142]
	c_ret

;; TERM 83: 'fun~w'
c_code local build_ref_83
	ret_reg &ref[83]
	call_c   Dyam_Create_Atom("fun~w")
	move_ret ref[83]
	c_ret

long local pool_fun26[3]=[2,build_ref_83,build_ref_34]

pl_code local fun26
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Reg_Load(0,V(4))
	move     &ref[83], R(2)
	move     0, R(3)
	move     &ref[34], R(4)
	move     S(5), R(5)
	pl_call  pred_format_3()
fun25:
	call_c   Dyam_Deallocate()
	pl_ret


;; TERM 82: named_function('$fun'(_B, _C, _D), _F, _G)
c_code local build_ref_82
	ret_reg &ref[82]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_142()
	call_c   Dyam_Term_Start(&ref[142],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_143()
	call_c   Dyam_Term_Start(&ref[143],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[82]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 143: named_function
c_code local build_ref_143
	ret_reg &ref[143]
	call_c   Dyam_Create_Atom("named_function")
	move_ret ref[143]
	c_ret

long local pool_fun27[4]=[65538,build_ref_80,build_ref_82,pool_fun26]

pl_code local fun27
	call_c   Dyam_Pool(pool_fun27)
	call_c   Dyam_Unify_Item(&ref[80])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun26)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[82])
	call_c   Dyam_Cut()
	call_c   DYAM_Write_2(V(4),V(5))
	fail_ret
	pl_jump  fun25()

;; TERM 81: '*GUARD*'(display_arg_aux('$fun'(_B, _C, _D), _E)) :> []
c_code local build_ref_81
	ret_reg &ref[81]
	call_c   build_ref_80()
	call_c   Dyam_Create_Binary(I(9),&ref[80],I(0))
	move_ret ref[81]
	c_ret

c_code local build_seed_19
	ret_reg &seed[19]
	call_c   build_ref_8()
	call_c   build_ref_86()
	call_c   Dyam_Seed_Start(&ref[8],&ref[86],I(0),fun0,1)
	call_c   build_ref_85()
	call_c   Dyam_Seed_Add_Comp(&ref[85],fun28,0)
	call_c   Dyam_Seed_End()
	move_ret seed[19]
	c_ret

;; TERM 85: '*GUARD*'(display_id_aux('$fun'(_B, _C, _D), _E))
c_code local build_ref_85
	ret_reg &ref[85]
	call_c   build_ref_9()
	call_c   build_ref_84()
	call_c   Dyam_Create_Unary(&ref[9],&ref[84])
	move_ret ref[85]
	c_ret

;; TERM 84: display_id_aux('$fun'(_B, _C, _D), _E)
c_code local build_ref_84
	ret_reg &ref[84]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_142()
	call_c   Dyam_Term_Start(&ref[142],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_130()
	call_c   Dyam_Create_Binary(&ref[130],R(0),V(4))
	move_ret ref[84]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun28[4]=[65538,build_ref_85,build_ref_82,pool_fun26]

pl_code local fun28
	call_c   Dyam_Pool(pool_fun28)
	call_c   Dyam_Unify_Item(&ref[85])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun26)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[82])
	call_c   Dyam_Cut()
	call_c   DYAM_Write_2(V(4),V(5))
	fail_ret
	pl_jump  fun25()

;; TERM 86: '*GUARD*'(display_id_aux('$fun'(_B, _C, _D), _E)) :> []
c_code local build_ref_86
	ret_reg &ref[86]
	call_c   build_ref_85()
	call_c   Dyam_Create_Binary(I(9),&ref[85],I(0))
	move_ret ref[86]
	c_ret

c_code local build_seed_28
	ret_reg &seed[28]
	call_c   build_ref_9()
	call_c   build_ref_96()
	call_c   Dyam_Seed_Start(&ref[9],&ref[96],I(0),fun29,1)
	call_c   build_ref_97()
	call_c   Dyam_Seed_Add_Comp(&ref[97],&ref[96],0)
	call_c   Dyam_Seed_End()
	move_ret seed[28]
	c_ret

pl_code local fun29
	pl_jump  Complete(0,0)

;; TERM 97: '*GUARD*'(display_arg_aux(_C, _B)) :> '$$HOLE$$'
c_code local build_ref_97
	ret_reg &ref[97]
	call_c   build_ref_96()
	call_c   Dyam_Create_Binary(I(9),&ref[96],I(7))
	move_ret ref[97]
	c_ret

;; TERM 96: '*GUARD*'(display_arg_aux(_C, _B))
c_code local build_ref_96
	ret_reg &ref[96]
	call_c   build_ref_9()
	call_c   build_ref_95()
	call_c   Dyam_Create_Unary(&ref[9],&ref[95])
	move_ret ref[96]
	c_ret

;; TERM 95: display_arg_aux(_C, _B)
c_code local build_ref_95
	ret_reg &ref[95]
	call_c   build_ref_126()
	call_c   Dyam_Create_Binary(&ref[126],V(2),V(1))
	move_ret ref[95]
	c_ret

c_code local build_seed_27
	ret_reg &seed[27]
	call_c   build_ref_9()
	call_c   build_ref_88()
	call_c   Dyam_Seed_Start(&ref[9],&ref[88],I(0),fun29,1)
	call_c   build_ref_89()
	call_c   Dyam_Seed_Add_Comp(&ref[89],&ref[88],0)
	call_c   Dyam_Seed_End()
	move_ret seed[27]
	c_ret

;; TERM 89: '*GUARD*'(display_id_aux(_C, _B)) :> '$$HOLE$$'
c_code local build_ref_89
	ret_reg &ref[89]
	call_c   build_ref_88()
	call_c   Dyam_Create_Binary(I(9),&ref[88],I(7))
	move_ret ref[89]
	c_ret

;; TERM 88: '*GUARD*'(display_id_aux(_C, _B))
c_code local build_ref_88
	ret_reg &ref[88]
	call_c   build_ref_9()
	call_c   build_ref_87()
	call_c   Dyam_Create_Unary(&ref[9],&ref[87])
	move_ret ref[88]
	c_ret

;; TERM 87: display_id_aux(_C, _B)
c_code local build_ref_87
	ret_reg &ref[87]
	call_c   build_ref_130()
	call_c   Dyam_Create_Binary(&ref[130],V(2),V(1))
	move_ret ref[87]
	c_ret

;; TERM 108: [_I|_J]
c_code local build_ref_108
	ret_reg &ref[108]
	call_c   Dyam_Create_List(V(8),V(9))
	move_ret ref[108]
	c_ret

;; TERM 107: [_G|_H]
c_code local build_ref_107
	ret_reg &ref[107]
	call_c   Dyam_Create_List(V(6),V(7))
	move_ret ref[107]
	c_ret

;; TERM 110: [_E]
c_code local build_ref_110
	ret_reg &ref[110]
	call_c   Dyam_Create_List(V(4),I(0))
	move_ret ref[110]
	c_ret

;; TERM 109: 'format: not a valid format option ~~~w'
c_code local build_ref_109
	ret_reg &ref[109]
	call_c   Dyam_Create_Atom("format: not a valid format option ~~~w")
	move_ret ref[109]
	c_ret

;; TERM 98: '&~w[~w]'
c_code local build_ref_98
	ret_reg &ref[98]
	call_c   Dyam_Create_Atom("&~w[~w]")
	move_ret ref[98]
	c_ret

;; TERM 100: '~w(~w)'
c_code local build_ref_100
	ret_reg &ref[100]
	call_c   Dyam_Create_Atom("~w(~w)")
	move_ret ref[100]
	c_ret

;; TERM 99: display_arg_base_alt(_C, _D, _E)
c_code local build_ref_99
	ret_reg &ref[99]
	call_c   build_ref_111()
	call_c   Dyam_Term_Start(&ref[111],3)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[99]
	c_ret

;; TERM 101: 'N(~w)'
c_code local build_ref_101
	ret_reg &ref[101]
	call_c   Dyam_Create_Atom("N(~w)")
	move_ret ref[101]
	c_ret

;; TERM 102: 'F(~w)'
c_code local build_ref_102
	ret_reg &ref[102]
	call_c   Dyam_Create_Atom("F(~w)")
	move_ret ref[102]
	c_ret

;; TERM 104: [_F]
c_code local build_ref_104
	ret_reg &ref[104]
	call_c   Dyam_Create_List(V(5),I(0))
	move_ret ref[104]
	c_ret

;; TERM 103: 'C(~w)'
c_code local build_ref_103
	ret_reg &ref[103]
	call_c   Dyam_Create_Atom("C(~w)")
	move_ret ref[103]
	c_ret

;; TERM 105: &
c_code local build_ref_105
	ret_reg &ref[105]
	call_c   Dyam_Create_Atom("&")
	move_ret ref[105]
	c_ret

;; TERM 106: 'format: not a valid ~~a argument ~w\n'
c_code local build_ref_106
	ret_reg &ref[106]
	call_c   Dyam_Create_Atom("format: not a valid ~~a argument ~w\n")
	move_ret ref[106]
	c_ret

;; TERM 92: [_E,_D]
c_code local build_ref_92
	ret_reg &ref[92]
	call_c   build_ref_73()
	call_c   Dyam_Create_List(V(4),&ref[73])
	move_ret ref[92]
	c_ret

;; TERM 91: '~w[~w]'
c_code local build_ref_91
	ret_reg &ref[91]
	call_c   Dyam_Create_Atom("~w[~w]")
	move_ret ref[91]
	c_ret

;; TERM 90: display_arg_base(_C, _D, _E)
c_code local build_ref_90
	ret_reg &ref[90]
	call_c   build_ref_119()
	call_c   Dyam_Term_Start(&ref[119],3)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[90]
	c_ret

;; TERM 94: [_C]
c_code local build_ref_94
	ret_reg &ref[94]
	call_c   Dyam_Create_List(V(2),I(0))
	move_ret ref[94]
	c_ret

;; TERM 93: 'format: not a valid ~~a label ~w\n'
c_code local build_ref_93
	ret_reg &ref[93]
	call_c   Dyam_Create_Atom("format: not a valid ~~a label ~w\n")
	move_ret ref[93]
	c_ret

pl_code local fun14
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Object(R(0))
	call_c   Dyam_Tabule()
	pl_ret

pl_code local fun15
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Choice(fun14)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Subsume(R(0))
	fail_ret
	call_c   Dyam_Cut()
	pl_ret

pl_code local fun20
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Object(R(0))
	call_c   Dyam_Tabule()
	pl_ret

pl_code local fun48
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(6),&ref[108])
	fail_ret
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(8))
	pl_call  pred_display_arg_2()
	call_c   Dyam_Choice(fun47)
	pl_call  Domain_2(V(10),V(9))
	call_c   Dyam_Reg_Load_Output(2,V(1))
	fail_ret
	call_c   DyALog_Put_Char(R(2),44)
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(10))
	pl_call  pred_display_arg_2()
	pl_fail

pl_code local fun47
	call_c   Dyam_Remove_Choice()
	pl_jump  fun46()

pl_code local fun49
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(6),&ref[108])
	fail_ret
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(8))
	pl_call  pred_display_id_2()
	call_c   Dyam_Choice(fun47)
	pl_call  Domain_2(V(10),V(9))
	call_c   Dyam_Reg_Load_Output(2,V(1))
	fail_ret
	call_c   DyALog_Put_Char(R(2),44)
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(10))
	pl_call  pred_display_id_2()
	pl_fail

pl_code local fun50
	call_c   Dyam_Remove_Choice()
	move     &ref[109], R(0)
	move     0, R(1)
	move     &ref[110], R(2)
	move     S(5), R(3)
	pl_call  pred_internal_error_2()
	pl_jump  fun46()

pl_code local fun51
	call_c   Dyam_Update_Choice(fun50)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(4),C(75))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(3),&ref[107])
	fail_ret
	call_c   DYAM_Writei_2(V(1),V(6))
	fail_ret
	pl_jump  fun46()

pl_code local fun52
	call_c   Dyam_Update_Choice(fun51)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(4),C(107))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(3),&ref[107])
	fail_ret
	call_c   DYAM_Writeq_2(V(1),V(6))
	fail_ret
	pl_jump  fun46()

pl_code local fun53
	call_c   Dyam_Update_Choice(fun52)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(4),C(113))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(3),&ref[107])
	fail_ret
	call_c   DYAM_Write_C_Atom_2(V(1),V(6))
	fail_ret
	pl_jump  fun46()

pl_code local fun54
	call_c   Dyam_Update_Choice(fun53)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(4),C(88))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(3),&ref[107])
	fail_ret
	call_c   Dyam_Choice(fun49)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(6),I(0))
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun46()

pl_code local fun55
	call_c   Dyam_Update_Choice(fun54)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(4),C(65))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(3),&ref[107])
	fail_ret
	call_c   Dyam_Choice(fun48)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(6),I(0))
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun46()

pl_code local fun56
	call_c   Dyam_Update_Choice(fun55)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(4),C(120))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(3),&ref[107])
	fail_ret
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(6))
	pl_call  pred_display_id_2()
	pl_jump  fun46()

pl_code local fun57
	call_c   Dyam_Update_Choice(fun56)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(4),C(97))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(3),&ref[107])
	fail_ret
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(6))
	pl_call  pred_display_arg_2()
	pl_jump  fun46()

pl_code local fun58
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun38
	call_c   Dyam_Remove_Choice()
	move     &ref[106], R(0)
	move     0, R(1)
	move     &ref[94], R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_error_2()

pl_code local fun39
	call_c   Dyam_Update_Choice(fun38)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_atom(V(2))
	fail_ret
	call_c   Dyam_Cut()
	call_c   DYAM_Write_2(V(1),&ref[105])
	fail_ret
	call_c   DYAM_Write_2(V(1),V(2))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun40
	call_c   Dyam_Update_Choice(fun39)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_char(V(2))
	fail_ret
	call_c   Dyam_Cut()
	call_c   DYAM_evpred_is(V(5),V(2))
	fail_ret
	call_c   Dyam_Reg_Load(0,V(1))
	move     &ref[103], R(2)
	move     0, R(3)
	move     &ref[104], R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  pred_format_3()

pl_code local fun41
	call_c   Dyam_Update_Choice(fun40)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_float(V(2))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(1))
	move     &ref[102], R(2)
	move     0, R(3)
	move     &ref[94], R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  pred_format_3()

pl_code local fun42
	call_c   Dyam_Update_Choice(fun41)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_number(V(2))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(1))
	move     &ref[101], R(2)
	move     0, R(3)
	move     &ref[94], R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  pred_format_3()

pl_code local fun43
	call_c   Dyam_Update_Choice(fun42)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[99])
	call_c   Dyam_Cut()
	move     &ref[100], R(0)
	move     0, R(1)
	move     &ref[92], R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_format_2()

pl_code local fun44
	call_c   Dyam_Update_Choice(fun43)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[90])
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(1))
	move     &ref[98], R(2)
	move     0, R(3)
	move     &ref[92], R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  pred_format_3()

pl_code local fun30
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Light_Object(R(0))
	pl_jump  Schedule_Prolog()

pl_code local fun32
	call_c   Dyam_Remove_Choice()
	call_c   DYAM_evpred_float(V(2))
	fail_ret
fun31:
	call_c   Dyam_Cut()
	call_c   DYAM_Write_2(V(1),V(2))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret


pl_code local fun33
	call_c   Dyam_Update_Choice(fun32)
	call_c   DYAM_evpred_number(V(2))
	fail_ret
	pl_jump  fun31()

pl_code local fun34
	call_c   Dyam_Remove_Choice()
	move     &ref[93], R(0)
	move     0, R(1)
	move     &ref[94], R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_error_2()

pl_code local fun35
	call_c   Dyam_Update_Choice(fun34)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Choice(fun33)
	call_c   DYAM_evpred_atom(V(2))
	fail_ret
	pl_jump  fun31()

pl_code local fun36
	call_c   Dyam_Update_Choice(fun35)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[90])
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(1))
	move     &ref[91], R(2)
	move     0, R(3)
	move     &ref[92], R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  pred_format_3()


;;------------------ INIT POOL ------------

c_code local build_init_pool
	call_c   build_seed_20()
	call_c   build_seed_21()
	call_c   build_seed_22()
	call_c   build_seed_23()
	call_c   build_seed_24()
	call_c   build_seed_25()
	call_c   build_seed_26()
	call_c   build_seed_8()
	call_c   build_seed_2()
	call_c   build_seed_3()
	call_c   build_seed_18()
	call_c   build_seed_12()
	call_c   build_seed_6()
	call_c   build_seed_1()
	call_c   build_seed_9()
	call_c   build_seed_10()
	call_c   build_seed_11()
	call_c   build_seed_16()
	call_c   build_seed_17()
	call_c   build_seed_4()
	call_c   build_seed_14()
	call_c   build_seed_0()
	call_c   build_seed_5()
	call_c   build_seed_7()
	call_c   build_seed_15()
	call_c   build_seed_13()
	call_c   build_seed_19()
	call_c   build_seed_28()
	call_c   build_seed_27()
	call_c   build_ref_108()
	call_c   build_ref_107()
	call_c   build_ref_110()
	call_c   build_ref_109()
	call_c   build_ref_98()
	call_c   build_ref_100()
	call_c   build_ref_99()
	call_c   build_ref_101()
	call_c   build_ref_102()
	call_c   build_ref_104()
	call_c   build_ref_103()
	call_c   build_ref_105()
	call_c   build_ref_106()
	call_c   build_ref_92()
	call_c   build_ref_91()
	call_c   build_ref_90()
	call_c   build_ref_94()
	call_c   build_ref_93()
	c_ret

long local ref[144]
long local seed[29]

long local _initialization

c_code global initialization_dyalog_format
	call_c   Dyam_Check_And_Update_Initialization(_initialization)
	fail_c_ret
	call_c   build_init_pool()
	call_c   build_viewers()
	call_c   load()
	c_ret

