;; Compiler: DyALog 1.14.0
;; File "maker.pl"


;;------------------ LOADING ------------

c_code local load
	call_c   Dyam_Loading_Set()
	pl_call  fun13(&seed[11],0)
	pl_call  fun13(&seed[10],0)
	pl_call  fun13(&seed[15],0)
	pl_call  fun13(&seed[9],0)
	pl_call  fun13(&seed[16],0)
	pl_call  fun13(&seed[17],0)
	pl_call  fun13(&seed[13],0)
	pl_call  fun13(&seed[14],0)
	pl_call  fun13(&seed[12],0)
	pl_call  fun13(&seed[8],0)
	pl_call  fun13(&seed[7],0)
	pl_call  fun13(&seed[4],0)
	pl_call  fun13(&seed[0],0)
	pl_call  fun13(&seed[6],0)
	pl_call  fun13(&seed[5],0)
	pl_call  fun13(&seed[2],0)
	pl_call  fun13(&seed[1],0)
	pl_call  fun13(&seed[3],0)
	call_c   Dyam_Loading_Reset()
	c_ret

pl_code global pred_make_callret_functor_5
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(5)
	call_c   Dyam_Choice(fun21)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(1),&ref[77])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(4),&ref[78])
	fail_ret
fun20:
	call_c   Dyam_Choice(fun19)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(1),&ref[79])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(5),&ref[80])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret


pl_code global pred_build_aux_name_3
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Reg_Unify(2,&ref[73])
	fail_ret
	call_c   Dyam_Reg_Bind(4,V(4))
	call_c   DYAM_evpred_atom_to_module(V(2),V(5),V(6))
	fail_ret
	call_c   Dyam_Reg_Load(0,V(1))
	move     &ref[72], R(2)
	move     S(5), R(3)
	move     V(7), R(4)
	move     S(5), R(5)
	pl_call  pred_name_builder_3()
	call_c   DYAM_evpred_atom_to_module(V(4),V(5),V(7))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret


;;------------------ VIEWERS ------------

c_code local build_viewers
	call_c   Dyam_Load_Viewer(&ref[22],&ref[23])
	call_c   Dyam_Load_Viewer(&ref[18],&ref[19])
	call_c   Dyam_Load_Viewer(&ref[14],&ref[15])
	call_c   Dyam_Load_Viewer(&ref[10],&ref[11])
	call_c   Dyam_Load_Viewer(&ref[7],&ref[5])
	call_c   Dyam_Load_Viewer(&ref[3],&ref[4])
	c_ret

c_code local build_seed_3
	ret_reg &seed[3]
	call_c   build_ref_24()
	call_c   build_ref_37()
	call_c   Dyam_Seed_Start(&ref[24],&ref[37],I(0),fun1,1)
	call_c   build_ref_36()
	call_c   Dyam_Seed_Add_Comp(&ref[36],fun4,0)
	call_c   Dyam_Seed_End()
	move_ret seed[3]
	c_ret

;; TERM 36: '*GUARD*'(make_callret_arg(_B, [], [], []))
c_code local build_ref_36
	ret_reg &ref[36]
	call_c   build_ref_25()
	call_c   build_ref_35()
	call_c   Dyam_Create_Unary(&ref[25],&ref[35])
	move_ret ref[36]
	c_ret

;; TERM 35: make_callret_arg(_B, [], [], [])
c_code local build_ref_35
	ret_reg &ref[35]
	call_c   build_ref_224()
	call_c   Dyam_Term_Start(&ref[224],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_End()
	move_ret ref[35]
	c_ret

;; TERM 224: make_callret_arg
c_code local build_ref_224
	ret_reg &ref[224]
	call_c   Dyam_Create_Atom("make_callret_arg")
	move_ret ref[224]
	c_ret

;; TERM 25: '*GUARD*'
c_code local build_ref_25
	ret_reg &ref[25]
	call_c   Dyam_Create_Atom("*GUARD*")
	move_ret ref[25]
	c_ret

pl_code local fun4
	call_c   build_ref_36()
	call_c   Dyam_Unify_Item(&ref[36])
	fail_ret
	pl_ret

pl_code local fun1
	pl_ret

;; TERM 37: '*GUARD*'(make_callret_arg(_B, [], [], [])) :> []
c_code local build_ref_37
	ret_reg &ref[37]
	call_c   build_ref_36()
	call_c   Dyam_Create_Binary(I(9),&ref[36],I(0))
	move_ret ref[37]
	c_ret

;; TERM 24: '*GUARD_CLAUSE*'
c_code local build_ref_24
	ret_reg &ref[24]
	call_c   Dyam_Create_Atom("*GUARD_CLAUSE*")
	move_ret ref[24]
	c_ret

c_code local build_seed_1
	ret_reg &seed[1]
	call_c   build_ref_24()
	call_c   build_ref_31()
	call_c   Dyam_Seed_Start(&ref[24],&ref[31],I(0),fun1,1)
	call_c   build_ref_30()
	call_c   Dyam_Seed_Add_Comp(&ref[30],fun2,0)
	call_c   Dyam_Seed_End()
	move_ret seed[1]
	c_ret

;; TERM 30: '*GUARD*'(make_callret_arg(-, _B, [], _B))
c_code local build_ref_30
	ret_reg &ref[30]
	call_c   build_ref_25()
	call_c   build_ref_29()
	call_c   Dyam_Create_Unary(&ref[25],&ref[29])
	move_ret ref[30]
	c_ret

;; TERM 29: make_callret_arg(-, _B, [], _B)
c_code local build_ref_29
	ret_reg &ref[29]
	call_c   build_ref_224()
	call_c   build_ref_77()
	call_c   Dyam_Term_Start(&ref[224],4)
	call_c   Dyam_Term_Arg(&ref[77])
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_End()
	move_ret ref[29]
	c_ret

;; TERM 77: -
c_code local build_ref_77
	ret_reg &ref[77]
	call_c   Dyam_Create_Atom("-")
	move_ret ref[77]
	c_ret

pl_code local fun2
	call_c   build_ref_30()
	call_c   Dyam_Unify_Item(&ref[30])
	fail_ret
	pl_ret

;; TERM 31: '*GUARD*'(make_callret_arg(-, _B, [], _B)) :> []
c_code local build_ref_31
	ret_reg &ref[31]
	call_c   build_ref_30()
	call_c   Dyam_Create_Binary(I(9),&ref[30],I(0))
	move_ret ref[31]
	c_ret

c_code local build_seed_2
	ret_reg &seed[2]
	call_c   build_ref_24()
	call_c   build_ref_34()
	call_c   Dyam_Seed_Start(&ref[24],&ref[34],I(0),fun1,1)
	call_c   build_ref_33()
	call_c   Dyam_Seed_Add_Comp(&ref[33],fun3,0)
	call_c   Dyam_Seed_End()
	move_ret seed[2]
	c_ret

;; TERM 33: '*GUARD*'(make_callret_arg(+, _B, _B, []))
c_code local build_ref_33
	ret_reg &ref[33]
	call_c   build_ref_25()
	call_c   build_ref_32()
	call_c   Dyam_Create_Unary(&ref[25],&ref[32])
	move_ret ref[33]
	c_ret

;; TERM 32: make_callret_arg(+, _B, _B, [])
c_code local build_ref_32
	ret_reg &ref[32]
	call_c   build_ref_224()
	call_c   build_ref_79()
	call_c   Dyam_Term_Start(&ref[224],4)
	call_c   Dyam_Term_Arg(&ref[79])
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_End()
	move_ret ref[32]
	c_ret

;; TERM 79: +
c_code local build_ref_79
	ret_reg &ref[79]
	call_c   Dyam_Create_Atom("+")
	move_ret ref[79]
	c_ret

pl_code local fun3
	call_c   build_ref_33()
	call_c   Dyam_Unify_Item(&ref[33])
	fail_ret
	pl_ret

;; TERM 34: '*GUARD*'(make_callret_arg(+, _B, _B, [])) :> []
c_code local build_ref_34
	ret_reg &ref[34]
	call_c   build_ref_33()
	call_c   Dyam_Create_Binary(I(9),&ref[33],I(0))
	move_ret ref[34]
	c_ret

c_code local build_seed_5
	ret_reg &seed[5]
	call_c   build_ref_24()
	call_c   build_ref_43()
	call_c   Dyam_Seed_Start(&ref[24],&ref[43],I(0),fun1,1)
	call_c   build_ref_42()
	call_c   Dyam_Seed_Add_Comp(&ref[42],fun6,0)
	call_c   Dyam_Seed_End()
	move_ret seed[5]
	c_ret

;; TERM 42: '*GUARD*'(make_callret_with_pat(-, _B, voidcall, _B))
c_code local build_ref_42
	ret_reg &ref[42]
	call_c   build_ref_25()
	call_c   build_ref_41()
	call_c   Dyam_Create_Unary(&ref[25],&ref[41])
	move_ret ref[42]
	c_ret

;; TERM 41: make_callret_with_pat(-, _B, voidcall, _B)
c_code local build_ref_41
	ret_reg &ref[41]
	call_c   build_ref_225()
	call_c   build_ref_77()
	call_c   build_ref_226()
	call_c   Dyam_Term_Start(&ref[225],4)
	call_c   Dyam_Term_Arg(&ref[77])
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(&ref[226])
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_End()
	move_ret ref[41]
	c_ret

;; TERM 226: voidcall
c_code local build_ref_226
	ret_reg &ref[226]
	call_c   Dyam_Create_Atom("voidcall")
	move_ret ref[226]
	c_ret

;; TERM 225: make_callret_with_pat
c_code local build_ref_225
	ret_reg &ref[225]
	call_c   Dyam_Create_Atom("make_callret_with_pat")
	move_ret ref[225]
	c_ret

pl_code local fun6
	call_c   build_ref_42()
	call_c   Dyam_Unify_Item(&ref[42])
	fail_ret
	pl_ret

;; TERM 43: '*GUARD*'(make_callret_with_pat(-, _B, voidcall, _B)) :> []
c_code local build_ref_43
	ret_reg &ref[43]
	call_c   build_ref_42()
	call_c   Dyam_Create_Binary(I(9),&ref[42],I(0))
	move_ret ref[43]
	c_ret

c_code local build_seed_6
	ret_reg &seed[6]
	call_c   build_ref_24()
	call_c   build_ref_46()
	call_c   Dyam_Seed_Start(&ref[24],&ref[46],I(0),fun1,1)
	call_c   build_ref_45()
	call_c   Dyam_Seed_Add_Comp(&ref[45],fun7,0)
	call_c   Dyam_Seed_End()
	move_ret seed[6]
	c_ret

;; TERM 45: '*GUARD*'(make_callret_with_pat(+, _B, _B, voidret))
c_code local build_ref_45
	ret_reg &ref[45]
	call_c   build_ref_25()
	call_c   build_ref_44()
	call_c   Dyam_Create_Unary(&ref[25],&ref[44])
	move_ret ref[45]
	c_ret

;; TERM 44: make_callret_with_pat(+, _B, _B, voidret)
c_code local build_ref_44
	ret_reg &ref[44]
	call_c   build_ref_225()
	call_c   build_ref_79()
	call_c   build_ref_6()
	call_c   Dyam_Term_Start(&ref[225],4)
	call_c   Dyam_Term_Arg(&ref[79])
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(&ref[6])
	call_c   Dyam_Term_End()
	move_ret ref[44]
	c_ret

;; TERM 6: voidret
c_code local build_ref_6
	ret_reg &ref[6]
	call_c   Dyam_Create_Atom("voidret")
	move_ret ref[6]
	c_ret

pl_code local fun7
	call_c   build_ref_45()
	call_c   Dyam_Unify_Item(&ref[45])
	fail_ret
	pl_ret

;; TERM 46: '*GUARD*'(make_callret_with_pat(+, _B, _B, voidret)) :> []
c_code local build_ref_46
	ret_reg &ref[46]
	call_c   build_ref_45()
	call_c   Dyam_Create_Binary(I(9),&ref[45],I(0))
	move_ret ref[46]
	c_ret

c_code local build_seed_0
	ret_reg &seed[0]
	call_c   build_ref_24()
	call_c   build_ref_28()
	call_c   Dyam_Seed_Start(&ref[24],&ref[28],I(0),fun1,1)
	call_c   build_ref_27()
	call_c   Dyam_Seed_Add_Comp(&ref[27],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[0]
	c_ret

;; TERM 27: '*GUARD*'(make_callret_arg(*, _B, _B, _B))
c_code local build_ref_27
	ret_reg &ref[27]
	call_c   build_ref_25()
	call_c   build_ref_26()
	call_c   Dyam_Create_Unary(&ref[25],&ref[26])
	move_ret ref[27]
	c_ret

;; TERM 26: make_callret_arg(*, _B, _B, _B)
c_code local build_ref_26
	ret_reg &ref[26]
	call_c   build_ref_224()
	call_c   build_ref_227()
	call_c   Dyam_Term_Start(&ref[224],4)
	call_c   Dyam_Term_Arg(&ref[227])
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_End()
	move_ret ref[26]
	c_ret

;; TERM 227: *
c_code local build_ref_227
	ret_reg &ref[227]
	call_c   Dyam_Create_Atom("*")
	move_ret ref[227]
	c_ret

pl_code local fun0
	call_c   build_ref_27()
	call_c   Dyam_Unify_Item(&ref[27])
	fail_ret
	pl_ret

;; TERM 28: '*GUARD*'(make_callret_arg(*, _B, _B, _B)) :> []
c_code local build_ref_28
	ret_reg &ref[28]
	call_c   build_ref_27()
	call_c   Dyam_Create_Binary(I(9),&ref[27],I(0))
	move_ret ref[28]
	c_ret

c_code local build_seed_4
	ret_reg &seed[4]
	call_c   build_ref_24()
	call_c   build_ref_40()
	call_c   Dyam_Seed_Start(&ref[24],&ref[40],I(0),fun1,1)
	call_c   build_ref_39()
	call_c   Dyam_Seed_Add_Comp(&ref[39],fun5,0)
	call_c   Dyam_Seed_End()
	move_ret seed[4]
	c_ret

;; TERM 39: '*GUARD*'(make_callret_with_pat(*, _B, _B, _B))
c_code local build_ref_39
	ret_reg &ref[39]
	call_c   build_ref_25()
	call_c   build_ref_38()
	call_c   Dyam_Create_Unary(&ref[25],&ref[38])
	move_ret ref[39]
	c_ret

;; TERM 38: make_callret_with_pat(*, _B, _B, _B)
c_code local build_ref_38
	ret_reg &ref[38]
	call_c   build_ref_225()
	call_c   build_ref_227()
	call_c   Dyam_Term_Start(&ref[225],4)
	call_c   Dyam_Term_Arg(&ref[227])
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_End()
	move_ret ref[38]
	c_ret

pl_code local fun5
	call_c   build_ref_39()
	call_c   Dyam_Unify_Item(&ref[39])
	fail_ret
	pl_ret

;; TERM 40: '*GUARD*'(make_callret_with_pat(*, _B, _B, _B)) :> []
c_code local build_ref_40
	ret_reg &ref[40]
	call_c   build_ref_39()
	call_c   Dyam_Create_Binary(I(9),&ref[39],I(0))
	move_ret ref[40]
	c_ret

c_code local build_seed_7
	ret_reg &seed[7]
	call_c   build_ref_47()
	call_c   build_ref_49()
	call_c   Dyam_Seed_Start(&ref[47],&ref[49],I(0),fun12,1)
	call_c   build_ref_51()
	call_c   Dyam_Seed_Add_Comp(&ref[51],fun31,0)
	call_c   Dyam_Seed_End()
	move_ret seed[7]
	c_ret

;; TERM 51: '*CITEM*'('call_bmg_patterns/2', _A)
c_code local build_ref_51
	ret_reg &ref[51]
	call_c   build_ref_50()
	call_c   build_ref_1()
	call_c   Dyam_Create_Binary(&ref[50],&ref[1],V(0))
	move_ret ref[51]
	c_ret

;; TERM 1: 'call_bmg_patterns/2'
c_code local build_ref_1
	ret_reg &ref[1]
	call_c   Dyam_Create_Atom("call_bmg_patterns/2")
	move_ret ref[1]
	c_ret

;; TERM 50: '*CITEM*'
c_code local build_ref_50
	ret_reg &ref[50]
	call_c   Dyam_Create_Atom("*CITEM*")
	move_ret ref[50]
	c_ret

;; TERM 95: '$CLOSURE'('$fun'(30, 0, 1149267124), '$TUPPLE'(35101651501856))
c_code local build_ref_95
	ret_reg &ref[95]
	call_c   build_ref_94()
	call_c   Dyam_Closure_Aux(fun30,&ref[94])
	move_ret ref[95]
	c_ret

;; TERM 92: '$CLOSURE'('$fun'(29, 0, 1149261076), '$TUPPLE'(35101660676100))
c_code local build_ref_92
	ret_reg &ref[92]
	call_c   build_ref_91()
	call_c   Dyam_Closure_Aux(fun29,&ref[91])
	move_ret ref[92]
	c_ret

c_code local build_seed_22
	ret_reg &seed[22]
	call_c   build_ref_0()
	call_c   build_ref_87()
	call_c   Dyam_Seed_Start(&ref[0],&ref[87],&ref[87],fun8,1)
	call_c   build_ref_88()
	call_c   Dyam_Seed_Add_Comp(&ref[88],&ref[87],0)
	call_c   Dyam_Seed_End()
	move_ret seed[22]
	c_ret

pl_code local fun8
	pl_jump  Complete(0,0)

;; TERM 88: '*RITEM*'(_A, return(_B, _C)) :> '$$HOLE$$'
c_code local build_ref_88
	ret_reg &ref[88]
	call_c   build_ref_87()
	call_c   Dyam_Create_Binary(I(9),&ref[87],I(7))
	move_ret ref[88]
	c_ret

;; TERM 87: '*RITEM*'(_A, return(_B, _C))
c_code local build_ref_87
	ret_reg &ref[87]
	call_c   build_ref_0()
	call_c   build_ref_21()
	call_c   Dyam_Create_Binary(&ref[0],V(0),&ref[21])
	move_ret ref[87]
	c_ret

;; TERM 21: return(_B, _C)
c_code local build_ref_21
	ret_reg &ref[21]
	call_c   build_ref_80()
	call_c   Dyam_Create_Binary(&ref[80],V(1),V(2))
	move_ret ref[21]
	c_ret

;; TERM 80: return
c_code local build_ref_80
	ret_reg &ref[80]
	call_c   Dyam_Create_Atom("return")
	move_ret ref[80]
	c_ret

;; TERM 0: '*RITEM*'
c_code local build_ref_0
	ret_reg &ref[0]
	call_c   Dyam_Create_Atom("*RITEM*")
	move_ret ref[0]
	c_ret

pl_code local fun9
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Object(R(0))
	call_c   Dyam_Tabule()
	pl_ret

pl_code local fun10
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Choice(fun9)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Subsume(R(0))
	fail_ret
	call_c   Dyam_Cut()
	pl_ret

long local pool_fun29[2]=[1,build_seed_22]

pl_code local fun29
	call_c   Dyam_Pool(pool_fun29)
	pl_jump  fun10(&seed[22],2)

;; TERM 91: '$TUPPLE'(35101660676100)
c_code local build_ref_91
	ret_reg &ref[91]
	call_c   Dyam_Create_Simple_Tupple(0,469762048)
	move_ret ref[91]
	c_ret

long local pool_fun30[3]=[2,build_ref_92,build_ref_77]

pl_code local fun30
	call_c   Dyam_Pool(pool_fun30)
	call_c   Dyam_Allocate(0)
	move     &ref[92], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	move     &ref[77], R(4)
	move     0, R(5)
	move     V(2), R(6)
	move     S(5), R(7)
	call_c   Dyam_Reg_Deallocate(4)
fun28:
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Reg_Bind(2,V(4))
	call_c   Dyam_Multi_Reg_Bind(4,2,2)
	call_c   Dyam_Choice(fun27)
	pl_call  fun24(&seed[18],1)
	pl_fail


;; TERM 94: '$TUPPLE'(35101651501856)
c_code local build_ref_94
	ret_reg &ref[94]
	call_c   Dyam_Create_Simple_Tupple(0,402653184)
	move_ret ref[94]
	c_ret

long local pool_fun31[4]=[3,build_ref_51,build_ref_95,build_ref_79]

pl_code local fun31
	call_c   Dyam_Pool(pool_fun31)
	call_c   Dyam_Unify_Item(&ref[51])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[95], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	move     &ref[79], R(4)
	move     0, R(5)
	move     V(1), R(6)
	move     S(5), R(7)
	call_c   Dyam_Reg_Deallocate(4)
	pl_jump  fun28()

pl_code local fun12
	pl_jump  Apply(0,0)

;; TERM 49: '*FIRST*'('call_bmg_patterns/2') :> []
c_code local build_ref_49
	ret_reg &ref[49]
	call_c   build_ref_48()
	call_c   Dyam_Create_Binary(I(9),&ref[48],I(0))
	move_ret ref[49]
	c_ret

;; TERM 48: '*FIRST*'('call_bmg_patterns/2')
c_code local build_ref_48
	ret_reg &ref[48]
	call_c   build_ref_47()
	call_c   build_ref_1()
	call_c   Dyam_Create_Unary(&ref[47],&ref[1])
	move_ret ref[48]
	c_ret

;; TERM 47: '*FIRST*'
c_code local build_ref_47
	ret_reg &ref[47]
	call_c   Dyam_Create_Atom("*FIRST*")
	move_ret ref[47]
	c_ret

c_code local build_seed_8
	ret_reg &seed[8]
	call_c   build_ref_47()
	call_c   build_ref_58()
	call_c   Dyam_Seed_Start(&ref[47],&ref[58],I(0),fun12,1)
	call_c   build_ref_59()
	call_c   Dyam_Seed_Add_Comp(&ref[59],fun11,0)
	call_c   Dyam_Seed_End()
	move_ret seed[8]
	c_ret

;; TERM 59: '*CITEM*'(bmg_uniform_stacks(_B, _C), _A)
c_code local build_ref_59
	ret_reg &ref[59]
	call_c   build_ref_50()
	call_c   build_ref_56()
	call_c   Dyam_Create_Binary(&ref[50],&ref[56],V(0))
	move_ret ref[59]
	c_ret

;; TERM 56: bmg_uniform_stacks(_B, _C)
c_code local build_ref_56
	ret_reg &ref[56]
	call_c   build_ref_228()
	call_c   Dyam_Create_Binary(&ref[228],V(1),V(2))
	move_ret ref[56]
	c_ret

;; TERM 228: bmg_uniform_stacks
c_code local build_ref_228
	ret_reg &ref[228]
	call_c   Dyam_Create_Atom("bmg_uniform_stacks")
	move_ret ref[228]
	c_ret

;; TERM 60: bmg_stacks(_D)
c_code local build_ref_60
	ret_reg &ref[60]
	call_c   build_ref_229()
	call_c   Dyam_Create_Unary(&ref[229],V(3))
	move_ret ref[60]
	c_ret

;; TERM 229: bmg_stacks
c_code local build_ref_229
	ret_reg &ref[229]
	call_c   Dyam_Create_Atom("bmg_stacks")
	move_ret ref[229]
	c_ret

c_code local build_seed_19
	ret_reg &seed[19]
	call_c   build_ref_0()
	call_c   build_ref_61()
	call_c   Dyam_Seed_Start(&ref[0],&ref[61],&ref[61],fun8,1)
	call_c   build_ref_62()
	call_c   Dyam_Seed_Add_Comp(&ref[62],&ref[61],0)
	call_c   Dyam_Seed_End()
	move_ret seed[19]
	c_ret

;; TERM 62: '*RITEM*'(_A, voidret) :> '$$HOLE$$'
c_code local build_ref_62
	ret_reg &ref[62]
	call_c   build_ref_61()
	call_c   Dyam_Create_Binary(I(9),&ref[61],I(7))
	move_ret ref[62]
	c_ret

;; TERM 61: '*RITEM*'(_A, voidret)
c_code local build_ref_61
	ret_reg &ref[61]
	call_c   build_ref_0()
	call_c   build_ref_6()
	call_c   Dyam_Create_Binary(&ref[0],V(0),&ref[6])
	move_ret ref[61]
	c_ret

long local pool_fun11[4]=[3,build_ref_59,build_ref_60,build_seed_19]

pl_code local fun11
	call_c   Dyam_Pool(pool_fun11)
	call_c   Dyam_Unify_Item(&ref[59])
	fail_ret
	call_c   Dyam_Allocate(0)
	pl_call  Object_1(&ref[60])
	call_c   DYAM_evpred_length(V(3),V(4))
	fail_ret
	call_c   DYAM_Make_List(V(2),V(4),V(1))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_jump  fun10(&seed[19],2)

;; TERM 58: '*FIRST*'(bmg_uniform_stacks(_B, _C)) :> []
c_code local build_ref_58
	ret_reg &ref[58]
	call_c   build_ref_57()
	call_c   Dyam_Create_Binary(I(9),&ref[57],I(0))
	move_ret ref[58]
	c_ret

;; TERM 57: '*FIRST*'(bmg_uniform_stacks(_B, _C))
c_code local build_ref_57
	ret_reg &ref[57]
	call_c   build_ref_47()
	call_c   build_ref_56()
	call_c   Dyam_Create_Unary(&ref[47],&ref[56])
	move_ret ref[57]
	c_ret

c_code local build_seed_12
	ret_reg &seed[12]
	call_c   build_ref_24()
	call_c   build_ref_65()
	call_c   Dyam_Seed_Start(&ref[24],&ref[65],I(0),fun1,1)
	call_c   build_ref_64()
	call_c   Dyam_Seed_Add_Comp(&ref[64],fun15,0)
	call_c   Dyam_Seed_End()
	move_ret seed[12]
	c_ret

;; TERM 64: '*GUARD*'(make_callret_arg([-|_B], [_C|_D], _E, [_C|_F]))
c_code local build_ref_64
	ret_reg &ref[64]
	call_c   build_ref_25()
	call_c   build_ref_63()
	call_c   Dyam_Create_Unary(&ref[25],&ref[63])
	move_ret ref[64]
	c_ret

;; TERM 63: make_callret_arg([-|_B], [_C|_D], _E, [_C|_F])
c_code local build_ref_63
	ret_reg &ref[63]
	call_c   Dyam_Pseudo_Choice(3)
	call_c   build_ref_77()
	call_c   Dyam_Create_List(&ref[77],V(1))
	move_ret R(0)
	call_c   Dyam_Create_List(V(2),V(3))
	move_ret R(1)
	call_c   Dyam_Create_List(V(2),V(5))
	move_ret R(2)
	call_c   build_ref_224()
	call_c   Dyam_Term_Start(&ref[224],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(R(2))
	call_c   Dyam_Term_End()
	move_ret ref[63]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_20
	ret_reg &seed[20]
	call_c   build_ref_25()
	call_c   build_ref_67()
	call_c   Dyam_Seed_Start(&ref[25],&ref[67],I(0),fun8,1)
	call_c   build_ref_68()
	call_c   Dyam_Seed_Add_Comp(&ref[68],&ref[67],0)
	call_c   Dyam_Seed_End()
	move_ret seed[20]
	c_ret

;; TERM 68: '*GUARD*'(make_callret_arg(_B, _D, _E, _F)) :> '$$HOLE$$'
c_code local build_ref_68
	ret_reg &ref[68]
	call_c   build_ref_67()
	call_c   Dyam_Create_Binary(I(9),&ref[67],I(7))
	move_ret ref[68]
	c_ret

;; TERM 67: '*GUARD*'(make_callret_arg(_B, _D, _E, _F))
c_code local build_ref_67
	ret_reg &ref[67]
	call_c   build_ref_25()
	call_c   build_ref_66()
	call_c   Dyam_Create_Unary(&ref[25],&ref[66])
	move_ret ref[67]
	c_ret

;; TERM 66: make_callret_arg(_B, _D, _E, _F)
c_code local build_ref_66
	ret_reg &ref[66]
	call_c   build_ref_224()
	call_c   Dyam_Term_Start(&ref[224],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[66]
	c_ret

pl_code local fun14
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Light_Object(R(0))
	pl_jump  Schedule_Prolog()

long local pool_fun15[3]=[2,build_ref_64,build_seed_20]

pl_code local fun15
	call_c   Dyam_Pool(pool_fun15)
	call_c   Dyam_Unify_Item(&ref[64])
	fail_ret
	pl_jump  fun14(&seed[20],1)

;; TERM 65: '*GUARD*'(make_callret_arg([-|_B], [_C|_D], _E, [_C|_F])) :> []
c_code local build_ref_65
	ret_reg &ref[65]
	call_c   build_ref_64()
	call_c   Dyam_Create_Binary(I(9),&ref[64],I(0))
	move_ret ref[65]
	c_ret

c_code local build_seed_14
	ret_reg &seed[14]
	call_c   build_ref_24()
	call_c   build_ref_71()
	call_c   Dyam_Seed_Start(&ref[24],&ref[71],I(0),fun1,1)
	call_c   build_ref_70()
	call_c   Dyam_Seed_Add_Comp(&ref[70],fun16,0)
	call_c   Dyam_Seed_End()
	move_ret seed[14]
	c_ret

;; TERM 70: '*GUARD*'(make_callret_arg([+|_B], [_C|_D], [_C|_E], _F))
c_code local build_ref_70
	ret_reg &ref[70]
	call_c   build_ref_25()
	call_c   build_ref_69()
	call_c   Dyam_Create_Unary(&ref[25],&ref[69])
	move_ret ref[70]
	c_ret

;; TERM 69: make_callret_arg([+|_B], [_C|_D], [_C|_E], _F)
c_code local build_ref_69
	ret_reg &ref[69]
	call_c   Dyam_Pseudo_Choice(3)
	call_c   build_ref_79()
	call_c   Dyam_Create_List(&ref[79],V(1))
	move_ret R(0)
	call_c   Dyam_Create_List(V(2),V(3))
	move_ret R(1)
	call_c   Dyam_Create_List(V(2),V(4))
	move_ret R(2)
	call_c   build_ref_224()
	call_c   Dyam_Term_Start(&ref[224],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(R(2))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[69]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun16[3]=[2,build_ref_70,build_seed_20]

pl_code local fun16
	call_c   Dyam_Pool(pool_fun16)
	call_c   Dyam_Unify_Item(&ref[70])
	fail_ret
	pl_jump  fun14(&seed[20],1)

;; TERM 71: '*GUARD*'(make_callret_arg([+|_B], [_C|_D], [_C|_E], _F)) :> []
c_code local build_ref_71
	ret_reg &ref[71]
	call_c   build_ref_70()
	call_c   Dyam_Create_Binary(I(9),&ref[70],I(0))
	move_ret ref[71]
	c_ret

c_code local build_seed_13
	ret_reg &seed[13]
	call_c   build_ref_24()
	call_c   build_ref_76()
	call_c   Dyam_Seed_Start(&ref[24],&ref[76],I(0),fun1,1)
	call_c   build_ref_75()
	call_c   Dyam_Seed_Add_Comp(&ref[75],fun18,0)
	call_c   Dyam_Seed_End()
	move_ret seed[13]
	c_ret

;; TERM 75: '*GUARD*'(make_callret_arg([*|_B], [_C|_D], [_C|_E], [_C|_F]))
c_code local build_ref_75
	ret_reg &ref[75]
	call_c   build_ref_25()
	call_c   build_ref_74()
	call_c   Dyam_Create_Unary(&ref[25],&ref[74])
	move_ret ref[75]
	c_ret

;; TERM 74: make_callret_arg([*|_B], [_C|_D], [_C|_E], [_C|_F])
c_code local build_ref_74
	ret_reg &ref[74]
	call_c   Dyam_Pseudo_Choice(4)
	call_c   build_ref_227()
	call_c   Dyam_Create_List(&ref[227],V(1))
	move_ret R(0)
	call_c   Dyam_Create_List(V(2),V(3))
	move_ret R(1)
	call_c   Dyam_Create_List(V(2),V(4))
	move_ret R(2)
	call_c   Dyam_Create_List(V(2),V(5))
	move_ret R(3)
	call_c   build_ref_224()
	call_c   Dyam_Term_Start(&ref[224],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(R(2))
	call_c   Dyam_Term_Arg(R(3))
	call_c   Dyam_Term_End()
	move_ret ref[74]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun18[3]=[2,build_ref_75,build_seed_20]

pl_code local fun18
	call_c   Dyam_Pool(pool_fun18)
	call_c   Dyam_Unify_Item(&ref[75])
	fail_ret
	pl_jump  fun14(&seed[20],1)

;; TERM 76: '*GUARD*'(make_callret_arg([*|_B], [_C|_D], [_C|_E], [_C|_F])) :> []
c_code local build_ref_76
	ret_reg &ref[76]
	call_c   build_ref_75()
	call_c   Dyam_Create_Binary(I(9),&ref[75],I(0))
	move_ret ref[76]
	c_ret

c_code local build_seed_17
	ret_reg &seed[17]
	call_c   build_ref_24()
	call_c   build_ref_98()
	call_c   Dyam_Seed_Start(&ref[24],&ref[98],I(0),fun1,1)
	call_c   build_ref_97()
	call_c   Dyam_Seed_Add_Comp(&ref[97],fun36,0)
	call_c   Dyam_Seed_End()
	move_ret seed[17]
	c_ret

;; TERM 97: '*GUARD*'(make_callret(_B, _C, _D))
c_code local build_ref_97
	ret_reg &ref[97]
	call_c   build_ref_25()
	call_c   build_ref_96()
	call_c   Dyam_Create_Unary(&ref[25],&ref[96])
	move_ret ref[97]
	c_ret

;; TERM 96: make_callret(_B, _C, _D)
c_code local build_ref_96
	ret_reg &ref[96]
	call_c   build_ref_230()
	call_c   Dyam_Term_Start(&ref[230],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[96]
	c_ret

;; TERM 230: make_callret
c_code local build_ref_230
	ret_reg &ref[230]
	call_c   Dyam_Create_Atom("make_callret")
	move_ret ref[230]
	c_ret

;; TERM 109: '$CLOSURE'('$fun'(35, 0, 1149314720), '$TUPPLE'(35101651502012))
c_code local build_ref_109
	ret_reg &ref[109]
	call_c   build_ref_108()
	call_c   Dyam_Closure_Aux(fun35,&ref[108])
	move_ret ref[109]
	c_ret

pl_code local fun35
	call_c   Dyam_Unify(V(1),V(6))
	fail_ret
	pl_ret

;; TERM 108: '$TUPPLE'(35101651502012)
c_code local build_ref_108
	ret_reg &ref[108]
	call_c   Dyam_Create_Simple_Tupple(0,138412032)
	move_ret ref[108]
	c_ret

long local pool_fun36[3]=[2,build_ref_97,build_ref_109]

pl_code local fun36
	call_c   Dyam_Pool(pool_fun36)
	call_c   Dyam_Unify_Item(&ref[97])
	fail_ret
	call_c   DYAM_evpred_functor(V(1),V(4),V(5))
	fail_ret
	call_c   DYAM_evpred_functor(V(6),V(4),V(5))
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[109], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Load(4,V(6))
	call_c   Dyam_Reg_Load(6,V(2))
	call_c   Dyam_Reg_Load(8,V(3))
	call_c   Dyam_Reg_Deallocate(5)
fun34:
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Reg_Bind(2,V(5))
	call_c   Dyam_Multi_Reg_Bind(4,2,3)
	call_c   Dyam_Choice(fun33)
	pl_call  fun24(&seed[23],1)
	pl_fail


;; TERM 98: '*GUARD*'(make_callret(_B, _C, _D)) :> []
c_code local build_ref_98
	ret_reg &ref[98]
	call_c   build_ref_97()
	call_c   Dyam_Create_Binary(I(9),&ref[97],I(0))
	move_ret ref[98]
	c_ret

c_code local build_seed_16
	ret_reg &seed[16]
	call_c   build_ref_47()
	call_c   build_ref_112()
	call_c   Dyam_Seed_Start(&ref[47],&ref[112],I(0),fun12,1)
	call_c   build_ref_113()
	call_c   Dyam_Seed_Add_Comp(&ref[113],fun42,0)
	call_c   Dyam_Seed_End()
	move_ret seed[16]
	c_ret

;; TERM 113: '*CITEM*'('call_make_true_callret/3'(_B), _A)
c_code local build_ref_113
	ret_reg &ref[113]
	call_c   build_ref_50()
	call_c   build_ref_110()
	call_c   Dyam_Create_Binary(&ref[50],&ref[110],V(0))
	move_ret ref[113]
	c_ret

;; TERM 110: 'call_make_true_callret/3'(_B)
c_code local build_ref_110
	ret_reg &ref[110]
	call_c   build_ref_231()
	call_c   Dyam_Create_Unary(&ref[231],V(1))
	move_ret ref[110]
	c_ret

;; TERM 231: 'call_make_true_callret/3'
c_code local build_ref_231
	ret_reg &ref[231]
	call_c   Dyam_Create_Atom("call_make_true_callret/3")
	move_ret ref[231]
	c_ret

c_code local build_seed_25
	ret_reg &seed[25]
	call_c   build_ref_25()
	call_c   build_ref_116()
	call_c   Dyam_Seed_Start(&ref[25],&ref[116],I(0),fun8,1)
	call_c   build_ref_117()
	call_c   Dyam_Seed_Add_Comp(&ref[117],&ref[116],0)
	call_c   Dyam_Seed_End()
	move_ret seed[25]
	c_ret

;; TERM 117: '*GUARD*'(make_callret_with_pat(_G, _B, _C, _D)) :> '$$HOLE$$'
c_code local build_ref_117
	ret_reg &ref[117]
	call_c   build_ref_116()
	call_c   Dyam_Create_Binary(I(9),&ref[116],I(7))
	move_ret ref[117]
	c_ret

;; TERM 116: '*GUARD*'(make_callret_with_pat(_G, _B, _C, _D))
c_code local build_ref_116
	ret_reg &ref[116]
	call_c   build_ref_25()
	call_c   build_ref_115()
	call_c   Dyam_Create_Unary(&ref[25],&ref[115])
	move_ret ref[116]
	c_ret

;; TERM 115: make_callret_with_pat(_G, _B, _C, _D)
c_code local build_ref_115
	ret_reg &ref[115]
	call_c   build_ref_225()
	call_c   Dyam_Term_Start(&ref[225],4)
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[115]
	c_ret

c_code local build_seed_26
	ret_reg &seed[26]
	call_c   build_ref_0()
	call_c   build_ref_121()
	call_c   Dyam_Seed_Start(&ref[0],&ref[121],&ref[121],fun8,1)
	call_c   build_ref_122()
	call_c   Dyam_Seed_Add_Comp(&ref[122],&ref[121],0)
	call_c   Dyam_Seed_End()
	move_ret seed[26]
	c_ret

;; TERM 122: '*RITEM*'(_A, return(_C, _D)) :> '$$HOLE$$'
c_code local build_ref_122
	ret_reg &ref[122]
	call_c   build_ref_121()
	call_c   Dyam_Create_Binary(I(9),&ref[121],I(7))
	move_ret ref[122]
	c_ret

;; TERM 121: '*RITEM*'(_A, return(_C, _D))
c_code local build_ref_121
	ret_reg &ref[121]
	call_c   build_ref_0()
	call_c   build_ref_120()
	call_c   Dyam_Create_Binary(&ref[0],V(0),&ref[120])
	move_ret ref[121]
	c_ret

;; TERM 120: return(_C, _D)
c_code local build_ref_120
	ret_reg &ref[120]
	call_c   build_ref_80()
	call_c   Dyam_Create_Binary(&ref[80],V(2),V(3))
	move_ret ref[120]
	c_ret

long local pool_fun37[2]=[1,build_seed_26]

pl_code local fun38
	call_c   Dyam_Remove_Choice()
fun37:
	call_c   Dyam_Deallocate()
	pl_jump  fun10(&seed[26],2)


;; TERM 118: '*RITEM*'(_C, _D)
c_code local build_ref_118
	ret_reg &ref[118]
	call_c   build_ref_0()
	call_c   Dyam_Create_Binary(&ref[0],V(2),V(3))
	move_ret ref[118]
	c_ret

;; TERM 119: viewer(_I, _J)
c_code local build_ref_119
	ret_reg &ref[119]
	call_c   build_ref_232()
	call_c   Dyam_Create_Binary(&ref[232],V(8),V(9))
	move_ret ref[119]
	c_ret

;; TERM 232: viewer
c_code local build_ref_232
	ret_reg &ref[232]
	call_c   Dyam_Create_Atom("viewer")
	move_ret ref[232]
	c_ret

long local pool_fun39[5]=[65539,build_seed_25,build_ref_118,build_ref_119,pool_fun37]

long local pool_fun40[3]=[65537,build_ref_79,pool_fun39]

pl_code local fun40
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(6),&ref[79])
	fail_ret
fun39:
	pl_call  fun14(&seed[25],1)
	call_c   Dyam_Choice(fun38)
	call_c   DYAM_Numbervars_3(V(1),N(0),V(10))
	fail_ret
	call_c   Dyam_Unify(V(7),&ref[118])
	fail_ret
	call_c   Dyam_Reg_Load(0,V(7))
	move     V(8), R(2)
	move     S(5), R(3)
	pl_call  pred_label_term_2()
	call_c   Dyam_Reg_Load(0,V(1))
	move     V(9), R(2)
	move     S(5), R(3)
	pl_call  pred_label_term_2()
	call_c   DYAM_evpred_assert_1(&ref[119])
	pl_fail


;; TERM 123: call_pattern('*default*', _G)
c_code local build_ref_123
	ret_reg &ref[123]
	call_c   build_ref_233()
	call_c   build_ref_234()
	call_c   Dyam_Create_Binary(&ref[233],&ref[234],V(6))
	move_ret ref[123]
	c_ret

;; TERM 234: '*default*'
c_code local build_ref_234
	ret_reg &ref[234]
	call_c   Dyam_Create_Atom("*default*")
	move_ret ref[234]
	c_ret

;; TERM 233: call_pattern
c_code local build_ref_233
	ret_reg &ref[233]
	call_c   Dyam_Create_Atom("call_pattern")
	move_ret ref[233]
	c_ret

long local pool_fun41[4]=[131073,build_ref_123,pool_fun40,pool_fun39]

pl_code local fun41
	call_c   Dyam_Update_Choice(fun40)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[123])
	call_c   Dyam_Cut()
	pl_jump  fun39()

;; TERM 114: call_pattern((_E / _F), _G)
c_code local build_ref_114
	ret_reg &ref[114]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_235()
	call_c   Dyam_Create_Binary(&ref[235],V(4),V(5))
	move_ret R(0)
	call_c   build_ref_233()
	call_c   Dyam_Create_Binary(&ref[233],R(0),V(6))
	move_ret ref[114]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 235: /
c_code local build_ref_235
	ret_reg &ref[235]
	call_c   Dyam_Create_Atom("/")
	move_ret ref[235]
	c_ret

long local pool_fun42[5]=[131074,build_ref_113,build_ref_114,pool_fun41,pool_fun39]

pl_code local fun42
	call_c   Dyam_Pool(pool_fun42)
	call_c   Dyam_Unify_Item(&ref[113])
	fail_ret
	call_c   DYAM_evpred_functor(V(1),V(4),V(5))
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun41)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[114])
	call_c   Dyam_Cut()
	pl_jump  fun39()

;; TERM 112: '*FIRST*'('call_make_true_callret/3'(_B)) :> []
c_code local build_ref_112
	ret_reg &ref[112]
	call_c   build_ref_111()
	call_c   Dyam_Create_Binary(I(9),&ref[111],I(0))
	move_ret ref[112]
	c_ret

;; TERM 111: '*FIRST*'('call_make_true_callret/3'(_B))
c_code local build_ref_111
	ret_reg &ref[111]
	call_c   build_ref_47()
	call_c   build_ref_110()
	call_c   Dyam_Create_Unary(&ref[47],&ref[110])
	move_ret ref[111]
	c_ret

c_code local build_seed_9
	ret_reg &seed[9]
	call_c   build_ref_47()
	call_c   build_ref_126()
	call_c   Dyam_Seed_Start(&ref[47],&ref[126],I(0),fun12,1)
	call_c   build_ref_127()
	call_c   Dyam_Seed_Add_Comp(&ref[127],fun52,0)
	call_c   Dyam_Seed_End()
	move_ret seed[9]
	c_ret

;; TERM 127: '*CITEM*'('call_bmg_args/6'(_D, _E), _A)
c_code local build_ref_127
	ret_reg &ref[127]
	call_c   build_ref_50()
	call_c   build_ref_124()
	call_c   Dyam_Create_Binary(&ref[50],&ref[124],V(0))
	move_ret ref[127]
	c_ret

;; TERM 124: 'call_bmg_args/6'(_D, _E)
c_code local build_ref_124
	ret_reg &ref[124]
	call_c   build_ref_236()
	call_c   Dyam_Create_Binary(&ref[236],V(3),V(4))
	move_ret ref[124]
	c_ret

;; TERM 236: 'call_bmg_args/6'
c_code local build_ref_236
	ret_reg &ref[236]
	call_c   Dyam_Create_Atom("call_bmg_args/6")
	move_ret ref[236]
	c_ret

;; TERM 158: '$CLOSURE'('$fun'(51, 0, 1149456828), '$TUPPLE'(35101650979568))
c_code local build_ref_158
	ret_reg &ref[158]
	call_c   build_ref_155()
	call_c   Dyam_Closure_Aux(fun51,&ref[155])
	move_ret ref[158]
	c_ret

;; TERM 156: '$CLOSURE'('$fun'(50, 0, 1149446468), '$TUPPLE'(35101650979568))
c_code local build_ref_156
	ret_reg &ref[156]
	call_c   build_ref_155()
	call_c   Dyam_Closure_Aux(fun50,&ref[155])
	move_ret ref[156]
	c_ret

c_code local build_seed_29
	ret_reg &seed[29]
	call_c   build_ref_25()
	call_c   build_ref_136()
	call_c   Dyam_Seed_Start(&ref[25],&ref[136],I(0),fun8,1)
	call_c   build_ref_137()
	call_c   Dyam_Seed_Add_Comp(&ref[137],&ref[136],0)
	call_c   Dyam_Seed_End()
	move_ret seed[29]
	c_ret

;; TERM 137: '*GUARD*'(append(_D, _E, _H)) :> '$$HOLE$$'
c_code local build_ref_137
	ret_reg &ref[137]
	call_c   build_ref_136()
	call_c   Dyam_Create_Binary(I(9),&ref[136],I(7))
	move_ret ref[137]
	c_ret

;; TERM 136: '*GUARD*'(append(_D, _E, _H))
c_code local build_ref_136
	ret_reg &ref[136]
	call_c   build_ref_25()
	call_c   build_ref_135()
	call_c   Dyam_Create_Unary(&ref[25],&ref[135])
	move_ret ref[136]
	c_ret

;; TERM 135: append(_D, _E, _H)
c_code local build_ref_135
	ret_reg &ref[135]
	call_c   build_ref_237()
	call_c   Dyam_Term_Start(&ref[237],3)
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[135]
	c_ret

;; TERM 237: append
c_code local build_ref_237
	ret_reg &ref[237]
	call_c   Dyam_Create_Atom("append")
	move_ret ref[237]
	c_ret

c_code local build_seed_30
	ret_reg &seed[30]
	call_c   build_ref_25()
	call_c   build_ref_139()
	call_c   Dyam_Seed_Start(&ref[25],&ref[139],I(0),fun8,1)
	call_c   build_ref_140()
	call_c   Dyam_Seed_Add_Comp(&ref[140],&ref[139],0)
	call_c   Dyam_Seed_End()
	move_ret seed[30]
	c_ret

;; TERM 140: '*GUARD*'(append(_H, _F, _G)) :> '$$HOLE$$'
c_code local build_ref_140
	ret_reg &ref[140]
	call_c   build_ref_139()
	call_c   Dyam_Create_Binary(I(9),&ref[139],I(7))
	move_ret ref[140]
	c_ret

;; TERM 139: '*GUARD*'(append(_H, _F, _G))
c_code local build_ref_139
	ret_reg &ref[139]
	call_c   build_ref_25()
	call_c   build_ref_138()
	call_c   Dyam_Create_Unary(&ref[25],&ref[138])
	move_ret ref[139]
	c_ret

;; TERM 138: append(_H, _F, _G)
c_code local build_ref_138
	ret_reg &ref[138]
	call_c   build_ref_237()
	call_c   Dyam_Term_Start(&ref[237],3)
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[138]
	c_ret

c_code local build_seed_31
	ret_reg &seed[31]
	call_c   build_ref_0()
	call_c   build_ref_142()
	call_c   Dyam_Seed_Start(&ref[0],&ref[142],&ref[142],fun8,1)
	call_c   build_ref_143()
	call_c   Dyam_Seed_Add_Comp(&ref[143],&ref[142],0)
	call_c   Dyam_Seed_End()
	move_ret seed[31]
	c_ret

;; TERM 143: '*RITEM*'(_A, return(_B, _C, _F, [_B,_C|_G])) :> '$$HOLE$$'
c_code local build_ref_143
	ret_reg &ref[143]
	call_c   build_ref_142()
	call_c   Dyam_Create_Binary(I(9),&ref[142],I(7))
	move_ret ref[143]
	c_ret

;; TERM 142: '*RITEM*'(_A, return(_B, _C, _F, [_B,_C|_G]))
c_code local build_ref_142
	ret_reg &ref[142]
	call_c   build_ref_0()
	call_c   build_ref_141()
	call_c   Dyam_Create_Binary(&ref[0],V(0),&ref[141])
	move_ret ref[142]
	c_ret

;; TERM 141: return(_B, _C, _F, [_B,_C|_G])
c_code local build_ref_141
	ret_reg &ref[141]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Tupple(1,2,V(6))
	move_ret R(0)
	call_c   build_ref_80()
	call_c   Dyam_Term_Start(&ref[80],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_End()
	move_ret ref[141]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun50[4]=[3,build_seed_29,build_seed_30,build_seed_31]

pl_code local fun50
	call_c   Dyam_Pool(pool_fun50)
	call_c   Dyam_Allocate(0)
	pl_call  fun14(&seed[29],1)
	pl_call  fun14(&seed[30],1)
	call_c   Dyam_Deallocate()
	pl_jump  fun10(&seed[31],2)

;; TERM 155: '$TUPPLE'(35101650979568)
c_code local build_ref_155
	ret_reg &ref[155]
	call_c   Dyam_Create_Simple_Tupple(0,318767104)
	move_ret ref[155]
	c_ret

long local pool_fun51[2]=[1,build_ref_156]

pl_code local fun51
	call_c   Dyam_Pool(pool_fun51)
	call_c   Dyam_Allocate(0)
	move     &ref[156], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Load(4,V(4))
	call_c   Dyam_Reg_Deallocate(3)
fun45:
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Reg_Bind(2,V(3))
	call_c   Dyam_Reg_Bind(4,V(2))
	call_c   Dyam_Choice(fun44)
	pl_call  fun24(&seed[27],1)
	pl_fail


long local pool_fun52[3]=[2,build_ref_127,build_ref_158]

pl_code local fun52
	call_c   Dyam_Pool(pool_fun52)
	call_c   Dyam_Unify_Item(&ref[127])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[158], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Load(4,V(3))
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  fun45()

;; TERM 126: '*FIRST*'('call_bmg_args/6'(_D, _E)) :> []
c_code local build_ref_126
	ret_reg &ref[126]
	call_c   build_ref_125()
	call_c   Dyam_Create_Binary(I(9),&ref[125],I(0))
	move_ret ref[126]
	c_ret

;; TERM 125: '*FIRST*'('call_bmg_args/6'(_D, _E))
c_code local build_ref_125
	ret_reg &ref[125]
	call_c   build_ref_47()
	call_c   build_ref_124()
	call_c   Dyam_Create_Unary(&ref[47],&ref[124])
	move_ret ref[125]
	c_ret

c_code local build_seed_15
	ret_reg &seed[15]
	call_c   build_ref_24()
	call_c   build_ref_146()
	call_c   Dyam_Seed_Start(&ref[24],&ref[146],I(0),fun1,1)
	call_c   build_ref_145()
	call_c   Dyam_Seed_Add_Comp(&ref[145],fun49,0)
	call_c   Dyam_Seed_End()
	move_ret seed[15]
	c_ret

;; TERM 145: '*GUARD*'(make_callret_with_pat(_B, _C, _D, _E))
c_code local build_ref_145
	ret_reg &ref[145]
	call_c   build_ref_25()
	call_c   build_ref_144()
	call_c   Dyam_Create_Unary(&ref[25],&ref[144])
	move_ret ref[145]
	c_ret

;; TERM 144: make_callret_with_pat(_B, _C, _D, _E)
c_code local build_ref_144
	ret_reg &ref[144]
	call_c   build_ref_225()
	call_c   Dyam_Term_Start(&ref[225],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[144]
	c_ret

pl_code local fun47
	call_c   Dyam_Remove_Choice()
	pl_fail

;; TERM 147: [_F|_G]
c_code local build_ref_147
	ret_reg &ref[147]
	call_c   Dyam_Create_List(V(5),V(6))
	move_ret ref[147]
	c_ret

;; TERM 148: [_H|_I]
c_code local build_ref_148
	ret_reg &ref[148]
	call_c   Dyam_Create_List(V(7),V(8))
	move_ret ref[148]
	c_ret

c_code local build_seed_32
	ret_reg &seed[32]
	call_c   build_ref_25()
	call_c   build_ref_150()
	call_c   Dyam_Seed_Start(&ref[25],&ref[150],I(0),fun8,1)
	call_c   build_ref_151()
	call_c   Dyam_Seed_Add_Comp(&ref[151],&ref[150],0)
	call_c   Dyam_Seed_End()
	move_ret seed[32]
	c_ret

;; TERM 151: '*GUARD*'(make_callret_arg(_G, _I, _N, _O)) :> '$$HOLE$$'
c_code local build_ref_151
	ret_reg &ref[151]
	call_c   build_ref_150()
	call_c   Dyam_Create_Binary(I(9),&ref[150],I(7))
	move_ret ref[151]
	c_ret

;; TERM 150: '*GUARD*'(make_callret_arg(_G, _I, _N, _O))
c_code local build_ref_150
	ret_reg &ref[150]
	call_c   build_ref_25()
	call_c   build_ref_149()
	call_c   Dyam_Create_Unary(&ref[25],&ref[149])
	move_ret ref[150]
	c_ret

;; TERM 149: make_callret_arg(_G, _I, _N, _O)
c_code local build_ref_149
	ret_reg &ref[149]
	call_c   build_ref_224()
	call_c   Dyam_Term_Start(&ref[224],4)
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_End()
	move_ret ref[149]
	c_ret

;; TERM 152: [_M|_O]
c_code local build_ref_152
	ret_reg &ref[152]
	call_c   Dyam_Create_List(V(12),V(14))
	move_ret ref[152]
	c_ret

;; TERM 153: [_L|_N]
c_code local build_ref_153
	ret_reg &ref[153]
	call_c   Dyam_Create_List(V(11),V(13))
	move_ret ref[153]
	c_ret

long local pool_fun46[5]=[4,build_ref_148,build_seed_32,build_ref_152,build_ref_153]

long local pool_fun48[3]=[65537,build_ref_147,pool_fun46]

pl_code local fun48
	call_c   Dyam_Update_Choice(fun47)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_compound(V(1))
	fail_ret
	call_c   Dyam_Cut()
	call_c   DYAM_evpred_univ(V(1),&ref[147])
	fail_ret
fun46:
	call_c   DYAM_evpred_univ(V(2),&ref[148])
	fail_ret
	call_c   DYAM_evpred_functor(V(2),V(9),V(10))
	fail_ret
	call_c   Dyam_Reg_Load(0,V(5))
	call_c   Dyam_Reg_Load(2,V(9))
	call_c   Dyam_Reg_Load(4,V(10))
	move     V(11), R(6)
	move     S(5), R(7)
	move     V(12), R(8)
	move     S(5), R(9)
	pl_call  pred_make_callret_functor_5()
	pl_call  fun14(&seed[32],1)
	call_c   DYAM_evpred_univ(V(4),&ref[152])
	fail_ret
	call_c   DYAM_evpred_univ(V(3),&ref[153])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret


long local pool_fun49[5]=[131074,build_ref_145,build_ref_147,pool_fun48,pool_fun46]

pl_code local fun49
	call_c   Dyam_Pool(pool_fun49)
	call_c   Dyam_Unify_Item(&ref[145])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun48)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[147])
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun46()

;; TERM 146: '*GUARD*'(make_callret_with_pat(_B, _C, _D, _E)) :> []
c_code local build_ref_146
	ret_reg &ref[146]
	call_c   build_ref_145()
	call_c   Dyam_Create_Binary(I(9),&ref[145],I(0))
	move_ret ref[146]
	c_ret

c_code local build_seed_10
	ret_reg &seed[10]
	call_c   build_ref_47()
	call_c   build_ref_161()
	call_c   Dyam_Seed_Start(&ref[47],&ref[161],I(0),fun12,1)
	call_c   build_ref_162()
	call_c   Dyam_Seed_Add_Comp(&ref[162],fun71,0)
	call_c   Dyam_Seed_End()
	move_ret seed[10]
	c_ret

;; TERM 162: '*CITEM*'('call_make_true_dcg_callret/7'(_B), _A)
c_code local build_ref_162
	ret_reg &ref[162]
	call_c   build_ref_50()
	call_c   build_ref_159()
	call_c   Dyam_Create_Binary(&ref[50],&ref[159],V(0))
	move_ret ref[162]
	c_ret

;; TERM 159: 'call_make_true_dcg_callret/7'(_B)
c_code local build_ref_159
	ret_reg &ref[159]
	call_c   build_ref_238()
	call_c   Dyam_Create_Unary(&ref[238],V(1))
	move_ret ref[159]
	c_ret

;; TERM 238: 'call_make_true_dcg_callret/7'
c_code local build_ref_238
	ret_reg &ref[238]
	call_c   Dyam_Create_Atom("call_make_true_dcg_callret/7")
	move_ret ref[238]
	c_ret

;; TERM 206: '$CLOSURE'('$fun'(67, 0, 1149657328), '$TUPPLE'(35101650980460))
c_code local build_ref_206
	ret_reg &ref[206]
	call_c   build_ref_205()
	call_c   Dyam_Closure_Aux(fun67,&ref[205])
	move_ret ref[206]
	c_ret

;; TERM 202: '$CLOSURE'('$fun'(64, 0, 1149645496), '$TUPPLE'(35101650980352))
c_code local build_ref_202
	ret_reg &ref[202]
	call_c   build_ref_201()
	call_c   Dyam_Closure_Aux(fun64,&ref[201])
	move_ret ref[202]
	c_ret

;; TERM 199: [_K|_R]
c_code local build_ref_199
	ret_reg &ref[199]
	call_c   Dyam_Create_List(V(10),V(17))
	move_ret ref[199]
	c_ret

;; TERM 180: [_T|_U]
c_code local build_ref_180
	ret_reg &ref[180]
	call_c   Dyam_Create_List(V(19),V(20))
	move_ret ref[180]
	c_ret

;; TERM 193: '$CLOSURE'('$fun'(61, 0, 1149552336), '$TUPPLE'(35101650980044))
c_code local build_ref_193
	ret_reg &ref[193]
	call_c   build_ref_192()
	call_c   Dyam_Closure_Aux(fun61,&ref[192])
	move_ret ref[193]
	c_ret

;; TERM 181: [_T|_V]
c_code local build_ref_181
	ret_reg &ref[181]
	call_c   Dyam_Create_List(V(19),V(21))
	move_ret ref[181]
	c_ret

c_code local build_seed_37
	ret_reg &seed[37]
	call_c   build_ref_25()
	call_c   build_ref_183()
	call_c   Dyam_Seed_Start(&ref[25],&ref[183],I(0),fun8,1)
	call_c   build_ref_184()
	call_c   Dyam_Seed_Add_Comp(&ref[184],&ref[183],0)
	call_c   Dyam_Seed_End()
	move_ret seed[37]
	c_ret

;; TERM 184: '*GUARD*'(make_callret_with_pat(_S, _W, _G, _H)) :> '$$HOLE$$'
c_code local build_ref_184
	ret_reg &ref[184]
	call_c   build_ref_183()
	call_c   Dyam_Create_Binary(I(9),&ref[183],I(7))
	move_ret ref[184]
	c_ret

;; TERM 183: '*GUARD*'(make_callret_with_pat(_S, _W, _G, _H))
c_code local build_ref_183
	ret_reg &ref[183]
	call_c   build_ref_25()
	call_c   build_ref_182()
	call_c   Dyam_Create_Unary(&ref[25],&ref[182])
	move_ret ref[183]
	c_ret

;; TERM 182: make_callret_with_pat(_S, _W, _G, _H)
c_code local build_ref_182
	ret_reg &ref[182]
	call_c   build_ref_225()
	call_c   Dyam_Term_Start(&ref[225],4)
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(22))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[182]
	c_ret

c_code local build_seed_38
	ret_reg &seed[38]
	call_c   build_ref_0()
	call_c   build_ref_189()
	call_c   Dyam_Seed_Start(&ref[0],&ref[189],&ref[189],fun8,1)
	call_c   build_ref_190()
	call_c   Dyam_Seed_Add_Comp(&ref[190],&ref[189],0)
	call_c   Dyam_Seed_End()
	move_ret seed[38]
	c_ret

;; TERM 190: '*RITEM*'(_A, return(_C, _D, _E, _F, _G, _H)) :> '$$HOLE$$'
c_code local build_ref_190
	ret_reg &ref[190]
	call_c   build_ref_189()
	call_c   Dyam_Create_Binary(I(9),&ref[189],I(7))
	move_ret ref[190]
	c_ret

;; TERM 189: '*RITEM*'(_A, return(_C, _D, _E, _F, _G, _H))
c_code local build_ref_189
	ret_reg &ref[189]
	call_c   build_ref_0()
	call_c   build_ref_188()
	call_c   Dyam_Create_Binary(&ref[0],V(0),&ref[188])
	move_ret ref[189]
	c_ret

;; TERM 188: return(_C, _D, _E, _F, _G, _H)
c_code local build_ref_188
	ret_reg &ref[188]
	call_c   build_ref_80()
	call_c   Dyam_Term_Start(&ref[80],6)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[188]
	c_ret

long local pool_fun59[2]=[1,build_seed_38]

pl_code local fun60
	call_c   Dyam_Remove_Choice()
fun59:
	call_c   Dyam_Deallocate()
	pl_jump  fun10(&seed[38],2)


;; TERM 185: '*RITEM*'(_G, _H)
c_code local build_ref_185
	ret_reg &ref[185]
	call_c   build_ref_0()
	call_c   Dyam_Create_Binary(&ref[0],V(6),V(7))
	move_ret ref[185]
	c_ret

;; TERM 186: _B(_C,_D)
c_code local build_ref_186
	ret_reg &ref[186]
	call_c   Dyam_Term_Start(I(3),3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[186]
	c_ret

;; TERM 187: viewer(_Y, _Z)
c_code local build_ref_187
	ret_reg &ref[187]
	call_c   build_ref_232()
	call_c   Dyam_Create_Binary(&ref[232],V(24),V(25))
	move_ret ref[187]
	c_ret

long local pool_fun61[7]=[65541,build_ref_181,build_seed_37,build_ref_185,build_ref_186,build_ref_187,pool_fun59]

pl_code local fun61
	call_c   Dyam_Pool(pool_fun61)
	call_c   DYAM_evpred_univ(V(22),&ref[181])
	fail_ret
	call_c   Dyam_Allocate(0)
	pl_call  fun14(&seed[37],1)
	call_c   Dyam_Choice(fun60)
	call_c   DYAM_Numbervars_3(V(22),N(0),V(26))
	fail_ret
	call_c   Dyam_Unify(V(23),&ref[185])
	fail_ret
	call_c   Dyam_Reg_Load(0,V(23))
	move     V(24), R(2)
	move     S(5), R(3)
	pl_call  pred_label_term_2()
	move     &ref[186], R(0)
	move     S(5), R(1)
	move     V(25), R(2)
	move     S(5), R(3)
	pl_call  pred_label_term_2()
	call_c   DYAM_evpred_assert_1(&ref[187])
	pl_fail

;; TERM 192: '$TUPPLE'(35101650980044)
c_code local build_ref_192
	ret_reg &ref[192]
	call_c   Dyam_Create_Simple_Tupple(0,528483968)
	move_ret ref[192]
	c_ret

;; TERM 194: bmg_args(_C, _D, _E, _F, _U, _V)
c_code local build_ref_194
	ret_reg &ref[194]
	call_c   build_ref_239()
	call_c   Dyam_Term_Start(&ref[239],6)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_Arg(V(21))
	call_c   Dyam_Term_End()
	move_ret ref[194]
	c_ret

;; TERM 239: bmg_args
c_code local build_ref_239
	ret_reg &ref[239]
	call_c   Dyam_Create_Atom("bmg_args")
	move_ret ref[239]
	c_ret

long local pool_fun62[4]=[3,build_ref_180,build_ref_193,build_ref_194]

long local pool_fun64[3]=[65537,build_ref_199,pool_fun62]

pl_code local fun64
	call_c   Dyam_Pool(pool_fun64)
	call_c   Dyam_Unify(V(18),&ref[199])
	fail_ret
	call_c   Dyam_Allocate(0)
fun62:
	call_c   DYAM_evpred_univ(V(1),&ref[180])
	fail_ret
	move     &ref[193], R(0)
	move     S(5), R(1)
	move     &ref[194], R(2)
	move     S(5), R(3)
	move     I(0), R(4)
	move     0, R(5)
	call_c   Dyam_Reg_Deallocate(3)
fun58:
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Reg_Unify(2,&ref[178])
	fail_ret
	call_c   Dyam_Reg_Bind(4,V(8))
	call_c   Dyam_Choice(fun57)
	pl_call  fun24(&seed[35],1)
	pl_fail



;; TERM 201: '$TUPPLE'(35101650980352)
c_code local build_ref_201
	ret_reg &ref[201]
	call_c   Dyam_Create_Simple_Tupple(0,402917376)
	move_ret ref[201]
	c_ret

;; TERM 203: bmg_args(_L, _M, _N, _O, _K, _R)
c_code local build_ref_203
	ret_reg &ref[203]
	call_c   build_ref_239()
	call_c   Dyam_Term_Start(&ref[239],6)
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_End()
	move_ret ref[203]
	c_ret

long local pool_fun65[3]=[2,build_ref_202,build_ref_203]

pl_code local fun65
	call_c   Dyam_Remove_Choice()
	move     &ref[202], R(0)
	move     S(5), R(1)
	move     &ref[203], R(2)
	move     S(5), R(3)
	move     I(0), R(4)
	move     0, R(5)
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  fun58()

;; TERM 169: [_P|_Q]
c_code local build_ref_169
	ret_reg &ref[169]
	call_c   Dyam_Create_List(V(15),V(16))
	move_ret ref[169]
	c_ret

;; TERM 197: '$CLOSURE'('$fun'(63, 0, 1149563132), '$TUPPLE'(35101650980144))
c_code local build_ref_197
	ret_reg &ref[197]
	call_c   build_ref_196()
	call_c   Dyam_Closure_Aux(fun63,&ref[196])
	move_ret ref[197]
	c_ret

;; TERM 179: [_P|_R]
c_code local build_ref_179
	ret_reg &ref[179]
	call_c   Dyam_Create_List(V(15),V(17))
	move_ret ref[179]
	c_ret

long local pool_fun63[3]=[65537,build_ref_179,pool_fun62]

pl_code local fun63
	call_c   Dyam_Pool(pool_fun63)
	call_c   Dyam_Unify(V(18),&ref[179])
	fail_ret
	call_c   Dyam_Allocate(0)
	pl_jump  fun62()

;; TERM 196: '$TUPPLE'(35101650980144)
c_code local build_ref_196
	ret_reg &ref[196]
	call_c   Dyam_Create_Simple_Tupple(0,402663424)
	move_ret ref[196]
	c_ret

;; TERM 198: bmg_args(_L, _M, _N, _O, _Q, _R)
c_code local build_ref_198
	ret_reg &ref[198]
	call_c   build_ref_239()
	call_c   Dyam_Term_Start(&ref[239],6)
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_End()
	move_ret ref[198]
	c_ret

long local pool_fun66[5]=[65539,build_ref_169,build_ref_197,build_ref_198,pool_fun65]

pl_code local fun66
	call_c   Dyam_Update_Choice(fun65)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_compound(V(10))
	fail_ret
	call_c   Dyam_Cut()
	call_c   DYAM_evpred_univ(V(10),&ref[169])
	fail_ret
	move     &ref[197], R(0)
	move     S(5), R(1)
	move     &ref[198], R(2)
	move     S(5), R(3)
	move     I(0), R(4)
	move     0, R(5)
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  fun58()

long local pool_fun67[5]=[65539,build_ref_169,build_ref_197,build_ref_198,pool_fun66]

pl_code local fun67
	call_c   Dyam_Pool(pool_fun67)
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun66)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(10),&ref[169])
	fail_ret
	call_c   Dyam_Cut()
	move     &ref[197], R(0)
	move     S(5), R(1)
	move     &ref[198], R(2)
	move     S(5), R(3)
	move     I(0), R(4)
	move     0, R(5)
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  fun58()

;; TERM 205: '$TUPPLE'(35101650980460)
c_code local build_ref_205
	ret_reg &ref[205]
	call_c   Dyam_Create_Simple_Tupple(0,403161088)
	move_ret ref[205]
	c_ret

long local pool_fun68[2]=[1,build_ref_206]

long local pool_fun69[4]=[65538,build_ref_79,build_ref_77,pool_fun68]

pl_code local fun69
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(10),&ref[79])
	fail_ret
	call_c   Dyam_Unify(V(11),&ref[79])
	fail_ret
	call_c   Dyam_Unify(V(12),&ref[77])
	fail_ret
fun68:
	move     &ref[206], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	move     V(13), R(4)
	move     S(5), R(5)
	move     V(14), R(6)
	move     S(5), R(7)
	call_c   Dyam_Reg_Deallocate(4)
fun55:
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Reg_Bind(2,V(4))
	call_c   Dyam_Multi_Reg_Bind(4,2,2)
	call_c   Dyam_Choice(fun54)
	pl_call  fun24(&seed[33],1)
	pl_fail



;; TERM 207: call_dcg_pattern('*default*', _K, _L, _M)
c_code local build_ref_207
	ret_reg &ref[207]
	call_c   build_ref_240()
	call_c   build_ref_234()
	call_c   Dyam_Term_Start(&ref[240],4)
	call_c   Dyam_Term_Arg(&ref[234])
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_End()
	move_ret ref[207]
	c_ret

;; TERM 240: call_dcg_pattern
c_code local build_ref_240
	ret_reg &ref[240]
	call_c   Dyam_Create_Atom("call_dcg_pattern")
	move_ret ref[240]
	c_ret

long local pool_fun70[4]=[131073,build_ref_207,pool_fun69,pool_fun68]

pl_code local fun70
	call_c   Dyam_Update_Choice(fun69)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[207])
	call_c   Dyam_Cut()
	pl_jump  fun68()

;; TERM 163: call_dcg_pattern((_I / _J), _K, _L, _M)
c_code local build_ref_163
	ret_reg &ref[163]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_235()
	call_c   Dyam_Create_Binary(&ref[235],V(8),V(9))
	move_ret R(0)
	call_c   build_ref_240()
	call_c   Dyam_Term_Start(&ref[240],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_End()
	move_ret ref[163]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun71[5]=[131074,build_ref_162,build_ref_163,pool_fun70,pool_fun68]

pl_code local fun71
	call_c   Dyam_Pool(pool_fun71)
	call_c   Dyam_Unify_Item(&ref[162])
	fail_ret
	call_c   DYAM_evpred_functor(V(1),V(8),V(9))
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun70)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[163])
	call_c   Dyam_Cut()
	pl_jump  fun68()

;; TERM 161: '*FIRST*'('call_make_true_dcg_callret/7'(_B)) :> []
c_code local build_ref_161
	ret_reg &ref[161]
	call_c   build_ref_160()
	call_c   Dyam_Create_Binary(I(9),&ref[160],I(0))
	move_ret ref[161]
	c_ret

;; TERM 160: '*FIRST*'('call_make_true_dcg_callret/7'(_B))
c_code local build_ref_160
	ret_reg &ref[160]
	call_c   build_ref_47()
	call_c   build_ref_159()
	call_c   Dyam_Create_Unary(&ref[47],&ref[159])
	move_ret ref[160]
	c_ret

c_code local build_seed_11
	ret_reg &seed[11]
	call_c   build_ref_24()
	call_c   build_ref_210()
	call_c   Dyam_Seed_Start(&ref[24],&ref[210],I(0),fun1,1)
	call_c   build_ref_209()
	call_c   Dyam_Seed_Add_Comp(&ref[209],fun76,0)
	call_c   Dyam_Seed_End()
	move_ret seed[11]
	c_ret

;; TERM 209: '*GUARD*'(make_dcg_callret(_B, _C, _D, _E, _F, _G, _H))
c_code local build_ref_209
	ret_reg &ref[209]
	call_c   build_ref_25()
	call_c   build_ref_208()
	call_c   Dyam_Create_Unary(&ref[25],&ref[208])
	move_ret ref[209]
	c_ret

;; TERM 208: make_dcg_callret(_B, _C, _D, _E, _F, _G, _H)
c_code local build_ref_208
	ret_reg &ref[208]
	call_c   build_ref_241()
	call_c   Dyam_Term_Start(&ref[241],7)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[208]
	c_ret

;; TERM 241: make_dcg_callret
c_code local build_ref_241
	ret_reg &ref[241]
	call_c   Dyam_Create_Atom("make_dcg_callret")
	move_ret ref[241]
	c_ret

;; TERM 222: '$CLOSURE'('$fun'(75, 0, 1149724924), '$TUPPLE'(35101650980736))
c_code local build_ref_222
	ret_reg &ref[222]
	call_c   build_ref_221()
	call_c   Dyam_Closure_Aux(fun75,&ref[221])
	move_ret ref[222]
	c_ret

pl_code local fun75
	call_c   Dyam_Unify(V(1),V(10))
	fail_ret
	call_c   Dyam_Unify(V(2),V(11))
	fail_ret
	call_c   Dyam_Unify(V(3),V(12))
	fail_ret
	call_c   Dyam_Unify(V(4),V(13))
	fail_ret
	call_c   Dyam_Unify(V(5),V(14))
	fail_ret
	pl_ret

;; TERM 221: '$TUPPLE'(35101650980736)
c_code local build_ref_221
	ret_reg &ref[221]
	call_c   Dyam_Create_Simple_Tupple(0,260554752)
	move_ret ref[221]
	c_ret

;; TERM 223: make_true_dcg_callret(_K, _L, _M, _N, _O, _G, _H)
c_code local build_ref_223
	ret_reg &ref[223]
	call_c   build_ref_242()
	call_c   Dyam_Term_Start(&ref[242],7)
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[223]
	c_ret

;; TERM 242: make_true_dcg_callret
c_code local build_ref_242
	ret_reg &ref[242]
	call_c   Dyam_Create_Atom("make_true_dcg_callret")
	move_ret ref[242]
	c_ret

long local pool_fun76[4]=[3,build_ref_209,build_ref_222,build_ref_223]

pl_code local fun76
	call_c   Dyam_Pool(pool_fun76)
	call_c   Dyam_Unify_Item(&ref[209])
	fail_ret
	call_c   DYAM_evpred_functor(V(1),V(8),V(9))
	fail_ret
	call_c   DYAM_evpred_functor(V(10),V(8),V(9))
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[222], R(0)
	move     S(5), R(1)
	move     &ref[223], R(2)
	move     S(5), R(3)
	move     I(0), R(4)
	move     0, R(5)
	call_c   Dyam_Reg_Deallocate(3)
fun74:
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Reg_Unify(2,&ref[219])
	fail_ret
	call_c   Dyam_Reg_Bind(4,V(9))
	call_c   Dyam_Choice(fun73)
	pl_call  fun24(&seed[39],1)
	pl_fail


;; TERM 210: '*GUARD*'(make_dcg_callret(_B, _C, _D, _E, _F, _G, _H)) :> []
c_code local build_ref_210
	ret_reg &ref[210]
	call_c   build_ref_209()
	call_c   Dyam_Create_Binary(I(9),&ref[209],I(0))
	move_ret ref[210]
	c_ret

c_code local build_seed_39
	ret_reg &seed[39]
	call_c   build_ref_50()
	call_c   build_ref_212()
	call_c   Dyam_Seed_Start(&ref[50],&ref[212],&ref[212],fun8,1)
	call_c   build_ref_214()
	call_c   Dyam_Seed_Add_Comp(&ref[214],&ref[212],0)
	call_c   Dyam_Seed_End()
	move_ret seed[39]
	c_ret

;; TERM 214: '*FIRST*'('call_make_true_dcg_callret/7'(_C)) :> '$$HOLE$$'
c_code local build_ref_214
	ret_reg &ref[214]
	call_c   build_ref_213()
	call_c   Dyam_Create_Binary(I(9),&ref[213],I(7))
	move_ret ref[214]
	c_ret

;; TERM 213: '*FIRST*'('call_make_true_dcg_callret/7'(_C))
c_code local build_ref_213
	ret_reg &ref[213]
	call_c   build_ref_47()
	call_c   build_ref_211()
	call_c   Dyam_Create_Unary(&ref[47],&ref[211])
	move_ret ref[213]
	c_ret

;; TERM 211: 'call_make_true_dcg_callret/7'(_C)
c_code local build_ref_211
	ret_reg &ref[211]
	call_c   build_ref_238()
	call_c   Dyam_Create_Unary(&ref[238],V(2))
	move_ret ref[211]
	c_ret

;; TERM 212: '*CITEM*'('call_make_true_dcg_callret/7'(_C), 'call_make_true_dcg_callret/7'(_C))
c_code local build_ref_212
	ret_reg &ref[212]
	call_c   build_ref_50()
	call_c   build_ref_211()
	call_c   Dyam_Create_Binary(&ref[50],&ref[211],&ref[211])
	move_ret ref[212]
	c_ret

c_code local build_seed_40
	ret_reg &seed[40]
	call_c   build_ref_83()
	call_c   build_ref_218()
	call_c   Dyam_Seed_Start(&ref[83],&ref[218],I(0),fun12,1)
	call_c   build_ref_216()
	call_c   Dyam_Seed_Add_Comp(&ref[216],fun72,0)
	call_c   Dyam_Seed_End()
	move_ret seed[40]
	c_ret

;; TERM 216: '*RITEM*'('call_make_true_dcg_callret/7'(_C), return(_D, _E, _F, _G, _H, _I))
c_code local build_ref_216
	ret_reg &ref[216]
	call_c   build_ref_0()
	call_c   build_ref_211()
	call_c   build_ref_215()
	call_c   Dyam_Create_Binary(&ref[0],&ref[211],&ref[215])
	move_ret ref[216]
	c_ret

;; TERM 215: return(_D, _E, _F, _G, _H, _I)
c_code local build_ref_215
	ret_reg &ref[215]
	call_c   build_ref_80()
	call_c   Dyam_Term_Start(&ref[80],6)
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_End()
	move_ret ref[215]
	c_ret

pl_code local fun72
	call_c   build_ref_216()
	call_c   Dyam_Unify_Item(&ref[216])
	fail_ret
	pl_jump  Follow_Cont(V(1))

;; TERM 218: '*RITEM*'('call_make_true_dcg_callret/7'(_C), return(_D, _E, _F, _G, _H, _I)) :> maker(5, '$TUPPLE'(35101651764716))
c_code local build_ref_218
	ret_reg &ref[218]
	call_c   build_ref_216()
	call_c   build_ref_217()
	call_c   Dyam_Create_Binary(I(9),&ref[216],&ref[217])
	move_ret ref[218]
	c_ret

;; TERM 217: maker(5, '$TUPPLE'(35101651764716))
c_code local build_ref_217
	ret_reg &ref[217]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,134217728)
	move_ret R(0)
	call_c   build_ref_243()
	call_c   Dyam_Create_Binary(&ref[243],N(5),R(0))
	move_ret ref[217]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 243: maker
c_code local build_ref_243
	ret_reg &ref[243]
	call_c   Dyam_Create_Atom("maker")
	move_ret ref[243]
	c_ret

;; TERM 83: '*CURNEXT*'
c_code local build_ref_83
	ret_reg &ref[83]
	call_c   Dyam_Create_Atom("*CURNEXT*")
	move_ret ref[83]
	c_ret

c_code local build_seed_35
	ret_reg &seed[35]
	call_c   build_ref_50()
	call_c   build_ref_171()
	call_c   Dyam_Seed_Start(&ref[50],&ref[171],&ref[171],fun8,1)
	call_c   build_ref_173()
	call_c   Dyam_Seed_Add_Comp(&ref[173],&ref[171],0)
	call_c   Dyam_Seed_End()
	move_ret seed[35]
	c_ret

;; TERM 173: '*FIRST*'('call_bmg_args/6'(_E, _F)) :> '$$HOLE$$'
c_code local build_ref_173
	ret_reg &ref[173]
	call_c   build_ref_172()
	call_c   Dyam_Create_Binary(I(9),&ref[172],I(7))
	move_ret ref[173]
	c_ret

;; TERM 172: '*FIRST*'('call_bmg_args/6'(_E, _F))
c_code local build_ref_172
	ret_reg &ref[172]
	call_c   build_ref_47()
	call_c   build_ref_170()
	call_c   Dyam_Create_Unary(&ref[47],&ref[170])
	move_ret ref[172]
	c_ret

;; TERM 170: 'call_bmg_args/6'(_E, _F)
c_code local build_ref_170
	ret_reg &ref[170]
	call_c   build_ref_236()
	call_c   Dyam_Create_Binary(&ref[236],V(4),V(5))
	move_ret ref[170]
	c_ret

;; TERM 171: '*CITEM*'('call_bmg_args/6'(_E, _F), 'call_bmg_args/6'(_E, _F))
c_code local build_ref_171
	ret_reg &ref[171]
	call_c   build_ref_50()
	call_c   build_ref_170()
	call_c   Dyam_Create_Binary(&ref[50],&ref[170],&ref[170])
	move_ret ref[171]
	c_ret

c_code local build_seed_36
	ret_reg &seed[36]
	call_c   build_ref_83()
	call_c   build_ref_177()
	call_c   Dyam_Seed_Start(&ref[83],&ref[177],I(0),fun12,1)
	call_c   build_ref_175()
	call_c   Dyam_Seed_Add_Comp(&ref[175],fun56,0)
	call_c   Dyam_Seed_End()
	move_ret seed[36]
	c_ret

;; TERM 175: '*RITEM*'('call_bmg_args/6'(_E, _F), return(_C, _D, _G, _H))
c_code local build_ref_175
	ret_reg &ref[175]
	call_c   build_ref_0()
	call_c   build_ref_170()
	call_c   build_ref_174()
	call_c   Dyam_Create_Binary(&ref[0],&ref[170],&ref[174])
	move_ret ref[175]
	c_ret

;; TERM 174: return(_C, _D, _G, _H)
c_code local build_ref_174
	ret_reg &ref[174]
	call_c   build_ref_80()
	call_c   Dyam_Term_Start(&ref[80],4)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[174]
	c_ret

pl_code local fun56
	call_c   build_ref_175()
	call_c   Dyam_Unify_Item(&ref[175])
	fail_ret
	pl_jump  Follow_Cont(V(1))

;; TERM 177: '*RITEM*'('call_bmg_args/6'(_E, _F), return(_C, _D, _G, _H)) :> maker(4, '$TUPPLE'(35101651764716))
c_code local build_ref_177
	ret_reg &ref[177]
	call_c   build_ref_175()
	call_c   build_ref_176()
	call_c   Dyam_Create_Binary(I(9),&ref[175],&ref[176])
	move_ret ref[177]
	c_ret

;; TERM 176: maker(4, '$TUPPLE'(35101651764716))
c_code local build_ref_176
	ret_reg &ref[176]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,134217728)
	move_ret R(0)
	call_c   build_ref_243()
	call_c   Dyam_Create_Binary(&ref[243],N(4),R(0))
	move_ret ref[176]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_33
	ret_reg &seed[33]
	call_c   build_ref_50()
	call_c   build_ref_164()
	call_c   Dyam_Seed_Start(&ref[50],&ref[164],&ref[164],fun8,1)
	call_c   build_ref_165()
	call_c   Dyam_Seed_Add_Comp(&ref[165],&ref[164],0)
	call_c   Dyam_Seed_End()
	move_ret seed[33]
	c_ret

;; TERM 165: '*FIRST*'('call_bmg_patterns/2') :> '$$HOLE$$'
c_code local build_ref_165
	ret_reg &ref[165]
	call_c   build_ref_48()
	call_c   Dyam_Create_Binary(I(9),&ref[48],I(7))
	move_ret ref[165]
	c_ret

;; TERM 164: '*CITEM*'('call_bmg_patterns/2', 'call_bmg_patterns/2')
c_code local build_ref_164
	ret_reg &ref[164]
	call_c   build_ref_50()
	call_c   build_ref_1()
	call_c   Dyam_Create_Binary(&ref[50],&ref[1],&ref[1])
	move_ret ref[164]
	c_ret

c_code local build_seed_34
	ret_reg &seed[34]
	call_c   build_ref_83()
	call_c   build_ref_168()
	call_c   Dyam_Seed_Start(&ref[83],&ref[168],I(0),fun12,1)
	call_c   build_ref_166()
	call_c   Dyam_Seed_Add_Comp(&ref[166],fun53,0)
	call_c   Dyam_Seed_End()
	move_ret seed[34]
	c_ret

;; TERM 166: '*RITEM*'('call_bmg_patterns/2', return(_C, _D))
c_code local build_ref_166
	ret_reg &ref[166]
	call_c   build_ref_0()
	call_c   build_ref_1()
	call_c   build_ref_120()
	call_c   Dyam_Create_Binary(&ref[0],&ref[1],&ref[120])
	move_ret ref[166]
	c_ret

pl_code local fun53
	call_c   build_ref_166()
	call_c   Dyam_Unify_Item(&ref[166])
	fail_ret
	pl_jump  Follow_Cont(V(1))

;; TERM 168: '*RITEM*'('call_bmg_patterns/2', return(_C, _D)) :> maker(3, '$TUPPLE'(35101651764716))
c_code local build_ref_168
	ret_reg &ref[168]
	call_c   build_ref_166()
	call_c   build_ref_167()
	call_c   Dyam_Create_Binary(I(9),&ref[166],&ref[167])
	move_ret ref[168]
	c_ret

;; TERM 167: maker(3, '$TUPPLE'(35101651764716))
c_code local build_ref_167
	ret_reg &ref[167]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,134217728)
	move_ret R(0)
	call_c   build_ref_243()
	call_c   Dyam_Create_Binary(&ref[243],N(3),R(0))
	move_ret ref[167]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_27
	ret_reg &seed[27]
	call_c   build_ref_50()
	call_c   build_ref_128()
	call_c   Dyam_Seed_Start(&ref[50],&ref[128],&ref[128],fun8,1)
	call_c   build_ref_130()
	call_c   Dyam_Seed_Add_Comp(&ref[130],&ref[128],0)
	call_c   Dyam_Seed_End()
	move_ret seed[27]
	c_ret

;; TERM 130: '*FIRST*'('call_bmg_get_stacks/1') :> '$$HOLE$$'
c_code local build_ref_130
	ret_reg &ref[130]
	call_c   build_ref_129()
	call_c   Dyam_Create_Binary(I(9),&ref[129],I(7))
	move_ret ref[130]
	c_ret

;; TERM 129: '*FIRST*'('call_bmg_get_stacks/1')
c_code local build_ref_129
	ret_reg &ref[129]
	call_c   build_ref_47()
	call_c   build_ref_12()
	call_c   Dyam_Create_Unary(&ref[47],&ref[12])
	move_ret ref[129]
	c_ret

;; TERM 12: 'call_bmg_get_stacks/1'
c_code local build_ref_12
	ret_reg &ref[12]
	call_c   Dyam_Create_Atom("call_bmg_get_stacks/1")
	move_ret ref[12]
	c_ret

;; TERM 128: '*CITEM*'('call_bmg_get_stacks/1', 'call_bmg_get_stacks/1')
c_code local build_ref_128
	ret_reg &ref[128]
	call_c   build_ref_50()
	call_c   build_ref_12()
	call_c   Dyam_Create_Binary(&ref[50],&ref[12],&ref[12])
	move_ret ref[128]
	c_ret

c_code local build_seed_28
	ret_reg &seed[28]
	call_c   build_ref_83()
	call_c   build_ref_134()
	call_c   Dyam_Seed_Start(&ref[83],&ref[134],I(0),fun12,1)
	call_c   build_ref_132()
	call_c   Dyam_Seed_Add_Comp(&ref[132],fun43,0)
	call_c   Dyam_Seed_End()
	move_ret seed[28]
	c_ret

;; TERM 132: '*RITEM*'('call_bmg_get_stacks/1', return(_C))
c_code local build_ref_132
	ret_reg &ref[132]
	call_c   build_ref_0()
	call_c   build_ref_12()
	call_c   build_ref_131()
	call_c   Dyam_Create_Binary(&ref[0],&ref[12],&ref[131])
	move_ret ref[132]
	c_ret

;; TERM 131: return(_C)
c_code local build_ref_131
	ret_reg &ref[131]
	call_c   build_ref_80()
	call_c   Dyam_Create_Unary(&ref[80],V(2))
	move_ret ref[131]
	c_ret

pl_code local fun43
	call_c   build_ref_132()
	call_c   Dyam_Unify_Item(&ref[132])
	fail_ret
	pl_jump  Follow_Cont(V(1))

;; TERM 134: '*RITEM*'('call_bmg_get_stacks/1', return(_C)) :> maker(2, '$TUPPLE'(35101651764716))
c_code local build_ref_134
	ret_reg &ref[134]
	call_c   build_ref_132()
	call_c   build_ref_133()
	call_c   Dyam_Create_Binary(I(9),&ref[132],&ref[133])
	move_ret ref[134]
	c_ret

;; TERM 133: maker(2, '$TUPPLE'(35101651764716))
c_code local build_ref_133
	ret_reg &ref[133]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,134217728)
	move_ret R(0)
	call_c   build_ref_243()
	call_c   Dyam_Create_Binary(&ref[243],N(2),R(0))
	move_ret ref[133]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_23
	ret_reg &seed[23]
	call_c   build_ref_50()
	call_c   build_ref_100()
	call_c   Dyam_Seed_Start(&ref[50],&ref[100],&ref[100],fun8,1)
	call_c   build_ref_102()
	call_c   Dyam_Seed_Add_Comp(&ref[102],&ref[100],0)
	call_c   Dyam_Seed_End()
	move_ret seed[23]
	c_ret

;; TERM 102: '*FIRST*'('call_make_true_callret/3'(_C)) :> '$$HOLE$$'
c_code local build_ref_102
	ret_reg &ref[102]
	call_c   build_ref_101()
	call_c   Dyam_Create_Binary(I(9),&ref[101],I(7))
	move_ret ref[102]
	c_ret

;; TERM 101: '*FIRST*'('call_make_true_callret/3'(_C))
c_code local build_ref_101
	ret_reg &ref[101]
	call_c   build_ref_47()
	call_c   build_ref_99()
	call_c   Dyam_Create_Unary(&ref[47],&ref[99])
	move_ret ref[101]
	c_ret

;; TERM 99: 'call_make_true_callret/3'(_C)
c_code local build_ref_99
	ret_reg &ref[99]
	call_c   build_ref_231()
	call_c   Dyam_Create_Unary(&ref[231],V(2))
	move_ret ref[99]
	c_ret

;; TERM 100: '*CITEM*'('call_make_true_callret/3'(_C), 'call_make_true_callret/3'(_C))
c_code local build_ref_100
	ret_reg &ref[100]
	call_c   build_ref_50()
	call_c   build_ref_99()
	call_c   Dyam_Create_Binary(&ref[50],&ref[99],&ref[99])
	move_ret ref[100]
	c_ret

c_code local build_seed_24
	ret_reg &seed[24]
	call_c   build_ref_83()
	call_c   build_ref_106()
	call_c   Dyam_Seed_Start(&ref[83],&ref[106],I(0),fun12,1)
	call_c   build_ref_104()
	call_c   Dyam_Seed_Add_Comp(&ref[104],fun32,0)
	call_c   Dyam_Seed_End()
	move_ret seed[24]
	c_ret

;; TERM 104: '*RITEM*'('call_make_true_callret/3'(_C), return(_D, _E))
c_code local build_ref_104
	ret_reg &ref[104]
	call_c   build_ref_0()
	call_c   build_ref_99()
	call_c   build_ref_103()
	call_c   Dyam_Create_Binary(&ref[0],&ref[99],&ref[103])
	move_ret ref[104]
	c_ret

;; TERM 103: return(_D, _E)
c_code local build_ref_103
	ret_reg &ref[103]
	call_c   build_ref_80()
	call_c   Dyam_Create_Binary(&ref[80],V(3),V(4))
	move_ret ref[103]
	c_ret

pl_code local fun32
	call_c   build_ref_104()
	call_c   Dyam_Unify_Item(&ref[104])
	fail_ret
	pl_jump  Follow_Cont(V(1))

;; TERM 106: '*RITEM*'('call_make_true_callret/3'(_C), return(_D, _E)) :> maker(1, '$TUPPLE'(35101651764716))
c_code local build_ref_106
	ret_reg &ref[106]
	call_c   build_ref_104()
	call_c   build_ref_105()
	call_c   Dyam_Create_Binary(I(9),&ref[104],&ref[105])
	move_ret ref[106]
	c_ret

;; TERM 105: maker(1, '$TUPPLE'(35101651764716))
c_code local build_ref_105
	ret_reg &ref[105]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,134217728)
	move_ret R(0)
	call_c   build_ref_243()
	call_c   Dyam_Create_Binary(&ref[243],N(1),R(0))
	move_ret ref[105]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_18
	ret_reg &seed[18]
	call_c   build_ref_50()
	call_c   build_ref_53()
	call_c   Dyam_Seed_Start(&ref[50],&ref[53],&ref[53],fun8,1)
	call_c   build_ref_55()
	call_c   Dyam_Seed_Add_Comp(&ref[55],&ref[53],0)
	call_c   Dyam_Seed_End()
	move_ret seed[18]
	c_ret

;; TERM 55: '*FIRST*'(bmg_uniform_stacks(_C, _D)) :> '$$HOLE$$'
c_code local build_ref_55
	ret_reg &ref[55]
	call_c   build_ref_54()
	call_c   Dyam_Create_Binary(I(9),&ref[54],I(7))
	move_ret ref[55]
	c_ret

;; TERM 54: '*FIRST*'(bmg_uniform_stacks(_C, _D))
c_code local build_ref_54
	ret_reg &ref[54]
	call_c   build_ref_47()
	call_c   build_ref_52()
	call_c   Dyam_Create_Unary(&ref[47],&ref[52])
	move_ret ref[54]
	c_ret

;; TERM 52: bmg_uniform_stacks(_C, _D)
c_code local build_ref_52
	ret_reg &ref[52]
	call_c   build_ref_228()
	call_c   Dyam_Create_Binary(&ref[228],V(2),V(3))
	move_ret ref[52]
	c_ret

;; TERM 53: '*CITEM*'(bmg_uniform_stacks(_C, _D), bmg_uniform_stacks(_C, _D))
c_code local build_ref_53
	ret_reg &ref[53]
	call_c   build_ref_50()
	call_c   build_ref_52()
	call_c   Dyam_Create_Binary(&ref[50],&ref[52],&ref[52])
	move_ret ref[53]
	c_ret

c_code local build_seed_21
	ret_reg &seed[21]
	call_c   build_ref_83()
	call_c   build_ref_86()
	call_c   Dyam_Seed_Start(&ref[83],&ref[86],I(0),fun12,1)
	call_c   build_ref_84()
	call_c   Dyam_Seed_Add_Comp(&ref[84],fun25,0)
	call_c   Dyam_Seed_End()
	move_ret seed[21]
	c_ret

;; TERM 84: '*RITEM*'(bmg_uniform_stacks(_C, _D), voidret)
c_code local build_ref_84
	ret_reg &ref[84]
	call_c   build_ref_0()
	call_c   build_ref_52()
	call_c   build_ref_6()
	call_c   Dyam_Create_Binary(&ref[0],&ref[52],&ref[6])
	move_ret ref[84]
	c_ret

pl_code local fun25
	call_c   build_ref_84()
	call_c   Dyam_Unify_Item(&ref[84])
	fail_ret
	pl_jump  Follow_Cont(V(1))

;; TERM 86: '*RITEM*'(bmg_uniform_stacks(_C, _D), voidret) :> maker(0, '$TUPPLE'(35101651764716))
c_code local build_ref_86
	ret_reg &ref[86]
	call_c   build_ref_84()
	call_c   build_ref_85()
	call_c   Dyam_Create_Binary(I(9),&ref[84],&ref[85])
	move_ret ref[86]
	c_ret

;; TERM 85: maker(0, '$TUPPLE'(35101651764716))
c_code local build_ref_85
	ret_reg &ref[85]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,134217728)
	move_ret R(0)
	call_c   build_ref_243()
	call_c   Dyam_Create_Binary(&ref[243],N(0),R(0))
	move_ret ref[85]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 4: bmg_patterns(_A, _B)
c_code local build_ref_4
	ret_reg &ref[4]
	call_c   build_ref_244()
	call_c   Dyam_Create_Binary(&ref[244],V(0),V(1))
	move_ret ref[4]
	c_ret

;; TERM 244: bmg_patterns
c_code local build_ref_244
	ret_reg &ref[244]
	call_c   Dyam_Create_Atom("bmg_patterns")
	move_ret ref[244]
	c_ret

;; TERM 3: '*RITEM*'('call_bmg_patterns/2', return(_A, _B))
c_code local build_ref_3
	ret_reg &ref[3]
	call_c   build_ref_0()
	call_c   build_ref_1()
	call_c   build_ref_2()
	call_c   Dyam_Create_Binary(&ref[0],&ref[1],&ref[2])
	move_ret ref[3]
	c_ret

;; TERM 2: return(_A, _B)
c_code local build_ref_2
	ret_reg &ref[2]
	call_c   build_ref_80()
	call_c   Dyam_Create_Binary(&ref[80],V(0),V(1))
	move_ret ref[2]
	c_ret

;; TERM 5: bmg_uniform_stacks(_A, _B)
c_code local build_ref_5
	ret_reg &ref[5]
	call_c   build_ref_228()
	call_c   Dyam_Create_Binary(&ref[228],V(0),V(1))
	move_ret ref[5]
	c_ret

;; TERM 7: '*RITEM*'(bmg_uniform_stacks(_A, _B), voidret)
c_code local build_ref_7
	ret_reg &ref[7]
	call_c   build_ref_0()
	call_c   build_ref_5()
	call_c   build_ref_6()
	call_c   Dyam_Create_Binary(&ref[0],&ref[5],&ref[6])
	move_ret ref[7]
	c_ret

;; TERM 11: bmg_args(_A, _B, _C, _D, _E, _F)
c_code local build_ref_11
	ret_reg &ref[11]
	call_c   build_ref_239()
	call_c   Dyam_Term_Start(&ref[239],6)
	call_c   Dyam_Term_Arg(V(0))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[11]
	c_ret

;; TERM 10: '*RITEM*'('call_bmg_args/6'(_C, _D), return(_A, _B, _E, _F))
c_code local build_ref_10
	ret_reg &ref[10]
	call_c   build_ref_0()
	call_c   build_ref_8()
	call_c   build_ref_9()
	call_c   Dyam_Create_Binary(&ref[0],&ref[8],&ref[9])
	move_ret ref[10]
	c_ret

;; TERM 9: return(_A, _B, _E, _F)
c_code local build_ref_9
	ret_reg &ref[9]
	call_c   build_ref_80()
	call_c   Dyam_Term_Start(&ref[80],4)
	call_c   Dyam_Term_Arg(V(0))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[9]
	c_ret

;; TERM 8: 'call_bmg_args/6'(_C, _D)
c_code local build_ref_8
	ret_reg &ref[8]
	call_c   build_ref_236()
	call_c   Dyam_Create_Binary(&ref[236],V(2),V(3))
	move_ret ref[8]
	c_ret

;; TERM 15: bmg_get_stacks(_A)
c_code local build_ref_15
	ret_reg &ref[15]
	call_c   build_ref_245()
	call_c   Dyam_Create_Unary(&ref[245],V(0))
	move_ret ref[15]
	c_ret

;; TERM 245: bmg_get_stacks
c_code local build_ref_245
	ret_reg &ref[245]
	call_c   Dyam_Create_Atom("bmg_get_stacks")
	move_ret ref[245]
	c_ret

;; TERM 14: '*RITEM*'('call_bmg_get_stacks/1', return(_A))
c_code local build_ref_14
	ret_reg &ref[14]
	call_c   build_ref_0()
	call_c   build_ref_12()
	call_c   build_ref_13()
	call_c   Dyam_Create_Binary(&ref[0],&ref[12],&ref[13])
	move_ret ref[14]
	c_ret

;; TERM 13: return(_A)
c_code local build_ref_13
	ret_reg &ref[13]
	call_c   build_ref_80()
	call_c   Dyam_Create_Unary(&ref[80],V(0))
	move_ret ref[13]
	c_ret

;; TERM 19: make_true_dcg_callret(_A, _B, _C, _D, _E, _F, _G)
c_code local build_ref_19
	ret_reg &ref[19]
	call_c   build_ref_242()
	call_c   Dyam_Term_Start(&ref[242],7)
	call_c   Dyam_Term_Arg(V(0))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[19]
	c_ret

;; TERM 18: '*RITEM*'('call_make_true_dcg_callret/7'(_A), return(_B, _C, _D, _E, _F, _G))
c_code local build_ref_18
	ret_reg &ref[18]
	call_c   build_ref_0()
	call_c   build_ref_16()
	call_c   build_ref_17()
	call_c   Dyam_Create_Binary(&ref[0],&ref[16],&ref[17])
	move_ret ref[18]
	c_ret

;; TERM 17: return(_B, _C, _D, _E, _F, _G)
c_code local build_ref_17
	ret_reg &ref[17]
	call_c   build_ref_80()
	call_c   Dyam_Term_Start(&ref[80],6)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[17]
	c_ret

;; TERM 16: 'call_make_true_dcg_callret/7'(_A)
c_code local build_ref_16
	ret_reg &ref[16]
	call_c   build_ref_238()
	call_c   Dyam_Create_Unary(&ref[238],V(0))
	move_ret ref[16]
	c_ret

;; TERM 23: make_true_callret(_A, _B, _C)
c_code local build_ref_23
	ret_reg &ref[23]
	call_c   build_ref_246()
	call_c   Dyam_Term_Start(&ref[246],3)
	call_c   Dyam_Term_Arg(V(0))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_End()
	move_ret ref[23]
	c_ret

;; TERM 246: make_true_callret
c_code local build_ref_246
	ret_reg &ref[246]
	call_c   Dyam_Create_Atom("make_true_callret")
	move_ret ref[246]
	c_ret

;; TERM 22: '*RITEM*'('call_make_true_callret/3'(_A), return(_B, _C))
c_code local build_ref_22
	ret_reg &ref[22]
	call_c   build_ref_0()
	call_c   build_ref_20()
	call_c   build_ref_21()
	call_c   Dyam_Create_Binary(&ref[0],&ref[20],&ref[21])
	move_ret ref[22]
	c_ret

;; TERM 20: 'call_make_true_callret/3'(_A)
c_code local build_ref_20
	ret_reg &ref[20]
	call_c   build_ref_231()
	call_c   Dyam_Create_Unary(&ref[231],V(0))
	move_ret ref[20]
	c_ret

;; TERM 219: make_true_dcg_callret(_C, _D, _E, _F, _G, _H, _I)
c_code local build_ref_219
	ret_reg &ref[219]
	call_c   build_ref_242()
	call_c   Dyam_Term_Start(&ref[242],7)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_End()
	move_ret ref[219]
	c_ret

;; TERM 178: bmg_args(_C, _D, _E, _F, _G, _H)
c_code local build_ref_178
	ret_reg &ref[178]
	call_c   build_ref_239()
	call_c   Dyam_Term_Start(&ref[239],6)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[178]
	c_ret

;; TERM 78: call
c_code local build_ref_78
	ret_reg &ref[78]
	call_c   Dyam_Create_Atom("call")
	move_ret ref[78]
	c_ret

;; TERM 81: 'return_~w/~w'
c_code local build_ref_81
	ret_reg &ref[81]
	call_c   Dyam_Create_Atom("return_~w/~w")
	move_ret ref[81]
	c_ret

;; TERM 82: 'call_~w/~w'
c_code local build_ref_82
	ret_reg &ref[82]
	call_c   Dyam_Create_Atom("call_~w/~w")
	move_ret ref[82]
	c_ret

;; TERM 72: [_G,_D]
c_code local build_ref_72
	ret_reg &ref[72]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(3),I(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(6),R(0))
	move_ret ref[72]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 73: _C / _D
c_code local build_ref_73
	ret_reg &ref[73]
	call_c   build_ref_235()
	call_c   Dyam_Create_Binary(&ref[235],V(2),V(3))
	move_ret ref[73]
	c_ret

pl_code local fun13
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Object(R(0))
	call_c   Dyam_Tabule()
	pl_ret

pl_code local fun73
	call_c   Dyam_Remove_Choice()
	pl_call  fun26(&seed[40],2,V(9))
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun57
	call_c   Dyam_Remove_Choice()
	pl_call  fun26(&seed[36],2,V(8))
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun54
	call_c   Dyam_Remove_Choice()
	pl_call  fun26(&seed[34],2,V(4))
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun44
	call_c   Dyam_Remove_Choice()
	pl_call  fun26(&seed[28],2,V(3))
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun33
	call_c   Dyam_Remove_Choice()
	pl_call  fun26(&seed[24],2,V(5))
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun23
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Object(R(0))
	call_c   Dyam_Tabule()
	pl_jump  Schedule_Prolog()

pl_code local fun24
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Choice(fun23)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Subsume(R(0))
	fail_ret
	call_c   Dyam_Cut()
	pl_ret

pl_code local fun26
	call_c   Dyam_Backptr_Trace(R(1),R(2))
	call_c   Dyam_Light_Object(R(0))
	pl_jump  Schedule_Prolog()

pl_code local fun27
	call_c   Dyam_Remove_Choice()
	pl_call  fun26(&seed[21],2,V(4))
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun19
	call_c   Dyam_Remove_Choice()
	move     &ref[81], R(0)
	move     0, R(1)
	move     &ref[73], R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Load(4,V(5))
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  pred_build_aux_name_3()

pl_code local fun21
	call_c   Dyam_Remove_Choice()
	move     &ref[82], R(0)
	move     0, R(1)
	move     &ref[73], R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Load(4,V(4))
	pl_call  pred_build_aux_name_3()
	pl_jump  fun20()


;;------------------ INIT POOL ------------

c_code local build_init_pool
	call_c   build_seed_3()
	call_c   build_seed_1()
	call_c   build_seed_2()
	call_c   build_seed_5()
	call_c   build_seed_6()
	call_c   build_seed_0()
	call_c   build_seed_4()
	call_c   build_seed_7()
	call_c   build_seed_8()
	call_c   build_seed_12()
	call_c   build_seed_14()
	call_c   build_seed_13()
	call_c   build_seed_17()
	call_c   build_seed_16()
	call_c   build_seed_9()
	call_c   build_seed_15()
	call_c   build_seed_10()
	call_c   build_seed_11()
	call_c   build_seed_39()
	call_c   build_seed_40()
	call_c   build_seed_35()
	call_c   build_seed_36()
	call_c   build_seed_33()
	call_c   build_seed_34()
	call_c   build_seed_27()
	call_c   build_seed_28()
	call_c   build_seed_23()
	call_c   build_seed_24()
	call_c   build_seed_18()
	call_c   build_seed_21()
	call_c   build_ref_4()
	call_c   build_ref_3()
	call_c   build_ref_5()
	call_c   build_ref_7()
	call_c   build_ref_11()
	call_c   build_ref_10()
	call_c   build_ref_15()
	call_c   build_ref_14()
	call_c   build_ref_19()
	call_c   build_ref_18()
	call_c   build_ref_23()
	call_c   build_ref_22()
	call_c   build_ref_219()
	call_c   build_ref_178()
	call_c   build_ref_78()
	call_c   build_ref_77()
	call_c   build_ref_80()
	call_c   build_ref_79()
	call_c   build_ref_81()
	call_c   build_ref_82()
	call_c   build_ref_72()
	call_c   build_ref_73()
	c_ret

long local ref[247]
long local seed[41]

long local _initialization

c_code global initialization_dyalog_maker
	call_c   Dyam_Check_And_Update_Initialization(_initialization)
	fail_c_ret
	call_c   initialization_dyalog_format()
	call_c   build_init_pool()
	call_c   build_viewers()
	call_c   load()
	c_ret

