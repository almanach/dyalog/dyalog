;; Compiler: DyALog 1.14.0
;; File "tig.pl"


;;------------------ LOADING ------------

c_code local load
	call_c   Dyam_Loading_Set()
	pl_call  fun27(&seed[15],0)
	pl_call  fun27(&seed[25],0)
	pl_call  fun27(&seed[34],0)
	pl_call  fun27(&seed[24],0)
	pl_call  fun27(&seed[37],0)
	pl_call  fun27(&seed[60],0)
	pl_call  fun27(&seed[39],0)
	pl_call  fun27(&seed[36],0)
	pl_call  fun27(&seed[33],0)
	pl_call  fun27(&seed[40],0)
	pl_call  fun27(&seed[12],0)
	pl_call  fun27(&seed[38],0)
	pl_call  fun27(&seed[35],0)
	pl_call  fun27(&seed[11],0)
	pl_call  fun27(&seed[59],0)
	pl_call  fun27(&seed[55],0)
	pl_call  fun27(&seed[8],0)
	pl_call  fun27(&seed[17],0)
	pl_call  fun27(&seed[9],0)
	pl_call  fun27(&seed[19],0)
	pl_call  fun27(&seed[42],0)
	pl_call  fun27(&seed[41],0)
	pl_call  fun27(&seed[20],0)
	pl_call  fun27(&seed[23],0)
	pl_call  fun27(&seed[22],0)
	pl_call  fun27(&seed[21],0)
	pl_call  fun27(&seed[30],0)
	pl_call  fun27(&seed[29],0)
	pl_call  fun27(&seed[18],0)
	pl_call  fun27(&seed[45],0)
	pl_call  fun27(&seed[10],0)
	pl_call  fun27(&seed[49],0)
	pl_call  fun27(&seed[56],0)
	pl_call  fun27(&seed[52],0)
	pl_call  fun27(&seed[51],0)
	pl_call  fun27(&seed[43],0)
	pl_call  fun27(&seed[53],0)
	pl_call  fun27(&seed[26],0)
	pl_call  fun27(&seed[14],0)
	pl_call  fun27(&seed[13],0)
	pl_call  fun27(&seed[31],0)
	pl_call  fun27(&seed[62],0)
	pl_call  fun27(&seed[44],0)
	pl_call  fun27(&seed[27],0)
	pl_call  fun27(&seed[61],0)
	pl_call  fun27(&seed[16],0)
	pl_call  fun27(&seed[48],0)
	pl_call  fun27(&seed[50],0)
	pl_call  fun27(&seed[28],0)
	pl_call  fun27(&seed[47],0)
	pl_call  fun27(&seed[46],0)
	pl_call  fun27(&seed[5],0)
	pl_call  fun27(&seed[57],0)
	pl_call  fun27(&seed[58],0)
	pl_call  fun27(&seed[2],0)
	pl_call  fun27(&seed[0],0)
	pl_call  fun27(&seed[7],0)
	pl_call  fun27(&seed[3],0)
	pl_call  fun27(&seed[1],0)
	pl_call  fun27(&seed[6],0)
	pl_call  fun27(&seed[4],0)
	pl_call  fun27(&seed[54],0)
	pl_call  fun27(&seed[32],0)
	pl_call  fun19(&seed[64],0)
	pl_call  fun19(&seed[63],0)
	call_c   Dyam_Loading_Reset()
	c_ret

pl_code global pred_lctag_add_trees_4
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(4)
	call_c   Dyam_Choice(fun487)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(3),I(0))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(4),&ref[1031])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code global pred_noop_first_2
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(2)
	call_c   Dyam_Choice(fun382)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[603])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(2),V(1))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code global pred_left_potential_tig_1
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Choice(fun360)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[667])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(2))
	pl_call  pred_left_potential_tig_1()
	call_c   Dyam_Reg_Load(0,V(3))
	call_c   Dyam_Reg_Deallocate(1)
	pl_jump  pred_left_potential_tig_1()

pl_code global pred_spine_node_2
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(2)
	call_c   Dyam_Choice(fun355)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[830])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun350)
	call_c   Dyam_Reg_Load(0,V(3))
	call_c   Dyam_Reg_Load(2,V(2))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_spine_node_2()

pl_code global pred_right_potential_tig_1
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Choice(fun307)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[667])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(2))
	pl_call  pred_right_potential_tig_1()
	call_c   Dyam_Reg_Load(0,V(3))
	call_c   Dyam_Reg_Deallocate(1)
	pl_jump  pred_right_potential_tig_1()

pl_code global pred_guard_eval_1
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Choice(fun263)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[487])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Deallocate()
	pl_ret

pl_code global pred_lctag_first_scan_2
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(2)
	call_c   Dyam_Choice(fun233)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),I(0))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(2),&ref[305])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code global pred_tig_2
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Unify(0,&ref[599])
	fail_ret
	call_c   Dyam_Reg_Bind(2,V(13))
	call_c   DYAM_evpred_functor(V(7),V(14),V(15))
	fail_ret
	move     &ref[581], R(0)
	move     S(5), R(1)
	pl_call  pred_record_without_doublon_1()
	move     &ref[602], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	move     &ref[599], R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Load(6,V(13))
	call_c   Dyam_Reg_Deallocate(4)
fun221:
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Reg_Bind(2,V(4))
	call_c   Dyam_Multi_Reg_Bind(4,2,2)
	call_c   Dyam_Choice(fun220)
	pl_call  fun29(&seed[117],1)
	pl_fail


pl_code global pred_make_tig_callret_7
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(7)
	call_c   DYAM_evpred_functor(V(2),V(8),V(9))
	fail_ret
	call_c   DYAM_evpred_functor(V(3),V(8),V(9))
	fail_ret
	call_c   DYAM_evpred_functor(V(10),V(8),V(9))
	fail_ret
	call_c   DYAM_evpred_functor(V(11),V(8),V(9))
	fail_ret
	move     &ref[555], R(0)
	move     S(5), R(1)
	move     &ref[556], R(2)
	move     S(5), R(3)
	move     I(0), R(4)
	move     0, R(5)
	call_c   Dyam_Reg_Deallocate(3)
fun202:
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Reg_Unify(2,&ref[445])
	fail_ret
	call_c   Dyam_Reg_Bind(4,V(9))
	call_c   Dyam_Choice(fun201)
	pl_call  fun29(&seed[98],1)
	pl_fail


pl_code global pred_tag2tig_emit_header_0
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   DYAM_Date_Time_6(V(1),V(2),V(3),V(4),V(5),V(6))
	fail_ret
	call_c   DYAM_Compiler_Info_1(&ref[435])
	fail_ret
	move     &ref[449], R(0)
	move     0, R(1)
	move     &ref[437], R(2)
	move     S(5), R(3)
	pl_call  pred_format_2()
	move     &ref[438], R(0)
	move     0, R(1)
	move     &ref[439], R(2)
	move     S(5), R(3)
	pl_call  pred_format_2()
	call_c   Dyam_Choice(fun128)
	pl_call  Object_1(&ref[440])
	call_c   Dyam_Choice(fun127)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(11),&ref[255])
	fail_ret
	call_c   Dyam_Cut()
	pl_fail

pl_code global pred_lctag_emit_header_0
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   DYAM_Date_Time_6(V(1),V(2),V(3),V(4),V(5),V(6))
	fail_ret
	call_c   DYAM_Compiler_Info_1(&ref[435])
	fail_ret
	move     &ref[436], R(0)
	move     0, R(1)
	move     &ref[437], R(2)
	move     S(5), R(3)
	pl_call  pred_format_2()
	move     &ref[438], R(0)
	move     0, R(1)
	move     &ref[439], R(2)
	move     S(5), R(3)
	pl_call  pred_format_2()
	call_c   Dyam_Choice(fun128)
	pl_call  Object_1(&ref[440])
	call_c   Dyam_Choice(fun127)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(11),&ref[255])
	fail_ret
	call_c   Dyam_Cut()
	pl_fail

pl_code global pred_tig_compile_adj_node_12
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Reg_Unify(2,&ref[351])
	fail_ret
	call_c   Dyam_Multi_Reg_Bind(4,6,8)
	call_c   Dyam_Reg_Bind(22,V(15))
	call_c   DYAM_evpred_functor(V(16),V(2),V(3))
	fail_ret
	call_c   DYAM_evpred_functor(V(17),V(2),V(3))
	fail_ret
	call_c   Dyam_Choice(fun97)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[317])
	call_c   Dyam_Cut()
fun96:
	move     &ref[318], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(21), R(4)
	move     S(5), R(5)
	pl_call  pred_tag_numbervars_3()
	call_c   Dyam_Reg_Load(0,V(4))
	call_c   Dyam_Reg_Load(2,V(16))
	call_c   Dyam_Reg_Load(4,V(17))
	call_c   Dyam_Reg_Load(6,V(19))
	call_c   Dyam_Reg_Load(8,V(20))
	move     V(22), R(10)
	move     S(5), R(11)
	move     V(23), R(12)
	move     S(5), R(13)
	pl_call  pred_make_tig_callret_7()
	call_c   Dyam_Choice(fun95)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(5),&ref[319])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(24),N(0))
	fail_ret
fun94:
	call_c   Dyam_Unify(V(25),&ref[320])
	fail_ret
	move     &ref[321], R(0)
	move     0, R(1)
	move     V(26), R(2)
	move     S(5), R(3)
	pl_call  pred_update_counter_2()
	move     &ref[322], R(0)
	move     0, R(1)
	move     &ref[323], R(2)
	move     S(5), R(3)
	move     V(27), R(4)
	move     S(5), R(5)
	pl_call  pred_name_builder_3()
	pl_call  Object_1(&ref[324])
	move     &ref[325], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(34), R(4)
	move     S(5), R(5)
	pl_call  pred_tag_numbervars_3()
	call_c   DYAM_evpred_univ(V(16),&ref[326])
	fail_ret
	call_c   Dyam_Unify(V(37),&ref[327])
	fail_ret
	call_c   DYAM_evpred_univ(V(17),&ref[328])
	fail_ret
	call_c   Dyam_Unify(V(40),&ref[329])
	fail_ret
	call_c   DYAM_evpred_univ(V(7),&ref[330])
	fail_ret
	call_c   Dyam_Unify(V(43),&ref[331])
	fail_ret
	call_c   DYAM_evpred_univ(V(9),&ref[332])
	fail_ret
	call_c   Dyam_Unify(V(46),&ref[333])
	fail_ret
	call_c   Dyam_Reg_Load(0,V(25))
	move     V(47), R(2)
	move     S(5), R(3)
	pl_call  pred_tupple_2()
	call_c   Dyam_Reg_Load(0,V(11))
	move     V(48), R(2)
	move     S(5), R(3)
	pl_call  pred_tupple_2()
	move     &ref[334], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(48))
	move     V(49), R(4)
	move     S(5), R(5)
	pl_call  pred_extend_tupple_3()
	pl_call  fun38(&seed[86],1)
	call_c   Dyam_Unify(V(51),&ref[338])
	fail_ret
	call_c   Dyam_Choice(fun93)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[111])
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(52),&ref[339])
	fail_ret
fun92:
	pl_call  fun38(&seed[87],1)
	pl_call  fun38(&seed[88],1)
	call_c   Dyam_Unify(V(55),&ref[346])
	fail_ret
	pl_call  fun38(&seed[89],1)
	call_c   Dyam_Deallocate()
	pl_ret




pl_code global pred_last_elt_2
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(2)
	call_c   Dyam_Choice(fun16)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[120])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Deallocate()
	pl_ret


;;------------------ VIEWERS ------------

c_code local build_viewers
	call_c   Dyam_Load_Viewer(&ref[51],&ref[50])
	call_c   Dyam_Load_Viewer(&ref[49],&ref[48])
	call_c   Dyam_Load_Viewer(&ref[47],&ref[46])
	call_c   Dyam_Load_Viewer(&ref[45],&ref[44])
	call_c   Dyam_Load_Viewer(&ref[43],&ref[42])
	call_c   Dyam_Load_Viewer(&ref[41],&ref[40])
	call_c   Dyam_Load_Viewer(&ref[38],&ref[39])
	call_c   Dyam_Load_Viewer(&ref[35],&ref[36])
	call_c   Dyam_Load_Viewer(&ref[33],&ref[32])
	call_c   Dyam_Load_Viewer(&ref[30],&ref[31])
	call_c   Dyam_Load_Viewer(&ref[27],&ref[26])
	call_c   Dyam_Load_Viewer(&ref[24],&ref[25])
	call_c   Dyam_Load_Viewer(&ref[22],&ref[20])
	call_c   Dyam_Load_Viewer(&ref[18],&ref[19])
	call_c   Dyam_Load_Viewer(&ref[14],&ref[15])
	call_c   Dyam_Load_Viewer(&ref[11],&ref[12])
	call_c   Dyam_Load_Viewer(&ref[7],&ref[8])
	call_c   Dyam_Load_Viewer(&ref[3],&ref[4])
	c_ret

c_code local build_seed_63
	ret_reg &seed[63]
	call_c   build_ref_52()
	call_c   build_ref_53()
	call_c   Dyam_Seed_Start(&ref[52],&ref[53],&ref[53],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[63]
	c_ret

pl_code local fun0
	pl_ret

;; TERM 53: small_sorted_tree_add(_B, [], [_B])
c_code local build_ref_53
	ret_reg &ref[53]
	call_c   build_ref_1118()
	call_c   build_ref_824()
	call_c   Dyam_Term_Start(&ref[1118],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(&ref[824])
	call_c   Dyam_Term_End()
	move_ret ref[53]
	c_ret

;; TERM 824: [_B]
c_code local build_ref_824
	ret_reg &ref[824]
	call_c   Dyam_Create_List(V(1),I(0))
	move_ret ref[824]
	c_ret

;; TERM 1118: small_sorted_tree_add
c_code local build_ref_1118
	ret_reg &ref[1118]
	call_c   Dyam_Create_Atom("small_sorted_tree_add")
	move_ret ref[1118]
	c_ret

;; TERM 52: '*DATABASE*'
c_code local build_ref_52
	ret_reg &ref[52]
	call_c   Dyam_Create_Atom("*DATABASE*")
	move_ret ref[52]
	c_ret

c_code local build_seed_64
	ret_reg &seed[64]
	call_c   build_ref_52()
	call_c   build_ref_85()
	call_c   Dyam_Seed_Start(&ref[52],&ref[85],&ref[85],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[64]
	c_ret

;; TERM 85: small_sorted_tree_add(_B, [_C], [_C])
c_code local build_ref_85
	ret_reg &ref[85]
	call_c   build_ref_1118()
	call_c   build_ref_120()
	call_c   Dyam_Term_Start(&ref[1118],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(&ref[120])
	call_c   Dyam_Term_Arg(&ref[120])
	call_c   Dyam_Term_End()
	move_ret ref[85]
	c_ret

;; TERM 120: [_C]
c_code local build_ref_120
	ret_reg &ref[120]
	call_c   Dyam_Create_List(V(2),I(0))
	move_ret ref[120]
	c_ret

c_code local build_seed_32
	ret_reg &seed[32]
	call_c   build_ref_54()
	call_c   build_ref_58()
	call_c   Dyam_Seed_Start(&ref[54],&ref[58],I(0),fun0,1)
	call_c   build_ref_57()
	call_c   Dyam_Seed_Add_Comp(&ref[57],fun1,0)
	call_c   Dyam_Seed_End()
	move_ret seed[32]
	c_ret

;; TERM 57: '*GUARD*'(check_analyzer_mode(lctag))
c_code local build_ref_57
	ret_reg &ref[57]
	call_c   build_ref_55()
	call_c   build_ref_56()
	call_c   Dyam_Create_Unary(&ref[55],&ref[56])
	move_ret ref[57]
	c_ret

;; TERM 56: check_analyzer_mode(lctag)
c_code local build_ref_56
	ret_reg &ref[56]
	call_c   build_ref_1119()
	call_c   build_ref_1120()
	call_c   Dyam_Create_Unary(&ref[1119],&ref[1120])
	move_ret ref[56]
	c_ret

;; TERM 1120: lctag
c_code local build_ref_1120
	ret_reg &ref[1120]
	call_c   Dyam_Create_Atom("lctag")
	move_ret ref[1120]
	c_ret

;; TERM 1119: check_analyzer_mode
c_code local build_ref_1119
	ret_reg &ref[1119]
	call_c   Dyam_Create_Atom("check_analyzer_mode")
	move_ret ref[1119]
	c_ret

;; TERM 55: '*GUARD*'
c_code local build_ref_55
	ret_reg &ref[55]
	call_c   Dyam_Create_Atom("*GUARD*")
	move_ret ref[55]
	c_ret

;; TERM 59: stop_compilation_at(pgm)
c_code local build_ref_59
	ret_reg &ref[59]
	call_c   build_ref_1121()
	call_c   build_ref_1122()
	call_c   Dyam_Create_Unary(&ref[1121],&ref[1122])
	move_ret ref[59]
	c_ret

;; TERM 1122: pgm
c_code local build_ref_1122
	ret_reg &ref[1122]
	call_c   Dyam_Create_Atom("pgm")
	move_ret ref[1122]
	c_ret

;; TERM 1121: stop_compilation_at
c_code local build_ref_1121
	ret_reg &ref[1121]
	call_c   Dyam_Create_Atom("stop_compilation_at")
	move_ret ref[1121]
	c_ret

long local pool_fun1[3]=[2,build_ref_57,build_ref_59]

pl_code local fun1
	call_c   Dyam_Pool(pool_fun1)
	call_c   Dyam_Unify_Item(&ref[57])
	fail_ret
	call_c   DYAM_evpred_assert_1(&ref[59])
	pl_ret

;; TERM 58: '*GUARD*'(check_analyzer_mode(lctag)) :> []
c_code local build_ref_58
	ret_reg &ref[58]
	call_c   build_ref_57()
	call_c   Dyam_Create_Binary(I(9),&ref[57],I(0))
	move_ret ref[58]
	c_ret

;; TERM 54: '*GUARD_CLAUSE*'
c_code local build_ref_54
	ret_reg &ref[54]
	call_c   Dyam_Create_Atom("*GUARD_CLAUSE*")
	move_ret ref[54]
	c_ret

c_code local build_seed_54
	ret_reg &seed[54]
	call_c   build_ref_54()
	call_c   build_ref_62()
	call_c   Dyam_Seed_Start(&ref[54],&ref[62],I(0),fun0,1)
	call_c   build_ref_61()
	call_c   Dyam_Seed_Add_Comp(&ref[61],fun2,0)
	call_c   Dyam_Seed_End()
	move_ret seed[54]
	c_ret

;; TERM 61: '*GUARD*'(check_analyzer_mode(tag2tig))
c_code local build_ref_61
	ret_reg &ref[61]
	call_c   build_ref_55()
	call_c   build_ref_60()
	call_c   Dyam_Create_Unary(&ref[55],&ref[60])
	move_ret ref[61]
	c_ret

;; TERM 60: check_analyzer_mode(tag2tig)
c_code local build_ref_60
	ret_reg &ref[60]
	call_c   build_ref_1119()
	call_c   build_ref_1123()
	call_c   Dyam_Create_Unary(&ref[1119],&ref[1123])
	move_ret ref[60]
	c_ret

;; TERM 1123: tag2tig
c_code local build_ref_1123
	ret_reg &ref[1123]
	call_c   Dyam_Create_Atom("tag2tig")
	move_ret ref[1123]
	c_ret

long local pool_fun2[3]=[2,build_ref_61,build_ref_59]

pl_code local fun2
	call_c   Dyam_Pool(pool_fun2)
	call_c   Dyam_Unify_Item(&ref[61])
	fail_ret
	call_c   DYAM_evpred_assert_1(&ref[59])
	pl_ret

;; TERM 62: '*GUARD*'(check_analyzer_mode(tag2tig)) :> []
c_code local build_ref_62
	ret_reg &ref[62]
	call_c   build_ref_61()
	call_c   Dyam_Create_Binary(I(9),&ref[61],I(0))
	move_ret ref[62]
	c_ret

c_code local build_seed_4
	ret_reg &seed[4]
	call_c   build_ref_54()
	call_c   build_ref_75()
	call_c   Dyam_Seed_Start(&ref[54],&ref[75],I(0),fun0,1)
	call_c   build_ref_74()
	call_c   Dyam_Seed_Add_Comp(&ref[74],fun5,0)
	call_c   Dyam_Seed_End()
	move_ret seed[4]
	c_ret

;; TERM 74: '*GUARD*'(lctag_simplify(_B, '$skip', keep, '$skip'))
c_code local build_ref_74
	ret_reg &ref[74]
	call_c   build_ref_55()
	call_c   build_ref_73()
	call_c   Dyam_Create_Unary(&ref[55],&ref[73])
	move_ret ref[74]
	c_ret

;; TERM 73: lctag_simplify(_B, '$skip', keep, '$skip')
c_code local build_ref_73
	ret_reg &ref[73]
	call_c   build_ref_1124()
	call_c   build_ref_1125()
	call_c   build_ref_300()
	call_c   Dyam_Term_Start(&ref[1124],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(&ref[1125])
	call_c   Dyam_Term_Arg(&ref[300])
	call_c   Dyam_Term_Arg(&ref[1125])
	call_c   Dyam_Term_End()
	move_ret ref[73]
	c_ret

;; TERM 300: keep
c_code local build_ref_300
	ret_reg &ref[300]
	call_c   Dyam_Create_Atom("keep")
	move_ret ref[300]
	c_ret

;; TERM 1125: '$skip'
c_code local build_ref_1125
	ret_reg &ref[1125]
	call_c   Dyam_Create_Atom("$skip")
	move_ret ref[1125]
	c_ret

;; TERM 1124: lctag_simplify
c_code local build_ref_1124
	ret_reg &ref[1124]
	call_c   Dyam_Create_Atom("lctag_simplify")
	move_ret ref[1124]
	c_ret

pl_code local fun5
	call_c   build_ref_74()
	call_c   Dyam_Unify_Item(&ref[74])
	fail_ret
	pl_ret

;; TERM 75: '*GUARD*'(lctag_simplify(_B, '$skip', keep, '$skip')) :> []
c_code local build_ref_75
	ret_reg &ref[75]
	call_c   build_ref_74()
	call_c   Dyam_Create_Binary(I(9),&ref[74],I(0))
	move_ret ref[75]
	c_ret

c_code local build_seed_6
	ret_reg &seed[6]
	call_c   build_ref_54()
	call_c   build_ref_78()
	call_c   Dyam_Seed_Start(&ref[54],&ref[78],I(0),fun0,1)
	call_c   build_ref_77()
	call_c   Dyam_Seed_Add_Comp(&ref[77],fun6,0)
	call_c   Dyam_Seed_End()
	move_ret seed[6]
	c_ret

;; TERM 77: '*GUARD*'(lctag_simplify(_B, [], keep, []))
c_code local build_ref_77
	ret_reg &ref[77]
	call_c   build_ref_55()
	call_c   build_ref_76()
	call_c   Dyam_Create_Unary(&ref[55],&ref[76])
	move_ret ref[77]
	c_ret

;; TERM 76: lctag_simplify(_B, [], keep, [])
c_code local build_ref_76
	ret_reg &ref[76]
	call_c   build_ref_1124()
	call_c   build_ref_300()
	call_c   Dyam_Term_Start(&ref[1124],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(&ref[300])
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_End()
	move_ret ref[76]
	c_ret

pl_code local fun6
	call_c   build_ref_77()
	call_c   Dyam_Unify_Item(&ref[77])
	fail_ret
	pl_ret

;; TERM 78: '*GUARD*'(lctag_simplify(_B, [], keep, [])) :> []
c_code local build_ref_78
	ret_reg &ref[78]
	call_c   build_ref_77()
	call_c   Dyam_Create_Binary(I(9),&ref[77],I(0))
	move_ret ref[78]
	c_ret

c_code local build_seed_1
	ret_reg &seed[1]
	call_c   build_ref_63()
	call_c   build_ref_67()
	call_c   Dyam_Seed_Start(&ref[63],&ref[67],I(0),fun0,1)
	call_c   build_ref_68()
	call_c   Dyam_Seed_Add_Comp(&ref[68],fun3,0)
	call_c   Dyam_Seed_End()
	move_ret seed[1]
	c_ret

;; TERM 68: '*PROLOG-ITEM*'{top=> lctag_first(_B, '$skip', epsilon, []), cont=> _A}
c_code local build_ref_68
	ret_reg &ref[68]
	call_c   build_ref_1126()
	call_c   build_ref_65()
	call_c   Dyam_Create_Binary(&ref[1126],&ref[65],V(0))
	move_ret ref[68]
	c_ret

;; TERM 65: lctag_first(_B, '$skip', epsilon, [])
c_code local build_ref_65
	ret_reg &ref[65]
	call_c   build_ref_1127()
	call_c   build_ref_1125()
	call_c   build_ref_305()
	call_c   Dyam_Term_Start(&ref[1127],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(&ref[1125])
	call_c   Dyam_Term_Arg(&ref[305])
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_End()
	move_ret ref[65]
	c_ret

;; TERM 305: epsilon
c_code local build_ref_305
	ret_reg &ref[305]
	call_c   Dyam_Create_Atom("epsilon")
	move_ret ref[305]
	c_ret

;; TERM 1127: lctag_first
c_code local build_ref_1127
	ret_reg &ref[1127]
	call_c   Dyam_Create_Atom("lctag_first")
	move_ret ref[1127]
	c_ret

;; TERM 1126: '*PROLOG-ITEM*'!'$ft'
c_code local build_ref_1126
	ret_reg &ref[1126]
	call_c   build_ref_176()
	call_c   Dyam_Create_Atom_Module("$ft",&ref[176])
	move_ret ref[1126]
	c_ret

;; TERM 176: '*PROLOG-ITEM*'
c_code local build_ref_176
	ret_reg &ref[176]
	call_c   Dyam_Create_Atom("*PROLOG-ITEM*")
	move_ret ref[176]
	c_ret

pl_code local fun3
	call_c   build_ref_68()
	call_c   Dyam_Unify_Item(&ref[68])
	fail_ret
	pl_jump  Follow_Cont(V(0))

;; TERM 67: '*PROLOG-FIRST*'(lctag_first(_B, '$skip', epsilon, [])) :> []
c_code local build_ref_67
	ret_reg &ref[67]
	call_c   build_ref_66()
	call_c   Dyam_Create_Binary(I(9),&ref[66],I(0))
	move_ret ref[67]
	c_ret

;; TERM 66: '*PROLOG-FIRST*'(lctag_first(_B, '$skip', epsilon, []))
c_code local build_ref_66
	ret_reg &ref[66]
	call_c   build_ref_64()
	call_c   build_ref_65()
	call_c   Dyam_Create_Unary(&ref[64],&ref[65])
	move_ret ref[66]
	c_ret

;; TERM 64: '*PROLOG-FIRST*'
c_code local build_ref_64
	ret_reg &ref[64]
	call_c   Dyam_Create_Atom("*PROLOG-FIRST*")
	move_ret ref[64]
	c_ret

;; TERM 63: '*PROLOG_FIRST*'
c_code local build_ref_63
	ret_reg &ref[63]
	call_c   Dyam_Create_Atom("*PROLOG_FIRST*")
	move_ret ref[63]
	c_ret

c_code local build_seed_3
	ret_reg &seed[3]
	call_c   build_ref_63()
	call_c   build_ref_71()
	call_c   Dyam_Seed_Start(&ref[63],&ref[71],I(0),fun0,1)
	call_c   build_ref_72()
	call_c   Dyam_Seed_Add_Comp(&ref[72],fun4,0)
	call_c   Dyam_Seed_End()
	move_ret seed[3]
	c_ret

;; TERM 72: '*PROLOG-ITEM*'{top=> lctag_first(_B, [], epsilon, []), cont=> _A}
c_code local build_ref_72
	ret_reg &ref[72]
	call_c   build_ref_1126()
	call_c   build_ref_69()
	call_c   Dyam_Create_Binary(&ref[1126],&ref[69],V(0))
	move_ret ref[72]
	c_ret

;; TERM 69: lctag_first(_B, [], epsilon, [])
c_code local build_ref_69
	ret_reg &ref[69]
	call_c   build_ref_1127()
	call_c   build_ref_305()
	call_c   Dyam_Term_Start(&ref[1127],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(&ref[305])
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_End()
	move_ret ref[69]
	c_ret

pl_code local fun4
	call_c   build_ref_72()
	call_c   Dyam_Unify_Item(&ref[72])
	fail_ret
	pl_jump  Follow_Cont(V(0))

;; TERM 71: '*PROLOG-FIRST*'(lctag_first(_B, [], epsilon, [])) :> []
c_code local build_ref_71
	ret_reg &ref[71]
	call_c   build_ref_70()
	call_c   Dyam_Create_Binary(I(9),&ref[70],I(0))
	move_ret ref[71]
	c_ret

;; TERM 70: '*PROLOG-FIRST*'(lctag_first(_B, [], epsilon, []))
c_code local build_ref_70
	ret_reg &ref[70]
	call_c   build_ref_64()
	call_c   build_ref_69()
	call_c   Dyam_Create_Unary(&ref[64],&ref[69])
	move_ret ref[70]
	c_ret

c_code local build_seed_7
	ret_reg &seed[7]
	call_c   build_ref_54()
	call_c   build_ref_81()
	call_c   Dyam_Seed_Start(&ref[54],&ref[81],I(0),fun0,1)
	call_c   build_ref_80()
	call_c   Dyam_Seed_Add_Comp(&ref[80],fun7,0)
	call_c   Dyam_Seed_End()
	move_ret seed[7]
	c_ret

;; TERM 80: '*GUARD*'(alt_optimize_topbot_args([], [], _B, _B))
c_code local build_ref_80
	ret_reg &ref[80]
	call_c   build_ref_55()
	call_c   build_ref_79()
	call_c   Dyam_Create_Unary(&ref[55],&ref[79])
	move_ret ref[80]
	c_ret

;; TERM 79: alt_optimize_topbot_args([], [], _B, _B)
c_code local build_ref_79
	ret_reg &ref[79]
	call_c   build_ref_1128()
	call_c   Dyam_Term_Start(&ref[1128],4)
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_End()
	move_ret ref[79]
	c_ret

;; TERM 1128: alt_optimize_topbot_args
c_code local build_ref_1128
	ret_reg &ref[1128]
	call_c   Dyam_Create_Atom("alt_optimize_topbot_args")
	move_ret ref[1128]
	c_ret

pl_code local fun7
	call_c   build_ref_80()
	call_c   Dyam_Unify_Item(&ref[80])
	fail_ret
	pl_ret

;; TERM 81: '*GUARD*'(alt_optimize_topbot_args([], [], _B, _B)) :> []
c_code local build_ref_81
	ret_reg &ref[81]
	call_c   build_ref_80()
	call_c   Dyam_Create_Binary(I(9),&ref[80],I(0))
	move_ret ref[81]
	c_ret

c_code local build_seed_0
	ret_reg &seed[0]
	call_c   build_ref_54()
	call_c   build_ref_84()
	call_c   Dyam_Seed_Start(&ref[54],&ref[84],I(0),fun0,1)
	call_c   build_ref_83()
	call_c   Dyam_Seed_Add_Comp(&ref[83],fun8,0)
	call_c   Dyam_Seed_End()
	move_ret seed[0]
	c_ret

;; TERM 83: '*GUARD*'(sorted_tree_add(_B, [], [_B]))
c_code local build_ref_83
	ret_reg &ref[83]
	call_c   build_ref_55()
	call_c   build_ref_82()
	call_c   Dyam_Create_Unary(&ref[55],&ref[82])
	move_ret ref[83]
	c_ret

;; TERM 82: sorted_tree_add(_B, [], [_B])
c_code local build_ref_82
	ret_reg &ref[82]
	call_c   build_ref_1129()
	call_c   build_ref_824()
	call_c   Dyam_Term_Start(&ref[1129],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(&ref[824])
	call_c   Dyam_Term_End()
	move_ret ref[82]
	c_ret

;; TERM 1129: sorted_tree_add
c_code local build_ref_1129
	ret_reg &ref[1129]
	call_c   Dyam_Create_Atom("sorted_tree_add")
	move_ret ref[1129]
	c_ret

pl_code local fun8
	call_c   build_ref_83()
	call_c   Dyam_Unify_Item(&ref[83])
	fail_ret
	pl_ret

;; TERM 84: '*GUARD*'(sorted_tree_add(_B, [], [_B])) :> []
c_code local build_ref_84
	ret_reg &ref[84]
	call_c   build_ref_83()
	call_c   Dyam_Create_Binary(I(9),&ref[83],I(0))
	move_ret ref[84]
	c_ret

c_code local build_seed_2
	ret_reg &seed[2]
	call_c   build_ref_63()
	call_c   build_ref_88()
	call_c   Dyam_Seed_Start(&ref[63],&ref[88],I(0),fun0,1)
	call_c   build_ref_89()
	call_c   Dyam_Seed_Add_Comp(&ref[89],fun9,0)
	call_c   Dyam_Seed_End()
	move_ret seed[2]
	c_ret

;; TERM 89: '*PROLOG-ITEM*'{top=> lctag_first(_B, '$pos'(_C), epsilon, []), cont=> _A}
c_code local build_ref_89
	ret_reg &ref[89]
	call_c   build_ref_1126()
	call_c   build_ref_86()
	call_c   Dyam_Create_Binary(&ref[1126],&ref[86],V(0))
	move_ret ref[89]
	c_ret

;; TERM 86: lctag_first(_B, '$pos'(_C), epsilon, [])
c_code local build_ref_86
	ret_reg &ref[86]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1130()
	call_c   Dyam_Create_Unary(&ref[1130],V(2))
	move_ret R(0)
	call_c   build_ref_1127()
	call_c   build_ref_305()
	call_c   Dyam_Term_Start(&ref[1127],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[305])
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_End()
	move_ret ref[86]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1130: '$pos'
c_code local build_ref_1130
	ret_reg &ref[1130]
	call_c   Dyam_Create_Atom("$pos")
	move_ret ref[1130]
	c_ret

pl_code local fun9
	call_c   build_ref_89()
	call_c   Dyam_Unify_Item(&ref[89])
	fail_ret
	pl_jump  Follow_Cont(V(0))

;; TERM 88: '*PROLOG-FIRST*'(lctag_first(_B, '$pos'(_C), epsilon, [])) :> []
c_code local build_ref_88
	ret_reg &ref[88]
	call_c   build_ref_87()
	call_c   Dyam_Create_Binary(I(9),&ref[87],I(0))
	move_ret ref[88]
	c_ret

;; TERM 87: '*PROLOG-FIRST*'(lctag_first(_B, '$pos'(_C), epsilon, []))
c_code local build_ref_87
	ret_reg &ref[87]
	call_c   build_ref_64()
	call_c   build_ref_86()
	call_c   Dyam_Create_Unary(&ref[64],&ref[86])
	move_ret ref[87]
	c_ret

c_code local build_seed_58
	ret_reg &seed[58]
	call_c   build_ref_93()
	call_c   build_ref_103()
	call_c   Dyam_Seed_Start(&ref[93],&ref[103],I(0),fun21,1)
	call_c   build_ref_104()
	call_c   Dyam_Seed_Add_Comp(&ref[104],fun20,0)
	call_c   Dyam_Seed_End()
	move_ret seed[58]
	c_ret

;; TERM 104: '*CITEM*'(potential_tig(_B, left), _A)
c_code local build_ref_104
	ret_reg &ref[104]
	call_c   build_ref_97()
	call_c   build_ref_101()
	call_c   Dyam_Create_Binary(&ref[97],&ref[101],V(0))
	move_ret ref[104]
	c_ret

;; TERM 101: potential_tig(_B, left)
c_code local build_ref_101
	ret_reg &ref[101]
	call_c   build_ref_1131()
	call_c   build_ref_451()
	call_c   Dyam_Create_Binary(&ref[1131],V(1),&ref[451])
	move_ret ref[101]
	c_ret

;; TERM 451: left
c_code local build_ref_451
	ret_reg &ref[451]
	call_c   Dyam_Create_Atom("left")
	move_ret ref[451]
	c_ret

;; TERM 1131: potential_tig
c_code local build_ref_1131
	ret_reg &ref[1131]
	call_c   Dyam_Create_Atom("potential_tig")
	move_ret ref[1131]
	c_ret

;; TERM 97: '*CITEM*'
c_code local build_ref_97
	ret_reg &ref[97]
	call_c   Dyam_Create_Atom("*CITEM*")
	move_ret ref[97]
	c_ret

c_code local build_seed_65
	ret_reg &seed[65]
	call_c   build_ref_0()
	call_c   build_ref_99()
	call_c   Dyam_Seed_Start(&ref[0],&ref[99],&ref[99],fun11,1)
	call_c   build_ref_100()
	call_c   Dyam_Seed_Add_Comp(&ref[100],&ref[99],0)
	call_c   Dyam_Seed_End()
	move_ret seed[65]
	c_ret

pl_code local fun11
	pl_jump  Complete(0,0)

;; TERM 100: '*RITEM*'(_A, voidret) :> '$$HOLE$$'
c_code local build_ref_100
	ret_reg &ref[100]
	call_c   build_ref_99()
	call_c   Dyam_Create_Binary(I(9),&ref[99],I(7))
	move_ret ref[100]
	c_ret

;; TERM 99: '*RITEM*'(_A, voidret)
c_code local build_ref_99
	ret_reg &ref[99]
	call_c   build_ref_0()
	call_c   build_ref_21()
	call_c   Dyam_Create_Binary(&ref[0],V(0),&ref[21])
	move_ret ref[99]
	c_ret

;; TERM 21: voidret
c_code local build_ref_21
	ret_reg &ref[21]
	call_c   Dyam_Create_Atom("voidret")
	move_ret ref[21]
	c_ret

;; TERM 0: '*RITEM*'
c_code local build_ref_0
	ret_reg &ref[0]
	call_c   Dyam_Create_Atom("*RITEM*")
	move_ret ref[0]
	c_ret

pl_code local fun19
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Choice(fun18)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Subsume(R(0))
	fail_ret
	call_c   Dyam_Cut()
	pl_ret

long local pool_fun20[3]=[2,build_ref_104,build_seed_65]

pl_code local fun20
	call_c   Dyam_Pool(pool_fun20)
	call_c   Dyam_Unify_Item(&ref[104])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(1))
	pl_call  pred_left_potential_tig_1()
	call_c   Dyam_Deallocate()
	pl_jump  fun19(&seed[65],2)

pl_code local fun21
	pl_jump  Apply(0,0)

;; TERM 103: '*FIRST*'(potential_tig(_B, left)) :> []
c_code local build_ref_103
	ret_reg &ref[103]
	call_c   build_ref_102()
	call_c   Dyam_Create_Binary(I(9),&ref[102],I(0))
	move_ret ref[103]
	c_ret

;; TERM 102: '*FIRST*'(potential_tig(_B, left))
c_code local build_ref_102
	ret_reg &ref[102]
	call_c   build_ref_93()
	call_c   build_ref_101()
	call_c   Dyam_Create_Unary(&ref[93],&ref[101])
	move_ret ref[102]
	c_ret

;; TERM 93: '*FIRST*'
c_code local build_ref_93
	ret_reg &ref[93]
	call_c   Dyam_Create_Atom("*FIRST*")
	move_ret ref[93]
	c_ret

c_code local build_seed_57
	ret_reg &seed[57]
	call_c   build_ref_93()
	call_c   build_ref_96()
	call_c   Dyam_Seed_Start(&ref[93],&ref[96],I(0),fun21,1)
	call_c   build_ref_98()
	call_c   Dyam_Seed_Add_Comp(&ref[98],fun22,0)
	call_c   Dyam_Seed_End()
	move_ret seed[57]
	c_ret

;; TERM 98: '*CITEM*'(potential_tig(_B, right), _A)
c_code local build_ref_98
	ret_reg &ref[98]
	call_c   build_ref_97()
	call_c   build_ref_94()
	call_c   Dyam_Create_Binary(&ref[97],&ref[94],V(0))
	move_ret ref[98]
	c_ret

;; TERM 94: potential_tig(_B, right)
c_code local build_ref_94
	ret_reg &ref[94]
	call_c   build_ref_1131()
	call_c   build_ref_458()
	call_c   Dyam_Create_Binary(&ref[1131],V(1),&ref[458])
	move_ret ref[94]
	c_ret

;; TERM 458: right
c_code local build_ref_458
	ret_reg &ref[458]
	call_c   Dyam_Create_Atom("right")
	move_ret ref[458]
	c_ret

long local pool_fun22[3]=[2,build_ref_98,build_seed_65]

pl_code local fun22
	call_c   Dyam_Pool(pool_fun22)
	call_c   Dyam_Unify_Item(&ref[98])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(1))
	pl_call  pred_right_potential_tig_1()
	call_c   Dyam_Deallocate()
	pl_jump  fun19(&seed[65],2)

;; TERM 96: '*FIRST*'(potential_tig(_B, right)) :> []
c_code local build_ref_96
	ret_reg &ref[96]
	call_c   build_ref_95()
	call_c   Dyam_Create_Binary(I(9),&ref[95],I(0))
	move_ret ref[96]
	c_ret

;; TERM 95: '*FIRST*'(potential_tig(_B, right))
c_code local build_ref_95
	ret_reg &ref[95]
	call_c   build_ref_93()
	call_c   build_ref_94()
	call_c   Dyam_Create_Unary(&ref[93],&ref[94])
	move_ret ref[95]
	c_ret

c_code local build_seed_5
	ret_reg &seed[5]
	call_c   build_ref_54()
	call_c   build_ref_92()
	call_c   Dyam_Seed_Start(&ref[54],&ref[92],I(0),fun0,1)
	call_c   build_ref_91()
	call_c   Dyam_Seed_Add_Comp(&ref[91],fun10,0)
	call_c   Dyam_Seed_End()
	move_ret seed[5]
	c_ret

;; TERM 91: '*GUARD*'(lctag_simplify(_B, '$pos'(_C), keep, '$pos'(_C)))
c_code local build_ref_91
	ret_reg &ref[91]
	call_c   build_ref_55()
	call_c   build_ref_90()
	call_c   Dyam_Create_Unary(&ref[55],&ref[90])
	move_ret ref[91]
	c_ret

;; TERM 90: lctag_simplify(_B, '$pos'(_C), keep, '$pos'(_C))
c_code local build_ref_90
	ret_reg &ref[90]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1130()
	call_c   Dyam_Create_Unary(&ref[1130],V(2))
	move_ret R(0)
	call_c   build_ref_1124()
	call_c   build_ref_300()
	call_c   Dyam_Term_Start(&ref[1124],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[300])
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_End()
	move_ret ref[90]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

pl_code local fun10
	call_c   build_ref_91()
	call_c   Dyam_Unify_Item(&ref[91])
	fail_ret
	pl_ret

;; TERM 92: '*GUARD*'(lctag_simplify(_B, '$pos'(_C), keep, '$pos'(_C))) :> []
c_code local build_ref_92
	ret_reg &ref[92]
	call_c   build_ref_91()
	call_c   Dyam_Create_Binary(I(9),&ref[91],I(0))
	move_ret ref[92]
	c_ret

c_code local build_seed_46
	ret_reg &seed[46]
	call_c   build_ref_54()
	call_c   build_ref_107()
	call_c   Dyam_Seed_Start(&ref[54],&ref[107],I(0),fun0,1)
	call_c   build_ref_106()
	call_c   Dyam_Seed_Add_Comp(&ref[106],fun12,0)
	call_c   Dyam_Seed_End()
	move_ret seed[46]
	c_ret

;; TERM 106: '*GUARD*'(directive(max_tig_adj(_B), _C, _D, _E))
c_code local build_ref_106
	ret_reg &ref[106]
	call_c   build_ref_55()
	call_c   build_ref_105()
	call_c   Dyam_Create_Unary(&ref[55],&ref[105])
	move_ret ref[106]
	c_ret

;; TERM 105: directive(max_tig_adj(_B), _C, _D, _E)
c_code local build_ref_105
	ret_reg &ref[105]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1133()
	call_c   Dyam_Create_Unary(&ref[1133],V(1))
	move_ret R(0)
	call_c   build_ref_1132()
	call_c   Dyam_Term_Start(&ref[1132],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[105]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1132: directive
c_code local build_ref_1132
	ret_reg &ref[1132]
	call_c   Dyam_Create_Atom("directive")
	move_ret ref[1132]
	c_ret

;; TERM 1133: max_tig_adj
c_code local build_ref_1133
	ret_reg &ref[1133]
	call_c   Dyam_Create_Atom("max_tig_adj")
	move_ret ref[1133]
	c_ret

pl_code local fun12
	call_c   build_ref_106()
	call_c   Dyam_Unify_Item(&ref[106])
	fail_ret
	pl_ret

;; TERM 107: '*GUARD*'(directive(max_tig_adj(_B), _C, _D, _E)) :> []
c_code local build_ref_107
	ret_reg &ref[107]
	call_c   build_ref_106()
	call_c   Dyam_Create_Binary(I(9),&ref[106],I(0))
	move_ret ref[107]
	c_ret

c_code local build_seed_47
	ret_reg &seed[47]
	call_c   build_ref_54()
	call_c   build_ref_110()
	call_c   Dyam_Seed_Start(&ref[54],&ref[110],I(0),fun0,1)
	call_c   build_ref_109()
	call_c   Dyam_Seed_Add_Comp(&ref[109],fun13,0)
	call_c   Dyam_Seed_End()
	move_ret seed[47]
	c_ret

;; TERM 109: '*GUARD*'(directive(no_empty_tig_trees, _B, _C, _D))
c_code local build_ref_109
	ret_reg &ref[109]
	call_c   build_ref_55()
	call_c   build_ref_108()
	call_c   Dyam_Create_Unary(&ref[55],&ref[108])
	move_ret ref[109]
	c_ret

;; TERM 108: directive(no_empty_tig_trees, _B, _C, _D)
c_code local build_ref_108
	ret_reg &ref[108]
	call_c   build_ref_1132()
	call_c   build_ref_111()
	call_c   Dyam_Term_Start(&ref[1132],4)
	call_c   Dyam_Term_Arg(&ref[111])
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[108]
	c_ret

;; TERM 111: no_empty_tig_trees
c_code local build_ref_111
	ret_reg &ref[111]
	call_c   Dyam_Create_Atom("no_empty_tig_trees")
	move_ret ref[111]
	c_ret

long local pool_fun13[3]=[2,build_ref_109,build_ref_111]

pl_code local fun13
	call_c   Dyam_Pool(pool_fun13)
	call_c   Dyam_Unify_Item(&ref[109])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[111], R(0)
	move     0, R(1)
	call_c   Dyam_Reg_Deallocate(1)
	pl_jump  pred_record_without_doublon_1()

;; TERM 110: '*GUARD*'(directive(no_empty_tig_trees, _B, _C, _D)) :> []
c_code local build_ref_110
	ret_reg &ref[110]
	call_c   build_ref_109()
	call_c   Dyam_Create_Binary(I(9),&ref[109],I(0))
	move_ret ref[110]
	c_ret

c_code local build_seed_28
	ret_reg &seed[28]
	call_c   build_ref_54()
	call_c   build_ref_114()
	call_c   Dyam_Seed_Start(&ref[54],&ref[114],I(0),fun0,1)
	call_c   build_ref_113()
	call_c   Dyam_Seed_Add_Comp(&ref[113],fun14,0)
	call_c   Dyam_Seed_End()
	move_ret seed[28]
	c_ret

;; TERM 113: '*GUARD*'(directive(lctag_safe(_B), _C, _D, _E))
c_code local build_ref_113
	ret_reg &ref[113]
	call_c   build_ref_55()
	call_c   build_ref_112()
	call_c   Dyam_Create_Unary(&ref[55],&ref[112])
	move_ret ref[113]
	c_ret

;; TERM 112: directive(lctag_safe(_B), _C, _D, _E)
c_code local build_ref_112
	ret_reg &ref[112]
	call_c   build_ref_1132()
	call_c   build_ref_115()
	call_c   Dyam_Term_Start(&ref[1132],4)
	call_c   Dyam_Term_Arg(&ref[115])
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[112]
	c_ret

;; TERM 115: lctag_safe(_B)
c_code local build_ref_115
	ret_reg &ref[115]
	call_c   build_ref_1134()
	call_c   Dyam_Create_Unary(&ref[1134],V(1))
	move_ret ref[115]
	c_ret

;; TERM 1134: lctag_safe
c_code local build_ref_1134
	ret_reg &ref[1134]
	call_c   Dyam_Create_Atom("lctag_safe")
	move_ret ref[1134]
	c_ret

long local pool_fun14[3]=[2,build_ref_113,build_ref_115]

pl_code local fun14
	call_c   Dyam_Pool(pool_fun14)
	call_c   Dyam_Unify_Item(&ref[113])
	fail_ret
	call_c   DYAM_evpred_assert_1(&ref[115])
	pl_ret

;; TERM 114: '*GUARD*'(directive(lctag_safe(_B), _C, _D, _E)) :> []
c_code local build_ref_114
	ret_reg &ref[114]
	call_c   build_ref_113()
	call_c   Dyam_Create_Binary(I(9),&ref[113],I(0))
	move_ret ref[114]
	c_ret

c_code local build_seed_50
	ret_reg &seed[50]
	call_c   build_ref_54()
	call_c   build_ref_118()
	call_c   Dyam_Seed_Start(&ref[54],&ref[118],I(0),fun0,1)
	call_c   build_ref_117()
	call_c   Dyam_Seed_Add_Comp(&ref[117],fun15,0)
	call_c   Dyam_Seed_End()
	move_ret seed[50]
	c_ret

;; TERM 117: '*GUARD*'(installer(adjkind(_B), _C, _D))
c_code local build_ref_117
	ret_reg &ref[117]
	call_c   build_ref_55()
	call_c   build_ref_116()
	call_c   Dyam_Create_Unary(&ref[55],&ref[116])
	move_ret ref[117]
	c_ret

;; TERM 116: installer(adjkind(_B), _C, _D)
c_code local build_ref_116
	ret_reg &ref[116]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1136()
	call_c   Dyam_Create_Unary(&ref[1136],V(1))
	move_ret R(0)
	call_c   build_ref_1135()
	call_c   Dyam_Term_Start(&ref[1135],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[116]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1135: installer
c_code local build_ref_1135
	ret_reg &ref[1135]
	call_c   Dyam_Create_Atom("installer")
	move_ret ref[1135]
	c_ret

;; TERM 1136: adjkind
c_code local build_ref_1136
	ret_reg &ref[1136]
	call_c   Dyam_Create_Atom("adjkind")
	move_ret ref[1136]
	c_ret

;; TERM 119: adjkind(_C, _B)
c_code local build_ref_119
	ret_reg &ref[119]
	call_c   build_ref_1136()
	call_c   Dyam_Create_Binary(&ref[1136],V(2),V(1))
	move_ret ref[119]
	c_ret

long local pool_fun15[3]=[2,build_ref_117,build_ref_119]

pl_code local fun15
	call_c   Dyam_Pool(pool_fun15)
	call_c   Dyam_Unify_Item(&ref[117])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[119], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Deallocate(1)
	pl_jump  pred_record_without_doublon_1()

;; TERM 118: '*GUARD*'(installer(adjkind(_B), _C, _D)) :> []
c_code local build_ref_118
	ret_reg &ref[118]
	call_c   build_ref_117()
	call_c   Dyam_Create_Binary(I(9),&ref[117],I(0))
	move_ret ref[118]
	c_ret

c_code local build_seed_48
	ret_reg &seed[48]
	call_c   build_ref_54()
	call_c   build_ref_124()
	call_c   Dyam_Seed_Start(&ref[54],&ref[124],I(0),fun0,1)
	call_c   build_ref_123()
	call_c   Dyam_Seed_Add_Comp(&ref[123],fun23,0)
	call_c   Dyam_Seed_End()
	move_ret seed[48]
	c_ret

;; TERM 123: '*GUARD*'(installer(adjrestr(_B, _C), _D, _E))
c_code local build_ref_123
	ret_reg &ref[123]
	call_c   build_ref_55()
	call_c   build_ref_122()
	call_c   Dyam_Create_Unary(&ref[55],&ref[122])
	move_ret ref[123]
	c_ret

;; TERM 122: installer(adjrestr(_B, _C), _D, _E)
c_code local build_ref_122
	ret_reg &ref[122]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1137()
	call_c   Dyam_Create_Binary(&ref[1137],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_1135()
	call_c   Dyam_Term_Start(&ref[1135],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[122]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1137: adjrestr
c_code local build_ref_1137
	ret_reg &ref[1137]
	call_c   Dyam_Create_Atom("adjrestr")
	move_ret ref[1137]
	c_ret

;; TERM 125: adjrestr(_D, _B, _C)
c_code local build_ref_125
	ret_reg &ref[125]
	call_c   build_ref_1137()
	call_c   Dyam_Term_Start(&ref[1137],3)
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_End()
	move_ret ref[125]
	c_ret

long local pool_fun23[3]=[2,build_ref_123,build_ref_125]

pl_code local fun23
	call_c   Dyam_Pool(pool_fun23)
	call_c   Dyam_Unify_Item(&ref[123])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[125], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Deallocate(1)
	pl_jump  pred_record_without_doublon_1()

;; TERM 124: '*GUARD*'(installer(adjrestr(_B, _C), _D, _E)) :> []
c_code local build_ref_124
	ret_reg &ref[124]
	call_c   build_ref_123()
	call_c   Dyam_Create_Binary(I(9),&ref[123],I(0))
	move_ret ref[124]
	c_ret

c_code local build_seed_16
	ret_reg &seed[16]
	call_c   build_ref_93()
	call_c   build_ref_128()
	call_c   Dyam_Seed_Start(&ref[93],&ref[128],I(0),fun21,1)
	call_c   build_ref_129()
	call_c   Dyam_Seed_Add_Comp(&ref[129],fun58,0)
	call_c   Dyam_Seed_End()
	move_ret seed[16]
	c_ret

;; TERM 129: '*CITEM*'('call_right_top_bot_constraints/3'(_B), _A)
c_code local build_ref_129
	ret_reg &ref[129]
	call_c   build_ref_97()
	call_c   build_ref_126()
	call_c   Dyam_Create_Binary(&ref[97],&ref[126],V(0))
	move_ret ref[129]
	c_ret

;; TERM 126: 'call_right_top_bot_constraints/3'(_B)
c_code local build_ref_126
	ret_reg &ref[126]
	call_c   build_ref_1138()
	call_c   Dyam_Create_Unary(&ref[1138],V(1))
	move_ret ref[126]
	c_ret

;; TERM 1138: 'call_right_top_bot_constraints/3'
c_code local build_ref_1138
	ret_reg &ref[1138]
	call_c   Dyam_Create_Atom("call_right_top_bot_constraints/3")
	move_ret ref[1138]
	c_ret

;; TERM 224: '$CLOSURE'('$fun'(57, 0, 1124684812), '$TUPPLE'(35144400634092))
c_code local build_ref_224
	ret_reg &ref[224]
	call_c   build_ref_223()
	call_c   Dyam_Closure_Aux(fun57,&ref[223])
	move_ret ref[224]
	c_ret

c_code local build_seed_77
	ret_reg &seed[77]
	call_c   build_ref_0()
	call_c   build_ref_152()
	call_c   Dyam_Seed_Start(&ref[0],&ref[152],&ref[152],fun11,1)
	call_c   build_ref_153()
	call_c   Dyam_Seed_Add_Comp(&ref[153],&ref[152],0)
	call_c   Dyam_Seed_End()
	move_ret seed[77]
	c_ret

;; TERM 153: '*RITEM*'(_A, return(_C, _D)) :> '$$HOLE$$'
c_code local build_ref_153
	ret_reg &ref[153]
	call_c   build_ref_152()
	call_c   Dyam_Create_Binary(I(9),&ref[152],I(7))
	move_ret ref[153]
	c_ret

;; TERM 152: '*RITEM*'(_A, return(_C, _D))
c_code local build_ref_152
	ret_reg &ref[152]
	call_c   build_ref_0()
	call_c   build_ref_6()
	call_c   Dyam_Create_Binary(&ref[0],V(0),&ref[6])
	move_ret ref[152]
	c_ret

;; TERM 6: return(_C, _D)
c_code local build_ref_6
	ret_reg &ref[6]
	call_c   build_ref_1139()
	call_c   Dyam_Create_Binary(&ref[1139],V(2),V(3))
	move_ret ref[6]
	c_ret

;; TERM 1139: return
c_code local build_ref_1139
	ret_reg &ref[1139]
	call_c   Dyam_Create_Atom("return")
	move_ret ref[1139]
	c_ret

pl_code local fun25
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Choice(fun24)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Subsume(R(0))
	fail_ret
	call_c   Dyam_Cut()
	pl_ret

long local pool_fun57[2]=[1,build_seed_77]

pl_code local fun57
	call_c   Dyam_Pool(pool_fun57)
	pl_jump  fun25(&seed[77],2)

;; TERM 223: '$TUPPLE'(35144400634092)
c_code local build_ref_223
	ret_reg &ref[223]
	call_c   Dyam_Create_Simple_Tupple(0,369098752)
	move_ret ref[223]
	c_ret

;; TERM 225: adjright(_B, _C, _D)
c_code local build_ref_225
	ret_reg &ref[225]
	call_c   build_ref_1140()
	call_c   Dyam_Term_Start(&ref[1140],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[225]
	c_ret

;; TERM 1140: adjright
c_code local build_ref_1140
	ret_reg &ref[1140]
	call_c   Dyam_Create_Atom("adjright")
	move_ret ref[1140]
	c_ret

long local pool_fun58[4]=[3,build_ref_129,build_ref_224,build_ref_225]

pl_code local fun58
	call_c   Dyam_Pool(pool_fun58)
	call_c   Dyam_Unify_Item(&ref[129])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[224], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	move     &ref[225], R(4)
	move     S(5), R(5)
	move     0, R(6)
	move     0, R(8)
	move     0, R(10)
	call_c   Dyam_Reg_Deallocate(6)
fun56:
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Reg_Bind(2,V(6))
	call_c   Dyam_Multi_Reg_Bind(4,2,4)
	pl_call  fun25(&seed[66],1)
	pl_call  fun55(&seed[67],2,V(6))
	call_c   Dyam_Deallocate()
	pl_ret


;; TERM 128: '*FIRST*'('call_right_top_bot_constraints/3'(_B)) :> []
c_code local build_ref_128
	ret_reg &ref[128]
	call_c   build_ref_127()
	call_c   Dyam_Create_Binary(I(9),&ref[127],I(0))
	move_ret ref[128]
	c_ret

;; TERM 127: '*FIRST*'('call_right_top_bot_constraints/3'(_B))
c_code local build_ref_127
	ret_reg &ref[127]
	call_c   build_ref_93()
	call_c   build_ref_126()
	call_c   Dyam_Create_Unary(&ref[93],&ref[126])
	move_ret ref[127]
	c_ret

c_code local build_seed_61
	ret_reg &seed[61]
	call_c   build_ref_63()
	call_c   build_ref_140()
	call_c   Dyam_Seed_Start(&ref[63],&ref[140],I(0),fun0,1)
	call_c   build_ref_141()
	call_c   Dyam_Seed_Add_Comp(&ref[141],fun48,0)
	call_c   Dyam_Seed_End()
	move_ret seed[61]
	c_ret

;; TERM 141: '*PROLOG-ITEM*'{top=> analyze_at_clause(tag_tree{family=> _B, name=> _C, tree=> _D}), cont=> _A}
c_code local build_ref_141
	ret_reg &ref[141]
	call_c   build_ref_1126()
	call_c   build_ref_138()
	call_c   Dyam_Create_Binary(&ref[1126],&ref[138],V(0))
	move_ret ref[141]
	c_ret

;; TERM 138: analyze_at_clause(tag_tree{family=> _B, name=> _C, tree=> _D})
c_code local build_ref_138
	ret_reg &ref[138]
	call_c   build_ref_1141()
	call_c   build_ref_205()
	call_c   Dyam_Create_Unary(&ref[1141],&ref[205])
	move_ret ref[138]
	c_ret

;; TERM 205: tag_tree{family=> _B, name=> _C, tree=> _D}
c_code local build_ref_205
	ret_reg &ref[205]
	call_c   build_ref_1142()
	call_c   Dyam_Term_Start(&ref[1142],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[205]
	c_ret

;; TERM 1142: tag_tree!'$ft'
c_code local build_ref_1142
	ret_reg &ref[1142]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1146()
	call_c   Dyam_Create_List(&ref[1146],I(0))
	move_ret R(0)
	call_c   build_ref_1145()
	call_c   Dyam_Create_List(&ref[1145],R(0))
	move_ret R(0)
	call_c   build_ref_1144()
	call_c   Dyam_Create_List(&ref[1144],R(0))
	move_ret R(0)
	call_c   build_ref_1143()
	call_c   Dyam_Create_Atom_Module("$ft",&ref[1143])
	move_ret ref[1142]
	call_c   DYAM_Feature_2(&ref[1143],R(0))
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1143: tag_tree
c_code local build_ref_1143
	ret_reg &ref[1143]
	call_c   Dyam_Create_Atom("tag_tree")
	move_ret ref[1143]
	c_ret

;; TERM 1144: family
c_code local build_ref_1144
	ret_reg &ref[1144]
	call_c   Dyam_Create_Atom("family")
	move_ret ref[1144]
	c_ret

;; TERM 1145: name
c_code local build_ref_1145
	ret_reg &ref[1145]
	call_c   Dyam_Create_Atom("name")
	move_ret ref[1145]
	c_ret

;; TERM 1146: tree
c_code local build_ref_1146
	ret_reg &ref[1146]
	call_c   Dyam_Create_Atom("tree")
	move_ret ref[1146]
	c_ret

;; TERM 1141: analyze_at_clause
c_code local build_ref_1141
	ret_reg &ref[1141]
	call_c   Dyam_Create_Atom("analyze_at_clause")
	move_ret ref[1141]
	c_ret

;; TERM 142: compiler_analyzer(lctag)
c_code local build_ref_142
	ret_reg &ref[142]
	call_c   build_ref_1147()
	call_c   build_ref_1120()
	call_c   Dyam_Create_Unary(&ref[1147],&ref[1120])
	move_ret ref[142]
	c_ret

;; TERM 1147: compiler_analyzer
c_code local build_ref_1147
	ret_reg &ref[1147]
	call_c   Dyam_Create_Atom("compiler_analyzer")
	move_ret ref[1147]
	c_ret

long local pool_fun48[4]=[3,build_ref_141,build_ref_142,build_ref_205]

pl_code local fun48
	call_c   Dyam_Pool(pool_fun48)
	call_c   Dyam_Unify_Item(&ref[141])
	fail_ret
	call_c   Dyam_Allocate(0)
	pl_call  Object_1(&ref[142])
	move     V(0), R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	move     &ref[205], R(4)
	move     S(5), R(5)
	move     0, R(6)
	call_c   Dyam_Reg_Deallocate(4)
fun46:
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Reg_Bind(2,V(4))
	call_c   Dyam_Multi_Reg_Bind(4,2,2)
	call_c   Dyam_Choice(fun45)
	pl_call  fun29(&seed[68],1)
	pl_fail


;; TERM 140: '*PROLOG-FIRST*'(analyze_at_clause(tag_tree{family=> _B, name=> _C, tree=> _D})) :> []
c_code local build_ref_140
	ret_reg &ref[140]
	call_c   build_ref_139()
	call_c   Dyam_Create_Binary(I(9),&ref[139],I(0))
	move_ret ref[140]
	c_ret

;; TERM 139: '*PROLOG-FIRST*'(analyze_at_clause(tag_tree{family=> _B, name=> _C, tree=> _D}))
c_code local build_ref_139
	ret_reg &ref[139]
	call_c   build_ref_64()
	call_c   build_ref_138()
	call_c   Dyam_Create_Unary(&ref[64],&ref[138])
	move_ret ref[139]
	c_ret

c_code local build_seed_27
	ret_reg &seed[27]
	call_c   build_ref_93()
	call_c   build_ref_149()
	call_c   Dyam_Seed_Start(&ref[93],&ref[149],I(0),fun21,1)
	call_c   build_ref_150()
	call_c   Dyam_Seed_Add_Comp(&ref[150],fun36,0)
	call_c   Dyam_Seed_End()
	move_ret seed[27]
	c_ret

;; TERM 150: '*CITEM*'('call_apply_feature_constraints/3'(_B), _A)
c_code local build_ref_150
	ret_reg &ref[150]
	call_c   build_ref_97()
	call_c   build_ref_147()
	call_c   Dyam_Create_Binary(&ref[97],&ref[147],V(0))
	move_ret ref[150]
	c_ret

;; TERM 147: 'call_apply_feature_constraints/3'(_B)
c_code local build_ref_147
	ret_reg &ref[147]
	call_c   build_ref_1148()
	call_c   Dyam_Create_Unary(&ref[1148],V(1))
	move_ret ref[147]
	c_ret

;; TERM 1148: 'call_apply_feature_constraints/3'
c_code local build_ref_1148
	ret_reg &ref[1148]
	call_c   Dyam_Create_Atom("call_apply_feature_constraints/3")
	move_ret ref[1148]
	c_ret

c_code local build_seed_69
	ret_reg &seed[69]
	call_c   build_ref_0()
	call_c   build_ref_152()
	call_c   Dyam_Seed_Start(&ref[0],&ref[152],&ref[152],fun11,1)
	call_c   build_ref_153()
	call_c   Dyam_Seed_Add_Comp(&ref[153],&ref[152],0)
	call_c   Dyam_Seed_End()
	move_ret seed[69]
	c_ret

long local pool_fun34[2]=[1,build_seed_69]

pl_code local fun35
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(2),V(1))
	fail_ret
	call_c   Dyam_Unify(V(3),V(1))
	fail_ret
fun34:
	call_c   Dyam_Deallocate()
	pl_jump  fun19(&seed[69],2)


;; TERM 151: tag_features(_B, _C, _D)
c_code local build_ref_151
	ret_reg &ref[151]
	call_c   build_ref_1149()
	call_c   Dyam_Term_Start(&ref[1149],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[151]
	c_ret

;; TERM 1149: tag_features
c_code local build_ref_1149
	ret_reg &ref[1149]
	call_c   Dyam_Create_Atom("tag_features")
	move_ret ref[1149]
	c_ret

long local pool_fun36[5]=[131074,build_ref_150,build_ref_151,pool_fun34,pool_fun34]

pl_code local fun36
	call_c   Dyam_Pool(pool_fun36)
	call_c   Dyam_Unify_Item(&ref[150])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun35)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[151])
	call_c   Dyam_Cut()
	pl_jump  fun34()

;; TERM 149: '*FIRST*'('call_apply_feature_constraints/3'(_B)) :> []
c_code local build_ref_149
	ret_reg &ref[149]
	call_c   build_ref_148()
	call_c   Dyam_Create_Binary(I(9),&ref[148],I(0))
	move_ret ref[149]
	c_ret

;; TERM 148: '*FIRST*'('call_apply_feature_constraints/3'(_B))
c_code local build_ref_148
	ret_reg &ref[148]
	call_c   build_ref_93()
	call_c   build_ref_147()
	call_c   Dyam_Create_Unary(&ref[93],&ref[147])
	move_ret ref[148]
	c_ret

c_code local build_seed_44
	ret_reg &seed[44]
	call_c   build_ref_93()
	call_c   build_ref_159()
	call_c   Dyam_Seed_Start(&ref[93],&ref[159],I(0),fun21,1)
	call_c   build_ref_160()
	call_c   Dyam_Seed_Add_Comp(&ref[160],fun33,0)
	call_c   Dyam_Seed_End()
	move_ret seed[44]
	c_ret

;; TERM 160: '*CITEM*'(show_tag_features, _A)
c_code local build_ref_160
	ret_reg &ref[160]
	call_c   build_ref_97()
	call_c   build_ref_44()
	call_c   Dyam_Create_Binary(&ref[97],&ref[44],V(0))
	move_ret ref[160]
	c_ret

;; TERM 44: show_tag_features
c_code local build_ref_44
	ret_reg &ref[44]
	call_c   Dyam_Create_Atom("show_tag_features")
	move_ret ref[44]
	c_ret

long local pool_fun31[2]=[1,build_seed_65]

pl_code local fun32
	call_c   Dyam_Remove_Choice()
fun31:
	call_c   Dyam_Deallocate()
	pl_jump  fun19(&seed[65],2)


;; TERM 161: 'TAG FEATURES nt=~w top=~w bot=~w\n'
c_code local build_ref_161
	ret_reg &ref[161]
	call_c   Dyam_Create_Atom("TAG FEATURES nt=~w top=~w bot=~w\n")
	move_ret ref[161]
	c_ret

;; TERM 162: [_B,_C,_D]
c_code local build_ref_162
	ret_reg &ref[162]
	call_c   Dyam_Create_Tupple(1,3,I(0))
	move_ret ref[162]
	c_ret

long local pool_fun33[6]=[65540,build_ref_160,build_ref_151,build_ref_161,build_ref_162,pool_fun31]

pl_code local fun33
	call_c   Dyam_Pool(pool_fun33)
	call_c   Dyam_Unify_Item(&ref[160])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun32)
	pl_call  Object_1(&ref[151])
	move     &ref[161], R(0)
	move     0, R(1)
	move     &ref[162], R(2)
	move     S(5), R(3)
	pl_call  pred_format_2()
	pl_fail

;; TERM 159: '*FIRST*'(show_tag_features) :> []
c_code local build_ref_159
	ret_reg &ref[159]
	call_c   build_ref_158()
	call_c   Dyam_Create_Binary(I(9),&ref[158],I(0))
	move_ret ref[159]
	c_ret

;; TERM 158: '*FIRST*'(show_tag_features)
c_code local build_ref_158
	ret_reg &ref[158]
	call_c   build_ref_93()
	call_c   build_ref_44()
	call_c   Dyam_Create_Unary(&ref[93],&ref[44])
	move_ret ref[158]
	c_ret

c_code local build_seed_62
	ret_reg &seed[62]
	call_c   build_ref_63()
	call_c   build_ref_165()
	call_c   Dyam_Seed_Start(&ref[63],&ref[165],I(0),fun0,1)
	call_c   build_ref_166()
	call_c   Dyam_Seed_Add_Comp(&ref[166],fun47,0)
	call_c   Dyam_Seed_End()
	move_ret seed[62]
	c_ret

;; TERM 166: '*PROLOG-ITEM*'{top=> analyze_at_clause(tag_tree{family=> _B, name=> _C, tree=> (auxtree _D)}), cont=> _A}
c_code local build_ref_166
	ret_reg &ref[166]
	call_c   build_ref_1126()
	call_c   build_ref_163()
	call_c   Dyam_Create_Binary(&ref[1126],&ref[163],V(0))
	move_ret ref[166]
	c_ret

;; TERM 163: analyze_at_clause(tag_tree{family=> _B, name=> _C, tree=> (auxtree _D)})
c_code local build_ref_163
	ret_reg &ref[163]
	call_c   build_ref_1141()
	call_c   build_ref_204()
	call_c   Dyam_Create_Unary(&ref[1141],&ref[204])
	move_ret ref[163]
	c_ret

;; TERM 204: tag_tree{family=> _B, name=> _C, tree=> (auxtree _D)}
c_code local build_ref_204
	ret_reg &ref[204]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1150()
	call_c   Dyam_Create_Unary(&ref[1150],V(3))
	move_ret R(0)
	call_c   build_ref_1142()
	call_c   Dyam_Term_Start(&ref[1142],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_End()
	move_ret ref[204]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1150: auxtree
c_code local build_ref_1150
	ret_reg &ref[1150]
	call_c   Dyam_Create_Atom("auxtree")
	move_ret ref[1150]
	c_ret

;; TERM 167: compiler_analyzer(tag2tig)
c_code local build_ref_167
	ret_reg &ref[167]
	call_c   build_ref_1147()
	call_c   build_ref_1123()
	call_c   Dyam_Create_Unary(&ref[1147],&ref[1123])
	move_ret ref[167]
	c_ret

long local pool_fun47[4]=[3,build_ref_166,build_ref_167,build_ref_204]

pl_code local fun47
	call_c   Dyam_Pool(pool_fun47)
	call_c   Dyam_Unify_Item(&ref[166])
	fail_ret
	call_c   Dyam_Allocate(0)
	pl_call  Object_1(&ref[167])
	move     V(0), R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	move     &ref[204], R(4)
	move     S(5), R(5)
	move     0, R(6)
	call_c   Dyam_Reg_Deallocate(4)
	pl_jump  fun46()

;; TERM 165: '*PROLOG-FIRST*'(analyze_at_clause(tag_tree{family=> _B, name=> _C, tree=> (auxtree _D)})) :> []
c_code local build_ref_165
	ret_reg &ref[165]
	call_c   build_ref_164()
	call_c   Dyam_Create_Binary(I(9),&ref[164],I(0))
	move_ret ref[165]
	c_ret

;; TERM 164: '*PROLOG-FIRST*'(analyze_at_clause(tag_tree{family=> _B, name=> _C, tree=> (auxtree _D)}))
c_code local build_ref_164
	ret_reg &ref[164]
	call_c   build_ref_64()
	call_c   build_ref_163()
	call_c   Dyam_Create_Unary(&ref[64],&ref[163])
	move_ret ref[164]
	c_ret

c_code local build_seed_31
	ret_reg &seed[31]
	call_c   build_ref_54()
	call_c   build_ref_170()
	call_c   Dyam_Seed_Start(&ref[54],&ref[170],I(0),fun0,1)
	call_c   build_ref_169()
	call_c   Dyam_Seed_Add_Comp(&ref[169],fun37,0)
	call_c   Dyam_Seed_End()
	move_ret seed[31]
	c_ret

;; TERM 169: '*GUARD*'(directive(lctag(_B, _C, _D, _E), _F, _G, _H))
c_code local build_ref_169
	ret_reg &ref[169]
	call_c   build_ref_55()
	call_c   build_ref_168()
	call_c   Dyam_Create_Unary(&ref[55],&ref[168])
	move_ret ref[169]
	c_ret

;; TERM 168: directive(lctag(_B, _C, _D, _E), _F, _G, _H)
c_code local build_ref_168
	ret_reg &ref[168]
	call_c   build_ref_1132()
	call_c   build_ref_171()
	call_c   Dyam_Term_Start(&ref[1132],4)
	call_c   Dyam_Term_Arg(&ref[171])
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[168]
	c_ret

;; TERM 171: lctag(_B, _C, _D, _E)
c_code local build_ref_171
	ret_reg &ref[171]
	call_c   build_ref_1120()
	call_c   Dyam_Term_Start(&ref[1120],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[171]
	c_ret

long local pool_fun37[3]=[2,build_ref_169,build_ref_171]

pl_code local fun37
	call_c   Dyam_Pool(pool_fun37)
	call_c   Dyam_Unify_Item(&ref[169])
	fail_ret
	call_c   DYAM_evpred_assert_1(&ref[171])
	pl_ret

;; TERM 170: '*GUARD*'(directive(lctag(_B, _C, _D, _E), _F, _G, _H)) :> []
c_code local build_ref_170
	ret_reg &ref[170]
	call_c   build_ref_169()
	call_c   Dyam_Create_Binary(I(9),&ref[169],I(0))
	move_ret ref[170]
	c_ret

c_code local build_seed_13
	ret_reg &seed[13]
	call_c   build_ref_63()
	call_c   build_ref_174()
	call_c   Dyam_Seed_Start(&ref[63],&ref[174],I(0),fun0,1)
	call_c   build_ref_175()
	call_c   Dyam_Seed_Add_Comp(&ref[175],fun40,0)
	call_c   Dyam_Seed_End()
	move_ret seed[13]
	c_ret

;; TERM 175: '*PROLOG-ITEM*'{top=> lctag_first(_B, (_C ; _D), _E, _F), cont=> _A}
c_code local build_ref_175
	ret_reg &ref[175]
	call_c   build_ref_1126()
	call_c   build_ref_172()
	call_c   Dyam_Create_Binary(&ref[1126],&ref[172],V(0))
	move_ret ref[175]
	c_ret

;; TERM 172: lctag_first(_B, (_C ; _D), _E, _F)
c_code local build_ref_172
	ret_reg &ref[172]
	call_c   build_ref_1127()
	call_c   build_ref_667()
	call_c   Dyam_Term_Start(&ref[1127],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(&ref[667])
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[172]
	c_ret

;; TERM 667: _C ; _D
c_code local build_ref_667
	ret_reg &ref[667]
	call_c   Dyam_Create_Binary(I(5),V(2),V(3))
	move_ret ref[667]
	c_ret

c_code local build_seed_72
	ret_reg &seed[72]
	call_c   build_ref_176()
	call_c   build_ref_181()
	call_c   Dyam_Seed_Start(&ref[176],&ref[181],I(0),fun11,1)
	call_c   build_ref_184()
	call_c   Dyam_Seed_Add_Comp(&ref[184],&ref[181],0)
	call_c   Dyam_Seed_End()
	move_ret seed[72]
	c_ret

;; TERM 184: '*PROLOG-FIRST*'(lctag_first(_B, _D, _E, _F)) :> '$$HOLE$$'
c_code local build_ref_184
	ret_reg &ref[184]
	call_c   build_ref_183()
	call_c   Dyam_Create_Binary(I(9),&ref[183],I(7))
	move_ret ref[184]
	c_ret

;; TERM 183: '*PROLOG-FIRST*'(lctag_first(_B, _D, _E, _F))
c_code local build_ref_183
	ret_reg &ref[183]
	call_c   build_ref_64()
	call_c   build_ref_182()
	call_c   Dyam_Create_Unary(&ref[64],&ref[182])
	move_ret ref[183]
	c_ret

;; TERM 182: lctag_first(_B, _D, _E, _F)
c_code local build_ref_182
	ret_reg &ref[182]
	call_c   build_ref_1127()
	call_c   Dyam_Term_Start(&ref[1127],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[182]
	c_ret

;; TERM 181: '*PROLOG-ITEM*'{top=> lctag_first(_B, _D, _E, _F), cont=> _A}
c_code local build_ref_181
	ret_reg &ref[181]
	call_c   build_ref_1126()
	call_c   build_ref_182()
	call_c   Dyam_Create_Binary(&ref[1126],&ref[182],V(0))
	move_ret ref[181]
	c_ret

pl_code local fun38
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Light_Object(R(0))
	pl_jump  Schedule_Prolog()

long local pool_fun39[2]=[1,build_seed_72]

pl_code local fun39
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Deallocate()
	pl_jump  fun38(&seed[72],12)

c_code local build_seed_71
	ret_reg &seed[71]
	call_c   build_ref_176()
	call_c   build_ref_177()
	call_c   Dyam_Seed_Start(&ref[176],&ref[177],I(0),fun11,1)
	call_c   build_ref_180()
	call_c   Dyam_Seed_Add_Comp(&ref[180],&ref[177],0)
	call_c   Dyam_Seed_End()
	move_ret seed[71]
	c_ret

;; TERM 180: '*PROLOG-FIRST*'(lctag_first(_B, _C, _E, _F)) :> '$$HOLE$$'
c_code local build_ref_180
	ret_reg &ref[180]
	call_c   build_ref_179()
	call_c   Dyam_Create_Binary(I(9),&ref[179],I(7))
	move_ret ref[180]
	c_ret

;; TERM 179: '*PROLOG-FIRST*'(lctag_first(_B, _C, _E, _F))
c_code local build_ref_179
	ret_reg &ref[179]
	call_c   build_ref_64()
	call_c   build_ref_178()
	call_c   Dyam_Create_Unary(&ref[64],&ref[178])
	move_ret ref[179]
	c_ret

;; TERM 178: lctag_first(_B, _C, _E, _F)
c_code local build_ref_178
	ret_reg &ref[178]
	call_c   build_ref_1127()
	call_c   Dyam_Term_Start(&ref[1127],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[178]
	c_ret

;; TERM 177: '*PROLOG-ITEM*'{top=> lctag_first(_B, _C, _E, _F), cont=> _A}
c_code local build_ref_177
	ret_reg &ref[177]
	call_c   build_ref_1126()
	call_c   build_ref_178()
	call_c   Dyam_Create_Binary(&ref[1126],&ref[178],V(0))
	move_ret ref[177]
	c_ret

long local pool_fun40[4]=[65538,build_ref_175,build_seed_71,pool_fun39]

pl_code local fun40
	call_c   Dyam_Pool(pool_fun40)
	call_c   Dyam_Unify_Item(&ref[175])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun39)
	call_c   Dyam_Deallocate()
	pl_jump  fun38(&seed[71],12)

;; TERM 174: '*PROLOG-FIRST*'(lctag_first(_B, (_C ; _D), _E, _F)) :> []
c_code local build_ref_174
	ret_reg &ref[174]
	call_c   build_ref_173()
	call_c   Dyam_Create_Binary(I(9),&ref[173],I(0))
	move_ret ref[174]
	c_ret

;; TERM 173: '*PROLOG-FIRST*'(lctag_first(_B, (_C ; _D), _E, _F))
c_code local build_ref_173
	ret_reg &ref[173]
	call_c   build_ref_64()
	call_c   build_ref_172()
	call_c   Dyam_Create_Unary(&ref[64],&ref[172])
	move_ret ref[173]
	c_ret

c_code local build_seed_14
	ret_reg &seed[14]
	call_c   build_ref_63()
	call_c   build_ref_187()
	call_c   Dyam_Seed_Start(&ref[63],&ref[187],I(0),fun0,1)
	call_c   build_ref_188()
	call_c   Dyam_Seed_Add_Comp(&ref[188],fun41,0)
	call_c   Dyam_Seed_End()
	move_ret seed[14]
	c_ret

;; TERM 188: '*PROLOG-ITEM*'{top=> lctag_first(_B, (_C ## _D), _E, _F), cont=> _A}
c_code local build_ref_188
	ret_reg &ref[188]
	call_c   build_ref_1126()
	call_c   build_ref_185()
	call_c   Dyam_Create_Binary(&ref[1126],&ref[185],V(0))
	move_ret ref[188]
	c_ret

;; TERM 185: lctag_first(_B, (_C ## _D), _E, _F)
c_code local build_ref_185
	ret_reg &ref[185]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1151()
	call_c   Dyam_Create_Binary(&ref[1151],V(2),V(3))
	move_ret R(0)
	call_c   build_ref_1127()
	call_c   Dyam_Term_Start(&ref[1127],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[185]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1151: ##
c_code local build_ref_1151
	ret_reg &ref[1151]
	call_c   Dyam_Create_Atom("##")
	move_ret ref[1151]
	c_ret

long local pool_fun41[4]=[65538,build_ref_188,build_seed_71,pool_fun39]

pl_code local fun41
	call_c   Dyam_Pool(pool_fun41)
	call_c   Dyam_Unify_Item(&ref[188])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun39)
	call_c   Dyam_Deallocate()
	pl_jump  fun38(&seed[71],12)

;; TERM 187: '*PROLOG-FIRST*'(lctag_first(_B, (_C ## _D), _E, _F)) :> []
c_code local build_ref_187
	ret_reg &ref[187]
	call_c   build_ref_186()
	call_c   Dyam_Create_Binary(I(9),&ref[186],I(0))
	move_ret ref[187]
	c_ret

;; TERM 186: '*PROLOG-FIRST*'(lctag_first(_B, (_C ## _D), _E, _F))
c_code local build_ref_186
	ret_reg &ref[186]
	call_c   build_ref_64()
	call_c   build_ref_185()
	call_c   Dyam_Create_Unary(&ref[64],&ref[185])
	move_ret ref[186]
	c_ret

c_code local build_seed_26
	ret_reg &seed[26]
	call_c   build_ref_93()
	call_c   build_ref_191()
	call_c   Dyam_Seed_Start(&ref[93],&ref[191],I(0),fun21,1)
	call_c   build_ref_192()
	call_c   Dyam_Seed_Add_Comp(&ref[192],fun53,0)
	call_c   Dyam_Seed_End()
	move_ret seed[26]
	c_ret

;; TERM 192: '*CITEM*'('call_apply_feature_mode_constraints/4'(_B, _C), _A)
c_code local build_ref_192
	ret_reg &ref[192]
	call_c   build_ref_97()
	call_c   build_ref_189()
	call_c   Dyam_Create_Binary(&ref[97],&ref[189],V(0))
	move_ret ref[192]
	c_ret

;; TERM 189: 'call_apply_feature_mode_constraints/4'(_B, _C)
c_code local build_ref_189
	ret_reg &ref[189]
	call_c   build_ref_1152()
	call_c   Dyam_Create_Binary(&ref[1152],V(1),V(2))
	move_ret ref[189]
	c_ret

;; TERM 1152: 'call_apply_feature_mode_constraints/4'
c_code local build_ref_1152
	ret_reg &ref[1152]
	call_c   Dyam_Create_Atom("call_apply_feature_mode_constraints/4")
	move_ret ref[1152]
	c_ret

;; TERM 209: '$CLOSURE'('$fun'(51, 0, 1124621696), '$TUPPLE'(35144395402116))
c_code local build_ref_209
	ret_reg &ref[209]
	call_c   build_ref_208()
	call_c   Dyam_Closure_Aux(fun51,&ref[208])
	move_ret ref[209]
	c_ret

c_code local build_seed_73
	ret_reg &seed[73]
	call_c   build_ref_0()
	call_c   build_ref_195()
	call_c   Dyam_Seed_Start(&ref[0],&ref[195],&ref[195],fun11,1)
	call_c   build_ref_196()
	call_c   Dyam_Seed_Add_Comp(&ref[196],&ref[195],0)
	call_c   Dyam_Seed_End()
	move_ret seed[73]
	c_ret

;; TERM 196: '*RITEM*'(_A, return(_D, _E)) :> '$$HOLE$$'
c_code local build_ref_196
	ret_reg &ref[196]
	call_c   build_ref_195()
	call_c   Dyam_Create_Binary(I(9),&ref[195],I(7))
	move_ret ref[196]
	c_ret

;; TERM 195: '*RITEM*'(_A, return(_D, _E))
c_code local build_ref_195
	ret_reg &ref[195]
	call_c   build_ref_0()
	call_c   build_ref_194()
	call_c   Dyam_Create_Binary(&ref[0],V(0),&ref[194])
	move_ret ref[195]
	c_ret

;; TERM 194: return(_D, _E)
c_code local build_ref_194
	ret_reg &ref[194]
	call_c   build_ref_1139()
	call_c   Dyam_Create_Binary(&ref[1139],V(3),V(4))
	move_ret ref[194]
	c_ret

long local pool_fun42[2]=[1,build_seed_73]

pl_code local fun51
	call_c   Dyam_Pool(pool_fun42)
	call_c   Dyam_Allocate(0)
fun42:
	call_c   Dyam_Deallocate()
	pl_jump  fun19(&seed[73],2)


;; TERM 208: '$TUPPLE'(35144395402116)
c_code local build_ref_208
	ret_reg &ref[208]
	call_c   Dyam_Create_Simple_Tupple(0,318767104)
	move_ret ref[208]
	c_ret

long local pool_fun52[2]=[1,build_ref_209]

pl_code local fun52
	call_c   Dyam_Remove_Choice()
	move     &ref[209], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Load(4,V(1))
	move     V(3), R(6)
	move     S(5), R(7)
	move     V(4), R(8)
	move     S(5), R(9)
	call_c   Dyam_Reg_Deallocate(5)
fun50:
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Reg_Bind(2,V(5))
	call_c   Dyam_Multi_Reg_Bind(4,2,3)
	call_c   Dyam_Choice(fun49)
	pl_call  fun29(&seed[74],1)
	pl_fail


;; TERM 193: tag_features_mode(_B, _C, _D, _E)
c_code local build_ref_193
	ret_reg &ref[193]
	call_c   build_ref_1153()
	call_c   Dyam_Term_Start(&ref[1153],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[193]
	c_ret

;; TERM 1153: tag_features_mode
c_code local build_ref_1153
	ret_reg &ref[1153]
	call_c   Dyam_Create_Atom("tag_features_mode")
	move_ret ref[1153]
	c_ret

long local pool_fun53[5]=[131074,build_ref_192,build_ref_193,pool_fun52,pool_fun42]

pl_code local fun53
	call_c   Dyam_Pool(pool_fun53)
	call_c   Dyam_Unify_Item(&ref[192])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun52)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[193])
	call_c   Dyam_Cut()
	pl_jump  fun42()

;; TERM 191: '*FIRST*'('call_apply_feature_mode_constraints/4'(_B, _C)) :> []
c_code local build_ref_191
	ret_reg &ref[191]
	call_c   build_ref_190()
	call_c   Dyam_Create_Binary(I(9),&ref[190],I(0))
	move_ret ref[191]
	c_ret

;; TERM 190: '*FIRST*'('call_apply_feature_mode_constraints/4'(_B, _C))
c_code local build_ref_190
	ret_reg &ref[190]
	call_c   build_ref_93()
	call_c   build_ref_189()
	call_c   Dyam_Create_Unary(&ref[93],&ref[189])
	move_ret ref[190]
	c_ret

c_code local build_seed_53
	ret_reg &seed[53]
	call_c   build_ref_54()
	call_c   build_ref_258()
	call_c   Dyam_Seed_Start(&ref[54],&ref[258],I(0),fun0,1)
	call_c   build_ref_257()
	call_c   Dyam_Seed_Add_Comp(&ref[257],fun70,0)
	call_c   Dyam_Seed_End()
	move_ret seed[53]
	c_ret

;; TERM 257: '*GUARD*'(directive(tig(_B, _C), _D, _E, _F))
c_code local build_ref_257
	ret_reg &ref[257]
	call_c   build_ref_55()
	call_c   build_ref_256()
	call_c   Dyam_Create_Unary(&ref[55],&ref[256])
	move_ret ref[257]
	c_ret

;; TERM 256: directive(tig(_B, _C), _D, _E, _F)
c_code local build_ref_256
	ret_reg &ref[256]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1154()
	call_c   Dyam_Create_Binary(&ref[1154],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_1132()
	call_c   Dyam_Term_Start(&ref[1132],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[256]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1154: tig
c_code local build_ref_1154
	ret_reg &ref[1154]
	call_c   Dyam_Create_Atom("tig")
	move_ret ref[1154]
	c_ret

;; TERM 261: 'Not a valid TIG mode ~w'
c_code local build_ref_261
	ret_reg &ref[261]
	call_c   Dyam_Create_Atom("Not a valid TIG mode ~w")
	move_ret ref[261]
	c_ret

;; TERM 262: [tig(_B, _C)]
c_code local build_ref_262
	ret_reg &ref[262]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1154()
	call_c   Dyam_Create_Binary(&ref[1154],V(1),V(2))
	move_ret R(0)
	call_c   Dyam_Create_List(R(0),I(0))
	move_ret ref[262]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 260: tig(_C)
c_code local build_ref_260
	ret_reg &ref[260]
	call_c   build_ref_1154()
	call_c   Dyam_Create_Unary(&ref[1154],V(2))
	move_ret ref[260]
	c_ret

long local pool_fun69[4]=[3,build_ref_261,build_ref_262,build_ref_260]

pl_code local fun69
	call_c   Dyam_Remove_Choice()
	move     &ref[261], R(0)
	move     0, R(1)
	move     &ref[262], R(2)
	move     S(5), R(3)
	pl_call  pred_error_2()
fun68:
	move     &ref[260], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(1))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_install_2()


;; TERM 259: [left,right]
c_code local build_ref_259
	ret_reg &ref[259]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_458()
	call_c   Dyam_Create_List(&ref[458],I(0))
	move_ret R(0)
	call_c   build_ref_451()
	call_c   Dyam_Create_List(&ref[451],R(0))
	move_ret ref[259]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun70[5]=[65539,build_ref_257,build_ref_259,build_ref_260,pool_fun69]

pl_code local fun70
	call_c   Dyam_Pool(pool_fun70)
	call_c   Dyam_Unify_Item(&ref[257])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun69)
	call_c   Dyam_Set_Cut()
	pl_call  Domain_2(V(2),&ref[259])
	call_c   Dyam_Cut()
	pl_jump  fun68()

;; TERM 258: '*GUARD*'(directive(tig(_B, _C), _D, _E, _F)) :> []
c_code local build_ref_258
	ret_reg &ref[258]
	call_c   build_ref_257()
	call_c   Dyam_Create_Binary(I(9),&ref[257],I(0))
	move_ret ref[258]
	c_ret

c_code local build_seed_43
	ret_reg &seed[43]
	call_c   build_ref_93()
	call_c   build_ref_228()
	call_c   Dyam_Seed_Start(&ref[93],&ref[228],I(0),fun21,1)
	call_c   build_ref_229()
	call_c   Dyam_Seed_Add_Comp(&ref[229],fun67,0)
	call_c   Dyam_Seed_End()
	move_ret seed[43]
	c_ret

;; TERM 229: '*CITEM*'(make_true_tig_callret(_B, _C, _D, _E, _F, _G, _H), _A)
c_code local build_ref_229
	ret_reg &ref[229]
	call_c   build_ref_97()
	call_c   build_ref_226()
	call_c   Dyam_Create_Binary(&ref[97],&ref[226],V(0))
	move_ret ref[229]
	c_ret

;; TERM 226: make_true_tig_callret(_B, _C, _D, _E, _F, _G, _H)
c_code local build_ref_226
	ret_reg &ref[226]
	call_c   build_ref_1155()
	call_c   Dyam_Term_Start(&ref[1155],7)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[226]
	c_ret

;; TERM 1155: make_true_tig_callret
c_code local build_ref_1155
	ret_reg &ref[1155]
	call_c   Dyam_Create_Atom("make_true_tig_callret")
	move_ret ref[1155]
	c_ret

;; TERM 231: tag_modulation{subst_modulation=> _M, top_modulation=> nt_modulation{nt_pattern=> _N, left_pattern=> _O, right_pattern=> _P}, bot_modulation=> _Q}
c_code local build_ref_231
	ret_reg &ref[231]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1157()
	call_c   Dyam_Term_Start(&ref[1157],3)
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_1156()
	call_c   Dyam_Term_Start(&ref[1156],3)
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_End()
	move_ret ref[231]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1156: tag_modulation!'$ft'
c_code local build_ref_1156
	ret_reg &ref[1156]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1161()
	call_c   Dyam_Create_List(&ref[1161],I(0))
	move_ret R(0)
	call_c   build_ref_1160()
	call_c   Dyam_Create_List(&ref[1160],R(0))
	move_ret R(0)
	call_c   build_ref_1159()
	call_c   Dyam_Create_List(&ref[1159],R(0))
	move_ret R(0)
	call_c   build_ref_1158()
	call_c   Dyam_Create_Atom_Module("$ft",&ref[1158])
	move_ret ref[1156]
	call_c   DYAM_Feature_2(&ref[1158],R(0))
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1158: tag_modulation
c_code local build_ref_1158
	ret_reg &ref[1158]
	call_c   Dyam_Create_Atom("tag_modulation")
	move_ret ref[1158]
	c_ret

;; TERM 1159: subst_modulation
c_code local build_ref_1159
	ret_reg &ref[1159]
	call_c   Dyam_Create_Atom("subst_modulation")
	move_ret ref[1159]
	c_ret

;; TERM 1160: top_modulation
c_code local build_ref_1160
	ret_reg &ref[1160]
	call_c   Dyam_Create_Atom("top_modulation")
	move_ret ref[1160]
	c_ret

;; TERM 1161: bot_modulation
c_code local build_ref_1161
	ret_reg &ref[1161]
	call_c   Dyam_Create_Atom("bot_modulation")
	move_ret ref[1161]
	c_ret

;; TERM 1157: nt_modulation!'$ft'
c_code local build_ref_1157
	ret_reg &ref[1157]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1165()
	call_c   Dyam_Create_List(&ref[1165],I(0))
	move_ret R(0)
	call_c   build_ref_1164()
	call_c   Dyam_Create_List(&ref[1164],R(0))
	move_ret R(0)
	call_c   build_ref_1163()
	call_c   Dyam_Create_List(&ref[1163],R(0))
	move_ret R(0)
	call_c   build_ref_1162()
	call_c   Dyam_Create_Atom_Module("$ft",&ref[1162])
	move_ret ref[1157]
	call_c   DYAM_Feature_2(&ref[1162],R(0))
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1162: nt_modulation
c_code local build_ref_1162
	ret_reg &ref[1162]
	call_c   Dyam_Create_Atom("nt_modulation")
	move_ret ref[1162]
	c_ret

;; TERM 1163: nt_pattern
c_code local build_ref_1163
	ret_reg &ref[1163]
	call_c   Dyam_Create_Atom("nt_pattern")
	move_ret ref[1163]
	c_ret

;; TERM 1164: left_pattern
c_code local build_ref_1164
	ret_reg &ref[1164]
	call_c   Dyam_Create_Atom("left_pattern")
	move_ret ref[1164]
	c_ret

;; TERM 1165: right_pattern
c_code local build_ref_1165
	ret_reg &ref[1165]
	call_c   Dyam_Create_Atom("right_pattern")
	move_ret ref[1165]
	c_ret

;; TERM 254: +
c_code local build_ref_254
	ret_reg &ref[254]
	call_c   Dyam_Create_Atom("+")
	move_ret ref[254]
	c_ret

;; TERM 255: -
c_code local build_ref_255
	ret_reg &ref[255]
	call_c   Dyam_Create_Atom("-")
	move_ret ref[255]
	c_ret

;; TERM 252: [_N,_O,_P|_N]
c_code local build_ref_252
	ret_reg &ref[252]
	call_c   Dyam_Create_Tupple(13,15,V(13))
	move_ret ref[252]
	c_ret

;; TERM 235: [_U|_V]
c_code local build_ref_235
	ret_reg &ref[235]
	call_c   Dyam_Create_List(V(20),V(21))
	move_ret ref[235]
	c_ret

;; TERM 236: [_W,_E,_F|_V]
c_code local build_ref_236
	ret_reg &ref[236]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Tupple(4,5,V(21))
	move_ret R(0)
	call_c   Dyam_Create_List(V(22),R(0))
	move_ret ref[236]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 237: [_U|_Y]
c_code local build_ref_237
	ret_reg &ref[237]
	call_c   Dyam_Create_List(V(20),V(24))
	move_ret ref[237]
	c_ret

c_code local build_seed_78
	ret_reg &seed[78]
	call_c   build_ref_55()
	call_c   build_ref_239()
	call_c   Dyam_Seed_Start(&ref[55],&ref[239],I(0),fun11,1)
	call_c   build_ref_240()
	call_c   Dyam_Seed_Add_Comp(&ref[240],&ref[239],0)
	call_c   Dyam_Seed_End()
	move_ret seed[78]
	c_ret

;; TERM 240: '*GUARD*'(alt_optimize_topbot_args(_V, _Y, [], _Z)) :> '$$HOLE$$'
c_code local build_ref_240
	ret_reg &ref[240]
	call_c   build_ref_239()
	call_c   Dyam_Create_Binary(I(9),&ref[239],I(7))
	move_ret ref[240]
	c_ret

;; TERM 239: '*GUARD*'(alt_optimize_topbot_args(_V, _Y, [], _Z))
c_code local build_ref_239
	ret_reg &ref[239]
	call_c   build_ref_55()
	call_c   build_ref_238()
	call_c   Dyam_Create_Unary(&ref[55],&ref[238])
	move_ret ref[239]
	c_ret

;; TERM 238: alt_optimize_topbot_args(_V, _Y, [], _Z)
c_code local build_ref_238
	ret_reg &ref[238]
	call_c   build_ref_1128()
	call_c   Dyam_Term_Start(&ref[1128],4)
	call_c   Dyam_Term_Arg(V(21))
	call_c   Dyam_Term_Arg(V(24))
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(V(25))
	call_c   Dyam_Term_End()
	move_ret ref[238]
	c_ret

c_code local build_seed_79
	ret_reg &seed[79]
	call_c   build_ref_55()
	call_c   build_ref_242()
	call_c   Dyam_Seed_Start(&ref[55],&ref[242],I(0),fun11,1)
	call_c   build_ref_243()
	call_c   Dyam_Seed_Add_Comp(&ref[243],&ref[242],0)
	call_c   Dyam_Seed_End()
	move_ret seed[79]
	c_ret

;; TERM 243: '*GUARD*'(make_callret_with_pat(_T, _X, _G, _A1)) :> '$$HOLE$$'
c_code local build_ref_243
	ret_reg &ref[243]
	call_c   build_ref_242()
	call_c   Dyam_Create_Binary(I(9),&ref[242],I(7))
	move_ret ref[243]
	c_ret

;; TERM 242: '*GUARD*'(make_callret_with_pat(_T, _X, _G, _A1))
c_code local build_ref_242
	ret_reg &ref[242]
	call_c   build_ref_55()
	call_c   build_ref_241()
	call_c   Dyam_Create_Unary(&ref[55],&ref[241])
	move_ret ref[242]
	c_ret

;; TERM 241: make_callret_with_pat(_T, _X, _G, _A1)
c_code local build_ref_241
	ret_reg &ref[241]
	call_c   build_ref_1166()
	call_c   Dyam_Term_Start(&ref[1166],4)
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_Arg(V(23))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(26))
	call_c   Dyam_Term_End()
	move_ret ref[241]
	c_ret

;; TERM 1166: make_callret_with_pat
c_code local build_ref_1166
	ret_reg &ref[1166]
	call_c   Dyam_Create_Atom("make_callret_with_pat")
	move_ret ref[1166]
	c_ret

;; TERM 244: [_B1|_C1]
c_code local build_ref_244
	ret_reg &ref[244]
	call_c   Dyam_Create_List(V(27),V(28))
	move_ret ref[244]
	c_ret

c_code local build_seed_80
	ret_reg &seed[80]
	call_c   build_ref_55()
	call_c   build_ref_246()
	call_c   Dyam_Seed_Start(&ref[55],&ref[246],I(0),fun11,1)
	call_c   build_ref_247()
	call_c   Dyam_Seed_Add_Comp(&ref[247],&ref[246],0)
	call_c   Dyam_Seed_End()
	move_ret seed[80]
	c_ret

;; TERM 247: '*GUARD*'(append(_C1, _Z, _D1)) :> '$$HOLE$$'
c_code local build_ref_247
	ret_reg &ref[247]
	call_c   build_ref_246()
	call_c   Dyam_Create_Binary(I(9),&ref[246],I(7))
	move_ret ref[247]
	c_ret

;; TERM 246: '*GUARD*'(append(_C1, _Z, _D1))
c_code local build_ref_246
	ret_reg &ref[246]
	call_c   build_ref_55()
	call_c   build_ref_245()
	call_c   Dyam_Create_Unary(&ref[55],&ref[245])
	move_ret ref[246]
	c_ret

;; TERM 245: append(_C1, _Z, _D1)
c_code local build_ref_245
	ret_reg &ref[245]
	call_c   build_ref_1167()
	call_c   Dyam_Term_Start(&ref[1167],3)
	call_c   Dyam_Term_Arg(V(28))
	call_c   Dyam_Term_Arg(V(25))
	call_c   Dyam_Term_Arg(V(29))
	call_c   Dyam_Term_End()
	move_ret ref[245]
	c_ret

;; TERM 1167: append
c_code local build_ref_1167
	ret_reg &ref[1167]
	call_c   Dyam_Create_Atom("append")
	move_ret ref[1167]
	c_ret

;; TERM 248: [_B1|_D1]
c_code local build_ref_248
	ret_reg &ref[248]
	call_c   Dyam_Create_List(V(27),V(29))
	move_ret ref[248]
	c_ret

c_code local build_seed_81
	ret_reg &seed[81]
	call_c   build_ref_55()
	call_c   build_ref_250()
	call_c   Dyam_Seed_Start(&ref[55],&ref[250],I(0),fun11,1)
	call_c   build_ref_251()
	call_c   Dyam_Seed_Add_Comp(&ref[251],&ref[250],0)
	call_c   Dyam_Seed_End()
	move_ret seed[81]
	c_ret

;; TERM 251: '*GUARD*'(tig_viewer(_B, _C, _D, _E, _F, _G, _H)) :> '$$HOLE$$'
c_code local build_ref_251
	ret_reg &ref[251]
	call_c   build_ref_250()
	call_c   Dyam_Create_Binary(I(9),&ref[250],I(7))
	move_ret ref[251]
	c_ret

;; TERM 250: '*GUARD*'(tig_viewer(_B, _C, _D, _E, _F, _G, _H))
c_code local build_ref_250
	ret_reg &ref[250]
	call_c   build_ref_55()
	call_c   build_ref_249()
	call_c   Dyam_Create_Unary(&ref[55],&ref[249])
	move_ret ref[250]
	c_ret

;; TERM 249: tig_viewer(_B, _C, _D, _E, _F, _G, _H)
c_code local build_ref_249
	ret_reg &ref[249]
	call_c   build_ref_1168()
	call_c   Dyam_Term_Start(&ref[1168],7)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[249]
	c_ret

;; TERM 1168: tig_viewer
c_code local build_ref_1168
	ret_reg &ref[1168]
	call_c   Dyam_Create_Atom("tig_viewer")
	move_ret ref[1168]
	c_ret

long local pool_fun59[11]=[65545,build_ref_235,build_ref_236,build_ref_237,build_seed_78,build_seed_79,build_ref_244,build_seed_80,build_ref_248,build_seed_81,pool_fun31]

long local pool_fun60[3]=[65537,build_ref_252,pool_fun59]

pl_code local fun60
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(19),&ref[252])
	fail_ret
fun59:
	call_c   DYAM_evpred_univ(V(2),&ref[235])
	fail_ret
	call_c   Dyam_Reg_Load(0,V(20))
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(22), R(4)
	move     S(5), R(5)
	pl_call  pred_deep_module_shift_3()
	call_c   DYAM_evpred_univ(V(23),&ref[236])
	fail_ret
	call_c   DYAM_evpred_univ(V(3),&ref[237])
	fail_ret
	pl_call  fun38(&seed[78],1)
	pl_call  fun38(&seed[79],1)
	call_c   DYAM_evpred_univ(V(26),&ref[244])
	fail_ret
	pl_call  fun38(&seed[80],1)
	call_c   DYAM_evpred_univ(V(7),&ref[248])
	fail_ret
	call_c   Dyam_Choice(fun32)
	pl_call  fun38(&seed[81],1)
	pl_fail


;; TERM 233: [_R|_S]
c_code local build_ref_233
	ret_reg &ref[233]
	call_c   Dyam_Create_List(V(17),V(18))
	move_ret ref[233]
	c_ret

;; TERM 234: [_R,_O,_P|_S]
c_code local build_ref_234
	ret_reg &ref[234]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Tupple(14,15,V(18))
	move_ret R(0)
	call_c   Dyam_Create_List(V(17),R(0))
	move_ret ref[234]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun61[5]=[131074,build_ref_233,build_ref_234,pool_fun60,pool_fun59]

pl_code local fun61
	call_c   Dyam_Update_Choice(fun60)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_compound(V(13))
	fail_ret
	call_c   Dyam_Cut()
	call_c   DYAM_evpred_univ(V(13),&ref[233])
	fail_ret
	call_c   Dyam_Unify(V(19),&ref[234])
	fail_ret
	pl_jump  fun59()

long local pool_fun62[5]=[131074,build_ref_233,build_ref_234,pool_fun61,pool_fun59]

long local pool_fun63[4]=[65538,build_ref_254,build_ref_255,pool_fun62]

pl_code local fun63
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(13),&ref[254])
	fail_ret
	call_c   Dyam_Unify(V(14),&ref[254])
	fail_ret
	call_c   Dyam_Unify(V(15),&ref[255])
	fail_ret
fun62:
	call_c   Dyam_Choice(fun61)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(13),&ref[233])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(19),&ref[234])
	fail_ret
	pl_jump  fun59()


;; TERM 253: tag_modulation('*default*', _L)
c_code local build_ref_253
	ret_reg &ref[253]
	call_c   build_ref_1158()
	call_c   build_ref_1169()
	call_c   Dyam_Create_Binary(&ref[1158],&ref[1169],V(11))
	move_ret ref[253]
	c_ret

;; TERM 1169: '*default*'
c_code local build_ref_1169
	ret_reg &ref[1169]
	call_c   Dyam_Create_Atom("*default*")
	move_ret ref[1169]
	c_ret

long local pool_fun64[4]=[131073,build_ref_253,pool_fun63,pool_fun62]

pl_code local fun64
	call_c   Dyam_Update_Choice(fun63)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[253])
	call_c   Dyam_Cut()
	pl_jump  fun62()

;; TERM 232: tag_modulation((_I / _J), _L)
c_code local build_ref_232
	ret_reg &ref[232]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1170()
	call_c   Dyam_Create_Binary(&ref[1170],V(8),V(9))
	move_ret R(0)
	call_c   build_ref_1158()
	call_c   Dyam_Create_Binary(&ref[1158],R(0),V(11))
	move_ret ref[232]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1170: /
c_code local build_ref_1170
	ret_reg &ref[1170]
	call_c   Dyam_Create_Atom("/")
	move_ret ref[1170]
	c_ret

long local pool_fun65[5]=[131074,build_ref_231,build_ref_232,pool_fun64,pool_fun62]

pl_code local fun66
	call_c   Dyam_Remove_Choice()
fun65:
	call_c   Dyam_Unify(V(11),&ref[231])
	fail_ret
	call_c   Dyam_Choice(fun64)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[232])
	call_c   Dyam_Cut()
	pl_jump  fun62()


;; TERM 230: tag_features(_K, _C, _D)
c_code local build_ref_230
	ret_reg &ref[230]
	call_c   build_ref_1149()
	call_c   Dyam_Term_Start(&ref[1149],3)
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[230]
	c_ret

long local pool_fun67[5]=[131074,build_ref_229,build_ref_230,pool_fun65,pool_fun65]

pl_code local fun67
	call_c   Dyam_Pool(pool_fun67)
	call_c   Dyam_Unify_Item(&ref[229])
	fail_ret
	call_c   DYAM_evpred_functor(V(2),V(8),V(9))
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun66)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[230])
	call_c   Dyam_Cut()
	pl_jump  fun65()

;; TERM 228: '*FIRST*'(make_true_tig_callret(_B, _C, _D, _E, _F, _G, _H)) :> []
c_code local build_ref_228
	ret_reg &ref[228]
	call_c   build_ref_227()
	call_c   Dyam_Create_Binary(I(9),&ref[227],I(0))
	move_ret ref[228]
	c_ret

;; TERM 227: '*FIRST*'(make_true_tig_callret(_B, _C, _D, _E, _F, _G, _H))
c_code local build_ref_227
	ret_reg &ref[227]
	call_c   build_ref_93()
	call_c   build_ref_226()
	call_c   Dyam_Create_Unary(&ref[93],&ref[226])
	move_ret ref[227]
	c_ret

c_code local build_seed_51
	ret_reg &seed[51]
	call_c   build_ref_54()
	call_c   build_ref_274()
	call_c   Dyam_Seed_Start(&ref[54],&ref[274],I(0),fun0,1)
	call_c   build_ref_273()
	call_c   Dyam_Seed_Add_Comp(&ref[273],fun74,0)
	call_c   Dyam_Seed_End()
	move_ret seed[51]
	c_ret

;; TERM 273: '*GUARD*'(directive(adjkind(_B, _C), _D, _E, _F))
c_code local build_ref_273
	ret_reg &ref[273]
	call_c   build_ref_55()
	call_c   build_ref_272()
	call_c   Dyam_Create_Unary(&ref[55],&ref[272])
	move_ret ref[273]
	c_ret

;; TERM 272: directive(adjkind(_B, _C), _D, _E, _F)
c_code local build_ref_272
	ret_reg &ref[272]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1136()
	call_c   Dyam_Create_Binary(&ref[1136],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_1132()
	call_c   Dyam_Term_Start(&ref[1132],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[272]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 277: [adjkind(_B, _C)]
c_code local build_ref_277
	ret_reg &ref[277]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1136()
	call_c   Dyam_Create_Binary(&ref[1136],V(1),V(2))
	move_ret R(0)
	call_c   Dyam_Create_List(R(0),I(0))
	move_ret ref[277]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 276: adjkind(_C)
c_code local build_ref_276
	ret_reg &ref[276]
	call_c   build_ref_1136()
	call_c   Dyam_Create_Unary(&ref[1136],V(2))
	move_ret ref[276]
	c_ret

long local pool_fun73[4]=[3,build_ref_261,build_ref_277,build_ref_276]

pl_code local fun73
	call_c   Dyam_Remove_Choice()
	move     &ref[261], R(0)
	move     0, R(1)
	move     &ref[277], R(2)
	move     S(5), R(3)
	pl_call  pred_error_2()
fun72:
	move     &ref[276], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(1))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_install_2()


;; TERM 275: [left,right,wrap,no]
c_code local build_ref_275
	ret_reg &ref[275]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_706()
	call_c   Dyam_Create_List(&ref[706],I(0))
	move_ret R(0)
	call_c   build_ref_512()
	call_c   Dyam_Create_List(&ref[512],R(0))
	move_ret R(0)
	call_c   build_ref_458()
	call_c   Dyam_Create_List(&ref[458],R(0))
	move_ret R(0)
	call_c   build_ref_451()
	call_c   Dyam_Create_List(&ref[451],R(0))
	move_ret ref[275]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 512: wrap
c_code local build_ref_512
	ret_reg &ref[512]
	call_c   Dyam_Create_Atom("wrap")
	move_ret ref[512]
	c_ret

;; TERM 706: no
c_code local build_ref_706
	ret_reg &ref[706]
	call_c   Dyam_Create_Atom("no")
	move_ret ref[706]
	c_ret

long local pool_fun74[5]=[65539,build_ref_273,build_ref_275,build_ref_276,pool_fun73]

pl_code local fun74
	call_c   Dyam_Pool(pool_fun74)
	call_c   Dyam_Unify_Item(&ref[273])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun73)
	call_c   Dyam_Set_Cut()
	pl_call  Domain_2(V(2),&ref[275])
	call_c   Dyam_Cut()
	pl_jump  fun72()

;; TERM 274: '*GUARD*'(directive(adjkind(_B, _C), _D, _E, _F)) :> []
c_code local build_ref_274
	ret_reg &ref[274]
	call_c   build_ref_273()
	call_c   Dyam_Create_Binary(I(9),&ref[273],I(0))
	move_ret ref[274]
	c_ret

c_code local build_seed_52
	ret_reg &seed[52]
	call_c   build_ref_54()
	call_c   build_ref_280()
	call_c   Dyam_Seed_Start(&ref[54],&ref[280],I(0),fun0,1)
	call_c   build_ref_279()
	call_c   Dyam_Seed_Add_Comp(&ref[279],fun79,0)
	call_c   Dyam_Seed_End()
	move_ret seed[52]
	c_ret

;; TERM 279: '*GUARD*'(installer(tig(_B), (_C / 0), _D))
c_code local build_ref_279
	ret_reg &ref[279]
	call_c   build_ref_55()
	call_c   build_ref_278()
	call_c   Dyam_Create_Unary(&ref[55],&ref[278])
	move_ret ref[279]
	c_ret

;; TERM 278: installer(tig(_B), (_C / 0), _D)
c_code local build_ref_278
	ret_reg &ref[278]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_1154()
	call_c   Dyam_Create_Unary(&ref[1154],V(1))
	move_ret R(0)
	call_c   build_ref_1170()
	call_c   Dyam_Create_Binary(&ref[1170],V(2),N(0))
	move_ret R(1)
	call_c   build_ref_1135()
	call_c   Dyam_Term_Start(&ref[1135],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[278]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 284: tig(_C, _B)
c_code local build_ref_284
	ret_reg &ref[284]
	call_c   build_ref_1154()
	call_c   Dyam_Create_Binary(&ref[1154],V(2),V(1))
	move_ret ref[284]
	c_ret

pl_code local fun78
	call_c   Dyam_Remove_Choice()
fun75:
	call_c   DYAM_evpred_assert_1(&ref[284])
	call_c   Dyam_Deallocate()
	pl_ret


;; TERM 281: tig(_C, _E)
c_code local build_ref_281
	ret_reg &ref[281]
	call_c   build_ref_1154()
	call_c   Dyam_Create_Binary(&ref[1154],V(2),V(4))
	move_ret ref[281]
	c_ret

;; TERM 282: 'multiple TIG mode for tree ~w : ~w and ~w'
c_code local build_ref_282
	ret_reg &ref[282]
	call_c   Dyam_Create_Atom("multiple TIG mode for tree ~w : ~w and ~w")
	move_ret ref[282]
	c_ret

;; TERM 283: [_C,_B,_E]
c_code local build_ref_283
	ret_reg &ref[283]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(4),I(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(1),R(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(2),R(0))
	move_ret ref[283]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun76[4]=[3,build_ref_282,build_ref_283,build_ref_284]

pl_code local fun77
	call_c   Dyam_Remove_Choice()
fun76:
	call_c   Dyam_Cut()
	move     &ref[282], R(0)
	move     0, R(1)
	move     &ref[283], R(2)
	move     S(5), R(3)
	pl_call  pred_error_2()
	pl_jump  fun75()


long local pool_fun79[5]=[65539,build_ref_279,build_ref_284,build_ref_281,pool_fun76]

pl_code local fun79
	call_c   Dyam_Pool(pool_fun79)
	call_c   Dyam_Unify_Item(&ref[279])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun78)
	call_c   Dyam_Set_Cut()
	pl_call  Object_2(&ref[281],V(5))
	call_c   DYAM_evpred_retract(&ref[281])
	fail_ret
	call_c   Dyam_Choice(fun77)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(4),V(1))
	fail_ret
	call_c   Dyam_Cut()
	pl_fail

;; TERM 280: '*GUARD*'(installer(tig(_B), (_C / 0), _D)) :> []
c_code local build_ref_280
	ret_reg &ref[280]
	call_c   build_ref_279()
	call_c   Dyam_Create_Binary(I(9),&ref[279],I(0))
	move_ret ref[280]
	c_ret

c_code local build_seed_56
	ret_reg &seed[56]
	call_c   build_ref_63()
	call_c   build_ref_212()
	call_c   Dyam_Seed_Start(&ref[63],&ref[212],I(0),fun0,1)
	call_c   build_ref_213()
	call_c   Dyam_Seed_Add_Comp(&ref[213],fun152,0)
	call_c   Dyam_Seed_End()
	move_ret seed[56]
	c_ret

;; TERM 213: '*PROLOG-ITEM*'{top=> emit_at_pda, cont=> _A}
c_code local build_ref_213
	ret_reg &ref[213]
	call_c   build_ref_1126()
	call_c   build_ref_210()
	call_c   Dyam_Create_Binary(&ref[1126],&ref[210],V(0))
	move_ret ref[213]
	c_ret

;; TERM 210: emit_at_pda
c_code local build_ref_210
	ret_reg &ref[210]
	call_c   Dyam_Create_Atom("emit_at_pda")
	move_ret ref[210]
	c_ret

;; TERM 465: ':-adjkind(_,no).\n'
c_code local build_ref_465
	ret_reg &ref[465]
	call_c   Dyam_Create_Atom(":-adjkind(_,no).\n")
	move_ret ref[465]
	c_ret

pl_code local fun144
	call_c   Dyam_Remove_Choice()
fun133:
	move     &ref[465], R(0)
	move     0, R(1)
	move     I(0), R(2)
	move     0, R(3)
	pl_call  pred_format_2()
	call_c   DYAM_Nl_0()
	fail_ret
	call_c   DYAM_Set_Output_1(V(1))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_jump  Follow_Cont(V(0))


;; TERM 265: tag_nt(_I, (_K / _L))
c_code local build_ref_265
	ret_reg &ref[265]
	call_c   build_ref_1171()
	call_c   build_ref_466()
	call_c   Dyam_Create_Binary(&ref[1171],V(8),&ref[466])
	move_ret ref[265]
	c_ret

;; TERM 466: _K / _L
c_code local build_ref_466
	ret_reg &ref[466]
	call_c   build_ref_1170()
	call_c   Dyam_Create_Binary(&ref[1170],V(10),V(11))
	move_ret ref[466]
	c_ret

;; TERM 1171: tag_nt
c_code local build_ref_1171
	ret_reg &ref[1171]
	call_c   Dyam_Create_Atom("tag_nt")
	move_ret ref[1171]
	c_ret

pl_code local fun114
	call_c   Dyam_Remove_Choice()
	pl_fail

c_code local build_seed_100
	ret_reg &seed[100]
	call_c   build_ref_133()
	call_c   build_ref_463()
	call_c   Dyam_Seed_Start(&ref[133],&ref[463],I(0),fun21,1)
	call_c   build_ref_461()
	call_c   Dyam_Seed_Add_Comp(&ref[461],fun132,0)
	call_c   Dyam_Seed_End()
	move_ret seed[100]
	c_ret

;; TERM 461: '*RITEM*'('call_normalize/2'(_B1), return(tag_node{id=> _Q, label=> _I, children=> _R, kind=> _S, adj=> _T, spine=> _U, top=> _M, bot=> _V, token=> _W, adjleft=> _X, adjright=> _Y, adjwrap=> _Z}))
c_code local build_ref_461
	ret_reg &ref[461]
	call_c   build_ref_0()
	call_c   build_ref_460()
	call_c   build_ref_268()
	call_c   Dyam_Create_Binary(&ref[0],&ref[460],&ref[268])
	move_ret ref[461]
	c_ret

;; TERM 268: return(tag_node{id=> _Q, label=> _I, children=> _R, kind=> _S, adj=> _T, spine=> _U, top=> _M, bot=> _V, token=> _W, adjleft=> _X, adjright=> _Y, adjwrap=> _Z})
c_code local build_ref_268
	ret_reg &ref[268]
	call_c   build_ref_1139()
	call_c   build_ref_450()
	call_c   Dyam_Create_Unary(&ref[1139],&ref[450])
	move_ret ref[268]
	c_ret

;; TERM 450: tag_node{id=> _Q, label=> _I, children=> _R, kind=> _S, adj=> _T, spine=> _U, top=> _M, bot=> _V, token=> _W, adjleft=> _X, adjright=> _Y, adjwrap=> _Z}
c_code local build_ref_450
	ret_reg &ref[450]
	call_c   build_ref_1172()
	call_c   Dyam_Term_Start(&ref[1172],12)
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(21))
	call_c   Dyam_Term_Arg(V(22))
	call_c   Dyam_Term_Arg(V(23))
	call_c   Dyam_Term_Arg(V(24))
	call_c   Dyam_Term_Arg(V(25))
	call_c   Dyam_Term_End()
	move_ret ref[450]
	c_ret

;; TERM 1172: tag_node!'$ft'
c_code local build_ref_1172
	ret_reg &ref[1172]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1184()
	call_c   Dyam_Create_List(&ref[1184],I(0))
	move_ret R(0)
	call_c   build_ref_1140()
	call_c   Dyam_Create_List(&ref[1140],R(0))
	move_ret R(0)
	call_c   build_ref_1183()
	call_c   Dyam_Create_List(&ref[1183],R(0))
	move_ret R(0)
	call_c   build_ref_1182()
	call_c   Dyam_Create_List(&ref[1182],R(0))
	move_ret R(0)
	call_c   build_ref_1181()
	call_c   Dyam_Create_List(&ref[1181],R(0))
	move_ret R(0)
	call_c   build_ref_1180()
	call_c   Dyam_Create_List(&ref[1180],R(0))
	move_ret R(0)
	call_c   build_ref_1179()
	call_c   Dyam_Create_List(&ref[1179],R(0))
	move_ret R(0)
	call_c   build_ref_1178()
	call_c   Dyam_Create_List(&ref[1178],R(0))
	move_ret R(0)
	call_c   build_ref_1177()
	call_c   Dyam_Create_List(&ref[1177],R(0))
	move_ret R(0)
	call_c   build_ref_1176()
	call_c   Dyam_Create_List(&ref[1176],R(0))
	move_ret R(0)
	call_c   build_ref_1175()
	call_c   Dyam_Create_List(&ref[1175],R(0))
	move_ret R(0)
	call_c   build_ref_1174()
	call_c   Dyam_Create_List(&ref[1174],R(0))
	move_ret R(0)
	call_c   build_ref_1173()
	call_c   Dyam_Create_Atom_Module("$ft",&ref[1173])
	move_ret ref[1172]
	call_c   DYAM_Feature_2(&ref[1173],R(0))
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1173: tag_node
c_code local build_ref_1173
	ret_reg &ref[1173]
	call_c   Dyam_Create_Atom("tag_node")
	move_ret ref[1173]
	c_ret

;; TERM 1174: id
c_code local build_ref_1174
	ret_reg &ref[1174]
	call_c   Dyam_Create_Atom("id")
	move_ret ref[1174]
	c_ret

;; TERM 1175: label
c_code local build_ref_1175
	ret_reg &ref[1175]
	call_c   Dyam_Create_Atom("label")
	move_ret ref[1175]
	c_ret

;; TERM 1176: children
c_code local build_ref_1176
	ret_reg &ref[1176]
	call_c   Dyam_Create_Atom("children")
	move_ret ref[1176]
	c_ret

;; TERM 1177: kind
c_code local build_ref_1177
	ret_reg &ref[1177]
	call_c   Dyam_Create_Atom("kind")
	move_ret ref[1177]
	c_ret

;; TERM 1178: adj
c_code local build_ref_1178
	ret_reg &ref[1178]
	call_c   Dyam_Create_Atom("adj")
	move_ret ref[1178]
	c_ret

;; TERM 1179: spine
c_code local build_ref_1179
	ret_reg &ref[1179]
	call_c   Dyam_Create_Atom("spine")
	move_ret ref[1179]
	c_ret

;; TERM 1180: top
c_code local build_ref_1180
	ret_reg &ref[1180]
	call_c   Dyam_Create_Atom("top")
	move_ret ref[1180]
	c_ret

;; TERM 1181: bot
c_code local build_ref_1181
	ret_reg &ref[1181]
	call_c   Dyam_Create_Atom("bot")
	move_ret ref[1181]
	c_ret

;; TERM 1182: token
c_code local build_ref_1182
	ret_reg &ref[1182]
	call_c   Dyam_Create_Atom("token")
	move_ret ref[1182]
	c_ret

;; TERM 1183: adjleft
c_code local build_ref_1183
	ret_reg &ref[1183]
	call_c   Dyam_Create_Atom("adjleft")
	move_ret ref[1183]
	c_ret

;; TERM 1184: adjwrap
c_code local build_ref_1184
	ret_reg &ref[1184]
	call_c   Dyam_Create_Atom("adjwrap")
	move_ret ref[1184]
	c_ret

;; TERM 460: 'call_normalize/2'(_B1)
c_code local build_ref_460
	ret_reg &ref[460]
	call_c   build_ref_1185()
	call_c   Dyam_Create_Unary(&ref[1185],V(27))
	move_ret ref[460]
	c_ret

;; TERM 1185: 'call_normalize/2'
c_code local build_ref_1185
	ret_reg &ref[1185]
	call_c   Dyam_Create_Atom("call_normalize/2")
	move_ret ref[1185]
	c_ret

pl_code local fun132
	call_c   build_ref_461()
	call_c   Dyam_Unify_Item(&ref[461])
	fail_ret
	pl_ret

;; TERM 463: '*RITEM*'('call_normalize/2'(_B1), return(tag_node{id=> _Q, label=> _I, children=> _R, kind=> _S, adj=> _T, spine=> _U, top=> _M, bot=> _V, token=> _W, adjleft=> _X, adjright=> _Y, adjwrap=> _Z})) :> tig(7, '$TUPPLE'(35144395401924))
c_code local build_ref_463
	ret_reg &ref[463]
	call_c   build_ref_461()
	call_c   build_ref_462()
	call_c   Dyam_Create_Binary(I(9),&ref[461],&ref[462])
	move_ret ref[463]
	c_ret

;; TERM 462: tig(7, '$TUPPLE'(35144395401924))
c_code local build_ref_462
	ret_reg &ref[462]
	call_c   build_ref_1154()
	call_c   build_ref_719()
	call_c   Dyam_Create_Binary(&ref[1154],N(7),&ref[719])
	move_ret ref[462]
	c_ret

;; TERM 719: '$TUPPLE'(35144395401924)
c_code local build_ref_719
	ret_reg &ref[719]
	call_c   Dyam_Create_Simple_Tupple(0,268435456)
	move_ret ref[719]
	c_ret

;; TERM 133: '*CURNEXT*'
c_code local build_ref_133
	ret_reg &ref[133]
	call_c   Dyam_Create_Atom("*CURNEXT*")
	move_ret ref[133]
	c_ret

;; TERM 464: ':-adjkind(~k,wrap).\n'
c_code local build_ref_464
	ret_reg &ref[464]
	call_c   Dyam_Create_Atom(":-adjkind(~k,wrap).\n")
	move_ret ref[464]
	c_ret

;; TERM 453: [_O]
c_code local build_ref_453
	ret_reg &ref[453]
	call_c   Dyam_Create_List(V(14),I(0))
	move_ret ref[453]
	c_ret

long local pool_fun134[3]=[2,build_ref_464,build_ref_453]

pl_code local fun135
	call_c   Dyam_Remove_Choice()
fun134:
	call_c   Dyam_Cut()
	move     &ref[464], R(0)
	move     0, R(1)
	move     &ref[453], R(2)
	move     S(5), R(3)
	pl_call  pred_format_2()
	pl_fail


long local pool_fun136[4]=[65538,build_ref_450,build_ref_458,pool_fun134]

pl_code local fun137
	call_c   Dyam_Remove_Choice()
fun136:
	call_c   Dyam_Choice(fun135)
	call_c   Dyam_Set_Cut()
	move     &ref[450], R(0)
	move     S(5), R(1)
	move     &ref[458], R(2)
	move     0, R(3)
	pl_call  pred_tig_2()
	call_c   Dyam_Cut()
	pl_fail


long local pool_fun138[5]=[65539,build_seed_100,build_ref_450,build_ref_451,pool_fun136]

pl_code local fun139
	call_c   Dyam_Remove_Choice()
fun138:
	call_c   Dyam_Choice(fun114)
	call_c   Dyam_Choice(fun114)
	call_c   Dyam_Set_Cut()
	pl_call  fun38(&seed[100],2)
	call_c   Dyam_Choice(fun137)
	call_c   Dyam_Set_Cut()
	move     &ref[450], R(0)
	move     S(5), R(1)
	move     &ref[451], R(2)
	move     0, R(3)
	pl_call  pred_tig_2()
	call_c   Dyam_Cut()
	pl_fail


c_code local build_seed_99
	ret_reg &seed[99]
	call_c   build_ref_133()
	call_c   build_ref_457()
	call_c   Dyam_Seed_Start(&ref[133],&ref[457],I(0),fun21,1)
	call_c   build_ref_455()
	call_c   Dyam_Seed_Add_Comp(&ref[455],fun131,0)
	call_c   Dyam_Seed_End()
	move_ret seed[99]
	c_ret

;; TERM 455: '*RITEM*'('call_normalize/2'(_A1), return(tag_node{id=> _Q, label=> _I, children=> _R, kind=> _S, adj=> _T, spine=> _U, top=> _M, bot=> _V, token=> _W, adjleft=> _X, adjright=> _Y, adjwrap=> _Z}))
c_code local build_ref_455
	ret_reg &ref[455]
	call_c   build_ref_0()
	call_c   build_ref_454()
	call_c   build_ref_268()
	call_c   Dyam_Create_Binary(&ref[0],&ref[454],&ref[268])
	move_ret ref[455]
	c_ret

;; TERM 454: 'call_normalize/2'(_A1)
c_code local build_ref_454
	ret_reg &ref[454]
	call_c   build_ref_1185()
	call_c   Dyam_Create_Unary(&ref[1185],V(26))
	move_ret ref[454]
	c_ret

pl_code local fun131
	call_c   build_ref_455()
	call_c   Dyam_Unify_Item(&ref[455])
	fail_ret
	pl_ret

;; TERM 457: '*RITEM*'('call_normalize/2'(_A1), return(tag_node{id=> _Q, label=> _I, children=> _R, kind=> _S, adj=> _T, spine=> _U, top=> _M, bot=> _V, token=> _W, adjleft=> _X, adjright=> _Y, adjwrap=> _Z})) :> tig(6, '$TUPPLE'(35144395401924))
c_code local build_ref_457
	ret_reg &ref[457]
	call_c   build_ref_455()
	call_c   build_ref_456()
	call_c   Dyam_Create_Binary(I(9),&ref[455],&ref[456])
	move_ret ref[457]
	c_ret

;; TERM 456: tig(6, '$TUPPLE'(35144395401924))
c_code local build_ref_456
	ret_reg &ref[456]
	call_c   build_ref_1154()
	call_c   build_ref_719()
	call_c   Dyam_Create_Binary(&ref[1154],N(6),&ref[719])
	move_ret ref[456]
	c_ret

;; TERM 459: ':-adjkind(~k,right).\n'
c_code local build_ref_459
	ret_reg &ref[459]
	call_c   Dyam_Create_Atom(":-adjkind(~k,right).\n")
	move_ret ref[459]
	c_ret

long local pool_fun140[7]=[65541,build_seed_99,build_ref_450,build_ref_458,build_ref_459,build_ref_453,pool_fun138]

pl_code local fun141
	call_c   Dyam_Remove_Choice()
fun140:
	call_c   Dyam_Choice(fun139)
	call_c   Dyam_Choice(fun114)
	call_c   Dyam_Set_Cut()
	pl_call  fun38(&seed[99],2)
	move     &ref[450], R(0)
	move     S(5), R(1)
	move     &ref[458], R(2)
	move     0, R(3)
	pl_call  pred_tig_2()
	call_c   Dyam_Cut()
	move     &ref[459], R(0)
	move     0, R(1)
	move     &ref[453], R(2)
	move     S(5), R(3)
	pl_call  pred_format_2()
	pl_fail


c_code local build_seed_82
	ret_reg &seed[82]
	call_c   build_ref_133()
	call_c   build_ref_271()
	call_c   Dyam_Seed_Start(&ref[133],&ref[271],I(0),fun21,1)
	call_c   build_ref_269()
	call_c   Dyam_Seed_Add_Comp(&ref[269],fun71,0)
	call_c   Dyam_Seed_End()
	move_ret seed[82]
	c_ret

;; TERM 269: '*RITEM*'('call_normalize/2'(_P), return(tag_node{id=> _Q, label=> _I, children=> _R, kind=> _S, adj=> _T, spine=> _U, top=> _M, bot=> _V, token=> _W, adjleft=> _X, adjright=> _Y, adjwrap=> _Z}))
c_code local build_ref_269
	ret_reg &ref[269]
	call_c   build_ref_0()
	call_c   build_ref_267()
	call_c   build_ref_268()
	call_c   Dyam_Create_Binary(&ref[0],&ref[267],&ref[268])
	move_ret ref[269]
	c_ret

;; TERM 267: 'call_normalize/2'(_P)
c_code local build_ref_267
	ret_reg &ref[267]
	call_c   build_ref_1185()
	call_c   Dyam_Create_Unary(&ref[1185],V(15))
	move_ret ref[267]
	c_ret

pl_code local fun71
	call_c   build_ref_269()
	call_c   Dyam_Unify_Item(&ref[269])
	fail_ret
	pl_ret

;; TERM 271: '*RITEM*'('call_normalize/2'(_P), return(tag_node{id=> _Q, label=> _I, children=> _R, kind=> _S, adj=> _T, spine=> _U, top=> _M, bot=> _V, token=> _W, adjleft=> _X, adjright=> _Y, adjwrap=> _Z})) :> tig(4, '$TUPPLE'(35144395401924))
c_code local build_ref_271
	ret_reg &ref[271]
	call_c   build_ref_269()
	call_c   build_ref_270()
	call_c   Dyam_Create_Binary(I(9),&ref[269],&ref[270])
	move_ret ref[271]
	c_ret

;; TERM 270: tig(4, '$TUPPLE'(35144395401924))
c_code local build_ref_270
	ret_reg &ref[270]
	call_c   build_ref_1154()
	call_c   build_ref_719()
	call_c   Dyam_Create_Binary(&ref[1154],N(4),&ref[719])
	move_ret ref[270]
	c_ret

;; TERM 452: ':-adjkind(~k,left).\n'
c_code local build_ref_452
	ret_reg &ref[452]
	call_c   Dyam_Create_Atom(":-adjkind(~k,left).\n")
	move_ret ref[452]
	c_ret

long local pool_fun142[7]=[65541,build_seed_82,build_ref_450,build_ref_451,build_ref_452,build_ref_453,pool_fun140]

long local pool_fun143[3]=[65537,build_ref_466,pool_fun142]

pl_code local fun143
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(14),&ref[466])
	fail_ret
fun142:
	call_c   Dyam_Choice(fun141)
	call_c   Dyam_Choice(fun114)
	call_c   Dyam_Set_Cut()
	pl_call  fun38(&seed[82],2)
	move     &ref[450], R(0)
	move     S(5), R(1)
	move     &ref[451], R(2)
	move     0, R(3)
	pl_call  pred_tig_2()
	call_c   Dyam_Cut()
	move     &ref[452], R(0)
	move     0, R(1)
	move     &ref[453], R(2)
	move     S(5), R(3)
	pl_call  pred_format_2()
	pl_fail


;; TERM 266: '$ft'
c_code local build_ref_266
	ret_reg &ref[266]
	call_c   Dyam_Create_Atom("$ft")
	move_ret ref[266]
	c_ret

long local pool_fun145[6]=[131075,build_ref_465,build_ref_265,build_ref_266,pool_fun143,pool_fun142]

pl_code local fun149
	call_c   Dyam_Remove_Choice()
fun145:
	call_c   DYAM_Nl_0()
	fail_ret
	call_c   Dyam_Choice(fun144)
	pl_call  Object_1(&ref[265])
	call_c   DYAM_evpred_functor(V(12),V(10),V(11))
	fail_ret
	call_c   Dyam_Choice(fun143)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_atom_to_module(V(10),V(13),&ref[266])
	fail_ret
	call_c   Dyam_Cut()
	call_c   DYAM_evpred_functor(V(14),V(10),V(11))
	fail_ret
	call_c   Dyam_Reg_Load(2,V(14))
	call_c   DyALog_Assign_Display_Info(&R(2))
	pl_jump  fun142()


c_code local build_seed_76
	ret_reg &seed[76]
	call_c   build_ref_133()
	call_c   build_ref_221()
	call_c   Dyam_Seed_Start(&ref[133],&ref[221],I(0),fun21,1)
	call_c   build_ref_219()
	call_c   Dyam_Seed_Add_Comp(&ref[219],fun54,0)
	call_c   Dyam_Seed_End()
	move_ret seed[76]
	c_ret

;; TERM 219: '*RITEM*'('call_normalize/2'(tag_tree{family=> _F, name=> _G, tree=> (auxtree _H)}), return(_I))
c_code local build_ref_219
	ret_reg &ref[219]
	call_c   build_ref_0()
	call_c   build_ref_217()
	call_c   build_ref_218()
	call_c   Dyam_Create_Binary(&ref[0],&ref[217],&ref[218])
	move_ret ref[219]
	c_ret

;; TERM 218: return(_I)
c_code local build_ref_218
	ret_reg &ref[218]
	call_c   build_ref_1139()
	call_c   Dyam_Create_Unary(&ref[1139],V(8))
	move_ret ref[218]
	c_ret

;; TERM 217: 'call_normalize/2'(tag_tree{family=> _F, name=> _G, tree=> (auxtree _H)})
c_code local build_ref_217
	ret_reg &ref[217]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1150()
	call_c   Dyam_Create_Unary(&ref[1150],V(7))
	move_ret R(0)
	call_c   build_ref_1142()
	call_c   Dyam_Term_Start(&ref[1142],3)
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_1185()
	call_c   Dyam_Create_Unary(&ref[1185],R(0))
	move_ret ref[217]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

pl_code local fun54
	call_c   build_ref_219()
	call_c   Dyam_Unify_Item(&ref[219])
	fail_ret
	pl_ret

;; TERM 221: '*RITEM*'('call_normalize/2'(tag_tree{family=> _F, name=> _G, tree=> (auxtree _H)}), return(_I)) :> tig(3, '$TUPPLE'(35144395401924))
c_code local build_ref_221
	ret_reg &ref[221]
	call_c   build_ref_219()
	call_c   build_ref_220()
	call_c   Dyam_Create_Binary(I(9),&ref[219],&ref[220])
	move_ret ref[221]
	c_ret

;; TERM 220: tig(3, '$TUPPLE'(35144395401924))
c_code local build_ref_220
	ret_reg &ref[220]
	call_c   build_ref_1154()
	call_c   build_ref_719()
	call_c   Dyam_Create_Binary(&ref[1154],N(3),&ref[719])
	move_ret ref[220]
	c_ret

;; TERM 472: '%%:-tig(~w,wrap).\n'
c_code local build_ref_472
	ret_reg &ref[472]
	call_c   Dyam_Create_Atom("%%:-tig(~w,wrap).\n")
	move_ret ref[472]
	c_ret

;; TERM 473: [_G]
c_code local build_ref_473
	ret_reg &ref[473]
	call_c   Dyam_Create_List(V(6),I(0))
	move_ret ref[473]
	c_ret

long local pool_fun147[3]=[2,build_ref_472,build_ref_473]

pl_code local fun147
	call_c   Dyam_Remove_Choice()
	move     &ref[472], R(0)
	move     0, R(1)
	move     &ref[473], R(2)
	move     S(5), R(3)
	pl_call  pred_format_2()
	pl_fail

c_code local build_seed_101
	ret_reg &seed[101]
	call_c   build_ref_133()
	call_c   build_ref_470()
	call_c   Dyam_Seed_Start(&ref[133],&ref[470],I(0),fun21,1)
	call_c   build_ref_468()
	call_c   Dyam_Seed_Add_Comp(&ref[468],fun146,0)
	call_c   Dyam_Seed_End()
	move_ret seed[101]
	c_ret

;; TERM 468: '*RITEM*'(potential_tig(_I, _J), voidret)
c_code local build_ref_468
	ret_reg &ref[468]
	call_c   build_ref_0()
	call_c   build_ref_467()
	call_c   build_ref_21()
	call_c   Dyam_Create_Binary(&ref[0],&ref[467],&ref[21])
	move_ret ref[468]
	c_ret

;; TERM 467: potential_tig(_I, _J)
c_code local build_ref_467
	ret_reg &ref[467]
	call_c   build_ref_1131()
	call_c   Dyam_Create_Binary(&ref[1131],V(8),V(9))
	move_ret ref[467]
	c_ret

pl_code local fun146
	call_c   build_ref_468()
	call_c   Dyam_Unify_Item(&ref[468])
	fail_ret
	pl_ret

;; TERM 470: '*RITEM*'(potential_tig(_I, _J), voidret) :> tig(8, '$TUPPLE'(35144395401924))
c_code local build_ref_470
	ret_reg &ref[470]
	call_c   build_ref_468()
	call_c   build_ref_469()
	call_c   Dyam_Create_Binary(I(9),&ref[468],&ref[469])
	move_ret ref[470]
	c_ret

;; TERM 469: tig(8, '$TUPPLE'(35144395401924))
c_code local build_ref_469
	ret_reg &ref[469]
	call_c   build_ref_1154()
	call_c   build_ref_719()
	call_c   Dyam_Create_Binary(&ref[1154],N(8),&ref[719])
	move_ret ref[469]
	c_ret

;; TERM 471: '%%:-tig(~w,~w).\n'
c_code local build_ref_471
	ret_reg &ref[471]
	call_c   Dyam_Create_Atom("%%:-tig(~w,~w).\n")
	move_ret ref[471]
	c_ret

;; TERM 264: [_G,_J]
c_code local build_ref_264
	ret_reg &ref[264]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(9),I(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(6),R(0))
	move_ret ref[264]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun148[5]=[65539,build_seed_101,build_ref_471,build_ref_264,pool_fun147]

pl_code local fun148
	call_c   Dyam_Update_Choice(fun147)
	call_c   Dyam_Set_Cut()
	pl_call  fun38(&seed[101],2)
	call_c   Dyam_Cut()
	move     &ref[471], R(0)
	move     0, R(1)
	move     &ref[264], R(2)
	move     S(5), R(3)
	pl_call  pred_format_2()
	pl_fail

;; TERM 263: ':-tig(~k,~w).\n'
c_code local build_ref_263
	ret_reg &ref[263]
	call_c   Dyam_Create_Atom(":-tig(~k,~w).\n")
	move_ret ref[263]
	c_ret

long local pool_fun150[6]=[131075,build_seed_76,build_ref_263,build_ref_264,pool_fun145,pool_fun148]

pl_code local fun151
	call_c   Dyam_Remove_Choice()
fun150:
	pl_call  pred_tag2tig_emit_header_0()
	call_c   Dyam_Choice(fun149)
	pl_call  fun38(&seed[76],2)
	call_c   Dyam_Choice(fun148)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Reg_Load(0,V(8))
	move     V(9), R(2)
	move     S(5), R(3)
	pl_call  pred_tig_2()
	call_c   Dyam_Cut()
	move     &ref[263], R(0)
	move     0, R(1)
	move     &ref[264], R(2)
	move     S(5), R(3)
	pl_call  pred_format_2()
	pl_fail


;; TERM 214: output_file(_C)
c_code local build_ref_214
	ret_reg &ref[214]
	call_c   build_ref_1186()
	call_c   Dyam_Create_Unary(&ref[1186],V(2))
	move_ret ref[214]
	c_ret

;; TERM 1186: output_file
c_code local build_ref_1186
	ret_reg &ref[1186]
	call_c   Dyam_Create_Atom("output_file")
	move_ret ref[1186]
	c_ret

;; TERM 215: write
c_code local build_ref_215
	ret_reg &ref[215]
	call_c   Dyam_Create_Atom("write")
	move_ret ref[215]
	c_ret

long local pool_fun152[7]=[131076,build_ref_213,build_ref_167,build_ref_214,build_ref_215,pool_fun150,pool_fun150]

pl_code local fun152
	call_c   Dyam_Pool(pool_fun152)
	call_c   Dyam_Unify_Item(&ref[213])
	fail_ret
	call_c   Dyam_Allocate(0)
	pl_call  Object_1(&ref[167])
	call_c   DYAM_Current_Output_1(V(1))
	fail_ret
	call_c   Dyam_Choice(fun151)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[214])
	call_c   Dyam_Cut()
	call_c   DYAM_Absolute_File_Name(V(2),V(3))
	fail_ret
	call_c   DYAM_Open_3(V(3),&ref[215],V(4))
	fail_ret
	call_c   DYAM_Set_Output_1(V(4))
	fail_ret
	pl_jump  fun150()

;; TERM 212: '*PROLOG-FIRST*'(emit_at_pda) :> []
c_code local build_ref_212
	ret_reg &ref[212]
	call_c   build_ref_211()
	call_c   Dyam_Create_Binary(I(9),&ref[211],I(0))
	move_ret ref[212]
	c_ret

;; TERM 211: '*PROLOG-FIRST*'(emit_at_pda)
c_code local build_ref_211
	ret_reg &ref[211]
	call_c   build_ref_64()
	call_c   build_ref_210()
	call_c   Dyam_Create_Unary(&ref[64],&ref[210])
	move_ret ref[211]
	c_ret

c_code local build_seed_49
	ret_reg &seed[49]
	call_c   build_ref_54()
	call_c   build_ref_287()
	call_c   Dyam_Seed_Start(&ref[54],&ref[287],I(0),fun0,1)
	call_c   build_ref_286()
	call_c   Dyam_Seed_Add_Comp(&ref[286],fun84,0)
	call_c   Dyam_Seed_End()
	move_ret seed[49]
	c_ret

;; TERM 286: '*GUARD*'(directive(adjrestr(_B, _C, _D), _E, _F, _G))
c_code local build_ref_286
	ret_reg &ref[286]
	call_c   build_ref_55()
	call_c   build_ref_285()
	call_c   Dyam_Create_Unary(&ref[55],&ref[285])
	move_ret ref[286]
	c_ret

;; TERM 285: directive(adjrestr(_B, _C, _D), _E, _F, _G)
c_code local build_ref_285
	ret_reg &ref[285]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1137()
	call_c   Dyam_Term_Start(&ref[1137],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_1132()
	call_c   Dyam_Term_Start(&ref[1132],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[285]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 292: [adjrestr(_B, _C, _D)]
c_code local build_ref_292
	ret_reg &ref[292]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1137()
	call_c   Dyam_Term_Start(&ref[1137],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   Dyam_Create_List(R(0),I(0))
	move_ret ref[292]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 291: 'Nat a valid adj restriction ~w'
c_code local build_ref_291
	ret_reg &ref[291]
	call_c   Dyam_Create_Atom("Nat a valid adj restriction ~w")
	move_ret ref[291]
	c_ret

;; TERM 290: adjrestr(_C, _D)
c_code local build_ref_290
	ret_reg &ref[290]
	call_c   build_ref_1137()
	call_c   Dyam_Create_Binary(&ref[1137],V(2),V(3))
	move_ret ref[290]
	c_ret

long local pool_fun81[4]=[3,build_ref_291,build_ref_292,build_ref_290]

pl_code local fun81
	call_c   Dyam_Remove_Choice()
	move     &ref[291], R(0)
	move     0, R(1)
	move     &ref[292], R(2)
	move     S(5), R(3)
	pl_call  pred_error_2()
fun80:
	move     &ref[290], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(1))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_install_2()


;; TERM 289: [atmostone,one,strict]
c_code local build_ref_289
	ret_reg &ref[289]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_673()
	call_c   Dyam_Create_List(&ref[673],I(0))
	move_ret R(0)
	call_c   build_ref_686()
	call_c   Dyam_Create_List(&ref[686],R(0))
	move_ret R(0)
	call_c   build_ref_685()
	call_c   Dyam_Create_List(&ref[685],R(0))
	move_ret ref[289]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 685: atmostone
c_code local build_ref_685
	ret_reg &ref[685]
	call_c   Dyam_Create_Atom("atmostone")
	move_ret ref[685]
	c_ret

;; TERM 686: one
c_code local build_ref_686
	ret_reg &ref[686]
	call_c   Dyam_Create_Atom("one")
	move_ret ref[686]
	c_ret

;; TERM 673: strict
c_code local build_ref_673
	ret_reg &ref[673]
	call_c   Dyam_Create_Atom("strict")
	move_ret ref[673]
	c_ret

long local pool_fun82[4]=[65538,build_ref_289,build_ref_290,pool_fun81]

long local pool_fun83[4]=[65538,build_ref_261,build_ref_292,pool_fun82]

pl_code local fun83
	call_c   Dyam_Remove_Choice()
	move     &ref[261], R(0)
	move     0, R(1)
	move     &ref[292], R(2)
	move     S(5), R(3)
	pl_call  pred_error_2()
fun82:
	call_c   Dyam_Choice(fun81)
	call_c   Dyam_Set_Cut()
	pl_call  Domain_2(V(3),&ref[289])
	call_c   Dyam_Cut()
	pl_jump  fun80()


;; TERM 288: [left,right,wrap]
c_code local build_ref_288
	ret_reg &ref[288]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_512()
	call_c   Dyam_Create_List(&ref[512],I(0))
	move_ret R(0)
	call_c   build_ref_458()
	call_c   Dyam_Create_List(&ref[458],R(0))
	move_ret R(0)
	call_c   build_ref_451()
	call_c   Dyam_Create_List(&ref[451],R(0))
	move_ret ref[288]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun84[5]=[131074,build_ref_286,build_ref_288,pool_fun83,pool_fun82]

pl_code local fun84
	call_c   Dyam_Pool(pool_fun84)
	call_c   Dyam_Unify_Item(&ref[286])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun83)
	call_c   Dyam_Set_Cut()
	pl_call  Domain_2(V(2),&ref[288])
	call_c   Dyam_Cut()
	pl_jump  fun82()

;; TERM 287: '*GUARD*'(directive(adjrestr(_B, _C, _D), _E, _F, _G)) :> []
c_code local build_ref_287
	ret_reg &ref[287]
	call_c   build_ref_286()
	call_c   Dyam_Create_Binary(I(9),&ref[286],I(0))
	move_ret ref[287]
	c_ret

c_code local build_seed_10
	ret_reg &seed[10]
	call_c   build_ref_63()
	call_c   build_ref_303()
	call_c   Dyam_Seed_Start(&ref[63],&ref[303],I(0),fun0,1)
	call_c   build_ref_304()
	call_c   Dyam_Seed_Add_Comp(&ref[304],fun89,0)
	call_c   Dyam_Seed_End()
	move_ret seed[10]
	c_ret

;; TERM 304: '*PROLOG-ITEM*'{top=> lctag_first(_B, @*{goal=> _C, vars=> _D, from=> _E, to=> _F, collect_first=> _G, collect_last=> _H, collect_loop=> _I, collect_next=> _J, collect_pred=> _K}, _L, _M), cont=> _A}
c_code local build_ref_304
	ret_reg &ref[304]
	call_c   build_ref_1126()
	call_c   build_ref_301()
	call_c   Dyam_Create_Binary(&ref[1126],&ref[301],V(0))
	move_ret ref[304]
	c_ret

;; TERM 301: lctag_first(_B, @*{goal=> _C, vars=> _D, from=> _E, to=> _F, collect_first=> _G, collect_last=> _H, collect_loop=> _I, collect_next=> _J, collect_pred=> _K}, _L, _M)
c_code local build_ref_301
	ret_reg &ref[301]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1187()
	call_c   Dyam_Term_Start(&ref[1187],9)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_1127()
	call_c   Dyam_Term_Start(&ref[1127],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_End()
	move_ret ref[301]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1187: @*!'$ft'
c_code local build_ref_1187
	ret_reg &ref[1187]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1197()
	call_c   Dyam_Create_List(&ref[1197],I(0))
	move_ret R(0)
	call_c   build_ref_1196()
	call_c   Dyam_Create_List(&ref[1196],R(0))
	move_ret R(0)
	call_c   build_ref_1195()
	call_c   Dyam_Create_List(&ref[1195],R(0))
	move_ret R(0)
	call_c   build_ref_1194()
	call_c   Dyam_Create_List(&ref[1194],R(0))
	move_ret R(0)
	call_c   build_ref_1193()
	call_c   Dyam_Create_List(&ref[1193],R(0))
	move_ret R(0)
	call_c   build_ref_1192()
	call_c   Dyam_Create_List(&ref[1192],R(0))
	move_ret R(0)
	call_c   build_ref_1191()
	call_c   Dyam_Create_List(&ref[1191],R(0))
	move_ret R(0)
	call_c   build_ref_1190()
	call_c   Dyam_Create_List(&ref[1190],R(0))
	move_ret R(0)
	call_c   build_ref_1189()
	call_c   Dyam_Create_List(&ref[1189],R(0))
	move_ret R(0)
	call_c   build_ref_1188()
	call_c   Dyam_Create_Atom_Module("$ft",&ref[1188])
	move_ret ref[1187]
	call_c   DYAM_Feature_2(&ref[1188],R(0))
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1188: @*
c_code local build_ref_1188
	ret_reg &ref[1188]
	call_c   Dyam_Create_Atom("@*")
	move_ret ref[1188]
	c_ret

;; TERM 1189: goal
c_code local build_ref_1189
	ret_reg &ref[1189]
	call_c   Dyam_Create_Atom("goal")
	move_ret ref[1189]
	c_ret

;; TERM 1190: vars
c_code local build_ref_1190
	ret_reg &ref[1190]
	call_c   Dyam_Create_Atom("vars")
	move_ret ref[1190]
	c_ret

;; TERM 1191: from
c_code local build_ref_1191
	ret_reg &ref[1191]
	call_c   Dyam_Create_Atom("from")
	move_ret ref[1191]
	c_ret

;; TERM 1192: to
c_code local build_ref_1192
	ret_reg &ref[1192]
	call_c   Dyam_Create_Atom("to")
	move_ret ref[1192]
	c_ret

;; TERM 1193: collect_first
c_code local build_ref_1193
	ret_reg &ref[1193]
	call_c   Dyam_Create_Atom("collect_first")
	move_ret ref[1193]
	c_ret

;; TERM 1194: collect_last
c_code local build_ref_1194
	ret_reg &ref[1194]
	call_c   Dyam_Create_Atom("collect_last")
	move_ret ref[1194]
	c_ret

;; TERM 1195: collect_loop
c_code local build_ref_1195
	ret_reg &ref[1195]
	call_c   Dyam_Create_Atom("collect_loop")
	move_ret ref[1195]
	c_ret

;; TERM 1196: collect_next
c_code local build_ref_1196
	ret_reg &ref[1196]
	call_c   Dyam_Create_Atom("collect_next")
	move_ret ref[1196]
	c_ret

;; TERM 1197: collect_pred
c_code local build_ref_1197
	ret_reg &ref[1197]
	call_c   Dyam_Create_Atom("collect_pred")
	move_ret ref[1197]
	c_ret

c_code local build_seed_84
	ret_reg &seed[84]
	call_c   build_ref_176()
	call_c   build_ref_306()
	call_c   Dyam_Seed_Start(&ref[176],&ref[306],I(0),fun11,1)
	call_c   build_ref_309()
	call_c   Dyam_Seed_Add_Comp(&ref[309],&ref[306],0)
	call_c   Dyam_Seed_End()
	move_ret seed[84]
	c_ret

;; TERM 309: '*PROLOG-FIRST*'(lctag_first(_B, _C, _L, _M)) :> '$$HOLE$$'
c_code local build_ref_309
	ret_reg &ref[309]
	call_c   build_ref_308()
	call_c   Dyam_Create_Binary(I(9),&ref[308],I(7))
	move_ret ref[309]
	c_ret

;; TERM 308: '*PROLOG-FIRST*'(lctag_first(_B, _C, _L, _M))
c_code local build_ref_308
	ret_reg &ref[308]
	call_c   build_ref_64()
	call_c   build_ref_307()
	call_c   Dyam_Create_Unary(&ref[64],&ref[307])
	move_ret ref[308]
	c_ret

;; TERM 307: lctag_first(_B, _C, _L, _M)
c_code local build_ref_307
	ret_reg &ref[307]
	call_c   build_ref_1127()
	call_c   Dyam_Term_Start(&ref[1127],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_End()
	move_ret ref[307]
	c_ret

;; TERM 306: '*PROLOG-ITEM*'{top=> lctag_first(_B, _C, _L, _M), cont=> _A}
c_code local build_ref_306
	ret_reg &ref[306]
	call_c   build_ref_1126()
	call_c   build_ref_307()
	call_c   Dyam_Create_Binary(&ref[1126],&ref[307],V(0))
	move_ret ref[306]
	c_ret

long local pool_fun88[2]=[1,build_seed_84]

pl_code local fun88
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Deallocate()
	pl_jump  fun38(&seed[84],12)

long local pool_fun89[4]=[65538,build_ref_304,build_ref_305,pool_fun88]

pl_code local fun89
	call_c   Dyam_Pool(pool_fun89)
	call_c   Dyam_Unify_Item(&ref[304])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun88)
	call_c   Dyam_Unify(V(11),&ref[305])
	fail_ret
	call_c   Dyam_Unify(V(12),I(0))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_jump  Follow_Cont(V(0))

;; TERM 303: '*PROLOG-FIRST*'(lctag_first(_B, @*{goal=> _C, vars=> _D, from=> _E, to=> _F, collect_first=> _G, collect_last=> _H, collect_loop=> _I, collect_next=> _J, collect_pred=> _K}, _L, _M)) :> []
c_code local build_ref_303
	ret_reg &ref[303]
	call_c   build_ref_302()
	call_c   Dyam_Create_Binary(I(9),&ref[302],I(0))
	move_ret ref[303]
	c_ret

;; TERM 302: '*PROLOG-FIRST*'(lctag_first(_B, @*{goal=> _C, vars=> _D, from=> _E, to=> _F, collect_first=> _G, collect_last=> _H, collect_loop=> _I, collect_next=> _J, collect_pred=> _K}, _L, _M))
c_code local build_ref_302
	ret_reg &ref[302]
	call_c   build_ref_64()
	call_c   build_ref_301()
	call_c   Dyam_Create_Unary(&ref[64],&ref[301])
	move_ret ref[302]
	c_ret

c_code local build_seed_45
	ret_reg &seed[45]
	call_c   build_ref_54()
	call_c   build_ref_312()
	call_c   Dyam_Seed_Start(&ref[54],&ref[312],I(0),fun0,1)
	call_c   build_ref_311()
	call_c   Dyam_Seed_Add_Comp(&ref[311],fun91,0)
	call_c   Dyam_Seed_End()
	move_ret seed[45]
	c_ret

;; TERM 311: '*GUARD*'(alt_optimize_topbot_args([_B|_C], [_D|_E], _F, _G))
c_code local build_ref_311
	ret_reg &ref[311]
	call_c   build_ref_55()
	call_c   build_ref_310()
	call_c   Dyam_Create_Unary(&ref[55],&ref[310])
	move_ret ref[311]
	c_ret

;; TERM 310: alt_optimize_topbot_args([_B|_C], [_D|_E], _F, _G)
c_code local build_ref_310
	ret_reg &ref[310]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(1),V(2))
	move_ret R(0)
	call_c   build_ref_1128()
	call_c   build_ref_121()
	call_c   Dyam_Term_Start(&ref[1128],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[121])
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[310]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 121: [_D|_E]
c_code local build_ref_121
	ret_reg &ref[121]
	call_c   Dyam_Create_List(V(3),V(4))
	move_ret ref[121]
	c_ret

c_code local build_seed_85
	ret_reg &seed[85]
	call_c   build_ref_55()
	call_c   build_ref_314()
	call_c   Dyam_Seed_Start(&ref[55],&ref[314],I(0),fun11,1)
	call_c   build_ref_315()
	call_c   Dyam_Seed_Add_Comp(&ref[315],&ref[314],0)
	call_c   Dyam_Seed_End()
	move_ret seed[85]
	c_ret

;; TERM 315: '*GUARD*'(alt_optimize_topbot_args(_C, _E, _F, _H)) :> '$$HOLE$$'
c_code local build_ref_315
	ret_reg &ref[315]
	call_c   build_ref_314()
	call_c   Dyam_Create_Binary(I(9),&ref[314],I(7))
	move_ret ref[315]
	c_ret

;; TERM 314: '*GUARD*'(alt_optimize_topbot_args(_C, _E, _F, _H))
c_code local build_ref_314
	ret_reg &ref[314]
	call_c   build_ref_55()
	call_c   build_ref_313()
	call_c   Dyam_Create_Unary(&ref[55],&ref[313])
	move_ret ref[314]
	c_ret

;; TERM 313: alt_optimize_topbot_args(_C, _E, _F, _H)
c_code local build_ref_313
	ret_reg &ref[313]
	call_c   build_ref_1128()
	call_c   Dyam_Term_Start(&ref[1128],4)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[313]
	c_ret

;; TERM 316: [_D|_H]
c_code local build_ref_316
	ret_reg &ref[316]
	call_c   Dyam_Create_List(V(3),V(7))
	move_ret ref[316]
	c_ret

long local pool_fun90[2]=[1,build_ref_316]

pl_code local fun90
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(6),&ref[316])
	fail_ret
fun85:
	call_c   Dyam_Deallocate()
	pl_ret


long local pool_fun91[4]=[65538,build_ref_311,build_seed_85,pool_fun90]

pl_code local fun91
	call_c   Dyam_Pool(pool_fun91)
	call_c   Dyam_Unify_Item(&ref[311])
	fail_ret
	call_c   Dyam_Allocate(0)
	pl_call  fun38(&seed[85],1)
	call_c   Dyam_Choice(fun90)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(1),V(3))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(6),V(7))
	fail_ret
	pl_jump  fun85()

;; TERM 312: '*GUARD*'(alt_optimize_topbot_args([_B|_C], [_D|_E], _F, _G)) :> []
c_code local build_ref_312
	ret_reg &ref[312]
	call_c   build_ref_311()
	call_c   Dyam_Create_Binary(I(9),&ref[311],I(0))
	move_ret ref[312]
	c_ret

c_code local build_seed_18
	ret_reg &seed[18]
	call_c   build_ref_54()
	call_c   build_ref_295()
	call_c   Dyam_Seed_Start(&ref[54],&ref[295],I(0),fun0,1)
	call_c   build_ref_294()
	call_c   Dyam_Seed_Add_Comp(&ref[294],fun87,0)
	call_c   Dyam_Seed_End()
	move_ret seed[18]
	c_ret

;; TERM 294: '*GUARD*'(lctag_simplify(_B, guard{goal=> _C, plus=> _D, minus=> _E}, _F, guard{goal=> _G, plus=> _D, minus=> _E}))
c_code local build_ref_294
	ret_reg &ref[294]
	call_c   build_ref_55()
	call_c   build_ref_293()
	call_c   Dyam_Create_Unary(&ref[55],&ref[293])
	move_ret ref[294]
	c_ret

;; TERM 293: lctag_simplify(_B, guard{goal=> _C, plus=> _D, minus=> _E}, _F, guard{goal=> _G, plus=> _D, minus=> _E})
c_code local build_ref_293
	ret_reg &ref[293]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_1198()
	call_c   Dyam_Term_Start(&ref[1198],3)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   Dyam_Term_Start(&ref[1198],3)
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret R(1)
	call_c   build_ref_1124()
	call_c   Dyam_Term_Start(&ref[1124],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_End()
	move_ret ref[293]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1198: guard!'$ft'
c_code local build_ref_1198
	ret_reg &ref[1198]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1201()
	call_c   Dyam_Create_List(&ref[1201],I(0))
	move_ret R(0)
	call_c   build_ref_1200()
	call_c   Dyam_Create_List(&ref[1200],R(0))
	move_ret R(0)
	call_c   build_ref_1189()
	call_c   Dyam_Create_List(&ref[1189],R(0))
	move_ret R(0)
	call_c   build_ref_1199()
	call_c   Dyam_Create_Atom_Module("$ft",&ref[1199])
	move_ret ref[1198]
	call_c   DYAM_Feature_2(&ref[1199],R(0))
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1199: guard
c_code local build_ref_1199
	ret_reg &ref[1199]
	call_c   Dyam_Create_Atom("guard")
	move_ret ref[1199]
	c_ret

;; TERM 1200: plus
c_code local build_ref_1200
	ret_reg &ref[1200]
	call_c   Dyam_Create_Atom("plus")
	move_ret ref[1200]
	c_ret

;; TERM 1201: minus
c_code local build_ref_1201
	ret_reg &ref[1201]
	call_c   Dyam_Create_Atom("minus")
	move_ret ref[1201]
	c_ret

c_code local build_seed_83
	ret_reg &seed[83]
	call_c   build_ref_55()
	call_c   build_ref_297()
	call_c   Dyam_Seed_Start(&ref[55],&ref[297],I(0),fun11,1)
	call_c   build_ref_298()
	call_c   Dyam_Seed_Add_Comp(&ref[298],&ref[297],0)
	call_c   Dyam_Seed_End()
	move_ret seed[83]
	c_ret

;; TERM 298: '*GUARD*'(lctag_simplify(_B, _C, _H, _G)) :> '$$HOLE$$'
c_code local build_ref_298
	ret_reg &ref[298]
	call_c   build_ref_297()
	call_c   Dyam_Create_Binary(I(9),&ref[297],I(7))
	move_ret ref[298]
	c_ret

;; TERM 297: '*GUARD*'(lctag_simplify(_B, _C, _H, _G))
c_code local build_ref_297
	ret_reg &ref[297]
	call_c   build_ref_55()
	call_c   build_ref_296()
	call_c   Dyam_Create_Unary(&ref[55],&ref[296])
	move_ret ref[297]
	c_ret

;; TERM 296: lctag_simplify(_B, _C, _H, _G)
c_code local build_ref_296
	ret_reg &ref[296]
	call_c   build_ref_1124()
	call_c   Dyam_Term_Start(&ref[1124],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[296]
	c_ret

long local pool_fun86[2]=[1,build_ref_300]

pl_code local fun86
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(5),&ref[300])
	fail_ret
	pl_jump  fun85()

;; TERM 299: fail
c_code local build_ref_299
	ret_reg &ref[299]
	call_c   Dyam_Create_Atom("fail")
	move_ret ref[299]
	c_ret

long local pool_fun87[5]=[65539,build_ref_294,build_seed_83,build_ref_299,pool_fun86]

pl_code local fun87
	call_c   Dyam_Pool(pool_fun87)
	call_c   Dyam_Unify_Item(&ref[294])
	fail_ret
	call_c   Dyam_Allocate(0)
	pl_call  fun38(&seed[83],1)
	call_c   Dyam_Choice(fun86)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(4),&ref[299])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(5),V(7))
	fail_ret
	pl_jump  fun85()

;; TERM 295: '*GUARD*'(lctag_simplify(_B, guard{goal=> _C, plus=> _D, minus=> _E}, _F, guard{goal=> _G, plus=> _D, minus=> _E})) :> []
c_code local build_ref_295
	ret_reg &ref[295]
	call_c   build_ref_294()
	call_c   Dyam_Create_Binary(I(9),&ref[294],I(0))
	move_ret ref[295]
	c_ret

c_code local build_seed_29
	ret_reg &seed[29]
	call_c   build_ref_54()
	call_c   build_ref_354()
	call_c   Dyam_Seed_Start(&ref[54],&ref[354],I(0),fun0,1)
	call_c   build_ref_353()
	call_c   Dyam_Seed_Add_Comp(&ref[353],fun99,0)
	call_c   Dyam_Seed_End()
	move_ret seed[29]
	c_ret

;; TERM 353: '*GUARD*'(directive(tagfilter_cat((_B ^ _C ^ _D ^ _E ^ _F ^ _G ^ _H)), _I, _J, _K))
c_code local build_ref_353
	ret_reg &ref[353]
	call_c   build_ref_55()
	call_c   build_ref_352()
	call_c   Dyam_Create_Unary(&ref[55],&ref[352])
	move_ret ref[353]
	c_ret

;; TERM 352: directive(tagfilter_cat((_B ^ _C ^ _D ^ _E ^ _F ^ _G ^ _H)), _I, _J, _K)
c_code local build_ref_352
	ret_reg &ref[352]
	call_c   build_ref_1132()
	call_c   build_ref_355()
	call_c   Dyam_Term_Start(&ref[1132],4)
	call_c   Dyam_Term_Arg(&ref[355])
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_End()
	move_ret ref[352]
	c_ret

;; TERM 355: tagfilter_cat((_B ^ _C ^ _D ^ _E ^ _F ^ _G ^ _H))
c_code local build_ref_355
	ret_reg &ref[355]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1203()
	call_c   Dyam_Create_Binary(&ref[1203],V(6),V(7))
	move_ret R(0)
	call_c   Dyam_Create_Binary(&ref[1203],V(5),R(0))
	move_ret R(0)
	call_c   Dyam_Create_Binary(&ref[1203],V(4),R(0))
	move_ret R(0)
	call_c   Dyam_Create_Binary(&ref[1203],V(3),R(0))
	move_ret R(0)
	call_c   Dyam_Create_Binary(&ref[1203],V(2),R(0))
	move_ret R(0)
	call_c   Dyam_Create_Binary(&ref[1203],V(1),R(0))
	move_ret R(0)
	call_c   build_ref_1202()
	call_c   Dyam_Create_Unary(&ref[1202],R(0))
	move_ret ref[355]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1202: tagfilter_cat
c_code local build_ref_1202
	ret_reg &ref[1202]
	call_c   Dyam_Create_Atom("tagfilter_cat")
	move_ret ref[1202]
	c_ret

;; TERM 1203: ^
c_code local build_ref_1203
	ret_reg &ref[1203]
	call_c   Dyam_Create_Atom("^")
	move_ret ref[1203]
	c_ret

long local pool_fun99[3]=[2,build_ref_353,build_ref_355]

pl_code local fun99
	call_c   Dyam_Pool(pool_fun99)
	call_c   Dyam_Unify_Item(&ref[353])
	fail_ret
	call_c   DYAM_evpred_assert_1(&ref[355])
	pl_ret

;; TERM 354: '*GUARD*'(directive(tagfilter_cat((_B ^ _C ^ _D ^ _E ^ _F ^ _G ^ _H)), _I, _J, _K)) :> []
c_code local build_ref_354
	ret_reg &ref[354]
	call_c   build_ref_353()
	call_c   Dyam_Create_Binary(I(9),&ref[353],I(0))
	move_ret ref[354]
	c_ret

c_code local build_seed_30
	ret_reg &seed[30]
	call_c   build_ref_54()
	call_c   build_ref_358()
	call_c   Dyam_Seed_Start(&ref[54],&ref[358],I(0),fun0,1)
	call_c   build_ref_357()
	call_c   Dyam_Seed_Add_Comp(&ref[357],fun100,0)
	call_c   Dyam_Seed_End()
	move_ret seed[30]
	c_ret

;; TERM 357: '*GUARD*'(directive(tagfilter((_B ^ _C ^ _D ^ _E ^ _F)), _G, _H, _I))
c_code local build_ref_357
	ret_reg &ref[357]
	call_c   build_ref_55()
	call_c   build_ref_356()
	call_c   Dyam_Create_Unary(&ref[55],&ref[356])
	move_ret ref[357]
	c_ret

;; TERM 356: directive(tagfilter((_B ^ _C ^ _D ^ _E ^ _F)), _G, _H, _I)
c_code local build_ref_356
	ret_reg &ref[356]
	call_c   build_ref_1132()
	call_c   build_ref_360()
	call_c   Dyam_Term_Start(&ref[1132],4)
	call_c   Dyam_Term_Arg(&ref[360])
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_End()
	move_ret ref[356]
	c_ret

;; TERM 360: tagfilter((_B ^ _C ^ _D ^ _E ^ _F))
c_code local build_ref_360
	ret_reg &ref[360]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1203()
	call_c   Dyam_Create_Binary(&ref[1203],V(4),V(5))
	move_ret R(0)
	call_c   Dyam_Create_Binary(&ref[1203],V(3),R(0))
	move_ret R(0)
	call_c   Dyam_Create_Binary(&ref[1203],V(2),R(0))
	move_ret R(0)
	call_c   Dyam_Create_Binary(&ref[1203],V(1),R(0))
	move_ret R(0)
	call_c   build_ref_1204()
	call_c   Dyam_Create_Unary(&ref[1204],R(0))
	move_ret ref[360]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1204: tagfilter
c_code local build_ref_1204
	ret_reg &ref[1204]
	call_c   Dyam_Create_Atom("tagfilter")
	move_ret ref[1204]
	c_ret

;; TERM 359: tagfilter((_J ^ _K ^ _L ^ _M ^ _N))
c_code local build_ref_359
	ret_reg &ref[359]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1203()
	call_c   Dyam_Create_Binary(&ref[1203],V(12),V(13))
	move_ret R(0)
	call_c   Dyam_Create_Binary(&ref[1203],V(11),R(0))
	move_ret R(0)
	call_c   Dyam_Create_Binary(&ref[1203],V(10),R(0))
	move_ret R(0)
	call_c   Dyam_Create_Binary(&ref[1203],V(9),R(0))
	move_ret R(0)
	call_c   build_ref_1204()
	call_c   Dyam_Create_Unary(&ref[1204],R(0))
	move_ret ref[359]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun100[4]=[3,build_ref_357,build_ref_359,build_ref_360]

pl_code local fun100
	call_c   Dyam_Pool(pool_fun100)
	call_c   Dyam_Unify_Item(&ref[357])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[359], R(0)
	move     S(5), R(1)
	move     &ref[360], R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_directive_updater_2()

;; TERM 358: '*GUARD*'(directive(tagfilter((_B ^ _C ^ _D ^ _E ^ _F)), _G, _H, _I)) :> []
c_code local build_ref_358
	ret_reg &ref[358]
	call_c   build_ref_357()
	call_c   Dyam_Create_Binary(I(9),&ref[357],I(0))
	move_ret ref[358]
	c_ret

c_code local build_seed_21
	ret_reg &seed[21]
	call_c   build_ref_54()
	call_c   build_ref_363()
	call_c   Dyam_Seed_Start(&ref[54],&ref[363],I(0),fun0,1)
	call_c   build_ref_362()
	call_c   Dyam_Seed_Add_Comp(&ref[362],fun102,0)
	call_c   Dyam_Seed_End()
	move_ret seed[21]
	c_ret

;; TERM 362: '*GUARD*'(lctag_simplify(_B, (_C , _D), _E, _F))
c_code local build_ref_362
	ret_reg &ref[362]
	call_c   build_ref_55()
	call_c   build_ref_361()
	call_c   Dyam_Create_Unary(&ref[55],&ref[361])
	move_ret ref[362]
	c_ret

;; TERM 361: lctag_simplify(_B, (_C , _D), _E, _F)
c_code local build_ref_361
	ret_reg &ref[361]
	call_c   build_ref_1124()
	call_c   build_ref_666()
	call_c   Dyam_Term_Start(&ref[1124],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(&ref[666])
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[361]
	c_ret

;; TERM 666: _C , _D
c_code local build_ref_666
	ret_reg &ref[666]
	call_c   Dyam_Create_Binary(I(4),V(2),V(3))
	move_ret ref[666]
	c_ret

c_code local build_seed_90
	ret_reg &seed[90]
	call_c   build_ref_55()
	call_c   build_ref_365()
	call_c   Dyam_Seed_Start(&ref[55],&ref[365],I(0),fun11,1)
	call_c   build_ref_366()
	call_c   Dyam_Seed_Add_Comp(&ref[366],&ref[365],0)
	call_c   Dyam_Seed_End()
	move_ret seed[90]
	c_ret

;; TERM 366: '*GUARD*'(lctag_simplify(_B, _C, _G, _H)) :> '$$HOLE$$'
c_code local build_ref_366
	ret_reg &ref[366]
	call_c   build_ref_365()
	call_c   Dyam_Create_Binary(I(9),&ref[365],I(7))
	move_ret ref[366]
	c_ret

;; TERM 365: '*GUARD*'(lctag_simplify(_B, _C, _G, _H))
c_code local build_ref_365
	ret_reg &ref[365]
	call_c   build_ref_55()
	call_c   build_ref_364()
	call_c   Dyam_Create_Unary(&ref[55],&ref[364])
	move_ret ref[365]
	c_ret

;; TERM 364: lctag_simplify(_B, _C, _G, _H)
c_code local build_ref_364
	ret_reg &ref[364]
	call_c   build_ref_1124()
	call_c   Dyam_Term_Start(&ref[1124],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[364]
	c_ret

c_code local build_seed_91
	ret_reg &seed[91]
	call_c   build_ref_55()
	call_c   build_ref_369()
	call_c   Dyam_Seed_Start(&ref[55],&ref[369],I(0),fun11,1)
	call_c   build_ref_370()
	call_c   Dyam_Seed_Add_Comp(&ref[370],&ref[369],0)
	call_c   Dyam_Seed_End()
	move_ret seed[91]
	c_ret

;; TERM 370: '*GUARD*'(lctag_simplify(_B, _D, _E, _I)) :> '$$HOLE$$'
c_code local build_ref_370
	ret_reg &ref[370]
	call_c   build_ref_369()
	call_c   Dyam_Create_Binary(I(9),&ref[369],I(7))
	move_ret ref[370]
	c_ret

;; TERM 369: '*GUARD*'(lctag_simplify(_B, _D, _E, _I))
c_code local build_ref_369
	ret_reg &ref[369]
	call_c   build_ref_55()
	call_c   build_ref_368()
	call_c   Dyam_Create_Unary(&ref[55],&ref[368])
	move_ret ref[369]
	c_ret

;; TERM 368: lctag_simplify(_B, _D, _E, _I)
c_code local build_ref_368
	ret_reg &ref[368]
	call_c   build_ref_1124()
	call_c   Dyam_Term_Start(&ref[1124],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_End()
	move_ret ref[368]
	c_ret

;; TERM 371: _H , _I
c_code local build_ref_371
	ret_reg &ref[371]
	call_c   Dyam_Create_Binary(I(4),V(7),V(8))
	move_ret ref[371]
	c_ret

long local pool_fun101[3]=[2,build_seed_91,build_ref_371]

pl_code local fun101
	call_c   Dyam_Remove_Choice()
	pl_call  fun38(&seed[91],1)
	call_c   Dyam_Unify(V(5),&ref[371])
	fail_ret
	pl_jump  fun85()

;; TERM 367: cut
c_code local build_ref_367
	ret_reg &ref[367]
	call_c   Dyam_Create_Atom("cut")
	move_ret ref[367]
	c_ret

long local pool_fun102[5]=[65539,build_ref_362,build_seed_90,build_ref_367,pool_fun101]

pl_code local fun102
	call_c   Dyam_Pool(pool_fun102)
	call_c   Dyam_Unify_Item(&ref[362])
	fail_ret
	call_c   Dyam_Allocate(0)
	pl_call  fun38(&seed[90],1)
	call_c   Dyam_Choice(fun101)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(6),&ref[367])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(5),V(7))
	fail_ret
	call_c   Dyam_Unify(V(4),&ref[367])
	fail_ret
	pl_jump  fun85()

;; TERM 363: '*GUARD*'(lctag_simplify(_B, (_C , _D), _E, _F)) :> []
c_code local build_ref_363
	ret_reg &ref[363]
	call_c   build_ref_362()
	call_c   Dyam_Create_Binary(I(9),&ref[362],I(0))
	move_ret ref[363]
	c_ret

c_code local build_seed_22
	ret_reg &seed[22]
	call_c   build_ref_54()
	call_c   build_ref_374()
	call_c   Dyam_Seed_Start(&ref[54],&ref[374],I(0),fun0,1)
	call_c   build_ref_373()
	call_c   Dyam_Seed_Add_Comp(&ref[373],fun104,0)
	call_c   Dyam_Seed_End()
	move_ret seed[22]
	c_ret

;; TERM 373: '*GUARD*'(lctag_simplify(_B, (_C ; _D), _E, (_F ; _G)))
c_code local build_ref_373
	ret_reg &ref[373]
	call_c   build_ref_55()
	call_c   build_ref_372()
	call_c   Dyam_Create_Unary(&ref[55],&ref[372])
	move_ret ref[373]
	c_ret

;; TERM 372: lctag_simplify(_B, (_C ; _D), _E, (_F ; _G))
c_code local build_ref_372
	ret_reg &ref[372]
	call_c   build_ref_1124()
	call_c   build_ref_667()
	call_c   build_ref_605()
	call_c   Dyam_Term_Start(&ref[1124],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(&ref[667])
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(&ref[605])
	call_c   Dyam_Term_End()
	move_ret ref[372]
	c_ret

;; TERM 605: _F ; _G
c_code local build_ref_605
	ret_reg &ref[605]
	call_c   Dyam_Create_Binary(I(5),V(5),V(6))
	move_ret ref[605]
	c_ret

c_code local build_seed_92
	ret_reg &seed[92]
	call_c   build_ref_55()
	call_c   build_ref_376()
	call_c   Dyam_Seed_Start(&ref[55],&ref[376],I(0),fun11,1)
	call_c   build_ref_377()
	call_c   Dyam_Seed_Add_Comp(&ref[377],&ref[376],0)
	call_c   Dyam_Seed_End()
	move_ret seed[92]
	c_ret

;; TERM 377: '*GUARD*'(lctag_simplify(_B, _C, _H, _F)) :> '$$HOLE$$'
c_code local build_ref_377
	ret_reg &ref[377]
	call_c   build_ref_376()
	call_c   Dyam_Create_Binary(I(9),&ref[376],I(7))
	move_ret ref[377]
	c_ret

;; TERM 376: '*GUARD*'(lctag_simplify(_B, _C, _H, _F))
c_code local build_ref_376
	ret_reg &ref[376]
	call_c   build_ref_55()
	call_c   build_ref_375()
	call_c   Dyam_Create_Unary(&ref[55],&ref[375])
	move_ret ref[376]
	c_ret

;; TERM 375: lctag_simplify(_B, _C, _H, _F)
c_code local build_ref_375
	ret_reg &ref[375]
	call_c   build_ref_1124()
	call_c   Dyam_Term_Start(&ref[1124],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[375]
	c_ret

c_code local build_seed_93
	ret_reg &seed[93]
	call_c   build_ref_55()
	call_c   build_ref_379()
	call_c   Dyam_Seed_Start(&ref[55],&ref[379],I(0),fun11,1)
	call_c   build_ref_380()
	call_c   Dyam_Seed_Add_Comp(&ref[380],&ref[379],0)
	call_c   Dyam_Seed_End()
	move_ret seed[93]
	c_ret

;; TERM 380: '*GUARD*'(lctag_simplify(_B, _D, _I, _G)) :> '$$HOLE$$'
c_code local build_ref_380
	ret_reg &ref[380]
	call_c   build_ref_379()
	call_c   Dyam_Create_Binary(I(9),&ref[379],I(7))
	move_ret ref[380]
	c_ret

;; TERM 379: '*GUARD*'(lctag_simplify(_B, _D, _I, _G))
c_code local build_ref_379
	ret_reg &ref[379]
	call_c   build_ref_55()
	call_c   build_ref_378()
	call_c   Dyam_Create_Unary(&ref[55],&ref[378])
	move_ret ref[379]
	c_ret

;; TERM 378: lctag_simplify(_B, _D, _I, _G)
c_code local build_ref_378
	ret_reg &ref[378]
	call_c   build_ref_1124()
	call_c   Dyam_Term_Start(&ref[1124],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[378]
	c_ret

long local pool_fun103[2]=[1,build_ref_300]

pl_code local fun103
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(4),&ref[300])
	fail_ret
	pl_jump  fun85()

long local pool_fun104[6]=[65540,build_ref_373,build_seed_92,build_seed_93,build_ref_367,pool_fun103]

pl_code local fun104
	call_c   Dyam_Pool(pool_fun104)
	call_c   Dyam_Unify_Item(&ref[373])
	fail_ret
	call_c   Dyam_Allocate(0)
	pl_call  fun38(&seed[92],1)
	pl_call  fun38(&seed[93],1)
	call_c   Dyam_Choice(fun103)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(7),&ref[367])
	fail_ret
	call_c   Dyam_Unify(V(8),&ref[367])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(4),&ref[367])
	fail_ret
	pl_jump  fun85()

;; TERM 374: '*GUARD*'(lctag_simplify(_B, (_C ; _D), _E, (_F ; _G))) :> []
c_code local build_ref_374
	ret_reg &ref[374]
	call_c   build_ref_373()
	call_c   Dyam_Create_Binary(I(9),&ref[373],I(0))
	move_ret ref[374]
	c_ret

c_code local build_seed_23
	ret_reg &seed[23]
	call_c   build_ref_54()
	call_c   build_ref_383()
	call_c   Dyam_Seed_Start(&ref[54],&ref[383],I(0),fun0,1)
	call_c   build_ref_382()
	call_c   Dyam_Seed_Add_Comp(&ref[382],fun105,0)
	call_c   Dyam_Seed_End()
	move_ret seed[23]
	c_ret

;; TERM 382: '*GUARD*'(lctag_simplify(_B, (_C ## _D), _E, (_F ## _G)))
c_code local build_ref_382
	ret_reg &ref[382]
	call_c   build_ref_55()
	call_c   build_ref_381()
	call_c   Dyam_Create_Unary(&ref[55],&ref[381])
	move_ret ref[382]
	c_ret

;; TERM 381: lctag_simplify(_B, (_C ## _D), _E, (_F ## _G))
c_code local build_ref_381
	ret_reg &ref[381]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_1151()
	call_c   Dyam_Create_Binary(&ref[1151],V(2),V(3))
	move_ret R(0)
	call_c   Dyam_Create_Binary(&ref[1151],V(5),V(6))
	move_ret R(1)
	call_c   build_ref_1124()
	call_c   Dyam_Term_Start(&ref[1124],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_End()
	move_ret ref[381]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun105[6]=[65540,build_ref_382,build_seed_92,build_seed_93,build_ref_367,pool_fun103]

pl_code local fun105
	call_c   Dyam_Pool(pool_fun105)
	call_c   Dyam_Unify_Item(&ref[382])
	fail_ret
	call_c   Dyam_Allocate(0)
	pl_call  fun38(&seed[92],1)
	pl_call  fun38(&seed[93],1)
	call_c   Dyam_Choice(fun103)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(7),&ref[367])
	fail_ret
	call_c   Dyam_Unify(V(8),&ref[367])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(4),&ref[367])
	fail_ret
	pl_jump  fun85()

;; TERM 383: '*GUARD*'(lctag_simplify(_B, (_C ## _D), _E, (_F ## _G))) :> []
c_code local build_ref_383
	ret_reg &ref[383]
	call_c   build_ref_382()
	call_c   Dyam_Create_Binary(I(9),&ref[382],I(0))
	move_ret ref[383]
	c_ret

c_code local build_seed_20
	ret_reg &seed[20]
	call_c   build_ref_54()
	call_c   build_ref_386()
	call_c   Dyam_Seed_Start(&ref[54],&ref[386],I(0),fun0,1)
	call_c   build_ref_385()
	call_c   Dyam_Seed_Add_Comp(&ref[385],fun107,0)
	call_c   Dyam_Seed_End()
	move_ret seed[20]
	c_ret

;; TERM 385: '*GUARD*'(lctag_simplify(_B, [_C|_D], _E, _F))
c_code local build_ref_385
	ret_reg &ref[385]
	call_c   build_ref_55()
	call_c   build_ref_384()
	call_c   Dyam_Create_Unary(&ref[55],&ref[384])
	move_ret ref[385]
	c_ret

;; TERM 384: lctag_simplify(_B, [_C|_D], _E, _F)
c_code local build_ref_384
	ret_reg &ref[384]
	call_c   build_ref_1124()
	call_c   build_ref_428()
	call_c   Dyam_Term_Start(&ref[1124],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(&ref[428])
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[384]
	c_ret

;; TERM 428: [_C|_D]
c_code local build_ref_428
	ret_reg &ref[428]
	call_c   Dyam_Create_List(V(2),V(3))
	move_ret ref[428]
	c_ret

;; TERM 388: [_H|_I]
c_code local build_ref_388
	ret_reg &ref[388]
	call_c   Dyam_Create_List(V(7),V(8))
	move_ret ref[388]
	c_ret

long local pool_fun106[3]=[2,build_seed_91,build_ref_388]

pl_code local fun106
	call_c   Dyam_Remove_Choice()
	pl_call  fun38(&seed[91],1)
	call_c   Dyam_Unify(V(5),&ref[388])
	fail_ret
	pl_jump  fun85()

;; TERM 387: [_H]
c_code local build_ref_387
	ret_reg &ref[387]
	call_c   Dyam_Create_List(V(7),I(0))
	move_ret ref[387]
	c_ret

long local pool_fun107[6]=[65540,build_ref_385,build_seed_90,build_ref_387,build_ref_367,pool_fun106]

pl_code local fun107
	call_c   Dyam_Pool(pool_fun107)
	call_c   Dyam_Unify_Item(&ref[385])
	fail_ret
	call_c   Dyam_Allocate(0)
	pl_call  fun38(&seed[90],1)
	call_c   Dyam_Choice(fun106)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(6),&ref[367])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(5),&ref[387])
	fail_ret
	call_c   Dyam_Unify(V(4),&ref[367])
	fail_ret
	pl_jump  fun85()

;; TERM 386: '*GUARD*'(lctag_simplify(_B, [_C|_D], _E, _F)) :> []
c_code local build_ref_386
	ret_reg &ref[386]
	call_c   build_ref_385()
	call_c   Dyam_Create_Binary(I(9),&ref[385],I(0))
	move_ret ref[386]
	c_ret

c_code local build_seed_41
	ret_reg &seed[41]
	call_c   build_ref_54()
	call_c   build_ref_391()
	call_c   Dyam_Seed_Start(&ref[54],&ref[391],I(0),fun0,1)
	call_c   build_ref_390()
	call_c   Dyam_Seed_Add_Comp(&ref[390],fun108,0)
	call_c   Dyam_Seed_End()
	move_ret seed[41]
	c_ret

;; TERM 390: '*GUARD*'(tig_viewer(right, _B, _C, _D, _E, _F, _G))
c_code local build_ref_390
	ret_reg &ref[390]
	call_c   build_ref_55()
	call_c   build_ref_389()
	call_c   Dyam_Create_Unary(&ref[55],&ref[389])
	move_ret ref[390]
	c_ret

;; TERM 389: tig_viewer(right, _B, _C, _D, _E, _F, _G)
c_code local build_ref_389
	ret_reg &ref[389]
	call_c   build_ref_1168()
	call_c   build_ref_458()
	call_c   Dyam_Term_Start(&ref[1168],7)
	call_c   Dyam_Term_Arg(&ref[458])
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[389]
	c_ret

;; TERM 392: '*RITEM*'(_F, _G)
c_code local build_ref_392
	ret_reg &ref[392]
	call_c   build_ref_0()
	call_c   Dyam_Create_Binary(&ref[0],V(5),V(6))
	move_ret ref[392]
	c_ret

;; TERM 396: _B(_D,_E) * _C(_E,_E)
c_code local build_ref_396
	ret_reg &ref[396]
	call_c   build_ref_393()
	call_c   build_ref_394()
	call_c   build_ref_395()
	call_c   Dyam_Create_Binary(&ref[393],&ref[394],&ref[395])
	move_ret ref[396]
	c_ret

;; TERM 395: _C(_E,_E)
c_code local build_ref_395
	ret_reg &ref[395]
	call_c   Dyam_Term_Start(I(3),3)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[395]
	c_ret

;; TERM 394: _B(_D,_E)
c_code local build_ref_394
	ret_reg &ref[394]
	call_c   Dyam_Term_Start(I(3),3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[394]
	c_ret

;; TERM 393: *
c_code local build_ref_393
	ret_reg &ref[393]
	call_c   Dyam_Create_Atom("*")
	move_ret ref[393]
	c_ret

;; TERM 397: viewer(_I, _J)
c_code local build_ref_397
	ret_reg &ref[397]
	call_c   build_ref_1205()
	call_c   Dyam_Create_Binary(&ref[1205],V(8),V(9))
	move_ret ref[397]
	c_ret

;; TERM 1205: viewer
c_code local build_ref_1205
	ret_reg &ref[1205]
	call_c   Dyam_Create_Atom("viewer")
	move_ret ref[1205]
	c_ret

long local pool_fun108[5]=[4,build_ref_390,build_ref_392,build_ref_396,build_ref_397]

pl_code local fun108
	call_c   Dyam_Pool(pool_fun108)
	call_c   Dyam_Unify_Item(&ref[390])
	fail_ret
	call_c   Dyam_Unify(V(7),&ref[392])
	fail_ret
	call_c   DYAM_Numbervars_3(V(7),N(0),V(10))
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(7))
	move     V(8), R(2)
	move     S(5), R(3)
	pl_call  pred_label_term_2()
	move     &ref[396], R(0)
	move     S(5), R(1)
	move     V(9), R(2)
	move     S(5), R(3)
	pl_call  pred_label_term_2()
	move     &ref[397], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Deallocate(1)
	pl_jump  pred_record_without_doublon_1()

;; TERM 391: '*GUARD*'(tig_viewer(right, _B, _C, _D, _E, _F, _G)) :> []
c_code local build_ref_391
	ret_reg &ref[391]
	call_c   build_ref_390()
	call_c   Dyam_Create_Binary(I(9),&ref[390],I(0))
	move_ret ref[391]
	c_ret

c_code local build_seed_42
	ret_reg &seed[42]
	call_c   build_ref_54()
	call_c   build_ref_400()
	call_c   Dyam_Seed_Start(&ref[54],&ref[400],I(0),fun0,1)
	call_c   build_ref_399()
	call_c   Dyam_Seed_Add_Comp(&ref[399],fun109,0)
	call_c   Dyam_Seed_End()
	move_ret seed[42]
	c_ret

;; TERM 399: '*GUARD*'(tig_viewer(left, _B, _C, _D, _E, _F, _G))
c_code local build_ref_399
	ret_reg &ref[399]
	call_c   build_ref_55()
	call_c   build_ref_398()
	call_c   Dyam_Create_Unary(&ref[55],&ref[398])
	move_ret ref[399]
	c_ret

;; TERM 398: tig_viewer(left, _B, _C, _D, _E, _F, _G)
c_code local build_ref_398
	ret_reg &ref[398]
	call_c   build_ref_1168()
	call_c   build_ref_451()
	call_c   Dyam_Term_Start(&ref[1168],7)
	call_c   Dyam_Term_Arg(&ref[451])
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[398]
	c_ret

;; TERM 403: _B(_D,_D) * _C(_D,_E)
c_code local build_ref_403
	ret_reg &ref[403]
	call_c   build_ref_393()
	call_c   build_ref_401()
	call_c   build_ref_402()
	call_c   Dyam_Create_Binary(&ref[393],&ref[401],&ref[402])
	move_ret ref[403]
	c_ret

;; TERM 402: _C(_D,_E)
c_code local build_ref_402
	ret_reg &ref[402]
	call_c   Dyam_Term_Start(I(3),3)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[402]
	c_ret

;; TERM 401: _B(_D,_D)
c_code local build_ref_401
	ret_reg &ref[401]
	call_c   Dyam_Term_Start(I(3),3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[401]
	c_ret

long local pool_fun109[5]=[4,build_ref_399,build_ref_392,build_ref_403,build_ref_397]

pl_code local fun109
	call_c   Dyam_Pool(pool_fun109)
	call_c   Dyam_Unify_Item(&ref[399])
	fail_ret
	call_c   Dyam_Unify(V(7),&ref[392])
	fail_ret
	call_c   DYAM_Numbervars_3(V(7),N(0),V(10))
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(7))
	move     V(8), R(2)
	move     S(5), R(3)
	pl_call  pred_label_term_2()
	move     &ref[403], R(0)
	move     S(5), R(1)
	move     V(9), R(2)
	move     S(5), R(3)
	pl_call  pred_label_term_2()
	move     &ref[397], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Deallocate(1)
	pl_jump  pred_record_without_doublon_1()

;; TERM 400: '*GUARD*'(tig_viewer(left, _B, _C, _D, _E, _F, _G)) :> []
c_code local build_ref_400
	ret_reg &ref[400]
	call_c   build_ref_399()
	call_c   Dyam_Create_Binary(I(9),&ref[399],I(0))
	move_ret ref[400]
	c_ret

c_code local build_seed_19
	ret_reg &seed[19]
	call_c   build_ref_54()
	call_c   build_ref_421()
	call_c   Dyam_Seed_Start(&ref[54],&ref[421],I(0),fun0,1)
	call_c   build_ref_420()
	call_c   Dyam_Seed_Add_Comp(&ref[420],fun119,0)
	call_c   Dyam_Seed_End()
	move_ret seed[19]
	c_ret

;; TERM 420: '*GUARD*'(lctag_simplify(_B, @*{goal=> _C, vars=> _D, from=> _E, to=> _F, collect_first=> _G, collect_last=> _H, collect_loop=> _I, collect_next=> _J, collect_pred=> _K}, keep, @*{goal=> _L, vars=> _M, from=> _N, to=> _O, collect_first=> _P, collect_last=> _Q, collect_loop=> _R, collect_next=> _S, collect_pred=> _T}))
c_code local build_ref_420
	ret_reg &ref[420]
	call_c   build_ref_55()
	call_c   build_ref_419()
	call_c   Dyam_Create_Unary(&ref[55],&ref[419])
	move_ret ref[420]
	c_ret

;; TERM 419: lctag_simplify(_B, @*{goal=> _C, vars=> _D, from=> _E, to=> _F, collect_first=> _G, collect_last=> _H, collect_loop=> _I, collect_next=> _J, collect_pred=> _K}, keep, @*{goal=> _L, vars=> _M, from=> _N, to=> _O, collect_first=> _P, collect_last=> _Q, collect_loop=> _R, collect_next=> _S, collect_pred=> _T})
c_code local build_ref_419
	ret_reg &ref[419]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_1187()
	call_c   Dyam_Term_Start(&ref[1187],9)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   Dyam_Term_Start(&ref[1187],9)
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_End()
	move_ret R(1)
	call_c   build_ref_1124()
	call_c   build_ref_300()
	call_c   Dyam_Term_Start(&ref[1124],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[300])
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_End()
	move_ret ref[419]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_96
	ret_reg &seed[96]
	call_c   build_ref_55()
	call_c   build_ref_423()
	call_c   Dyam_Seed_Start(&ref[55],&ref[423],I(0),fun11,1)
	call_c   build_ref_424()
	call_c   Dyam_Seed_Add_Comp(&ref[424],&ref[423],0)
	call_c   Dyam_Seed_End()
	move_ret seed[96]
	c_ret

;; TERM 424: '*GUARD*'(lctag_simplify(_B, _C, _U, _L)) :> '$$HOLE$$'
c_code local build_ref_424
	ret_reg &ref[424]
	call_c   build_ref_423()
	call_c   Dyam_Create_Binary(I(9),&ref[423],I(7))
	move_ret ref[424]
	c_ret

;; TERM 423: '*GUARD*'(lctag_simplify(_B, _C, _U, _L))
c_code local build_ref_423
	ret_reg &ref[423]
	call_c   build_ref_55()
	call_c   build_ref_422()
	call_c   Dyam_Create_Unary(&ref[55],&ref[422])
	move_ret ref[423]
	c_ret

;; TERM 422: lctag_simplify(_B, _C, _U, _L)
c_code local build_ref_422
	ret_reg &ref[422]
	call_c   build_ref_1124()
	call_c   Dyam_Term_Start(&ref[1124],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_End()
	move_ret ref[422]
	c_ret

long local pool_fun119[3]=[2,build_ref_420,build_seed_96]

pl_code local fun119
	call_c   Dyam_Pool(pool_fun119)
	call_c   Dyam_Unify_Item(&ref[420])
	fail_ret
	pl_jump  fun38(&seed[96],1)

;; TERM 421: '*GUARD*'(lctag_simplify(_B, @*{goal=> _C, vars=> _D, from=> _E, to=> _F, collect_first=> _G, collect_last=> _H, collect_loop=> _I, collect_next=> _J, collect_pred=> _K}, keep, @*{goal=> _L, vars=> _M, from=> _N, to=> _O, collect_first=> _P, collect_last=> _Q, collect_loop=> _R, collect_next=> _S, collect_pred=> _T})) :> []
c_code local build_ref_421
	ret_reg &ref[421]
	call_c   build_ref_420()
	call_c   Dyam_Create_Binary(I(9),&ref[420],I(0))
	move_ret ref[421]
	c_ret

c_code local build_seed_9
	ret_reg &seed[9]
	call_c   build_ref_63()
	call_c   build_ref_406()
	call_c   Dyam_Seed_Start(&ref[63],&ref[406],I(0),fun0,1)
	call_c   build_ref_407()
	call_c   Dyam_Seed_Add_Comp(&ref[407],fun117,0)
	call_c   Dyam_Seed_End()
	move_ret seed[9]
	c_ret

;; TERM 407: '*PROLOG-ITEM*'{top=> lctag_first(_B, guard{goal=> _C, plus=> _D, minus=> _E}, _F, _G), cont=> _A}
c_code local build_ref_407
	ret_reg &ref[407]
	call_c   build_ref_1126()
	call_c   build_ref_404()
	call_c   Dyam_Create_Binary(&ref[1126],&ref[404],V(0))
	move_ret ref[407]
	c_ret

;; TERM 404: lctag_first(_B, guard{goal=> _C, plus=> _D, minus=> _E}, _F, _G)
c_code local build_ref_404
	ret_reg &ref[404]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1198()
	call_c   Dyam_Term_Start(&ref[1198],3)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_1127()
	call_c   Dyam_Term_Start(&ref[1127],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[404]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

pl_code local fun116
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(5),&ref[305])
	fail_ret
	call_c   Dyam_Unify(V(6),I(0))
	fail_ret
	call_c   Dyam_Reg_Load(0,V(4))
	pl_call  pred_guard_eval_1()
	call_c   Dyam_Deallocate()
	pl_jump  Follow_Cont(V(0))

c_code local build_seed_94
	ret_reg &seed[94]
	call_c   build_ref_176()
	call_c   build_ref_408()
	call_c   Dyam_Seed_Start(&ref[176],&ref[408],I(0),fun11,1)
	call_c   build_ref_411()
	call_c   Dyam_Seed_Add_Comp(&ref[411],&ref[408],0)
	call_c   Dyam_Seed_End()
	move_ret seed[94]
	c_ret

;; TERM 411: '*PROLOG-FIRST*'(lctag_first(_B, _C, _F, _G)) :> '$$HOLE$$'
c_code local build_ref_411
	ret_reg &ref[411]
	call_c   build_ref_410()
	call_c   Dyam_Create_Binary(I(9),&ref[410],I(7))
	move_ret ref[411]
	c_ret

;; TERM 410: '*PROLOG-FIRST*'(lctag_first(_B, _C, _F, _G))
c_code local build_ref_410
	ret_reg &ref[410]
	call_c   build_ref_64()
	call_c   build_ref_409()
	call_c   Dyam_Create_Unary(&ref[64],&ref[409])
	move_ret ref[410]
	c_ret

;; TERM 409: lctag_first(_B, _C, _F, _G)
c_code local build_ref_409
	ret_reg &ref[409]
	call_c   build_ref_1127()
	call_c   Dyam_Term_Start(&ref[1127],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[409]
	c_ret

;; TERM 408: '*PROLOG-ITEM*'{top=> lctag_first(_B, _C, _F, _G), cont=> '$CLOSURE'('$fun'(115, 0, 1125371372), '$TUPPLE'(35144395402176))}
c_code local build_ref_408
	ret_reg &ref[408]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,301989888)
	move_ret R(0)
	call_c   Dyam_Closure_Aux(fun115,R(0))
	move_ret R(0)
	call_c   build_ref_1126()
	call_c   build_ref_409()
	call_c   Dyam_Create_Binary(&ref[1126],&ref[409],R(0))
	move_ret ref[408]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

pl_code local fun113
	call_c   Dyam_Remove_Choice()
fun110:
	call_c   Dyam_Cut()
	call_c   Dyam_Deallocate()
	pl_jump  Follow_Cont(V(0))


pl_code local fun112
	call_c   Dyam_Remove_Choice()
fun111:
	call_c   Dyam_Cut()
	pl_fail


pl_code local fun115
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun114)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Choice(fun113)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Choice(fun112)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Reg_Load(0,V(3))
	pl_call  pred_guard_eval_1()
	call_c   Dyam_Cut()
	pl_fail

long local pool_fun117[4]=[3,build_ref_407,build_ref_305,build_seed_94]

pl_code local fun117
	call_c   Dyam_Pool(pool_fun117)
	call_c   Dyam_Unify_Item(&ref[407])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun116)
	call_c   Dyam_Deallocate()
	pl_jump  fun38(&seed[94],12)

;; TERM 406: '*PROLOG-FIRST*'(lctag_first(_B, guard{goal=> _C, plus=> _D, minus=> _E}, _F, _G)) :> []
c_code local build_ref_406
	ret_reg &ref[406]
	call_c   build_ref_405()
	call_c   Dyam_Create_Binary(I(9),&ref[405],I(0))
	move_ret ref[406]
	c_ret

;; TERM 405: '*PROLOG-FIRST*'(lctag_first(_B, guard{goal=> _C, plus=> _D, minus=> _E}, _F, _G))
c_code local build_ref_405
	ret_reg &ref[405]
	call_c   build_ref_64()
	call_c   build_ref_404()
	call_c   Dyam_Create_Unary(&ref[64],&ref[404])
	move_ret ref[405]
	c_ret

c_code local build_seed_17
	ret_reg &seed[17]
	call_c   build_ref_93()
	call_c   build_ref_412()
	call_c   Dyam_Seed_Start(&ref[93],&ref[412],I(0),fun21,1)
	call_c   build_ref_413()
	call_c   Dyam_Seed_Add_Comp(&ref[413],fun209,0)
	call_c   Dyam_Seed_End()
	move_ret seed[17]
	c_ret

;; TERM 413: '*CITEM*'('call_lctag_closure/4', _A)
c_code local build_ref_413
	ret_reg &ref[413]
	call_c   build_ref_97()
	call_c   build_ref_1()
	call_c   Dyam_Create_Binary(&ref[97],&ref[1],V(0))
	move_ret ref[413]
	c_ret

;; TERM 1: 'call_lctag_closure/4'
c_code local build_ref_1
	ret_reg &ref[1]
	call_c   Dyam_Create_Atom("call_lctag_closure/4")
	move_ret ref[1]
	c_ret

c_code local build_seed_106
	ret_reg &seed[106]
	call_c   build_ref_133()
	call_c   build_ref_520()
	call_c   Dyam_Seed_Start(&ref[133],&ref[520],I(0),fun21,1)
	call_c   build_ref_518()
	call_c   Dyam_Seed_Add_Comp(&ref[518],fun173,0)
	call_c   Dyam_Seed_End()
	move_ret seed[106]
	c_ret

;; TERM 518: '*RITEM*'('call_normalize/2'(tag_tree{family=> _F, name=> _D, tree=> _G}), return(guard{goal=> tag_node{id=> _H, label=> _I, children=> _J, kind=> _K, adj=> _L, spine=> _M, top=> _N, bot=> _O, token=> _P, adjleft=> _Q, adjright=> _R, adjwrap=> _S}, plus=> _T, minus=> _U}))
c_code local build_ref_518
	ret_reg &ref[518]
	call_c   build_ref_0()
	call_c   build_ref_414()
	call_c   build_ref_517()
	call_c   Dyam_Create_Binary(&ref[0],&ref[414],&ref[517])
	move_ret ref[518]
	c_ret

;; TERM 517: return(guard{goal=> tag_node{id=> _H, label=> _I, children=> _J, kind=> _K, adj=> _L, spine=> _M, top=> _N, bot=> _O, token=> _P, adjleft=> _Q, adjright=> _R, adjwrap=> _S}, plus=> _T, minus=> _U})
c_code local build_ref_517
	ret_reg &ref[517]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1198()
	call_c   build_ref_476()
	call_c   Dyam_Term_Start(&ref[1198],3)
	call_c   Dyam_Term_Arg(&ref[476])
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_1139()
	call_c   Dyam_Create_Unary(&ref[1139],R(0))
	move_ret ref[517]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 476: tag_node{id=> _H, label=> _I, children=> _J, kind=> _K, adj=> _L, spine=> _M, top=> _N, bot=> _O, token=> _P, adjleft=> _Q, adjright=> _R, adjwrap=> _S}
c_code local build_ref_476
	ret_reg &ref[476]
	call_c   build_ref_1172()
	call_c   Dyam_Term_Start(&ref[1172],12)
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_End()
	move_ret ref[476]
	c_ret

;; TERM 414: 'call_normalize/2'(tag_tree{family=> _F, name=> _D, tree=> _G})
c_code local build_ref_414
	ret_reg &ref[414]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1142()
	call_c   Dyam_Term_Start(&ref[1142],3)
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_1185()
	call_c   Dyam_Create_Unary(&ref[1185],R(0))
	move_ret ref[414]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

pl_code local fun173
	call_c   build_ref_518()
	call_c   Dyam_Unify_Item(&ref[518])
	fail_ret
	pl_ret

;; TERM 520: '*RITEM*'('call_normalize/2'(tag_tree{family=> _F, name=> _D, tree=> _G}), return(guard{goal=> tag_node{id=> _H, label=> _I, children=> _J, kind=> _K, adj=> _L, spine=> _M, top=> _N, bot=> _O, token=> _P, adjleft=> _Q, adjright=> _R, adjwrap=> _S}, plus=> _T, minus=> _U})) :> tig(10, '$TUPPLE'(35144395401924))
c_code local build_ref_520
	ret_reg &ref[520]
	call_c   build_ref_518()
	call_c   build_ref_519()
	call_c   Dyam_Create_Binary(I(9),&ref[518],&ref[519])
	move_ret ref[520]
	c_ret

;; TERM 519: tig(10, '$TUPPLE'(35144395401924))
c_code local build_ref_519
	ret_reg &ref[519]
	call_c   build_ref_1154()
	call_c   build_ref_719()
	call_c   Dyam_Create_Binary(&ref[1154],N(10),&ref[719])
	move_ret ref[519]
	c_ret

;; TERM 516: '$CLOSURE'('$fun'(171, 0, 1125788900), '$TUPPLE'(35144395138456))
c_code local build_ref_516
	ret_reg &ref[516]
	call_c   build_ref_515()
	call_c   Dyam_Closure_Aux(fun171,&ref[515])
	move_ret ref[516]
	c_ret

;; TERM 513: subst(_I, _N)
c_code local build_ref_513
	ret_reg &ref[513]
	call_c   build_ref_1080()
	call_c   Dyam_Create_Binary(&ref[1080],V(8),V(13))
	move_ret ref[513]
	c_ret

;; TERM 1080: subst
c_code local build_ref_1080
	ret_reg &ref[1080]
	call_c   Dyam_Create_Atom("subst")
	move_ret ref[1080]
	c_ret

c_code local build_seed_105
	ret_reg &seed[105]
	call_c   build_ref_176()
	call_c   build_ref_493()
	call_c   Dyam_Seed_Start(&ref[176],&ref[493],I(0),fun11,1)
	call_c   build_ref_496()
	call_c   Dyam_Seed_Add_Comp(&ref[496],&ref[493],0)
	call_c   Dyam_Seed_End()
	move_ret seed[105]
	c_ret

;; TERM 496: '*PROLOG-FIRST*'(lctag_first(_D, _X, _I2, _E)) :> '$$HOLE$$'
c_code local build_ref_496
	ret_reg &ref[496]
	call_c   build_ref_495()
	call_c   Dyam_Create_Binary(I(9),&ref[495],I(7))
	move_ret ref[496]
	c_ret

;; TERM 495: '*PROLOG-FIRST*'(lctag_first(_D, _X, _I2, _E))
c_code local build_ref_495
	ret_reg &ref[495]
	call_c   build_ref_64()
	call_c   build_ref_494()
	call_c   Dyam_Create_Unary(&ref[64],&ref[494])
	move_ret ref[495]
	c_ret

;; TERM 494: lctag_first(_D, _X, _I2, _E)
c_code local build_ref_494
	ret_reg &ref[494]
	call_c   build_ref_1127()
	call_c   Dyam_Term_Start(&ref[1127],4)
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(23))
	call_c   Dyam_Term_Arg(V(60))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[494]
	c_ret

;; TERM 493: '*PROLOG-ITEM*'{top=> lctag_first(_D, _X, _I2, _E), cont=> '$CLOSURE'('$fun'(159, 0, 1125714616), '$TUPPLE'(35144395137400))}
c_code local build_ref_493
	ret_reg &ref[493]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Start_Tupple(0,452984832)
	call_c   Dyam_Almost_End_Tupple(58,67108864)
	move_ret R(0)
	call_c   Dyam_Closure_Aux(fun159,R(0))
	move_ret R(0)
	call_c   build_ref_1126()
	call_c   build_ref_494()
	call_c   Dyam_Create_Binary(&ref[1126],&ref[494],R(0))
	move_ret ref[493]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_104
	ret_reg &seed[104]
	call_c   build_ref_0()
	call_c   build_ref_489()
	call_c   Dyam_Seed_Start(&ref[0],&ref[489],&ref[489],fun11,1)
	call_c   build_ref_490()
	call_c   Dyam_Seed_Add_Comp(&ref[490],&ref[489],0)
	call_c   Dyam_Seed_End()
	move_ret seed[104]
	c_ret

;; TERM 490: '*RITEM*'(_A, return(_B, _C, _D, _E)) :> '$$HOLE$$'
c_code local build_ref_490
	ret_reg &ref[490]
	call_c   build_ref_489()
	call_c   Dyam_Create_Binary(I(9),&ref[489],I(7))
	move_ret ref[490]
	c_ret

;; TERM 489: '*RITEM*'(_A, return(_B, _C, _D, _E))
c_code local build_ref_489
	ret_reg &ref[489]
	call_c   build_ref_0()
	call_c   build_ref_488()
	call_c   Dyam_Create_Binary(&ref[0],V(0),&ref[488])
	move_ret ref[489]
	c_ret

;; TERM 488: return(_B, _C, _D, _E)
c_code local build_ref_488
	ret_reg &ref[488]
	call_c   build_ref_1139()
	call_c   Dyam_Term_Start(&ref[1139],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[488]
	c_ret

long local pool_fun156[2]=[1,build_seed_104]

pl_code local fun157
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(2),V(60))
	fail_ret
fun156:
	call_c   Dyam_Deallocate()
	pl_jump  fun25(&seed[104],2)


;; TERM 491: _J2 + '$VAR'(_K2, ['$SET$',lctag_labels(epsilon, true, no_epsilon),3])
c_code local build_ref_491
	ret_reg &ref[491]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1206()
	call_c   build_ref_305()
	call_c   build_ref_487()
	call_c   build_ref_1207()
	call_c   Dyam_Term_Start(&ref[1206],3)
	call_c   Dyam_Term_Arg(&ref[305])
	call_c   Dyam_Term_Arg(&ref[487])
	call_c   Dyam_Term_Arg(&ref[1207])
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   Dyam_Term_Start(I(8),3)
	call_c   Dyam_Term_Arg(V(62))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(N(3))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_254()
	call_c   Dyam_Create_Binary(&ref[254],V(61),R(0))
	move_ret ref[491]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1207: no_epsilon
c_code local build_ref_1207
	ret_reg &ref[1207]
	call_c   Dyam_Create_Atom("no_epsilon")
	move_ret ref[1207]
	c_ret

;; TERM 487: true
c_code local build_ref_487
	ret_reg &ref[487]
	call_c   Dyam_Create_Atom("true")
	move_ret ref[487]
	c_ret

;; TERM 1206: lctag_labels
c_code local build_ref_1206
	ret_reg &ref[1206]
	call_c   Dyam_Create_Atom("lctag_labels")
	move_ret ref[1206]
	c_ret

;; TERM 492: _J2 + epsilon
c_code local build_ref_492
	ret_reg &ref[492]
	call_c   build_ref_254()
	call_c   build_ref_305()
	call_c   Dyam_Create_Binary(&ref[254],V(61),&ref[305])
	move_ret ref[492]
	c_ret

long local pool_fun158[5]=[131074,build_ref_491,build_ref_492,pool_fun156,pool_fun156]

pl_code local fun158
	call_c   Dyam_Update_Choice(fun157)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(60),&ref[491])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(2),&ref[492])
	fail_ret
	pl_jump  fun156()

long local pool_fun159[5]=[131074,build_ref_487,build_ref_305,pool_fun158,pool_fun156]

pl_code local fun159
	call_c   Dyam_Pool(pool_fun159)
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun158)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(60),&ref[487])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(2),&ref[305])
	fail_ret
	pl_jump  fun156()

long local pool_fun160[2]=[1,build_seed_105]

long local pool_fun168[4]=[65538,build_ref_513,build_ref_476,pool_fun160]

pl_code local fun168
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(1),&ref[513])
	fail_ret
	call_c   Dyam_Unify(V(23),&ref[476])
	fail_ret
fun160:
	call_c   Dyam_Deallocate()
	pl_jump  fun38(&seed[105],12)


;; TERM 319: yes
c_code local build_ref_319
	ret_reg &ref[319]
	call_c   Dyam_Create_Atom("yes")
	move_ret ref[319]
	c_ret

;; TERM 508: tag_node{id=> _X1, label=> _Y1, children=> _Z1, kind=> _A2, adj=> _B2, spine=> _C2, top=> _D2, bot=> _V, token=> _E2, adjleft=> _F2, adjright=> _G2, adjwrap=> _H2}
c_code local build_ref_508
	ret_reg &ref[508]
	call_c   build_ref_1172()
	call_c   Dyam_Term_Start(&ref[1172],12)
	call_c   Dyam_Term_Arg(V(49))
	call_c   Dyam_Term_Arg(V(50))
	call_c   Dyam_Term_Arg(V(51))
	call_c   Dyam_Term_Arg(V(52))
	call_c   Dyam_Term_Arg(V(53))
	call_c   Dyam_Term_Arg(V(54))
	call_c   Dyam_Term_Arg(V(55))
	call_c   Dyam_Term_Arg(V(21))
	call_c   Dyam_Term_Arg(V(56))
	call_c   Dyam_Term_Arg(V(57))
	call_c   Dyam_Term_Arg(V(58))
	call_c   Dyam_Term_Arg(V(59))
	call_c   Dyam_Term_End()
	move_ret ref[508]
	c_ret

;; TERM 511: '$CLOSURE'('$fun'(167, 0, 1125770528), '$TUPPLE'(35144395138236))
c_code local build_ref_511
	ret_reg &ref[511]
	call_c   build_ref_510()
	call_c   Dyam_Closure_Aux(fun167,&ref[510])
	move_ret ref[511]
	c_ret

;; TERM 486: adjleft(_I, _N, _V)
c_code local build_ref_486
	ret_reg &ref[486]
	call_c   build_ref_1183()
	call_c   Dyam_Term_Start(&ref[1183],3)
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(21))
	call_c   Dyam_Term_End()
	move_ret ref[486]
	c_ret

long local pool_fun167[4]=[65538,build_ref_486,build_ref_476,pool_fun160]

pl_code local fun167
	call_c   Dyam_Pool(pool_fun167)
	call_c   Dyam_Unify(V(1),&ref[486])
	fail_ret
	call_c   Dyam_Unify(V(23),&ref[476])
	fail_ret
	call_c   Dyam_Allocate(0)
	pl_jump  fun160()

;; TERM 510: '$TUPPLE'(35144395138236)
c_code local build_ref_510
	ret_reg &ref[510]
	call_c   Dyam_Create_Simple_Tupple(0,306183296)
	move_ret ref[510]
	c_ret

long local pool_fun169[7]=[65541,build_ref_319,build_ref_476,build_ref_508,build_ref_511,build_ref_512,pool_fun168]

pl_code local fun169
	call_c   Dyam_Update_Choice(fun168)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(12),&ref[319])
	fail_ret
	call_c   Dyam_Cut()
	move     &ref[476], R(0)
	move     S(5), R(1)
	move     &ref[508], R(2)
	move     S(5), R(3)
	pl_call  pred_tree_foot_2()
	move     &ref[511], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Load(4,V(8))
	move     &ref[512], R(6)
	move     0, R(7)
	call_c   Dyam_Reg_Load(8,V(13))
	call_c   Dyam_Reg_Load(10,V(21))
	call_c   Dyam_Reg_Deallocate(6)
fun155:
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Reg_Bind(2,V(6))
	call_c   Dyam_Multi_Reg_Bind(4,2,4)
	call_c   Dyam_Choice(fun154)
	pl_call  fun29(&seed[102],1)
	pl_fail


;; TERM 501: tig(_D, right)
c_code local build_ref_501
	ret_reg &ref[501]
	call_c   build_ref_1154()
	call_c   build_ref_458()
	call_c   Dyam_Create_Binary(&ref[1154],V(3),&ref[458])
	move_ret ref[501]
	c_ret

;; TERM 507: '$VAR'(_L1, ['$SET$',adj(no, yes, strict, atmostone, one, sync),62])
c_code local build_ref_507
	ret_reg &ref[507]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1178()
	call_c   build_ref_706()
	call_c   build_ref_319()
	call_c   build_ref_673()
	call_c   build_ref_685()
	call_c   build_ref_686()
	call_c   build_ref_623()
	call_c   Dyam_Term_Start(&ref[1178],6)
	call_c   Dyam_Term_Arg(&ref[706])
	call_c   Dyam_Term_Arg(&ref[319])
	call_c   Dyam_Term_Arg(&ref[673])
	call_c   Dyam_Term_Arg(&ref[685])
	call_c   Dyam_Term_Arg(&ref[686])
	call_c   Dyam_Term_Arg(&ref[623])
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   Dyam_Term_Start(I(8),3)
	call_c   Dyam_Term_Arg(V(37))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(N(62))
	call_c   Dyam_Term_End()
	move_ret ref[507]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 623: sync
c_code local build_ref_623
	ret_reg &ref[623]
	call_c   Dyam_Create_Atom("sync")
	move_ret ref[623]
	c_ret

;; TERM 503: tag_node{id=> _M1, label=> _N1, children=> _O1, kind=> _P1, adj=> _Q1, spine=> _R1, top=> _S1, bot=> _V, token=> _T1, adjleft=> _U1, adjright=> _V1, adjwrap=> _W1}
c_code local build_ref_503
	ret_reg &ref[503]
	call_c   build_ref_1172()
	call_c   Dyam_Term_Start(&ref[1172],12)
	call_c   Dyam_Term_Arg(V(38))
	call_c   Dyam_Term_Arg(V(39))
	call_c   Dyam_Term_Arg(V(40))
	call_c   Dyam_Term_Arg(V(41))
	call_c   Dyam_Term_Arg(V(42))
	call_c   Dyam_Term_Arg(V(43))
	call_c   Dyam_Term_Arg(V(44))
	call_c   Dyam_Term_Arg(V(21))
	call_c   Dyam_Term_Arg(V(45))
	call_c   Dyam_Term_Arg(V(46))
	call_c   Dyam_Term_Arg(V(47))
	call_c   Dyam_Term_Arg(V(48))
	call_c   Dyam_Term_End()
	move_ret ref[503]
	c_ret

;; TERM 506: '$CLOSURE'('$fun'(164, 0, 1125752552), '$TUPPLE'(35144395137452))
c_code local build_ref_506
	ret_reg &ref[506]
	call_c   build_ref_498()
	call_c   Dyam_Closure_Aux(fun164,&ref[498])
	move_ret ref[506]
	c_ret

;; TERM 504: adjright(_I, _N, _V)
c_code local build_ref_504
	ret_reg &ref[504]
	call_c   build_ref_1140()
	call_c   Dyam_Term_Start(&ref[1140],3)
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(21))
	call_c   Dyam_Term_End()
	move_ret ref[504]
	c_ret

long local pool_fun164[3]=[65537,build_ref_504,pool_fun160]

pl_code local fun164
	call_c   Dyam_Pool(pool_fun164)
	call_c   Dyam_Unify(V(1),&ref[504])
	fail_ret
	call_c   Dyam_Allocate(0)
	pl_jump  fun160()

;; TERM 498: '$TUPPLE'(35144395137452)
c_code local build_ref_498
	ret_reg &ref[498]
	call_c   Dyam_Create_Simple_Tupple(0,303071392)
	move_ret ref[498]
	c_ret

long local pool_fun165[5]=[4,build_ref_476,build_ref_503,build_ref_506,build_ref_458]

long local pool_fun166[4]=[65538,build_ref_507,build_ref_476,pool_fun165]

pl_code local fun166
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(17),&ref[507])
	fail_ret
	call_c   Dyam_Unify(V(23),&ref[476])
	fail_ret
fun165:
	move     &ref[476], R(0)
	move     S(5), R(1)
	move     &ref[503], R(2)
	move     S(5), R(3)
	pl_call  pred_tree_foot_2()
	move     &ref[506], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Load(4,V(8))
	move     &ref[458], R(6)
	move     0, R(7)
	call_c   Dyam_Reg_Load(8,V(13))
	call_c   Dyam_Reg_Load(10,V(21))
	call_c   Dyam_Reg_Deallocate(6)
	pl_jump  fun155()


;; TERM 502: '$VAR'(_K1, ['$SET$',adj(no, yes, strict, atmostone, one, sync),43])
c_code local build_ref_502
	ret_reg &ref[502]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1178()
	call_c   build_ref_706()
	call_c   build_ref_319()
	call_c   build_ref_673()
	call_c   build_ref_685()
	call_c   build_ref_686()
	call_c   build_ref_623()
	call_c   Dyam_Term_Start(&ref[1178],6)
	call_c   Dyam_Term_Arg(&ref[706])
	call_c   Dyam_Term_Arg(&ref[319])
	call_c   Dyam_Term_Arg(&ref[673])
	call_c   Dyam_Term_Arg(&ref[685])
	call_c   Dyam_Term_Arg(&ref[686])
	call_c   Dyam_Term_Arg(&ref[623])
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   Dyam_Term_Start(I(8),3)
	call_c   Dyam_Term_Arg(V(36))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(N(43))
	call_c   Dyam_Term_End()
	move_ret ref[502]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun170[6]=[196610,build_ref_501,build_ref_502,pool_fun169,pool_fun166,pool_fun165]

pl_code local fun170
	call_c   Dyam_Update_Choice(fun169)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[501])
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun166)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(13),V(14))
	fail_ret
	call_c   Dyam_Unify(V(11),&ref[502])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(23),V(9))
	fail_ret
	pl_jump  fun165()

;; TERM 474: tig(_D, left)
c_code local build_ref_474
	ret_reg &ref[474]
	call_c   build_ref_1154()
	call_c   build_ref_451()
	call_c   Dyam_Create_Binary(&ref[1154],V(3),&ref[451])
	move_ret ref[474]
	c_ret

;; TERM 500: '$VAR'(_Y, ['$SET$',adj(no, yes, strict, atmostone, one, sync),62])
c_code local build_ref_500
	ret_reg &ref[500]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1178()
	call_c   build_ref_706()
	call_c   build_ref_319()
	call_c   build_ref_673()
	call_c   build_ref_685()
	call_c   build_ref_686()
	call_c   build_ref_623()
	call_c   Dyam_Term_Start(&ref[1178],6)
	call_c   Dyam_Term_Arg(&ref[706])
	call_c   Dyam_Term_Arg(&ref[319])
	call_c   Dyam_Term_Arg(&ref[673])
	call_c   Dyam_Term_Arg(&ref[685])
	call_c   Dyam_Term_Arg(&ref[686])
	call_c   Dyam_Term_Arg(&ref[623])
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   Dyam_Term_Start(I(8),3)
	call_c   Dyam_Term_Arg(V(24))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(N(62))
	call_c   Dyam_Term_End()
	move_ret ref[500]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 477: tag_node{id=> _Z, label=> _A1, children=> _B1, kind=> _C1, adj=> _D1, spine=> _E1, top=> _F1, bot=> _V, token=> _G1, adjleft=> _H1, adjright=> _I1, adjwrap=> _J1}
c_code local build_ref_477
	ret_reg &ref[477]
	call_c   build_ref_1172()
	call_c   Dyam_Term_Start(&ref[1172],12)
	call_c   Dyam_Term_Arg(V(25))
	call_c   Dyam_Term_Arg(V(26))
	call_c   Dyam_Term_Arg(V(27))
	call_c   Dyam_Term_Arg(V(28))
	call_c   Dyam_Term_Arg(V(29))
	call_c   Dyam_Term_Arg(V(30))
	call_c   Dyam_Term_Arg(V(31))
	call_c   Dyam_Term_Arg(V(21))
	call_c   Dyam_Term_Arg(V(32))
	call_c   Dyam_Term_Arg(V(33))
	call_c   Dyam_Term_Arg(V(34))
	call_c   Dyam_Term_Arg(V(35))
	call_c   Dyam_Term_End()
	move_ret ref[477]
	c_ret

;; TERM 499: '$CLOSURE'('$fun'(161, 0, 1125729264), '$TUPPLE'(35144395137452))
c_code local build_ref_499
	ret_reg &ref[499]
	call_c   build_ref_498()
	call_c   Dyam_Closure_Aux(fun161,&ref[498])
	move_ret ref[499]
	c_ret

long local pool_fun161[3]=[65537,build_ref_486,pool_fun160]

pl_code local fun161
	call_c   Dyam_Pool(pool_fun161)
	call_c   Dyam_Unify(V(1),&ref[486])
	fail_ret
	call_c   Dyam_Allocate(0)
	pl_jump  fun160()

long local pool_fun162[5]=[4,build_ref_476,build_ref_477,build_ref_499,build_ref_451]

long local pool_fun163[4]=[65538,build_ref_500,build_ref_476,pool_fun162]

pl_code local fun163
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(16),&ref[500])
	fail_ret
	call_c   Dyam_Unify(V(23),&ref[476])
	fail_ret
fun162:
	move     &ref[476], R(0)
	move     S(5), R(1)
	move     &ref[477], R(2)
	move     S(5), R(3)
	pl_call  pred_tree_foot_2()
	move     &ref[499], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Load(4,V(8))
	move     &ref[451], R(6)
	move     0, R(7)
	call_c   Dyam_Reg_Load(8,V(13))
	call_c   Dyam_Reg_Load(10,V(21))
	call_c   Dyam_Reg_Deallocate(6)
	pl_jump  fun155()


;; TERM 475: '$VAR'(_W, ['$SET$',adj(no, yes, strict, atmostone, one, sync),43])
c_code local build_ref_475
	ret_reg &ref[475]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1178()
	call_c   build_ref_706()
	call_c   build_ref_319()
	call_c   build_ref_673()
	call_c   build_ref_685()
	call_c   build_ref_686()
	call_c   build_ref_623()
	call_c   Dyam_Term_Start(&ref[1178],6)
	call_c   Dyam_Term_Arg(&ref[706])
	call_c   Dyam_Term_Arg(&ref[319])
	call_c   Dyam_Term_Arg(&ref[673])
	call_c   Dyam_Term_Arg(&ref[685])
	call_c   Dyam_Term_Arg(&ref[686])
	call_c   Dyam_Term_Arg(&ref[623])
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   Dyam_Term_Start(I(8),3)
	call_c   Dyam_Term_Arg(V(22))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(N(43))
	call_c   Dyam_Term_End()
	move_ret ref[475]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun171[6]=[196610,build_ref_474,build_ref_475,pool_fun170,pool_fun163,pool_fun162]

pl_code local fun171
	call_c   Dyam_Pool(pool_fun171)
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun170)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[474])
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun163)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(13),V(14))
	fail_ret
	call_c   Dyam_Unify(V(11),&ref[475])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(23),V(9))
	fail_ret
	pl_jump  fun162()

;; TERM 515: '$TUPPLE'(35144395138456)
c_code local build_ref_515
	ret_reg &ref[515]
	call_c   Dyam_Create_Simple_Tupple(0,306183296)
	move_ret ref[515]
	c_ret

long local pool_fun172[2]=[1,build_ref_516]

long local pool_fun208[3]=[65537,build_seed_106,pool_fun172]

pl_code local fun208
	call_c   Dyam_Remove_Choice()
	pl_call  fun38(&seed[106],2)
fun172:
	move     &ref[516], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Load(4,V(8))
	call_c   Dyam_Reg_Load(6,V(13))
	move     V(21), R(8)
	move     S(5), R(9)
	call_c   Dyam_Reg_Deallocate(5)
	pl_jump  fun50()


c_code local build_seed_95
	ret_reg &seed[95]
	call_c   build_ref_133()
	call_c   build_ref_418()
	call_c   Dyam_Seed_Start(&ref[133],&ref[418],I(0),fun21,1)
	call_c   build_ref_416()
	call_c   Dyam_Seed_Add_Comp(&ref[416],fun118,0)
	call_c   Dyam_Seed_End()
	move_ret seed[95]
	c_ret

;; TERM 416: '*RITEM*'('call_normalize/2'(tag_tree{family=> _F, name=> _D, tree=> _G}), return(tag_node{id=> _H, label=> _I, children=> _J, kind=> _K, adj=> _L, spine=> _M, top=> _N, bot=> _O, token=> _P, adjleft=> _Q, adjright=> _R, adjwrap=> _S}))
c_code local build_ref_416
	ret_reg &ref[416]
	call_c   build_ref_0()
	call_c   build_ref_414()
	call_c   build_ref_415()
	call_c   Dyam_Create_Binary(&ref[0],&ref[414],&ref[415])
	move_ret ref[416]
	c_ret

;; TERM 415: return(tag_node{id=> _H, label=> _I, children=> _J, kind=> _K, adj=> _L, spine=> _M, top=> _N, bot=> _O, token=> _P, adjleft=> _Q, adjright=> _R, adjwrap=> _S})
c_code local build_ref_415
	ret_reg &ref[415]
	call_c   build_ref_1139()
	call_c   build_ref_476()
	call_c   Dyam_Create_Unary(&ref[1139],&ref[476])
	move_ret ref[415]
	c_ret

pl_code local fun118
	call_c   build_ref_416()
	call_c   Dyam_Unify_Item(&ref[416])
	fail_ret
	pl_ret

;; TERM 418: '*RITEM*'('call_normalize/2'(tag_tree{family=> _F, name=> _D, tree=> _G}), return(tag_node{id=> _H, label=> _I, children=> _J, kind=> _K, adj=> _L, spine=> _M, top=> _N, bot=> _O, token=> _P, adjleft=> _Q, adjright=> _R, adjwrap=> _S})) :> tig(5, '$TUPPLE'(35144395401924))
c_code local build_ref_418
	ret_reg &ref[418]
	call_c   build_ref_416()
	call_c   build_ref_417()
	call_c   Dyam_Create_Binary(I(9),&ref[416],&ref[417])
	move_ret ref[418]
	c_ret

;; TERM 417: tig(5, '$TUPPLE'(35144395401924))
c_code local build_ref_417
	ret_reg &ref[417]
	call_c   build_ref_1154()
	call_c   build_ref_719()
	call_c   Dyam_Create_Binary(&ref[1154],N(5),&ref[719])
	move_ret ref[417]
	c_ret

long local pool_fun209[5]=[131074,build_ref_413,build_seed_95,pool_fun208,pool_fun172]

pl_code local fun209
	call_c   Dyam_Pool(pool_fun209)
	call_c   Dyam_Unify_Item(&ref[413])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun208)
	pl_call  fun38(&seed[95],2)
	pl_jump  fun172()

;; TERM 412: '*FIRST*'('call_lctag_closure/4') :> []
c_code local build_ref_412
	ret_reg &ref[412]
	call_c   build_ref_131()
	call_c   Dyam_Create_Binary(I(9),&ref[131],I(0))
	move_ret ref[412]
	c_ret

;; TERM 131: '*FIRST*'('call_lctag_closure/4')
c_code local build_ref_131
	ret_reg &ref[131]
	call_c   build_ref_93()
	call_c   build_ref_1()
	call_c   Dyam_Create_Unary(&ref[93],&ref[1])
	move_ret ref[131]
	c_ret

c_code local build_seed_8
	ret_reg &seed[8]
	call_c   build_ref_54()
	call_c   build_ref_427()
	call_c   Dyam_Seed_Start(&ref[54],&ref[427],I(0),fun0,1)
	call_c   build_ref_426()
	call_c   Dyam_Seed_Add_Comp(&ref[426],fun122,0)
	call_c   Dyam_Seed_End()
	move_ret seed[8]
	c_ret

;; TERM 426: '*GUARD*'(sorted_tree_add(_B, [_C|_D], _E))
c_code local build_ref_426
	ret_reg &ref[426]
	call_c   build_ref_55()
	call_c   build_ref_425()
	call_c   Dyam_Create_Unary(&ref[55],&ref[425])
	move_ret ref[426]
	c_ret

;; TERM 425: sorted_tree_add(_B, [_C|_D], _E)
c_code local build_ref_425
	ret_reg &ref[425]
	call_c   build_ref_1129()
	call_c   build_ref_428()
	call_c   Dyam_Term_Start(&ref[1129],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(&ref[428])
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[425]
	c_ret

c_code local build_seed_97
	ret_reg &seed[97]
	call_c   build_ref_55()
	call_c   build_ref_432()
	call_c   Dyam_Seed_Start(&ref[55],&ref[432],I(0),fun11,1)
	call_c   build_ref_433()
	call_c   Dyam_Seed_Add_Comp(&ref[433],&ref[432],0)
	call_c   Dyam_Seed_End()
	move_ret seed[97]
	c_ret

;; TERM 433: '*GUARD*'(sorted_tree_add(_B, _D, _F)) :> '$$HOLE$$'
c_code local build_ref_433
	ret_reg &ref[433]
	call_c   build_ref_432()
	call_c   Dyam_Create_Binary(I(9),&ref[432],I(7))
	move_ret ref[433]
	c_ret

;; TERM 432: '*GUARD*'(sorted_tree_add(_B, _D, _F))
c_code local build_ref_432
	ret_reg &ref[432]
	call_c   build_ref_55()
	call_c   build_ref_431()
	call_c   Dyam_Create_Unary(&ref[55],&ref[431])
	move_ret ref[432]
	c_ret

;; TERM 431: sorted_tree_add(_B, _D, _F)
c_code local build_ref_431
	ret_reg &ref[431]
	call_c   build_ref_1129()
	call_c   Dyam_Term_Start(&ref[1129],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[431]
	c_ret

;; TERM 434: [_C|_F]
c_code local build_ref_434
	ret_reg &ref[434]
	call_c   Dyam_Create_List(V(2),V(5))
	move_ret ref[434]
	c_ret

long local pool_fun120[3]=[2,build_seed_97,build_ref_434]

pl_code local fun120
	call_c   Dyam_Remove_Choice()
	pl_call  fun38(&seed[97],1)
	call_c   Dyam_Unify(V(4),&ref[434])
	fail_ret
	pl_jump  fun85()

;; TERM 429: <
c_code local build_ref_429
	ret_reg &ref[429]
	call_c   Dyam_Create_Atom("<")
	move_ret ref[429]
	c_ret

;; TERM 430: [_B,_C|_D]
c_code local build_ref_430
	ret_reg &ref[430]
	call_c   Dyam_Create_Tupple(1,2,V(3))
	move_ret ref[430]
	c_ret

long local pool_fun121[4]=[65538,build_ref_429,build_ref_430,pool_fun120]

pl_code local fun121
	call_c   Dyam_Update_Choice(fun120)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_Compare_3(&ref[429],V(1),V(2))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(4),&ref[430])
	fail_ret
	pl_jump  fun85()

long local pool_fun122[4]=[65538,build_ref_426,build_ref_428,pool_fun121]

pl_code local fun122
	call_c   Dyam_Pool(pool_fun122)
	call_c   Dyam_Unify_Item(&ref[426])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun121)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(1),V(2))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(4),&ref[428])
	fail_ret
	pl_jump  fun85()

;; TERM 427: '*GUARD*'(sorted_tree_add(_B, [_C|_D], _E)) :> []
c_code local build_ref_427
	ret_reg &ref[427]
	call_c   build_ref_426()
	call_c   Dyam_Create_Binary(I(9),&ref[426],I(0))
	move_ret ref[427]
	c_ret

c_code local build_seed_55
	ret_reg &seed[55]
	call_c   build_ref_63()
	call_c   build_ref_212()
	call_c   Dyam_Seed_Start(&ref[63],&ref[212],I(0),fun0,1)
	call_c   build_ref_213()
	call_c   Dyam_Seed_Add_Comp(&ref[213],fun303,0)
	call_c   Dyam_Seed_End()
	move_ret seed[55]
	c_ret

c_code local build_seed_107
	ret_reg &seed[107]
	call_c   build_ref_521()
	call_c   build_ref_216()
	call_c   Dyam_Seed_Start(&ref[521],&ref[216],I(0),fun299,0)
	call_c   Dyam_Seed_End()
	move_ret seed[107]
	c_ret

c_code local build_seed_108
	ret_reg &seed[108]
	call_c   build_ref_521()
	call_c   build_ref_522()
	call_c   Dyam_Seed_Start(&ref[521],&ref[522],I(0),fun174,0)
	call_c   Dyam_Seed_End()
	move_ret seed[108]
	c_ret

pl_code local fun174
	call_c   DYAM_Nl_0()
	fail_ret
	call_c   DYAM_Set_Output_1(V(1))
	fail_ret
	pl_jump  Follow_Cont(V(0))

;; TERM 522: cont('$TUPPLE'(35144395138564))
c_code local build_ref_522
	ret_reg &ref[522]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,402653184)
	move_ret R(0)
	call_c   build_ref_1208()
	call_c   Dyam_Create_Unary(&ref[1208],R(0))
	move_ret ref[522]
	call_c   Dyam_Set_Not_Copyable(&ref[522])
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1208: cont
c_code local build_ref_1208
	ret_reg &ref[1208]
	call_c   Dyam_Create_Atom("cont")
	move_ret ref[1208]
	c_ret

;; TERM 521: '*WAIT_CONT*'
c_code local build_ref_521
	ret_reg &ref[521]
	call_c   Dyam_Create_Atom("*WAIT_CONT*")
	move_ret ref[521]
	c_ret

pl_code local fun175
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Object(R(0))
	call_c   Dyam_Schedule_Last()
	pl_ret

c_code local build_seed_112
	ret_reg &seed[112]
	call_c   build_ref_133()
	call_c   build_ref_549()
	call_c   Dyam_Seed_Start(&ref[133],&ref[549],I(0),fun21,1)
	call_c   build_ref_547()
	call_c   Dyam_Seed_Add_Comp(&ref[547],fun199,0)
	call_c   Dyam_Seed_End()
	move_ret seed[112]
	c_ret

;; TERM 547: '*RITEM*'('call_normalize/2'(tag_tree{family=> _Y, name=> _K, tree=> _Z}), return(guard{goal=> tag_node{id=> _A1, label=> _N, children=> _B1, kind=> _C1, adj=> _D1, spine=> _E1, top=> _F1, bot=> _G1, token=> _H1, adjleft=> _I1, adjright=> _J1, adjwrap=> _K1}, plus=> _L1, minus=> _M1}))
c_code local build_ref_547
	ret_reg &ref[547]
	call_c   build_ref_0()
	call_c   build_ref_545()
	call_c   build_ref_546()
	call_c   Dyam_Create_Binary(&ref[0],&ref[545],&ref[546])
	move_ret ref[547]
	c_ret

;; TERM 546: return(guard{goal=> tag_node{id=> _A1, label=> _N, children=> _B1, kind=> _C1, adj=> _D1, spine=> _E1, top=> _F1, bot=> _G1, token=> _H1, adjleft=> _I1, adjright=> _J1, adjwrap=> _K1}, plus=> _L1, minus=> _M1})
c_code local build_ref_546
	ret_reg &ref[546]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1172()
	call_c   Dyam_Term_Start(&ref[1172],12)
	call_c   Dyam_Term_Arg(V(26))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(27))
	call_c   Dyam_Term_Arg(V(28))
	call_c   Dyam_Term_Arg(V(29))
	call_c   Dyam_Term_Arg(V(30))
	call_c   Dyam_Term_Arg(V(31))
	call_c   Dyam_Term_Arg(V(32))
	call_c   Dyam_Term_Arg(V(33))
	call_c   Dyam_Term_Arg(V(34))
	call_c   Dyam_Term_Arg(V(35))
	call_c   Dyam_Term_Arg(V(36))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_1198()
	call_c   Dyam_Term_Start(&ref[1198],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(37))
	call_c   Dyam_Term_Arg(V(38))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_1139()
	call_c   Dyam_Create_Unary(&ref[1139],R(0))
	move_ret ref[546]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 545: 'call_normalize/2'(tag_tree{family=> _Y, name=> _K, tree=> _Z})
c_code local build_ref_545
	ret_reg &ref[545]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1142()
	call_c   Dyam_Term_Start(&ref[1142],3)
	call_c   Dyam_Term_Arg(V(24))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(25))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_1185()
	call_c   Dyam_Create_Unary(&ref[1185],R(0))
	move_ret ref[545]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

pl_code local fun199
	call_c   build_ref_547()
	call_c   Dyam_Unify_Item(&ref[547])
	fail_ret
	pl_ret

;; TERM 549: '*RITEM*'('call_normalize/2'(tag_tree{family=> _Y, name=> _K, tree=> _Z}), return(guard{goal=> tag_node{id=> _A1, label=> _N, children=> _B1, kind=> _C1, adj=> _D1, spine=> _E1, top=> _F1, bot=> _G1, token=> _H1, adjleft=> _I1, adjright=> _J1, adjwrap=> _K1}, plus=> _L1, minus=> _M1})) :> tig(13, '$TUPPLE'(35144395401924))
c_code local build_ref_549
	ret_reg &ref[549]
	call_c   build_ref_547()
	call_c   build_ref_548()
	call_c   Dyam_Create_Binary(I(9),&ref[547],&ref[548])
	move_ret ref[549]
	c_ret

;; TERM 548: tig(13, '$TUPPLE'(35144395401924))
c_code local build_ref_548
	ret_reg &ref[548]
	call_c   build_ref_1154()
	call_c   build_ref_719()
	call_c   Dyam_Create_Binary(&ref[1154],N(13),&ref[719])
	move_ret ref[548]
	c_ret

;; TERM 543: '$CLOSURE'('$fun'(197, 0, 1125940356), '$TUPPLE'(35144395139444))
c_code local build_ref_543
	ret_reg &ref[543]
	call_c   build_ref_542()
	call_c   Dyam_Closure_Aux(fun197,&ref[542])
	move_ret ref[543]
	c_ret

;; TERM 332: [_S1|_T1]
c_code local build_ref_332
	ret_reg &ref[332]
	call_c   Dyam_Create_List(V(44),V(45))
	move_ret ref[332]
	c_ret

;; TERM 540: ':-lctag(~k,pos,~k,~k).\n'
c_code local build_ref_540
	ret_reg &ref[540]
	call_c   Dyam_Create_Atom(":-lctag(~k,pos,~k,~k).\n")
	move_ret ref[540]
	c_ret

;; TERM 536: [_K,_S1,_U1]
c_code local build_ref_536
	ret_reg &ref[536]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(46),I(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(44),R(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(10),R(0))
	move_ret ref[536]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun179[3]=[2,build_ref_540,build_ref_536]

pl_code local fun180
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(46),V(45))
	fail_ret
fun179:
	move     &ref[540], R(0)
	move     0, R(1)
	move     &ref[536], R(2)
	move     S(5), R(3)
	pl_call  pred_format_2()
	pl_fail


long local pool_fun181[4]=[131073,build_ref_332,pool_fun179,pool_fun179]

pl_code local fun184
	call_c   Dyam_Remove_Choice()
fun181:
	call_c   Dyam_Choice(fun114)
	pl_call  Domain_2(&ref[332],V(43))
	call_c   Dyam_Choice(fun180)
	call_c   Dyam_Set_Cut()
	pl_call  Domain_2(V(10),V(45))
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(46),I(0))
	fail_ret
	pl_jump  fun179()


;; TERM 539: ':-lctag(~k,postcat,~k,~k).\n'
c_code local build_ref_539
	ret_reg &ref[539]
	call_c   Dyam_Create_Atom(":-lctag(~k,postcat,~k,~k).\n")
	move_ret ref[539]
	c_ret

long local pool_fun182[3]=[2,build_ref_539,build_ref_536]

pl_code local fun183
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(46),V(45))
	fail_ret
fun182:
	move     &ref[539], R(0)
	move     0, R(1)
	move     &ref[536], R(2)
	move     S(5), R(3)
	pl_call  pred_format_2()
	pl_fail


long local pool_fun185[5]=[196609,build_ref_332,pool_fun181,pool_fun182,pool_fun182]

pl_code local fun188
	call_c   Dyam_Remove_Choice()
fun185:
	call_c   Dyam_Choice(fun184)
	pl_call  Domain_2(&ref[332],V(42))
	call_c   Dyam_Choice(fun183)
	call_c   Dyam_Set_Cut()
	pl_call  Domain_2(V(10),V(45))
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(46),I(0))
	fail_ret
	pl_jump  fun182()


;; TERM 538: ':-lctag(~k,postscan,~k,~k).\n'
c_code local build_ref_538
	ret_reg &ref[538]
	call_c   Dyam_Create_Atom(":-lctag(~k,postscan,~k,~k).\n")
	move_ret ref[538]
	c_ret

long local pool_fun186[3]=[2,build_ref_538,build_ref_536]

pl_code local fun187
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(46),V(45))
	fail_ret
fun186:
	move     &ref[538], R(0)
	move     0, R(1)
	move     &ref[536], R(2)
	move     S(5), R(3)
	pl_call  pred_format_2()
	pl_fail


long local pool_fun189[5]=[196609,build_ref_332,pool_fun185,pool_fun186,pool_fun186]

pl_code local fun192
	call_c   Dyam_Remove_Choice()
fun189:
	call_c   Dyam_Choice(fun188)
	pl_call  Domain_2(&ref[332],V(41))
	call_c   Dyam_Choice(fun187)
	call_c   Dyam_Set_Cut()
	pl_call  Domain_2(V(10),V(45))
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(46),I(0))
	fail_ret
	pl_jump  fun186()


;; TERM 537: ':-lctag(~k,cat,~k,~k).\n'
c_code local build_ref_537
	ret_reg &ref[537]
	call_c   Dyam_Create_Atom(":-lctag(~k,cat,~k,~k).\n")
	move_ret ref[537]
	c_ret

long local pool_fun190[3]=[2,build_ref_537,build_ref_536]

pl_code local fun191
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(46),V(45))
	fail_ret
fun190:
	move     &ref[537], R(0)
	move     0, R(1)
	move     &ref[536], R(2)
	move     S(5), R(3)
	pl_call  pred_format_2()
	pl_fail


long local pool_fun193[5]=[196609,build_ref_332,pool_fun189,pool_fun190,pool_fun190]

pl_code local fun196
	call_c   Dyam_Remove_Choice()
fun193:
	call_c   Dyam_Choice(fun192)
	pl_call  Domain_2(&ref[332],V(40))
	call_c   Dyam_Choice(fun191)
	call_c   Dyam_Set_Cut()
	pl_call  Domain_2(V(10),V(45))
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(46),I(0))
	fail_ret
	pl_jump  fun190()


;; TERM 535: ':-lctag(~k,scan,~k,~k).\n'
c_code local build_ref_535
	ret_reg &ref[535]
	call_c   Dyam_Create_Atom(":-lctag(~k,scan,~k,~k).\n")
	move_ret ref[535]
	c_ret

long local pool_fun194[3]=[2,build_ref_535,build_ref_536]

pl_code local fun195
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(46),V(45))
	fail_ret
fun194:
	move     &ref[535], R(0)
	move     0, R(1)
	move     &ref[536], R(2)
	move     S(5), R(3)
	pl_call  pred_format_2()
	pl_fail


long local pool_fun197[5]=[196609,build_ref_332,pool_fun193,pool_fun194,pool_fun194]

pl_code local fun197
	call_c   Dyam_Pool(pool_fun197)
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun196)
	pl_call  Domain_2(&ref[332],V(39))
	call_c   Dyam_Choice(fun195)
	call_c   Dyam_Set_Cut()
	pl_call  Domain_2(V(10),V(45))
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(46),I(0))
	fail_ret
	pl_jump  fun194()

;; TERM 542: '$TUPPLE'(35144395139444)
c_code local build_ref_542
	ret_reg &ref[542]
	call_c   Dyam_Start_Tupple(0,268697600)
	call_c   Dyam_Almost_End_Tupple(29,507904)
	move_ret ref[542]
	c_ret

;; TERM 544: lctag_filter(_K, _N1, _O1, _P1, _Q1, _R1)
c_code local build_ref_544
	ret_reg &ref[544]
	call_c   build_ref_1209()
	call_c   Dyam_Term_Start(&ref[1209],6)
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(39))
	call_c   Dyam_Term_Arg(V(40))
	call_c   Dyam_Term_Arg(V(41))
	call_c   Dyam_Term_Arg(V(42))
	call_c   Dyam_Term_Arg(V(43))
	call_c   Dyam_Term_End()
	move_ret ref[544]
	c_ret

;; TERM 1209: lctag_filter
c_code local build_ref_1209
	ret_reg &ref[1209]
	call_c   Dyam_Create_Atom("lctag_filter")
	move_ret ref[1209]
	c_ret

long local pool_fun198[3]=[2,build_ref_543,build_ref_544]

long local pool_fun210[3]=[65537,build_seed_112,pool_fun198]

pl_code local fun210
	call_c   Dyam_Remove_Choice()
	pl_call  fun38(&seed[112],2)
fun198:
	move     &ref[543], R(0)
	move     S(5), R(1)
	move     &ref[544], R(2)
	move     S(5), R(3)
	move     I(0), R(4)
	move     0, R(5)
	call_c   Dyam_Reg_Deallocate(3)
fun178:
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Reg_Unify(2,&ref[528])
	fail_ret
	call_c   Dyam_Reg_Bind(4,V(8))
	pl_call  fun25(&seed[110],1)
	pl_call  fun55(&seed[111],2,V(8))
	call_c   Dyam_Deallocate()
	pl_ret



c_code local build_seed_109
	ret_reg &seed[109]
	call_c   build_ref_133()
	call_c   build_ref_527()
	call_c   Dyam_Seed_Start(&ref[133],&ref[527],I(0),fun21,1)
	call_c   build_ref_525()
	call_c   Dyam_Seed_Add_Comp(&ref[525],fun176,0)
	call_c   Dyam_Seed_End()
	move_ret seed[109]
	c_ret

;; TERM 525: '*RITEM*'('call_normalize/2'(tag_tree{family=> _J, name=> _K, tree=> _L}), return(tag_node{id=> _M, label=> _N, children=> _O, kind=> _P, adj=> _Q, spine=> _R, top=> _S, bot=> _T, token=> _U, adjleft=> _V, adjright=> _W, adjwrap=> _X}))
c_code local build_ref_525
	ret_reg &ref[525]
	call_c   build_ref_0()
	call_c   build_ref_523()
	call_c   build_ref_524()
	call_c   Dyam_Create_Binary(&ref[0],&ref[523],&ref[524])
	move_ret ref[525]
	c_ret

;; TERM 524: return(tag_node{id=> _M, label=> _N, children=> _O, kind=> _P, adj=> _Q, spine=> _R, top=> _S, bot=> _T, token=> _U, adjleft=> _V, adjright=> _W, adjwrap=> _X})
c_code local build_ref_524
	ret_reg &ref[524]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1172()
	call_c   Dyam_Term_Start(&ref[1172],12)
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_Arg(V(21))
	call_c   Dyam_Term_Arg(V(22))
	call_c   Dyam_Term_Arg(V(23))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_1139()
	call_c   Dyam_Create_Unary(&ref[1139],R(0))
	move_ret ref[524]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 523: 'call_normalize/2'(tag_tree{family=> _J, name=> _K, tree=> _L})
c_code local build_ref_523
	ret_reg &ref[523]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1142()
	call_c   Dyam_Term_Start(&ref[1142],3)
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_1185()
	call_c   Dyam_Create_Unary(&ref[1185],R(0))
	move_ret ref[523]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

pl_code local fun176
	call_c   build_ref_525()
	call_c   Dyam_Unify_Item(&ref[525])
	fail_ret
	pl_ret

;; TERM 527: '*RITEM*'('call_normalize/2'(tag_tree{family=> _J, name=> _K, tree=> _L}), return(tag_node{id=> _M, label=> _N, children=> _O, kind=> _P, adj=> _Q, spine=> _R, top=> _S, bot=> _T, token=> _U, adjleft=> _V, adjright=> _W, adjwrap=> _X})) :> tig(11, '$TUPPLE'(35144395401924))
c_code local build_ref_527
	ret_reg &ref[527]
	call_c   build_ref_525()
	call_c   build_ref_526()
	call_c   Dyam_Create_Binary(I(9),&ref[525],&ref[526])
	move_ret ref[527]
	c_ret

;; TERM 526: tig(11, '$TUPPLE'(35144395401924))
c_code local build_ref_526
	ret_reg &ref[526]
	call_c   build_ref_1154()
	call_c   build_ref_719()
	call_c   Dyam_Create_Binary(&ref[1154],N(11),&ref[719])
	move_ret ref[526]
	c_ret

long local pool_fun299[5]=[131074,build_seed_108,build_seed_109,pool_fun210,pool_fun198]

pl_code local fun299
	call_c   Dyam_Pool(pool_fun299)
	call_c   Dyam_Allocate(0)
	pl_call  fun175(&seed[108],12)
	call_c   Dyam_Choice(fun210)
	pl_call  fun38(&seed[109],2)
	pl_jump  fun198()

;; TERM 216: cont('$TUPPLE'(35144400633912))
c_code local build_ref_216
	ret_reg &ref[216]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Start_Tupple(0,403701759)
	call_c   Dyam_Almost_End_Tupple(29,536868864)
	move_ret R(0)
	call_c   build_ref_1208()
	call_c   Dyam_Create_Unary(&ref[1208],R(0))
	move_ret ref[216]
	call_c   Dyam_Set_Not_Copyable(&ref[216])
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 720: '$CLOSURE'('$fun'(300, 0, 1126870100), '$TUPPLE'(35144395401924))
c_code local build_ref_720
	ret_reg &ref[720]
	call_c   build_ref_719()
	call_c   Dyam_Closure_Aux(fun300,&ref[719])
	move_ret ref[720]
	c_ret

pl_code local fun300
	pl_fail

long local pool_fun301[3]=[2,build_seed_107,build_ref_720]

pl_code local fun302
	call_c   Dyam_Remove_Choice()
fun301:
	pl_call  pred_lctag_emit_header_0()
	pl_call  fun175(&seed[107],12)
	move     &ref[720], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	move     0, R(4)
	move     0, R(6)
	move     0, R(8)
	move     0, R(10)
	call_c   Dyam_Reg_Deallocate(6)
	pl_jump  fun56()


long local pool_fun303[7]=[131076,build_ref_213,build_ref_142,build_ref_214,build_ref_215,pool_fun301,pool_fun301]

pl_code local fun303
	call_c   Dyam_Pool(pool_fun303)
	call_c   Dyam_Unify_Item(&ref[213])
	fail_ret
	call_c   Dyam_Allocate(0)
	pl_call  Object_1(&ref[142])
	call_c   DYAM_Current_Output_1(V(1))
	fail_ret
	call_c   Dyam_Choice(fun302)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[214])
	call_c   Dyam_Cut()
	call_c   DYAM_Absolute_File_Name(V(2),V(3))
	fail_ret
	call_c   DYAM_Open_3(V(3),&ref[215],V(4))
	fail_ret
	call_c   DYAM_Set_Output_1(V(4))
	fail_ret
	pl_jump  fun301()

c_code local build_seed_59
	ret_reg &seed[59]
	call_c   build_ref_93()
	call_c   build_ref_559()
	call_c   Dyam_Seed_Start(&ref[93],&ref[559],I(0),fun21,1)
	call_c   build_ref_560()
	call_c   Dyam_Seed_Add_Comp(&ref[560],fun207,0)
	call_c   Dyam_Seed_End()
	move_ret seed[59]
	c_ret

;; TERM 560: '*CITEM*'(node_auxtree(_B, _C, tag_node{id=> _D, label=> _B, children=> _E, kind=> _F, adj=> _G, spine=> yes, top=> _H, bot=> _I, token=> _J, adjleft=> _K, adjright=> _L, adjwrap=> _M}), _A)
c_code local build_ref_560
	ret_reg &ref[560]
	call_c   build_ref_97()
	call_c   build_ref_557()
	call_c   Dyam_Create_Binary(&ref[97],&ref[557],V(0))
	move_ret ref[560]
	c_ret

;; TERM 557: node_auxtree(_B, _C, tag_node{id=> _D, label=> _B, children=> _E, kind=> _F, adj=> _G, spine=> yes, top=> _H, bot=> _I, token=> _J, adjleft=> _K, adjright=> _L, adjwrap=> _M})
c_code local build_ref_557
	ret_reg &ref[557]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1172()
	call_c   build_ref_319()
	call_c   Dyam_Term_Start(&ref[1172],12)
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(&ref[319])
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_1210()
	call_c   Dyam_Term_Start(&ref[1210],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_End()
	move_ret ref[557]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1210: node_auxtree
c_code local build_ref_1210
	ret_reg &ref[1210]
	call_c   Dyam_Create_Atom("node_auxtree")
	move_ret ref[1210]
	c_ret

c_code local build_seed_114
	ret_reg &seed[114]
	call_c   build_ref_133()
	call_c   build_ref_565()
	call_c   Dyam_Seed_Start(&ref[133],&ref[565],I(0),fun21,1)
	call_c   build_ref_563()
	call_c   Dyam_Seed_Add_Comp(&ref[563],fun205,0)
	call_c   Dyam_Seed_End()
	move_ret seed[114]
	c_ret

;; TERM 563: '*RITEM*'('call_normalize/2'(_N), return(tag_node{id=> _D, label=> _B, children=> _E, kind=> _F, adj=> _G, spine=> yes, top=> _H, bot=> _I, token=> _J, adjleft=> _K, adjright=> _L, adjwrap=> _M}))
c_code local build_ref_563
	ret_reg &ref[563]
	call_c   build_ref_0()
	call_c   build_ref_561()
	call_c   build_ref_562()
	call_c   Dyam_Create_Binary(&ref[0],&ref[561],&ref[562])
	move_ret ref[563]
	c_ret

;; TERM 562: return(tag_node{id=> _D, label=> _B, children=> _E, kind=> _F, adj=> _G, spine=> yes, top=> _H, bot=> _I, token=> _J, adjleft=> _K, adjright=> _L, adjwrap=> _M})
c_code local build_ref_562
	ret_reg &ref[562]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1172()
	call_c   build_ref_319()
	call_c   Dyam_Term_Start(&ref[1172],12)
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(&ref[319])
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_1139()
	call_c   Dyam_Create_Unary(&ref[1139],R(0))
	move_ret ref[562]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 561: 'call_normalize/2'(_N)
c_code local build_ref_561
	ret_reg &ref[561]
	call_c   build_ref_1185()
	call_c   Dyam_Create_Unary(&ref[1185],V(13))
	move_ret ref[561]
	c_ret

pl_code local fun205
	call_c   build_ref_563()
	call_c   Dyam_Unify_Item(&ref[563])
	fail_ret
	pl_ret

;; TERM 565: '*RITEM*'('call_normalize/2'(_N), return(tag_node{id=> _D, label=> _B, children=> _E, kind=> _F, adj=> _G, spine=> yes, top=> _H, bot=> _I, token=> _J, adjleft=> _K, adjright=> _L, adjwrap=> _M})) :> tig(15, '$TUPPLE'(35144395401924))
c_code local build_ref_565
	ret_reg &ref[565]
	call_c   build_ref_563()
	call_c   build_ref_564()
	call_c   Dyam_Create_Binary(I(9),&ref[563],&ref[564])
	move_ret ref[565]
	c_ret

;; TERM 564: tig(15, '$TUPPLE'(35144395401924))
c_code local build_ref_564
	ret_reg &ref[564]
	call_c   build_ref_1154()
	call_c   build_ref_719()
	call_c   Dyam_Create_Binary(&ref[1154],N(15),&ref[719])
	move_ret ref[564]
	c_ret

long local pool_fun206[3]=[65537,build_seed_114,pool_fun31]

pl_code local fun206
	call_c   Dyam_Remove_Choice()
	pl_call  fun38(&seed[114],2)
	pl_jump  fun31()

long local pool_fun207[3]=[65537,build_ref_560,pool_fun206]

pl_code local fun207
	call_c   Dyam_Pool(pool_fun207)
	call_c   Dyam_Unify_Item(&ref[560])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun206)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Choice(fun112)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(2),V(7))
	fail_ret
	call_c   Dyam_Cut()
	pl_fail

;; TERM 559: '*FIRST*'(node_auxtree(_B, _C, tag_node{id=> _D, label=> _B, children=> _E, kind=> _F, adj=> _G, spine=> yes, top=> _H, bot=> _I, token=> _J, adjleft=> _K, adjright=> _L, adjwrap=> _M})) :> []
c_code local build_ref_559
	ret_reg &ref[559]
	call_c   build_ref_558()
	call_c   Dyam_Create_Binary(I(9),&ref[558],I(0))
	move_ret ref[559]
	c_ret

;; TERM 558: '*FIRST*'(node_auxtree(_B, _C, tag_node{id=> _D, label=> _B, children=> _E, kind=> _F, adj=> _G, spine=> yes, top=> _H, bot=> _I, token=> _J, adjleft=> _K, adjright=> _L, adjwrap=> _M}))
c_code local build_ref_558
	ret_reg &ref[558]
	call_c   build_ref_93()
	call_c   build_ref_557()
	call_c   Dyam_Create_Unary(&ref[93],&ref[557])
	move_ret ref[558]
	c_ret

c_code local build_seed_11
	ret_reg &seed[11]
	call_c   build_ref_63()
	call_c   build_ref_568()
	call_c   Dyam_Seed_Start(&ref[63],&ref[568],I(0),fun0,1)
	call_c   build_ref_569()
	call_c   Dyam_Seed_Add_Comp(&ref[569],fun218,0)
	call_c   Dyam_Seed_End()
	move_ret seed[11]
	c_ret

;; TERM 569: '*PROLOG-ITEM*'{top=> lctag_first(_B, [_C|_D], _E, _F), cont=> _A}
c_code local build_ref_569
	ret_reg &ref[569]
	call_c   build_ref_1126()
	call_c   build_ref_566()
	call_c   Dyam_Create_Binary(&ref[1126],&ref[566],V(0))
	move_ret ref[569]
	c_ret

;; TERM 566: lctag_first(_B, [_C|_D], _E, _F)
c_code local build_ref_566
	ret_reg &ref[566]
	call_c   build_ref_1127()
	call_c   build_ref_428()
	call_c   Dyam_Term_Start(&ref[1127],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(&ref[428])
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[566]
	c_ret

c_code local build_seed_116
	ret_reg &seed[116]
	call_c   build_ref_176()
	call_c   build_ref_577()
	call_c   Dyam_Seed_Start(&ref[176],&ref[577],I(0),fun11,1)
	call_c   build_ref_580()
	call_c   Dyam_Seed_Add_Comp(&ref[580],&ref[577],0)
	call_c   Dyam_Seed_End()
	move_ret seed[116]
	c_ret

;; TERM 580: '*PROLOG-FIRST*'(lctag_first(_B, _C, _G, _H)) :> '$$HOLE$$'
c_code local build_ref_580
	ret_reg &ref[580]
	call_c   build_ref_579()
	call_c   Dyam_Create_Binary(I(9),&ref[579],I(7))
	move_ret ref[580]
	c_ret

;; TERM 579: '*PROLOG-FIRST*'(lctag_first(_B, _C, _G, _H))
c_code local build_ref_579
	ret_reg &ref[579]
	call_c   build_ref_64()
	call_c   build_ref_578()
	call_c   Dyam_Create_Unary(&ref[64],&ref[578])
	move_ret ref[579]
	c_ret

;; TERM 578: lctag_first(_B, _C, _G, _H)
c_code local build_ref_578
	ret_reg &ref[578]
	call_c   build_ref_1127()
	call_c   Dyam_Term_Start(&ref[1127],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[578]
	c_ret

;; TERM 577: '*PROLOG-ITEM*'{top=> lctag_first(_B, _C, _G, _H), cont=> '$CLOSURE'('$fun'(217, 0, 1126084992), '$TUPPLE'(35144395140360))}
c_code local build_ref_577
	ret_reg &ref[577]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,467664896)
	move_ret R(0)
	call_c   Dyam_Closure_Aux(fun217,R(0))
	move_ret R(0)
	call_c   build_ref_1126()
	call_c   build_ref_578()
	call_c   Dyam_Create_Binary(&ref[1126],&ref[578],R(0))
	move_ret ref[577]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

pl_code local fun215
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(4),V(6))
	fail_ret
	call_c   Dyam_Unify(V(5),V(7))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_jump  Follow_Cont(V(0))

;; TERM 570: [pos(_I),postcat(_J, _K),postscan(_L),_M + epsilon]
c_code local build_ref_570
	ret_reg &ref[570]
	call_c   Dyam_Pseudo_Choice(4)
	call_c   build_ref_1211()
	call_c   Dyam_Create_Unary(&ref[1211],V(8))
	move_ret R(0)
	call_c   build_ref_1212()
	call_c   Dyam_Create_Binary(&ref[1212],V(9),V(10))
	move_ret R(1)
	call_c   build_ref_1213()
	call_c   Dyam_Create_Unary(&ref[1213],V(11))
	move_ret R(2)
	call_c   build_ref_254()
	call_c   build_ref_305()
	call_c   Dyam_Create_Binary(&ref[254],V(12),&ref[305])
	move_ret R(3)
	call_c   Dyam_Create_List(R(3),I(0))
	move_ret R(3)
	call_c   Dyam_Create_List(R(2),R(3))
	move_ret R(3)
	call_c   Dyam_Create_List(R(1),R(3))
	move_ret R(3)
	call_c   Dyam_Create_List(R(0),R(3))
	move_ret ref[570]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1213: postscan
c_code local build_ref_1213
	ret_reg &ref[1213]
	call_c   Dyam_Create_Atom("postscan")
	move_ret ref[1213]
	c_ret

;; TERM 1212: postcat
c_code local build_ref_1212
	ret_reg &ref[1212]
	call_c   Dyam_Create_Atom("postcat")
	move_ret ref[1212]
	c_ret

;; TERM 1211: pos
c_code local build_ref_1211
	ret_reg &ref[1211]
	call_c   Dyam_Create_Atom("pos")
	move_ret ref[1211]
	c_ret

c_code local build_seed_115
	ret_reg &seed[115]
	call_c   build_ref_176()
	call_c   build_ref_573()
	call_c   Dyam_Seed_Start(&ref[176],&ref[573],I(0),fun11,1)
	call_c   build_ref_576()
	call_c   Dyam_Seed_Add_Comp(&ref[576],&ref[573],0)
	call_c   Dyam_Seed_End()
	move_ret seed[115]
	c_ret

;; TERM 576: '*PROLOG-FIRST*'(lctag_first(_B, _D, _O, _P)) :> '$$HOLE$$'
c_code local build_ref_576
	ret_reg &ref[576]
	call_c   build_ref_575()
	call_c   Dyam_Create_Binary(I(9),&ref[575],I(7))
	move_ret ref[576]
	c_ret

;; TERM 575: '*PROLOG-FIRST*'(lctag_first(_B, _D, _O, _P))
c_code local build_ref_575
	ret_reg &ref[575]
	call_c   build_ref_64()
	call_c   build_ref_574()
	call_c   Dyam_Create_Unary(&ref[64],&ref[574])
	move_ret ref[575]
	c_ret

;; TERM 574: lctag_first(_B, _D, _O, _P)
c_code local build_ref_574
	ret_reg &ref[574]
	call_c   build_ref_1127()
	call_c   Dyam_Term_Start(&ref[1127],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_End()
	move_ret ref[574]
	c_ret

;; TERM 573: '*PROLOG-ITEM*'{top=> lctag_first(_B, _D, _O, _P), cont=> '$CLOSURE'('$fun'(212, 0, 1126070896), '$TUPPLE'(35144395140280))}
c_code local build_ref_573
	ret_reg &ref[573]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,285261824)
	move_ret R(0)
	call_c   Dyam_Closure_Aux(fun212,R(0))
	move_ret R(0)
	call_c   build_ref_1126()
	call_c   build_ref_574()
	call_c   Dyam_Create_Binary(&ref[1126],&ref[574],R(0))
	move_ret ref[573]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

pl_code local fun211
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(4),V(14))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_jump  Follow_Cont(V(0))

;; TERM 572: '$VAR'(_Q, ['$SET$',lctag_labels(epsilon, true, no_epsilon),3])
c_code local build_ref_572
	ret_reg &ref[572]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1206()
	call_c   build_ref_305()
	call_c   build_ref_487()
	call_c   build_ref_1207()
	call_c   Dyam_Term_Start(&ref[1206],3)
	call_c   Dyam_Term_Arg(&ref[305])
	call_c   Dyam_Term_Arg(&ref[487])
	call_c   Dyam_Term_Arg(&ref[1207])
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   Dyam_Term_Start(I(8),3)
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(N(3))
	call_c   Dyam_Term_End()
	move_ret ref[572]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 571: _N + epsilon
c_code local build_ref_571
	ret_reg &ref[571]
	call_c   build_ref_254()
	call_c   build_ref_305()
	call_c   Dyam_Create_Binary(&ref[254],V(13),&ref[305])
	move_ret ref[571]
	c_ret

long local pool_fun212[3]=[2,build_ref_572,build_ref_571]

pl_code local fun212
	call_c   Dyam_Pool(pool_fun212)
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun211)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(14),&ref[572])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(4),&ref[571])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_jump  Follow_Cont(V(0))

long local pool_fun213[2]=[1,build_seed_115]

pl_code local fun214
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(6),V(13))
	fail_ret
fun213:
	call_c   Dyam_Unify(V(5),V(7))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_jump  fun38(&seed[115],12)


long local pool_fun216[5]=[131074,build_ref_570,build_ref_571,pool_fun213,pool_fun213]

pl_code local fun216
	call_c   Dyam_Update_Choice(fun215)
	call_c   Dyam_Set_Cut()
	pl_call  Domain_2(V(6),&ref[570])
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun214)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(6),&ref[571])
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun213()

long local pool_fun217[4]=[65538,build_ref_305,build_seed_72,pool_fun216]

pl_code local fun217
	call_c   Dyam_Pool(pool_fun217)
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun216)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(6),&ref[305])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Deallocate()
	pl_jump  fun38(&seed[72],12)

long local pool_fun218[3]=[2,build_ref_569,build_seed_116]

pl_code local fun218
	call_c   Dyam_Pool(pool_fun218)
	call_c   Dyam_Unify_Item(&ref[569])
	fail_ret
	pl_jump  fun38(&seed[116],12)

;; TERM 568: '*PROLOG-FIRST*'(lctag_first(_B, [_C|_D], _E, _F)) :> []
c_code local build_ref_568
	ret_reg &ref[568]
	call_c   build_ref_567()
	call_c   Dyam_Create_Binary(I(9),&ref[567],I(0))
	move_ret ref[568]
	c_ret

;; TERM 567: '*PROLOG-FIRST*'(lctag_first(_B, [_C|_D], _E, _F))
c_code local build_ref_567
	ret_reg &ref[567]
	call_c   build_ref_64()
	call_c   build_ref_566()
	call_c   Dyam_Create_Unary(&ref[64],&ref[566])
	move_ret ref[567]
	c_ret

c_code local build_seed_35
	ret_reg &seed[35]
	call_c   build_ref_93()
	call_c   build_ref_609()
	call_c   Dyam_Seed_Start(&ref[93],&ref[609],I(0),fun21,1)
	call_c   build_ref_610()
	call_c   Dyam_Seed_Add_Comp(&ref[610],fun257,0)
	call_c   Dyam_Seed_End()
	move_ret seed[35]
	c_ret

;; TERM 610: '*CITEM*'(wrapping_predicate(tig_adj((_B / _C), _D, _E, _F)), _A)
c_code local build_ref_610
	ret_reg &ref[610]
	call_c   build_ref_97()
	call_c   build_ref_607()
	call_c   Dyam_Create_Binary(&ref[97],&ref[607],V(0))
	move_ret ref[610]
	c_ret

;; TERM 607: wrapping_predicate(tig_adj((_B / _C), _D, _E, _F))
c_code local build_ref_607
	ret_reg &ref[607]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1170()
	call_c   Dyam_Create_Binary(&ref[1170],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_1215()
	call_c   Dyam_Term_Start(&ref[1215],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_1214()
	call_c   Dyam_Create_Unary(&ref[1214],R(0))
	move_ret ref[607]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1214: wrapping_predicate
c_code local build_ref_1214
	ret_reg &ref[1214]
	call_c   Dyam_Create_Atom("wrapping_predicate")
	move_ret ref[1214]
	c_ret

;; TERM 1215: tig_adj
c_code local build_ref_1215
	ret_reg &ref[1215]
	call_c   Dyam_Create_Atom("tig_adj")
	move_ret ref[1215]
	c_ret

;; TERM 664: '$CLOSURE'('$fun'(256, 0, 1126430668), '$TUPPLE'(35144395917660))
c_code local build_ref_664
	ret_reg &ref[664]
	call_c   build_ref_663()
	call_c   Dyam_Closure_Aux(fun256,&ref[663])
	move_ret ref[664]
	c_ret

;; TERM 659: '$CLOSURE'('$fun'(255, 0, 1126381832), '$TUPPLE'(35144395917532))
c_code local build_ref_659
	ret_reg &ref[659]
	call_c   build_ref_658()
	call_c   Dyam_Closure_Aux(fun255,&ref[658])
	move_ret ref[659]
	c_ret

;; TERM 619: [_T|_N]
c_code local build_ref_619
	ret_reg &ref[619]
	call_c   Dyam_Create_List(V(19),V(13))
	move_ret ref[619]
	c_ret

;; TERM 655: '$CLOSURE'('$fun'(250, 0, 1126362472), '$TUPPLE'(35144395917396))
c_code local build_ref_655
	ret_reg &ref[655]
	call_c   build_ref_654()
	call_c   Dyam_Closure_Aux(fun250,&ref[654])
	move_ret ref[655]
	c_ret

;; TERM 652: '*WRAPPER-CALL-ALT*'(tig_adj((_B / _C), _D, yes, _F), _Q, '*PROLOG-LAST*')
c_code local build_ref_652
	ret_reg &ref[652]
	call_c   build_ref_1216()
	call_c   build_ref_656()
	call_c   build_ref_621()
	call_c   Dyam_Term_Start(&ref[1216],3)
	call_c   Dyam_Term_Arg(&ref[656])
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(&ref[621])
	call_c   Dyam_Term_End()
	move_ret ref[652]
	c_ret

;; TERM 621: '*PROLOG-LAST*'
c_code local build_ref_621
	ret_reg &ref[621]
	call_c   Dyam_Create_Atom("*PROLOG-LAST*")
	move_ret ref[621]
	c_ret

;; TERM 656: tig_adj((_B / _C), _D, yes, _F)
c_code local build_ref_656
	ret_reg &ref[656]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1170()
	call_c   Dyam_Create_Binary(&ref[1170],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_1215()
	call_c   build_ref_319()
	call_c   Dyam_Term_Start(&ref[1215],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(&ref[319])
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[656]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1216: '*WRAPPER-CALL-ALT*'
c_code local build_ref_1216
	ret_reg &ref[1216]
	call_c   Dyam_Create_Atom("*WRAPPER-CALL-ALT*")
	move_ret ref[1216]
	c_ret

;; TERM 643: '*CONT*'('*SA-SUBST*'(_Y, _Z, _A1, _K))
c_code local build_ref_643
	ret_reg &ref[643]
	call_c   build_ref_1217()
	call_c   build_ref_622()
	call_c   Dyam_Create_Unary(&ref[1217],&ref[622])
	move_ret ref[643]
	c_ret

;; TERM 622: '*SA-SUBST*'(_Y, _Z, _A1, _K)
c_code local build_ref_622
	ret_reg &ref[622]
	call_c   build_ref_1218()
	call_c   Dyam_Term_Start(&ref[1218],4)
	call_c   Dyam_Term_Arg(V(24))
	call_c   Dyam_Term_Arg(V(25))
	call_c   Dyam_Term_Arg(V(26))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_End()
	move_ret ref[622]
	c_ret

;; TERM 1218: '*SA-SUBST*'
c_code local build_ref_1218
	ret_reg &ref[1218]
	call_c   Dyam_Create_Atom("*SA-SUBST*")
	move_ret ref[1218]
	c_ret

;; TERM 1217: '*CONT*'
c_code local build_ref_1217
	ret_reg &ref[1217]
	call_c   Dyam_Create_Atom("*CONT*")
	move_ret ref[1217]
	c_ret

c_code local build_seed_127
	ret_reg &seed[127]
	call_c   build_ref_55()
	call_c   build_ref_641()
	call_c   Dyam_Seed_Start(&ref[55],&ref[641],I(0),fun11,1)
	call_c   build_ref_642()
	call_c   Dyam_Seed_Add_Comp(&ref[642],&ref[641],0)
	call_c   Dyam_Seed_End()
	move_ret seed[127]
	c_ret

;; TERM 642: '*GUARD*'(body_to_lpda(_U, (_L = +), _B1, _C1, _D1)) :> '$$HOLE$$'
c_code local build_ref_642
	ret_reg &ref[642]
	call_c   build_ref_641()
	call_c   Dyam_Create_Binary(I(9),&ref[641],I(7))
	move_ret ref[642]
	c_ret

;; TERM 641: '*GUARD*'(body_to_lpda(_U, (_L = +), _B1, _C1, _D1))
c_code local build_ref_641
	ret_reg &ref[641]
	call_c   build_ref_55()
	call_c   build_ref_640()
	call_c   Dyam_Create_Unary(&ref[55],&ref[640])
	move_ret ref[641]
	c_ret

;; TERM 640: body_to_lpda(_U, (_L = +), _B1, _C1, _D1)
c_code local build_ref_640
	ret_reg &ref[640]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1220()
	call_c   build_ref_254()
	call_c   Dyam_Create_Binary(&ref[1220],V(11),&ref[254])
	move_ret R(0)
	call_c   build_ref_1219()
	call_c   Dyam_Term_Start(&ref[1219],5)
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(27))
	call_c   Dyam_Term_Arg(V(28))
	call_c   Dyam_Term_Arg(V(29))
	call_c   Dyam_Term_End()
	move_ret ref[640]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1219: body_to_lpda
c_code local build_ref_1219
	ret_reg &ref[1219]
	call_c   Dyam_Create_Atom("body_to_lpda")
	move_ret ref[1219]
	c_ret

;; TERM 1220: =
c_code local build_ref_1220
	ret_reg &ref[1220]
	call_c   Dyam_Create_Atom("=")
	move_ret ref[1220]
	c_ret

c_code local build_seed_125
	ret_reg &seed[125]
	call_c   build_ref_55()
	call_c   build_ref_632()
	call_c   Dyam_Seed_Start(&ref[55],&ref[632],I(0),fun11,1)
	call_c   build_ref_633()
	call_c   Dyam_Seed_Add_Comp(&ref[633],&ref[632],0)
	call_c   Dyam_Seed_End()
	move_ret seed[125]
	c_ret

;; TERM 633: '*GUARD*'(body_to_lpda(_U, (_I = _J), ('*PROLOG-LAST*' :> fail), _H1, _D1)) :> '$$HOLE$$'
c_code local build_ref_633
	ret_reg &ref[633]
	call_c   build_ref_632()
	call_c   Dyam_Create_Binary(I(9),&ref[632],I(7))
	move_ret ref[633]
	c_ret

;; TERM 632: '*GUARD*'(body_to_lpda(_U, (_I = _J), ('*PROLOG-LAST*' :> fail), _H1, _D1))
c_code local build_ref_632
	ret_reg &ref[632]
	call_c   build_ref_55()
	call_c   build_ref_631()
	call_c   Dyam_Create_Unary(&ref[55],&ref[631])
	move_ret ref[632]
	c_ret

;; TERM 631: body_to_lpda(_U, (_I = _J), ('*PROLOG-LAST*' :> fail), _H1, _D1)
c_code local build_ref_631
	ret_reg &ref[631]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_1220()
	call_c   Dyam_Create_Binary(&ref[1220],V(8),V(9))
	move_ret R(0)
	call_c   build_ref_621()
	call_c   build_ref_299()
	call_c   Dyam_Create_Binary(I(9),&ref[621],&ref[299])
	move_ret R(1)
	call_c   build_ref_1219()
	call_c   Dyam_Term_Start(&ref[1219],5)
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(33))
	call_c   Dyam_Term_Arg(V(29))
	call_c   Dyam_Term_End()
	move_ret ref[631]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_126
	ret_reg &seed[126]
	call_c   build_ref_55()
	call_c   build_ref_635()
	call_c   Dyam_Seed_Start(&ref[55],&ref[635],I(0),fun11,1)
	call_c   build_ref_636()
	call_c   Dyam_Seed_Add_Comp(&ref[636],&ref[635],0)
	call_c   Dyam_Seed_End()
	move_ret seed[126]
	c_ret

;; TERM 636: '*GUARD*'(body_to_lpda(_U, (_G = _H), _H1, _I1, dyalog)) :> '$$HOLE$$'
c_code local build_ref_636
	ret_reg &ref[636]
	call_c   build_ref_635()
	call_c   Dyam_Create_Binary(I(9),&ref[635],I(7))
	move_ret ref[636]
	c_ret

;; TERM 635: '*GUARD*'(body_to_lpda(_U, (_G = _H), _H1, _I1, dyalog))
c_code local build_ref_635
	ret_reg &ref[635]
	call_c   build_ref_55()
	call_c   build_ref_634()
	call_c   Dyam_Create_Unary(&ref[55],&ref[634])
	move_ret ref[635]
	c_ret

;; TERM 634: body_to_lpda(_U, (_G = _H), _H1, _I1, dyalog)
c_code local build_ref_634
	ret_reg &ref[634]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1220()
	call_c   Dyam_Create_Binary(&ref[1220],V(6),V(7))
	move_ret R(0)
	call_c   build_ref_1219()
	call_c   build_ref_1221()
	call_c   Dyam_Term_Start(&ref[1219],5)
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(33))
	call_c   Dyam_Term_Arg(V(34))
	call_c   Dyam_Term_Arg(&ref[1221])
	call_c   Dyam_Term_End()
	move_ret ref[634]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1221: dyalog
c_code local build_ref_1221
	ret_reg &ref[1221]
	call_c   Dyam_Create_Atom("dyalog")
	move_ret ref[1221]
	c_ret

;; TERM 639: '*ALTERNATIVE-LAYER*'(_I1, _G1)
c_code local build_ref_639
	ret_reg &ref[639]
	call_c   build_ref_1222()
	call_c   Dyam_Create_Binary(&ref[1222],V(34),V(32))
	move_ret ref[639]
	c_ret

;; TERM 1222: '*ALTERNATIVE-LAYER*'
c_code local build_ref_1222
	ret_reg &ref[1222]
	call_c   Dyam_Create_Atom("*ALTERNATIVE-LAYER*")
	move_ret ref[1222]
	c_ret

;; TERM 638: '*WRAPPER*'(tig_adj((_B / _C), _D, _E, _F), [_T|_N], _K1)
c_code local build_ref_638
	ret_reg &ref[638]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1170()
	call_c   Dyam_Create_Binary(&ref[1170],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_1215()
	call_c   Dyam_Term_Start(&ref[1215],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_1223()
	call_c   build_ref_619()
	call_c   Dyam_Term_Start(&ref[1223],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[619])
	call_c   Dyam_Term_Arg(V(36))
	call_c   Dyam_Term_End()
	move_ret ref[638]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1223: '*WRAPPER*'
c_code local build_ref_1223
	ret_reg &ref[1223]
	call_c   Dyam_Create_Atom("*WRAPPER*")
	move_ret ref[1223]
	c_ret

long local pool_fun238[3]=[2,build_ref_638,build_seed_65]

long local pool_fun239[3]=[65537,build_ref_639,pool_fun238]

pl_code local fun239
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(36),&ref[639])
	fail_ret
fun238:
	call_c   DYAM_evpred_assert_1(&ref[638])
	call_c   Dyam_Deallocate()
	pl_jump  fun19(&seed[65],2)


;; TERM 637: '$VAR'(_J1, ['$SET$',adj(no, yes, strict, atmostone, one, sync),20])
c_code local build_ref_637
	ret_reg &ref[637]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1178()
	call_c   build_ref_706()
	call_c   build_ref_319()
	call_c   build_ref_673()
	call_c   build_ref_685()
	call_c   build_ref_686()
	call_c   build_ref_623()
	call_c   Dyam_Term_Start(&ref[1178],6)
	call_c   Dyam_Term_Arg(&ref[706])
	call_c   Dyam_Term_Arg(&ref[319])
	call_c   Dyam_Term_Arg(&ref[673])
	call_c   Dyam_Term_Arg(&ref[685])
	call_c   Dyam_Term_Arg(&ref[686])
	call_c   Dyam_Term_Arg(&ref[623])
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   Dyam_Term_Start(I(8),3)
	call_c   Dyam_Term_Arg(V(35))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(N(20))
	call_c   Dyam_Term_End()
	move_ret ref[637]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun240[6]=[131075,build_seed_125,build_seed_126,build_ref_637,pool_fun239,pool_fun238]

pl_code local fun241
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(32),V(28))
	fail_ret
fun240:
	pl_call  fun38(&seed[125],1)
	pl_call  fun38(&seed[126],1)
	call_c   Dyam_Choice(fun239)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(4),&ref[637])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(36),V(32))
	fail_ret
	pl_jump  fun238()


;; TERM 627: tagfilter_cat((_M ^ _F ^ _D ^ _G ^ _H ^ _I ^ _E1))
c_code local build_ref_627
	ret_reg &ref[627]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1203()
	call_c   Dyam_Create_Binary(&ref[1203],V(8),V(30))
	move_ret R(0)
	call_c   Dyam_Create_Binary(&ref[1203],V(7),R(0))
	move_ret R(0)
	call_c   Dyam_Create_Binary(&ref[1203],V(6),R(0))
	move_ret R(0)
	call_c   Dyam_Create_Binary(&ref[1203],V(3),R(0))
	move_ret R(0)
	call_c   Dyam_Create_Binary(&ref[1203],V(5),R(0))
	move_ret R(0)
	call_c   Dyam_Create_Binary(&ref[1203],V(12),R(0))
	move_ret R(0)
	call_c   build_ref_1202()
	call_c   Dyam_Create_Unary(&ref[1202],R(0))
	move_ret ref[627]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_124
	ret_reg &seed[124]
	call_c   build_ref_55()
	call_c   build_ref_629()
	call_c   Dyam_Seed_Start(&ref[55],&ref[629],I(0),fun11,1)
	call_c   build_ref_630()
	call_c   Dyam_Seed_Add_Comp(&ref[630],&ref[629],0)
	call_c   Dyam_Seed_End()
	move_ret seed[124]
	c_ret

;; TERM 630: '*GUARD*'(body_to_lpda(_U, _E1, _C1, _G1, dyalog)) :> '$$HOLE$$'
c_code local build_ref_630
	ret_reg &ref[630]
	call_c   build_ref_629()
	call_c   Dyam_Create_Binary(I(9),&ref[629],I(7))
	move_ret ref[630]
	c_ret

;; TERM 629: '*GUARD*'(body_to_lpda(_U, _E1, _C1, _G1, dyalog))
c_code local build_ref_629
	ret_reg &ref[629]
	call_c   build_ref_55()
	call_c   build_ref_628()
	call_c   Dyam_Create_Unary(&ref[55],&ref[628])
	move_ret ref[629]
	c_ret

;; TERM 628: body_to_lpda(_U, _E1, _C1, _G1, dyalog)
c_code local build_ref_628
	ret_reg &ref[628]
	call_c   build_ref_1219()
	call_c   build_ref_1221()
	call_c   Dyam_Term_Start(&ref[1219],5)
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_Arg(V(30))
	call_c   Dyam_Term_Arg(V(28))
	call_c   Dyam_Term_Arg(V(32))
	call_c   Dyam_Term_Arg(&ref[1221])
	call_c   Dyam_Term_End()
	move_ret ref[628]
	c_ret

long local pool_fun242[5]=[131074,build_ref_627,build_seed_124,pool_fun240,pool_fun240]

long local pool_fun243[3]=[65537,build_seed_127,pool_fun242]

pl_code local fun243
	call_c   Dyam_Remove_Choice()
	pl_call  fun38(&seed[127],1)
fun242:
	call_c   Dyam_Choice(fun241)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[627])
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(30))
	call_c   Dyam_Reg_Load(2,V(20))
	move     V(31), R(4)
	move     S(5), R(5)
	pl_call  pred_tag_numbervars_3()
	pl_call  fun38(&seed[124],1)
	pl_jump  fun240()


c_code local build_seed_123
	ret_reg &seed[123]
	call_c   build_ref_55()
	call_c   build_ref_625()
	call_c   Dyam_Seed_Start(&ref[55],&ref[625],I(0),fun11,1)
	call_c   build_ref_626()
	call_c   Dyam_Seed_Add_Comp(&ref[626],&ref[625],0)
	call_c   Dyam_Seed_End()
	move_ret seed[123]
	c_ret

;; TERM 626: '*GUARD*'(body_to_lpda(_U, (_L == +), _B1, _C1, _D1)) :> '$$HOLE$$'
c_code local build_ref_626
	ret_reg &ref[626]
	call_c   build_ref_625()
	call_c   Dyam_Create_Binary(I(9),&ref[625],I(7))
	move_ret ref[626]
	c_ret

;; TERM 625: '*GUARD*'(body_to_lpda(_U, (_L == +), _B1, _C1, _D1))
c_code local build_ref_625
	ret_reg &ref[625]
	call_c   build_ref_55()
	call_c   build_ref_624()
	call_c   Dyam_Create_Unary(&ref[55],&ref[624])
	move_ret ref[625]
	c_ret

;; TERM 624: body_to_lpda(_U, (_L == +), _B1, _C1, _D1)
c_code local build_ref_624
	ret_reg &ref[624]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1224()
	call_c   build_ref_254()
	call_c   Dyam_Create_Binary(&ref[1224],V(11),&ref[254])
	move_ret R(0)
	call_c   build_ref_1219()
	call_c   Dyam_Term_Start(&ref[1219],5)
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(27))
	call_c   Dyam_Term_Arg(V(28))
	call_c   Dyam_Term_Arg(V(29))
	call_c   Dyam_Term_End()
	move_ret ref[624]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1224: ==
c_code local build_ref_1224
	ret_reg &ref[1224]
	call_c   Dyam_Create_Atom("==")
	move_ret ref[1224]
	c_ret

long local pool_fun244[5]=[131074,build_ref_623,build_seed_123,pool_fun243,pool_fun242]

long local pool_fun245[3]=[65537,build_ref_643,pool_fun244]

pl_code local fun245
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(27),&ref[643])
	fail_ret
fun244:
	call_c   Dyam_Choice(fun243)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(4),&ref[623])
	fail_ret
	call_c   Dyam_Cut()
	pl_call  fun38(&seed[123],1)
	pl_jump  fun242()


long local pool_fun246[5]=[131074,build_ref_111,build_ref_622,pool_fun245,pool_fun244]

long local pool_fun250[3]=[65537,build_ref_652,pool_fun246]

pl_code local fun250
	call_c   Dyam_Pool(pool_fun250)
	call_c   Dyam_Unify(V(26),&ref[652])
	fail_ret
	call_c   Dyam_Allocate(0)
fun246:
	call_c   Dyam_Choice(fun245)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[111])
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(27),&ref[622])
	fail_ret
	pl_jump  fun244()


;; TERM 654: '$TUPPLE'(35144395917396)
c_code local build_ref_654
	ret_reg &ref[654]
	call_c   Dyam_Create_Simple_Tupple(0,536843032)
	move_ret ref[654]
	c_ret

long local pool_fun251[3]=[2,build_ref_655,build_ref_656]

pl_code local fun251
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Reg_Load(0,V(3))
	call_c   Dyam_Reg_Load(2,V(8))
	call_c   Dyam_Reg_Load(4,V(15))
	call_c   Dyam_Reg_Load(6,V(6))
	call_c   Dyam_Reg_Load(8,V(14))
	move     V(24), R(10)
	move     S(5), R(11)
	move     V(25), R(12)
	move     S(5), R(13)
	pl_call  pred_make_tig_callret_7()
	move     &ref[655], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	move     &ref[656], R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Deallocate(3)
fun249:
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Reg_Bind(2,V(3))
	call_c   Dyam_Reg_Bind(4,V(2))
	call_c   Dyam_Choice(fun248)
	pl_call  fun29(&seed[128],1)
	pl_fail


;; TERM 644: '*WRAPPER-INNER-CALL*'(tig_adj((_B / _C), _D, yes, _F), _Q, '*PROLOG-LAST*')
c_code local build_ref_644
	ret_reg &ref[644]
	call_c   build_ref_1225()
	call_c   build_ref_656()
	call_c   build_ref_621()
	call_c   Dyam_Term_Start(&ref[1225],3)
	call_c   Dyam_Term_Arg(&ref[656])
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(&ref[621])
	call_c   Dyam_Term_End()
	move_ret ref[644]
	c_ret

;; TERM 1225: '*WRAPPER-INNER-CALL*'
c_code local build_ref_1225
	ret_reg &ref[1225]
	call_c   Dyam_Create_Atom("*WRAPPER-INNER-CALL*")
	move_ret ref[1225]
	c_ret

long local pool_fun252[5]=[131074,build_ref_319,build_ref_644,pool_fun251,pool_fun246]

pl_code local fun252
	call_c   Dyam_Update_Choice(fun251)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(4),&ref[319])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(3))
	call_c   Dyam_Reg_Load(2,V(8))
	call_c   Dyam_Reg_Load(4,V(15))
	call_c   Dyam_Reg_Load(6,V(6))
	call_c   Dyam_Reg_Load(8,V(14))
	move     V(24), R(10)
	move     S(5), R(11)
	move     V(25), R(12)
	move     S(5), R(13)
	pl_call  pred_make_tig_callret_7()
	call_c   Dyam_Unify(V(26),&ref[644])
	fail_ret
	pl_jump  fun246()

;; TERM 620: '$VAR'(_X, ['$SET$',adj(no, yes, strict, atmostone, one, sync),56])
c_code local build_ref_620
	ret_reg &ref[620]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1178()
	call_c   build_ref_706()
	call_c   build_ref_319()
	call_c   build_ref_673()
	call_c   build_ref_685()
	call_c   build_ref_686()
	call_c   build_ref_623()
	call_c   Dyam_Term_Start(&ref[1178],6)
	call_c   Dyam_Term_Arg(&ref[706])
	call_c   Dyam_Term_Arg(&ref[319])
	call_c   Dyam_Term_Arg(&ref[673])
	call_c   Dyam_Term_Arg(&ref[685])
	call_c   Dyam_Term_Arg(&ref[686])
	call_c   Dyam_Term_Arg(&ref[623])
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   Dyam_Term_Start(I(8),3)
	call_c   Dyam_Term_Arg(V(23))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(N(56))
	call_c   Dyam_Term_End()
	move_ret ref[620]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun253[6]=[131075,build_ref_619,build_ref_620,build_ref_621,pool_fun252,pool_fun246]

pl_code local fun254
	call_c   Dyam_Remove_Choice()
fun253:
	move     &ref[619], R(0)
	move     S(5), R(1)
	move     V(20), R(2)
	move     S(5), R(3)
	move     V(21), R(4)
	move     S(5), R(5)
	pl_call  pred_tag_numbervars_3()
	call_c   Dyam_Reg_Load(0,V(16))
	call_c   Dyam_Reg_Load(2,V(20))
	move     V(22), R(4)
	move     S(5), R(5)
	pl_call  pred_tag_numbervars_3()
	call_c   Dyam_Choice(fun252)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(4),&ref[620])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(3))
	call_c   Dyam_Reg_Load(2,V(8))
	call_c   Dyam_Reg_Load(4,V(9))
	call_c   Dyam_Reg_Load(6,V(6))
	call_c   Dyam_Reg_Load(8,V(7))
	move     V(24), R(10)
	move     S(5), R(11)
	move     V(25), R(12)
	move     S(5), R(13)
	pl_call  pred_make_tig_callret_7()
	call_c   Dyam_Unify(V(26),&ref[621])
	fail_ret
	pl_jump  fun246()


;; TERM 618: callret(_R, _S)
c_code local build_ref_618
	ret_reg &ref[618]
	call_c   build_ref_1226()
	call_c   Dyam_Create_Binary(&ref[1226],V(17),V(18))
	move_ret ref[618]
	c_ret

;; TERM 1226: callret
c_code local build_ref_1226
	ret_reg &ref[1226]
	call_c   Dyam_Create_Atom("callret")
	move_ret ref[1226]
	c_ret

long local pool_fun255[3]=[65537,build_ref_618,pool_fun253]

pl_code local fun255
	call_c   Dyam_Pool(pool_fun255)
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun254)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(13),&ref[618])
	fail_ret
	call_c   Dyam_Cut()
	pl_fail

;; TERM 658: '$TUPPLE'(35144395917532)
c_code local build_ref_658
	ret_reg &ref[658]
	call_c   Dyam_Create_Simple_Tupple(0,536866816)
	move_ret ref[658]
	c_ret

;; TERM 660: tig_adj((_B / _C), _D)
c_code local build_ref_660
	ret_reg &ref[660]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1170()
	call_c   Dyam_Create_Binary(&ref[1170],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_1215()
	call_c   Dyam_Create_Binary(&ref[1215],R(0),V(3))
	move_ret ref[660]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 661: [_O,_H,_P,_J,_K,_L,_M]
c_code local build_ref_661
	ret_reg &ref[661]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Tupple(9,12,I(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(15),R(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(7),R(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(14),R(0))
	move_ret ref[661]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun256[4]=[3,build_ref_659,build_ref_660,build_ref_661]

pl_code local fun256
	call_c   Dyam_Pool(pool_fun256)
	call_c   Dyam_Allocate(0)
	move     &ref[659], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	move     &ref[660], R(4)
	move     S(5), R(5)
	move     &ref[661], R(6)
	move     S(5), R(7)
	move     V(16), R(8)
	move     S(5), R(9)
	call_c   Dyam_Reg_Deallocate(5)
fun237:
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Reg_Bind(2,V(5))
	call_c   Dyam_Multi_Reg_Bind(4,2,3)
	call_c   Dyam_Choice(fun236)
	pl_call  fun29(&seed[121],1)
	pl_fail


;; TERM 663: '$TUPPLE'(35144395917660)
c_code local build_ref_663
	ret_reg &ref[663]
	call_c   Dyam_Create_Simple_Tupple(0,536838144)
	move_ret ref[663]
	c_ret

;; TERM 665: [_G,_H,_I,_J,_K,_L,_M]
c_code local build_ref_665
	ret_reg &ref[665]
	call_c   Dyam_Create_Tupple(6,12,I(0))
	move_ret ref[665]
	c_ret

long local pool_fun257[5]=[4,build_ref_610,build_ref_664,build_ref_660,build_ref_665]

pl_code local fun257
	call_c   Dyam_Pool(pool_fun257)
	call_c   Dyam_Unify_Item(&ref[610])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[664], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	move     &ref[660], R(4)
	move     S(5), R(5)
	move     &ref[665], R(6)
	move     S(5), R(7)
	move     V(13), R(8)
	move     S(5), R(9)
	call_c   Dyam_Reg_Deallocate(5)
	pl_jump  fun237()

;; TERM 609: '*FIRST*'(wrapping_predicate(tig_adj((_B / _C), _D, _E, _F))) :> []
c_code local build_ref_609
	ret_reg &ref[609]
	call_c   build_ref_608()
	call_c   Dyam_Create_Binary(I(9),&ref[608],I(0))
	move_ret ref[609]
	c_ret

;; TERM 608: '*FIRST*'(wrapping_predicate(tig_adj((_B / _C), _D, _E, _F)))
c_code local build_ref_608
	ret_reg &ref[608]
	call_c   build_ref_93()
	call_c   build_ref_607()
	call_c   Dyam_Create_Unary(&ref[93],&ref[607])
	move_ret ref[608]
	c_ret

c_code local build_seed_38
	ret_reg &seed[38]
	call_c   build_ref_54()
	call_c   build_ref_672()
	call_c   Dyam_Seed_Start(&ref[54],&ref[672],I(0),fun0,1)
	call_c   build_ref_671()
	call_c   Dyam_Seed_Add_Comp(&ref[671],fun298,0)
	call_c   Dyam_Seed_End()
	move_ret seed[38]
	c_ret

;; TERM 671: '*GUARD*'(tig_compile_subtree_left(_B, tag_node{id=> _C, label=> _D, children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}, _O, _P, _Q, _R, _S, _T, _U, _V))
c_code local build_ref_671
	ret_reg &ref[671]
	call_c   build_ref_55()
	call_c   build_ref_670()
	call_c   Dyam_Create_Unary(&ref[55],&ref[670])
	move_ret ref[671]
	c_ret

;; TERM 670: tig_compile_subtree_left(_B, tag_node{id=> _C, label=> _D, children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}, _O, _P, _Q, _R, _S, _T, _U, _V)
c_code local build_ref_670
	ret_reg &ref[670]
	call_c   build_ref_1227()
	call_c   build_ref_991()
	call_c   Dyam_Term_Start(&ref[1227],10)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(&ref[991])
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_Arg(V(21))
	call_c   Dyam_Term_End()
	move_ret ref[670]
	c_ret

;; TERM 991: tag_node{id=> _C, label=> _D, children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}
c_code local build_ref_991
	ret_reg &ref[991]
	call_c   build_ref_1172()
	call_c   Dyam_Term_Start(&ref[1172],12)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_End()
	move_ret ref[991]
	c_ret

;; TERM 1227: tig_compile_subtree_left
c_code local build_ref_1227
	ret_reg &ref[1227]
	call_c   Dyam_Create_Atom("tig_compile_subtree_left")
	move_ret ref[1227]
	c_ret

c_code local build_seed_134
	ret_reg &seed[134]
	call_c   build_ref_55()
	call_c   build_ref_716()
	call_c   Dyam_Seed_Start(&ref[55],&ref[716],I(0),fun11,1)
	call_c   build_ref_717()
	call_c   Dyam_Seed_Add_Comp(&ref[717],&ref[716],0)
	call_c   Dyam_Seed_End()
	move_ret seed[134]
	c_ret

;; TERM 717: '*GUARD*'(tig_compile_subtree_right(_B, tag_node{id=> _C, label=> _D, children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}, _O, _P, _E1, _R, _S, _T, _U, _V, _Y)) :> '$$HOLE$$'
c_code local build_ref_717
	ret_reg &ref[717]
	call_c   build_ref_716()
	call_c   Dyam_Create_Binary(I(9),&ref[716],I(7))
	move_ret ref[717]
	c_ret

;; TERM 716: '*GUARD*'(tig_compile_subtree_right(_B, tag_node{id=> _C, label=> _D, children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}, _O, _P, _E1, _R, _S, _T, _U, _V, _Y))
c_code local build_ref_716
	ret_reg &ref[716]
	call_c   build_ref_55()
	call_c   build_ref_715()
	call_c   Dyam_Create_Unary(&ref[55],&ref[715])
	move_ret ref[716]
	c_ret

;; TERM 715: tig_compile_subtree_right(_B, tag_node{id=> _C, label=> _D, children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}, _O, _P, _E1, _R, _S, _T, _U, _V, _Y)
c_code local build_ref_715
	ret_reg &ref[715]
	call_c   build_ref_1228()
	call_c   build_ref_991()
	call_c   Dyam_Term_Start(&ref[1228],11)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(&ref[991])
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(30))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_Arg(V(21))
	call_c   Dyam_Term_Arg(V(24))
	call_c   Dyam_Term_End()
	move_ret ref[715]
	c_ret

;; TERM 1228: tig_compile_subtree_right
c_code local build_ref_1228
	ret_reg &ref[1228]
	call_c   Dyam_Create_Atom("tig_compile_subtree_right")
	move_ret ref[1228]
	c_ret

long local pool_fun293[2]=[1,build_seed_134]

pl_code local fun294
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(16),V(30))
	fail_ret
fun293:
	pl_call  fun38(&seed[134],1)
	pl_jump  fun85()


c_code local build_seed_132
	ret_reg &seed[132]
	call_c   build_ref_55()
	call_c   build_ref_708()
	call_c   Dyam_Seed_Start(&ref[55],&ref[708],I(0),fun11,1)
	call_c   build_ref_709()
	call_c   Dyam_Seed_Add_Comp(&ref[709],&ref[708],0)
	call_c   Dyam_Seed_End()
	move_ret seed[132]
	c_ret

;; TERM 709: '*GUARD*'(body_to_lpda(_B, (_Y == +), _Q, _E1, _S)) :> '$$HOLE$$'
c_code local build_ref_709
	ret_reg &ref[709]
	call_c   build_ref_708()
	call_c   Dyam_Create_Binary(I(9),&ref[708],I(7))
	move_ret ref[709]
	c_ret

;; TERM 708: '*GUARD*'(body_to_lpda(_B, (_Y == +), _Q, _E1, _S))
c_code local build_ref_708
	ret_reg &ref[708]
	call_c   build_ref_55()
	call_c   build_ref_707()
	call_c   Dyam_Create_Unary(&ref[55],&ref[707])
	move_ret ref[708]
	c_ret

;; TERM 707: body_to_lpda(_B, (_Y == +), _Q, _E1, _S)
c_code local build_ref_707
	ret_reg &ref[707]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1224()
	call_c   build_ref_254()
	call_c   Dyam_Create_Binary(&ref[1224],V(24),&ref[254])
	move_ret R(0)
	call_c   build_ref_1219()
	call_c   Dyam_Term_Start(&ref[1219],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(30))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_End()
	move_ret ref[707]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun295[5]=[131074,build_ref_673,build_seed_132,pool_fun293,pool_fun293]

pl_code local fun295
	call_c   Dyam_Update_Choice(fun294)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(6),&ref[673])
	fail_ret
	call_c   Dyam_Cut()
	pl_call  fun38(&seed[132],1)
	pl_jump  fun293()

;; TERM 674: adjkind((_W / _X), left)
c_code local build_ref_674
	ret_reg &ref[674]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1170()
	call_c   Dyam_Create_Binary(&ref[1170],V(22),V(23))
	move_ret R(0)
	call_c   build_ref_1136()
	call_c   build_ref_451()
	call_c   Dyam_Create_Binary(&ref[1136],R(0),&ref[451])
	move_ret ref[674]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 675: '$VAR'(_A1, ['$SET$',adj(no, yes, strict, atmostone, one, sync),62])
c_code local build_ref_675
	ret_reg &ref[675]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1178()
	call_c   build_ref_706()
	call_c   build_ref_319()
	call_c   build_ref_673()
	call_c   build_ref_685()
	call_c   build_ref_686()
	call_c   build_ref_623()
	call_c   Dyam_Term_Start(&ref[1178],6)
	call_c   Dyam_Term_Arg(&ref[706])
	call_c   Dyam_Term_Arg(&ref[319])
	call_c   Dyam_Term_Arg(&ref[673])
	call_c   Dyam_Term_Arg(&ref[685])
	call_c   Dyam_Term_Arg(&ref[686])
	call_c   Dyam_Term_Arg(&ref[623])
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   Dyam_Term_Start(I(8),3)
	call_c   Dyam_Term_Arg(V(26))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(N(62))
	call_c   Dyam_Term_End()
	move_ret ref[675]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 704: 'can''t apply feature_mode constraints nt=~w mode=~w top=~w bot=~w nid=~w tid=~w\n'
c_code local build_ref_704
	ret_reg &ref[704]
	call_c   Dyam_Create_Atom("can't apply feature_mode constraints nt=~w mode=~w top=~w bot=~w nid=~w tid=~w\n")
	move_ret ref[704]
	c_ret

;; TERM 705: [_D,left,_U,_D1,_C,_B]
c_code local build_ref_705
	ret_reg &ref[705]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_824()
	call_c   Dyam_Create_List(V(2),&ref[824])
	move_ret R(0)
	call_c   Dyam_Create_List(V(29),R(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(20),R(0))
	move_ret R(0)
	call_c   build_ref_451()
	call_c   Dyam_Create_List(&ref[451],R(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(3),R(0))
	move_ret ref[705]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_133
	ret_reg &seed[133]
	call_c   build_ref_55()
	call_c   build_ref_711()
	call_c   Dyam_Seed_Start(&ref[55],&ref[711],I(0),fun11,1)
	call_c   build_ref_712()
	call_c   Dyam_Seed_Add_Comp(&ref[712],&ref[711],0)
	call_c   Dyam_Seed_End()
	move_ret seed[133]
	c_ret

;; TERM 712: '*GUARD*'(tig_compile_subtree_right(_B, tag_node{id=> _C, label=> _D, children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}, _B1, _P, _E1, _F1, _S, _T, _D1, _V, _Y)) :> '$$HOLE$$'
c_code local build_ref_712
	ret_reg &ref[712]
	call_c   build_ref_711()
	call_c   Dyam_Create_Binary(I(9),&ref[711],I(7))
	move_ret ref[712]
	c_ret

;; TERM 711: '*GUARD*'(tig_compile_subtree_right(_B, tag_node{id=> _C, label=> _D, children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}, _B1, _P, _E1, _F1, _S, _T, _D1, _V, _Y))
c_code local build_ref_711
	ret_reg &ref[711]
	call_c   build_ref_55()
	call_c   build_ref_710()
	call_c   Dyam_Create_Unary(&ref[55],&ref[710])
	move_ret ref[711]
	c_ret

;; TERM 710: tig_compile_subtree_right(_B, tag_node{id=> _C, label=> _D, children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}, _B1, _P, _E1, _F1, _S, _T, _D1, _V, _Y)
c_code local build_ref_710
	ret_reg &ref[710]
	call_c   build_ref_1228()
	call_c   build_ref_991()
	call_c   Dyam_Term_Start(&ref[1228],11)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(&ref[991])
	call_c   Dyam_Term_Arg(V(27))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(30))
	call_c   Dyam_Term_Arg(V(31))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_Arg(V(29))
	call_c   Dyam_Term_Arg(V(21))
	call_c   Dyam_Term_Arg(V(24))
	call_c   Dyam_Term_End()
	move_ret ref[710]
	c_ret

;; TERM 697: '$CLOSURE'('$fun'(266, 0, 1126534420), '$TUPPLE'(35144395918580))
c_code local build_ref_697
	ret_reg &ref[697]
	call_c   build_ref_696()
	call_c   Dyam_Closure_Aux(fun266,&ref[696])
	move_ret ref[697]
	c_ret

;; TERM 692: '$CLOSURE'('$fun'(265, 0, 1126520192), '$TUPPLE'(35144395918428))
c_code local build_ref_692
	ret_reg &ref[692]
	call_c   build_ref_691()
	call_c   Dyam_Closure_Aux(fun265,&ref[691])
	move_ret ref[692]
	c_ret

;; TERM 689: '*WRAPPER-CALL-ALT*'(tig_adj((_W / _X), left, _I1, _D), _L1, _F1)
c_code local build_ref_689
	ret_reg &ref[689]
	call_c   build_ref_1216()
	call_c   build_ref_698()
	call_c   Dyam_Term_Start(&ref[1216],3)
	call_c   Dyam_Term_Arg(&ref[698])
	call_c   Dyam_Term_Arg(V(37))
	call_c   Dyam_Term_Arg(V(31))
	call_c   Dyam_Term_End()
	move_ret ref[689]
	c_ret

;; TERM 698: tig_adj((_W / _X), left, _I1, _D)
c_code local build_ref_698
	ret_reg &ref[698]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1170()
	call_c   Dyam_Create_Binary(&ref[1170],V(22),V(23))
	move_ret R(0)
	call_c   build_ref_1215()
	call_c   build_ref_451()
	call_c   Dyam_Term_Start(&ref[1215],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[451])
	call_c   Dyam_Term_Arg(V(34))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[698]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun265[2]=[1,build_ref_689]

pl_code local fun265
	call_c   Dyam_Pool(pool_fun265)
	call_c   Dyam_Unify(V(17),&ref[689])
	fail_ret
	call_c   Dyam_Allocate(0)
	pl_jump  fun85()

;; TERM 691: '$TUPPLE'(35144395918428)
c_code local build_ref_691
	ret_reg &ref[691]
	call_c   Dyam_Start_Tupple(0,33556576)
	call_c   Dyam_Almost_End_Tupple(29,76546048)
	move_ret ref[691]
	c_ret

;; TERM 693: tig_adj((_W / _X), left)
c_code local build_ref_693
	ret_reg &ref[693]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1170()
	call_c   Dyam_Create_Binary(&ref[1170],V(22),V(23))
	move_ret R(0)
	call_c   build_ref_1215()
	call_c   build_ref_451()
	call_c   Dyam_Create_Binary(&ref[1215],R(0),&ref[451])
	move_ret ref[693]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 694: [_O,_B1,_U,_D1,_C,_Y,_B]
c_code local build_ref_694
	ret_reg &ref[694]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_824()
	call_c   Dyam_Create_List(V(24),&ref[824])
	move_ret R(0)
	call_c   Dyam_Create_List(V(2),R(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(29),R(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(20),R(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(27),R(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(14),R(0))
	move_ret ref[694]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun266[4]=[3,build_ref_692,build_ref_693,build_ref_694]

pl_code local fun266
	call_c   Dyam_Pool(pool_fun266)
	call_c   Dyam_Allocate(0)
	move     &ref[692], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	move     &ref[693], R(4)
	move     S(5), R(5)
	move     &ref[694], R(6)
	move     S(5), R(7)
	move     V(37), R(8)
	move     S(5), R(9)
	call_c   Dyam_Reg_Deallocate(5)
	pl_jump  fun237()

;; TERM 696: '$TUPPLE'(35144395918580)
c_code local build_ref_696
	ret_reg &ref[696]
	call_c   Dyam_Start_Tupple(0,234899826)
	call_c   Dyam_Almost_End_Tupple(29,343932928)
	move_ret ref[696]
	c_ret

long local pool_fun267[3]=[2,build_ref_697,build_ref_698]

pl_code local fun267
	call_c   Dyam_Remove_Choice()
	move     &ref[697], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	move     &ref[698], R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  fun249()

;; TERM 687: no_tig_wrapper
c_code local build_ref_687
	ret_reg &ref[687]
	call_c   Dyam_Create_Atom("no_tig_wrapper")
	move_ret ref[687]
	c_ret

;; TERM 688: tig_adj((_W / _X), left, _I1)
c_code local build_ref_688
	ret_reg &ref[688]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1170()
	call_c   Dyam_Create_Binary(&ref[1170],V(22),V(23))
	move_ret R(0)
	call_c   build_ref_1215()
	call_c   build_ref_451()
	call_c   Dyam_Term_Start(&ref[1215],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[451])
	call_c   Dyam_Term_Arg(V(34))
	call_c   Dyam_Term_End()
	move_ret ref[688]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun268[4]=[65538,build_ref_687,build_ref_688,pool_fun267]

pl_code local fun271
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(34),V(6))
	fail_ret
fun268:
	call_c   Dyam_Choice(fun267)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[687])
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(1))
	move     &ref[688], R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Load(4,V(14))
	call_c   Dyam_Reg_Load(6,V(20))
	call_c   Dyam_Reg_Load(8,V(27))
	call_c   Dyam_Reg_Load(10,V(29))
	call_c   Dyam_Reg_Load(12,V(2))
	call_c   Dyam_Reg_Load(14,V(31))
	call_c   Dyam_Reg_Load(16,V(17))
	call_c   Dyam_Reg_Load(18,V(18))
	call_c   Dyam_Reg_Load(20,V(21))
	call_c   Dyam_Reg_Load(22,V(24))
	pl_call  pred_tig_compile_adj_node_12()
	pl_jump  fun85()


;; TERM 700: adjkind((_W / _X), _K1)
c_code local build_ref_700
	ret_reg &ref[700]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1170()
	call_c   Dyam_Create_Binary(&ref[1170],V(22),V(23))
	move_ret R(0)
	call_c   build_ref_1136()
	call_c   Dyam_Create_Binary(&ref[1136],R(0),V(36))
	move_ret ref[700]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun269[3]=[65537,build_ref_319,pool_fun268]

pl_code local fun270
	call_c   Dyam_Remove_Choice()
fun269:
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(34),&ref[319])
	fail_ret
	pl_jump  fun268()


long local pool_fun272[5]=[131074,build_ref_700,build_ref_451,pool_fun268,pool_fun269]

pl_code local fun272
	call_c   Dyam_Update_Choice(fun271)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[700])
	call_c   Dyam_Choice(fun270)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(36),&ref[451])
	fail_ret
	call_c   Dyam_Cut()
	pl_fail

;; TERM 699: '$VAR'(_J1, ['$SET$',adj(no, yes, strict, atmostone, one, sync),28])
c_code local build_ref_699
	ret_reg &ref[699]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1178()
	call_c   build_ref_706()
	call_c   build_ref_319()
	call_c   build_ref_673()
	call_c   build_ref_685()
	call_c   build_ref_686()
	call_c   build_ref_623()
	call_c   Dyam_Term_Start(&ref[1178],6)
	call_c   Dyam_Term_Arg(&ref[706])
	call_c   Dyam_Term_Arg(&ref[319])
	call_c   Dyam_Term_Arg(&ref[673])
	call_c   Dyam_Term_Arg(&ref[685])
	call_c   Dyam_Term_Arg(&ref[686])
	call_c   Dyam_Term_Arg(&ref[623])
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   Dyam_Term_Start(I(8),3)
	call_c   Dyam_Term_Arg(V(35))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(N(28))
	call_c   Dyam_Term_End()
	move_ret ref[699]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun273[4]=[131073,build_ref_699,pool_fun272,pool_fun268]

pl_code local fun273
	call_c   Dyam_Update_Choice(fun272)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(11),&ref[699])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(34),V(11))
	fail_ret
	pl_jump  fun268()

long local pool_fun274[7]=[131076,build_ref_673,build_ref_685,build_ref_623,build_ref_686,pool_fun273,pool_fun268]

long local pool_fun285[3]=[65537,build_seed_133,pool_fun274]

pl_code local fun286
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(16),V(30))
	fail_ret
fun285:
	pl_call  fun38(&seed[133],1)
fun274:
	call_c   Dyam_Reg_Load(0,V(20))
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(33), R(4)
	move     S(5), R(5)
	pl_call  pred_tag_numbervars_3()
	call_c   Dyam_Choice(fun273)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(6),&ref[673])
	fail_ret
	call_c   Dyam_Unify(V(11),&ref[685])
	fail_ret
	call_c   Dyam_Unify(V(12),&ref[623])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(34),&ref[686])
	fail_ret
	pl_jump  fun268()



long local pool_fun287[5]=[131074,build_ref_673,build_seed_132,pool_fun285,pool_fun285]

long local pool_fun289[4]=[65538,build_ref_704,build_ref_705,pool_fun287]

pl_code local fun289
	call_c   Dyam_Remove_Choice()
	move     &ref[704], R(0)
	move     0, R(1)
	move     &ref[705], R(2)
	move     S(5), R(3)
	pl_call  pred_format_2()
fun287:
	call_c   Dyam_Choice(fun286)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(6),&ref[673])
	fail_ret
	call_c   Dyam_Cut()
	pl_call  fun38(&seed[132],1)
	pl_jump  fun285()


;; TERM 714: '$CLOSURE'('$fun'(288, 0, 1126723848), '$TUPPLE'(35144395918196))
c_code local build_ref_714
	ret_reg &ref[714]
	call_c   build_ref_702()
	call_c   Dyam_Closure_Aux(fun288,&ref[702])
	move_ret ref[714]
	c_ret

pl_code local fun288
	call_c   Dyam_Pool(pool_fun287)
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Cut()
	pl_jump  fun287()

;; TERM 702: '$TUPPLE'(35144395918196)
c_code local build_ref_702
	ret_reg &ref[702]
	call_c   Dyam_Start_Tupple(0,268435442)
	call_c   Dyam_Almost_End_Tupple(29,268435456)
	move_ret ref[702]
	c_ret

long local pool_fun290[4]=[65538,build_ref_714,build_ref_451,pool_fun289]

pl_code local fun290
	call_c   Dyam_Remove_Choice()
	move     V(27), R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(32), R(4)
	move     S(5), R(5)
	pl_call  pred_tag_numbervars_3()
	call_c   Dyam_Choice(fun289)
	call_c   Dyam_Set_Cut()
	move     &ref[714], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Load(4,V(3))
	move     &ref[451], R(6)
	move     0, R(7)
	call_c   Dyam_Reg_Load(8,V(20))
	move     V(29), R(10)
	move     S(5), R(11)
	call_c   Dyam_Reg_Deallocate(6)
	pl_jump  fun155()

c_code local build_seed_130
	ret_reg &seed[130]
	call_c   build_ref_55()
	call_c   build_ref_680()
	call_c   Dyam_Seed_Start(&ref[55],&ref[680],I(0),fun11,1)
	call_c   build_ref_681()
	call_c   Dyam_Seed_Add_Comp(&ref[681],&ref[680],0)
	call_c   Dyam_Seed_End()
	move_ret seed[130]
	c_ret

;; TERM 681: '*GUARD*'(tag_compile_subtree_noadj(_B, tag_node{id=> _C, label=> _D, children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}, _B1, _P, _Q, _E1, _S, _T, _V)) :> '$$HOLE$$'
c_code local build_ref_681
	ret_reg &ref[681]
	call_c   build_ref_680()
	call_c   Dyam_Create_Binary(I(9),&ref[680],I(7))
	move_ret ref[681]
	c_ret

;; TERM 680: '*GUARD*'(tag_compile_subtree_noadj(_B, tag_node{id=> _C, label=> _D, children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}, _B1, _P, _Q, _E1, _S, _T, _V))
c_code local build_ref_680
	ret_reg &ref[680]
	call_c   build_ref_55()
	call_c   build_ref_679()
	call_c   Dyam_Create_Unary(&ref[55],&ref[679])
	move_ret ref[680]
	c_ret

;; TERM 679: tag_compile_subtree_noadj(_B, tag_node{id=> _C, label=> _D, children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}, _B1, _P, _Q, _E1, _S, _T, _V)
c_code local build_ref_679
	ret_reg &ref[679]
	call_c   build_ref_1229()
	call_c   build_ref_991()
	call_c   Dyam_Term_Start(&ref[1229],9)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(&ref[991])
	call_c   Dyam_Term_Arg(V(27))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(30))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_Arg(V(21))
	call_c   Dyam_Term_End()
	move_ret ref[679]
	c_ret

;; TERM 1229: tag_compile_subtree_noadj
c_code local build_ref_1229
	ret_reg &ref[1229]
	call_c   Dyam_Create_Atom("tag_compile_subtree_noadj")
	move_ret ref[1229]
	c_ret

pl_code local fun275
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(31),V(30))
	fail_ret
	pl_jump  fun274()

c_code local build_seed_131
	ret_reg &seed[131]
	call_c   build_ref_55()
	call_c   build_ref_683()
	call_c   Dyam_Seed_Start(&ref[55],&ref[683],I(0),fun11,1)
	call_c   build_ref_684()
	call_c   Dyam_Seed_Add_Comp(&ref[684],&ref[683],0)
	call_c   Dyam_Seed_End()
	move_ret seed[131]
	c_ret

;; TERM 684: '*GUARD*'(body_to_lpda(_B, (_Y == +), _E1, _F1, _S)) :> '$$HOLE$$'
c_code local build_ref_684
	ret_reg &ref[684]
	call_c   build_ref_683()
	call_c   Dyam_Create_Binary(I(9),&ref[683],I(7))
	move_ret ref[684]
	c_ret

;; TERM 683: '*GUARD*'(body_to_lpda(_B, (_Y == +), _E1, _F1, _S))
c_code local build_ref_683
	ret_reg &ref[683]
	call_c   build_ref_55()
	call_c   build_ref_682()
	call_c   Dyam_Create_Unary(&ref[55],&ref[682])
	move_ret ref[683]
	c_ret

;; TERM 682: body_to_lpda(_B, (_Y == +), _E1, _F1, _S)
c_code local build_ref_682
	ret_reg &ref[682]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1224()
	call_c   build_ref_254()
	call_c   Dyam_Create_Binary(&ref[1224],V(24),&ref[254])
	move_ret R(0)
	call_c   build_ref_1219()
	call_c   Dyam_Term_Start(&ref[1219],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(30))
	call_c   Dyam_Term_Arg(V(31))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_End()
	move_ret ref[682]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun276[6]=[131075,build_seed_130,build_ref_673,build_seed_131,pool_fun274,pool_fun274]

long local pool_fun278[4]=[65538,build_ref_704,build_ref_705,pool_fun276]

pl_code local fun278
	call_c   Dyam_Remove_Choice()
	move     &ref[704], R(0)
	move     0, R(1)
	move     &ref[705], R(2)
	move     S(5), R(3)
	pl_call  pred_format_2()
fun276:
	pl_call  fun38(&seed[130],1)
	call_c   Dyam_Choice(fun275)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(6),&ref[673])
	fail_ret
	call_c   Dyam_Cut()
	pl_call  fun38(&seed[131],1)
	pl_jump  fun274()


;; TERM 703: '$CLOSURE'('$fun'(277, 0, 1126607652), '$TUPPLE'(35144395918196))
c_code local build_ref_703
	ret_reg &ref[703]
	call_c   build_ref_702()
	call_c   Dyam_Closure_Aux(fun277,&ref[702])
	move_ret ref[703]
	c_ret

pl_code local fun277
	call_c   Dyam_Pool(pool_fun276)
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Cut()
	pl_jump  fun276()

long local pool_fun279[4]=[65538,build_ref_703,build_ref_451,pool_fun278]

pl_code local fun280
	call_c   Dyam_Remove_Choice()
fun279:
	call_c   Dyam_Cut()
	move     V(27), R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(28), R(4)
	move     S(5), R(5)
	pl_call  pred_tag_numbervars_3()
	call_c   Dyam_Unify(V(29),V(9))
	fail_ret
	call_c   Dyam_Choice(fun278)
	call_c   Dyam_Set_Cut()
	move     &ref[703], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Load(4,V(3))
	move     &ref[451], R(6)
	move     0, R(7)
	call_c   Dyam_Reg_Load(8,V(20))
	call_c   Dyam_Reg_Load(10,V(29))
	call_c   Dyam_Reg_Deallocate(6)
	pl_jump  fun155()


;; TERM 678: adjkind((_W / _X), wrap)
c_code local build_ref_678
	ret_reg &ref[678]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1170()
	call_c   Dyam_Create_Binary(&ref[1170],V(22),V(23))
	move_ret R(0)
	call_c   build_ref_1136()
	call_c   build_ref_512()
	call_c   Dyam_Create_Binary(&ref[1136],R(0),&ref[512])
	move_ret ref[678]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun281[3]=[65537,build_ref_678,pool_fun279]

long local pool_fun284[3]=[65537,build_ref_706,pool_fun281]

pl_code local fun284
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(12),&ref[706])
	fail_ret
fun281:
	call_c   Dyam_Choice(fun280)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[678])
	call_c   Dyam_Cut()
	pl_fail


pl_code local fun283
	call_c   Dyam_Remove_Choice()
fun282:
	call_c   Dyam_Cut()
	pl_jump  fun281()


;; TERM 677: adjkind((_W / _X), right)
c_code local build_ref_677
	ret_reg &ref[677]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1170()
	call_c   Dyam_Create_Binary(&ref[1170],V(22),V(23))
	move_ret R(0)
	call_c   build_ref_1136()
	call_c   build_ref_458()
	call_c   Dyam_Create_Binary(&ref[1136],R(0),&ref[458])
	move_ret ref[677]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun291[5]=[196609,build_ref_677,pool_fun290,pool_fun284,pool_fun281]

pl_code local fun292
	call_c   Dyam_Remove_Choice()
fun291:
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun290)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Choice(fun284)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Choice(fun283)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[677])
	call_c   Dyam_Cut()
	pl_fail


;; TERM 676: tig(_B, right)
c_code local build_ref_676
	ret_reg &ref[676]
	call_c   build_ref_1154()
	call_c   build_ref_458()
	call_c   Dyam_Create_Binary(&ref[1154],V(1),&ref[458])
	move_ret ref[676]
	c_ret

long local pool_fun296[7]=[131076,build_ref_674,build_ref_675,build_ref_319,build_ref_676,pool_fun295,pool_fun291]

long local pool_fun297[3]=[65537,build_ref_254,pool_fun296]

pl_code local fun297
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(24),&ref[254])
	fail_ret
fun296:
	call_c   Dyam_Choice(fun295)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[674])
	call_c   Dyam_Unify(V(11),&ref[675])
	fail_ret
	call_c   Dyam_Choice(fun292)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(7),&ref[319])
	fail_ret
	pl_call  Object_1(&ref[676])
	call_c   Dyam_Cut()
	pl_fail


long local pool_fun298[5]=[131074,build_ref_671,build_ref_673,pool_fun297,pool_fun296]

pl_code local fun298
	call_c   Dyam_Pool(pool_fun298)
	call_c   Dyam_Unify_Item(&ref[671])
	fail_ret
	call_c   DYAM_evpred_functor(V(20),V(22),V(23))
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun297)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(6),&ref[673])
	fail_ret
	call_c   Dyam_Cut()
	move     V(24), R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(25), R(4)
	move     S(5), R(5)
	pl_call  pred_tag_numbervars_3()
	pl_jump  fun296()

;; TERM 672: '*GUARD*'(tig_compile_subtree_left(_B, tag_node{id=> _C, label=> _D, children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}, _O, _P, _Q, _R, _S, _T, _U, _V)) :> []
c_code local build_ref_672
	ret_reg &ref[672]
	call_c   build_ref_671()
	call_c   Dyam_Create_Binary(I(9),&ref[671],I(0))
	move_ret ref[672]
	c_ret

c_code local build_seed_12
	ret_reg &seed[12]
	call_c   build_ref_63()
	call_c   build_ref_727()
	call_c   Dyam_Seed_Start(&ref[63],&ref[727],I(0),fun0,1)
	call_c   build_ref_728()
	call_c   Dyam_Seed_Add_Comp(&ref[728],fun315,0)
	call_c   Dyam_Seed_End()
	move_ret seed[12]
	c_ret

;; TERM 728: '*PROLOG-ITEM*'{top=> lctag_first(_B, (_C , _D), _E, _F), cont=> _A}
c_code local build_ref_728
	ret_reg &ref[728]
	call_c   build_ref_1126()
	call_c   build_ref_725()
	call_c   Dyam_Create_Binary(&ref[1126],&ref[725],V(0))
	move_ret ref[728]
	c_ret

;; TERM 725: lctag_first(_B, (_C , _D), _E, _F)
c_code local build_ref_725
	ret_reg &ref[725]
	call_c   build_ref_1127()
	call_c   build_ref_666()
	call_c   Dyam_Term_Start(&ref[1127],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(&ref[666])
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[725]
	c_ret

c_code local build_seed_136
	ret_reg &seed[136]
	call_c   build_ref_176()
	call_c   build_ref_737()
	call_c   Dyam_Seed_Start(&ref[176],&ref[737],I(0),fun11,1)
	call_c   build_ref_580()
	call_c   Dyam_Seed_Add_Comp(&ref[580],&ref[737],0)
	call_c   Dyam_Seed_End()
	move_ret seed[136]
	c_ret

;; TERM 737: '*PROLOG-ITEM*'{top=> lctag_first(_B, _C, _G, _H), cont=> '$CLOSURE'('$fun'(314, 0, 1126941768), '$TUPPLE'(35144395140360))}
c_code local build_ref_737
	ret_reg &ref[737]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,467664896)
	move_ret R(0)
	call_c   Dyam_Closure_Aux(fun314,R(0))
	move_ret R(0)
	call_c   build_ref_1126()
	call_c   build_ref_578()
	call_c   Dyam_Create_Binary(&ref[1126],&ref[578],R(0))
	move_ret ref[737]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 729: [pos(_I),postcat(_J, _K),postscan(_L),_M + '$VAR'(_N, ['$SET$',lctag_labels(epsilon, true, no_epsilon),3])]
c_code local build_ref_729
	ret_reg &ref[729]
	call_c   Dyam_Pseudo_Choice(4)
	call_c   build_ref_1211()
	call_c   Dyam_Create_Unary(&ref[1211],V(8))
	move_ret R(0)
	call_c   build_ref_1212()
	call_c   Dyam_Create_Binary(&ref[1212],V(9),V(10))
	move_ret R(1)
	call_c   build_ref_1213()
	call_c   Dyam_Create_Unary(&ref[1213],V(11))
	move_ret R(2)
	call_c   build_ref_1206()
	call_c   build_ref_305()
	call_c   build_ref_487()
	call_c   build_ref_1207()
	call_c   Dyam_Term_Start(&ref[1206],3)
	call_c   Dyam_Term_Arg(&ref[305])
	call_c   Dyam_Term_Arg(&ref[487])
	call_c   Dyam_Term_Arg(&ref[1207])
	call_c   Dyam_Term_End()
	move_ret R(3)
	call_c   Dyam_Term_Start(I(8),3)
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(R(3))
	call_c   Dyam_Term_Arg(N(3))
	call_c   Dyam_Term_End()
	move_ret R(3)
	call_c   build_ref_254()
	call_c   Dyam_Create_Binary(&ref[254],V(12),R(3))
	move_ret R(3)
	call_c   Dyam_Create_List(R(3),I(0))
	move_ret R(3)
	call_c   Dyam_Create_List(R(2),R(3))
	move_ret R(3)
	call_c   Dyam_Create_List(R(1),R(3))
	move_ret R(3)
	call_c   Dyam_Create_List(R(0),R(3))
	move_ret ref[729]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_135
	ret_reg &seed[135]
	call_c   build_ref_176()
	call_c   build_ref_733()
	call_c   Dyam_Seed_Start(&ref[176],&ref[733],I(0),fun11,1)
	call_c   build_ref_736()
	call_c   Dyam_Seed_Add_Comp(&ref[736],&ref[733],0)
	call_c   Dyam_Seed_End()
	move_ret seed[135]
	c_ret

;; TERM 736: '*PROLOG-FIRST*'(lctag_first(_B, _D, _Q, _R)) :> '$$HOLE$$'
c_code local build_ref_736
	ret_reg &ref[736]
	call_c   build_ref_735()
	call_c   Dyam_Create_Binary(I(9),&ref[735],I(7))
	move_ret ref[736]
	c_ret

;; TERM 735: '*PROLOG-FIRST*'(lctag_first(_B, _D, _Q, _R))
c_code local build_ref_735
	ret_reg &ref[735]
	call_c   build_ref_64()
	call_c   build_ref_734()
	call_c   Dyam_Create_Unary(&ref[64],&ref[734])
	move_ret ref[735]
	c_ret

;; TERM 734: lctag_first(_B, _D, _Q, _R)
c_code local build_ref_734
	ret_reg &ref[734]
	call_c   build_ref_1127()
	call_c   Dyam_Term_Start(&ref[1127],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_End()
	move_ret ref[734]
	c_ret

;; TERM 733: '*PROLOG-ITEM*'{top=> lctag_first(_B, _D, _Q, _R), cont=> '$CLOSURE'('$fun'(310, 0, 1126934128), '$TUPPLE'(35144395405052))}
c_code local build_ref_733
	ret_reg &ref[733]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,285233152)
	move_ret R(0)
	call_c   Dyam_Closure_Aux(fun310,R(0))
	move_ret R(0)
	call_c   build_ref_1126()
	call_c   build_ref_734()
	call_c   Dyam_Create_Binary(&ref[1126],&ref[734],R(0))
	move_ret ref[733]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

pl_code local fun309
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(4),V(16))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_jump  Follow_Cont(V(0))

;; TERM 731: '$VAR'(_S, ['$SET$',lctag_labels(epsilon, true, no_epsilon),3])
c_code local build_ref_731
	ret_reg &ref[731]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1206()
	call_c   build_ref_305()
	call_c   build_ref_487()
	call_c   build_ref_1207()
	call_c   Dyam_Term_Start(&ref[1206],3)
	call_c   Dyam_Term_Arg(&ref[305])
	call_c   Dyam_Term_Arg(&ref[487])
	call_c   Dyam_Term_Arg(&ref[1207])
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   Dyam_Term_Start(I(8),3)
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(N(3))
	call_c   Dyam_Term_End()
	move_ret ref[731]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 732: _O + _Q
c_code local build_ref_732
	ret_reg &ref[732]
	call_c   build_ref_254()
	call_c   Dyam_Create_Binary(&ref[254],V(14),V(16))
	move_ret ref[732]
	c_ret

long local pool_fun310[3]=[2,build_ref_731,build_ref_732]

pl_code local fun310
	call_c   Dyam_Pool(pool_fun310)
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun309)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(16),&ref[731])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(4),&ref[732])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_jump  Follow_Cont(V(0))

long local pool_fun311[2]=[1,build_seed_135]

pl_code local fun312
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(6),V(14))
	fail_ret
fun311:
	call_c   Dyam_Unify(V(5),V(7))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_jump  fun38(&seed[135],12)


;; TERM 730: _O + '$VAR'(_P, ['$SET$',lctag_labels(epsilon, true, no_epsilon),3])
c_code local build_ref_730
	ret_reg &ref[730]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1206()
	call_c   build_ref_305()
	call_c   build_ref_487()
	call_c   build_ref_1207()
	call_c   Dyam_Term_Start(&ref[1206],3)
	call_c   Dyam_Term_Arg(&ref[305])
	call_c   Dyam_Term_Arg(&ref[487])
	call_c   Dyam_Term_Arg(&ref[1207])
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   Dyam_Term_Start(I(8),3)
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(N(3))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_254()
	call_c   Dyam_Create_Binary(&ref[254],V(14),R(0))
	move_ret ref[730]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun313[5]=[131074,build_ref_729,build_ref_730,pool_fun311,pool_fun311]

pl_code local fun313
	call_c   Dyam_Update_Choice(fun215)
	call_c   Dyam_Set_Cut()
	pl_call  Domain_2(V(6),&ref[729])
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun312)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(6),&ref[730])
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun311()

long local pool_fun314[4]=[65538,build_ref_305,build_seed_72,pool_fun313]

pl_code local fun314
	call_c   Dyam_Pool(pool_fun314)
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun313)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(6),&ref[305])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Deallocate()
	pl_jump  fun38(&seed[72],12)

long local pool_fun315[3]=[2,build_ref_728,build_seed_136]

pl_code local fun315
	call_c   Dyam_Pool(pool_fun315)
	call_c   Dyam_Unify_Item(&ref[728])
	fail_ret
	pl_jump  fun38(&seed[136],12)

;; TERM 727: '*PROLOG-FIRST*'(lctag_first(_B, (_C , _D), _E, _F)) :> []
c_code local build_ref_727
	ret_reg &ref[727]
	call_c   build_ref_726()
	call_c   Dyam_Create_Binary(I(9),&ref[726],I(0))
	move_ret ref[727]
	c_ret

;; TERM 726: '*PROLOG-FIRST*'(lctag_first(_B, (_C , _D), _E, _F))
c_code local build_ref_726
	ret_reg &ref[726]
	call_c   build_ref_64()
	call_c   build_ref_725()
	call_c   Dyam_Create_Unary(&ref[64],&ref[725])
	move_ret ref[726]
	c_ret

c_code local build_seed_40
	ret_reg &seed[40]
	call_c   build_ref_54()
	call_c   build_ref_740()
	call_c   Dyam_Seed_Start(&ref[54],&ref[740],I(0),fun0,1)
	call_c   build_ref_739()
	call_c   Dyam_Seed_Add_Comp(&ref[739],fun349,0)
	call_c   Dyam_Seed_End()
	move_ret seed[40]
	c_ret

;; TERM 739: '*GUARD*'(tag_compile_tree(_B, _C, _D, _E, _F, [_G,_H]))
c_code local build_ref_739
	ret_reg &ref[739]
	call_c   build_ref_55()
	call_c   build_ref_738()
	call_c   Dyam_Create_Unary(&ref[55],&ref[738])
	move_ret ref[739]
	c_ret

;; TERM 738: tag_compile_tree(_B, _C, _D, _E, _F, [_G,_H])
c_code local build_ref_738
	ret_reg &ref[738]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Tupple(6,7,I(0))
	move_ret R(0)
	call_c   build_ref_1230()
	call_c   Dyam_Term_Start(&ref[1230],6)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_End()
	move_ret ref[738]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1230: tag_compile_tree
c_code local build_ref_1230
	ret_reg &ref[1230]
	call_c   Dyam_Create_Atom("tag_compile_tree")
	move_ret ref[1230]
	c_ret

;; TERM 741: tig(_B, _I)
c_code local build_ref_741
	ret_reg &ref[741]
	call_c   build_ref_1154()
	call_c   Dyam_Create_Binary(&ref[1154],V(1),V(8))
	move_ret ref[741]
	c_ret

;; TERM 742: tag_node{id=> _J, label=> _K, children=> _L, kind=> _M, adj=> _N, spine=> _O, top=> _P, bot=> _Q, token=> _R, adjleft=> _S, adjright=> _T, adjwrap=> _U}
c_code local build_ref_742
	ret_reg &ref[742]
	call_c   build_ref_1172()
	call_c   Dyam_Term_Start(&ref[1172],12)
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_End()
	move_ret ref[742]
	c_ret

;; TERM 829: guard{goal=> tag_node{id=> _G1, label=> _W, children=> _H1, kind=> _I1, adj=> _J1, spine=> yes, top=> _A1, bot=> _B1, token=> _K1, adjleft=> _L1, adjright=> _M1, adjwrap=> _N1}, plus=> _O1, minus=> _P1}
c_code local build_ref_829
	ret_reg &ref[829]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1172()
	call_c   build_ref_319()
	call_c   Dyam_Term_Start(&ref[1172],12)
	call_c   Dyam_Term_Arg(V(32))
	call_c   Dyam_Term_Arg(V(22))
	call_c   Dyam_Term_Arg(V(33))
	call_c   Dyam_Term_Arg(V(34))
	call_c   Dyam_Term_Arg(V(35))
	call_c   Dyam_Term_Arg(&ref[319])
	call_c   Dyam_Term_Arg(V(26))
	call_c   Dyam_Term_Arg(V(27))
	call_c   Dyam_Term_Arg(V(36))
	call_c   Dyam_Term_Arg(V(37))
	call_c   Dyam_Term_Arg(V(38))
	call_c   Dyam_Term_Arg(V(39))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_1198()
	call_c   Dyam_Term_Start(&ref[1198],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(40))
	call_c   Dyam_Term_Arg(V(41))
	call_c   Dyam_Term_End()
	move_ret ref[829]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 827: '$CLOSURE'('$fun'(346, 0, 1127774412), '$TUPPLE'(35144395140532))
c_code local build_ref_827
	ret_reg &ref[827]
	call_c   build_ref_826()
	call_c   Dyam_Closure_Aux(fun346,&ref[826])
	move_ret ref[827]
	c_ret

;; TERM 823: 'Not a TIG mode in {left,right} for tree ~w'
c_code local build_ref_823
	ret_reg &ref[823]
	call_c   Dyam_Create_Atom("Not a TIG mode in {left,right} for tree ~w")
	move_ret ref[823]
	c_ret

;; TERM 791: 'Top/Bot mistmatch for ~w TIG aux tree ~w\n'
c_code local build_ref_791
	ret_reg &ref[791]
	call_c   Dyam_Create_Atom("Top/Bot mistmatch for ~w TIG aux tree ~w\n")
	move_ret ref[791]
	c_ret

;; TERM 792: [_A1,_B1,_I,_B]
c_code local build_ref_792
	ret_reg &ref[792]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_824()
	call_c   Dyam_Create_List(V(8),&ref[824])
	move_ret R(0)
	call_c   Dyam_Create_Tupple(26,27,R(0))
	move_ret ref[792]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_146
	ret_reg &seed[146]
	call_c   build_ref_55()
	call_c   build_ref_794()
	call_c   Dyam_Seed_Start(&ref[55],&ref[794],I(0),fun11,1)
	call_c   build_ref_795()
	call_c   Dyam_Seed_Add_Comp(&ref[795],&ref[794],0)
	call_c   Dyam_Seed_End()
	move_ret seed[146]
	c_ret

;; TERM 795: '*GUARD*'(tag_compile_subtree(_B, _C, _G, _H, _A2, _L2, _E, _Y1, _C2)) :> '$$HOLE$$'
c_code local build_ref_795
	ret_reg &ref[795]
	call_c   build_ref_794()
	call_c   Dyam_Create_Binary(I(9),&ref[794],I(7))
	move_ret ref[795]
	c_ret

;; TERM 794: '*GUARD*'(tag_compile_subtree(_B, _C, _G, _H, _A2, _L2, _E, _Y1, _C2))
c_code local build_ref_794
	ret_reg &ref[794]
	call_c   build_ref_55()
	call_c   build_ref_793()
	call_c   Dyam_Create_Unary(&ref[55],&ref[793])
	move_ret ref[794]
	c_ret

;; TERM 793: tag_compile_subtree(_B, _C, _G, _H, _A2, _L2, _E, _Y1, _C2)
c_code local build_ref_793
	ret_reg &ref[793]
	call_c   build_ref_1231()
	call_c   Dyam_Term_Start(&ref[1231],9)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(52))
	call_c   Dyam_Term_Arg(V(63))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(50))
	call_c   Dyam_Term_Arg(V(54))
	call_c   Dyam_Term_End()
	move_ret ref[793]
	c_ret

;; TERM 1231: tag_compile_subtree
c_code local build_ref_1231
	ret_reg &ref[1231]
	call_c   Dyam_Create_Atom("tag_compile_subtree")
	move_ret ref[1231]
	c_ret

pl_code local fun325
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(51),V(69))
	fail_ret
	pl_jump  fun85()

;; TERM 776: tagfilter((_B ^ _G ^ _H ^ _W ^ _A1 ^ _S2))
c_code local build_ref_776
	ret_reg &ref[776]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1203()
	call_c   Dyam_Create_Binary(&ref[1203],V(26),V(70))
	move_ret R(0)
	call_c   Dyam_Create_Binary(&ref[1203],V(22),R(0))
	move_ret R(0)
	call_c   Dyam_Create_Binary(&ref[1203],V(7),R(0))
	move_ret R(0)
	call_c   Dyam_Create_Binary(&ref[1203],V(6),R(0))
	move_ret R(0)
	call_c   Dyam_Create_Binary(&ref[1203],V(1),R(0))
	move_ret R(0)
	call_c   build_ref_1204()
	call_c   Dyam_Create_Unary(&ref[1204],R(0))
	move_ret ref[776]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 777: lctag(_B, _T2, _U2, _V2)
c_code local build_ref_777
	ret_reg &ref[777]
	call_c   build_ref_1120()
	call_c   Dyam_Term_Start(&ref[1120],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(71))
	call_c   Dyam_Term_Arg(V(72))
	call_c   Dyam_Term_Arg(V(73))
	call_c   Dyam_Term_End()
	move_ret ref[777]
	c_ret

c_code local build_seed_144
	ret_reg &seed[144]
	call_c   build_ref_55()
	call_c   build_ref_779()
	call_c   Dyam_Seed_Start(&ref[55],&ref[779],I(0),fun11,1)
	call_c   build_ref_780()
	call_c   Dyam_Seed_Add_Comp(&ref[780],&ref[779],0)
	call_c   Dyam_Seed_End()
	move_ret seed[144]
	c_ret

;; TERM 780: '*GUARD*'(body_to_lpda(_B, _S2, _R2, _Z1, _E)) :> '$$HOLE$$'
c_code local build_ref_780
	ret_reg &ref[780]
	call_c   build_ref_779()
	call_c   Dyam_Create_Binary(I(9),&ref[779],I(7))
	move_ret ref[780]
	c_ret

;; TERM 779: '*GUARD*'(body_to_lpda(_B, _S2, _R2, _Z1, _E))
c_code local build_ref_779
	ret_reg &ref[779]
	call_c   build_ref_55()
	call_c   build_ref_778()
	call_c   Dyam_Create_Unary(&ref[55],&ref[778])
	move_ret ref[779]
	c_ret

;; TERM 778: body_to_lpda(_B, _S2, _R2, _Z1, _E)
c_code local build_ref_778
	ret_reg &ref[778]
	call_c   build_ref_1219()
	call_c   Dyam_Term_Start(&ref[1219],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(70))
	call_c   Dyam_Term_Arg(V(69))
	call_c   Dyam_Term_Arg(V(51))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[778]
	c_ret

long local pool_fun326[5]=[4,build_ref_451,build_ref_776,build_ref_777,build_seed_144]

pl_code local fun329
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(69),V(63))
	fail_ret
fun326:
	call_c   Dyam_Choice(fun325)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(8),&ref[451])
	fail_ret
	pl_call  Object_1(&ref[776])
	pl_call  Object_1(&ref[777])
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(70))
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(74), R(4)
	move     S(5), R(5)
	pl_call  pred_tag_numbervars_3()
	pl_call  fun38(&seed[144],1)
	pl_jump  fun85()


;; TERM 787: debug
c_code local build_ref_787
	ret_reg &ref[787]
	call_c   Dyam_Create_Atom("debug")
	move_ret ref[787]
	c_ret

c_code local build_seed_145
	ret_reg &seed[145]
	call_c   build_ref_55()
	call_c   build_ref_789()
	call_c   Dyam_Seed_Start(&ref[55],&ref[789],I(0),fun11,1)
	call_c   build_ref_790()
	call_c   Dyam_Seed_Add_Comp(&ref[790],&ref[789],0)
	call_c   Dyam_Seed_End()
	move_ret seed[145]
	c_ret

;; TERM 790: '*GUARD*'(body_to_lpda(_B, format('Selecting cat=~w left=~w tree ~w\n', [_A1,_G,_B]), _R2, _L2, _E)) :> '$$HOLE$$'
c_code local build_ref_790
	ret_reg &ref[790]
	call_c   build_ref_789()
	call_c   Dyam_Create_Binary(I(9),&ref[789],I(7))
	move_ret ref[790]
	c_ret

;; TERM 789: '*GUARD*'(body_to_lpda(_B, format('Selecting cat=~w left=~w tree ~w\n', [_A1,_G,_B]), _R2, _L2, _E))
c_code local build_ref_789
	ret_reg &ref[789]
	call_c   build_ref_55()
	call_c   build_ref_788()
	call_c   Dyam_Create_Unary(&ref[55],&ref[788])
	move_ret ref[789]
	c_ret

;; TERM 788: body_to_lpda(_B, format('Selecting cat=~w left=~w tree ~w\n', [_A1,_G,_B]), _R2, _L2, _E)
c_code local build_ref_788
	ret_reg &ref[788]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_824()
	call_c   Dyam_Create_List(V(6),&ref[824])
	move_ret R(0)
	call_c   Dyam_Create_List(V(26),R(0))
	move_ret R(0)
	call_c   build_ref_1232()
	call_c   build_ref_1233()
	call_c   Dyam_Create_Binary(&ref[1232],&ref[1233],R(0))
	move_ret R(0)
	call_c   build_ref_1219()
	call_c   Dyam_Term_Start(&ref[1219],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(69))
	call_c   Dyam_Term_Arg(V(63))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[788]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1233: 'Selecting cat=~w left=~w tree ~w\n'
c_code local build_ref_1233
	ret_reg &ref[1233]
	call_c   Dyam_Create_Atom("Selecting cat=~w left=~w tree ~w\n")
	move_ret ref[1233]
	c_ret

;; TERM 1232: format
c_code local build_ref_1232
	ret_reg &ref[1232]
	call_c   Dyam_Create_Atom("format")
	move_ret ref[1232]
	c_ret

long local pool_fun330[6]=[131075,build_ref_451,build_ref_787,build_seed_145,pool_fun326,pool_fun326]

pl_code local fun330
	call_c   Dyam_Update_Choice(fun329)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(8),&ref[451])
	fail_ret
	pl_call  Object_1(&ref[787])
	call_c   Dyam_Cut()
	pl_call  fun38(&seed[145],1)
	pl_jump  fun326()

;; TERM 759: guide(strip)
c_code local build_ref_759
	ret_reg &ref[759]
	call_c   build_ref_1234()
	call_c   build_ref_1235()
	call_c   Dyam_Create_Unary(&ref[1234],&ref[1235])
	move_ret ref[759]
	c_ret

;; TERM 1235: strip
c_code local build_ref_1235
	ret_reg &ref[1235]
	call_c   Dyam_Create_Atom("strip")
	move_ret ref[1235]
	c_ret

;; TERM 1234: guide
c_code local build_ref_1234
	ret_reg &ref[1234]
	call_c   Dyam_Create_Atom("guide")
	move_ret ref[1234]
	c_ret

;; TERM 786: '$CLOSURE'('$fun'(328, 0, 1122695804), '$TUPPLE'(35144395140956))
c_code local build_ref_786
	ret_reg &ref[786]
	call_c   build_ref_785()
	call_c   Dyam_Closure_Aux(fun328,&ref[785])
	move_ret ref[786]
	c_ret

;; TERM 783: '$CLOSURE'('$fun'(327, 0, 1122682276), '$TUPPLE'(35144395919140))
c_code local build_ref_783
	ret_reg &ref[783]
	call_c   build_ref_782()
	call_c   Dyam_Closure_Aux(fun327,&ref[782])
	move_ret ref[783]
	c_ret

;; TERM 775: '*GUIDE*'(_P2, _O2, _L2)
c_code local build_ref_775
	ret_reg &ref[775]
	call_c   build_ref_1236()
	call_c   Dyam_Term_Start(&ref[1236],3)
	call_c   Dyam_Term_Arg(V(67))
	call_c   Dyam_Term_Arg(V(66))
	call_c   Dyam_Term_Arg(V(63))
	call_c   Dyam_Term_End()
	move_ret ref[775]
	c_ret

;; TERM 1236: '*GUIDE*'
c_code local build_ref_1236
	ret_reg &ref[1236]
	call_c   Dyam_Create_Atom("*GUIDE*")
	move_ret ref[1236]
	c_ret

long local pool_fun327[3]=[65537,build_ref_775,pool_fun326]

pl_code local fun327
	call_c   Dyam_Pool(pool_fun327)
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(8))
	call_c   Dyam_Reg_Load(2,V(65))
	call_c   Dyam_Reg_Load(4,V(65))
	call_c   Dyam_Reg_Load(6,V(6))
	call_c   Dyam_Reg_Load(8,V(7))
	move     V(67), R(10)
	move     S(5), R(11)
	move     V(68), R(12)
	move     S(5), R(13)
	pl_call  pred_make_tig_callret_7()
	call_c   Dyam_Unify(V(69),&ref[775])
	fail_ret
	pl_jump  fun326()

;; TERM 782: '$TUPPLE'(35144395919140)
c_code local build_ref_782
	ret_reg &ref[782]
	call_c   Dyam_Start_Tupple(0,158335044)
	call_c   Dyam_Write_Tupple(29,64)
	call_c   Dyam_Almost_End_Tupple(58,11534336)
	move_ret ref[782]
	c_ret

long local pool_fun328[2]=[1,build_ref_783]

pl_code local fun328
	call_c   Dyam_Pool(pool_fun328)
	call_c   Dyam_Allocate(0)
	move     &ref[783], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Load(4,V(1))
	call_c   Dyam_Reg_Load(6,V(8))
	move     V(66), R(8)
	move     S(5), R(9)
	call_c   Dyam_Reg_Deallocate(5)
fun324:
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Reg_Bind(2,V(5))
	call_c   Dyam_Multi_Reg_Bind(4,2,3)
	call_c   Dyam_Choice(fun323)
	pl_call  fun29(&seed[142],1)
	pl_fail


;; TERM 785: '$TUPPLE'(35144395140956)
c_code local build_ref_785
	ret_reg &ref[785]
	call_c   Dyam_Start_Tupple(0,158335044)
	call_c   Dyam_Write_Tupple(29,64)
	call_c   Dyam_Almost_End_Tupple(58,10485760)
	move_ret ref[785]
	c_ret

long local pool_fun331[5]=[65539,build_ref_451,build_ref_759,build_ref_786,pool_fun330]

long local pool_fun332[5]=[65539,build_ref_791,build_ref_792,build_seed_146,pool_fun331]

pl_code local fun332
	call_c   Dyam_Remove_Choice()
	move     &ref[791], R(0)
	move     0, R(1)
	move     &ref[792], R(2)
	move     S(5), R(3)
	pl_call  pred_warning_2()
	call_c   Dyam_Reg_Load(0,V(2))
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(64), R(4)
	move     S(5), R(5)
	pl_call  pred_tag_numbervars_3()
	pl_call  fun38(&seed[146],1)
fun331:
	call_c   Dyam_Choice(fun330)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(8),&ref[451])
	fail_ret
	pl_call  Object_1(&ref[759])
	call_c   Dyam_Cut()
	move     &ref[786], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Load(4,V(22))
	move     V(65), R(6)
	move     S(5), R(7)
	call_c   Dyam_Reg_Deallocate(4)
fun321:
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Reg_Bind(2,V(4))
	call_c   Dyam_Multi_Reg_Bind(4,2,2)
	call_c   Dyam_Choice(fun320)
	pl_call  fun29(&seed[140],1)
	pl_fail



c_code local build_seed_139
	ret_reg &seed[139]
	call_c   build_ref_55()
	call_c   build_ref_757()
	call_c   Dyam_Seed_Start(&ref[55],&ref[757],I(0),fun11,1)
	call_c   build_ref_758()
	call_c   Dyam_Seed_Add_Comp(&ref[758],&ref[757],0)
	call_c   Dyam_Seed_End()
	move_ret seed[139]
	c_ret

;; TERM 758: '*GUARD*'(tag_compile_subtree_noadj(_B, _C, _G, _H, _A2, _L2, _E, _Y1, _C2)) :> '$$HOLE$$'
c_code local build_ref_758
	ret_reg &ref[758]
	call_c   build_ref_757()
	call_c   Dyam_Create_Binary(I(9),&ref[757],I(7))
	move_ret ref[758]
	c_ret

;; TERM 757: '*GUARD*'(tag_compile_subtree_noadj(_B, _C, _G, _H, _A2, _L2, _E, _Y1, _C2))
c_code local build_ref_757
	ret_reg &ref[757]
	call_c   build_ref_55()
	call_c   build_ref_756()
	call_c   Dyam_Create_Unary(&ref[55],&ref[756])
	move_ret ref[757]
	c_ret

;; TERM 756: tag_compile_subtree_noadj(_B, _C, _G, _H, _A2, _L2, _E, _Y1, _C2)
c_code local build_ref_756
	ret_reg &ref[756]
	call_c   build_ref_1229()
	call_c   Dyam_Term_Start(&ref[1229],9)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(52))
	call_c   Dyam_Term_Arg(V(63))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(50))
	call_c   Dyam_Term_Arg(V(54))
	call_c   Dyam_Term_End()
	move_ret ref[756]
	c_ret

long local pool_fun333[4]=[131073,build_seed_139,pool_fun332,pool_fun331]

long local pool_fun344[4]=[65538,build_ref_823,build_ref_824,pool_fun333]

pl_code local fun344
	call_c   Dyam_Remove_Choice()
	move     &ref[823], R(0)
	move     0, R(1)
	move     &ref[824], R(2)
	move     S(5), R(3)
	pl_call  pred_error_2()
fun333:
	call_c   Dyam_Choice(fun332)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(26),V(27))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(2))
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(62), R(4)
	move     S(5), R(5)
	pl_call  pred_tag_numbervars_3()
	pl_call  fun38(&seed[139],1)
	pl_jump  fun331()


;; TERM 797: _F * right_tig(_P, _D2, _W1)
c_code local build_ref_797
	ret_reg &ref[797]
	call_c   build_ref_393()
	call_c   build_ref_796()
	call_c   Dyam_Create_Binary(&ref[393],V(5),&ref[796])
	move_ret ref[797]
	c_ret

;; TERM 796: right_tig(_P, _D2, _W1)
c_code local build_ref_796
	ret_reg &ref[796]
	call_c   build_ref_1237()
	call_c   Dyam_Term_Start(&ref[1237],3)
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(55))
	call_c   Dyam_Term_Arg(V(48))
	call_c   Dyam_Term_End()
	move_ret ref[796]
	c_ret

;; TERM 1237: right_tig
c_code local build_ref_1237
	ret_reg &ref[1237]
	call_c   Dyam_Create_Atom("right_tig")
	move_ret ref[1237]
	c_ret

;; TERM 798: '*SA-LAST*'(_X1)
c_code local build_ref_798
	ret_reg &ref[798]
	call_c   build_ref_1238()
	call_c   Dyam_Create_Unary(&ref[1238],V(49))
	move_ret ref[798]
	c_ret

;; TERM 1238: '*SA-LAST*'
c_code local build_ref_1238
	ret_reg &ref[1238]
	call_c   Dyam_Create_Atom("*SA-LAST*")
	move_ret ref[1238]
	c_ret

pl_code local fun343
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(52),V(57))
	fail_ret
fun340:
	move     I(0), R(0)
	move     0, R(1)
	move     V(54), R(2)
	move     S(5), R(3)
	pl_call  pred_tupple_2()
	pl_jump  fun333()


;; TERM 822: '$CLOSURE'('$fun'(342, 0, 1127765580), '$TUPPLE'(35144395919664))
c_code local build_ref_822
	ret_reg &ref[822]
	call_c   build_ref_821()
	call_c   Dyam_Closure_Aux(fun342,&ref[821])
	move_ret ref[822]
	c_ret

;; TERM 818: '$CLOSURE'('$fun'(341, 0, 1122825816), '$TUPPLE'(35144395919512))
c_code local build_ref_818
	ret_reg &ref[818]
	call_c   build_ref_817()
	call_c   Dyam_Closure_Aux(fun341,&ref[817])
	move_ret ref[818]
	c_ret

c_code local build_seed_151
	ret_reg &seed[151]
	call_c   build_ref_55()
	call_c   build_ref_814()
	call_c   Dyam_Seed_Start(&ref[55],&ref[814],I(0),fun11,1)
	call_c   build_ref_815()
	call_c   Dyam_Seed_Add_Comp(&ref[815],&ref[814],0)
	call_c   Dyam_Seed_End()
	move_ret seed[151]
	c_ret

;; TERM 815: '*GUARD*'(body_to_lpda(_B, verbose!struct(_B, _G2), _F2, _A2, _E)) :> '$$HOLE$$'
c_code local build_ref_815
	ret_reg &ref[815]
	call_c   build_ref_814()
	call_c   Dyam_Create_Binary(I(9),&ref[814],I(7))
	move_ret ref[815]
	c_ret

;; TERM 814: '*GUARD*'(body_to_lpda(_B, verbose!struct(_B, _G2), _F2, _A2, _E))
c_code local build_ref_814
	ret_reg &ref[814]
	call_c   build_ref_55()
	call_c   build_ref_813()
	call_c   Dyam_Create_Unary(&ref[55],&ref[813])
	move_ret ref[814]
	c_ret

;; TERM 813: body_to_lpda(_B, verbose!struct(_B, _G2), _F2, _A2, _E)
c_code local build_ref_813
	ret_reg &ref[813]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1239()
	call_c   Dyam_Create_Binary(&ref[1239],V(1),V(58))
	move_ret R(0)
	call_c   build_ref_1219()
	call_c   Dyam_Term_Start(&ref[1219],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(57))
	call_c   Dyam_Term_Arg(V(52))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[813]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1239: verbose!struct
c_code local build_ref_1239
	ret_reg &ref[1239]
	call_c   build_ref_1240()
	call_c   Dyam_Create_Atom_Module("struct",&ref[1240])
	move_ret ref[1239]
	c_ret

;; TERM 1240: verbose
c_code local build_ref_1240
	ret_reg &ref[1240]
	call_c   Dyam_Create_Atom("verbose")
	move_ret ref[1240]
	c_ret

long local pool_fun341[3]=[65537,build_seed_151,pool_fun333]

pl_code local fun341
	call_c   Dyam_Pool(pool_fun341)
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(58))
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(61), R(4)
	move     S(5), R(5)
	pl_call  pred_tag_numbervars_3()
	pl_call  fun38(&seed[151],1)
	pl_jump  fun340()

;; TERM 817: '$TUPPLE'(35144395919512)
c_code local build_ref_817
	ret_reg &ref[817]
	call_c   Dyam_Start_Tupple(0,225443910)
	call_c   Dyam_Write_Tupple(29,193)
	call_c   Dyam_Almost_End_Tupple(58,268435456)
	move_ret ref[817]
	c_ret

;; TERM 819: tag_anchor{family=> _G2, coanchors=> _H2, equations=> _I2}
c_code local build_ref_819
	ret_reg &ref[819]
	call_c   build_ref_1241()
	call_c   Dyam_Term_Start(&ref[1241],3)
	call_c   Dyam_Term_Arg(V(58))
	call_c   Dyam_Term_Arg(V(59))
	call_c   Dyam_Term_Arg(V(60))
	call_c   Dyam_Term_End()
	move_ret ref[819]
	c_ret

;; TERM 1241: tag_anchor!'$ft'
c_code local build_ref_1241
	ret_reg &ref[1241]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1244()
	call_c   Dyam_Create_List(&ref[1244],I(0))
	move_ret R(0)
	call_c   build_ref_1243()
	call_c   Dyam_Create_List(&ref[1243],R(0))
	move_ret R(0)
	call_c   build_ref_1144()
	call_c   Dyam_Create_List(&ref[1144],R(0))
	move_ret R(0)
	call_c   build_ref_1242()
	call_c   Dyam_Create_Atom_Module("$ft",&ref[1242])
	move_ret ref[1241]
	call_c   DYAM_Feature_2(&ref[1242],R(0))
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1242: tag_anchor
c_code local build_ref_1242
	ret_reg &ref[1242]
	call_c   Dyam_Create_Atom("tag_anchor")
	move_ret ref[1242]
	c_ret

;; TERM 1243: coanchors
c_code local build_ref_1243
	ret_reg &ref[1243]
	call_c   Dyam_Create_Atom("coanchors")
	move_ret ref[1243]
	c_ret

;; TERM 1244: equations
c_code local build_ref_1244
	ret_reg &ref[1244]
	call_c   Dyam_Create_Atom("equations")
	move_ret ref[1244]
	c_ret

long local pool_fun342[3]=[2,build_ref_818,build_ref_819]

pl_code local fun342
	call_c   Dyam_Pool(pool_fun342)
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Cut()
	move     &ref[818], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Load(4,V(1))
	call_c   Dyam_Reg_Load(6,V(5))
	move     &ref[819], R(8)
	move     S(5), R(9)
	call_c   Dyam_Reg_Deallocate(5)
fun339:
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Reg_Bind(2,V(5))
	call_c   Dyam_Multi_Reg_Bind(4,2,3)
	call_c   Dyam_Choice(fun338)
	pl_call  fun29(&seed[149],1)
	pl_fail


;; TERM 821: '$TUPPLE'(35144395919664)
c_code local build_ref_821
	ret_reg &ref[821]
	call_c   Dyam_Start_Tupple(0,233832518)
	call_c   Dyam_Almost_End_Tupple(29,193)
	move_ret ref[821]
	c_ret

long local pool_fun345[7]=[131076,build_ref_458,build_ref_797,build_ref_798,build_ref_822,pool_fun344,pool_fun333]

pl_code local fun345
	call_c   Dyam_Update_Choice(fun344)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(8),&ref[458])
	fail_ret
	call_c   Dyam_Cut()
	move     V(55), R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(56), R(4)
	move     S(5), R(5)
	pl_call  pred_tag_numbervars_3()
	call_c   Dyam_Reg_Load(0,V(8))
	call_c   Dyam_Reg_Load(2,V(15))
	call_c   Dyam_Reg_Load(4,V(26))
	call_c   Dyam_Reg_Load(6,V(55))
	call_c   Dyam_Reg_Load(8,V(7))
	move     V(48), R(10)
	move     S(5), R(11)
	move     V(49), R(12)
	move     S(5), R(13)
	pl_call  pred_make_tig_callret_7()
	call_c   Dyam_Unify(V(50),&ref[797])
	fail_ret
	call_c   Dyam_Unify(V(3),V(51))
	fail_ret
	call_c   Dyam_Unify(V(57),&ref[798])
	fail_ret
	call_c   Dyam_Choice(fun343)
	call_c   Dyam_Set_Cut()
	move     &ref[822], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Load(4,V(1))
	call_c   Dyam_Reg_Deallocate(3)
fun336:
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Reg_Bind(2,V(3))
	call_c   Dyam_Reg_Bind(4,V(2))
	call_c   Dyam_Choice(fun335)
	pl_call  fun29(&seed[147],1)
	pl_fail


;; TERM 752: _F * left_tig(_P, _U1, _X1)
c_code local build_ref_752
	ret_reg &ref[752]
	call_c   build_ref_393()
	call_c   build_ref_751()
	call_c   Dyam_Create_Binary(&ref[393],V(5),&ref[751])
	move_ret ref[752]
	c_ret

;; TERM 751: left_tig(_P, _U1, _X1)
c_code local build_ref_751
	ret_reg &ref[751]
	call_c   build_ref_1245()
	call_c   Dyam_Term_Start(&ref[1245],3)
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(46))
	call_c   Dyam_Term_Arg(V(49))
	call_c   Dyam_Term_End()
	move_ret ref[751]
	c_ret

;; TERM 1245: left_tig
c_code local build_ref_1245
	ret_reg &ref[1245]
	call_c   Dyam_Create_Atom("left_tig")
	move_ret ref[1245]
	c_ret

;; TERM 754: '*SAFIRST*'(_W1) :> _Z1
c_code local build_ref_754
	ret_reg &ref[754]
	call_c   build_ref_753()
	call_c   Dyam_Create_Binary(I(9),&ref[753],V(51))
	move_ret ref[754]
	c_ret

;; TERM 753: '*SAFIRST*'(_W1)
c_code local build_ref_753
	ret_reg &ref[753]
	call_c   build_ref_1246()
	call_c   Dyam_Create_Unary(&ref[1246],V(48))
	move_ret ref[753]
	c_ret

;; TERM 1246: '*SAFIRST*'
c_code local build_ref_1246
	ret_reg &ref[1246]
	call_c   Dyam_Create_Atom("*SAFIRST*")
	move_ret ref[1246]
	c_ret

;; TERM 755: noop
c_code local build_ref_755
	ret_reg &ref[755]
	call_c   Dyam_Create_Atom("noop")
	move_ret ref[755]
	c_ret

long local pool_fun346[7]=[131076,build_ref_451,build_ref_752,build_ref_754,build_ref_755,pool_fun345,pool_fun333]

pl_code local fun346
	call_c   Dyam_Pool(pool_fun346)
	call_c   DYAM_evpred_functor(V(15),V(42),V(43))
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(6))
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(44), R(4)
	move     S(5), R(5)
	pl_call  pred_tag_numbervars_3()
	call_c   Dyam_Reg_Load(0,V(7))
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(45), R(4)
	move     S(5), R(5)
	pl_call  pred_tag_numbervars_3()
	call_c   Dyam_Choice(fun345)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(8),&ref[451])
	fail_ret
	call_c   Dyam_Cut()
	move     V(46), R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(47), R(4)
	move     S(5), R(5)
	pl_call  pred_tag_numbervars_3()
	call_c   Dyam_Reg_Load(0,V(8))
	call_c   Dyam_Reg_Load(2,V(26))
	call_c   Dyam_Reg_Load(4,V(15))
	call_c   Dyam_Reg_Load(6,V(6))
	call_c   Dyam_Reg_Load(8,V(46))
	move     V(48), R(10)
	move     S(5), R(11)
	move     V(49), R(12)
	move     S(5), R(13)
	pl_call  pred_make_tig_callret_7()
	call_c   Dyam_Unify(V(50),&ref[752])
	fail_ret
	call_c   Dyam_Unify(V(3),&ref[754])
	fail_ret
	call_c   Dyam_Unify(V(52),&ref[755])
	fail_ret
	call_c   Dyam_Reg_Load(0,V(48))
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(53), R(4)
	move     S(5), R(5)
	pl_call  pred_tag_numbervars_3()
	call_c   Dyam_Reg_Load(0,V(48))
	move     V(54), R(2)
	move     S(5), R(3)
	pl_call  pred_tupple_2()
	pl_jump  fun333()

;; TERM 826: '$TUPPLE'(35144395140532)
c_code local build_ref_826
	ret_reg &ref[826]
	call_c   Dyam_Start_Tupple(0,267395142)
	call_c   Dyam_Almost_End_Tupple(29,49152)
	move_ret ref[826]
	c_ret

;; TERM 828: tag((_Q1 / _R1))
c_code local build_ref_828
	ret_reg &ref[828]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1170()
	call_c   Dyam_Create_Binary(&ref[1170],V(42),V(43))
	move_ret R(0)
	call_c   build_ref_1247()
	call_c   Dyam_Create_Unary(&ref[1247],R(0))
	move_ret ref[828]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1247: tag
c_code local build_ref_1247
	ret_reg &ref[1247]
	call_c   Dyam_Create_Atom("tag")
	move_ret ref[1247]
	c_ret

long local pool_fun347[3]=[2,build_ref_827,build_ref_828]

long local pool_fun348[3]=[65537,build_ref_829,pool_fun347]

pl_code local fun348
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(2),&ref[829])
	fail_ret
fun347:
	call_c   DYAM_evpred_functor(V(26),V(42),V(43))
	fail_ret
	move     &ref[827], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	move     &ref[828], R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Deallocate(3)
fun318:
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Reg_Bind(2,V(3))
	call_c   Dyam_Reg_Bind(4,V(2))
	call_c   Dyam_Choice(fun317)
	pl_call  fun29(&seed[137],1)
	pl_fail



;; TERM 743: tag_node{id=> _V, label=> _W, children=> _X, kind=> _Y, adj=> _Z, spine=> yes, top=> _A1, bot=> _B1, token=> _C1, adjleft=> _D1, adjright=> _E1, adjwrap=> _F1}
c_code local build_ref_743
	ret_reg &ref[743]
	call_c   build_ref_1172()
	call_c   build_ref_319()
	call_c   Dyam_Term_Start(&ref[1172],12)
	call_c   Dyam_Term_Arg(V(21))
	call_c   Dyam_Term_Arg(V(22))
	call_c   Dyam_Term_Arg(V(23))
	call_c   Dyam_Term_Arg(V(24))
	call_c   Dyam_Term_Arg(V(25))
	call_c   Dyam_Term_Arg(&ref[319])
	call_c   Dyam_Term_Arg(V(26))
	call_c   Dyam_Term_Arg(V(27))
	call_c   Dyam_Term_Arg(V(28))
	call_c   Dyam_Term_Arg(V(29))
	call_c   Dyam_Term_Arg(V(30))
	call_c   Dyam_Term_Arg(V(31))
	call_c   Dyam_Term_End()
	move_ret ref[743]
	c_ret

long local pool_fun349[7]=[131076,build_ref_739,build_ref_741,build_ref_742,build_ref_743,pool_fun348,pool_fun347]

pl_code local fun349
	call_c   Dyam_Pool(pool_fun349)
	call_c   Dyam_Unify_Item(&ref[739])
	fail_ret
	call_c   Dyam_Allocate(0)
	pl_call  Object_1(&ref[741])
	call_c   Dyam_Reg_Load(0,V(2))
	call_c   Dyam_Reg_Load(2,V(5))
	pl_call  pred_tree_info_2()
	call_c   Dyam_Reg_Load(0,V(2))
	move     &ref[742], R(2)
	move     S(5), R(3)
	pl_call  pred_tree_foot_2()
	call_c   Dyam_Choice(fun348)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(2),&ref[743])
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun347()

;; TERM 740: '*GUARD*'(tag_compile_tree(_B, _C, _D, _E, _F, [_G,_H])) :> []
c_code local build_ref_740
	ret_reg &ref[740]
	call_c   build_ref_739()
	call_c   Dyam_Create_Binary(I(9),&ref[739],I(0))
	move_ret ref[740]
	c_ret

c_code local build_seed_33
	ret_reg &seed[33]
	call_c   build_ref_93()
	call_c   build_ref_840()
	call_c   Dyam_Seed_Start(&ref[93],&ref[840],I(0),fun21,1)
	call_c   build_ref_841()
	call_c   Dyam_Seed_Add_Comp(&ref[841],fun367,0)
	call_c   Dyam_Seed_End()
	move_ret seed[33]
	c_ret

;; TERM 841: '*CITEM*'('call_decompose_args/3'(callret_tig_adj((_B / _C), _D)), _A)
c_code local build_ref_841
	ret_reg &ref[841]
	call_c   build_ref_97()
	call_c   build_ref_838()
	call_c   Dyam_Create_Binary(&ref[97],&ref[838],V(0))
	move_ret ref[841]
	c_ret

;; TERM 838: 'call_decompose_args/3'(callret_tig_adj((_B / _C), _D))
c_code local build_ref_838
	ret_reg &ref[838]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1170()
	call_c   Dyam_Create_Binary(&ref[1170],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_1249()
	call_c   Dyam_Create_Binary(&ref[1249],R(0),V(3))
	move_ret R(0)
	call_c   build_ref_1248()
	call_c   Dyam_Create_Unary(&ref[1248],R(0))
	move_ret ref[838]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1248: 'call_decompose_args/3'
c_code local build_ref_1248
	ret_reg &ref[1248]
	call_c   Dyam_Create_Atom("call_decompose_args/3")
	move_ret ref[1248]
	c_ret

;; TERM 1249: callret_tig_adj
c_code local build_ref_1249
	ret_reg &ref[1249]
	call_c   Dyam_Create_Atom("callret_tig_adj")
	move_ret ref[1249]
	c_ret

;; TERM 851: [_S|_P]
c_code local build_ref_851
	ret_reg &ref[851]
	call_c   Dyam_Create_List(V(18),V(15))
	move_ret ref[851]
	c_ret

;; TERM 852: [_T|_R]
c_code local build_ref_852
	ret_reg &ref[852]
	call_c   Dyam_Create_List(V(19),V(17))
	move_ret ref[852]
	c_ret

;; TERM 845: callret([_E,_I,_J,_K|_P], [_F,_I,_J|_R])
c_code local build_ref_845
	ret_reg &ref[845]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   Dyam_Create_Tupple(8,10,V(15))
	move_ret R(0)
	call_c   Dyam_Create_List(V(4),R(0))
	move_ret R(0)
	call_c   Dyam_Create_Tupple(8,9,V(17))
	move_ret R(1)
	call_c   Dyam_Create_List(V(5),R(1))
	move_ret R(1)
	call_c   build_ref_1226()
	call_c   Dyam_Create_Binary(&ref[1226],R(0),R(1))
	move_ret ref[845]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_152
	ret_reg &seed[152]
	call_c   build_ref_0()
	call_c   build_ref_847()
	call_c   Dyam_Seed_Start(&ref[0],&ref[847],&ref[847],fun11,1)
	call_c   build_ref_848()
	call_c   Dyam_Seed_Add_Comp(&ref[848],&ref[847],0)
	call_c   Dyam_Seed_End()
	move_ret seed[152]
	c_ret

;; TERM 848: '*RITEM*'(_A, return([_E,_F,_G,_H,_I,_J,_K], _L)) :> '$$HOLE$$'
c_code local build_ref_848
	ret_reg &ref[848]
	call_c   build_ref_847()
	call_c   Dyam_Create_Binary(I(9),&ref[847],I(7))
	move_ret ref[848]
	c_ret

;; TERM 847: '*RITEM*'(_A, return([_E,_F,_G,_H,_I,_J,_K], _L))
c_code local build_ref_847
	ret_reg &ref[847]
	call_c   build_ref_0()
	call_c   build_ref_846()
	call_c   Dyam_Create_Binary(&ref[0],V(0),&ref[846])
	move_ret ref[847]
	c_ret

;; TERM 846: return([_E,_F,_G,_H,_I,_J,_K], _L)
c_code local build_ref_846
	ret_reg &ref[846]
	call_c   build_ref_1139()
	call_c   build_ref_850()
	call_c   Dyam_Create_Binary(&ref[1139],&ref[850],V(11))
	move_ret ref[846]
	c_ret

;; TERM 850: [_E,_F,_G,_H,_I,_J,_K]
c_code local build_ref_850
	ret_reg &ref[850]
	call_c   Dyam_Create_Tupple(4,10,I(0))
	move_ret ref[850]
	c_ret

long local pool_fun362[2]=[1,build_seed_152]

long local pool_fun365[5]=[65539,build_ref_851,build_ref_852,build_ref_845,pool_fun362]

pl_code local fun365
	call_c   Dyam_Remove_Choice()
	call_c   DYAM_evpred_univ(V(6),&ref[851])
	fail_ret
	call_c   DYAM_evpred_univ(V(7),&ref[852])
	fail_ret
	call_c   Dyam_Unify(V(11),&ref[845])
	fail_ret
fun362:
	call_c   Dyam_Deallocate()
	pl_jump  fun19(&seed[152],2)


long local pool_fun366[4]=[131073,build_ref_850,pool_fun365,pool_fun362]

pl_code local fun366
	call_c   Dyam_Update_Choice(fun365)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_gt(V(2),N(15))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(11),&ref[850])
	fail_ret
	pl_jump  fun362()

;; TERM 849: tag_features(_N, _G, _H)
c_code local build_ref_849
	ret_reg &ref[849]
	call_c   build_ref_1149()
	call_c   Dyam_Term_Start(&ref[1149],3)
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[849]
	c_ret

;; TERM 843: [_O|_P]
c_code local build_ref_843
	ret_reg &ref[843]
	call_c   Dyam_Create_List(V(14),V(15))
	move_ret ref[843]
	c_ret

;; TERM 844: [_Q|_R]
c_code local build_ref_844
	ret_reg &ref[844]
	call_c   Dyam_Create_List(V(16),V(17))
	move_ret ref[844]
	c_ret

long local pool_fun363[5]=[65539,build_ref_843,build_ref_844,build_ref_845,pool_fun362]

long local pool_fun364[3]=[65537,build_ref_849,pool_fun363]

pl_code local fun364
	call_c   Dyam_Remove_Choice()
	pl_call  Object_1(&ref[849])
fun363:
	call_c   Dyam_Cut()
	call_c   DYAM_evpred_univ(V(6),&ref[843])
	fail_ret
	call_c   DYAM_evpred_univ(V(7),&ref[844])
	fail_ret
	call_c   Dyam_Unify(V(11),&ref[845])
	fail_ret
	pl_jump  fun362()


;; TERM 842: tag_features_mode(_M, _D, _G, _H)
c_code local build_ref_842
	ret_reg &ref[842]
	call_c   build_ref_1153()
	call_c   Dyam_Term_Start(&ref[1153],4)
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[842]
	c_ret

long local pool_fun367[6]=[196610,build_ref_841,build_ref_842,pool_fun366,pool_fun364,pool_fun363]

pl_code local fun367
	call_c   Dyam_Pool(pool_fun367)
	call_c   Dyam_Unify_Item(&ref[841])
	fail_ret
	call_c   DYAM_evpred_functor(V(6),V(1),V(2))
	fail_ret
	call_c   DYAM_evpred_functor(V(7),V(1),V(2))
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun366)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Choice(fun364)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[842])
	call_c   Dyam_Cut()
	pl_jump  fun363()

;; TERM 840: '*FIRST*'('call_decompose_args/3'(callret_tig_adj((_B / _C), _D))) :> []
c_code local build_ref_840
	ret_reg &ref[840]
	call_c   build_ref_839()
	call_c   Dyam_Create_Binary(I(9),&ref[839],I(0))
	move_ret ref[840]
	c_ret

;; TERM 839: '*FIRST*'('call_decompose_args/3'(callret_tig_adj((_B / _C), _D)))
c_code local build_ref_839
	ret_reg &ref[839]
	call_c   build_ref_93()
	call_c   build_ref_838()
	call_c   Dyam_Create_Unary(&ref[93],&ref[838])
	move_ret ref[839]
	c_ret

c_code local build_seed_36
	ret_reg &seed[36]
	call_c   build_ref_93()
	call_c   build_ref_855()
	call_c   Dyam_Seed_Start(&ref[93],&ref[855],I(0),fun21,1)
	call_c   build_ref_856()
	call_c   Dyam_Seed_Add_Comp(&ref[856],fun372,0)
	call_c   Dyam_Seed_End()
	move_ret seed[36]
	c_ret

;; TERM 856: '*CITEM*'('call_decompose_args/3'(tig_adj((_B / _C), _D)), _A)
c_code local build_ref_856
	ret_reg &ref[856]
	call_c   build_ref_97()
	call_c   build_ref_853()
	call_c   Dyam_Create_Binary(&ref[97],&ref[853],V(0))
	move_ret ref[856]
	c_ret

;; TERM 853: 'call_decompose_args/3'(tig_adj((_B / _C), _D))
c_code local build_ref_853
	ret_reg &ref[853]
	call_c   build_ref_1248()
	call_c   build_ref_660()
	call_c   Dyam_Create_Unary(&ref[1248],&ref[660])
	move_ret ref[853]
	c_ret

;; TERM 863: [_T|_U]
c_code local build_ref_863
	ret_reg &ref[863]
	call_c   Dyam_Create_List(V(19),V(20))
	move_ret ref[863]
	c_ret

c_code local build_seed_154
	ret_reg &seed[154]
	call_c   build_ref_55()
	call_c   build_ref_865()
	call_c   Dyam_Seed_Start(&ref[55],&ref[865],I(0),fun11,1)
	call_c   build_ref_866()
	call_c   Dyam_Seed_Add_Comp(&ref[866],&ref[865],0)
	call_c   Dyam_Seed_End()
	move_ret seed[154]
	c_ret

;; TERM 866: '*GUARD*'(append(_S, _U, _V)) :> '$$HOLE$$'
c_code local build_ref_866
	ret_reg &ref[866]
	call_c   build_ref_865()
	call_c   Dyam_Create_Binary(I(9),&ref[865],I(7))
	move_ret ref[866]
	c_ret

;; TERM 865: '*GUARD*'(append(_S, _U, _V))
c_code local build_ref_865
	ret_reg &ref[865]
	call_c   build_ref_55()
	call_c   build_ref_864()
	call_c   Dyam_Create_Unary(&ref[55],&ref[864])
	move_ret ref[865]
	c_ret

;; TERM 864: append(_S, _U, _V)
c_code local build_ref_864
	ret_reg &ref[864]
	call_c   build_ref_1167()
	call_c   Dyam_Term_Start(&ref[1167],3)
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_Arg(V(21))
	call_c   Dyam_Term_End()
	move_ret ref[864]
	c_ret

;; TERM 867: [_E,_F,_I,_J,_K|_V]
c_code local build_ref_867
	ret_reg &ref[867]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Tupple(8,10,V(21))
	move_ret R(0)
	call_c   Dyam_Create_Tupple(4,5,R(0))
	move_ret ref[867]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun370[6]=[65540,build_ref_233,build_ref_863,build_seed_154,build_ref_867,pool_fun362]

pl_code local fun370
	call_c   Dyam_Remove_Choice()
	call_c   DYAM_evpred_univ(V(6),&ref[233])
	fail_ret
	call_c   DYAM_evpred_univ(V(7),&ref[863])
	fail_ret
	pl_call  fun38(&seed[154],1)
	call_c   Dyam_Unify(V(11),&ref[867])
	fail_ret
	pl_jump  fun362()

long local pool_fun371[4]=[131073,build_ref_850,pool_fun370,pool_fun362]

pl_code local fun371
	call_c   Dyam_Update_Choice(fun370)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_gt(V(2),N(15))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(11),&ref[850])
	fail_ret
	pl_jump  fun362()

;; TERM 857: [_B|_O]
c_code local build_ref_857
	ret_reg &ref[857]
	call_c   Dyam_Create_List(V(1),V(14))
	move_ret ref[857]
	c_ret

;; TERM 858: [_B|_P]
c_code local build_ref_858
	ret_reg &ref[858]
	call_c   Dyam_Create_List(V(1),V(15))
	move_ret ref[858]
	c_ret

c_code local build_seed_153
	ret_reg &seed[153]
	call_c   build_ref_55()
	call_c   build_ref_860()
	call_c   Dyam_Seed_Start(&ref[55],&ref[860],I(0),fun11,1)
	call_c   build_ref_861()
	call_c   Dyam_Seed_Add_Comp(&ref[861],&ref[860],0)
	call_c   Dyam_Seed_End()
	move_ret seed[153]
	c_ret

;; TERM 861: '*GUARD*'(optimize_topbot_args(_O, _P, [_I,_J,_K], _Q)) :> '$$HOLE$$'
c_code local build_ref_861
	ret_reg &ref[861]
	call_c   build_ref_860()
	call_c   Dyam_Create_Binary(I(9),&ref[860],I(7))
	move_ret ref[861]
	c_ret

;; TERM 860: '*GUARD*'(optimize_topbot_args(_O, _P, [_I,_J,_K], _Q))
c_code local build_ref_860
	ret_reg &ref[860]
	call_c   build_ref_55()
	call_c   build_ref_859()
	call_c   Dyam_Create_Unary(&ref[55],&ref[859])
	move_ret ref[860]
	c_ret

;; TERM 859: optimize_topbot_args(_O, _P, [_I,_J,_K], _Q)
c_code local build_ref_859
	ret_reg &ref[859]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Tupple(8,10,I(0))
	move_ret R(0)
	call_c   build_ref_1250()
	call_c   Dyam_Term_Start(&ref[1250],4)
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_End()
	move_ret ref[859]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1250: optimize_topbot_args
c_code local build_ref_1250
	ret_reg &ref[1250]
	call_c   Dyam_Create_Atom("optimize_topbot_args")
	move_ret ref[1250]
	c_ret

;; TERM 862: [_E,_F|_Q]
c_code local build_ref_862
	ret_reg &ref[862]
	call_c   Dyam_Create_Tupple(4,5,V(16))
	move_ret ref[862]
	c_ret

long local pool_fun368[6]=[65540,build_ref_857,build_ref_858,build_seed_153,build_ref_862,pool_fun362]

long local pool_fun369[3]=[65537,build_ref_849,pool_fun368]

pl_code local fun369
	call_c   Dyam_Remove_Choice()
	pl_call  Object_1(&ref[849])
fun368:
	call_c   Dyam_Cut()
	call_c   DYAM_evpred_univ(V(6),&ref[857])
	fail_ret
	call_c   DYAM_evpred_univ(V(7),&ref[858])
	fail_ret
	pl_call  fun38(&seed[153],1)
	call_c   Dyam_Unify(V(11),&ref[862])
	fail_ret
	pl_jump  fun362()


long local pool_fun372[6]=[196610,build_ref_856,build_ref_842,pool_fun371,pool_fun369,pool_fun368]

pl_code local fun372
	call_c   Dyam_Pool(pool_fun372)
	call_c   Dyam_Unify_Item(&ref[856])
	fail_ret
	call_c   DYAM_evpred_functor(V(6),V(1),V(2))
	fail_ret
	call_c   DYAM_evpred_functor(V(7),V(1),V(2))
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun371)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Choice(fun369)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[842])
	call_c   Dyam_Cut()
	pl_jump  fun368()

;; TERM 855: '*FIRST*'('call_decompose_args/3'(tig_adj((_B / _C), _D))) :> []
c_code local build_ref_855
	ret_reg &ref[855]
	call_c   build_ref_854()
	call_c   Dyam_Create_Binary(I(9),&ref[854],I(0))
	move_ret ref[855]
	c_ret

;; TERM 854: '*FIRST*'('call_decompose_args/3'(tig_adj((_B / _C), _D)))
c_code local build_ref_854
	ret_reg &ref[854]
	call_c   build_ref_93()
	call_c   build_ref_853()
	call_c   Dyam_Create_Unary(&ref[93],&ref[853])
	move_ret ref[854]
	c_ret

c_code local build_seed_39
	ret_reg &seed[39]
	call_c   build_ref_54()
	call_c   build_ref_878()
	call_c   Dyam_Seed_Start(&ref[54],&ref[878],I(0),fun0,1)
	call_c   build_ref_877()
	call_c   Dyam_Seed_Add_Comp(&ref[877],fun414,0)
	call_c   Dyam_Seed_End()
	move_ret seed[39]
	c_ret

;; TERM 877: '*GUARD*'(tag_compile_node(_B, tag_node{id=> _C, label=> _D, children=> _E, kind=> foot, adj=> _F, spine=> _G, top=> _H, bot=> _I, token=> _J, adjleft=> _K, adjright=> _L, adjwrap=> _M}, _N, _O, _P, _Q, _R, _S, _T))
c_code local build_ref_877
	ret_reg &ref[877]
	call_c   build_ref_55()
	call_c   build_ref_876()
	call_c   Dyam_Create_Unary(&ref[55],&ref[876])
	move_ret ref[877]
	c_ret

;; TERM 876: tag_compile_node(_B, tag_node{id=> _C, label=> _D, children=> _E, kind=> foot, adj=> _F, spine=> _G, top=> _H, bot=> _I, token=> _J, adjleft=> _K, adjright=> _L, adjwrap=> _M}, _N, _O, _P, _Q, _R, _S, _T)
c_code local build_ref_876
	ret_reg &ref[876]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1172()
	call_c   build_ref_992()
	call_c   Dyam_Term_Start(&ref[1172],12)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(&ref[992])
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_1251()
	call_c   Dyam_Term_Start(&ref[1251],9)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_End()
	move_ret ref[876]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1251: tag_compile_node
c_code local build_ref_1251
	ret_reg &ref[1251]
	call_c   Dyam_Create_Atom("tag_compile_node")
	move_ret ref[1251]
	c_ret

;; TERM 992: foot
c_code local build_ref_992
	ret_reg &ref[992]
	call_c   Dyam_Create_Atom("foot")
	move_ret ref[992]
	c_ret

;; TERM 879: tig(_B, _U)
c_code local build_ref_879
	ret_reg &ref[879]
	call_c   build_ref_1154()
	call_c   Dyam_Create_Binary(&ref[1154],V(1),V(20))
	move_ret ref[879]
	c_ret

;; TERM 902: _W * right_tig(_I, _I1, _J1)
c_code local build_ref_902
	ret_reg &ref[902]
	call_c   build_ref_393()
	call_c   build_ref_901()
	call_c   Dyam_Create_Binary(&ref[393],V(22),&ref[901])
	move_ret ref[902]
	c_ret

;; TERM 901: right_tig(_I, _I1, _J1)
c_code local build_ref_901
	ret_reg &ref[901]
	call_c   build_ref_1237()
	call_c   Dyam_Term_Start(&ref[1237],3)
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(34))
	call_c   Dyam_Term_Arg(V(35))
	call_c   Dyam_Term_End()
	move_ret ref[901]
	c_ret

;; TERM 923: unify(_N, _O) :> _L1
c_code local build_ref_923
	ret_reg &ref[923]
	call_c   build_ref_896()
	call_c   Dyam_Create_Binary(I(9),&ref[896],V(37))
	move_ret ref[923]
	c_ret

;; TERM 896: unify(_N, _O)
c_code local build_ref_896
	ret_reg &ref[896]
	call_c   build_ref_1252()
	call_c   Dyam_Create_Binary(&ref[1252],V(13),V(14))
	move_ret ref[896]
	c_ret

;; TERM 1252: unify
c_code local build_ref_1252
	ret_reg &ref[1252]
	call_c   Dyam_Create_Atom("unify")
	move_ret ref[1252]
	c_ret

;; TERM 922: unify(_N, _I1) :> _C1
c_code local build_ref_922
	ret_reg &ref[922]
	call_c   build_ref_921()
	call_c   Dyam_Create_Binary(I(9),&ref[921],V(28))
	move_ret ref[922]
	c_ret

;; TERM 921: unify(_N, _I1)
c_code local build_ref_921
	ret_reg &ref[921]
	call_c   build_ref_1252()
	call_c   Dyam_Create_Binary(&ref[1252],V(13),V(34))
	move_ret ref[921]
	c_ret

;; TERM 904: '*SAFIRST*'(_J1) :> _D1
c_code local build_ref_904
	ret_reg &ref[904]
	call_c   build_ref_903()
	call_c   Dyam_Create_Binary(I(9),&ref[903],V(29))
	move_ret ref[904]
	c_ret

;; TERM 903: '*SAFIRST*'(_J1)
c_code local build_ref_903
	ret_reg &ref[903]
	call_c   build_ref_1246()
	call_c   Dyam_Create_Unary(&ref[1246],V(35))
	move_ret ref[903]
	c_ret

pl_code local fun395
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(36),V(37))
	fail_ret
	pl_jump  fun85()

;; TERM 907: tagfilter((_B ^ _N ^ _O ^ _D ^ _I ^ _T1))
c_code local build_ref_907
	ret_reg &ref[907]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1203()
	call_c   Dyam_Create_Binary(&ref[1203],V(8),V(45))
	move_ret R(0)
	call_c   Dyam_Create_Binary(&ref[1203],V(3),R(0))
	move_ret R(0)
	call_c   Dyam_Create_Binary(&ref[1203],V(14),R(0))
	move_ret R(0)
	call_c   Dyam_Create_Binary(&ref[1203],V(13),R(0))
	move_ret R(0)
	call_c   Dyam_Create_Binary(&ref[1203],V(1),R(0))
	move_ret R(0)
	call_c   build_ref_1204()
	call_c   Dyam_Create_Unary(&ref[1204],R(0))
	move_ret ref[907]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 908: lctag(_B, _U1, _V1, _W1)
c_code local build_ref_908
	ret_reg &ref[908]
	call_c   build_ref_1120()
	call_c   Dyam_Term_Start(&ref[1120],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(46))
	call_c   Dyam_Term_Arg(V(47))
	call_c   Dyam_Term_Arg(V(48))
	call_c   Dyam_Term_End()
	move_ret ref[908]
	c_ret

c_code local build_seed_156
	ret_reg &seed[156]
	call_c   build_ref_55()
	call_c   build_ref_910()
	call_c   Dyam_Seed_Start(&ref[55],&ref[910],I(0),fun11,1)
	call_c   build_ref_911()
	call_c   Dyam_Seed_Add_Comp(&ref[911],&ref[910],0)
	call_c   Dyam_Seed_End()
	move_ret seed[156]
	c_ret

;; TERM 911: '*GUARD*'(body_to_lpda(_B, _T1, _K1, _L1, _R)) :> '$$HOLE$$'
c_code local build_ref_911
	ret_reg &ref[911]
	call_c   build_ref_910()
	call_c   Dyam_Create_Binary(I(9),&ref[910],I(7))
	move_ret ref[911]
	c_ret

;; TERM 910: '*GUARD*'(body_to_lpda(_B, _T1, _K1, _L1, _R))
c_code local build_ref_910
	ret_reg &ref[910]
	call_c   build_ref_55()
	call_c   build_ref_909()
	call_c   Dyam_Create_Unary(&ref[55],&ref[909])
	move_ret ref[910]
	c_ret

;; TERM 909: body_to_lpda(_B, _T1, _K1, _L1, _R)
c_code local build_ref_909
	ret_reg &ref[909]
	call_c   build_ref_1219()
	call_c   Dyam_Term_Start(&ref[1219],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(45))
	call_c   Dyam_Term_Arg(V(36))
	call_c   Dyam_Term_Arg(V(37))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_End()
	move_ret ref[909]
	c_ret

long local pool_fun396[4]=[3,build_ref_907,build_ref_908,build_seed_156]

pl_code local fun401
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(36),V(15))
	fail_ret
fun396:
	call_c   Dyam_Choice(fun395)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[907])
	pl_call  Object_1(&ref[908])
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(45))
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(49), R(4)
	move     S(5), R(5)
	pl_call  pred_tag_numbervars_3()
	pl_call  fun38(&seed[156],1)
	pl_jump  fun85()


c_code local build_seed_157
	ret_reg &seed[157]
	call_c   build_ref_55()
	call_c   build_ref_919()
	call_c   Dyam_Seed_Start(&ref[55],&ref[919],I(0),fun11,1)
	call_c   build_ref_920()
	call_c   Dyam_Seed_Add_Comp(&ref[920],&ref[919],0)
	call_c   Dyam_Seed_End()
	move_ret seed[157]
	c_ret

;; TERM 920: '*GUARD*'(body_to_lpda(_B, format('Selecting cat=~w left=~w tree ~w\n', [_I,_N,_B]), _P, _K1, _R)) :> '$$HOLE$$'
c_code local build_ref_920
	ret_reg &ref[920]
	call_c   build_ref_919()
	call_c   Dyam_Create_Binary(I(9),&ref[919],I(7))
	move_ret ref[920]
	c_ret

;; TERM 919: '*GUARD*'(body_to_lpda(_B, format('Selecting cat=~w left=~w tree ~w\n', [_I,_N,_B]), _P, _K1, _R))
c_code local build_ref_919
	ret_reg &ref[919]
	call_c   build_ref_55()
	call_c   build_ref_918()
	call_c   Dyam_Create_Unary(&ref[55],&ref[918])
	move_ret ref[919]
	c_ret

;; TERM 918: body_to_lpda(_B, format('Selecting cat=~w left=~w tree ~w\n', [_I,_N,_B]), _P, _K1, _R)
c_code local build_ref_918
	ret_reg &ref[918]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_824()
	call_c   Dyam_Create_List(V(13),&ref[824])
	move_ret R(0)
	call_c   Dyam_Create_List(V(8),R(0))
	move_ret R(0)
	call_c   build_ref_1232()
	call_c   build_ref_1233()
	call_c   Dyam_Create_Binary(&ref[1232],&ref[1233],R(0))
	move_ret R(0)
	call_c   build_ref_1219()
	call_c   Dyam_Term_Start(&ref[1219],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(36))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_End()
	move_ret ref[918]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun402[5]=[131074,build_ref_787,build_seed_157,pool_fun396,pool_fun396]

pl_code local fun402
	call_c   Dyam_Update_Choice(fun401)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[787])
	call_c   Dyam_Cut()
	pl_call  fun38(&seed[157],1)
	pl_jump  fun396()

;; TERM 928: '$CLOSURE'('$fun'(406, 0, 1128213200), '$TUPPLE'(35144395922616))
c_code local build_ref_928
	ret_reg &ref[928]
	call_c   build_ref_927()
	call_c   Dyam_Closure_Aux(fun406,&ref[927])
	move_ret ref[928]
	c_ret

;; TERM 925: '$CLOSURE'('$fun'(397, 0, 1128140172), '$TUPPLE'(35144395922632))
c_code local build_ref_925
	ret_reg &ref[925]
	call_c   build_ref_924()
	call_c   Dyam_Closure_Aux(fun397,&ref[924])
	move_ret ref[925]
	c_ret

;; TERM 906: '*GUIDE*'(_R1, _P1, _P)
c_code local build_ref_906
	ret_reg &ref[906]
	call_c   build_ref_1236()
	call_c   Dyam_Term_Start(&ref[1236],3)
	call_c   Dyam_Term_Arg(V(43))
	call_c   Dyam_Term_Arg(V(41))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_End()
	move_ret ref[906]
	c_ret

long local pool_fun397[3]=[65537,build_ref_906,pool_fun396]

pl_code local fun397
	call_c   Dyam_Pool(pool_fun397)
	call_c   Dyam_Allocate(0)
	move     V(23), R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(42), R(4)
	move     S(5), R(5)
	pl_call  pred_tag_numbervars_3()
	call_c   Dyam_Reg_Load(0,V(20))
	call_c   Dyam_Reg_Load(2,V(40))
	call_c   Dyam_Reg_Load(4,V(40))
	call_c   Dyam_Reg_Load(6,V(13))
	call_c   Dyam_Reg_Load(8,V(23))
	move     V(43), R(10)
	move     S(5), R(11)
	move     V(44), R(12)
	move     S(5), R(13)
	pl_call  pred_make_tig_callret_7()
	call_c   Dyam_Unify(V(36),&ref[906])
	fail_ret
	pl_jump  fun396()

;; TERM 924: '$TUPPLE'(35144395922632)
c_code local build_ref_924
	ret_reg &ref[924]
	call_c   Dyam_Start_Tupple(0,168880384)
	call_c   Dyam_Almost_End_Tupple(29,1245184)
	move_ret ref[924]
	c_ret

long local pool_fun406[2]=[1,build_ref_925]

pl_code local fun406
	call_c   Dyam_Pool(pool_fun406)
	call_c   Dyam_Allocate(0)
	move     &ref[925], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Load(4,V(1))
	call_c   Dyam_Reg_Load(6,V(20))
	move     V(41), R(8)
	move     S(5), R(9)
	call_c   Dyam_Reg_Deallocate(5)
	pl_jump  fun324()

;; TERM 927: '$TUPPLE'(35144395922616)
c_code local build_ref_927
	ret_reg &ref[927]
	call_c   Dyam_Start_Tupple(0,168880384)
	call_c   Dyam_Almost_End_Tupple(29,1179648)
	move_ret ref[927]
	c_ret

long local pool_fun407[2]=[1,build_ref_928]

pl_code local fun408
	call_c   Dyam_Remove_Choice()
fun407:
	call_c   Dyam_Cut()
	move     &ref[928], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Load(4,V(3))
	move     V(40), R(6)
	move     S(5), R(7)
	call_c   Dyam_Reg_Deallocate(4)
	pl_jump  fun321()


;; TERM 905: guide(_B, _M1, _N1)
c_code local build_ref_905
	ret_reg &ref[905]
	call_c   build_ref_1234()
	call_c   Dyam_Term_Start(&ref[1234],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(38))
	call_c   Dyam_Term_Arg(V(39))
	call_c   Dyam_Term_End()
	move_ret ref[905]
	c_ret

long local pool_fun409[6]=[131075,build_ref_904,build_ref_759,build_ref_905,pool_fun402,pool_fun407]

long local pool_fun410[3]=[65537,build_ref_922,pool_fun409]

pl_code local fun410
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(29),&ref[922])
	fail_ret
fun409:
	call_c   Dyam_Unify(V(16),&ref[904])
	fail_ret
	call_c   Dyam_Choice(fun402)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[759])
	call_c   Dyam_Choice(fun408)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[905])
	call_c   Dyam_Cut()
	pl_fail


long local pool_fun411[3]=[131072,pool_fun410,pool_fun409]

long local pool_fun412[3]=[65537,build_ref_923,pool_fun411]

pl_code local fun412
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(28),&ref[923])
	fail_ret
fun411:
	call_c   Dyam_Choice(fun410)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(13),V(34))
	fail_ret
	call_c   Dyam_Unify(V(29),V(28))
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun409()


;; TERM 917: '$CLOSURE'('$fun'(398, 0, 1128153736), '$TUPPLE'(35144395921792))
c_code local build_ref_917
	ret_reg &ref[917]
	call_c   build_ref_916()
	call_c   Dyam_Closure_Aux(fun398,&ref[916])
	move_ret ref[917]
	c_ret

;; TERM 914: '$CLOSURE'('$fun'(397, 0, 1128140172), '$TUPPLE'(35144395921808))
c_code local build_ref_914
	ret_reg &ref[914]
	call_c   build_ref_913()
	call_c   Dyam_Closure_Aux(fun397,&ref[913])
	move_ret ref[914]
	c_ret

;; TERM 913: '$TUPPLE'(35144395921808)
c_code local build_ref_913
	ret_reg &ref[913]
	call_c   Dyam_Start_Tupple(0,168880384)
	call_c   Dyam_Almost_End_Tupple(29,2293760)
	move_ret ref[913]
	c_ret

long local pool_fun398[2]=[1,build_ref_914]

pl_code local fun398
	call_c   Dyam_Pool(pool_fun398)
	call_c   Dyam_Allocate(0)
	move     &ref[914], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Load(4,V(1))
	call_c   Dyam_Reg_Load(6,V(20))
	move     V(41), R(8)
	move     S(5), R(9)
	call_c   Dyam_Reg_Deallocate(5)
	pl_jump  fun324()

;; TERM 916: '$TUPPLE'(35144395921792)
c_code local build_ref_916
	ret_reg &ref[916]
	call_c   Dyam_Start_Tupple(0,168880384)
	call_c   Dyam_Almost_End_Tupple(29,2228224)
	move_ret ref[916]
	c_ret

long local pool_fun399[2]=[1,build_ref_917]

pl_code local fun400
	call_c   Dyam_Remove_Choice()
fun399:
	call_c   Dyam_Cut()
	move     &ref[917], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Load(4,V(3))
	move     V(40), R(6)
	move     S(5), R(7)
	call_c   Dyam_Reg_Deallocate(4)
	pl_jump  fun321()


long local pool_fun403[6]=[131075,build_ref_904,build_ref_759,build_ref_905,pool_fun402,pool_fun399]

long local pool_fun404[3]=[65537,build_ref_922,pool_fun403]

pl_code local fun404
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(29),&ref[922])
	fail_ret
fun403:
	call_c   Dyam_Unify(V(16),&ref[904])
	fail_ret
	call_c   Dyam_Choice(fun402)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[759])
	call_c   Dyam_Choice(fun400)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[905])
	call_c   Dyam_Cut()
	pl_fail


long local pool_fun405[3]=[131072,pool_fun404,pool_fun403]

long local pool_fun413[4]=[131073,build_ref_902,pool_fun412,pool_fun405]

pl_code local fun413
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(18),&ref[902])
	fail_ret
	call_c   Dyam_Choice(fun412)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(13),V(14))
	fail_ret
	call_c   Dyam_Unify(V(28),V(36))
	fail_ret
	call_c   Dyam_Cut()
fun405:
	call_c   Dyam_Choice(fun404)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(13),V(34))
	fail_ret
	call_c   Dyam_Unify(V(29),V(28))
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun403()


;; TERM 899: 'Foot in left auxtree=~w cont=~w\n'
c_code local build_ref_899
	ret_reg &ref[899]
	call_c   Dyam_Create_Atom("Foot in left auxtree=~w cont=~w\n")
	move_ret ref[899]
	c_ret

;; TERM 900: [_B,_P]
c_code local build_ref_900
	ret_reg &ref[900]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(15),I(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(1),R(0))
	move_ret ref[900]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 881: _W * left_tig(_I, _X, _Y)
c_code local build_ref_881
	ret_reg &ref[881]
	call_c   build_ref_393()
	call_c   build_ref_880()
	call_c   Dyam_Create_Binary(&ref[393],V(22),&ref[880])
	move_ret ref[881]
	c_ret

;; TERM 880: left_tig(_I, _X, _Y)
c_code local build_ref_880
	ret_reg &ref[880]
	call_c   build_ref_1245()
	call_c   Dyam_Term_Start(&ref[1245],3)
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(23))
	call_c   Dyam_Term_Arg(V(24))
	call_c   Dyam_Term_End()
	move_ret ref[880]
	c_ret

;; TERM 898: '*SA-LAST*'(_Y)
c_code local build_ref_898
	ret_reg &ref[898]
	call_c   build_ref_1238()
	call_c   Dyam_Create_Unary(&ref[1238],V(24))
	move_ret ref[898]
	c_ret

;; TERM 897: unify(_N, _O) :> _B1
c_code local build_ref_897
	ret_reg &ref[897]
	call_c   build_ref_896()
	call_c   Dyam_Create_Binary(I(9),&ref[896],V(27))
	move_ret ref[897]
	c_ret

;; TERM 895: unify(_O, _X) :> _C1
c_code local build_ref_895
	ret_reg &ref[895]
	call_c   build_ref_894()
	call_c   Dyam_Create_Binary(I(9),&ref[894],V(28))
	move_ret ref[895]
	c_ret

;; TERM 894: unify(_O, _X)
c_code local build_ref_894
	ret_reg &ref[894]
	call_c   build_ref_1252()
	call_c   Dyam_Create_Binary(&ref[1252],V(14),V(23))
	move_ret ref[894]
	c_ret

pl_code local fun386
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(16),V(29))
	fail_ret
	pl_jump  fun85()

;; TERM 893: '$CLOSURE'('$fun'(385, 0, 1128057976), '$TUPPLE'(35144395921152))
c_code local build_ref_893
	ret_reg &ref[893]
	call_c   build_ref_892()
	call_c   Dyam_Closure_Aux(fun385,&ref[892])
	move_ret ref[893]
	c_ret

;; TERM 889: '$CLOSURE'('$fun'(384, 0, 1128058708), '$TUPPLE'(35144395921240))
c_code local build_ref_889
	ret_reg &ref[889]
	call_c   build_ref_888()
	call_c   Dyam_Closure_Aux(fun384,&ref[888])
	move_ret ref[889]
	c_ret

c_code local build_seed_155
	ret_reg &seed[155]
	call_c   build_ref_55()
	call_c   build_ref_885()
	call_c   Dyam_Seed_Start(&ref[55],&ref[885],I(0),fun11,1)
	call_c   build_ref_886()
	call_c   Dyam_Seed_Add_Comp(&ref[886],&ref[885],0)
	call_c   Dyam_Seed_End()
	move_ret seed[155]
	c_ret

;; TERM 886: '*GUARD*'(body_to_lpda(_B, verbose!struct(_B, _E1), _D1, _Q, _R)) :> '$$HOLE$$'
c_code local build_ref_886
	ret_reg &ref[886]
	call_c   build_ref_885()
	call_c   Dyam_Create_Binary(I(9),&ref[885],I(7))
	move_ret ref[886]
	c_ret

;; TERM 885: '*GUARD*'(body_to_lpda(_B, verbose!struct(_B, _E1), _D1, _Q, _R))
c_code local build_ref_885
	ret_reg &ref[885]
	call_c   build_ref_55()
	call_c   build_ref_884()
	call_c   Dyam_Create_Unary(&ref[55],&ref[884])
	move_ret ref[885]
	c_ret

;; TERM 884: body_to_lpda(_B, verbose!struct(_B, _E1), _D1, _Q, _R)
c_code local build_ref_884
	ret_reg &ref[884]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1239()
	call_c   Dyam_Create_Binary(&ref[1239],V(1),V(30))
	move_ret R(0)
	call_c   build_ref_1219()
	call_c   Dyam_Term_Start(&ref[1219],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(29))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_End()
	move_ret ref[884]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun384[2]=[1,build_seed_155]

pl_code local fun384
	call_c   Dyam_Pool(pool_fun384)
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(30))
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(33), R(4)
	move     S(5), R(5)
	pl_call  pred_tag_numbervars_3()
	pl_call  fun38(&seed[155],1)
	pl_jump  fun85()

;; TERM 888: '$TUPPLE'(35144395921240)
c_code local build_ref_888
	ret_reg &ref[888]
	call_c   Dyam_Start_Tupple(0,134223872)
	call_c   Dyam_Almost_End_Tupple(29,402653184)
	move_ret ref[888]
	c_ret

;; TERM 890: tag_anchor{family=> _E1, coanchors=> _F1, equations=> _G1}
c_code local build_ref_890
	ret_reg &ref[890]
	call_c   build_ref_1241()
	call_c   Dyam_Term_Start(&ref[1241],3)
	call_c   Dyam_Term_Arg(V(30))
	call_c   Dyam_Term_Arg(V(31))
	call_c   Dyam_Term_Arg(V(32))
	call_c   Dyam_Term_End()
	move_ret ref[890]
	c_ret

long local pool_fun385[3]=[2,build_ref_889,build_ref_890]

pl_code local fun385
	call_c   Dyam_Pool(pool_fun385)
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Cut()
	move     &ref[889], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Load(4,V(1))
	call_c   Dyam_Reg_Load(6,V(22))
	move     &ref[890], R(8)
	move     S(5), R(9)
	call_c   Dyam_Reg_Deallocate(5)
	pl_jump  fun339()

;; TERM 892: '$TUPPLE'(35144395921152)
c_code local build_ref_892
	ret_reg &ref[892]
	call_c   Dyam_Start_Tupple(0,134223936)
	call_c   Dyam_Almost_End_Tupple(29,268435456)
	move_ret ref[892]
	c_ret

long local pool_fun387[2]=[1,build_ref_893]

long local pool_fun388[3]=[65537,build_ref_895,pool_fun387]

pl_code local fun388
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(29),&ref[895])
	fail_ret
fun387:
	call_c   Dyam_Choice(fun386)
	call_c   Dyam_Set_Cut()
	move     &ref[893], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Load(4,V(1))
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  fun336()


long local pool_fun389[3]=[131072,pool_fun388,pool_fun387]

long local pool_fun390[3]=[65537,build_ref_897,pool_fun389]

pl_code local fun390
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(28),&ref[897])
	fail_ret
fun389:
	call_c   Dyam_Choice(fun388)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(14),V(23))
	fail_ret
	call_c   Dyam_Unify(V(29),V(28))
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun387()


long local pool_fun391[3]=[131072,pool_fun390,pool_fun389]

long local pool_fun392[3]=[65537,build_ref_898,pool_fun391]

pl_code local fun392
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(27),&ref[898])
	fail_ret
fun391:
	call_c   Dyam_Choice(fun390)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(13),V(14))
	fail_ret
	call_c   Dyam_Unify(V(28),V(27))
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun389()


;; TERM 882: guide(_B, _Z, _A1)
c_code local build_ref_882
	ret_reg &ref[882]
	call_c   build_ref_1234()
	call_c   Dyam_Term_Start(&ref[1234],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(25))
	call_c   Dyam_Term_Arg(V(26))
	call_c   Dyam_Term_End()
	move_ret ref[882]
	c_ret

;; TERM 883: '*GUIDE-LAST*'(_Y, _B)
c_code local build_ref_883
	ret_reg &ref[883]
	call_c   build_ref_1253()
	call_c   Dyam_Create_Binary(&ref[1253],V(24),V(1))
	move_ret ref[883]
	c_ret

;; TERM 1253: '*GUIDE-LAST*'
c_code local build_ref_1253
	ret_reg &ref[1253]
	call_c   Dyam_Create_Atom("*GUIDE-LAST*")
	move_ret ref[1253]
	c_ret

long local pool_fun393[6]=[131075,build_ref_881,build_ref_882,build_ref_883,pool_fun392,pool_fun391]

long local pool_fun394[4]=[65538,build_ref_899,build_ref_900,pool_fun393]

pl_code local fun394
	call_c   Dyam_Remove_Choice()
	move     &ref[899], R(0)
	move     0, R(1)
	move     &ref[900], R(2)
	move     S(5), R(3)
	pl_call  pred_warning_2()
fun393:
	call_c   Dyam_Unify(V(18),&ref[881])
	fail_ret
	call_c   Dyam_Choice(fun392)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[882])
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(27),&ref[883])
	fail_ret
	pl_jump  fun391()


long local pool_fun414[8]=[196612,build_ref_877,build_ref_879,build_ref_451,build_ref_755,pool_fun413,pool_fun394,pool_fun393]

pl_code local fun414
	call_c   Dyam_Pool(pool_fun414)
	call_c   Dyam_Unify_Item(&ref[877])
	fail_ret
	call_c   Dyam_Allocate(0)
	pl_call  Object_1(&ref[879])
	call_c   Dyam_Reg_Load(0,V(8))
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(21), R(4)
	move     S(5), R(5)
	pl_call  pred_tag_numbervars_3()
	call_c   Dyam_Choice(fun413)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(20),&ref[451])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun394)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(15),&ref[755])
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun393()

;; TERM 878: '*GUARD*'(tag_compile_node(_B, tag_node{id=> _C, label=> _D, children=> _E, kind=> foot, adj=> _F, spine=> _G, top=> _H, bot=> _I, token=> _J, adjleft=> _K, adjright=> _L, adjwrap=> _M}, _N, _O, _P, _Q, _R, _S, _T)) :> []
c_code local build_ref_878
	ret_reg &ref[878]
	call_c   build_ref_877()
	call_c   Dyam_Create_Binary(I(9),&ref[877],I(0))
	move_ret ref[878]
	c_ret

c_code local build_seed_60
	ret_reg &seed[60]
	call_c   build_ref_93()
	call_c   build_ref_963()
	call_c   Dyam_Seed_Start(&ref[93],&ref[963],I(0),fun21,1)
	call_c   build_ref_964()
	call_c   Dyam_Seed_Add_Comp(&ref[964],fun450,0)
	call_c   Dyam_Seed_End()
	move_ret seed[60]
	c_ret

;; TERM 964: '*CITEM*'(not_tig(_B, _C), _A)
c_code local build_ref_964
	ret_reg &ref[964]
	call_c   build_ref_97()
	call_c   build_ref_961()
	call_c   Dyam_Create_Binary(&ref[97],&ref[961],V(0))
	move_ret ref[964]
	c_ret

;; TERM 961: not_tig(_B, _C)
c_code local build_ref_961
	ret_reg &ref[961]
	call_c   build_ref_1254()
	call_c   Dyam_Create_Binary(&ref[1254],V(1),V(2))
	move_ret ref[961]
	c_ret

;; TERM 1254: not_tig
c_code local build_ref_1254
	ret_reg &ref[1254]
	call_c   Dyam_Create_Atom("not_tig")
	move_ret ref[1254]
	c_ret

;; TERM 965: tag_node{id=> _D, label=> _E, children=> _F, kind=> _G, adj=> _H, spine=> _I, top=> _J, bot=> _K, token=> _L, adjleft=> _M, adjright=> _N, adjwrap=> _O}
c_code local build_ref_965
	ret_reg &ref[965]
	call_c   build_ref_1172()
	call_c   Dyam_Term_Start(&ref[1172],12)
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_End()
	move_ret ref[965]
	c_ret

;; TERM 980: '$VAR'(_Q, ['$SET$',adj(no, yes, strict, atmostone, one, sync),62])
c_code local build_ref_980
	ret_reg &ref[980]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1178()
	call_c   build_ref_706()
	call_c   build_ref_319()
	call_c   build_ref_673()
	call_c   build_ref_685()
	call_c   build_ref_686()
	call_c   build_ref_623()
	call_c   Dyam_Term_Start(&ref[1178],6)
	call_c   Dyam_Term_Arg(&ref[706])
	call_c   Dyam_Term_Arg(&ref[319])
	call_c   Dyam_Term_Arg(&ref[673])
	call_c   Dyam_Term_Arg(&ref[685])
	call_c   Dyam_Term_Arg(&ref[686])
	call_c   Dyam_Term_Arg(&ref[623])
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   Dyam_Term_Start(I(8),3)
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(N(62))
	call_c   Dyam_Term_End()
	move_ret ref[980]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 979: '$CLOSURE'('$fun'(443, 0, 1128536616), '$TUPPLE'(35144395662736))
c_code local build_ref_979
	ret_reg &ref[979]
	call_c   build_ref_978()
	call_c   Dyam_Closure_Aux(fun443,&ref[978])
	move_ret ref[979]
	c_ret

;; TERM 976: '$CLOSURE'('$fun'(441, 0, 1128518956), '$TUPPLE'(35144395401924))
c_code local build_ref_976
	ret_reg &ref[976]
	call_c   build_ref_719()
	call_c   Dyam_Closure_Aux(fun441,&ref[719])
	move_ret ref[976]
	c_ret

pl_code local fun441
	call_c   Dyam_Pool(pool_fun31)
	call_c   Dyam_Allocate(0)
	pl_jump  fun31()

long local pool_fun442[2]=[1,build_ref_976]

pl_code local fun442
	call_c   Dyam_Remove_Choice()
	move     &ref[976], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Load(4,V(17))
	call_c   Dyam_Reg_Load(6,V(2))
	call_c   Dyam_Reg_Deallocate(4)
fun224:
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Reg_Bind(2,V(4))
	call_c   Dyam_Multi_Reg_Bind(4,2,2)
	call_c   Dyam_Choice(fun223)
	pl_call  fun29(&seed[119],1)
	pl_fail


pl_code local fun440
	call_c   Dyam_Remove_Choice()
fun439:
	call_c   Dyam_Cut()
	pl_jump  fun31()


;; TERM 974: '$CLOSURE'('$fun'(225, 0, 1126151852), '$TUPPLE'(35144395401924))
c_code local build_ref_974
	ret_reg &ref[974]
	call_c   build_ref_719()
	call_c   Dyam_Closure_Aux(fun225,&ref[719])
	move_ret ref[974]
	c_ret

pl_code local fun225
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Cut()
	pl_fail

long local pool_fun443[4]=[131073,build_ref_974,pool_fun442,pool_fun31]

pl_code local fun443
	call_c   Dyam_Pool(pool_fun443)
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun442)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Choice(fun440)
	call_c   Dyam_Set_Cut()
	move     &ref[974], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Load(4,V(17))
	call_c   Dyam_Reg_Load(6,V(2))
	call_c   Dyam_Reg_Deallocate(4)
	pl_jump  fun221()

;; TERM 978: '$TUPPLE'(35144395662736)
c_code local build_ref_978
	ret_reg &ref[978]
	call_c   Dyam_Create_Simple_Tupple(0,335546368)
	move_ret ref[978]
	c_ret

long local pool_fun444[2]=[1,build_ref_979]

long local pool_fun445[4]=[65538,build_ref_458,build_ref_980,pool_fun444]

pl_code local fun445
	call_c   Dyam_Remove_Choice()
	call_c   DYAM_sfol_identical(V(2),&ref[458])
	fail_ret
	call_c   DYAM_sfol_identical(V(13),&ref[980])
	fail_ret
fun444:
	move     &ref[979], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Load(4,V(4))
	call_c   Dyam_Reg_Load(6,V(9))
	move     V(17), R(8)
	move     S(5), R(9)
	call_c   Dyam_Reg_Deallocate(5)
fun438:
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Reg_Bind(2,V(5))
	call_c   Dyam_Multi_Reg_Bind(4,2,3)
	call_c   Dyam_Choice(fun437)
	pl_call  fun29(&seed[160],1)
	pl_fail



;; TERM 966: '$VAR'(_P, ['$SET$',adj(no, yes, strict, atmostone, one, sync),62])
c_code local build_ref_966
	ret_reg &ref[966]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1178()
	call_c   build_ref_706()
	call_c   build_ref_319()
	call_c   build_ref_673()
	call_c   build_ref_685()
	call_c   build_ref_686()
	call_c   build_ref_623()
	call_c   Dyam_Term_Start(&ref[1178],6)
	call_c   Dyam_Term_Arg(&ref[706])
	call_c   Dyam_Term_Arg(&ref[319])
	call_c   Dyam_Term_Arg(&ref[673])
	call_c   Dyam_Term_Arg(&ref[685])
	call_c   Dyam_Term_Arg(&ref[686])
	call_c   Dyam_Term_Arg(&ref[623])
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   Dyam_Term_Start(I(8),3)
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(N(62))
	call_c   Dyam_Term_End()
	move_ret ref[966]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun450[7]=[131076,build_ref_964,build_ref_965,build_ref_451,build_ref_966,pool_fun445,pool_fun444]

pl_code local fun450
	call_c   Dyam_Pool(pool_fun450)
	call_c   Dyam_Unify_Item(&ref[964])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun114)
	call_c   Dyam_Reg_Load(0,V(1))
	move     &ref[965], R(2)
	move     S(5), R(3)
	pl_call  pred_spine_node_2()
	call_c   Dyam_Choice(fun445)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(2),&ref[451])
	fail_ret
	call_c   Dyam_Unify(V(12),&ref[966])
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun444()

;; TERM 963: '*FIRST*'(not_tig(_B, _C)) :> []
c_code local build_ref_963
	ret_reg &ref[963]
	call_c   build_ref_962()
	call_c   Dyam_Create_Binary(I(9),&ref[962],I(0))
	move_ret ref[963]
	c_ret

;; TERM 962: '*FIRST*'(not_tig(_B, _C))
c_code local build_ref_962
	ret_reg &ref[962]
	call_c   build_ref_93()
	call_c   build_ref_961()
	call_c   Dyam_Create_Unary(&ref[93],&ref[961])
	move_ret ref[962]
	c_ret

c_code local build_seed_37
	ret_reg &seed[37]
	call_c   build_ref_54()
	call_c   build_ref_931()
	call_c   Dyam_Seed_Start(&ref[54],&ref[931],I(0),fun0,1)
	call_c   build_ref_930()
	call_c   Dyam_Seed_Add_Comp(&ref[930],fun435,0)
	call_c   Dyam_Seed_End()
	move_ret seed[37]
	c_ret

;; TERM 930: '*GUARD*'(tig_compile_subtree_right(_B, tag_node{id=> _C, label=> _D, children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}, _O, _P, _Q, _R, _S, _T, _U, _V, _W))
c_code local build_ref_930
	ret_reg &ref[930]
	call_c   build_ref_55()
	call_c   build_ref_929()
	call_c   Dyam_Create_Unary(&ref[55],&ref[929])
	move_ret ref[930]
	c_ret

;; TERM 929: tig_compile_subtree_right(_B, tag_node{id=> _C, label=> _D, children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}, _O, _P, _Q, _R, _S, _T, _U, _V, _W)
c_code local build_ref_929
	ret_reg &ref[929]
	call_c   build_ref_1228()
	call_c   build_ref_991()
	call_c   Dyam_Term_Start(&ref[1228],11)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(&ref[991])
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_Arg(V(21))
	call_c   Dyam_Term_Arg(V(22))
	call_c   Dyam_Term_End()
	move_ret ref[929]
	c_ret

c_code local build_seed_159
	ret_reg &seed[159]
	call_c   build_ref_55()
	call_c   build_ref_959()
	call_c   Dyam_Seed_Start(&ref[55],&ref[959],I(0),fun11,1)
	call_c   build_ref_960()
	call_c   Dyam_Seed_Add_Comp(&ref[960],&ref[959],0)
	call_c   Dyam_Seed_End()
	move_ret seed[159]
	c_ret

;; TERM 960: '*GUARD*'(tag_compile_subtree_potential_adj(_B, tag_node{id=> _C, label=> _D, children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}, _O, _P, _Q, _R, _S, _T, _U, _J, _V, _W)) :> '$$HOLE$$'
c_code local build_ref_960
	ret_reg &ref[960]
	call_c   build_ref_959()
	call_c   Dyam_Create_Binary(I(9),&ref[959],I(7))
	move_ret ref[960]
	c_ret

;; TERM 959: '*GUARD*'(tag_compile_subtree_potential_adj(_B, tag_node{id=> _C, label=> _D, children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}, _O, _P, _Q, _R, _S, _T, _U, _J, _V, _W))
c_code local build_ref_959
	ret_reg &ref[959]
	call_c   build_ref_55()
	call_c   build_ref_958()
	call_c   Dyam_Create_Unary(&ref[55],&ref[958])
	move_ret ref[959]
	c_ret

;; TERM 958: tag_compile_subtree_potential_adj(_B, tag_node{id=> _C, label=> _D, children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}, _O, _P, _Q, _R, _S, _T, _U, _J, _V, _W)
c_code local build_ref_958
	ret_reg &ref[958]
	call_c   build_ref_1255()
	call_c   build_ref_991()
	call_c   Dyam_Term_Start(&ref[1255],12)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(&ref[991])
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(21))
	call_c   Dyam_Term_Arg(V(22))
	call_c   Dyam_Term_End()
	move_ret ref[958]
	c_ret

;; TERM 1255: tag_compile_subtree_potential_adj
c_code local build_ref_1255
	ret_reg &ref[1255]
	call_c   Dyam_Create_Atom("tag_compile_subtree_potential_adj")
	move_ret ref[1255]
	c_ret

long local pool_fun434[2]=[1,build_seed_159]

pl_code local fun434
	call_c   Dyam_Remove_Choice()
	pl_call  fun38(&seed[159],1)
	pl_jump  fun85()

;; TERM 932: adjkind((_X / _Y), right)
c_code local build_ref_932
	ret_reg &ref[932]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1170()
	call_c   Dyam_Create_Binary(&ref[1170],V(23),V(24))
	move_ret R(0)
	call_c   build_ref_1136()
	call_c   build_ref_458()
	call_c   Dyam_Create_Binary(&ref[1136],R(0),&ref[458])
	move_ret ref[932]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 933: '$VAR'(_Z, ['$SET$',adj(no, yes, strict, atmostone, one, sync),62])
c_code local build_ref_933
	ret_reg &ref[933]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1178()
	call_c   build_ref_706()
	call_c   build_ref_319()
	call_c   build_ref_673()
	call_c   build_ref_685()
	call_c   build_ref_686()
	call_c   build_ref_623()
	call_c   Dyam_Term_Start(&ref[1178],6)
	call_c   Dyam_Term_Arg(&ref[706])
	call_c   Dyam_Term_Arg(&ref[319])
	call_c   Dyam_Term_Arg(&ref[673])
	call_c   Dyam_Term_Arg(&ref[685])
	call_c   Dyam_Term_Arg(&ref[686])
	call_c   Dyam_Term_Arg(&ref[623])
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   Dyam_Term_Start(I(8),3)
	call_c   Dyam_Term_Arg(V(25))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(N(62))
	call_c   Dyam_Term_End()
	move_ret ref[933]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 951: '$CLOSURE'('$fun'(419, 0, 1128342128), '$TUPPLE'(35144395661940))
c_code local build_ref_951
	ret_reg &ref[951]
	call_c   build_ref_950()
	call_c   Dyam_Closure_Aux(fun419,&ref[950])
	move_ret ref[951]
	c_ret

;; TERM 947: '*** PB DECOMPOSE adj_right ~w ~w nid=~w id=~w\n'
c_code local build_ref_947
	ret_reg &ref[947]
	call_c   Dyam_Create_Atom("*** PB DECOMPOSE adj_right ~w ~w nid=~w id=~w\n")
	move_ret ref[947]
	c_ret

;; TERM 948: [_J,_J1,_C,_B]
c_code local build_ref_948
	ret_reg &ref[948]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_824()
	call_c   Dyam_Create_List(V(2),&ref[824])
	move_ret R(0)
	call_c   Dyam_Create_List(V(35),R(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(9),R(0))
	move_ret ref[948]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 941: '*WRAPPER-CALL-ALT*'(tig_adj((_X / _Y), right, _B1, _D), _K1, _Q)
c_code local build_ref_941
	ret_reg &ref[941]
	call_c   build_ref_1216()
	call_c   build_ref_952()
	call_c   Dyam_Term_Start(&ref[1216],3)
	call_c   Dyam_Term_Arg(&ref[952])
	call_c   Dyam_Term_Arg(V(36))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_End()
	move_ret ref[941]
	c_ret

;; TERM 952: tig_adj((_X / _Y), right, _B1, _D)
c_code local build_ref_952
	ret_reg &ref[952]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1170()
	call_c   Dyam_Create_Binary(&ref[1170],V(23),V(24))
	move_ret R(0)
	call_c   build_ref_1215()
	call_c   build_ref_458()
	call_c   Dyam_Term_Start(&ref[1215],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[458])
	call_c   Dyam_Term_Arg(V(27))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[952]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_158
	ret_reg &seed[158]
	call_c   build_ref_55()
	call_c   build_ref_939()
	call_c   Dyam_Seed_Start(&ref[55],&ref[939],I(0),fun11,1)
	call_c   build_ref_940()
	call_c   Dyam_Seed_Add_Comp(&ref[940],&ref[939],0)
	call_c   Dyam_Seed_End()
	move_ret seed[158]
	c_ret

;; TERM 940: '*GUARD*'(tag_compile_subtree_potential_adj(_B, tag_node{id=> _C, label=> _D, children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}, _O, _F1, _I1, _R, _S, _T, _U, _E1, _V, _W)) :> '$$HOLE$$'
c_code local build_ref_940
	ret_reg &ref[940]
	call_c   build_ref_939()
	call_c   Dyam_Create_Binary(I(9),&ref[939],I(7))
	move_ret ref[940]
	c_ret

;; TERM 939: '*GUARD*'(tag_compile_subtree_potential_adj(_B, tag_node{id=> _C, label=> _D, children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}, _O, _F1, _I1, _R, _S, _T, _U, _E1, _V, _W))
c_code local build_ref_939
	ret_reg &ref[939]
	call_c   build_ref_55()
	call_c   build_ref_938()
	call_c   Dyam_Create_Unary(&ref[55],&ref[938])
	move_ret ref[939]
	c_ret

;; TERM 938: tag_compile_subtree_potential_adj(_B, tag_node{id=> _C, label=> _D, children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}, _O, _F1, _I1, _R, _S, _T, _U, _E1, _V, _W)
c_code local build_ref_938
	ret_reg &ref[938]
	call_c   build_ref_1255()
	call_c   build_ref_991()
	call_c   Dyam_Term_Start(&ref[1255],12)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(&ref[991])
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(31))
	call_c   Dyam_Term_Arg(V(34))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_Arg(V(30))
	call_c   Dyam_Term_Arg(V(21))
	call_c   Dyam_Term_Arg(V(22))
	call_c   Dyam_Term_End()
	move_ret ref[938]
	c_ret

long local pool_fun415[2]=[1,build_seed_158]

long local pool_fun416[3]=[65537,build_ref_941,pool_fun415]

long local pool_fun418[4]=[65538,build_ref_947,build_ref_948,pool_fun416]

pl_code local fun418
	call_c   Dyam_Remove_Choice()
	move     &ref[947], R(0)
	move     0, R(1)
	move     &ref[948], R(2)
	move     S(5), R(3)
	pl_call  pred_format_2()
fun416:
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(30))
	call_c   Dyam_Reg_Load(4,V(35))
	move     &ref[941], R(6)
	move     S(5), R(7)
	move     V(34), R(8)
	move     S(5), R(9)
	call_c   Dyam_Reg_Load(10,V(18))
	pl_call  pred_feature_args_unif_6()
fun415:
	pl_call  fun38(&seed[158],1)
	pl_jump  fun85()



;; TERM 944: '$CLOSURE'('$fun'(417, 0, 1128327288), '$TUPPLE'(35144395661652))
c_code local build_ref_944
	ret_reg &ref[944]
	call_c   build_ref_943()
	call_c   Dyam_Closure_Aux(fun417,&ref[943])
	move_ret ref[944]
	c_ret

pl_code local fun417
	call_c   Dyam_Pool(pool_fun416)
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Cut()
	pl_jump  fun416()

;; TERM 943: '$TUPPLE'(35144395661652)
c_code local build_ref_943
	ret_reg &ref[943]
	call_c   Dyam_Start_Tupple(0,268427250)
	call_c   Dyam_Almost_End_Tupple(29,207618048)
	move_ret ref[943]
	c_ret

;; TERM 945: tig_adj((_X / _Y), right)
c_code local build_ref_945
	ret_reg &ref[945]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1170()
	call_c   Dyam_Create_Binary(&ref[1170],V(23),V(24))
	move_ret R(0)
	call_c   build_ref_1215()
	call_c   build_ref_458()
	call_c   Dyam_Create_Binary(&ref[1215],R(0),&ref[458])
	move_ret ref[945]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 946: [_F1,_P,_J,_J1,_C,_W,_B]
c_code local build_ref_946
	ret_reg &ref[946]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_824()
	call_c   Dyam_Create_List(V(22),&ref[824])
	move_ret R(0)
	call_c   Dyam_Create_List(V(2),R(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(35),R(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(9),R(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(15),R(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(31),R(0))
	move_ret ref[946]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun419[5]=[65539,build_ref_944,build_ref_945,build_ref_946,pool_fun418]

pl_code local fun419
	call_c   Dyam_Pool(pool_fun419)
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun418)
	call_c   Dyam_Set_Cut()
	move     &ref[944], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	move     &ref[945], R(4)
	move     S(5), R(5)
	move     &ref[946], R(6)
	move     S(5), R(7)
	move     V(36), R(8)
	move     S(5), R(9)
	call_c   Dyam_Reg_Deallocate(5)
	pl_jump  fun237()

;; TERM 950: '$TUPPLE'(35144395661940)
c_code local build_ref_950
	ret_reg &ref[950]
	call_c   Dyam_Start_Tupple(0,268435442)
	call_c   Dyam_Almost_End_Tupple(29,201326592)
	move_ret ref[950]
	c_ret

long local pool_fun420[3]=[2,build_ref_951,build_ref_952]

pl_code local fun420
	call_c   Dyam_Remove_Choice()
	move     &ref[951], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	move     &ref[952], R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  fun249()

;; TERM 937: tig_adj((_X / _Y), right, _B1)
c_code local build_ref_937
	ret_reg &ref[937]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1170()
	call_c   Dyam_Create_Binary(&ref[1170],V(23),V(24))
	move_ret R(0)
	call_c   build_ref_1215()
	call_c   build_ref_458()
	call_c   Dyam_Term_Start(&ref[1215],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[458])
	call_c   Dyam_Term_Arg(V(27))
	call_c   Dyam_Term_End()
	move_ret ref[937]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun421[5]=[131074,build_ref_687,build_ref_937,pool_fun420,pool_fun415]

pl_code local fun423
	call_c   Dyam_Remove_Choice()
fun421:
	move     V(31), R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(32), R(4)
	move     S(5), R(5)
	pl_call  pred_tag_numbervars_3()
	call_c   Dyam_Reg_Load(0,V(20))
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(33), R(4)
	move     S(5), R(5)
	pl_call  pred_tag_numbervars_3()
	call_c   Dyam_Choice(fun420)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[687])
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(1))
	move     &ref[937], R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Load(4,V(31))
	call_c   Dyam_Reg_Load(6,V(9))
	call_c   Dyam_Reg_Load(8,V(15))
	call_c   Dyam_Reg_Load(10,V(30))
	call_c   Dyam_Reg_Load(12,V(2))
	call_c   Dyam_Reg_Load(14,V(16))
	move     V(34), R(16)
	move     S(5), R(17)
	call_c   Dyam_Reg_Load(18,V(18))
	call_c   Dyam_Reg_Load(20,V(21))
	call_c   Dyam_Reg_Load(22,V(22))
	pl_call  pred_tig_compile_adj_node_12()
	pl_jump  fun415()


;; TERM 955: '$CLOSURE'('$fun'(422, 0, 1128353248), '$TUPPLE'(35144395661468))
c_code local build_ref_955
	ret_reg &ref[955]
	call_c   build_ref_954()
	call_c   Dyam_Closure_Aux(fun422,&ref[954])
	move_ret ref[955]
	c_ret

pl_code local fun422
	call_c   Dyam_Pool(pool_fun421)
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Cut()
	pl_jump  fun421()

;; TERM 954: '$TUPPLE'(35144395661468)
c_code local build_ref_954
	ret_reg &ref[954]
	call_c   Dyam_Start_Tupple(0,268435442)
	call_c   Dyam_Almost_End_Tupple(29,134217728)
	move_ret ref[954]
	c_ret

long local pool_fun424[3]=[65537,build_ref_955,pool_fun421]

pl_code local fun425
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(20),V(30))
	fail_ret
fun424:
	call_c   Dyam_Choice(fun423)
	call_c   Dyam_Set_Cut()
	move     &ref[955], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Load(4,V(3))
	call_c   Dyam_Reg_Load(6,V(20))
	call_c   Dyam_Reg_Load(8,V(30))
	call_c   Dyam_Reg_Deallocate(5)
	pl_jump  fun50()


;; TERM 936: adjkind((_X / _Y), wrap)
c_code local build_ref_936
	ret_reg &ref[936]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1170()
	call_c   Dyam_Create_Binary(&ref[1170],V(23),V(24))
	move_ret R(0)
	call_c   build_ref_1136()
	call_c   build_ref_512()
	call_c   Dyam_Create_Binary(&ref[1136],R(0),&ref[512])
	move_ret ref[936]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun426[4]=[131073,build_ref_936,pool_fun424,pool_fun424]

pl_code local fun429
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(27),V(6))
	fail_ret
fun426:
	call_c   Dyam_Choice(fun425)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[936])
	call_c   Dyam_Cut()
	call_c   DYAM_evpred_functor(V(30),V(23),V(24))
	fail_ret
	pl_jump  fun424()


;; TERM 957: adjkind((_X / _Y), _D1)
c_code local build_ref_957
	ret_reg &ref[957]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1170()
	call_c   Dyam_Create_Binary(&ref[1170],V(23),V(24))
	move_ret R(0)
	call_c   build_ref_1136()
	call_c   Dyam_Create_Binary(&ref[1136],R(0),V(29))
	move_ret ref[957]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun427[3]=[65537,build_ref_319,pool_fun426]

pl_code local fun428
	call_c   Dyam_Remove_Choice()
fun427:
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(27),&ref[319])
	fail_ret
	pl_jump  fun426()


long local pool_fun430[5]=[131074,build_ref_957,build_ref_458,pool_fun426,pool_fun427]

pl_code local fun430
	call_c   Dyam_Update_Choice(fun429)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[957])
	call_c   Dyam_Choice(fun428)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(29),&ref[458])
	fail_ret
	call_c   Dyam_Cut()
	pl_fail

;; TERM 956: '$VAR'(_C1, ['$SET$',adj(no, yes, strict, atmostone, one, sync),28])
c_code local build_ref_956
	ret_reg &ref[956]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1178()
	call_c   build_ref_706()
	call_c   build_ref_319()
	call_c   build_ref_673()
	call_c   build_ref_685()
	call_c   build_ref_686()
	call_c   build_ref_623()
	call_c   Dyam_Term_Start(&ref[1178],6)
	call_c   Dyam_Term_Arg(&ref[706])
	call_c   Dyam_Term_Arg(&ref[319])
	call_c   Dyam_Term_Arg(&ref[673])
	call_c   Dyam_Term_Arg(&ref[685])
	call_c   Dyam_Term_Arg(&ref[686])
	call_c   Dyam_Term_Arg(&ref[623])
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   Dyam_Term_Start(I(8),3)
	call_c   Dyam_Term_Arg(V(28))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(N(28))
	call_c   Dyam_Term_End()
	move_ret ref[956]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun431[4]=[131073,build_ref_956,pool_fun430,pool_fun426]

pl_code local fun431
	call_c   Dyam_Update_Choice(fun430)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(12),&ref[956])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(27),V(12))
	fail_ret
	pl_jump  fun426()

;; TERM 935: '$VAR'(_A1, ['$SET$',adj(no, yes, strict, atmostone, one, sync),24])
c_code local build_ref_935
	ret_reg &ref[935]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1178()
	call_c   build_ref_706()
	call_c   build_ref_319()
	call_c   build_ref_673()
	call_c   build_ref_685()
	call_c   build_ref_686()
	call_c   build_ref_623()
	call_c   Dyam_Term_Start(&ref[1178],6)
	call_c   Dyam_Term_Arg(&ref[706])
	call_c   Dyam_Term_Arg(&ref[319])
	call_c   Dyam_Term_Arg(&ref[673])
	call_c   Dyam_Term_Arg(&ref[685])
	call_c   Dyam_Term_Arg(&ref[686])
	call_c   Dyam_Term_Arg(&ref[623])
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   Dyam_Term_Start(I(8),3)
	call_c   Dyam_Term_Arg(V(26))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(N(24))
	call_c   Dyam_Term_End()
	move_ret ref[935]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun432[7]=[131076,build_ref_623,build_ref_673,build_ref_935,build_ref_686,pool_fun431,pool_fun426]

pl_code local fun433
	call_c   Dyam_Remove_Choice()
fun432:
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun431)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(12),&ref[623])
	fail_ret
	call_c   Dyam_Unify(V(6),&ref[673])
	fail_ret
	call_c   Dyam_Unify(V(11),&ref[935])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(27),&ref[686])
	fail_ret
	pl_jump  fun426()


;; TERM 934: tig(_B, left)
c_code local build_ref_934
	ret_reg &ref[934]
	call_c   build_ref_1154()
	call_c   build_ref_451()
	call_c   Dyam_Create_Binary(&ref[1154],V(1),&ref[451])
	move_ret ref[934]
	c_ret

long local pool_fun435[8]=[131077,build_ref_930,build_ref_932,build_ref_933,build_ref_319,build_ref_934,pool_fun434,pool_fun432]

pl_code local fun435
	call_c   Dyam_Pool(pool_fun435)
	call_c   Dyam_Unify_Item(&ref[930])
	fail_ret
	call_c   DYAM_evpred_functor(V(20),V(23),V(24))
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun434)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[932])
	call_c   Dyam_Unify(V(12),&ref[933])
	fail_ret
	call_c   Dyam_Choice(fun433)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(7),&ref[319])
	fail_ret
	pl_call  Object_1(&ref[934])
	call_c   Dyam_Cut()
	pl_fail

;; TERM 931: '*GUARD*'(tig_compile_subtree_right(_B, tag_node{id=> _C, label=> _D, children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}, _O, _P, _Q, _R, _S, _T, _U, _V, _W)) :> []
c_code local build_ref_931
	ret_reg &ref[931]
	call_c   build_ref_930()
	call_c   Dyam_Create_Binary(I(9),&ref[930],I(0))
	move_ret ref[931]
	c_ret

c_code local build_seed_24
	ret_reg &seed[24]
	call_c   build_ref_54()
	call_c   build_ref_989()
	call_c   Dyam_Seed_Start(&ref[54],&ref[989],I(0),fun0,1)
	call_c   build_ref_988()
	call_c   Dyam_Seed_Add_Comp(&ref[988],fun455,0)
	call_c   Dyam_Seed_End()
	move_ret seed[24]
	c_ret

;; TERM 988: '*GUARD*'(lctag_simplify(_B, tag_node{id=> _C, label=> _D, children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}, _O, _P))
c_code local build_ref_988
	ret_reg &ref[988]
	call_c   build_ref_55()
	call_c   build_ref_987()
	call_c   Dyam_Create_Unary(&ref[55],&ref[987])
	move_ret ref[988]
	c_ret

;; TERM 987: lctag_simplify(_B, tag_node{id=> _C, label=> _D, children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}, _O, _P)
c_code local build_ref_987
	ret_reg &ref[987]
	call_c   build_ref_1124()
	call_c   build_ref_991()
	call_c   Dyam_Term_Start(&ref[1124],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(&ref[991])
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_End()
	move_ret ref[987]
	c_ret

c_code local build_seed_162
	ret_reg &seed[162]
	call_c   build_ref_55()
	call_c   build_ref_996()
	call_c   Dyam_Seed_Start(&ref[55],&ref[996],I(0),fun11,1)
	call_c   build_ref_997()
	call_c   Dyam_Seed_Add_Comp(&ref[997],&ref[996],0)
	call_c   Dyam_Seed_End()
	move_ret seed[162]
	c_ret

;; TERM 997: '*GUARD*'(lctag_simplify(_B, _E, _O, _R)) :> '$$HOLE$$'
c_code local build_ref_997
	ret_reg &ref[997]
	call_c   build_ref_996()
	call_c   Dyam_Create_Binary(I(9),&ref[996],I(7))
	move_ret ref[997]
	c_ret

;; TERM 996: '*GUARD*'(lctag_simplify(_B, _E, _O, _R))
c_code local build_ref_996
	ret_reg &ref[996]
	call_c   build_ref_55()
	call_c   build_ref_995()
	call_c   Dyam_Create_Unary(&ref[55],&ref[995])
	move_ret ref[996]
	c_ret

;; TERM 995: lctag_simplify(_B, _E, _O, _R)
c_code local build_ref_995
	ret_reg &ref[995]
	call_c   build_ref_1124()
	call_c   Dyam_Term_Start(&ref[1124],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_End()
	move_ret ref[995]
	c_ret

;; TERM 998: tag_node{id=> _C, label=> _D, children=> _R, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}
c_code local build_ref_998
	ret_reg &ref[998]
	call_c   build_ref_1172()
	call_c   Dyam_Term_Start(&ref[1172],12)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_End()
	move_ret ref[998]
	c_ret

long local pool_fun452[3]=[2,build_seed_162,build_ref_998]

pl_code local fun452
	call_c   Dyam_Remove_Choice()
	pl_call  fun38(&seed[162],1)
	call_c   Dyam_Unify(V(15),&ref[998])
	fail_ret
	pl_jump  fun85()

;; TERM 994: [escape,subst]
c_code local build_ref_994
	ret_reg &ref[994]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1080()
	call_c   Dyam_Create_List(&ref[1080],I(0))
	move_ret R(0)
	call_c   build_ref_1086()
	call_c   Dyam_Create_List(&ref[1086],R(0))
	move_ret ref[994]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1086: escape
c_code local build_ref_1086
	ret_reg &ref[1086]
	call_c   Dyam_Create_Atom("escape")
	move_ret ref[1086]
	c_ret

long local pool_fun453[5]=[65539,build_ref_994,build_ref_300,build_ref_991,pool_fun452]

pl_code local fun453
	call_c   Dyam_Update_Choice(fun452)
	call_c   Dyam_Set_Cut()
	pl_call  Domain_2(V(5),&ref[994])
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(14),&ref[300])
	fail_ret
	call_c   Dyam_Unify(V(15),&ref[991])
	fail_ret
	pl_jump  fun85()

long local pool_fun451[2]=[1,build_ref_367]

pl_code local fun451
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(14),&ref[367])
	fail_ret
	pl_jump  fun85()

;; TERM 993: tig(_B, _Q)
c_code local build_ref_993
	ret_reg &ref[993]
	call_c   build_ref_1154()
	call_c   Dyam_Create_Binary(&ref[1154],V(1),V(16))
	move_ret ref[993]
	c_ret

long local pool_fun454[7]=[131076,build_ref_992,build_ref_991,build_ref_993,build_ref_300,pool_fun453,pool_fun451]

pl_code local fun454
	call_c   Dyam_Update_Choice(fun453)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(5),&ref[992])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(15),&ref[991])
	fail_ret
	call_c   Dyam_Choice(fun451)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[993])
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(14),&ref[300])
	fail_ret
	pl_jump  fun85()

;; TERM 990: [coanchor,scan,anchor]
c_code local build_ref_990
	ret_reg &ref[990]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1076()
	call_c   Dyam_Create_List(&ref[1076],I(0))
	move_ret R(0)
	call_c   build_ref_1062()
	call_c   Dyam_Create_List(&ref[1062],R(0))
	move_ret R(0)
	call_c   build_ref_1063()
	call_c   Dyam_Create_List(&ref[1063],R(0))
	move_ret ref[990]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1063: coanchor
c_code local build_ref_1063
	ret_reg &ref[1063]
	call_c   Dyam_Create_Atom("coanchor")
	move_ret ref[1063]
	c_ret

;; TERM 1062: scan
c_code local build_ref_1062
	ret_reg &ref[1062]
	call_c   Dyam_Create_Atom("scan")
	move_ret ref[1062]
	c_ret

;; TERM 1076: anchor
c_code local build_ref_1076
	ret_reg &ref[1076]
	call_c   Dyam_Create_Atom("anchor")
	move_ret ref[1076]
	c_ret

long local pool_fun455[6]=[65540,build_ref_988,build_ref_990,build_ref_367,build_ref_991,pool_fun454]

pl_code local fun455
	call_c   Dyam_Pool(pool_fun455)
	call_c   Dyam_Unify_Item(&ref[988])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun454)
	call_c   Dyam_Set_Cut()
	pl_call  Domain_2(V(5),&ref[990])
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(14),&ref[367])
	fail_ret
	call_c   Dyam_Unify(V(15),&ref[991])
	fail_ret
	pl_jump  fun85()

;; TERM 989: '*GUARD*'(lctag_simplify(_B, tag_node{id=> _C, label=> _D, children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}, _O, _P)) :> []
c_code local build_ref_989
	ret_reg &ref[989]
	call_c   build_ref_988()
	call_c   Dyam_Create_Binary(I(9),&ref[988],I(0))
	move_ret ref[989]
	c_ret

c_code local build_seed_34
	ret_reg &seed[34]
	call_c   build_ref_93()
	call_c   build_ref_609()
	call_c   Dyam_Seed_Start(&ref[93],&ref[609],I(0),fun21,1)
	call_c   build_ref_610()
	call_c   Dyam_Seed_Add_Comp(&ref[610],fun470,0)
	call_c   Dyam_Seed_End()
	move_ret seed[34]
	c_ret

;; TERM 1030: '$CLOSURE'('$fun'(469, 0, 1128754692), '$TUPPLE'(35144395917660))
c_code local build_ref_1030
	ret_reg &ref[1030]
	call_c   build_ref_663()
	call_c   Dyam_Closure_Aux(fun469,&ref[663])
	move_ret ref[1030]
	c_ret

;; TERM 1028: '$CLOSURE'('$fun'(468, 0, 1128755112), '$TUPPLE'(35144395917532))
c_code local build_ref_1028
	ret_reg &ref[1028]
	call_c   build_ref_658()
	call_c   Dyam_Closure_Aux(fun468,&ref[658])
	move_ret ref[1028]
	c_ret

;; TERM 999: callret(_T, _U)
c_code local build_ref_999
	ret_reg &ref[999]
	call_c   build_ref_1226()
	call_c   Dyam_Create_Binary(&ref[1226],V(19),V(20))
	move_ret ref[999]
	c_ret

;; TERM 1000: [_V|_N]
c_code local build_ref_1000
	ret_reg &ref[1000]
	call_c   Dyam_Create_List(V(21),V(13))
	move_ret ref[1000]
	c_ret

;; TERM 1026: '$CLOSURE'('$fun'(465, 0, 1128718408), '$TUPPLE'(35144395663944))
c_code local build_ref_1026
	ret_reg &ref[1026]
	call_c   build_ref_1025()
	call_c   Dyam_Closure_Aux(fun465,&ref[1025])
	move_ret ref[1026]
	c_ret

;; TERM 1023: '*CONT*'('*SA-SUBST*'(_A1, _B1, _C1, _K))
c_code local build_ref_1023
	ret_reg &ref[1023]
	call_c   build_ref_1217()
	call_c   build_ref_1003()
	call_c   Dyam_Create_Unary(&ref[1217],&ref[1003])
	move_ret ref[1023]
	c_ret

;; TERM 1003: '*SA-SUBST*'(_A1, _B1, _C1, _K)
c_code local build_ref_1003
	ret_reg &ref[1003]
	call_c   build_ref_1218()
	call_c   Dyam_Term_Start(&ref[1218],4)
	call_c   Dyam_Term_Arg(V(26))
	call_c   Dyam_Term_Arg(V(27))
	call_c   Dyam_Term_Arg(V(28))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_End()
	move_ret ref[1003]
	c_ret

c_code local build_seed_167
	ret_reg &seed[167]
	call_c   build_ref_55()
	call_c   build_ref_1021()
	call_c   Dyam_Seed_Start(&ref[55],&ref[1021],I(0),fun11,1)
	call_c   build_ref_1022()
	call_c   Dyam_Seed_Add_Comp(&ref[1022],&ref[1021],0)
	call_c   Dyam_Seed_End()
	move_ret seed[167]
	c_ret

;; TERM 1022: '*GUARD*'(body_to_lpda(_W, (_L = +), _D1, _E1, _F1)) :> '$$HOLE$$'
c_code local build_ref_1022
	ret_reg &ref[1022]
	call_c   build_ref_1021()
	call_c   Dyam_Create_Binary(I(9),&ref[1021],I(7))
	move_ret ref[1022]
	c_ret

;; TERM 1021: '*GUARD*'(body_to_lpda(_W, (_L = +), _D1, _E1, _F1))
c_code local build_ref_1021
	ret_reg &ref[1021]
	call_c   build_ref_55()
	call_c   build_ref_1020()
	call_c   Dyam_Create_Unary(&ref[55],&ref[1020])
	move_ret ref[1021]
	c_ret

;; TERM 1020: body_to_lpda(_W, (_L = +), _D1, _E1, _F1)
c_code local build_ref_1020
	ret_reg &ref[1020]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1220()
	call_c   build_ref_254()
	call_c   Dyam_Create_Binary(&ref[1220],V(11),&ref[254])
	move_ret R(0)
	call_c   build_ref_1219()
	call_c   Dyam_Term_Start(&ref[1219],5)
	call_c   Dyam_Term_Arg(V(22))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(29))
	call_c   Dyam_Term_Arg(V(30))
	call_c   Dyam_Term_Arg(V(31))
	call_c   Dyam_Term_End()
	move_ret ref[1020]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_165
	ret_reg &seed[165]
	call_c   build_ref_55()
	call_c   build_ref_1012()
	call_c   Dyam_Seed_Start(&ref[55],&ref[1012],I(0),fun11,1)
	call_c   build_ref_1013()
	call_c   Dyam_Seed_Add_Comp(&ref[1013],&ref[1012],0)
	call_c   Dyam_Seed_End()
	move_ret seed[165]
	c_ret

;; TERM 1013: '*GUARD*'(body_to_lpda(_W, (_I = _J), ('*RETARGS*'(_S) :> fail), _J1, _F1)) :> '$$HOLE$$'
c_code local build_ref_1013
	ret_reg &ref[1013]
	call_c   build_ref_1012()
	call_c   Dyam_Create_Binary(I(9),&ref[1012],I(7))
	move_ret ref[1013]
	c_ret

;; TERM 1012: '*GUARD*'(body_to_lpda(_W, (_I = _J), ('*RETARGS*'(_S) :> fail), _J1, _F1))
c_code local build_ref_1012
	ret_reg &ref[1012]
	call_c   build_ref_55()
	call_c   build_ref_1011()
	call_c   Dyam_Create_Unary(&ref[55],&ref[1011])
	move_ret ref[1012]
	c_ret

;; TERM 1011: body_to_lpda(_W, (_I = _J), ('*RETARGS*'(_S) :> fail), _J1, _F1)
c_code local build_ref_1011
	ret_reg &ref[1011]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_1220()
	call_c   Dyam_Create_Binary(&ref[1220],V(8),V(9))
	move_ret R(0)
	call_c   build_ref_1002()
	call_c   build_ref_299()
	call_c   Dyam_Create_Binary(I(9),&ref[1002],&ref[299])
	move_ret R(1)
	call_c   build_ref_1219()
	call_c   Dyam_Term_Start(&ref[1219],5)
	call_c   Dyam_Term_Arg(V(22))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(35))
	call_c   Dyam_Term_Arg(V(31))
	call_c   Dyam_Term_End()
	move_ret ref[1011]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1002: '*RETARGS*'(_S)
c_code local build_ref_1002
	ret_reg &ref[1002]
	call_c   build_ref_1256()
	call_c   Dyam_Create_Unary(&ref[1256],V(18))
	move_ret ref[1002]
	c_ret

;; TERM 1256: '*RETARGS*'
c_code local build_ref_1256
	ret_reg &ref[1256]
	call_c   Dyam_Create_Atom("*RETARGS*")
	move_ret ref[1256]
	c_ret

c_code local build_seed_166
	ret_reg &seed[166]
	call_c   build_ref_55()
	call_c   build_ref_1015()
	call_c   Dyam_Seed_Start(&ref[55],&ref[1015],I(0),fun11,1)
	call_c   build_ref_1016()
	call_c   Dyam_Seed_Add_Comp(&ref[1016],&ref[1015],0)
	call_c   Dyam_Seed_End()
	move_ret seed[166]
	c_ret

;; TERM 1016: '*GUARD*'(body_to_lpda(_W, (_G = _H), _J1, _K1, dyalog)) :> '$$HOLE$$'
c_code local build_ref_1016
	ret_reg &ref[1016]
	call_c   build_ref_1015()
	call_c   Dyam_Create_Binary(I(9),&ref[1015],I(7))
	move_ret ref[1016]
	c_ret

;; TERM 1015: '*GUARD*'(body_to_lpda(_W, (_G = _H), _J1, _K1, dyalog))
c_code local build_ref_1015
	ret_reg &ref[1015]
	call_c   build_ref_55()
	call_c   build_ref_1014()
	call_c   Dyam_Create_Unary(&ref[55],&ref[1014])
	move_ret ref[1015]
	c_ret

;; TERM 1014: body_to_lpda(_W, (_G = _H), _J1, _K1, dyalog)
c_code local build_ref_1014
	ret_reg &ref[1014]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1220()
	call_c   Dyam_Create_Binary(&ref[1220],V(6),V(7))
	move_ret R(0)
	call_c   build_ref_1219()
	call_c   build_ref_1221()
	call_c   Dyam_Term_Start(&ref[1219],5)
	call_c   Dyam_Term_Arg(V(22))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(35))
	call_c   Dyam_Term_Arg(V(36))
	call_c   Dyam_Term_Arg(&ref[1221])
	call_c   Dyam_Term_End()
	move_ret ref[1014]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1019: '*ALTERNATIVE-LAYER*'(_K1, _I1)
c_code local build_ref_1019
	ret_reg &ref[1019]
	call_c   build_ref_1222()
	call_c   Dyam_Create_Binary(&ref[1222],V(36),V(34))
	move_ret ref[1019]
	c_ret

;; TERM 1018: '*WRAPPER*'(tig_adj((_B / _C), _D, _E, _F), [_V|_N], _M1)
c_code local build_ref_1018
	ret_reg &ref[1018]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1170()
	call_c   Dyam_Create_Binary(&ref[1170],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_1215()
	call_c   Dyam_Term_Start(&ref[1215],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_1223()
	call_c   build_ref_1000()
	call_c   Dyam_Term_Start(&ref[1223],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[1000])
	call_c   Dyam_Term_Arg(V(38))
	call_c   Dyam_Term_End()
	move_ret ref[1018]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun456[3]=[2,build_ref_1018,build_seed_65]

long local pool_fun457[3]=[65537,build_ref_1019,pool_fun456]

pl_code local fun457
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(38),&ref[1019])
	fail_ret
fun456:
	call_c   DYAM_evpred_assert_1(&ref[1018])
	call_c   Dyam_Deallocate()
	pl_jump  fun19(&seed[65],2)


;; TERM 1017: '$VAR'(_L1, ['$SET$',adj(no, yes, strict, atmostone, one, sync),20])
c_code local build_ref_1017
	ret_reg &ref[1017]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1178()
	call_c   build_ref_706()
	call_c   build_ref_319()
	call_c   build_ref_673()
	call_c   build_ref_685()
	call_c   build_ref_686()
	call_c   build_ref_623()
	call_c   Dyam_Term_Start(&ref[1178],6)
	call_c   Dyam_Term_Arg(&ref[706])
	call_c   Dyam_Term_Arg(&ref[319])
	call_c   Dyam_Term_Arg(&ref[673])
	call_c   Dyam_Term_Arg(&ref[685])
	call_c   Dyam_Term_Arg(&ref[686])
	call_c   Dyam_Term_Arg(&ref[623])
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   Dyam_Term_Start(I(8),3)
	call_c   Dyam_Term_Arg(V(37))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(N(20))
	call_c   Dyam_Term_End()
	move_ret ref[1017]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun458[6]=[131075,build_seed_165,build_seed_166,build_ref_1017,pool_fun457,pool_fun456]

pl_code local fun459
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(34),V(30))
	fail_ret
fun458:
	pl_call  fun38(&seed[165],1)
	pl_call  fun38(&seed[166],1)
	call_c   Dyam_Choice(fun457)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(4),&ref[1017])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(38),V(34))
	fail_ret
	pl_jump  fun456()


;; TERM 1007: tagfilter_cat((_M ^ _F ^ _D ^ _G ^ _H ^ _I ^ _G1))
c_code local build_ref_1007
	ret_reg &ref[1007]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1203()
	call_c   Dyam_Create_Binary(&ref[1203],V(8),V(32))
	move_ret R(0)
	call_c   Dyam_Create_Binary(&ref[1203],V(7),R(0))
	move_ret R(0)
	call_c   Dyam_Create_Binary(&ref[1203],V(6),R(0))
	move_ret R(0)
	call_c   Dyam_Create_Binary(&ref[1203],V(3),R(0))
	move_ret R(0)
	call_c   Dyam_Create_Binary(&ref[1203],V(5),R(0))
	move_ret R(0)
	call_c   Dyam_Create_Binary(&ref[1203],V(12),R(0))
	move_ret R(0)
	call_c   build_ref_1202()
	call_c   Dyam_Create_Unary(&ref[1202],R(0))
	move_ret ref[1007]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_164
	ret_reg &seed[164]
	call_c   build_ref_55()
	call_c   build_ref_1009()
	call_c   Dyam_Seed_Start(&ref[55],&ref[1009],I(0),fun11,1)
	call_c   build_ref_1010()
	call_c   Dyam_Seed_Add_Comp(&ref[1010],&ref[1009],0)
	call_c   Dyam_Seed_End()
	move_ret seed[164]
	c_ret

;; TERM 1010: '*GUARD*'(body_to_lpda(_W, _G1, _E1, _I1, dyalog)) :> '$$HOLE$$'
c_code local build_ref_1010
	ret_reg &ref[1010]
	call_c   build_ref_1009()
	call_c   Dyam_Create_Binary(I(9),&ref[1009],I(7))
	move_ret ref[1010]
	c_ret

;; TERM 1009: '*GUARD*'(body_to_lpda(_W, _G1, _E1, _I1, dyalog))
c_code local build_ref_1009
	ret_reg &ref[1009]
	call_c   build_ref_55()
	call_c   build_ref_1008()
	call_c   Dyam_Create_Unary(&ref[55],&ref[1008])
	move_ret ref[1009]
	c_ret

;; TERM 1008: body_to_lpda(_W, _G1, _E1, _I1, dyalog)
c_code local build_ref_1008
	ret_reg &ref[1008]
	call_c   build_ref_1219()
	call_c   build_ref_1221()
	call_c   Dyam_Term_Start(&ref[1219],5)
	call_c   Dyam_Term_Arg(V(22))
	call_c   Dyam_Term_Arg(V(32))
	call_c   Dyam_Term_Arg(V(30))
	call_c   Dyam_Term_Arg(V(34))
	call_c   Dyam_Term_Arg(&ref[1221])
	call_c   Dyam_Term_End()
	move_ret ref[1008]
	c_ret

long local pool_fun460[5]=[131074,build_ref_1007,build_seed_164,pool_fun458,pool_fun458]

long local pool_fun461[3]=[65537,build_seed_167,pool_fun460]

pl_code local fun461
	call_c   Dyam_Remove_Choice()
	pl_call  fun38(&seed[167],1)
fun460:
	call_c   Dyam_Choice(fun459)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[1007])
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(32))
	call_c   Dyam_Reg_Load(2,V(22))
	move     V(33), R(4)
	move     S(5), R(5)
	pl_call  pred_tag_numbervars_3()
	pl_call  fun38(&seed[164],1)
	pl_jump  fun458()


c_code local build_seed_163
	ret_reg &seed[163]
	call_c   build_ref_55()
	call_c   build_ref_1005()
	call_c   Dyam_Seed_Start(&ref[55],&ref[1005],I(0),fun11,1)
	call_c   build_ref_1006()
	call_c   Dyam_Seed_Add_Comp(&ref[1006],&ref[1005],0)
	call_c   Dyam_Seed_End()
	move_ret seed[163]
	c_ret

;; TERM 1006: '*GUARD*'(body_to_lpda(_W, (_L == +), _D1, _E1, _F1)) :> '$$HOLE$$'
c_code local build_ref_1006
	ret_reg &ref[1006]
	call_c   build_ref_1005()
	call_c   Dyam_Create_Binary(I(9),&ref[1005],I(7))
	move_ret ref[1006]
	c_ret

;; TERM 1005: '*GUARD*'(body_to_lpda(_W, (_L == +), _D1, _E1, _F1))
c_code local build_ref_1005
	ret_reg &ref[1005]
	call_c   build_ref_55()
	call_c   build_ref_1004()
	call_c   Dyam_Create_Unary(&ref[55],&ref[1004])
	move_ret ref[1005]
	c_ret

;; TERM 1004: body_to_lpda(_W, (_L == +), _D1, _E1, _F1)
c_code local build_ref_1004
	ret_reg &ref[1004]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1224()
	call_c   build_ref_254()
	call_c   Dyam_Create_Binary(&ref[1224],V(11),&ref[254])
	move_ret R(0)
	call_c   build_ref_1219()
	call_c   Dyam_Term_Start(&ref[1219],5)
	call_c   Dyam_Term_Arg(V(22))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(29))
	call_c   Dyam_Term_Arg(V(30))
	call_c   Dyam_Term_Arg(V(31))
	call_c   Dyam_Term_End()
	move_ret ref[1004]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun462[5]=[131074,build_ref_623,build_seed_163,pool_fun461,pool_fun460]

long local pool_fun463[3]=[65537,build_ref_1023,pool_fun462]

pl_code local fun463
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(29),&ref[1023])
	fail_ret
fun462:
	call_c   Dyam_Choice(fun461)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(4),&ref[623])
	fail_ret
	call_c   Dyam_Cut()
	pl_call  fun38(&seed[163],1)
	pl_jump  fun460()


long local pool_fun464[5]=[131074,build_ref_111,build_ref_1003,pool_fun463,pool_fun462]

long local pool_fun465[3]=[65537,build_ref_652,pool_fun464]

pl_code local fun465
	call_c   Dyam_Pool(pool_fun465)
	call_c   Dyam_Unify(V(28),&ref[652])
	fail_ret
	call_c   Dyam_Allocate(0)
fun464:
	call_c   Dyam_Choice(fun463)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[111])
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(29),&ref[1003])
	fail_ret
	pl_jump  fun462()


;; TERM 1025: '$TUPPLE'(35144395663944)
c_code local build_ref_1025
	ret_reg &ref[1025]
	call_c   Dyam_Create_Simple_Tupple(0,536843462)
	move_ret ref[1025]
	c_ret

long local pool_fun466[3]=[2,build_ref_1026,build_ref_656]

pl_code local fun466
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Reg_Load(0,V(3))
	call_c   Dyam_Reg_Load(2,V(8))
	call_c   Dyam_Reg_Load(4,V(15))
	call_c   Dyam_Reg_Load(6,V(6))
	call_c   Dyam_Reg_Load(8,V(14))
	move     V(26), R(10)
	move     S(5), R(11)
	move     V(27), R(12)
	move     S(5), R(13)
	pl_call  pred_make_tig_callret_7()
	move     &ref[1026], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	move     &ref[656], R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  fun249()

long local pool_fun467[5]=[131074,build_ref_319,build_ref_644,pool_fun466,pool_fun464]

pl_code local fun467
	call_c   Dyam_Update_Choice(fun466)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(4),&ref[319])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(3))
	call_c   Dyam_Reg_Load(2,V(8))
	call_c   Dyam_Reg_Load(4,V(15))
	call_c   Dyam_Reg_Load(6,V(6))
	call_c   Dyam_Reg_Load(8,V(14))
	move     V(26), R(10)
	move     S(5), R(11)
	move     V(27), R(12)
	move     S(5), R(13)
	pl_call  pred_make_tig_callret_7()
	call_c   Dyam_Unify(V(28),&ref[644])
	fail_ret
	pl_jump  fun464()

;; TERM 1001: '$VAR'(_Z, ['$SET$',adj(no, yes, strict, atmostone, one, sync),56])
c_code local build_ref_1001
	ret_reg &ref[1001]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1178()
	call_c   build_ref_706()
	call_c   build_ref_319()
	call_c   build_ref_673()
	call_c   build_ref_685()
	call_c   build_ref_686()
	call_c   build_ref_623()
	call_c   Dyam_Term_Start(&ref[1178],6)
	call_c   Dyam_Term_Arg(&ref[706])
	call_c   Dyam_Term_Arg(&ref[319])
	call_c   Dyam_Term_Arg(&ref[673])
	call_c   Dyam_Term_Arg(&ref[685])
	call_c   Dyam_Term_Arg(&ref[686])
	call_c   Dyam_Term_Arg(&ref[623])
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   Dyam_Term_Start(I(8),3)
	call_c   Dyam_Term_Arg(V(25))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(N(56))
	call_c   Dyam_Term_End()
	move_ret ref[1001]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun468[8]=[131077,build_ref_618,build_ref_999,build_ref_1000,build_ref_1001,build_ref_1002,pool_fun467,pool_fun464]

pl_code local fun468
	call_c   Dyam_Pool(pool_fun468)
	call_c   Dyam_Unify(V(13),&ref[618])
	fail_ret
	call_c   Dyam_Unify(V(16),&ref[999])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[1000], R(0)
	move     S(5), R(1)
	move     V(22), R(2)
	move     S(5), R(3)
	move     V(23), R(4)
	move     S(5), R(5)
	pl_call  pred_tag_numbervars_3()
	call_c   Dyam_Reg_Load(0,V(16))
	call_c   Dyam_Reg_Load(2,V(22))
	move     V(24), R(4)
	move     S(5), R(5)
	pl_call  pred_tag_numbervars_3()
	call_c   Dyam_Choice(fun467)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(4),&ref[1001])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(3))
	call_c   Dyam_Reg_Load(2,V(8))
	call_c   Dyam_Reg_Load(4,V(9))
	call_c   Dyam_Reg_Load(6,V(6))
	call_c   Dyam_Reg_Load(8,V(7))
	move     V(26), R(10)
	move     S(5), R(11)
	move     V(27), R(12)
	move     S(5), R(13)
	pl_call  pred_make_tig_callret_7()
	call_c   Dyam_Unify(V(28),&ref[1002])
	fail_ret
	pl_jump  fun464()

long local pool_fun469[4]=[3,build_ref_1028,build_ref_660,build_ref_661]

pl_code local fun469
	call_c   Dyam_Pool(pool_fun469)
	call_c   Dyam_Allocate(0)
	move     &ref[1028], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	move     &ref[660], R(4)
	move     S(5), R(5)
	move     &ref[661], R(6)
	move     S(5), R(7)
	move     V(16), R(8)
	move     S(5), R(9)
	call_c   Dyam_Reg_Deallocate(5)
	pl_jump  fun237()

long local pool_fun470[5]=[4,build_ref_610,build_ref_1030,build_ref_660,build_ref_665]

pl_code local fun470
	call_c   Dyam_Pool(pool_fun470)
	call_c   Dyam_Unify_Item(&ref[610])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[1030], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	move     &ref[660], R(4)
	move     S(5), R(5)
	move     &ref[665], R(6)
	move     S(5), R(7)
	move     V(13), R(8)
	move     S(5), R(9)
	call_c   Dyam_Reg_Deallocate(5)
	pl_jump  fun237()

c_code local build_seed_25
	ret_reg &seed[25]
	call_c   build_ref_93()
	call_c   build_ref_1042()
	call_c   Dyam_Seed_Start(&ref[93],&ref[1042],I(0),fun21,1)
	call_c   build_ref_1043()
	call_c   Dyam_Seed_Add_Comp(&ref[1043],fun506,0)
	call_c   Dyam_Seed_End()
	move_ret seed[25]
	c_ret

;; TERM 1043: '*CITEM*'(lctag_filter(_B, _C, _D, _E, _F, _G), _A)
c_code local build_ref_1043
	ret_reg &ref[1043]
	call_c   build_ref_97()
	call_c   build_ref_1040()
	call_c   Dyam_Create_Binary(&ref[97],&ref[1040],V(0))
	move_ret ref[1043]
	c_ret

;; TERM 1040: lctag_filter(_B, _C, _D, _E, _F, _G)
c_code local build_ref_1040
	ret_reg &ref[1040]
	call_c   build_ref_1209()
	call_c   Dyam_Term_Start(&ref[1209],6)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[1040]
	c_ret

c_code local build_seed_168
	ret_reg &seed[168]
	call_c   build_ref_521()
	call_c   build_ref_1044()
	call_c   Dyam_Seed_Start(&ref[521],&ref[1044],I(0),fun492,0)
	call_c   Dyam_Seed_End()
	move_ret seed[168]
	c_ret

c_code local build_seed_169
	ret_reg &seed[169]
	call_c   build_ref_0()
	call_c   build_ref_99()
	call_c   Dyam_Seed_Start(&ref[0],&ref[99],&ref[99],fun11,1)
	call_c   build_ref_100()
	call_c   Dyam_Seed_Add_Comp(&ref[100],&ref[99],0)
	call_c   Dyam_Seed_End()
	move_ret seed[169]
	c_ret

long local pool_fun489[2]=[1,build_seed_169]

pl_code local fun490
	call_c   Dyam_Remove_Choice()
fun489:
	call_c   Dyam_Deallocate()
	pl_jump  fun25(&seed[169],2)


pl_code local fun491
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Reg_Load_Ptr(2,V(7))
	fail_ret
	call_c   Dyam_Reg_Load(4,V(2))
	call_c   DyALog_Mutable_Read(R(2),&R(4))
	fail_ret
	call_c   Dyam_Reg_Load_Ptr(2,V(8))
	fail_ret
	call_c   Dyam_Reg_Load(4,V(3))
	call_c   DyALog_Mutable_Read(R(2),&R(4))
	fail_ret
	call_c   Dyam_Reg_Load_Ptr(2,V(11))
	fail_ret
	call_c   Dyam_Reg_Load(4,V(6))
	call_c   DyALog_Mutable_Read(R(2),&R(4))
	fail_ret
	call_c   Dyam_Reg_Load_Ptr(2,V(9))
	fail_ret
	call_c   Dyam_Reg_Load(4,V(4))
	call_c   DyALog_Mutable_Read(R(2),&R(4))
	fail_ret
	call_c   Dyam_Reg_Load_Ptr(2,V(10))
	fail_ret
	call_c   Dyam_Reg_Load(4,V(5))
	call_c   DyALog_Mutable_Read(R(2),&R(4))
	fail_ret
	call_c   Dyam_Choice(fun490)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(2),I(0))
	fail_ret
	call_c   Dyam_Unify(V(3),I(0))
	fail_ret
	call_c   Dyam_Unify(V(6),I(0))
	fail_ret
	call_c   Dyam_Unify(V(4),I(0))
	fail_ret
	call_c   Dyam_Unify(V(5),I(0))
	fail_ret
	call_c   Dyam_Cut()
	pl_fail

;; TERM 1045: lctag_true(_B)
c_code local build_ref_1045
	ret_reg &ref[1045]
	call_c   build_ref_1257()
	call_c   Dyam_Create_Unary(&ref[1257],V(1))
	move_ret ref[1045]
	c_ret

;; TERM 1257: lctag_true
c_code local build_ref_1257
	ret_reg &ref[1257]
	call_c   Dyam_Create_Atom("lctag_true")
	move_ret ref[1257]
	c_ret

long local pool_fun492[3]=[65537,build_ref_1045,pool_fun489]

pl_code local fun492
	call_c   Dyam_Pool(pool_fun492)
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun491)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[1045])
	call_c   Dyam_Cut()
	pl_fail

;; TERM 1044: cont('$TUPPLE'(35144395923296))
c_code local build_ref_1044
	ret_reg &ref[1044]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,536739840)
	move_ret R(0)
	call_c   build_ref_1208()
	call_c   Dyam_Create_Unary(&ref[1208],R(0))
	move_ret ref[1044]
	call_c   Dyam_Set_Not_Copyable(&ref[1044])
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1046: lctag_closure(_N, _O, _B, _P)
c_code local build_ref_1046
	ret_reg &ref[1046]
	call_c   build_ref_1258()
	call_c   Dyam_Term_Start(&ref[1258],4)
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_End()
	move_ret ref[1046]
	c_ret

;; TERM 1258: lctag_closure
c_code local build_ref_1258
	ret_reg &ref[1258]
	call_c   Dyam_Create_Atom("lctag_closure")
	move_ret ref[1258]
	c_ret

;; TERM 1057: pos(_U) + _F1
c_code local build_ref_1057
	ret_reg &ref[1057]
	call_c   build_ref_254()
	call_c   build_ref_1056()
	call_c   Dyam_Create_Binary(&ref[254],&ref[1056],V(31))
	move_ret ref[1057]
	c_ret

;; TERM 1056: pos(_U)
c_code local build_ref_1056
	ret_reg &ref[1056]
	call_c   build_ref_1211()
	call_c   Dyam_Create_Unary(&ref[1211],V(20))
	move_ret ref[1056]
	c_ret

long local pool_fun499[2]=[1,build_ref_1057]

pl_code local fun499
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(14),&ref[1057])
	fail_ret
fun498:
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load_Ptr(2,V(11))
	fail_ret
	call_c   Dyam_Reg_Load(4,V(19))
	call_c   DyALog_Mutable_Read(R(2),&R(4))
	fail_ret
	call_c   Dyam_Reg_Load(0,V(20))
	call_c   Dyam_Reg_Load(2,V(15))
	call_c   Dyam_Reg_Load(4,V(19))
	move     V(32), R(6)
	move     S(5), R(7)
	pl_call  pred_lctag_add_trees_4()
	call_c   Dyam_Reg_Load_Ptr(2,V(11))
	fail_ret
	call_c   Dyam_Reg_Load(4,V(32))
	call_c   DyALog_Mutable_Write(R(2),&R(4),0)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(11))
	fail_ret
	pl_fail


long local pool_fun500[3]=[65537,build_ref_1056,pool_fun499]

pl_code local fun500
	call_c   Dyam_Update_Choice(fun114)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Choice(fun499)
	call_c   Dyam_Unify(V(14),&ref[1056])
	fail_ret
	pl_jump  fun498()

;; TERM 1055: postcat(_U, _W) + _C1
c_code local build_ref_1055
	ret_reg &ref[1055]
	call_c   build_ref_254()
	call_c   build_ref_1054()
	call_c   Dyam_Create_Binary(&ref[254],&ref[1054],V(28))
	move_ret ref[1055]
	c_ret

;; TERM 1054: postcat(_U, _W)
c_code local build_ref_1054
	ret_reg &ref[1054]
	call_c   build_ref_1212()
	call_c   Dyam_Create_Binary(&ref[1212],V(20),V(22))
	move_ret ref[1054]
	c_ret

;; TERM 1049: _U : _W
c_code local build_ref_1049
	ret_reg &ref[1049]
	call_c   build_ref_1259()
	call_c   Dyam_Create_Binary(&ref[1259],V(20),V(22))
	move_ret ref[1049]
	c_ret

;; TERM 1259: :
c_code local build_ref_1259
	ret_reg &ref[1259]
	call_c   Dyam_Create_Atom(":")
	move_ret ref[1259]
	c_ret

long local pool_fun497[3]=[2,build_ref_1055,build_ref_1049]

pl_code local fun497
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(14),&ref[1055])
	fail_ret
fun496:
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load_Ptr(2,V(10))
	fail_ret
	move     V(29), R(4)
	move     S(5), R(5)
	call_c   DyALog_Mutable_Read(R(2),&R(4))
	fail_ret
	move     &ref[1049], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(15))
	call_c   Dyam_Reg_Load(4,V(29))
	move     V(30), R(6)
	move     S(5), R(7)
	pl_call  pred_lctag_add_trees_4()
	call_c   Dyam_Reg_Load_Ptr(2,V(10))
	fail_ret
	call_c   Dyam_Reg_Load(4,V(30))
	call_c   DyALog_Mutable_Write(R(2),&R(4),0)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(10))
	fail_ret
	pl_fail


long local pool_fun501[5]=[131074,build_ref_1054,build_ref_1049,pool_fun500,pool_fun497]

pl_code local fun501
	call_c   Dyam_Update_Choice(fun500)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Choice(fun497)
	call_c   Dyam_Unify(V(14),&ref[1054])
	fail_ret
	pl_jump  fun496()

;; TERM 1053: postscan(_U) + _Z
c_code local build_ref_1053
	ret_reg &ref[1053]
	call_c   build_ref_254()
	call_c   build_ref_1052()
	call_c   Dyam_Create_Binary(&ref[254],&ref[1052],V(25))
	move_ret ref[1053]
	c_ret

;; TERM 1052: postscan(_U)
c_code local build_ref_1052
	ret_reg &ref[1052]
	call_c   build_ref_1213()
	call_c   Dyam_Create_Unary(&ref[1213],V(20))
	move_ret ref[1052]
	c_ret

long local pool_fun495[2]=[1,build_ref_1053]

pl_code local fun495
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(14),&ref[1053])
	fail_ret
fun494:
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load_Ptr(2,V(9))
	fail_ret
	move     V(26), R(4)
	move     S(5), R(5)
	call_c   DyALog_Mutable_Read(R(2),&R(4))
	fail_ret
	call_c   Dyam_Reg_Load(0,V(20))
	call_c   Dyam_Reg_Load(2,V(15))
	call_c   Dyam_Reg_Load(4,V(26))
	move     V(27), R(6)
	move     S(5), R(7)
	pl_call  pred_lctag_add_trees_4()
	call_c   Dyam_Reg_Load_Ptr(2,V(9))
	fail_ret
	call_c   Dyam_Reg_Load(4,V(27))
	call_c   DyALog_Mutable_Write(R(2),&R(4),0)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(9))
	fail_ret
	pl_fail


long local pool_fun502[4]=[131073,build_ref_1052,pool_fun501,pool_fun495]

pl_code local fun502
	call_c   Dyam_Update_Choice(fun501)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Choice(fun495)
	call_c   Dyam_Unify(V(14),&ref[1052])
	fail_ret
	pl_jump  fun494()

;; TERM 1051: '$VAR'(_Y, ['$SET$',lctag_labels(epsilon, true, no_epsilon),3])
c_code local build_ref_1051
	ret_reg &ref[1051]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1206()
	call_c   build_ref_305()
	call_c   build_ref_487()
	call_c   build_ref_1207()
	call_c   Dyam_Term_Start(&ref[1206],3)
	call_c   Dyam_Term_Arg(&ref[305])
	call_c   Dyam_Term_Arg(&ref[487])
	call_c   Dyam_Term_Arg(&ref[1207])
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   Dyam_Term_Start(I(8),3)
	call_c   Dyam_Term_Arg(V(24))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(N(3))
	call_c   Dyam_Term_End()
	move_ret ref[1051]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun503[4]=[65538,build_ref_1051,build_ref_1045,pool_fun502]

pl_code local fun503
	call_c   Dyam_Update_Choice(fun502)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(14),&ref[1051])
	fail_ret
	call_c   Dyam_Cut()
	call_c   DYAM_evpred_assert_1(&ref[1045])
	pl_fail

;; TERM 1050: coanchor(_U, _W)
c_code local build_ref_1050
	ret_reg &ref[1050]
	call_c   build_ref_1063()
	call_c   Dyam_Create_Binary(&ref[1063],V(20),V(22))
	move_ret ref[1050]
	c_ret

long local pool_fun504[4]=[65538,build_ref_1050,build_ref_1049,pool_fun503]

pl_code local fun504
	call_c   Dyam_Update_Choice(fun503)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(14),&ref[1050])
	fail_ret
	call_c   Dyam_Cut()
	move     &ref[1049], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(15))
	call_c   Dyam_Reg_Load(4,V(16))
	move     V(23), R(6)
	move     S(5), R(7)
	pl_call  pred_lctag_add_trees_4()
	call_c   Dyam_Reg_Load_Ptr(2,V(8))
	fail_ret
	call_c   Dyam_Reg_Load(4,V(23))
	call_c   DyALog_Mutable_Write(R(2),&R(4),0)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(8))
	fail_ret
	pl_fail

;; TERM 1048: anchor(_U, _W)
c_code local build_ref_1048
	ret_reg &ref[1048]
	call_c   build_ref_1076()
	call_c   Dyam_Create_Binary(&ref[1076],V(20),V(22))
	move_ret ref[1048]
	c_ret

long local pool_fun505[4]=[65538,build_ref_1048,build_ref_1049,pool_fun504]

pl_code local fun505
	call_c   Dyam_Update_Choice(fun504)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(14),&ref[1048])
	fail_ret
	call_c   Dyam_Cut()
	move     &ref[1049], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(15))
	call_c   Dyam_Reg_Load(4,V(16))
	move     V(23), R(6)
	move     S(5), R(7)
	pl_call  pred_lctag_add_trees_4()
	call_c   Dyam_Reg_Load_Ptr(2,V(8))
	fail_ret
	call_c   Dyam_Reg_Load(4,V(23))
	call_c   DyALog_Mutable_Write(R(2),&R(4),0)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(8))
	fail_ret
	pl_fail

;; TERM 1047: scan(_U)
c_code local build_ref_1047
	ret_reg &ref[1047]
	call_c   build_ref_1062()
	call_c   Dyam_Create_Unary(&ref[1062],V(20))
	move_ret ref[1047]
	c_ret

pl_code local fun493
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Reg_Load(0,V(20))
	call_c   Dyam_Reg_Load(2,V(15))
	call_c   Dyam_Reg_Load(4,V(17))
	move     V(21), R(6)
	move     S(5), R(7)
	pl_call  pred_lctag_add_trees_4()
	call_c   Dyam_Reg_Load_Ptr(2,V(7))
	fail_ret
	call_c   Dyam_Reg_Load(4,V(21))
	call_c   DyALog_Mutable_Write(R(2),&R(4),0)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(7))
	fail_ret
	pl_fail

long local pool_fun506[7]=[65541,build_ref_1043,build_seed_168,build_ref_1046,build_ref_1047,build_ref_1045,pool_fun505]

pl_code local fun506
	call_c   Dyam_Pool(pool_fun506)
	call_c   Dyam_Unify_Item(&ref[1043])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load_Ptr(2,V(7))
	fail_ret
	move     I(0), R(4)
	move     0, R(5)
	call_c   DyALog_Mutable_Write(R(2),&R(4),0)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(7))
	fail_ret
	call_c   Dyam_Reg_Load_Ptr(2,V(8))
	fail_ret
	move     I(0), R(4)
	move     0, R(5)
	call_c   DyALog_Mutable_Write(R(2),&R(4),0)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(8))
	fail_ret
	call_c   Dyam_Reg_Load_Ptr(2,V(9))
	fail_ret
	move     I(0), R(4)
	move     0, R(5)
	call_c   DyALog_Mutable_Write(R(2),&R(4),0)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(9))
	fail_ret
	call_c   Dyam_Reg_Load_Ptr(2,V(10))
	fail_ret
	move     I(0), R(4)
	move     0, R(5)
	call_c   DyALog_Mutable_Write(R(2),&R(4),0)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(10))
	fail_ret
	call_c   Dyam_Reg_Load_Ptr(2,V(11))
	fail_ret
	move     I(0), R(4)
	move     0, R(5)
	call_c   DyALog_Mutable_Write(R(2),&R(4),0)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(11))
	fail_ret
	pl_call  fun175(&seed[168],12)
	pl_call  Callret_2(V(12),&ref[1046])
	pl_call  Object_1(V(12))
	call_c   Dyam_Reg_Load_Ptr(2,V(8))
	fail_ret
	move     V(16), R(4)
	move     S(5), R(5)
	call_c   DyALog_Mutable_Read(R(2),&R(4))
	fail_ret
	call_c   Dyam_Reg_Load_Ptr(2,V(7))
	fail_ret
	move     V(17), R(4)
	move     S(5), R(5)
	call_c   DyALog_Mutable_Read(R(2),&R(4))
	fail_ret
	call_c   Dyam_Reg_Load_Ptr(2,V(10))
	fail_ret
	move     V(18), R(4)
	move     S(5), R(5)
	call_c   DyALog_Mutable_Read(R(2),&R(4))
	fail_ret
	call_c   Dyam_Reg_Load_Ptr(2,V(11))
	fail_ret
	move     V(19), R(4)
	move     S(5), R(5)
	call_c   DyALog_Mutable_Read(R(2),&R(4))
	fail_ret
	call_c   Dyam_Choice(fun505)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(14),&ref[1047])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun493)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_variable(V(20))
	fail_ret
	call_c   Dyam_Cut()
	call_c   DYAM_evpred_assert_1(&ref[1045])
	pl_fail

;; TERM 1042: '*FIRST*'(lctag_filter(_B, _C, _D, _E, _F, _G)) :> []
c_code local build_ref_1042
	ret_reg &ref[1042]
	call_c   build_ref_1041()
	call_c   Dyam_Create_Binary(I(9),&ref[1041],I(0))
	move_ret ref[1042]
	c_ret

;; TERM 1041: '*FIRST*'(lctag_filter(_B, _C, _D, _E, _F, _G))
c_code local build_ref_1041
	ret_reg &ref[1041]
	call_c   build_ref_93()
	call_c   build_ref_1040()
	call_c   Dyam_Create_Unary(&ref[93],&ref[1040])
	move_ret ref[1041]
	c_ret

c_code local build_seed_15
	ret_reg &seed[15]
	call_c   build_ref_63()
	call_c   build_ref_1060()
	call_c   Dyam_Seed_Start(&ref[63],&ref[1060],I(0),fun0,1)
	call_c   build_ref_1061()
	call_c   Dyam_Seed_Add_Comp(&ref[1061],fun543,0)
	call_c   Dyam_Seed_End()
	move_ret seed[15]
	c_ret

;; TERM 1061: '*PROLOG-ITEM*'{top=> lctag_first(_B, tag_node{id=> _C, label=> _D, children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}, _O, _P), cont=> _A}
c_code local build_ref_1061
	ret_reg &ref[1061]
	call_c   build_ref_1126()
	call_c   build_ref_1058()
	call_c   Dyam_Create_Binary(&ref[1126],&ref[1058],V(0))
	move_ret ref[1061]
	c_ret

;; TERM 1058: lctag_first(_B, tag_node{id=> _C, label=> _D, children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}, _O, _P)
c_code local build_ref_1058
	ret_reg &ref[1058]
	call_c   build_ref_1127()
	call_c   build_ref_991()
	call_c   Dyam_Term_Start(&ref[1127],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(&ref[991])
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_End()
	move_ret ref[1058]
	c_ret

;; TERM 1117: '$VAR'(_J1, ['$SET$',adj(no, yes, strict, atmostone, one, sync),43])
c_code local build_ref_1117
	ret_reg &ref[1117]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1178()
	call_c   build_ref_706()
	call_c   build_ref_319()
	call_c   build_ref_673()
	call_c   build_ref_685()
	call_c   build_ref_686()
	call_c   build_ref_623()
	call_c   Dyam_Term_Start(&ref[1178],6)
	call_c   Dyam_Term_Arg(&ref[706])
	call_c   Dyam_Term_Arg(&ref[319])
	call_c   Dyam_Term_Arg(&ref[673])
	call_c   Dyam_Term_Arg(&ref[685])
	call_c   Dyam_Term_Arg(&ref[686])
	call_c   Dyam_Term_Arg(&ref[623])
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   Dyam_Term_Start(I(8),3)
	call_c   Dyam_Term_Arg(V(35))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(N(43))
	call_c   Dyam_Term_End()
	move_ret ref[1117]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_170
	ret_reg &seed[170]
	call_c   build_ref_176()
	call_c   build_ref_1091()
	call_c   Dyam_Seed_Start(&ref[176],&ref[1091],I(0),fun11,1)
	call_c   build_ref_1094()
	call_c   Dyam_Seed_Add_Comp(&ref[1094],&ref[1091],0)
	call_c   Dyam_Seed_End()
	move_ret seed[170]
	c_ret

;; TERM 1094: '*PROLOG-FIRST*'(lctag_first(_B, _E, _O, _P)) :> '$$HOLE$$'
c_code local build_ref_1094
	ret_reg &ref[1094]
	call_c   build_ref_1093()
	call_c   Dyam_Create_Binary(I(9),&ref[1093],I(7))
	move_ret ref[1094]
	c_ret

;; TERM 1093: '*PROLOG-FIRST*'(lctag_first(_B, _E, _O, _P))
c_code local build_ref_1093
	ret_reg &ref[1093]
	call_c   build_ref_64()
	call_c   build_ref_1092()
	call_c   Dyam_Create_Unary(&ref[64],&ref[1092])
	move_ret ref[1093]
	c_ret

;; TERM 1092: lctag_first(_B, _E, _O, _P)
c_code local build_ref_1092
	ret_reg &ref[1092]
	call_c   build_ref_1127()
	call_c   Dyam_Term_Start(&ref[1127],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_End()
	move_ret ref[1092]
	c_ret

;; TERM 1091: '*PROLOG-ITEM*'{top=> lctag_first(_B, _E, _O, _P), cont=> _A}
c_code local build_ref_1091
	ret_reg &ref[1091]
	call_c   build_ref_1126()
	call_c   build_ref_1092()
	call_c   Dyam_Create_Binary(&ref[1126],&ref[1092],V(0))
	move_ret ref[1091]
	c_ret

long local pool_fun536[3]=[2,build_ref_1117,build_seed_170]

pl_code local fun536
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(6),&ref[1117])
	fail_ret
	call_c   Dyam_Unify(V(8),V(9))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_jump  fun38(&seed[170],12)

;; TERM 986: '$VAR'(_B1, ['$SET$',adj(no, yes, strict, atmostone, one, sync),62])
c_code local build_ref_986
	ret_reg &ref[986]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1178()
	call_c   build_ref_706()
	call_c   build_ref_319()
	call_c   build_ref_673()
	call_c   build_ref_685()
	call_c   build_ref_686()
	call_c   build_ref_623()
	call_c   Dyam_Term_Start(&ref[1178],6)
	call_c   Dyam_Term_Arg(&ref[706])
	call_c   Dyam_Term_Arg(&ref[319])
	call_c   Dyam_Term_Arg(&ref[673])
	call_c   Dyam_Term_Arg(&ref[685])
	call_c   Dyam_Term_Arg(&ref[686])
	call_c   Dyam_Term_Arg(&ref[623])
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   Dyam_Term_Start(I(8),3)
	call_c   Dyam_Term_Arg(V(27))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(N(62))
	call_c   Dyam_Term_End()
	move_ret ref[986]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1088: '$VAR'(_C1, ['$SET$',adj(no, yes, strict, atmostone, one, sync),62])
c_code local build_ref_1088
	ret_reg &ref[1088]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1178()
	call_c   build_ref_706()
	call_c   build_ref_319()
	call_c   build_ref_673()
	call_c   build_ref_685()
	call_c   build_ref_686()
	call_c   build_ref_623()
	call_c   Dyam_Term_Start(&ref[1178],6)
	call_c   Dyam_Term_Arg(&ref[706])
	call_c   Dyam_Term_Arg(&ref[319])
	call_c   Dyam_Term_Arg(&ref[673])
	call_c   Dyam_Term_Arg(&ref[685])
	call_c   Dyam_Term_Arg(&ref[686])
	call_c   Dyam_Term_Arg(&ref[623])
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   Dyam_Term_Start(I(8),3)
	call_c   Dyam_Term_Arg(V(28))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(N(62))
	call_c   Dyam_Term_End()
	move_ret ref[1088]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1116: '$CLOSURE'('$fun'(535, 0, 1129166760), '$TUPPLE'(35144395407004))
c_code local build_ref_1116
	ret_reg &ref[1116]
	call_c   build_ref_1115()
	call_c   Dyam_Closure_Aux(fun535,&ref[1115])
	move_ret ref[1116]
	c_ret

;; TERM 1098: adjkind((_Z / _A1), right)
c_code local build_ref_1098
	ret_reg &ref[1098]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1170()
	call_c   Dyam_Create_Binary(&ref[1170],V(25),V(26))
	move_ret R(0)
	call_c   build_ref_1136()
	call_c   build_ref_458()
	call_c   Dyam_Create_Binary(&ref[1136],R(0),&ref[458])
	move_ret ref[1098]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 834: '$VAR'(_H1, ['$SET$',adj(no, yes, strict, atmostone, one, sync),62])
c_code local build_ref_834
	ret_reg &ref[834]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1178()
	call_c   build_ref_706()
	call_c   build_ref_319()
	call_c   build_ref_673()
	call_c   build_ref_685()
	call_c   build_ref_686()
	call_c   build_ref_623()
	call_c   Dyam_Term_Start(&ref[1178],6)
	call_c   Dyam_Term_Arg(&ref[706])
	call_c   Dyam_Term_Arg(&ref[319])
	call_c   Dyam_Term_Arg(&ref[673])
	call_c   Dyam_Term_Arg(&ref[685])
	call_c   Dyam_Term_Arg(&ref[686])
	call_c   Dyam_Term_Arg(&ref[623])
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   Dyam_Term_Start(I(8),3)
	call_c   Dyam_Term_Arg(V(33))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(N(62))
	call_c   Dyam_Term_End()
	move_ret ref[834]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1113: '$CLOSURE'('$fun'(528, 0, 1129125404), '$TUPPLE'(35144395406752))
c_code local build_ref_1113
	ret_reg &ref[1113]
	call_c   build_ref_1112()
	call_c   Dyam_Closure_Aux(fun528,&ref[1112])
	move_ret ref[1113]
	c_ret

c_code local build_seed_173
	ret_reg &seed[173]
	call_c   build_ref_176()
	call_c   build_ref_1107()
	call_c   Dyam_Seed_Start(&ref[176],&ref[1107],I(0),fun11,1)
	call_c   build_ref_1110()
	call_c   Dyam_Seed_Add_Comp(&ref[1110],&ref[1107],0)
	call_c   Dyam_Seed_End()
	move_ret seed[173]
	c_ret

;; TERM 1110: '*PROLOG-FIRST*'(lctag_first(_B, _E, _E1, _I1)) :> '$$HOLE$$'
c_code local build_ref_1110
	ret_reg &ref[1110]
	call_c   build_ref_1109()
	call_c   Dyam_Create_Binary(I(9),&ref[1109],I(7))
	move_ret ref[1110]
	c_ret

;; TERM 1109: '*PROLOG-FIRST*'(lctag_first(_B, _E, _E1, _I1))
c_code local build_ref_1109
	ret_reg &ref[1109]
	call_c   build_ref_64()
	call_c   build_ref_1108()
	call_c   Dyam_Create_Unary(&ref[64],&ref[1108])
	move_ret ref[1109]
	c_ret

;; TERM 1108: lctag_first(_B, _E, _E1, _I1)
c_code local build_ref_1108
	ret_reg &ref[1108]
	call_c   build_ref_1127()
	call_c   Dyam_Term_Start(&ref[1127],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(30))
	call_c   Dyam_Term_Arg(V(34))
	call_c   Dyam_Term_End()
	move_ret ref[1108]
	c_ret

;; TERM 1107: '*PROLOG-ITEM*'{top=> lctag_first(_B, _E, _E1, _I1), cont=> '$CLOSURE'('$fun'(527, 0, 1129118296), '$TUPPLE'(35144395406700))}
c_code local build_ref_1107
	ret_reg &ref[1107]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Start_Tupple(0,303587328)
	call_c   Dyam_Almost_End_Tupple(29,142606336)
	move_ret R(0)
	call_c   Dyam_Closure_Aux(fun527,R(0))
	move_ret R(0)
	call_c   build_ref_1126()
	call_c   build_ref_1108()
	call_c   Dyam_Create_Binary(&ref[1126],&ref[1108],R(0))
	move_ret ref[1107]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

pl_code local fun526
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(14),V(30))
	fail_ret
	call_c   Dyam_Unify(V(15),V(34))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_jump  Follow_Cont(V(0))

;; TERM 1069: '$CLOSURE'('$fun'(507, 0, 1128972528), '$TUPPLE'(35144395141188))
c_code local build_ref_1069
	ret_reg &ref[1069]
	call_c   build_ref_1068()
	call_c   Dyam_Closure_Aux(fun507,&ref[1068])
	move_ret ref[1069]
	c_ret

;; TERM 1066: small_sorted_tree_add(_T, _U, _P)
c_code local build_ref_1066
	ret_reg &ref[1066]
	call_c   build_ref_1118()
	call_c   Dyam_Term_Start(&ref[1118],3)
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_End()
	move_ret ref[1066]
	c_ret

pl_code local fun507
	call_c   build_ref_1066()
	call_c   Dyam_Allocate(0)
	pl_call  Object_1(&ref[1066])
	call_c   Dyam_Deallocate()
	pl_jump  Follow_Cont(V(0))

;; TERM 1068: '$TUPPLE'(35144395141188)
c_code local build_ref_1068
	ret_reg &ref[1068]
	call_c   Dyam_Create_Simple_Tupple(0,268444416)
	move_ret ref[1068]
	c_ret

;; TERM 1106: adjright(_D, _I, _J)
c_code local build_ref_1106
	ret_reg &ref[1106]
	call_c   build_ref_1140()
	call_c   Dyam_Term_Start(&ref[1140],3)
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret ref[1106]
	c_ret

long local pool_fun527[4]=[3,build_ref_305,build_ref_1069,build_ref_1106]

pl_code local fun527
	call_c   Dyam_Pool(pool_fun527)
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun526)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(30),&ref[305])
	fail_ret
	call_c   Dyam_Cut()
	move     &ref[1069], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	move     &ref[1106], R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Load(6,V(14))
	move     V(19), R(8)
	move     S(5), R(9)
	move     V(20), R(10)
	move     S(5), R(11)
	call_c   Dyam_Reg_Deallocate(6)
	pl_jump  fun56()

long local pool_fun528[2]=[1,build_seed_173]

pl_code local fun528
	call_c   Dyam_Pool(pool_fun528)
	pl_jump  fun38(&seed[173],12)

;; TERM 1112: '$TUPPLE'(35144395406752)
c_code local build_ref_1112
	ret_reg &ref[1112]
	call_c   Dyam_Create_Simple_Tupple(0,454582272)
	move_ret ref[1112]
	c_ret

long local pool_fun529[4]=[3,build_ref_1098,build_ref_834,build_ref_1113]

pl_code local fun529
	call_c   Dyam_Remove_Choice()
	pl_call  Object_1(&ref[1098])
	call_c   Dyam_Unify(V(12),&ref[834])
	fail_ret
	move     &ref[1113], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Load(4,V(3))
	call_c   Dyam_Reg_Load(6,V(8))
	call_c   Dyam_Reg_Load(8,V(9))
	call_c   Dyam_Reg_Deallocate(5)
fun525:
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Reg_Bind(2,V(5))
	call_c   Dyam_Multi_Reg_Bind(4,2,3)
	pl_call  fun25(&seed[171],1)
	pl_call  fun55(&seed[172],2,V(5))
	call_c   Dyam_Deallocate()
	pl_ret


;; TERM 1089: false
c_code local build_ref_1089
	ret_reg &ref[1089]
	call_c   Dyam_Create_Atom("false")
	move_ret ref[1089]
	c_ret

;; TERM 1097: '$CLOSURE'('$fun'(523, 0, 1129072964), '$TUPPLE'(35144395406224))
c_code local build_ref_1097
	ret_reg &ref[1097]
	call_c   build_ref_1096()
	call_c   Dyam_Closure_Aux(fun523,&ref[1096])
	move_ret ref[1097]
	c_ret

long local pool_fun522[4]=[3,build_ref_1089,build_ref_487,build_seed_170]

pl_code local fun522
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Reg_Load_Ptr(2,V(29))
	fail_ret
	move     &ref[1089], R(4)
	move     0, R(5)
	call_c   DyALog_Mutable_Read(R(2),&R(4))
	fail_ret
	call_c   Dyam_Reg_Load_Ptr(2,V(29))
	fail_ret
	move     &ref[487], R(4)
	move     0, R(5)
	call_c   DyALog_Mutable_Write(R(2),&R(4),0)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(29))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_jump  fun38(&seed[170],12)

pl_code local fun519
	call_c   Dyam_Remove_Choice()
fun518:
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(14),V(30))
	fail_ret
	pl_call  Object_1(&ref[1066])
	call_c   Dyam_Deallocate()
	pl_jump  Follow_Cont(V(0))


;; TERM 1090: _F1 + '$VAR'(_G1, ['$SET$',lctag_labels(epsilon, true, no_epsilon),3])
c_code local build_ref_1090
	ret_reg &ref[1090]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1206()
	call_c   build_ref_305()
	call_c   build_ref_487()
	call_c   build_ref_1207()
	call_c   Dyam_Term_Start(&ref[1206],3)
	call_c   Dyam_Term_Arg(&ref[305])
	call_c   Dyam_Term_Arg(&ref[487])
	call_c   Dyam_Term_Arg(&ref[1207])
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   Dyam_Term_Start(I(8),3)
	call_c   Dyam_Term_Arg(V(32))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(N(3))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_254()
	call_c   Dyam_Create_Binary(&ref[254],V(31),R(0))
	move_ret ref[1090]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun520[3]=[2,build_ref_1066,build_ref_1090]

pl_code local fun521
	call_c   Dyam_Remove_Choice()
fun520:
	call_c   Dyam_Choice(fun519)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(30),&ref[1090])
	fail_ret
	call_c   Dyam_Cut()
	pl_fail


long local pool_fun523[4]=[131073,build_ref_305,pool_fun522,pool_fun520]

pl_code local fun523
	call_c   Dyam_Pool(pool_fun523)
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun522)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Choice(fun521)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(30),&ref[305])
	fail_ret
	call_c   Dyam_Cut()
	pl_fail

;; TERM 1096: '$TUPPLE'(35144395406224)
c_code local build_ref_1096
	ret_reg &ref[1096]
	call_c   Dyam_Start_Tupple(0,419455744)
	call_c   Dyam_Almost_End_Tupple(29,402653184)
	move_ret ref[1096]
	c_ret

;; TERM 1070: adjleft(_D, _I, _S)
c_code local build_ref_1070
	ret_reg &ref[1070]
	call_c   build_ref_1183()
	call_c   Dyam_Term_Start(&ref[1183],3)
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_End()
	move_ret ref[1070]
	c_ret

long local pool_fun530[5]=[65539,build_ref_1089,build_ref_1097,build_ref_1070,pool_fun529]

pl_code local fun533
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(9),V(18))
	fail_ret
fun530:
	call_c   Dyam_Choice(fun529)
	call_c   Dyam_Reg_Load_Ptr(2,V(29))
	fail_ret
	move     &ref[1089], R(4)
	move     0, R(5)
	call_c   DyALog_Mutable_Write(R(2),&R(4),0)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(29))
	fail_ret
	move     &ref[1097], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	move     &ref[1070], R(4)
	move     S(5), R(5)
	move     V(30), R(6)
	move     S(5), R(7)
	move     V(19), R(8)
	move     S(5), R(9)
	move     V(20), R(10)
	move     S(5), R(11)
	call_c   Dyam_Reg_Deallocate(6)
	pl_jump  fun56()


pl_code local fun532
	call_c   Dyam_Remove_Choice()
fun531:
	call_c   Dyam_Cut()
	pl_jump  fun530()


long local pool_fun534[3]=[131072,pool_fun530,pool_fun530]

pl_code local fun534
	call_c   Dyam_Update_Choice(fun533)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Choice(fun532)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(8),V(9))
	fail_ret
	call_c   Dyam_Cut()
	pl_fail

;; TERM 1065: lctag_safe(_D)
c_code local build_ref_1065
	ret_reg &ref[1065]
	call_c   build_ref_1134()
	call_c   Dyam_Create_Unary(&ref[1134],V(3))
	move_ret ref[1065]
	c_ret

long local pool_fun535[4]=[131073,build_ref_1065,pool_fun534,pool_fun530]

pl_code local fun535
	call_c   Dyam_Pool(pool_fun535)
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun534)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[1065])
	call_c   Dyam_Cut()
	pl_jump  fun530()

;; TERM 1115: '$TUPPLE'(35144395407004)
c_code local build_ref_1115
	ret_reg &ref[1115]
	call_c   Dyam_Create_Simple_Tupple(0,454648844)
	move_ret ref[1115]
	c_ret

long local pool_fun537[6]=[65540,build_ref_986,build_ref_1088,build_ref_1116,build_ref_451,pool_fun536]

pl_code local fun537
	call_c   Dyam_Remove_Choice()
	call_c   DYAM_evpred_functor(V(8),V(25),V(26))
	fail_ret
	call_c   Dyam_Choice(fun536)
	call_c   Dyam_Unify(V(6),&ref[986])
	fail_ret
	call_c   Dyam_Unify(V(11),&ref[1088])
	fail_ret
	move     &ref[1116], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Load(4,V(3))
	move     &ref[451], R(6)
	move     0, R(7)
	call_c   Dyam_Reg_Load(8,V(8))
	move     V(18), R(10)
	move     S(5), R(11)
	call_c   Dyam_Reg_Deallocate(6)
	pl_jump  fun155()

long local pool_fun517[2]=[1,build_ref_305]

pl_code local fun517
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(14),&ref[305])
	fail_ret
fun511:
	call_c   Dyam_Unify(V(15),I(0))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_jump  Follow_Cont(V(0))


;; TERM 1087: noop(_Y)
c_code local build_ref_1087
	ret_reg &ref[1087]
	call_c   build_ref_755()
	call_c   Dyam_Create_Unary(&ref[755],V(24))
	move_ret ref[1087]
	c_ret

long local pool_fun538[5]=[131074,build_ref_1086,build_ref_1087,pool_fun537,pool_fun517]

pl_code local fun538
	call_c   Dyam_Update_Choice(fun537)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(5),&ref[1086])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun517)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(3),&ref[1087])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(24))
	call_c   Dyam_Reg_Load(2,V(14))
	pl_call  pred_noop_first_2()
	pl_jump  fun511()

pl_code local fun516
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(14),&ref[487])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_jump  Follow_Cont(V(0))

long local pool_fun539[6]=[65540,build_ref_992,build_ref_487,build_ref_676,build_ref_305,pool_fun538]

pl_code local fun539
	call_c   Dyam_Update_Choice(fun538)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(5),&ref[992])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(15),I(0))
	fail_ret
	call_c   Dyam_Choice(fun516)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[676])
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(14),&ref[305])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_jump  Follow_Cont(V(0))

;; TERM 1084: '$CLOSURE'('$fun'(515, 0, 1129029340), '$TUPPLE'(35144395405572))
c_code local build_ref_1084
	ret_reg &ref[1084]
	call_c   build_ref_1083()
	call_c   Dyam_Closure_Aux(fun515,&ref[1083])
	move_ret ref[1084]
	c_ret

;; TERM 1081: small_sorted_tree_add(_X, _U, _P)
c_code local build_ref_1081
	ret_reg &ref[1081]
	call_c   build_ref_1118()
	call_c   Dyam_Term_Start(&ref[1118],3)
	call_c   Dyam_Term_Arg(V(23))
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_End()
	move_ret ref[1081]
	c_ret

pl_code local fun515
	call_c   build_ref_1081()
	call_c   Dyam_Allocate(0)
	pl_call  Object_1(&ref[1081])
	call_c   Dyam_Deallocate()
	pl_jump  Follow_Cont(V(0))

;; TERM 1083: '$TUPPLE'(35144395405572)
c_code local build_ref_1083
	ret_reg &ref[1083]
	call_c   Dyam_Create_Simple_Tupple(0,268443936)
	move_ret ref[1083]
	c_ret

;; TERM 1085: subst(_D, _I)
c_code local build_ref_1085
	ret_reg &ref[1085]
	call_c   build_ref_1080()
	call_c   Dyam_Create_Binary(&ref[1080],V(3),V(8))
	move_ret ref[1085]
	c_ret

long local pool_fun540[5]=[65539,build_ref_1080,build_ref_1084,build_ref_1085,pool_fun539]

pl_code local fun540
	call_c   Dyam_Update_Choice(fun539)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(5),&ref[1080])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(8),V(9))
	fail_ret
	move     &ref[1084], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	move     &ref[1085], R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Load(6,V(14))
	move     V(23), R(8)
	move     S(5), R(9)
	move     V(20), R(10)
	move     S(5), R(11)
	call_c   Dyam_Reg_Deallocate(6)
	pl_jump  fun56()

;; TERM 1075: scan(_K)
c_code local build_ref_1075
	ret_reg &ref[1075]
	call_c   build_ref_1062()
	call_c   Dyam_Create_Unary(&ref[1062],V(10))
	move_ret ref[1075]
	c_ret

long local pool_fun512[2]=[1,build_ref_1075]

pl_code local fun512
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(14),&ref[1075])
	fail_ret
	pl_jump  fun511()

;; TERM 1079: anchor(_D, _J)
c_code local build_ref_1079
	ret_reg &ref[1079]
	call_c   build_ref_1076()
	call_c   Dyam_Create_Binary(&ref[1076],V(3),V(9))
	move_ret ref[1079]
	c_ret

long local pool_fun514[3]=[65537,build_ref_1079,pool_fun512]

pl_code local fun514
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(8),V(9))
	fail_ret
	call_c   Dyam_Choice(fun512)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_variable(V(10))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(14),&ref[1079])
	fail_ret
	pl_jump  fun511()

;; TERM 1077: '$VAR'(_V, ['$SET$',adj(no, yes, strict, atmostone, one, sync),62])
c_code local build_ref_1077
	ret_reg &ref[1077]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1178()
	call_c   build_ref_706()
	call_c   build_ref_319()
	call_c   build_ref_673()
	call_c   build_ref_685()
	call_c   build_ref_686()
	call_c   build_ref_623()
	call_c   Dyam_Term_Start(&ref[1178],6)
	call_c   Dyam_Term_Arg(&ref[706])
	call_c   Dyam_Term_Arg(&ref[319])
	call_c   Dyam_Term_Arg(&ref[673])
	call_c   Dyam_Term_Arg(&ref[685])
	call_c   Dyam_Term_Arg(&ref[686])
	call_c   Dyam_Term_Arg(&ref[623])
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   Dyam_Term_Start(I(8),3)
	call_c   Dyam_Term_Arg(V(21))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(N(62))
	call_c   Dyam_Term_End()
	move_ret ref[1077]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1078: '$VAR'(_W, ['$SET$',adj(no, yes, strict, atmostone, one, sync),62])
c_code local build_ref_1078
	ret_reg &ref[1078]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1178()
	call_c   build_ref_706()
	call_c   build_ref_319()
	call_c   build_ref_673()
	call_c   build_ref_685()
	call_c   build_ref_686()
	call_c   build_ref_623()
	call_c   Dyam_Term_Start(&ref[1178],6)
	call_c   Dyam_Term_Arg(&ref[706])
	call_c   Dyam_Term_Arg(&ref[319])
	call_c   Dyam_Term_Arg(&ref[673])
	call_c   Dyam_Term_Arg(&ref[685])
	call_c   Dyam_Term_Arg(&ref[686])
	call_c   Dyam_Term_Arg(&ref[623])
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   Dyam_Term_Start(I(8),3)
	call_c   Dyam_Term_Arg(V(22))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(N(62))
	call_c   Dyam_Term_End()
	move_ret ref[1078]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1073: '$CLOSURE'('$fun'(510, 0, 1128990320), '$TUPPLE'(35144395141356))
c_code local build_ref_1073
	ret_reg &ref[1073]
	call_c   build_ref_1072()
	call_c   Dyam_Closure_Aux(fun510,&ref[1072])
	move_ret ref[1073]
	c_ret

long local pool_fun508[3]=[2,build_ref_1069,build_ref_1070]

pl_code local fun509
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(9),V(18))
	fail_ret
fun508:
	move     &ref[1069], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	move     &ref[1070], R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Load(6,V(14))
	move     V(19), R(8)
	move     S(5), R(9)
	move     V(20), R(10)
	move     S(5), R(11)
	call_c   Dyam_Reg_Deallocate(6)
	pl_jump  fun56()


long local pool_fun510[4]=[131073,build_ref_1065,pool_fun508,pool_fun508]

pl_code local fun510
	call_c   Dyam_Pool(pool_fun510)
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun509)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[1065])
	call_c   Dyam_Cut()
	pl_jump  fun508()

;; TERM 1072: '$TUPPLE'(35144395141356)
c_code local build_ref_1072
	ret_reg &ref[1072]
	call_c   Dyam_Create_Simple_Tupple(0,303588352)
	move_ret ref[1072]
	c_ret

long local pool_fun541[8]=[131077,build_ref_1076,build_ref_1077,build_ref_1078,build_ref_1073,build_ref_451,pool_fun540,pool_fun514]

pl_code local fun541
	call_c   Dyam_Update_Choice(fun540)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(5),&ref[1076])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun514)
	call_c   Dyam_Unify(V(6),&ref[1077])
	fail_ret
	call_c   Dyam_Unify(V(11),&ref[1078])
	fail_ret
	move     &ref[1073], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Load(4,V(3))
	move     &ref[451], R(6)
	move     0, R(7)
	call_c   Dyam_Reg_Load(8,V(8))
	move     V(18), R(10)
	move     S(5), R(11)
	call_c   Dyam_Reg_Deallocate(6)
	pl_jump  fun155()

;; TERM 1074: coanchor(_D, _J)
c_code local build_ref_1074
	ret_reg &ref[1074]
	call_c   build_ref_1063()
	call_c   Dyam_Create_Binary(&ref[1063],V(3),V(9))
	move_ret ref[1074]
	c_ret

long local pool_fun513[3]=[65537,build_ref_1074,pool_fun512]

pl_code local fun513
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(8),V(9))
	fail_ret
	call_c   Dyam_Choice(fun512)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_variable(V(10))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(14),&ref[1074])
	fail_ret
	pl_jump  fun511()

;; TERM 1064: '$VAR'(_R, ['$SET$',adj(no, yes, strict, atmostone, one, sync),62])
c_code local build_ref_1064
	ret_reg &ref[1064]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1178()
	call_c   build_ref_706()
	call_c   build_ref_319()
	call_c   build_ref_673()
	call_c   build_ref_685()
	call_c   build_ref_686()
	call_c   build_ref_623()
	call_c   Dyam_Term_Start(&ref[1178],6)
	call_c   Dyam_Term_Arg(&ref[706])
	call_c   Dyam_Term_Arg(&ref[319])
	call_c   Dyam_Term_Arg(&ref[673])
	call_c   Dyam_Term_Arg(&ref[685])
	call_c   Dyam_Term_Arg(&ref[686])
	call_c   Dyam_Term_Arg(&ref[623])
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   Dyam_Term_Start(I(8),3)
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(N(62))
	call_c   Dyam_Term_End()
	move_ret ref[1064]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun542[8]=[131077,build_ref_1063,build_ref_980,build_ref_1064,build_ref_1073,build_ref_451,pool_fun541,pool_fun513]

pl_code local fun542
	call_c   Dyam_Update_Choice(fun541)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(5),&ref[1063])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun513)
	call_c   Dyam_Unify(V(6),&ref[980])
	fail_ret
	call_c   Dyam_Unify(V(11),&ref[1064])
	fail_ret
	move     &ref[1073], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Load(4,V(3))
	move     &ref[451], R(6)
	move     0, R(7)
	call_c   Dyam_Reg_Load(8,V(8))
	move     V(18), R(10)
	move     S(5), R(11)
	call_c   Dyam_Reg_Deallocate(6)
	pl_jump  fun155()

long local pool_fun543[4]=[65538,build_ref_1061,build_ref_1062,pool_fun542]

pl_code local fun543
	call_c   Dyam_Pool(pool_fun543)
	call_c   Dyam_Unify_Item(&ref[1061])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun542)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(5),&ref[1062])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(3))
	call_c   Dyam_Reg_Load(2,V(14))
	pl_call  pred_lctag_first_scan_2()
	call_c   Dyam_Unify(V(15),I(0))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_jump  Follow_Cont(V(0))

;; TERM 1060: '*PROLOG-FIRST*'(lctag_first(_B, tag_node{id=> _C, label=> _D, children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}, _O, _P)) :> []
c_code local build_ref_1060
	ret_reg &ref[1060]
	call_c   build_ref_1059()
	call_c   Dyam_Create_Binary(I(9),&ref[1059],I(0))
	move_ret ref[1060]
	c_ret

;; TERM 1059: '*PROLOG-FIRST*'(lctag_first(_B, tag_node{id=> _C, label=> _D, children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}, _O, _P))
c_code local build_ref_1059
	ret_reg &ref[1059]
	call_c   build_ref_64()
	call_c   build_ref_1058()
	call_c   Dyam_Create_Unary(&ref[64],&ref[1058])
	move_ret ref[1059]
	c_ret

c_code local build_seed_172
	ret_reg &seed[172]
	call_c   build_ref_133()
	call_c   build_ref_1105()
	call_c   Dyam_Seed_Start(&ref[133],&ref[1105],I(0),fun21,1)
	call_c   build_ref_1103()
	call_c   Dyam_Seed_Add_Comp(&ref[1103],fun524,0)
	call_c   Dyam_Seed_End()
	move_ret seed[172]
	c_ret

;; TERM 1103: '*RITEM*'('call_right_top_bot_constraints/3'(_C), return(_D, _E))
c_code local build_ref_1103
	ret_reg &ref[1103]
	call_c   build_ref_0()
	call_c   build_ref_1099()
	call_c   build_ref_194()
	call_c   Dyam_Create_Binary(&ref[0],&ref[1099],&ref[194])
	move_ret ref[1103]
	c_ret

;; TERM 1099: 'call_right_top_bot_constraints/3'(_C)
c_code local build_ref_1099
	ret_reg &ref[1099]
	call_c   build_ref_1138()
	call_c   Dyam_Create_Unary(&ref[1138],V(2))
	move_ret ref[1099]
	c_ret

pl_code local fun524
	call_c   build_ref_1103()
	call_c   Dyam_Unify_Item(&ref[1103])
	fail_ret
	pl_jump  Follow_Cont(V(1))

;; TERM 1105: '*RITEM*'('call_right_top_bot_constraints/3'(_C), return(_D, _E)) :> tig(26, '$TUPPLE'(35144395400924))
c_code local build_ref_1105
	ret_reg &ref[1105]
	call_c   build_ref_1103()
	call_c   build_ref_1104()
	call_c   Dyam_Create_Binary(I(9),&ref[1103],&ref[1104])
	move_ret ref[1105]
	c_ret

;; TERM 1104: tig(26, '$TUPPLE'(35144395400924))
c_code local build_ref_1104
	ret_reg &ref[1104]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,134217728)
	move_ret R(0)
	call_c   build_ref_1154()
	call_c   Dyam_Create_Binary(&ref[1154],N(26),R(0))
	move_ret ref[1104]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_171
	ret_reg &seed[171]
	call_c   build_ref_97()
	call_c   build_ref_1100()
	call_c   Dyam_Seed_Start(&ref[97],&ref[1100],&ref[1100],fun11,1)
	call_c   build_ref_1102()
	call_c   Dyam_Seed_Add_Comp(&ref[1102],&ref[1100],0)
	call_c   Dyam_Seed_End()
	move_ret seed[171]
	c_ret

;; TERM 1102: '*FIRST*'('call_right_top_bot_constraints/3'(_C)) :> '$$HOLE$$'
c_code local build_ref_1102
	ret_reg &ref[1102]
	call_c   build_ref_1101()
	call_c   Dyam_Create_Binary(I(9),&ref[1101],I(7))
	move_ret ref[1102]
	c_ret

;; TERM 1101: '*FIRST*'('call_right_top_bot_constraints/3'(_C))
c_code local build_ref_1101
	ret_reg &ref[1101]
	call_c   build_ref_93()
	call_c   build_ref_1099()
	call_c   Dyam_Create_Unary(&ref[93],&ref[1099])
	move_ret ref[1101]
	c_ret

;; TERM 1100: '*CITEM*'('call_right_top_bot_constraints/3'(_C), 'call_right_top_bot_constraints/3'(_C))
c_code local build_ref_1100
	ret_reg &ref[1100]
	call_c   build_ref_97()
	call_c   build_ref_1099()
	call_c   Dyam_Create_Binary(&ref[97],&ref[1099],&ref[1099])
	move_ret ref[1100]
	c_ret

c_code local build_seed_160
	ret_reg &seed[160]
	call_c   build_ref_97()
	call_c   build_ref_968()
	call_c   Dyam_Seed_Start(&ref[97],&ref[968],&ref[968],fun11,1)
	call_c   build_ref_970()
	call_c   Dyam_Seed_Add_Comp(&ref[970],&ref[968],0)
	call_c   Dyam_Seed_End()
	move_ret seed[160]
	c_ret

;; TERM 970: '*FIRST*'(node_auxtree(_C, _D, _E)) :> '$$HOLE$$'
c_code local build_ref_970
	ret_reg &ref[970]
	call_c   build_ref_969()
	call_c   Dyam_Create_Binary(I(9),&ref[969],I(7))
	move_ret ref[970]
	c_ret

;; TERM 969: '*FIRST*'(node_auxtree(_C, _D, _E))
c_code local build_ref_969
	ret_reg &ref[969]
	call_c   build_ref_93()
	call_c   build_ref_967()
	call_c   Dyam_Create_Unary(&ref[93],&ref[967])
	move_ret ref[969]
	c_ret

;; TERM 967: node_auxtree(_C, _D, _E)
c_code local build_ref_967
	ret_reg &ref[967]
	call_c   build_ref_1210()
	call_c   Dyam_Term_Start(&ref[1210],3)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[967]
	c_ret

;; TERM 968: '*CITEM*'(node_auxtree(_C, _D, _E), node_auxtree(_C, _D, _E))
c_code local build_ref_968
	ret_reg &ref[968]
	call_c   build_ref_97()
	call_c   build_ref_967()
	call_c   Dyam_Create_Binary(&ref[97],&ref[967],&ref[967])
	move_ret ref[968]
	c_ret

c_code local build_seed_161
	ret_reg &seed[161]
	call_c   build_ref_133()
	call_c   build_ref_973()
	call_c   Dyam_Seed_Start(&ref[133],&ref[973],I(0),fun21,1)
	call_c   build_ref_971()
	call_c   Dyam_Seed_Add_Comp(&ref[971],fun436,0)
	call_c   Dyam_Seed_End()
	move_ret seed[161]
	c_ret

;; TERM 971: '*RITEM*'(node_auxtree(_C, _D, _E), voidret)
c_code local build_ref_971
	ret_reg &ref[971]
	call_c   build_ref_0()
	call_c   build_ref_967()
	call_c   build_ref_21()
	call_c   Dyam_Create_Binary(&ref[0],&ref[967],&ref[21])
	move_ret ref[971]
	c_ret

pl_code local fun436
	call_c   build_ref_971()
	call_c   Dyam_Unify_Item(&ref[971])
	fail_ret
	pl_jump  Follow_Cont(V(1))

;; TERM 973: '*RITEM*'(node_auxtree(_C, _D, _E), voidret) :> tig(25, '$TUPPLE'(35144395400924))
c_code local build_ref_973
	ret_reg &ref[973]
	call_c   build_ref_971()
	call_c   build_ref_972()
	call_c   Dyam_Create_Binary(I(9),&ref[971],&ref[972])
	move_ret ref[973]
	c_ret

;; TERM 972: tig(25, '$TUPPLE'(35144395400924))
c_code local build_ref_972
	ret_reg &ref[972]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,134217728)
	move_ret R(0)
	call_c   build_ref_1154()
	call_c   Dyam_Create_Binary(&ref[1154],N(25),R(0))
	move_ret ref[972]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_149
	ret_reg &seed[149]
	call_c   build_ref_97()
	call_c   build_ref_807()
	call_c   Dyam_Seed_Start(&ref[97],&ref[807],&ref[807],fun11,1)
	call_c   build_ref_809()
	call_c   Dyam_Seed_Add_Comp(&ref[809],&ref[807],0)
	call_c   Dyam_Seed_End()
	move_ret seed[149]
	c_ret

;; TERM 809: '*FIRST*'('call_core_info/3'(_C, _D)) :> '$$HOLE$$'
c_code local build_ref_809
	ret_reg &ref[809]
	call_c   build_ref_808()
	call_c   Dyam_Create_Binary(I(9),&ref[808],I(7))
	move_ret ref[809]
	c_ret

;; TERM 808: '*FIRST*'('call_core_info/3'(_C, _D))
c_code local build_ref_808
	ret_reg &ref[808]
	call_c   build_ref_93()
	call_c   build_ref_806()
	call_c   Dyam_Create_Unary(&ref[93],&ref[806])
	move_ret ref[808]
	c_ret

;; TERM 806: 'call_core_info/3'(_C, _D)
c_code local build_ref_806
	ret_reg &ref[806]
	call_c   build_ref_1260()
	call_c   Dyam_Create_Binary(&ref[1260],V(2),V(3))
	move_ret ref[806]
	c_ret

;; TERM 1260: 'call_core_info/3'
c_code local build_ref_1260
	ret_reg &ref[1260]
	call_c   Dyam_Create_Atom("call_core_info/3")
	move_ret ref[1260]
	c_ret

;; TERM 807: '*CITEM*'('call_core_info/3'(_C, _D), 'call_core_info/3'(_C, _D))
c_code local build_ref_807
	ret_reg &ref[807]
	call_c   build_ref_97()
	call_c   build_ref_806()
	call_c   Dyam_Create_Binary(&ref[97],&ref[806],&ref[806])
	move_ret ref[807]
	c_ret

c_code local build_seed_150
	ret_reg &seed[150]
	call_c   build_ref_133()
	call_c   build_ref_812()
	call_c   Dyam_Seed_Start(&ref[133],&ref[812],I(0),fun21,1)
	call_c   build_ref_810()
	call_c   Dyam_Seed_Add_Comp(&ref[810],fun337,0)
	call_c   Dyam_Seed_End()
	move_ret seed[150]
	c_ret

;; TERM 810: '*RITEM*'('call_core_info/3'(_C, _D), return(_E))
c_code local build_ref_810
	ret_reg &ref[810]
	call_c   build_ref_0()
	call_c   build_ref_806()
	call_c   build_ref_771()
	call_c   Dyam_Create_Binary(&ref[0],&ref[806],&ref[771])
	move_ret ref[810]
	c_ret

;; TERM 771: return(_E)
c_code local build_ref_771
	ret_reg &ref[771]
	call_c   build_ref_1139()
	call_c   Dyam_Create_Unary(&ref[1139],V(4))
	move_ret ref[771]
	c_ret

pl_code local fun337
	call_c   build_ref_810()
	call_c   Dyam_Unify_Item(&ref[810])
	fail_ret
	pl_jump  Follow_Cont(V(1))

;; TERM 812: '*RITEM*'('call_core_info/3'(_C, _D), return(_E)) :> tig(24, '$TUPPLE'(35144395400924))
c_code local build_ref_812
	ret_reg &ref[812]
	call_c   build_ref_810()
	call_c   build_ref_811()
	call_c   Dyam_Create_Binary(I(9),&ref[810],&ref[811])
	move_ret ref[812]
	c_ret

;; TERM 811: tig(24, '$TUPPLE'(35144395400924))
c_code local build_ref_811
	ret_reg &ref[811]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,134217728)
	move_ret R(0)
	call_c   build_ref_1154()
	call_c   Dyam_Create_Binary(&ref[1154],N(24),R(0))
	move_ret ref[811]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_147
	ret_reg &seed[147]
	call_c   build_ref_97()
	call_c   build_ref_800()
	call_c   Dyam_Seed_Start(&ref[97],&ref[800],&ref[800],fun11,1)
	call_c   build_ref_802()
	call_c   Dyam_Seed_Add_Comp(&ref[802],&ref[800],0)
	call_c   Dyam_Seed_End()
	move_ret seed[147]
	c_ret

;; TERM 802: '*FIRST*'(verbose_tag(_C)) :> '$$HOLE$$'
c_code local build_ref_802
	ret_reg &ref[802]
	call_c   build_ref_801()
	call_c   Dyam_Create_Binary(I(9),&ref[801],I(7))
	move_ret ref[802]
	c_ret

;; TERM 801: '*FIRST*'(verbose_tag(_C))
c_code local build_ref_801
	ret_reg &ref[801]
	call_c   build_ref_93()
	call_c   build_ref_799()
	call_c   Dyam_Create_Unary(&ref[93],&ref[799])
	move_ret ref[801]
	c_ret

;; TERM 799: verbose_tag(_C)
c_code local build_ref_799
	ret_reg &ref[799]
	call_c   build_ref_1261()
	call_c   Dyam_Create_Unary(&ref[1261],V(2))
	move_ret ref[799]
	c_ret

;; TERM 1261: verbose_tag
c_code local build_ref_1261
	ret_reg &ref[1261]
	call_c   Dyam_Create_Atom("verbose_tag")
	move_ret ref[1261]
	c_ret

;; TERM 800: '*CITEM*'(verbose_tag(_C), verbose_tag(_C))
c_code local build_ref_800
	ret_reg &ref[800]
	call_c   build_ref_97()
	call_c   build_ref_799()
	call_c   Dyam_Create_Binary(&ref[97],&ref[799],&ref[799])
	move_ret ref[800]
	c_ret

c_code local build_seed_148
	ret_reg &seed[148]
	call_c   build_ref_133()
	call_c   build_ref_805()
	call_c   Dyam_Seed_Start(&ref[133],&ref[805],I(0),fun21,1)
	call_c   build_ref_803()
	call_c   Dyam_Seed_Add_Comp(&ref[803],fun334,0)
	call_c   Dyam_Seed_End()
	move_ret seed[148]
	c_ret

;; TERM 803: '*RITEM*'(verbose_tag(_C), voidret)
c_code local build_ref_803
	ret_reg &ref[803]
	call_c   build_ref_0()
	call_c   build_ref_799()
	call_c   build_ref_21()
	call_c   Dyam_Create_Binary(&ref[0],&ref[799],&ref[21])
	move_ret ref[803]
	c_ret

pl_code local fun334
	call_c   build_ref_803()
	call_c   Dyam_Unify_Item(&ref[803])
	fail_ret
	pl_jump  Follow_Cont(V(1))

;; TERM 805: '*RITEM*'(verbose_tag(_C), voidret) :> tig(23, '$TUPPLE'(35144395400924))
c_code local build_ref_805
	ret_reg &ref[805]
	call_c   build_ref_803()
	call_c   build_ref_804()
	call_c   Dyam_Create_Binary(I(9),&ref[803],&ref[804])
	move_ret ref[805]
	c_ret

;; TERM 804: tig(23, '$TUPPLE'(35144395400924))
c_code local build_ref_804
	ret_reg &ref[804]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,134217728)
	move_ret R(0)
	call_c   build_ref_1154()
	call_c   Dyam_Create_Binary(&ref[1154],N(23),R(0))
	move_ret ref[804]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_142
	ret_reg &seed[142]
	call_c   build_ref_97()
	call_c   build_ref_768()
	call_c   Dyam_Seed_Start(&ref[97],&ref[768],&ref[768],fun11,1)
	call_c   build_ref_770()
	call_c   Dyam_Seed_Add_Comp(&ref[770],&ref[768],0)
	call_c   Dyam_Seed_End()
	move_ret seed[142]
	c_ret

;; TERM 770: '*FIRST*'('call_tagguide_name/3'(_C, _D)) :> '$$HOLE$$'
c_code local build_ref_770
	ret_reg &ref[770]
	call_c   build_ref_769()
	call_c   Dyam_Create_Binary(I(9),&ref[769],I(7))
	move_ret ref[770]
	c_ret

;; TERM 769: '*FIRST*'('call_tagguide_name/3'(_C, _D))
c_code local build_ref_769
	ret_reg &ref[769]
	call_c   build_ref_93()
	call_c   build_ref_767()
	call_c   Dyam_Create_Unary(&ref[93],&ref[767])
	move_ret ref[769]
	c_ret

;; TERM 767: 'call_tagguide_name/3'(_C, _D)
c_code local build_ref_767
	ret_reg &ref[767]
	call_c   build_ref_1262()
	call_c   Dyam_Create_Binary(&ref[1262],V(2),V(3))
	move_ret ref[767]
	c_ret

;; TERM 1262: 'call_tagguide_name/3'
c_code local build_ref_1262
	ret_reg &ref[1262]
	call_c   Dyam_Create_Atom("call_tagguide_name/3")
	move_ret ref[1262]
	c_ret

;; TERM 768: '*CITEM*'('call_tagguide_name/3'(_C, _D), 'call_tagguide_name/3'(_C, _D))
c_code local build_ref_768
	ret_reg &ref[768]
	call_c   build_ref_97()
	call_c   build_ref_767()
	call_c   Dyam_Create_Binary(&ref[97],&ref[767],&ref[767])
	move_ret ref[768]
	c_ret

c_code local build_seed_143
	ret_reg &seed[143]
	call_c   build_ref_133()
	call_c   build_ref_774()
	call_c   Dyam_Seed_Start(&ref[133],&ref[774],I(0),fun21,1)
	call_c   build_ref_772()
	call_c   Dyam_Seed_Add_Comp(&ref[772],fun322,0)
	call_c   Dyam_Seed_End()
	move_ret seed[143]
	c_ret

;; TERM 772: '*RITEM*'('call_tagguide_name/3'(_C, _D), return(_E))
c_code local build_ref_772
	ret_reg &ref[772]
	call_c   build_ref_0()
	call_c   build_ref_767()
	call_c   build_ref_771()
	call_c   Dyam_Create_Binary(&ref[0],&ref[767],&ref[771])
	move_ret ref[772]
	c_ret

pl_code local fun322
	call_c   build_ref_772()
	call_c   Dyam_Unify_Item(&ref[772])
	fail_ret
	pl_jump  Follow_Cont(V(1))

;; TERM 774: '*RITEM*'('call_tagguide_name/3'(_C, _D), return(_E)) :> tig(22, '$TUPPLE'(35144395400924))
c_code local build_ref_774
	ret_reg &ref[774]
	call_c   build_ref_772()
	call_c   build_ref_773()
	call_c   Dyam_Create_Binary(I(9),&ref[772],&ref[773])
	move_ret ref[774]
	c_ret

;; TERM 773: tig(22, '$TUPPLE'(35144395400924))
c_code local build_ref_773
	ret_reg &ref[773]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,134217728)
	move_ret R(0)
	call_c   build_ref_1154()
	call_c   Dyam_Create_Binary(&ref[1154],N(22),R(0))
	move_ret ref[773]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_140
	ret_reg &seed[140]
	call_c   build_ref_97()
	call_c   build_ref_761()
	call_c   Dyam_Seed_Start(&ref[97],&ref[761],&ref[761],fun11,1)
	call_c   build_ref_763()
	call_c   Dyam_Seed_Add_Comp(&ref[763],&ref[761],0)
	call_c   Dyam_Seed_End()
	move_ret seed[140]
	c_ret

;; TERM 763: '*FIRST*'('call_tagguide_pred/2'(_C)) :> '$$HOLE$$'
c_code local build_ref_763
	ret_reg &ref[763]
	call_c   build_ref_762()
	call_c   Dyam_Create_Binary(I(9),&ref[762],I(7))
	move_ret ref[763]
	c_ret

;; TERM 762: '*FIRST*'('call_tagguide_pred/2'(_C))
c_code local build_ref_762
	ret_reg &ref[762]
	call_c   build_ref_93()
	call_c   build_ref_760()
	call_c   Dyam_Create_Unary(&ref[93],&ref[760])
	move_ret ref[762]
	c_ret

;; TERM 760: 'call_tagguide_pred/2'(_C)
c_code local build_ref_760
	ret_reg &ref[760]
	call_c   build_ref_1263()
	call_c   Dyam_Create_Unary(&ref[1263],V(2))
	move_ret ref[760]
	c_ret

;; TERM 1263: 'call_tagguide_pred/2'
c_code local build_ref_1263
	ret_reg &ref[1263]
	call_c   Dyam_Create_Atom("call_tagguide_pred/2")
	move_ret ref[1263]
	c_ret

;; TERM 761: '*CITEM*'('call_tagguide_pred/2'(_C), 'call_tagguide_pred/2'(_C))
c_code local build_ref_761
	ret_reg &ref[761]
	call_c   build_ref_97()
	call_c   build_ref_760()
	call_c   Dyam_Create_Binary(&ref[97],&ref[760],&ref[760])
	move_ret ref[761]
	c_ret

c_code local build_seed_141
	ret_reg &seed[141]
	call_c   build_ref_133()
	call_c   build_ref_766()
	call_c   Dyam_Seed_Start(&ref[133],&ref[766],I(0),fun21,1)
	call_c   build_ref_764()
	call_c   Dyam_Seed_Add_Comp(&ref[764],fun319,0)
	call_c   Dyam_Seed_End()
	move_ret seed[141]
	c_ret

;; TERM 764: '*RITEM*'('call_tagguide_pred/2'(_C), return(_D))
c_code local build_ref_764
	ret_reg &ref[764]
	call_c   build_ref_0()
	call_c   build_ref_760()
	call_c   build_ref_154()
	call_c   Dyam_Create_Binary(&ref[0],&ref[760],&ref[154])
	move_ret ref[764]
	c_ret

;; TERM 154: return(_D)
c_code local build_ref_154
	ret_reg &ref[154]
	call_c   build_ref_1139()
	call_c   Dyam_Create_Unary(&ref[1139],V(3))
	move_ret ref[154]
	c_ret

pl_code local fun319
	call_c   build_ref_764()
	call_c   Dyam_Unify_Item(&ref[764])
	fail_ret
	pl_jump  Follow_Cont(V(1))

;; TERM 766: '*RITEM*'('call_tagguide_pred/2'(_C), return(_D)) :> tig(21, '$TUPPLE'(35144395400924))
c_code local build_ref_766
	ret_reg &ref[766]
	call_c   build_ref_764()
	call_c   build_ref_765()
	call_c   Dyam_Create_Binary(I(9),&ref[764],&ref[765])
	move_ret ref[766]
	c_ret

;; TERM 765: tig(21, '$TUPPLE'(35144395400924))
c_code local build_ref_765
	ret_reg &ref[765]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,134217728)
	move_ret R(0)
	call_c   build_ref_1154()
	call_c   Dyam_Create_Binary(&ref[1154],N(21),R(0))
	move_ret ref[765]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_137
	ret_reg &seed[137]
	call_c   build_ref_97()
	call_c   build_ref_745()
	call_c   Dyam_Seed_Start(&ref[97],&ref[745],&ref[745],fun11,1)
	call_c   build_ref_747()
	call_c   Dyam_Seed_Add_Comp(&ref[747],&ref[745],0)
	call_c   Dyam_Seed_End()
	move_ret seed[137]
	c_ret

;; TERM 747: '*FIRST*'(register_predicate(_C)) :> '$$HOLE$$'
c_code local build_ref_747
	ret_reg &ref[747]
	call_c   build_ref_746()
	call_c   Dyam_Create_Binary(I(9),&ref[746],I(7))
	move_ret ref[747]
	c_ret

;; TERM 746: '*FIRST*'(register_predicate(_C))
c_code local build_ref_746
	ret_reg &ref[746]
	call_c   build_ref_93()
	call_c   build_ref_744()
	call_c   Dyam_Create_Unary(&ref[93],&ref[744])
	move_ret ref[746]
	c_ret

;; TERM 744: register_predicate(_C)
c_code local build_ref_744
	ret_reg &ref[744]
	call_c   build_ref_1264()
	call_c   Dyam_Create_Unary(&ref[1264],V(2))
	move_ret ref[744]
	c_ret

;; TERM 1264: register_predicate
c_code local build_ref_1264
	ret_reg &ref[1264]
	call_c   Dyam_Create_Atom("register_predicate")
	move_ret ref[1264]
	c_ret

;; TERM 745: '*CITEM*'(register_predicate(_C), register_predicate(_C))
c_code local build_ref_745
	ret_reg &ref[745]
	call_c   build_ref_97()
	call_c   build_ref_744()
	call_c   Dyam_Create_Binary(&ref[97],&ref[744],&ref[744])
	move_ret ref[745]
	c_ret

c_code local build_seed_138
	ret_reg &seed[138]
	call_c   build_ref_133()
	call_c   build_ref_750()
	call_c   Dyam_Seed_Start(&ref[133],&ref[750],I(0),fun21,1)
	call_c   build_ref_748()
	call_c   Dyam_Seed_Add_Comp(&ref[748],fun316,0)
	call_c   Dyam_Seed_End()
	move_ret seed[138]
	c_ret

;; TERM 748: '*RITEM*'(register_predicate(_C), voidret)
c_code local build_ref_748
	ret_reg &ref[748]
	call_c   build_ref_0()
	call_c   build_ref_744()
	call_c   build_ref_21()
	call_c   Dyam_Create_Binary(&ref[0],&ref[744],&ref[21])
	move_ret ref[748]
	c_ret

pl_code local fun316
	call_c   build_ref_748()
	call_c   Dyam_Unify_Item(&ref[748])
	fail_ret
	pl_jump  Follow_Cont(V(1))

;; TERM 750: '*RITEM*'(register_predicate(_C), voidret) :> tig(20, '$TUPPLE'(35144395400924))
c_code local build_ref_750
	ret_reg &ref[750]
	call_c   build_ref_748()
	call_c   build_ref_749()
	call_c   Dyam_Create_Binary(I(9),&ref[748],&ref[749])
	move_ret ref[750]
	c_ret

;; TERM 749: tig(20, '$TUPPLE'(35144395400924))
c_code local build_ref_749
	ret_reg &ref[749]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,134217728)
	move_ret R(0)
	call_c   build_ref_1154()
	call_c   Dyam_Create_Binary(&ref[1154],N(20),R(0))
	move_ret ref[749]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_128
	ret_reg &seed[128]
	call_c   build_ref_97()
	call_c   build_ref_646()
	call_c   Dyam_Seed_Start(&ref[97],&ref[646],&ref[646],fun11,1)
	call_c   build_ref_648()
	call_c   Dyam_Seed_Add_Comp(&ref[648],&ref[646],0)
	call_c   Dyam_Seed_End()
	move_ret seed[128]
	c_ret

;; TERM 648: '*FIRST*'(wrapping_predicate(_C)) :> '$$HOLE$$'
c_code local build_ref_648
	ret_reg &ref[648]
	call_c   build_ref_647()
	call_c   Dyam_Create_Binary(I(9),&ref[647],I(7))
	move_ret ref[648]
	c_ret

;; TERM 647: '*FIRST*'(wrapping_predicate(_C))
c_code local build_ref_647
	ret_reg &ref[647]
	call_c   build_ref_93()
	call_c   build_ref_645()
	call_c   Dyam_Create_Unary(&ref[93],&ref[645])
	move_ret ref[647]
	c_ret

;; TERM 645: wrapping_predicate(_C)
c_code local build_ref_645
	ret_reg &ref[645]
	call_c   build_ref_1214()
	call_c   Dyam_Create_Unary(&ref[1214],V(2))
	move_ret ref[645]
	c_ret

;; TERM 646: '*CITEM*'(wrapping_predicate(_C), wrapping_predicate(_C))
c_code local build_ref_646
	ret_reg &ref[646]
	call_c   build_ref_97()
	call_c   build_ref_645()
	call_c   Dyam_Create_Binary(&ref[97],&ref[645],&ref[645])
	move_ret ref[646]
	c_ret

c_code local build_seed_129
	ret_reg &seed[129]
	call_c   build_ref_133()
	call_c   build_ref_651()
	call_c   Dyam_Seed_Start(&ref[133],&ref[651],I(0),fun21,1)
	call_c   build_ref_649()
	call_c   Dyam_Seed_Add_Comp(&ref[649],fun247,0)
	call_c   Dyam_Seed_End()
	move_ret seed[129]
	c_ret

;; TERM 649: '*RITEM*'(wrapping_predicate(_C), voidret)
c_code local build_ref_649
	ret_reg &ref[649]
	call_c   build_ref_0()
	call_c   build_ref_645()
	call_c   build_ref_21()
	call_c   Dyam_Create_Binary(&ref[0],&ref[645],&ref[21])
	move_ret ref[649]
	c_ret

pl_code local fun247
	call_c   build_ref_649()
	call_c   Dyam_Unify_Item(&ref[649])
	fail_ret
	pl_jump  Follow_Cont(V(1))

;; TERM 651: '*RITEM*'(wrapping_predicate(_C), voidret) :> tig(19, '$TUPPLE'(35144395400924))
c_code local build_ref_651
	ret_reg &ref[651]
	call_c   build_ref_649()
	call_c   build_ref_650()
	call_c   Dyam_Create_Binary(I(9),&ref[649],&ref[650])
	move_ret ref[651]
	c_ret

;; TERM 650: tig(19, '$TUPPLE'(35144395400924))
c_code local build_ref_650
	ret_reg &ref[650]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,134217728)
	move_ret R(0)
	call_c   build_ref_1154()
	call_c   Dyam_Create_Binary(&ref[1154],N(19),R(0))
	move_ret ref[650]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_121
	ret_reg &seed[121]
	call_c   build_ref_97()
	call_c   build_ref_612()
	call_c   Dyam_Seed_Start(&ref[97],&ref[612],&ref[612],fun11,1)
	call_c   build_ref_614()
	call_c   Dyam_Seed_Add_Comp(&ref[614],&ref[612],0)
	call_c   Dyam_Seed_End()
	move_ret seed[121]
	c_ret

;; TERM 614: '*FIRST*'('call_decompose_args/3'(_C)) :> '$$HOLE$$'
c_code local build_ref_614
	ret_reg &ref[614]
	call_c   build_ref_613()
	call_c   Dyam_Create_Binary(I(9),&ref[613],I(7))
	move_ret ref[614]
	c_ret

;; TERM 613: '*FIRST*'('call_decompose_args/3'(_C))
c_code local build_ref_613
	ret_reg &ref[613]
	call_c   build_ref_93()
	call_c   build_ref_611()
	call_c   Dyam_Create_Unary(&ref[93],&ref[611])
	move_ret ref[613]
	c_ret

;; TERM 611: 'call_decompose_args/3'(_C)
c_code local build_ref_611
	ret_reg &ref[611]
	call_c   build_ref_1248()
	call_c   Dyam_Create_Unary(&ref[1248],V(2))
	move_ret ref[611]
	c_ret

;; TERM 612: '*CITEM*'('call_decompose_args/3'(_C), 'call_decompose_args/3'(_C))
c_code local build_ref_612
	ret_reg &ref[612]
	call_c   build_ref_97()
	call_c   build_ref_611()
	call_c   Dyam_Create_Binary(&ref[97],&ref[611],&ref[611])
	move_ret ref[612]
	c_ret

c_code local build_seed_122
	ret_reg &seed[122]
	call_c   build_ref_133()
	call_c   build_ref_617()
	call_c   Dyam_Seed_Start(&ref[133],&ref[617],I(0),fun21,1)
	call_c   build_ref_615()
	call_c   Dyam_Seed_Add_Comp(&ref[615],fun235,0)
	call_c   Dyam_Seed_End()
	move_ret seed[122]
	c_ret

;; TERM 615: '*RITEM*'('call_decompose_args/3'(_C), return(_D, _E))
c_code local build_ref_615
	ret_reg &ref[615]
	call_c   build_ref_0()
	call_c   build_ref_611()
	call_c   build_ref_194()
	call_c   Dyam_Create_Binary(&ref[0],&ref[611],&ref[194])
	move_ret ref[615]
	c_ret

pl_code local fun235
	call_c   build_ref_615()
	call_c   Dyam_Unify_Item(&ref[615])
	fail_ret
	pl_jump  Follow_Cont(V(1))

;; TERM 617: '*RITEM*'('call_decompose_args/3'(_C), return(_D, _E)) :> tig(18, '$TUPPLE'(35144395400924))
c_code local build_ref_617
	ret_reg &ref[617]
	call_c   build_ref_615()
	call_c   build_ref_616()
	call_c   Dyam_Create_Binary(I(9),&ref[615],&ref[616])
	move_ret ref[617]
	c_ret

;; TERM 616: tig(18, '$TUPPLE'(35144395400924))
c_code local build_ref_616
	ret_reg &ref[616]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,134217728)
	move_ret R(0)
	call_c   build_ref_1154()
	call_c   Dyam_Create_Binary(&ref[1154],N(18),R(0))
	move_ret ref[616]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_119
	ret_reg &seed[119]
	call_c   build_ref_97()
	call_c   build_ref_590()
	call_c   Dyam_Seed_Start(&ref[97],&ref[590],&ref[590],fun11,1)
	call_c   build_ref_592()
	call_c   Dyam_Seed_Add_Comp(&ref[592],&ref[590],0)
	call_c   Dyam_Seed_End()
	move_ret seed[119]
	c_ret

;; TERM 592: '*FIRST*'(not_tig(_C, _D)) :> '$$HOLE$$'
c_code local build_ref_592
	ret_reg &ref[592]
	call_c   build_ref_591()
	call_c   Dyam_Create_Binary(I(9),&ref[591],I(7))
	move_ret ref[592]
	c_ret

;; TERM 591: '*FIRST*'(not_tig(_C, _D))
c_code local build_ref_591
	ret_reg &ref[591]
	call_c   build_ref_93()
	call_c   build_ref_589()
	call_c   Dyam_Create_Unary(&ref[93],&ref[589])
	move_ret ref[591]
	c_ret

;; TERM 589: not_tig(_C, _D)
c_code local build_ref_589
	ret_reg &ref[589]
	call_c   build_ref_1254()
	call_c   Dyam_Create_Binary(&ref[1254],V(2),V(3))
	move_ret ref[589]
	c_ret

;; TERM 590: '*CITEM*'(not_tig(_C, _D), not_tig(_C, _D))
c_code local build_ref_590
	ret_reg &ref[590]
	call_c   build_ref_97()
	call_c   build_ref_589()
	call_c   Dyam_Create_Binary(&ref[97],&ref[589],&ref[589])
	move_ret ref[590]
	c_ret

c_code local build_seed_120
	ret_reg &seed[120]
	call_c   build_ref_133()
	call_c   build_ref_595()
	call_c   Dyam_Seed_Start(&ref[133],&ref[595],I(0),fun21,1)
	call_c   build_ref_593()
	call_c   Dyam_Seed_Add_Comp(&ref[593],fun222,0)
	call_c   Dyam_Seed_End()
	move_ret seed[120]
	c_ret

;; TERM 593: '*RITEM*'(not_tig(_C, _D), voidret)
c_code local build_ref_593
	ret_reg &ref[593]
	call_c   build_ref_0()
	call_c   build_ref_589()
	call_c   build_ref_21()
	call_c   Dyam_Create_Binary(&ref[0],&ref[589],&ref[21])
	move_ret ref[593]
	c_ret

pl_code local fun222
	call_c   build_ref_593()
	call_c   Dyam_Unify_Item(&ref[593])
	fail_ret
	pl_jump  Follow_Cont(V(1))

;; TERM 595: '*RITEM*'(not_tig(_C, _D), voidret) :> tig(17, '$TUPPLE'(35144395400924))
c_code local build_ref_595
	ret_reg &ref[595]
	call_c   build_ref_593()
	call_c   build_ref_594()
	call_c   Dyam_Create_Binary(I(9),&ref[593],&ref[594])
	move_ret ref[595]
	c_ret

;; TERM 594: tig(17, '$TUPPLE'(35144395400924))
c_code local build_ref_594
	ret_reg &ref[594]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,134217728)
	move_ret R(0)
	call_c   build_ref_1154()
	call_c   Dyam_Create_Binary(&ref[1154],N(17),R(0))
	move_ret ref[594]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_117
	ret_reg &seed[117]
	call_c   build_ref_97()
	call_c   build_ref_583()
	call_c   Dyam_Seed_Start(&ref[97],&ref[583],&ref[583],fun11,1)
	call_c   build_ref_585()
	call_c   Dyam_Seed_Add_Comp(&ref[585],&ref[583],0)
	call_c   Dyam_Seed_End()
	move_ret seed[117]
	c_ret

;; TERM 585: '*FIRST*'(potential_tig(_C, _D)) :> '$$HOLE$$'
c_code local build_ref_585
	ret_reg &ref[585]
	call_c   build_ref_584()
	call_c   Dyam_Create_Binary(I(9),&ref[584],I(7))
	move_ret ref[585]
	c_ret

;; TERM 584: '*FIRST*'(potential_tig(_C, _D))
c_code local build_ref_584
	ret_reg &ref[584]
	call_c   build_ref_93()
	call_c   build_ref_582()
	call_c   Dyam_Create_Unary(&ref[93],&ref[582])
	move_ret ref[584]
	c_ret

;; TERM 582: potential_tig(_C, _D)
c_code local build_ref_582
	ret_reg &ref[582]
	call_c   build_ref_1131()
	call_c   Dyam_Create_Binary(&ref[1131],V(2),V(3))
	move_ret ref[582]
	c_ret

;; TERM 583: '*CITEM*'(potential_tig(_C, _D), potential_tig(_C, _D))
c_code local build_ref_583
	ret_reg &ref[583]
	call_c   build_ref_97()
	call_c   build_ref_582()
	call_c   Dyam_Create_Binary(&ref[97],&ref[582],&ref[582])
	move_ret ref[583]
	c_ret

c_code local build_seed_118
	ret_reg &seed[118]
	call_c   build_ref_133()
	call_c   build_ref_588()
	call_c   Dyam_Seed_Start(&ref[133],&ref[588],I(0),fun21,1)
	call_c   build_ref_586()
	call_c   Dyam_Seed_Add_Comp(&ref[586],fun219,0)
	call_c   Dyam_Seed_End()
	move_ret seed[118]
	c_ret

;; TERM 586: '*RITEM*'(potential_tig(_C, _D), voidret)
c_code local build_ref_586
	ret_reg &ref[586]
	call_c   build_ref_0()
	call_c   build_ref_582()
	call_c   build_ref_21()
	call_c   Dyam_Create_Binary(&ref[0],&ref[582],&ref[21])
	move_ret ref[586]
	c_ret

pl_code local fun219
	call_c   build_ref_586()
	call_c   Dyam_Unify_Item(&ref[586])
	fail_ret
	pl_jump  Follow_Cont(V(1))

;; TERM 588: '*RITEM*'(potential_tig(_C, _D), voidret) :> tig(16, '$TUPPLE'(35144395400924))
c_code local build_ref_588
	ret_reg &ref[588]
	call_c   build_ref_586()
	call_c   build_ref_587()
	call_c   Dyam_Create_Binary(I(9),&ref[586],&ref[587])
	move_ret ref[588]
	c_ret

;; TERM 587: tig(16, '$TUPPLE'(35144395400924))
c_code local build_ref_587
	ret_reg &ref[587]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,134217728)
	move_ret R(0)
	call_c   build_ref_1154()
	call_c   Dyam_Create_Binary(&ref[1154],N(16),R(0))
	move_ret ref[587]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_98
	ret_reg &seed[98]
	call_c   build_ref_97()
	call_c   build_ref_446()
	call_c   Dyam_Seed_Start(&ref[97],&ref[446],&ref[446],fun11,1)
	call_c   build_ref_448()
	call_c   Dyam_Seed_Add_Comp(&ref[448],&ref[446],0)
	call_c   Dyam_Seed_End()
	move_ret seed[98]
	c_ret

;; TERM 448: '*FIRST*'(make_true_tig_callret(_C, _D, _E, _F, _G, _H, _I)) :> '$$HOLE$$'
c_code local build_ref_448
	ret_reg &ref[448]
	call_c   build_ref_447()
	call_c   Dyam_Create_Binary(I(9),&ref[447],I(7))
	move_ret ref[448]
	c_ret

;; TERM 447: '*FIRST*'(make_true_tig_callret(_C, _D, _E, _F, _G, _H, _I))
c_code local build_ref_447
	ret_reg &ref[447]
	call_c   build_ref_93()
	call_c   build_ref_445()
	call_c   Dyam_Create_Unary(&ref[93],&ref[445])
	move_ret ref[447]
	c_ret

;; TERM 445: make_true_tig_callret(_C, _D, _E, _F, _G, _H, _I)
c_code local build_ref_445
	ret_reg &ref[445]
	call_c   build_ref_1155()
	call_c   Dyam_Term_Start(&ref[1155],7)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_End()
	move_ret ref[445]
	c_ret

;; TERM 446: '*CITEM*'(make_true_tig_callret(_C, _D, _E, _F, _G, _H, _I), make_true_tig_callret(_C, _D, _E, _F, _G, _H, _I))
c_code local build_ref_446
	ret_reg &ref[446]
	call_c   build_ref_97()
	call_c   build_ref_445()
	call_c   Dyam_Create_Binary(&ref[97],&ref[445],&ref[445])
	move_ret ref[446]
	c_ret

c_code local build_seed_113
	ret_reg &seed[113]
	call_c   build_ref_133()
	call_c   build_ref_552()
	call_c   Dyam_Seed_Start(&ref[133],&ref[552],I(0),fun21,1)
	call_c   build_ref_550()
	call_c   Dyam_Seed_Add_Comp(&ref[550],fun200,0)
	call_c   Dyam_Seed_End()
	move_ret seed[113]
	c_ret

;; TERM 550: '*RITEM*'(make_true_tig_callret(_C, _D, _E, _F, _G, _H, _I), voidret)
c_code local build_ref_550
	ret_reg &ref[550]
	call_c   build_ref_0()
	call_c   build_ref_445()
	call_c   build_ref_21()
	call_c   Dyam_Create_Binary(&ref[0],&ref[445],&ref[21])
	move_ret ref[550]
	c_ret

pl_code local fun200
	call_c   build_ref_550()
	call_c   Dyam_Unify_Item(&ref[550])
	fail_ret
	pl_jump  Follow_Cont(V(1))

;; TERM 552: '*RITEM*'(make_true_tig_callret(_C, _D, _E, _F, _G, _H, _I), voidret) :> tig(14, '$TUPPLE'(35144395400924))
c_code local build_ref_552
	ret_reg &ref[552]
	call_c   build_ref_550()
	call_c   build_ref_551()
	call_c   Dyam_Create_Binary(I(9),&ref[550],&ref[551])
	move_ret ref[552]
	c_ret

;; TERM 551: tig(14, '$TUPPLE'(35144395400924))
c_code local build_ref_551
	ret_reg &ref[551]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,134217728)
	move_ret R(0)
	call_c   build_ref_1154()
	call_c   Dyam_Create_Binary(&ref[1154],N(14),R(0))
	move_ret ref[551]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_111
	ret_reg &seed[111]
	call_c   build_ref_133()
	call_c   build_ref_534()
	call_c   Dyam_Seed_Start(&ref[133],&ref[534],I(0),fun21,1)
	call_c   build_ref_532()
	call_c   Dyam_Seed_Add_Comp(&ref[532],fun177,0)
	call_c   Dyam_Seed_End()
	move_ret seed[111]
	c_ret

;; TERM 532: '*RITEM*'(lctag_filter(_C, _D, _E, _F, _G, _H), voidret)
c_code local build_ref_532
	ret_reg &ref[532]
	call_c   build_ref_0()
	call_c   build_ref_528()
	call_c   build_ref_21()
	call_c   Dyam_Create_Binary(&ref[0],&ref[528],&ref[21])
	move_ret ref[532]
	c_ret

;; TERM 528: lctag_filter(_C, _D, _E, _F, _G, _H)
c_code local build_ref_528
	ret_reg &ref[528]
	call_c   build_ref_1209()
	call_c   Dyam_Term_Start(&ref[1209],6)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[528]
	c_ret

pl_code local fun177
	call_c   build_ref_532()
	call_c   Dyam_Unify_Item(&ref[532])
	fail_ret
	pl_jump  Follow_Cont(V(1))

;; TERM 534: '*RITEM*'(lctag_filter(_C, _D, _E, _F, _G, _H), voidret) :> tig(12, '$TUPPLE'(35144395400924))
c_code local build_ref_534
	ret_reg &ref[534]
	call_c   build_ref_532()
	call_c   build_ref_533()
	call_c   Dyam_Create_Binary(I(9),&ref[532],&ref[533])
	move_ret ref[534]
	c_ret

;; TERM 533: tig(12, '$TUPPLE'(35144395400924))
c_code local build_ref_533
	ret_reg &ref[533]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,134217728)
	move_ret R(0)
	call_c   build_ref_1154()
	call_c   Dyam_Create_Binary(&ref[1154],N(12),R(0))
	move_ret ref[533]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_110
	ret_reg &seed[110]
	call_c   build_ref_97()
	call_c   build_ref_529()
	call_c   Dyam_Seed_Start(&ref[97],&ref[529],&ref[529],fun11,1)
	call_c   build_ref_531()
	call_c   Dyam_Seed_Add_Comp(&ref[531],&ref[529],0)
	call_c   Dyam_Seed_End()
	move_ret seed[110]
	c_ret

;; TERM 531: '*FIRST*'(lctag_filter(_C, _D, _E, _F, _G, _H)) :> '$$HOLE$$'
c_code local build_ref_531
	ret_reg &ref[531]
	call_c   build_ref_530()
	call_c   Dyam_Create_Binary(I(9),&ref[530],I(7))
	move_ret ref[531]
	c_ret

;; TERM 530: '*FIRST*'(lctag_filter(_C, _D, _E, _F, _G, _H))
c_code local build_ref_530
	ret_reg &ref[530]
	call_c   build_ref_93()
	call_c   build_ref_528()
	call_c   Dyam_Create_Unary(&ref[93],&ref[528])
	move_ret ref[530]
	c_ret

;; TERM 529: '*CITEM*'(lctag_filter(_C, _D, _E, _F, _G, _H), lctag_filter(_C, _D, _E, _F, _G, _H))
c_code local build_ref_529
	ret_reg &ref[529]
	call_c   build_ref_97()
	call_c   build_ref_528()
	call_c   Dyam_Create_Binary(&ref[97],&ref[528],&ref[528])
	move_ret ref[529]
	c_ret

c_code local build_seed_102
	ret_reg &seed[102]
	call_c   build_ref_97()
	call_c   build_ref_479()
	call_c   Dyam_Seed_Start(&ref[97],&ref[479],&ref[479],fun11,1)
	call_c   build_ref_481()
	call_c   Dyam_Seed_Add_Comp(&ref[481],&ref[479],0)
	call_c   Dyam_Seed_End()
	move_ret seed[102]
	c_ret

;; TERM 481: '*FIRST*'('call_apply_feature_mode_constraints/4'(_C, _D)) :> '$$HOLE$$'
c_code local build_ref_481
	ret_reg &ref[481]
	call_c   build_ref_480()
	call_c   Dyam_Create_Binary(I(9),&ref[480],I(7))
	move_ret ref[481]
	c_ret

;; TERM 480: '*FIRST*'('call_apply_feature_mode_constraints/4'(_C, _D))
c_code local build_ref_480
	ret_reg &ref[480]
	call_c   build_ref_93()
	call_c   build_ref_478()
	call_c   Dyam_Create_Unary(&ref[93],&ref[478])
	move_ret ref[480]
	c_ret

;; TERM 478: 'call_apply_feature_mode_constraints/4'(_C, _D)
c_code local build_ref_478
	ret_reg &ref[478]
	call_c   build_ref_1152()
	call_c   Dyam_Create_Binary(&ref[1152],V(2),V(3))
	move_ret ref[478]
	c_ret

;; TERM 479: '*CITEM*'('call_apply_feature_mode_constraints/4'(_C, _D), 'call_apply_feature_mode_constraints/4'(_C, _D))
c_code local build_ref_479
	ret_reg &ref[479]
	call_c   build_ref_97()
	call_c   build_ref_478()
	call_c   Dyam_Create_Binary(&ref[97],&ref[478],&ref[478])
	move_ret ref[479]
	c_ret

c_code local build_seed_103
	ret_reg &seed[103]
	call_c   build_ref_133()
	call_c   build_ref_485()
	call_c   Dyam_Seed_Start(&ref[133],&ref[485],I(0),fun21,1)
	call_c   build_ref_483()
	call_c   Dyam_Seed_Add_Comp(&ref[483],fun153,0)
	call_c   Dyam_Seed_End()
	move_ret seed[103]
	c_ret

;; TERM 483: '*RITEM*'('call_apply_feature_mode_constraints/4'(_C, _D), return(_E, _F))
c_code local build_ref_483
	ret_reg &ref[483]
	call_c   build_ref_0()
	call_c   build_ref_478()
	call_c   build_ref_482()
	call_c   Dyam_Create_Binary(&ref[0],&ref[478],&ref[482])
	move_ret ref[483]
	c_ret

;; TERM 482: return(_E, _F)
c_code local build_ref_482
	ret_reg &ref[482]
	call_c   build_ref_1139()
	call_c   Dyam_Create_Binary(&ref[1139],V(4),V(5))
	move_ret ref[482]
	c_ret

pl_code local fun153
	call_c   build_ref_483()
	call_c   Dyam_Unify_Item(&ref[483])
	fail_ret
	pl_jump  Follow_Cont(V(1))

;; TERM 485: '*RITEM*'('call_apply_feature_mode_constraints/4'(_C, _D), return(_E, _F)) :> tig(9, '$TUPPLE'(35144395400924))
c_code local build_ref_485
	ret_reg &ref[485]
	call_c   build_ref_483()
	call_c   build_ref_484()
	call_c   Dyam_Create_Binary(I(9),&ref[483],&ref[484])
	move_ret ref[485]
	c_ret

;; TERM 484: tig(9, '$TUPPLE'(35144395400924))
c_code local build_ref_484
	ret_reg &ref[484]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,134217728)
	move_ret R(0)
	call_c   build_ref_1154()
	call_c   Dyam_Create_Binary(&ref[1154],N(9),R(0))
	move_ret ref[484]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_89
	ret_reg &seed[89]
	call_c   build_ref_55()
	call_c   build_ref_348()
	call_c   Dyam_Seed_Start(&ref[55],&ref[348],I(0),fun11,1)
	call_c   build_ref_349()
	call_c   Dyam_Seed_Add_Comp(&ref[349],&ref[348],0)
	call_c   Dyam_Seed_End()
	move_ret seed[89]
	c_ret

;; TERM 349: '*GUARD*'(body_to_lpda(_B, (_E1 , _G = _T , _H = _Q), _D2, _M, _N)) :> '$$HOLE$$'
c_code local build_ref_349
	ret_reg &ref[349]
	call_c   build_ref_348()
	call_c   Dyam_Create_Binary(I(9),&ref[348],I(7))
	move_ret ref[349]
	c_ret

;; TERM 348: '*GUARD*'(body_to_lpda(_B, (_E1 , _G = _T , _H = _Q), _D2, _M, _N))
c_code local build_ref_348
	ret_reg &ref[348]
	call_c   build_ref_55()
	call_c   build_ref_347()
	call_c   Dyam_Create_Unary(&ref[55],&ref[347])
	move_ret ref[348]
	c_ret

;; TERM 347: body_to_lpda(_B, (_E1 , _G = _T , _H = _Q), _D2, _M, _N)
c_code local build_ref_347
	ret_reg &ref[347]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_1220()
	call_c   Dyam_Create_Binary(&ref[1220],V(6),V(19))
	move_ret R(0)
	call_c   Dyam_Create_Binary(&ref[1220],V(7),V(16))
	move_ret R(1)
	call_c   Dyam_Create_Binary(I(4),R(0),R(1))
	move_ret R(1)
	call_c   Dyam_Create_Binary(I(4),V(30),R(1))
	move_ret R(1)
	call_c   build_ref_1219()
	call_c   Dyam_Term_Start(&ref[1219],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(55))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_End()
	move_ret ref[347]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_88
	ret_reg &seed[88]
	call_c   build_ref_55()
	call_c   build_ref_344()
	call_c   Dyam_Seed_Start(&ref[55],&ref[344],I(0),fun11,1)
	call_c   build_ref_345()
	call_c   Dyam_Seed_Add_Comp(&ref[345],&ref[344],0)
	call_c   Dyam_Seed_End()
	move_ret seed[88]
	c_ret

;; TERM 345: '*GUARD*'(body_to_lpda(_B, _G1, _B2, _C2, _N)) :> '$$HOLE$$'
c_code local build_ref_345
	ret_reg &ref[345]
	call_c   build_ref_344()
	call_c   Dyam_Create_Binary(I(9),&ref[344],I(7))
	move_ret ref[345]
	c_ret

;; TERM 344: '*GUARD*'(body_to_lpda(_B, _G1, _B2, _C2, _N))
c_code local build_ref_344
	ret_reg &ref[344]
	call_c   build_ref_55()
	call_c   build_ref_343()
	call_c   Dyam_Create_Unary(&ref[55],&ref[343])
	move_ret ref[344]
	c_ret

;; TERM 343: body_to_lpda(_B, _G1, _B2, _C2, _N)
c_code local build_ref_343
	ret_reg &ref[343]
	call_c   build_ref_1219()
	call_c   Dyam_Term_Start(&ref[1219],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(32))
	call_c   Dyam_Term_Arg(V(53))
	call_c   Dyam_Term_Arg(V(54))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_End()
	move_ret ref[343]
	c_ret

c_code local build_seed_87
	ret_reg &seed[87]
	call_c   build_ref_55()
	call_c   build_ref_341()
	call_c   Dyam_Seed_Start(&ref[55],&ref[341],I(0),fun11,1)
	call_c   build_ref_342()
	call_c   Dyam_Seed_Add_Comp(&ref[342],&ref[341],0)
	call_c   Dyam_Seed_End()
	move_ret seed[87]
	c_ret

;; TERM 342: '*GUARD*'(body_to_lpda(_B, (_P = +), _A2, _B2, _N)) :> '$$HOLE$$'
c_code local build_ref_342
	ret_reg &ref[342]
	call_c   build_ref_341()
	call_c   Dyam_Create_Binary(I(9),&ref[341],I(7))
	move_ret ref[342]
	c_ret

;; TERM 341: '*GUARD*'(body_to_lpda(_B, (_P = +), _A2, _B2, _N))
c_code local build_ref_341
	ret_reg &ref[341]
	call_c   build_ref_55()
	call_c   build_ref_340()
	call_c   Dyam_Create_Unary(&ref[55],&ref[340])
	move_ret ref[341]
	c_ret

;; TERM 340: body_to_lpda(_B, (_P = +), _A2, _B2, _N)
c_code local build_ref_340
	ret_reg &ref[340]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1220()
	call_c   build_ref_254()
	call_c   Dyam_Create_Binary(&ref[1220],V(15),&ref[254])
	move_ret R(0)
	call_c   build_ref_1219()
	call_c   Dyam_Term_Start(&ref[1219],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(52))
	call_c   Dyam_Term_Arg(V(53))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_End()
	move_ret ref[340]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_86
	ret_reg &seed[86]
	call_c   build_ref_55()
	call_c   build_ref_336()
	call_c   Dyam_Seed_Start(&ref[55],&ref[336],I(0),fun11,1)
	call_c   build_ref_337()
	call_c   Dyam_Seed_Add_Comp(&ref[337],&ref[336],0)
	call_c   Dyam_Seed_End()
	move_ret seed[86]
	c_ret

;; TERM 337: '*GUARD*'(body_to_lpda(_B, (_F1 , _I = _T , _J = _Q), _L, _Y1, _N)) :> '$$HOLE$$'
c_code local build_ref_337
	ret_reg &ref[337]
	call_c   build_ref_336()
	call_c   Dyam_Create_Binary(I(9),&ref[336],I(7))
	move_ret ref[337]
	c_ret

;; TERM 336: '*GUARD*'(body_to_lpda(_B, (_F1 , _I = _T , _J = _Q), _L, _Y1, _N))
c_code local build_ref_336
	ret_reg &ref[336]
	call_c   build_ref_55()
	call_c   build_ref_335()
	call_c   Dyam_Create_Unary(&ref[55],&ref[335])
	move_ret ref[336]
	c_ret

;; TERM 335: body_to_lpda(_B, (_F1 , _I = _T , _J = _Q), _L, _Y1, _N)
c_code local build_ref_335
	ret_reg &ref[335]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_1220()
	call_c   Dyam_Create_Binary(&ref[1220],V(8),V(19))
	move_ret R(0)
	call_c   Dyam_Create_Binary(&ref[1220],V(9),V(16))
	move_ret R(1)
	call_c   Dyam_Create_Binary(I(4),R(0),R(1))
	move_ret R(1)
	call_c   Dyam_Create_Binary(I(4),V(31),R(1))
	move_ret R(1)
	call_c   build_ref_1219()
	call_c   Dyam_Term_Start(&ref[1219],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(50))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_End()
	move_ret ref[335]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_67
	ret_reg &seed[67]
	call_c   build_ref_133()
	call_c   build_ref_137()
	call_c   Dyam_Seed_Start(&ref[133],&ref[137],I(0),fun21,1)
	call_c   build_ref_135()
	call_c   Dyam_Seed_Add_Comp(&ref[135],fun26,0)
	call_c   Dyam_Seed_End()
	move_ret seed[67]
	c_ret

;; TERM 135: '*RITEM*'('call_lctag_closure/4', return(_C, _D, _E, _F))
c_code local build_ref_135
	ret_reg &ref[135]
	call_c   build_ref_0()
	call_c   build_ref_1()
	call_c   build_ref_134()
	call_c   Dyam_Create_Binary(&ref[0],&ref[1],&ref[134])
	move_ret ref[135]
	c_ret

;; TERM 134: return(_C, _D, _E, _F)
c_code local build_ref_134
	ret_reg &ref[134]
	call_c   build_ref_1139()
	call_c   Dyam_Term_Start(&ref[1139],4)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[134]
	c_ret

pl_code local fun26
	call_c   build_ref_135()
	call_c   Dyam_Unify_Item(&ref[135])
	fail_ret
	pl_jump  Follow_Cont(V(1))

;; TERM 137: '*RITEM*'('call_lctag_closure/4', return(_C, _D, _E, _F)) :> tig(0, '$TUPPLE'(35144395400924))
c_code local build_ref_137
	ret_reg &ref[137]
	call_c   build_ref_135()
	call_c   build_ref_136()
	call_c   Dyam_Create_Binary(I(9),&ref[135],&ref[136])
	move_ret ref[137]
	c_ret

;; TERM 136: tig(0, '$TUPPLE'(35144395400924))
c_code local build_ref_136
	ret_reg &ref[136]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,134217728)
	move_ret R(0)
	call_c   build_ref_1154()
	call_c   Dyam_Create_Binary(&ref[1154],N(0),R(0))
	move_ret ref[136]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_66
	ret_reg &seed[66]
	call_c   build_ref_97()
	call_c   build_ref_130()
	call_c   Dyam_Seed_Start(&ref[97],&ref[130],&ref[130],fun11,1)
	call_c   build_ref_132()
	call_c   Dyam_Seed_Add_Comp(&ref[132],&ref[130],0)
	call_c   Dyam_Seed_End()
	move_ret seed[66]
	c_ret

;; TERM 132: '*FIRST*'('call_lctag_closure/4') :> '$$HOLE$$'
c_code local build_ref_132
	ret_reg &ref[132]
	call_c   build_ref_131()
	call_c   Dyam_Create_Binary(I(9),&ref[131],I(7))
	move_ret ref[132]
	c_ret

;; TERM 130: '*CITEM*'('call_lctag_closure/4', 'call_lctag_closure/4')
c_code local build_ref_130
	ret_reg &ref[130]
	call_c   build_ref_97()
	call_c   build_ref_1()
	call_c   Dyam_Create_Binary(&ref[97],&ref[1],&ref[1])
	move_ret ref[130]
	c_ret

c_code local build_seed_74
	ret_reg &seed[74]
	call_c   build_ref_97()
	call_c   build_ref_198()
	call_c   Dyam_Seed_Start(&ref[97],&ref[198],&ref[198],fun11,1)
	call_c   build_ref_200()
	call_c   Dyam_Seed_Add_Comp(&ref[200],&ref[198],0)
	call_c   Dyam_Seed_End()
	move_ret seed[74]
	c_ret

;; TERM 200: '*FIRST*'('call_apply_feature_constraints/3'(_C)) :> '$$HOLE$$'
c_code local build_ref_200
	ret_reg &ref[200]
	call_c   build_ref_199()
	call_c   Dyam_Create_Binary(I(9),&ref[199],I(7))
	move_ret ref[200]
	c_ret

;; TERM 199: '*FIRST*'('call_apply_feature_constraints/3'(_C))
c_code local build_ref_199
	ret_reg &ref[199]
	call_c   build_ref_93()
	call_c   build_ref_197()
	call_c   Dyam_Create_Unary(&ref[93],&ref[197])
	move_ret ref[199]
	c_ret

;; TERM 197: 'call_apply_feature_constraints/3'(_C)
c_code local build_ref_197
	ret_reg &ref[197]
	call_c   build_ref_1148()
	call_c   Dyam_Create_Unary(&ref[1148],V(2))
	move_ret ref[197]
	c_ret

;; TERM 198: '*CITEM*'('call_apply_feature_constraints/3'(_C), 'call_apply_feature_constraints/3'(_C))
c_code local build_ref_198
	ret_reg &ref[198]
	call_c   build_ref_97()
	call_c   build_ref_197()
	call_c   Dyam_Create_Binary(&ref[97],&ref[197],&ref[197])
	move_ret ref[198]
	c_ret

c_code local build_seed_75
	ret_reg &seed[75]
	call_c   build_ref_133()
	call_c   build_ref_203()
	call_c   Dyam_Seed_Start(&ref[133],&ref[203],I(0),fun21,1)
	call_c   build_ref_201()
	call_c   Dyam_Seed_Add_Comp(&ref[201],fun43,0)
	call_c   Dyam_Seed_End()
	move_ret seed[75]
	c_ret

;; TERM 201: '*RITEM*'('call_apply_feature_constraints/3'(_C), return(_D, _E))
c_code local build_ref_201
	ret_reg &ref[201]
	call_c   build_ref_0()
	call_c   build_ref_197()
	call_c   build_ref_194()
	call_c   Dyam_Create_Binary(&ref[0],&ref[197],&ref[194])
	move_ret ref[201]
	c_ret

pl_code local fun43
	call_c   build_ref_201()
	call_c   Dyam_Unify_Item(&ref[201])
	fail_ret
	pl_jump  Follow_Cont(V(1))

;; TERM 203: '*RITEM*'('call_apply_feature_constraints/3'(_C), return(_D, _E)) :> tig(2, '$TUPPLE'(35144395400924))
c_code local build_ref_203
	ret_reg &ref[203]
	call_c   build_ref_201()
	call_c   build_ref_202()
	call_c   Dyam_Create_Binary(I(9),&ref[201],&ref[202])
	move_ret ref[203]
	c_ret

;; TERM 202: tig(2, '$TUPPLE'(35144395400924))
c_code local build_ref_202
	ret_reg &ref[202]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,134217728)
	move_ret R(0)
	call_c   build_ref_1154()
	call_c   Dyam_Create_Binary(&ref[1154],N(2),R(0))
	move_ret ref[202]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_68
	ret_reg &seed[68]
	call_c   build_ref_97()
	call_c   build_ref_144()
	call_c   Dyam_Seed_Start(&ref[97],&ref[144],&ref[144],fun11,1)
	call_c   build_ref_146()
	call_c   Dyam_Seed_Add_Comp(&ref[146],&ref[144],0)
	call_c   Dyam_Seed_End()
	move_ret seed[68]
	c_ret

;; TERM 146: '*FIRST*'('call_normalize/2'(_C)) :> '$$HOLE$$'
c_code local build_ref_146
	ret_reg &ref[146]
	call_c   build_ref_145()
	call_c   Dyam_Create_Binary(I(9),&ref[145],I(7))
	move_ret ref[146]
	c_ret

;; TERM 145: '*FIRST*'('call_normalize/2'(_C))
c_code local build_ref_145
	ret_reg &ref[145]
	call_c   build_ref_93()
	call_c   build_ref_143()
	call_c   Dyam_Create_Unary(&ref[93],&ref[143])
	move_ret ref[145]
	c_ret

;; TERM 143: 'call_normalize/2'(_C)
c_code local build_ref_143
	ret_reg &ref[143]
	call_c   build_ref_1185()
	call_c   Dyam_Create_Unary(&ref[1185],V(2))
	move_ret ref[143]
	c_ret

;; TERM 144: '*CITEM*'('call_normalize/2'(_C), 'call_normalize/2'(_C))
c_code local build_ref_144
	ret_reg &ref[144]
	call_c   build_ref_97()
	call_c   build_ref_143()
	call_c   Dyam_Create_Binary(&ref[97],&ref[143],&ref[143])
	move_ret ref[144]
	c_ret

c_code local build_seed_70
	ret_reg &seed[70]
	call_c   build_ref_133()
	call_c   build_ref_157()
	call_c   Dyam_Seed_Start(&ref[133],&ref[157],I(0),fun21,1)
	call_c   build_ref_155()
	call_c   Dyam_Seed_Add_Comp(&ref[155],fun30,0)
	call_c   Dyam_Seed_End()
	move_ret seed[70]
	c_ret

;; TERM 155: '*RITEM*'('call_normalize/2'(_C), return(_D))
c_code local build_ref_155
	ret_reg &ref[155]
	call_c   build_ref_0()
	call_c   build_ref_143()
	call_c   build_ref_154()
	call_c   Dyam_Create_Binary(&ref[0],&ref[143],&ref[154])
	move_ret ref[155]
	c_ret

pl_code local fun30
	call_c   build_ref_155()
	call_c   Dyam_Unify_Item(&ref[155])
	fail_ret
	pl_jump  Follow_Cont(V(1))

;; TERM 157: '*RITEM*'('call_normalize/2'(_C), return(_D)) :> tig(1, '$TUPPLE'(35144395400924))
c_code local build_ref_157
	ret_reg &ref[157]
	call_c   build_ref_155()
	call_c   build_ref_156()
	call_c   Dyam_Create_Binary(I(9),&ref[155],&ref[156])
	move_ret ref[157]
	c_ret

;; TERM 156: tig(1, '$TUPPLE'(35144395400924))
c_code local build_ref_156
	ret_reg &ref[156]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,134217728)
	move_ret R(0)
	call_c   build_ref_1154()
	call_c   Dyam_Create_Binary(&ref[1154],N(1),R(0))
	move_ret ref[156]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 4: lctag_closure(_A, _B, _C, _D)
c_code local build_ref_4
	ret_reg &ref[4]
	call_c   build_ref_1258()
	call_c   Dyam_Term_Start(&ref[1258],4)
	call_c   Dyam_Term_Arg(V(0))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[4]
	c_ret

;; TERM 3: '*RITEM*'('call_lctag_closure/4', return(_A, _B, _C, _D))
c_code local build_ref_3
	ret_reg &ref[3]
	call_c   build_ref_0()
	call_c   build_ref_1()
	call_c   build_ref_2()
	call_c   Dyam_Create_Binary(&ref[0],&ref[1],&ref[2])
	move_ret ref[3]
	c_ret

;; TERM 2: return(_A, _B, _C, _D)
c_code local build_ref_2
	ret_reg &ref[2]
	call_c   build_ref_1139()
	call_c   Dyam_Term_Start(&ref[1139],4)
	call_c   Dyam_Term_Arg(V(0))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[2]
	c_ret

;; TERM 8: apply_feature_mode_constraints(_A, _B, _C, _D)
c_code local build_ref_8
	ret_reg &ref[8]
	call_c   build_ref_1265()
	call_c   Dyam_Term_Start(&ref[1265],4)
	call_c   Dyam_Term_Arg(V(0))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[8]
	c_ret

;; TERM 1265: apply_feature_mode_constraints
c_code local build_ref_1265
	ret_reg &ref[1265]
	call_c   Dyam_Create_Atom("apply_feature_mode_constraints")
	move_ret ref[1265]
	c_ret

;; TERM 7: '*RITEM*'('call_apply_feature_mode_constraints/4'(_A, _B), return(_C, _D))
c_code local build_ref_7
	ret_reg &ref[7]
	call_c   build_ref_0()
	call_c   build_ref_5()
	call_c   build_ref_6()
	call_c   Dyam_Create_Binary(&ref[0],&ref[5],&ref[6])
	move_ret ref[7]
	c_ret

;; TERM 5: 'call_apply_feature_mode_constraints/4'(_A, _B)
c_code local build_ref_5
	ret_reg &ref[5]
	call_c   build_ref_1152()
	call_c   Dyam_Create_Binary(&ref[1152],V(0),V(1))
	move_ret ref[5]
	c_ret

;; TERM 12: right_top_bot_constraints(_A, _B, _C)
c_code local build_ref_12
	ret_reg &ref[12]
	call_c   build_ref_1266()
	call_c   Dyam_Term_Start(&ref[1266],3)
	call_c   Dyam_Term_Arg(V(0))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_End()
	move_ret ref[12]
	c_ret

;; TERM 1266: right_top_bot_constraints
c_code local build_ref_1266
	ret_reg &ref[1266]
	call_c   Dyam_Create_Atom("right_top_bot_constraints")
	move_ret ref[1266]
	c_ret

;; TERM 11: '*RITEM*'('call_right_top_bot_constraints/3'(_A), return(_B, _C))
c_code local build_ref_11
	ret_reg &ref[11]
	call_c   build_ref_0()
	call_c   build_ref_9()
	call_c   build_ref_10()
	call_c   Dyam_Create_Binary(&ref[0],&ref[9],&ref[10])
	move_ret ref[11]
	c_ret

;; TERM 10: return(_B, _C)
c_code local build_ref_10
	ret_reg &ref[10]
	call_c   build_ref_1139()
	call_c   Dyam_Create_Binary(&ref[1139],V(1),V(2))
	move_ret ref[10]
	c_ret

;; TERM 9: 'call_right_top_bot_constraints/3'(_A)
c_code local build_ref_9
	ret_reg &ref[9]
	call_c   build_ref_1138()
	call_c   Dyam_Create_Unary(&ref[1138],V(0))
	move_ret ref[9]
	c_ret

;; TERM 15: apply_feature_constraints(_A, _B, _C)
c_code local build_ref_15
	ret_reg &ref[15]
	call_c   build_ref_1267()
	call_c   Dyam_Term_Start(&ref[1267],3)
	call_c   Dyam_Term_Arg(V(0))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_End()
	move_ret ref[15]
	c_ret

;; TERM 1267: apply_feature_constraints
c_code local build_ref_1267
	ret_reg &ref[1267]
	call_c   Dyam_Create_Atom("apply_feature_constraints")
	move_ret ref[1267]
	c_ret

;; TERM 14: '*RITEM*'('call_apply_feature_constraints/3'(_A), return(_B, _C))
c_code local build_ref_14
	ret_reg &ref[14]
	call_c   build_ref_0()
	call_c   build_ref_13()
	call_c   build_ref_10()
	call_c   Dyam_Create_Binary(&ref[0],&ref[13],&ref[10])
	move_ret ref[14]
	c_ret

;; TERM 13: 'call_apply_feature_constraints/3'(_A)
c_code local build_ref_13
	ret_reg &ref[13]
	call_c   build_ref_1148()
	call_c   Dyam_Create_Unary(&ref[1148],V(0))
	move_ret ref[13]
	c_ret

;; TERM 19: normalize(_A, _B)
c_code local build_ref_19
	ret_reg &ref[19]
	call_c   build_ref_1268()
	call_c   Dyam_Create_Binary(&ref[1268],V(0),V(1))
	move_ret ref[19]
	c_ret

;; TERM 1268: normalize
c_code local build_ref_1268
	ret_reg &ref[1268]
	call_c   Dyam_Create_Atom("normalize")
	move_ret ref[1268]
	c_ret

;; TERM 18: '*RITEM*'('call_normalize/2'(_A), return(_B))
c_code local build_ref_18
	ret_reg &ref[18]
	call_c   build_ref_0()
	call_c   build_ref_16()
	call_c   build_ref_17()
	call_c   Dyam_Create_Binary(&ref[0],&ref[16],&ref[17])
	move_ret ref[18]
	c_ret

;; TERM 17: return(_B)
c_code local build_ref_17
	ret_reg &ref[17]
	call_c   build_ref_1139()
	call_c   Dyam_Create_Unary(&ref[1139],V(1))
	move_ret ref[17]
	c_ret

;; TERM 16: 'call_normalize/2'(_A)
c_code local build_ref_16
	ret_reg &ref[16]
	call_c   build_ref_1185()
	call_c   Dyam_Create_Unary(&ref[1185],V(0))
	move_ret ref[16]
	c_ret

;; TERM 20: lctag_filter(_A, _B, _C, _D, _E, _F)
c_code local build_ref_20
	ret_reg &ref[20]
	call_c   build_ref_1209()
	call_c   Dyam_Term_Start(&ref[1209],6)
	call_c   Dyam_Term_Arg(V(0))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[20]
	c_ret

;; TERM 22: '*RITEM*'(lctag_filter(_A, _B, _C, _D, _E, _F), voidret)
c_code local build_ref_22
	ret_reg &ref[22]
	call_c   build_ref_0()
	call_c   build_ref_20()
	call_c   build_ref_21()
	call_c   Dyam_Create_Binary(&ref[0],&ref[20],&ref[21])
	move_ret ref[22]
	c_ret

;; TERM 25: decompose_args(_A, _B, _C)
c_code local build_ref_25
	ret_reg &ref[25]
	call_c   build_ref_1269()
	call_c   Dyam_Term_Start(&ref[1269],3)
	call_c   Dyam_Term_Arg(V(0))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_End()
	move_ret ref[25]
	c_ret

;; TERM 1269: decompose_args
c_code local build_ref_1269
	ret_reg &ref[1269]
	call_c   Dyam_Create_Atom("decompose_args")
	move_ret ref[1269]
	c_ret

;; TERM 24: '*RITEM*'('call_decompose_args/3'(_A), return(_B, _C))
c_code local build_ref_24
	ret_reg &ref[24]
	call_c   build_ref_0()
	call_c   build_ref_23()
	call_c   build_ref_10()
	call_c   Dyam_Create_Binary(&ref[0],&ref[23],&ref[10])
	move_ret ref[24]
	c_ret

;; TERM 23: 'call_decompose_args/3'(_A)
c_code local build_ref_23
	ret_reg &ref[23]
	call_c   build_ref_1248()
	call_c   Dyam_Create_Unary(&ref[1248],V(0))
	move_ret ref[23]
	c_ret

;; TERM 26: wrapping_predicate(_A)
c_code local build_ref_26
	ret_reg &ref[26]
	call_c   build_ref_1214()
	call_c   Dyam_Create_Unary(&ref[1214],V(0))
	move_ret ref[26]
	c_ret

;; TERM 27: '*RITEM*'(wrapping_predicate(_A), voidret)
c_code local build_ref_27
	ret_reg &ref[27]
	call_c   build_ref_0()
	call_c   build_ref_26()
	call_c   build_ref_21()
	call_c   Dyam_Create_Binary(&ref[0],&ref[26],&ref[21])
	move_ret ref[27]
	c_ret

;; TERM 31: core_info(_A, _B, _C)
c_code local build_ref_31
	ret_reg &ref[31]
	call_c   build_ref_1270()
	call_c   Dyam_Term_Start(&ref[1270],3)
	call_c   Dyam_Term_Arg(V(0))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_End()
	move_ret ref[31]
	c_ret

;; TERM 1270: core_info
c_code local build_ref_1270
	ret_reg &ref[1270]
	call_c   Dyam_Create_Atom("core_info")
	move_ret ref[1270]
	c_ret

;; TERM 30: '*RITEM*'('call_core_info/3'(_A, _B), return(_C))
c_code local build_ref_30
	ret_reg &ref[30]
	call_c   build_ref_0()
	call_c   build_ref_28()
	call_c   build_ref_29()
	call_c   Dyam_Create_Binary(&ref[0],&ref[28],&ref[29])
	move_ret ref[30]
	c_ret

;; TERM 29: return(_C)
c_code local build_ref_29
	ret_reg &ref[29]
	call_c   build_ref_1139()
	call_c   Dyam_Create_Unary(&ref[1139],V(2))
	move_ret ref[29]
	c_ret

;; TERM 28: 'call_core_info/3'(_A, _B)
c_code local build_ref_28
	ret_reg &ref[28]
	call_c   build_ref_1260()
	call_c   Dyam_Create_Binary(&ref[1260],V(0),V(1))
	move_ret ref[28]
	c_ret

;; TERM 32: verbose_tag(_A)
c_code local build_ref_32
	ret_reg &ref[32]
	call_c   build_ref_1261()
	call_c   Dyam_Create_Unary(&ref[1261],V(0))
	move_ret ref[32]
	c_ret

;; TERM 33: '*RITEM*'(verbose_tag(_A), voidret)
c_code local build_ref_33
	ret_reg &ref[33]
	call_c   build_ref_0()
	call_c   build_ref_32()
	call_c   build_ref_21()
	call_c   Dyam_Create_Binary(&ref[0],&ref[32],&ref[21])
	move_ret ref[33]
	c_ret

;; TERM 36: tagguide_name(_A, _B, _C)
c_code local build_ref_36
	ret_reg &ref[36]
	call_c   build_ref_1271()
	call_c   Dyam_Term_Start(&ref[1271],3)
	call_c   Dyam_Term_Arg(V(0))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_End()
	move_ret ref[36]
	c_ret

;; TERM 1271: tagguide_name
c_code local build_ref_1271
	ret_reg &ref[1271]
	call_c   Dyam_Create_Atom("tagguide_name")
	move_ret ref[1271]
	c_ret

;; TERM 35: '*RITEM*'('call_tagguide_name/3'(_A, _B), return(_C))
c_code local build_ref_35
	ret_reg &ref[35]
	call_c   build_ref_0()
	call_c   build_ref_34()
	call_c   build_ref_29()
	call_c   Dyam_Create_Binary(&ref[0],&ref[34],&ref[29])
	move_ret ref[35]
	c_ret

;; TERM 34: 'call_tagguide_name/3'(_A, _B)
c_code local build_ref_34
	ret_reg &ref[34]
	call_c   build_ref_1262()
	call_c   Dyam_Create_Binary(&ref[1262],V(0),V(1))
	move_ret ref[34]
	c_ret

;; TERM 39: tagguide_pred(_A, _B)
c_code local build_ref_39
	ret_reg &ref[39]
	call_c   build_ref_1272()
	call_c   Dyam_Create_Binary(&ref[1272],V(0),V(1))
	move_ret ref[39]
	c_ret

;; TERM 1272: tagguide_pred
c_code local build_ref_1272
	ret_reg &ref[1272]
	call_c   Dyam_Create_Atom("tagguide_pred")
	move_ret ref[1272]
	c_ret

;; TERM 38: '*RITEM*'('call_tagguide_pred/2'(_A), return(_B))
c_code local build_ref_38
	ret_reg &ref[38]
	call_c   build_ref_0()
	call_c   build_ref_37()
	call_c   build_ref_17()
	call_c   Dyam_Create_Binary(&ref[0],&ref[37],&ref[17])
	move_ret ref[38]
	c_ret

;; TERM 37: 'call_tagguide_pred/2'(_A)
c_code local build_ref_37
	ret_reg &ref[37]
	call_c   build_ref_1263()
	call_c   Dyam_Create_Unary(&ref[1263],V(0))
	move_ret ref[37]
	c_ret

;; TERM 40: register_predicate(_A)
c_code local build_ref_40
	ret_reg &ref[40]
	call_c   build_ref_1264()
	call_c   Dyam_Create_Unary(&ref[1264],V(0))
	move_ret ref[40]
	c_ret

;; TERM 41: '*RITEM*'(register_predicate(_A), voidret)
c_code local build_ref_41
	ret_reg &ref[41]
	call_c   build_ref_0()
	call_c   build_ref_40()
	call_c   build_ref_21()
	call_c   Dyam_Create_Binary(&ref[0],&ref[40],&ref[21])
	move_ret ref[41]
	c_ret

;; TERM 42: make_true_tig_callret(_A, _B, _C, _D, _E, _F, _G)
c_code local build_ref_42
	ret_reg &ref[42]
	call_c   build_ref_1155()
	call_c   Dyam_Term_Start(&ref[1155],7)
	call_c   Dyam_Term_Arg(V(0))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[42]
	c_ret

;; TERM 43: '*RITEM*'(make_true_tig_callret(_A, _B, _C, _D, _E, _F, _G), voidret)
c_code local build_ref_43
	ret_reg &ref[43]
	call_c   build_ref_0()
	call_c   build_ref_42()
	call_c   build_ref_21()
	call_c   Dyam_Create_Binary(&ref[0],&ref[42],&ref[21])
	move_ret ref[43]
	c_ret

;; TERM 45: '*RITEM*'(show_tag_features, voidret)
c_code local build_ref_45
	ret_reg &ref[45]
	call_c   build_ref_0()
	call_c   build_ref_44()
	call_c   build_ref_21()
	call_c   Dyam_Create_Binary(&ref[0],&ref[44],&ref[21])
	move_ret ref[45]
	c_ret

;; TERM 46: potential_tig(_A, _B)
c_code local build_ref_46
	ret_reg &ref[46]
	call_c   build_ref_1131()
	call_c   Dyam_Create_Binary(&ref[1131],V(0),V(1))
	move_ret ref[46]
	c_ret

;; TERM 47: '*RITEM*'(potential_tig(_A, _B), voidret)
c_code local build_ref_47
	ret_reg &ref[47]
	call_c   build_ref_0()
	call_c   build_ref_46()
	call_c   build_ref_21()
	call_c   Dyam_Create_Binary(&ref[0],&ref[46],&ref[21])
	move_ret ref[47]
	c_ret

;; TERM 48: node_auxtree(_A, _B, _C)
c_code local build_ref_48
	ret_reg &ref[48]
	call_c   build_ref_1210()
	call_c   Dyam_Term_Start(&ref[1210],3)
	call_c   Dyam_Term_Arg(V(0))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_End()
	move_ret ref[48]
	c_ret

;; TERM 49: '*RITEM*'(node_auxtree(_A, _B, _C), voidret)
c_code local build_ref_49
	ret_reg &ref[49]
	call_c   build_ref_0()
	call_c   build_ref_48()
	call_c   build_ref_21()
	call_c   Dyam_Create_Binary(&ref[0],&ref[48],&ref[21])
	move_ret ref[49]
	c_ret

;; TERM 50: not_tig(_A, _B)
c_code local build_ref_50
	ret_reg &ref[50]
	call_c   build_ref_1254()
	call_c   Dyam_Create_Binary(&ref[1254],V(0),V(1))
	move_ret ref[50]
	c_ret

;; TERM 51: '*RITEM*'(not_tig(_A, _B), voidret)
c_code local build_ref_51
	ret_reg &ref[51]
	call_c   build_ref_0()
	call_c   build_ref_50()
	call_c   build_ref_21()
	call_c   Dyam_Create_Binary(&ref[0],&ref[50],&ref[21])
	move_ret ref[51]
	c_ret

;; TERM 1031: [[_B|_C]]
c_code local build_ref_1031
	ret_reg &ref[1031]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(1),V(2))
	move_ret R(0)
	call_c   Dyam_Create_List(R(0),I(0))
	move_ret ref[1031]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1033: [[_F]|_H]
c_code local build_ref_1033
	ret_reg &ref[1033]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(5),I(0))
	move_ret R(0)
	call_c   Dyam_Create_List(R(0),V(7))
	move_ret ref[1033]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1035: [[_F,_I|_G]|_H]
c_code local build_ref_1035
	ret_reg &ref[1035]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1038()
	call_c   Dyam_Create_List(V(5),&ref[1038])
	move_ret R(0)
	call_c   Dyam_Create_List(R(0),V(7))
	move_ret ref[1035]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1038: [_I|_G]
c_code local build_ref_1038
	ret_reg &ref[1038]
	call_c   Dyam_Create_List(V(8),V(6))
	move_ret ref[1038]
	c_ret

;; TERM 1034: [_I]
c_code local build_ref_1034
	ret_reg &ref[1034]
	call_c   Dyam_Create_List(V(8),I(0))
	move_ret ref[1034]
	c_ret

;; TERM 1032: [[_F|_G]|_H]
c_code local build_ref_1032
	ret_reg &ref[1032]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(5),V(6))
	move_ret R(0)
	call_c   Dyam_Create_List(R(0),V(7))
	move_ret ref[1032]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1039: [_O|_H]
c_code local build_ref_1039
	ret_reg &ref[1039]
	call_c   Dyam_Create_List(V(14),V(7))
	move_ret ref[1039]
	c_ret

;; TERM 868: pos(_D)
c_code local build_ref_868
	ret_reg &ref[868]
	call_c   build_ref_1211()
	call_c   Dyam_Create_Unary(&ref[1211],V(3))
	move_ret ref[868]
	c_ret

;; TERM 870: postscan(_E)
c_code local build_ref_870
	ret_reg &ref[870]
	call_c   build_ref_1213()
	call_c   Dyam_Create_Unary(&ref[1213],V(4))
	move_ret ref[870]
	c_ret

;; TERM 869: postscan(_D)
c_code local build_ref_869
	ret_reg &ref[869]
	call_c   build_ref_1213()
	call_c   Dyam_Create_Unary(&ref[1213],V(3))
	move_ret ref[869]
	c_ret

;; TERM 873: postcat(_F, _H)
c_code local build_ref_873
	ret_reg &ref[873]
	call_c   build_ref_1212()
	call_c   Dyam_Create_Binary(&ref[1212],V(5),V(7))
	move_ret ref[873]
	c_ret

;; TERM 871: postcat(_D)
c_code local build_ref_871
	ret_reg &ref[871]
	call_c   build_ref_1212()
	call_c   Dyam_Create_Unary(&ref[1212],V(3))
	move_ret ref[871]
	c_ret

;; TERM 874: _I ; _J
c_code local build_ref_874
	ret_reg &ref[874]
	call_c   Dyam_Create_Binary(I(5),V(8),V(9))
	move_ret ref[874]
	c_ret

;; TERM 872: tag_features(_F, _G, _H)
c_code local build_ref_872
	ret_reg &ref[872]
	call_c   build_ref_1149()
	call_c   Dyam_Term_Start(&ref[1149],3)
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[872]
	c_ret

;; TERM 875: coanchor(_F, _H)
c_code local build_ref_875
	ret_reg &ref[875]
	call_c   build_ref_1063()
	call_c   Dyam_Create_Binary(&ref[1063],V(5),V(7))
	move_ret ref[875]
	c_ret

;; TERM 836: tag_node{id=> _J, label=> _K, children=> _L, kind=> foot, adj=> _M, spine=> _N, top=> _O, bot=> _P, token=> _Q, adjleft=> _R, adjright=> _S, adjwrap=> _T}
c_code local build_ref_836
	ret_reg &ref[836]
	call_c   build_ref_1172()
	call_c   build_ref_992()
	call_c   Dyam_Term_Start(&ref[1172],12)
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(&ref[992])
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_End()
	move_ret ref[836]
	c_ret

;; TERM 837: tag_node{id=> _U, label=> _V, children=> _W, kind=> _X, adj=> _Y, spine=> yes, top=> _Z, bot=> _A1, token=> _B1, adjleft=> _C1, adjright=> _D1, adjwrap=> _E1}
c_code local build_ref_837
	ret_reg &ref[837]
	call_c   build_ref_1172()
	call_c   build_ref_319()
	call_c   Dyam_Term_Start(&ref[1172],12)
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_Arg(V(21))
	call_c   Dyam_Term_Arg(V(22))
	call_c   Dyam_Term_Arg(V(23))
	call_c   Dyam_Term_Arg(V(24))
	call_c   Dyam_Term_Arg(&ref[319])
	call_c   Dyam_Term_Arg(V(25))
	call_c   Dyam_Term_Arg(V(26))
	call_c   Dyam_Term_Arg(V(27))
	call_c   Dyam_Term_Arg(V(28))
	call_c   Dyam_Term_Arg(V(29))
	call_c   Dyam_Term_Arg(V(30))
	call_c   Dyam_Term_End()
	move_ret ref[837]
	c_ret

;; TERM 830: _D ; _E
c_code local build_ref_830
	ret_reg &ref[830]
	call_c   Dyam_Create_Binary(I(5),V(3),V(4))
	move_ret ref[830]
	c_ret

;; TERM 831: guard{goal=> _D, plus=> _F, minus=> _G}
c_code local build_ref_831
	ret_reg &ref[831]
	call_c   build_ref_1198()
	call_c   Dyam_Term_Start(&ref[1198],3)
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[831]
	c_ret

;; TERM 833: '$VAR'(_G1, ['$SET$',adj(no, yes, strict, atmostone, one, sync),62])
c_code local build_ref_833
	ret_reg &ref[833]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1178()
	call_c   build_ref_706()
	call_c   build_ref_319()
	call_c   build_ref_673()
	call_c   build_ref_685()
	call_c   build_ref_686()
	call_c   build_ref_623()
	call_c   Dyam_Term_Start(&ref[1178],6)
	call_c   Dyam_Term_Arg(&ref[706])
	call_c   Dyam_Term_Arg(&ref[319])
	call_c   Dyam_Term_Arg(&ref[673])
	call_c   Dyam_Term_Arg(&ref[685])
	call_c   Dyam_Term_Arg(&ref[686])
	call_c   Dyam_Term_Arg(&ref[623])
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   Dyam_Term_Start(I(8),3)
	call_c   Dyam_Term_Arg(V(32))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(N(62))
	call_c   Dyam_Term_End()
	move_ret ref[833]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 832: tag_node{id=> _T, label=> _U, children=> _V, kind=> _W, adj=> _X, spine=> yes, top=> _Y, bot=> _Z, token=> _A1, adjleft=> _B1, adjright=> _C1, adjwrap=> _D1}
c_code local build_ref_832
	ret_reg &ref[832]
	call_c   build_ref_1172()
	call_c   build_ref_319()
	call_c   Dyam_Term_Start(&ref[1172],12)
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_Arg(V(21))
	call_c   Dyam_Term_Arg(V(22))
	call_c   Dyam_Term_Arg(V(23))
	call_c   Dyam_Term_Arg(&ref[319])
	call_c   Dyam_Term_Arg(V(24))
	call_c   Dyam_Term_Arg(V(25))
	call_c   Dyam_Term_Arg(V(26))
	call_c   Dyam_Term_Arg(V(27))
	call_c   Dyam_Term_Arg(V(28))
	call_c   Dyam_Term_Arg(V(29))
	call_c   Dyam_Term_End()
	move_ret ref[832]
	c_ret

;; TERM 835: guard{goal=> tag_node{id=> _T, label=> _U, children=> _V, kind=> _W, adj=> _X, spine=> yes, top=> _Y, bot=> _Z, token=> _A1, adjleft=> _B1, adjright=> _C1, adjwrap=> _D1}, plus=> _E1, minus=> _F1}
c_code local build_ref_835
	ret_reg &ref[835]
	call_c   build_ref_1198()
	call_c   build_ref_832()
	call_c   Dyam_Term_Start(&ref[1198],3)
	call_c   Dyam_Term_Arg(&ref[832])
	call_c   Dyam_Term_Arg(V(30))
	call_c   Dyam_Term_Arg(V(31))
	call_c   Dyam_Term_End()
	move_ret ref[835]
	c_ret

;; TERM 721: guard{goal=> _C, plus=> _E, minus=> _F}
c_code local build_ref_721
	ret_reg &ref[721]
	call_c   build_ref_1198()
	call_c   Dyam_Term_Start(&ref[1198],3)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[721]
	c_ret

;; TERM 722: [_G|_H]
c_code local build_ref_722
	ret_reg &ref[722]
	call_c   Dyam_Create_List(V(6),V(7))
	move_ret ref[722]
	c_ret

;; TERM 723: tag_node{id=> _I, label=> _J, children=> _K, kind=> foot, adj=> _L, spine=> _M, top=> _N, bot=> _O, token=> _P, adjleft=> _Q, adjright=> _R, adjwrap=> _S}
c_code local build_ref_723
	ret_reg &ref[723]
	call_c   build_ref_1172()
	call_c   build_ref_992()
	call_c   Dyam_Term_Start(&ref[1172],12)
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(&ref[992])
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_End()
	move_ret ref[723]
	c_ret

;; TERM 724: tag_node{id=> _T, label=> _U, children=> [_G|_V], kind=> _W, adj=> _X, spine=> yes, top=> _Y, bot=> _Z, token=> _A1, adjleft=> _B1, adjright=> _C1, adjwrap=> _D1}
c_code local build_ref_724
	ret_reg &ref[724]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(6),V(21))
	move_ret R(0)
	call_c   build_ref_1172()
	call_c   build_ref_319()
	call_c   Dyam_Term_Start(&ref[1172],12)
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(22))
	call_c   Dyam_Term_Arg(V(23))
	call_c   Dyam_Term_Arg(&ref[319])
	call_c   Dyam_Term_Arg(V(24))
	call_c   Dyam_Term_Arg(V(25))
	call_c   Dyam_Term_Arg(V(26))
	call_c   Dyam_Term_Arg(V(27))
	call_c   Dyam_Term_Arg(V(28))
	call_c   Dyam_Term_Arg(V(29))
	call_c   Dyam_Term_End()
	move_ret ref[724]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 668: \+ \+ _E = _F
c_code local build_ref_668
	ret_reg &ref[668]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1220()
	call_c   Dyam_Create_Binary(&ref[1220],V(4),V(5))
	move_ret R(0)
	call_c   build_ref_1273()
	call_c   Dyam_Create_Unary(&ref[1273],R(0))
	move_ret R(0)
	call_c   Dyam_Create_Unary(&ref[1273],R(0))
	move_ret ref[668]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1273: \+
c_code local build_ref_1273
	ret_reg &ref[1273]
	call_c   Dyam_Create_Atom("\\+")
	move_ret ref[1273]
	c_ret

;; TERM 669: _E = _E
c_code local build_ref_669
	ret_reg &ref[669]
	call_c   build_ref_1220()
	call_c   Dyam_Create_Binary(&ref[1220],V(4),V(4))
	move_ret ref[669]
	c_ret

;; TERM 604: [(_F ; _G)|_H]
c_code local build_ref_604
	ret_reg &ref[604]
	call_c   build_ref_605()
	call_c   Dyam_Create_List(&ref[605],V(7))
	move_ret ref[604]
	c_ret

;; TERM 603: scan(_D)
c_code local build_ref_603
	ret_reg &ref[603]
	call_c   build_ref_1062()
	call_c   Dyam_Create_Unary(&ref[1062],V(3))
	move_ret ref[603]
	c_ret

;; TERM 606: [_D|_I]
c_code local build_ref_606
	ret_reg &ref[606]
	call_c   Dyam_Create_List(V(3),V(8))
	move_ret ref[606]
	c_ret

;; TERM 599: tag_node{id=> _B, label=> _C, children=> _D, kind=> _E, adj=> _F, spine=> _G, top=> _H, bot=> _I, token=> _J, adjleft=> _K, adjright=> _L, adjwrap=> _M}
c_code local build_ref_599
	ret_reg &ref[599]
	call_c   build_ref_1172()
	call_c   Dyam_Term_Start(&ref[1172],12)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_End()
	move_ret ref[599]
	c_ret

;; TERM 602: '$CLOSURE'('$fun'(227, 0, 1126158756), '$TUPPLE'(35144395659396))
c_code local build_ref_602
	ret_reg &ref[602]
	call_c   build_ref_601()
	call_c   Dyam_Closure_Aux(fun227,&ref[601])
	move_ret ref[602]
	c_ret

pl_code local fun226
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Deallocate()
	pl_ret

;; TERM 598: '$CLOSURE'('$fun'(225, 0, 1126151852), '$TUPPLE'(0))
c_code local build_ref_598
	ret_reg &ref[598]
	call_c   Dyam_Closure_Aux(fun225,I(0))
	move_ret ref[598]
	c_ret

long local pool_fun227[3]=[2,build_ref_598,build_ref_599]

pl_code local fun227
	call_c   Dyam_Pool(pool_fun227)
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun226)
	call_c   Dyam_Set_Cut()
	move     &ref[598], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	move     &ref[599], R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Load(6,V(13))
	call_c   Dyam_Reg_Deallocate(4)
	pl_jump  fun224()

;; TERM 601: '$TUPPLE'(35144395659396)
c_code local build_ref_601
	ret_reg &ref[601]
	call_c   Dyam_Create_Simple_Tupple(0,268402688)
	move_ret ref[601]
	c_ret

;; TERM 581: tag_nt(_C, (_O / _P))
c_code local build_ref_581
	ret_reg &ref[581]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1170()
	call_c   Dyam_Create_Binary(&ref[1170],V(14),V(15))
	move_ret R(0)
	call_c   build_ref_1171()
	call_c   Dyam_Create_Binary(&ref[1171],V(2),R(0))
	move_ret ref[581]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 556: make_true_tig_callret(_B, _K, _L, _M, _N, _G, _H)
c_code local build_ref_556
	ret_reg &ref[556]
	call_c   build_ref_1155()
	call_c   Dyam_Term_Start(&ref[1155],7)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[556]
	c_ret

;; TERM 555: '$CLOSURE'('$fun'(203, 0, 1125990724), '$TUPPLE'(35144395139652))
c_code local build_ref_555
	ret_reg &ref[555]
	call_c   build_ref_554()
	call_c   Dyam_Closure_Aux(fun203,&ref[554])
	move_ret ref[555]
	c_ret

pl_code local fun203
	call_c   Dyam_Unify(V(2),V(10))
	fail_ret
	call_c   Dyam_Unify(V(3),V(11))
	fail_ret
	call_c   Dyam_Unify(V(4),V(12))
	fail_ret
	call_c   Dyam_Unify(V(5),V(13))
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Deallocate()
	pl_ret

;; TERM 554: '$TUPPLE'(35144395139652)
c_code local build_ref_554
	ret_reg &ref[554]
	call_c   Dyam_Create_Simple_Tupple(0,126320640)
	move_ret ref[554]
	c_ret

;; TERM 449: '%% Compiler Analyzer tag2tig: ~w ~w\n'
c_code local build_ref_449
	ret_reg &ref[449]
	call_c   Dyam_Create_Atom("%% Compiler Analyzer tag2tig: ~w ~w\n")
	move_ret ref[449]
	c_ret

;; TERM 442: [_N]
c_code local build_ref_442
	ret_reg &ref[442]
	call_c   Dyam_Create_List(V(13),I(0))
	move_ret ref[442]
	c_ret

;; TERM 441: '%% File ~q\n'
c_code local build_ref_441
	ret_reg &ref[441]
	call_c   Dyam_Create_Atom("%% File ~q\n")
	move_ret ref[441]
	c_ret

;; TERM 440: main_file(_L, _M)
c_code local build_ref_440
	ret_reg &ref[440]
	call_c   build_ref_1274()
	call_c   Dyam_Create_Binary(&ref[1274],V(11),V(12))
	move_ret ref[440]
	c_ret

;; TERM 1274: main_file
c_code local build_ref_1274
	ret_reg &ref[1274]
	call_c   Dyam_Create_Atom("main_file")
	move_ret ref[1274]
	c_ret

;; TERM 444: ';; File stdin\n'
c_code local build_ref_444
	ret_reg &ref[444]
	call_c   Dyam_Create_Atom(";; File stdin\n")
	move_ret ref[444]
	c_ret

;; TERM 443: main_file(-, _O)
c_code local build_ref_443
	ret_reg &ref[443]
	call_c   build_ref_1274()
	call_c   build_ref_255()
	call_c   Dyam_Create_Binary(&ref[1274],&ref[255],V(14))
	move_ret ref[443]
	c_ret

;; TERM 439: [_C,_D,_B,_E,_F,_G]
c_code local build_ref_439
	ret_reg &ref[439]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Tupple(4,6,I(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(1),R(0))
	move_ret R(0)
	call_c   Dyam_Create_Tupple(2,3,R(0))
	move_ret ref[439]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 438: '%% Date    : ~w ~w ~w at ~w:~w:~w\n'
c_code local build_ref_438
	ret_reg &ref[438]
	call_c   Dyam_Create_Atom("%% Date    : ~w ~w ~w at ~w:~w:~w\n")
	move_ret ref[438]
	c_ret

;; TERM 437: [_H,_I]
c_code local build_ref_437
	ret_reg &ref[437]
	call_c   Dyam_Create_Tupple(7,8,I(0))
	move_ret ref[437]
	c_ret

;; TERM 436: '%% Compiler Analyzer lctag: ~w ~w\n'
c_code local build_ref_436
	ret_reg &ref[436]
	call_c   Dyam_Create_Atom("%% Compiler Analyzer lctag: ~w ~w\n")
	move_ret ref[436]
	c_ret

;; TERM 435: compiler_info{name=> _H, version=> _I, author=> _J, email=> _K}
c_code local build_ref_435
	ret_reg &ref[435]
	call_c   build_ref_1275()
	call_c   Dyam_Term_Start(&ref[1275],4)
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_End()
	move_ret ref[435]
	c_ret

;; TERM 1275: compiler_info!'$ft'
c_code local build_ref_1275
	ret_reg &ref[1275]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1279()
	call_c   Dyam_Create_List(&ref[1279],I(0))
	move_ret R(0)
	call_c   build_ref_1278()
	call_c   Dyam_Create_List(&ref[1278],R(0))
	move_ret R(0)
	call_c   build_ref_1277()
	call_c   Dyam_Create_List(&ref[1277],R(0))
	move_ret R(0)
	call_c   build_ref_1145()
	call_c   Dyam_Create_List(&ref[1145],R(0))
	move_ret R(0)
	call_c   build_ref_1276()
	call_c   Dyam_Create_Atom_Module("$ft",&ref[1276])
	move_ret ref[1275]
	call_c   DYAM_Feature_2(&ref[1276],R(0))
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1276: compiler_info
c_code local build_ref_1276
	ret_reg &ref[1276]
	call_c   Dyam_Create_Atom("compiler_info")
	move_ret ref[1276]
	c_ret

;; TERM 1277: version
c_code local build_ref_1277
	ret_reg &ref[1277]
	call_c   Dyam_Create_Atom("version")
	move_ret ref[1277]
	c_ret

;; TERM 1278: author
c_code local build_ref_1278
	ret_reg &ref[1278]
	call_c   Dyam_Create_Atom("author")
	move_ret ref[1278]
	c_ret

;; TERM 1279: email
c_code local build_ref_1279
	ret_reg &ref[1279]
	call_c   Dyam_Create_Atom("email")
	move_ret ref[1279]
	c_ret

;; TERM 317: tag_features(_S, _Q, _R)
c_code local build_ref_317
	ret_reg &ref[317]
	call_c   build_ref_1149()
	call_c   Dyam_Term_Start(&ref[1149],3)
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_End()
	move_ret ref[317]
	c_ret

;; TERM 339: '*SA-SUBST*'(_W, _X, _Z1, _K)
c_code local build_ref_339
	ret_reg &ref[339]
	call_c   build_ref_1218()
	call_c   Dyam_Term_Start(&ref[1218],4)
	call_c   Dyam_Term_Arg(V(22))
	call_c   Dyam_Term_Arg(V(23))
	call_c   Dyam_Term_Arg(V(51))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_End()
	move_ret ref[339]
	c_ret

;; TERM 346: '*SIMPLE-KLEENE*'(_B1, _V1, '*KLEENE-ALTERNATIVE*'(_Y1, _C2))
c_code local build_ref_346
	ret_reg &ref[346]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1281()
	call_c   Dyam_Create_Binary(&ref[1281],V(50),V(54))
	move_ret R(0)
	call_c   build_ref_1280()
	call_c   Dyam_Term_Start(&ref[1280],3)
	call_c   Dyam_Term_Arg(V(27))
	call_c   Dyam_Term_Arg(V(47))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_End()
	move_ret ref[346]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1280: '*SIMPLE-KLEENE*'
c_code local build_ref_1280
	ret_reg &ref[1280]
	call_c   Dyam_Create_Atom("*SIMPLE-KLEENE*")
	move_ret ref[1280]
	c_ret

;; TERM 1281: '*KLEENE-ALTERNATIVE*'
c_code local build_ref_1281
	ret_reg &ref[1281]
	call_c   Dyam_Create_Atom("*KLEENE-ALTERNATIVE*")
	move_ret ref[1281]
	c_ret

;; TERM 350: '*CONT*'('*SA-SUBST*'(_W, _X, _Z1, _K))
c_code local build_ref_350
	ret_reg &ref[350]
	call_c   build_ref_1217()
	call_c   build_ref_339()
	call_c   Dyam_Create_Unary(&ref[1217],&ref[339])
	move_ret ref[350]
	c_ret

;; TERM 338: '*KLEENE-LAST*'(_B1, _O1, _L1, _V1, _X1, noop)
c_code local build_ref_338
	ret_reg &ref[338]
	call_c   build_ref_1282()
	call_c   build_ref_755()
	call_c   Dyam_Term_Start(&ref[1282],6)
	call_c   Dyam_Term_Arg(V(27))
	call_c   Dyam_Term_Arg(V(40))
	call_c   Dyam_Term_Arg(V(37))
	call_c   Dyam_Term_Arg(V(47))
	call_c   Dyam_Term_Arg(V(49))
	call_c   Dyam_Term_Arg(&ref[755])
	call_c   Dyam_Term_End()
	move_ret ref[338]
	c_ret

;; TERM 1282: '*KLEENE-LAST*'
c_code local build_ref_1282
	ret_reg &ref[1282]
	call_c   Dyam_Create_Atom("*KLEENE-LAST*")
	move_ret ref[1282]
	c_ret

;; TERM 334: [_I,_J,[_Y,inf]]
c_code local build_ref_334
	ret_reg &ref[334]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1283()
	call_c   Dyam_Create_List(&ref[1283],I(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(24),R(0))
	move_ret R(0)
	call_c   Dyam_Create_List(R(0),I(0))
	move_ret R(0)
	call_c   Dyam_Create_Tupple(8,9,R(0))
	move_ret ref[334]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1283: inf
c_code local build_ref_1283
	ret_reg &ref[1283]
	call_c   Dyam_Create_Atom("inf")
	move_ret ref[1283]
	c_ret

;; TERM 333: [_I|_T1]
c_code local build_ref_333
	ret_reg &ref[333]
	call_c   Dyam_Create_List(V(8),V(45))
	move_ret ref[333]
	c_ret

;; TERM 331: [_G|_Q1]
c_code local build_ref_331
	ret_reg &ref[331]
	call_c   Dyam_Create_List(V(6),V(42))
	move_ret ref[331]
	c_ret

;; TERM 330: [_P1|_Q1]
c_code local build_ref_330
	ret_reg &ref[330]
	call_c   Dyam_Create_List(V(41),V(42))
	move_ret ref[330]
	c_ret

;; TERM 329: [_D1,_U|_N1]
c_code local build_ref_329
	ret_reg &ref[329]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(20),V(39))
	move_ret R(0)
	call_c   Dyam_Create_List(V(29),R(0))
	move_ret ref[329]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 328: [_M1|_N1]
c_code local build_ref_328
	ret_reg &ref[328]
	call_c   Dyam_Create_List(V(38),V(39))
	move_ret ref[328]
	c_ret

;; TERM 327: [_C1,_T|_K1]
c_code local build_ref_327
	ret_reg &ref[327]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(19),V(36))
	move_ret R(0)
	call_c   Dyam_Create_List(V(28),R(0))
	move_ret ref[327]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 326: [_J1|_K1]
c_code local build_ref_326
	ret_reg &ref[326]
	call_c   Dyam_Create_List(V(35),V(36))
	move_ret ref[326]
	c_ret

;; TERM 325: [_C1,_D1,_E1,_F1,_G1,_H1]
c_code local build_ref_325
	ret_reg &ref[325]
	call_c   Dyam_Create_Tupple(28,33,I(0))
	move_ret ref[325]
	c_ret

;; TERM 324: kleene_conditions([_Y,inf], [_C1,_D1], _E1, _F1, _G1)
c_code local build_ref_324
	ret_reg &ref[324]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_1283()
	call_c   Dyam_Create_List(&ref[1283],I(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(24),R(0))
	move_ret R(0)
	call_c   Dyam_Create_Tupple(28,29,I(0))
	move_ret R(1)
	call_c   build_ref_1284()
	call_c   Dyam_Term_Start(&ref[1284],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(30))
	call_c   Dyam_Term_Arg(V(31))
	call_c   Dyam_Term_Arg(V(32))
	call_c   Dyam_Term_End()
	move_ret ref[324]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1284: kleene_conditions
c_code local build_ref_1284
	ret_reg &ref[1284]
	call_c   Dyam_Create_Atom("kleene_conditions")
	move_ret ref[1284]
	c_ret

;; TERM 323: [_A1]
c_code local build_ref_323
	ret_reg &ref[323]
	call_c   Dyam_Create_List(V(26),I(0))
	move_ret ref[323]
	c_ret

;; TERM 322: '_kleene~w'
c_code local build_ref_322
	ret_reg &ref[322]
	call_c   Dyam_Create_Atom("_kleene~w")
	move_ret ref[322]
	c_ret

;; TERM 321: kleene
c_code local build_ref_321
	ret_reg &ref[321]
	call_c   Dyam_Create_Atom("kleene")
	move_ret ref[321]
	c_ret

;; TERM 320: [_K]
c_code local build_ref_320
	ret_reg &ref[320]
	call_c   Dyam_Create_List(V(10),I(0))
	move_ret ref[320]
	c_ret

;; TERM 318: [_T,_Q,_U,_R,_G,_H,_I,_J]
c_code local build_ref_318
	ret_reg &ref[318]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Tupple(6,9,I(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(17),R(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(20),R(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(16),R(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(19),R(0))
	move_ret ref[318]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 351: tig_adj((_C / _D), _E, _F)
c_code local build_ref_351
	ret_reg &ref[351]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1170()
	call_c   Dyam_Create_Binary(&ref[1170],V(2),V(3))
	move_ret R(0)
	call_c   build_ref_1215()
	call_c   Dyam_Term_Start(&ref[1215],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[351]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

pl_code local fun18
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Object(R(0))
	call_c   Dyam_Tabule()
	pl_ret

pl_code local fun27
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Object(R(0))
	call_c   Dyam_Tabule()
	pl_ret

pl_code local fun475
	call_c   Dyam_Update_Choice(fun114)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(2),&ref[1034])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(4),&ref[1035])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun476
	call_c   Dyam_Update_Choice(fun475)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(2),&ref[1034])
	fail_ret
	pl_call  Domain_2(V(8),V(6))
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(4),V(3))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun477
	call_c   Dyam_Update_Choice(fun476)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(6),I(0))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(4),V(3))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun485
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(3),&ref[1039])
	fail_ret
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(2))
	call_c   Dyam_Reg_Load(4,V(7))
	move     V(15), R(6)
	move     S(5), R(7)
	pl_call  pred_lctag_add_trees_4()
	call_c   Dyam_Unify(V(4),&ref[843])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun486
	call_c   Dyam_Update_Choice(fun485)
	call_c   Dyam_Set_Cut()
	pl_fail

pl_code local fun487
	call_c   Dyam_Update_Choice(fun486)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(3),&ref[1032])
	fail_ret
	call_c   DYAM_Subsumes_Chk_2(V(5),V(1))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun477)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(2),I(0))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(4),&ref[1033])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun437
	call_c   Dyam_Remove_Choice()
	pl_call  fun44(&seed[161],2,V(5))
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun374
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(7),V(5))
	fail_ret
fun373:
	call_c   Dyam_Unify(V(2),&ref[873])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret


pl_code local fun375
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Reg_Load(0,V(9))
	call_c   Dyam_Reg_Load(2,V(2))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_noop_first_2()

pl_code local fun377
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(7),V(5))
	fail_ret
fun376:
	call_c   Dyam_Unify(V(2),&ref[875])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret


pl_code local fun378
	call_c   Dyam_Remove_Choice()
	pl_call  Domain_2(V(5),V(1))
	call_c   Dyam_Choice(fun377)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[872])
	call_c   Dyam_Cut()
	pl_jump  fun376()

pl_code local fun379
	call_c   Dyam_Update_Choice(fun378)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[874])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun375)
	call_c   Dyam_Reg_Load(0,V(8))
	call_c   Dyam_Reg_Load(2,V(2))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_noop_first_2()

pl_code local fun380
	call_c   Dyam_Update_Choice(fun379)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[871])
	fail_ret
	call_c   Dyam_Cut()
	pl_call  Domain_2(V(5),V(3))
	call_c   Dyam_Choice(fun374)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[872])
	call_c   Dyam_Cut()
	pl_jump  fun373()

pl_code local fun381
	call_c   Dyam_Update_Choice(fun380)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[869])
	fail_ret
	call_c   Dyam_Cut()
	pl_call  Domain_2(V(4),V(3))
	call_c   Dyam_Unify(V(2),&ref[870])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun382
	call_c   Dyam_Update_Choice(fun381)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[868])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(2),&ref[868])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun357
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(1),&ref[837])
	fail_ret
	call_c   Dyam_Reg_Load(0,V(22))
	move     V(8), R(2)
	move     S(5), R(3)
	pl_call  pred_last_elt_2()
	call_c   Dyam_Reg_Load(0,V(8))
	call_c   Dyam_Reg_Deallocate(1)
	pl_jump  pred_left_potential_tig_1()

pl_code local fun358
	call_c   Dyam_Update_Choice(fun357)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[836])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun359
	call_c   Dyam_Update_Choice(fun358)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[722])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(1))
	move     V(8), R(2)
	move     S(5), R(3)
	pl_call  pred_last_elt_2()
	call_c   Dyam_Reg_Load(0,V(8))
	call_c   Dyam_Reg_Deallocate(1)
	pl_jump  pred_left_potential_tig_1()

pl_code local fun360
	call_c   Dyam_Update_Choice(fun359)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[721])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(2))
	call_c   Dyam_Reg_Deallocate(1)
	pl_jump  pred_left_potential_tig_1()

pl_code local fun350
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Reg_Load(0,V(4))
	call_c   Dyam_Reg_Load(2,V(2))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_spine_node_2()

pl_code local fun351
	call_c   Dyam_Remove_Choice()
	move     &ref[832], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(2))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_spine_node_2()

pl_code local fun353
	call_c   Dyam_Remove_Choice()
	pl_call  Domain_2(&ref[835],V(9))
fun352:
	call_c   Dyam_Choice(fun351)
	call_c   Dyam_Unify(V(2),&ref[832])
	fail_ret
	call_c   Dyam_Unify(V(23),&ref[833])
	fail_ret
	call_c   Dyam_Unify(V(29),&ref[834])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret


pl_code local fun354
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(1),&ref[476])
	fail_ret
	call_c   Dyam_Choice(fun353)
	pl_call  Domain_2(&ref[832],V(9))
	pl_jump  fun352()

pl_code local fun355
	call_c   Dyam_Update_Choice(fun354)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[831])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(3))
	call_c   Dyam_Reg_Load(2,V(2))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_spine_node_2()

pl_code local fun338
	call_c   Dyam_Remove_Choice()
	pl_call  fun44(&seed[150],2,V(5))
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun335
	call_c   Dyam_Remove_Choice()
	pl_call  fun44(&seed[148],2,V(3))
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun323
	call_c   Dyam_Remove_Choice()
	pl_call  fun44(&seed[143],2,V(5))
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun320
	call_c   Dyam_Remove_Choice()
	pl_call  fun44(&seed[141],2,V(4))
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun317
	call_c   Dyam_Remove_Choice()
	pl_call  fun44(&seed[138],2,V(3))
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun304
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(1),&ref[724])
	fail_ret
	call_c   Dyam_Reg_Load(0,V(6))
	call_c   Dyam_Reg_Deallocate(1)
	pl_jump  pred_right_potential_tig_1()

pl_code local fun305
	call_c   Dyam_Update_Choice(fun304)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[723])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun306
	call_c   Dyam_Update_Choice(fun305)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[722])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(6))
	call_c   Dyam_Reg_Deallocate(1)
	pl_jump  pred_right_potential_tig_1()

pl_code local fun307
	call_c   Dyam_Update_Choice(fun306)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[721])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(2))
	call_c   Dyam_Reg_Deallocate(1)
	pl_jump  pred_right_potential_tig_1()

pl_code local fun258
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Reg_Load(0,V(3))
	call_c   Dyam_Reg_Deallocate(1)
	pl_jump  pred_guard_eval_1()

pl_code local fun259
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(1),&ref[669])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun260
	call_c   Dyam_Update_Choice(fun259)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[668])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun226)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Choice(fun112)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(4),V(5))
	fail_ret
	call_c   Dyam_Cut()
	pl_fail

pl_code local fun261
	call_c   Dyam_Update_Choice(fun260)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[667])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun258)
	call_c   Dyam_Reg_Load(0,V(2))
	call_c   Dyam_Reg_Deallocate(1)
	pl_jump  pred_guard_eval_1()

pl_code local fun262
	call_c   Dyam_Update_Choice(fun261)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[666])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(2))
	pl_call  pred_guard_eval_1()
	call_c   Dyam_Reg_Load(0,V(3))
	call_c   Dyam_Reg_Deallocate(1)
	pl_jump  pred_guard_eval_1()

pl_code local fun263
	call_c   Dyam_Update_Choice(fun262)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[299])
	fail_ret
	call_c   Dyam_Cut()
	pl_fail

pl_code local fun248
	call_c   Dyam_Remove_Choice()
	pl_call  fun44(&seed[129],2,V(3))
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun236
	call_c   Dyam_Remove_Choice()
	pl_call  fun44(&seed[122],2,V(5))
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun229
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Reg_Load(0,V(6))
	call_c   Dyam_Reg_Load(2,V(2))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_lctag_first_scan_2()

pl_code local fun230
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(1),&ref[606])
	fail_ret
	call_c   Dyam_Unify(V(2),&ref[603])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun231
	call_c   Dyam_Update_Choice(fun230)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[605])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun229)
	call_c   Dyam_Reg_Load(0,V(5))
	call_c   Dyam_Reg_Load(2,V(2))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_lctag_first_scan_2()

pl_code local fun232
	call_c   Dyam_Update_Choice(fun231)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[604])
	fail_ret
	call_c   Dyam_Cut()
	move     &ref[605], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(2))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_lctag_first_scan_2()

pl_code local fun233
	call_c   Dyam_Update_Choice(fun232)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[121])
	fail_ret
	call_c   DYAM_evpred_variable(V(3))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(2),&ref[603])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun223
	call_c   Dyam_Remove_Choice()
	pl_call  fun44(&seed[120],2,V(4))
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun220
	call_c   Dyam_Remove_Choice()
	pl_call  fun44(&seed[118],2,V(4))
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun201
	call_c   Dyam_Remove_Choice()
	pl_call  fun44(&seed[113],2,V(9))
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun154
	call_c   Dyam_Remove_Choice()
	pl_call  fun44(&seed[103],2,V(6))
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun127
	call_c   Dyam_Remove_Choice()
fun126:
	call_c   DYAM_Absolute_File_Name(V(11),V(13))
	fail_ret
	move     &ref[441], R(0)
	move     0, R(1)
	move     &ref[442], R(2)
	move     S(5), R(3)
	pl_call  pred_format_2()
	pl_fail


pl_code local fun124
	call_c   Dyam_Remove_Choice()
fun123:
	call_c   DYAM_Nl_0()
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret


pl_code local fun128
	call_c   Dyam_Remove_Choice()
fun125:
	call_c   Dyam_Choice(fun124)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[443])
	call_c   Dyam_Cut()
	move     &ref[444], R(0)
	move     0, R(1)
	move     I(0), R(2)
	move     0, R(3)
	pl_call  pred_format_2()
	pl_jump  fun123()


pl_code local fun93
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(52),&ref[350])
	fail_ret
	pl_jump  fun92()

pl_code local fun95
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(24),N(1))
	fail_ret
	pl_jump  fun94()

pl_code local fun97
	call_c   Dyam_Remove_Choice()
	pl_jump  fun96()

pl_code local fun55
	call_c   Dyam_Backptr_Trace(R(1),R(2))
	call_c   Dyam_Object(R(0))
	call_c   Dyam_Tabule()
	call_c   Dyam_Now()
	pl_ret

pl_code local fun24
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Object(R(0))
	call_c   Dyam_Tabule()
	call_c   Dyam_Schedule()
	pl_ret

pl_code local fun49
	call_c   Dyam_Remove_Choice()
	pl_call  fun44(&seed[75],2,V(5))
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun28
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Object(R(0))
	call_c   Dyam_Tabule()
	pl_jump  Schedule_Prolog()

pl_code local fun29
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Choice(fun28)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Subsume(R(0))
	fail_ret
	call_c   Dyam_Cut()
	pl_ret

pl_code local fun44
	call_c   Dyam_Backptr_Trace(R(1),R(2))
	call_c   Dyam_Light_Object(R(0))
	pl_jump  Schedule_Prolog()

pl_code local fun45
	call_c   Dyam_Remove_Choice()
	pl_call  fun44(&seed[70],2,V(4))
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun16
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(1),&ref[121])
	fail_ret
	call_c   Dyam_Reg_Load(0,V(4))
	call_c   Dyam_Reg_Load(2,V(2))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_last_elt_2()


;;------------------ INIT POOL ------------

c_code local build_init_pool
	call_c   build_seed_63()
	call_c   build_seed_64()
	call_c   build_seed_32()
	call_c   build_seed_54()
	call_c   build_seed_4()
	call_c   build_seed_6()
	call_c   build_seed_1()
	call_c   build_seed_3()
	call_c   build_seed_7()
	call_c   build_seed_0()
	call_c   build_seed_2()
	call_c   build_seed_58()
	call_c   build_seed_57()
	call_c   build_seed_5()
	call_c   build_seed_46()
	call_c   build_seed_47()
	call_c   build_seed_28()
	call_c   build_seed_50()
	call_c   build_seed_48()
	call_c   build_seed_16()
	call_c   build_seed_61()
	call_c   build_seed_27()
	call_c   build_seed_44()
	call_c   build_seed_62()
	call_c   build_seed_31()
	call_c   build_seed_13()
	call_c   build_seed_14()
	call_c   build_seed_26()
	call_c   build_seed_53()
	call_c   build_seed_43()
	call_c   build_seed_51()
	call_c   build_seed_52()
	call_c   build_seed_56()
	call_c   build_seed_49()
	call_c   build_seed_10()
	call_c   build_seed_45()
	call_c   build_seed_18()
	call_c   build_seed_29()
	call_c   build_seed_30()
	call_c   build_seed_21()
	call_c   build_seed_22()
	call_c   build_seed_23()
	call_c   build_seed_20()
	call_c   build_seed_41()
	call_c   build_seed_42()
	call_c   build_seed_19()
	call_c   build_seed_9()
	call_c   build_seed_17()
	call_c   build_seed_8()
	call_c   build_seed_55()
	call_c   build_seed_59()
	call_c   build_seed_11()
	call_c   build_seed_35()
	call_c   build_seed_38()
	call_c   build_seed_12()
	call_c   build_seed_40()
	call_c   build_seed_33()
	call_c   build_seed_36()
	call_c   build_seed_39()
	call_c   build_seed_60()
	call_c   build_seed_37()
	call_c   build_seed_24()
	call_c   build_seed_34()
	call_c   build_seed_25()
	call_c   build_seed_15()
	call_c   build_seed_172()
	call_c   build_seed_171()
	call_c   build_seed_160()
	call_c   build_seed_161()
	call_c   build_seed_149()
	call_c   build_seed_150()
	call_c   build_seed_147()
	call_c   build_seed_148()
	call_c   build_seed_142()
	call_c   build_seed_143()
	call_c   build_seed_140()
	call_c   build_seed_141()
	call_c   build_seed_137()
	call_c   build_seed_138()
	call_c   build_seed_128()
	call_c   build_seed_129()
	call_c   build_seed_121()
	call_c   build_seed_122()
	call_c   build_seed_119()
	call_c   build_seed_120()
	call_c   build_seed_117()
	call_c   build_seed_118()
	call_c   build_seed_98()
	call_c   build_seed_113()
	call_c   build_seed_111()
	call_c   build_seed_110()
	call_c   build_seed_102()
	call_c   build_seed_103()
	call_c   build_seed_89()
	call_c   build_seed_88()
	call_c   build_seed_87()
	call_c   build_seed_86()
	call_c   build_seed_67()
	call_c   build_seed_66()
	call_c   build_seed_74()
	call_c   build_seed_75()
	call_c   build_seed_68()
	call_c   build_seed_70()
	call_c   build_ref_4()
	call_c   build_ref_3()
	call_c   build_ref_8()
	call_c   build_ref_7()
	call_c   build_ref_12()
	call_c   build_ref_11()
	call_c   build_ref_15()
	call_c   build_ref_14()
	call_c   build_ref_19()
	call_c   build_ref_18()
	call_c   build_ref_20()
	call_c   build_ref_22()
	call_c   build_ref_25()
	call_c   build_ref_24()
	call_c   build_ref_26()
	call_c   build_ref_27()
	call_c   build_ref_31()
	call_c   build_ref_30()
	call_c   build_ref_32()
	call_c   build_ref_33()
	call_c   build_ref_36()
	call_c   build_ref_35()
	call_c   build_ref_39()
	call_c   build_ref_38()
	call_c   build_ref_40()
	call_c   build_ref_41()
	call_c   build_ref_42()
	call_c   build_ref_43()
	call_c   build_ref_44()
	call_c   build_ref_45()
	call_c   build_ref_46()
	call_c   build_ref_47()
	call_c   build_ref_48()
	call_c   build_ref_49()
	call_c   build_ref_50()
	call_c   build_ref_51()
	call_c   build_ref_1031()
	call_c   build_ref_1033()
	call_c   build_ref_1035()
	call_c   build_ref_1034()
	call_c   build_ref_1032()
	call_c   build_ref_843()
	call_c   build_ref_1039()
	call_c   build_ref_868()
	call_c   build_ref_870()
	call_c   build_ref_869()
	call_c   build_ref_873()
	call_c   build_ref_871()
	call_c   build_ref_874()
	call_c   build_ref_872()
	call_c   build_ref_875()
	call_c   build_ref_836()
	call_c   build_ref_837()
	call_c   build_ref_830()
	call_c   build_ref_831()
	call_c   build_ref_834()
	call_c   build_ref_833()
	call_c   build_ref_832()
	call_c   build_ref_835()
	call_c   build_ref_476()
	call_c   build_ref_721()
	call_c   build_ref_722()
	call_c   build_ref_723()
	call_c   build_ref_724()
	call_c   build_ref_487()
	call_c   build_ref_299()
	call_c   build_ref_666()
	call_c   build_ref_667()
	call_c   build_ref_668()
	call_c   build_ref_669()
	call_c   build_ref_305()
	call_c   build_ref_604()
	call_c   build_ref_605()
	call_c   build_ref_603()
	call_c   build_ref_606()
	call_c   build_ref_599()
	call_c   build_ref_602()
	call_c   build_ref_581()
	call_c   build_ref_556()
	call_c   build_ref_555()
	call_c   build_ref_445()
	call_c   build_ref_528()
	call_c   build_ref_449()
	call_c   build_ref_255()
	call_c   build_ref_442()
	call_c   build_ref_441()
	call_c   build_ref_440()
	call_c   build_ref_444()
	call_c   build_ref_443()
	call_c   build_ref_439()
	call_c   build_ref_438()
	call_c   build_ref_437()
	call_c   build_ref_436()
	call_c   build_ref_435()
	call_c   build_ref_317()
	call_c   build_ref_319()
	call_c   build_ref_339()
	call_c   build_ref_111()
	call_c   build_ref_346()
	call_c   build_ref_350()
	call_c   build_ref_338()
	call_c   build_ref_334()
	call_c   build_ref_333()
	call_c   build_ref_332()
	call_c   build_ref_331()
	call_c   build_ref_330()
	call_c   build_ref_329()
	call_c   build_ref_328()
	call_c   build_ref_327()
	call_c   build_ref_326()
	call_c   build_ref_325()
	call_c   build_ref_324()
	call_c   build_ref_323()
	call_c   build_ref_322()
	call_c   build_ref_321()
	call_c   build_ref_320()
	call_c   build_ref_318()
	call_c   build_ref_351()
	call_c   build_ref_120()
	call_c   build_ref_121()
	c_ret

long local ref[1285]
long local seed[174]

long local _initialization

c_code global initialization_dyalog_tig
	call_c   Dyam_Check_And_Update_Initialization(_initialization)
	fail_c_ret
	call_c   initialization_dyalog_tag_5Fdyn()
	call_c   initialization_dyalog_tag_5Fcompile()
	call_c   initialization_dyalog_maker()
	call_c   initialization_dyalog_tag_5Fmaker()
	call_c   initialization_dyalog_tag_5Fnormalize()
	call_c   initialization_dyalog_reader()
	call_c   initialization_dyalog_format()
	call_c   build_init_pool()
	call_c   build_viewers()
	call_c   load()
	c_ret

