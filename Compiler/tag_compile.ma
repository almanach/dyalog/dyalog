;; Compiler: DyALog 1.14.0
;; File "tag_compile.pl"


;;------------------ LOADING ------------

c_code local load
	call_c   Dyam_Loading_Set()
	pl_call  fun20(&seed[46],0)
	pl_call  fun20(&seed[33],0)
	pl_call  fun20(&seed[14],0)
	pl_call  fun20(&seed[47],0)
	pl_call  fun20(&seed[49],0)
	pl_call  fun20(&seed[43],0)
	pl_call  fun20(&seed[25],0)
	pl_call  fun20(&seed[42],0)
	pl_call  fun20(&seed[16],0)
	pl_call  fun20(&seed[37],0)
	pl_call  fun20(&seed[40],0)
	pl_call  fun20(&seed[58],0)
	pl_call  fun20(&seed[55],0)
	pl_call  fun20(&seed[19],0)
	pl_call  fun20(&seed[29],0)
	pl_call  fun20(&seed[34],0)
	pl_call  fun20(&seed[39],0)
	pl_call  fun20(&seed[26],0)
	pl_call  fun20(&seed[35],0)
	pl_call  fun20(&seed[56],0)
	pl_call  fun20(&seed[54],0)
	pl_call  fun20(&seed[28],0)
	pl_call  fun20(&seed[30],0)
	pl_call  fun20(&seed[31],0)
	pl_call  fun20(&seed[59],0)
	pl_call  fun20(&seed[20],0)
	pl_call  fun20(&seed[57],0)
	pl_call  fun20(&seed[27],0)
	pl_call  fun20(&seed[50],0)
	pl_call  fun20(&seed[44],0)
	pl_call  fun20(&seed[32],0)
	pl_call  fun20(&seed[8],0)
	pl_call  fun20(&seed[17],0)
	pl_call  fun20(&seed[15],0)
	pl_call  fun20(&seed[18],0)
	pl_call  fun20(&seed[38],0)
	pl_call  fun20(&seed[45],0)
	pl_call  fun20(&seed[1],0)
	pl_call  fun20(&seed[60],0)
	pl_call  fun20(&seed[7],0)
	pl_call  fun20(&seed[52],0)
	pl_call  fun20(&seed[51],0)
	pl_call  fun20(&seed[21],0)
	pl_call  fun20(&seed[53],0)
	pl_call  fun20(&seed[61],0)
	pl_call  fun20(&seed[23],0)
	pl_call  fun20(&seed[41],0)
	pl_call  fun20(&seed[64],0)
	pl_call  fun20(&seed[63],0)
	pl_call  fun20(&seed[11],0)
	pl_call  fun20(&seed[13],0)
	pl_call  fun20(&seed[62],0)
	pl_call  fun20(&seed[12],0)
	pl_call  fun20(&seed[10],0)
	pl_call  fun20(&seed[9],0)
	pl_call  fun20(&seed[36],0)
	pl_call  fun20(&seed[0],0)
	pl_call  fun20(&seed[22],0)
	pl_call  fun20(&seed[24],0)
	pl_call  fun20(&seed[4],0)
	pl_call  fun20(&seed[48],0)
	pl_call  fun20(&seed[3],0)
	pl_call  fun20(&seed[5],0)
	pl_call  fun20(&seed[2],0)
	pl_call  fun20(&seed[6],0)
	pl_call  fun13(&seed[68],0)
	pl_call  fun13(&seed[66],0)
	pl_call  fun13(&seed[65],0)
	call_c   Dyam_Loading_Reset()
	c_ret

pl_code global pred_tag_autoloader_6
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(6)
	call_c   Dyam_Choice(fun477)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(2),&ref[1197])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(7))
	call_c   Dyam_Reg_Load(4,V(3))
	call_c   Dyam_Reg_Load(6,V(4))
	call_c   Dyam_Reg_Load(8,V(5))
	call_c   Dyam_Reg_Load(10,V(6))
	call_c   Dyam_Reg_Deallocate(6)
	pl_jump  pred_tag_autoloader_6()

pl_code global pred_guard_approx_5
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(5)
	call_c   Dyam_Choice(fun416)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_variable(V(1))
	fail_ret
	call_c   Dyam_Cut()
	move     &ref[776], R(0)
	move     0, R(1)
	move     &ref[148], R(2)
	move     S(5), R(3)
	pl_call  pred_format_2()
	call_c   Dyam_Unify(V(2),V(1))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code global pred_tree_foot_2
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(2)
	call_c   Dyam_Choice(fun387)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_variable(V(1))
	fail_ret
	call_c   Dyam_Cut()
	pl_fail

pl_code global pred_tree_info_2
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(2)
	call_c   Dyam_Choice(fun375)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_variable(V(1))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Deallocate()
	pl_ret

pl_code global pred_double_neg_handler_6
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(6)
	call_c   Dyam_Choice(fun330)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(2),&ref[898])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(8))
	call_c   Dyam_Reg_Load(4,V(3))
	move     V(9), R(6)
	move     S(5), R(7)
	call_c   Dyam_Reg_Load(8,V(5))
	call_c   Dyam_Reg_Load(10,V(6))
	pl_call  pred_double_neg_handler_6()
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(7))
	call_c   Dyam_Reg_Load(4,V(9))
	call_c   Dyam_Reg_Load(6,V(4))
	call_c   Dyam_Reg_Load(8,V(5))
	call_c   Dyam_Reg_Load(10,V(6))
	call_c   Dyam_Reg_Deallocate(6)
	pl_jump  pred_double_neg_handler_6()

pl_code global pred_il_tupple_2
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(2)
	call_c   Dyam_Reg_Load(0,V(1))
	move     I(0), R(2)
	move     0, R(3)
	move     V(3), R(4)
	move     S(5), R(5)
	pl_call  pred_il_all_tupples_3()
	move     V(4), R(0)
	move     S(5), R(1)
	pl_call  pred_null_tupple_1()
	call_c   Dyam_Reg_Load_Ptr(2,V(5))
	fail_ret
	call_c   Dyam_Reg_Load(4,V(4))
	call_c   DyALog_Mutable_Write(R(2),&R(4),0)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(5))
	fail_ret
	call_c   Dyam_Reg_Load_Ptr(2,V(6))
	fail_ret
	call_c   Dyam_Reg_Load(4,V(4))
	call_c   DyALog_Mutable_Write(R(2),&R(4),0)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(6))
	fail_ret
	call_c   Dyam_Choice(fun300)
	pl_call  Domain_2(V(7),V(3))
	call_c   Dyam_Reg_Load_Ptr(2,V(5))
	fail_ret
	move     V(8), R(4)
	move     S(5), R(5)
	call_c   DyALog_Mutable_Read(R(2),&R(4))
	fail_ret
	call_c   Dyam_Reg_Load(0,V(7))
	call_c   Dyam_Reg_Load(2,V(8))
	move     V(9), R(4)
	move     S(5), R(5)
	pl_call  pred_inter_3()
	call_c   Dyam_Reg_Load_Ptr(2,V(6))
	fail_ret
	move     V(10), R(4)
	move     S(5), R(5)
	call_c   DyALog_Mutable_Read(R(2),&R(4))
	fail_ret
	call_c   Dyam_Reg_Load(0,V(9))
	call_c   Dyam_Reg_Load(2,V(10))
	move     V(11), R(4)
	move     S(5), R(5)
	pl_call  pred_union_3()
	call_c   Dyam_Reg_Load_Ptr(2,V(6))
	fail_ret
	call_c   Dyam_Reg_Load(4,V(11))
	call_c   DyALog_Mutable_Write(R(2),&R(4),0)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(6))
	fail_ret
	call_c   Dyam_Reg_Load(0,V(8))
	call_c   Dyam_Reg_Load(2,V(7))
	move     V(12), R(4)
	move     S(5), R(5)
	pl_call  pred_union_3()
	call_c   Dyam_Reg_Load_Ptr(2,V(5))
	fail_ret
	call_c   Dyam_Reg_Load(4,V(12))
	call_c   DyALog_Mutable_Write(R(2),&R(4),0)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(5))
	fail_ret
	pl_fail

pl_code global pred_guard_depth_2
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(2)
	call_c   Dyam_Choice(fun296)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[341])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(3))
	move     V(5), R(2)
	move     S(5), R(3)
	pl_call  pred_guard_depth_2()
	call_c   Dyam_Reg_Load(0,V(4))
	move     V(6), R(2)
	move     S(5), R(3)
	pl_call  pred_guard_depth_2()
	call_c   DYAM_evpred_is(V(2),&ref[783])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code global pred_tag_extract_anchor_guards_2
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(2)
	call_c   Dyam_Choice(fun291)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Choice(fun287)
	call_c   Dyam_Unify(V(1),&ref[778])
	fail_ret
fun284:
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun283)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Reg_Load(0,V(3))
	call_c   Dyam_Reg_Load(2,V(2))
	pl_call  pred_tag_extract_anchor_guards_2()
	call_c   Dyam_Cut()
	call_c   Dyam_Deallocate()
	pl_ret


pl_code global pred_autoload_guard_approx_2
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(2)
	call_c   Dyam_Choice(fun281)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_variable(V(1))
	fail_ret
	call_c   Dyam_Cut()
	move     &ref[776], R(0)
	move     0, R(1)
	move     &ref[148], R(2)
	move     S(5), R(3)
	pl_call  pred_format_2()
	call_c   Dyam_Unify(V(2),V(1))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code global pred_tag_numbervars_3
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(3)
	call_c   Dyam_Choice(fun271)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_variable(V(2))
	fail_ret
	call_c   Dyam_Cut()
	call_c   DyALog_Gensym()
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Number(0,V(2))
	fail_ret
	call_c   DYAM_Numbervars_3(V(1),N(1),V(3))
	fail_ret
	call_c   Dyam_Reg_Load_Ptr(2,V(4))
	fail_ret
	call_c   Dyam_Reg_Load(4,V(3))
	call_c   DyALog_Mutable_Write(R(2),&R(4),0)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(4))
	fail_ret
	call_c   DYAM_evpred_assert_1(&ref[760])
	call_c   Dyam_Deallocate()
	pl_ret

pl_code global pred_tag_autoloader_simplify_2
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(2)
	call_c   Dyam_Choice(fun253)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[695])
	fail_ret
	call_c   Dyam_Cut()
	move     &ref[696], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(2))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_tag_autoloader_simplify_2()

pl_code global pred_tag_il_loop_to_lpda_11
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(8)
	call_c   Dyam_Reg_Unify(16,&ref[629])
	fail_ret
	call_c   Dyam_Reg_Bind(18,V(10))
	move     &ref[620], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(14), R(4)
	move     S(5), R(5)
	pl_call  pred_tag_numbervars_3()
	move     &ref[627], R(0)
	move     S(5), R(1)
	move     &ref[628], R(2)
	move     S(5), R(3)
	move     I(0), R(4)
	move     0, R(5)
	call_c   Dyam_Reg_Deallocate(3)
fun219:
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Reg_Unify(2,&ref[410])
	fail_ret
	call_c   Dyam_Reg_Bind(4,V(10))
	call_c   Dyam_Choice(fun218)
	pl_call  fun46(&seed[108],1)
	pl_fail


pl_code global pred_tag_il_start_to_lpda_16
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(12)
	call_c   Dyam_Reg_Unify(24,&ref[609])
	fail_ret
	call_c   Dyam_Reg_Bind(26,V(16))
	call_c   Dyam_Reg_Bind(30,V(18))
	call_c   Dyam_Reg_Load(0,V(13))
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(19), R(4)
	move     S(5), R(5)
	pl_call  pred_tag_numbervars_3()
	move     &ref[606], R(0)
	move     S(5), R(1)
	move     &ref[607], R(2)
	move     S(5), R(3)
	move     I(0), R(4)
	move     0, R(5)
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  fun219()

pl_code global pred_tag_il_register_to_lpda_17
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(13)
	call_c   Dyam_Reg_Unify(26,&ref[603])
	fail_ret
	call_c   Dyam_Reg_Bind(28,V(17))
	call_c   Dyam_Reg_Bind(32,V(19))
	call_c   Dyam_Reg_Load(0,V(14))
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(20), R(4)
	move     S(5), R(5)
	pl_call  pred_my_numbervars_3()
	move     &ref[600], R(0)
	move     S(5), R(1)
	move     &ref[601], R(2)
	move     S(5), R(3)
	move     I(0), R(4)
	move     0, R(5)
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  fun219()

pl_code global pred_guard_post_handler_5
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(5)
	call_c   Dyam_Choice(fun205)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(2),&ref[545])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(7))
	call_c   Dyam_Reg_Load(4,V(3))
	move     V(8), R(6)
	move     S(5), R(7)
	call_c   Dyam_Reg_Load(8,V(5))
	pl_call  pred_guard_post_handler_5()
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(6))
	call_c   Dyam_Reg_Load(4,V(8))
	call_c   Dyam_Reg_Load(6,V(4))
	call_c   Dyam_Reg_Load(8,V(5))
	call_c   Dyam_Reg_Deallocate(5)
	pl_jump  pred_guard_post_handler_5()

pl_code global pred_tag_compile_lemma_anchor_2
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Unify(0,&ref[150])
	fail_ret
	call_c   Dyam_Reg_Bind(2,V(4))
	call_c   Dyam_Choice(fun164)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_variable(V(1))
	fail_ret
	call_c   Dyam_Cut()
	move     &ref[434], R(0)
	move     0, R(1)
	move     &ref[435], R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_error_2()

pl_code global pred_tag_subst_compile_9
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Reg_Unify(2,&ref[433])
	fail_ret
	call_c   Dyam_Multi_Reg_Bind(4,13,4)
	pl_call  fun28(&seed[111],1)
	call_c   DYAM_evpred_functor(V(8),V(22),V(23))
	fail_ret
	call_c   Dyam_Choice(fun155)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[426])
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(24),&ref[427])
	fail_ret
fun154:
	call_c   Dyam_Choice(fun153)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[428])
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(25))
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(26), R(4)
	move     S(5), R(5)
	pl_call  pred_tag_numbervars_3()
	pl_call  fun28(&seed[112],1)
	call_c   Dyam_Deallocate()
	pl_ret


pl_code global pred_guard_length_2
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(2)
	call_c   Dyam_Choice(fun123)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[341])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(4))
	move     V(5), R(2)
	move     S(5), R(3)
	pl_call  pred_guard_length_2()
	call_c   DYAM_evpred_is(V(2),&ref[342])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code global pred_tag_autoloader_wrapper_4
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(4)
	call_c   Dyam_Choice(fun105)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(2),&ref[281])
	fail_ret
	call_c   Dyam_Cut()
fun104:
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(2))
	call_c   Dyam_Reg_Load(4,V(3))
	move     V(19), R(6)
	move     S(5), R(7)
	move     &ref[282], R(8)
	move     S(5), R(9)
	move     &ref[283], R(10)
	move     S(5), R(11)
	pl_call  pred_tag_autoloader_6()
	call_c   Dyam_Choice(fun103)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Choice(fun101)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(10),&ref[161])
	fail_ret
	call_c   Dyam_Cut()
	pl_fail


pl_code global pred_tag_il_body_to_lpda_13
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(13)
	call_c   Dyam_Choice(fun90)
	call_c   Dyam_Set_Cut()
	pl_call  fun28(&seed[85],1)
	call_c   Dyam_Reg_Load(0,V(14))
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(15), R(4)
	move     S(5), R(5)
	pl_call  pred_my_numbervars_3()
	call_c   Dyam_Cut()
fun89:
	call_c   Dyam_Choice(fun88)
	call_c   Dyam_Set_Cut()
	pl_call  fun28(&seed[86],1)
	call_c   Dyam_Cut()
	call_c   Dyam_Deallocate()
	pl_ret


pl_code global pred_feature_args_unif_6
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(6)
	call_c   Dyam_Choice(fun70)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(2),V(3))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(4),V(5))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code global pred_il_all_tupples_3
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(3)
	call_c   Dyam_Choice(fun68)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[228])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(4))
	call_c   Dyam_Reg_Load(2,V(2))
	move     V(6), R(4)
	move     S(5), R(5)
	pl_call  pred_il_all_tupples_3()
	call_c   Dyam_Reg_Load(0,V(5))
	call_c   Dyam_Reg_Load(2,V(6))
	call_c   Dyam_Reg_Load(4,V(3))
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  pred_il_all_tupples_3()

pl_code global pred_allowed_wrap_adj_2
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Reg_Unify(2,&ref[167])
	fail_ret
	call_c   Dyam_Choice(fun42)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(7),&ref[161])
	fail_ret
	pl_call  Object_1(&ref[162])
	call_c   Dyam_Cut()
	pl_fail

pl_code global pred_tag_compile_info_anchor_2
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Unify(0,&ref[150])
	fail_ret
	call_c   Dyam_Reg_Unify(2,&ref[151])
	fail_ret
	call_c   Dyam_Choice(fun35)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[149])
	call_c   Dyam_Cut()
	call_c   Dyam_Deallocate()
	pl_ret

pl_code global pred_tag_compile_lemma_check_2
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(2)
	call_c   Dyam_Choice(fun32)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[146])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(2),&ref[147])
	fail_ret
	call_c   Dyam_Reg_Load(0,V(4))
	call_c   Dyam_Reg_Load(2,V(5))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_tag_compile_lemma_check_2()

pl_code global pred_local_var_in_tupple_2
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Reg_Unify(2,&ref[82])
	fail_ret
	pl_call  Object_1(&ref[81])
	call_c   Dyam_Reg_Load_Number(2,V(3))
	fail_ret
	call_c   Dyam_Reg_Load_Ptr(4,V(2))
	fail_ret
	call_c   oset_member(R(2),R(4))
	fail_ret
	call_c   Dyam_Reg_Deallocate(3)
	pl_ret

pl_code global pred_numbervars_delete_1
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   DYAM_evpred_retract(&ref[43])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret


;;------------------ VIEWERS ------------

c_code local build_viewers
	call_c   Dyam_Load_Viewer(&ref[42],&ref[41])
	call_c   Dyam_Load_Viewer(&ref[40],&ref[39])
	call_c   Dyam_Load_Viewer(&ref[38],&ref[37])
	call_c   Dyam_Load_Viewer(&ref[35],&ref[36])
	call_c   Dyam_Load_Viewer(&ref[32],&ref[33])
	call_c   Dyam_Load_Viewer(&ref[29],&ref[30])
	call_c   Dyam_Load_Viewer(&ref[26],&ref[25])
	call_c   Dyam_Load_Viewer(&ref[23],&ref[24])
	call_c   Dyam_Load_Viewer(&ref[21],&ref[20])
	call_c   Dyam_Load_Viewer(&ref[18],&ref[19])
	call_c   Dyam_Load_Viewer(&ref[15],&ref[14])
	call_c   Dyam_Load_Viewer(&ref[12],&ref[13])
	call_c   Dyam_Load_Viewer(&ref[8],&ref[9])
	call_c   Dyam_Load_Viewer(&ref[5],&ref[4])
	call_c   Dyam_Load_Viewer(&ref[3],&ref[1])
	c_ret

c_code local build_seed_65
	ret_reg &seed[65]
	call_c   build_ref_44()
	call_c   build_ref_45()
	call_c   Dyam_Seed_Start(&ref[44],&ref[45],&ref[45],fun1,0)
	call_c   Dyam_Seed_End()
	move_ret seed[65]
	c_ret

pl_code local fun1
	pl_ret

;; TERM 45: local_var(_S43297779, _B)
c_code local build_ref_45
	ret_reg &ref[45]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Unary(I(6),V(1))
	move_ret R(0)
	call_c   build_ref_1226()
	call_c   Dyam_Create_Binary(&ref[1226],R(0),V(1))
	move_ret ref[45]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1226: local_var
c_code local build_ref_1226
	ret_reg &ref[1226]
	call_c   Dyam_Create_Atom("local_var")
	move_ret ref[1226]
	c_ret

;; TERM 44: '*DATABASE*'
c_code local build_ref_44
	ret_reg &ref[44]
	call_c   Dyam_Create_Atom("*DATABASE*")
	move_ret ref[44]
	c_ret

c_code local build_seed_66
	ret_reg &seed[66]
	call_c   build_ref_44()
	call_c   build_ref_46()
	call_c   Dyam_Seed_Start(&ref[44],&ref[46],&ref[46],fun1,0)
	call_c   Dyam_Seed_End()
	move_ret seed[66]
	c_ret

;; TERM 46: local_strict_var(_S43297779, _B)
c_code local build_ref_46
	ret_reg &ref[46]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Unary(I(6),V(1))
	move_ret R(0)
	call_c   build_ref_1227()
	call_c   Dyam_Create_Binary(&ref[1227],R(0),V(1))
	move_ret ref[46]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1227: local_strict_var
c_code local build_ref_1227
	ret_reg &ref[1227]
	call_c   Dyam_Create_Atom("local_strict_var")
	move_ret ref[1227]
	c_ret

c_code local build_seed_68
	ret_reg &seed[68]
	call_c   build_ref_44()
	call_c   build_ref_61()
	call_c   Dyam_Seed_Start(&ref[44],&ref[61],&ref[61],fun1,0)
	call_c   Dyam_Seed_End()
	move_ret seed[68]
	c_ret

;; TERM 61: local_var('$VAR'(_S43297779, _C), _B)
c_code local build_ref_61
	ret_reg &ref[61]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Unary(I(6),V(1))
	move_ret R(0)
	call_c   Dyam_Create_Binary(I(6),R(0),V(2))
	move_ret R(0)
	call_c   build_ref_1226()
	call_c   Dyam_Create_Binary(&ref[1226],R(0),V(1))
	move_ret ref[61]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_6
	ret_reg &seed[6]
	call_c   build_ref_47()
	call_c   build_ref_51()
	call_c   Dyam_Seed_Start(&ref[47],&ref[51],I(0),fun1,1)
	call_c   build_ref_50()
	call_c   Dyam_Seed_Add_Comp(&ref[50],fun2,0)
	call_c   Dyam_Seed_End()
	move_ret seed[6]
	c_ret

;; TERM 50: '*GUARD*'(feature_args_unif_aux([], []))
c_code local build_ref_50
	ret_reg &ref[50]
	call_c   build_ref_48()
	call_c   build_ref_49()
	call_c   Dyam_Create_Unary(&ref[48],&ref[49])
	move_ret ref[50]
	c_ret

;; TERM 49: feature_args_unif_aux([], [])
c_code local build_ref_49
	ret_reg &ref[49]
	call_c   build_ref_1228()
	call_c   Dyam_Create_Binary(&ref[1228],I(0),I(0))
	move_ret ref[49]
	c_ret

;; TERM 1228: feature_args_unif_aux
c_code local build_ref_1228
	ret_reg &ref[1228]
	call_c   Dyam_Create_Atom("feature_args_unif_aux")
	move_ret ref[1228]
	c_ret

;; TERM 48: '*GUARD*'
c_code local build_ref_48
	ret_reg &ref[48]
	call_c   Dyam_Create_Atom("*GUARD*")
	move_ret ref[48]
	c_ret

pl_code local fun2
	call_c   build_ref_50()
	call_c   Dyam_Unify_Item(&ref[50])
	fail_ret
	pl_ret

;; TERM 51: '*GUARD*'(feature_args_unif_aux([], [])) :> []
c_code local build_ref_51
	ret_reg &ref[51]
	call_c   build_ref_50()
	call_c   Dyam_Create_Binary(I(9),&ref[50],I(0))
	move_ret ref[51]
	c_ret

;; TERM 47: '*GUARD_CLAUSE*'
c_code local build_ref_47
	ret_reg &ref[47]
	call_c   Dyam_Create_Atom("*GUARD_CLAUSE*")
	move_ret ref[47]
	c_ret

c_code local build_seed_2
	ret_reg &seed[2]
	call_c   build_ref_47()
	call_c   build_ref_64()
	call_c   Dyam_Seed_Start(&ref[47],&ref[64],I(0),fun1,1)
	call_c   build_ref_63()
	call_c   Dyam_Seed_Add_Comp(&ref[63],fun4,0)
	call_c   Dyam_Seed_End()
	move_ret seed[2]
	c_ret

;; TERM 63: '*GUARD*'(tag_compile_lemma_equations([], _B, _C))
c_code local build_ref_63
	ret_reg &ref[63]
	call_c   build_ref_48()
	call_c   build_ref_62()
	call_c   Dyam_Create_Unary(&ref[48],&ref[62])
	move_ret ref[63]
	c_ret

;; TERM 62: tag_compile_lemma_equations([], _B, _C)
c_code local build_ref_62
	ret_reg &ref[62]
	call_c   build_ref_1229()
	call_c   Dyam_Term_Start(&ref[1229],3)
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_End()
	move_ret ref[62]
	c_ret

;; TERM 1229: tag_compile_lemma_equations
c_code local build_ref_1229
	ret_reg &ref[1229]
	call_c   Dyam_Create_Atom("tag_compile_lemma_equations")
	move_ret ref[1229]
	c_ret

pl_code local fun4
	call_c   build_ref_63()
	call_c   Dyam_Unify_Item(&ref[63])
	fail_ret
	pl_ret

;; TERM 64: '*GUARD*'(tag_compile_lemma_equations([], _B, _C)) :> []
c_code local build_ref_64
	ret_reg &ref[64]
	call_c   build_ref_63()
	call_c   Dyam_Create_Binary(I(9),&ref[63],I(0))
	move_ret ref[64]
	c_ret

c_code local build_seed_5
	ret_reg &seed[5]
	call_c   build_ref_47()
	call_c   build_ref_67()
	call_c   Dyam_Seed_Start(&ref[47],&ref[67],I(0),fun1,1)
	call_c   build_ref_66()
	call_c   Dyam_Seed_Add_Comp(&ref[66],fun5,0)
	call_c   Dyam_Seed_End()
	move_ret seed[5]
	c_ret

;; TERM 66: '*GUARD*'(optimize_topbot_args([], [], _B, _B))
c_code local build_ref_66
	ret_reg &ref[66]
	call_c   build_ref_48()
	call_c   build_ref_65()
	call_c   Dyam_Create_Unary(&ref[48],&ref[65])
	move_ret ref[66]
	c_ret

;; TERM 65: optimize_topbot_args([], [], _B, _B)
c_code local build_ref_65
	ret_reg &ref[65]
	call_c   build_ref_1230()
	call_c   Dyam_Term_Start(&ref[1230],4)
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_End()
	move_ret ref[65]
	c_ret

;; TERM 1230: optimize_topbot_args
c_code local build_ref_1230
	ret_reg &ref[1230]
	call_c   Dyam_Create_Atom("optimize_topbot_args")
	move_ret ref[1230]
	c_ret

pl_code local fun5
	call_c   build_ref_66()
	call_c   Dyam_Unify_Item(&ref[66])
	fail_ret
	pl_ret

;; TERM 67: '*GUARD*'(optimize_topbot_args([], [], _B, _B)) :> []
c_code local build_ref_67
	ret_reg &ref[67]
	call_c   build_ref_66()
	call_c   Dyam_Create_Binary(I(9),&ref[66],I(0))
	move_ret ref[67]
	c_ret

c_code local build_seed_3
	ret_reg &seed[3]
	call_c   build_ref_47()
	call_c   build_ref_77()
	call_c   Dyam_Seed_Start(&ref[47],&ref[77],I(0),fun1,1)
	call_c   build_ref_76()
	call_c   Dyam_Seed_Add_Comp(&ref[76],fun6,0)
	call_c   Dyam_Seed_End()
	move_ret seed[3]
	c_ret

;; TERM 76: '*GUARD*'(tag_compile_lemma_coanchors([], _B, _C, true))
c_code local build_ref_76
	ret_reg &ref[76]
	call_c   build_ref_48()
	call_c   build_ref_75()
	call_c   Dyam_Create_Unary(&ref[48],&ref[75])
	move_ret ref[76]
	c_ret

;; TERM 75: tag_compile_lemma_coanchors([], _B, _C, true)
c_code local build_ref_75
	ret_reg &ref[75]
	call_c   build_ref_1231()
	call_c   build_ref_261()
	call_c   Dyam_Term_Start(&ref[1231],4)
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(&ref[261])
	call_c   Dyam_Term_End()
	move_ret ref[75]
	c_ret

;; TERM 261: true
c_code local build_ref_261
	ret_reg &ref[261]
	call_c   Dyam_Create_Atom("true")
	move_ret ref[261]
	c_ret

;; TERM 1231: tag_compile_lemma_coanchors
c_code local build_ref_1231
	ret_reg &ref[1231]
	call_c   Dyam_Create_Atom("tag_compile_lemma_coanchors")
	move_ret ref[1231]
	c_ret

pl_code local fun6
	call_c   build_ref_76()
	call_c   Dyam_Unify_Item(&ref[76])
	fail_ret
	pl_ret

;; TERM 77: '*GUARD*'(tag_compile_lemma_coanchors([], _B, _C, true)) :> []
c_code local build_ref_77
	ret_reg &ref[77]
	call_c   build_ref_76()
	call_c   Dyam_Create_Binary(I(9),&ref[76],I(0))
	move_ret ref[77]
	c_ret

c_code local build_seed_48
	ret_reg &seed[48]
	call_c   build_ref_52()
	call_c   build_ref_69()
	call_c   Dyam_Seed_Start(&ref[52],&ref[69],I(0),fun15,1)
	call_c   build_ref_70()
	call_c   Dyam_Seed_Add_Comp(&ref[70],fun14,0)
	call_c   Dyam_Seed_End()
	move_ret seed[48]
	c_ret

;; TERM 70: '*CITEM*'(format_require, _A)
c_code local build_ref_70
	ret_reg &ref[70]
	call_c   build_ref_56()
	call_c   build_ref_37()
	call_c   Dyam_Create_Binary(&ref[56],&ref[37],V(0))
	move_ret ref[70]
	c_ret

;; TERM 37: format_require
c_code local build_ref_37
	ret_reg &ref[37]
	call_c   Dyam_Create_Atom("format_require")
	move_ret ref[37]
	c_ret

;; TERM 56: '*CITEM*'
c_code local build_ref_56
	ret_reg &ref[56]
	call_c   Dyam_Create_Atom("*CITEM*")
	move_ret ref[56]
	c_ret

;; TERM 71: 'format.pl'
c_code local build_ref_71
	ret_reg &ref[71]
	call_c   Dyam_Create_Atom("format.pl")
	move_ret ref[71]
	c_ret

;; TERM 72: require
c_code local build_ref_72
	ret_reg &ref[72]
	call_c   Dyam_Create_Atom("require")
	move_ret ref[72]
	c_ret

c_code local build_seed_69
	ret_reg &seed[69]
	call_c   build_ref_0()
	call_c   build_ref_73()
	call_c   Dyam_Seed_Start(&ref[0],&ref[73],&ref[73],fun3,1)
	call_c   build_ref_74()
	call_c   Dyam_Seed_Add_Comp(&ref[74],&ref[73],0)
	call_c   Dyam_Seed_End()
	move_ret seed[69]
	c_ret

pl_code local fun3
	pl_jump  Complete(0,0)

;; TERM 74: '*RITEM*'(_A, voidret) :> '$$HOLE$$'
c_code local build_ref_74
	ret_reg &ref[74]
	call_c   build_ref_73()
	call_c   Dyam_Create_Binary(I(9),&ref[73],I(7))
	move_ret ref[74]
	c_ret

;; TERM 73: '*RITEM*'(_A, voidret)
c_code local build_ref_73
	ret_reg &ref[73]
	call_c   build_ref_0()
	call_c   build_ref_2()
	call_c   Dyam_Create_Binary(&ref[0],V(0),&ref[2])
	move_ret ref[73]
	c_ret

;; TERM 2: voidret
c_code local build_ref_2
	ret_reg &ref[2]
	call_c   Dyam_Create_Atom("voidret")
	move_ret ref[2]
	c_ret

;; TERM 0: '*RITEM*'
c_code local build_ref_0
	ret_reg &ref[0]
	call_c   Dyam_Create_Atom("*RITEM*")
	move_ret ref[0]
	c_ret

pl_code local fun13
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Choice(fun12)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Subsume(R(0))
	fail_ret
	call_c   Dyam_Cut()
	pl_ret

long local pool_fun14[5]=[4,build_ref_70,build_ref_71,build_ref_72,build_seed_69]

pl_code local fun14
	call_c   Dyam_Pool(pool_fun14)
	call_c   Dyam_Unify_Item(&ref[70])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[71], R(0)
	move     0, R(1)
	move     &ref[72], R(2)
	move     0, R(3)
	pl_call  pred_read_files_2()
	call_c   Dyam_Deallocate()
	pl_jump  fun13(&seed[69],2)

pl_code local fun15
	pl_jump  Apply(0,0)

;; TERM 69: '*FIRST*'(format_require) :> []
c_code local build_ref_69
	ret_reg &ref[69]
	call_c   build_ref_68()
	call_c   Dyam_Create_Binary(I(9),&ref[68],I(0))
	move_ret ref[69]
	c_ret

;; TERM 68: '*FIRST*'(format_require)
c_code local build_ref_68
	ret_reg &ref[68]
	call_c   build_ref_52()
	call_c   build_ref_37()
	call_c   Dyam_Create_Unary(&ref[52],&ref[37])
	move_ret ref[68]
	c_ret

;; TERM 52: '*FIRST*'
c_code local build_ref_52
	ret_reg &ref[52]
	call_c   Dyam_Create_Atom("*FIRST*")
	move_ret ref[52]
	c_ret

c_code local build_seed_4
	ret_reg &seed[4]
	call_c   build_ref_52()
	call_c   build_ref_55()
	call_c   Dyam_Seed_Start(&ref[52],&ref[55],I(0),fun15,1)
	call_c   build_ref_57()
	call_c   Dyam_Seed_Add_Comp(&ref[57],fun34,0)
	call_c   Dyam_Seed_End()
	move_ret seed[4]
	c_ret

;; TERM 57: '*CITEM*'('call_decompose_args/3'(tag_skipper), _A)
c_code local build_ref_57
	ret_reg &ref[57]
	call_c   build_ref_56()
	call_c   build_ref_53()
	call_c   Dyam_Create_Binary(&ref[56],&ref[53],V(0))
	move_ret ref[57]
	c_ret

;; TERM 53: 'call_decompose_args/3'(tag_skipper)
c_code local build_ref_53
	ret_reg &ref[53]
	call_c   build_ref_1232()
	call_c   build_ref_682()
	call_c   Dyam_Create_Unary(&ref[1232],&ref[682])
	move_ret ref[53]
	c_ret

;; TERM 682: tag_skipper
c_code local build_ref_682
	ret_reg &ref[682]
	call_c   Dyam_Create_Atom("tag_skipper")
	move_ret ref[682]
	c_ret

;; TERM 1232: 'call_decompose_args/3'
c_code local build_ref_1232
	ret_reg &ref[1232]
	call_c   Dyam_Create_Atom("call_decompose_args/3")
	move_ret ref[1232]
	c_ret

c_code local build_seed_67
	ret_reg &seed[67]
	call_c   build_ref_0()
	call_c   build_ref_59()
	call_c   Dyam_Seed_Start(&ref[0],&ref[59],&ref[59],fun3,1)
	call_c   build_ref_60()
	call_c   Dyam_Seed_Add_Comp(&ref[60],&ref[59],0)
	call_c   Dyam_Seed_End()
	move_ret seed[67]
	c_ret

;; TERM 60: '*RITEM*'(_A, return(_B, _B)) :> '$$HOLE$$'
c_code local build_ref_60
	ret_reg &ref[60]
	call_c   build_ref_59()
	call_c   Dyam_Create_Binary(I(9),&ref[59],I(7))
	move_ret ref[60]
	c_ret

;; TERM 59: '*RITEM*'(_A, return(_B, _B))
c_code local build_ref_59
	ret_reg &ref[59]
	call_c   build_ref_0()
	call_c   build_ref_58()
	call_c   Dyam_Create_Binary(&ref[0],V(0),&ref[58])
	move_ret ref[59]
	c_ret

;; TERM 58: return(_B, _B)
c_code local build_ref_58
	ret_reg &ref[58]
	call_c   build_ref_1233()
	call_c   Dyam_Create_Binary(&ref[1233],V(1),V(1))
	move_ret ref[58]
	c_ret

;; TERM 1233: return
c_code local build_ref_1233
	ret_reg &ref[1233]
	call_c   Dyam_Create_Atom("return")
	move_ret ref[1233]
	c_ret

long local pool_fun34[3]=[2,build_ref_57,build_seed_67]

pl_code local fun34
	call_c   Dyam_Pool(pool_fun34)
	call_c   Dyam_Unify_Item(&ref[57])
	fail_ret
	pl_jump  fun13(&seed[67],2)

;; TERM 55: '*FIRST*'('call_decompose_args/3'(tag_skipper)) :> []
c_code local build_ref_55
	ret_reg &ref[55]
	call_c   build_ref_54()
	call_c   Dyam_Create_Binary(I(9),&ref[54],I(0))
	move_ret ref[55]
	c_ret

;; TERM 54: '*FIRST*'('call_decompose_args/3'(tag_skipper))
c_code local build_ref_54
	ret_reg &ref[54]
	call_c   build_ref_52()
	call_c   build_ref_53()
	call_c   Dyam_Create_Unary(&ref[52],&ref[53])
	move_ret ref[54]
	c_ret

c_code local build_seed_24
	ret_reg &seed[24]
	call_c   build_ref_47()
	call_c   build_ref_80()
	call_c   Dyam_Seed_Start(&ref[47],&ref[80],I(0),fun1,1)
	call_c   build_ref_79()
	call_c   Dyam_Seed_Add_Comp(&ref[79],fun7,0)
	call_c   Dyam_Seed_End()
	move_ret seed[24]
	c_ret

;; TERM 79: '*GUARD*'(il_combine_tupples([], _B))
c_code local build_ref_79
	ret_reg &ref[79]
	call_c   build_ref_48()
	call_c   build_ref_78()
	call_c   Dyam_Create_Unary(&ref[48],&ref[78])
	move_ret ref[79]
	c_ret

;; TERM 78: il_combine_tupples([], _B)
c_code local build_ref_78
	ret_reg &ref[78]
	call_c   build_ref_1234()
	call_c   Dyam_Create_Binary(&ref[1234],I(0),V(1))
	move_ret ref[78]
	c_ret

;; TERM 1234: il_combine_tupples
c_code local build_ref_1234
	ret_reg &ref[1234]
	call_c   Dyam_Create_Atom("il_combine_tupples")
	move_ret ref[1234]
	c_ret

pl_code local fun7
	call_c   build_ref_79()
	call_c   Dyam_Unify_Item(&ref[79])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Deallocate(1)
	pl_jump  pred_null_tupple_1()

;; TERM 80: '*GUARD*'(il_combine_tupples([], _B)) :> []
c_code local build_ref_80
	ret_reg &ref[80]
	call_c   build_ref_79()
	call_c   Dyam_Create_Binary(I(9),&ref[79],I(0))
	move_ret ref[80]
	c_ret

c_code local build_seed_22
	ret_reg &seed[22]
	call_c   build_ref_47()
	call_c   build_ref_85()
	call_c   Dyam_Seed_Start(&ref[47],&ref[85],I(0),fun1,1)
	call_c   build_ref_84()
	call_c   Dyam_Seed_Add_Comp(&ref[84],fun9,0)
	call_c   Dyam_Seed_End()
	move_ret seed[22]
	c_ret

;; TERM 84: '*GUARD*'(il_combine_tupples_aux(_B, [], _C))
c_code local build_ref_84
	ret_reg &ref[84]
	call_c   build_ref_48()
	call_c   build_ref_83()
	call_c   Dyam_Create_Unary(&ref[48],&ref[83])
	move_ret ref[84]
	c_ret

;; TERM 83: il_combine_tupples_aux(_B, [], _C)
c_code local build_ref_83
	ret_reg &ref[83]
	call_c   build_ref_1235()
	call_c   Dyam_Term_Start(&ref[1235],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_End()
	move_ret ref[83]
	c_ret

;; TERM 1235: il_combine_tupples_aux
c_code local build_ref_1235
	ret_reg &ref[1235]
	call_c   Dyam_Create_Atom("il_combine_tupples_aux")
	move_ret ref[1235]
	c_ret

pl_code local fun9
	call_c   build_ref_84()
	call_c   Dyam_Unify_Item(&ref[84])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(2))
	call_c   Dyam_Reg_Deallocate(1)
	pl_jump  pred_null_tupple_1()

;; TERM 85: '*GUARD*'(il_combine_tupples_aux(_B, [], _C)) :> []
c_code local build_ref_85
	ret_reg &ref[85]
	call_c   build_ref_84()
	call_c   Dyam_Create_Binary(I(9),&ref[84],I(0))
	move_ret ref[85]
	c_ret

c_code local build_seed_0
	ret_reg &seed[0]
	call_c   build_ref_52()
	call_c   build_ref_88()
	call_c   Dyam_Seed_Start(&ref[52],&ref[88],I(0),fun15,1)
	call_c   build_ref_89()
	call_c   Dyam_Seed_Add_Comp(&ref[89],fun18,0)
	call_c   Dyam_Seed_End()
	move_ret seed[0]
	c_ret

;; TERM 89: '*CITEM*'(tag_il_args(_B, _C, [_B,_C]), _A)
c_code local build_ref_89
	ret_reg &ref[89]
	call_c   build_ref_56()
	call_c   build_ref_86()
	call_c   Dyam_Create_Binary(&ref[56],&ref[86],V(0))
	move_ret ref[89]
	c_ret

;; TERM 86: tag_il_args(_B, _C, [_B,_C])
c_code local build_ref_86
	ret_reg &ref[86]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Tupple(1,2,I(0))
	move_ret R(0)
	call_c   build_ref_1236()
	call_c   Dyam_Term_Start(&ref[1236],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_End()
	move_ret ref[86]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1236: tag_il_args
c_code local build_ref_1236
	ret_reg &ref[1236]
	call_c   Dyam_Create_Atom("tag_il_args")
	move_ret ref[1236]
	c_ret

c_code local build_seed_70
	ret_reg &seed[70]
	call_c   build_ref_0()
	call_c   build_ref_73()
	call_c   Dyam_Seed_Start(&ref[0],&ref[73],&ref[73],fun3,1)
	call_c   build_ref_74()
	call_c   Dyam_Seed_Add_Comp(&ref[74],&ref[73],0)
	call_c   Dyam_Seed_End()
	move_ret seed[70]
	c_ret

pl_code local fun16
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Object(R(0))
	call_c   Dyam_Tabule()
	call_c   Dyam_Schedule()
	pl_ret

pl_code local fun17
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Choice(fun16)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Subsume(R(0))
	fail_ret
	call_c   Dyam_Cut()
	pl_ret

long local pool_fun18[3]=[2,build_ref_89,build_seed_70]

pl_code local fun18
	call_c   Dyam_Pool(pool_fun18)
	call_c   Dyam_Unify_Item(&ref[89])
	fail_ret
	pl_jump  fun17(&seed[70],2)

;; TERM 88: '*FIRST*'(tag_il_args(_B, _C, [_B,_C])) :> []
c_code local build_ref_88
	ret_reg &ref[88]
	call_c   build_ref_87()
	call_c   Dyam_Create_Binary(I(9),&ref[87],I(0))
	move_ret ref[88]
	c_ret

;; TERM 87: '*FIRST*'(tag_il_args(_B, _C, [_B,_C]))
c_code local build_ref_87
	ret_reg &ref[87]
	call_c   build_ref_52()
	call_c   build_ref_86()
	call_c   Dyam_Create_Unary(&ref[52],&ref[86])
	move_ret ref[87]
	c_ret

c_code local build_seed_36
	ret_reg &seed[36]
	call_c   build_ref_52()
	call_c   build_ref_92()
	call_c   Dyam_Seed_Start(&ref[52],&ref[92],I(0),fun15,1)
	call_c   build_ref_93()
	call_c   Dyam_Seed_Add_Comp(&ref[93],fun27,0)
	call_c   Dyam_Seed_End()
	move_ret seed[36]
	c_ret

;; TERM 93: '*CITEM*'('call_tag_custom_subst/2'(_B), _A)
c_code local build_ref_93
	ret_reg &ref[93]
	call_c   build_ref_56()
	call_c   build_ref_90()
	call_c   Dyam_Create_Binary(&ref[56],&ref[90],V(0))
	move_ret ref[93]
	c_ret

;; TERM 90: 'call_tag_custom_subst/2'(_B)
c_code local build_ref_90
	ret_reg &ref[90]
	call_c   build_ref_1237()
	call_c   Dyam_Create_Unary(&ref[1237],V(1))
	move_ret ref[90]
	c_ret

;; TERM 1237: 'call_tag_custom_subst/2'
c_code local build_ref_1237
	ret_reg &ref[1237]
	call_c   Dyam_Create_Atom("call_tag_custom_subst/2")
	move_ret ref[1237]
	c_ret

;; TERM 95: tag_custom_subst(_C)
c_code local build_ref_95
	ret_reg &ref[95]
	call_c   build_ref_1238()
	call_c   Dyam_Create_Unary(&ref[1238],V(2))
	move_ret ref[95]
	c_ret

;; TERM 1238: tag_custom_subst
c_code local build_ref_1238
	ret_reg &ref[1238]
	call_c   Dyam_Create_Atom("tag_custom_subst")
	move_ret ref[1238]
	c_ret

c_code local build_seed_71
	ret_reg &seed[71]
	call_c   build_ref_0()
	call_c   build_ref_96()
	call_c   Dyam_Seed_Start(&ref[0],&ref[96],&ref[96],fun3,1)
	call_c   build_ref_97()
	call_c   Dyam_Seed_Add_Comp(&ref[97],&ref[96],0)
	call_c   Dyam_Seed_End()
	move_ret seed[71]
	c_ret

;; TERM 97: '*RITEM*'(_A, return(_C)) :> '$$HOLE$$'
c_code local build_ref_97
	ret_reg &ref[97]
	call_c   build_ref_96()
	call_c   Dyam_Create_Binary(I(9),&ref[96],I(7))
	move_ret ref[97]
	c_ret

;; TERM 96: '*RITEM*'(_A, return(_C))
c_code local build_ref_96
	ret_reg &ref[96]
	call_c   build_ref_0()
	call_c   build_ref_11()
	call_c   Dyam_Create_Binary(&ref[0],V(0),&ref[11])
	move_ret ref[96]
	c_ret

;; TERM 11: return(_C)
c_code local build_ref_11
	ret_reg &ref[11]
	call_c   build_ref_1233()
	call_c   Dyam_Create_Unary(&ref[1233],V(2))
	move_ret ref[11]
	c_ret

long local pool_fun25[3]=[2,build_ref_95,build_seed_71]

pl_code local fun26
	call_c   Dyam_Remove_Choice()
fun25:
	pl_call  Object_1(&ref[95])
	call_c   Dyam_Deallocate()
	pl_jump  fun13(&seed[71],2)


;; TERM 94: protect(_D)
c_code local build_ref_94
	ret_reg &ref[94]
	call_c   build_ref_1239()
	call_c   Dyam_Create_Unary(&ref[1239],V(3))
	move_ret ref[94]
	c_ret

;; TERM 1239: protect
c_code local build_ref_1239
	ret_reg &ref[1239]
	call_c   Dyam_Create_Atom("protect")
	move_ret ref[1239]
	c_ret

long local pool_fun27[4]=[65538,build_ref_93,build_ref_94,pool_fun25]

pl_code local fun27
	call_c   Dyam_Pool(pool_fun27)
	call_c   Dyam_Unify_Item(&ref[93])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun26)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[94])
	fail_ret
	call_c   Dyam_Cut()
	pl_fail

;; TERM 92: '*FIRST*'('call_tag_custom_subst/2'(_B)) :> []
c_code local build_ref_92
	ret_reg &ref[92]
	call_c   build_ref_91()
	call_c   Dyam_Create_Binary(I(9),&ref[91],I(0))
	move_ret ref[92]
	c_ret

;; TERM 91: '*FIRST*'('call_tag_custom_subst/2'(_B))
c_code local build_ref_91
	ret_reg &ref[91]
	call_c   build_ref_52()
	call_c   build_ref_90()
	call_c   Dyam_Create_Unary(&ref[52],&ref[90])
	move_ret ref[91]
	c_ret

c_code local build_seed_9
	ret_reg &seed[9]
	call_c   build_ref_52()
	call_c   build_ref_100()
	call_c   Dyam_Seed_Start(&ref[52],&ref[100],I(0),fun15,1)
	call_c   build_ref_101()
	call_c   Dyam_Seed_Add_Comp(&ref[101],fun44,0)
	call_c   Dyam_Seed_End()
	move_ret seed[9]
	c_ret

;; TERM 101: '*CITEM*'('call_core_info/3'(_B, tag_anchor{family=> _C, coanchors=> _D, equations=> _E}), _A)
c_code local build_ref_101
	ret_reg &ref[101]
	call_c   build_ref_56()
	call_c   build_ref_98()
	call_c   Dyam_Create_Binary(&ref[56],&ref[98],V(0))
	move_ret ref[101]
	c_ret

;; TERM 98: 'call_core_info/3'(_B, tag_anchor{family=> _C, coanchors=> _D, equations=> _E})
c_code local build_ref_98
	ret_reg &ref[98]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1241()
	call_c   Dyam_Term_Start(&ref[1241],3)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_1240()
	call_c   Dyam_Create_Binary(&ref[1240],V(1),R(0))
	move_ret ref[98]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1240: 'call_core_info/3'
c_code local build_ref_1240
	ret_reg &ref[1240]
	call_c   Dyam_Create_Atom("call_core_info/3")
	move_ret ref[1240]
	c_ret

;; TERM 1241: tag_anchor!'$ft'
c_code local build_ref_1241
	ret_reg &ref[1241]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1245()
	call_c   Dyam_Create_List(&ref[1245],I(0))
	move_ret R(0)
	call_c   build_ref_1244()
	call_c   Dyam_Create_List(&ref[1244],R(0))
	move_ret R(0)
	call_c   build_ref_1243()
	call_c   Dyam_Create_List(&ref[1243],R(0))
	move_ret R(0)
	call_c   build_ref_1242()
	call_c   Dyam_Create_Atom_Module("$ft",&ref[1242])
	move_ret ref[1241]
	call_c   DYAM_Feature_2(&ref[1242],R(0))
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1242: tag_anchor
c_code local build_ref_1242
	ret_reg &ref[1242]
	call_c   Dyam_Create_Atom("tag_anchor")
	move_ret ref[1242]
	c_ret

;; TERM 1243: family
c_code local build_ref_1243
	ret_reg &ref[1243]
	call_c   Dyam_Create_Atom("family")
	move_ret ref[1243]
	c_ret

;; TERM 1244: coanchors
c_code local build_ref_1244
	ret_reg &ref[1244]
	call_c   Dyam_Create_Atom("coanchors")
	move_ret ref[1244]
	c_ret

;; TERM 1245: equations
c_code local build_ref_1245
	ret_reg &ref[1245]
	call_c   Dyam_Create_Atom("equations")
	move_ret ref[1245]
	c_ret

c_code local build_seed_72
	ret_reg &seed[72]
	call_c   build_ref_0()
	call_c   build_ref_103()
	call_c   Dyam_Seed_Start(&ref[0],&ref[103],&ref[103],fun3,1)
	call_c   build_ref_104()
	call_c   Dyam_Seed_Add_Comp(&ref[104],&ref[103],0)
	call_c   Dyam_Seed_End()
	move_ret seed[72]
	c_ret

;; TERM 104: '*RITEM*'(_A, return(tag_anchor{family=> _C, coanchors=> _D, equations=> _E})) :> '$$HOLE$$'
c_code local build_ref_104
	ret_reg &ref[104]
	call_c   build_ref_103()
	call_c   Dyam_Create_Binary(I(9),&ref[103],I(7))
	move_ret ref[104]
	c_ret

;; TERM 103: '*RITEM*'(_A, return(tag_anchor{family=> _C, coanchors=> _D, equations=> _E}))
c_code local build_ref_103
	ret_reg &ref[103]
	call_c   build_ref_0()
	call_c   build_ref_102()
	call_c   Dyam_Create_Binary(&ref[0],V(0),&ref[102])
	move_ret ref[103]
	c_ret

;; TERM 102: return(tag_anchor{family=> _C, coanchors=> _D, equations=> _E})
c_code local build_ref_102
	ret_reg &ref[102]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1241()
	call_c   Dyam_Term_Start(&ref[1241],3)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_1233()
	call_c   Dyam_Create_Unary(&ref[1233],R(0))
	move_ret ref[102]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun44[3]=[2,build_ref_101,build_seed_72]

pl_code local fun44
	call_c   Dyam_Pool(pool_fun44)
	call_c   Dyam_Unify_Item(&ref[101])
	fail_ret
	pl_jump  fun13(&seed[72],2)

;; TERM 100: '*FIRST*'('call_core_info/3'(_B, tag_anchor{family=> _C, coanchors=> _D, equations=> _E})) :> []
c_code local build_ref_100
	ret_reg &ref[100]
	call_c   build_ref_99()
	call_c   Dyam_Create_Binary(I(9),&ref[99],I(0))
	move_ret ref[100]
	c_ret

;; TERM 99: '*FIRST*'('call_core_info/3'(_B, tag_anchor{family=> _C, coanchors=> _D, equations=> _E}))
c_code local build_ref_99
	ret_reg &ref[99]
	call_c   build_ref_52()
	call_c   build_ref_98()
	call_c   Dyam_Create_Unary(&ref[52],&ref[98])
	move_ret ref[99]
	c_ret

c_code local build_seed_10
	ret_reg &seed[10]
	call_c   build_ref_47()
	call_c   build_ref_107()
	call_c   Dyam_Seed_Start(&ref[47],&ref[107],I(0),fun1,1)
	call_c   build_ref_106()
	call_c   Dyam_Seed_Add_Comp(&ref[106],fun10,0)
	call_c   Dyam_Seed_End()
	move_ret seed[10]
	c_ret

;; TERM 106: '*GUARD*'(directive(tag_tree_cutter(_B), _C, _D, _E))
c_code local build_ref_106
	ret_reg &ref[106]
	call_c   build_ref_48()
	call_c   build_ref_105()
	call_c   Dyam_Create_Unary(&ref[48],&ref[105])
	move_ret ref[106]
	c_ret

;; TERM 105: directive(tag_tree_cutter(_B), _C, _D, _E)
c_code local build_ref_105
	ret_reg &ref[105]
	call_c   build_ref_1246()
	call_c   build_ref_108()
	call_c   Dyam_Term_Start(&ref[1246],4)
	call_c   Dyam_Term_Arg(&ref[108])
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[105]
	c_ret

;; TERM 108: tag_tree_cutter(_B)
c_code local build_ref_108
	ret_reg &ref[108]
	call_c   build_ref_1247()
	call_c   Dyam_Create_Unary(&ref[1247],V(1))
	move_ret ref[108]
	c_ret

;; TERM 1247: tag_tree_cutter
c_code local build_ref_1247
	ret_reg &ref[1247]
	call_c   Dyam_Create_Atom("tag_tree_cutter")
	move_ret ref[1247]
	c_ret

;; TERM 1246: directive
c_code local build_ref_1246
	ret_reg &ref[1246]
	call_c   Dyam_Create_Atom("directive")
	move_ret ref[1246]
	c_ret

long local pool_fun10[3]=[2,build_ref_106,build_ref_108]

pl_code local fun10
	call_c   Dyam_Pool(pool_fun10)
	call_c   Dyam_Unify_Item(&ref[106])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[108], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Deallocate(1)
	pl_jump  pred_record_without_doublon_1()

;; TERM 107: '*GUARD*'(directive(tag_tree_cutter(_B), _C, _D, _E)) :> []
c_code local build_ref_107
	ret_reg &ref[107]
	call_c   build_ref_106()
	call_c   Dyam_Create_Binary(I(9),&ref[106],I(0))
	move_ret ref[107]
	c_ret

c_code local build_seed_12
	ret_reg &seed[12]
	call_c   build_ref_47()
	call_c   build_ref_111()
	call_c   Dyam_Seed_Start(&ref[47],&ref[111],I(0),fun1,1)
	call_c   build_ref_110()
	call_c   Dyam_Seed_Add_Comp(&ref[110],fun11,0)
	call_c   Dyam_Seed_End()
	move_ret seed[12]
	c_ret

;; TERM 110: '*GUARD*'(directive(tag_terminal_cat(_B), _C, _D, _E))
c_code local build_ref_110
	ret_reg &ref[110]
	call_c   build_ref_48()
	call_c   build_ref_109()
	call_c   Dyam_Create_Unary(&ref[48],&ref[109])
	move_ret ref[110]
	c_ret

;; TERM 109: directive(tag_terminal_cat(_B), _C, _D, _E)
c_code local build_ref_109
	ret_reg &ref[109]
	call_c   build_ref_1246()
	call_c   build_ref_112()
	call_c   Dyam_Term_Start(&ref[1246],4)
	call_c   Dyam_Term_Arg(&ref[112])
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[109]
	c_ret

;; TERM 112: tag_terminal_cat(_B)
c_code local build_ref_112
	ret_reg &ref[112]
	call_c   build_ref_1248()
	call_c   Dyam_Create_Unary(&ref[1248],V(1))
	move_ret ref[112]
	c_ret

;; TERM 1248: tag_terminal_cat
c_code local build_ref_1248
	ret_reg &ref[1248]
	call_c   Dyam_Create_Atom("tag_terminal_cat")
	move_ret ref[1248]
	c_ret

long local pool_fun11[3]=[2,build_ref_110,build_ref_112]

pl_code local fun11
	call_c   Dyam_Pool(pool_fun11)
	call_c   Dyam_Unify_Item(&ref[110])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[112], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Deallocate(1)
	pl_jump  pred_record_without_doublon_1()

;; TERM 111: '*GUARD*'(directive(tag_terminal_cat(_B), _C, _D, _E)) :> []
c_code local build_ref_111
	ret_reg &ref[111]
	call_c   build_ref_110()
	call_c   Dyam_Create_Binary(I(9),&ref[110],I(0))
	move_ret ref[111]
	c_ret

c_code local build_seed_62
	ret_reg &seed[62]
	call_c   build_ref_52()
	call_c   build_ref_115()
	call_c   Dyam_Seed_Start(&ref[52],&ref[115],I(0),fun15,1)
	call_c   build_ref_116()
	call_c   Dyam_Seed_Add_Comp(&ref[116],fun56,0)
	call_c   Dyam_Seed_End()
	move_ret seed[62]
	c_ret

;; TERM 116: '*CITEM*'('call_core_info/3'(_B, strip(_C)), _A)
c_code local build_ref_116
	ret_reg &ref[116]
	call_c   build_ref_56()
	call_c   build_ref_113()
	call_c   Dyam_Create_Binary(&ref[56],&ref[113],V(0))
	move_ret ref[116]
	c_ret

;; TERM 113: 'call_core_info/3'(_B, strip(_C))
c_code local build_ref_113
	ret_reg &ref[113]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1249()
	call_c   Dyam_Create_Unary(&ref[1249],V(2))
	move_ret R(0)
	call_c   build_ref_1240()
	call_c   Dyam_Create_Binary(&ref[1240],V(1),R(0))
	move_ret ref[113]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1249: strip
c_code local build_ref_1249
	ret_reg &ref[1249]
	call_c   Dyam_Create_Atom("strip")
	move_ret ref[1249]
	c_ret

;; TERM 205: '$CLOSURE'('$fun'(55, 0, 1127715116), '$TUPPLE'(35075412199660))
c_code local build_ref_205
	ret_reg &ref[205]
	call_c   build_ref_204()
	call_c   Dyam_Closure_Aux(fun55,&ref[204])
	move_ret ref[205]
	c_ret

c_code local build_seed_81
	ret_reg &seed[81]
	call_c   build_ref_0()
	call_c   build_ref_197()
	call_c   Dyam_Seed_Start(&ref[0],&ref[197],&ref[197],fun3,1)
	call_c   build_ref_198()
	call_c   Dyam_Seed_Add_Comp(&ref[198],&ref[197],0)
	call_c   Dyam_Seed_End()
	move_ret seed[81]
	c_ret

;; TERM 198: '*RITEM*'(_A, return(_D)) :> '$$HOLE$$'
c_code local build_ref_198
	ret_reg &ref[198]
	call_c   build_ref_197()
	call_c   Dyam_Create_Binary(I(9),&ref[197],I(7))
	move_ret ref[198]
	c_ret

;; TERM 197: '*RITEM*'(_A, return(_D))
c_code local build_ref_197
	ret_reg &ref[197]
	call_c   build_ref_0()
	call_c   build_ref_196()
	call_c   Dyam_Create_Binary(&ref[0],V(0),&ref[196])
	move_ret ref[197]
	c_ret

;; TERM 196: return(_D)
c_code local build_ref_196
	ret_reg &ref[196]
	call_c   build_ref_1233()
	call_c   Dyam_Create_Unary(&ref[1233],V(3))
	move_ret ref[196]
	c_ret

long local pool_fun55[2]=[1,build_seed_81]

pl_code local fun55
	call_c   Dyam_Pool(pool_fun55)
	pl_jump  fun13(&seed[81],2)

;; TERM 204: '$TUPPLE'(35075412199660)
c_code local build_ref_204
	ret_reg &ref[204]
	call_c   Dyam_Create_Simple_Tupple(0,301989888)
	move_ret ref[204]
	c_ret

long local pool_fun56[3]=[2,build_ref_116,build_ref_205]

pl_code local fun56
	call_c   Dyam_Pool(pool_fun56)
	call_c   Dyam_Unify_Item(&ref[116])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[205], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Load(4,V(1))
	call_c   Dyam_Reg_Load(6,V(2))
	move     V(3), R(8)
	move     S(5), R(9)
	call_c   Dyam_Reg_Deallocate(5)
fun52:
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Reg_Bind(2,V(5))
	call_c   Dyam_Multi_Reg_Bind(4,2,3)
	call_c   Dyam_Choice(fun51)
	pl_call  fun46(&seed[73],1)
	pl_fail


;; TERM 115: '*FIRST*'('call_core_info/3'(_B, strip(_C))) :> []
c_code local build_ref_115
	ret_reg &ref[115]
	call_c   build_ref_114()
	call_c   Dyam_Create_Binary(I(9),&ref[114],I(0))
	move_ret ref[115]
	c_ret

;; TERM 114: '*FIRST*'('call_core_info/3'(_B, strip(_C)))
c_code local build_ref_114
	ret_reg &ref[114]
	call_c   build_ref_52()
	call_c   build_ref_113()
	call_c   Dyam_Create_Unary(&ref[52],&ref[113])
	move_ret ref[114]
	c_ret

c_code local build_seed_13
	ret_reg &seed[13]
	call_c   build_ref_47()
	call_c   build_ref_123()
	call_c   Dyam_Seed_Start(&ref[47],&ref[123],I(0),fun1,1)
	call_c   build_ref_122()
	call_c   Dyam_Seed_Add_Comp(&ref[122],fun19,0)
	call_c   Dyam_Seed_End()
	move_ret seed[13]
	c_ret

;; TERM 122: '*GUARD*'(directive(tag_custom_subst(_B), _C, _D, _E))
c_code local build_ref_122
	ret_reg &ref[122]
	call_c   build_ref_48()
	call_c   build_ref_121()
	call_c   Dyam_Create_Unary(&ref[48],&ref[121])
	move_ret ref[122]
	c_ret

;; TERM 121: directive(tag_custom_subst(_B), _C, _D, _E)
c_code local build_ref_121
	ret_reg &ref[121]
	call_c   build_ref_1246()
	call_c   build_ref_125()
	call_c   Dyam_Term_Start(&ref[1246],4)
	call_c   Dyam_Term_Arg(&ref[125])
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[121]
	c_ret

;; TERM 125: tag_custom_subst(_B)
c_code local build_ref_125
	ret_reg &ref[125]
	call_c   build_ref_1238()
	call_c   Dyam_Create_Unary(&ref[1238],V(1))
	move_ret ref[125]
	c_ret

;; TERM 124: tag_custom_subst(_F)
c_code local build_ref_124
	ret_reg &ref[124]
	call_c   build_ref_1238()
	call_c   Dyam_Create_Unary(&ref[1238],V(5))
	move_ret ref[124]
	c_ret

long local pool_fun19[4]=[3,build_ref_122,build_ref_124,build_ref_125]

pl_code local fun19
	call_c   Dyam_Pool(pool_fun19)
	call_c   Dyam_Unify_Item(&ref[122])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[124], R(0)
	move     S(5), R(1)
	move     &ref[125], R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_directive_updater_2()

;; TERM 123: '*GUARD*'(directive(tag_custom_subst(_B), _C, _D, _E)) :> []
c_code local build_ref_123
	ret_reg &ref[123]
	call_c   build_ref_122()
	call_c   Dyam_Create_Binary(I(9),&ref[122],I(0))
	move_ret ref[123]
	c_ret

c_code local build_seed_11
	ret_reg &seed[11]
	call_c   build_ref_47()
	call_c   build_ref_132()
	call_c   Dyam_Seed_Start(&ref[47],&ref[132],I(0),fun1,1)
	call_c   build_ref_131()
	call_c   Dyam_Seed_Add_Comp(&ref[131],fun21,0)
	call_c   Dyam_Seed_End()
	move_ret seed[11]
	c_ret

;; TERM 131: '*GUARD*'(directive(tag_subst_cat(_B, _C), _D, _E, _F))
c_code local build_ref_131
	ret_reg &ref[131]
	call_c   build_ref_48()
	call_c   build_ref_130()
	call_c   Dyam_Create_Unary(&ref[48],&ref[130])
	move_ret ref[131]
	c_ret

;; TERM 130: directive(tag_subst_cat(_B, _C), _D, _E, _F)
c_code local build_ref_130
	ret_reg &ref[130]
	call_c   build_ref_1246()
	call_c   build_ref_133()
	call_c   Dyam_Term_Start(&ref[1246],4)
	call_c   Dyam_Term_Arg(&ref[133])
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[130]
	c_ret

;; TERM 133: tag_subst_cat(_B, _C)
c_code local build_ref_133
	ret_reg &ref[133]
	call_c   build_ref_1250()
	call_c   Dyam_Create_Binary(&ref[1250],V(1),V(2))
	move_ret ref[133]
	c_ret

;; TERM 1250: tag_subst_cat
c_code local build_ref_1250
	ret_reg &ref[1250]
	call_c   Dyam_Create_Atom("tag_subst_cat")
	move_ret ref[1250]
	c_ret

long local pool_fun21[3]=[2,build_ref_131,build_ref_133]

pl_code local fun21
	call_c   Dyam_Pool(pool_fun21)
	call_c   Dyam_Unify_Item(&ref[131])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[133], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Deallocate(1)
	pl_jump  pred_record_without_doublon_1()

;; TERM 132: '*GUARD*'(directive(tag_subst_cat(_B, _C), _D, _E, _F)) :> []
c_code local build_ref_132
	ret_reg &ref[132]
	call_c   build_ref_131()
	call_c   Dyam_Create_Binary(I(9),&ref[131],I(0))
	move_ret ref[132]
	c_ret

c_code local build_seed_63
	ret_reg &seed[63]
	call_c   build_ref_52()
	call_c   build_ref_128()
	call_c   Dyam_Seed_Start(&ref[52],&ref[128],I(0),fun15,1)
	call_c   build_ref_129()
	call_c   Dyam_Seed_Add_Comp(&ref[129],fun54,0)
	call_c   Dyam_Seed_End()
	move_ret seed[63]
	c_ret

;; TERM 129: '*CITEM*'('call_core_info/3'(_B, (_C * _D)), _A)
c_code local build_ref_129
	ret_reg &ref[129]
	call_c   build_ref_56()
	call_c   build_ref_126()
	call_c   Dyam_Create_Binary(&ref[56],&ref[126],V(0))
	move_ret ref[129]
	c_ret

;; TERM 126: 'call_core_info/3'(_B, (_C * _D))
c_code local build_ref_126
	ret_reg &ref[126]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1251()
	call_c   Dyam_Create_Binary(&ref[1251],V(2),V(3))
	move_ret R(0)
	call_c   build_ref_1240()
	call_c   Dyam_Create_Binary(&ref[1240],V(1),R(0))
	move_ret ref[126]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1251: *
c_code local build_ref_1251
	ret_reg &ref[1251]
	call_c   Dyam_Create_Atom("*")
	move_ret ref[1251]
	c_ret

;; TERM 202: '$CLOSURE'('$fun'(53, 0, 1127706488), '$TUPPLE'(35075412199552))
c_code local build_ref_202
	ret_reg &ref[202]
	call_c   build_ref_201()
	call_c   Dyam_Closure_Aux(fun53,&ref[201])
	move_ret ref[202]
	c_ret

c_code local build_seed_80
	ret_reg &seed[80]
	call_c   build_ref_0()
	call_c   build_ref_194()
	call_c   Dyam_Seed_Start(&ref[0],&ref[194],&ref[194],fun3,1)
	call_c   build_ref_195()
	call_c   Dyam_Seed_Add_Comp(&ref[195],&ref[194],0)
	call_c   Dyam_Seed_End()
	move_ret seed[80]
	c_ret

;; TERM 195: '*RITEM*'(_A, return(_E)) :> '$$HOLE$$'
c_code local build_ref_195
	ret_reg &ref[195]
	call_c   build_ref_194()
	call_c   Dyam_Create_Binary(I(9),&ref[194],I(7))
	move_ret ref[195]
	c_ret

;; TERM 194: '*RITEM*'(_A, return(_E))
c_code local build_ref_194
	ret_reg &ref[194]
	call_c   build_ref_0()
	call_c   build_ref_187()
	call_c   Dyam_Create_Binary(&ref[0],V(0),&ref[187])
	move_ret ref[194]
	c_ret

;; TERM 187: return(_E)
c_code local build_ref_187
	ret_reg &ref[187]
	call_c   build_ref_1233()
	call_c   Dyam_Create_Unary(&ref[1233],V(4))
	move_ret ref[187]
	c_ret

long local pool_fun53[2]=[1,build_seed_80]

pl_code local fun53
	call_c   Dyam_Pool(pool_fun53)
	pl_jump  fun13(&seed[80],2)

;; TERM 201: '$TUPPLE'(35075412199552)
c_code local build_ref_201
	ret_reg &ref[201]
	call_c   Dyam_Create_Simple_Tupple(0,285212672)
	move_ret ref[201]
	c_ret

long local pool_fun54[3]=[2,build_ref_129,build_ref_202]

pl_code local fun54
	call_c   Dyam_Pool(pool_fun54)
	call_c   Dyam_Unify_Item(&ref[129])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[202], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Load(4,V(1))
	call_c   Dyam_Reg_Load(6,V(2))
	move     V(4), R(8)
	move     S(5), R(9)
	call_c   Dyam_Reg_Deallocate(5)
	pl_jump  fun52()

;; TERM 128: '*FIRST*'('call_core_info/3'(_B, (_C * _D))) :> []
c_code local build_ref_128
	ret_reg &ref[128]
	call_c   build_ref_127()
	call_c   Dyam_Create_Binary(I(9),&ref[127],I(0))
	move_ret ref[128]
	c_ret

;; TERM 127: '*FIRST*'('call_core_info/3'(_B, (_C * _D)))
c_code local build_ref_127
	ret_reg &ref[127]
	call_c   build_ref_52()
	call_c   build_ref_126()
	call_c   Dyam_Create_Unary(&ref[52],&ref[126])
	move_ret ref[127]
	c_ret

c_code local build_seed_64
	ret_reg &seed[64]
	call_c   build_ref_52()
	call_c   build_ref_136()
	call_c   Dyam_Seed_Start(&ref[52],&ref[136],I(0),fun15,1)
	call_c   build_ref_137()
	call_c   Dyam_Seed_Add_Comp(&ref[137],fun24,0)
	call_c   Dyam_Seed_End()
	move_ret seed[64]
	c_ret

;; TERM 137: '*CITEM*'(verbose_tag(_B), _A)
c_code local build_ref_137
	ret_reg &ref[137]
	call_c   build_ref_56()
	call_c   build_ref_134()
	call_c   Dyam_Create_Binary(&ref[56],&ref[134],V(0))
	move_ret ref[137]
	c_ret

;; TERM 134: verbose_tag(_B)
c_code local build_ref_134
	ret_reg &ref[134]
	call_c   build_ref_138()
	call_c   Dyam_Create_Unary(&ref[138],V(1))
	move_ret ref[134]
	c_ret

;; TERM 138: verbose_tag
c_code local build_ref_138
	ret_reg &ref[138]
	call_c   Dyam_Create_Atom("verbose_tag")
	move_ret ref[138]
	c_ret

long local pool_fun22[2]=[1,build_seed_69]

pl_code local fun23
	call_c   Dyam_Remove_Choice()
fun22:
	call_c   Dyam_Deallocate()
	pl_jump  fun13(&seed[69],2)


;; TERM 139: guide(_B, _C, _D)
c_code local build_ref_139
	ret_reg &ref[139]
	call_c   build_ref_1252()
	call_c   Dyam_Term_Start(&ref[1252],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[139]
	c_ret

;; TERM 1252: guide
c_code local build_ref_1252
	ret_reg &ref[1252]
	call_c   Dyam_Create_Atom("guide")
	move_ret ref[1252]
	c_ret

long local pool_fun24[5]=[65539,build_ref_137,build_ref_138,build_ref_139,pool_fun22]

pl_code local fun24
	call_c   Dyam_Pool(pool_fun24)
	call_c   Dyam_Unify_Item(&ref[137])
	fail_ret
	call_c   Dyam_Allocate(0)
	pl_call  Object_1(&ref[138])
	call_c   Dyam_Choice(fun23)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[139])
	call_c   Dyam_Cut()
	pl_fail

;; TERM 136: '*FIRST*'(verbose_tag(_B)) :> []
c_code local build_ref_136
	ret_reg &ref[136]
	call_c   build_ref_135()
	call_c   Dyam_Create_Binary(I(9),&ref[135],I(0))
	move_ret ref[136]
	c_ret

;; TERM 135: '*FIRST*'(verbose_tag(_B))
c_code local build_ref_135
	ret_reg &ref[135]
	call_c   build_ref_52()
	call_c   build_ref_134()
	call_c   Dyam_Create_Unary(&ref[52],&ref[134])
	move_ret ref[135]
	c_ret

c_code local build_seed_41
	ret_reg &seed[41]
	call_c   build_ref_47()
	call_c   build_ref_142()
	call_c   Dyam_Seed_Start(&ref[47],&ref[142],I(0),fun1,1)
	call_c   build_ref_141()
	call_c   Dyam_Seed_Add_Comp(&ref[141],fun31,0)
	call_c   Dyam_Seed_End()
	move_ret seed[41]
	c_ret

;; TERM 141: '*GUARD*'(feature_args_unif_aux([_B|_C], [_D|_E]))
c_code local build_ref_141
	ret_reg &ref[141]
	call_c   build_ref_48()
	call_c   build_ref_140()
	call_c   Dyam_Create_Unary(&ref[48],&ref[140])
	move_ret ref[141]
	c_ret

;; TERM 140: feature_args_unif_aux([_B|_C], [_D|_E])
c_code local build_ref_140
	ret_reg &ref[140]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(1),V(2))
	move_ret R(0)
	call_c   build_ref_1228()
	call_c   build_ref_778()
	call_c   Dyam_Create_Binary(&ref[1228],R(0),&ref[778])
	move_ret ref[140]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 778: [_D|_E]
c_code local build_ref_778
	ret_reg &ref[778]
	call_c   Dyam_Create_List(V(3),V(4))
	move_ret ref[778]
	c_ret

c_code local build_seed_74
	ret_reg &seed[74]
	call_c   build_ref_48()
	call_c   build_ref_144()
	call_c   Dyam_Seed_Start(&ref[48],&ref[144],I(0),fun3,1)
	call_c   build_ref_145()
	call_c   Dyam_Seed_Add_Comp(&ref[145],&ref[144],0)
	call_c   Dyam_Seed_End()
	move_ret seed[74]
	c_ret

;; TERM 145: '*GUARD*'(feature_args_unif_aux(_C, _E)) :> '$$HOLE$$'
c_code local build_ref_145
	ret_reg &ref[145]
	call_c   build_ref_144()
	call_c   Dyam_Create_Binary(I(9),&ref[144],I(7))
	move_ret ref[145]
	c_ret

;; TERM 144: '*GUARD*'(feature_args_unif_aux(_C, _E))
c_code local build_ref_144
	ret_reg &ref[144]
	call_c   build_ref_48()
	call_c   build_ref_143()
	call_c   Dyam_Create_Unary(&ref[48],&ref[143])
	move_ret ref[144]
	c_ret

;; TERM 143: feature_args_unif_aux(_C, _E)
c_code local build_ref_143
	ret_reg &ref[143]
	call_c   build_ref_1228()
	call_c   Dyam_Create_Binary(&ref[1228],V(2),V(4))
	move_ret ref[143]
	c_ret

pl_code local fun28
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Light_Object(R(0))
	pl_jump  Schedule_Prolog()

long local pool_fun29[2]=[1,build_seed_74]

pl_code local fun30
	call_c   Dyam_Remove_Choice()
fun29:
	call_c   Dyam_Deallocate()
	pl_jump  fun28(&seed[74],1)


long local pool_fun31[4]=[131073,build_ref_141,pool_fun29,pool_fun29]

pl_code local fun31
	call_c   Dyam_Pool(pool_fun31)
	call_c   Dyam_Unify_Item(&ref[141])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun30)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),V(3))
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun29()

;; TERM 142: '*GUARD*'(feature_args_unif_aux([_B|_C], [_D|_E])) :> []
c_code local build_ref_142
	ret_reg &ref[142]
	call_c   build_ref_141()
	call_c   Dyam_Create_Binary(I(9),&ref[141],I(0))
	move_ret ref[142]
	c_ret

c_code local build_seed_23
	ret_reg &seed[23]
	call_c   build_ref_47()
	call_c   build_ref_154()
	call_c   Dyam_Seed_Start(&ref[47],&ref[154],I(0),fun1,1)
	call_c   build_ref_153()
	call_c   Dyam_Seed_Add_Comp(&ref[153],fun37,0)
	call_c   Dyam_Seed_End()
	move_ret seed[23]
	c_ret

;; TERM 153: '*GUARD*'(il_combine_tupples([_B|_C], _D))
c_code local build_ref_153
	ret_reg &ref[153]
	call_c   build_ref_48()
	call_c   build_ref_152()
	call_c   Dyam_Create_Unary(&ref[48],&ref[152])
	move_ret ref[153]
	c_ret

;; TERM 152: il_combine_tupples([_B|_C], _D)
c_code local build_ref_152
	ret_reg &ref[152]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(1),V(2))
	move_ret R(0)
	call_c   build_ref_1234()
	call_c   Dyam_Create_Binary(&ref[1234],R(0),V(3))
	move_ret ref[152]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_75
	ret_reg &seed[75]
	call_c   build_ref_48()
	call_c   build_ref_156()
	call_c   Dyam_Seed_Start(&ref[48],&ref[156],I(0),fun3,1)
	call_c   build_ref_157()
	call_c   Dyam_Seed_Add_Comp(&ref[157],&ref[156],0)
	call_c   Dyam_Seed_End()
	move_ret seed[75]
	c_ret

;; TERM 157: '*GUARD*'(il_combine_tupples(_C, _E)) :> '$$HOLE$$'
c_code local build_ref_157
	ret_reg &ref[157]
	call_c   build_ref_156()
	call_c   Dyam_Create_Binary(I(9),&ref[156],I(7))
	move_ret ref[157]
	c_ret

;; TERM 156: '*GUARD*'(il_combine_tupples(_C, _E))
c_code local build_ref_156
	ret_reg &ref[156]
	call_c   build_ref_48()
	call_c   build_ref_155()
	call_c   Dyam_Create_Unary(&ref[48],&ref[155])
	move_ret ref[156]
	c_ret

;; TERM 155: il_combine_tupples(_C, _E)
c_code local build_ref_155
	ret_reg &ref[155]
	call_c   build_ref_1234()
	call_c   Dyam_Create_Binary(&ref[1234],V(2),V(4))
	move_ret ref[155]
	c_ret

c_code local build_seed_76
	ret_reg &seed[76]
	call_c   build_ref_48()
	call_c   build_ref_159()
	call_c   Dyam_Seed_Start(&ref[48],&ref[159],I(0),fun3,1)
	call_c   build_ref_160()
	call_c   Dyam_Seed_Add_Comp(&ref[160],&ref[159],0)
	call_c   Dyam_Seed_End()
	move_ret seed[76]
	c_ret

;; TERM 160: '*GUARD*'(il_combine_tupples_aux(_B, _C, _F)) :> '$$HOLE$$'
c_code local build_ref_160
	ret_reg &ref[160]
	call_c   build_ref_159()
	call_c   Dyam_Create_Binary(I(9),&ref[159],I(7))
	move_ret ref[160]
	c_ret

;; TERM 159: '*GUARD*'(il_combine_tupples_aux(_B, _C, _F))
c_code local build_ref_159
	ret_reg &ref[159]
	call_c   build_ref_48()
	call_c   build_ref_158()
	call_c   Dyam_Create_Unary(&ref[48],&ref[158])
	move_ret ref[159]
	c_ret

;; TERM 158: il_combine_tupples_aux(_B, _C, _F)
c_code local build_ref_158
	ret_reg &ref[158]
	call_c   build_ref_1235()
	call_c   Dyam_Term_Start(&ref[1235],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[158]
	c_ret

long local pool_fun37[4]=[3,build_ref_153,build_seed_75,build_seed_76]

pl_code local fun37
	call_c   Dyam_Pool(pool_fun37)
	call_c   Dyam_Unify_Item(&ref[153])
	fail_ret
	call_c   Dyam_Allocate(0)
	pl_call  fun28(&seed[75],1)
	pl_call  fun28(&seed[76],1)
	call_c   Dyam_Reg_Load(0,V(5))
	call_c   Dyam_Reg_Load(2,V(4))
	call_c   Dyam_Reg_Load(4,V(3))
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  pred_union_3()

;; TERM 154: '*GUARD*'(il_combine_tupples([_B|_C], _D)) :> []
c_code local build_ref_154
	ret_reg &ref[154]
	call_c   build_ref_153()
	call_c   Dyam_Create_Binary(I(9),&ref[153],I(0))
	move_ret ref[154]
	c_ret

c_code local build_seed_61
	ret_reg &seed[61]
	call_c   build_ref_47()
	call_c   build_ref_170()
	call_c   Dyam_Seed_Start(&ref[47],&ref[170],I(0),fun1,1)
	call_c   build_ref_169()
	call_c   Dyam_Seed_Add_Comp(&ref[169],fun64,0)
	call_c   Dyam_Seed_End()
	move_ret seed[61]
	c_ret

;; TERM 169: '*GUARD*'(pgm_to_lpda(register_predicate(tag(_B)), ('*PROLOG-FIRST*'(_C) :> _D)))
c_code local build_ref_169
	ret_reg &ref[169]
	call_c   build_ref_48()
	call_c   build_ref_168()
	call_c   Dyam_Create_Unary(&ref[48],&ref[168])
	move_ret ref[169]
	c_ret

;; TERM 168: pgm_to_lpda(register_predicate(tag(_B)), ('*PROLOG-FIRST*'(_C) :> _D))
c_code local build_ref_168
	ret_reg &ref[168]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_1255()
	call_c   Dyam_Create_Unary(&ref[1255],V(1))
	move_ret R(0)
	call_c   build_ref_1254()
	call_c   Dyam_Create_Unary(&ref[1254],R(0))
	move_ret R(0)
	call_c   build_ref_1256()
	call_c   Dyam_Create_Unary(&ref[1256],V(2))
	move_ret R(1)
	call_c   Dyam_Create_Binary(I(9),R(1),V(3))
	move_ret R(1)
	call_c   build_ref_1253()
	call_c   Dyam_Create_Binary(&ref[1253],R(0),R(1))
	move_ret ref[168]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1253: pgm_to_lpda
c_code local build_ref_1253
	ret_reg &ref[1253]
	call_c   Dyam_Create_Atom("pgm_to_lpda")
	move_ret ref[1253]
	c_ret

;; TERM 1256: '*PROLOG-FIRST*'
c_code local build_ref_1256
	ret_reg &ref[1256]
	call_c   Dyam_Create_Atom("*PROLOG-FIRST*")
	move_ret ref[1256]
	c_ret

;; TERM 1254: register_predicate
c_code local build_ref_1254
	ret_reg &ref[1254]
	call_c   Dyam_Create_Atom("register_predicate")
	move_ret ref[1254]
	c_ret

;; TERM 1255: tag
c_code local build_ref_1255
	ret_reg &ref[1255]
	call_c   Dyam_Create_Atom("tag")
	move_ret ref[1255]
	c_ret

;; TERM 209: public(tag('*default*'))
c_code local build_ref_209
	ret_reg &ref[209]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1255()
	call_c   build_ref_1258()
	call_c   Dyam_Create_Unary(&ref[1255],&ref[1258])
	move_ret R(0)
	call_c   build_ref_1257()
	call_c   Dyam_Create_Unary(&ref[1257],R(0))
	move_ret ref[209]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1257: public
c_code local build_ref_1257
	ret_reg &ref[1257]
	call_c   Dyam_Create_Atom("public")
	move_ret ref[1257]
	c_ret

;; TERM 1258: '*default*'
c_code local build_ref_1258
	ret_reg &ref[1258]
	call_c   Dyam_Create_Atom("*default*")
	move_ret ref[1258]
	c_ret

;; TERM 173: tag_node{id=> _H, label=> _B, children=> [], kind=> subst, adj=> no, spine=> _I, top=> _E, bot=> _F, token=> _J, adjleft=> _K, adjright=> _L, adjwrap=> _M}
c_code local build_ref_173
	ret_reg &ref[173]
	call_c   build_ref_1259()
	call_c   build_ref_284()
	call_c   build_ref_397()
	call_c   Dyam_Term_Start(&ref[1259],12)
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(&ref[284])
	call_c   Dyam_Term_Arg(&ref[397])
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_End()
	move_ret ref[173]
	c_ret

;; TERM 397: no
c_code local build_ref_397
	ret_reg &ref[397]
	call_c   Dyam_Create_Atom("no")
	move_ret ref[397]
	c_ret

;; TERM 284: subst
c_code local build_ref_284
	ret_reg &ref[284]
	call_c   Dyam_Create_Atom("subst")
	move_ret ref[284]
	c_ret

;; TERM 1259: tag_node!'$ft'
c_code local build_ref_1259
	ret_reg &ref[1259]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1272()
	call_c   Dyam_Create_List(&ref[1272],I(0))
	move_ret R(0)
	call_c   build_ref_1271()
	call_c   Dyam_Create_List(&ref[1271],R(0))
	move_ret R(0)
	call_c   build_ref_1270()
	call_c   Dyam_Create_List(&ref[1270],R(0))
	move_ret R(0)
	call_c   build_ref_1269()
	call_c   Dyam_Create_List(&ref[1269],R(0))
	move_ret R(0)
	call_c   build_ref_1268()
	call_c   Dyam_Create_List(&ref[1268],R(0))
	move_ret R(0)
	call_c   build_ref_1267()
	call_c   Dyam_Create_List(&ref[1267],R(0))
	move_ret R(0)
	call_c   build_ref_1266()
	call_c   Dyam_Create_List(&ref[1266],R(0))
	move_ret R(0)
	call_c   build_ref_1265()
	call_c   Dyam_Create_List(&ref[1265],R(0))
	move_ret R(0)
	call_c   build_ref_1264()
	call_c   Dyam_Create_List(&ref[1264],R(0))
	move_ret R(0)
	call_c   build_ref_1263()
	call_c   Dyam_Create_List(&ref[1263],R(0))
	move_ret R(0)
	call_c   build_ref_1262()
	call_c   Dyam_Create_List(&ref[1262],R(0))
	move_ret R(0)
	call_c   build_ref_1261()
	call_c   Dyam_Create_List(&ref[1261],R(0))
	move_ret R(0)
	call_c   build_ref_1260()
	call_c   Dyam_Create_Atom_Module("$ft",&ref[1260])
	move_ret ref[1259]
	call_c   DYAM_Feature_2(&ref[1260],R(0))
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1260: tag_node
c_code local build_ref_1260
	ret_reg &ref[1260]
	call_c   Dyam_Create_Atom("tag_node")
	move_ret ref[1260]
	c_ret

;; TERM 1261: id
c_code local build_ref_1261
	ret_reg &ref[1261]
	call_c   Dyam_Create_Atom("id")
	move_ret ref[1261]
	c_ret

;; TERM 1262: label
c_code local build_ref_1262
	ret_reg &ref[1262]
	call_c   Dyam_Create_Atom("label")
	move_ret ref[1262]
	c_ret

;; TERM 1263: children
c_code local build_ref_1263
	ret_reg &ref[1263]
	call_c   Dyam_Create_Atom("children")
	move_ret ref[1263]
	c_ret

;; TERM 1264: kind
c_code local build_ref_1264
	ret_reg &ref[1264]
	call_c   Dyam_Create_Atom("kind")
	move_ret ref[1264]
	c_ret

;; TERM 1265: adj
c_code local build_ref_1265
	ret_reg &ref[1265]
	call_c   Dyam_Create_Atom("adj")
	move_ret ref[1265]
	c_ret

;; TERM 1266: spine
c_code local build_ref_1266
	ret_reg &ref[1266]
	call_c   Dyam_Create_Atom("spine")
	move_ret ref[1266]
	c_ret

;; TERM 1267: top
c_code local build_ref_1267
	ret_reg &ref[1267]
	call_c   Dyam_Create_Atom("top")
	move_ret ref[1267]
	c_ret

;; TERM 1268: bot
c_code local build_ref_1268
	ret_reg &ref[1268]
	call_c   Dyam_Create_Atom("bot")
	move_ret ref[1268]
	c_ret

;; TERM 1269: token
c_code local build_ref_1269
	ret_reg &ref[1269]
	call_c   Dyam_Create_Atom("token")
	move_ret ref[1269]
	c_ret

;; TERM 1270: adjleft
c_code local build_ref_1270
	ret_reg &ref[1270]
	call_c   Dyam_Create_Atom("adjleft")
	move_ret ref[1270]
	c_ret

;; TERM 1271: adjright
c_code local build_ref_1271
	ret_reg &ref[1271]
	call_c   Dyam_Create_Atom("adjright")
	move_ret ref[1271]
	c_ret

;; TERM 1272: adjwrap
c_code local build_ref_1272
	ret_reg &ref[1272]
	call_c   Dyam_Create_Atom("adjwrap")
	move_ret ref[1272]
	c_ret

;; TERM 174: [_G,_N,_O,_P]
c_code local build_ref_174
	ret_reg &ref[174]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Tupple(13,15,I(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(6),R(0))
	move_ret ref[174]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 207: '$protect'(_G)
c_code local build_ref_207
	ret_reg &ref[207]
	call_c   build_ref_1273()
	call_c   Dyam_Create_Unary(&ref[1273],V(6))
	move_ret ref[207]
	c_ret

;; TERM 1273: '$protect'
c_code local build_ref_1273
	ret_reg &ref[1273]
	call_c   Dyam_Create_Atom("$protect")
	move_ret ref[1273]
	c_ret

;; TERM 208: protect(prolog)
c_code local build_ref_208
	ret_reg &ref[208]
	call_c   build_ref_1239()
	call_c   build_ref_175()
	call_c   Dyam_Create_Unary(&ref[1239],&ref[175])
	move_ret ref[208]
	c_ret

;; TERM 175: prolog
c_code local build_ref_175
	ret_reg &ref[175]
	call_c   Dyam_Create_Atom("prolog")
	move_ret ref[175]
	c_ret

;; TERM 176: '$tagcall'(_S, _N, _O, _P)
c_code local build_ref_176
	ret_reg &ref[176]
	call_c   build_ref_1274()
	call_c   Dyam_Term_Start(&ref[1274],4)
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_End()
	move_ret ref[176]
	c_ret

;; TERM 1274: '$tagcall'
c_code local build_ref_1274
	ret_reg &ref[1274]
	call_c   Dyam_Create_Atom("$tagcall")
	move_ret ref[1274]
	c_ret

c_code local build_seed_77
	ret_reg &seed[77]
	call_c   build_ref_48()
	call_c   build_ref_178()
	call_c   Dyam_Seed_Start(&ref[48],&ref[178],I(0),fun3,1)
	call_c   build_ref_179()
	call_c   Dyam_Seed_Add_Comp(&ref[179],&ref[178],0)
	call_c   Dyam_Seed_End()
	move_ret seed[77]
	c_ret

;; TERM 179: '*GUARD*'(tag_compile_subtree(_Q, _S, _N, _O, '*PROLOG-LAST*', _D, _T, _P, _U)) :> '$$HOLE$$'
c_code local build_ref_179
	ret_reg &ref[179]
	call_c   build_ref_178()
	call_c   Dyam_Create_Binary(I(9),&ref[178],I(7))
	move_ret ref[179]
	c_ret

;; TERM 178: '*GUARD*'(tag_compile_subtree(_Q, _S, _N, _O, '*PROLOG-LAST*', _D, _T, _P, _U))
c_code local build_ref_178
	ret_reg &ref[178]
	call_c   build_ref_48()
	call_c   build_ref_177()
	call_c   Dyam_Create_Unary(&ref[48],&ref[177])
	move_ret ref[178]
	c_ret

;; TERM 177: tag_compile_subtree(_Q, _S, _N, _O, '*PROLOG-LAST*', _D, _T, _P, _U)
c_code local build_ref_177
	ret_reg &ref[177]
	call_c   build_ref_1275()
	call_c   build_ref_584()
	call_c   Dyam_Term_Start(&ref[1275],9)
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(&ref[584])
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_End()
	move_ret ref[177]
	c_ret

;; TERM 584: '*PROLOG-LAST*'
c_code local build_ref_584
	ret_reg &ref[584]
	call_c   Dyam_Create_Atom("*PROLOG-LAST*")
	move_ret ref[584]
	c_ret

;; TERM 1275: tag_compile_subtree
c_code local build_ref_1275
	ret_reg &ref[1275]
	call_c   Dyam_Create_Atom("tag_compile_subtree")
	move_ret ref[1275]
	c_ret

long local pool_fun57[3]=[2,build_ref_176,build_seed_77]

long local pool_fun58[4]=[65538,build_ref_207,build_ref_208,pool_fun57]

pl_code local fun58
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(18),&ref[207])
	fail_ret
	call_c   Dyam_Unify(V(19),&ref[208])
	fail_ret
fun57:
	call_c   Dyam_Unify(V(2),&ref[176])
	fail_ret
	call_c   Dyam_Reg_Load(0,V(2))
	move     V(20), R(2)
	move     S(5), R(3)
	pl_call  pred_tupple_2()
	call_c   Dyam_Deallocate()
	pl_jump  fun28(&seed[77],1)


;; TERM 206: '$answers'(_G)
c_code local build_ref_206
	ret_reg &ref[206]
	call_c   build_ref_1276()
	call_c   Dyam_Create_Unary(&ref[1276],V(6))
	move_ret ref[206]
	c_ret

;; TERM 1276: '$answers'
c_code local build_ref_1276
	ret_reg &ref[1276]
	call_c   Dyam_Create_Atom("$answers")
	move_ret ref[1276]
	c_ret

long local pool_fun59[5]=[131074,build_ref_206,build_ref_175,pool_fun58,pool_fun57]

pl_code local fun59
	call_c   Dyam_Update_Choice(fun58)
	call_c   Dyam_Unify(V(18),&ref[206])
	fail_ret
	call_c   Dyam_Unify(V(19),&ref[175])
	fail_ret
	pl_jump  fun57()

long local pool_fun60[6]=[131075,build_ref_173,build_ref_174,build_ref_175,pool_fun59,pool_fun57]

pl_code local fun61
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(1),V(4))
	fail_ret
	call_c   Dyam_Unify(V(1),V(5))
	fail_ret
fun60:
	call_c   Dyam_Unify(V(6),&ref[173])
	fail_ret
	move     &ref[174], R(0)
	move     S(5), R(1)
	move     V(16), R(2)
	move     S(5), R(3)
	move     V(17), R(4)
	move     S(5), R(5)
	pl_call  pred_tag_numbervars_3()
	call_c   Dyam_Choice(fun59)
	call_c   Dyam_Unify(V(18),V(6))
	fail_ret
	call_c   Dyam_Unify(V(19),&ref[175])
	fail_ret
	pl_jump  fun57()


;; TERM 172: tag_features(_B, _E, _F)
c_code local build_ref_172
	ret_reg &ref[172]
	call_c   build_ref_1277()
	call_c   Dyam_Term_Start(&ref[1277],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[172]
	c_ret

;; TERM 1277: tag_features
c_code local build_ref_1277
	ret_reg &ref[1277]
	call_c   Dyam_Create_Atom("tag_features")
	move_ret ref[1277]
	c_ret

long local pool_fun62[4]=[131073,build_ref_172,pool_fun60,pool_fun60]

long local pool_fun63[3]=[65537,build_ref_209,pool_fun62]

pl_code local fun63
	call_c   Dyam_Remove_Choice()
	pl_call  Object_1(&ref[209])
fun62:
	call_c   Dyam_Choice(fun61)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[172])
	call_c   Dyam_Cut()
	pl_jump  fun60()


;; TERM 171: public(tag(_B))
c_code local build_ref_171
	ret_reg &ref[171]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1255()
	call_c   Dyam_Create_Unary(&ref[1255],V(1))
	move_ret R(0)
	call_c   build_ref_1257()
	call_c   Dyam_Create_Unary(&ref[1257],R(0))
	move_ret ref[171]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun64[5]=[131074,build_ref_169,build_ref_171,pool_fun63,pool_fun62]

pl_code local fun64
	call_c   Dyam_Pool(pool_fun64)
	call_c   Dyam_Unify_Item(&ref[169])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun63)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[171])
	call_c   Dyam_Cut()
	pl_jump  fun62()

;; TERM 170: '*GUARD*'(pgm_to_lpda(register_predicate(tag(_B)), ('*PROLOG-FIRST*'(_C) :> _D))) :> []
c_code local build_ref_170
	ret_reg &ref[170]
	call_c   build_ref_169()
	call_c   Dyam_Create_Binary(I(9),&ref[169],I(0))
	move_ret ref[170]
	c_ret

c_code local build_seed_53
	ret_reg &seed[53]
	call_c   build_ref_47()
	call_c   build_ref_182()
	call_c   Dyam_Seed_Start(&ref[47],&ref[182],I(0),fun1,1)
	call_c   build_ref_181()
	call_c   Dyam_Seed_Add_Comp(&ref[181],fun49,0)
	call_c   Dyam_Seed_End()
	move_ret seed[53]
	c_ret

;; TERM 181: '*GUARD*'(tag_compile_subtree_handler(_B, '$skip', _C, _D, _E, _F, _G, _H, _I))
c_code local build_ref_181
	ret_reg &ref[181]
	call_c   build_ref_48()
	call_c   build_ref_180()
	call_c   Dyam_Create_Unary(&ref[48],&ref[180])
	move_ret ref[181]
	c_ret

;; TERM 180: tag_compile_subtree_handler(_B, '$skip', _C, _D, _E, _F, _G, _H, _I)
c_code local build_ref_180
	ret_reg &ref[180]
	call_c   build_ref_1278()
	call_c   build_ref_1279()
	call_c   Dyam_Term_Start(&ref[1278],9)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(&ref[1279])
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_End()
	move_ret ref[180]
	c_ret

;; TERM 1279: '$skip'
c_code local build_ref_1279
	ret_reg &ref[1279]
	call_c   Dyam_Create_Atom("$skip")
	move_ret ref[1279]
	c_ret

;; TERM 1278: tag_compile_subtree_handler
c_code local build_ref_1278
	ret_reg &ref[1278]
	call_c   Dyam_Create_Atom("tag_compile_subtree_handler")
	move_ret ref[1278]
	c_ret

c_code local build_seed_78
	ret_reg &seed[78]
	call_c   build_ref_48()
	call_c   build_ref_184()
	call_c   Dyam_Seed_Start(&ref[48],&ref[184],I(0),fun3,1)
	call_c   build_ref_185()
	call_c   Dyam_Seed_Add_Comp(&ref[185],&ref[184],0)
	call_c   Dyam_Seed_End()
	move_ret seed[78]
	c_ret

;; TERM 185: '*GUARD*'(tag_wrap_skipper(_B, _C, _D, _D, true, _E, _F, _G)) :> '$$HOLE$$'
c_code local build_ref_185
	ret_reg &ref[185]
	call_c   build_ref_184()
	call_c   Dyam_Create_Binary(I(9),&ref[184],I(7))
	move_ret ref[185]
	c_ret

;; TERM 184: '*GUARD*'(tag_wrap_skipper(_B, _C, _D, _D, true, _E, _F, _G))
c_code local build_ref_184
	ret_reg &ref[184]
	call_c   build_ref_48()
	call_c   build_ref_183()
	call_c   Dyam_Create_Unary(&ref[48],&ref[183])
	move_ret ref[184]
	c_ret

;; TERM 183: tag_wrap_skipper(_B, _C, _D, _D, true, _E, _F, _G)
c_code local build_ref_183
	ret_reg &ref[183]
	call_c   build_ref_1280()
	call_c   build_ref_261()
	call_c   Dyam_Term_Start(&ref[1280],8)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(&ref[261])
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[183]
	c_ret

;; TERM 1280: tag_wrap_skipper
c_code local build_ref_1280
	ret_reg &ref[1280]
	call_c   Dyam_Create_Atom("tag_wrap_skipper")
	move_ret ref[1280]
	c_ret

long local pool_fun49[3]=[2,build_ref_181,build_seed_78]

pl_code local fun49
	call_c   Dyam_Pool(pool_fun49)
	call_c   Dyam_Unify_Item(&ref[181])
	fail_ret
	pl_jump  fun28(&seed[78],1)

;; TERM 182: '*GUARD*'(tag_compile_subtree_handler(_B, '$skip', _C, _D, _E, _F, _G, _H, _I)) :> []
c_code local build_ref_182
	ret_reg &ref[182]
	call_c   build_ref_181()
	call_c   Dyam_Create_Binary(I(9),&ref[181],I(0))
	move_ret ref[182]
	c_ret

c_code local build_seed_21
	ret_reg &seed[21]
	call_c   build_ref_47()
	call_c   build_ref_212()
	call_c   Dyam_Seed_Start(&ref[47],&ref[212],I(0),fun1,1)
	call_c   build_ref_211()
	call_c   Dyam_Seed_Add_Comp(&ref[211],fun65,0)
	call_c   Dyam_Seed_End()
	move_ret seed[21]
	c_ret

;; TERM 211: '*GUARD*'(il_combine_tupples_aux(_B, [_C|_D], _E))
c_code local build_ref_211
	ret_reg &ref[211]
	call_c   build_ref_48()
	call_c   build_ref_210()
	call_c   Dyam_Create_Unary(&ref[48],&ref[210])
	move_ret ref[211]
	c_ret

;; TERM 210: il_combine_tupples_aux(_B, [_C|_D], _E)
c_code local build_ref_210
	ret_reg &ref[210]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(2),V(3))
	move_ret R(0)
	call_c   build_ref_1235()
	call_c   Dyam_Term_Start(&ref[1235],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[210]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_82
	ret_reg &seed[82]
	call_c   build_ref_48()
	call_c   build_ref_214()
	call_c   Dyam_Seed_Start(&ref[48],&ref[214],I(0),fun3,1)
	call_c   build_ref_215()
	call_c   Dyam_Seed_Add_Comp(&ref[215],&ref[214],0)
	call_c   Dyam_Seed_End()
	move_ret seed[82]
	c_ret

;; TERM 215: '*GUARD*'(il_combine_tupples_aux(_B, _D, _F)) :> '$$HOLE$$'
c_code local build_ref_215
	ret_reg &ref[215]
	call_c   build_ref_214()
	call_c   Dyam_Create_Binary(I(9),&ref[214],I(7))
	move_ret ref[215]
	c_ret

;; TERM 214: '*GUARD*'(il_combine_tupples_aux(_B, _D, _F))
c_code local build_ref_214
	ret_reg &ref[214]
	call_c   build_ref_48()
	call_c   build_ref_213()
	call_c   Dyam_Create_Unary(&ref[48],&ref[213])
	move_ret ref[214]
	c_ret

;; TERM 213: il_combine_tupples_aux(_B, _D, _F)
c_code local build_ref_213
	ret_reg &ref[213]
	call_c   build_ref_1235()
	call_c   Dyam_Term_Start(&ref[1235],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[213]
	c_ret

long local pool_fun65[3]=[2,build_ref_211,build_seed_82]

pl_code local fun65
	call_c   Dyam_Pool(pool_fun65)
	call_c   Dyam_Unify_Item(&ref[211])
	fail_ret
	call_c   Dyam_Allocate(0)
	pl_call  fun28(&seed[82],1)
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(2))
	move     V(6), R(4)
	move     S(5), R(5)
	pl_call  pred_inter_3()
	call_c   Dyam_Reg_Load(0,V(6))
	call_c   Dyam_Reg_Load(2,V(5))
	call_c   Dyam_Reg_Load(4,V(4))
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  pred_union_3()

;; TERM 212: '*GUARD*'(il_combine_tupples_aux(_B, [_C|_D], _E)) :> []
c_code local build_ref_212
	ret_reg &ref[212]
	call_c   build_ref_211()
	call_c   Dyam_Create_Binary(I(9),&ref[211],I(0))
	move_ret ref[212]
	c_ret

c_code local build_seed_51
	ret_reg &seed[51]
	call_c   build_ref_47()
	call_c   build_ref_218()
	call_c   Dyam_Seed_Start(&ref[47],&ref[218],I(0),fun1,1)
	call_c   build_ref_217()
	call_c   Dyam_Seed_Add_Comp(&ref[217],fun66,0)
	call_c   Dyam_Seed_End()
	move_ret seed[51]
	c_ret

;; TERM 217: '*GUARD*'(tag_compile_subtree_handler(_B, '$protect'(_C), _D, _E, _F, _G, _H, _I, _J))
c_code local build_ref_217
	ret_reg &ref[217]
	call_c   build_ref_48()
	call_c   build_ref_216()
	call_c   Dyam_Create_Unary(&ref[48],&ref[216])
	move_ret ref[217]
	c_ret

;; TERM 216: tag_compile_subtree_handler(_B, '$protect'(_C), _D, _E, _F, _G, _H, _I, _J)
c_code local build_ref_216
	ret_reg &ref[216]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1273()
	call_c   Dyam_Create_Unary(&ref[1273],V(2))
	move_ret R(0)
	call_c   build_ref_1278()
	call_c   Dyam_Term_Start(&ref[1278],9)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret ref[216]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_83
	ret_reg &seed[83]
	call_c   build_ref_48()
	call_c   build_ref_220()
	call_c   Dyam_Seed_Start(&ref[48],&ref[220],I(0),fun3,1)
	call_c   build_ref_221()
	call_c   Dyam_Seed_Add_Comp(&ref[221],&ref[220],0)
	call_c   Dyam_Seed_End()
	move_ret seed[83]
	c_ret

;; TERM 221: '*GUARD*'(tag_compile_subtree(_B, _C, _D, _E, _F, _G, protect(_H), _I, _J)) :> '$$HOLE$$'
c_code local build_ref_221
	ret_reg &ref[221]
	call_c   build_ref_220()
	call_c   Dyam_Create_Binary(I(9),&ref[220],I(7))
	move_ret ref[221]
	c_ret

;; TERM 220: '*GUARD*'(tag_compile_subtree(_B, _C, _D, _E, _F, _G, protect(_H), _I, _J))
c_code local build_ref_220
	ret_reg &ref[220]
	call_c   build_ref_48()
	call_c   build_ref_219()
	call_c   Dyam_Create_Unary(&ref[48],&ref[219])
	move_ret ref[220]
	c_ret

;; TERM 219: tag_compile_subtree(_B, _C, _D, _E, _F, _G, protect(_H), _I, _J)
c_code local build_ref_219
	ret_reg &ref[219]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1239()
	call_c   Dyam_Create_Unary(&ref[1239],V(7))
	move_ret R(0)
	call_c   build_ref_1275()
	call_c   Dyam_Term_Start(&ref[1275],9)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret ref[219]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun66[3]=[2,build_ref_217,build_seed_83]

pl_code local fun66
	call_c   Dyam_Pool(pool_fun66)
	call_c   Dyam_Unify_Item(&ref[217])
	fail_ret
	pl_jump  fun28(&seed[83],1)

;; TERM 218: '*GUARD*'(tag_compile_subtree_handler(_B, '$protect'(_C), _D, _E, _F, _G, _H, _I, _J)) :> []
c_code local build_ref_218
	ret_reg &ref[218]
	call_c   build_ref_217()
	call_c   Dyam_Create_Binary(I(9),&ref[217],I(0))
	move_ret ref[218]
	c_ret

c_code local build_seed_52
	ret_reg &seed[52]
	call_c   build_ref_47()
	call_c   build_ref_224()
	call_c   Dyam_Seed_Start(&ref[47],&ref[224],I(0),fun1,1)
	call_c   build_ref_223()
	call_c   Dyam_Seed_Add_Comp(&ref[223],fun67,0)
	call_c   Dyam_Seed_End()
	move_ret seed[52]
	c_ret

;; TERM 223: '*GUARD*'(tag_compile_subtree_handler(_B, '$answers'(_C), _D, _E, _F, _G, _H, _I, _J))
c_code local build_ref_223
	ret_reg &ref[223]
	call_c   build_ref_48()
	call_c   build_ref_222()
	call_c   Dyam_Create_Unary(&ref[48],&ref[222])
	move_ret ref[223]
	c_ret

;; TERM 222: tag_compile_subtree_handler(_B, '$answers'(_C), _D, _E, _F, _G, _H, _I, _J)
c_code local build_ref_222
	ret_reg &ref[222]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1276()
	call_c   Dyam_Create_Unary(&ref[1276],V(2))
	move_ret R(0)
	call_c   build_ref_1278()
	call_c   Dyam_Term_Start(&ref[1278],9)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret ref[222]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_84
	ret_reg &seed[84]
	call_c   build_ref_48()
	call_c   build_ref_226()
	call_c   Dyam_Seed_Start(&ref[48],&ref[226],I(0),fun3,1)
	call_c   build_ref_227()
	call_c   Dyam_Seed_Add_Comp(&ref[227],&ref[226],0)
	call_c   Dyam_Seed_End()
	move_ret seed[84]
	c_ret

;; TERM 227: '*GUARD*'(tag_compile_subtree(_B, _C, _D, _E, _F, _G, answer(_H), _I, _J)) :> '$$HOLE$$'
c_code local build_ref_227
	ret_reg &ref[227]
	call_c   build_ref_226()
	call_c   Dyam_Create_Binary(I(9),&ref[226],I(7))
	move_ret ref[227]
	c_ret

;; TERM 226: '*GUARD*'(tag_compile_subtree(_B, _C, _D, _E, _F, _G, answer(_H), _I, _J))
c_code local build_ref_226
	ret_reg &ref[226]
	call_c   build_ref_48()
	call_c   build_ref_225()
	call_c   Dyam_Create_Unary(&ref[48],&ref[225])
	move_ret ref[226]
	c_ret

;; TERM 225: tag_compile_subtree(_B, _C, _D, _E, _F, _G, answer(_H), _I, _J)
c_code local build_ref_225
	ret_reg &ref[225]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1281()
	call_c   Dyam_Create_Unary(&ref[1281],V(7))
	move_ret R(0)
	call_c   build_ref_1275()
	call_c   Dyam_Term_Start(&ref[1275],9)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret ref[225]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1281: answer
c_code local build_ref_1281
	ret_reg &ref[1281]
	call_c   Dyam_Create_Atom("answer")
	move_ret ref[1281]
	c_ret

long local pool_fun67[3]=[2,build_ref_223,build_seed_84]

pl_code local fun67
	call_c   Dyam_Pool(pool_fun67)
	call_c   Dyam_Unify_Item(&ref[223])
	fail_ret
	pl_jump  fun28(&seed[84],1)

;; TERM 224: '*GUARD*'(tag_compile_subtree_handler(_B, '$answers'(_C), _D, _E, _F, _G, _H, _I, _J)) :> []
c_code local build_ref_224
	ret_reg &ref[224]
	call_c   build_ref_223()
	call_c   Dyam_Create_Binary(I(9),&ref[223],I(0))
	move_ret ref[224]
	c_ret

c_code local build_seed_7
	ret_reg &seed[7]
	call_c   build_ref_47()
	call_c   build_ref_193()
	call_c   Dyam_Seed_Start(&ref[47],&ref[193],I(0),fun1,1)
	call_c   build_ref_192()
	call_c   Dyam_Seed_Add_Comp(&ref[192],fun48,0)
	call_c   Dyam_Seed_End()
	move_ret seed[7]
	c_ret

;; TERM 192: '*GUARD*'(tag_compile_subtree_handler(_B, '$pos'(_C), _D, _E, _F, (unify(_C, _D) :> unify(_D, _E) :> _F), _G, _H, _I))
c_code local build_ref_192
	ret_reg &ref[192]
	call_c   build_ref_48()
	call_c   build_ref_191()
	call_c   Dyam_Create_Unary(&ref[48],&ref[191])
	move_ret ref[192]
	c_ret

;; TERM 191: tag_compile_subtree_handler(_B, '$pos'(_C), _D, _E, _F, (unify(_C, _D) :> unify(_D, _E) :> _F), _G, _H, _I)
c_code local build_ref_191
	ret_reg &ref[191]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_1282()
	call_c   Dyam_Create_Unary(&ref[1282],V(2))
	move_ret R(0)
	call_c   build_ref_1283()
	call_c   Dyam_Create_Binary(&ref[1283],V(3),V(4))
	move_ret R(1)
	call_c   Dyam_Create_Binary(I(9),R(1),V(5))
	move_ret R(1)
	call_c   build_ref_749()
	call_c   Dyam_Create_Binary(I(9),&ref[749],R(1))
	move_ret R(1)
	call_c   build_ref_1278()
	call_c   Dyam_Term_Start(&ref[1278],9)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_End()
	move_ret ref[191]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 749: unify(_C, _D)
c_code local build_ref_749
	ret_reg &ref[749]
	call_c   build_ref_1283()
	call_c   Dyam_Create_Binary(&ref[1283],V(2),V(3))
	move_ret ref[749]
	c_ret

;; TERM 1283: unify
c_code local build_ref_1283
	ret_reg &ref[1283]
	call_c   Dyam_Create_Atom("unify")
	move_ret ref[1283]
	c_ret

;; TERM 1282: '$pos'
c_code local build_ref_1282
	ret_reg &ref[1282]
	call_c   Dyam_Create_Atom("$pos")
	move_ret ref[1282]
	c_ret

pl_code local fun48
	call_c   build_ref_192()
	call_c   Dyam_Unify_Item(&ref[192])
	fail_ret
	pl_ret

;; TERM 193: '*GUARD*'(tag_compile_subtree_handler(_B, '$pos'(_C), _D, _E, _F, (unify(_C, _D) :> unify(_D, _E) :> _F), _G, _H, _I)) :> []
c_code local build_ref_193
	ret_reg &ref[193]
	call_c   build_ref_192()
	call_c   Dyam_Create_Binary(I(9),&ref[192],I(0))
	move_ret ref[193]
	c_ret

c_code local build_seed_60
	ret_reg &seed[60]
	call_c   build_ref_47()
	call_c   build_ref_246()
	call_c   Dyam_Seed_Start(&ref[47],&ref[246],I(0),fun1,1)
	call_c   build_ref_245()
	call_c   Dyam_Seed_Add_Comp(&ref[245],fun82,0)
	call_c   Dyam_Seed_End()
	move_ret seed[60]
	c_ret

;; TERM 245: '*GUARD*'(pgm_to_lpda(tag_tree{family=> _B, name=> _C, tree=> _D}, _E))
c_code local build_ref_245
	ret_reg &ref[245]
	call_c   build_ref_48()
	call_c   build_ref_244()
	call_c   Dyam_Create_Unary(&ref[48],&ref[244])
	move_ret ref[245]
	c_ret

;; TERM 244: pgm_to_lpda(tag_tree{family=> _B, name=> _C, tree=> _D}, _E)
c_code local build_ref_244
	ret_reg &ref[244]
	call_c   build_ref_1253()
	call_c   build_ref_268()
	call_c   Dyam_Create_Binary(&ref[1253],&ref[268],V(4))
	move_ret ref[244]
	c_ret

;; TERM 268: tag_tree{family=> _B, name=> _C, tree=> _D}
c_code local build_ref_268
	ret_reg &ref[268]
	call_c   build_ref_1284()
	call_c   Dyam_Term_Start(&ref[1284],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[268]
	c_ret

;; TERM 1284: tag_tree!'$ft'
c_code local build_ref_1284
	ret_reg &ref[1284]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1287()
	call_c   Dyam_Create_List(&ref[1287],I(0))
	move_ret R(0)
	call_c   build_ref_1286()
	call_c   Dyam_Create_List(&ref[1286],R(0))
	move_ret R(0)
	call_c   build_ref_1243()
	call_c   Dyam_Create_List(&ref[1243],R(0))
	move_ret R(0)
	call_c   build_ref_1285()
	call_c   Dyam_Create_Atom_Module("$ft",&ref[1285])
	move_ret ref[1284]
	call_c   DYAM_Feature_2(&ref[1285],R(0))
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1285: tag_tree
c_code local build_ref_1285
	ret_reg &ref[1285]
	call_c   Dyam_Create_Atom("tag_tree")
	move_ret ref[1285]
	c_ret

;; TERM 1286: name
c_code local build_ref_1286
	ret_reg &ref[1286]
	call_c   Dyam_Create_Atom("name")
	move_ret ref[1286]
	c_ret

;; TERM 1287: tree
c_code local build_ref_1287
	ret_reg &ref[1287]
	call_c   Dyam_Create_Atom("tree")
	move_ret ref[1287]
	c_ret

;; TERM 267: '$CLOSURE'('$fun'(81, 0, 1127940288), '$TUPPLE'(35075411939352))
c_code local build_ref_267
	ret_reg &ref[267]
	call_c   build_ref_266()
	call_c   Dyam_Closure_Aux(fun81,&ref[266])
	move_ret ref[267]
	c_ret

;; TERM 255: [_K,_L]
c_code local build_ref_255
	ret_reg &ref[255]
	call_c   Dyam_Create_Tupple(10,11,I(0))
	move_ret ref[255]
	c_ret

c_code local build_seed_91
	ret_reg &seed[91]
	call_c   build_ref_48()
	call_c   build_ref_257()
	call_c   Dyam_Seed_Start(&ref[48],&ref[257],I(0),fun3,1)
	call_c   build_ref_258()
	call_c   Dyam_Seed_Add_Comp(&ref[258],&ref[257],0)
	call_c   Dyam_Seed_End()
	move_ret seed[91]
	c_ret

;; TERM 258: '*GUARD*'(tag_compile_tree(_C, _F, _N, dyalog, tag_anchor{family=> _B, coanchors=> _G, equations=> _H}, [_K,_L])) :> '$$HOLE$$'
c_code local build_ref_258
	ret_reg &ref[258]
	call_c   build_ref_257()
	call_c   Dyam_Create_Binary(I(9),&ref[257],I(7))
	move_ret ref[258]
	c_ret

;; TERM 257: '*GUARD*'(tag_compile_tree(_C, _F, _N, dyalog, tag_anchor{family=> _B, coanchors=> _G, equations=> _H}, [_K,_L]))
c_code local build_ref_257
	ret_reg &ref[257]
	call_c   build_ref_48()
	call_c   build_ref_256()
	call_c   Dyam_Create_Unary(&ref[48],&ref[256])
	move_ret ref[257]
	c_ret

;; TERM 256: tag_compile_tree(_C, _F, _N, dyalog, tag_anchor{family=> _B, coanchors=> _G, equations=> _H}, [_K,_L])
c_code local build_ref_256
	ret_reg &ref[256]
	call_c   build_ref_1288()
	call_c   build_ref_1289()
	call_c   build_ref_254()
	call_c   build_ref_255()
	call_c   Dyam_Term_Start(&ref[1288],6)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(&ref[1289])
	call_c   Dyam_Term_Arg(&ref[254])
	call_c   Dyam_Term_Arg(&ref[255])
	call_c   Dyam_Term_End()
	move_ret ref[256]
	c_ret

;; TERM 254: tag_anchor{family=> _B, coanchors=> _G, equations=> _H}
c_code local build_ref_254
	ret_reg &ref[254]
	call_c   build_ref_1241()
	call_c   Dyam_Term_Start(&ref[1241],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[254]
	c_ret

;; TERM 1289: dyalog
c_code local build_ref_1289
	ret_reg &ref[1289]
	call_c   Dyam_Create_Atom("dyalog")
	move_ret ref[1289]
	c_ret

;; TERM 1288: tag_compile_tree
c_code local build_ref_1288
	ret_reg &ref[1288]
	call_c   Dyam_Create_Atom("tag_compile_tree")
	move_ret ref[1288]
	c_ret

pl_code local fun78
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(4),V(13))
	fail_ret
fun75:
	call_c   Dyam_Reg_Load(0,V(2))
	call_c   Dyam_Reg_Deallocate(1)
	pl_jump  pred_numbervars_delete_1()


;; TERM 259: autoload
c_code local build_ref_259
	ret_reg &ref[259]
	call_c   Dyam_Create_Atom("autoload")
	move_ret ref[259]
	c_ret

;; TERM 260: parse_mode(token)
c_code local build_ref_260
	ret_reg &ref[260]
	call_c   build_ref_1290()
	call_c   build_ref_1269()
	call_c   Dyam_Create_Unary(&ref[1290],&ref[1269])
	move_ret ref[260]
	c_ret

;; TERM 1290: parse_mode
c_code local build_ref_1290
	ret_reg &ref[1290]
	call_c   Dyam_Create_Atom("parse_mode")
	move_ret ref[1290]
	c_ret

c_code local build_seed_92
	ret_reg &seed[92]
	call_c   build_ref_48()
	call_c   build_ref_263()
	call_c   Dyam_Seed_Start(&ref[48],&ref[263],I(0),fun3,1)
	call_c   build_ref_264()
	call_c   Dyam_Seed_Add_Comp(&ref[264],&ref[263],0)
	call_c   Dyam_Seed_End()
	move_ret seed[92]
	c_ret

;; TERM 264: '*GUARD*'(build_cond_loader(_C, (_P -> true ; fail), _N, _E)) :> '$$HOLE$$'
c_code local build_ref_264
	ret_reg &ref[264]
	call_c   build_ref_263()
	call_c   Dyam_Create_Binary(I(9),&ref[263],I(7))
	move_ret ref[264]
	c_ret

;; TERM 263: '*GUARD*'(build_cond_loader(_C, (_P -> true ; fail), _N, _E))
c_code local build_ref_263
	ret_reg &ref[263]
	call_c   build_ref_48()
	call_c   build_ref_262()
	call_c   Dyam_Create_Unary(&ref[48],&ref[262])
	move_ret ref[263]
	c_ret

;; TERM 262: build_cond_loader(_C, (_P -> true ; fail), _N, _E)
c_code local build_ref_262
	ret_reg &ref[262]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1292()
	call_c   build_ref_261()
	call_c   Dyam_Create_Binary(&ref[1292],V(15),&ref[261])
	move_ret R(0)
	call_c   build_ref_330()
	call_c   Dyam_Create_Binary(I(5),R(0),&ref[330])
	move_ret R(0)
	call_c   build_ref_1291()
	call_c   Dyam_Term_Start(&ref[1291],4)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[262]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1291: build_cond_loader
c_code local build_ref_1291
	ret_reg &ref[1291]
	call_c   Dyam_Create_Atom("build_cond_loader")
	move_ret ref[1291]
	c_ret

;; TERM 330: fail
c_code local build_ref_330
	ret_reg &ref[330]
	call_c   Dyam_Create_Atom("fail")
	move_ret ref[330]
	c_ret

;; TERM 1292: ->
c_code local build_ref_1292
	ret_reg &ref[1292]
	call_c   Dyam_Create_Atom("->")
	move_ret ref[1292]
	c_ret

long local pool_fun76[2]=[1,build_seed_92]

pl_code local fun77
	call_c   Dyam_Remove_Choice()
fun76:
	call_c   Dyam_Cut()
	pl_call  fun28(&seed[92],1)
	pl_jump  fun75()


long local pool_fun79[8]=[65542,build_ref_255,build_seed_91,build_ref_259,build_ref_260,build_ref_254,build_ref_261,pool_fun76]

pl_code local fun80
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(6),V(8))
	fail_ret
	call_c   Dyam_Unify(V(7),V(9))
	fail_ret
fun79:
	move     &ref[255], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(2))
	move     V(12), R(4)
	move     S(5), R(5)
	pl_call  pred_tag_numbervars_3()
	pl_call  fun28(&seed[91],1)
	move     &ref[254], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(2))
	move     V(14), R(4)
	move     S(5), R(5)
	pl_call  pred_tag_numbervars_3()
	call_c   Dyam_Choice(fun78)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[259])
	pl_call  Object_1(&ref[260])
	call_c   Dyam_Reg_Load(0,V(2))
	call_c   Dyam_Reg_Load(2,V(5))
	move     &ref[254], R(4)
	move     S(5), R(5)
	move     V(15), R(6)
	move     S(5), R(7)
	pl_call  pred_tag_autoloader_wrapper_4()
	call_c   Dyam_Choice(fun77)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(15),&ref[261])
	fail_ret
	call_c   Dyam_Cut()
	pl_fail


long local pool_fun81[4]=[131073,build_ref_254,pool_fun79,pool_fun79]

pl_code local fun81
	call_c   Dyam_Pool(pool_fun81)
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun80)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[254])
	call_c   Dyam_Cut()
	pl_jump  fun79()

;; TERM 266: '$TUPPLE'(35075411939352)
c_code local build_ref_266
	ret_reg &ref[266]
	call_c   Dyam_Create_Simple_Tupple(0,226492416)
	move_ret ref[266]
	c_ret

long local pool_fun82[4]=[3,build_ref_245,build_ref_267,build_ref_268]

pl_code local fun82
	call_c   Dyam_Pool(pool_fun82)
	call_c   Dyam_Unify_Item(&ref[245])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[267], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	move     &ref[268], R(4)
	move     S(5), R(5)
	move     V(5), R(6)
	move     S(5), R(7)
	call_c   Dyam_Reg_Deallocate(4)
fun74:
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Reg_Bind(2,V(4))
	call_c   Dyam_Multi_Reg_Bind(4,2,2)
	call_c   Dyam_Choice(fun73)
	pl_call  fun46(&seed[89],1)
	pl_fail


;; TERM 246: '*GUARD*'(pgm_to_lpda(tag_tree{family=> _B, name=> _C, tree=> _D}, _E)) :> []
c_code local build_ref_246
	ret_reg &ref[246]
	call_c   build_ref_245()
	call_c   Dyam_Create_Binary(I(9),&ref[245],I(0))
	move_ret ref[246]
	c_ret

c_code local build_seed_1
	ret_reg &seed[1]
	call_c   build_ref_52()
	call_c   build_ref_271()
	call_c   Dyam_Seed_Start(&ref[52],&ref[271],I(0),fun15,1)
	call_c   build_ref_272()
	call_c   Dyam_Seed_Add_Comp(&ref[272],fun83,0)
	call_c   Dyam_Seed_End()
	move_ret seed[1]
	c_ret

;; TERM 272: '*CITEM*'(tag_il_args(_B, _C, _D, _E, _F, _G, _H, [_B,_C,_D,_E,_F,_G,_H]), _A)
c_code local build_ref_272
	ret_reg &ref[272]
	call_c   build_ref_56()
	call_c   build_ref_269()
	call_c   Dyam_Create_Binary(&ref[56],&ref[269],V(0))
	move_ret ref[272]
	c_ret

;; TERM 269: tag_il_args(_B, _C, _D, _E, _F, _G, _H, [_B,_C,_D,_E,_F,_G,_H])
c_code local build_ref_269
	ret_reg &ref[269]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Tupple(1,7,I(0))
	move_ret R(0)
	call_c   build_ref_1236()
	call_c   Dyam_Term_Start(&ref[1236],8)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_End()
	move_ret ref[269]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun83[3]=[2,build_ref_272,build_seed_69]

pl_code local fun83
	call_c   Dyam_Pool(pool_fun83)
	call_c   Dyam_Unify_Item(&ref[272])
	fail_ret
	pl_jump  fun13(&seed[69],2)

;; TERM 271: '*FIRST*'(tag_il_args(_B, _C, _D, _E, _F, _G, _H, [_B,_C,_D,_E,_F,_G,_H])) :> []
c_code local build_ref_271
	ret_reg &ref[271]
	call_c   build_ref_270()
	call_c   Dyam_Create_Binary(I(9),&ref[270],I(0))
	move_ret ref[271]
	c_ret

;; TERM 270: '*FIRST*'(tag_il_args(_B, _C, _D, _E, _F, _G, _H, [_B,_C,_D,_E,_F,_G,_H]))
c_code local build_ref_270
	ret_reg &ref[270]
	call_c   build_ref_52()
	call_c   build_ref_269()
	call_c   Dyam_Create_Unary(&ref[52],&ref[269])
	move_ret ref[270]
	c_ret

c_code local build_seed_45
	ret_reg &seed[45]
	call_c   build_ref_52()
	call_c   build_ref_298()
	call_c   Dyam_Seed_Start(&ref[52],&ref[298],I(0),fun15,1)
	call_c   build_ref_299()
	call_c   Dyam_Seed_Add_Comp(&ref[299],fun109,0)
	call_c   Dyam_Seed_End()
	move_ret seed[45]
	c_ret

;; TERM 299: '*CITEM*'('call_decompose_args/3'(tag_foot((_B / _C))), _A)
c_code local build_ref_299
	ret_reg &ref[299]
	call_c   build_ref_56()
	call_c   build_ref_296()
	call_c   Dyam_Create_Binary(&ref[56],&ref[296],V(0))
	move_ret ref[299]
	c_ret

;; TERM 296: 'call_decompose_args/3'(tag_foot((_B / _C)))
c_code local build_ref_296
	ret_reg &ref[296]
	call_c   build_ref_1232()
	call_c   build_ref_374()
	call_c   Dyam_Create_Unary(&ref[1232],&ref[374])
	move_ret ref[296]
	c_ret

;; TERM 374: tag_foot((_B / _C))
c_code local build_ref_374
	ret_reg &ref[374]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1294()
	call_c   Dyam_Create_Binary(&ref[1294],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_1293()
	call_c   Dyam_Create_Unary(&ref[1293],R(0))
	move_ret ref[374]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1293: tag_foot
c_code local build_ref_1293
	ret_reg &ref[1293]
	call_c   Dyam_Create_Atom("tag_foot")
	move_ret ref[1293]
	c_ret

;; TERM 1294: /
c_code local build_ref_1294
	ret_reg &ref[1294]
	call_c   Dyam_Create_Atom("/")
	move_ret ref[1294]
	c_ret

;; TERM 236: [_H|_I]
c_code local build_ref_236
	ret_reg &ref[236]
	call_c   Dyam_Create_List(V(7),V(8))
	move_ret ref[236]
	c_ret

;; TERM 304: [_D,_E|_I]
c_code local build_ref_304
	ret_reg &ref[304]
	call_c   Dyam_Create_Tupple(3,4,V(8))
	move_ret ref[304]
	c_ret

c_code local build_seed_94
	ret_reg &seed[94]
	call_c   build_ref_0()
	call_c   build_ref_302()
	call_c   Dyam_Seed_Start(&ref[0],&ref[302],&ref[302],fun3,1)
	call_c   build_ref_303()
	call_c   Dyam_Seed_Add_Comp(&ref[303],&ref[302],0)
	call_c   Dyam_Seed_End()
	move_ret seed[94]
	c_ret

;; TERM 303: '*RITEM*'(_A, return([_D,_E,_F], _G)) :> '$$HOLE$$'
c_code local build_ref_303
	ret_reg &ref[303]
	call_c   build_ref_302()
	call_c   Dyam_Create_Binary(I(9),&ref[302],I(7))
	move_ret ref[303]
	c_ret

;; TERM 302: '*RITEM*'(_A, return([_D,_E,_F], _G))
c_code local build_ref_302
	ret_reg &ref[302]
	call_c   build_ref_0()
	call_c   build_ref_301()
	call_c   Dyam_Create_Binary(&ref[0],V(0),&ref[301])
	move_ret ref[302]
	c_ret

;; TERM 301: return([_D,_E,_F], _G)
c_code local build_ref_301
	ret_reg &ref[301]
	call_c   build_ref_1233()
	call_c   build_ref_300()
	call_c   Dyam_Create_Binary(&ref[1233],&ref[300],V(6))
	move_ret ref[301]
	c_ret

;; TERM 300: [_D,_E,_F]
c_code local build_ref_300
	ret_reg &ref[300]
	call_c   Dyam_Create_Tupple(3,5,I(0))
	move_ret ref[300]
	c_ret

long local pool_fun107[2]=[1,build_seed_94]

long local pool_fun108[4]=[65538,build_ref_236,build_ref_304,pool_fun107]

pl_code local fun108
	call_c   Dyam_Remove_Choice()
	call_c   DYAM_evpred_univ(V(5),&ref[236])
	fail_ret
	call_c   Dyam_Unify(V(6),&ref[304])
	fail_ret
fun107:
	call_c   Dyam_Deallocate()
	pl_jump  fun13(&seed[94],2)


long local pool_fun109[5]=[131074,build_ref_299,build_ref_300,pool_fun108,pool_fun107]

pl_code local fun109
	call_c   Dyam_Pool(pool_fun109)
	call_c   Dyam_Unify_Item(&ref[299])
	fail_ret
	call_c   DYAM_evpred_functor(V(5),V(1),V(2))
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun108)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_gt(V(2),N(30))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(6),&ref[300])
	fail_ret
	pl_jump  fun107()

;; TERM 298: '*FIRST*'('call_decompose_args/3'(tag_foot((_B / _C)))) :> []
c_code local build_ref_298
	ret_reg &ref[298]
	call_c   build_ref_297()
	call_c   Dyam_Create_Binary(I(9),&ref[297],I(0))
	move_ret ref[298]
	c_ret

;; TERM 297: '*FIRST*'('call_decompose_args/3'(tag_foot((_B / _C))))
c_code local build_ref_297
	ret_reg &ref[297]
	call_c   build_ref_52()
	call_c   build_ref_296()
	call_c   Dyam_Create_Unary(&ref[52],&ref[296])
	move_ret ref[297]
	c_ret

c_code local build_seed_38
	ret_reg &seed[38]
	call_c   build_ref_47()
	call_c   build_ref_307()
	call_c   Dyam_Seed_Start(&ref[47],&ref[307],I(0),fun1,1)
	call_c   build_ref_306()
	call_c   Dyam_Seed_Add_Comp(&ref[306],fun112,0)
	call_c   Dyam_Seed_End()
	move_ret seed[38]
	c_ret

;; TERM 306: '*GUARD*'(optimize_topbot_args([_B|_C], [_D|_E], _F, _G))
c_code local build_ref_306
	ret_reg &ref[306]
	call_c   build_ref_48()
	call_c   build_ref_305()
	call_c   Dyam_Create_Unary(&ref[48],&ref[305])
	move_ret ref[306]
	c_ret

;; TERM 305: optimize_topbot_args([_B|_C], [_D|_E], _F, _G)
c_code local build_ref_305
	ret_reg &ref[305]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(1),V(2))
	move_ret R(0)
	call_c   build_ref_1230()
	call_c   build_ref_778()
	call_c   Dyam_Term_Start(&ref[1230],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[778])
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[305]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_95
	ret_reg &seed[95]
	call_c   build_ref_48()
	call_c   build_ref_309()
	call_c   Dyam_Seed_Start(&ref[48],&ref[309],I(0),fun3,1)
	call_c   build_ref_310()
	call_c   Dyam_Seed_Add_Comp(&ref[310],&ref[309],0)
	call_c   Dyam_Seed_End()
	move_ret seed[95]
	c_ret

;; TERM 310: '*GUARD*'(optimize_topbot_args(_C, _E, _F, _H)) :> '$$HOLE$$'
c_code local build_ref_310
	ret_reg &ref[310]
	call_c   build_ref_309()
	call_c   Dyam_Create_Binary(I(9),&ref[309],I(7))
	move_ret ref[310]
	c_ret

;; TERM 309: '*GUARD*'(optimize_topbot_args(_C, _E, _F, _H))
c_code local build_ref_309
	ret_reg &ref[309]
	call_c   build_ref_48()
	call_c   build_ref_308()
	call_c   Dyam_Create_Unary(&ref[48],&ref[308])
	move_ret ref[309]
	c_ret

;; TERM 308: optimize_topbot_args(_C, _E, _F, _H)
c_code local build_ref_308
	ret_reg &ref[308]
	call_c   build_ref_1230()
	call_c   Dyam_Term_Start(&ref[1230],4)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[308]
	c_ret

;; TERM 312: [_B,_D|_H]
c_code local build_ref_312
	ret_reg &ref[312]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(3),V(7))
	move_ret R(0)
	call_c   Dyam_Create_List(V(1),R(0))
	move_ret ref[312]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun111[2]=[1,build_ref_312]

pl_code local fun111
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(6),&ref[312])
	fail_ret
fun110:
	call_c   Dyam_Deallocate()
	pl_ret


;; TERM 311: [_B|_H]
c_code local build_ref_311
	ret_reg &ref[311]
	call_c   Dyam_Create_List(V(1),V(7))
	move_ret ref[311]
	c_ret

long local pool_fun112[5]=[65539,build_ref_306,build_seed_95,build_ref_311,pool_fun111]

pl_code local fun112
	call_c   Dyam_Pool(pool_fun112)
	call_c   Dyam_Unify_Item(&ref[306])
	fail_ret
	call_c   Dyam_Allocate(0)
	pl_call  fun28(&seed[95],1)
	call_c   Dyam_Choice(fun111)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(1),V(3))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(6),&ref[311])
	fail_ret
	pl_jump  fun110()

;; TERM 307: '*GUARD*'(optimize_topbot_args([_B|_C], [_D|_E], _F, _G)) :> []
c_code local build_ref_307
	ret_reg &ref[307]
	call_c   build_ref_306()
	call_c   Dyam_Create_Binary(I(9),&ref[306],I(0))
	move_ret ref[307]
	c_ret

c_code local build_seed_18
	ret_reg &seed[18]
	call_c   build_ref_47()
	call_c   build_ref_322()
	call_c   Dyam_Seed_Start(&ref[47],&ref[322],I(0),fun1,1)
	call_c   build_ref_321()
	call_c   Dyam_Seed_Add_Comp(&ref[321],fun115,0)
	call_c   Dyam_Seed_End()
	move_ret seed[18]
	c_ret

;; TERM 321: '*GUARD*'(tag_il_body_to_lpda_handler(_B, (_C ; _D), _E, _F, _G, _H, _I, _J, _K, _L, _M, _N, _O))
c_code local build_ref_321
	ret_reg &ref[321]
	call_c   build_ref_48()
	call_c   build_ref_320()
	call_c   Dyam_Create_Unary(&ref[48],&ref[320])
	move_ret ref[321]
	c_ret

;; TERM 320: tag_il_body_to_lpda_handler(_B, (_C ; _D), _E, _F, _G, _H, _I, _J, _K, _L, _M, _N, _O)
c_code local build_ref_320
	ret_reg &ref[320]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Binary(I(5),V(2),V(3))
	move_ret R(0)
	call_c   build_ref_1295()
	call_c   Dyam_Term_Start(&ref[1295],13)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_End()
	move_ret ref[320]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1295: tag_il_body_to_lpda_handler
c_code local build_ref_1295
	ret_reg &ref[1295]
	call_c   Dyam_Create_Atom("tag_il_body_to_lpda_handler")
	move_ret ref[1295]
	c_ret

pl_code local fun114
	call_c   Dyam_Remove_Choice()
fun113:
	call_c   Dyam_Reg_Load(0,V(9))
	call_c   Dyam_Reg_Load(2,V(10))
	move     V(17), R(4)
	move     S(5), R(5)
	move     V(18), R(6)
	move     S(5), R(7)
	move     V(19), R(8)
	move     S(5), R(9)
	pl_call  pred_handle_choice_5()
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(2))
	call_c   Dyam_Reg_Load(4,V(4))
	call_c   Dyam_Reg_Load(6,V(5))
	call_c   Dyam_Reg_Load(8,V(6))
	call_c   Dyam_Reg_Load(10,V(7))
	call_c   Dyam_Reg_Load(12,V(8))
	call_c   Dyam_Reg_Load(14,V(17))
	call_c   Dyam_Reg_Load(16,V(18))
	call_c   Dyam_Reg_Load(18,V(11))
	call_c   Dyam_Reg_Load(20,V(12))
	call_c   Dyam_Reg_Load(22,V(13))
	call_c   Dyam_Reg_Load(24,V(14))
	pl_call  pred_tag_il_body_to_lpda_13()
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(3))
	call_c   Dyam_Reg_Load(4,V(4))
	call_c   Dyam_Reg_Load(6,V(5))
	call_c   Dyam_Reg_Load(8,V(6))
	call_c   Dyam_Reg_Load(10,V(7))
	call_c   Dyam_Reg_Load(12,V(8))
	call_c   Dyam_Reg_Load(14,V(17))
	call_c   Dyam_Reg_Load(16,V(19))
	call_c   Dyam_Reg_Load(18,V(11))
	call_c   Dyam_Reg_Load(20,V(12))
	call_c   Dyam_Reg_Load(22,V(13))
	call_c   Dyam_Reg_Load(24,V(14))
	call_c   Dyam_Reg_Deallocate(13)
	pl_jump  pred_tag_il_body_to_lpda_13()


;; TERM 323: _P -> _Q
c_code local build_ref_323
	ret_reg &ref[323]
	call_c   build_ref_1292()
	call_c   Dyam_Create_Binary(&ref[1292],V(15),V(16))
	move_ret ref[323]
	c_ret

long local pool_fun115[3]=[2,build_ref_321,build_ref_323]

pl_code local fun115
	call_c   Dyam_Pool(pool_fun115)
	call_c   Dyam_Unify_Item(&ref[321])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun114)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(2),&ref[323])
	fail_ret
	call_c   Dyam_Cut()
	pl_fail

;; TERM 322: '*GUARD*'(tag_il_body_to_lpda_handler(_B, (_C ; _D), _E, _F, _G, _H, _I, _J, _K, _L, _M, _N, _O)) :> []
c_code local build_ref_322
	ret_reg &ref[322]
	call_c   build_ref_321()
	call_c   Dyam_Create_Binary(I(9),&ref[321],I(0))
	move_ret ref[322]
	c_ret

c_code local build_seed_15
	ret_reg &seed[15]
	call_c   build_ref_47()
	call_c   build_ref_315()
	call_c   Dyam_Seed_Start(&ref[47],&ref[315],I(0),fun1,1)
	call_c   build_ref_314()
	call_c   Dyam_Seed_Add_Comp(&ref[314],fun152,0)
	call_c   Dyam_Seed_End()
	move_ret seed[15]
	c_ret

;; TERM 314: '*GUARD*'(tag_il_body_to_lpda_handler(_B, (_C @*), _D, _E, _F, _G, _H, _I, _J, _K, _L, _M, _N))
c_code local build_ref_314
	ret_reg &ref[314]
	call_c   build_ref_48()
	call_c   build_ref_313()
	call_c   Dyam_Create_Unary(&ref[48],&ref[313])
	move_ret ref[314]
	c_ret

;; TERM 313: tag_il_body_to_lpda_handler(_B, (_C @*), _D, _E, _F, _G, _H, _I, _J, _K, _L, _M, _N)
c_code local build_ref_313
	ret_reg &ref[313]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1296()
	call_c   Dyam_Create_Unary(&ref[1296],V(2))
	move_ret R(0)
	call_c   build_ref_1295()
	call_c   Dyam_Term_Start(&ref[1295],13)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_End()
	move_ret ref[313]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1296: @*
c_code local build_ref_1296
	ret_reg &ref[1296]
	call_c   Dyam_Create_Atom("@*")
	move_ret ref[1296]
	c_ret

c_code local build_seed_96
	ret_reg &seed[96]
	call_c   build_ref_48()
	call_c   build_ref_318()
	call_c   Dyam_Seed_Start(&ref[48],&ref[318],I(0),fun3,1)
	call_c   build_ref_319()
	call_c   Dyam_Seed_Add_Comp(&ref[319],&ref[318],0)
	call_c   Dyam_Seed_End()
	move_ret seed[96]
	c_ret

;; TERM 319: '*GUARD*'(tag_il_body_to_lpda_handler(_B, @*{goal=> _P, vars=> _O, from=> _Q, to=> _R, collect_first=> _S, collect_last=> _T, collect_loop=> _U, collect_next=> _V, collect_pred=> _W}, _D, _E, _F, _G, _H, _I, _J, _K, _L, _M, _N)) :> '$$HOLE$$'
c_code local build_ref_319
	ret_reg &ref[319]
	call_c   build_ref_318()
	call_c   Dyam_Create_Binary(I(9),&ref[318],I(7))
	move_ret ref[319]
	c_ret

;; TERM 318: '*GUARD*'(tag_il_body_to_lpda_handler(_B, @*{goal=> _P, vars=> _O, from=> _Q, to=> _R, collect_first=> _S, collect_last=> _T, collect_loop=> _U, collect_next=> _V, collect_pred=> _W}, _D, _E, _F, _G, _H, _I, _J, _K, _L, _M, _N))
c_code local build_ref_318
	ret_reg &ref[318]
	call_c   build_ref_48()
	call_c   build_ref_317()
	call_c   Dyam_Create_Unary(&ref[48],&ref[317])
	move_ret ref[318]
	c_ret

;; TERM 317: tag_il_body_to_lpda_handler(_B, @*{goal=> _P, vars=> _O, from=> _Q, to=> _R, collect_first=> _S, collect_last=> _T, collect_loop=> _U, collect_next=> _V, collect_pred=> _W}, _D, _E, _F, _G, _H, _I, _J, _K, _L, _M, _N)
c_code local build_ref_317
	ret_reg &ref[317]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1297()
	call_c   Dyam_Term_Start(&ref[1297],9)
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_Arg(V(21))
	call_c   Dyam_Term_Arg(V(22))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_1295()
	call_c   Dyam_Term_Start(&ref[1295],13)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_End()
	move_ret ref[317]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1297: @*!'$ft'
c_code local build_ref_1297
	ret_reg &ref[1297]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1306()
	call_c   Dyam_Create_List(&ref[1306],I(0))
	move_ret R(0)
	call_c   build_ref_1305()
	call_c   Dyam_Create_List(&ref[1305],R(0))
	move_ret R(0)
	call_c   build_ref_1304()
	call_c   Dyam_Create_List(&ref[1304],R(0))
	move_ret R(0)
	call_c   build_ref_1303()
	call_c   Dyam_Create_List(&ref[1303],R(0))
	move_ret R(0)
	call_c   build_ref_1302()
	call_c   Dyam_Create_List(&ref[1302],R(0))
	move_ret R(0)
	call_c   build_ref_1301()
	call_c   Dyam_Create_List(&ref[1301],R(0))
	move_ret R(0)
	call_c   build_ref_1300()
	call_c   Dyam_Create_List(&ref[1300],R(0))
	move_ret R(0)
	call_c   build_ref_1299()
	call_c   Dyam_Create_List(&ref[1299],R(0))
	move_ret R(0)
	call_c   build_ref_1298()
	call_c   Dyam_Create_List(&ref[1298],R(0))
	move_ret R(0)
	call_c   build_ref_1296()
	call_c   Dyam_Create_Atom_Module("$ft",&ref[1296])
	move_ret ref[1297]
	call_c   DYAM_Feature_2(&ref[1296],R(0))
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1298: goal
c_code local build_ref_1298
	ret_reg &ref[1298]
	call_c   Dyam_Create_Atom("goal")
	move_ret ref[1298]
	c_ret

;; TERM 1299: vars
c_code local build_ref_1299
	ret_reg &ref[1299]
	call_c   Dyam_Create_Atom("vars")
	move_ret ref[1299]
	c_ret

;; TERM 1300: from
c_code local build_ref_1300
	ret_reg &ref[1300]
	call_c   Dyam_Create_Atom("from")
	move_ret ref[1300]
	c_ret

;; TERM 1301: to
c_code local build_ref_1301
	ret_reg &ref[1301]
	call_c   Dyam_Create_Atom("to")
	move_ret ref[1301]
	c_ret

;; TERM 1302: collect_first
c_code local build_ref_1302
	ret_reg &ref[1302]
	call_c   Dyam_Create_Atom("collect_first")
	move_ret ref[1302]
	c_ret

;; TERM 1303: collect_last
c_code local build_ref_1303
	ret_reg &ref[1303]
	call_c   Dyam_Create_Atom("collect_last")
	move_ret ref[1303]
	c_ret

;; TERM 1304: collect_loop
c_code local build_ref_1304
	ret_reg &ref[1304]
	call_c   Dyam_Create_Atom("collect_loop")
	move_ret ref[1304]
	c_ret

;; TERM 1305: collect_next
c_code local build_ref_1305
	ret_reg &ref[1305]
	call_c   Dyam_Create_Atom("collect_next")
	move_ret ref[1305]
	c_ret

;; TERM 1306: collect_pred
c_code local build_ref_1306
	ret_reg &ref[1306]
	call_c   Dyam_Create_Atom("collect_pred")
	move_ret ref[1306]
	c_ret

long local pool_fun150[2]=[1,build_seed_96]

pl_code local fun151
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(2),V(15))
	fail_ret
fun150:
	call_c   Dyam_Deallocate()
	pl_jump  fun28(&seed[96],1)


;; TERM 316: _O ^ _P
c_code local build_ref_316
	ret_reg &ref[316]
	call_c   build_ref_1307()
	call_c   Dyam_Create_Binary(&ref[1307],V(14),V(15))
	move_ret ref[316]
	c_ret

;; TERM 1307: ^
c_code local build_ref_1307
	ret_reg &ref[1307]
	call_c   Dyam_Create_Atom("^")
	move_ret ref[1307]
	c_ret

long local pool_fun152[5]=[131074,build_ref_314,build_ref_316,pool_fun150,pool_fun150]

pl_code local fun152
	call_c   Dyam_Pool(pool_fun152)
	call_c   Dyam_Unify_Item(&ref[314])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun151)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(2),&ref[316])
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun150()

;; TERM 315: '*GUARD*'(tag_il_body_to_lpda_handler(_B, (_C @*), _D, _E, _F, _G, _H, _I, _J, _K, _L, _M, _N)) :> []
c_code local build_ref_315
	ret_reg &ref[315]
	call_c   build_ref_314()
	call_c   Dyam_Create_Binary(I(9),&ref[314],I(0))
	move_ret ref[315]
	c_ret

c_code local build_seed_17
	ret_reg &seed[17]
	call_c   build_ref_47()
	call_c   build_ref_329()
	call_c   Dyam_Seed_Start(&ref[47],&ref[329],I(0),fun1,1)
	call_c   build_ref_328()
	call_c   Dyam_Seed_Add_Comp(&ref[328],fun121,0)
	call_c   Dyam_Seed_End()
	move_ret seed[17]
	c_ret

;; TERM 328: '*GUARD*'(tag_il_body_to_lpda_handler(_B, guard{goal=> _C, plus=> _D, minus=> _E}, _F, _G, _H, _I, _J, _K, _L, _M, _N, _O, _P))
c_code local build_ref_328
	ret_reg &ref[328]
	call_c   build_ref_48()
	call_c   build_ref_327()
	call_c   Dyam_Create_Unary(&ref[48],&ref[327])
	move_ret ref[328]
	c_ret

;; TERM 327: tag_il_body_to_lpda_handler(_B, guard{goal=> _C, plus=> _D, minus=> _E}, _F, _G, _H, _I, _J, _K, _L, _M, _N, _O, _P)
c_code local build_ref_327
	ret_reg &ref[327]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1308()
	call_c   Dyam_Term_Start(&ref[1308],3)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_1295()
	call_c   Dyam_Term_Start(&ref[1295],13)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_End()
	move_ret ref[327]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1308: guard!'$ft'
c_code local build_ref_1308
	ret_reg &ref[1308]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1311()
	call_c   Dyam_Create_List(&ref[1311],I(0))
	move_ret R(0)
	call_c   build_ref_1310()
	call_c   Dyam_Create_List(&ref[1310],R(0))
	move_ret R(0)
	call_c   build_ref_1298()
	call_c   Dyam_Create_List(&ref[1298],R(0))
	move_ret R(0)
	call_c   build_ref_1309()
	call_c   Dyam_Create_Atom_Module("$ft",&ref[1309])
	move_ret ref[1308]
	call_c   DYAM_Feature_2(&ref[1309],R(0))
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1309: guard
c_code local build_ref_1309
	ret_reg &ref[1309]
	call_c   Dyam_Create_Atom("guard")
	move_ret ref[1309]
	c_ret

;; TERM 1310: plus
c_code local build_ref_1310
	ret_reg &ref[1310]
	call_c   Dyam_Create_Atom("plus")
	move_ret ref[1310]
	c_ret

;; TERM 1311: minus
c_code local build_ref_1311
	ret_reg &ref[1311]
	call_c   Dyam_Create_Atom("minus")
	move_ret ref[1311]
	c_ret

pl_code local fun121
	call_c   build_ref_328()
	call_c   Dyam_Unify_Item(&ref[328])
	pl_fail

;; TERM 329: '*GUARD*'(tag_il_body_to_lpda_handler(_B, guard{goal=> _C, plus=> _D, minus=> _E}, _F, _G, _H, _I, _J, _K, _L, _M, _N, _O, _P)) :> []
c_code local build_ref_329
	ret_reg &ref[329]
	call_c   build_ref_328()
	call_c   Dyam_Create_Binary(I(9),&ref[328],I(0))
	move_ret ref[329]
	c_ret

c_code local build_seed_8
	ret_reg &seed[8]
	call_c   build_ref_47()
	call_c   build_ref_326()
	call_c   Dyam_Seed_Start(&ref[47],&ref[326],I(0),fun1,1)
	call_c   build_ref_325()
	call_c   Dyam_Seed_Add_Comp(&ref[325],fun116,0)
	call_c   Dyam_Seed_End()
	move_ret seed[8]
	c_ret

;; TERM 325: '*GUARD*'(tag_compile_subtree_handler(_B, tag_node{id=> _C, label=> [], children=> _D, kind=> scan, adj=> _E, spine=> _F, top=> _G, bot=> _H, token=> _I, adjleft=> _J, adjright=> _K, adjwrap=> _L}, _M, _N, _O, (unify(_M, _N) :> _O), _P, _Q, _R))
c_code local build_ref_325
	ret_reg &ref[325]
	call_c   build_ref_48()
	call_c   build_ref_324()
	call_c   Dyam_Create_Unary(&ref[48],&ref[324])
	move_ret ref[325]
	c_ret

;; TERM 324: tag_compile_subtree_handler(_B, tag_node{id=> _C, label=> [], children=> _D, kind=> scan, adj=> _E, spine=> _F, top=> _G, bot=> _H, token=> _I, adjleft=> _J, adjright=> _K, adjwrap=> _L}, _M, _N, _O, (unify(_M, _N) :> _O), _P, _Q, _R)
c_code local build_ref_324
	ret_reg &ref[324]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_1259()
	call_c   build_ref_1312()
	call_c   Dyam_Term_Start(&ref[1259],12)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(&ref[1312])
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_1283()
	call_c   Dyam_Create_Binary(&ref[1283],V(12),V(13))
	move_ret R(1)
	call_c   Dyam_Create_Binary(I(9),R(1),V(14))
	move_ret R(1)
	call_c   build_ref_1278()
	call_c   Dyam_Term_Start(&ref[1278],9)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_End()
	move_ret ref[324]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1312: scan
c_code local build_ref_1312
	ret_reg &ref[1312]
	call_c   Dyam_Create_Atom("scan")
	move_ret ref[1312]
	c_ret

pl_code local fun116
	call_c   build_ref_325()
	call_c   Dyam_Unify_Item(&ref[325])
	fail_ret
	pl_ret

;; TERM 326: '*GUARD*'(tag_compile_subtree_handler(_B, tag_node{id=> _C, label=> [], children=> _D, kind=> scan, adj=> _E, spine=> _F, top=> _G, bot=> _H, token=> _I, adjleft=> _J, adjright=> _K, adjwrap=> _L}, _M, _N, _O, (unify(_M, _N) :> _O), _P, _Q, _R)) :> []
c_code local build_ref_326
	ret_reg &ref[326]
	call_c   build_ref_325()
	call_c   Dyam_Create_Binary(I(9),&ref[325],I(0))
	move_ret ref[326]
	c_ret

c_code local build_seed_32
	ret_reg &seed[32]
	call_c   build_ref_52()
	call_c   build_ref_346()
	call_c   Dyam_Seed_Start(&ref[52],&ref[346],I(0),fun15,1)
	call_c   build_ref_347()
	call_c   Dyam_Seed_Add_Comp(&ref[347],fun127,0)
	call_c   Dyam_Seed_End()
	move_ret seed[32]
	c_ret

;; TERM 347: '*CITEM*'('call_decompose_args/3'(tag_subst((_B / _C))), _A)
c_code local build_ref_347
	ret_reg &ref[347]
	call_c   build_ref_56()
	call_c   build_ref_344()
	call_c   Dyam_Create_Binary(&ref[56],&ref[344],V(0))
	move_ret ref[347]
	c_ret

;; TERM 344: 'call_decompose_args/3'(tag_subst((_B / _C)))
c_code local build_ref_344
	ret_reg &ref[344]
	call_c   build_ref_1232()
	call_c   build_ref_569()
	call_c   Dyam_Create_Unary(&ref[1232],&ref[569])
	move_ret ref[344]
	c_ret

;; TERM 569: tag_subst((_B / _C))
c_code local build_ref_569
	ret_reg &ref[569]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1294()
	call_c   Dyam_Create_Binary(&ref[1294],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_1313()
	call_c   Dyam_Create_Unary(&ref[1313],R(0))
	move_ret ref[569]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1313: tag_subst
c_code local build_ref_1313
	ret_reg &ref[1313]
	call_c   Dyam_Create_Atom("tag_subst")
	move_ret ref[1313]
	c_ret

;; TERM 352: [_J|_K]
c_code local build_ref_352
	ret_reg &ref[352]
	call_c   Dyam_Create_List(V(9),V(10))
	move_ret ref[352]
	c_ret

;; TERM 353: [_D,_E,_F,_H|_K]
c_code local build_ref_353
	ret_reg &ref[353]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(7),V(10))
	move_ret R(0)
	call_c   Dyam_Create_Tupple(3,5,R(0))
	move_ret ref[353]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_99
	ret_reg &seed[99]
	call_c   build_ref_0()
	call_c   build_ref_350()
	call_c   Dyam_Seed_Start(&ref[0],&ref[350],&ref[350],fun3,1)
	call_c   build_ref_351()
	call_c   Dyam_Seed_Add_Comp(&ref[351],&ref[350],0)
	call_c   Dyam_Seed_End()
	move_ret seed[99]
	c_ret

;; TERM 351: '*RITEM*'(_A, return([_D,_E,_F,_G,_H], _I)) :> '$$HOLE$$'
c_code local build_ref_351
	ret_reg &ref[351]
	call_c   build_ref_350()
	call_c   Dyam_Create_Binary(I(9),&ref[350],I(7))
	move_ret ref[351]
	c_ret

;; TERM 350: '*RITEM*'(_A, return([_D,_E,_F,_G,_H], _I))
c_code local build_ref_350
	ret_reg &ref[350]
	call_c   build_ref_0()
	call_c   build_ref_349()
	call_c   Dyam_Create_Binary(&ref[0],V(0),&ref[349])
	move_ret ref[350]
	c_ret

;; TERM 349: return([_D,_E,_F,_G,_H], _I)
c_code local build_ref_349
	ret_reg &ref[349]
	call_c   build_ref_1233()
	call_c   build_ref_348()
	call_c   Dyam_Create_Binary(&ref[1233],&ref[348],V(8))
	move_ret ref[349]
	c_ret

;; TERM 348: [_D,_E,_F,_G,_H]
c_code local build_ref_348
	ret_reg &ref[348]
	call_c   Dyam_Create_Tupple(3,7,I(0))
	move_ret ref[348]
	c_ret

long local pool_fun125[2]=[1,build_seed_99]

long local pool_fun126[4]=[65538,build_ref_352,build_ref_353,pool_fun125]

pl_code local fun126
	call_c   Dyam_Remove_Choice()
	call_c   DYAM_evpred_univ(V(6),&ref[352])
	fail_ret
	call_c   Dyam_Unify(V(8),&ref[353])
	fail_ret
fun125:
	call_c   Dyam_Deallocate()
	pl_jump  fun13(&seed[99],2)


long local pool_fun127[5]=[131074,build_ref_347,build_ref_348,pool_fun126,pool_fun125]

pl_code local fun127
	call_c   Dyam_Pool(pool_fun127)
	call_c   Dyam_Unify_Item(&ref[347])
	fail_ret
	call_c   DYAM_evpred_functor(V(6),V(1),V(2))
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun126)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_gt(V(2),N(30))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(8),&ref[348])
	fail_ret
	pl_jump  fun125()

;; TERM 346: '*FIRST*'('call_decompose_args/3'(tag_subst((_B / _C)))) :> []
c_code local build_ref_346
	ret_reg &ref[346]
	call_c   build_ref_345()
	call_c   Dyam_Create_Binary(I(9),&ref[345],I(0))
	move_ret ref[346]
	c_ret

;; TERM 345: '*FIRST*'('call_decompose_args/3'(tag_subst((_B / _C))))
c_code local build_ref_345
	ret_reg &ref[345]
	call_c   build_ref_52()
	call_c   build_ref_344()
	call_c   Dyam_Create_Unary(&ref[52],&ref[344])
	move_ret ref[345]
	c_ret

c_code local build_seed_44
	ret_reg &seed[44]
	call_c   build_ref_52()
	call_c   build_ref_356()
	call_c   Dyam_Seed_Start(&ref[52],&ref[356],I(0),fun15,1)
	call_c   build_ref_357()
	call_c   Dyam_Seed_Add_Comp(&ref[357],fun132,0)
	call_c   Dyam_Seed_End()
	move_ret seed[44]
	c_ret

;; TERM 357: '*CITEM*'(wrapping_predicate(tag_foot((_B / _C))), _A)
c_code local build_ref_357
	ret_reg &ref[357]
	call_c   build_ref_56()
	call_c   build_ref_354()
	call_c   Dyam_Create_Binary(&ref[56],&ref[354],V(0))
	move_ret ref[357]
	c_ret

;; TERM 354: wrapping_predicate(tag_foot((_B / _C)))
c_code local build_ref_354
	ret_reg &ref[354]
	call_c   build_ref_1314()
	call_c   build_ref_374()
	call_c   Dyam_Create_Unary(&ref[1314],&ref[374])
	move_ret ref[354]
	c_ret

;; TERM 1314: wrapping_predicate
c_code local build_ref_1314
	ret_reg &ref[1314]
	call_c   Dyam_Create_Atom("wrapping_predicate")
	move_ret ref[1314]
	c_ret

;; TERM 373: '$CLOSURE'('$fun'(131, 0, 1128349596), '$TUPPLE'(35075411940092))
c_code local build_ref_373
	ret_reg &ref[373]
	call_c   build_ref_372()
	call_c   Dyam_Closure_Aux(fun131,&ref[372])
	move_ret ref[373]
	c_ret

c_code local build_seed_102
	ret_reg &seed[102]
	call_c   build_ref_48()
	call_c   build_ref_367()
	call_c   Dyam_Seed_Start(&ref[48],&ref[367],I(0),fun3,1)
	call_c   build_ref_368()
	call_c   Dyam_Seed_Add_Comp(&ref[368],&ref[367],0)
	call_c   Dyam_Seed_End()
	move_ret seed[102]
	c_ret

;; TERM 368: '*GUARD*'(make_tag_bot_callret(_F, _D, _E, _H, _I)) :> '$$HOLE$$'
c_code local build_ref_368
	ret_reg &ref[368]
	call_c   build_ref_367()
	call_c   Dyam_Create_Binary(I(9),&ref[367],I(7))
	move_ret ref[368]
	c_ret

;; TERM 367: '*GUARD*'(make_tag_bot_callret(_F, _D, _E, _H, _I))
c_code local build_ref_367
	ret_reg &ref[367]
	call_c   build_ref_48()
	call_c   build_ref_366()
	call_c   Dyam_Create_Unary(&ref[48],&ref[366])
	move_ret ref[367]
	c_ret

;; TERM 366: make_tag_bot_callret(_F, _D, _E, _H, _I)
c_code local build_ref_366
	ret_reg &ref[366]
	call_c   build_ref_1315()
	call_c   Dyam_Term_Start(&ref[1315],5)
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_End()
	move_ret ref[366]
	c_ret

;; TERM 1315: make_tag_bot_callret
c_code local build_ref_1315
	ret_reg &ref[1315]
	call_c   Dyam_Create_Atom("make_tag_bot_callret")
	move_ret ref[1315]
	c_ret

;; TERM 369: [_J|_G]
c_code local build_ref_369
	ret_reg &ref[369]
	call_c   Dyam_Create_List(V(9),V(6))
	move_ret ref[369]
	c_ret

;; TERM 370: '*SA-FOOT-WRAPPER*'(tag_foot((_B / _C)), _K, [_J|_G], '*SA-FOOT*'(_H, _I, '*SA-PROLOG-LAST*'))
c_code local build_ref_370
	ret_reg &ref[370]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1317()
	call_c   build_ref_847()
	call_c   Dyam_Term_Start(&ref[1317],3)
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(&ref[847])
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_1316()
	call_c   build_ref_374()
	call_c   build_ref_369()
	call_c   Dyam_Term_Start(&ref[1316],4)
	call_c   Dyam_Term_Arg(&ref[374])
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(&ref[369])
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_End()
	move_ret ref[370]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1316: '*SA-FOOT-WRAPPER*'
c_code local build_ref_1316
	ret_reg &ref[1316]
	call_c   Dyam_Create_Atom("*SA-FOOT-WRAPPER*")
	move_ret ref[1316]
	c_ret

;; TERM 847: '*SA-PROLOG-LAST*'
c_code local build_ref_847
	ret_reg &ref[847]
	call_c   Dyam_Create_Atom("*SA-PROLOG-LAST*")
	move_ret ref[847]
	c_ret

;; TERM 1317: '*SA-FOOT*'
c_code local build_ref_1317
	ret_reg &ref[1317]
	call_c   Dyam_Create_Atom("*SA-FOOT*")
	move_ret ref[1317]
	c_ret

long local pool_fun131[5]=[4,build_seed_102,build_ref_369,build_ref_370,build_seed_69]

pl_code local fun131
	call_c   Dyam_Pool(pool_fun131)
	call_c   Dyam_Allocate(0)
	pl_call  fun28(&seed[102],1)
	move     &ref[369], R(0)
	move     S(5), R(1)
	move     V(10), R(2)
	move     S(5), R(3)
	move     V(11), R(4)
	move     S(5), R(5)
	pl_call  pred_tag_numbervars_3()
	call_c   DYAM_evpred_assert_1(&ref[370])
	call_c   Dyam_Deallocate()
	pl_jump  fun13(&seed[69],2)

;; TERM 372: '$TUPPLE'(35075411940092)
c_code local build_ref_372
	ret_reg &ref[372]
	call_c   Dyam_Create_Simple_Tupple(0,532676608)
	move_ret ref[372]
	c_ret

long local pool_fun132[5]=[4,build_ref_357,build_ref_373,build_ref_374,build_ref_300]

pl_code local fun132
	call_c   Dyam_Pool(pool_fun132)
	call_c   Dyam_Unify_Item(&ref[357])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[373], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	move     &ref[374], R(4)
	move     S(5), R(5)
	move     &ref[300], R(6)
	move     S(5), R(7)
	move     V(6), R(8)
	move     S(5), R(9)
	call_c   Dyam_Reg_Deallocate(5)
fun130:
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Reg_Bind(2,V(5))
	call_c   Dyam_Multi_Reg_Bind(4,2,3)
	call_c   Dyam_Choice(fun129)
	pl_call  fun46(&seed[100],1)
	pl_fail


;; TERM 356: '*FIRST*'(wrapping_predicate(tag_foot((_B / _C)))) :> []
c_code local build_ref_356
	ret_reg &ref[356]
	call_c   build_ref_355()
	call_c   Dyam_Create_Binary(I(9),&ref[355],I(0))
	move_ret ref[356]
	c_ret

;; TERM 355: '*FIRST*'(wrapping_predicate(tag_foot((_B / _C))))
c_code local build_ref_355
	ret_reg &ref[355]
	call_c   build_ref_52()
	call_c   build_ref_354()
	call_c   Dyam_Create_Unary(&ref[52],&ref[354])
	move_ret ref[355]
	c_ret

c_code local build_seed_50
	ret_reg &seed[50]
	call_c   build_ref_47()
	call_c   build_ref_377()
	call_c   Dyam_Seed_Start(&ref[47],&ref[377],I(0),fun1,1)
	call_c   build_ref_376()
	call_c   Dyam_Seed_Add_Comp(&ref[376],fun145,0)
	call_c   Dyam_Seed_End()
	move_ret seed[50]
	c_ret

;; TERM 376: '*GUARD*'(tag_compile_subtree_handler(_B, '$plusguard'(_C, _D), _E, _F, _G, _H, _I, _J, _K))
c_code local build_ref_376
	ret_reg &ref[376]
	call_c   build_ref_48()
	call_c   build_ref_375()
	call_c   Dyam_Create_Unary(&ref[48],&ref[375])
	move_ret ref[376]
	c_ret

;; TERM 375: tag_compile_subtree_handler(_B, '$plusguard'(_C, _D), _E, _F, _G, _H, _I, _J, _K)
c_code local build_ref_375
	ret_reg &ref[375]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1318()
	call_c   Dyam_Create_Binary(&ref[1318],V(2),V(3))
	move_ret R(0)
	call_c   build_ref_1278()
	call_c   Dyam_Term_Start(&ref[1278],9)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_End()
	move_ret ref[375]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1318: '$plusguard'
c_code local build_ref_1318
	ret_reg &ref[1318]
	call_c   Dyam_Create_Atom("$plusguard")
	move_ret ref[1318]
	c_ret

;; TERM 396: not_a_node
c_code local build_ref_396
	ret_reg &ref[396]
	call_c   Dyam_Create_Atom("not_a_node")
	move_ret ref[396]
	c_ret

c_code local build_seed_103
	ret_reg &seed[103]
	call_c   build_ref_48()
	call_c   build_ref_382()
	call_c   Dyam_Seed_Start(&ref[48],&ref[382],I(0),fun3,1)
	call_c   build_ref_383()
	call_c   Dyam_Seed_Add_Comp(&ref[383],&ref[382],0)
	call_c   Dyam_Seed_End()
	move_ret seed[103]
	c_ret

;; TERM 383: '*GUARD*'(tag_compile_subtree(_B, _C, _E, _F, _X, _D1, _I, _J, _C1)) :> '$$HOLE$$'
c_code local build_ref_383
	ret_reg &ref[383]
	call_c   build_ref_382()
	call_c   Dyam_Create_Binary(I(9),&ref[382],I(7))
	move_ret ref[383]
	c_ret

;; TERM 382: '*GUARD*'(tag_compile_subtree(_B, _C, _E, _F, _X, _D1, _I, _J, _C1))
c_code local build_ref_382
	ret_reg &ref[382]
	call_c   build_ref_48()
	call_c   build_ref_381()
	call_c   Dyam_Create_Unary(&ref[48],&ref[381])
	move_ret ref[382]
	c_ret

;; TERM 381: tag_compile_subtree(_B, _C, _E, _F, _X, _D1, _I, _J, _C1)
c_code local build_ref_381
	ret_reg &ref[381]
	call_c   build_ref_1275()
	call_c   Dyam_Term_Start(&ref[1275],9)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(23))
	call_c   Dyam_Term_Arg(V(29))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(28))
	call_c   Dyam_Term_End()
	move_ret ref[381]
	c_ret

c_code local build_seed_105
	ret_reg &seed[105]
	call_c   build_ref_48()
	call_c   build_ref_390()
	call_c   Dyam_Seed_Start(&ref[48],&ref[390],I(0),fun3,1)
	call_c   build_ref_391()
	call_c   Dyam_Seed_Add_Comp(&ref[391],&ref[390],0)
	call_c   Dyam_Seed_End()
	move_ret seed[105]
	c_ret

;; TERM 391: '*GUARD*'(body_to_lpda(_B, domain(_L, _L), _D1, _E1, _I)) :> '$$HOLE$$'
c_code local build_ref_391
	ret_reg &ref[391]
	call_c   build_ref_390()
	call_c   Dyam_Create_Binary(I(9),&ref[390],I(7))
	move_ret ref[391]
	c_ret

;; TERM 390: '*GUARD*'(body_to_lpda(_B, domain(_L, _L), _D1, _E1, _I))
c_code local build_ref_390
	ret_reg &ref[390]
	call_c   build_ref_48()
	call_c   build_ref_389()
	call_c   Dyam_Create_Unary(&ref[48],&ref[389])
	move_ret ref[390]
	c_ret

;; TERM 389: body_to_lpda(_B, domain(_L, _L), _D1, _E1, _I)
c_code local build_ref_389
	ret_reg &ref[389]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1320()
	call_c   Dyam_Create_Binary(&ref[1320],V(11),V(11))
	move_ret R(0)
	call_c   build_ref_1319()
	call_c   Dyam_Term_Start(&ref[1319],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(29))
	call_c   Dyam_Term_Arg(V(30))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_End()
	move_ret ref[389]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1319: body_to_lpda
c_code local build_ref_1319
	ret_reg &ref[1319]
	call_c   Dyam_Create_Atom("body_to_lpda")
	move_ret ref[1319]
	c_ret

;; TERM 1320: domain
c_code local build_ref_1320
	ret_reg &ref[1320]
	call_c   Dyam_Create_Atom("domain")
	move_ret ref[1320]
	c_ret

c_code local build_seed_104
	ret_reg &seed[104]
	call_c   build_ref_48()
	call_c   build_ref_385()
	call_c   Dyam_Seed_Start(&ref[48],&ref[385],I(0),fun3,1)
	call_c   build_ref_386()
	call_c   Dyam_Seed_Add_Comp(&ref[386],&ref[385],0)
	call_c   Dyam_Seed_End()
	move_ret seed[104]
	c_ret

;; TERM 386: '*GUARD*'(body_to_lpda(_B, _Y, _E1, _F1, _I)) :> '$$HOLE$$'
c_code local build_ref_386
	ret_reg &ref[386]
	call_c   build_ref_385()
	call_c   Dyam_Create_Binary(I(9),&ref[385],I(7))
	move_ret ref[386]
	c_ret

;; TERM 385: '*GUARD*'(body_to_lpda(_B, _Y, _E1, _F1, _I))
c_code local build_ref_385
	ret_reg &ref[385]
	call_c   build_ref_48()
	call_c   build_ref_384()
	call_c   Dyam_Create_Unary(&ref[48],&ref[384])
	move_ret ref[385]
	c_ret

;; TERM 384: body_to_lpda(_B, _Y, _E1, _F1, _I)
c_code local build_ref_384
	ret_reg &ref[384]
	call_c   build_ref_1319()
	call_c   Dyam_Term_Start(&ref[1319],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(24))
	call_c   Dyam_Term_Arg(V(30))
	call_c   Dyam_Term_Arg(V(31))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_End()
	move_ret ref[384]
	c_ret

;; TERM 388: '*NOOP*'(_G1) :> _F1
c_code local build_ref_388
	ret_reg &ref[388]
	call_c   build_ref_387()
	call_c   Dyam_Create_Binary(I(9),&ref[387],V(31))
	move_ret ref[388]
	c_ret

;; TERM 387: '*NOOP*'(_G1)
c_code local build_ref_387
	ret_reg &ref[387]
	call_c   build_ref_1321()
	call_c   Dyam_Create_Unary(&ref[1321],V(32))
	move_ret ref[387]
	c_ret

;; TERM 1321: '*NOOP*'
c_code local build_ref_1321
	ret_reg &ref[1321]
	call_c   Dyam_Create_Atom("*NOOP*")
	move_ret ref[1321]
	c_ret

long local pool_fun133[2]=[1,build_ref_388]

pl_code local fun133
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Reg_Load(0,V(24))
	move     V(32), R(2)
	move     S(5), R(3)
	pl_call  pred_tupple_2()
	call_c   Dyam_Unify(V(7),&ref[388])
	fail_ret
	pl_jump  fun110()

long local pool_fun134[3]=[65537,build_seed_104,pool_fun133]

long local pool_fun135[3]=[65537,build_seed_105,pool_fun134]

pl_code local fun135
	call_c   Dyam_Remove_Choice()
	pl_call  fun28(&seed[105],1)
fun134:
	pl_call  fun28(&seed[104],1)
	call_c   Dyam_Choice(fun133)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Reg_Load(0,V(24))
	move     N(0), R(2)
	move     0, R(3)
	pl_call  pred_guard_depth_2()
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(7),V(31))
	fail_ret
	pl_jump  fun110()


long local pool_fun136[4]=[131073,build_seed_103,pool_fun135,pool_fun134]

pl_code local fun141
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(23),V(6))
	fail_ret
	call_c   Dyam_Unify(V(24),V(3))
	fail_ret
fun136:
	call_c   Dyam_Reg_Load(0,V(24))
	call_c   Dyam_Reg_Load(2,V(10))
	move     V(28), R(4)
	move     S(5), R(5)
	pl_call  pred_extend_tupple_3()
	pl_call  fun28(&seed[103],1)
	call_c   Dyam_Choice(fun135)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_atom(V(11))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(30),V(29))
	fail_ret
	pl_jump  fun134()


;; TERM 392: [anchor,coanchor,lexical,subst,std]
c_code local build_ref_392
	ret_reg &ref[392]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1146()
	call_c   Dyam_Create_List(&ref[1146],I(0))
	move_ret R(0)
	call_c   build_ref_284()
	call_c   Dyam_Create_List(&ref[284],R(0))
	move_ret R(0)
	call_c   build_ref_1322()
	call_c   Dyam_Create_List(&ref[1322],R(0))
	move_ret R(0)
	call_c   build_ref_1114()
	call_c   Dyam_Create_List(&ref[1114],R(0))
	move_ret R(0)
	call_c   build_ref_379()
	call_c   Dyam_Create_List(&ref[379],R(0))
	move_ret ref[392]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 379: anchor
c_code local build_ref_379
	ret_reg &ref[379]
	call_c   Dyam_Create_Atom("anchor")
	move_ret ref[379]
	c_ret

;; TERM 1114: coanchor
c_code local build_ref_1114
	ret_reg &ref[1114]
	call_c   Dyam_Create_Atom("coanchor")
	move_ret ref[1114]
	c_ret

;; TERM 1322: lexical
c_code local build_ref_1322
	ret_reg &ref[1322]
	call_c   Dyam_Create_Atom("lexical")
	move_ret ref[1322]
	c_ret

;; TERM 1146: std
c_code local build_ref_1146
	ret_reg &ref[1146]
	call_c   Dyam_Create_Atom("std")
	move_ret ref[1146]
	c_ret

pl_code local fun138
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(27),V(10))
	fail_ret
fun137:
	call_c   Dyam_Reg_Load(0,V(3))
	move     V(24), R(2)
	move     S(5), R(3)
	move     N(0), R(4)
	move     0, R(5)
	call_c   Dyam_Reg_Load(6,V(27))
	call_c   Dyam_Reg_Load(8,V(14))
	pl_call  pred_guard_approx_5()
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(3))
	call_c   Dyam_Reg_Load(4,V(6))
	move     V(23), R(6)
	move     S(5), R(7)
	call_c   Dyam_Reg_Load(8,V(8))
	pl_call  pred_guard_post_handler_5()
	pl_jump  fun136()


c_code local build_seed_106
	ret_reg &seed[106]
	call_c   build_ref_48()
	call_c   build_ref_394()
	call_c   Dyam_Seed_Start(&ref[48],&ref[394],I(0),fun3,1)
	call_c   build_ref_395()
	call_c   Dyam_Seed_Add_Comp(&ref[395],&ref[394],0)
	call_c   Dyam_Seed_End()
	move_ret seed[106]
	c_ret

;; TERM 395: '*GUARD*'(make_tag_subst_callret(_R, _E, _F, _Z, _A1)) :> '$$HOLE$$'
c_code local build_ref_395
	ret_reg &ref[395]
	call_c   build_ref_394()
	call_c   Dyam_Create_Binary(I(9),&ref[394],I(7))
	move_ret ref[395]
	c_ret

;; TERM 394: '*GUARD*'(make_tag_subst_callret(_R, _E, _F, _Z, _A1))
c_code local build_ref_394
	ret_reg &ref[394]
	call_c   build_ref_48()
	call_c   build_ref_393()
	call_c   Dyam_Create_Unary(&ref[48],&ref[393])
	move_ret ref[394]
	c_ret

;; TERM 393: make_tag_subst_callret(_R, _E, _F, _Z, _A1)
c_code local build_ref_393
	ret_reg &ref[393]
	call_c   build_ref_1323()
	call_c   Dyam_Term_Start(&ref[1323],5)
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(25))
	call_c   Dyam_Term_Arg(V(26))
	call_c   Dyam_Term_End()
	move_ret ref[393]
	c_ret

;; TERM 1323: make_tag_subst_callret
c_code local build_ref_1323
	ret_reg &ref[1323]
	call_c   Dyam_Create_Atom("make_tag_subst_callret")
	move_ret ref[1323]
	c_ret

long local pool_fun139[5]=[131074,build_ref_284,build_seed_106,pool_fun136,pool_fun136]

pl_code local fun140
	call_c   Dyam_Remove_Choice()
fun139:
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun138)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(14),&ref[284])
	fail_ret
	call_c   Dyam_Cut()
	pl_call  fun28(&seed[106],1)
	call_c   Dyam_Reg_Load(0,V(25))
	call_c   Dyam_Reg_Load(2,V(10))
	move     V(27), R(4)
	move     S(5), R(5)
	pl_call  pred_extend_tupple_3()
	pl_jump  fun137()


;; TERM 161: yes
c_code local build_ref_161
	ret_reg &ref[161]
	call_c   Dyam_Create_Atom("yes")
	move_ret ref[161]
	c_ret

long local pool_fun142[5]=[131074,build_ref_392,build_ref_161,pool_fun136,pool_fun139]

pl_code local fun142
	call_c   Dyam_Update_Choice(fun141)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_atom(V(11))
	fail_ret
	pl_call  Domain_2(V(14),&ref[392])
	call_c   Dyam_Choice(fun140)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(16),&ref[161])
	fail_ret
	call_c   Dyam_Cut()
	pl_fail

;; TERM 380: tag_extracted_anchor_guard(_B)
c_code local build_ref_380
	ret_reg &ref[380]
	call_c   build_ref_1324()
	call_c   Dyam_Create_Unary(&ref[1324],V(1))
	move_ret ref[380]
	c_ret

;; TERM 1324: tag_extracted_anchor_guard
c_code local build_ref_1324
	ret_reg &ref[1324]
	call_c   Dyam_Create_Atom("tag_extracted_anchor_guard")
	move_ret ref[1324]
	c_ret

long local pool_fun143[5]=[131074,build_ref_379,build_ref_380,pool_fun142,pool_fun136]

long local pool_fun144[4]=[65538,build_ref_396,build_ref_397,pool_fun143]

pl_code local fun144
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(11),I(0))
	fail_ret
	call_c   Dyam_Unify(V(14),&ref[396])
	fail_ret
	call_c   Dyam_Unify(V(16),&ref[397])
	fail_ret
fun143:
	call_c   Dyam_Choice(fun142)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(14),&ref[379])
	fail_ret
	pl_call  Object_1(&ref[380])
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(23),V(6))
	fail_ret
	call_c   Dyam_Reg_Load(0,V(3))
	move     V(24), R(2)
	move     S(5), R(3)
	move     N(0), R(4)
	move     0, R(5)
	call_c   Dyam_Reg_Load(6,V(10))
	call_c   Dyam_Reg_Load(8,V(14))
	pl_call  pred_guard_approx_5()
	pl_jump  fun136()


;; TERM 378: tag_node{id=> _L, label=> _M, children=> _N, kind=> _O, adj=> _P, spine=> _Q, top=> _R, bot=> _S, token=> _T, adjleft=> _U, adjright=> _V, adjwrap=> _W}
c_code local build_ref_378
	ret_reg &ref[378]
	call_c   build_ref_1259()
	call_c   Dyam_Term_Start(&ref[1259],12)
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_Arg(V(21))
	call_c   Dyam_Term_Arg(V(22))
	call_c   Dyam_Term_End()
	move_ret ref[378]
	c_ret

long local pool_fun145[5]=[131074,build_ref_376,build_ref_378,pool_fun144,pool_fun143]

pl_code local fun145
	call_c   Dyam_Pool(pool_fun145)
	call_c   Dyam_Unify_Item(&ref[376])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun144)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(2),&ref[378])
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun143()

;; TERM 377: '*GUARD*'(tag_compile_subtree_handler(_B, '$plusguard'(_C, _D), _E, _F, _G, _H, _I, _J, _K)) :> []
c_code local build_ref_377
	ret_reg &ref[377]
	call_c   build_ref_376()
	call_c   Dyam_Create_Binary(I(9),&ref[376],I(0))
	move_ret ref[377]
	c_ret

c_code local build_seed_27
	ret_reg &seed[27]
	call_c   build_ref_47()
	call_c   build_ref_403()
	call_c   Dyam_Seed_Start(&ref[47],&ref[403],I(0),fun1,1)
	call_c   build_ref_402()
	call_c   Dyam_Seed_Add_Comp(&ref[402],fun148,0)
	call_c   Dyam_Seed_End()
	move_ret seed[27]
	c_ret

;; TERM 402: '*GUARD*'(tag_compile_lemma_coanchors([_B = _C|_D], _E, _F, (check_coanchor_in_anchor(_G, _H) , _I)))
c_code local build_ref_402
	ret_reg &ref[402]
	call_c   build_ref_48()
	call_c   build_ref_401()
	call_c   Dyam_Create_Unary(&ref[48],&ref[401])
	move_ret ref[402]
	c_ret

;; TERM 401: tag_compile_lemma_coanchors([_B = _C|_D], _E, _F, (check_coanchor_in_anchor(_G, _H) , _I))
c_code local build_ref_401
	ret_reg &ref[401]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_1325()
	call_c   Dyam_Create_Binary(&ref[1325],V(1),V(2))
	move_ret R(0)
	call_c   Dyam_Create_List(R(0),V(3))
	move_ret R(0)
	call_c   build_ref_1326()
	call_c   Dyam_Create_Binary(&ref[1326],V(6),V(7))
	move_ret R(1)
	call_c   Dyam_Create_Binary(I(4),R(1),V(8))
	move_ret R(1)
	call_c   build_ref_1231()
	call_c   Dyam_Term_Start(&ref[1231],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_End()
	move_ret ref[401]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1326: check_coanchor_in_anchor
c_code local build_ref_1326
	ret_reg &ref[1326]
	call_c   Dyam_Create_Atom("check_coanchor_in_anchor")
	move_ret ref[1326]
	c_ret

;; TERM 1325: =
c_code local build_ref_1325
	ret_reg &ref[1325]
	call_c   Dyam_Create_Atom("=")
	move_ret ref[1325]
	c_ret

;; TERM 408: 'Node ~w not referenced as a coanchor for family ~w\n'
c_code local build_ref_408
	ret_reg &ref[408]
	call_c   Dyam_Create_Atom("Node ~w not referenced as a coanchor for family ~w\n")
	move_ret ref[408]
	c_ret

;; TERM 409: [_B,_F]
c_code local build_ref_409
	ret_reg &ref[409]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(5),I(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(1),R(0))
	move_ret ref[409]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun147[3]=[2,build_ref_408,build_ref_409]

pl_code local fun147
	call_c   Dyam_Remove_Choice()
	move     &ref[408], R(0)
	move     0, R(1)
	move     &ref[409], R(2)
	move     S(5), R(3)
	pl_call  pred_error_2()
	pl_jump  fun110()

;; TERM 404: tag_coanchor(_F, _B, _G, _E)
c_code local build_ref_404
	ret_reg &ref[404]
	call_c   build_ref_1327()
	call_c   Dyam_Term_Start(&ref[1327],4)
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[404]
	c_ret

;; TERM 1327: tag_coanchor
c_code local build_ref_1327
	ret_reg &ref[1327]
	call_c   Dyam_Create_Atom("tag_coanchor")
	move_ret ref[1327]
	c_ret

c_code local build_seed_107
	ret_reg &seed[107]
	call_c   build_ref_48()
	call_c   build_ref_406()
	call_c   Dyam_Seed_Start(&ref[48],&ref[406],I(0),fun3,1)
	call_c   build_ref_407()
	call_c   Dyam_Seed_Add_Comp(&ref[407],&ref[406],0)
	call_c   Dyam_Seed_End()
	move_ret seed[107]
	c_ret

;; TERM 407: '*GUARD*'(tag_compile_lemma_coanchors(_D, _E, _F, _I)) :> '$$HOLE$$'
c_code local build_ref_407
	ret_reg &ref[407]
	call_c   build_ref_406()
	call_c   Dyam_Create_Binary(I(9),&ref[406],I(7))
	move_ret ref[407]
	c_ret

;; TERM 406: '*GUARD*'(tag_compile_lemma_coanchors(_D, _E, _F, _I))
c_code local build_ref_406
	ret_reg &ref[406]
	call_c   build_ref_48()
	call_c   build_ref_405()
	call_c   Dyam_Create_Unary(&ref[48],&ref[405])
	move_ret ref[406]
	c_ret

;; TERM 405: tag_compile_lemma_coanchors(_D, _E, _F, _I)
c_code local build_ref_405
	ret_reg &ref[405]
	call_c   build_ref_1231()
	call_c   Dyam_Term_Start(&ref[1231],4)
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_End()
	move_ret ref[405]
	c_ret

long local pool_fun148[5]=[65539,build_ref_402,build_ref_404,build_seed_107,pool_fun147]

pl_code local fun148
	call_c   Dyam_Pool(pool_fun148)
	call_c   Dyam_Unify_Item(&ref[402])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun147)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[404])
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(2))
	call_c   Dyam_Reg_Load(2,V(7))
	pl_call  pred_tag_compile_lemma_check_2()
	pl_call  fun28(&seed[107],1)
	pl_jump  fun110()

;; TERM 403: '*GUARD*'(tag_compile_lemma_coanchors([_B = _C|_D], _E, _F, (check_coanchor_in_anchor(_G, _H) , _I))) :> []
c_code local build_ref_403
	ret_reg &ref[403]
	call_c   build_ref_402()
	call_c   Dyam_Create_Binary(I(9),&ref[402],I(0))
	move_ret ref[403]
	c_ret

c_code local build_seed_57
	ret_reg &seed[57]
	call_c   build_ref_47()
	call_c   build_ref_416()
	call_c   Dyam_Seed_Start(&ref[47],&ref[416],I(0),fun1,1)
	call_c   build_ref_415()
	call_c   Dyam_Seed_Add_Comp(&ref[415],fun149,0)
	call_c   Dyam_Seed_End()
	move_ret seed[57]
	c_ret

;; TERM 415: '*GUARD*'(tag_compile_subtree_handler(_B, (_C ; _D), _E, _F, _G, _H, _I, _J, _K))
c_code local build_ref_415
	ret_reg &ref[415]
	call_c   build_ref_48()
	call_c   build_ref_414()
	call_c   Dyam_Create_Unary(&ref[48],&ref[414])
	move_ret ref[415]
	c_ret

;; TERM 414: tag_compile_subtree_handler(_B, (_C ; _D), _E, _F, _G, _H, _I, _J, _K)
c_code local build_ref_414
	ret_reg &ref[414]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Binary(I(5),V(2),V(3))
	move_ret R(0)
	call_c   build_ref_1278()
	call_c   Dyam_Term_Start(&ref[1278],9)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_End()
	move_ret ref[414]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_109
	ret_reg &seed[109]
	call_c   build_ref_48()
	call_c   build_ref_418()
	call_c   Dyam_Seed_Start(&ref[48],&ref[418],I(0),fun3,1)
	call_c   build_ref_419()
	call_c   Dyam_Seed_Add_Comp(&ref[419],&ref[418],0)
	call_c   Dyam_Seed_End()
	move_ret seed[109]
	c_ret

;; TERM 419: '*GUARD*'(tag_compile_subtree(_B, _C, _E, _F, _L, _M, _I, _J, _K)) :> '$$HOLE$$'
c_code local build_ref_419
	ret_reg &ref[419]
	call_c   build_ref_418()
	call_c   Dyam_Create_Binary(I(9),&ref[418],I(7))
	move_ret ref[419]
	c_ret

;; TERM 418: '*GUARD*'(tag_compile_subtree(_B, _C, _E, _F, _L, _M, _I, _J, _K))
c_code local build_ref_418
	ret_reg &ref[418]
	call_c   build_ref_48()
	call_c   build_ref_417()
	call_c   Dyam_Create_Unary(&ref[48],&ref[417])
	move_ret ref[418]
	c_ret

;; TERM 417: tag_compile_subtree(_B, _C, _E, _F, _L, _M, _I, _J, _K)
c_code local build_ref_417
	ret_reg &ref[417]
	call_c   build_ref_1275()
	call_c   Dyam_Term_Start(&ref[1275],9)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_End()
	move_ret ref[417]
	c_ret

c_code local build_seed_110
	ret_reg &seed[110]
	call_c   build_ref_48()
	call_c   build_ref_421()
	call_c   Dyam_Seed_Start(&ref[48],&ref[421],I(0),fun3,1)
	call_c   build_ref_422()
	call_c   Dyam_Seed_Add_Comp(&ref[422],&ref[421],0)
	call_c   Dyam_Seed_End()
	move_ret seed[110]
	c_ret

;; TERM 422: '*GUARD*'(tag_compile_subtree(_B, _D, _E, _F, _L, _N, _I, _J, _K)) :> '$$HOLE$$'
c_code local build_ref_422
	ret_reg &ref[422]
	call_c   build_ref_421()
	call_c   Dyam_Create_Binary(I(9),&ref[421],I(7))
	move_ret ref[422]
	c_ret

;; TERM 421: '*GUARD*'(tag_compile_subtree(_B, _D, _E, _F, _L, _N, _I, _J, _K))
c_code local build_ref_421
	ret_reg &ref[421]
	call_c   build_ref_48()
	call_c   build_ref_420()
	call_c   Dyam_Create_Unary(&ref[48],&ref[420])
	move_ret ref[421]
	c_ret

;; TERM 420: tag_compile_subtree(_B, _D, _E, _F, _L, _N, _I, _J, _K)
c_code local build_ref_420
	ret_reg &ref[420]
	call_c   build_ref_1275()
	call_c   Dyam_Term_Start(&ref[1275],9)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_End()
	move_ret ref[420]
	c_ret

long local pool_fun149[4]=[3,build_ref_415,build_seed_109,build_seed_110]

pl_code local fun149
	call_c   Dyam_Pool(pool_fun149)
	call_c   Dyam_Unify_Item(&ref[415])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(6))
	call_c   Dyam_Reg_Load(2,V(7))
	move     V(11), R(4)
	move     S(5), R(5)
	move     V(12), R(6)
	move     S(5), R(7)
	move     V(13), R(8)
	move     S(5), R(9)
	pl_call  pred_handle_choice_5()
	pl_call  fun28(&seed[109],1)
	call_c   Dyam_Deallocate()
	pl_jump  fun28(&seed[110],1)

;; TERM 416: '*GUARD*'(tag_compile_subtree_handler(_B, (_C ; _D), _E, _F, _G, _H, _I, _J, _K)) :> []
c_code local build_ref_416
	ret_reg &ref[416]
	call_c   build_ref_415()
	call_c   Dyam_Create_Binary(I(9),&ref[415],I(0))
	move_ret ref[416]
	c_ret

c_code local build_seed_20
	ret_reg &seed[20]
	call_c   build_ref_47()
	call_c   build_ref_400()
	call_c   Dyam_Seed_Start(&ref[47],&ref[400],I(0),fun1,1)
	call_c   build_ref_399()
	call_c   Dyam_Seed_Add_Comp(&ref[399],fun146,0)
	call_c   Dyam_Seed_End()
	move_ret seed[20]
	c_ret

;; TERM 399: '*GUARD*'(tag_il_body_to_lpda_handler(_B, tag_node{id=> _C, label=> [], children=> _D, kind=> scan, adj=> _E, spine=> _F, top=> _G, bot=> _H, token=> _I, adjleft=> _J, adjright=> _K, adjwrap=> _L}, _M, _N, _O, _P, _Q, _R, (unify(_M, _N) :> _R), _S, _T, _U, _V))
c_code local build_ref_399
	ret_reg &ref[399]
	call_c   build_ref_48()
	call_c   build_ref_398()
	call_c   Dyam_Create_Unary(&ref[48],&ref[398])
	move_ret ref[399]
	c_ret

;; TERM 398: tag_il_body_to_lpda_handler(_B, tag_node{id=> _C, label=> [], children=> _D, kind=> scan, adj=> _E, spine=> _F, top=> _G, bot=> _H, token=> _I, adjleft=> _J, adjright=> _K, adjwrap=> _L}, _M, _N, _O, _P, _Q, _R, (unify(_M, _N) :> _R), _S, _T, _U, _V)
c_code local build_ref_398
	ret_reg &ref[398]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_1259()
	call_c   build_ref_1312()
	call_c   Dyam_Term_Start(&ref[1259],12)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(&ref[1312])
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_1283()
	call_c   Dyam_Create_Binary(&ref[1283],V(12),V(13))
	move_ret R(1)
	call_c   Dyam_Create_Binary(I(9),R(1),V(17))
	move_ret R(1)
	call_c   build_ref_1295()
	call_c   Dyam_Term_Start(&ref[1295],13)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_Arg(V(21))
	call_c   Dyam_Term_End()
	move_ret ref[398]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

pl_code local fun146
	call_c   build_ref_399()
	call_c   Dyam_Unify_Item(&ref[399])
	fail_ret
	pl_ret

;; TERM 400: '*GUARD*'(tag_il_body_to_lpda_handler(_B, tag_node{id=> _C, label=> [], children=> _D, kind=> scan, adj=> _E, spine=> _F, top=> _G, bot=> _H, token=> _I, adjleft=> _J, adjright=> _K, adjwrap=> _L}, _M, _N, _O, _P, _Q, _R, (unify(_M, _N) :> _R), _S, _T, _U, _V)) :> []
c_code local build_ref_400
	ret_reg &ref[400]
	call_c   build_ref_399()
	call_c   Dyam_Create_Binary(I(9),&ref[399],I(0))
	move_ret ref[400]
	c_ret

c_code local build_seed_59
	ret_reg &seed[59]
	call_c   build_ref_47()
	call_c   build_ref_448()
	call_c   Dyam_Seed_Start(&ref[47],&ref[448],I(0),fun1,1)
	call_c   build_ref_447()
	call_c   Dyam_Seed_Add_Comp(&ref[447],fun203,0)
	call_c   Dyam_Seed_End()
	move_ret seed[59]
	c_ret

;; TERM 447: '*GUARD*'(tag_compile_tree(_B, _C, _D, _E, _F, [_G,_H]))
c_code local build_ref_447
	ret_reg &ref[447]
	call_c   build_ref_48()
	call_c   build_ref_446()
	call_c   Dyam_Create_Unary(&ref[48],&ref[446])
	move_ret ref[447]
	c_ret

;; TERM 446: tag_compile_tree(_B, _C, _D, _E, _F, [_G,_H])
c_code local build_ref_446
	ret_reg &ref[446]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Tupple(6,7,I(0))
	move_ret R(0)
	call_c   build_ref_1288()
	call_c   Dyam_Term_Start(&ref[1288],6)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_End()
	move_ret ref[446]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 544: guard{goal=> tag_node{id=> _I, label=> _J, children=> _K, kind=> _L, adj=> _M, spine=> _N, top=> _O, bot=> _P, token=> _Q, adjleft=> _R, adjright=> _S, adjwrap=> _T}, plus=> _U, minus=> _V}
c_code local build_ref_544
	ret_reg &ref[544]
	call_c   build_ref_1308()
	call_c   build_ref_449()
	call_c   Dyam_Term_Start(&ref[1308],3)
	call_c   Dyam_Term_Arg(&ref[449])
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_Arg(V(21))
	call_c   Dyam_Term_End()
	move_ret ref[544]
	c_ret

;; TERM 449: tag_node{id=> _I, label=> _J, children=> _K, kind=> _L, adj=> _M, spine=> _N, top=> _O, bot=> _P, token=> _Q, adjleft=> _R, adjright=> _S, adjwrap=> _T}
c_code local build_ref_449
	ret_reg &ref[449]
	call_c   build_ref_1259()
	call_c   Dyam_Term_Start(&ref[1259],12)
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_End()
	move_ret ref[449]
	c_ret

;; TERM 542: '$CLOSURE'('$fun'(200, 0, 1129098564), '$TUPPLE'(35075411942504))
c_code local build_ref_542
	ret_reg &ref[542]
	call_c   build_ref_541()
	call_c   Dyam_Closure_Aux(fun200,&ref[541])
	move_ret ref[542]
	c_ret

c_code local build_seed_132
	ret_reg &seed[132]
	call_c   build_ref_48()
	call_c   build_ref_533()
	call_c   Dyam_Seed_Start(&ref[48],&ref[533],I(0),fun3,1)
	call_c   build_ref_534()
	call_c   Dyam_Seed_Add_Comp(&ref[534],&ref[533],0)
	call_c   Dyam_Seed_End()
	move_ret seed[132]
	c_ret

;; TERM 534: '*GUARD*'(make_tag_subst_callret(_O, _G, _H, _B1, _C1)) :> '$$HOLE$$'
c_code local build_ref_534
	ret_reg &ref[534]
	call_c   build_ref_533()
	call_c   Dyam_Create_Binary(I(9),&ref[533],I(7))
	move_ret ref[534]
	c_ret

;; TERM 533: '*GUARD*'(make_tag_subst_callret(_O, _G, _H, _B1, _C1))
c_code local build_ref_533
	ret_reg &ref[533]
	call_c   build_ref_48()
	call_c   build_ref_532()
	call_c   Dyam_Create_Unary(&ref[48],&ref[532])
	move_ret ref[533]
	c_ret

;; TERM 532: make_tag_subst_callret(_O, _G, _H, _B1, _C1)
c_code local build_ref_532
	ret_reg &ref[532]
	call_c   build_ref_1323()
	call_c   Dyam_Term_Start(&ref[1323],5)
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(27))
	call_c   Dyam_Term_Arg(V(28))
	call_c   Dyam_Term_End()
	move_ret ref[532]
	c_ret

;; TERM 536: '*SAFIRST*'(_B1) :> _D1
c_code local build_ref_536
	ret_reg &ref[536]
	call_c   build_ref_535()
	call_c   Dyam_Create_Binary(I(9),&ref[535],V(29))
	move_ret ref[536]
	c_ret

;; TERM 535: '*SAFIRST*'(_B1)
c_code local build_ref_535
	ret_reg &ref[535]
	call_c   build_ref_1328()
	call_c   Dyam_Create_Unary(&ref[1328],V(27))
	move_ret ref[535]
	c_ret

;; TERM 1328: '*SAFIRST*'
c_code local build_ref_1328
	ret_reg &ref[1328]
	call_c   Dyam_Create_Atom("*SAFIRST*")
	move_ret ref[1328]
	c_ret

;; TERM 539: '*LAST*'(_C1)
c_code local build_ref_539
	ret_reg &ref[539]
	call_c   build_ref_1329()
	call_c   Dyam_Create_Unary(&ref[1329],V(28))
	move_ret ref[539]
	c_ret

;; TERM 1329: '*LAST*'
c_code local build_ref_1329
	ret_reg &ref[1329]
	call_c   Dyam_Create_Atom("*LAST*")
	move_ret ref[1329]
	c_ret

c_code local build_seed_121
	ret_reg &seed[121]
	call_c   build_ref_48()
	call_c   build_ref_475()
	call_c   Dyam_Seed_Start(&ref[48],&ref[475],I(0),fun3,1)
	call_c   build_ref_476()
	call_c   Dyam_Seed_Add_Comp(&ref[476],&ref[475],0)
	call_c   Dyam_Seed_End()
	move_ret seed[121]
	c_ret

;; TERM 476: '*GUARD*'(body_to_lpda(_B, _U, _M1, _O1, _E)) :> '$$HOLE$$'
c_code local build_ref_476
	ret_reg &ref[476]
	call_c   build_ref_475()
	call_c   Dyam_Create_Binary(I(9),&ref[475],I(7))
	move_ret ref[476]
	c_ret

;; TERM 475: '*GUARD*'(body_to_lpda(_B, _U, _M1, _O1, _E))
c_code local build_ref_475
	ret_reg &ref[475]
	call_c   build_ref_48()
	call_c   build_ref_474()
	call_c   Dyam_Create_Unary(&ref[48],&ref[474])
	move_ret ref[475]
	c_ret

;; TERM 474: body_to_lpda(_B, _U, _M1, _O1, _E)
c_code local build_ref_474
	ret_reg &ref[474]
	call_c   build_ref_1319()
	call_c   Dyam_Term_Start(&ref[1319],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_Arg(V(38))
	call_c   Dyam_Term_Arg(V(40))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[474]
	c_ret

c_code local build_seed_131
	ret_reg &seed[131]
	call_c   build_ref_48()
	call_c   build_ref_523()
	call_c   Dyam_Seed_Start(&ref[48],&ref[523],I(0),fun3,1)
	call_c   build_ref_524()
	call_c   Dyam_Seed_Add_Comp(&ref[524],&ref[523],0)
	call_c   Dyam_Seed_End()
	move_ret seed[131]
	c_ret

;; TERM 524: '*GUARD*'(tag_compile_subtree(_B, tag_node{id=> _I, label=> _J, children=> _K, kind=> _L, adj=> _M, spine=> _N, top=> _O, bot=> _P, token=> _Q, adjleft=> _R, adjright=> _S, adjwrap=> _T}, _G, _H, _Q1, _E2, _E, _F, _N1)) :> '$$HOLE$$'
c_code local build_ref_524
	ret_reg &ref[524]
	call_c   build_ref_523()
	call_c   Dyam_Create_Binary(I(9),&ref[523],I(7))
	move_ret ref[524]
	c_ret

;; TERM 523: '*GUARD*'(tag_compile_subtree(_B, tag_node{id=> _I, label=> _J, children=> _K, kind=> _L, adj=> _M, spine=> _N, top=> _O, bot=> _P, token=> _Q, adjleft=> _R, adjright=> _S, adjwrap=> _T}, _G, _H, _Q1, _E2, _E, _F, _N1))
c_code local build_ref_523
	ret_reg &ref[523]
	call_c   build_ref_48()
	call_c   build_ref_522()
	call_c   Dyam_Create_Unary(&ref[48],&ref[522])
	move_ret ref[523]
	c_ret

;; TERM 522: tag_compile_subtree(_B, tag_node{id=> _I, label=> _J, children=> _K, kind=> _L, adj=> _M, spine=> _N, top=> _O, bot=> _P, token=> _Q, adjleft=> _R, adjright=> _S, adjwrap=> _T}, _G, _H, _Q1, _E2, _E, _F, _N1)
c_code local build_ref_522
	ret_reg &ref[522]
	call_c   build_ref_1275()
	call_c   build_ref_449()
	call_c   Dyam_Term_Start(&ref[1275],9)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(&ref[449])
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(42))
	call_c   Dyam_Term_Arg(V(56))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(39))
	call_c   Dyam_Term_End()
	move_ret ref[522]
	c_ret

pl_code local fun178
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(29),V(63))
	fail_ret
	pl_jump  fun110()

;; TERM 500: tagfilter((_B ^ _G ^ _H ^ _J ^ _O ^ _P2))
c_code local build_ref_500
	ret_reg &ref[500]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1307()
	call_c   Dyam_Create_Binary(&ref[1307],V(14),V(67))
	move_ret R(0)
	call_c   Dyam_Create_Binary(&ref[1307],V(9),R(0))
	move_ret R(0)
	call_c   Dyam_Create_Binary(&ref[1307],V(7),R(0))
	move_ret R(0)
	call_c   Dyam_Create_Binary(&ref[1307],V(6),R(0))
	move_ret R(0)
	call_c   Dyam_Create_Binary(&ref[1307],V(1),R(0))
	move_ret R(0)
	call_c   build_ref_1330()
	call_c   Dyam_Create_Unary(&ref[1330],R(0))
	move_ret ref[500]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1330: tagfilter
c_code local build_ref_1330
	ret_reg &ref[1330]
	call_c   Dyam_Create_Atom("tagfilter")
	move_ret ref[1330]
	c_ret

;; TERM 501: lctag(_B, _Q2, _R2, _S2)
c_code local build_ref_501
	ret_reg &ref[501]
	call_c   build_ref_1331()
	call_c   Dyam_Term_Start(&ref[1331],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(68))
	call_c   Dyam_Term_Arg(V(69))
	call_c   Dyam_Term_Arg(V(70))
	call_c   Dyam_Term_End()
	move_ret ref[501]
	c_ret

;; TERM 1331: lctag
c_code local build_ref_1331
	ret_reg &ref[1331]
	call_c   Dyam_Create_Atom("lctag")
	move_ret ref[1331]
	c_ret

c_code local build_seed_128
	ret_reg &seed[128]
	call_c   build_ref_48()
	call_c   build_ref_503()
	call_c   Dyam_Seed_Start(&ref[48],&ref[503],I(0),fun3,1)
	call_c   build_ref_504()
	call_c   Dyam_Seed_Add_Comp(&ref[504],&ref[503],0)
	call_c   Dyam_Seed_End()
	move_ret seed[128]
	c_ret

;; TERM 504: '*GUARD*'(body_to_lpda(_B, _P2, _L2, _D1, _E)) :> '$$HOLE$$'
c_code local build_ref_504
	ret_reg &ref[504]
	call_c   build_ref_503()
	call_c   Dyam_Create_Binary(I(9),&ref[503],I(7))
	move_ret ref[504]
	c_ret

;; TERM 503: '*GUARD*'(body_to_lpda(_B, _P2, _L2, _D1, _E))
c_code local build_ref_503
	ret_reg &ref[503]
	call_c   build_ref_48()
	call_c   build_ref_502()
	call_c   Dyam_Create_Unary(&ref[48],&ref[502])
	move_ret ref[503]
	c_ret

;; TERM 502: body_to_lpda(_B, _P2, _L2, _D1, _E)
c_code local build_ref_502
	ret_reg &ref[502]
	call_c   build_ref_1319()
	call_c   Dyam_Term_Start(&ref[1319],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(67))
	call_c   Dyam_Term_Arg(V(63))
	call_c   Dyam_Term_Arg(V(29))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[502]
	c_ret

long local pool_fun179[4]=[3,build_ref_500,build_ref_501,build_seed_128]

pl_code local fun184
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(63),V(56))
	fail_ret
fun179:
	call_c   Dyam_Choice(fun178)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[500])
	pl_call  Object_1(&ref[501])
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(67))
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(71), R(4)
	move     S(5), R(5)
	pl_call  pred_tag_numbervars_3()
	pl_call  fun28(&seed[128],1)
	pl_jump  fun110()


;; TERM 518: debug
c_code local build_ref_518
	ret_reg &ref[518]
	call_c   Dyam_Create_Atom("debug")
	move_ret ref[518]
	c_ret

c_code local build_seed_130
	ret_reg &seed[130]
	call_c   build_ref_48()
	call_c   build_ref_520()
	call_c   Dyam_Seed_Start(&ref[48],&ref[520],I(0),fun3,1)
	call_c   build_ref_521()
	call_c   Dyam_Seed_Add_Comp(&ref[521],&ref[520],0)
	call_c   Dyam_Seed_End()
	move_ret seed[130]
	c_ret

;; TERM 521: '*GUARD*'(body_to_lpda(_B, format('Selecting cat=~w left=~w tree ~w\n', [_O,_G,_B]), _E2, _L2, _E)) :> '$$HOLE$$'
c_code local build_ref_521
	ret_reg &ref[521]
	call_c   build_ref_520()
	call_c   Dyam_Create_Binary(I(9),&ref[520],I(7))
	move_ret ref[521]
	c_ret

;; TERM 520: '*GUARD*'(body_to_lpda(_B, format('Selecting cat=~w left=~w tree ~w\n', [_O,_G,_B]), _E2, _L2, _E))
c_code local build_ref_520
	ret_reg &ref[520]
	call_c   build_ref_48()
	call_c   build_ref_519()
	call_c   Dyam_Create_Unary(&ref[48],&ref[519])
	move_ret ref[520]
	c_ret

;; TERM 519: body_to_lpda(_B, format('Selecting cat=~w left=~w tree ~w\n', [_O,_G,_B]), _E2, _L2, _E)
c_code local build_ref_519
	ret_reg &ref[519]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_148()
	call_c   Dyam_Create_List(V(6),&ref[148])
	move_ret R(0)
	call_c   Dyam_Create_List(V(14),R(0))
	move_ret R(0)
	call_c   build_ref_1332()
	call_c   build_ref_1333()
	call_c   Dyam_Create_Binary(&ref[1332],&ref[1333],R(0))
	move_ret R(0)
	call_c   build_ref_1319()
	call_c   Dyam_Term_Start(&ref[1319],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(56))
	call_c   Dyam_Term_Arg(V(63))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[519]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1333: 'Selecting cat=~w left=~w tree ~w\n'
c_code local build_ref_1333
	ret_reg &ref[1333]
	call_c   Dyam_Create_Atom("Selecting cat=~w left=~w tree ~w\n")
	move_ret ref[1333]
	c_ret

;; TERM 1332: format
c_code local build_ref_1332
	ret_reg &ref[1332]
	call_c   Dyam_Create_Atom("format")
	move_ret ref[1332]
	c_ret

;; TERM 148: [_B]
c_code local build_ref_148
	ret_reg &ref[148]
	call_c   Dyam_Create_List(V(1),I(0))
	move_ret ref[148]
	c_ret

long local pool_fun185[5]=[131074,build_ref_518,build_seed_130,pool_fun179,pool_fun179]

pl_code local fun185
	call_c   Dyam_Update_Choice(fun184)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[518])
	call_c   Dyam_Cut()
	pl_call  fun28(&seed[130],1)
	pl_jump  fun179()

;; TERM 483: guide(strip)
c_code local build_ref_483
	ret_reg &ref[483]
	call_c   build_ref_1252()
	call_c   build_ref_1249()
	call_c   Dyam_Create_Unary(&ref[1252],&ref[1249])
	move_ret ref[483]
	c_ret

;; TERM 517: '$CLOSURE'('$fun'(183, 0, 1128941288), '$TUPPLE'(35075411941260))
c_code local build_ref_517
	ret_reg &ref[517]
	call_c   build_ref_516()
	call_c   Dyam_Closure_Aux(fun183,&ref[516])
	move_ret ref[517]
	c_ret

;; TERM 514: '$CLOSURE'('$fun'(181, 0, 1128932216), '$TUPPLE'(35075411941728))
c_code local build_ref_514
	ret_reg &ref[514]
	call_c   build_ref_513()
	call_c   Dyam_Closure_Aux(fun181,&ref[513])
	move_ret ref[514]
	c_ret

c_code local build_seed_129
	ret_reg &seed[129]
	call_c   build_ref_48()
	call_c   build_ref_509()
	call_c   Dyam_Seed_Start(&ref[48],&ref[509],I(0),fun3,1)
	call_c   build_ref_510()
	call_c   Dyam_Seed_Add_Comp(&ref[510],&ref[509],0)
	call_c   Dyam_Seed_End()
	move_ret seed[129]
	c_ret

;; TERM 510: '*GUARD*'(make_tag_subst_callret(_F2, _G, _H, _N2, _O2)) :> '$$HOLE$$'
c_code local build_ref_510
	ret_reg &ref[510]
	call_c   build_ref_509()
	call_c   Dyam_Create_Binary(I(9),&ref[509],I(7))
	move_ret ref[510]
	c_ret

;; TERM 509: '*GUARD*'(make_tag_subst_callret(_F2, _G, _H, _N2, _O2))
c_code local build_ref_509
	ret_reg &ref[509]
	call_c   build_ref_48()
	call_c   build_ref_508()
	call_c   Dyam_Create_Unary(&ref[48],&ref[508])
	move_ret ref[509]
	c_ret

;; TERM 508: make_tag_subst_callret(_F2, _G, _H, _N2, _O2)
c_code local build_ref_508
	ret_reg &ref[508]
	call_c   build_ref_1323()
	call_c   Dyam_Term_Start(&ref[1323],5)
	call_c   Dyam_Term_Arg(V(57))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(65))
	call_c   Dyam_Term_Arg(V(66))
	call_c   Dyam_Term_End()
	move_ret ref[508]
	c_ret

;; TERM 511: '*GUIDE*'(_N2, _M2, _E2)
c_code local build_ref_511
	ret_reg &ref[511]
	call_c   build_ref_1334()
	call_c   Dyam_Term_Start(&ref[1334],3)
	call_c   Dyam_Term_Arg(V(65))
	call_c   Dyam_Term_Arg(V(64))
	call_c   Dyam_Term_Arg(V(56))
	call_c   Dyam_Term_End()
	move_ret ref[511]
	c_ret

;; TERM 1334: '*GUIDE*'
c_code local build_ref_1334
	ret_reg &ref[1334]
	call_c   Dyam_Create_Atom("*GUIDE*")
	move_ret ref[1334]
	c_ret

long local pool_fun181[4]=[65538,build_seed_129,build_ref_511,pool_fun179]

pl_code local fun181
	call_c   Dyam_Pool(pool_fun181)
	call_c   Dyam_Allocate(0)
	pl_call  fun28(&seed[129],1)
	call_c   Dyam_Unify(V(63),&ref[511])
	fail_ret
	pl_jump  fun179()

;; TERM 513: '$TUPPLE'(35075411941728)
c_code local build_ref_513
	ret_reg &ref[513]
	call_c   Dyam_Start_Tupple(0,157827072)
	call_c   Dyam_Write_Tupple(29,268435459)
	call_c   Dyam_Almost_End_Tupple(58,4194304)
	move_ret ref[513]
	c_ret

long local pool_fun182[3]=[2,build_ref_514,build_ref_397]

pl_code local fun182
	call_c   Dyam_Remove_Choice()
	move     &ref[514], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Load(4,V(1))
	move     &ref[397], R(6)
	move     0, R(7)
	move     V(64), R(8)
	move     S(5), R(9)
	call_c   Dyam_Reg_Deallocate(5)
fun177:
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Reg_Bind(2,V(5))
	call_c   Dyam_Multi_Reg_Bind(4,2,3)
	call_c   Dyam_Choice(fun176)
	pl_call  fun46(&seed[126],1)
	pl_fail


;; TERM 507: '$CLOSURE'('$fun'(180, 0, 1128912736), '$TUPPLE'(35075411941552))
c_code local build_ref_507
	ret_reg &ref[507]
	call_c   build_ref_506()
	call_c   Dyam_Closure_Aux(fun180,&ref[506])
	move_ret ref[507]
	c_ret

;; TERM 498: left
c_code local build_ref_498
	ret_reg &ref[498]
	call_c   Dyam_Create_Atom("left")
	move_ret ref[498]
	c_ret

;; TERM 499: '*GUIDE*'(_J2, _G2, _E2)
c_code local build_ref_499
	ret_reg &ref[499]
	call_c   build_ref_1334()
	call_c   Dyam_Term_Start(&ref[1334],3)
	call_c   Dyam_Term_Arg(V(61))
	call_c   Dyam_Term_Arg(V(58))
	call_c   Dyam_Term_Arg(V(56))
	call_c   Dyam_Term_End()
	move_ret ref[499]
	c_ret

long local pool_fun180[4]=[65538,build_ref_498,build_ref_499,pool_fun179]

pl_code local fun180
	call_c   Dyam_Pool(pool_fun180)
	call_c   Dyam_Allocate(0)
	move     V(59), R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(60), R(4)
	move     S(5), R(5)
	pl_call  pred_tag_numbervars_3()
	move     &ref[498], R(0)
	move     0, R(1)
	call_c   Dyam_Reg_Load(2,V(57))
	call_c   Dyam_Reg_Load(4,V(57))
	call_c   Dyam_Reg_Load(6,V(6))
	call_c   Dyam_Reg_Load(8,V(59))
	move     V(61), R(10)
	move     S(5), R(11)
	move     V(62), R(12)
	move     S(5), R(13)
	pl_call  pred_make_tig_callret_7()
	call_c   Dyam_Unify(V(63),&ref[499])
	fail_ret
	pl_jump  fun179()

;; TERM 506: '$TUPPLE'(35075411941552)
c_code local build_ref_506
	ret_reg &ref[506]
	call_c   Dyam_Start_Tupple(0,157827072)
	call_c   Dyam_Write_Tupple(29,268435459)
	call_c   Dyam_Almost_End_Tupple(58,268435456)
	move_ret ref[506]
	c_ret

long local pool_fun183[5]=[65539,build_ref_161,build_ref_507,build_ref_498,pool_fun182]

pl_code local fun183
	call_c   Dyam_Pool(pool_fun183)
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun182)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(13),&ref[161])
	fail_ret
	call_c   Dyam_Cut()
	move     &ref[507], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Load(4,V(1))
	move     &ref[498], R(6)
	move     0, R(7)
	move     V(58), R(8)
	move     S(5), R(9)
	call_c   Dyam_Reg_Deallocate(5)
	pl_jump  fun177()

;; TERM 516: '$TUPPLE'(35075411941260)
c_code local build_ref_516
	ret_reg &ref[516]
	call_c   Dyam_Start_Tupple(0,157859840)
	call_c   Dyam_Almost_End_Tupple(29,268435459)
	move_ret ref[516]
	c_ret

long local pool_fun186[4]=[65538,build_ref_483,build_ref_517,pool_fun185]

long local pool_fun187[3]=[65537,build_seed_131,pool_fun186]

pl_code local fun187
	call_c   Dyam_Remove_Choice()
	pl_call  fun28(&seed[131],1)
fun186:
	call_c   Dyam_Choice(fun185)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[483])
	call_c   Dyam_Cut()
	move     &ref[517], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Load(4,V(9))
	move     V(57), R(6)
	move     S(5), R(7)
	call_c   Dyam_Reg_Deallocate(4)
fun174:
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Reg_Bind(2,V(4))
	call_c   Dyam_Multi_Reg_Bind(4,2,2)
	call_c   Dyam_Choice(fun173)
	pl_call  fun46(&seed[124],1)
	pl_fail



c_code local build_seed_123
	ret_reg &seed[123]
	call_c   build_ref_48()
	call_c   build_ref_481()
	call_c   Dyam_Seed_Start(&ref[48],&ref[481],I(0),fun3,1)
	call_c   build_ref_482()
	call_c   Dyam_Seed_Add_Comp(&ref[482],&ref[481],0)
	call_c   Dyam_Seed_End()
	move_ret seed[123]
	c_ret

;; TERM 482: '*GUARD*'(tag_compile_subtree_adj(_B, tag_node{id=> _I, label=> _J, children=> _K, kind=> _L, adj=> _M, spine=> _N, top=> _O, bot=> _P, token=> _Q, adjleft=> _R, adjright=> _S, adjwrap=> _T}, _G, _H, _Q1, _E2, _E, _F, _W1, _X1, _N1, _C2)) :> '$$HOLE$$'
c_code local build_ref_482
	ret_reg &ref[482]
	call_c   build_ref_481()
	call_c   Dyam_Create_Binary(I(9),&ref[481],I(7))
	move_ret ref[482]
	c_ret

;; TERM 481: '*GUARD*'(tag_compile_subtree_adj(_B, tag_node{id=> _I, label=> _J, children=> _K, kind=> _L, adj=> _M, spine=> _N, top=> _O, bot=> _P, token=> _Q, adjleft=> _R, adjright=> _S, adjwrap=> _T}, _G, _H, _Q1, _E2, _E, _F, _W1, _X1, _N1, _C2))
c_code local build_ref_481
	ret_reg &ref[481]
	call_c   build_ref_48()
	call_c   build_ref_480()
	call_c   Dyam_Create_Unary(&ref[48],&ref[480])
	move_ret ref[481]
	c_ret

;; TERM 480: tag_compile_subtree_adj(_B, tag_node{id=> _I, label=> _J, children=> _K, kind=> _L, adj=> _M, spine=> _N, top=> _O, bot=> _P, token=> _Q, adjleft=> _R, adjright=> _S, adjwrap=> _T}, _G, _H, _Q1, _E2, _E, _F, _W1, _X1, _N1, _C2)
c_code local build_ref_480
	ret_reg &ref[480]
	call_c   build_ref_1335()
	call_c   build_ref_449()
	call_c   Dyam_Term_Start(&ref[1335],12)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(&ref[449])
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(42))
	call_c   Dyam_Term_Arg(V(56))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(48))
	call_c   Dyam_Term_Arg(V(49))
	call_c   Dyam_Term_Arg(V(39))
	call_c   Dyam_Term_Arg(V(54))
	call_c   Dyam_Term_End()
	move_ret ref[480]
	c_ret

;; TERM 1335: tag_compile_subtree_adj
c_code local build_ref_1335
	ret_reg &ref[1335]
	call_c   Dyam_Create_Atom("tag_compile_subtree_adj")
	move_ret ref[1335]
	c_ret

long local pool_fun188[5]=[131074,build_ref_161,build_seed_123,pool_fun187,pool_fun186]

pl_code local fun189
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(42),V(40))
	fail_ret
fun188:
	call_c   Dyam_Choice(fun187)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(13),&ref[161])
	fail_ret
	call_c   Dyam_Unify(V(8),V(43))
	fail_ret
	call_c   Dyam_Unify(V(9),V(44))
	fail_ret
	call_c   Dyam_Unify(V(10),V(45))
	fail_ret
	call_c   Dyam_Unify(V(11),V(46))
	fail_ret
	call_c   Dyam_Unify(V(12),&ref[161])
	fail_ret
	call_c   Dyam_Unify(V(13),V(47))
	fail_ret
	call_c   Dyam_Unify(V(14),V(48))
	fail_ret
	call_c   Dyam_Unify(V(15),V(49))
	fail_ret
	call_c   Dyam_Unify(V(16),V(50))
	fail_ret
	call_c   Dyam_Unify(V(17),V(51))
	fail_ret
	call_c   Dyam_Unify(V(18),V(52))
	fail_ret
	call_c   Dyam_Unify(V(19),V(53))
	fail_ret
	call_c   Dyam_Cut()
	move     V(54), R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(55), R(4)
	move     S(5), R(5)
	pl_call  pred_tag_numbervars_3()
	pl_call  fun28(&seed[123],1)
	pl_jump  fun186()


long local pool_fun190[3]=[65537,build_seed_121,pool_fun188]

pl_code local fun193
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(38),V(30))
	fail_ret
	call_c   Dyam_Unify(V(39),V(33))
	fail_ret
fun190:
	pl_call  fun28(&seed[121],1)
	call_c   Dyam_Choice(fun189)
	call_c   Dyam_Set_Cut()
	pl_fail


;; TERM 531: '$CLOSURE'('$fun'(192, 0, 1129025848), '$TUPPLE'(35075411942344))
c_code local build_ref_531
	ret_reg &ref[531]
	call_c   build_ref_530()
	call_c   Dyam_Closure_Aux(fun192,&ref[530])
	move_ret ref[531]
	c_ret

;; TERM 527: '$CLOSURE'('$fun'(191, 0, 1129001200), '$TUPPLE'(35075411942192))
c_code local build_ref_527
	ret_reg &ref[527]
	call_c   build_ref_526()
	call_c   Dyam_Closure_Aux(fun191,&ref[526])
	move_ret ref[527]
	c_ret

c_code local build_seed_120
	ret_reg &seed[120]
	call_c   build_ref_48()
	call_c   build_ref_472()
	call_c   Dyam_Seed_Start(&ref[48],&ref[472],I(0),fun3,1)
	call_c   build_ref_473()
	call_c   Dyam_Seed_Add_Comp(&ref[473],&ref[472],0)
	call_c   Dyam_Seed_End()
	move_ret seed[120]
	c_ret

;; TERM 473: '*GUARD*'(body_to_lpda(_B, verbose!struct(_B, _I1), _E1, _M1, _E)) :> '$$HOLE$$'
c_code local build_ref_473
	ret_reg &ref[473]
	call_c   build_ref_472()
	call_c   Dyam_Create_Binary(I(9),&ref[472],I(7))
	move_ret ref[473]
	c_ret

;; TERM 472: '*GUARD*'(body_to_lpda(_B, verbose!struct(_B, _I1), _E1, _M1, _E))
c_code local build_ref_472
	ret_reg &ref[472]
	call_c   build_ref_48()
	call_c   build_ref_471()
	call_c   Dyam_Create_Unary(&ref[48],&ref[471])
	move_ret ref[472]
	c_ret

;; TERM 471: body_to_lpda(_B, verbose!struct(_B, _I1), _E1, _M1, _E)
c_code local build_ref_471
	ret_reg &ref[471]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1336()
	call_c   Dyam_Create_Binary(&ref[1336],V(1),V(34))
	move_ret R(0)
	call_c   build_ref_1319()
	call_c   Dyam_Term_Start(&ref[1319],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(30))
	call_c   Dyam_Term_Arg(V(38))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[471]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1336: verbose!struct
c_code local build_ref_1336
	ret_reg &ref[1336]
	call_c   build_ref_1337()
	call_c   Dyam_Create_Atom_Module("struct",&ref[1337])
	move_ret ref[1336]
	c_ret

;; TERM 1337: verbose
c_code local build_ref_1337
	ret_reg &ref[1337]
	call_c   Dyam_Create_Atom("verbose")
	move_ret ref[1337]
	c_ret

long local pool_fun191[3]=[65537,build_seed_120,pool_fun190]

pl_code local fun191
	call_c   Dyam_Pool(pool_fun191)
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(34))
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(37), R(4)
	move     S(5), R(5)
	pl_call  pred_tag_numbervars_3()
	pl_call  fun28(&seed[120],1)
	call_c   Dyam_Reg_Load(0,V(34))
	call_c   Dyam_Reg_Load(2,V(33))
	move     V(39), R(4)
	move     S(5), R(5)
	pl_call  pred_extend_tupple_3()
	pl_jump  fun190()

;; TERM 526: '$TUPPLE'(35075411942192)
c_code local build_ref_526
	ret_reg &ref[526]
	call_c   Dyam_Start_Tupple(0,234880768)
	call_c   Dyam_Almost_End_Tupple(29,427819008)
	move_ret ref[526]
	c_ret

;; TERM 528: tag_anchor{family=> _I1, coanchors=> _J1, equations=> _K1}
c_code local build_ref_528
	ret_reg &ref[528]
	call_c   build_ref_1241()
	call_c   Dyam_Term_Start(&ref[1241],3)
	call_c   Dyam_Term_Arg(V(34))
	call_c   Dyam_Term_Arg(V(35))
	call_c   Dyam_Term_Arg(V(36))
	call_c   Dyam_Term_End()
	move_ret ref[528]
	c_ret

long local pool_fun192[3]=[2,build_ref_527,build_ref_528]

pl_code local fun192
	call_c   Dyam_Pool(pool_fun192)
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Cut()
	move     &ref[527], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Load(4,V(1))
	call_c   Dyam_Reg_Load(6,V(5))
	move     &ref[528], R(8)
	move     S(5), R(9)
	call_c   Dyam_Reg_Deallocate(5)
	pl_jump  fun52()

;; TERM 530: '$TUPPLE'(35075411942344)
c_code local build_ref_530
	ret_reg &ref[530]
	call_c   Dyam_Start_Tupple(0,234880768)
	call_c   Dyam_Almost_End_Tupple(29,419430400)
	move_ret ref[530]
	c_ret

long local pool_fun194[3]=[65537,build_ref_531,pool_fun190]

long local pool_fun197[3]=[65537,build_ref_284,pool_fun194]

long local pool_fun198[3]=[65537,build_ref_539,pool_fun197]

pl_code local fun198
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(30),&ref[539])
	fail_ret
fun197:
	call_c   Dyam_Unify(V(31),&ref[284])
	fail_ret
fun194:
	call_c   Dyam_Reg_Load(0,V(2))
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(32), R(4)
	move     S(5), R(5)
	pl_call  pred_tag_numbervars_3()
	call_c   Dyam_Reg_Load(0,V(27))
	move     V(33), R(2)
	move     S(5), R(3)
	pl_call  pred_tupple_2()
	call_c   Dyam_Choice(fun193)
	call_c   Dyam_Set_Cut()
	move     &ref[531], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Load(4,V(1))
	call_c   Dyam_Reg_Deallocate(3)
fun171:
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Reg_Bind(2,V(3))
	call_c   Dyam_Reg_Bind(4,V(2))
	call_c   Dyam_Choice(fun170)
	pl_call  fun46(&seed[118],1)
	pl_fail




;; TERM 537: light_tabular tag((_Y / _Z))
c_code local build_ref_537
	ret_reg &ref[537]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1294()
	call_c   Dyam_Create_Binary(&ref[1294],V(24),V(25))
	move_ret R(0)
	call_c   build_ref_1255()
	call_c   Dyam_Create_Unary(&ref[1255],R(0))
	move_ret R(0)
	call_c   build_ref_1338()
	call_c   Dyam_Create_Unary(&ref[1338],R(0))
	move_ret ref[537]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1338: light_tabular
c_code local build_ref_1338
	ret_reg &ref[1338]
	call_c   Dyam_Create_Atom("light_tabular")
	move_ret ref[1338]
	c_ret

;; TERM 538: '*LIGHTLAST*'(_C1)
c_code local build_ref_538
	ret_reg &ref[538]
	call_c   build_ref_1339()
	call_c   Dyam_Create_Unary(&ref[1339],V(28))
	move_ret ref[538]
	c_ret

;; TERM 1339: '*LIGHTLAST*'
c_code local build_ref_1339
	ret_reg &ref[1339]
	call_c   Dyam_Create_Atom("*LIGHTLAST*")
	move_ret ref[1339]
	c_ret

long local pool_fun199[7]=[131076,build_seed_132,build_ref_536,build_ref_537,build_ref_538,pool_fun198,pool_fun197]

pl_code local fun199
	call_c   Dyam_Remove_Choice()
	pl_call  fun28(&seed[132],1)
	call_c   Dyam_Unify(V(3),&ref[536])
	fail_ret
	call_c   Dyam_Choice(fun198)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[537])
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(30),&ref[538])
	fail_ret
	pl_jump  fun197()

c_code local build_seed_117
	ret_reg &seed[117]
	call_c   build_ref_48()
	call_c   build_ref_459()
	call_c   Dyam_Seed_Start(&ref[48],&ref[459],I(0),fun3,1)
	call_c   build_ref_460()
	call_c   Dyam_Seed_Add_Comp(&ref[460],&ref[459],0)
	call_c   Dyam_Seed_End()
	move_ret seed[117]
	c_ret

;; TERM 460: '*GUARD*'(make_tag_top_callret(_O, _G, _H, _B1, _C1)) :> '$$HOLE$$'
c_code local build_ref_460
	ret_reg &ref[460]
	call_c   build_ref_459()
	call_c   Dyam_Create_Binary(I(9),&ref[459],I(7))
	move_ret ref[460]
	c_ret

;; TERM 459: '*GUARD*'(make_tag_top_callret(_O, _G, _H, _B1, _C1))
c_code local build_ref_459
	ret_reg &ref[459]
	call_c   build_ref_48()
	call_c   build_ref_458()
	call_c   Dyam_Create_Unary(&ref[48],&ref[458])
	move_ret ref[459]
	c_ret

;; TERM 458: make_tag_top_callret(_O, _G, _H, _B1, _C1)
c_code local build_ref_458
	ret_reg &ref[458]
	call_c   build_ref_1340()
	call_c   Dyam_Term_Start(&ref[1340],5)
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(27))
	call_c   Dyam_Term_Arg(V(28))
	call_c   Dyam_Term_End()
	move_ret ref[458]
	c_ret

;; TERM 1340: make_tag_top_callret
c_code local build_ref_1340
	ret_reg &ref[1340]
	call_c   Dyam_Create_Atom("make_tag_top_callret")
	move_ret ref[1340]
	c_ret

;; TERM 462: '*SA-AUX-FIRST*'(_B1) :> _D1
c_code local build_ref_462
	ret_reg &ref[462]
	call_c   build_ref_461()
	call_c   Dyam_Create_Binary(I(9),&ref[461],V(29))
	move_ret ref[462]
	c_ret

;; TERM 461: '*SA-AUX-FIRST*'(_B1)
c_code local build_ref_461
	ret_reg &ref[461]
	call_c   build_ref_1341()
	call_c   Dyam_Create_Unary(&ref[1341],V(27))
	move_ret ref[461]
	c_ret

;; TERM 1341: '*SA-AUX-FIRST*'
c_code local build_ref_1341
	ret_reg &ref[1341]
	call_c   Dyam_Create_Atom("*SA-AUX-FIRST*")
	move_ret ref[1341]
	c_ret

;; TERM 463: '*SA-AUX-LAST*'(_C1)
c_code local build_ref_463
	ret_reg &ref[463]
	call_c   build_ref_1342()
	call_c   Dyam_Create_Unary(&ref[1342],V(28))
	move_ret ref[463]
	c_ret

;; TERM 1342: '*SA-AUX-LAST*'
c_code local build_ref_1342
	ret_reg &ref[1342]
	call_c   Dyam_Create_Atom("*SA-AUX-LAST*")
	move_ret ref[1342]
	c_ret

;; TERM 294: wrap
c_code local build_ref_294
	ret_reg &ref[294]
	call_c   Dyam_Create_Atom("wrap")
	move_ret ref[294]
	c_ret

long local pool_fun195[6]=[65540,build_seed_117,build_ref_462,build_ref_463,build_ref_294,pool_fun194]

pl_code local fun196
	call_c   Dyam_Remove_Choice()
fun195:
	pl_call  fun28(&seed[117],1)
	call_c   Dyam_Unify(V(3),&ref[462])
	fail_ret
	call_c   Dyam_Unify(V(30),&ref[463])
	fail_ret
	call_c   Dyam_Unify(V(31),&ref[294])
	fail_ret
	pl_jump  fun194()


;; TERM 457: tig(_B, _A1)
c_code local build_ref_457
	ret_reg &ref[457]
	call_c   build_ref_1343()
	call_c   Dyam_Create_Binary(&ref[1343],V(1),V(26))
	move_ret ref[457]
	c_ret

;; TERM 1343: tig
c_code local build_ref_1343
	ret_reg &ref[1343]
	call_c   Dyam_Create_Atom("tig")
	move_ret ref[1343]
	c_ret

long local pool_fun200[5]=[131074,build_ref_161,build_ref_457,pool_fun199,pool_fun195]

pl_code local fun200
	call_c   Dyam_Pool(pool_fun200)
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun199)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(13),&ref[161])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun196)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[457])
	call_c   Dyam_Cut()
	pl_fail

;; TERM 541: '$TUPPLE'(35075411942504)
c_code local build_ref_541
	ret_reg &ref[541]
	call_c   Dyam_Create_Simple_Tupple(0,268435224)
	move_ret ref[541]
	c_ret

;; TERM 543: tag(_J)
c_code local build_ref_543
	ret_reg &ref[543]
	call_c   build_ref_1255()
	call_c   Dyam_Create_Unary(&ref[1255],V(9))
	move_ret ref[543]
	c_ret

long local pool_fun201[3]=[2,build_ref_542,build_ref_543]

long local pool_fun202[3]=[65537,build_ref_544,pool_fun201]

pl_code local fun202
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(2),&ref[544])
	fail_ret
fun201:
	call_c   Dyam_Reg_Load(0,V(2))
	call_c   Dyam_Reg_Load(2,V(5))
	pl_call  pred_tree_info_2()
	call_c   Dyam_Reg_Load(0,V(6))
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(22), R(4)
	move     S(5), R(5)
	pl_call  pred_tag_numbervars_3()
	call_c   Dyam_Reg_Load(0,V(7))
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(23), R(4)
	move     S(5), R(5)
	pl_call  pred_tag_numbervars_3()
	call_c   DYAM_evpred_functor(V(14),V(24),V(25))
	fail_ret
	move     &ref[542], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	move     &ref[543], R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Deallocate(3)
fun168:
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Reg_Bind(2,V(3))
	call_c   Dyam_Reg_Bind(4,V(2))
	call_c   Dyam_Choice(fun167)
	pl_call  fun46(&seed[115],1)
	pl_fail



long local pool_fun203[6]=[131075,build_ref_447,build_ref_449,build_ref_261,pool_fun202,pool_fun201]

pl_code local fun203
	call_c   Dyam_Pool(pool_fun203)
	call_c   Dyam_Unify_Item(&ref[447])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun202)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(2),&ref[449])
	fail_ret
	call_c   Dyam_Unify(V(20),&ref[261])
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun201()

;; TERM 448: '*GUARD*'(tag_compile_tree(_B, _C, _D, _E, _F, [_G,_H])) :> []
c_code local build_ref_448
	ret_reg &ref[448]
	call_c   build_ref_447()
	call_c   Dyam_Create_Binary(I(9),&ref[447],I(0))
	move_ret ref[448]
	c_ret

c_code local build_seed_31
	ret_reg &seed[31]
	call_c   build_ref_52()
	call_c   build_ref_554()
	call_c   Dyam_Seed_Start(&ref[52],&ref[554],I(0),fun15,1)
	call_c   build_ref_555()
	call_c   Dyam_Seed_Add_Comp(&ref[555],fun208,0)
	call_c   Dyam_Seed_End()
	move_ret seed[31]
	c_ret

;; TERM 555: '*CITEM*'('call_decompose_args/3'(callret_tag_subst((_B / _C))), _A)
c_code local build_ref_555
	ret_reg &ref[555]
	call_c   build_ref_56()
	call_c   build_ref_552()
	call_c   Dyam_Create_Binary(&ref[56],&ref[552],V(0))
	move_ret ref[555]
	c_ret

;; TERM 552: 'call_decompose_args/3'(callret_tag_subst((_B / _C)))
c_code local build_ref_552
	ret_reg &ref[552]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1294()
	call_c   Dyam_Create_Binary(&ref[1294],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_1344()
	call_c   Dyam_Create_Unary(&ref[1344],R(0))
	move_ret R(0)
	call_c   build_ref_1232()
	call_c   Dyam_Create_Unary(&ref[1232],R(0))
	move_ret ref[552]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1344: callret_tag_subst
c_code local build_ref_1344
	ret_reg &ref[1344]
	call_c   Dyam_Create_Atom("callret_tag_subst")
	move_ret ref[1344]
	c_ret

c_code local build_seed_134
	ret_reg &seed[134]
	call_c   build_ref_48()
	call_c   build_ref_557()
	call_c   Dyam_Seed_Start(&ref[48],&ref[557],I(0),fun3,1)
	call_c   build_ref_558()
	call_c   Dyam_Seed_Add_Comp(&ref[558],&ref[557],0)
	call_c   Dyam_Seed_End()
	move_ret seed[134]
	c_ret

;; TERM 558: '*GUARD*'(make_tag_subst_callret(_G, _D, _E, _J, _K)) :> '$$HOLE$$'
c_code local build_ref_558
	ret_reg &ref[558]
	call_c   build_ref_557()
	call_c   Dyam_Create_Binary(I(9),&ref[557],I(7))
	move_ret ref[558]
	c_ret

;; TERM 557: '*GUARD*'(make_tag_subst_callret(_G, _D, _E, _J, _K))
c_code local build_ref_557
	ret_reg &ref[557]
	call_c   build_ref_48()
	call_c   build_ref_556()
	call_c   Dyam_Create_Unary(&ref[48],&ref[556])
	move_ret ref[557]
	c_ret

;; TERM 556: make_tag_subst_callret(_G, _D, _E, _J, _K)
c_code local build_ref_556
	ret_reg &ref[556]
	call_c   build_ref_1323()
	call_c   Dyam_Term_Start(&ref[1323],5)
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_End()
	move_ret ref[556]
	c_ret

;; TERM 559: [_L|_M]
c_code local build_ref_559
	ret_reg &ref[559]
	call_c   Dyam_Create_List(V(11),V(12))
	move_ret ref[559]
	c_ret

;; TERM 560: [_N|_O]
c_code local build_ref_560
	ret_reg &ref[560]
	call_c   Dyam_Create_List(V(13),V(14))
	move_ret ref[560]
	c_ret

c_code local build_seed_135
	ret_reg &seed[135]
	call_c   build_ref_48()
	call_c   build_ref_562()
	call_c   Dyam_Seed_Start(&ref[48],&ref[562],I(0),fun3,1)
	call_c   build_ref_563()
	call_c   Dyam_Seed_Add_Comp(&ref[563],&ref[562],0)
	call_c   Dyam_Seed_End()
	move_ret seed[135]
	c_ret

;; TERM 563: '*GUARD*'(append(_M, [_F,_H], _P)) :> '$$HOLE$$'
c_code local build_ref_563
	ret_reg &ref[563]
	call_c   build_ref_562()
	call_c   Dyam_Create_Binary(I(9),&ref[562],I(7))
	move_ret ref[563]
	c_ret

;; TERM 562: '*GUARD*'(append(_M, [_F,_H], _P))
c_code local build_ref_562
	ret_reg &ref[562]
	call_c   build_ref_48()
	call_c   build_ref_561()
	call_c   Dyam_Create_Unary(&ref[48],&ref[561])
	move_ret ref[562]
	c_ret

;; TERM 561: append(_M, [_F,_H], _P)
c_code local build_ref_561
	ret_reg &ref[561]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1197()
	call_c   Dyam_Create_List(V(5),&ref[1197])
	move_ret R(0)
	call_c   build_ref_1345()
	call_c   Dyam_Term_Start(&ref[1345],3)
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_End()
	move_ret ref[561]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1345: append
c_code local build_ref_1345
	ret_reg &ref[1345]
	call_c   Dyam_Create_Atom("append")
	move_ret ref[1345]
	c_ret

;; TERM 1197: [_H]
c_code local build_ref_1197
	ret_reg &ref[1197]
	call_c   Dyam_Create_List(V(7),I(0))
	move_ret ref[1197]
	c_ret

;; TERM 564: callret(_P, _O)
c_code local build_ref_564
	ret_reg &ref[564]
	call_c   build_ref_1346()
	call_c   Dyam_Create_Binary(&ref[1346],V(15),V(14))
	move_ret ref[564]
	c_ret

;; TERM 1346: callret
c_code local build_ref_1346
	ret_reg &ref[1346]
	call_c   Dyam_Create_Atom("callret")
	move_ret ref[1346]
	c_ret

long local pool_fun207[6]=[65540,build_ref_559,build_ref_560,build_seed_135,build_ref_564,pool_fun125]

pl_code local fun207
	call_c   Dyam_Remove_Choice()
	call_c   DYAM_evpred_univ(V(9),&ref[559])
	fail_ret
	call_c   DYAM_evpred_univ(V(10),&ref[560])
	fail_ret
	pl_call  fun28(&seed[135],1)
	call_c   Dyam_Unify(V(8),&ref[564])
	fail_ret
	pl_jump  fun125()

long local pool_fun208[6]=[131075,build_ref_555,build_seed_134,build_ref_348,pool_fun207,pool_fun125]

pl_code local fun208
	call_c   Dyam_Pool(pool_fun208)
	call_c   Dyam_Unify_Item(&ref[555])
	fail_ret
	call_c   DYAM_evpred_functor(V(6),V(1),V(2))
	fail_ret
	call_c   Dyam_Allocate(0)
	pl_call  fun28(&seed[134],1)
	call_c   Dyam_Choice(fun207)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_gt(V(2),N(30))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(8),&ref[348])
	fail_ret
	pl_jump  fun125()

;; TERM 554: '*FIRST*'('call_decompose_args/3'(callret_tag_subst((_B / _C)))) :> []
c_code local build_ref_554
	ret_reg &ref[554]
	call_c   build_ref_553()
	call_c   Dyam_Create_Binary(I(9),&ref[553],I(0))
	move_ret ref[554]
	c_ret

;; TERM 553: '*FIRST*'('call_decompose_args/3'(callret_tag_subst((_B / _C))))
c_code local build_ref_553
	ret_reg &ref[553]
	call_c   build_ref_52()
	call_c   build_ref_552()
	call_c   Dyam_Create_Unary(&ref[52],&ref[552])
	move_ret ref[553]
	c_ret

c_code local build_seed_30
	ret_reg &seed[30]
	call_c   build_ref_52()
	call_c   build_ref_567()
	call_c   Dyam_Seed_Start(&ref[52],&ref[567],I(0),fun15,1)
	call_c   build_ref_568()
	call_c   Dyam_Seed_Add_Comp(&ref[568],fun216,0)
	call_c   Dyam_Seed_End()
	move_ret seed[30]
	c_ret

;; TERM 568: '*CITEM*'(wrapping_predicate(tag_subst((_B / _C), _D)), _A)
c_code local build_ref_568
	ret_reg &ref[568]
	call_c   build_ref_56()
	call_c   build_ref_565()
	call_c   Dyam_Create_Binary(&ref[56],&ref[565],V(0))
	move_ret ref[568]
	c_ret

;; TERM 565: wrapping_predicate(tag_subst((_B / _C), _D))
c_code local build_ref_565
	ret_reg &ref[565]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1294()
	call_c   Dyam_Create_Binary(&ref[1294],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_1313()
	call_c   Dyam_Create_Binary(&ref[1313],R(0),V(3))
	move_ret R(0)
	call_c   build_ref_1314()
	call_c   Dyam_Create_Unary(&ref[1314],R(0))
	move_ret ref[565]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 587: '$CLOSURE'('$fun'(215, 0, 1129255524), '$TUPPLE'(35075411419792))
c_code local build_ref_587
	ret_reg &ref[587]
	call_c   build_ref_586()
	call_c   Dyam_Closure_Aux(fun215,&ref[586])
	move_ret ref[587]
	c_ret

c_code local build_seed_136
	ret_reg &seed[136]
	call_c   build_ref_48()
	call_c   build_ref_571()
	call_c   Dyam_Seed_Start(&ref[48],&ref[571],I(0),fun3,1)
	call_c   build_ref_572()
	call_c   Dyam_Seed_Add_Comp(&ref[572],&ref[571],0)
	call_c   Dyam_Seed_End()
	move_ret seed[136]
	c_ret

;; TERM 572: '*GUARD*'(make_tag_subst_callret(_I, _F, _G, _L, _M)) :> '$$HOLE$$'
c_code local build_ref_572
	ret_reg &ref[572]
	call_c   build_ref_571()
	call_c   Dyam_Create_Binary(I(9),&ref[571],I(7))
	move_ret ref[572]
	c_ret

;; TERM 571: '*GUARD*'(make_tag_subst_callret(_I, _F, _G, _L, _M))
c_code local build_ref_571
	ret_reg &ref[571]
	call_c   build_ref_48()
	call_c   build_ref_570()
	call_c   Dyam_Create_Unary(&ref[48],&ref[570])
	move_ret ref[571]
	c_ret

;; TERM 570: make_tag_subst_callret(_I, _F, _G, _L, _M)
c_code local build_ref_570
	ret_reg &ref[570]
	call_c   build_ref_1323()
	call_c   Dyam_Term_Start(&ref[1323],5)
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_End()
	move_ret ref[570]
	c_ret

;; TERM 573: [_N|_K]
c_code local build_ref_573
	ret_reg &ref[573]
	call_c   Dyam_Create_List(V(13),V(10))
	move_ret ref[573]
	c_ret

;; TERM 583: '*SA-SUBST*'(_L, _M, _S, _H)
c_code local build_ref_583
	ret_reg &ref[583]
	call_c   build_ref_1347()
	call_c   Dyam_Term_Start(&ref[1347],4)
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[583]
	c_ret

;; TERM 1347: '*SA-SUBST*'
c_code local build_ref_1347
	ret_reg &ref[1347]
	call_c   Dyam_Create_Atom("*SA-SUBST*")
	move_ret ref[1347]
	c_ret

;; TERM 582: '*WRAPPER*'(_E, [_N|_K], _W)
c_code local build_ref_582
	ret_reg &ref[582]
	call_c   build_ref_1348()
	call_c   build_ref_573()
	call_c   Dyam_Term_Start(&ref[1348],3)
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(&ref[573])
	call_c   Dyam_Term_Arg(V(22))
	call_c   Dyam_Term_End()
	move_ret ref[582]
	c_ret

;; TERM 1348: '*WRAPPER*'
c_code local build_ref_1348
	ret_reg &ref[1348]
	call_c   Dyam_Create_Atom("*WRAPPER*")
	move_ret ref[1348]
	c_ret

long local pool_fun209[3]=[2,build_ref_582,build_seed_69]

pl_code local fun210
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(22),V(19))
	fail_ret
fun209:
	call_c   DYAM_evpred_assert_1(&ref[582])
	call_c   Dyam_Deallocate()
	pl_jump  fun13(&seed[69],2)


;; TERM 578: tagfilter_cat((_J ^ _D ^ subst ^ _F ^ _G ^ _I ^ _U))
c_code local build_ref_578
	ret_reg &ref[578]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1307()
	call_c   Dyam_Create_Binary(&ref[1307],V(8),V(20))
	move_ret R(0)
	call_c   Dyam_Create_Binary(&ref[1307],V(6),R(0))
	move_ret R(0)
	call_c   Dyam_Create_Binary(&ref[1307],V(5),R(0))
	move_ret R(0)
	call_c   build_ref_284()
	call_c   Dyam_Create_Binary(&ref[1307],&ref[284],R(0))
	move_ret R(0)
	call_c   Dyam_Create_Binary(&ref[1307],V(3),R(0))
	move_ret R(0)
	call_c   Dyam_Create_Binary(&ref[1307],V(9),R(0))
	move_ret R(0)
	call_c   build_ref_1349()
	call_c   Dyam_Create_Unary(&ref[1349],R(0))
	move_ret ref[578]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1349: tagfilter_cat
c_code local build_ref_1349
	ret_reg &ref[1349]
	call_c   Dyam_Create_Atom("tagfilter_cat")
	move_ret ref[1349]
	c_ret

c_code local build_seed_137
	ret_reg &seed[137]
	call_c   build_ref_48()
	call_c   build_ref_580()
	call_c   Dyam_Seed_Start(&ref[48],&ref[580],I(0),fun3,1)
	call_c   build_ref_581()
	call_c   Dyam_Seed_Add_Comp(&ref[581],&ref[580],0)
	call_c   Dyam_Seed_End()
	move_ret seed[137]
	c_ret

;; TERM 581: '*GUARD*'(body_to_lpda(_O, _U, _T, _W, dyalog)) :> '$$HOLE$$'
c_code local build_ref_581
	ret_reg &ref[581]
	call_c   build_ref_580()
	call_c   Dyam_Create_Binary(I(9),&ref[580],I(7))
	move_ret ref[581]
	c_ret

;; TERM 580: '*GUARD*'(body_to_lpda(_O, _U, _T, _W, dyalog))
c_code local build_ref_580
	ret_reg &ref[580]
	call_c   build_ref_48()
	call_c   build_ref_579()
	call_c   Dyam_Create_Unary(&ref[48],&ref[579])
	move_ret ref[580]
	c_ret

;; TERM 579: body_to_lpda(_O, _U, _T, _W, dyalog)
c_code local build_ref_579
	ret_reg &ref[579]
	call_c   build_ref_1319()
	call_c   build_ref_1289()
	call_c   Dyam_Term_Start(&ref[1319],5)
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_Arg(V(22))
	call_c   Dyam_Term_Arg(&ref[1289])
	call_c   Dyam_Term_End()
	move_ret ref[579]
	c_ret

long local pool_fun211[5]=[131074,build_ref_578,build_seed_137,pool_fun209,pool_fun209]

long local pool_fun212[3]=[65537,build_ref_583,pool_fun211]

pl_code local fun212
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(19),&ref[583])
	fail_ret
fun211:
	call_c   Dyam_Choice(fun210)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[578])
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(20))
	call_c   Dyam_Reg_Load(2,V(14))
	move     V(21), R(4)
	move     S(5), R(5)
	pl_call  pred_tag_numbervars_3()
	pl_call  fun28(&seed[137],1)
	pl_jump  fun209()


;; TERM 576: light_tabular tag((_B / _C))
c_code local build_ref_576
	ret_reg &ref[576]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1294()
	call_c   Dyam_Create_Binary(&ref[1294],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_1255()
	call_c   Dyam_Create_Unary(&ref[1255],R(0))
	move_ret R(0)
	call_c   build_ref_1338()
	call_c   Dyam_Create_Unary(&ref[1338],R(0))
	move_ret ref[576]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 577: '*SA-SUBST-LIGHT*'(_L, _M, _S, _H)
c_code local build_ref_577
	ret_reg &ref[577]
	call_c   build_ref_1350()
	call_c   Dyam_Term_Start(&ref[1350],4)
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[577]
	c_ret

;; TERM 1350: '*SA-SUBST-LIGHT*'
c_code local build_ref_1350
	ret_reg &ref[1350]
	call_c   Dyam_Create_Atom("*SA-SUBST-LIGHT*")
	move_ret ref[1350]
	c_ret

long local pool_fun213[5]=[131074,build_ref_576,build_ref_577,pool_fun212,pool_fun211]

long local pool_fun214[3]=[65537,build_ref_584,pool_fun213]

pl_code local fun214
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(18),&ref[584])
	fail_ret
fun213:
	call_c   Dyam_Choice(fun212)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[576])
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(19),&ref[577])
	fail_ret
	pl_jump  fun211()


;; TERM 574: callret(_Q, _R)
c_code local build_ref_574
	ret_reg &ref[574]
	call_c   build_ref_1346()
	call_c   Dyam_Create_Binary(&ref[1346],V(16),V(17))
	move_ret ref[574]
	c_ret

;; TERM 575: '*RETARGS*'(_R)
c_code local build_ref_575
	ret_reg &ref[575]
	call_c   build_ref_1351()
	call_c   Dyam_Create_Unary(&ref[1351],V(17))
	move_ret ref[575]
	c_ret

;; TERM 1351: '*RETARGS*'
c_code local build_ref_1351
	ret_reg &ref[1351]
	call_c   Dyam_Create_Atom("*RETARGS*")
	move_ret ref[1351]
	c_ret

long local pool_fun215[7]=[131076,build_seed_136,build_ref_573,build_ref_574,build_ref_575,pool_fun214,pool_fun213]

pl_code local fun215
	call_c   Dyam_Pool(pool_fun215)
	call_c   Dyam_Allocate(0)
	pl_call  fun28(&seed[136],1)
	move     &ref[573], R(0)
	move     S(5), R(1)
	move     V(14), R(2)
	move     S(5), R(3)
	move     V(15), R(4)
	move     S(5), R(5)
	pl_call  pred_tag_numbervars_3()
	call_c   Dyam_Choice(fun214)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(10),&ref[574])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(18),&ref[575])
	fail_ret
	pl_jump  fun213()

;; TERM 586: '$TUPPLE'(35075411419792)
c_code local build_ref_586
	ret_reg &ref[586]
	call_c   Dyam_Create_Simple_Tupple(0,536608768)
	move_ret ref[586]
	c_ret

;; TERM 588: [_F,_G,_H,_I,_J]
c_code local build_ref_588
	ret_reg &ref[588]
	call_c   Dyam_Create_Tupple(5,9,I(0))
	move_ret ref[588]
	c_ret

long local pool_fun216[5]=[4,build_ref_568,build_ref_569,build_ref_587,build_ref_588]

pl_code local fun216
	call_c   Dyam_Pool(pool_fun216)
	call_c   Dyam_Unify_Item(&ref[568])
	fail_ret
	call_c   Dyam_Unify(V(4),&ref[569])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[587], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Load(4,V(4))
	move     &ref[588], R(6)
	move     S(5), R(7)
	move     V(10), R(8)
	move     S(5), R(9)
	call_c   Dyam_Reg_Deallocate(5)
	pl_jump  fun130()

;; TERM 567: '*FIRST*'(wrapping_predicate(tag_subst((_B / _C), _D))) :> []
c_code local build_ref_567
	ret_reg &ref[567]
	call_c   build_ref_566()
	call_c   Dyam_Create_Binary(I(9),&ref[566],I(0))
	move_ret ref[567]
	c_ret

;; TERM 566: '*FIRST*'(wrapping_predicate(tag_subst((_B / _C), _D)))
c_code local build_ref_566
	ret_reg &ref[566]
	call_c   build_ref_52()
	call_c   build_ref_565()
	call_c   Dyam_Create_Unary(&ref[52],&ref[565])
	move_ret ref[566]
	c_ret

c_code local build_seed_28
	ret_reg &seed[28]
	call_c   build_ref_47()
	call_c   build_ref_612()
	call_c   Dyam_Seed_Start(&ref[47],&ref[612],I(0),fun1,1)
	call_c   build_ref_611()
	call_c   Dyam_Seed_Add_Comp(&ref[611],fun227,0)
	call_c   Dyam_Seed_End()
	move_ret seed[28]
	c_ret

;; TERM 611: '*GUARD*'(pgm_to_lpda(tag_lemma(_B, _C, _D), _E))
c_code local build_ref_611
	ret_reg &ref[611]
	call_c   build_ref_48()
	call_c   build_ref_610()
	call_c   Dyam_Create_Unary(&ref[48],&ref[610])
	move_ret ref[611]
	c_ret

;; TERM 610: pgm_to_lpda(tag_lemma(_B, _C, _D), _E)
c_code local build_ref_610
	ret_reg &ref[610]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1352()
	call_c   Dyam_Term_Start(&ref[1352],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_1253()
	call_c   Dyam_Create_Binary(&ref[1253],R(0),V(4))
	move_ret ref[610]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1352: tag_lemma
c_code local build_ref_1352
	ret_reg &ref[1352]
	call_c   Dyam_Create_Atom("tag_lemma")
	move_ret ref[1352]
	c_ret

;; TERM 613: normalized_tag_lemma(_B, _C, _G, _H, _I) :- _J
c_code local build_ref_613
	ret_reg &ref[613]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1354()
	call_c   Dyam_Term_Start(&ref[1354],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_1353()
	call_c   Dyam_Create_Binary(&ref[1353],R(0),V(9))
	move_ret ref[613]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1353: :-
c_code local build_ref_1353
	ret_reg &ref[1353]
	call_c   Dyam_Create_Atom(":-")
	move_ret ref[1353]
	c_ret

;; TERM 1354: normalized_tag_lemma
c_code local build_ref_1354
	ret_reg &ref[1354]
	call_c   Dyam_Create_Atom("normalized_tag_lemma")
	move_ret ref[1354]
	c_ret

c_code local build_seed_141
	ret_reg &seed[141]
	call_c   build_ref_48()
	call_c   build_ref_617()
	call_c   Dyam_Seed_Start(&ref[48],&ref[617],I(0),fun3,1)
	call_c   build_ref_618()
	call_c   Dyam_Seed_Add_Comp(&ref[618],&ref[617],0)
	call_c   Dyam_Seed_End()
	move_ret seed[141]
	c_ret

;; TERM 618: '*GUARD*'(clause_to_lpda(_F, _E)) :> '$$HOLE$$'
c_code local build_ref_618
	ret_reg &ref[618]
	call_c   build_ref_617()
	call_c   Dyam_Create_Binary(I(9),&ref[617],I(7))
	move_ret ref[618]
	c_ret

;; TERM 617: '*GUARD*'(clause_to_lpda(_F, _E))
c_code local build_ref_617
	ret_reg &ref[617]
	call_c   build_ref_48()
	call_c   build_ref_616()
	call_c   Dyam_Create_Unary(&ref[48],&ref[616])
	move_ret ref[617]
	c_ret

;; TERM 616: clause_to_lpda(_F, _E)
c_code local build_ref_616
	ret_reg &ref[616]
	call_c   build_ref_1355()
	call_c   Dyam_Create_Binary(&ref[1355],V(5),V(4))
	move_ret ref[616]
	c_ret

;; TERM 1355: clause_to_lpda
c_code local build_ref_1355
	ret_reg &ref[1355]
	call_c   Dyam_Create_Atom("clause_to_lpda")
	move_ret ref[1355]
	c_ret

long local pool_fun224[2]=[1,build_seed_141]

pl_code local fun225
	call_c   Dyam_Remove_Choice()
	pl_call  Domain_2(V(13),V(3))
	call_c   Dyam_Reg_Load(0,V(13))
	call_c   Dyam_Reg_Load(2,V(5))
	pl_call  pred_tag_compile_lemma_anchor_2()
fun224:
	call_c   Dyam_Deallocate()
	pl_jump  fun28(&seed[141],1)


;; TERM 619: tag_anchor{family=> _K, coanchors=> _L, equations=> _M}
c_code local build_ref_619
	ret_reg &ref[619]
	call_c   build_ref_1241()
	call_c   Dyam_Term_Start(&ref[1241],3)
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_End()
	move_ret ref[619]
	c_ret

long local pool_fun226[4]=[131073,build_ref_619,pool_fun224,pool_fun224]

pl_code local fun226
	call_c   Dyam_Update_Choice(fun225)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(3),&ref[619])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(3))
	call_c   Dyam_Reg_Load(2,V(5))
	pl_call  pred_tag_compile_lemma_anchor_2()
	pl_jump  fun224()

;; TERM 614: 'tag_lemma: unexpected variable as anchor ~w'
c_code local build_ref_614
	ret_reg &ref[614]
	call_c   Dyam_Create_Atom("tag_lemma: unexpected variable as anchor ~w")
	move_ret ref[614]
	c_ret

;; TERM 615: [tag_lemma(_B, _C, _D)]
c_code local build_ref_615
	ret_reg &ref[615]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1352()
	call_c   Dyam_Term_Start(&ref[1352],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   Dyam_Create_List(R(0),I(0))
	move_ret ref[615]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun227[7]=[131076,build_ref_611,build_ref_613,build_ref_614,build_ref_615,pool_fun226,pool_fun224]

pl_code local fun227
	call_c   Dyam_Pool(pool_fun227)
	call_c   Dyam_Unify_Item(&ref[611])
	fail_ret
	call_c   Dyam_Unify(V(5),&ref[613])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun226)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_variable(V(3))
	fail_ret
	call_c   Dyam_Cut()
	move     &ref[614], R(0)
	move     0, R(1)
	move     &ref[615], R(2)
	move     S(5), R(3)
	pl_call  pred_error_2()
	pl_jump  fun224()

;; TERM 612: '*GUARD*'(pgm_to_lpda(tag_lemma(_B, _C, _D), _E)) :> []
c_code local build_ref_612
	ret_reg &ref[612]
	call_c   build_ref_611()
	call_c   Dyam_Create_Binary(I(9),&ref[611],I(0))
	move_ret ref[612]
	c_ret

c_code local build_seed_54
	ret_reg &seed[54]
	call_c   build_ref_47()
	call_c   build_ref_632()
	call_c   Dyam_Seed_Start(&ref[47],&ref[632],I(0),fun1,1)
	call_c   build_ref_631()
	call_c   Dyam_Seed_Add_Comp(&ref[631],fun231,0)
	call_c   Dyam_Seed_End()
	move_ret seed[54]
	c_ret

;; TERM 631: '*GUARD*'(tag_compile_subtree_handler(_B, tag_node{id=> _C, label=> _D, children=> _E, kind=> escape, adj=> _F, spine=> _G, top=> _H, bot=> _I, token=> _J, adjleft=> _K, adjright=> _L, adjwrap=> _M}, _N, _O, _P, (unify(_N, _O) :> _Q), _R, _S, _T))
c_code local build_ref_631
	ret_reg &ref[631]
	call_c   build_ref_48()
	call_c   build_ref_630()
	call_c   Dyam_Create_Unary(&ref[48],&ref[630])
	move_ret ref[631]
	c_ret

;; TERM 630: tag_compile_subtree_handler(_B, tag_node{id=> _C, label=> _D, children=> _E, kind=> escape, adj=> _F, spine=> _G, top=> _H, bot=> _I, token=> _J, adjleft=> _K, adjright=> _L, adjwrap=> _M}, _N, _O, _P, (unify(_N, _O) :> _Q), _R, _S, _T)
c_code local build_ref_630
	ret_reg &ref[630]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_1259()
	call_c   build_ref_1356()
	call_c   Dyam_Term_Start(&ref[1259],12)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(&ref[1356])
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_1283()
	call_c   Dyam_Create_Binary(&ref[1283],V(13),V(14))
	move_ret R(1)
	call_c   Dyam_Create_Binary(I(9),R(1),V(16))
	move_ret R(1)
	call_c   build_ref_1278()
	call_c   Dyam_Term_Start(&ref[1278],9)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_End()
	move_ret ref[630]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1356: escape
c_code local build_ref_1356
	ret_reg &ref[1356]
	call_c   Dyam_Create_Atom("escape")
	move_ret ref[1356]
	c_ret

c_code local build_seed_143
	ret_reg &seed[143]
	call_c   build_ref_48()
	call_c   build_ref_635()
	call_c   Dyam_Seed_Start(&ref[48],&ref[635],I(0),fun3,1)
	call_c   build_ref_636()
	call_c   Dyam_Seed_Add_Comp(&ref[636],&ref[635],0)
	call_c   Dyam_Seed_End()
	move_ret seed[143]
	c_ret

;; TERM 636: '*GUARD*'(body_to_lpda(_B, _D, _P, _Q, _R)) :> '$$HOLE$$'
c_code local build_ref_636
	ret_reg &ref[636]
	call_c   build_ref_635()
	call_c   Dyam_Create_Binary(I(9),&ref[635],I(7))
	move_ret ref[636]
	c_ret

;; TERM 635: '*GUARD*'(body_to_lpda(_B, _D, _P, _Q, _R))
c_code local build_ref_635
	ret_reg &ref[635]
	call_c   build_ref_48()
	call_c   build_ref_634()
	call_c   Dyam_Create_Unary(&ref[48],&ref[634])
	move_ret ref[635]
	c_ret

;; TERM 634: body_to_lpda(_B, _D, _P, _Q, _R)
c_code local build_ref_634
	ret_reg &ref[634]
	call_c   build_ref_1319()
	call_c   Dyam_Term_Start(&ref[1319],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_End()
	move_ret ref[634]
	c_ret

long local pool_fun230[2]=[1,build_seed_143]

pl_code local fun230
	call_c   Dyam_Remove_Choice()
	pl_call  fun28(&seed[143],1)
	pl_jump  fun110()

;; TERM 633: noop(_U)
c_code local build_ref_633
	ret_reg &ref[633]
	call_c   build_ref_274()
	call_c   Dyam_Create_Unary(&ref[274],V(20))
	move_ret ref[633]
	c_ret

;; TERM 274: noop
c_code local build_ref_274
	ret_reg &ref[274]
	call_c   Dyam_Create_Atom("noop")
	move_ret ref[274]
	c_ret

long local pool_fun231[4]=[65538,build_ref_631,build_ref_633,pool_fun230]

pl_code local fun231
	call_c   Dyam_Pool(pool_fun231)
	call_c   Dyam_Unify_Item(&ref[631])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun230)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(3),&ref[633])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(15),V(16))
	fail_ret
	pl_jump  fun110()

;; TERM 632: '*GUARD*'(tag_compile_subtree_handler(_B, tag_node{id=> _C, label=> _D, children=> _E, kind=> escape, adj=> _F, spine=> _G, top=> _H, bot=> _I, token=> _J, adjleft=> _K, adjright=> _L, adjwrap=> _M}, _N, _O, _P, (unify(_N, _O) :> _Q), _R, _S, _T)) :> []
c_code local build_ref_632
	ret_reg &ref[632]
	call_c   build_ref_631()
	call_c   Dyam_Create_Binary(I(9),&ref[631],I(0))
	move_ret ref[632]
	c_ret

c_code local build_seed_56
	ret_reg &seed[56]
	call_c   build_ref_47()
	call_c   build_ref_639()
	call_c   Dyam_Seed_Start(&ref[47],&ref[639],I(0),fun1,1)
	call_c   build_ref_638()
	call_c   Dyam_Seed_Add_Comp(&ref[638],fun240,0)
	call_c   Dyam_Seed_End()
	move_ret seed[56]
	c_ret

;; TERM 638: '*GUARD*'(tag_compile_subtree_handler(_B, tag_node{id=> _C, label=> [_D|_E], children=> _F, kind=> scan, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}, _O, _P, _Q, _R, _S, _T, _U))
c_code local build_ref_638
	ret_reg &ref[638]
	call_c   build_ref_48()
	call_c   build_ref_637()
	call_c   Dyam_Create_Unary(&ref[48],&ref[637])
	move_ret ref[638]
	c_ret

;; TERM 637: tag_compile_subtree_handler(_B, tag_node{id=> _C, label=> [_D|_E], children=> _F, kind=> scan, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}, _O, _P, _Q, _R, _S, _T, _U)
c_code local build_ref_637
	ret_reg &ref[637]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1259()
	call_c   build_ref_778()
	call_c   build_ref_1312()
	call_c   Dyam_Term_Start(&ref[1259],12)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(&ref[778])
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(&ref[1312])
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_1278()
	call_c   Dyam_Term_Start(&ref[1278],9)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_End()
	move_ret ref[637]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_145
	ret_reg &seed[145]
	call_c   build_ref_48()
	call_c   build_ref_647()
	call_c   Dyam_Seed_Start(&ref[48],&ref[647],I(0),fun3,1)
	call_c   build_ref_648()
	call_c   Dyam_Seed_Add_Comp(&ref[648],&ref[647],0)
	call_c   Dyam_Seed_End()
	move_ret seed[145]
	c_ret

;; TERM 648: '*GUARD*'(scan_to_lpda(_B, [_D|_E], _Z, _P, _E1, _F1, _S)) :> '$$HOLE$$'
c_code local build_ref_648
	ret_reg &ref[648]
	call_c   build_ref_647()
	call_c   Dyam_Create_Binary(I(9),&ref[647],I(7))
	move_ret ref[648]
	c_ret

;; TERM 647: '*GUARD*'(scan_to_lpda(_B, [_D|_E], _Z, _P, _E1, _F1, _S))
c_code local build_ref_647
	ret_reg &ref[647]
	call_c   build_ref_48()
	call_c   build_ref_646()
	call_c   Dyam_Create_Unary(&ref[48],&ref[646])
	move_ret ref[647]
	c_ret

;; TERM 646: scan_to_lpda(_B, [_D|_E], _Z, _P, _E1, _F1, _S)
c_code local build_ref_646
	ret_reg &ref[646]
	call_c   build_ref_1357()
	call_c   build_ref_778()
	call_c   Dyam_Term_Start(&ref[1357],7)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(&ref[778])
	call_c   Dyam_Term_Arg(V(25))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(30))
	call_c   Dyam_Term_Arg(V(31))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_End()
	move_ret ref[646]
	c_ret

;; TERM 1357: scan_to_lpda
c_code local build_ref_1357
	ret_reg &ref[1357]
	call_c   Dyam_Create_Atom("scan_to_lpda")
	move_ret ref[1357]
	c_ret

c_code local build_seed_147
	ret_reg &seed[147]
	call_c   build_ref_48()
	call_c   build_ref_655()
	call_c   Dyam_Seed_Start(&ref[48],&ref[655],I(0),fun3,1)
	call_c   build_ref_656()
	call_c   Dyam_Seed_Add_Comp(&ref[656],&ref[655],0)
	call_c   Dyam_Seed_End()
	move_ret seed[147]
	c_ret

;; TERM 656: '*GUARD*'(tag_wrap_skipper(_B, _O, _Z, _P, true, _M1, _R, _S)) :> '$$HOLE$$'
c_code local build_ref_656
	ret_reg &ref[656]
	call_c   build_ref_655()
	call_c   Dyam_Create_Binary(I(9),&ref[655],I(7))
	move_ret ref[656]
	c_ret

;; TERM 655: '*GUARD*'(tag_wrap_skipper(_B, _O, _Z, _P, true, _M1, _R, _S))
c_code local build_ref_655
	ret_reg &ref[655]
	call_c   build_ref_48()
	call_c   build_ref_654()
	call_c   Dyam_Create_Unary(&ref[48],&ref[654])
	move_ret ref[655]
	c_ret

;; TERM 654: tag_wrap_skipper(_B, _O, _Z, _P, true, _M1, _R, _S)
c_code local build_ref_654
	ret_reg &ref[654]
	call_c   build_ref_1280()
	call_c   build_ref_261()
	call_c   Dyam_Term_Start(&ref[1280],8)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(25))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(&ref[261])
	call_c   Dyam_Term_Arg(V(38))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_End()
	move_ret ref[654]
	c_ret

long local pool_fun232[2]=[1,build_seed_147]

pl_code local fun234
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(38),V(31))
	fail_ret
fun232:
	call_c   Dyam_Deallocate()
	pl_jump  fun28(&seed[147],1)


;; TERM 659: '$CLOSURE'('$fun'(233, 0, 1129460648), '$TUPPLE'(35075411420832))
c_code local build_ref_659
	ret_reg &ref[659]
	call_c   build_ref_658()
	call_c   Dyam_Closure_Aux(fun233,&ref[658])
	move_ret ref[659]
	c_ret

;; TERM 649: tag_anchor{family=> _H1, coanchors=> _I1, equations=> _J1}
c_code local build_ref_649
	ret_reg &ref[649]
	call_c   build_ref_1241()
	call_c   Dyam_Term_Start(&ref[1241],3)
	call_c   Dyam_Term_Arg(V(33))
	call_c   Dyam_Term_Arg(V(34))
	call_c   Dyam_Term_Arg(V(35))
	call_c   Dyam_Term_End()
	move_ret ref[649]
	c_ret

;; TERM 650: tag_coanchor(_H1, _C, _K1, _I1)
c_code local build_ref_650
	ret_reg &ref[650]
	call_c   build_ref_1327()
	call_c   Dyam_Term_Start(&ref[1327],4)
	call_c   Dyam_Term_Arg(V(33))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(36))
	call_c   Dyam_Term_Arg(V(34))
	call_c   Dyam_Term_End()
	move_ret ref[650]
	c_ret

c_code local build_seed_146
	ret_reg &seed[146]
	call_c   build_ref_48()
	call_c   build_ref_652()
	call_c   Dyam_Seed_Start(&ref[48],&ref[652],I(0),fun3,1)
	call_c   build_ref_653()
	call_c   Dyam_Seed_Add_Comp(&ref[653],&ref[652],0)
	call_c   Dyam_Seed_End()
	move_ret seed[146]
	c_ret

;; TERM 653: '*GUARD*'(body_to_lpda(_B, check_coanchor(_K1, _Z, _P), _F1, _M1, _S)) :> '$$HOLE$$'
c_code local build_ref_653
	ret_reg &ref[653]
	call_c   build_ref_652()
	call_c   Dyam_Create_Binary(I(9),&ref[652],I(7))
	move_ret ref[653]
	c_ret

;; TERM 652: '*GUARD*'(body_to_lpda(_B, check_coanchor(_K1, _Z, _P), _F1, _M1, _S))
c_code local build_ref_652
	ret_reg &ref[652]
	call_c   build_ref_48()
	call_c   build_ref_651()
	call_c   Dyam_Create_Unary(&ref[48],&ref[651])
	move_ret ref[652]
	c_ret

;; TERM 651: body_to_lpda(_B, check_coanchor(_K1, _Z, _P), _F1, _M1, _S)
c_code local build_ref_651
	ret_reg &ref[651]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1358()
	call_c   Dyam_Term_Start(&ref[1358],3)
	call_c   Dyam_Term_Arg(V(36))
	call_c   Dyam_Term_Arg(V(25))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_1319()
	call_c   Dyam_Term_Start(&ref[1319],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(31))
	call_c   Dyam_Term_Arg(V(38))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_End()
	move_ret ref[651]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1358: check_coanchor
c_code local build_ref_1358
	ret_reg &ref[1358]
	call_c   Dyam_Create_Atom("check_coanchor")
	move_ret ref[1358]
	c_ret

long local pool_fun233[5]=[65539,build_ref_649,build_ref_650,build_seed_146,pool_fun232]

pl_code local fun233
	call_c   Dyam_Pool(pool_fun233)
	call_c   Dyam_Unify(V(32),&ref[649])
	fail_ret
	call_c   Dyam_Allocate(0)
	pl_call  Object_1(&ref[650])
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(36))
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(37), R(4)
	move     S(5), R(5)
	pl_call  pred_tag_numbervars_3()
	pl_call  fun28(&seed[146],1)
	pl_jump  fun232()

;; TERM 658: '$TUPPLE'(35075411420832)
c_code local build_ref_658
	ret_reg &ref[658]
	call_c   Dyam_Start_Tupple(0,201354248)
	call_c   Dyam_Almost_End_Tupple(29,100663296)
	move_ret ref[658]
	c_ret

long local pool_fun235[4]=[65538,build_seed_145,build_ref_659,pool_fun232]

pl_code local fun237
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(30),V(16))
	fail_ret
fun235:
	pl_call  fun28(&seed[145],1)
	call_c   Dyam_Choice(fun234)
	call_c   Dyam_Set_Cut()
	move     &ref[659], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Load(4,V(1))
	call_c   Dyam_Reg_Load(6,V(19))
	move     V(32), R(8)
	move     S(5), R(9)
	call_c   Dyam_Reg_Deallocate(5)
	pl_jump  fun52()


;; TERM 662: '$CLOSURE'('$fun'(236, 0, 1129469324), '$TUPPLE'(35075411420620))
c_code local build_ref_662
	ret_reg &ref[662]
	call_c   build_ref_661()
	call_c   Dyam_Closure_Aux(fun236,&ref[661])
	move_ret ref[662]
	c_ret

;; TERM 642: [_B1,_C1]
c_code local build_ref_642
	ret_reg &ref[642]
	call_c   Dyam_Create_Tupple(27,28,I(0))
	move_ret ref[642]
	c_ret

c_code local build_seed_144
	ret_reg &seed[144]
	call_c   build_ref_48()
	call_c   build_ref_644()
	call_c   Dyam_Seed_Start(&ref[48],&ref[644],I(0),fun3,1)
	call_c   build_ref_645()
	call_c   Dyam_Seed_Add_Comp(&ref[645],&ref[644],0)
	call_c   Dyam_Seed_End()
	move_ret seed[144]
	c_ret

;; TERM 645: '*GUARD*'(body_to_lpda(_B, verbose!lexical([_D|_E], _Z, _P, _B1, _C1), _Q, _E1, _S)) :> '$$HOLE$$'
c_code local build_ref_645
	ret_reg &ref[645]
	call_c   build_ref_644()
	call_c   Dyam_Create_Binary(I(9),&ref[644],I(7))
	move_ret ref[645]
	c_ret

;; TERM 644: '*GUARD*'(body_to_lpda(_B, verbose!lexical([_D|_E], _Z, _P, _B1, _C1), _Q, _E1, _S))
c_code local build_ref_644
	ret_reg &ref[644]
	call_c   build_ref_48()
	call_c   build_ref_643()
	call_c   Dyam_Create_Unary(&ref[48],&ref[643])
	move_ret ref[644]
	c_ret

;; TERM 643: body_to_lpda(_B, verbose!lexical([_D|_E], _Z, _P, _B1, _C1), _Q, _E1, _S)
c_code local build_ref_643
	ret_reg &ref[643]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1359()
	call_c   build_ref_778()
	call_c   Dyam_Term_Start(&ref[1359],5)
	call_c   Dyam_Term_Arg(&ref[778])
	call_c   Dyam_Term_Arg(V(25))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(27))
	call_c   Dyam_Term_Arg(V(28))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_1319()
	call_c   Dyam_Term_Start(&ref[1319],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(30))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_End()
	move_ret ref[643]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1359: verbose!lexical
c_code local build_ref_1359
	ret_reg &ref[1359]
	call_c   build_ref_1337()
	call_c   Dyam_Create_Atom_Module("lexical",&ref[1337])
	move_ret ref[1359]
	c_ret

long local pool_fun236[4]=[65538,build_ref_642,build_seed_144,pool_fun235]

pl_code local fun236
	call_c   Dyam_Pool(pool_fun236)
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Cut()
	move     &ref[642], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(29), R(4)
	move     S(5), R(5)
	pl_call  pred_tag_numbervars_3()
	pl_call  fun28(&seed[144],1)
	pl_jump  fun235()

;; TERM 661: '$TUPPLE'(35075411420620)
c_code local build_ref_661
	ret_reg &ref[661]
	call_c   Dyam_Create_Simple_Tupple(0,251690504)
	move_ret ref[661]
	c_ret

long local pool_fun238[3]=[65537,build_ref_662,pool_fun235]

pl_code local fun239
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(25),V(14))
	fail_ret
fun238:
	call_c   Dyam_Choice(fun237)
	call_c   Dyam_Set_Cut()
	move     &ref[662], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Load(4,V(1))
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  fun171()


;; TERM 640: skipper(_V, _W, _X, _Y)
c_code local build_ref_640
	ret_reg &ref[640]
	call_c   build_ref_1360()
	call_c   Dyam_Term_Start(&ref[1360],4)
	call_c   Dyam_Term_Arg(V(21))
	call_c   Dyam_Term_Arg(V(22))
	call_c   Dyam_Term_Arg(V(23))
	call_c   Dyam_Term_Arg(V(24))
	call_c   Dyam_Term_End()
	move_ret ref[640]
	c_ret

;; TERM 1360: skipper
c_code local build_ref_1360
	ret_reg &ref[1360]
	call_c   Dyam_Create_Atom("skipper")
	move_ret ref[1360]
	c_ret

;; TERM 641: [_Z]
c_code local build_ref_641
	ret_reg &ref[641]
	call_c   Dyam_Create_List(V(25),I(0))
	move_ret ref[641]
	c_ret

long local pool_fun240[6]=[131075,build_ref_638,build_ref_640,build_ref_641,pool_fun238,pool_fun238]

pl_code local fun240
	call_c   Dyam_Pool(pool_fun240)
	call_c   Dyam_Unify_Item(&ref[638])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun239)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[640])
	call_c   Dyam_Cut()
	move     &ref[641], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(26), R(4)
	move     S(5), R(5)
	pl_call  pred_tag_numbervars_3()
	pl_jump  fun238()

;; TERM 639: '*GUARD*'(tag_compile_subtree_handler(_B, tag_node{id=> _C, label=> [_D|_E], children=> _F, kind=> scan, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}, _O, _P, _Q, _R, _S, _T, _U)) :> []
c_code local build_ref_639
	ret_reg &ref[639]
	call_c   build_ref_638()
	call_c   Dyam_Create_Binary(I(9),&ref[638],I(0))
	move_ret ref[639]
	c_ret

c_code local build_seed_35
	ret_reg &seed[35]
	call_c   build_ref_47()
	call_c   build_ref_665()
	call_c   Dyam_Seed_Start(&ref[47],&ref[665],I(0),fun1,1)
	call_c   build_ref_664()
	call_c   Dyam_Seed_Add_Comp(&ref[664],fun250,0)
	call_c   Dyam_Seed_End()
	move_ret seed[35]
	c_ret

;; TERM 664: '*GUARD*'(tag_wrap_skipper(_B, _C, _D, _E, _F, _G, _H, _I))
c_code local build_ref_664
	ret_reg &ref[664]
	call_c   build_ref_48()
	call_c   build_ref_663()
	call_c   Dyam_Create_Unary(&ref[48],&ref[663])
	move_ret ref[664]
	c_ret

;; TERM 663: tag_wrap_skipper(_B, _C, _D, _E, _F, _G, _H, _I)
c_code local build_ref_663
	ret_reg &ref[663]
	call_c   build_ref_1280()
	call_c   Dyam_Term_Start(&ref[1280],8)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_End()
	move_ret ref[663]
	c_ret

c_code local build_seed_151
	ret_reg &seed[151]
	call_c   build_ref_48()
	call_c   build_ref_693()
	call_c   Dyam_Seed_Start(&ref[48],&ref[693],I(0),fun3,1)
	call_c   build_ref_694()
	call_c   Dyam_Seed_Add_Comp(&ref[694],&ref[693],0)
	call_c   Dyam_Seed_End()
	move_ret seed[151]
	c_ret

;; TERM 694: '*GUARD*'(body_to_lpda(_B, _F, _G, _H, _I)) :> '$$HOLE$$'
c_code local build_ref_694
	ret_reg &ref[694]
	call_c   build_ref_693()
	call_c   Dyam_Create_Binary(I(9),&ref[693],I(7))
	move_ret ref[694]
	c_ret

;; TERM 693: '*GUARD*'(body_to_lpda(_B, _F, _G, _H, _I))
c_code local build_ref_693
	ret_reg &ref[693]
	call_c   build_ref_48()
	call_c   build_ref_692()
	call_c   Dyam_Create_Unary(&ref[48],&ref[692])
	move_ret ref[693]
	c_ret

;; TERM 692: body_to_lpda(_B, _F, _G, _H, _I)
c_code local build_ref_692
	ret_reg &ref[692]
	call_c   build_ref_1319()
	call_c   Dyam_Term_Start(&ref[1319],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_End()
	move_ret ref[692]
	c_ret

long local pool_fun249[2]=[1,build_seed_151]

pl_code local fun249
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(2),V(3))
	fail_ret
	pl_call  fun28(&seed[151],1)
	pl_jump  fun110()

;; TERM 666: skipper(_J, _K, _L, _M)
c_code local build_ref_666
	ret_reg &ref[666]
	call_c   build_ref_1360()
	call_c   Dyam_Term_Start(&ref[1360],4)
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_End()
	move_ret ref[666]
	c_ret

;; TERM 667: [_D]
c_code local build_ref_667
	ret_reg &ref[667]
	call_c   Dyam_Create_List(V(3),I(0))
	move_ret ref[667]
	c_ret

c_code local build_seed_148
	ret_reg &seed[148]
	call_c   build_ref_48()
	call_c   build_ref_669()
	call_c   Dyam_Seed_Start(&ref[48],&ref[669],I(0),fun3,1)
	call_c   build_ref_670()
	call_c   Dyam_Seed_Add_Comp(&ref[670],&ref[669],0)
	call_c   Dyam_Seed_End()
	move_ret seed[148]
	c_ret

;; TERM 670: '*GUARD*'(body_to_lpda(_B, _F, _G, _O, _I)) :> '$$HOLE$$'
c_code local build_ref_670
	ret_reg &ref[670]
	call_c   build_ref_669()
	call_c   Dyam_Create_Binary(I(9),&ref[669],I(7))
	move_ret ref[670]
	c_ret

;; TERM 669: '*GUARD*'(body_to_lpda(_B, _F, _G, _O, _I))
c_code local build_ref_669
	ret_reg &ref[669]
	call_c   build_ref_48()
	call_c   build_ref_668()
	call_c   Dyam_Create_Unary(&ref[48],&ref[668])
	move_ret ref[669]
	c_ret

;; TERM 668: body_to_lpda(_B, _F, _G, _O, _I)
c_code local build_ref_668
	ret_reg &ref[668]
	call_c   build_ref_1319()
	call_c   Dyam_Term_Start(&ref[1319],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_End()
	move_ret ref[668]
	c_ret

;; TERM 691: false
c_code local build_ref_691
	ret_reg &ref[691]
	call_c   Dyam_Create_Atom("false")
	move_ret ref[691]
	c_ret

;; TERM 686: '$CLOSURE'('$fun'(245, 0, 1129548968), '$TUPPLE'(35075411421188))
c_code local build_ref_686
	ret_reg &ref[686]
	call_c   build_ref_685()
	call_c   Dyam_Closure_Aux(fun245,&ref[685])
	move_ret ref[686]
	c_ret

;; TERM 681: '$CLOSURE'('$fun'(244, 0, 1129541908), '$TUPPLE'(35075411421264))
c_code local build_ref_681
	ret_reg &ref[681]
	call_c   build_ref_680()
	call_c   Dyam_Closure_Aux(fun244,&ref[680])
	move_ret ref[681]
	c_ret

;; TERM 678: '*WRAPPER-CALL-ALT*'(tag_skipper(_P), _Q, _O)
c_code local build_ref_678
	ret_reg &ref[678]
	call_c   build_ref_1361()
	call_c   build_ref_687()
	call_c   Dyam_Term_Start(&ref[1361],3)
	call_c   Dyam_Term_Arg(&ref[687])
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_End()
	move_ret ref[678]
	c_ret

;; TERM 687: tag_skipper(_P)
c_code local build_ref_687
	ret_reg &ref[687]
	call_c   build_ref_682()
	call_c   Dyam_Create_Unary(&ref[682],V(15))
	move_ret ref[687]
	c_ret

;; TERM 1361: '*WRAPPER-CALL-ALT*'
c_code local build_ref_1361
	ret_reg &ref[1361]
	call_c   Dyam_Create_Atom("*WRAPPER-CALL-ALT*")
	move_ret ref[1361]
	c_ret

long local pool_fun244[2]=[1,build_ref_678]

pl_code local fun244
	call_c   Dyam_Pool(pool_fun244)
	call_c   Dyam_Unify(V(7),&ref[678])
	fail_ret
	call_c   Dyam_Allocate(0)
	pl_jump  fun110()

;; TERM 680: '$TUPPLE'(35075411421264)
c_code local build_ref_680
	ret_reg &ref[680]
	call_c   Dyam_Create_Simple_Tupple(0,2125824)
	move_ret ref[680]
	c_ret

;; TERM 683: [_C,_D]
c_code local build_ref_683
	ret_reg &ref[683]
	call_c   Dyam_Create_Tupple(2,3,I(0))
	move_ret ref[683]
	c_ret

long local pool_fun245[4]=[3,build_ref_681,build_ref_682,build_ref_683]

pl_code local fun245
	call_c   Dyam_Pool(pool_fun245)
	call_c   Dyam_Allocate(0)
	move     &ref[681], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	move     &ref[682], R(4)
	move     0, R(5)
	move     &ref[683], R(6)
	move     S(5), R(7)
	move     V(16), R(8)
	move     S(5), R(9)
	call_c   Dyam_Reg_Deallocate(5)
	pl_jump  fun130()

;; TERM 685: '$TUPPLE'(35075411421188)
c_code local build_ref_685
	ret_reg &ref[685]
	call_c   Dyam_Create_Simple_Tupple(0,102785024)
	move_ret ref[685]
	c_ret

long local pool_fun246[3]=[2,build_ref_686,build_ref_687]

long local pool_fun248[3]=[65537,build_ref_691,pool_fun246]

pl_code local fun248
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(15),&ref[691])
	fail_ret
fun246:
	move     &ref[686], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	move     &ref[687], R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Deallocate(3)
fun243:
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Reg_Bind(2,V(3))
	call_c   Dyam_Reg_Bind(4,V(2))
	call_c   Dyam_Choice(fun242)
	pl_call  fun46(&seed[149],1)
	pl_fail



;; TERM 690: '$CLOSURE'('$fun'(247, 0, 1129569144), '$TUPPLE'(35075411421336))
c_code local build_ref_690
	ret_reg &ref[690]
	call_c   build_ref_689()
	call_c   Dyam_Closure_Aux(fun247,&ref[689])
	move_ret ref[690]
	c_ret

long local pool_fun247[3]=[65537,build_ref_261,pool_fun246]

pl_code local fun247
	call_c   Dyam_Pool(pool_fun247)
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(15),&ref[261])
	fail_ret
	pl_jump  fun246()

;; TERM 689: '$TUPPLE'(35075411421336)
c_code local build_ref_689
	ret_reg &ref[689]
	call_c   Dyam_Create_Simple_Tupple(0,102776832)
	move_ret ref[689]
	c_ret

long local pool_fun250[8]=[131077,build_ref_664,build_ref_666,build_ref_667,build_seed_148,build_ref_690,pool_fun249,pool_fun248]

pl_code local fun250
	call_c   Dyam_Pool(pool_fun250)
	call_c   Dyam_Unify_Item(&ref[664])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun249)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[666])
	call_c   Dyam_Cut()
	move     &ref[667], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(13), R(4)
	move     S(5), R(5)
	pl_call  pred_tag_numbervars_3()
	pl_call  fun28(&seed[148],1)
	call_c   Dyam_Choice(fun248)
	call_c   Dyam_Set_Cut()
	move     &ref[690], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Load(4,V(1))
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  fun171()

;; TERM 665: '*GUARD*'(tag_wrap_skipper(_B, _C, _D, _E, _F, _G, _H, _I)) :> []
c_code local build_ref_665
	ret_reg &ref[665]
	call_c   build_ref_664()
	call_c   Dyam_Create_Binary(I(9),&ref[664],I(0))
	move_ret ref[665]
	c_ret

c_code local build_seed_26
	ret_reg &seed[26]
	call_c   build_ref_47()
	call_c   build_ref_703()
	call_c   Dyam_Seed_Start(&ref[47],&ref[703],I(0),fun1,1)
	call_c   build_ref_702()
	call_c   Dyam_Seed_Add_Comp(&ref[702],fun258,0)
	call_c   Dyam_Seed_End()
	move_ret seed[26]
	c_ret

;; TERM 702: '*GUARD*'(tag_compile_lemma_equations([_B|_C], _D, _E))
c_code local build_ref_702
	ret_reg &ref[702]
	call_c   build_ref_48()
	call_c   build_ref_701()
	call_c   Dyam_Create_Unary(&ref[48],&ref[701])
	move_ret ref[702]
	c_ret

;; TERM 701: tag_compile_lemma_equations([_B|_C], _D, _E)
c_code local build_ref_701
	ret_reg &ref[701]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(1),V(2))
	move_ret R(0)
	call_c   build_ref_1229()
	call_c   Dyam_Term_Start(&ref[1229],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[701]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 712: 'tag_lemma: bad format for equation ~w'
c_code local build_ref_712
	ret_reg &ref[712]
	call_c   Dyam_Create_Atom("tag_lemma: bad format for equation ~w")
	move_ret ref[712]
	c_ret

long local pool_fun256[3]=[2,build_ref_712,build_ref_148]

pl_code local fun256
	call_c   Dyam_Remove_Choice()
	move     &ref[712], R(0)
	move     0, R(1)
	move     &ref[148], R(2)
	move     S(5), R(3)
	pl_call  pred_error_2()
	pl_jump  fun110()

;; TERM 705: _F at _G
c_code local build_ref_705
	ret_reg &ref[705]
	call_c   build_ref_1362()
	call_c   Dyam_Create_Binary(&ref[1362],V(5),V(6))
	move_ret ref[705]
	c_ret

;; TERM 1362: at
c_code local build_ref_1362
	ret_reg &ref[1362]
	call_c   Dyam_Create_Atom("at")
	move_ret ref[1362]
	c_ret

;; TERM 710: 'Node ~w not referenced to support equations for family ~w\n'
c_code local build_ref_710
	ret_reg &ref[710]
	call_c   Dyam_Create_Atom("Node ~w not referenced to support equations for family ~w\n")
	move_ret ref[710]
	c_ret

;; TERM 711: [_G,_E]
c_code local build_ref_711
	ret_reg &ref[711]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(4),I(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(6),R(0))
	move_ret ref[711]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun255[3]=[2,build_ref_710,build_ref_711]

pl_code local fun255
	call_c   Dyam_Remove_Choice()
	move     &ref[710], R(0)
	move     0, R(1)
	move     &ref[711], R(2)
	move     S(5), R(3)
	pl_call  pred_error_2()
	pl_jump  fun110()

;; TERM 706: tag_equation(_E, _G, _H, _I, _D)
c_code local build_ref_706
	ret_reg &ref[706]
	call_c   build_ref_1363()
	call_c   Dyam_Term_Start(&ref[1363],5)
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[706]
	c_ret

;; TERM 1363: tag_equation
c_code local build_ref_1363
	ret_reg &ref[1363]
	call_c   Dyam_Create_Atom("tag_equation")
	move_ret ref[1363]
	c_ret

c_code local build_seed_152
	ret_reg &seed[152]
	call_c   build_ref_48()
	call_c   build_ref_708()
	call_c   Dyam_Seed_Start(&ref[48],&ref[708],I(0),fun3,1)
	call_c   build_ref_709()
	call_c   Dyam_Seed_Add_Comp(&ref[709],&ref[708],0)
	call_c   Dyam_Seed_End()
	move_ret seed[152]
	c_ret

;; TERM 709: '*GUARD*'(tag_compile_lemma_equations(_C, _D, _E)) :> '$$HOLE$$'
c_code local build_ref_709
	ret_reg &ref[709]
	call_c   build_ref_708()
	call_c   Dyam_Create_Binary(I(9),&ref[708],I(7))
	move_ret ref[709]
	c_ret

;; TERM 708: '*GUARD*'(tag_compile_lemma_equations(_C, _D, _E))
c_code local build_ref_708
	ret_reg &ref[708]
	call_c   build_ref_48()
	call_c   build_ref_707()
	call_c   Dyam_Create_Unary(&ref[48],&ref[707])
	move_ret ref[708]
	c_ret

;; TERM 707: tag_compile_lemma_equations(_C, _D, _E)
c_code local build_ref_707
	ret_reg &ref[707]
	call_c   build_ref_1229()
	call_c   Dyam_Term_Start(&ref[1229],3)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[707]
	c_ret

long local pool_fun257[6]=[131075,build_ref_705,build_ref_706,build_seed_152,pool_fun256,pool_fun255]

pl_code local fun257
	call_c   Dyam_Update_Choice(fun256)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[705])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun255)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[706])
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(5))
	call_c   Dyam_Reg_Load(2,V(7))
	call_c   Dyam_Reg_Load(4,V(8))
	pl_call  pred_tag_anchor_analyse_equation_3()
	pl_call  fun28(&seed[152],1)
	pl_jump  fun110()

;; TERM 704: 'tag_lemma: unexpected variables in equation'
c_code local build_ref_704
	ret_reg &ref[704]
	call_c   Dyam_Create_Atom("tag_lemma: unexpected variables in equation")
	move_ret ref[704]
	c_ret

long local pool_fun258[4]=[65538,build_ref_702,build_ref_704,pool_fun257]

pl_code local fun258
	call_c   Dyam_Pool(pool_fun258)
	call_c   Dyam_Unify_Item(&ref[702])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun257)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_variable(V(1))
	fail_ret
	call_c   Dyam_Cut()
	move     &ref[704], R(0)
	move     0, R(1)
	move     I(0), R(2)
	move     0, R(3)
	pl_call  pred_error_2()
	pl_jump  fun110()

;; TERM 703: '*GUARD*'(tag_compile_lemma_equations([_B|_C], _D, _E)) :> []
c_code local build_ref_703
	ret_reg &ref[703]
	call_c   build_ref_702()
	call_c   Dyam_Create_Binary(I(9),&ref[702],I(0))
	move_ret ref[703]
	c_ret

c_code local build_seed_39
	ret_reg &seed[39]
	call_c   build_ref_52()
	call_c   build_ref_715()
	call_c   Dyam_Seed_Start(&ref[52],&ref[715],I(0),fun15,1)
	call_c   build_ref_716()
	call_c   Dyam_Seed_Add_Comp(&ref[716],fun264,0)
	call_c   Dyam_Seed_End()
	move_ret seed[39]
	c_ret

;; TERM 716: '*CITEM*'(wrapping_predicate(tag_adj_strict((_B / _C))), _A)
c_code local build_ref_716
	ret_reg &ref[716]
	call_c   build_ref_56()
	call_c   build_ref_713()
	call_c   Dyam_Create_Binary(&ref[56],&ref[713],V(0))
	move_ret ref[716]
	c_ret

;; TERM 713: wrapping_predicate(tag_adj_strict((_B / _C)))
c_code local build_ref_713
	ret_reg &ref[713]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1294()
	call_c   Dyam_Create_Binary(&ref[1294],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_1364()
	call_c   Dyam_Create_Unary(&ref[1364],R(0))
	move_ret R(0)
	call_c   build_ref_1314()
	call_c   Dyam_Create_Unary(&ref[1314],R(0))
	move_ret ref[713]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1364: tag_adj_strict
c_code local build_ref_1364
	ret_reg &ref[1364]
	call_c   Dyam_Create_Atom("tag_adj_strict")
	move_ret ref[1364]
	c_ret

;; TERM 733: '$CLOSURE'('$fun'(263, 0, 1129687136), '$TUPPLE'(35075411943244))
c_code local build_ref_733
	ret_reg &ref[733]
	call_c   build_ref_732()
	call_c   Dyam_Closure_Aux(fun263,&ref[732])
	move_ret ref[733]
	c_ret

c_code local build_seed_153
	ret_reg &seed[153]
	call_c   build_ref_48()
	call_c   build_ref_718()
	call_c   Dyam_Seed_Start(&ref[48],&ref[718],I(0),fun3,1)
	call_c   build_ref_719()
	call_c   Dyam_Seed_Add_Comp(&ref[719],&ref[718],0)
	call_c   Dyam_Seed_End()
	move_ret seed[153]
	c_ret

;; TERM 719: '*GUARD*'(make_tag_top_callret(_H, _D, _E, _M, _N)) :> '$$HOLE$$'
c_code local build_ref_719
	ret_reg &ref[719]
	call_c   build_ref_718()
	call_c   Dyam_Create_Binary(I(9),&ref[718],I(7))
	move_ret ref[719]
	c_ret

;; TERM 718: '*GUARD*'(make_tag_top_callret(_H, _D, _E, _M, _N))
c_code local build_ref_718
	ret_reg &ref[718]
	call_c   build_ref_48()
	call_c   build_ref_717()
	call_c   Dyam_Create_Unary(&ref[48],&ref[717])
	move_ret ref[718]
	c_ret

;; TERM 717: make_tag_top_callret(_H, _D, _E, _M, _N)
c_code local build_ref_717
	ret_reg &ref[717]
	call_c   build_ref_1340()
	call_c   Dyam_Term_Start(&ref[1340],5)
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_End()
	move_ret ref[717]
	c_ret

c_code local build_seed_154
	ret_reg &seed[154]
	call_c   build_ref_48()
	call_c   build_ref_721()
	call_c   Dyam_Seed_Start(&ref[48],&ref[721],I(0),fun3,1)
	call_c   build_ref_722()
	call_c   Dyam_Seed_Add_Comp(&ref[722],&ref[721],0)
	call_c   Dyam_Seed_End()
	move_ret seed[154]
	c_ret

;; TERM 722: '*GUARD*'(make_tag_bot_callret(_I, _F, _G, _O, _P)) :> '$$HOLE$$'
c_code local build_ref_722
	ret_reg &ref[722]
	call_c   build_ref_721()
	call_c   Dyam_Create_Binary(I(9),&ref[721],I(7))
	move_ret ref[722]
	c_ret

;; TERM 721: '*GUARD*'(make_tag_bot_callret(_I, _F, _G, _O, _P))
c_code local build_ref_721
	ret_reg &ref[721]
	call_c   build_ref_48()
	call_c   build_ref_720()
	call_c   Dyam_Create_Unary(&ref[48],&ref[720])
	move_ret ref[721]
	c_ret

;; TERM 720: make_tag_bot_callret(_I, _F, _G, _O, _P)
c_code local build_ref_720
	ret_reg &ref[720]
	call_c   build_ref_1315()
	call_c   Dyam_Term_Start(&ref[1315],5)
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_End()
	move_ret ref[720]
	c_ret

;; TERM 723: [_Q,_R,_S|_L]
c_code local build_ref_723
	ret_reg &ref[723]
	call_c   Dyam_Create_Tupple(16,18,V(11))
	move_ret ref[723]
	c_ret

;; TERM 724: '*SA-ADJ-FIRST*'(_M, _O, '*SA-PROLOG-LAST*')
c_code local build_ref_724
	ret_reg &ref[724]
	call_c   build_ref_1365()
	call_c   build_ref_847()
	call_c   Dyam_Term_Start(&ref[1365],3)
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(&ref[847])
	call_c   Dyam_Term_End()
	move_ret ref[724]
	c_ret

;; TERM 1365: '*SA-ADJ-FIRST*'
c_code local build_ref_1365
	ret_reg &ref[1365]
	call_c   Dyam_Create_Atom("*SA-ADJ-FIRST*")
	move_ret ref[1365]
	c_ret

;; TERM 730: '*SA-ADJ-WRAPPER*'(tag_adj_strict((_B / _C)), [_Q,_R,_S|_L], _Z, '*SA-ADJ-LAST*'(_N, _P, '*SA-PROLOG-LAST*', _J))
c_code local build_ref_730
	ret_reg &ref[730]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_1294()
	call_c   Dyam_Create_Binary(&ref[1294],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_1364()
	call_c   Dyam_Create_Unary(&ref[1364],R(0))
	move_ret R(0)
	call_c   build_ref_1367()
	call_c   build_ref_847()
	call_c   Dyam_Term_Start(&ref[1367],4)
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(&ref[847])
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret R(1)
	call_c   build_ref_1366()
	call_c   build_ref_723()
	call_c   Dyam_Term_Start(&ref[1366],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[723])
	call_c   Dyam_Term_Arg(V(25))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_End()
	move_ret ref[730]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1366: '*SA-ADJ-WRAPPER*'
c_code local build_ref_1366
	ret_reg &ref[1366]
	call_c   Dyam_Create_Atom("*SA-ADJ-WRAPPER*")
	move_ret ref[1366]
	c_ret

;; TERM 1367: '*SA-ADJ-LAST*'
c_code local build_ref_1367
	ret_reg &ref[1367]
	call_c   Dyam_Create_Atom("*SA-ADJ-LAST*")
	move_ret ref[1367]
	c_ret

long local pool_fun259[3]=[2,build_ref_730,build_seed_69]

pl_code local fun260
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(25),V(21))
	fail_ret
fun259:
	call_c   DYAM_evpred_assert_1(&ref[730])
	call_c   Dyam_Deallocate()
	pl_jump  fun13(&seed[69],2)


;; TERM 726: tagfilter_cat((_K ^ _W ^ wrap ^ _D ^ _E ^ _H ^ _X))
c_code local build_ref_726
	ret_reg &ref[726]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1307()
	call_c   Dyam_Create_Binary(&ref[1307],V(7),V(23))
	move_ret R(0)
	call_c   Dyam_Create_Binary(&ref[1307],V(4),R(0))
	move_ret R(0)
	call_c   Dyam_Create_Binary(&ref[1307],V(3),R(0))
	move_ret R(0)
	call_c   build_ref_294()
	call_c   Dyam_Create_Binary(&ref[1307],&ref[294],R(0))
	move_ret R(0)
	call_c   Dyam_Create_Binary(&ref[1307],V(22),R(0))
	move_ret R(0)
	call_c   Dyam_Create_Binary(&ref[1307],V(10),R(0))
	move_ret R(0)
	call_c   build_ref_1349()
	call_c   Dyam_Create_Unary(&ref[1349],R(0))
	move_ret ref[726]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_155
	ret_reg &seed[155]
	call_c   build_ref_48()
	call_c   build_ref_728()
	call_c   Dyam_Seed_Start(&ref[48],&ref[728],I(0),fun3,1)
	call_c   build_ref_729()
	call_c   Dyam_Seed_Add_Comp(&ref[729],&ref[728],0)
	call_c   Dyam_Seed_End()
	move_ret seed[155]
	c_ret

;; TERM 729: '*GUARD*'(body_to_lpda(_T, _X, _V, _Z, dyalog)) :> '$$HOLE$$'
c_code local build_ref_729
	ret_reg &ref[729]
	call_c   build_ref_728()
	call_c   Dyam_Create_Binary(I(9),&ref[728],I(7))
	move_ret ref[729]
	c_ret

;; TERM 728: '*GUARD*'(body_to_lpda(_T, _X, _V, _Z, dyalog))
c_code local build_ref_728
	ret_reg &ref[728]
	call_c   build_ref_48()
	call_c   build_ref_727()
	call_c   Dyam_Create_Unary(&ref[48],&ref[727])
	move_ret ref[728]
	c_ret

;; TERM 727: body_to_lpda(_T, _X, _V, _Z, dyalog)
c_code local build_ref_727
	ret_reg &ref[727]
	call_c   build_ref_1319()
	call_c   build_ref_1289()
	call_c   Dyam_Term_Start(&ref[1319],5)
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_Arg(V(23))
	call_c   Dyam_Term_Arg(V(21))
	call_c   Dyam_Term_Arg(V(25))
	call_c   Dyam_Term_Arg(&ref[1289])
	call_c   Dyam_Term_End()
	move_ret ref[727]
	c_ret

long local pool_fun261[5]=[131074,build_ref_726,build_seed_155,pool_fun259,pool_fun259]

pl_code local fun262
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(22),V(1))
	fail_ret
fun261:
	call_c   Dyam_Choice(fun260)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[726])
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(23))
	call_c   Dyam_Reg_Load(2,V(19))
	move     V(24), R(4)
	move     S(5), R(5)
	pl_call  pred_tag_numbervars_3()
	pl_call  fun28(&seed[155],1)
	pl_jump  fun259()


;; TERM 725: tag_features(_W, _H, _I)
c_code local build_ref_725
	ret_reg &ref[725]
	call_c   build_ref_1277()
	call_c   Dyam_Term_Start(&ref[1277],3)
	call_c   Dyam_Term_Arg(V(22))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_End()
	move_ret ref[725]
	c_ret

long local pool_fun263[8]=[131077,build_seed_153,build_seed_154,build_ref_723,build_ref_724,build_ref_725,pool_fun261,pool_fun261]

pl_code local fun263
	call_c   Dyam_Pool(pool_fun263)
	call_c   Dyam_Allocate(0)
	pl_call  fun28(&seed[153],1)
	pl_call  fun28(&seed[154],1)
	move     &ref[723], R(0)
	move     S(5), R(1)
	move     V(19), R(2)
	move     S(5), R(3)
	move     V(20), R(4)
	move     S(5), R(5)
	pl_call  pred_tag_numbervars_3()
	call_c   Dyam_Unify(V(21),&ref[724])
	fail_ret
	call_c   Dyam_Choice(fun262)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[725])
	call_c   Dyam_Cut()
	pl_jump  fun261()

;; TERM 732: '$TUPPLE'(35075411943244)
c_code local build_ref_732
	ret_reg &ref[732]
	call_c   Dyam_Create_Simple_Tupple(0,536739840)
	move_ret ref[732]
	c_ret

;; TERM 734: tag_adj((_B / _C))
c_code local build_ref_734
	ret_reg &ref[734]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1294()
	call_c   Dyam_Create_Binary(&ref[1294],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_1368()
	call_c   Dyam_Create_Unary(&ref[1368],R(0))
	move_ret ref[734]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1368: tag_adj
c_code local build_ref_1368
	ret_reg &ref[1368]
	call_c   Dyam_Create_Atom("tag_adj")
	move_ret ref[1368]
	c_ret

;; TERM 735: [_D,_E,_F,_G,_H,_I,_J,+,_K]
c_code local build_ref_735
	ret_reg &ref[735]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1369()
	call_c   build_ref_819()
	call_c   Dyam_Create_List(&ref[1369],&ref[819])
	move_ret R(0)
	call_c   Dyam_Create_Tupple(3,9,R(0))
	move_ret ref[735]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 819: [_K]
c_code local build_ref_819
	ret_reg &ref[819]
	call_c   Dyam_Create_List(V(10),I(0))
	move_ret ref[819]
	c_ret

;; TERM 1369: +
c_code local build_ref_1369
	ret_reg &ref[1369]
	call_c   Dyam_Create_Atom("+")
	move_ret ref[1369]
	c_ret

long local pool_fun264[5]=[4,build_ref_716,build_ref_733,build_ref_734,build_ref_735]

pl_code local fun264
	call_c   Dyam_Pool(pool_fun264)
	call_c   Dyam_Unify_Item(&ref[716])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[733], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	move     &ref[734], R(4)
	move     S(5), R(5)
	move     &ref[735], R(6)
	move     S(5), R(7)
	move     V(11), R(8)
	move     S(5), R(9)
	call_c   Dyam_Reg_Deallocate(5)
	pl_jump  fun130()

;; TERM 715: '*FIRST*'(wrapping_predicate(tag_adj_strict((_B / _C)))) :> []
c_code local build_ref_715
	ret_reg &ref[715]
	call_c   build_ref_714()
	call_c   Dyam_Create_Binary(I(9),&ref[714],I(0))
	move_ret ref[715]
	c_ret

;; TERM 714: '*FIRST*'(wrapping_predicate(tag_adj_strict((_B / _C))))
c_code local build_ref_714
	ret_reg &ref[714]
	call_c   build_ref_52()
	call_c   build_ref_713()
	call_c   Dyam_Create_Unary(&ref[52],&ref[713])
	move_ret ref[714]
	c_ret

c_code local build_seed_34
	ret_reg &seed[34]
	call_c   build_ref_52()
	call_c   build_ref_738()
	call_c   Dyam_Seed_Start(&ref[52],&ref[738],I(0),fun15,1)
	call_c   build_ref_739()
	call_c   Dyam_Seed_Add_Comp(&ref[739],fun269,0)
	call_c   Dyam_Seed_End()
	move_ret seed[34]
	c_ret

;; TERM 739: '*CITEM*'(wrapping_predicate(tag_skipper(_B)), _A)
c_code local build_ref_739
	ret_reg &ref[739]
	call_c   build_ref_56()
	call_c   build_ref_736()
	call_c   Dyam_Create_Binary(&ref[56],&ref[736],V(0))
	move_ret ref[739]
	c_ret

;; TERM 736: wrapping_predicate(tag_skipper(_B))
c_code local build_ref_736
	ret_reg &ref[736]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_682()
	call_c   Dyam_Create_Unary(&ref[682],V(1))
	move_ret R(0)
	call_c   build_ref_1314()
	call_c   Dyam_Create_Unary(&ref[1314],R(0))
	move_ret ref[736]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 759: '$CLOSURE'('$fun'(268, 0, 1129758452), '$TUPPLE'(35075411943592))
c_code local build_ref_759
	ret_reg &ref[759]
	call_c   build_ref_758()
	call_c   Dyam_Closure_Aux(fun268,&ref[758])
	move_ret ref[759]
	c_ret

;; TERM 755: '$CLOSURE'('$fun'(267, 0, 1129743624), '$TUPPLE'(35075411940092))
c_code local build_ref_755
	ret_reg &ref[755]
	call_c   build_ref_372()
	call_c   Dyam_Closure_Aux(fun267,&ref[372])
	move_ret ref[755]
	c_ret

;; TERM 740: skipper(_C, _H, _F, _I)
c_code local build_ref_740
	ret_reg &ref[740]
	call_c   build_ref_1360()
	call_c   Dyam_Term_Start(&ref[1360],4)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_End()
	move_ret ref[740]
	c_ret

;; TERM 741: [_J|_E]
c_code local build_ref_741
	ret_reg &ref[741]
	call_c   Dyam_Create_List(V(9),V(4))
	move_ret ref[741]
	c_ret

;; TERM 744: [_Q]
c_code local build_ref_744
	ret_reg &ref[744]
	call_c   Dyam_Create_List(V(16),I(0))
	move_ret ref[744]
	c_ret

;; TERM 745: '*WRAPPER-INNER-CALL*'(tag_skipper(_B), _G, '*PROLOG-LAST*')
c_code local build_ref_745
	ret_reg &ref[745]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_682()
	call_c   Dyam_Create_Unary(&ref[682],V(1))
	move_ret R(0)
	call_c   build_ref_1370()
	call_c   build_ref_584()
	call_c   Dyam_Term_Start(&ref[1370],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(&ref[584])
	call_c   Dyam_Term_End()
	move_ret ref[745]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1370: '*WRAPPER-INNER-CALL*'
c_code local build_ref_1370
	ret_reg &ref[1370]
	call_c   Dyam_Create_Atom("*WRAPPER-INNER-CALL*")
	move_ret ref[1370]
	c_ret

c_code local build_seed_156
	ret_reg &seed[156]
	call_c   build_ref_48()
	call_c   build_ref_747()
	call_c   Dyam_Seed_Start(&ref[48],&ref[747],I(0),fun3,1)
	call_c   build_ref_748()
	call_c   Dyam_Seed_Add_Comp(&ref[748],&ref[747],0)
	call_c   Dyam_Seed_End()
	move_ret seed[156]
	c_ret

;; TERM 748: '*GUARD*'(body_to_lpda(_K, _Q, _S, _T, dyalog)) :> '$$HOLE$$'
c_code local build_ref_748
	ret_reg &ref[748]
	call_c   build_ref_747()
	call_c   Dyam_Create_Binary(I(9),&ref[747],I(7))
	move_ret ref[748]
	c_ret

;; TERM 747: '*GUARD*'(body_to_lpda(_K, _Q, _S, _T, dyalog))
c_code local build_ref_747
	ret_reg &ref[747]
	call_c   build_ref_48()
	call_c   build_ref_746()
	call_c   Dyam_Create_Unary(&ref[48],&ref[746])
	move_ret ref[747]
	c_ret

;; TERM 746: body_to_lpda(_K, _Q, _S, _T, dyalog)
c_code local build_ref_746
	ret_reg &ref[746]
	call_c   build_ref_1319()
	call_c   build_ref_1289()
	call_c   Dyam_Term_Start(&ref[1319],5)
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_Arg(&ref[1289])
	call_c   Dyam_Term_End()
	move_ret ref[746]
	c_ret

;; TERM 751: unify(_C, _D) :> '*PROLOG-LAST*' :> fail
c_code local build_ref_751
	ret_reg &ref[751]
	call_c   build_ref_749()
	call_c   build_ref_750()
	call_c   Dyam_Create_Binary(I(9),&ref[749],&ref[750])
	move_ret ref[751]
	c_ret

;; TERM 750: '*PROLOG-LAST*' :> fail
c_code local build_ref_750
	ret_reg &ref[750]
	call_c   build_ref_584()
	call_c   build_ref_330()
	call_c   Dyam_Create_Binary(I(9),&ref[584],&ref[330])
	move_ret ref[750]
	c_ret

;; TERM 752: '*ALTERNATIVE-LAYER*'(_V, _U)
c_code local build_ref_752
	ret_reg &ref[752]
	call_c   build_ref_1371()
	call_c   Dyam_Create_Binary(&ref[1371],V(21),V(20))
	move_ret ref[752]
	c_ret

;; TERM 1371: '*ALTERNATIVE-LAYER*'
c_code local build_ref_1371
	ret_reg &ref[1371]
	call_c   Dyam_Create_Atom("*ALTERNATIVE-LAYER*")
	move_ret ref[1371]
	c_ret

;; TERM 753: '*WRAPPER*'(tag_skipper(_B), [_J|_E], _W)
c_code local build_ref_753
	ret_reg &ref[753]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_682()
	call_c   Dyam_Create_Unary(&ref[682],V(1))
	move_ret R(0)
	call_c   build_ref_1348()
	call_c   build_ref_741()
	call_c   Dyam_Term_Start(&ref[1348],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[741])
	call_c   Dyam_Term_Arg(V(22))
	call_c   Dyam_Term_End()
	move_ret ref[753]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun265[8]=[7,build_ref_744,build_ref_745,build_seed_156,build_ref_751,build_ref_752,build_ref_753,build_seed_69]

pl_code local fun266
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(16),V(8))
	fail_ret
fun265:
	move     &ref[744], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(10))
	move     V(17), R(4)
	move     S(5), R(5)
	pl_call  pred_tag_numbervars_3()
	call_c   Dyam_Unify(V(18),&ref[745])
	fail_ret
	pl_call  fun28(&seed[156],1)
	call_c   Dyam_Unify(V(20),V(19))
	fail_ret
	call_c   Dyam_Unify(V(21),&ref[751])
	fail_ret
	call_c   Dyam_Unify(V(22),&ref[752])
	fail_ret
	call_c   DYAM_evpred_assert_1(&ref[753])
	call_c   Dyam_Deallocate()
	pl_jump  fun13(&seed[69],2)


;; TERM 742: '$tagop'(skip, verbose!epsilon(_H, _C, _F, _N))
c_code local build_ref_742
	ret_reg &ref[742]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1374()
	call_c   Dyam_Term_Start(&ref[1374],4)
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_1372()
	call_c   build_ref_1373()
	call_c   Dyam_Create_Binary(&ref[1372],&ref[1373],R(0))
	move_ret ref[742]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1373: skip
c_code local build_ref_1373
	ret_reg &ref[1373]
	call_c   Dyam_Create_Atom("skip")
	move_ret ref[1373]
	c_ret

;; TERM 1372: '$tagop'
c_code local build_ref_1372
	ret_reg &ref[1372]
	call_c   Dyam_Create_Atom("$tagop")
	move_ret ref[1372]
	c_ret

;; TERM 1374: verbose!epsilon
c_code local build_ref_1374
	ret_reg &ref[1374]
	call_c   build_ref_1337()
	call_c   Dyam_Create_Atom_Module("epsilon",&ref[1337])
	move_ret ref[1374]
	c_ret

;; TERM 743: _I , _P
c_code local build_ref_743
	ret_reg &ref[743]
	call_c   Dyam_Create_Binary(I(4),V(8),V(15))
	move_ret ref[743]
	c_ret

long local pool_fun267[8]=[131077,build_ref_740,build_ref_741,build_ref_261,build_ref_742,build_ref_743,pool_fun265,pool_fun265]

pl_code local fun267
	call_c   Dyam_Pool(pool_fun267)
	call_c   Dyam_Allocate(0)
	pl_call  Object_1(&ref[740])
	move     &ref[741], R(0)
	move     S(5), R(1)
	move     V(10), R(2)
	move     S(5), R(3)
	move     V(11), R(4)
	move     S(5), R(5)
	pl_call  pred_tag_numbervars_3()
	call_c   Dyam_Reg_Load(0,V(6))
	call_c   Dyam_Reg_Load(2,V(10))
	move     V(12), R(4)
	move     S(5), R(5)
	pl_call  pred_tag_numbervars_3()
	call_c   Dyam_Choice(fun266)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[261])
	fail_ret
	call_c   Dyam_Cut()
	move     V(13), R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(10))
	move     V(14), R(4)
	move     S(5), R(5)
	pl_call  pred_tag_numbervars_3()
	call_c   Dyam_Unify(V(15),&ref[742])
	fail_ret
	call_c   Dyam_Unify(V(16),&ref[743])
	fail_ret
	pl_jump  fun265()

;; TERM 756: [_F,_D]
c_code local build_ref_756
	ret_reg &ref[756]
	call_c   build_ref_667()
	call_c   Dyam_Create_List(V(5),&ref[667])
	move_ret ref[756]
	c_ret

long local pool_fun268[4]=[3,build_ref_755,build_ref_682,build_ref_756]

pl_code local fun268
	call_c   Dyam_Pool(pool_fun268)
	call_c   Dyam_Allocate(0)
	move     &ref[755], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	move     &ref[682], R(4)
	move     0, R(5)
	move     &ref[756], R(6)
	move     S(5), R(7)
	move     V(6), R(8)
	move     S(5), R(9)
	call_c   Dyam_Reg_Deallocate(5)
	pl_jump  fun130()

;; TERM 758: '$TUPPLE'(35075411943592)
c_code local build_ref_758
	ret_reg &ref[758]
	call_c   Dyam_Create_Simple_Tupple(0,520093696)
	move_ret ref[758]
	c_ret

long local pool_fun269[5]=[4,build_ref_739,build_ref_759,build_ref_682,build_ref_683]

pl_code local fun269
	call_c   Dyam_Pool(pool_fun269)
	call_c   Dyam_Unify_Item(&ref[739])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[759], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	move     &ref[682], R(4)
	move     0, R(5)
	move     &ref[683], R(6)
	move     S(5), R(7)
	move     V(4), R(8)
	move     S(5), R(9)
	call_c   Dyam_Reg_Deallocate(5)
	pl_jump  fun130()

;; TERM 738: '*FIRST*'(wrapping_predicate(tag_skipper(_B))) :> []
c_code local build_ref_738
	ret_reg &ref[738]
	call_c   build_ref_737()
	call_c   Dyam_Create_Binary(I(9),&ref[737],I(0))
	move_ret ref[738]
	c_ret

;; TERM 737: '*FIRST*'(wrapping_predicate(tag_skipper(_B)))
c_code local build_ref_737
	ret_reg &ref[737]
	call_c   build_ref_52()
	call_c   build_ref_736()
	call_c   Dyam_Create_Unary(&ref[52],&ref[736])
	move_ret ref[737]
	c_ret

c_code local build_seed_29
	ret_reg &seed[29]
	call_c   build_ref_47()
	call_c   build_ref_763()
	call_c   Dyam_Seed_Start(&ref[47],&ref[763],I(0),fun1,1)
	call_c   build_ref_762()
	call_c   Dyam_Seed_Add_Comp(&ref[762],fun275,0)
	call_c   Dyam_Seed_End()
	move_ret seed[29]
	c_ret

;; TERM 762: '*GUARD*'(tag_normalize_and_compile(_B, _C, _D, _E, _F, _G, _H))
c_code local build_ref_762
	ret_reg &ref[762]
	call_c   build_ref_48()
	call_c   build_ref_761()
	call_c   Dyam_Create_Unary(&ref[48],&ref[761])
	move_ret ref[762]
	c_ret

;; TERM 761: tag_normalize_and_compile(_B, _C, _D, _E, _F, _G, _H)
c_code local build_ref_761
	ret_reg &ref[761]
	call_c   build_ref_1375()
	call_c   Dyam_Term_Start(&ref[1375],7)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[761]
	c_ret

;; TERM 1375: tag_normalize_and_compile
c_code local build_ref_1375
	ret_reg &ref[1375]
	call_c   Dyam_Create_Atom("tag_normalize_and_compile")
	move_ret ref[1375]
	c_ret

;; TERM 774: '$CLOSURE'('$fun'(274, 0, 1129807160), '$TUPPLE'(35075411943928))
c_code local build_ref_774
	ret_reg &ref[774]
	call_c   build_ref_773()
	call_c   Dyam_Closure_Aux(fun274,&ref[773])
	move_ret ref[774]
	c_ret

c_code local build_seed_157
	ret_reg &seed[157]
	call_c   build_ref_48()
	call_c   build_ref_765()
	call_c   Dyam_Seed_Start(&ref[48],&ref[765],I(0),fun3,1)
	call_c   build_ref_766()
	call_c   Dyam_Seed_Add_Comp(&ref[766],&ref[765],0)
	call_c   Dyam_Seed_End()
	move_ret seed[157]
	c_ret

;; TERM 766: '*GUARD*'(tag_compile_subtree(_B, _K, _D, _E, _F, _N, _H, [], _M)) :> '$$HOLE$$'
c_code local build_ref_766
	ret_reg &ref[766]
	call_c   build_ref_765()
	call_c   Dyam_Create_Binary(I(9),&ref[765],I(7))
	move_ret ref[766]
	c_ret

;; TERM 765: '*GUARD*'(tag_compile_subtree(_B, _K, _D, _E, _F, _N, _H, [], _M))
c_code local build_ref_765
	ret_reg &ref[765]
	call_c   build_ref_48()
	call_c   build_ref_764()
	call_c   Dyam_Create_Unary(&ref[48],&ref[764])
	move_ret ref[765]
	c_ret

;; TERM 764: tag_compile_subtree(_B, _K, _D, _E, _F, _N, _H, [], _M)
c_code local build_ref_764
	ret_reg &ref[764]
	call_c   build_ref_1275()
	call_c   Dyam_Term_Start(&ref[1275],9)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_End()
	move_ret ref[764]
	c_ret

pl_code local fun273
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(6),V(13))
	fail_ret
	pl_jump  fun110()

c_code local build_seed_158
	ret_reg &seed[158]
	call_c   build_ref_48()
	call_c   build_ref_768()
	call_c   Dyam_Seed_Start(&ref[48],&ref[768],I(0),fun3,1)
	call_c   build_ref_769()
	call_c   Dyam_Seed_Add_Comp(&ref[769],&ref[768],0)
	call_c   Dyam_Seed_End()
	move_ret seed[158]
	c_ret

;; TERM 769: '*GUARD*'(tag_compile_subtree(_B, _O, _D, _E, fail, _P, _H, [], _M)) :> '$$HOLE$$'
c_code local build_ref_769
	ret_reg &ref[769]
	call_c   build_ref_768()
	call_c   Dyam_Create_Binary(I(9),&ref[768],I(7))
	move_ret ref[769]
	c_ret

;; TERM 768: '*GUARD*'(tag_compile_subtree(_B, _O, _D, _E, fail, _P, _H, [], _M))
c_code local build_ref_768
	ret_reg &ref[768]
	call_c   build_ref_48()
	call_c   build_ref_767()
	call_c   Dyam_Create_Unary(&ref[48],&ref[767])
	move_ret ref[768]
	c_ret

;; TERM 767: tag_compile_subtree(_B, _O, _D, _E, fail, _P, _H, [], _M)
c_code local build_ref_767
	ret_reg &ref[767]
	call_c   build_ref_1275()
	call_c   build_ref_330()
	call_c   Dyam_Term_Start(&ref[1275],9)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(&ref[330])
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_End()
	move_ret ref[767]
	c_ret

;; TERM 771: '*WAIT*'(_N) :> _P
c_code local build_ref_771
	ret_reg &ref[771]
	call_c   build_ref_770()
	call_c   Dyam_Create_Binary(I(9),&ref[770],V(15))
	move_ret ref[771]
	c_ret

;; TERM 770: '*WAIT*'(_N)
c_code local build_ref_770
	ret_reg &ref[770]
	call_c   build_ref_1376()
	call_c   Dyam_Create_Unary(&ref[1376],V(13))
	move_ret ref[770]
	c_ret

;; TERM 1376: '*WAIT*'
c_code local build_ref_1376
	ret_reg &ref[1376]
	call_c   Dyam_Create_Atom("*WAIT*")
	move_ret ref[1376]
	c_ret

long local pool_fun274[6]=[5,build_seed_157,build_ref_483,build_ref_397,build_seed_158,build_ref_771]

pl_code local fun274
	call_c   Dyam_Pool(pool_fun274)
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(10))
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(11), R(4)
	move     S(5), R(5)
	pl_call  pred_tag_numbervars_3()
	move     I(0), R(0)
	move     0, R(1)
	move     V(12), R(2)
	move     S(5), R(3)
	pl_call  pred_tupple_2()
	pl_call  fun28(&seed[157],1)
	call_c   Dyam_Choice(fun273)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[483])
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(10))
	move     &ref[397], R(2)
	move     0, R(3)
	move     V(14), R(4)
	move     S(5), R(5)
	pl_call  pred_tagguide_strip_3()
	pl_call  fun28(&seed[158],1)
	call_c   Dyam_Unify(V(6),&ref[771])
	fail_ret
	pl_jump  fun110()

;; TERM 773: '$TUPPLE'(35075411943928)
c_code local build_ref_773
	ret_reg &ref[773]
	call_c   Dyam_Create_Simple_Tupple(0,199491584)
	move_ret ref[773]
	c_ret

;; TERM 775: tag_tree{family=> _I, name=> _J, tree=> (tree _C)}
c_code local build_ref_775
	ret_reg &ref[775]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1287()
	call_c   Dyam_Create_Unary(&ref[1287],V(2))
	move_ret R(0)
	call_c   build_ref_1284()
	call_c   Dyam_Term_Start(&ref[1284],3)
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_End()
	move_ret ref[775]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun275[4]=[3,build_ref_762,build_ref_774,build_ref_775]

pl_code local fun275
	call_c   Dyam_Pool(pool_fun275)
	call_c   Dyam_Unify_Item(&ref[762])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[774], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	move     &ref[775], R(4)
	move     S(5), R(5)
	move     V(10), R(6)
	move     S(5), R(7)
	call_c   Dyam_Reg_Deallocate(4)
	pl_jump  fun74()

;; TERM 763: '*GUARD*'(tag_normalize_and_compile(_B, _C, _D, _E, _F, _G, _H)) :> []
c_code local build_ref_763
	ret_reg &ref[763]
	call_c   build_ref_762()
	call_c   Dyam_Create_Binary(I(9),&ref[762],I(0))
	move_ret ref[763]
	c_ret

c_code local build_seed_19
	ret_reg &seed[19]
	call_c   build_ref_47()
	call_c   build_ref_789()
	call_c   Dyam_Seed_Start(&ref[47],&ref[789],I(0),fun1,1)
	call_c   build_ref_788()
	call_c   Dyam_Seed_Add_Comp(&ref[788],fun298,0)
	call_c   Dyam_Seed_End()
	move_ret seed[19]
	c_ret

;; TERM 788: '*GUARD*'(tag_il_body_to_lpda_handler(_B, (_C , _D), _E, _F, _G, _H, _I, _J, _K, _L, _M, _N, _O))
c_code local build_ref_788
	ret_reg &ref[788]
	call_c   build_ref_48()
	call_c   build_ref_787()
	call_c   Dyam_Create_Unary(&ref[48],&ref[787])
	move_ret ref[788]
	c_ret

;; TERM 787: tag_il_body_to_lpda_handler(_B, (_C , _D), _E, _F, _G, _H, _I, _J, _K, _L, _M, _N, _O)
c_code local build_ref_787
	ret_reg &ref[787]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Binary(I(4),V(2),V(3))
	move_ret R(0)
	call_c   build_ref_1295()
	call_c   Dyam_Term_Start(&ref[1295],13)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_End()
	move_ret ref[787]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 790: [_P,_Q]
c_code local build_ref_790
	ret_reg &ref[790]
	call_c   Dyam_Create_Tupple(15,16,I(0))
	move_ret ref[790]
	c_ret

long local pool_fun298[4]=[3,build_ref_788,build_ref_790,build_ref_274]

pl_code local fun298
	call_c   Dyam_Pool(pool_fun298)
	call_c   Dyam_Unify_Item(&ref[788])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[790], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(17), R(4)
	move     S(5), R(5)
	pl_call  pred_tag_numbervars_3()
	call_c   Dyam_Reg_Load(0,V(2))
	call_c   Dyam_Reg_Load(2,V(14))
	move     V(18), R(4)
	move     S(5), R(5)
	pl_call  pred_extend_tupple_3()
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(3))
	call_c   Dyam_Reg_Load(4,V(15))
	call_c   Dyam_Reg_Load(6,V(16))
	call_c   Dyam_Reg_Load(8,V(6))
	call_c   Dyam_Reg_Load(10,V(7))
	call_c   Dyam_Reg_Load(12,V(8))
	move     &ref[274], R(14)
	move     0, R(15)
	move     V(19), R(16)
	move     S(5), R(17)
	call_c   Dyam_Reg_Load(18,V(11))
	call_c   Dyam_Reg_Load(20,V(12))
	call_c   Dyam_Reg_Load(22,V(13))
	call_c   Dyam_Reg_Load(24,V(18))
	pl_call  pred_tag_il_body_to_lpda_13()
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(2))
	call_c   Dyam_Reg_Load(4,V(4))
	call_c   Dyam_Reg_Load(6,V(5))
	call_c   Dyam_Reg_Load(8,V(15))
	call_c   Dyam_Reg_Load(10,V(16))
	call_c   Dyam_Reg_Load(12,V(19))
	call_c   Dyam_Reg_Load(14,V(9))
	call_c   Dyam_Reg_Load(16,V(10))
	call_c   Dyam_Reg_Load(18,V(11))
	call_c   Dyam_Reg_Load(20,V(12))
	call_c   Dyam_Reg_Load(22,V(13))
	call_c   Dyam_Reg_Load(24,V(14))
	call_c   Dyam_Reg_Deallocate(13)
	pl_jump  pred_tag_il_body_to_lpda_13()

;; TERM 789: '*GUARD*'(tag_il_body_to_lpda_handler(_B, (_C , _D), _E, _F, _G, _H, _I, _J, _K, _L, _M, _N, _O)) :> []
c_code local build_ref_789
	ret_reg &ref[789]
	call_c   build_ref_788()
	call_c   Dyam_Create_Binary(I(9),&ref[788],I(0))
	move_ret ref[789]
	c_ret

c_code local build_seed_55
	ret_reg &seed[55]
	call_c   build_ref_47()
	call_c   build_ref_793()
	call_c   Dyam_Seed_Start(&ref[47],&ref[793],I(0),fun1,1)
	call_c   build_ref_792()
	call_c   Dyam_Seed_Add_Comp(&ref[792],fun308,0)
	call_c   Dyam_Seed_End()
	move_ret seed[55]
	c_ret

;; TERM 792: '*GUARD*'(tag_compile_subtree_handler(_B, tag_node{id=> _C, label=> '$call'(_D), children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}, _O, _P, _Q, _R, _S, _T, _U))
c_code local build_ref_792
	ret_reg &ref[792]
	call_c   build_ref_48()
	call_c   build_ref_791()
	call_c   Dyam_Create_Unary(&ref[48],&ref[791])
	move_ret ref[792]
	c_ret

;; TERM 791: tag_compile_subtree_handler(_B, tag_node{id=> _C, label=> '$call'(_D), children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}, _O, _P, _Q, _R, _S, _T, _U)
c_code local build_ref_791
	ret_reg &ref[791]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1377()
	call_c   Dyam_Create_Unary(&ref[1377],V(3))
	move_ret R(0)
	call_c   build_ref_1259()
	call_c   Dyam_Term_Start(&ref[1259],12)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_1278()
	call_c   Dyam_Term_Start(&ref[1278],9)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_End()
	move_ret ref[791]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1377: '$call'
c_code local build_ref_1377
	ret_reg &ref[1377]
	call_c   Dyam_Create_Atom("$call")
	move_ret ref[1377]
	c_ret

;; TERM 812: '$CLOSURE'('$fun'(307, 0, 1129993324), '$TUPPLE'(35075412199252))
c_code local build_ref_812
	ret_reg &ref[812]
	call_c   build_ref_811()
	call_c   Dyam_Closure_Aux(fun307,&ref[811])
	move_ret ref[812]
	c_ret

;; TERM 167: tag_node{id=> _C, label=> _D, children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}
c_code local build_ref_167
	ret_reg &ref[167]
	call_c   build_ref_1259()
	call_c   Dyam_Term_Start(&ref[1259],12)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_End()
	move_ret ref[167]
	c_ret

c_code local build_seed_163
	ret_reg &seed[163]
	call_c   build_ref_48()
	call_c   build_ref_808()
	call_c   Dyam_Seed_Start(&ref[48],&ref[808],I(0),fun3,1)
	call_c   build_ref_809()
	call_c   Dyam_Seed_Add_Comp(&ref[809],&ref[808],0)
	call_c   Dyam_Seed_End()
	move_ret seed[163]
	c_ret

;; TERM 809: '*GUARD*'(body_to_lpda(_B, call!tagcall(_V, _O, _P, _T), _Q, _R, _S)) :> '$$HOLE$$'
c_code local build_ref_809
	ret_reg &ref[809]
	call_c   build_ref_808()
	call_c   Dyam_Create_Binary(I(9),&ref[808],I(7))
	move_ret ref[809]
	c_ret

;; TERM 808: '*GUARD*'(body_to_lpda(_B, call!tagcall(_V, _O, _P, _T), _Q, _R, _S))
c_code local build_ref_808
	ret_reg &ref[808]
	call_c   build_ref_48()
	call_c   build_ref_807()
	call_c   Dyam_Create_Unary(&ref[48],&ref[807])
	move_ret ref[808]
	c_ret

;; TERM 807: body_to_lpda(_B, call!tagcall(_V, _O, _P, _T), _Q, _R, _S)
c_code local build_ref_807
	ret_reg &ref[807]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1378()
	call_c   Dyam_Term_Start(&ref[1378],4)
	call_c   Dyam_Term_Arg(V(21))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_1319()
	call_c   Dyam_Term_Start(&ref[1319],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_End()
	move_ret ref[807]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1378: call!tagcall
c_code local build_ref_1378
	ret_reg &ref[1378]
	call_c   build_ref_1379()
	call_c   Dyam_Create_Atom_Module("tagcall",&ref[1379])
	move_ret ref[1378]
	c_ret

;; TERM 1379: call
c_code local build_ref_1379
	ret_reg &ref[1379]
	call_c   Dyam_Create_Atom("call")
	move_ret ref[1379]
	c_ret

long local pool_fun305[2]=[1,build_seed_163]

pl_code local fun305
	call_c   Dyam_Remove_Choice()
	pl_call  fun28(&seed[163],1)
	pl_jump  fun110()

;; TERM 803: protect(_W)
c_code local build_ref_803
	ret_reg &ref[803]
	call_c   build_ref_1239()
	call_c   Dyam_Create_Unary(&ref[1239],V(22))
	move_ret ref[803]
	c_ret

c_code local build_seed_162
	ret_reg &seed[162]
	call_c   build_ref_48()
	call_c   build_ref_805()
	call_c   Dyam_Seed_Start(&ref[48],&ref[805],I(0),fun3,1)
	call_c   build_ref_806()
	call_c   Dyam_Seed_Add_Comp(&ref[806],&ref[805],0)
	call_c   Dyam_Seed_End()
	move_ret seed[162]
	c_ret

;; TERM 806: '*GUARD*'(body_to_lpda(_B, call!tagcall('$protect'(_V), _O, _P, _T), _Q, _R, _W)) :> '$$HOLE$$'
c_code local build_ref_806
	ret_reg &ref[806]
	call_c   build_ref_805()
	call_c   Dyam_Create_Binary(I(9),&ref[805],I(7))
	move_ret ref[806]
	c_ret

;; TERM 805: '*GUARD*'(body_to_lpda(_B, call!tagcall('$protect'(_V), _O, _P, _T), _Q, _R, _W))
c_code local build_ref_805
	ret_reg &ref[805]
	call_c   build_ref_48()
	call_c   build_ref_804()
	call_c   Dyam_Create_Unary(&ref[48],&ref[804])
	move_ret ref[805]
	c_ret

;; TERM 804: body_to_lpda(_B, call!tagcall('$protect'(_V), _O, _P, _T), _Q, _R, _W)
c_code local build_ref_804
	ret_reg &ref[804]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1273()
	call_c   Dyam_Create_Unary(&ref[1273],V(21))
	move_ret R(0)
	call_c   build_ref_1378()
	call_c   Dyam_Term_Start(&ref[1378],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_1319()
	call_c   Dyam_Term_Start(&ref[1319],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(22))
	call_c   Dyam_Term_End()
	move_ret ref[804]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun306[4]=[65538,build_ref_803,build_seed_162,pool_fun305]

pl_code local fun306
	call_c   Dyam_Update_Choice(fun305)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(18),&ref[803])
	fail_ret
	call_c   Dyam_Cut()
	pl_call  fun28(&seed[162],1)
	pl_jump  fun110()

;; TERM 799: answer(_W)
c_code local build_ref_799
	ret_reg &ref[799]
	call_c   build_ref_1281()
	call_c   Dyam_Create_Unary(&ref[1281],V(22))
	move_ret ref[799]
	c_ret

c_code local build_seed_161
	ret_reg &seed[161]
	call_c   build_ref_48()
	call_c   build_ref_801()
	call_c   Dyam_Seed_Start(&ref[48],&ref[801],I(0),fun3,1)
	call_c   build_ref_802()
	call_c   Dyam_Seed_Add_Comp(&ref[802],&ref[801],0)
	call_c   Dyam_Seed_End()
	move_ret seed[161]
	c_ret

;; TERM 802: '*GUARD*'(body_to_lpda(_B, call!tagcall('$answers'(_V), _O, _P, _T), _Q, _R, _W)) :> '$$HOLE$$'
c_code local build_ref_802
	ret_reg &ref[802]
	call_c   build_ref_801()
	call_c   Dyam_Create_Binary(I(9),&ref[801],I(7))
	move_ret ref[802]
	c_ret

;; TERM 801: '*GUARD*'(body_to_lpda(_B, call!tagcall('$answers'(_V), _O, _P, _T), _Q, _R, _W))
c_code local build_ref_801
	ret_reg &ref[801]
	call_c   build_ref_48()
	call_c   build_ref_800()
	call_c   Dyam_Create_Unary(&ref[48],&ref[800])
	move_ret ref[801]
	c_ret

;; TERM 800: body_to_lpda(_B, call!tagcall('$answers'(_V), _O, _P, _T), _Q, _R, _W)
c_code local build_ref_800
	ret_reg &ref[800]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1276()
	call_c   Dyam_Create_Unary(&ref[1276],V(21))
	move_ret R(0)
	call_c   build_ref_1378()
	call_c   Dyam_Term_Start(&ref[1378],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_1319()
	call_c   Dyam_Term_Start(&ref[1319],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(22))
	call_c   Dyam_Term_End()
	move_ret ref[800]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun307[5]=[65539,build_ref_167,build_ref_799,build_seed_161,pool_fun306]

pl_code local fun307
	call_c   Dyam_Pool(pool_fun307)
	call_c   Dyam_Unify(V(21),&ref[167])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun306)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(18),&ref[799])
	fail_ret
	call_c   Dyam_Cut()
	pl_call  fun28(&seed[161],1)
	pl_jump  fun110()

;; TERM 811: '$TUPPLE'(35075412199252)
c_code local build_ref_811
	ret_reg &ref[811]
	call_c   Dyam_Create_Simple_Tupple(0,268434944)
	move_ret ref[811]
	c_ret

long local pool_fun308[3]=[2,build_ref_792,build_ref_812]

pl_code local fun308
	call_c   Dyam_Pool(pool_fun308)
	call_c   Dyam_Unify_Item(&ref[792])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[812], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Deallocate(2)
fun304:
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(2)
	call_c   Dyam_Choice(fun303)
	pl_call  fun46(&seed[159],1)
	pl_fail


;; TERM 793: '*GUARD*'(tag_compile_subtree_handler(_B, tag_node{id=> _C, label=> '$call'(_D), children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}, _O, _P, _Q, _R, _S, _T, _U)) :> []
c_code local build_ref_793
	ret_reg &ref[793]
	call_c   build_ref_792()
	call_c   Dyam_Create_Binary(I(9),&ref[792],I(0))
	move_ret ref[793]
	c_ret

c_code local build_seed_58
	ret_reg &seed[58]
	call_c   build_ref_47()
	call_c   build_ref_815()
	call_c   Dyam_Seed_Start(&ref[47],&ref[815],I(0),fun1,1)
	call_c   build_ref_814()
	call_c   Dyam_Seed_Add_Comp(&ref[814],fun312,0)
	call_c   Dyam_Seed_End()
	move_ret seed[58]
	c_ret

;; TERM 814: '*GUARD*'(tag_compile_subtree(_B, _C, _D, _E, _F, _G, _H, _I, _J))
c_code local build_ref_814
	ret_reg &ref[814]
	call_c   build_ref_48()
	call_c   build_ref_813()
	call_c   Dyam_Create_Unary(&ref[48],&ref[813])
	move_ret ref[814]
	c_ret

;; TERM 813: tag_compile_subtree(_B, _C, _D, _E, _F, _G, _H, _I, _J)
c_code local build_ref_813
	ret_reg &ref[813]
	call_c   build_ref_1275()
	call_c   Dyam_Term_Start(&ref[1275],9)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret ref[813]
	c_ret

c_code local build_seed_168
	ret_reg &seed[168]
	call_c   build_ref_48()
	call_c   build_ref_831()
	call_c   Dyam_Seed_Start(&ref[48],&ref[831],I(0),fun3,1)
	call_c   build_ref_832()
	call_c   Dyam_Seed_Add_Comp(&ref[832],&ref[831],0)
	call_c   Dyam_Seed_End()
	move_ret seed[168]
	c_ret

;; TERM 832: '*GUARD*'(tag_compile_node(_B, _C, _D, _E, _F, _G, _H, _I, _J)) :> '$$HOLE$$'
c_code local build_ref_832
	ret_reg &ref[832]
	call_c   build_ref_831()
	call_c   Dyam_Create_Binary(I(9),&ref[831],I(7))
	move_ret ref[832]
	c_ret

;; TERM 831: '*GUARD*'(tag_compile_node(_B, _C, _D, _E, _F, _G, _H, _I, _J))
c_code local build_ref_831
	ret_reg &ref[831]
	call_c   build_ref_48()
	call_c   build_ref_830()
	call_c   Dyam_Create_Unary(&ref[48],&ref[830])
	move_ret ref[831]
	c_ret

;; TERM 830: tag_compile_node(_B, _C, _D, _E, _F, _G, _H, _I, _J)
c_code local build_ref_830
	ret_reg &ref[830]
	call_c   build_ref_1380()
	call_c   Dyam_Term_Start(&ref[1380],9)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret ref[830]
	c_ret

;; TERM 1380: tag_compile_node
c_code local build_ref_1380
	ret_reg &ref[1380]
	call_c   Dyam_Create_Atom("tag_compile_node")
	move_ret ref[1380]
	c_ret

long local pool_fun309[2]=[1,build_seed_168]

pl_code local fun309
	call_c   Dyam_Remove_Choice()
	pl_call  fun28(&seed[168],1)
	pl_jump  fun110()

;; TERM 823: [_K|_L]
c_code local build_ref_823
	ret_reg &ref[823]
	call_c   Dyam_Create_List(V(10),V(11))
	move_ret ref[823]
	c_ret

c_code local build_seed_166
	ret_reg &seed[166]
	call_c   build_ref_48()
	call_c   build_ref_825()
	call_c   Dyam_Seed_Start(&ref[48],&ref[825],I(0),fun3,1)
	call_c   build_ref_826()
	call_c   Dyam_Seed_Add_Comp(&ref[826],&ref[825],0)
	call_c   Dyam_Seed_End()
	move_ret seed[166]
	c_ret

;; TERM 826: '*GUARD*'(tag_compile_subtree(_B, _L, _M, _E, _F, _P, _H, _I, _O)) :> '$$HOLE$$'
c_code local build_ref_826
	ret_reg &ref[826]
	call_c   build_ref_825()
	call_c   Dyam_Create_Binary(I(9),&ref[825],I(7))
	move_ret ref[826]
	c_ret

;; TERM 825: '*GUARD*'(tag_compile_subtree(_B, _L, _M, _E, _F, _P, _H, _I, _O))
c_code local build_ref_825
	ret_reg &ref[825]
	call_c   build_ref_48()
	call_c   build_ref_824()
	call_c   Dyam_Create_Unary(&ref[48],&ref[824])
	move_ret ref[825]
	c_ret

;; TERM 824: tag_compile_subtree(_B, _L, _M, _E, _F, _P, _H, _I, _O)
c_code local build_ref_824
	ret_reg &ref[824]
	call_c   build_ref_1275()
	call_c   Dyam_Term_Start(&ref[1275],9)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_End()
	move_ret ref[824]
	c_ret

c_code local build_seed_167
	ret_reg &seed[167]
	call_c   build_ref_48()
	call_c   build_ref_828()
	call_c   Dyam_Seed_Start(&ref[48],&ref[828],I(0),fun3,1)
	call_c   build_ref_829()
	call_c   Dyam_Seed_Add_Comp(&ref[829],&ref[828],0)
	call_c   Dyam_Seed_End()
	move_ret seed[167]
	c_ret

;; TERM 829: '*GUARD*'(tag_compile_subtree(_B, _K, _D, _M, _P, _G, _H, _I, _J)) :> '$$HOLE$$'
c_code local build_ref_829
	ret_reg &ref[829]
	call_c   build_ref_828()
	call_c   Dyam_Create_Binary(I(9),&ref[828],I(7))
	move_ret ref[829]
	c_ret

;; TERM 828: '*GUARD*'(tag_compile_subtree(_B, _K, _D, _M, _P, _G, _H, _I, _J))
c_code local build_ref_828
	ret_reg &ref[828]
	call_c   build_ref_48()
	call_c   build_ref_827()
	call_c   Dyam_Create_Unary(&ref[48],&ref[827])
	move_ret ref[828]
	c_ret

;; TERM 827: tag_compile_subtree(_B, _K, _D, _M, _P, _G, _H, _I, _J)
c_code local build_ref_827
	ret_reg &ref[827]
	call_c   build_ref_1275()
	call_c   Dyam_Term_Start(&ref[1275],9)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret ref[827]
	c_ret

long local pool_fun310[5]=[65539,build_ref_823,build_seed_166,build_seed_167,pool_fun309]

pl_code local fun310
	call_c   Dyam_Update_Choice(fun309)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(2),&ref[823])
	fail_ret
	call_c   Dyam_Cut()
	move     V(12), R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(13), R(4)
	move     S(5), R(5)
	pl_call  pred_tag_numbervars_3()
	call_c   Dyam_Reg_Load(0,V(10))
	call_c   Dyam_Reg_Load(2,V(9))
	move     V(14), R(4)
	move     S(5), R(5)
	pl_call  pred_extend_tupple_3()
	pl_call  fun28(&seed[166],1)
	pl_call  fun28(&seed[167],1)
	pl_jump  fun110()

c_code local build_seed_165
	ret_reg &seed[165]
	call_c   build_ref_48()
	call_c   build_ref_821()
	call_c   Dyam_Seed_Start(&ref[48],&ref[821],I(0),fun3,1)
	call_c   build_ref_822()
	call_c   Dyam_Seed_Add_Comp(&ref[822],&ref[821],0)
	call_c   Dyam_Seed_End()
	move_ret seed[165]
	c_ret

;; TERM 822: '*GUARD*'(tag_compile_subtree(_B, _K, _D, _E, _F, _G, _H, _I, _J)) :> '$$HOLE$$'
c_code local build_ref_822
	ret_reg &ref[822]
	call_c   build_ref_821()
	call_c   Dyam_Create_Binary(I(9),&ref[821],I(7))
	move_ret ref[822]
	c_ret

;; TERM 821: '*GUARD*'(tag_compile_subtree(_B, _K, _D, _E, _F, _G, _H, _I, _J))
c_code local build_ref_821
	ret_reg &ref[821]
	call_c   build_ref_48()
	call_c   build_ref_820()
	call_c   Dyam_Create_Unary(&ref[48],&ref[820])
	move_ret ref[821]
	c_ret

;; TERM 820: tag_compile_subtree(_B, _K, _D, _E, _F, _G, _H, _I, _J)
c_code local build_ref_820
	ret_reg &ref[820]
	call_c   build_ref_1275()
	call_c   Dyam_Term_Start(&ref[1275],9)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret ref[820]
	c_ret

long local pool_fun311[4]=[65538,build_ref_819,build_seed_165,pool_fun310]

pl_code local fun311
	call_c   Dyam_Update_Choice(fun310)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(2),&ref[819])
	fail_ret
	call_c   Dyam_Cut()
	pl_call  fun28(&seed[165],1)
	pl_jump  fun110()

c_code local build_seed_164
	ret_reg &seed[164]
	call_c   build_ref_48()
	call_c   build_ref_817()
	call_c   Dyam_Seed_Start(&ref[48],&ref[817],I(0),fun3,1)
	call_c   build_ref_818()
	call_c   Dyam_Seed_Add_Comp(&ref[818],&ref[817],0)
	call_c   Dyam_Seed_End()
	move_ret seed[164]
	c_ret

;; TERM 818: '*GUARD*'(tag_compile_subtree_handler(_B, _C, _D, _E, _F, _G, _H, _I, _J)) :> '$$HOLE$$'
c_code local build_ref_818
	ret_reg &ref[818]
	call_c   build_ref_817()
	call_c   Dyam_Create_Binary(I(9),&ref[817],I(7))
	move_ret ref[818]
	c_ret

;; TERM 817: '*GUARD*'(tag_compile_subtree_handler(_B, _C, _D, _E, _F, _G, _H, _I, _J))
c_code local build_ref_817
	ret_reg &ref[817]
	call_c   build_ref_48()
	call_c   build_ref_816()
	call_c   Dyam_Create_Unary(&ref[48],&ref[816])
	move_ret ref[817]
	c_ret

;; TERM 816: tag_compile_subtree_handler(_B, _C, _D, _E, _F, _G, _H, _I, _J)
c_code local build_ref_816
	ret_reg &ref[816]
	call_c   build_ref_1278()
	call_c   Dyam_Term_Start(&ref[1278],9)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret ref[816]
	c_ret

long local pool_fun312[4]=[65538,build_ref_814,build_seed_164,pool_fun311]

pl_code local fun312
	call_c   Dyam_Pool(pool_fun312)
	call_c   Dyam_Unify_Item(&ref[814])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun311)
	call_c   Dyam_Set_Cut()
	pl_call  fun28(&seed[164],1)
	call_c   Dyam_Cut()
	pl_jump  fun110()

;; TERM 815: '*GUARD*'(tag_compile_subtree(_B, _C, _D, _E, _F, _G, _H, _I, _J)) :> []
c_code local build_ref_815
	ret_reg &ref[815]
	call_c   build_ref_814()
	call_c   Dyam_Create_Binary(I(9),&ref[814],I(0))
	move_ret ref[815]
	c_ret

c_code local build_seed_40
	ret_reg &seed[40]
	call_c   build_ref_52()
	call_c   build_ref_835()
	call_c   Dyam_Seed_Start(&ref[52],&ref[835],I(0),fun15,1)
	call_c   build_ref_836()
	call_c   Dyam_Seed_Add_Comp(&ref[836],fun320,0)
	call_c   Dyam_Seed_End()
	move_ret seed[40]
	c_ret

;; TERM 836: '*CITEM*'(wrapping_predicate(tag_adj((_B / _C))), _A)
c_code local build_ref_836
	ret_reg &ref[836]
	call_c   build_ref_56()
	call_c   build_ref_833()
	call_c   Dyam_Create_Binary(&ref[56],&ref[833],V(0))
	move_ret ref[836]
	c_ret

;; TERM 833: wrapping_predicate(tag_adj((_B / _C)))
c_code local build_ref_833
	ret_reg &ref[833]
	call_c   build_ref_1314()
	call_c   build_ref_734()
	call_c   Dyam_Create_Unary(&ref[1314],&ref[734])
	move_ret ref[833]
	c_ret

;; TERM 862: '$CLOSURE'('$fun'(319, 0, 1130130828), '$TUPPLE'(35075412200928))
c_code local build_ref_862
	ret_reg &ref[862]
	call_c   build_ref_861()
	call_c   Dyam_Closure_Aux(fun319,&ref[861])
	move_ret ref[862]
	c_ret

c_code local build_seed_169
	ret_reg &seed[169]
	call_c   build_ref_48()
	call_c   build_ref_838()
	call_c   Dyam_Seed_Start(&ref[48],&ref[838],I(0),fun3,1)
	call_c   build_ref_839()
	call_c   Dyam_Seed_Add_Comp(&ref[839],&ref[838],0)
	call_c   Dyam_Seed_End()
	move_ret seed[169]
	c_ret

;; TERM 839: '*GUARD*'(make_tag_top_callret(_H, _D, _E, _N, _O)) :> '$$HOLE$$'
c_code local build_ref_839
	ret_reg &ref[839]
	call_c   build_ref_838()
	call_c   Dyam_Create_Binary(I(9),&ref[838],I(7))
	move_ret ref[839]
	c_ret

;; TERM 838: '*GUARD*'(make_tag_top_callret(_H, _D, _E, _N, _O))
c_code local build_ref_838
	ret_reg &ref[838]
	call_c   build_ref_48()
	call_c   build_ref_837()
	call_c   Dyam_Create_Unary(&ref[48],&ref[837])
	move_ret ref[838]
	c_ret

;; TERM 837: make_tag_top_callret(_H, _D, _E, _N, _O)
c_code local build_ref_837
	ret_reg &ref[837]
	call_c   build_ref_1340()
	call_c   Dyam_Term_Start(&ref[1340],5)
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_End()
	move_ret ref[837]
	c_ret

c_code local build_seed_170
	ret_reg &seed[170]
	call_c   build_ref_48()
	call_c   build_ref_841()
	call_c   Dyam_Seed_Start(&ref[48],&ref[841],I(0),fun3,1)
	call_c   build_ref_842()
	call_c   Dyam_Seed_Add_Comp(&ref[842],&ref[841],0)
	call_c   Dyam_Seed_End()
	move_ret seed[170]
	c_ret

;; TERM 842: '*GUARD*'(make_tag_bot_callret(_I, _F, _G, _P, _Q)) :> '$$HOLE$$'
c_code local build_ref_842
	ret_reg &ref[842]
	call_c   build_ref_841()
	call_c   Dyam_Create_Binary(I(9),&ref[841],I(7))
	move_ret ref[842]
	c_ret

;; TERM 841: '*GUARD*'(make_tag_bot_callret(_I, _F, _G, _P, _Q))
c_code local build_ref_841
	ret_reg &ref[841]
	call_c   build_ref_48()
	call_c   build_ref_840()
	call_c   Dyam_Create_Unary(&ref[48],&ref[840])
	move_ret ref[841]
	c_ret

;; TERM 840: make_tag_bot_callret(_I, _F, _G, _P, _Q)
c_code local build_ref_840
	ret_reg &ref[840]
	call_c   build_ref_1315()
	call_c   Dyam_Term_Start(&ref[1315],5)
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_End()
	move_ret ref[840]
	c_ret

;; TERM 843: [_R,_S,_T|_M]
c_code local build_ref_843
	ret_reg &ref[843]
	call_c   Dyam_Create_Tupple(17,19,V(12))
	move_ret ref[843]
	c_ret

c_code local build_seed_171
	ret_reg &seed[171]
	call_c   build_ref_48()
	call_c   build_ref_845()
	call_c   Dyam_Seed_Start(&ref[48],&ref[845],I(0),fun3,1)
	call_c   build_ref_846()
	call_c   Dyam_Seed_Add_Comp(&ref[846],&ref[845],0)
	call_c   Dyam_Seed_End()
	move_ret seed[171]
	c_ret

;; TERM 846: '*GUARD*'(body_to_lpda(_U, (_N = _P , _O = _Q), '*SA-PROLOG-LAST*', _W, dyalog)) :> '$$HOLE$$'
c_code local build_ref_846
	ret_reg &ref[846]
	call_c   build_ref_845()
	call_c   Dyam_Create_Binary(I(9),&ref[845],I(7))
	move_ret ref[846]
	c_ret

;; TERM 845: '*GUARD*'(body_to_lpda(_U, (_N = _P , _O = _Q), '*SA-PROLOG-LAST*', _W, dyalog))
c_code local build_ref_845
	ret_reg &ref[845]
	call_c   build_ref_48()
	call_c   build_ref_844()
	call_c   Dyam_Create_Unary(&ref[48],&ref[844])
	move_ret ref[845]
	c_ret

;; TERM 844: body_to_lpda(_U, (_N = _P , _O = _Q), '*SA-PROLOG-LAST*', _W, dyalog)
c_code local build_ref_844
	ret_reg &ref[844]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_1325()
	call_c   Dyam_Create_Binary(&ref[1325],V(13),V(15))
	move_ret R(0)
	call_c   Dyam_Create_Binary(&ref[1325],V(14),V(16))
	move_ret R(1)
	call_c   Dyam_Create_Binary(I(4),R(0),R(1))
	move_ret R(1)
	call_c   build_ref_1319()
	call_c   build_ref_847()
	call_c   build_ref_1289()
	call_c   Dyam_Term_Start(&ref[1319],5)
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(&ref[847])
	call_c   Dyam_Term_Arg(V(22))
	call_c   Dyam_Term_Arg(&ref[1289])
	call_c   Dyam_Term_End()
	move_ret ref[844]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 848: '*SA-ADJ-FIRST*'(_N, _P, '*SA-PROLOG-LAST*')
c_code local build_ref_848
	ret_reg &ref[848]
	call_c   build_ref_1365()
	call_c   build_ref_847()
	call_c   Dyam_Term_Start(&ref[1365],3)
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(&ref[847])
	call_c   Dyam_Term_End()
	move_ret ref[848]
	c_ret

c_code local build_seed_172
	ret_reg &seed[172]
	call_c   build_ref_48()
	call_c   build_ref_850()
	call_c   Dyam_Seed_Start(&ref[48],&ref[850],I(0),fun3,1)
	call_c   build_ref_851()
	call_c   Dyam_Seed_Add_Comp(&ref[851],&ref[850],0)
	call_c   Dyam_Seed_End()
	move_ret seed[172]
	c_ret

;; TERM 851: '*GUARD*'(body_to_lpda(_U, (_K = +), _Y, _Z, dyalog)) :> '$$HOLE$$'
c_code local build_ref_851
	ret_reg &ref[851]
	call_c   build_ref_850()
	call_c   Dyam_Create_Binary(I(9),&ref[850],I(7))
	move_ret ref[851]
	c_ret

;; TERM 850: '*GUARD*'(body_to_lpda(_U, (_K = +), _Y, _Z, dyalog))
c_code local build_ref_850
	ret_reg &ref[850]
	call_c   build_ref_48()
	call_c   build_ref_849()
	call_c   Dyam_Create_Unary(&ref[48],&ref[849])
	move_ret ref[850]
	c_ret

;; TERM 849: body_to_lpda(_U, (_K = +), _Y, _Z, dyalog)
c_code local build_ref_849
	ret_reg &ref[849]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1325()
	call_c   build_ref_1369()
	call_c   Dyam_Create_Binary(&ref[1325],V(10),&ref[1369])
	move_ret R(0)
	call_c   build_ref_1319()
	call_c   build_ref_1289()
	call_c   Dyam_Term_Start(&ref[1319],5)
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(24))
	call_c   Dyam_Term_Arg(V(25))
	call_c   Dyam_Term_Arg(&ref[1289])
	call_c   Dyam_Term_End()
	move_ret ref[849]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 857: '*SA-ADJ-WRAPPER*'(tag_adj((_B / _C)), [_R,_S,_T|_M], [_W|_E1], [_X|'*SA-ADJ-LAST*'(_O, _Q, '*SA-PROLOG-LAST*', _J)])
c_code local build_ref_857
	ret_reg &ref[857]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   Dyam_Create_List(V(22),V(30))
	move_ret R(0)
	call_c   build_ref_1367()
	call_c   build_ref_847()
	call_c   Dyam_Term_Start(&ref[1367],4)
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(&ref[847])
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret R(1)
	call_c   Dyam_Create_List(V(23),R(1))
	move_ret R(1)
	call_c   build_ref_1366()
	call_c   build_ref_734()
	call_c   build_ref_843()
	call_c   Dyam_Term_Start(&ref[1366],4)
	call_c   Dyam_Term_Arg(&ref[734])
	call_c   Dyam_Term_Arg(&ref[843])
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_End()
	move_ret ref[857]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun313[3]=[2,build_ref_857,build_seed_69]

pl_code local fun314
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(30),V(25))
	fail_ret
fun313:
	call_c   DYAM_evpred_assert_1(&ref[857])
	call_c   Dyam_Deallocate()
	pl_jump  fun13(&seed[69],2)


;; TERM 853: tagfilter_cat((_L ^ _A1 ^ wrap ^ _D ^ _E ^ _H ^ _C1))
c_code local build_ref_853
	ret_reg &ref[853]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1307()
	call_c   Dyam_Create_Binary(&ref[1307],V(7),V(28))
	move_ret R(0)
	call_c   Dyam_Create_Binary(&ref[1307],V(4),R(0))
	move_ret R(0)
	call_c   Dyam_Create_Binary(&ref[1307],V(3),R(0))
	move_ret R(0)
	call_c   build_ref_294()
	call_c   Dyam_Create_Binary(&ref[1307],&ref[294],R(0))
	move_ret R(0)
	call_c   Dyam_Create_Binary(&ref[1307],V(26),R(0))
	move_ret R(0)
	call_c   Dyam_Create_Binary(&ref[1307],V(11),R(0))
	move_ret R(0)
	call_c   build_ref_1349()
	call_c   Dyam_Create_Unary(&ref[1349],R(0))
	move_ret ref[853]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_173
	ret_reg &seed[173]
	call_c   build_ref_48()
	call_c   build_ref_855()
	call_c   Dyam_Seed_Start(&ref[48],&ref[855],I(0),fun3,1)
	call_c   build_ref_856()
	call_c   Dyam_Seed_Add_Comp(&ref[856],&ref[855],0)
	call_c   Dyam_Seed_End()
	move_ret seed[173]
	c_ret

;; TERM 856: '*GUARD*'(body_to_lpda(_U, _C1, _Z, _E1, dyalog)) :> '$$HOLE$$'
c_code local build_ref_856
	ret_reg &ref[856]
	call_c   build_ref_855()
	call_c   Dyam_Create_Binary(I(9),&ref[855],I(7))
	move_ret ref[856]
	c_ret

;; TERM 855: '*GUARD*'(body_to_lpda(_U, _C1, _Z, _E1, dyalog))
c_code local build_ref_855
	ret_reg &ref[855]
	call_c   build_ref_48()
	call_c   build_ref_854()
	call_c   Dyam_Create_Unary(&ref[48],&ref[854])
	move_ret ref[855]
	c_ret

;; TERM 854: body_to_lpda(_U, _C1, _Z, _E1, dyalog)
c_code local build_ref_854
	ret_reg &ref[854]
	call_c   build_ref_1319()
	call_c   build_ref_1289()
	call_c   Dyam_Term_Start(&ref[1319],5)
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_Arg(V(28))
	call_c   Dyam_Term_Arg(V(25))
	call_c   Dyam_Term_Arg(V(30))
	call_c   Dyam_Term_Arg(&ref[1289])
	call_c   Dyam_Term_End()
	move_ret ref[854]
	c_ret

long local pool_fun315[5]=[131074,build_ref_853,build_seed_173,pool_fun313,pool_fun313]

pl_code local fun316
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(26),V(1))
	fail_ret
fun315:
	call_c   Dyam_Choice(fun314)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[853])
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(28))
	call_c   Dyam_Reg_Load(2,V(20))
	move     V(29), R(4)
	move     S(5), R(5)
	pl_call  pred_tag_numbervars_3()
	pl_call  fun28(&seed[173],1)
	pl_jump  fun313()


;; TERM 859: tag_features(_A1, _H, _B1)
c_code local build_ref_859
	ret_reg &ref[859]
	call_c   build_ref_1277()
	call_c   Dyam_Term_Start(&ref[1277],3)
	call_c   Dyam_Term_Arg(V(26))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(27))
	call_c   Dyam_Term_End()
	move_ret ref[859]
	c_ret

long local pool_fun317[4]=[131073,build_ref_859,pool_fun315,pool_fun315]

pl_code local fun317
	call_c   Dyam_Update_Choice(fun316)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[859])
	call_c   Dyam_Cut()
	pl_jump  fun315()

;; TERM 858: tag_features(_A1, _H, _I)
c_code local build_ref_858
	ret_reg &ref[858]
	call_c   build_ref_1277()
	call_c   Dyam_Term_Start(&ref[1277],3)
	call_c   Dyam_Term_Arg(V(26))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_End()
	move_ret ref[858]
	c_ret

long local pool_fun318[4]=[131073,build_ref_858,pool_fun317,pool_fun315]

pl_code local fun318
	call_c   Dyam_Update_Choice(fun317)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[858])
	call_c   Dyam_Cut()
	pl_jump  fun315()

;; TERM 852: tag_features_mode(_A1, wrap, _H, _I)
c_code local build_ref_852
	ret_reg &ref[852]
	call_c   build_ref_1381()
	call_c   build_ref_294()
	call_c   Dyam_Term_Start(&ref[1381],4)
	call_c   Dyam_Term_Arg(V(26))
	call_c   Dyam_Term_Arg(&ref[294])
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_End()
	move_ret ref[852]
	c_ret

;; TERM 1381: tag_features_mode
c_code local build_ref_1381
	ret_reg &ref[1381]
	call_c   Dyam_Create_Atom("tag_features_mode")
	move_ret ref[1381]
	c_ret

long local pool_fun319[11]=[131080,build_seed_169,build_seed_170,build_ref_843,build_seed_171,build_ref_847,build_ref_848,build_seed_172,build_ref_852,pool_fun318,pool_fun315]

pl_code local fun319
	call_c   Dyam_Pool(pool_fun319)
	call_c   Dyam_Allocate(0)
	pl_call  fun28(&seed[169],1)
	pl_call  fun28(&seed[170],1)
	move     &ref[843], R(0)
	move     S(5), R(1)
	move     V(20), R(2)
	move     S(5), R(3)
	move     V(21), R(4)
	move     S(5), R(5)
	pl_call  pred_tag_numbervars_3()
	pl_call  fun28(&seed[171],1)
	call_c   Dyam_Unify(V(23),&ref[847])
	fail_ret
	call_c   Dyam_Unify(V(24),&ref[848])
	fail_ret
	pl_call  fun28(&seed[172],1)
	call_c   Dyam_Choice(fun318)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[852])
	call_c   Dyam_Cut()
	pl_jump  fun315()

;; TERM 861: '$TUPPLE'(35075412200928)
c_code local build_ref_861
	ret_reg &ref[861]
	call_c   Dyam_Create_Simple_Tupple(0,536805376)
	move_ret ref[861]
	c_ret

;; TERM 863: [_D,_E,_F,_G,_H,_I,_J,_K,_L]
c_code local build_ref_863
	ret_reg &ref[863]
	call_c   Dyam_Create_Tupple(3,11,I(0))
	move_ret ref[863]
	c_ret

long local pool_fun320[5]=[4,build_ref_836,build_ref_862,build_ref_734,build_ref_863]

pl_code local fun320
	call_c   Dyam_Pool(pool_fun320)
	call_c   Dyam_Unify_Item(&ref[836])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[862], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	move     &ref[734], R(4)
	move     S(5), R(5)
	move     &ref[863], R(6)
	move     S(5), R(7)
	move     V(12), R(8)
	move     S(5), R(9)
	call_c   Dyam_Reg_Deallocate(5)
	pl_jump  fun130()

;; TERM 835: '*FIRST*'(wrapping_predicate(tag_adj((_B / _C)))) :> []
c_code local build_ref_835
	ret_reg &ref[835]
	call_c   build_ref_834()
	call_c   Dyam_Create_Binary(I(9),&ref[834],I(0))
	move_ret ref[835]
	c_ret

;; TERM 834: '*FIRST*'(wrapping_predicate(tag_adj((_B / _C))))
c_code local build_ref_834
	ret_reg &ref[834]
	call_c   build_ref_52()
	call_c   build_ref_833()
	call_c   Dyam_Create_Unary(&ref[52],&ref[833])
	move_ret ref[834]
	c_ret

c_code local build_seed_37
	ret_reg &seed[37]
	call_c   build_ref_52()
	call_c   build_ref_866()
	call_c   Dyam_Seed_Start(&ref[52],&ref[866],I(0),fun15,1)
	call_c   build_ref_867()
	call_c   Dyam_Seed_Add_Comp(&ref[867],fun326,0)
	call_c   Dyam_Seed_End()
	move_ret seed[37]
	c_ret

;; TERM 867: '*CITEM*'('call_decompose_args/3'(tag_adj((_B / _C))), _A)
c_code local build_ref_867
	ret_reg &ref[867]
	call_c   build_ref_56()
	call_c   build_ref_864()
	call_c   Dyam_Create_Binary(&ref[56],&ref[864],V(0))
	move_ret ref[867]
	c_ret

;; TERM 864: 'call_decompose_args/3'(tag_adj((_B / _C)))
c_code local build_ref_864
	ret_reg &ref[864]
	call_c   build_ref_1232()
	call_c   build_ref_734()
	call_c   Dyam_Create_Unary(&ref[1232],&ref[734])
	move_ret ref[864]
	c_ret

c_code local build_seed_176
	ret_reg &seed[176]
	call_c   build_ref_48()
	call_c   build_ref_880()
	call_c   Dyam_Seed_Start(&ref[48],&ref[880],I(0),fun3,1)
	call_c   build_ref_881()
	call_c   Dyam_Seed_Add_Comp(&ref[881],&ref[880],0)
	call_c   Dyam_Seed_End()
	move_ret seed[176]
	c_ret

;; TERM 881: '*GUARD*'(make_tag_top_callret(_H, _D, _E, _S, _T)) :> '$$HOLE$$'
c_code local build_ref_881
	ret_reg &ref[881]
	call_c   build_ref_880()
	call_c   Dyam_Create_Binary(I(9),&ref[880],I(7))
	move_ret ref[881]
	c_ret

;; TERM 880: '*GUARD*'(make_tag_top_callret(_H, _D, _E, _S, _T))
c_code local build_ref_880
	ret_reg &ref[880]
	call_c   build_ref_48()
	call_c   build_ref_879()
	call_c   Dyam_Create_Unary(&ref[48],&ref[879])
	move_ret ref[880]
	c_ret

;; TERM 879: make_tag_top_callret(_H, _D, _E, _S, _T)
c_code local build_ref_879
	ret_reg &ref[879]
	call_c   build_ref_1340()
	call_c   Dyam_Term_Start(&ref[1340],5)
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_End()
	move_ret ref[879]
	c_ret

c_code local build_seed_177
	ret_reg &seed[177]
	call_c   build_ref_48()
	call_c   build_ref_883()
	call_c   Dyam_Seed_Start(&ref[48],&ref[883],I(0),fun3,1)
	call_c   build_ref_884()
	call_c   Dyam_Seed_Add_Comp(&ref[884],&ref[883],0)
	call_c   Dyam_Seed_End()
	move_ret seed[177]
	c_ret

;; TERM 884: '*GUARD*'(make_tag_bot_callret(_I, _F, _G, _U, _V)) :> '$$HOLE$$'
c_code local build_ref_884
	ret_reg &ref[884]
	call_c   build_ref_883()
	call_c   Dyam_Create_Binary(I(9),&ref[883],I(7))
	move_ret ref[884]
	c_ret

;; TERM 883: '*GUARD*'(make_tag_bot_callret(_I, _F, _G, _U, _V))
c_code local build_ref_883
	ret_reg &ref[883]
	call_c   build_ref_48()
	call_c   build_ref_882()
	call_c   Dyam_Create_Unary(&ref[48],&ref[882])
	move_ret ref[883]
	c_ret

;; TERM 882: make_tag_bot_callret(_I, _F, _G, _U, _V)
c_code local build_ref_882
	ret_reg &ref[882]
	call_c   build_ref_1315()
	call_c   Dyam_Term_Start(&ref[1315],5)
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_Arg(V(21))
	call_c   Dyam_Term_End()
	move_ret ref[882]
	c_ret

;; TERM 885: [_W|_X]
c_code local build_ref_885
	ret_reg &ref[885]
	call_c   Dyam_Create_List(V(22),V(23))
	move_ret ref[885]
	c_ret

;; TERM 886: [_Y|_Z]
c_code local build_ref_886
	ret_reg &ref[886]
	call_c   Dyam_Create_List(V(24),V(25))
	move_ret ref[886]
	c_ret

c_code local build_seed_178
	ret_reg &seed[178]
	call_c   build_ref_48()
	call_c   build_ref_888()
	call_c   Dyam_Seed_Start(&ref[48],&ref[888],I(0),fun3,1)
	call_c   build_ref_889()
	call_c   Dyam_Seed_Add_Comp(&ref[889],&ref[888],0)
	call_c   Dyam_Seed_End()
	move_ret seed[178]
	c_ret

;; TERM 889: '*GUARD*'(append(_X, _Z, _A1)) :> '$$HOLE$$'
c_code local build_ref_889
	ret_reg &ref[889]
	call_c   build_ref_888()
	call_c   Dyam_Create_Binary(I(9),&ref[888],I(7))
	move_ret ref[889]
	c_ret

;; TERM 888: '*GUARD*'(append(_X, _Z, _A1))
c_code local build_ref_888
	ret_reg &ref[888]
	call_c   build_ref_48()
	call_c   build_ref_887()
	call_c   Dyam_Create_Unary(&ref[48],&ref[887])
	move_ret ref[888]
	c_ret

;; TERM 887: append(_X, _Z, _A1)
c_code local build_ref_887
	ret_reg &ref[887]
	call_c   build_ref_1345()
	call_c   Dyam_Term_Start(&ref[1345],3)
	call_c   Dyam_Term_Arg(V(23))
	call_c   Dyam_Term_Arg(V(25))
	call_c   Dyam_Term_Arg(V(26))
	call_c   Dyam_Term_End()
	move_ret ref[887]
	c_ret

;; TERM 890: [_B1|_C1]
c_code local build_ref_890
	ret_reg &ref[890]
	call_c   Dyam_Create_List(V(27),V(28))
	move_ret ref[890]
	c_ret

;; TERM 891: [_D1|_E1]
c_code local build_ref_891
	ret_reg &ref[891]
	call_c   Dyam_Create_List(V(29),V(30))
	move_ret ref[891]
	c_ret

c_code local build_seed_179
	ret_reg &seed[179]
	call_c   build_ref_48()
	call_c   build_ref_893()
	call_c   Dyam_Seed_Start(&ref[48],&ref[893],I(0),fun3,1)
	call_c   build_ref_894()
	call_c   Dyam_Seed_Add_Comp(&ref[894],&ref[893],0)
	call_c   Dyam_Seed_End()
	move_ret seed[179]
	c_ret

;; TERM 894: '*GUARD*'(append([_J,_K,_L|_C1], _E1, _F1)) :> '$$HOLE$$'
c_code local build_ref_894
	ret_reg &ref[894]
	call_c   build_ref_893()
	call_c   Dyam_Create_Binary(I(9),&ref[893],I(7))
	move_ret ref[894]
	c_ret

;; TERM 893: '*GUARD*'(append([_J,_K,_L|_C1], _E1, _F1))
c_code local build_ref_893
	ret_reg &ref[893]
	call_c   build_ref_48()
	call_c   build_ref_892()
	call_c   Dyam_Create_Unary(&ref[48],&ref[892])
	move_ret ref[893]
	c_ret

;; TERM 892: append([_J,_K,_L|_C1], _E1, _F1)
c_code local build_ref_892
	ret_reg &ref[892]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Tupple(9,11,V(28))
	move_ret R(0)
	call_c   build_ref_1345()
	call_c   Dyam_Term_Start(&ref[1345],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(30))
	call_c   Dyam_Term_Arg(V(31))
	call_c   Dyam_Term_End()
	move_ret ref[892]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_180
	ret_reg &seed[180]
	call_c   build_ref_48()
	call_c   build_ref_896()
	call_c   Dyam_Seed_Start(&ref[48],&ref[896],I(0),fun3,1)
	call_c   build_ref_897()
	call_c   Dyam_Seed_Add_Comp(&ref[897],&ref[896],0)
	call_c   Dyam_Seed_End()
	move_ret seed[180]
	c_ret

;; TERM 897: '*GUARD*'(append(_A1, _F1, _M)) :> '$$HOLE$$'
c_code local build_ref_897
	ret_reg &ref[897]
	call_c   build_ref_896()
	call_c   Dyam_Create_Binary(I(9),&ref[896],I(7))
	move_ret ref[897]
	c_ret

;; TERM 896: '*GUARD*'(append(_A1, _F1, _M))
c_code local build_ref_896
	ret_reg &ref[896]
	call_c   build_ref_48()
	call_c   build_ref_895()
	call_c   Dyam_Create_Unary(&ref[48],&ref[895])
	move_ret ref[896]
	c_ret

;; TERM 895: append(_A1, _F1, _M)
c_code local build_ref_895
	ret_reg &ref[895]
	call_c   build_ref_1345()
	call_c   Dyam_Term_Start(&ref[1345],3)
	call_c   Dyam_Term_Arg(V(26))
	call_c   Dyam_Term_Arg(V(31))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_End()
	move_ret ref[895]
	c_ret

c_code local build_seed_175
	ret_reg &seed[175]
	call_c   build_ref_0()
	call_c   build_ref_876()
	call_c   Dyam_Seed_Start(&ref[0],&ref[876],&ref[876],fun3,1)
	call_c   build_ref_877()
	call_c   Dyam_Seed_Add_Comp(&ref[877],&ref[876],0)
	call_c   Dyam_Seed_End()
	move_ret seed[175]
	c_ret

;; TERM 877: '*RITEM*'(_A, return([_D,_E,_F,_G,_H,_I,_J,_K,_L], _M)) :> '$$HOLE$$'
c_code local build_ref_877
	ret_reg &ref[877]
	call_c   build_ref_876()
	call_c   Dyam_Create_Binary(I(9),&ref[876],I(7))
	move_ret ref[877]
	c_ret

;; TERM 876: '*RITEM*'(_A, return([_D,_E,_F,_G,_H,_I,_J,_K,_L], _M))
c_code local build_ref_876
	ret_reg &ref[876]
	call_c   build_ref_0()
	call_c   build_ref_875()
	call_c   Dyam_Create_Binary(&ref[0],V(0),&ref[875])
	move_ret ref[876]
	c_ret

;; TERM 875: return([_D,_E,_F,_G,_H,_I,_J,_K,_L], _M)
c_code local build_ref_875
	ret_reg &ref[875]
	call_c   build_ref_1233()
	call_c   build_ref_863()
	call_c   Dyam_Create_Binary(&ref[1233],&ref[863],V(12))
	move_ret ref[875]
	c_ret

long local pool_fun321[2]=[1,build_seed_175]

long local pool_fun324[11]=[65545,build_seed_176,build_seed_177,build_ref_885,build_ref_886,build_seed_178,build_ref_890,build_ref_891,build_seed_179,build_seed_180,pool_fun321]

pl_code local fun324
	call_c   Dyam_Remove_Choice()
	pl_call  fun28(&seed[176],1)
	pl_call  fun28(&seed[177],1)
	call_c   DYAM_evpred_univ(V(18),&ref[885])
	fail_ret
	call_c   DYAM_evpred_univ(V(20),&ref[886])
	fail_ret
	pl_call  fun28(&seed[178],1)
	call_c   DYAM_evpred_univ(V(19),&ref[890])
	fail_ret
	call_c   DYAM_evpred_univ(V(21),&ref[891])
	fail_ret
	pl_call  fun28(&seed[179],1)
	pl_call  fun28(&seed[180],1)
fun321:
	call_c   Dyam_Deallocate()
	pl_jump  fun13(&seed[175],2)


long local pool_fun325[4]=[131073,build_ref_863,pool_fun324,pool_fun321]

pl_code local fun325
	call_c   Dyam_Update_Choice(fun324)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_gt(V(2),N(15))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(12),&ref[863])
	fail_ret
	pl_jump  fun321()

;; TERM 878: tag_features(_O, _H, _I)
c_code local build_ref_878
	ret_reg &ref[878]
	call_c   build_ref_1277()
	call_c   Dyam_Term_Start(&ref[1277],3)
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_End()
	move_ret ref[878]
	c_ret

;; TERM 869: [_B|_P]
c_code local build_ref_869
	ret_reg &ref[869]
	call_c   Dyam_Create_List(V(1),V(15))
	move_ret ref[869]
	c_ret

;; TERM 870: [_B|_Q]
c_code local build_ref_870
	ret_reg &ref[870]
	call_c   Dyam_Create_List(V(1),V(16))
	move_ret ref[870]
	c_ret

c_code local build_seed_174
	ret_reg &seed[174]
	call_c   build_ref_48()
	call_c   build_ref_872()
	call_c   Dyam_Seed_Start(&ref[48],&ref[872],I(0),fun3,1)
	call_c   build_ref_873()
	call_c   Dyam_Seed_Add_Comp(&ref[873],&ref[872],0)
	call_c   Dyam_Seed_End()
	move_ret seed[174]
	c_ret

;; TERM 873: '*GUARD*'(optimize_topbot_args(_P, _Q, [_J,_K,_L], _R)) :> '$$HOLE$$'
c_code local build_ref_873
	ret_reg &ref[873]
	call_c   build_ref_872()
	call_c   Dyam_Create_Binary(I(9),&ref[872],I(7))
	move_ret ref[873]
	c_ret

;; TERM 872: '*GUARD*'(optimize_topbot_args(_P, _Q, [_J,_K,_L], _R))
c_code local build_ref_872
	ret_reg &ref[872]
	call_c   build_ref_48()
	call_c   build_ref_871()
	call_c   Dyam_Create_Unary(&ref[48],&ref[871])
	move_ret ref[872]
	c_ret

;; TERM 871: optimize_topbot_args(_P, _Q, [_J,_K,_L], _R)
c_code local build_ref_871
	ret_reg &ref[871]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Tupple(9,11,I(0))
	move_ret R(0)
	call_c   build_ref_1230()
	call_c   Dyam_Term_Start(&ref[1230],4)
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_End()
	move_ret ref[871]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 874: [_D,_E,_F,_G|_R]
c_code local build_ref_874
	ret_reg &ref[874]
	call_c   Dyam_Create_Tupple(3,6,V(17))
	move_ret ref[874]
	c_ret

long local pool_fun322[6]=[65540,build_ref_869,build_ref_870,build_seed_174,build_ref_874,pool_fun321]

long local pool_fun323[3]=[65537,build_ref_878,pool_fun322]

pl_code local fun323
	call_c   Dyam_Remove_Choice()
	pl_call  Object_1(&ref[878])
fun322:
	call_c   Dyam_Cut()
	call_c   DYAM_evpred_univ(V(7),&ref[869])
	fail_ret
	call_c   DYAM_evpred_univ(V(8),&ref[870])
	fail_ret
	pl_call  fun28(&seed[174],1)
	call_c   Dyam_Unify(V(12),&ref[874])
	fail_ret
	pl_jump  fun321()


;; TERM 868: tag_features_mode(_N, wrap, _H, _I)
c_code local build_ref_868
	ret_reg &ref[868]
	call_c   build_ref_1381()
	call_c   build_ref_294()
	call_c   Dyam_Term_Start(&ref[1381],4)
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(&ref[294])
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_End()
	move_ret ref[868]
	c_ret

long local pool_fun326[6]=[196610,build_ref_867,build_ref_868,pool_fun325,pool_fun323,pool_fun322]

pl_code local fun326
	call_c   Dyam_Pool(pool_fun326)
	call_c   Dyam_Unify_Item(&ref[867])
	fail_ret
	call_c   DYAM_evpred_functor(V(7),V(1),V(2))
	fail_ret
	call_c   DYAM_evpred_functor(V(8),V(1),V(2))
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun325)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Choice(fun323)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[868])
	call_c   Dyam_Cut()
	pl_jump  fun322()

;; TERM 866: '*FIRST*'('call_decompose_args/3'(tag_adj((_B / _C)))) :> []
c_code local build_ref_866
	ret_reg &ref[866]
	call_c   build_ref_865()
	call_c   Dyam_Create_Binary(I(9),&ref[865],I(0))
	move_ret ref[866]
	c_ret

;; TERM 865: '*FIRST*'('call_decompose_args/3'(tag_adj((_B / _C))))
c_code local build_ref_865
	ret_reg &ref[865]
	call_c   build_ref_52()
	call_c   build_ref_864()
	call_c   Dyam_Create_Unary(&ref[52],&ref[864])
	move_ret ref[865]
	c_ret

c_code local build_seed_16
	ret_reg &seed[16]
	call_c   build_ref_47()
	call_c   build_ref_906()
	call_c   Dyam_Seed_Start(&ref[47],&ref[906],I(0),fun1,1)
	call_c   build_ref_905()
	call_c   Dyam_Seed_Add_Comp(&ref[905],fun334,0)
	call_c   Dyam_Seed_End()
	move_ret seed[16]
	c_ret

;; TERM 905: '*GUARD*'(tag_il_body_to_lpda_handler(_B, (_C ## _D), _E, _F, _G, _H, _I, _J, _K, _L, _M, _N, _O))
c_code local build_ref_905
	ret_reg &ref[905]
	call_c   build_ref_48()
	call_c   build_ref_904()
	call_c   Dyam_Create_Unary(&ref[48],&ref[904])
	move_ret ref[905]
	c_ret

;; TERM 904: tag_il_body_to_lpda_handler(_B, (_C ## _D), _E, _F, _G, _H, _I, _J, _K, _L, _M, _N, _O)
c_code local build_ref_904
	ret_reg &ref[904]
	call_c   build_ref_1295()
	call_c   build_ref_947()
	call_c   Dyam_Term_Start(&ref[1295],13)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(&ref[947])
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_End()
	move_ret ref[904]
	c_ret

;; TERM 947: _C ## _D
c_code local build_ref_947
	ret_reg &ref[947]
	call_c   build_ref_1382()
	call_c   Dyam_Create_Binary(&ref[1382],V(2),V(3))
	move_ret ref[947]
	c_ret

;; TERM 1382: ##
c_code local build_ref_1382
	ret_reg &ref[1382]
	call_c   Dyam_Create_Atom("##")
	move_ret ref[1382]
	c_ret

;; TERM 907: [_T,_Q,_R,_U,_V,_W,_X,_Y]
c_code local build_ref_907
	ret_reg &ref[907]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Tupple(20,24,I(0))
	move_ret R(0)
	call_c   Dyam_Create_Tupple(16,17,R(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(19),R(0))
	move_ret ref[907]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 908: _I :> _A1
c_code local build_ref_908
	ret_reg &ref[908]
	call_c   Dyam_Create_Binary(I(9),V(8),V(26))
	move_ret ref[908]
	c_ret

long local pool_fun333[4]=[3,build_ref_907,build_ref_908,build_ref_274]

pl_code local fun333
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Reg_Load(0,V(13))
	move     V(15), R(2)
	move     S(5), R(3)
	pl_call  pred_il_label_2()
	move     &ref[907], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(25), R(4)
	move     S(5), R(5)
	pl_call  pred_tag_numbervars_3()
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(19))
	call_c   Dyam_Reg_Load(4,V(21))
	call_c   Dyam_Reg_Load(6,V(20))
	call_c   Dyam_Reg_Load(8,V(23))
	call_c   Dyam_Reg_Load(10,V(24))
	call_c   Dyam_Reg_Load(12,V(22))
	call_c   Dyam_Reg_Load(14,V(7))
	move     V(26), R(16)
	move     S(5), R(17)
	call_c   Dyam_Reg_Load(18,V(11))
	call_c   Dyam_Reg_Load(20,V(12))
	pl_call  pred_tag_il_loop_to_lpda_11()
	call_c   Dyam_Reg_Load(0,V(1))
	move     &ref[908], R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Load(4,V(19))
	call_c   Dyam_Reg_Load(6,V(21))
	call_c   Dyam_Reg_Load(8,V(20))
	call_c   Dyam_Reg_Load(10,V(23))
	call_c   Dyam_Reg_Load(12,V(24))
	call_c   Dyam_Reg_Load(14,V(22))
	call_c   Dyam_Reg_Load(16,V(6))
	call_c   Dyam_Reg_Load(18,V(4))
	call_c   Dyam_Reg_Load(20,V(16))
	move     V(18), R(22)
	move     S(5), R(23)
	call_c   Dyam_Reg_Load(24,V(10))
	call_c   Dyam_Reg_Load(26,V(11))
	call_c   Dyam_Reg_Load(28,V(12))
	call_c   Dyam_Reg_Load(30,V(15))
	pl_call  pred_tag_il_start_to_lpda_16()
fun332:
	move     V(27), R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(28), R(4)
	move     S(5), R(5)
	pl_call  pred_tag_numbervars_3()
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(3))
	call_c   Dyam_Reg_Load(4,V(27))
	call_c   Dyam_Reg_Load(6,V(5))
	move     V(29), R(8)
	move     S(5), R(9)
	move     V(29), R(10)
	move     S(5), R(11)
	move     &ref[274], R(12)
	move     0, R(13)
	call_c   Dyam_Reg_Load(14,V(9))
	move     V(30), R(16)
	move     S(5), R(17)
	call_c   Dyam_Reg_Load(18,V(11))
	call_c   Dyam_Reg_Load(20,V(12))
	call_c   Dyam_Reg_Load(22,V(15))
	call_c   Dyam_Reg_Load(24,V(14))
	pl_call  pred_tag_il_body_to_lpda_13()
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(2))
	call_c   Dyam_Reg_Load(4,V(16))
	call_c   Dyam_Reg_Load(6,V(27))
	move     V(31), R(8)
	move     S(5), R(9)
	move     V(31), R(10)
	move     S(5), R(11)
	move     &ref[274], R(12)
	move     0, R(13)
	call_c   Dyam_Reg_Load(14,V(30))
	call_c   Dyam_Reg_Load(16,V(18))
	call_c   Dyam_Reg_Load(18,V(11))
	call_c   Dyam_Reg_Load(20,V(12))
	call_c   Dyam_Reg_Load(22,V(15))
	call_c   Dyam_Reg_Load(24,V(14))
	call_c   Dyam_Reg_Deallocate(13)
	pl_jump  pred_tag_il_body_to_lpda_13()


long local pool_fun334[5]=[65539,build_ref_905,build_ref_274,build_ref_274,pool_fun333]

pl_code local fun334
	call_c   Dyam_Pool(pool_fun334)
	call_c   Dyam_Unify_Item(&ref[905])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun333)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(8),&ref[274])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(15),V(13))
	fail_ret
	call_c   Dyam_Unify(V(16),V(4))
	fail_ret
	call_c   Dyam_Unify(V(17),V(5))
	fail_ret
	call_c   Dyam_Unify(V(10),V(18))
	fail_ret
	pl_jump  fun332()

;; TERM 906: '*GUARD*'(tag_il_body_to_lpda_handler(_B, (_C ## _D), _E, _F, _G, _H, _I, _J, _K, _L, _M, _N, _O)) :> []
c_code local build_ref_906
	ret_reg &ref[906]
	call_c   build_ref_905()
	call_c   Dyam_Create_Binary(I(9),&ref[905],I(0))
	move_ret ref[906]
	c_ret

c_code local build_seed_42
	ret_reg &seed[42]
	call_c   build_ref_47()
	call_c   build_ref_911()
	call_c   Dyam_Seed_Start(&ref[47],&ref[911],I(0),fun1,1)
	call_c   build_ref_910()
	call_c   Dyam_Seed_Add_Comp(&ref[910],fun347,0)
	call_c   Dyam_Seed_End()
	move_ret seed[42]
	c_ret

;; TERM 910: '*GUARD*'(tag_compile_subtree_adj(_B, tag_node{id=> _C, label=> _D, children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}, _O, _P, _Q, _R, _S, _T, _U, _V, _W, _X))
c_code local build_ref_910
	ret_reg &ref[910]
	call_c   build_ref_48()
	call_c   build_ref_909()
	call_c   Dyam_Create_Unary(&ref[48],&ref[909])
	move_ret ref[910]
	c_ret

;; TERM 909: tag_compile_subtree_adj(_B, tag_node{id=> _C, label=> _D, children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}, _O, _P, _Q, _R, _S, _T, _U, _V, _W, _X)
c_code local build_ref_909
	ret_reg &ref[909]
	call_c   build_ref_1335()
	call_c   build_ref_167()
	call_c   Dyam_Term_Start(&ref[1335],12)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(&ref[167])
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_Arg(V(21))
	call_c   Dyam_Term_Arg(V(22))
	call_c   Dyam_Term_Arg(V(23))
	call_c   Dyam_Term_End()
	move_ret ref[909]
	c_ret

;; TERM 930: tag_adj((_I1 / _J1))
c_code local build_ref_930
	ret_reg &ref[930]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1294()
	call_c   Dyam_Create_Binary(&ref[1294],V(34),V(35))
	move_ret R(0)
	call_c   build_ref_1368()
	call_c   Dyam_Create_Unary(&ref[1368],R(0))
	move_ret ref[930]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 937: '$CLOSURE'('$fun'(340, 0, 1130366948), '$TUPPLE'(35075411945232))
c_code local build_ref_937
	ret_reg &ref[937]
	call_c   build_ref_936()
	call_c   Dyam_Closure_Aux(fun340,&ref[936])
	move_ret ref[937]
	c_ret

c_code local build_seed_182
	ret_reg &seed[182]
	call_c   build_ref_48()
	call_c   build_ref_916()
	call_c   Dyam_Seed_Start(&ref[48],&ref[916],I(0),fun3,1)
	call_c   build_ref_917()
	call_c   Dyam_Seed_Add_Comp(&ref[917],&ref[916],0)
	call_c   Dyam_Seed_End()
	move_ret seed[182]
	c_ret

;; TERM 917: '*GUARD*'(tag_compile_subtree_noadj(_B, tag_node{id=> _C, label=> _D, children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}, _Y, _A1, '*SA-PROLOG-LAST*'(_Q), _N1, _S, _T, _W)) :> '$$HOLE$$'
c_code local build_ref_917
	ret_reg &ref[917]
	call_c   build_ref_916()
	call_c   Dyam_Create_Binary(I(9),&ref[916],I(7))
	move_ret ref[917]
	c_ret

;; TERM 916: '*GUARD*'(tag_compile_subtree_noadj(_B, tag_node{id=> _C, label=> _D, children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}, _Y, _A1, '*SA-PROLOG-LAST*'(_Q), _N1, _S, _T, _W))
c_code local build_ref_916
	ret_reg &ref[916]
	call_c   build_ref_48()
	call_c   build_ref_915()
	call_c   Dyam_Create_Unary(&ref[48],&ref[915])
	move_ret ref[916]
	c_ret

;; TERM 915: tag_compile_subtree_noadj(_B, tag_node{id=> _C, label=> _D, children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}, _Y, _A1, '*SA-PROLOG-LAST*'(_Q), _N1, _S, _T, _W)
c_code local build_ref_915
	ret_reg &ref[915]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_847()
	call_c   Dyam_Create_Unary(&ref[847],V(16))
	move_ret R(0)
	call_c   build_ref_1383()
	call_c   build_ref_167()
	call_c   Dyam_Term_Start(&ref[1383],9)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(&ref[167])
	call_c   Dyam_Term_Arg(V(24))
	call_c   Dyam_Term_Arg(V(26))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(39))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_Arg(V(22))
	call_c   Dyam_Term_End()
	move_ret ref[915]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1383: tag_compile_subtree_noadj
c_code local build_ref_1383
	ret_reg &ref[1383]
	call_c   Dyam_Create_Atom("tag_compile_subtree_noadj")
	move_ret ref[1383]
	c_ret

;; TERM 934: '$CLOSURE'('$fun'(339, 0, 1130357360), '$TUPPLE'(35075411945584))
c_code local build_ref_934
	ret_reg &ref[934]
	call_c   build_ref_933()
	call_c   Dyam_Closure_Aux(fun339,&ref[933])
	move_ret ref[934]
	c_ret

;; TERM 929: '$CLOSURE'('$fun'(338, 0, 1130350252), '$TUPPLE'(35075411945408))
c_code local build_ref_929
	ret_reg &ref[929]
	call_c   build_ref_928()
	call_c   Dyam_Closure_Aux(fun338,&ref[928])
	move_ret ref[929]
	c_ret

;; TERM 926: '*SA-ADJ-WRAPPER-CALL*'(_M1, _P1, _E1, _N1, _G1)
c_code local build_ref_926
	ret_reg &ref[926]
	call_c   build_ref_1384()
	call_c   Dyam_Term_Start(&ref[1384],5)
	call_c   Dyam_Term_Arg(V(38))
	call_c   Dyam_Term_Arg(V(41))
	call_c   Dyam_Term_Arg(V(30))
	call_c   Dyam_Term_Arg(V(39))
	call_c   Dyam_Term_Arg(V(32))
	call_c   Dyam_Term_End()
	move_ret ref[926]
	c_ret

;; TERM 1384: '*SA-ADJ-WRAPPER-CALL*'
c_code local build_ref_1384
	ret_reg &ref[1384]
	call_c   Dyam_Create_Atom("*SA-ADJ-WRAPPER-CALL*")
	move_ret ref[1384]
	c_ret

pl_code local fun338
	call_c   build_ref_926()
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(21))
	call_c   Dyam_Reg_Load(4,V(40))
	move     &ref[926], R(6)
	move     S(5), R(7)
	call_c   Dyam_Reg_Load(8,V(17))
	call_c   Dyam_Reg_Load(10,V(18))
	call_c   Dyam_Reg_Deallocate(6)
	pl_jump  pred_feature_args_unif_6()

;; TERM 928: '$TUPPLE'(35075411945408)
c_code local build_ref_928
	ret_reg &ref[928]
	call_c   Dyam_Start_Tupple(0,134220928)
	call_c   Dyam_Almost_End_Tupple(29,168755200)
	move_ret ref[928]
	c_ret

;; TERM 931: [_O,_P,_Y,_A1,_U,_O1,_C,_X,_B]
c_code local build_ref_931
	ret_reg &ref[931]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_148()
	call_c   Dyam_Create_List(V(23),&ref[148])
	move_ret R(0)
	call_c   Dyam_Create_List(V(2),R(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(40),R(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(20),R(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(26),R(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(24),R(0))
	move_ret R(0)
	call_c   Dyam_Create_Tupple(14,15,R(0))
	move_ret ref[931]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun339[4]=[3,build_ref_929,build_ref_930,build_ref_931]

pl_code local fun339
	call_c   Dyam_Pool(pool_fun339)
	call_c   Dyam_Allocate(0)
	move     &ref[929], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	move     &ref[930], R(4)
	move     S(5), R(5)
	move     &ref[931], R(6)
	move     S(5), R(7)
	move     V(41), R(8)
	move     S(5), R(9)
	call_c   Dyam_Reg_Deallocate(5)
	pl_jump  fun130()

;; TERM 933: '$TUPPLE'(35075411945584)
c_code local build_ref_933
	ret_reg &ref[933]
	call_c   Dyam_Start_Tupple(0,201354676)
	call_c   Dyam_Almost_End_Tupple(29,181272576)
	move_ret ref[933]
	c_ret

long local pool_fun340[4]=[3,build_seed_182,build_ref_934,build_ref_294]

pl_code local fun340
	call_c   Dyam_Pool(pool_fun340)
	call_c   Dyam_Allocate(0)
	pl_call  fun28(&seed[182],1)
	move     &ref[934], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Load(4,V(3))
	move     &ref[294], R(6)
	move     0, R(7)
	call_c   Dyam_Reg_Load(8,V(20))
	move     V(40), R(10)
	move     S(5), R(11)
	call_c   Dyam_Reg_Deallocate(6)
fun337:
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Reg_Bind(2,V(6))
	call_c   Dyam_Multi_Reg_Bind(4,2,4)
	call_c   Dyam_Choice(fun336)
	pl_call  fun46(&seed[183],1)
	pl_fail


;; TERM 936: '$TUPPLE'(35075411945232)
c_code local build_ref_936
	ret_reg &ref[936]
	call_c   Dyam_Start_Tupple(0,268435444)
	call_c   Dyam_Almost_End_Tupple(29,180879360)
	move_ret ref[936]
	c_ret

long local pool_fun341[2]=[1,build_ref_937]

long local pool_fun346[3]=[65537,build_ref_930,pool_fun341]

pl_code local fun346
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(38),&ref[930])
	fail_ret
fun341:
	move     &ref[937], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Load(4,V(38))
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  fun243()


;; TERM 912: '$VAR'(_K1, ['$SET$',adj(no, yes, strict, atmostone, one, sync),20])
c_code local build_ref_912
	ret_reg &ref[912]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1265()
	call_c   build_ref_397()
	call_c   build_ref_161()
	call_c   build_ref_1385()
	call_c   build_ref_1386()
	call_c   build_ref_1387()
	call_c   build_ref_1388()
	call_c   Dyam_Term_Start(&ref[1265],6)
	call_c   Dyam_Term_Arg(&ref[397])
	call_c   Dyam_Term_Arg(&ref[161])
	call_c   Dyam_Term_Arg(&ref[1385])
	call_c   Dyam_Term_Arg(&ref[1386])
	call_c   Dyam_Term_Arg(&ref[1387])
	call_c   Dyam_Term_Arg(&ref[1388])
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   Dyam_Term_Start(I(8),3)
	call_c   Dyam_Term_Arg(V(36))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(N(20))
	call_c   Dyam_Term_End()
	move_ret ref[912]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1388: sync
c_code local build_ref_1388
	ret_reg &ref[1388]
	call_c   Dyam_Create_Atom("sync")
	move_ret ref[1388]
	c_ret

;; TERM 1387: one
c_code local build_ref_1387
	ret_reg &ref[1387]
	call_c   Dyam_Create_Atom("one")
	move_ret ref[1387]
	c_ret

;; TERM 1386: atmostone
c_code local build_ref_1386
	ret_reg &ref[1386]
	call_c   Dyam_Create_Atom("atmostone")
	move_ret ref[1386]
	c_ret

;; TERM 1385: strict
c_code local build_ref_1385
	ret_reg &ref[1385]
	call_c   Dyam_Create_Atom("strict")
	move_ret ref[1385]
	c_ret

;; TERM 914: tag_adj_strict((_I1 / _J1))
c_code local build_ref_914
	ret_reg &ref[914]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1294()
	call_c   Dyam_Create_Binary(&ref[1294],V(34),V(35))
	move_ret R(0)
	call_c   build_ref_1364()
	call_c   Dyam_Create_Unary(&ref[1364],R(0))
	move_ret ref[914]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun342[3]=[65537,build_ref_914,pool_fun341]

pl_code local fun345
	call_c   Dyam_Remove_Choice()
fun342:
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(38),&ref[914])
	fail_ret
	pl_jump  fun341()


;; TERM 913: adjkind((_I1 / _J1), _L1)
c_code local build_ref_913
	ret_reg &ref[913]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1294()
	call_c   Dyam_Create_Binary(&ref[1294],V(34),V(35))
	move_ret R(0)
	call_c   build_ref_1389()
	call_c   Dyam_Create_Binary(&ref[1389],R(0),V(37))
	move_ret ref[913]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1389: adjkind
c_code local build_ref_1389
	ret_reg &ref[1389]
	call_c   Dyam_Create_Atom("adjkind")
	move_ret ref[1389]
	c_ret

pl_code local fun344
	call_c   Dyam_Remove_Choice()
fun343:
	call_c   Dyam_Cut()
	pl_fail


long local pool_fun347[7]=[131076,build_ref_910,build_ref_912,build_ref_913,build_ref_294,pool_fun346,pool_fun342]

pl_code local fun347
	call_c   Dyam_Pool(pool_fun347)
	call_c   Dyam_Unify_Item(&ref[910])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     V(24), R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(25), R(4)
	move     S(5), R(5)
	pl_call  pred_tag_numbervars_3()
	move     V(26), R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(27), R(4)
	move     S(5), R(5)
	pl_call  pred_tag_numbervars_3()
	call_c   Dyam_Reg_Load(0,V(20))
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(28), R(4)
	move     S(5), R(5)
	pl_call  pred_tag_numbervars_3()
	call_c   Dyam_Reg_Load(0,V(21))
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(29), R(4)
	move     S(5), R(5)
	pl_call  pred_tag_numbervars_3()
	move     V(30), R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(31), R(4)
	move     S(5), R(5)
	pl_call  pred_tag_numbervars_3()
	move     V(32), R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(33), R(4)
	move     S(5), R(5)
	pl_call  pred_tag_numbervars_3()
	call_c   DYAM_evpred_functor(V(20),V(34),V(35))
	fail_ret
	call_c   Dyam_Choice(fun346)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(6),&ref[912])
	fail_ret
	call_c   Dyam_Choice(fun345)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[913])
	call_c   Dyam_Choice(fun344)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(37),&ref[294])
	fail_ret
	call_c   Dyam_Cut()
	pl_fail

;; TERM 911: '*GUARD*'(tag_compile_subtree_adj(_B, tag_node{id=> _C, label=> _D, children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}, _O, _P, _Q, _R, _S, _T, _U, _V, _W, _X)) :> []
c_code local build_ref_911
	ret_reg &ref[911]
	call_c   build_ref_910()
	call_c   Dyam_Create_Binary(I(9),&ref[910],I(0))
	move_ret ref[911]
	c_ret

c_code local build_seed_25
	ret_reg &seed[25]
	call_c   build_ref_47()
	call_c   build_ref_940()
	call_c   Dyam_Seed_Start(&ref[47],&ref[940],I(0),fun1,1)
	call_c   build_ref_939()
	call_c   Dyam_Seed_Add_Comp(&ref[939],fun352,0)
	call_c   Dyam_Seed_End()
	move_ret seed[25]
	c_ret

;; TERM 939: '*GUARD*'(tag_compile_subtree_handler(_B, (_C ## _D), _E, _F, _G, _H, _I, _J, _K))
c_code local build_ref_939
	ret_reg &ref[939]
	call_c   build_ref_48()
	call_c   build_ref_938()
	call_c   Dyam_Create_Unary(&ref[48],&ref[938])
	move_ret ref[939]
	c_ret

;; TERM 938: tag_compile_subtree_handler(_B, (_C ## _D), _E, _F, _G, _H, _I, _J, _K)
c_code local build_ref_938
	ret_reg &ref[938]
	call_c   build_ref_1278()
	call_c   build_ref_947()
	call_c   Dyam_Term_Start(&ref[1278],9)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(&ref[947])
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_End()
	move_ret ref[938]
	c_ret

;; TERM 953: '$CLOSURE'('$fun'(351, 0, 1130428788), '$TUPPLE'(35075411938808))
c_code local build_ref_953
	ret_reg &ref[953]
	call_c   build_ref_952()
	call_c   Dyam_Closure_Aux(fun351,&ref[952])
	move_ret ref[953]
	c_ret

;; TERM 946: [_M,_N,_O,_P,_Q,_R,_S,_T]
c_code local build_ref_946
	ret_reg &ref[946]
	call_c   Dyam_Create_Tupple(12,19,I(0))
	move_ret ref[946]
	c_ret

;; TERM 950: '*NOOP*'(_Z) :> '*IL-START*'(_E1)
c_code local build_ref_950
	ret_reg &ref[950]
	call_c   build_ref_948()
	call_c   build_ref_949()
	call_c   Dyam_Create_Binary(I(9),&ref[948],&ref[949])
	move_ret ref[950]
	c_ret

;; TERM 949: '*IL-START*'(_E1)
c_code local build_ref_949
	ret_reg &ref[949]
	call_c   build_ref_1390()
	call_c   Dyam_Create_Unary(&ref[1390],V(30))
	move_ret ref[949]
	c_ret

;; TERM 1390: '*IL-START*'
c_code local build_ref_1390
	ret_reg &ref[1390]
	call_c   Dyam_Create_Atom("*IL-START*")
	move_ret ref[1390]
	c_ret

;; TERM 948: '*NOOP*'(_Z)
c_code local build_ref_948
	ret_reg &ref[948]
	call_c   build_ref_1321()
	call_c   Dyam_Create_Unary(&ref[1321],V(25))
	move_ret ref[948]
	c_ret

long local pool_fun351[5]=[4,build_ref_946,build_ref_947,build_ref_274,build_ref_950]

pl_code local fun351
	call_c   Dyam_Pool(pool_fun351)
	call_c   Dyam_Allocate(0)
	move     I(0), R(0)
	move     0, R(1)
	move     V(11), R(2)
	move     S(5), R(3)
	pl_call  pred_il_label_2()
	move     &ref[946], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(20), R(4)
	move     S(5), R(5)
	pl_call  pred_tag_numbervars_3()
	move     &ref[947], R(0)
	move     S(5), R(1)
	move     V(21), R(2)
	move     S(5), R(3)
	pl_call  pred_tupple_2()
	call_c   Dyam_Reg_Load(0,V(6))
	move     V(22), R(2)
	move     S(5), R(3)
	pl_call  pred_tupple_2()
	call_c   Dyam_Reg_Load(0,V(22))
	call_c   Dyam_Reg_Load(2,V(21))
	move     V(23), R(4)
	move     S(5), R(5)
	pl_call  pred_inter_3()
	call_c   Dyam_Reg_Load(0,V(10))
	call_c   Dyam_Reg_Load(2,V(23))
	move     V(24), R(4)
	move     S(5), R(5)
	pl_call  pred_union_3()
	move     &ref[947], R(0)
	move     S(5), R(1)
	move     V(25), R(2)
	move     S(5), R(3)
	pl_call  pred_il_tupple_2()
	call_c   Dyam_Reg_Load(0,V(24))
	call_c   Dyam_Reg_Load(2,V(25))
	move     V(26), R(4)
	move     S(5), R(5)
	pl_call  pred_union_3()
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(4))
	call_c   Dyam_Reg_Load(4,V(15))
	call_c   Dyam_Reg_Load(6,V(14))
	call_c   Dyam_Reg_Load(8,V(16))
	call_c   Dyam_Reg_Load(10,V(17))
	call_c   Dyam_Reg_Load(12,V(18))
	call_c   Dyam_Reg_Load(14,V(13))
	move     V(27), R(16)
	move     S(5), R(17)
	call_c   Dyam_Reg_Load(18,V(8))
	call_c   Dyam_Reg_Load(20,V(9))
	pl_call  pred_tag_il_loop_to_lpda_11()
	call_c   Dyam_Reg_Load(0,V(1))
	move     &ref[947], R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Load(4,V(12))
	call_c   Dyam_Reg_Load(6,V(13))
	move     V(28), R(8)
	move     S(5), R(9)
	move     V(28), R(10)
	move     S(5), R(11)
	move     &ref[274], R(12)
	move     0, R(13)
	call_c   Dyam_Reg_Load(14,V(27))
	move     V(29), R(16)
	move     S(5), R(17)
	call_c   Dyam_Reg_Load(18,V(8))
	call_c   Dyam_Reg_Load(20,V(9))
	call_c   Dyam_Reg_Load(22,V(11))
	call_c   Dyam_Reg_Load(24,V(26))
	pl_call  pred_tag_il_body_to_lpda_13()
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(6))
	call_c   Dyam_Reg_Load(4,V(5))
	call_c   Dyam_Reg_Load(6,V(5))
	call_c   Dyam_Reg_Load(8,V(14))
	call_c   Dyam_Reg_Load(10,V(19))
	call_c   Dyam_Reg_Load(12,V(19))
	call_c   Dyam_Reg_Load(14,V(18))
	move     I(0), R(16)
	move     0, R(17)
	move     I(0), R(18)
	move     0, R(19)
	call_c   Dyam_Reg_Load(20,V(12))
	call_c   Dyam_Reg_Load(22,V(29))
	move     V(30), R(24)
	move     S(5), R(25)
	call_c   Dyam_Reg_Load(26,V(8))
	call_c   Dyam_Reg_Load(28,V(9))
	call_c   Dyam_Reg_Load(30,V(11))
	pl_call  pred_tag_il_start_to_lpda_16()
	call_c   Dyam_Unify(V(7),&ref[950])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

;; TERM 952: '$TUPPLE'(35075411938808)
c_code local build_ref_952
	ret_reg &ref[952]
	call_c   Dyam_Create_Simple_Tupple(0,268173312)
	move_ret ref[952]
	c_ret

long local pool_fun352[3]=[2,build_ref_939,build_ref_953]

pl_code local fun352
	call_c   Dyam_Pool(pool_fun352)
	call_c   Dyam_Unify_Item(&ref[939])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[953], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Deallocate(2)
fun350:
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(2)
	call_c   Dyam_Choice(fun349)
	pl_call  fun46(&seed[185],1)
	pl_fail


;; TERM 940: '*GUARD*'(tag_compile_subtree_handler(_B, (_C ## _D), _E, _F, _G, _H, _I, _J, _K)) :> []
c_code local build_ref_940
	ret_reg &ref[940]
	call_c   build_ref_939()
	call_c   Dyam_Create_Binary(I(9),&ref[939],I(0))
	move_ret ref[940]
	c_ret

c_code local build_seed_43
	ret_reg &seed[43]
	call_c   build_ref_47()
	call_c   build_ref_956()
	call_c   Dyam_Seed_Start(&ref[47],&ref[956],I(0),fun1,1)
	call_c   build_ref_955()
	call_c   Dyam_Seed_Add_Comp(&ref[955],fun357,0)
	call_c   Dyam_Seed_End()
	move_ret seed[43]
	c_ret

;; TERM 955: '*GUARD*'(tag_compile_subtree_potential_adj(_B, tag_node{id=> _C, label=> _D, children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}, _O, _P, _Q, _R, _S, _T, _U, _V, _W, _X))
c_code local build_ref_955
	ret_reg &ref[955]
	call_c   build_ref_48()
	call_c   build_ref_954()
	call_c   Dyam_Create_Unary(&ref[48],&ref[954])
	move_ret ref[955]
	c_ret

;; TERM 954: tag_compile_subtree_potential_adj(_B, tag_node{id=> _C, label=> _D, children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}, _O, _P, _Q, _R, _S, _T, _U, _V, _W, _X)
c_code local build_ref_954
	ret_reg &ref[954]
	call_c   build_ref_1391()
	call_c   build_ref_167()
	call_c   Dyam_Term_Start(&ref[1391],12)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(&ref[167])
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_Arg(V(21))
	call_c   Dyam_Term_Arg(V(22))
	call_c   Dyam_Term_Arg(V(23))
	call_c   Dyam_Term_End()
	move_ret ref[954]
	c_ret

;; TERM 1391: tag_compile_subtree_potential_adj
c_code local build_ref_1391
	ret_reg &ref[1391]
	call_c   Dyam_Create_Atom("tag_compile_subtree_potential_adj")
	move_ret ref[1391]
	c_ret

c_code local build_seed_188
	ret_reg &seed[188]
	call_c   build_ref_48()
	call_c   build_ref_959()
	call_c   Dyam_Seed_Start(&ref[48],&ref[959],I(0),fun3,1)
	call_c   build_ref_960()
	call_c   Dyam_Seed_Add_Comp(&ref[960],&ref[959],0)
	call_c   Dyam_Seed_End()
	move_ret seed[188]
	c_ret

;; TERM 960: '*GUARD*'(destructure_unify(_U, _V, _A1, _R)) :> '$$HOLE$$'
c_code local build_ref_960
	ret_reg &ref[960]
	call_c   build_ref_959()
	call_c   Dyam_Create_Binary(I(9),&ref[959],I(7))
	move_ret ref[960]
	c_ret

;; TERM 959: '*GUARD*'(destructure_unify(_U, _V, _A1, _R))
c_code local build_ref_959
	ret_reg &ref[959]
	call_c   build_ref_48()
	call_c   build_ref_958()
	call_c   Dyam_Create_Unary(&ref[48],&ref[958])
	move_ret ref[959]
	c_ret

;; TERM 958: destructure_unify(_U, _V, _A1, _R)
c_code local build_ref_958
	ret_reg &ref[958]
	call_c   build_ref_1392()
	call_c   Dyam_Term_Start(&ref[1392],4)
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_Arg(V(21))
	call_c   Dyam_Term_Arg(V(26))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_End()
	move_ret ref[958]
	c_ret

;; TERM 1392: destructure_unify
c_code local build_ref_1392
	ret_reg &ref[1392]
	call_c   Dyam_Create_Atom("destructure_unify")
	move_ret ref[1392]
	c_ret

c_code local build_seed_189
	ret_reg &seed[189]
	call_c   build_ref_48()
	call_c   build_ref_962()
	call_c   Dyam_Seed_Start(&ref[48],&ref[962],I(0),fun3,1)
	call_c   build_ref_963()
	call_c   Dyam_Seed_Add_Comp(&ref[963],&ref[962],0)
	call_c   Dyam_Seed_End()
	move_ret seed[189]
	c_ret

;; TERM 963: '*GUARD*'(tag_compile_subtree_noadj(_B, tag_node{id=> _C, label=> _D, children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}, _O, _P, _Q, _A1, _S, _T, _W)) :> '$$HOLE$$'
c_code local build_ref_963
	ret_reg &ref[963]
	call_c   build_ref_962()
	call_c   Dyam_Create_Binary(I(9),&ref[962],I(7))
	move_ret ref[963]
	c_ret

;; TERM 962: '*GUARD*'(tag_compile_subtree_noadj(_B, tag_node{id=> _C, label=> _D, children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}, _O, _P, _Q, _A1, _S, _T, _W))
c_code local build_ref_962
	ret_reg &ref[962]
	call_c   build_ref_48()
	call_c   build_ref_961()
	call_c   Dyam_Create_Unary(&ref[48],&ref[961])
	move_ret ref[962]
	c_ret

;; TERM 961: tag_compile_subtree_noadj(_B, tag_node{id=> _C, label=> _D, children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}, _O, _P, _Q, _A1, _S, _T, _W)
c_code local build_ref_961
	ret_reg &ref[961]
	call_c   build_ref_1383()
	call_c   build_ref_167()
	call_c   Dyam_Term_Start(&ref[1383],9)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(&ref[167])
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(26))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_Arg(V(22))
	call_c   Dyam_Term_End()
	move_ret ref[961]
	c_ret

long local pool_fun353[2]=[1,build_seed_189]

long local pool_fun354[3]=[65537,build_seed_188,pool_fun353]

pl_code local fun354
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Reg_Load(0,V(20))
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(28), R(4)
	move     S(5), R(5)
	pl_call  pred_tag_numbervars_3()
	call_c   Dyam_Reg_Load(0,V(21))
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(29), R(4)
	move     S(5), R(5)
	pl_call  pred_tag_numbervars_3()
	pl_call  fun28(&seed[188],1)
fun353:
	pl_call  fun28(&seed[189],1)
	pl_jump  fun110()


long local pool_fun355[3]=[131072,pool_fun354,pool_fun353]

pl_code local fun355
	call_c   Dyam_Update_Choice(fun354)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(20),V(21))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(20))
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(27), R(4)
	move     S(5), R(5)
	pl_call  pred_tag_numbervars_3()
	call_c   Dyam_Unify(V(17),V(26))
	fail_ret
	pl_jump  fun353()

long local pool_fun356[4]=[131073,build_seed_188,pool_fun355,pool_fun353]

pl_code local fun356
	call_c   Dyam_Update_Choice(fun355)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(20),V(8))
	fail_ret
	call_c   DYAM_sfol_identical(V(21),V(9))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(20))
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(24), R(4)
	move     S(5), R(5)
	pl_call  pred_tag_numbervars_3()
	call_c   Dyam_Reg_Load(0,V(21))
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(25), R(4)
	move     S(5), R(5)
	pl_call  pred_tag_numbervars_3()
	pl_call  fun28(&seed[188],1)
	pl_jump  fun353()

c_code local build_seed_187
	ret_reg &seed[187]
	call_c   build_ref_48()
	call_c   build_ref_910()
	call_c   Dyam_Seed_Start(&ref[48],&ref[910],I(0),fun3,1)
	call_c   build_ref_957()
	call_c   Dyam_Seed_Add_Comp(&ref[957],&ref[910],0)
	call_c   Dyam_Seed_End()
	move_ret seed[187]
	c_ret

;; TERM 957: '*GUARD*'(tag_compile_subtree_adj(_B, tag_node{id=> _C, label=> _D, children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}, _O, _P, _Q, _R, _S, _T, _U, _V, _W, _X)) :> '$$HOLE$$'
c_code local build_ref_957
	ret_reg &ref[957]
	call_c   build_ref_910()
	call_c   Dyam_Create_Binary(I(9),&ref[910],I(7))
	move_ret ref[957]
	c_ret

long local pool_fun357[5]=[65539,build_ref_955,build_ref_167,build_seed_187,pool_fun356]

pl_code local fun357
	call_c   Dyam_Pool(pool_fun357)
	call_c   Dyam_Unify_Item(&ref[955])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun356)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Reg_Load(0,V(1))
	move     &ref[167], R(2)
	move     S(5), R(3)
	pl_call  pred_allowed_wrap_adj_2()
	call_c   Dyam_Cut()
	pl_call  fun28(&seed[187],1)
	pl_jump  fun110()

;; TERM 956: '*GUARD*'(tag_compile_subtree_potential_adj(_B, tag_node{id=> _C, label=> _D, children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}, _O, _P, _Q, _R, _S, _T, _U, _V, _W, _X)) :> []
c_code local build_ref_956
	ret_reg &ref[956]
	call_c   build_ref_955()
	call_c   Dyam_Create_Binary(I(9),&ref[955],I(0))
	move_ret ref[956]
	c_ret

c_code local build_seed_49
	ret_reg &seed[49]
	call_c   build_ref_47()
	call_c   build_ref_966()
	call_c   Dyam_Seed_Start(&ref[47],&ref[966],I(0),fun1,1)
	call_c   build_ref_965()
	call_c   Dyam_Seed_Add_Comp(&ref[965],fun363,0)
	call_c   Dyam_Seed_End()
	move_ret seed[49]
	c_ret

;; TERM 965: '*GUARD*'(tag_compile_subtree_handler(_B, guard{goal=> _C, plus=> _D, minus=> _E}, _F, _G, _H, _I, _J, _K, _L))
c_code local build_ref_965
	ret_reg &ref[965]
	call_c   build_ref_48()
	call_c   build_ref_964()
	call_c   Dyam_Create_Unary(&ref[48],&ref[964])
	move_ret ref[965]
	c_ret

;; TERM 964: tag_compile_subtree_handler(_B, guard{goal=> _C, plus=> _D, minus=> _E}, _F, _G, _H, _I, _J, _K, _L)
c_code local build_ref_964
	ret_reg &ref[964]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1308()
	call_c   Dyam_Term_Start(&ref[1308],3)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_1278()
	call_c   Dyam_Term_Start(&ref[1278],9)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_End()
	move_ret ref[964]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_195
	ret_reg &seed[195]
	call_c   build_ref_48()
	call_c   build_ref_984()
	call_c   Dyam_Seed_Start(&ref[48],&ref[984],I(0),fun3,1)
	call_c   build_ref_985()
	call_c   Dyam_Seed_Add_Comp(&ref[985],&ref[984],0)
	call_c   Dyam_Seed_End()
	move_ret seed[195]
	c_ret

;; TERM 985: '*GUARD*'(body_to_lpda(_B, _E, (unify(_F, _G) :> _O), _R, _J)) :> '$$HOLE$$'
c_code local build_ref_985
	ret_reg &ref[985]
	call_c   build_ref_984()
	call_c   Dyam_Create_Binary(I(9),&ref[984],I(7))
	move_ret ref[985]
	c_ret

;; TERM 984: '*GUARD*'(body_to_lpda(_B, _E, (unify(_F, _G) :> _O), _R, _J))
c_code local build_ref_984
	ret_reg &ref[984]
	call_c   build_ref_48()
	call_c   build_ref_983()
	call_c   Dyam_Create_Unary(&ref[48],&ref[983])
	move_ret ref[984]
	c_ret

;; TERM 983: body_to_lpda(_B, _E, (unify(_F, _G) :> _O), _R, _J)
c_code local build_ref_983
	ret_reg &ref[983]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1283()
	call_c   Dyam_Create_Binary(&ref[1283],V(5),V(6))
	move_ret R(0)
	call_c   Dyam_Create_Binary(I(9),R(0),V(14))
	move_ret R(0)
	call_c   build_ref_1319()
	call_c   Dyam_Term_Start(&ref[1319],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret ref[983]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 987: '*NOOP*'(_S) :> _R
c_code local build_ref_987
	ret_reg &ref[987]
	call_c   build_ref_986()
	call_c   Dyam_Create_Binary(I(9),&ref[986],V(17))
	move_ret ref[987]
	c_ret

;; TERM 986: '*NOOP*'(_S)
c_code local build_ref_986
	ret_reg &ref[986]
	call_c   build_ref_1321()
	call_c   Dyam_Create_Unary(&ref[1321],V(18))
	move_ret ref[986]
	c_ret

c_code local build_seed_196
	ret_reg &seed[196]
	call_c   build_ref_48()
	call_c   build_ref_989()
	call_c   Dyam_Seed_Start(&ref[48],&ref[989],I(0),fun3,1)
	call_c   build_ref_990()
	call_c   Dyam_Seed_Add_Comp(&ref[990],&ref[989],0)
	call_c   Dyam_Seed_End()
	move_ret seed[196]
	c_ret

;; TERM 990: '*GUARD*'(tag_compile_subtree(_B, '$plusguard'(_C, _D), _F, _G, _O, _P, _J, _K, _L)) :> '$$HOLE$$'
c_code local build_ref_990
	ret_reg &ref[990]
	call_c   build_ref_989()
	call_c   Dyam_Create_Binary(I(9),&ref[989],I(7))
	move_ret ref[990]
	c_ret

;; TERM 989: '*GUARD*'(tag_compile_subtree(_B, '$plusguard'(_C, _D), _F, _G, _O, _P, _J, _K, _L))
c_code local build_ref_989
	ret_reg &ref[989]
	call_c   build_ref_48()
	call_c   build_ref_988()
	call_c   Dyam_Create_Unary(&ref[48],&ref[988])
	move_ret ref[989]
	c_ret

;; TERM 988: tag_compile_subtree(_B, '$plusguard'(_C, _D), _F, _G, _O, _P, _J, _K, _L)
c_code local build_ref_988
	ret_reg &ref[988]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1318()
	call_c   Dyam_Create_Binary(&ref[1318],V(2),V(3))
	move_ret R(0)
	call_c   build_ref_1275()
	call_c   Dyam_Term_Start(&ref[1275],9)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_End()
	move_ret ref[988]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun358[4]=[3,build_seed_195,build_ref_987,build_seed_196]

pl_code local fun358
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Reg_Load(0,V(7))
	call_c   Dyam_Reg_Load(2,V(8))
	move     V(14), R(4)
	move     S(5), R(5)
	move     V(15), R(6)
	move     S(5), R(7)
	move     V(16), R(8)
	move     S(5), R(9)
	pl_call  pred_handle_choice_5()
	pl_call  fun28(&seed[195],1)
	call_c   Dyam_Reg_Load(0,V(4))
	move     V(18), R(2)
	move     S(5), R(3)
	pl_call  pred_tupple_2()
	call_c   Dyam_Unify(V(16),&ref[987])
	fail_ret
	pl_call  fun28(&seed[196],1)
	pl_jump  fun110()

c_code local build_seed_194
	ret_reg &seed[194]
	call_c   build_ref_48()
	call_c   build_ref_981()
	call_c   Dyam_Seed_Start(&ref[48],&ref[981],I(0),fun3,1)
	call_c   build_ref_982()
	call_c   Dyam_Seed_Add_Comp(&ref[982],&ref[981],0)
	call_c   Dyam_Seed_End()
	move_ret seed[194]
	c_ret

;; TERM 982: '*GUARD*'(tag_compile_subtree(_B, '$plusguard'(_C, _D), _F, _G, _H, _I, _J, _K, _L)) :> '$$HOLE$$'
c_code local build_ref_982
	ret_reg &ref[982]
	call_c   build_ref_981()
	call_c   Dyam_Create_Binary(I(9),&ref[981],I(7))
	move_ret ref[982]
	c_ret

;; TERM 981: '*GUARD*'(tag_compile_subtree(_B, '$plusguard'(_C, _D), _F, _G, _H, _I, _J, _K, _L))
c_code local build_ref_981
	ret_reg &ref[981]
	call_c   build_ref_48()
	call_c   build_ref_980()
	call_c   Dyam_Create_Unary(&ref[48],&ref[980])
	move_ret ref[981]
	c_ret

;; TERM 980: tag_compile_subtree(_B, '$plusguard'(_C, _D), _F, _G, _H, _I, _J, _K, _L)
c_code local build_ref_980
	ret_reg &ref[980]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1318()
	call_c   Dyam_Create_Binary(&ref[1318],V(2),V(3))
	move_ret R(0)
	call_c   build_ref_1275()
	call_c   Dyam_Term_Start(&ref[1275],9)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_End()
	move_ret ref[980]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun359[4]=[65538,build_ref_330,build_seed_194,pool_fun358]

pl_code local fun359
	call_c   Dyam_Update_Choice(fun358)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(4),&ref[330])
	fail_ret
	call_c   Dyam_Cut()
	pl_call  fun28(&seed[194],1)
	pl_jump  fun110()

;; TERM 976: '$postguard'(_M)
c_code local build_ref_976
	ret_reg &ref[976]
	call_c   build_ref_1393()
	call_c   Dyam_Create_Unary(&ref[1393],V(12))
	move_ret ref[976]
	c_ret

;; TERM 1393: '$postguard'
c_code local build_ref_1393
	ret_reg &ref[1393]
	call_c   Dyam_Create_Atom("$postguard")
	move_ret ref[1393]
	c_ret

c_code local build_seed_193
	ret_reg &seed[193]
	call_c   build_ref_48()
	call_c   build_ref_978()
	call_c   Dyam_Seed_Start(&ref[48],&ref[978],I(0),fun3,1)
	call_c   build_ref_979()
	call_c   Dyam_Seed_Add_Comp(&ref[979],&ref[978],0)
	call_c   Dyam_Seed_End()
	move_ret seed[193]
	c_ret

;; TERM 979: '*GUARD*'(tag_compile_subtree(_B, _C, _F, _G, _N, _I, _J, _K, _L)) :> '$$HOLE$$'
c_code local build_ref_979
	ret_reg &ref[979]
	call_c   build_ref_978()
	call_c   Dyam_Create_Binary(I(9),&ref[978],I(7))
	move_ret ref[979]
	c_ret

;; TERM 978: '*GUARD*'(tag_compile_subtree(_B, _C, _F, _G, _N, _I, _J, _K, _L))
c_code local build_ref_978
	ret_reg &ref[978]
	call_c   build_ref_48()
	call_c   build_ref_977()
	call_c   Dyam_Create_Unary(&ref[48],&ref[977])
	move_ret ref[978]
	c_ret

;; TERM 977: tag_compile_subtree(_B, _C, _F, _G, _N, _I, _J, _K, _L)
c_code local build_ref_977
	ret_reg &ref[977]
	call_c   build_ref_1275()
	call_c   Dyam_Term_Start(&ref[1275],9)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_End()
	move_ret ref[977]
	c_ret

long local pool_fun360[4]=[65538,build_ref_976,build_seed_193,pool_fun359]

pl_code local fun360
	call_c   Dyam_Update_Choice(fun359)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(3),&ref[976])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(12))
	call_c   Dyam_Reg_Load(4,V(7))
	move     V(13), R(6)
	move     S(5), R(7)
	call_c   Dyam_Reg_Load(8,V(9))
	pl_call  pred_guard_post_handler_5()
	pl_call  fun28(&seed[193],1)
	pl_jump  fun110()

c_code local build_seed_192
	ret_reg &seed[192]
	call_c   build_ref_48()
	call_c   build_ref_974()
	call_c   Dyam_Seed_Start(&ref[48],&ref[974],I(0),fun3,1)
	call_c   build_ref_975()
	call_c   Dyam_Seed_Add_Comp(&ref[975],&ref[974],0)
	call_c   Dyam_Seed_End()
	move_ret seed[192]
	c_ret

;; TERM 975: '*GUARD*'(body_to_lpda(_B, _E, (unify(_F, _G) :> _H), _I, _J)) :> '$$HOLE$$'
c_code local build_ref_975
	ret_reg &ref[975]
	call_c   build_ref_974()
	call_c   Dyam_Create_Binary(I(9),&ref[974],I(7))
	move_ret ref[975]
	c_ret

;; TERM 974: '*GUARD*'(body_to_lpda(_B, _E, (unify(_F, _G) :> _H), _I, _J))
c_code local build_ref_974
	ret_reg &ref[974]
	call_c   build_ref_48()
	call_c   build_ref_973()
	call_c   Dyam_Create_Unary(&ref[48],&ref[973])
	move_ret ref[974]
	c_ret

;; TERM 973: body_to_lpda(_B, _E, (unify(_F, _G) :> _H), _I, _J)
c_code local build_ref_973
	ret_reg &ref[973]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1283()
	call_c   Dyam_Create_Binary(&ref[1283],V(5),V(6))
	move_ret R(0)
	call_c   Dyam_Create_Binary(I(9),R(0),V(7))
	move_ret R(0)
	call_c   build_ref_1319()
	call_c   Dyam_Term_Start(&ref[1319],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret ref[973]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun361[4]=[65538,build_ref_330,build_seed_192,pool_fun360]

pl_code local fun361
	call_c   Dyam_Update_Choice(fun360)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(3),&ref[330])
	fail_ret
	call_c   Dyam_Cut()
	pl_call  fun28(&seed[192],1)
	pl_jump  fun110()

c_code local build_seed_191
	ret_reg &seed[191]
	call_c   build_ref_48()
	call_c   build_ref_971()
	call_c   Dyam_Seed_Start(&ref[48],&ref[971],I(0),fun3,1)
	call_c   build_ref_972()
	call_c   Dyam_Seed_Add_Comp(&ref[972],&ref[971],0)
	call_c   Dyam_Seed_End()
	move_ret seed[191]
	c_ret

;; TERM 972: '*GUARD*'(body_to_lpda(_B, fail, _H, _I, _J)) :> '$$HOLE$$'
c_code local build_ref_972
	ret_reg &ref[972]
	call_c   build_ref_971()
	call_c   Dyam_Create_Binary(I(9),&ref[971],I(7))
	move_ret ref[972]
	c_ret

;; TERM 971: '*GUARD*'(body_to_lpda(_B, fail, _H, _I, _J))
c_code local build_ref_971
	ret_reg &ref[971]
	call_c   build_ref_48()
	call_c   build_ref_970()
	call_c   Dyam_Create_Unary(&ref[48],&ref[970])
	move_ret ref[971]
	c_ret

;; TERM 970: body_to_lpda(_B, fail, _H, _I, _J)
c_code local build_ref_970
	ret_reg &ref[970]
	call_c   build_ref_1319()
	call_c   build_ref_330()
	call_c   Dyam_Term_Start(&ref[1319],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(&ref[330])
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret ref[970]
	c_ret

long local pool_fun362[4]=[65538,build_ref_330,build_seed_191,pool_fun361]

pl_code local fun362
	call_c   Dyam_Update_Choice(fun361)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(3),&ref[330])
	fail_ret
	call_c   DYAM_sfol_identical(V(4),&ref[330])
	fail_ret
	call_c   Dyam_Cut()
	pl_call  fun28(&seed[191],1)
	pl_jump  fun110()

c_code local build_seed_190
	ret_reg &seed[190]
	call_c   build_ref_48()
	call_c   build_ref_968()
	call_c   Dyam_Seed_Start(&ref[48],&ref[968],I(0),fun3,1)
	call_c   build_ref_969()
	call_c   Dyam_Seed_Add_Comp(&ref[969],&ref[968],0)
	call_c   Dyam_Seed_End()
	move_ret seed[190]
	c_ret

;; TERM 969: '*GUARD*'(tag_compile_subtree(_B, _C, _F, _G, _H, _I, _J, _K, _L)) :> '$$HOLE$$'
c_code local build_ref_969
	ret_reg &ref[969]
	call_c   build_ref_968()
	call_c   Dyam_Create_Binary(I(9),&ref[968],I(7))
	move_ret ref[969]
	c_ret

;; TERM 968: '*GUARD*'(tag_compile_subtree(_B, _C, _F, _G, _H, _I, _J, _K, _L))
c_code local build_ref_968
	ret_reg &ref[968]
	call_c   build_ref_48()
	call_c   build_ref_967()
	call_c   Dyam_Create_Unary(&ref[48],&ref[967])
	move_ret ref[968]
	c_ret

;; TERM 967: tag_compile_subtree(_B, _C, _F, _G, _H, _I, _J, _K, _L)
c_code local build_ref_967
	ret_reg &ref[967]
	call_c   build_ref_1275()
	call_c   Dyam_Term_Start(&ref[1275],9)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_End()
	move_ret ref[967]
	c_ret

long local pool_fun363[6]=[65540,build_ref_965,build_ref_261,build_ref_330,build_seed_190,pool_fun362]

pl_code local fun363
	call_c   Dyam_Pool(pool_fun363)
	call_c   Dyam_Unify_Item(&ref[965])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun362)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(3),&ref[261])
	fail_ret
	call_c   Dyam_Unify(V(4),&ref[330])
	fail_ret
	call_c   Dyam_Cut()
	pl_call  fun28(&seed[190],1)
	pl_jump  fun110()

;; TERM 966: '*GUARD*'(tag_compile_subtree_handler(_B, guard{goal=> _C, plus=> _D, minus=> _E}, _F, _G, _H, _I, _J, _K, _L)) :> []
c_code local build_ref_966
	ret_reg &ref[966]
	call_c   build_ref_965()
	call_c   Dyam_Create_Binary(I(9),&ref[965],I(0))
	move_ret ref[966]
	c_ret

c_code local build_seed_47
	ret_reg &seed[47]
	call_c   build_ref_47()
	call_c   build_ref_1002()
	call_c   Dyam_Seed_Start(&ref[47],&ref[1002],I(0),fun1,1)
	call_c   build_ref_1001()
	call_c   Dyam_Seed_Add_Comp(&ref[1001],fun389,0)
	call_c   Dyam_Seed_End()
	move_ret seed[47]
	c_ret

;; TERM 1001: '*GUARD*'(tag_compile_subtree_handler(_B, @*{goal=> _C, vars=> _D, from=> _E, to=> _F, collect_first=> _G, collect_last=> _H, collect_loop=> _I, collect_next=> _J, collect_pred=> _K}, _L, _M, _N, _O, _P, _Q, _R))
c_code local build_ref_1001
	ret_reg &ref[1001]
	call_c   build_ref_48()
	call_c   build_ref_1000()
	call_c   Dyam_Create_Unary(&ref[48],&ref[1000])
	move_ret ref[1001]
	c_ret

;; TERM 1000: tag_compile_subtree_handler(_B, @*{goal=> _C, vars=> _D, from=> _E, to=> _F, collect_first=> _G, collect_last=> _H, collect_loop=> _I, collect_next=> _J, collect_pred=> _K}, _L, _M, _N, _O, _P, _Q, _R)
c_code local build_ref_1000
	ret_reg &ref[1000]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1297()
	call_c   Dyam_Term_Start(&ref[1297],9)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_1278()
	call_c   Dyam_Term_Start(&ref[1278],9)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_End()
	move_ret ref[1000]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1003: kleene
c_code local build_ref_1003
	ret_reg &ref[1003]
	call_c   Dyam_Create_Atom("kleene")
	move_ret ref[1003]
	c_ret

;; TERM 1004: '_kleene~w'
c_code local build_ref_1004
	ret_reg &ref[1004]
	call_c   Dyam_Create_Atom("_kleene~w")
	move_ret ref[1004]
	c_ret

;; TERM 1005: [_S]
c_code local build_ref_1005
	ret_reg &ref[1005]
	call_c   Dyam_Create_List(V(18),I(0))
	move_ret ref[1005]
	c_ret

;; TERM 1006: kleene_conditions([_E,_F], [_U,_V], _W, _X, _Y)
c_code local build_ref_1006
	ret_reg &ref[1006]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   Dyam_Create_Tupple(4,5,I(0))
	move_ret R(0)
	call_c   Dyam_Create_Tupple(20,21,I(0))
	move_ret R(1)
	call_c   build_ref_1394()
	call_c   Dyam_Term_Start(&ref[1394],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(22))
	call_c   Dyam_Term_Arg(V(23))
	call_c   Dyam_Term_Arg(V(24))
	call_c   Dyam_Term_End()
	move_ret ref[1006]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1394: kleene_conditions
c_code local build_ref_1394
	ret_reg &ref[1394]
	call_c   Dyam_Create_Atom("kleene_conditions")
	move_ret ref[1394]
	c_ret

;; TERM 1007: [_Z,_A1,_M]
c_code local build_ref_1007
	ret_reg &ref[1007]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(12),I(0))
	move_ret R(0)
	call_c   Dyam_Create_Tupple(25,26,R(0))
	move_ret ref[1007]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1008: [_U,_V,_W,_X,_Y,@*{goal=> _C, vars=> _D, from=> _E, to=> _F, collect_first=> _G, collect_last=> _H, collect_loop=> _I, collect_next=> _J, collect_pred=> _K}]
c_code local build_ref_1008
	ret_reg &ref[1008]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1297()
	call_c   Dyam_Term_Start(&ref[1297],9)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   Dyam_Create_List(R(0),I(0))
	move_ret R(0)
	call_c   Dyam_Create_Tupple(20,24,R(0))
	move_ret ref[1008]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1009: [_U,_Z]
c_code local build_ref_1009
	ret_reg &ref[1009]
	call_c   build_ref_641()
	call_c   Dyam_Create_List(V(20),&ref[641])
	move_ret ref[1009]
	c_ret

;; TERM 1010: [_V,_A1]
c_code local build_ref_1010
	ret_reg &ref[1010]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(26),I(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(21),R(0))
	move_ret ref[1010]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_197
	ret_reg &seed[197]
	call_c   build_ref_48()
	call_c   build_ref_1012()
	call_c   Dyam_Seed_Start(&ref[48],&ref[1012],I(0),fun3,1)
	call_c   build_ref_1013()
	call_c   Dyam_Seed_Add_Comp(&ref[1013],&ref[1012],0)
	call_c   Dyam_Seed_End()
	move_ret seed[197]
	c_ret

;; TERM 1013: '*GUARD*'(append(_D1, _I, _F1)) :> '$$HOLE$$'
c_code local build_ref_1013
	ret_reg &ref[1013]
	call_c   build_ref_1012()
	call_c   Dyam_Create_Binary(I(9),&ref[1012],I(7))
	move_ret ref[1013]
	c_ret

;; TERM 1012: '*GUARD*'(append(_D1, _I, _F1))
c_code local build_ref_1012
	ret_reg &ref[1012]
	call_c   build_ref_48()
	call_c   build_ref_1011()
	call_c   Dyam_Create_Unary(&ref[48],&ref[1011])
	move_ret ref[1012]
	c_ret

;; TERM 1011: append(_D1, _I, _F1)
c_code local build_ref_1011
	ret_reg &ref[1011]
	call_c   build_ref_1345()
	call_c   Dyam_Term_Start(&ref[1345],3)
	call_c   Dyam_Term_Arg(V(29))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(31))
	call_c   Dyam_Term_End()
	move_ret ref[1011]
	c_ret

c_code local build_seed_198
	ret_reg &seed[198]
	call_c   build_ref_48()
	call_c   build_ref_1015()
	call_c   Dyam_Seed_Start(&ref[48],&ref[1015],I(0),fun3,1)
	call_c   build_ref_1016()
	call_c   Dyam_Seed_Add_Comp(&ref[1016],&ref[1015],0)
	call_c   Dyam_Seed_End()
	move_ret seed[198]
	c_ret

;; TERM 1016: '*GUARD*'(append(_E1, _J, _G1)) :> '$$HOLE$$'
c_code local build_ref_1016
	ret_reg &ref[1016]
	call_c   build_ref_1015()
	call_c   Dyam_Create_Binary(I(9),&ref[1015],I(7))
	move_ret ref[1016]
	c_ret

;; TERM 1015: '*GUARD*'(append(_E1, _J, _G1))
c_code local build_ref_1015
	ret_reg &ref[1015]
	call_c   build_ref_48()
	call_c   build_ref_1014()
	call_c   Dyam_Create_Unary(&ref[48],&ref[1014])
	move_ret ref[1015]
	c_ret

;; TERM 1014: append(_E1, _J, _G1)
c_code local build_ref_1014
	ret_reg &ref[1014]
	call_c   build_ref_1345()
	call_c   Dyam_Term_Start(&ref[1345],3)
	call_c   Dyam_Term_Arg(V(30))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(32))
	call_c   Dyam_Term_End()
	move_ret ref[1014]
	c_ret

;; TERM 1017: [_M,_H,[_E,_F]]
c_code local build_ref_1017
	ret_reg &ref[1017]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Tupple(4,5,I(0))
	move_ret R(0)
	call_c   Dyam_Create_List(R(0),I(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(7),R(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(12),R(0))
	move_ret ref[1017]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_199
	ret_reg &seed[199]
	call_c   build_ref_48()
	call_c   build_ref_1019()
	call_c   Dyam_Seed_Start(&ref[48],&ref[1019],I(0),fun3,1)
	call_c   build_ref_1020()
	call_c   Dyam_Seed_Add_Comp(&ref[1020],&ref[1019],0)
	call_c   Dyam_Seed_End()
	move_ret seed[199]
	c_ret

;; TERM 1020: '*GUARD*'(body_to_lpda(_B, (_Z = _M , _X , _H = _I), _N, _K1, _P)) :> '$$HOLE$$'
c_code local build_ref_1020
	ret_reg &ref[1020]
	call_c   build_ref_1019()
	call_c   Dyam_Create_Binary(I(9),&ref[1019],I(7))
	move_ret ref[1020]
	c_ret

;; TERM 1019: '*GUARD*'(body_to_lpda(_B, (_Z = _M , _X , _H = _I), _N, _K1, _P))
c_code local build_ref_1019
	ret_reg &ref[1019]
	call_c   build_ref_48()
	call_c   build_ref_1018()
	call_c   Dyam_Create_Unary(&ref[48],&ref[1018])
	move_ret ref[1019]
	c_ret

;; TERM 1018: body_to_lpda(_B, (_Z = _M , _X , _H = _I), _N, _K1, _P)
c_code local build_ref_1018
	ret_reg &ref[1018]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_1325()
	call_c   Dyam_Create_Binary(&ref[1325],V(25),V(12))
	move_ret R(0)
	call_c   Dyam_Create_Binary(&ref[1325],V(7),V(8))
	move_ret R(1)
	call_c   Dyam_Create_Binary(I(4),V(23),R(1))
	move_ret R(1)
	call_c   Dyam_Create_Binary(I(4),R(0),R(1))
	move_ret R(1)
	call_c   build_ref_1319()
	call_c   Dyam_Term_Start(&ref[1319],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(36))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_End()
	move_ret ref[1018]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_200
	ret_reg &seed[200]
	call_c   build_ref_48()
	call_c   build_ref_1022()
	call_c   Dyam_Seed_Start(&ref[48],&ref[1022],I(0),fun3,1)
	call_c   build_ref_1023()
	call_c   Dyam_Seed_Add_Comp(&ref[1023],&ref[1022],0)
	call_c   Dyam_Seed_End()
	move_ret seed[200]
	c_ret

;; TERM 1023: '*GUARD*'(tag_compile_subtree(_B, _C, _Z, _A1, '*KLEENE-LAST*'(_T, _G1, _F1, _H1, _J1, noop), _L1, _P, _Q, _R)) :> '$$HOLE$$'
c_code local build_ref_1023
	ret_reg &ref[1023]
	call_c   build_ref_1022()
	call_c   Dyam_Create_Binary(I(9),&ref[1022],I(7))
	move_ret ref[1023]
	c_ret

;; TERM 1022: '*GUARD*'(tag_compile_subtree(_B, _C, _Z, _A1, '*KLEENE-LAST*'(_T, _G1, _F1, _H1, _J1, noop), _L1, _P, _Q, _R))
c_code local build_ref_1022
	ret_reg &ref[1022]
	call_c   build_ref_48()
	call_c   build_ref_1021()
	call_c   Dyam_Create_Unary(&ref[48],&ref[1021])
	move_ret ref[1022]
	c_ret

;; TERM 1021: tag_compile_subtree(_B, _C, _Z, _A1, '*KLEENE-LAST*'(_T, _G1, _F1, _H1, _J1, noop), _L1, _P, _Q, _R)
c_code local build_ref_1021
	ret_reg &ref[1021]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1395()
	call_c   build_ref_274()
	call_c   Dyam_Term_Start(&ref[1395],6)
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_Arg(V(32))
	call_c   Dyam_Term_Arg(V(31))
	call_c   Dyam_Term_Arg(V(33))
	call_c   Dyam_Term_Arg(V(35))
	call_c   Dyam_Term_Arg(&ref[274])
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_1275()
	call_c   Dyam_Term_Start(&ref[1275],9)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(25))
	call_c   Dyam_Term_Arg(V(26))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(37))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_End()
	move_ret ref[1021]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1395: '*KLEENE-LAST*'
c_code local build_ref_1395
	ret_reg &ref[1395]
	call_c   Dyam_Create_Atom("*KLEENE-LAST*")
	move_ret ref[1395]
	c_ret

c_code local build_seed_201
	ret_reg &seed[201]
	call_c   build_ref_48()
	call_c   build_ref_1025()
	call_c   Dyam_Seed_Start(&ref[48],&ref[1025],I(0),fun3,1)
	call_c   build_ref_1026()
	call_c   Dyam_Seed_Add_Comp(&ref[1026],&ref[1025],0)
	call_c   Dyam_Seed_End()
	move_ret seed[201]
	c_ret

;; TERM 1026: '*GUARD*'(body_to_lpda(_B, _Y, _L1, _M1, _P)) :> '$$HOLE$$'
c_code local build_ref_1026
	ret_reg &ref[1026]
	call_c   build_ref_1025()
	call_c   Dyam_Create_Binary(I(9),&ref[1025],I(7))
	move_ret ref[1026]
	c_ret

;; TERM 1025: '*GUARD*'(body_to_lpda(_B, _Y, _L1, _M1, _P))
c_code local build_ref_1025
	ret_reg &ref[1025]
	call_c   build_ref_48()
	call_c   build_ref_1024()
	call_c   Dyam_Create_Unary(&ref[48],&ref[1024])
	move_ret ref[1025]
	c_ret

;; TERM 1024: body_to_lpda(_B, _Y, _L1, _M1, _P)
c_code local build_ref_1024
	ret_reg &ref[1024]
	call_c   build_ref_1319()
	call_c   Dyam_Term_Start(&ref[1319],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(24))
	call_c   Dyam_Term_Arg(V(37))
	call_c   Dyam_Term_Arg(V(38))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_End()
	move_ret ref[1024]
	c_ret

;; TERM 1027: '*KLEENE*'(_T, _H1, '*KLEENE-ALTERNATIVE*'(_K1, _M1))
c_code local build_ref_1027
	ret_reg &ref[1027]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1397()
	call_c   Dyam_Create_Binary(&ref[1397],V(36),V(38))
	move_ret R(0)
	call_c   build_ref_1396()
	call_c   Dyam_Term_Start(&ref[1396],3)
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_Arg(V(33))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_End()
	move_ret ref[1027]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1396: '*KLEENE*'
c_code local build_ref_1396
	ret_reg &ref[1396]
	call_c   Dyam_Create_Atom("*KLEENE*")
	move_ret ref[1396]
	c_ret

;; TERM 1397: '*KLEENE-ALTERNATIVE*'
c_code local build_ref_1397
	ret_reg &ref[1397]
	call_c   Dyam_Create_Atom("*KLEENE-ALTERNATIVE*")
	move_ret ref[1397]
	c_ret

c_code local build_seed_202
	ret_reg &seed[202]
	call_c   build_ref_48()
	call_c   build_ref_1029()
	call_c   Dyam_Seed_Start(&ref[48],&ref[1029],I(0),fun3,1)
	call_c   build_ref_1030()
	call_c   Dyam_Seed_Add_Comp(&ref[1030],&ref[1029],0)
	call_c   Dyam_Seed_End()
	move_ret seed[202]
	c_ret

;; TERM 1030: '*GUARD*'(body_to_lpda(_B, (_W , _L = _Z , _G = _I), _N1, _O, _P)) :> '$$HOLE$$'
c_code local build_ref_1030
	ret_reg &ref[1030]
	call_c   build_ref_1029()
	call_c   Dyam_Create_Binary(I(9),&ref[1029],I(7))
	move_ret ref[1030]
	c_ret

;; TERM 1029: '*GUARD*'(body_to_lpda(_B, (_W , _L = _Z , _G = _I), _N1, _O, _P))
c_code local build_ref_1029
	ret_reg &ref[1029]
	call_c   build_ref_48()
	call_c   build_ref_1028()
	call_c   Dyam_Create_Unary(&ref[48],&ref[1028])
	move_ret ref[1029]
	c_ret

;; TERM 1028: body_to_lpda(_B, (_W , _L = _Z , _G = _I), _N1, _O, _P)
c_code local build_ref_1028
	ret_reg &ref[1028]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_1325()
	call_c   Dyam_Create_Binary(&ref[1325],V(11),V(25))
	move_ret R(0)
	call_c   Dyam_Create_Binary(&ref[1325],V(6),V(8))
	move_ret R(1)
	call_c   Dyam_Create_Binary(I(4),R(0),R(1))
	move_ret R(1)
	call_c   Dyam_Create_Binary(I(4),V(22),R(1))
	move_ret R(1)
	call_c   build_ref_1319()
	call_c   Dyam_Term_Start(&ref[1319],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(39))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_End()
	move_ret ref[1028]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun389[18]=[17,build_ref_1001,build_ref_1003,build_ref_1004,build_ref_1005,build_ref_1006,build_ref_1007,build_ref_1008,build_ref_1009,build_ref_1010,build_seed_197,build_seed_198,build_ref_1017,build_seed_199,build_seed_200,build_seed_201,build_ref_1027,build_seed_202]

pl_code local fun389
	call_c   Dyam_Pool(pool_fun389)
	call_c   Dyam_Unify_Item(&ref[1001])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[1003], R(0)
	move     0, R(1)
	move     V(18), R(2)
	move     S(5), R(3)
	pl_call  pred_update_counter_2()
	move     &ref[1004], R(0)
	move     0, R(1)
	move     &ref[1005], R(2)
	move     S(5), R(3)
	move     V(19), R(4)
	move     S(5), R(5)
	pl_call  pred_name_builder_3()
	pl_call  Object_1(&ref[1006])
	move     &ref[1007], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(27), R(4)
	move     S(5), R(5)
	pl_call  pred_tag_numbervars_3()
	move     &ref[1008], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(28), R(4)
	move     S(5), R(5)
	pl_call  pred_tag_numbervars_3()
	call_c   Dyam_Unify(V(29),&ref[1009])
	fail_ret
	call_c   Dyam_Unify(V(30),&ref[1010])
	fail_ret
	pl_call  fun28(&seed[197],1)
	pl_call  fun28(&seed[198],1)
	call_c   Dyam_Reg_Load(0,V(3))
	move     V(33), R(2)
	move     S(5), R(3)
	pl_call  pred_tupple_2()
	call_c   Dyam_Reg_Load(0,V(13))
	move     V(34), R(2)
	move     S(5), R(3)
	pl_call  pred_tupple_2()
	move     &ref[1017], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(34))
	move     V(35), R(4)
	move     S(5), R(5)
	pl_call  pred_extend_tupple_3()
	pl_call  fun28(&seed[199],1)
	pl_call  fun28(&seed[200],1)
	pl_call  fun28(&seed[201],1)
	call_c   Dyam_Unify(V(39),&ref[1027])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_jump  fun28(&seed[202],1)

;; TERM 1002: '*GUARD*'(tag_compile_subtree_handler(_B, @*{goal=> _C, vars=> _D, from=> _E, to=> _F, collect_first=> _G, collect_last=> _H, collect_loop=> _I, collect_next=> _J, collect_pred=> _K}, _L, _M, _N, _O, _P, _Q, _R)) :> []
c_code local build_ref_1002
	ret_reg &ref[1002]
	call_c   build_ref_1001()
	call_c   Dyam_Create_Binary(I(9),&ref[1001],I(0))
	move_ret ref[1002]
	c_ret

c_code local build_seed_14
	ret_reg &seed[14]
	call_c   build_ref_47()
	call_c   build_ref_1042()
	call_c   Dyam_Seed_Start(&ref[47],&ref[1042],I(0),fun1,1)
	call_c   build_ref_1041()
	call_c   Dyam_Seed_Add_Comp(&ref[1041],fun418,0)
	call_c   Dyam_Seed_End()
	move_ret seed[14]
	c_ret

;; TERM 1041: '*GUARD*'(tag_il_body_to_lpda_handler(_B, _C, _D, _E, _F, _G, _H, _I, _J, _K, _L, _M, _N))
c_code local build_ref_1041
	ret_reg &ref[1041]
	call_c   build_ref_48()
	call_c   build_ref_1040()
	call_c   Dyam_Create_Unary(&ref[48],&ref[1040])
	move_ret ref[1041]
	c_ret

;; TERM 1040: tag_il_body_to_lpda_handler(_B, _C, _D, _E, _F, _G, _H, _I, _J, _K, _L, _M, _N)
c_code local build_ref_1040
	ret_reg &ref[1040]
	call_c   build_ref_1295()
	call_c   Dyam_Term_Start(&ref[1295],13)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_End()
	move_ret ref[1040]
	c_ret

;; TERM 1043: @*{goal=> _O, vars=> _P, from=> _Q, to=> _R, collect_first=> _S, collect_last=> _T, collect_loop=> _U, collect_next=> _V, collect_pred=> _W}
c_code local build_ref_1043
	ret_reg &ref[1043]
	call_c   build_ref_1297()
	call_c   Dyam_Term_Start(&ref[1297],9)
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_Arg(V(21))
	call_c   Dyam_Term_Arg(V(22))
	call_c   Dyam_Term_End()
	move_ret ref[1043]
	c_ret

;; TERM 1044: [_X]
c_code local build_ref_1044
	ret_reg &ref[1044]
	call_c   Dyam_Create_List(V(23),I(0))
	move_ret ref[1044]
	c_ret

;; TERM 1045: kleene_conditions([_Q,_R], [_Z,_A1], _B1, _C1, _D1)
c_code local build_ref_1045
	ret_reg &ref[1045]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   Dyam_Create_Tupple(16,17,I(0))
	move_ret R(0)
	call_c   Dyam_Create_Tupple(25,26,I(0))
	move_ret R(1)
	call_c   build_ref_1394()
	call_c   Dyam_Term_Start(&ref[1394],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(27))
	call_c   Dyam_Term_Arg(V(28))
	call_c   Dyam_Term_Arg(V(29))
	call_c   Dyam_Term_End()
	move_ret ref[1045]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1046: [_Z,_A1,_B1,_C1,_D1,@*{goal=> _O, vars=> _P, from=> _Q, to=> _R, collect_first=> _S, collect_last=> _T, collect_loop=> _U, collect_next=> _V, collect_pred=> _W}]
c_code local build_ref_1046
	ret_reg &ref[1046]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1043()
	call_c   Dyam_Create_List(&ref[1043],I(0))
	move_ret R(0)
	call_c   Dyam_Create_Tupple(25,29,R(0))
	move_ret ref[1046]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1047: [_G,_F,_F1,_G1,_H1]
c_code local build_ref_1047
	ret_reg &ref[1047]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Tupple(31,33,I(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(5),R(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(6),R(0))
	move_ret ref[1047]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1048: [_Z,_F1,_M1]
c_code local build_ref_1048
	ret_reg &ref[1048]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(38),I(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(31),R(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(25),R(0))
	move_ret ref[1048]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1049: [_A1,_H1,_J1]
c_code local build_ref_1049
	ret_reg &ref[1049]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(35),I(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(33),R(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(26),R(0))
	move_ret ref[1049]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_203
	ret_reg &seed[203]
	call_c   build_ref_48()
	call_c   build_ref_1051()
	call_c   Dyam_Seed_Start(&ref[48],&ref[1051],I(0),fun3,1)
	call_c   build_ref_1052()
	call_c   Dyam_Seed_Add_Comp(&ref[1052],&ref[1051],0)
	call_c   Dyam_Seed_End()
	move_ret seed[203]
	c_ret

;; TERM 1052: '*GUARD*'(append(_L1, _U, _O1)) :> '$$HOLE$$'
c_code local build_ref_1052
	ret_reg &ref[1052]
	call_c   build_ref_1051()
	call_c   Dyam_Create_Binary(I(9),&ref[1051],I(7))
	move_ret ref[1052]
	c_ret

;; TERM 1051: '*GUARD*'(append(_L1, _U, _O1))
c_code local build_ref_1051
	ret_reg &ref[1051]
	call_c   build_ref_48()
	call_c   build_ref_1050()
	call_c   Dyam_Create_Unary(&ref[48],&ref[1050])
	move_ret ref[1051]
	c_ret

;; TERM 1050: append(_L1, _U, _O1)
c_code local build_ref_1050
	ret_reg &ref[1050]
	call_c   build_ref_1345()
	call_c   Dyam_Term_Start(&ref[1345],3)
	call_c   Dyam_Term_Arg(V(37))
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_Arg(V(40))
	call_c   Dyam_Term_End()
	move_ret ref[1050]
	c_ret

c_code local build_seed_204
	ret_reg &seed[204]
	call_c   build_ref_48()
	call_c   build_ref_1054()
	call_c   Dyam_Seed_Start(&ref[48],&ref[1054],I(0),fun3,1)
	call_c   build_ref_1055()
	call_c   Dyam_Seed_Add_Comp(&ref[1055],&ref[1054],0)
	call_c   Dyam_Seed_End()
	move_ret seed[204]
	c_ret

;; TERM 1055: '*GUARD*'(append(_N1, _V, _P1)) :> '$$HOLE$$'
c_code local build_ref_1055
	ret_reg &ref[1055]
	call_c   build_ref_1054()
	call_c   Dyam_Create_Binary(I(9),&ref[1054],I(7))
	move_ret ref[1055]
	c_ret

;; TERM 1054: '*GUARD*'(append(_N1, _V, _P1))
c_code local build_ref_1054
	ret_reg &ref[1054]
	call_c   build_ref_48()
	call_c   build_ref_1053()
	call_c   Dyam_Create_Unary(&ref[48],&ref[1053])
	move_ret ref[1054]
	c_ret

;; TERM 1053: append(_N1, _V, _P1)
c_code local build_ref_1053
	ret_reg &ref[1053]
	call_c   build_ref_1345()
	call_c   Dyam_Term_Start(&ref[1345],3)
	call_c   Dyam_Term_Arg(V(39))
	call_c   Dyam_Term_Arg(V(21))
	call_c   Dyam_Term_Arg(V(41))
	call_c   Dyam_Term_End()
	move_ret ref[1053]
	c_ret

;; TERM 1056: [_F,_G,_E,_T,[_Q,_R]]
c_code local build_ref_1056
	ret_reg &ref[1056]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Tupple(16,17,I(0))
	move_ret R(0)
	call_c   Dyam_Create_List(R(0),I(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(19),R(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(4),R(0))
	move_ret R(0)
	call_c   Dyam_Create_Tupple(5,6,R(0))
	move_ret ref[1056]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_205
	ret_reg &seed[205]
	call_c   build_ref_48()
	call_c   build_ref_1058()
	call_c   Dyam_Seed_Start(&ref[48],&ref[1058],I(0),fun3,1)
	call_c   build_ref_1059()
	call_c   Dyam_Seed_Add_Comp(&ref[1059],&ref[1058],0)
	call_c   Dyam_Seed_End()
	move_ret seed[205]
	c_ret

;; TERM 1059: '*GUARD*'(body_to_lpda(_B, (_C1 , _T = _U , _F1 = _F , _G1 = _G), _H, _T1, _K)) :> '$$HOLE$$'
c_code local build_ref_1059
	ret_reg &ref[1059]
	call_c   build_ref_1058()
	call_c   Dyam_Create_Binary(I(9),&ref[1058],I(7))
	move_ret ref[1059]
	c_ret

;; TERM 1058: '*GUARD*'(body_to_lpda(_B, (_C1 , _T = _U , _F1 = _F , _G1 = _G), _H, _T1, _K))
c_code local build_ref_1058
	ret_reg &ref[1058]
	call_c   build_ref_48()
	call_c   build_ref_1057()
	call_c   Dyam_Create_Unary(&ref[48],&ref[1057])
	move_ret ref[1058]
	c_ret

;; TERM 1057: body_to_lpda(_B, (_C1 , _T = _U , _F1 = _F , _G1 = _G), _H, _T1, _K)
c_code local build_ref_1057
	ret_reg &ref[1057]
	call_c   Dyam_Pseudo_Choice(3)
	call_c   build_ref_1325()
	call_c   Dyam_Create_Binary(&ref[1325],V(19),V(20))
	move_ret R(0)
	call_c   Dyam_Create_Binary(&ref[1325],V(31),V(5))
	move_ret R(1)
	call_c   Dyam_Create_Binary(&ref[1325],V(32),V(6))
	move_ret R(2)
	call_c   Dyam_Create_Binary(I(4),R(1),R(2))
	move_ret R(2)
	call_c   Dyam_Create_Binary(I(4),R(0),R(2))
	move_ret R(2)
	call_c   Dyam_Create_Binary(I(4),V(28),R(2))
	move_ret R(2)
	call_c   build_ref_1319()
	call_c   Dyam_Term_Start(&ref[1319],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(2))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(45))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_End()
	move_ret ref[1057]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1060: [_F1,_G1,_H1,_E,_D,_G,_F]
c_code local build_ref_1060
	ret_reg &ref[1060]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(5),I(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(6),R(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(3),R(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(4),R(0))
	move_ret R(0)
	call_c   Dyam_Create_Tupple(31,33,R(0))
	move_ret ref[1060]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1061: [_U1 ^ _W1,_J1,_M1] ^ '*KLEENE-LAST*'(_Y, _P1, _O1, _Q1, _S1, _W1)
c_code local build_ref_1061
	ret_reg &ref[1061]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_1307()
	call_c   Dyam_Create_Binary(&ref[1307],V(46),V(48))
	move_ret R(0)
	call_c   Dyam_Create_List(V(38),I(0))
	move_ret R(1)
	call_c   Dyam_Create_List(V(35),R(1))
	move_ret R(1)
	call_c   Dyam_Create_List(R(0),R(1))
	move_ret R(1)
	call_c   build_ref_1395()
	call_c   Dyam_Term_Start(&ref[1395],6)
	call_c   Dyam_Term_Arg(V(24))
	call_c   Dyam_Term_Arg(V(41))
	call_c   Dyam_Term_Arg(V(40))
	call_c   Dyam_Term_Arg(V(42))
	call_c   Dyam_Term_Arg(V(44))
	call_c   Dyam_Term_Arg(V(48))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   Dyam_Create_Binary(&ref[1307],R(1),R(0))
	move_ret ref[1061]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_206
	ret_reg &seed[206]
	call_c   build_ref_48()
	call_c   build_ref_1063()
	call_c   Dyam_Seed_Start(&ref[48],&ref[1063],I(0),fun3,1)
	call_c   build_ref_1064()
	call_c   Dyam_Seed_Add_Comp(&ref[1064],&ref[1063],0)
	call_c   Dyam_Seed_End()
	move_ret seed[206]
	c_ret

;; TERM 1064: '*GUARD*'(body_to_lpda(_B, _D1, _X1, _Y1, _K)) :> '$$HOLE$$'
c_code local build_ref_1064
	ret_reg &ref[1064]
	call_c   build_ref_1063()
	call_c   Dyam_Create_Binary(I(9),&ref[1063],I(7))
	move_ret ref[1064]
	c_ret

;; TERM 1063: '*GUARD*'(body_to_lpda(_B, _D1, _X1, _Y1, _K))
c_code local build_ref_1063
	ret_reg &ref[1063]
	call_c   build_ref_48()
	call_c   build_ref_1062()
	call_c   Dyam_Create_Unary(&ref[48],&ref[1062])
	move_ret ref[1063]
	c_ret

;; TERM 1062: body_to_lpda(_B, _D1, _X1, _Y1, _K)
c_code local build_ref_1062
	ret_reg &ref[1062]
	call_c   build_ref_1319()
	call_c   Dyam_Term_Start(&ref[1319],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(29))
	call_c   Dyam_Term_Arg(V(49))
	call_c   Dyam_Term_Arg(V(50))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_End()
	move_ret ref[1062]
	c_ret

;; TERM 1065: '*KLEENE*'(_Y, _Q1, '*ALTERNATIVE*'(_T1, _Y1))
c_code local build_ref_1065
	ret_reg &ref[1065]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1398()
	call_c   Dyam_Create_Binary(&ref[1398],V(45),V(50))
	move_ret R(0)
	call_c   build_ref_1396()
	call_c   Dyam_Term_Start(&ref[1396],3)
	call_c   Dyam_Term_Arg(V(24))
	call_c   Dyam_Term_Arg(V(42))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_End()
	move_ret ref[1065]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1398: '*ALTERNATIVE*'
c_code local build_ref_1398
	ret_reg &ref[1398]
	call_c   Dyam_Create_Atom("*ALTERNATIVE*")
	move_ret ref[1398]
	c_ret

c_code local build_seed_207
	ret_reg &seed[207]
	call_c   build_ref_48()
	call_c   build_ref_1067()
	call_c   Dyam_Seed_Start(&ref[48],&ref[1067],I(0),fun3,1)
	call_c   build_ref_1068()
	call_c   Dyam_Seed_Add_Comp(&ref[1068],&ref[1067],0)
	call_c   Dyam_Seed_End()
	move_ret seed[207]
	c_ret

;; TERM 1068: '*GUARD*'(body_to_lpda(_B, (_E = _G1), _I, _A2, _K)) :> '$$HOLE$$'
c_code local build_ref_1068
	ret_reg &ref[1068]
	call_c   build_ref_1067()
	call_c   Dyam_Create_Binary(I(9),&ref[1067],I(7))
	move_ret ref[1068]
	c_ret

;; TERM 1067: '*GUARD*'(body_to_lpda(_B, (_E = _G1), _I, _A2, _K))
c_code local build_ref_1067
	ret_reg &ref[1067]
	call_c   build_ref_48()
	call_c   build_ref_1066()
	call_c   Dyam_Create_Unary(&ref[48],&ref[1066])
	move_ret ref[1067]
	c_ret

;; TERM 1066: body_to_lpda(_B, (_E = _G1), _I, _A2, _K)
c_code local build_ref_1066
	ret_reg &ref[1066]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1325()
	call_c   Dyam_Create_Binary(&ref[1325],V(4),V(32))
	move_ret R(0)
	call_c   build_ref_1319()
	call_c   Dyam_Term_Start(&ref[1319],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(52))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_End()
	move_ret ref[1066]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_208
	ret_reg &seed[208]
	call_c   build_ref_48()
	call_c   build_ref_1070()
	call_c   Dyam_Seed_Start(&ref[48],&ref[1070],I(0),fun3,1)
	call_c   build_ref_1071()
	call_c   Dyam_Seed_Add_Comp(&ref[1071],&ref[1070],0)
	call_c   Dyam_Seed_End()
	move_ret seed[208]
	c_ret

;; TERM 1071: '*GUARD*'(body_to_lpda(_B, (_B1 , _S = _U , _D = _F1), (_Z1 :> _A2), _J, _K)) :> '$$HOLE$$'
c_code local build_ref_1071
	ret_reg &ref[1071]
	call_c   build_ref_1070()
	call_c   Dyam_Create_Binary(I(9),&ref[1070],I(7))
	move_ret ref[1071]
	c_ret

;; TERM 1070: '*GUARD*'(body_to_lpda(_B, (_B1 , _S = _U , _D = _F1), (_Z1 :> _A2), _J, _K))
c_code local build_ref_1070
	ret_reg &ref[1070]
	call_c   build_ref_48()
	call_c   build_ref_1069()
	call_c   Dyam_Create_Unary(&ref[48],&ref[1069])
	move_ret ref[1070]
	c_ret

;; TERM 1069: body_to_lpda(_B, (_B1 , _S = _U , _D = _F1), (_Z1 :> _A2), _J, _K)
c_code local build_ref_1069
	ret_reg &ref[1069]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_1325()
	call_c   Dyam_Create_Binary(&ref[1325],V(18),V(20))
	move_ret R(0)
	call_c   Dyam_Create_Binary(&ref[1325],V(3),V(31))
	move_ret R(1)
	call_c   Dyam_Create_Binary(I(4),R(0),R(1))
	move_ret R(1)
	call_c   Dyam_Create_Binary(I(4),V(27),R(1))
	move_ret R(1)
	call_c   Dyam_Create_Binary(I(9),V(51),V(52))
	move_ret R(0)
	call_c   build_ref_1319()
	call_c   Dyam_Term_Start(&ref[1319],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_End()
	move_ret ref[1069]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun418[22]=[21,build_ref_1041,build_ref_1043,build_ref_1003,build_ref_1004,build_ref_1044,build_ref_1045,build_ref_1046,build_ref_1047,build_ref_1048,build_ref_1049,build_seed_203,build_seed_204,build_ref_1056,build_seed_205,build_ref_1060,build_ref_1061,build_ref_274,build_seed_206,build_ref_1065,build_seed_207,build_seed_208]

pl_code local fun418
	call_c   Dyam_Pool(pool_fun418)
	call_c   Dyam_Unify_Item(&ref[1041])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(2))
	move     &ref[1043], R(2)
	move     S(5), R(3)
	pl_call  pred_kleene_normalize_2()
	move     &ref[1003], R(0)
	move     0, R(1)
	move     V(23), R(2)
	move     S(5), R(3)
	pl_call  pred_update_counter_2()
	move     &ref[1004], R(0)
	move     0, R(1)
	move     &ref[1044], R(2)
	move     S(5), R(3)
	move     V(24), R(4)
	move     S(5), R(5)
	pl_call  pred_name_builder_3()
	pl_call  Object_1(&ref[1045])
	move     &ref[1046], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(30), R(4)
	move     S(5), R(5)
	pl_call  pred_tag_numbervars_3()
	move     &ref[1047], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(34), R(4)
	move     S(5), R(5)
	pl_call  pred_tag_numbervars_3()
	move     V(35), R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(36), R(4)
	move     S(5), R(5)
	pl_call  pred_tag_numbervars_3()
	call_c   Dyam_Unify(V(37),&ref[1048])
	fail_ret
	call_c   Dyam_Unify(V(39),&ref[1049])
	fail_ret
	pl_call  fun28(&seed[203],1)
	pl_call  fun28(&seed[204],1)
	call_c   Dyam_Reg_Load(0,V(15))
	move     V(42), R(2)
	move     S(5), R(3)
	pl_call  pred_tupple_2()
	call_c   Dyam_Reg_Load(0,V(7))
	move     V(43), R(2)
	move     S(5), R(3)
	pl_call  pred_tupple_2()
	move     &ref[1056], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(43))
	move     V(44), R(4)
	move     S(5), R(5)
	pl_call  pred_extend_tupple_3()
	pl_call  fun28(&seed[205],1)
	call_c   Dyam_Unify(V(46),&ref[1060])
	fail_ret
	call_c   Dyam_Unify(V(47),&ref[1061])
	fail_ret
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(14))
	call_c   Dyam_Reg_Load(4,V(31))
	call_c   Dyam_Reg_Load(6,V(32))
	call_c   Dyam_Reg_Load(8,V(33))
	call_c   Dyam_Reg_Load(10,V(32))
	call_c   Dyam_Reg_Load(12,V(47))
	move     &ref[274], R(14)
	move     0, R(15)
	move     V(49), R(16)
	move     S(5), R(17)
	call_c   Dyam_Reg_Load(18,V(10))
	call_c   Dyam_Reg_Load(20,V(11))
	call_c   Dyam_Reg_Load(22,V(12))
	call_c   Dyam_Reg_Load(24,V(13))
	pl_call  pred_tag_il_body_to_lpda_13()
	pl_call  fun28(&seed[206],1)
	call_c   Dyam_Unify(V(51),&ref[1065])
	fail_ret
	pl_call  fun28(&seed[207],1)
	call_c   Dyam_Deallocate()
	pl_jump  fun28(&seed[208],1)

;; TERM 1042: '*GUARD*'(tag_il_body_to_lpda_handler(_B, _C, _D, _E, _F, _G, _H, _I, _J, _K, _L, _M, _N)) :> []
c_code local build_ref_1042
	ret_reg &ref[1042]
	call_c   build_ref_1041()
	call_c   Dyam_Create_Binary(I(9),&ref[1041],I(0))
	move_ret ref[1042]
	c_ret

c_code local build_seed_33
	ret_reg &seed[33]
	call_c   build_ref_47()
	call_c   build_ref_1074()
	call_c   Dyam_Seed_Start(&ref[47],&ref[1074],I(0),fun1,1)
	call_c   build_ref_1073()
	call_c   Dyam_Seed_Add_Comp(&ref[1073],fun445,0)
	call_c   Dyam_Seed_End()
	move_ret seed[33]
	c_ret

;; TERM 1073: '*GUARD*'(tag_compile_subtree_noadj(_B, tag_node{id=> _C, label=> _D, children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}, _O, _P, _Q, _R, _S, _T, _U))
c_code local build_ref_1073
	ret_reg &ref[1073]
	call_c   build_ref_48()
	call_c   build_ref_1072()
	call_c   Dyam_Create_Unary(&ref[48],&ref[1072])
	move_ret ref[1073]
	c_ret

;; TERM 1072: tag_compile_subtree_noadj(_B, tag_node{id=> _C, label=> _D, children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}, _O, _P, _Q, _R, _S, _T, _U)
c_code local build_ref_1072
	ret_reg &ref[1072]
	call_c   build_ref_1383()
	call_c   build_ref_167()
	call_c   Dyam_Term_Start(&ref[1383],9)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(&ref[167])
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_End()
	move_ret ref[1072]
	c_ret

;; TERM 1150: 'TAG to SA compilation error: ~w'
c_code local build_ref_1150
	ret_reg &ref[1150]
	call_c   Dyam_Create_Atom("TAG to SA compilation error: ~w")
	move_ret ref[1150]
	c_ret

;; TERM 1151: [tag_node{id=> _C, label=> _D, children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}]
c_code local build_ref_1151
	ret_reg &ref[1151]
	call_c   build_ref_167()
	call_c   Dyam_Create_List(&ref[167],I(0))
	move_ret ref[1151]
	c_ret

long local pool_fun441[3]=[2,build_ref_1150,build_ref_1151]

pl_code local fun441
	call_c   Dyam_Remove_Choice()
	move     &ref[1150], R(0)
	move     0, R(1)
	move     &ref[1151], R(2)
	move     S(5), R(3)
	pl_call  pred_error_2()
	pl_jump  fun110()

c_code local build_seed_218
	ret_reg &seed[218]
	call_c   build_ref_48()
	call_c   build_ref_1148()
	call_c   Dyam_Seed_Start(&ref[48],&ref[1148],I(0),fun3,1)
	call_c   build_ref_1149()
	call_c   Dyam_Seed_Add_Comp(&ref[1149],&ref[1148],0)
	call_c   Dyam_Seed_End()
	move_ret seed[218]
	c_ret

;; TERM 1149: '*GUARD*'(tag_compile_subtree(_B, _E, _O, _P, _Q, _R, _S, _T, _U)) :> '$$HOLE$$'
c_code local build_ref_1149
	ret_reg &ref[1149]
	call_c   build_ref_1148()
	call_c   Dyam_Create_Binary(I(9),&ref[1148],I(7))
	move_ret ref[1149]
	c_ret

;; TERM 1148: '*GUARD*'(tag_compile_subtree(_B, _E, _O, _P, _Q, _R, _S, _T, _U))
c_code local build_ref_1148
	ret_reg &ref[1148]
	call_c   build_ref_48()
	call_c   build_ref_1147()
	call_c   Dyam_Create_Unary(&ref[48],&ref[1147])
	move_ret ref[1148]
	c_ret

;; TERM 1147: tag_compile_subtree(_B, _E, _O, _P, _Q, _R, _S, _T, _U)
c_code local build_ref_1147
	ret_reg &ref[1147]
	call_c   build_ref_1275()
	call_c   Dyam_Term_Start(&ref[1275],9)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_End()
	move_ret ref[1147]
	c_ret

long local pool_fun442[4]=[65538,build_ref_1146,build_seed_218,pool_fun441]

pl_code local fun442
	call_c   Dyam_Update_Choice(fun441)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(5),&ref[1146])
	fail_ret
	call_c   Dyam_Cut()
	pl_call  fun28(&seed[218],1)
	pl_jump  fun110()

;; TERM 1131: anchor(_Y1, _K, _D, _P1, _P, _J, _Q1)
c_code local build_ref_1131
	ret_reg &ref[1131]
	call_c   build_ref_379()
	call_c   Dyam_Term_Start(&ref[379],7)
	call_c   Dyam_Term_Arg(V(50))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(41))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(42))
	call_c   Dyam_Term_End()
	move_ret ref[1131]
	c_ret

;; TERM 1142: '$CLOSURE'('$fun'(435, 0, 1131770672), '$TUPPLE'(35075411949076))
c_code local build_ref_1142
	ret_reg &ref[1142]
	call_c   build_ref_1141()
	call_c   Dyam_Closure_Aux(fun435,&ref[1141])
	move_ret ref[1142]
	c_ret

c_code local build_seed_217
	ret_reg &seed[217]
	call_c   build_ref_48()
	call_c   build_ref_1138()
	call_c   Dyam_Seed_Start(&ref[48],&ref[1138],I(0),fun3,1)
	call_c   build_ref_1139()
	call_c   Dyam_Seed_Add_Comp(&ref[1139],&ref[1138],0)
	call_c   Dyam_Seed_End()
	move_ret seed[217]
	c_ret

;; TERM 1139: '*GUARD*'(tag_wrap_skipper(_B, _O, _P1, _P, _B2, _Q, _R, _S)) :> '$$HOLE$$'
c_code local build_ref_1139
	ret_reg &ref[1139]
	call_c   build_ref_1138()
	call_c   Dyam_Create_Binary(I(9),&ref[1138],I(7))
	move_ret ref[1139]
	c_ret

;; TERM 1138: '*GUARD*'(tag_wrap_skipper(_B, _O, _P1, _P, _B2, _Q, _R, _S))
c_code local build_ref_1138
	ret_reg &ref[1138]
	call_c   build_ref_48()
	call_c   build_ref_1137()
	call_c   Dyam_Create_Unary(&ref[48],&ref[1137])
	move_ret ref[1138]
	c_ret

;; TERM 1137: tag_wrap_skipper(_B, _O, _P1, _P, _B2, _Q, _R, _S)
c_code local build_ref_1137
	ret_reg &ref[1137]
	call_c   build_ref_1280()
	call_c   Dyam_Term_Start(&ref[1280],8)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(41))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(53))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_End()
	move_ret ref[1137]
	c_ret

long local pool_fun435[2]=[1,build_seed_217]

pl_code local fun435
	call_c   Dyam_Pool(pool_fun435)
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(50))
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(54), R(4)
	move     S(5), R(5)
	pl_call  pred_tag_numbervars_3()
	pl_call  fun28(&seed[217],1)
	pl_jump  fun110()

;; TERM 1141: '$TUPPLE'(35075411949076)
c_code local build_ref_1141
	ret_reg &ref[1141]
	call_c   Dyam_Start_Tupple(0,134249472)
	call_c   Dyam_Almost_End_Tupple(29,65680)
	move_ret ref[1141]
	c_ret

long local pool_fun436[2]=[1,build_ref_1142]

pl_code local fun438
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(53),V(52))
	fail_ret
fun436:
	move     &ref[1142], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Load(4,V(1))
	call_c   Dyam_Reg_Load(6,V(19))
	call_c   Dyam_Reg_Load(8,V(50))
	call_c   Dyam_Reg_Deallocate(5)
	pl_jump  fun52()


;; TERM 1145: '$CLOSURE'('$fun'(437, 0, 1131780780), '$TUPPLE'(35075411948948))
c_code local build_ref_1145
	ret_reg &ref[1145]
	call_c   build_ref_1144()
	call_c   Dyam_Closure_Aux(fun437,&ref[1144])
	move_ret ref[1145]
	c_ret

;; TERM 1135: '$tagop'(_C, verbose!anchor(_K, _P1, _P, _B, _J, _Q1, _Y1))
c_code local build_ref_1135
	ret_reg &ref[1135]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1399()
	call_c   Dyam_Term_Start(&ref[1399],7)
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(41))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(42))
	call_c   Dyam_Term_Arg(V(50))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_1372()
	call_c   Dyam_Create_Binary(&ref[1372],V(2),R(0))
	move_ret ref[1135]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1399: verbose!anchor
c_code local build_ref_1399
	ret_reg &ref[1399]
	call_c   build_ref_1337()
	call_c   Dyam_Create_Atom_Module("anchor",&ref[1337])
	move_ret ref[1399]
	c_ret

;; TERM 1136: _A2 , _S1
c_code local build_ref_1136
	ret_reg &ref[1136]
	call_c   Dyam_Create_Binary(I(4),V(52),V(44))
	move_ret ref[1136]
	c_ret

long local pool_fun437[4]=[65538,build_ref_1135,build_ref_1136,pool_fun436]

pl_code local fun437
	call_c   Dyam_Pool(pool_fun437)
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(44),&ref[1135])
	fail_ret
	call_c   Dyam_Unify(V(53),&ref[1136])
	fail_ret
	pl_jump  fun436()

;; TERM 1144: '$TUPPLE'(35075411948948)
c_code local build_ref_1144
	ret_reg &ref[1144]
	call_c   Dyam_Start_Tupple(0,202145280)
	call_c   Dyam_Almost_End_Tupple(29,98464)
	move_ret ref[1144]
	c_ret

long local pool_fun439[3]=[65537,build_ref_1145,pool_fun436]

pl_code local fun440
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(52),V(49))
	fail_ret
fun439:
	call_c   Dyam_Choice(fun438)
	call_c   Dyam_Set_Cut()
	move     &ref[1145], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Load(4,V(1))
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  fun171()


;; TERM 1132: tag_corrections
c_code local build_ref_1132
	ret_reg &ref[1132]
	call_c   Dyam_Create_Atom("tag_corrections")
	move_ret ref[1132]
	c_ret

;; TERM 1133: correction_anchor(_P1)
c_code local build_ref_1133
	ret_reg &ref[1133]
	call_c   build_ref_1400()
	call_c   Dyam_Create_Unary(&ref[1400],V(41))
	move_ret ref[1133]
	c_ret

;; TERM 1400: correction_anchor
c_code local build_ref_1400
	ret_reg &ref[1400]
	call_c   Dyam_Create_Atom("correction_anchor")
	move_ret ref[1400]
	c_ret

;; TERM 1134: _Z1 , _X1
c_code local build_ref_1134
	ret_reg &ref[1134]
	call_c   Dyam_Create_Binary(I(4),V(51),V(49))
	move_ret ref[1134]
	c_ret

long local pool_fun443[9]=[196613,build_ref_379,build_ref_1131,build_ref_1132,build_ref_1133,build_ref_1134,pool_fun442,pool_fun439,pool_fun439]

pl_code local fun443
	call_c   Dyam_Update_Choice(fun442)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(5),&ref[379])
	fail_ret
	call_c   Dyam_Cut()
	move     V(42), R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(48), R(4)
	move     S(5), R(5)
	pl_call  pred_tag_numbervars_3()
	call_c   Dyam_Unify(V(49),&ref[1131])
	fail_ret
	call_c   Dyam_Choice(fun440)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[1132])
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(51),&ref[1133])
	fail_ret
	call_c   Dyam_Unify(V(52),&ref[1134])
	fail_ret
	pl_jump  fun439()

;; TERM 1115: coanchor(_K, _D, _P1, _P, _J)
c_code local build_ref_1115
	ret_reg &ref[1115]
	call_c   build_ref_1114()
	call_c   Dyam_Term_Start(&ref[1114],5)
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(41))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret ref[1115]
	c_ret

c_code local build_seed_215
	ret_reg &seed[215]
	call_c   build_ref_48()
	call_c   build_ref_1119()
	call_c   Dyam_Seed_Start(&ref[48],&ref[1119],I(0),fun3,1)
	call_c   build_ref_1120()
	call_c   Dyam_Seed_Add_Comp(&ref[1120],&ref[1119],0)
	call_c   Dyam_Seed_End()
	move_ret seed[215]
	c_ret

;; TERM 1120: '*GUARD*'(tag_wrap_skipper(_B, _O, _P1, _P, _T1, _Q, _Z, _S)) :> '$$HOLE$$'
c_code local build_ref_1120
	ret_reg &ref[1120]
	call_c   build_ref_1119()
	call_c   Dyam_Create_Binary(I(9),&ref[1119],I(7))
	move_ret ref[1120]
	c_ret

;; TERM 1119: '*GUARD*'(tag_wrap_skipper(_B, _O, _P1, _P, _T1, _Q, _Z, _S))
c_code local build_ref_1119
	ret_reg &ref[1119]
	call_c   build_ref_48()
	call_c   build_ref_1118()
	call_c   Dyam_Create_Unary(&ref[48],&ref[1118])
	move_ret ref[1119]
	c_ret

;; TERM 1118: tag_wrap_skipper(_B, _O, _P1, _P, _T1, _Q, _Z, _S)
c_code local build_ref_1118
	ret_reg &ref[1118]
	call_c   build_ref_1280()
	call_c   Dyam_Term_Start(&ref[1280],8)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(41))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(45))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(25))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_End()
	move_ret ref[1118]
	c_ret

pl_code local fun420
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(17),V(25))
	fail_ret
	pl_jump  fun110()

;; TERM 1127: '$CLOSURE'('$fun'(431, 0, 1131723052), '$TUPPLE'(35075411948604))
c_code local build_ref_1127
	ret_reg &ref[1127]
	call_c   build_ref_1126()
	call_c   Dyam_Closure_Aux(fun431,&ref[1126])
	move_ret ref[1127]
	c_ret

;; TERM 1121: tag_anchor{family=> _J1, coanchors=> _K1, equations=> _U1}
c_code local build_ref_1121
	ret_reg &ref[1121]
	call_c   build_ref_1241()
	call_c   Dyam_Term_Start(&ref[1241],3)
	call_c   Dyam_Term_Arg(V(35))
	call_c   Dyam_Term_Arg(V(36))
	call_c   Dyam_Term_Arg(V(46))
	call_c   Dyam_Term_End()
	move_ret ref[1121]
	c_ret

;; TERM 1080: tag_coanchor(_J1, _C, _M1, _K1)
c_code local build_ref_1080
	ret_reg &ref[1080]
	call_c   build_ref_1327()
	call_c   Dyam_Term_Start(&ref[1327],4)
	call_c   Dyam_Term_Arg(V(35))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(38))
	call_c   Dyam_Term_Arg(V(36))
	call_c   Dyam_Term_End()
	move_ret ref[1080]
	c_ret

c_code local build_seed_216
	ret_reg &seed[216]
	call_c   build_ref_48()
	call_c   build_ref_1123()
	call_c   Dyam_Seed_Start(&ref[48],&ref[1123],I(0),fun3,1)
	call_c   build_ref_1124()
	call_c   Dyam_Seed_Add_Comp(&ref[1124],&ref[1123],0)
	call_c   Dyam_Seed_End()
	move_ret seed[216]
	c_ret

;; TERM 1124: '*GUARD*'(body_to_lpda(_B, check_coanchor(_M1, _P1, _P), _Z, _R, _S)) :> '$$HOLE$$'
c_code local build_ref_1124
	ret_reg &ref[1124]
	call_c   build_ref_1123()
	call_c   Dyam_Create_Binary(I(9),&ref[1123],I(7))
	move_ret ref[1124]
	c_ret

;; TERM 1123: '*GUARD*'(body_to_lpda(_B, check_coanchor(_M1, _P1, _P), _Z, _R, _S))
c_code local build_ref_1123
	ret_reg &ref[1123]
	call_c   build_ref_48()
	call_c   build_ref_1122()
	call_c   Dyam_Create_Unary(&ref[48],&ref[1122])
	move_ret ref[1123]
	c_ret

;; TERM 1122: body_to_lpda(_B, check_coanchor(_M1, _P1, _P), _Z, _R, _S)
c_code local build_ref_1122
	ret_reg &ref[1122]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1358()
	call_c   Dyam_Term_Start(&ref[1358],3)
	call_c   Dyam_Term_Arg(V(38))
	call_c   Dyam_Term_Arg(V(41))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_1319()
	call_c   Dyam_Term_Start(&ref[1319],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(25))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_End()
	move_ret ref[1122]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun431[4]=[3,build_ref_1121,build_ref_1080,build_seed_216]

pl_code local fun431
	call_c   Dyam_Pool(pool_fun431)
	call_c   Dyam_Unify(V(34),&ref[1121])
	fail_ret
	call_c   Dyam_Allocate(0)
	pl_call  Object_1(&ref[1080])
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(38))
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(47), R(4)
	move     S(5), R(5)
	pl_call  pred_tag_numbervars_3()
	pl_call  fun28(&seed[216],1)
	pl_jump  fun110()

;; TERM 1126: '$TUPPLE'(35075411948604)
c_code local build_ref_1126
	ret_reg &ref[1126]
	call_c   Dyam_Start_Tupple(0,201337864)
	call_c   Dyam_Almost_End_Tupple(29,8454144)
	move_ret ref[1126]
	c_ret

long local pool_fun432[3]=[2,build_seed_215,build_ref_1127]

pl_code local fun434
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(45),V(40))
	fail_ret
fun432:
	pl_call  fun28(&seed[215],1)
	call_c   Dyam_Choice(fun420)
	call_c   Dyam_Set_Cut()
	move     &ref[1127], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Load(4,V(1))
	call_c   Dyam_Reg_Load(6,V(19))
	move     V(34), R(8)
	move     S(5), R(9)
	call_c   Dyam_Reg_Deallocate(5)
	pl_jump  fun52()


;; TERM 1130: '$CLOSURE'('$fun'(433, 0, 1131733076), '$TUPPLE'(35075411948744))
c_code local build_ref_1130
	ret_reg &ref[1130]
	call_c   build_ref_1129()
	call_c   Dyam_Closure_Aux(fun433,&ref[1129])
	move_ret ref[1130]
	c_ret

;; TERM 1116: '$tagop'(_C, verbose!coanchor(_K, _P1, _P, _J, _Q1))
c_code local build_ref_1116
	ret_reg &ref[1116]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1401()
	call_c   Dyam_Term_Start(&ref[1401],5)
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(41))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(42))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_1372()
	call_c   Dyam_Create_Binary(&ref[1372],V(2),R(0))
	move_ret ref[1116]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1401: verbose!coanchor
c_code local build_ref_1401
	ret_reg &ref[1401]
	call_c   build_ref_1337()
	call_c   Dyam_Create_Atom_Module("coanchor",&ref[1337])
	move_ret ref[1401]
	c_ret

;; TERM 1117: _O1 , _S1
c_code local build_ref_1117
	ret_reg &ref[1117]
	call_c   Dyam_Create_Binary(I(4),V(40),V(44))
	move_ret ref[1117]
	c_ret

long local pool_fun433[4]=[65538,build_ref_1116,build_ref_1117,pool_fun432]

pl_code local fun433
	call_c   Dyam_Pool(pool_fun433)
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Cut()
	move     V(42), R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(43), R(4)
	move     S(5), R(5)
	pl_call  pred_tag_numbervars_3()
	call_c   Dyam_Unify(V(44),&ref[1116])
	fail_ret
	call_c   Dyam_Unify(V(45),&ref[1117])
	fail_ret
	pl_jump  fun432()

;; TERM 1129: '$TUPPLE'(35075411948744)
c_code local build_ref_1129
	ret_reg &ref[1129]
	call_c   Dyam_Start_Tupple(0,202145280)
	call_c   Dyam_Almost_End_Tupple(29,196608)
	move_ret ref[1129]
	c_ret

long local pool_fun444[6]=[131075,build_ref_1114,build_ref_1115,build_ref_1130,pool_fun443,pool_fun432]

pl_code local fun444
	call_c   Dyam_Update_Choice(fun443)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(5),&ref[1114])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(40),&ref[1115])
	fail_ret
	call_c   Dyam_Choice(fun434)
	call_c   Dyam_Set_Cut()
	move     &ref[1130], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Load(4,V(1))
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  fun171()

;; TERM 1111: '$CLOSURE'('$fun'(427, 0, 1131684464), '$TUPPLE'(35075411948128))
c_code local build_ref_1111
	ret_reg &ref[1111]
	call_c   build_ref_1110()
	call_c   Dyam_Closure_Aux(fun427,&ref[1110])
	move_ret ref[1111]
	c_ret

;; TERM 1107: '$CLOSURE'('$fun'(426, 0, 1131672200), '$TUPPLE'(35075411948064))
c_code local build_ref_1107
	ret_reg &ref[1107]
	call_c   build_ref_1106()
	call_c   Dyam_Closure_Aux(fun426,&ref[1106])
	move_ret ref[1107]
	c_ret

;; TERM 1104: '*WRAPPER-CALL-ALT*'(tag_subst((_F1 / _G1)), _H1, _Q)
c_code local build_ref_1104
	ret_reg &ref[1104]
	call_c   build_ref_1361()
	call_c   build_ref_1112()
	call_c   Dyam_Term_Start(&ref[1361],3)
	call_c   Dyam_Term_Arg(&ref[1112])
	call_c   Dyam_Term_Arg(V(33))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_End()
	move_ret ref[1104]
	c_ret

;; TERM 1112: tag_subst((_F1 / _G1))
c_code local build_ref_1112
	ret_reg &ref[1112]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1294()
	call_c   Dyam_Create_Binary(&ref[1294],V(31),V(32))
	move_ret R(0)
	call_c   build_ref_1313()
	call_c   Dyam_Create_Unary(&ref[1313],R(0))
	move_ret ref[1112]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1086: '$CLOSURE'('$fun'(419, 0, 1131593956), '$TUPPLE'(35075411947440))
c_code local build_ref_1086
	ret_reg &ref[1086]
	call_c   build_ref_1085()
	call_c   Dyam_Closure_Aux(fun419,&ref[1085])
	move_ret ref[1086]
	c_ret

;; TERM 1079: tag_anchor{family=> _J1, coanchors=> _K1, equations=> _L1}
c_code local build_ref_1079
	ret_reg &ref[1079]
	call_c   build_ref_1241()
	call_c   Dyam_Term_Start(&ref[1241],3)
	call_c   Dyam_Term_Arg(V(35))
	call_c   Dyam_Term_Arg(V(36))
	call_c   Dyam_Term_Arg(V(37))
	call_c   Dyam_Term_End()
	move_ret ref[1079]
	c_ret

c_code local build_seed_210
	ret_reg &seed[210]
	call_c   build_ref_48()
	call_c   build_ref_1082()
	call_c   Dyam_Seed_Start(&ref[48],&ref[1082],I(0),fun3,1)
	call_c   build_ref_1083()
	call_c   Dyam_Seed_Add_Comp(&ref[1083],&ref[1082],0)
	call_c   Dyam_Seed_End()
	move_ret seed[210]
	c_ret

;; TERM 1083: '*GUARD*'(body_to_lpda(_B, check_coanchor(_M1, _O, _P), _Z, _R, _S)) :> '$$HOLE$$'
c_code local build_ref_1083
	ret_reg &ref[1083]
	call_c   build_ref_1082()
	call_c   Dyam_Create_Binary(I(9),&ref[1082],I(7))
	move_ret ref[1083]
	c_ret

;; TERM 1082: '*GUARD*'(body_to_lpda(_B, check_coanchor(_M1, _O, _P), _Z, _R, _S))
c_code local build_ref_1082
	ret_reg &ref[1082]
	call_c   build_ref_48()
	call_c   build_ref_1081()
	call_c   Dyam_Create_Unary(&ref[48],&ref[1081])
	move_ret ref[1082]
	c_ret

;; TERM 1081: body_to_lpda(_B, check_coanchor(_M1, _O, _P), _Z, _R, _S)
c_code local build_ref_1081
	ret_reg &ref[1081]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1358()
	call_c   Dyam_Term_Start(&ref[1358],3)
	call_c   Dyam_Term_Arg(V(38))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_1319()
	call_c   Dyam_Term_Start(&ref[1319],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(25))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_End()
	move_ret ref[1081]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun419[4]=[3,build_ref_1079,build_ref_1080,build_seed_210]

pl_code local fun419
	call_c   Dyam_Pool(pool_fun419)
	call_c   Dyam_Unify(V(34),&ref[1079])
	fail_ret
	call_c   Dyam_Allocate(0)
	pl_call  Object_1(&ref[1080])
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(38))
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(39), R(4)
	move     S(5), R(5)
	pl_call  pred_tag_numbervars_3()
	pl_call  fun28(&seed[210],1)
	pl_jump  fun110()

;; TERM 1085: '$TUPPLE'(35075411947440)
c_code local build_ref_1085
	ret_reg &ref[1085]
	call_c   Dyam_Start_Tupple(0,201354248)
	call_c   Dyam_Almost_End_Tupple(29,8388608)
	move_ret ref[1085]
	c_ret

long local pool_fun421[2]=[1,build_ref_1086]

long local pool_fun426[3]=[65537,build_ref_1104,pool_fun421]

pl_code local fun426
	call_c   Dyam_Pool(pool_fun426)
	call_c   Dyam_Unify(V(25),&ref[1104])
	fail_ret
	call_c   Dyam_Allocate(0)
fun421:
	call_c   Dyam_Choice(fun420)
	call_c   Dyam_Set_Cut()
	move     &ref[1086], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Load(4,V(1))
	call_c   Dyam_Reg_Load(6,V(19))
	move     V(34), R(8)
	move     S(5), R(9)
	call_c   Dyam_Reg_Deallocate(5)
	pl_jump  fun52()


;; TERM 1106: '$TUPPLE'(35075411948064)
c_code local build_ref_1106
	ret_reg &ref[1106]
	call_c   Dyam_Start_Tupple(0,201358848)
	call_c   Dyam_Almost_End_Tupple(29,117440512)
	move_ret ref[1106]
	c_ret

;; TERM 1108: tag_subst((_F1 / _G1), _D)
c_code local build_ref_1108
	ret_reg &ref[1108]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1294()
	call_c   Dyam_Create_Binary(&ref[1294],V(31),V(32))
	move_ret R(0)
	call_c   build_ref_1313()
	call_c   Dyam_Create_Binary(&ref[1313],R(0),V(3))
	move_ret ref[1108]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun427[3]=[2,build_ref_1107,build_ref_1108]

pl_code local fun427
	call_c   Dyam_Pool(pool_fun427)
	call_c   Dyam_Allocate(0)
	move     &ref[1107], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	move     &ref[1108], R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  fun243()

;; TERM 1110: '$TUPPLE'(35075411948128)
c_code local build_ref_1110
	ret_reg &ref[1110]
	call_c   Dyam_Start_Tupple(0,234913280)
	call_c   Dyam_Almost_End_Tupple(29,117440512)
	move_ret ref[1110]
	c_ret

;; TERM 1113: [_O,_P,_C,_J,_B]
c_code local build_ref_1113
	ret_reg &ref[1113]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_148()
	call_c   Dyam_Create_List(V(9),&ref[148])
	move_ret R(0)
	call_c   Dyam_Create_List(V(2),R(0))
	move_ret R(0)
	call_c   Dyam_Create_Tupple(14,15,R(0))
	move_ret ref[1113]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun428[4]=[3,build_ref_1111,build_ref_1112,build_ref_1113]

pl_code local fun428
	call_c   Dyam_Remove_Choice()
	call_c   DYAM_evpred_functor(V(9),V(31),V(32))
	fail_ret
	move     &ref[1111], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	move     &ref[1112], R(4)
	move     S(5), R(5)
	move     &ref[1113], R(6)
	move     S(5), R(7)
	move     V(33), R(8)
	move     S(5), R(9)
	call_c   Dyam_Reg_Deallocate(5)
	pl_jump  fun130()

pl_code local fun429
	call_c   Dyam_Update_Choice(fun428)
	call_c   Dyam_Set_Cut()
	pl_fail

;; TERM 1103: '$CLOSURE'('$fun'(425, 0, 1131661260), '$TUPPLE'(35075411947796))
c_code local build_ref_1103
	ret_reg &ref[1103]
	call_c   build_ref_1102()
	call_c   Dyam_Closure_Aux(fun425,&ref[1102])
	move_ret ref[1103]
	c_ret

;; TERM 1094: [_A1,_D,_J,_O,_P,_C]
c_code local build_ref_1094
	ret_reg &ref[1094]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(2),I(0))
	move_ret R(0)
	call_c   Dyam_Create_Tupple(14,15,R(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(9),R(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(3),R(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(26),R(0))
	move_ret ref[1094]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_213
	ret_reg &seed[213]
	call_c   build_ref_48()
	call_c   build_ref_1096()
	call_c   Dyam_Seed_Start(&ref[48],&ref[1096],I(0),fun3,1)
	call_c   build_ref_1097()
	call_c   Dyam_Seed_Add_Comp(&ref[1097],&ref[1096],0)
	call_c   Dyam_Seed_End()
	move_ret seed[213]
	c_ret

;; TERM 1097: '*GUARD*'(body_to_lpda(_B, _B1, _C1, _E1, _S)) :> '$$HOLE$$'
c_code local build_ref_1097
	ret_reg &ref[1097]
	call_c   build_ref_1096()
	call_c   Dyam_Create_Binary(I(9),&ref[1096],I(7))
	move_ret ref[1097]
	c_ret

;; TERM 1096: '*GUARD*'(body_to_lpda(_B, _B1, _C1, _E1, _S))
c_code local build_ref_1096
	ret_reg &ref[1096]
	call_c   build_ref_48()
	call_c   build_ref_1095()
	call_c   Dyam_Create_Unary(&ref[48],&ref[1095])
	move_ret ref[1096]
	c_ret

;; TERM 1095: body_to_lpda(_B, _B1, _C1, _E1, _S)
c_code local build_ref_1095
	ret_reg &ref[1095]
	call_c   build_ref_1319()
	call_c   Dyam_Term_Start(&ref[1319],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(27))
	call_c   Dyam_Term_Arg(V(28))
	call_c   Dyam_Term_Arg(V(30))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_End()
	move_ret ref[1095]
	c_ret

c_code local build_seed_214
	ret_reg &seed[214]
	call_c   build_ref_48()
	call_c   build_ref_1099()
	call_c   Dyam_Seed_Start(&ref[48],&ref[1099],I(0),fun3,1)
	call_c   build_ref_1100()
	call_c   Dyam_Seed_Add_Comp(&ref[1100],&ref[1099],0)
	call_c   Dyam_Seed_End()
	move_ret seed[214]
	c_ret

;; TERM 1100: '*GUARD*'(tag_compile_subtree(_B, '$protect'(tag_node{id=> _C, label=> _D, children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}), _O, _P, _C1, _D1, _S, _T, _U)) :> '$$HOLE$$'
c_code local build_ref_1100
	ret_reg &ref[1100]
	call_c   build_ref_1099()
	call_c   Dyam_Create_Binary(I(9),&ref[1099],I(7))
	move_ret ref[1100]
	c_ret

;; TERM 1099: '*GUARD*'(tag_compile_subtree(_B, '$protect'(tag_node{id=> _C, label=> _D, children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}), _O, _P, _C1, _D1, _S, _T, _U))
c_code local build_ref_1099
	ret_reg &ref[1099]
	call_c   build_ref_48()
	call_c   build_ref_1098()
	call_c   Dyam_Create_Unary(&ref[48],&ref[1098])
	move_ret ref[1099]
	c_ret

;; TERM 1098: tag_compile_subtree(_B, '$protect'(tag_node{id=> _C, label=> _D, children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}), _O, _P, _C1, _D1, _S, _T, _U)
c_code local build_ref_1098
	ret_reg &ref[1098]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1273()
	call_c   build_ref_167()
	call_c   Dyam_Create_Unary(&ref[1273],&ref[167])
	move_ret R(0)
	call_c   build_ref_1275()
	call_c   Dyam_Term_Start(&ref[1275],9)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(28))
	call_c   Dyam_Term_Arg(V(29))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_End()
	move_ret ref[1098]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun425[5]=[65539,build_ref_1094,build_seed_213,build_seed_214,pool_fun421]

pl_code local fun425
	call_c   Dyam_Pool(pool_fun425)
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Cut()
	call_c   DYAM_evpred_univ(V(27),&ref[1094])
	fail_ret
	call_c   Dyam_Reg_Load(0,V(16))
	move     V(25), R(2)
	move     S(5), R(3)
	move     V(28), R(4)
	move     S(5), R(5)
	move     V(29), R(6)
	move     S(5), R(7)
	move     V(30), R(8)
	move     S(5), R(9)
	pl_call  pred_handle_choice_5()
	pl_call  fun28(&seed[213],1)
	pl_call  fun28(&seed[214],1)
	pl_jump  fun421()

;; TERM 1102: '$TUPPLE'(35075411947796)
c_code local build_ref_1102
	ret_reg &ref[1102]
	call_c   Dyam_Create_Simple_Tupple(0,268435204)
	move_ret ref[1102]
	c_ret

long local pool_fun430[3]=[65537,build_ref_1103,pool_fun428]

pl_code local fun430
	call_c   Dyam_Update_Choice(fun429)
	call_c   Dyam_Set_Cut()
	move     &ref[1103], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Load(4,V(18))
	move     V(26), R(6)
	move     S(5), R(7)
	call_c   Dyam_Reg_Deallocate(4)
fun424:
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Reg_Bind(2,V(4))
	call_c   Dyam_Multi_Reg_Bind(4,2,2)
	call_c   Dyam_Choice(fun423)
	pl_call  fun46(&seed[211],1)
	pl_fail


c_code local build_seed_209
	ret_reg &seed[209]
	call_c   build_ref_48()
	call_c   build_ref_1076()
	call_c   Dyam_Seed_Start(&ref[48],&ref[1076],I(0),fun3,1)
	call_c   build_ref_1077()
	call_c   Dyam_Seed_Add_Comp(&ref[1077],&ref[1076],0)
	call_c   Dyam_Seed_End()
	move_ret seed[209]
	c_ret

;; TERM 1077: '*GUARD*'(make_tag_subst_callret(_J, _O, _P, _X, _Y)) :> '$$HOLE$$'
c_code local build_ref_1077
	ret_reg &ref[1077]
	call_c   build_ref_1076()
	call_c   Dyam_Create_Binary(I(9),&ref[1076],I(7))
	move_ret ref[1077]
	c_ret

;; TERM 1076: '*GUARD*'(make_tag_subst_callret(_J, _O, _P, _X, _Y))
c_code local build_ref_1076
	ret_reg &ref[1076]
	call_c   build_ref_48()
	call_c   build_ref_1075()
	call_c   Dyam_Create_Unary(&ref[48],&ref[1075])
	move_ret ref[1076]
	c_ret

;; TERM 1075: make_tag_subst_callret(_J, _O, _P, _X, _Y)
c_code local build_ref_1075
	ret_reg &ref[1075]
	call_c   build_ref_1323()
	call_c   Dyam_Term_Start(&ref[1323],5)
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(23))
	call_c   Dyam_Term_Arg(V(24))
	call_c   Dyam_Term_End()
	move_ret ref[1075]
	c_ret

;; TERM 1078: '*SA-PSEUDO-SUBST*'(_X, _Y, _Q, _C)
c_code local build_ref_1078
	ret_reg &ref[1078]
	call_c   build_ref_1402()
	call_c   Dyam_Term_Start(&ref[1402],4)
	call_c   Dyam_Term_Arg(V(23))
	call_c   Dyam_Term_Arg(V(24))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_End()
	move_ret ref[1078]
	c_ret

;; TERM 1402: '*SA-PSEUDO-SUBST*'
c_code local build_ref_1402
	ret_reg &ref[1402]
	call_c   Dyam_Create_Atom("*SA-PSEUDO-SUBST*")
	move_ret ref[1402]
	c_ret

long local pool_fun445[9]=[196613,build_ref_1073,build_ref_284,build_ref_799,build_seed_209,build_ref_1078,pool_fun444,pool_fun430,pool_fun421]

pl_code local fun445
	call_c   Dyam_Pool(pool_fun445)
	call_c   Dyam_Unify_Item(&ref[1073])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(9))
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(21), R(4)
	move     S(5), R(5)
	pl_call  pred_tag_numbervars_3()
	call_c   Dyam_Choice(fun444)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(5),&ref[284])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun430)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(18),&ref[799])
	fail_ret
	call_c   Dyam_Cut()
	pl_call  fun28(&seed[209],1)
	call_c   Dyam_Unify(V(25),&ref[1078])
	fail_ret
	pl_jump  fun421()

;; TERM 1074: '*GUARD*'(tag_compile_subtree_noadj(_B, tag_node{id=> _C, label=> _D, children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}, _O, _P, _Q, _R, _S, _T, _U)) :> []
c_code local build_ref_1074
	ret_reg &ref[1074]
	call_c   build_ref_1073()
	call_c   Dyam_Create_Binary(I(9),&ref[1073],I(0))
	move_ret ref[1074]
	c_ret

c_code local build_seed_46
	ret_reg &seed[46]
	call_c   build_ref_47()
	call_c   build_ref_1154()
	call_c   Dyam_Seed_Start(&ref[47],&ref[1154],I(0),fun1,1)
	call_c   build_ref_1153()
	call_c   Dyam_Seed_Add_Comp(&ref[1153],fun459,0)
	call_c   Dyam_Seed_End()
	move_ret seed[46]
	c_ret

;; TERM 1153: '*GUARD*'(tag_compile_node(_B, tag_node{id=> _C, label=> _D, children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}, _O, _P, _Q, _R, _S, _T, _U))
c_code local build_ref_1153
	ret_reg &ref[1153]
	call_c   build_ref_48()
	call_c   build_ref_1152()
	call_c   Dyam_Create_Unary(&ref[48],&ref[1152])
	move_ret ref[1153]
	c_ret

;; TERM 1152: tag_compile_node(_B, tag_node{id=> _C, label=> _D, children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}, _O, _P, _Q, _R, _S, _T, _U)
c_code local build_ref_1152
	ret_reg &ref[1152]
	call_c   build_ref_1380()
	call_c   build_ref_167()
	call_c   Dyam_Term_Start(&ref[1380],9)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(&ref[167])
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_End()
	move_ret ref[1152]
	c_ret

c_code local build_seed_222
	ret_reg &seed[222]
	call_c   build_ref_48()
	call_c   build_ref_1195()
	call_c   Dyam_Seed_Start(&ref[48],&ref[1195],I(0),fun3,1)
	call_c   build_ref_1196()
	call_c   Dyam_Seed_Add_Comp(&ref[1196],&ref[1195],0)
	call_c   Dyam_Seed_End()
	move_ret seed[222]
	c_ret

;; TERM 1196: '*GUARD*'(tig_compile_subtree_left(_B, tag_node{id=> _C, label=> _D, children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}, _O, _P, _Q, _R, _S, _T, _W2, _U)) :> '$$HOLE$$'
c_code local build_ref_1196
	ret_reg &ref[1196]
	call_c   build_ref_1195()
	call_c   Dyam_Create_Binary(I(9),&ref[1195],I(7))
	move_ret ref[1196]
	c_ret

;; TERM 1195: '*GUARD*'(tig_compile_subtree_left(_B, tag_node{id=> _C, label=> _D, children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}, _O, _P, _Q, _R, _S, _T, _W2, _U))
c_code local build_ref_1195
	ret_reg &ref[1195]
	call_c   build_ref_48()
	call_c   build_ref_1194()
	call_c   Dyam_Create_Unary(&ref[48],&ref[1194])
	move_ret ref[1195]
	c_ret

;; TERM 1194: tig_compile_subtree_left(_B, tag_node{id=> _C, label=> _D, children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}, _O, _P, _Q, _R, _S, _T, _W2, _U)
c_code local build_ref_1194
	ret_reg &ref[1194]
	call_c   build_ref_1403()
	call_c   build_ref_167()
	call_c   Dyam_Term_Start(&ref[1403],10)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(&ref[167])
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_Arg(V(74))
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_End()
	move_ret ref[1194]
	c_ret

;; TERM 1403: tig_compile_subtree_left
c_code local build_ref_1403
	ret_reg &ref[1403]
	call_c   Dyam_Create_Atom("tig_compile_subtree_left")
	move_ret ref[1403]
	c_ret

long local pool_fun455[2]=[1,build_seed_222]

pl_code local fun455
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(2),V(89))
	fail_ret
	call_c   Dyam_Unify(V(3),V(90))
	fail_ret
	call_c   Dyam_Unify(V(4),V(91))
	fail_ret
	call_c   Dyam_Unify(V(5),V(92))
	fail_ret
	call_c   Dyam_Unify(V(6),V(93))
	fail_ret
	call_c   Dyam_Unify(V(7),V(94))
	fail_ret
	call_c   Dyam_Unify(V(8),V(74))
	fail_ret
	call_c   Dyam_Unify(V(9),V(95))
	fail_ret
	call_c   Dyam_Unify(V(10),V(96))
	fail_ret
	call_c   Dyam_Unify(V(11),V(97))
	fail_ret
	call_c   Dyam_Unify(V(12),V(98))
	fail_ret
	call_c   Dyam_Unify(V(13),V(99))
	fail_ret
	pl_call  fun28(&seed[222],1)
	pl_jump  fun110()

;; TERM 1180: tag_tree_cutter(_D)
c_code local build_ref_1180
	ret_reg &ref[1180]
	call_c   build_ref_1247()
	call_c   Dyam_Create_Unary(&ref[1247],V(3))
	move_ret ref[1180]
	c_ret

;; TERM 1181: [_S2|_T2]
c_code local build_ref_1181
	ret_reg &ref[1181]
	call_c   Dyam_Create_List(V(70),V(71))
	move_ret ref[1181]
	c_ret

;; TERM 1182: cutter
c_code local build_ref_1182
	ret_reg &ref[1182]
	call_c   Dyam_Create_Atom("cutter")
	move_ret ref[1182]
	c_ret

;; TERM 1183: '_cutter~w'
c_code local build_ref_1183
	ret_reg &ref[1183]
	call_c   Dyam_Create_Atom("_cutter~w")
	move_ret ref[1183]
	c_ret

;; TERM 1184: [_C3]
c_code local build_ref_1184
	ret_reg &ref[1184]
	call_c   Dyam_Create_List(V(80),I(0))
	move_ret ref[1184]
	c_ret

;; TERM 1185: '*SA-CUT-LAST*'(_D3, _P, _H3)
c_code local build_ref_1185
	ret_reg &ref[1185]
	call_c   build_ref_1404()
	call_c   Dyam_Term_Start(&ref[1404],3)
	call_c   Dyam_Term_Arg(V(81))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(85))
	call_c   Dyam_Term_End()
	move_ret ref[1185]
	c_ret

;; TERM 1404: '*SA-CUT-LAST*'
c_code local build_ref_1404
	ret_reg &ref[1404]
	call_c   Dyam_Create_Atom("*SA-CUT-LAST*")
	move_ret ref[1404]
	c_ret

;; TERM 1193: tree_cut(tag_node{id=> _C, label=> _D, children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}, _B)
c_code local build_ref_1193
	ret_reg &ref[1193]
	call_c   build_ref_1405()
	call_c   build_ref_167()
	call_c   Dyam_Create_Binary(&ref[1405],&ref[167],V(1))
	move_ret ref[1193]
	c_ret

;; TERM 1405: tree_cut
c_code local build_ref_1405
	ret_reg &ref[1405]
	call_c   Dyam_Create_Atom("tree_cut")
	move_ret ref[1405]
	c_ret

c_code local build_seed_221
	ret_reg &seed[221]
	call_c   build_ref_48()
	call_c   build_ref_1190()
	call_c   Dyam_Seed_Start(&ref[48],&ref[1190],I(0),fun3,1)
	call_c   build_ref_1191()
	call_c   Dyam_Seed_Add_Comp(&ref[1191],&ref[1190],0)
	call_c   Dyam_Seed_End()
	move_ret seed[221]
	c_ret

;; TERM 1191: '*GUARD*'(tig_compile_subtree_left(_B, tag_node{id=> _C, label=> _D, children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}, _O, _P, _I3, _K3, _S, _T, _W2, _U)) :> '$$HOLE$$'
c_code local build_ref_1191
	ret_reg &ref[1191]
	call_c   build_ref_1190()
	call_c   Dyam_Create_Binary(I(9),&ref[1190],I(7))
	move_ret ref[1191]
	c_ret

;; TERM 1190: '*GUARD*'(tig_compile_subtree_left(_B, tag_node{id=> _C, label=> _D, children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}, _O, _P, _I3, _K3, _S, _T, _W2, _U))
c_code local build_ref_1190
	ret_reg &ref[1190]
	call_c   build_ref_48()
	call_c   build_ref_1189()
	call_c   Dyam_Create_Unary(&ref[48],&ref[1189])
	move_ret ref[1190]
	c_ret

;; TERM 1189: tig_compile_subtree_left(_B, tag_node{id=> _C, label=> _D, children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}, _O, _P, _I3, _K3, _S, _T, _W2, _U)
c_code local build_ref_1189
	ret_reg &ref[1189]
	call_c   build_ref_1403()
	call_c   build_ref_167()
	call_c   Dyam_Term_Start(&ref[1403],10)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(&ref[167])
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(86))
	call_c   Dyam_Term_Arg(V(88))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_Arg(V(74))
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_End()
	move_ret ref[1189]
	c_ret

;; TERM 1192: '*SA-CUTTER*'(_D3, _O, _F3, _K3, _I3, _Q)
c_code local build_ref_1192
	ret_reg &ref[1192]
	call_c   build_ref_1406()
	call_c   Dyam_Term_Start(&ref[1406],6)
	call_c   Dyam_Term_Arg(V(81))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(83))
	call_c   Dyam_Term_Arg(V(88))
	call_c   Dyam_Term_Arg(V(86))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_End()
	move_ret ref[1192]
	c_ret

;; TERM 1406: '*SA-CUTTER*'
c_code local build_ref_1406
	ret_reg &ref[1406]
	call_c   Dyam_Create_Atom("*SA-CUTTER*")
	move_ret ref[1406]
	c_ret

long local pool_fun453[3]=[2,build_seed_221,build_ref_1192]

long local pool_fun454[3]=[65537,build_ref_1193,pool_fun453]

pl_code local fun454
	call_c   Dyam_Remove_Choice()
	call_c   DYAM_evpred_assert_1(&ref[1193])
fun453:
	pl_call  fun28(&seed[221],1)
	call_c   Dyam_Unify(V(17),&ref[1192])
	fail_ret
	pl_jump  fun110()


;; TERM 1186: tree_cut(tag_node{id=> _C, label=> _D, children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}, _J3)
c_code local build_ref_1186
	ret_reg &ref[1186]
	call_c   build_ref_1405()
	call_c   build_ref_167()
	call_c   Dyam_Create_Binary(&ref[1405],&ref[167],V(87))
	move_ret ref[1186]
	c_ret

;; TERM 1187: 'found similar cut tree for ~w and ~w\n'
c_code local build_ref_1187
	ret_reg &ref[1187]
	call_c   Dyam_Create_Atom("found similar cut tree for ~w and ~w\n")
	move_ret ref[1187]
	c_ret

;; TERM 1188: [_B,_J3]
c_code local build_ref_1188
	ret_reg &ref[1188]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(87),I(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(1),R(0))
	move_ret ref[1188]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun456[15]=[196619,build_ref_1180,build_ref_1181,build_ref_397,build_ref_1182,build_ref_1183,build_ref_1184,build_ref_167,build_ref_1185,build_ref_1186,build_ref_1187,build_ref_1188,pool_fun455,pool_fun454,pool_fun453]

pl_code local fun456
	call_c   Dyam_Update_Choice(fun455)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[1180])
	call_c   Dyam_Unify(V(2),V(68))
	fail_ret
	call_c   Dyam_Unify(V(3),V(69))
	fail_ret
	call_c   Dyam_Unify(V(4),&ref[1181])
	fail_ret
	call_c   Dyam_Unify(V(5),V(72))
	fail_ret
	call_c   Dyam_Unify(V(6),V(73))
	fail_ret
	call_c   Dyam_Unify(V(7),&ref[397])
	fail_ret
	call_c   Dyam_Unify(V(8),V(74))
	fail_ret
	call_c   Dyam_Unify(V(9),V(75))
	fail_ret
	call_c   Dyam_Unify(V(10),V(76))
	fail_ret
	call_c   Dyam_Unify(V(11),V(77))
	fail_ret
	call_c   Dyam_Unify(V(12),V(78))
	fail_ret
	call_c   Dyam_Unify(V(13),V(79))
	fail_ret
	call_c   Dyam_Cut()
	move     &ref[1182], R(0)
	move     0, R(1)
	move     V(80), R(2)
	move     S(5), R(3)
	pl_call  pred_update_counter_2()
	move     &ref[1183], R(0)
	move     0, R(1)
	move     &ref[1184], R(2)
	move     S(5), R(3)
	move     V(81), R(4)
	move     S(5), R(5)
	pl_call  pred_name_builder_3()
	move     &ref[167], R(0)
	move     S(5), R(1)
	move     V(82), R(2)
	move     S(5), R(3)
	pl_call  pred_tupple_2()
	call_c   Dyam_Reg_Load(0,V(82))
	call_c   Dyam_Reg_Load(2,V(20))
	move     V(83), R(4)
	move     S(5), R(5)
	pl_call  pred_inter_3()
	call_c   Dyam_Reg_Load(0,V(16))
	move     V(84), R(2)
	move     S(5), R(3)
	pl_call  pred_tupple_2()
	call_c   Dyam_Reg_Load(0,V(82))
	call_c   Dyam_Reg_Load(2,V(84))
	move     V(85), R(4)
	move     S(5), R(5)
	pl_call  pred_inter_3()
	call_c   Dyam_Unify(V(86),&ref[1185])
	fail_ret
	call_c   Dyam_Choice(fun454)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[1186])
	call_c   Dyam_Cut()
	move     &ref[1187], R(0)
	move     0, R(1)
	move     &ref[1188], R(2)
	move     S(5), R(3)
	pl_call  pred_format_2()
	pl_jump  fun453()

c_code local build_seed_220
	ret_reg &seed[220]
	call_c   build_ref_48()
	call_c   build_ref_1073()
	call_c   Dyam_Seed_Start(&ref[48],&ref[1073],I(0),fun3,1)
	call_c   build_ref_1179()
	call_c   Dyam_Seed_Add_Comp(&ref[1179],&ref[1073],0)
	call_c   Dyam_Seed_End()
	move_ret seed[220]
	c_ret

;; TERM 1179: '*GUARD*'(tag_compile_subtree_noadj(_B, tag_node{id=> _C, label=> _D, children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}, _O, _P, _Q, _R, _S, _T, _U)) :> '$$HOLE$$'
c_code local build_ref_1179
	ret_reg &ref[1179]
	call_c   build_ref_1073()
	call_c   Dyam_Create_Binary(I(9),&ref[1073],I(7))
	move_ret ref[1179]
	c_ret

long local pool_fun457[4]=[65538,build_ref_397,build_seed_220,pool_fun456]

pl_code local fun457
	call_c   Dyam_Update_Choice(fun456)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(2),V(57))
	fail_ret
	call_c   Dyam_Unify(V(3),V(58))
	fail_ret
	call_c   Dyam_Unify(V(4),V(59))
	fail_ret
	call_c   Dyam_Unify(V(5),V(60))
	fail_ret
	call_c   Dyam_Unify(V(6),&ref[397])
	fail_ret
	call_c   Dyam_Unify(V(7),V(61))
	fail_ret
	call_c   Dyam_Unify(V(8),V(62))
	fail_ret
	call_c   Dyam_Unify(V(9),V(63))
	fail_ret
	call_c   Dyam_Unify(V(10),V(64))
	fail_ret
	call_c   Dyam_Unify(V(11),V(65))
	fail_ret
	call_c   Dyam_Unify(V(12),V(66))
	fail_ret
	call_c   Dyam_Unify(V(13),V(67))
	fail_ret
	call_c   Dyam_Cut()
	pl_call  fun28(&seed[220],1)
	pl_jump  fun110()

;; TERM 1174: substfoot
c_code local build_ref_1174
	ret_reg &ref[1174]
	call_c   Dyam_Create_Atom("substfoot")
	move_ret ref[1174]
	c_ret

c_code local build_seed_219
	ret_reg &seed[219]
	call_c   build_ref_48()
	call_c   build_ref_1176()
	call_c   Dyam_Seed_Start(&ref[48],&ref[1176],I(0),fun3,1)
	call_c   build_ref_1177()
	call_c   Dyam_Seed_Add_Comp(&ref[1177],&ref[1176],0)
	call_c   Dyam_Seed_End()
	move_ret seed[219]
	c_ret

;; TERM 1177: '*GUARD*'(make_tag_bot_callret(_A1, _O, _P, _D2, _E2)) :> '$$HOLE$$'
c_code local build_ref_1177
	ret_reg &ref[1177]
	call_c   build_ref_1176()
	call_c   Dyam_Create_Binary(I(9),&ref[1176],I(7))
	move_ret ref[1177]
	c_ret

;; TERM 1176: '*GUARD*'(make_tag_bot_callret(_A1, _O, _P, _D2, _E2))
c_code local build_ref_1176
	ret_reg &ref[1176]
	call_c   build_ref_48()
	call_c   build_ref_1175()
	call_c   Dyam_Create_Unary(&ref[48],&ref[1175])
	move_ret ref[1176]
	c_ret

;; TERM 1175: make_tag_bot_callret(_A1, _O, _P, _D2, _E2)
c_code local build_ref_1175
	ret_reg &ref[1175]
	call_c   build_ref_1315()
	call_c   Dyam_Term_Start(&ref[1315],5)
	call_c   Dyam_Term_Arg(V(26))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(55))
	call_c   Dyam_Term_Arg(V(56))
	call_c   Dyam_Term_End()
	move_ret ref[1175]
	c_ret

;; TERM 1178: '*SA-SUBSTFOOT*'(_D2, _E2, _Q)
c_code local build_ref_1178
	ret_reg &ref[1178]
	call_c   build_ref_1407()
	call_c   Dyam_Term_Start(&ref[1407],3)
	call_c   Dyam_Term_Arg(V(55))
	call_c   Dyam_Term_Arg(V(56))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_End()
	move_ret ref[1178]
	c_ret

;; TERM 1407: '*SA-SUBSTFOOT*'
c_code local build_ref_1407
	ret_reg &ref[1407]
	call_c   Dyam_Create_Atom("*SA-SUBSTFOOT*")
	move_ret ref[1407]
	c_ret

long local pool_fun458[5]=[65539,build_ref_1174,build_seed_219,build_ref_1178,pool_fun457]

pl_code local fun458
	call_c   Dyam_Update_Choice(fun457)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(2),V(44))
	fail_ret
	call_c   Dyam_Unify(V(3),V(45))
	fail_ret
	call_c   Dyam_Unify(V(4),V(46))
	fail_ret
	call_c   Dyam_Unify(V(5),&ref[1174])
	fail_ret
	call_c   Dyam_Unify(V(6),V(47))
	fail_ret
	call_c   Dyam_Unify(V(7),V(48))
	fail_ret
	call_c   Dyam_Unify(V(8),V(49))
	fail_ret
	call_c   Dyam_Unify(V(9),V(26))
	fail_ret
	call_c   Dyam_Unify(V(10),V(50))
	fail_ret
	call_c   Dyam_Unify(V(11),V(51))
	fail_ret
	call_c   Dyam_Unify(V(12),V(52))
	fail_ret
	call_c   Dyam_Unify(V(13),V(53))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(26))
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(54), R(4)
	move     S(5), R(5)
	pl_call  pred_tag_numbervars_3()
	pl_call  fun28(&seed[219],1)
	call_c   Dyam_Unify(V(17),&ref[1178])
	fail_ret
	pl_jump  fun110()

;; TERM 1155: foot
c_code local build_ref_1155
	ret_reg &ref[1155]
	call_c   Dyam_Create_Atom("foot")
	move_ret ref[1155]
	c_ret

;; TERM 1157: tag_foot((_H1 / _I1))
c_code local build_ref_1157
	ret_reg &ref[1157]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1294()
	call_c   Dyam_Create_Binary(&ref[1294],V(33),V(34))
	move_ret R(0)
	call_c   build_ref_1293()
	call_c   Dyam_Create_Unary(&ref[1293],R(0))
	move_ret ref[1157]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1173: '$CLOSURE'('$fun'(450, 0, 1131883592), '$TUPPLE'(35075411950160))
c_code local build_ref_1173
	ret_reg &ref[1173]
	call_c   build_ref_1172()
	call_c   Dyam_Closure_Aux(fun450,&ref[1172])
	move_ret ref[1173]
	c_ret

;; TERM 1169: '$CLOSURE'('$fun'(449, 0, 1131884492), '$TUPPLE'(35075411949988))
c_code local build_ref_1169
	ret_reg &ref[1169]
	call_c   build_ref_1168()
	call_c   Dyam_Closure_Aux(fun449,&ref[1168])
	move_ret ref[1169]
	c_ret

;; TERM 1158: '*SA-FOOT-WRAPPER-CALL*'(_J1, _K1, _L1)
c_code local build_ref_1158
	ret_reg &ref[1158]
	call_c   build_ref_1408()
	call_c   Dyam_Term_Start(&ref[1408],3)
	call_c   Dyam_Term_Arg(V(35))
	call_c   Dyam_Term_Arg(V(36))
	call_c   Dyam_Term_Arg(V(37))
	call_c   Dyam_Term_End()
	move_ret ref[1158]
	c_ret

;; TERM 1408: '*SA-FOOT-WRAPPER-CALL*'
c_code local build_ref_1408
	ret_reg &ref[1408]
	call_c   Dyam_Create_Atom("*SA-FOOT-WRAPPER-CALL*")
	move_ret ref[1408]
	c_ret

pl_code local fun448
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(37),V(16))
	fail_ret
	pl_jump  fun110()

long local pool_fun449[2]=[1,build_ref_1158]

pl_code local fun449
	call_c   Dyam_Pool(pool_fun449)
	call_c   Dyam_Unify(V(17),&ref[1158])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun448)
	call_c   Dyam_Set_Cut()
	pl_fail

;; TERM 1168: '$TUPPLE'(35075411949988)
c_code local build_ref_1168
	ret_reg &ref[1168]
	call_c   Dyam_Start_Tupple(0,167786496)
	call_c   Dyam_Almost_End_Tupple(29,6291456)
	move_ret ref[1168]
	c_ret

;; TERM 1170: [_O,_P,_A1]
c_code local build_ref_1170
	ret_reg &ref[1170]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(26),I(0))
	move_ret R(0)
	call_c   Dyam_Create_Tupple(14,15,R(0))
	move_ret ref[1170]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun450[3]=[2,build_ref_1169,build_ref_1170]

pl_code local fun450
	call_c   Dyam_Pool(pool_fun450)
	call_c   Dyam_Allocate(0)
	move     &ref[1169], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Load(4,V(35))
	move     &ref[1170], R(6)
	move     S(5), R(7)
	move     V(36), R(8)
	move     S(5), R(9)
	call_c   Dyam_Reg_Deallocate(5)
	pl_jump  fun130()

;; TERM 1172: '$TUPPLE'(35075411950160)
c_code local build_ref_1172
	ret_reg &ref[1172]
	call_c   Dyam_Start_Tupple(0,167802884)
	call_c   Dyam_Almost_End_Tupple(29,4194304)
	move_ret ref[1172]
	c_ret

long local pool_fun451[3]=[2,build_ref_1157,build_ref_1173]

pl_code local fun452
	call_c   Dyam_Remove_Choice()
fun451:
	call_c   Dyam_Reg_Load(0,V(26))
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(32), R(4)
	move     S(5), R(5)
	pl_call  pred_tag_numbervars_3()
	call_c   DYAM_evpred_functor(V(26),V(33),V(34))
	fail_ret
	call_c   Dyam_Unify(V(35),&ref[1157])
	fail_ret
	move     &ref[1173], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Load(4,V(35))
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  fun243()


;; TERM 1156: tig(_B, _F1)
c_code local build_ref_1156
	ret_reg &ref[1156]
	call_c   build_ref_1343()
	call_c   Dyam_Create_Binary(&ref[1343],V(1),V(31))
	move_ret ref[1156]
	c_ret

long local pool_fun459[6]=[131075,build_ref_1153,build_ref_1155,build_ref_1156,pool_fun458,pool_fun451]

pl_code local fun459
	call_c   Dyam_Pool(pool_fun459)
	call_c   Dyam_Unify_Item(&ref[1153])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun458)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(2),V(21))
	fail_ret
	call_c   Dyam_Unify(V(4),V(22))
	fail_ret
	call_c   Dyam_Unify(V(5),&ref[1155])
	fail_ret
	call_c   Dyam_Unify(V(6),V(23))
	fail_ret
	call_c   Dyam_Unify(V(7),V(24))
	fail_ret
	call_c   Dyam_Unify(V(8),V(25))
	fail_ret
	call_c   Dyam_Unify(V(9),V(26))
	fail_ret
	call_c   Dyam_Unify(V(10),V(27))
	fail_ret
	call_c   Dyam_Unify(V(11),V(28))
	fail_ret
	call_c   Dyam_Unify(V(12),V(29))
	fail_ret
	call_c   Dyam_Unify(V(13),V(30))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun452)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[1156])
	call_c   Dyam_Cut()
	pl_fail

;; TERM 1154: '*GUARD*'(tag_compile_node(_B, tag_node{id=> _C, label=> _D, children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}, _O, _P, _Q, _R, _S, _T, _U)) :> []
c_code local build_ref_1154
	ret_reg &ref[1154]
	call_c   build_ref_1153()
	call_c   Dyam_Create_Binary(I(9),&ref[1153],I(0))
	move_ret ref[1154]
	c_ret

c_code local build_seed_211
	ret_reg &seed[211]
	call_c   build_ref_56()
	call_c   build_ref_1088()
	call_c   Dyam_Seed_Start(&ref[56],&ref[1088],&ref[1088],fun3,1)
	call_c   build_ref_1090()
	call_c   Dyam_Seed_Add_Comp(&ref[1090],&ref[1088],0)
	call_c   Dyam_Seed_End()
	move_ret seed[211]
	c_ret

;; TERM 1090: '*FIRST*'('call_tag_custom_subst/2'(_C)) :> '$$HOLE$$'
c_code local build_ref_1090
	ret_reg &ref[1090]
	call_c   build_ref_1089()
	call_c   Dyam_Create_Binary(I(9),&ref[1089],I(7))
	move_ret ref[1090]
	c_ret

;; TERM 1089: '*FIRST*'('call_tag_custom_subst/2'(_C))
c_code local build_ref_1089
	ret_reg &ref[1089]
	call_c   build_ref_52()
	call_c   build_ref_1087()
	call_c   Dyam_Create_Unary(&ref[52],&ref[1087])
	move_ret ref[1089]
	c_ret

;; TERM 1087: 'call_tag_custom_subst/2'(_C)
c_code local build_ref_1087
	ret_reg &ref[1087]
	call_c   build_ref_1237()
	call_c   Dyam_Create_Unary(&ref[1237],V(2))
	move_ret ref[1087]
	c_ret

;; TERM 1088: '*CITEM*'('call_tag_custom_subst/2'(_C), 'call_tag_custom_subst/2'(_C))
c_code local build_ref_1088
	ret_reg &ref[1088]
	call_c   build_ref_56()
	call_c   build_ref_1087()
	call_c   Dyam_Create_Binary(&ref[56],&ref[1087],&ref[1087])
	move_ret ref[1088]
	c_ret

c_code local build_seed_212
	ret_reg &seed[212]
	call_c   build_ref_186()
	call_c   build_ref_1093()
	call_c   Dyam_Seed_Start(&ref[186],&ref[1093],I(0),fun15,1)
	call_c   build_ref_1091()
	call_c   Dyam_Seed_Add_Comp(&ref[1091],fun422,0)
	call_c   Dyam_Seed_End()
	move_ret seed[212]
	c_ret

;; TERM 1091: '*RITEM*'('call_tag_custom_subst/2'(_C), return(_D))
c_code local build_ref_1091
	ret_reg &ref[1091]
	call_c   build_ref_0()
	call_c   build_ref_1087()
	call_c   build_ref_196()
	call_c   Dyam_Create_Binary(&ref[0],&ref[1087],&ref[196])
	move_ret ref[1091]
	c_ret

pl_code local fun422
	call_c   build_ref_1091()
	call_c   Dyam_Unify_Item(&ref[1091])
	fail_ret
	pl_jump  Follow_Cont(V(1))

;; TERM 1093: '*RITEM*'('call_tag_custom_subst/2'(_C), return(_D)) :> tag_compile(12, '$TUPPLE'(35075412198240))
c_code local build_ref_1093
	ret_reg &ref[1093]
	call_c   build_ref_1091()
	call_c   build_ref_1092()
	call_c   Dyam_Create_Binary(I(9),&ref[1091],&ref[1092])
	move_ret ref[1093]
	c_ret

;; TERM 1092: tag_compile(12, '$TUPPLE'(35075412198240))
c_code local build_ref_1092
	ret_reg &ref[1092]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,134217728)
	move_ret R(0)
	call_c   build_ref_1409()
	call_c   Dyam_Create_Binary(&ref[1409],N(12),R(0))
	move_ret ref[1092]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1409: tag_compile
c_code local build_ref_1409
	ret_reg &ref[1409]
	call_c   Dyam_Create_Atom("tag_compile")
	move_ret ref[1409]
	c_ret

;; TERM 186: '*CURNEXT*'
c_code local build_ref_186
	ret_reg &ref[186]
	call_c   Dyam_Create_Atom("*CURNEXT*")
	move_ret ref[186]
	c_ret

c_code local build_seed_185
	ret_reg &seed[185]
	call_c   build_ref_56()
	call_c   build_ref_941()
	call_c   Dyam_Seed_Start(&ref[56],&ref[941],&ref[941],fun3,1)
	call_c   build_ref_943()
	call_c   Dyam_Seed_Add_Comp(&ref[943],&ref[941],0)
	call_c   Dyam_Seed_End()
	move_ret seed[185]
	c_ret

;; TERM 943: '*FIRST*'(interleave_initialize) :> '$$HOLE$$'
c_code local build_ref_943
	ret_reg &ref[943]
	call_c   build_ref_942()
	call_c   Dyam_Create_Binary(I(9),&ref[942],I(7))
	move_ret ref[943]
	c_ret

;; TERM 942: '*FIRST*'(interleave_initialize)
c_code local build_ref_942
	ret_reg &ref[942]
	call_c   build_ref_52()
	call_c   build_ref_14()
	call_c   Dyam_Create_Unary(&ref[52],&ref[14])
	move_ret ref[942]
	c_ret

;; TERM 14: interleave_initialize
c_code local build_ref_14
	ret_reg &ref[14]
	call_c   Dyam_Create_Atom("interleave_initialize")
	move_ret ref[14]
	c_ret

;; TERM 941: '*CITEM*'(interleave_initialize, interleave_initialize)
c_code local build_ref_941
	ret_reg &ref[941]
	call_c   build_ref_56()
	call_c   build_ref_14()
	call_c   Dyam_Create_Binary(&ref[56],&ref[14],&ref[14])
	move_ret ref[941]
	c_ret

c_code local build_seed_186
	ret_reg &seed[186]
	call_c   build_ref_186()
	call_c   build_ref_945()
	call_c   Dyam_Seed_Start(&ref[186],&ref[945],I(0),fun15,1)
	call_c   build_ref_15()
	call_c   Dyam_Seed_Add_Comp(&ref[15],fun348,0)
	call_c   Dyam_Seed_End()
	move_ret seed[186]
	c_ret

;; TERM 15: '*RITEM*'(interleave_initialize, voidret)
c_code local build_ref_15
	ret_reg &ref[15]
	call_c   build_ref_0()
	call_c   build_ref_14()
	call_c   build_ref_2()
	call_c   Dyam_Create_Binary(&ref[0],&ref[14],&ref[2])
	move_ret ref[15]
	c_ret

pl_code local fun348
	call_c   build_ref_15()
	call_c   Dyam_Unify_Item(&ref[15])
	fail_ret
	pl_jump  Follow_Cont(V(1))

;; TERM 945: '*RITEM*'(interleave_initialize, voidret) :> tag_compile(11, '$TUPPLE'(35075412198240))
c_code local build_ref_945
	ret_reg &ref[945]
	call_c   build_ref_15()
	call_c   build_ref_944()
	call_c   Dyam_Create_Binary(I(9),&ref[15],&ref[944])
	move_ret ref[945]
	c_ret

;; TERM 944: tag_compile(11, '$TUPPLE'(35075412198240))
c_code local build_ref_944
	ret_reg &ref[944]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,134217728)
	move_ret R(0)
	call_c   build_ref_1409()
	call_c   Dyam_Create_Binary(&ref[1409],N(11),R(0))
	move_ret ref[944]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_183
	ret_reg &seed[183]
	call_c   build_ref_56()
	call_c   build_ref_919()
	call_c   Dyam_Seed_Start(&ref[56],&ref[919],&ref[919],fun3,1)
	call_c   build_ref_921()
	call_c   Dyam_Seed_Add_Comp(&ref[921],&ref[919],0)
	call_c   Dyam_Seed_End()
	move_ret seed[183]
	c_ret

;; TERM 921: '*FIRST*'('call_apply_feature_mode_constraints/4'(_C, _D)) :> '$$HOLE$$'
c_code local build_ref_921
	ret_reg &ref[921]
	call_c   build_ref_920()
	call_c   Dyam_Create_Binary(I(9),&ref[920],I(7))
	move_ret ref[921]
	c_ret

;; TERM 920: '*FIRST*'('call_apply_feature_mode_constraints/4'(_C, _D))
c_code local build_ref_920
	ret_reg &ref[920]
	call_c   build_ref_52()
	call_c   build_ref_918()
	call_c   Dyam_Create_Unary(&ref[52],&ref[918])
	move_ret ref[920]
	c_ret

;; TERM 918: 'call_apply_feature_mode_constraints/4'(_C, _D)
c_code local build_ref_918
	ret_reg &ref[918]
	call_c   build_ref_1410()
	call_c   Dyam_Create_Binary(&ref[1410],V(2),V(3))
	move_ret ref[918]
	c_ret

;; TERM 1410: 'call_apply_feature_mode_constraints/4'
c_code local build_ref_1410
	ret_reg &ref[1410]
	call_c   Dyam_Create_Atom("call_apply_feature_mode_constraints/4")
	move_ret ref[1410]
	c_ret

;; TERM 919: '*CITEM*'('call_apply_feature_mode_constraints/4'(_C, _D), 'call_apply_feature_mode_constraints/4'(_C, _D))
c_code local build_ref_919
	ret_reg &ref[919]
	call_c   build_ref_56()
	call_c   build_ref_918()
	call_c   Dyam_Create_Binary(&ref[56],&ref[918],&ref[918])
	move_ret ref[919]
	c_ret

c_code local build_seed_184
	ret_reg &seed[184]
	call_c   build_ref_186()
	call_c   build_ref_925()
	call_c   Dyam_Seed_Start(&ref[186],&ref[925],I(0),fun15,1)
	call_c   build_ref_923()
	call_c   Dyam_Seed_Add_Comp(&ref[923],fun335,0)
	call_c   Dyam_Seed_End()
	move_ret seed[184]
	c_ret

;; TERM 923: '*RITEM*'('call_apply_feature_mode_constraints/4'(_C, _D), return(_E, _F))
c_code local build_ref_923
	ret_reg &ref[923]
	call_c   build_ref_0()
	call_c   build_ref_918()
	call_c   build_ref_922()
	call_c   Dyam_Create_Binary(&ref[0],&ref[918],&ref[922])
	move_ret ref[923]
	c_ret

;; TERM 922: return(_E, _F)
c_code local build_ref_922
	ret_reg &ref[922]
	call_c   build_ref_1233()
	call_c   Dyam_Create_Binary(&ref[1233],V(4),V(5))
	move_ret ref[922]
	c_ret

pl_code local fun335
	call_c   build_ref_923()
	call_c   Dyam_Unify_Item(&ref[923])
	fail_ret
	pl_jump  Follow_Cont(V(1))

;; TERM 925: '*RITEM*'('call_apply_feature_mode_constraints/4'(_C, _D), return(_E, _F)) :> tag_compile(10, '$TUPPLE'(35075412198240))
c_code local build_ref_925
	ret_reg &ref[925]
	call_c   build_ref_923()
	call_c   build_ref_924()
	call_c   Dyam_Create_Binary(I(9),&ref[923],&ref[924])
	move_ret ref[925]
	c_ret

;; TERM 924: tag_compile(10, '$TUPPLE'(35075412198240))
c_code local build_ref_924
	ret_reg &ref[924]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,134217728)
	move_ret R(0)
	call_c   build_ref_1409()
	call_c   Dyam_Create_Binary(&ref[1409],N(10),R(0))
	move_ret ref[924]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_181
	ret_reg &seed[181]
	call_c   build_ref_48()
	call_c   build_ref_902()
	call_c   Dyam_Seed_Start(&ref[48],&ref[902],I(0),fun3,1)
	call_c   build_ref_903()
	call_c   Dyam_Seed_Add_Comp(&ref[903],&ref[902],0)
	call_c   Dyam_Seed_End()
	move_ret seed[181]
	c_ret

;; TERM 903: '*GUARD*'(body_to_lpda(_B, (\+ \+ _C), _D, _E, _F)) :> '$$HOLE$$'
c_code local build_ref_903
	ret_reg &ref[903]
	call_c   build_ref_902()
	call_c   Dyam_Create_Binary(I(9),&ref[902],I(7))
	move_ret ref[903]
	c_ret

;; TERM 902: '*GUARD*'(body_to_lpda(_B, (\+ \+ _C), _D, _E, _F))
c_code local build_ref_902
	ret_reg &ref[902]
	call_c   build_ref_48()
	call_c   build_ref_901()
	call_c   Dyam_Create_Unary(&ref[48],&ref[901])
	move_ret ref[902]
	c_ret

;; TERM 901: body_to_lpda(_B, (\+ \+ _C), _D, _E, _F)
c_code local build_ref_901
	ret_reg &ref[901]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1411()
	call_c   Dyam_Create_Unary(&ref[1411],V(2))
	move_ret R(0)
	call_c   Dyam_Create_Unary(&ref[1411],R(0))
	move_ret R(0)
	call_c   build_ref_1319()
	call_c   Dyam_Term_Start(&ref[1319],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[901]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1411: \+
c_code local build_ref_1411
	ret_reg &ref[1411]
	call_c   Dyam_Create_Atom("\\+")
	move_ret ref[1411]
	c_ret

c_code local build_seed_159
	ret_reg &seed[159]
	call_c   build_ref_56()
	call_c   build_ref_794()
	call_c   Dyam_Seed_Start(&ref[56],&ref[794],&ref[794],fun3,1)
	call_c   build_ref_796()
	call_c   Dyam_Seed_Add_Comp(&ref[796],&ref[794],0)
	call_c   Dyam_Seed_End()
	move_ret seed[159]
	c_ret

;; TERM 796: '*FIRST*'(metacall_initialize) :> '$$HOLE$$'
c_code local build_ref_796
	ret_reg &ref[796]
	call_c   build_ref_795()
	call_c   Dyam_Create_Binary(I(9),&ref[795],I(7))
	move_ret ref[796]
	c_ret

;; TERM 795: '*FIRST*'(metacall_initialize)
c_code local build_ref_795
	ret_reg &ref[795]
	call_c   build_ref_52()
	call_c   build_ref_39()
	call_c   Dyam_Create_Unary(&ref[52],&ref[39])
	move_ret ref[795]
	c_ret

;; TERM 39: metacall_initialize
c_code local build_ref_39
	ret_reg &ref[39]
	call_c   Dyam_Create_Atom("metacall_initialize")
	move_ret ref[39]
	c_ret

;; TERM 794: '*CITEM*'(metacall_initialize, metacall_initialize)
c_code local build_ref_794
	ret_reg &ref[794]
	call_c   build_ref_56()
	call_c   build_ref_39()
	call_c   Dyam_Create_Binary(&ref[56],&ref[39],&ref[39])
	move_ret ref[794]
	c_ret

c_code local build_seed_160
	ret_reg &seed[160]
	call_c   build_ref_186()
	call_c   build_ref_798()
	call_c   Dyam_Seed_Start(&ref[186],&ref[798],I(0),fun15,1)
	call_c   build_ref_40()
	call_c   Dyam_Seed_Add_Comp(&ref[40],fun302,0)
	call_c   Dyam_Seed_End()
	move_ret seed[160]
	c_ret

;; TERM 40: '*RITEM*'(metacall_initialize, voidret)
c_code local build_ref_40
	ret_reg &ref[40]
	call_c   build_ref_0()
	call_c   build_ref_39()
	call_c   build_ref_2()
	call_c   Dyam_Create_Binary(&ref[0],&ref[39],&ref[2])
	move_ret ref[40]
	c_ret

pl_code local fun302
	call_c   build_ref_40()
	call_c   Dyam_Unify_Item(&ref[40])
	fail_ret
	pl_jump  Follow_Cont(V(1))

;; TERM 798: '*RITEM*'(metacall_initialize, voidret) :> tag_compile(9, '$TUPPLE'(35075412198240))
c_code local build_ref_798
	ret_reg &ref[798]
	call_c   build_ref_40()
	call_c   build_ref_797()
	call_c   Dyam_Create_Binary(I(9),&ref[40],&ref[797])
	move_ret ref[798]
	c_ret

;; TERM 797: tag_compile(9, '$TUPPLE'(35075412198240))
c_code local build_ref_797
	ret_reg &ref[797]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,134217728)
	move_ret R(0)
	call_c   build_ref_1409()
	call_c   Dyam_Create_Binary(&ref[1409],N(9),R(0))
	move_ret ref[797]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_149
	ret_reg &seed[149]
	call_c   build_ref_56()
	call_c   build_ref_672()
	call_c   Dyam_Seed_Start(&ref[56],&ref[672],&ref[672],fun3,1)
	call_c   build_ref_674()
	call_c   Dyam_Seed_Add_Comp(&ref[674],&ref[672],0)
	call_c   Dyam_Seed_End()
	move_ret seed[149]
	c_ret

;; TERM 674: '*FIRST*'(wrapping_predicate(_C)) :> '$$HOLE$$'
c_code local build_ref_674
	ret_reg &ref[674]
	call_c   build_ref_673()
	call_c   Dyam_Create_Binary(I(9),&ref[673],I(7))
	move_ret ref[674]
	c_ret

;; TERM 673: '*FIRST*'(wrapping_predicate(_C))
c_code local build_ref_673
	ret_reg &ref[673]
	call_c   build_ref_52()
	call_c   build_ref_671()
	call_c   Dyam_Create_Unary(&ref[52],&ref[671])
	move_ret ref[673]
	c_ret

;; TERM 671: wrapping_predicate(_C)
c_code local build_ref_671
	ret_reg &ref[671]
	call_c   build_ref_1314()
	call_c   Dyam_Create_Unary(&ref[1314],V(2))
	move_ret ref[671]
	c_ret

;; TERM 672: '*CITEM*'(wrapping_predicate(_C), wrapping_predicate(_C))
c_code local build_ref_672
	ret_reg &ref[672]
	call_c   build_ref_56()
	call_c   build_ref_671()
	call_c   Dyam_Create_Binary(&ref[56],&ref[671],&ref[671])
	move_ret ref[672]
	c_ret

c_code local build_seed_150
	ret_reg &seed[150]
	call_c   build_ref_186()
	call_c   build_ref_677()
	call_c   Dyam_Seed_Start(&ref[186],&ref[677],I(0),fun15,1)
	call_c   build_ref_675()
	call_c   Dyam_Seed_Add_Comp(&ref[675],fun241,0)
	call_c   Dyam_Seed_End()
	move_ret seed[150]
	c_ret

;; TERM 675: '*RITEM*'(wrapping_predicate(_C), voidret)
c_code local build_ref_675
	ret_reg &ref[675]
	call_c   build_ref_0()
	call_c   build_ref_671()
	call_c   build_ref_2()
	call_c   Dyam_Create_Binary(&ref[0],&ref[671],&ref[2])
	move_ret ref[675]
	c_ret

pl_code local fun241
	call_c   build_ref_675()
	call_c   Dyam_Unify_Item(&ref[675])
	fail_ret
	pl_jump  Follow_Cont(V(1))

;; TERM 677: '*RITEM*'(wrapping_predicate(_C), voidret) :> tag_compile(8, '$TUPPLE'(35075412198240))
c_code local build_ref_677
	ret_reg &ref[677]
	call_c   build_ref_675()
	call_c   build_ref_676()
	call_c   Dyam_Create_Binary(I(9),&ref[675],&ref[676])
	move_ret ref[677]
	c_ret

;; TERM 676: tag_compile(8, '$TUPPLE'(35075412198240))
c_code local build_ref_676
	ret_reg &ref[676]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,134217728)
	move_ret R(0)
	call_c   build_ref_1409()
	call_c   Dyam_Create_Binary(&ref[1409],N(8),R(0))
	move_ret ref[676]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_108
	ret_reg &seed[108]
	call_c   build_ref_56()
	call_c   build_ref_411()
	call_c   Dyam_Seed_Start(&ref[56],&ref[411],&ref[411],fun3,1)
	call_c   build_ref_413()
	call_c   Dyam_Seed_Add_Comp(&ref[413],&ref[411],0)
	call_c   Dyam_Seed_End()
	move_ret seed[108]
	c_ret

;; TERM 413: '*FIRST*'(tag_il_args(_C, _D, _E, _F, _G, _H, _I, _J)) :> '$$HOLE$$'
c_code local build_ref_413
	ret_reg &ref[413]
	call_c   build_ref_412()
	call_c   Dyam_Create_Binary(I(9),&ref[412],I(7))
	move_ret ref[413]
	c_ret

;; TERM 412: '*FIRST*'(tag_il_args(_C, _D, _E, _F, _G, _H, _I, _J))
c_code local build_ref_412
	ret_reg &ref[412]
	call_c   build_ref_52()
	call_c   build_ref_410()
	call_c   Dyam_Create_Unary(&ref[52],&ref[410])
	move_ret ref[412]
	c_ret

;; TERM 410: tag_il_args(_C, _D, _E, _F, _G, _H, _I, _J)
c_code local build_ref_410
	ret_reg &ref[410]
	call_c   build_ref_1236()
	call_c   Dyam_Term_Start(&ref[1236],8)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret ref[410]
	c_ret

;; TERM 411: '*CITEM*'(tag_il_args(_C, _D, _E, _F, _G, _H, _I, _J), tag_il_args(_C, _D, _E, _F, _G, _H, _I, _J))
c_code local build_ref_411
	ret_reg &ref[411]
	call_c   build_ref_56()
	call_c   build_ref_410()
	call_c   Dyam_Create_Binary(&ref[56],&ref[410],&ref[410])
	move_ret ref[411]
	c_ret

c_code local build_seed_138
	ret_reg &seed[138]
	call_c   build_ref_186()
	call_c   build_ref_591()
	call_c   Dyam_Seed_Start(&ref[186],&ref[591],I(0),fun15,1)
	call_c   build_ref_589()
	call_c   Dyam_Seed_Add_Comp(&ref[589],fun217,0)
	call_c   Dyam_Seed_End()
	move_ret seed[138]
	c_ret

;; TERM 589: '*RITEM*'(tag_il_args(_C, _D, _E, _F, _G, _H, _I, _J), voidret)
c_code local build_ref_589
	ret_reg &ref[589]
	call_c   build_ref_0()
	call_c   build_ref_410()
	call_c   build_ref_2()
	call_c   Dyam_Create_Binary(&ref[0],&ref[410],&ref[2])
	move_ret ref[589]
	c_ret

pl_code local fun217
	call_c   build_ref_589()
	call_c   Dyam_Unify_Item(&ref[589])
	fail_ret
	pl_jump  Follow_Cont(V(1))

;; TERM 591: '*RITEM*'(tag_il_args(_C, _D, _E, _F, _G, _H, _I, _J), voidret) :> tag_compile(7, '$TUPPLE'(35075412198240))
c_code local build_ref_591
	ret_reg &ref[591]
	call_c   build_ref_589()
	call_c   build_ref_590()
	call_c   Dyam_Create_Binary(I(9),&ref[589],&ref[590])
	move_ret ref[591]
	c_ret

;; TERM 590: tag_compile(7, '$TUPPLE'(35075412198240))
c_code local build_ref_590
	ret_reg &ref[590]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,134217728)
	move_ret R(0)
	call_c   build_ref_1409()
	call_c   Dyam_Create_Binary(&ref[1409],N(7),R(0))
	move_ret ref[590]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_133
	ret_reg &seed[133]
	call_c   build_ref_48()
	call_c   build_ref_548()
	call_c   Dyam_Seed_Start(&ref[48],&ref[548],I(0),fun3,1)
	call_c   build_ref_549()
	call_c   Dyam_Seed_Add_Comp(&ref[549],&ref[548],0)
	call_c   Dyam_Seed_End()
	move_ret seed[133]
	c_ret

;; TERM 549: '*GUARD*'(body_to_lpda(_B, _C, _D, _J, _F)) :> '$$HOLE$$'
c_code local build_ref_549
	ret_reg &ref[549]
	call_c   build_ref_548()
	call_c   Dyam_Create_Binary(I(9),&ref[548],I(7))
	move_ret ref[549]
	c_ret

;; TERM 548: '*GUARD*'(body_to_lpda(_B, _C, _D, _J, _F))
c_code local build_ref_548
	ret_reg &ref[548]
	call_c   build_ref_48()
	call_c   build_ref_547()
	call_c   Dyam_Create_Unary(&ref[48],&ref[547])
	move_ret ref[548]
	c_ret

;; TERM 547: body_to_lpda(_B, _C, _D, _J, _F)
c_code local build_ref_547
	ret_reg &ref[547]
	call_c   build_ref_1319()
	call_c   Dyam_Term_Start(&ref[1319],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[547]
	c_ret

c_code local build_seed_126
	ret_reg &seed[126]
	call_c   build_ref_56()
	call_c   build_ref_492()
	call_c   Dyam_Seed_Start(&ref[56],&ref[492],&ref[492],fun3,1)
	call_c   build_ref_494()
	call_c   Dyam_Seed_Add_Comp(&ref[494],&ref[492],0)
	call_c   Dyam_Seed_End()
	move_ret seed[126]
	c_ret

;; TERM 494: '*FIRST*'('call_tagguide_name/3'(_C, _D)) :> '$$HOLE$$'
c_code local build_ref_494
	ret_reg &ref[494]
	call_c   build_ref_493()
	call_c   Dyam_Create_Binary(I(9),&ref[493],I(7))
	move_ret ref[494]
	c_ret

;; TERM 493: '*FIRST*'('call_tagguide_name/3'(_C, _D))
c_code local build_ref_493
	ret_reg &ref[493]
	call_c   build_ref_52()
	call_c   build_ref_491()
	call_c   Dyam_Create_Unary(&ref[52],&ref[491])
	move_ret ref[493]
	c_ret

;; TERM 491: 'call_tagguide_name/3'(_C, _D)
c_code local build_ref_491
	ret_reg &ref[491]
	call_c   build_ref_1412()
	call_c   Dyam_Create_Binary(&ref[1412],V(2),V(3))
	move_ret ref[491]
	c_ret

;; TERM 1412: 'call_tagguide_name/3'
c_code local build_ref_1412
	ret_reg &ref[1412]
	call_c   Dyam_Create_Atom("call_tagguide_name/3")
	move_ret ref[1412]
	c_ret

;; TERM 492: '*CITEM*'('call_tagguide_name/3'(_C, _D), 'call_tagguide_name/3'(_C, _D))
c_code local build_ref_492
	ret_reg &ref[492]
	call_c   build_ref_56()
	call_c   build_ref_491()
	call_c   Dyam_Create_Binary(&ref[56],&ref[491],&ref[491])
	move_ret ref[492]
	c_ret

c_code local build_seed_127
	ret_reg &seed[127]
	call_c   build_ref_186()
	call_c   build_ref_497()
	call_c   Dyam_Seed_Start(&ref[186],&ref[497],I(0),fun15,1)
	call_c   build_ref_495()
	call_c   Dyam_Seed_Add_Comp(&ref[495],fun175,0)
	call_c   Dyam_Seed_End()
	move_ret seed[127]
	c_ret

;; TERM 495: '*RITEM*'('call_tagguide_name/3'(_C, _D), return(_E))
c_code local build_ref_495
	ret_reg &ref[495]
	call_c   build_ref_0()
	call_c   build_ref_491()
	call_c   build_ref_187()
	call_c   Dyam_Create_Binary(&ref[0],&ref[491],&ref[187])
	move_ret ref[495]
	c_ret

pl_code local fun175
	call_c   build_ref_495()
	call_c   Dyam_Unify_Item(&ref[495])
	fail_ret
	pl_jump  Follow_Cont(V(1))

;; TERM 497: '*RITEM*'('call_tagguide_name/3'(_C, _D), return(_E)) :> tag_compile(6, '$TUPPLE'(35075412198240))
c_code local build_ref_497
	ret_reg &ref[497]
	call_c   build_ref_495()
	call_c   build_ref_496()
	call_c   Dyam_Create_Binary(I(9),&ref[495],&ref[496])
	move_ret ref[497]
	c_ret

;; TERM 496: tag_compile(6, '$TUPPLE'(35075412198240))
c_code local build_ref_496
	ret_reg &ref[496]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,134217728)
	move_ret R(0)
	call_c   build_ref_1409()
	call_c   Dyam_Create_Binary(&ref[1409],N(6),R(0))
	move_ret ref[496]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_124
	ret_reg &seed[124]
	call_c   build_ref_56()
	call_c   build_ref_485()
	call_c   Dyam_Seed_Start(&ref[56],&ref[485],&ref[485],fun3,1)
	call_c   build_ref_487()
	call_c   Dyam_Seed_Add_Comp(&ref[487],&ref[485],0)
	call_c   Dyam_Seed_End()
	move_ret seed[124]
	c_ret

;; TERM 487: '*FIRST*'('call_tagguide_pred/2'(_C)) :> '$$HOLE$$'
c_code local build_ref_487
	ret_reg &ref[487]
	call_c   build_ref_486()
	call_c   Dyam_Create_Binary(I(9),&ref[486],I(7))
	move_ret ref[487]
	c_ret

;; TERM 486: '*FIRST*'('call_tagguide_pred/2'(_C))
c_code local build_ref_486
	ret_reg &ref[486]
	call_c   build_ref_52()
	call_c   build_ref_484()
	call_c   Dyam_Create_Unary(&ref[52],&ref[484])
	move_ret ref[486]
	c_ret

;; TERM 484: 'call_tagguide_pred/2'(_C)
c_code local build_ref_484
	ret_reg &ref[484]
	call_c   build_ref_1413()
	call_c   Dyam_Create_Unary(&ref[1413],V(2))
	move_ret ref[484]
	c_ret

;; TERM 1413: 'call_tagguide_pred/2'
c_code local build_ref_1413
	ret_reg &ref[1413]
	call_c   Dyam_Create_Atom("call_tagguide_pred/2")
	move_ret ref[1413]
	c_ret

;; TERM 485: '*CITEM*'('call_tagguide_pred/2'(_C), 'call_tagguide_pred/2'(_C))
c_code local build_ref_485
	ret_reg &ref[485]
	call_c   build_ref_56()
	call_c   build_ref_484()
	call_c   Dyam_Create_Binary(&ref[56],&ref[484],&ref[484])
	move_ret ref[485]
	c_ret

c_code local build_seed_125
	ret_reg &seed[125]
	call_c   build_ref_186()
	call_c   build_ref_490()
	call_c   Dyam_Seed_Start(&ref[186],&ref[490],I(0),fun15,1)
	call_c   build_ref_488()
	call_c   Dyam_Seed_Add_Comp(&ref[488],fun172,0)
	call_c   Dyam_Seed_End()
	move_ret seed[125]
	c_ret

;; TERM 488: '*RITEM*'('call_tagguide_pred/2'(_C), return(_D))
c_code local build_ref_488
	ret_reg &ref[488]
	call_c   build_ref_0()
	call_c   build_ref_484()
	call_c   build_ref_196()
	call_c   Dyam_Create_Binary(&ref[0],&ref[484],&ref[196])
	move_ret ref[488]
	c_ret

pl_code local fun172
	call_c   build_ref_488()
	call_c   Dyam_Unify_Item(&ref[488])
	fail_ret
	pl_jump  Follow_Cont(V(1))

;; TERM 490: '*RITEM*'('call_tagguide_pred/2'(_C), return(_D)) :> tag_compile(5, '$TUPPLE'(35075412198240))
c_code local build_ref_490
	ret_reg &ref[490]
	call_c   build_ref_488()
	call_c   build_ref_489()
	call_c   Dyam_Create_Binary(I(9),&ref[488],&ref[489])
	move_ret ref[490]
	c_ret

;; TERM 489: tag_compile(5, '$TUPPLE'(35075412198240))
c_code local build_ref_489
	ret_reg &ref[489]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,134217728)
	move_ret R(0)
	call_c   build_ref_1409()
	call_c   Dyam_Create_Binary(&ref[1409],N(5),R(0))
	move_ret ref[489]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_118
	ret_reg &seed[118]
	call_c   build_ref_56()
	call_c   build_ref_465()
	call_c   Dyam_Seed_Start(&ref[56],&ref[465],&ref[465],fun3,1)
	call_c   build_ref_467()
	call_c   Dyam_Seed_Add_Comp(&ref[467],&ref[465],0)
	call_c   Dyam_Seed_End()
	move_ret seed[118]
	c_ret

;; TERM 467: '*FIRST*'(verbose_tag(_C)) :> '$$HOLE$$'
c_code local build_ref_467
	ret_reg &ref[467]
	call_c   build_ref_466()
	call_c   Dyam_Create_Binary(I(9),&ref[466],I(7))
	move_ret ref[467]
	c_ret

;; TERM 466: '*FIRST*'(verbose_tag(_C))
c_code local build_ref_466
	ret_reg &ref[466]
	call_c   build_ref_52()
	call_c   build_ref_464()
	call_c   Dyam_Create_Unary(&ref[52],&ref[464])
	move_ret ref[466]
	c_ret

;; TERM 464: verbose_tag(_C)
c_code local build_ref_464
	ret_reg &ref[464]
	call_c   build_ref_138()
	call_c   Dyam_Create_Unary(&ref[138],V(2))
	move_ret ref[464]
	c_ret

;; TERM 465: '*CITEM*'(verbose_tag(_C), verbose_tag(_C))
c_code local build_ref_465
	ret_reg &ref[465]
	call_c   build_ref_56()
	call_c   build_ref_464()
	call_c   Dyam_Create_Binary(&ref[56],&ref[464],&ref[464])
	move_ret ref[465]
	c_ret

c_code local build_seed_119
	ret_reg &seed[119]
	call_c   build_ref_186()
	call_c   build_ref_470()
	call_c   Dyam_Seed_Start(&ref[186],&ref[470],I(0),fun15,1)
	call_c   build_ref_468()
	call_c   Dyam_Seed_Add_Comp(&ref[468],fun169,0)
	call_c   Dyam_Seed_End()
	move_ret seed[119]
	c_ret

;; TERM 468: '*RITEM*'(verbose_tag(_C), voidret)
c_code local build_ref_468
	ret_reg &ref[468]
	call_c   build_ref_0()
	call_c   build_ref_464()
	call_c   build_ref_2()
	call_c   Dyam_Create_Binary(&ref[0],&ref[464],&ref[2])
	move_ret ref[468]
	c_ret

pl_code local fun169
	call_c   build_ref_468()
	call_c   Dyam_Unify_Item(&ref[468])
	fail_ret
	pl_jump  Follow_Cont(V(1))

;; TERM 470: '*RITEM*'(verbose_tag(_C), voidret) :> tag_compile(4, '$TUPPLE'(35075412198240))
c_code local build_ref_470
	ret_reg &ref[470]
	call_c   build_ref_468()
	call_c   build_ref_469()
	call_c   Dyam_Create_Binary(I(9),&ref[468],&ref[469])
	move_ret ref[470]
	c_ret

;; TERM 469: tag_compile(4, '$TUPPLE'(35075412198240))
c_code local build_ref_469
	ret_reg &ref[469]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,134217728)
	move_ret R(0)
	call_c   build_ref_1409()
	call_c   Dyam_Create_Binary(&ref[1409],N(4),R(0))
	move_ret ref[469]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_115
	ret_reg &seed[115]
	call_c   build_ref_56()
	call_c   build_ref_451()
	call_c   Dyam_Seed_Start(&ref[56],&ref[451],&ref[451],fun3,1)
	call_c   build_ref_453()
	call_c   Dyam_Seed_Add_Comp(&ref[453],&ref[451],0)
	call_c   Dyam_Seed_End()
	move_ret seed[115]
	c_ret

;; TERM 453: '*FIRST*'(register_predicate(_C)) :> '$$HOLE$$'
c_code local build_ref_453
	ret_reg &ref[453]
	call_c   build_ref_452()
	call_c   Dyam_Create_Binary(I(9),&ref[452],I(7))
	move_ret ref[453]
	c_ret

;; TERM 452: '*FIRST*'(register_predicate(_C))
c_code local build_ref_452
	ret_reg &ref[452]
	call_c   build_ref_52()
	call_c   build_ref_450()
	call_c   Dyam_Create_Unary(&ref[52],&ref[450])
	move_ret ref[452]
	c_ret

;; TERM 450: register_predicate(_C)
c_code local build_ref_450
	ret_reg &ref[450]
	call_c   build_ref_1254()
	call_c   Dyam_Create_Unary(&ref[1254],V(2))
	move_ret ref[450]
	c_ret

;; TERM 451: '*CITEM*'(register_predicate(_C), register_predicate(_C))
c_code local build_ref_451
	ret_reg &ref[451]
	call_c   build_ref_56()
	call_c   build_ref_450()
	call_c   Dyam_Create_Binary(&ref[56],&ref[450],&ref[450])
	move_ret ref[451]
	c_ret

c_code local build_seed_116
	ret_reg &seed[116]
	call_c   build_ref_186()
	call_c   build_ref_456()
	call_c   Dyam_Seed_Start(&ref[186],&ref[456],I(0),fun15,1)
	call_c   build_ref_454()
	call_c   Dyam_Seed_Add_Comp(&ref[454],fun166,0)
	call_c   Dyam_Seed_End()
	move_ret seed[116]
	c_ret

;; TERM 454: '*RITEM*'(register_predicate(_C), voidret)
c_code local build_ref_454
	ret_reg &ref[454]
	call_c   build_ref_0()
	call_c   build_ref_450()
	call_c   build_ref_2()
	call_c   Dyam_Create_Binary(&ref[0],&ref[450],&ref[2])
	move_ret ref[454]
	c_ret

pl_code local fun166
	call_c   build_ref_454()
	call_c   Dyam_Unify_Item(&ref[454])
	fail_ret
	pl_jump  Follow_Cont(V(1))

;; TERM 456: '*RITEM*'(register_predicate(_C), voidret) :> tag_compile(3, '$TUPPLE'(35075412198240))
c_code local build_ref_456
	ret_reg &ref[456]
	call_c   build_ref_454()
	call_c   build_ref_455()
	call_c   Dyam_Create_Binary(I(9),&ref[454],&ref[455])
	move_ret ref[456]
	c_ret

;; TERM 455: tag_compile(3, '$TUPPLE'(35075412198240))
c_code local build_ref_455
	ret_reg &ref[455]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,134217728)
	move_ret R(0)
	call_c   build_ref_1409()
	call_c   Dyam_Create_Binary(&ref[1409],N(3),R(0))
	move_ret ref[455]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_114
	ret_reg &seed[114]
	call_c   build_ref_48()
	call_c   build_ref_442()
	call_c   Dyam_Seed_Start(&ref[48],&ref[442],I(0),fun3,1)
	call_c   build_ref_443()
	call_c   Dyam_Seed_Add_Comp(&ref[443],&ref[442],0)
	call_c   Dyam_Seed_End()
	move_ret seed[114]
	c_ret

;; TERM 443: '*GUARD*'(tag_compile_lemma_equations(_D, _G, _B)) :> '$$HOLE$$'
c_code local build_ref_443
	ret_reg &ref[443]
	call_c   build_ref_442()
	call_c   Dyam_Create_Binary(I(9),&ref[442],I(7))
	move_ret ref[443]
	c_ret

;; TERM 442: '*GUARD*'(tag_compile_lemma_equations(_D, _G, _B))
c_code local build_ref_442
	ret_reg &ref[442]
	call_c   build_ref_48()
	call_c   build_ref_441()
	call_c   Dyam_Create_Unary(&ref[48],&ref[441])
	move_ret ref[442]
	c_ret

;; TERM 441: tag_compile_lemma_equations(_D, _G, _B)
c_code local build_ref_441
	ret_reg &ref[441]
	call_c   build_ref_1229()
	call_c   Dyam_Term_Start(&ref[1229],3)
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_End()
	move_ret ref[441]
	c_ret

c_code local build_seed_113
	ret_reg &seed[113]
	call_c   build_ref_48()
	call_c   build_ref_439()
	call_c   Dyam_Seed_Start(&ref[48],&ref[439],I(0),fun3,1)
	call_c   build_ref_440()
	call_c   Dyam_Seed_Add_Comp(&ref[440],&ref[439],0)
	call_c   Dyam_Seed_End()
	move_ret seed[113]
	c_ret

;; TERM 440: '*GUARD*'(tag_compile_lemma_coanchors(_C, _F, _B, _J)) :> '$$HOLE$$'
c_code local build_ref_440
	ret_reg &ref[440]
	call_c   build_ref_439()
	call_c   Dyam_Create_Binary(I(9),&ref[439],I(7))
	move_ret ref[440]
	c_ret

;; TERM 439: '*GUARD*'(tag_compile_lemma_coanchors(_C, _F, _B, _J))
c_code local build_ref_439
	ret_reg &ref[439]
	call_c   build_ref_48()
	call_c   build_ref_438()
	call_c   Dyam_Create_Unary(&ref[48],&ref[438])
	move_ret ref[439]
	c_ret

;; TERM 438: tag_compile_lemma_coanchors(_C, _F, _B, _J)
c_code local build_ref_438
	ret_reg &ref[438]
	call_c   build_ref_1231()
	call_c   Dyam_Term_Start(&ref[1231],4)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret ref[438]
	c_ret

c_code local build_seed_112
	ret_reg &seed[112]
	call_c   build_ref_48()
	call_c   build_ref_430()
	call_c   Dyam_Seed_Start(&ref[48],&ref[430],I(0),fun3,1)
	call_c   build_ref_431()
	call_c   Dyam_Seed_Add_Comp(&ref[431],&ref[430],0)
	call_c   Dyam_Seed_End()
	move_ret seed[112]
	c_ret

;; TERM 431: '*GUARD*'(body_to_lpda(_B, _Z, _Y, _Q, dyalog)) :> '$$HOLE$$'
c_code local build_ref_431
	ret_reg &ref[431]
	call_c   build_ref_430()
	call_c   Dyam_Create_Binary(I(9),&ref[430],I(7))
	move_ret ref[431]
	c_ret

;; TERM 430: '*GUARD*'(body_to_lpda(_B, _Z, _Y, _Q, dyalog))
c_code local build_ref_430
	ret_reg &ref[430]
	call_c   build_ref_48()
	call_c   build_ref_429()
	call_c   Dyam_Create_Unary(&ref[48],&ref[429])
	move_ret ref[430]
	c_ret

;; TERM 429: body_to_lpda(_B, _Z, _Y, _Q, dyalog)
c_code local build_ref_429
	ret_reg &ref[429]
	call_c   build_ref_1319()
	call_c   build_ref_1289()
	call_c   Dyam_Term_Start(&ref[1319],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(25))
	call_c   Dyam_Term_Arg(V(24))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(&ref[1289])
	call_c   Dyam_Term_End()
	move_ret ref[429]
	c_ret

c_code local build_seed_111
	ret_reg &seed[111]
	call_c   build_ref_48()
	call_c   build_ref_424()
	call_c   Dyam_Seed_Start(&ref[48],&ref[424],I(0),fun3,1)
	call_c   build_ref_425()
	call_c   Dyam_Seed_Add_Comp(&ref[425],&ref[424],0)
	call_c   Dyam_Seed_End()
	move_ret seed[111]
	c_ret

;; TERM 425: '*GUARD*'(make_tag_subst_callret(_I, _N, _O, _U, _V)) :> '$$HOLE$$'
c_code local build_ref_425
	ret_reg &ref[425]
	call_c   build_ref_424()
	call_c   Dyam_Create_Binary(I(9),&ref[424],I(7))
	move_ret ref[425]
	c_ret

;; TERM 424: '*GUARD*'(make_tag_subst_callret(_I, _N, _O, _U, _V))
c_code local build_ref_424
	ret_reg &ref[424]
	call_c   build_ref_48()
	call_c   build_ref_423()
	call_c   Dyam_Create_Unary(&ref[48],&ref[423])
	move_ret ref[424]
	c_ret

;; TERM 423: make_tag_subst_callret(_I, _N, _O, _U, _V)
c_code local build_ref_423
	ret_reg &ref[423]
	call_c   build_ref_1323()
	call_c   Dyam_Term_Start(&ref[1323],5)
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_Arg(V(21))
	call_c   Dyam_Term_End()
	move_ret ref[423]
	c_ret

c_code local build_seed_100
	ret_reg &seed[100]
	call_c   build_ref_56()
	call_c   build_ref_359()
	call_c   Dyam_Seed_Start(&ref[56],&ref[359],&ref[359],fun3,1)
	call_c   build_ref_361()
	call_c   Dyam_Seed_Add_Comp(&ref[361],&ref[359],0)
	call_c   Dyam_Seed_End()
	move_ret seed[100]
	c_ret

;; TERM 361: '*FIRST*'('call_decompose_args/3'(_C)) :> '$$HOLE$$'
c_code local build_ref_361
	ret_reg &ref[361]
	call_c   build_ref_360()
	call_c   Dyam_Create_Binary(I(9),&ref[360],I(7))
	move_ret ref[361]
	c_ret

;; TERM 360: '*FIRST*'('call_decompose_args/3'(_C))
c_code local build_ref_360
	ret_reg &ref[360]
	call_c   build_ref_52()
	call_c   build_ref_358()
	call_c   Dyam_Create_Unary(&ref[52],&ref[358])
	move_ret ref[360]
	c_ret

;; TERM 358: 'call_decompose_args/3'(_C)
c_code local build_ref_358
	ret_reg &ref[358]
	call_c   build_ref_1232()
	call_c   Dyam_Create_Unary(&ref[1232],V(2))
	move_ret ref[358]
	c_ret

;; TERM 359: '*CITEM*'('call_decompose_args/3'(_C), 'call_decompose_args/3'(_C))
c_code local build_ref_359
	ret_reg &ref[359]
	call_c   build_ref_56()
	call_c   build_ref_358()
	call_c   Dyam_Create_Binary(&ref[56],&ref[358],&ref[358])
	move_ret ref[359]
	c_ret

c_code local build_seed_101
	ret_reg &seed[101]
	call_c   build_ref_186()
	call_c   build_ref_365()
	call_c   Dyam_Seed_Start(&ref[186],&ref[365],I(0),fun15,1)
	call_c   build_ref_363()
	call_c   Dyam_Seed_Add_Comp(&ref[363],fun128,0)
	call_c   Dyam_Seed_End()
	move_ret seed[101]
	c_ret

;; TERM 363: '*RITEM*'('call_decompose_args/3'(_C), return(_D, _E))
c_code local build_ref_363
	ret_reg &ref[363]
	call_c   build_ref_0()
	call_c   build_ref_358()
	call_c   build_ref_362()
	call_c   Dyam_Create_Binary(&ref[0],&ref[358],&ref[362])
	move_ret ref[363]
	c_ret

;; TERM 362: return(_D, _E)
c_code local build_ref_362
	ret_reg &ref[362]
	call_c   build_ref_1233()
	call_c   Dyam_Create_Binary(&ref[1233],V(3),V(4))
	move_ret ref[362]
	c_ret

pl_code local fun128
	call_c   build_ref_363()
	call_c   Dyam_Unify_Item(&ref[363])
	fail_ret
	pl_jump  Follow_Cont(V(1))

;; TERM 365: '*RITEM*'('call_decompose_args/3'(_C), return(_D, _E)) :> tag_compile(2, '$TUPPLE'(35075412198240))
c_code local build_ref_365
	ret_reg &ref[365]
	call_c   build_ref_363()
	call_c   build_ref_364()
	call_c   Dyam_Create_Binary(I(9),&ref[363],&ref[364])
	move_ret ref[365]
	c_ret

;; TERM 364: tag_compile(2, '$TUPPLE'(35075412198240))
c_code local build_ref_364
	ret_reg &ref[364]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,134217728)
	move_ret R(0)
	call_c   build_ref_1409()
	call_c   Dyam_Create_Binary(&ref[1409],N(2),R(0))
	move_ret ref[364]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_85
	ret_reg &seed[85]
	call_c   build_ref_48()
	call_c   build_ref_231()
	call_c   Dyam_Seed_Start(&ref[48],&ref[231],I(0),fun3,1)
	call_c   build_ref_232()
	call_c   Dyam_Seed_Add_Comp(&ref[232],&ref[231],0)
	call_c   Dyam_Seed_End()
	move_ret seed[85]
	c_ret

;; TERM 232: '*GUARD*'(toplevel(tag_term_expand(_C, _O))) :> '$$HOLE$$'
c_code local build_ref_232
	ret_reg &ref[232]
	call_c   build_ref_231()
	call_c   Dyam_Create_Binary(I(9),&ref[231],I(7))
	move_ret ref[232]
	c_ret

;; TERM 231: '*GUARD*'(toplevel(tag_term_expand(_C, _O)))
c_code local build_ref_231
	ret_reg &ref[231]
	call_c   build_ref_48()
	call_c   build_ref_230()
	call_c   Dyam_Create_Unary(&ref[48],&ref[230])
	move_ret ref[231]
	c_ret

;; TERM 230: toplevel(tag_term_expand(_C, _O))
c_code local build_ref_230
	ret_reg &ref[230]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1415()
	call_c   Dyam_Create_Binary(&ref[1415],V(2),V(14))
	move_ret R(0)
	call_c   build_ref_1414()
	call_c   Dyam_Create_Unary(&ref[1414],R(0))
	move_ret ref[230]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1414: toplevel
c_code local build_ref_1414
	ret_reg &ref[1414]
	call_c   Dyam_Create_Atom("toplevel")
	move_ret ref[1414]
	c_ret

;; TERM 1415: tag_term_expand
c_code local build_ref_1415
	ret_reg &ref[1415]
	call_c   Dyam_Create_Atom("tag_term_expand")
	move_ret ref[1415]
	c_ret

c_code local build_seed_86
	ret_reg &seed[86]
	call_c   build_ref_48()
	call_c   build_ref_234()
	call_c   Dyam_Seed_Start(&ref[48],&ref[234],I(0),fun3,1)
	call_c   build_ref_235()
	call_c   Dyam_Seed_Add_Comp(&ref[235],&ref[234],0)
	call_c   Dyam_Seed_End()
	move_ret seed[86]
	c_ret

;; TERM 235: '*GUARD*'(tag_il_body_to_lpda_handler(_B, _O, _D, _E, _F, _G, _H, _I, _J, _K, _L, _M, _N)) :> '$$HOLE$$'
c_code local build_ref_235
	ret_reg &ref[235]
	call_c   build_ref_234()
	call_c   Dyam_Create_Binary(I(9),&ref[234],I(7))
	move_ret ref[235]
	c_ret

;; TERM 234: '*GUARD*'(tag_il_body_to_lpda_handler(_B, _O, _D, _E, _F, _G, _H, _I, _J, _K, _L, _M, _N))
c_code local build_ref_234
	ret_reg &ref[234]
	call_c   build_ref_48()
	call_c   build_ref_233()
	call_c   Dyam_Create_Unary(&ref[48],&ref[233])
	move_ret ref[234]
	c_ret

;; TERM 233: tag_il_body_to_lpda_handler(_B, _O, _D, _E, _F, _G, _H, _I, _J, _K, _L, _M, _N)
c_code local build_ref_233
	ret_reg &ref[233]
	call_c   build_ref_1295()
	call_c   Dyam_Term_Start(&ref[1295],13)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_End()
	move_ret ref[233]
	c_ret

c_code local build_seed_93
	ret_reg &seed[93]
	call_c   build_ref_48()
	call_c   build_ref_276()
	call_c   Dyam_Seed_Start(&ref[48],&ref[276],I(0),fun3,1)
	call_c   build_ref_277()
	call_c   Dyam_Seed_Add_Comp(&ref[277],&ref[276],0)
	call_c   Dyam_Seed_End()
	move_ret seed[93]
	c_ret

;; TERM 277: '*GUARD*'(tag_compile_subtree(_B, _O, _Q, _B1, _A1, _E1, _K, _L, _N)) :> '$$HOLE$$'
c_code local build_ref_277
	ret_reg &ref[277]
	call_c   build_ref_276()
	call_c   Dyam_Create_Binary(I(9),&ref[276],I(7))
	move_ret ref[277]
	c_ret

;; TERM 276: '*GUARD*'(tag_compile_subtree(_B, _O, _Q, _B1, _A1, _E1, _K, _L, _N))
c_code local build_ref_276
	ret_reg &ref[276]
	call_c   build_ref_48()
	call_c   build_ref_275()
	call_c   Dyam_Create_Unary(&ref[48],&ref[275])
	move_ret ref[276]
	c_ret

;; TERM 275: tag_compile_subtree(_B, _O, _Q, _B1, _A1, _E1, _K, _L, _N)
c_code local build_ref_275
	ret_reg &ref[275]
	call_c   build_ref_1275()
	call_c   Dyam_Term_Start(&ref[1275],9)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(27))
	call_c   Dyam_Term_Arg(V(26))
	call_c   Dyam_Term_Arg(V(30))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_End()
	move_ret ref[275]
	c_ret

c_code local build_seed_89
	ret_reg &seed[89]
	call_c   build_ref_56()
	call_c   build_ref_248()
	call_c   Dyam_Seed_Start(&ref[56],&ref[248],&ref[248],fun3,1)
	call_c   build_ref_250()
	call_c   Dyam_Seed_Add_Comp(&ref[250],&ref[248],0)
	call_c   Dyam_Seed_End()
	move_ret seed[89]
	c_ret

;; TERM 250: '*FIRST*'('call_normalize/2'(_C)) :> '$$HOLE$$'
c_code local build_ref_250
	ret_reg &ref[250]
	call_c   build_ref_249()
	call_c   Dyam_Create_Binary(I(9),&ref[249],I(7))
	move_ret ref[250]
	c_ret

;; TERM 249: '*FIRST*'('call_normalize/2'(_C))
c_code local build_ref_249
	ret_reg &ref[249]
	call_c   build_ref_52()
	call_c   build_ref_247()
	call_c   Dyam_Create_Unary(&ref[52],&ref[247])
	move_ret ref[249]
	c_ret

;; TERM 247: 'call_normalize/2'(_C)
c_code local build_ref_247
	ret_reg &ref[247]
	call_c   build_ref_1416()
	call_c   Dyam_Create_Unary(&ref[1416],V(2))
	move_ret ref[247]
	c_ret

;; TERM 1416: 'call_normalize/2'
c_code local build_ref_1416
	ret_reg &ref[1416]
	call_c   Dyam_Create_Atom("call_normalize/2")
	move_ret ref[1416]
	c_ret

;; TERM 248: '*CITEM*'('call_normalize/2'(_C), 'call_normalize/2'(_C))
c_code local build_ref_248
	ret_reg &ref[248]
	call_c   build_ref_56()
	call_c   build_ref_247()
	call_c   Dyam_Create_Binary(&ref[56],&ref[247],&ref[247])
	move_ret ref[248]
	c_ret

c_code local build_seed_90
	ret_reg &seed[90]
	call_c   build_ref_186()
	call_c   build_ref_253()
	call_c   Dyam_Seed_Start(&ref[186],&ref[253],I(0),fun15,1)
	call_c   build_ref_251()
	call_c   Dyam_Seed_Add_Comp(&ref[251],fun72,0)
	call_c   Dyam_Seed_End()
	move_ret seed[90]
	c_ret

;; TERM 251: '*RITEM*'('call_normalize/2'(_C), return(_D))
c_code local build_ref_251
	ret_reg &ref[251]
	call_c   build_ref_0()
	call_c   build_ref_247()
	call_c   build_ref_196()
	call_c   Dyam_Create_Binary(&ref[0],&ref[247],&ref[196])
	move_ret ref[251]
	c_ret

pl_code local fun72
	call_c   build_ref_251()
	call_c   Dyam_Unify_Item(&ref[251])
	fail_ret
	pl_jump  Follow_Cont(V(1))

;; TERM 253: '*RITEM*'('call_normalize/2'(_C), return(_D)) :> tag_compile(1, '$TUPPLE'(35075412198240))
c_code local build_ref_253
	ret_reg &ref[253]
	call_c   build_ref_251()
	call_c   build_ref_252()
	call_c   Dyam_Create_Binary(I(9),&ref[251],&ref[252])
	move_ret ref[253]
	c_ret

;; TERM 252: tag_compile(1, '$TUPPLE'(35075412198240))
c_code local build_ref_252
	ret_reg &ref[252]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,134217728)
	move_ret R(0)
	call_c   build_ref_1409()
	call_c   Dyam_Create_Binary(&ref[1409],N(1),R(0))
	move_ret ref[252]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_88
	ret_reg &seed[88]
	call_c   build_ref_48()
	call_c   build_ref_242()
	call_c   Dyam_Seed_Start(&ref[48],&ref[242],I(0),fun3,1)
	call_c   build_ref_243()
	call_c   Dyam_Seed_Add_Comp(&ref[243],&ref[242],0)
	call_c   Dyam_Seed_End()
	move_ret seed[88]
	c_ret

;; TERM 243: '*GUARD*'(body_to_lpda(_B, (_C = _D), _E, _F, _G)) :> '$$HOLE$$'
c_code local build_ref_243
	ret_reg &ref[243]
	call_c   build_ref_242()
	call_c   Dyam_Create_Binary(I(9),&ref[242],I(7))
	move_ret ref[243]
	c_ret

;; TERM 242: '*GUARD*'(body_to_lpda(_B, (_C = _D), _E, _F, _G))
c_code local build_ref_242
	ret_reg &ref[242]
	call_c   build_ref_48()
	call_c   build_ref_241()
	call_c   Dyam_Create_Unary(&ref[48],&ref[241])
	move_ret ref[242]
	c_ret

;; TERM 241: body_to_lpda(_B, (_C = _D), _E, _F, _G)
c_code local build_ref_241
	ret_reg &ref[241]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1325()
	call_c   Dyam_Create_Binary(&ref[1325],V(2),V(3))
	move_ret R(0)
	call_c   build_ref_1319()
	call_c   Dyam_Term_Start(&ref[1319],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[241]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_87
	ret_reg &seed[87]
	call_c   build_ref_48()
	call_c   build_ref_239()
	call_c   Dyam_Seed_Start(&ref[48],&ref[239],I(0),fun3,1)
	call_c   build_ref_240()
	call_c   Dyam_Seed_Add_Comp(&ref[240],&ref[239],0)
	call_c   Dyam_Seed_End()
	move_ret seed[87]
	c_ret

;; TERM 240: '*GUARD*'(feature_args_unif_aux(_I, _J)) :> '$$HOLE$$'
c_code local build_ref_240
	ret_reg &ref[240]
	call_c   build_ref_239()
	call_c   Dyam_Create_Binary(I(9),&ref[239],I(7))
	move_ret ref[240]
	c_ret

;; TERM 239: '*GUARD*'(feature_args_unif_aux(_I, _J))
c_code local build_ref_239
	ret_reg &ref[239]
	call_c   build_ref_48()
	call_c   build_ref_238()
	call_c   Dyam_Create_Unary(&ref[48],&ref[238])
	move_ret ref[239]
	c_ret

;; TERM 238: feature_args_unif_aux(_I, _J)
c_code local build_ref_238
	ret_reg &ref[238]
	call_c   build_ref_1228()
	call_c   Dyam_Create_Binary(&ref[1228],V(8),V(9))
	move_ret ref[238]
	c_ret

c_code local build_seed_73
	ret_reg &seed[73]
	call_c   build_ref_56()
	call_c   build_ref_118()
	call_c   Dyam_Seed_Start(&ref[56],&ref[118],&ref[118],fun3,1)
	call_c   build_ref_120()
	call_c   Dyam_Seed_Add_Comp(&ref[120],&ref[118],0)
	call_c   Dyam_Seed_End()
	move_ret seed[73]
	c_ret

;; TERM 120: '*FIRST*'('call_core_info/3'(_C, _D)) :> '$$HOLE$$'
c_code local build_ref_120
	ret_reg &ref[120]
	call_c   build_ref_119()
	call_c   Dyam_Create_Binary(I(9),&ref[119],I(7))
	move_ret ref[120]
	c_ret

;; TERM 119: '*FIRST*'('call_core_info/3'(_C, _D))
c_code local build_ref_119
	ret_reg &ref[119]
	call_c   build_ref_52()
	call_c   build_ref_117()
	call_c   Dyam_Create_Unary(&ref[52],&ref[117])
	move_ret ref[119]
	c_ret

;; TERM 117: 'call_core_info/3'(_C, _D)
c_code local build_ref_117
	ret_reg &ref[117]
	call_c   build_ref_1240()
	call_c   Dyam_Create_Binary(&ref[1240],V(2),V(3))
	move_ret ref[117]
	c_ret

;; TERM 118: '*CITEM*'('call_core_info/3'(_C, _D), 'call_core_info/3'(_C, _D))
c_code local build_ref_118
	ret_reg &ref[118]
	call_c   build_ref_56()
	call_c   build_ref_117()
	call_c   Dyam_Create_Binary(&ref[56],&ref[117],&ref[117])
	move_ret ref[118]
	c_ret

c_code local build_seed_79
	ret_reg &seed[79]
	call_c   build_ref_186()
	call_c   build_ref_190()
	call_c   Dyam_Seed_Start(&ref[186],&ref[190],I(0),fun15,1)
	call_c   build_ref_188()
	call_c   Dyam_Seed_Add_Comp(&ref[188],fun47,0)
	call_c   Dyam_Seed_End()
	move_ret seed[79]
	c_ret

;; TERM 188: '*RITEM*'('call_core_info/3'(_C, _D), return(_E))
c_code local build_ref_188
	ret_reg &ref[188]
	call_c   build_ref_0()
	call_c   build_ref_117()
	call_c   build_ref_187()
	call_c   Dyam_Create_Binary(&ref[0],&ref[117],&ref[187])
	move_ret ref[188]
	c_ret

pl_code local fun47
	call_c   build_ref_188()
	call_c   Dyam_Unify_Item(&ref[188])
	fail_ret
	pl_jump  Follow_Cont(V(1))

;; TERM 190: '*RITEM*'('call_core_info/3'(_C, _D), return(_E)) :> tag_compile(0, '$TUPPLE'(35075412198240))
c_code local build_ref_190
	ret_reg &ref[190]
	call_c   build_ref_188()
	call_c   build_ref_189()
	call_c   Dyam_Create_Binary(I(9),&ref[188],&ref[189])
	move_ret ref[190]
	c_ret

;; TERM 189: tag_compile(0, '$TUPPLE'(35075412198240))
c_code local build_ref_189
	ret_reg &ref[189]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,134217728)
	move_ret R(0)
	call_c   build_ref_1409()
	call_c   Dyam_Create_Binary(&ref[1409],N(0),R(0))
	move_ret ref[189]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1: tag_il_args(_A, _B, _C)
c_code local build_ref_1
	ret_reg &ref[1]
	call_c   build_ref_1236()
	call_c   Dyam_Term_Start(&ref[1236],3)
	call_c   Dyam_Term_Arg(V(0))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_End()
	move_ret ref[1]
	c_ret

;; TERM 3: '*RITEM*'(tag_il_args(_A, _B, _C), voidret)
c_code local build_ref_3
	ret_reg &ref[3]
	call_c   build_ref_0()
	call_c   build_ref_1()
	call_c   build_ref_2()
	call_c   Dyam_Create_Binary(&ref[0],&ref[1],&ref[2])
	move_ret ref[3]
	c_ret

;; TERM 4: tag_il_args(_A, _B, _C, _D, _E, _F, _G, _H)
c_code local build_ref_4
	ret_reg &ref[4]
	call_c   build_ref_1236()
	call_c   Dyam_Term_Start(&ref[1236],8)
	call_c   Dyam_Term_Arg(V(0))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[4]
	c_ret

;; TERM 5: '*RITEM*'(tag_il_args(_A, _B, _C, _D, _E, _F, _G, _H), voidret)
c_code local build_ref_5
	ret_reg &ref[5]
	call_c   build_ref_0()
	call_c   build_ref_4()
	call_c   build_ref_2()
	call_c   Dyam_Create_Binary(&ref[0],&ref[4],&ref[2])
	move_ret ref[5]
	c_ret

;; TERM 9: decompose_args(_A, _B, _C)
c_code local build_ref_9
	ret_reg &ref[9]
	call_c   build_ref_1417()
	call_c   Dyam_Term_Start(&ref[1417],3)
	call_c   Dyam_Term_Arg(V(0))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_End()
	move_ret ref[9]
	c_ret

;; TERM 1417: decompose_args
c_code local build_ref_1417
	ret_reg &ref[1417]
	call_c   Dyam_Create_Atom("decompose_args")
	move_ret ref[1417]
	c_ret

;; TERM 8: '*RITEM*'('call_decompose_args/3'(_A), return(_B, _C))
c_code local build_ref_8
	ret_reg &ref[8]
	call_c   build_ref_0()
	call_c   build_ref_6()
	call_c   build_ref_7()
	call_c   Dyam_Create_Binary(&ref[0],&ref[6],&ref[7])
	move_ret ref[8]
	c_ret

;; TERM 7: return(_B, _C)
c_code local build_ref_7
	ret_reg &ref[7]
	call_c   build_ref_1233()
	call_c   Dyam_Create_Binary(&ref[1233],V(1),V(2))
	move_ret ref[7]
	c_ret

;; TERM 6: 'call_decompose_args/3'(_A)
c_code local build_ref_6
	ret_reg &ref[6]
	call_c   build_ref_1232()
	call_c   Dyam_Create_Unary(&ref[1232],V(0))
	move_ret ref[6]
	c_ret

;; TERM 13: core_info(_A, _B, _C)
c_code local build_ref_13
	ret_reg &ref[13]
	call_c   build_ref_1418()
	call_c   Dyam_Term_Start(&ref[1418],3)
	call_c   Dyam_Term_Arg(V(0))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_End()
	move_ret ref[13]
	c_ret

;; TERM 1418: core_info
c_code local build_ref_1418
	ret_reg &ref[1418]
	call_c   Dyam_Create_Atom("core_info")
	move_ret ref[1418]
	c_ret

;; TERM 12: '*RITEM*'('call_core_info/3'(_A, _B), return(_C))
c_code local build_ref_12
	ret_reg &ref[12]
	call_c   build_ref_0()
	call_c   build_ref_10()
	call_c   build_ref_11()
	call_c   Dyam_Create_Binary(&ref[0],&ref[10],&ref[11])
	move_ret ref[12]
	c_ret

;; TERM 10: 'call_core_info/3'(_A, _B)
c_code local build_ref_10
	ret_reg &ref[10]
	call_c   build_ref_1240()
	call_c   Dyam_Create_Binary(&ref[1240],V(0),V(1))
	move_ret ref[10]
	c_ret

;; TERM 19: normalize(_A, _B)
c_code local build_ref_19
	ret_reg &ref[19]
	call_c   build_ref_1419()
	call_c   Dyam_Create_Binary(&ref[1419],V(0),V(1))
	move_ret ref[19]
	c_ret

;; TERM 1419: normalize
c_code local build_ref_1419
	ret_reg &ref[1419]
	call_c   Dyam_Create_Atom("normalize")
	move_ret ref[1419]
	c_ret

;; TERM 18: '*RITEM*'('call_normalize/2'(_A), return(_B))
c_code local build_ref_18
	ret_reg &ref[18]
	call_c   build_ref_0()
	call_c   build_ref_16()
	call_c   build_ref_17()
	call_c   Dyam_Create_Binary(&ref[0],&ref[16],&ref[17])
	move_ret ref[18]
	c_ret

;; TERM 17: return(_B)
c_code local build_ref_17
	ret_reg &ref[17]
	call_c   build_ref_1233()
	call_c   Dyam_Create_Unary(&ref[1233],V(1))
	move_ret ref[17]
	c_ret

;; TERM 16: 'call_normalize/2'(_A)
c_code local build_ref_16
	ret_reg &ref[16]
	call_c   build_ref_1416()
	call_c   Dyam_Create_Unary(&ref[1416],V(0))
	move_ret ref[16]
	c_ret

;; TERM 20: wrapping_predicate(_A)
c_code local build_ref_20
	ret_reg &ref[20]
	call_c   build_ref_1314()
	call_c   Dyam_Create_Unary(&ref[1314],V(0))
	move_ret ref[20]
	c_ret

;; TERM 21: '*RITEM*'(wrapping_predicate(_A), voidret)
c_code local build_ref_21
	ret_reg &ref[21]
	call_c   build_ref_0()
	call_c   build_ref_20()
	call_c   build_ref_2()
	call_c   Dyam_Create_Binary(&ref[0],&ref[20],&ref[2])
	move_ret ref[21]
	c_ret

;; TERM 24: tag_custom_subst(_A, _B)
c_code local build_ref_24
	ret_reg &ref[24]
	call_c   build_ref_1238()
	call_c   Dyam_Create_Binary(&ref[1238],V(0),V(1))
	move_ret ref[24]
	c_ret

;; TERM 23: '*RITEM*'('call_tag_custom_subst/2'(_A), return(_B))
c_code local build_ref_23
	ret_reg &ref[23]
	call_c   build_ref_0()
	call_c   build_ref_22()
	call_c   build_ref_17()
	call_c   Dyam_Create_Binary(&ref[0],&ref[22],&ref[17])
	move_ret ref[23]
	c_ret

;; TERM 22: 'call_tag_custom_subst/2'(_A)
c_code local build_ref_22
	ret_reg &ref[22]
	call_c   build_ref_1237()
	call_c   Dyam_Create_Unary(&ref[1237],V(0))
	move_ret ref[22]
	c_ret

;; TERM 25: verbose_tag(_A)
c_code local build_ref_25
	ret_reg &ref[25]
	call_c   build_ref_138()
	call_c   Dyam_Create_Unary(&ref[138],V(0))
	move_ret ref[25]
	c_ret

;; TERM 26: '*RITEM*'(verbose_tag(_A), voidret)
c_code local build_ref_26
	ret_reg &ref[26]
	call_c   build_ref_0()
	call_c   build_ref_25()
	call_c   build_ref_2()
	call_c   Dyam_Create_Binary(&ref[0],&ref[25],&ref[2])
	move_ret ref[26]
	c_ret

;; TERM 30: apply_feature_mode_constraints(_A, _B, _C, _D)
c_code local build_ref_30
	ret_reg &ref[30]
	call_c   build_ref_1420()
	call_c   Dyam_Term_Start(&ref[1420],4)
	call_c   Dyam_Term_Arg(V(0))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[30]
	c_ret

;; TERM 1420: apply_feature_mode_constraints
c_code local build_ref_1420
	ret_reg &ref[1420]
	call_c   Dyam_Create_Atom("apply_feature_mode_constraints")
	move_ret ref[1420]
	c_ret

;; TERM 29: '*RITEM*'('call_apply_feature_mode_constraints/4'(_A, _B), return(_C, _D))
c_code local build_ref_29
	ret_reg &ref[29]
	call_c   build_ref_0()
	call_c   build_ref_27()
	call_c   build_ref_28()
	call_c   Dyam_Create_Binary(&ref[0],&ref[27],&ref[28])
	move_ret ref[29]
	c_ret

;; TERM 28: return(_C, _D)
c_code local build_ref_28
	ret_reg &ref[28]
	call_c   build_ref_1233()
	call_c   Dyam_Create_Binary(&ref[1233],V(2),V(3))
	move_ret ref[28]
	c_ret

;; TERM 27: 'call_apply_feature_mode_constraints/4'(_A, _B)
c_code local build_ref_27
	ret_reg &ref[27]
	call_c   build_ref_1410()
	call_c   Dyam_Create_Binary(&ref[1410],V(0),V(1))
	move_ret ref[27]
	c_ret

;; TERM 33: tagguide_name(_A, _B, _C)
c_code local build_ref_33
	ret_reg &ref[33]
	call_c   build_ref_1421()
	call_c   Dyam_Term_Start(&ref[1421],3)
	call_c   Dyam_Term_Arg(V(0))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_End()
	move_ret ref[33]
	c_ret

;; TERM 1421: tagguide_name
c_code local build_ref_1421
	ret_reg &ref[1421]
	call_c   Dyam_Create_Atom("tagguide_name")
	move_ret ref[1421]
	c_ret

;; TERM 32: '*RITEM*'('call_tagguide_name/3'(_A, _B), return(_C))
c_code local build_ref_32
	ret_reg &ref[32]
	call_c   build_ref_0()
	call_c   build_ref_31()
	call_c   build_ref_11()
	call_c   Dyam_Create_Binary(&ref[0],&ref[31],&ref[11])
	move_ret ref[32]
	c_ret

;; TERM 31: 'call_tagguide_name/3'(_A, _B)
c_code local build_ref_31
	ret_reg &ref[31]
	call_c   build_ref_1412()
	call_c   Dyam_Create_Binary(&ref[1412],V(0),V(1))
	move_ret ref[31]
	c_ret

;; TERM 36: tagguide_pred(_A, _B)
c_code local build_ref_36
	ret_reg &ref[36]
	call_c   build_ref_1422()
	call_c   Dyam_Create_Binary(&ref[1422],V(0),V(1))
	move_ret ref[36]
	c_ret

;; TERM 1422: tagguide_pred
c_code local build_ref_1422
	ret_reg &ref[1422]
	call_c   Dyam_Create_Atom("tagguide_pred")
	move_ret ref[1422]
	c_ret

;; TERM 35: '*RITEM*'('call_tagguide_pred/2'(_A), return(_B))
c_code local build_ref_35
	ret_reg &ref[35]
	call_c   build_ref_0()
	call_c   build_ref_34()
	call_c   build_ref_17()
	call_c   Dyam_Create_Binary(&ref[0],&ref[34],&ref[17])
	move_ret ref[35]
	c_ret

;; TERM 34: 'call_tagguide_pred/2'(_A)
c_code local build_ref_34
	ret_reg &ref[34]
	call_c   build_ref_1413()
	call_c   Dyam_Create_Unary(&ref[1413],V(0))
	move_ret ref[34]
	c_ret

;; TERM 38: '*RITEM*'(format_require, voidret)
c_code local build_ref_38
	ret_reg &ref[38]
	call_c   build_ref_0()
	call_c   build_ref_37()
	call_c   build_ref_2()
	call_c   Dyam_Create_Binary(&ref[0],&ref[37],&ref[2])
	move_ret ref[38]
	c_ret

;; TERM 41: register_predicate(_A)
c_code local build_ref_41
	ret_reg &ref[41]
	call_c   build_ref_1254()
	call_c   Dyam_Create_Unary(&ref[1254],V(0))
	move_ret ref[41]
	c_ret

;; TERM 42: '*RITEM*'(register_predicate(_A), voidret)
c_code local build_ref_42
	ret_reg &ref[42]
	call_c   build_ref_0()
	call_c   build_ref_41()
	call_c   build_ref_2()
	call_c   Dyam_Create_Binary(&ref[0],&ref[41],&ref[2])
	move_ret ref[42]
	c_ret

;; TERM 1198: _J , _L
c_code local build_ref_1198
	ret_reg &ref[1198]
	call_c   Dyam_Create_Binary(I(4),V(9),V(11))
	move_ret ref[1198]
	c_ret

;; TERM 1200: term_range(_P, _Q, _F) , term_range(_P, _Q, _N) , term_range(_P, _Q, _O) , _J , _L
c_code local build_ref_1200
	ret_reg &ref[1200]
	call_c   Dyam_Pseudo_Choice(3)
	call_c   build_ref_1423()
	call_c   Dyam_Term_Start(&ref[1423],3)
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   Dyam_Term_Start(&ref[1423],3)
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_End()
	move_ret R(1)
	call_c   Dyam_Term_Start(&ref[1423],3)
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_End()
	move_ret R(2)
	call_c   build_ref_1198()
	call_c   Dyam_Create_Binary(I(4),R(2),&ref[1198])
	move_ret R(2)
	call_c   Dyam_Create_Binary(I(4),R(1),R(2))
	move_ret R(2)
	call_c   Dyam_Create_Binary(I(4),R(0),R(2))
	move_ret ref[1200]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1423: term_range
c_code local build_ref_1423
	ret_reg &ref[1423]
	call_c   Dyam_Create_Atom("term_range")
	move_ret ref[1423]
	c_ret

;; TERM 1199: _H ## _I
c_code local build_ref_1199
	ret_reg &ref[1199]
	call_c   build_ref_1382()
	call_c   Dyam_Create_Binary(&ref[1382],V(7),V(8))
	move_ret ref[1199]
	c_ret

;; TERM 1201: _J ; _L
c_code local build_ref_1201
	ret_reg &ref[1201]
	call_c   Dyam_Create_Binary(I(5),V(9),V(11))
	move_ret ref[1201]
	c_ret

;; TERM 1202: tag_node{id=> _R, label=> [_S|_T], children=> _U, kind=> scan, adj=> _V, spine=> _W, top=> _X, bot=> _Y, token=> _Z, adjleft=> _A1, adjright=> _B1, adjwrap=> _C1}
c_code local build_ref_1202
	ret_reg &ref[1202]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(18),V(19))
	move_ret R(0)
	call_c   build_ref_1259()
	call_c   build_ref_1312()
	call_c   Dyam_Term_Start(&ref[1259],12)
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_Arg(&ref[1312])
	call_c   Dyam_Term_Arg(V(21))
	call_c   Dyam_Term_Arg(V(22))
	call_c   Dyam_Term_Arg(V(23))
	call_c   Dyam_Term_Arg(V(24))
	call_c   Dyam_Term_Arg(V(25))
	call_c   Dyam_Term_Arg(V(26))
	call_c   Dyam_Term_Arg(V(27))
	call_c   Dyam_Term_Arg(V(28))
	call_c   Dyam_Term_End()
	move_ret ref[1202]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1203: phrase('$noskip'([_S|_T]), _F, _D1) , term_range(_D1, 1000, _G)
c_code local build_ref_1203
	ret_reg &ref[1203]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   Dyam_Create_List(V(18),V(19))
	move_ret R(0)
	call_c   build_ref_1425()
	call_c   Dyam_Create_Unary(&ref[1425],R(0))
	move_ret R(0)
	call_c   build_ref_1424()
	call_c   Dyam_Term_Start(&ref[1424],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(29))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_1423()
	call_c   Dyam_Term_Start(&ref[1423],3)
	call_c   Dyam_Term_Arg(V(29))
	call_c   Dyam_Term_Arg(N(1000))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret R(1)
	call_c   Dyam_Create_Binary(I(4),R(0),R(1))
	move_ret ref[1203]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1424: phrase
c_code local build_ref_1424
	ret_reg &ref[1424]
	call_c   Dyam_Create_Atom("phrase")
	move_ret ref[1424]
	c_ret

;; TERM 1425: '$noskip'
c_code local build_ref_1425
	ret_reg &ref[1425]
	call_c   Dyam_Create_Atom("$noskip")
	move_ret ref[1425]
	c_ret

;; TERM 1204: tag_node{id=> _E1, label=> noop(expect(scan, [_S|_T])), children=> _F1, kind=> escape, adj=> _G1, spine=> _H1, top=> _I1, bot=> _J1, token=> _K1, adjleft=> _L1, adjright=> _M1, adjwrap=> _N1}
c_code local build_ref_1204
	ret_reg &ref[1204]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(18),V(19))
	move_ret R(0)
	call_c   build_ref_1426()
	call_c   build_ref_1312()
	call_c   Dyam_Create_Binary(&ref[1426],&ref[1312],R(0))
	move_ret R(0)
	call_c   build_ref_274()
	call_c   Dyam_Create_Unary(&ref[274],R(0))
	move_ret R(0)
	call_c   build_ref_1259()
	call_c   build_ref_1356()
	call_c   Dyam_Term_Start(&ref[1259],12)
	call_c   Dyam_Term_Arg(V(30))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(31))
	call_c   Dyam_Term_Arg(&ref[1356])
	call_c   Dyam_Term_Arg(V(32))
	call_c   Dyam_Term_Arg(V(33))
	call_c   Dyam_Term_Arg(V(34))
	call_c   Dyam_Term_Arg(V(35))
	call_c   Dyam_Term_Arg(V(36))
	call_c   Dyam_Term_Arg(V(37))
	call_c   Dyam_Term_Arg(V(38))
	call_c   Dyam_Term_Arg(V(39))
	call_c   Dyam_Term_End()
	move_ret ref[1204]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1426: expect
c_code local build_ref_1426
	ret_reg &ref[1426]
	call_c   Dyam_Create_Atom("expect")
	move_ret ref[1426]
	c_ret

;; TERM 1207: tag_family_load(_Z1, _P1, _T1, _V1, _B, _F, _D1) , term_range(_D1, 1000, _G)
c_code local build_ref_1207
	ret_reg &ref[1207]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_1427()
	call_c   Dyam_Term_Start(&ref[1427],7)
	call_c   Dyam_Term_Arg(V(51))
	call_c   Dyam_Term_Arg(V(41))
	call_c   Dyam_Term_Arg(V(45))
	call_c   Dyam_Term_Arg(V(47))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(29))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_1423()
	call_c   Dyam_Term_Start(&ref[1423],3)
	call_c   Dyam_Term_Arg(V(29))
	call_c   Dyam_Term_Arg(N(1000))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret R(1)
	call_c   Dyam_Create_Binary(I(4),R(0),R(1))
	move_ret ref[1207]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1427: tag_family_load
c_code local build_ref_1427
	ret_reg &ref[1427]
	call_c   Dyam_Create_Atom("tag_family_load")
	move_ret ref[1427]
	c_ret

;; TERM 1206: tag_anchor{family=> _Z1, coanchors=> _A2, equations=> _B2}
c_code local build_ref_1206
	ret_reg &ref[1206]
	call_c   build_ref_1241()
	call_c   Dyam_Term_Start(&ref[1241],3)
	call_c   Dyam_Term_Arg(V(51))
	call_c   Dyam_Term_Arg(V(52))
	call_c   Dyam_Term_Arg(V(53))
	call_c   Dyam_Term_End()
	move_ret ref[1206]
	c_ret

;; TERM 1205: tag_node{id=> _O1, label=> _P1, children=> _Q1, kind=> anchor, adj=> _R1, spine=> _S1, top=> _T1, bot=> _U1, token=> _V1, adjleft=> _W1, adjright=> _X1, adjwrap=> _Y1}
c_code local build_ref_1205
	ret_reg &ref[1205]
	call_c   build_ref_1259()
	call_c   build_ref_379()
	call_c   Dyam_Term_Start(&ref[1259],12)
	call_c   Dyam_Term_Arg(V(40))
	call_c   Dyam_Term_Arg(V(41))
	call_c   Dyam_Term_Arg(V(42))
	call_c   Dyam_Term_Arg(&ref[379])
	call_c   Dyam_Term_Arg(V(43))
	call_c   Dyam_Term_Arg(V(44))
	call_c   Dyam_Term_Arg(V(45))
	call_c   Dyam_Term_Arg(V(46))
	call_c   Dyam_Term_Arg(V(47))
	call_c   Dyam_Term_Arg(V(48))
	call_c   Dyam_Term_Arg(V(49))
	call_c   Dyam_Term_Arg(V(50))
	call_c   Dyam_Term_End()
	move_ret ref[1205]
	c_ret

;; TERM 1209: tag_check_coanchor(_P1, _T1, _V1, _F, _D1, _B) , term_range(_D1, 1000, _G)
c_code local build_ref_1209
	ret_reg &ref[1209]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_1428()
	call_c   Dyam_Term_Start(&ref[1428],6)
	call_c   Dyam_Term_Arg(V(41))
	call_c   Dyam_Term_Arg(V(45))
	call_c   Dyam_Term_Arg(V(47))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(29))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_1423()
	call_c   Dyam_Term_Start(&ref[1423],3)
	call_c   Dyam_Term_Arg(V(29))
	call_c   Dyam_Term_Arg(N(1000))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret R(1)
	call_c   Dyam_Create_Binary(I(4),R(0),R(1))
	move_ret ref[1209]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1428: tag_check_coanchor
c_code local build_ref_1428
	ret_reg &ref[1428]
	call_c   Dyam_Create_Atom("tag_check_coanchor")
	move_ret ref[1428]
	c_ret

;; TERM 1208: tag_node{id=> _C2, label=> _P1, children=> _D2, kind=> coanchor, adj=> _E2, spine=> _F2, top=> _T1, bot=> _G2, token=> _V1, adjleft=> _H2, adjright=> _I2, adjwrap=> _J2}
c_code local build_ref_1208
	ret_reg &ref[1208]
	call_c   build_ref_1259()
	call_c   build_ref_1114()
	call_c   Dyam_Term_Start(&ref[1259],12)
	call_c   Dyam_Term_Arg(V(54))
	call_c   Dyam_Term_Arg(V(41))
	call_c   Dyam_Term_Arg(V(55))
	call_c   Dyam_Term_Arg(&ref[1114])
	call_c   Dyam_Term_Arg(V(56))
	call_c   Dyam_Term_Arg(V(57))
	call_c   Dyam_Term_Arg(V(45))
	call_c   Dyam_Term_Arg(V(58))
	call_c   Dyam_Term_Arg(V(47))
	call_c   Dyam_Term_Arg(V(59))
	call_c   Dyam_Term_Arg(V(60))
	call_c   Dyam_Term_Arg(V(61))
	call_c   Dyam_Term_End()
	move_ret ref[1208]
	c_ret

;; TERM 1212: tig(_B, _S2)
c_code local build_ref_1212
	ret_reg &ref[1212]
	call_c   build_ref_1343()
	call_c   Dyam_Create_Binary(&ref[1343],V(1),V(70))
	move_ret ref[1212]
	c_ret

;; TERM 1213: tag_check_foot(_P1, _T1, _F, _D1, _B, _S2) , term_range(_D1, 1000, _G)
c_code local build_ref_1213
	ret_reg &ref[1213]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_1429()
	call_c   Dyam_Term_Start(&ref[1429],6)
	call_c   Dyam_Term_Arg(V(41))
	call_c   Dyam_Term_Arg(V(45))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(29))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(70))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_1423()
	call_c   Dyam_Term_Start(&ref[1423],3)
	call_c   Dyam_Term_Arg(V(29))
	call_c   Dyam_Term_Arg(N(1000))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret R(1)
	call_c   Dyam_Create_Binary(I(4),R(0),R(1))
	move_ret ref[1213]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1429: tag_check_foot
c_code local build_ref_1429
	ret_reg &ref[1429]
	call_c   Dyam_Create_Atom("tag_check_foot")
	move_ret ref[1429]
	c_ret

;; TERM 1211: tag_terminal_cat(_P1)
c_code local build_ref_1211
	ret_reg &ref[1211]
	call_c   build_ref_1248()
	call_c   Dyam_Create_Unary(&ref[1248],V(41))
	move_ret ref[1211]
	c_ret

;; TERM 1210: tag_node{id=> _K2, label=> _P1, children=> _L2, kind=> foot, adj=> _M2, spine=> _N2, top=> _T1, bot=> _O2, token=> _V1, adjleft=> _P2, adjright=> _Q2, adjwrap=> _R2}
c_code local build_ref_1210
	ret_reg &ref[1210]
	call_c   build_ref_1259()
	call_c   build_ref_1155()
	call_c   Dyam_Term_Start(&ref[1259],12)
	call_c   Dyam_Term_Arg(V(62))
	call_c   Dyam_Term_Arg(V(41))
	call_c   Dyam_Term_Arg(V(63))
	call_c   Dyam_Term_Arg(&ref[1155])
	call_c   Dyam_Term_Arg(V(64))
	call_c   Dyam_Term_Arg(V(65))
	call_c   Dyam_Term_Arg(V(45))
	call_c   Dyam_Term_Arg(V(66))
	call_c   Dyam_Term_Arg(V(47))
	call_c   Dyam_Term_Arg(V(67))
	call_c   Dyam_Term_Arg(V(68))
	call_c   Dyam_Term_Arg(V(69))
	call_c   Dyam_Term_End()
	move_ret ref[1210]
	c_ret

;; TERM 1216: tag_check_subst(_P1, _B3, _F, _D1, _B) , term_range(_D1, 1000, _G)
c_code local build_ref_1216
	ret_reg &ref[1216]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_1430()
	call_c   Dyam_Term_Start(&ref[1430],5)
	call_c   Dyam_Term_Arg(V(41))
	call_c   Dyam_Term_Arg(V(79))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(29))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_1423()
	call_c   Dyam_Term_Start(&ref[1423],3)
	call_c   Dyam_Term_Arg(V(29))
	call_c   Dyam_Term_Arg(N(1000))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret R(1)
	call_c   Dyam_Create_Binary(I(4),R(0),R(1))
	move_ret ref[1216]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1430: tag_check_subst
c_code local build_ref_1430
	ret_reg &ref[1430]
	call_c   Dyam_Create_Atom("tag_check_subst")
	move_ret ref[1430]
	c_ret

;; TERM 1215: tag_subst_cat(_P1, _B3)
c_code local build_ref_1215
	ret_reg &ref[1215]
	call_c   build_ref_1250()
	call_c   Dyam_Create_Binary(&ref[1250],V(41),V(79))
	move_ret ref[1215]
	c_ret

;; TERM 1214: tag_node{id=> _T2, label=> _P1, children=> _U2, kind=> subst, adj=> _V2, spine=> _W2, top=> _T1, bot=> _X2, token=> _V1, adjleft=> _Y2, adjright=> _Z2, adjwrap=> _A3}
c_code local build_ref_1214
	ret_reg &ref[1214]
	call_c   build_ref_1259()
	call_c   build_ref_284()
	call_c   Dyam_Term_Start(&ref[1259],12)
	call_c   Dyam_Term_Arg(V(71))
	call_c   Dyam_Term_Arg(V(41))
	call_c   Dyam_Term_Arg(V(72))
	call_c   Dyam_Term_Arg(&ref[284])
	call_c   Dyam_Term_Arg(V(73))
	call_c   Dyam_Term_Arg(V(74))
	call_c   Dyam_Term_Arg(V(45))
	call_c   Dyam_Term_Arg(V(75))
	call_c   Dyam_Term_Arg(V(47))
	call_c   Dyam_Term_Arg(V(76))
	call_c   Dyam_Term_Arg(V(77))
	call_c   Dyam_Term_Arg(V(78))
	call_c   Dyam_Term_End()
	move_ret ref[1214]
	c_ret

;; TERM 1220: tag_autoload_adj(_D3, _T1, _J3, _F, _G, _N, _D1) , _J
c_code local build_ref_1220
	ret_reg &ref[1220]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1431()
	call_c   Dyam_Term_Start(&ref[1431],7)
	call_c   Dyam_Term_Arg(V(81))
	call_c   Dyam_Term_Arg(V(45))
	call_c   Dyam_Term_Arg(V(87))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(29))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   Dyam_Create_Binary(I(4),R(0),V(9))
	move_ret ref[1220]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1431: tag_autoload_adj
c_code local build_ref_1431
	ret_reg &ref[1431]
	call_c   Dyam_Create_Atom("tag_autoload_adj")
	move_ret ref[1431]
	c_ret

;; TERM 1218: '$VAR'(_O3, ['$SET$',adj(no, yes, strict, atmostone, one, sync),20])
c_code local build_ref_1218
	ret_reg &ref[1218]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1265()
	call_c   build_ref_397()
	call_c   build_ref_161()
	call_c   build_ref_1385()
	call_c   build_ref_1386()
	call_c   build_ref_1387()
	call_c   build_ref_1388()
	call_c   Dyam_Term_Start(&ref[1265],6)
	call_c   Dyam_Term_Arg(&ref[397])
	call_c   Dyam_Term_Arg(&ref[161])
	call_c   Dyam_Term_Arg(&ref[1385])
	call_c   Dyam_Term_Arg(&ref[1386])
	call_c   Dyam_Term_Arg(&ref[1387])
	call_c   Dyam_Term_Arg(&ref[1388])
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   Dyam_Term_Start(I(8),3)
	call_c   Dyam_Term_Arg(V(92))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(N(20))
	call_c   Dyam_Term_End()
	move_ret ref[1218]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1219: [_E3|_F3]
c_code local build_ref_1219
	ret_reg &ref[1219]
	call_c   Dyam_Create_List(V(82),V(83))
	move_ret ref[1219]
	c_ret

;; TERM 1217: tag_node{id=> _C3, label=> _D3, children=> [_E3|_F3], kind=> _G3, adj=> _H3, spine=> _I3, top=> _T1, bot=> _J3, token=> _K3, adjleft=> _L3, adjright=> _M3, adjwrap=> _N3}
c_code local build_ref_1217
	ret_reg &ref[1217]
	call_c   build_ref_1259()
	call_c   build_ref_1219()
	call_c   Dyam_Term_Start(&ref[1259],12)
	call_c   Dyam_Term_Arg(V(80))
	call_c   Dyam_Term_Arg(V(81))
	call_c   Dyam_Term_Arg(&ref[1219])
	call_c   Dyam_Term_Arg(V(84))
	call_c   Dyam_Term_Arg(V(85))
	call_c   Dyam_Term_Arg(V(86))
	call_c   Dyam_Term_Arg(V(45))
	call_c   Dyam_Term_Arg(V(87))
	call_c   Dyam_Term_Arg(V(88))
	call_c   Dyam_Term_Arg(V(89))
	call_c   Dyam_Term_Arg(V(90))
	call_c   Dyam_Term_Arg(V(91))
	call_c   Dyam_Term_End()
	move_ret ref[1217]
	c_ret

;; TERM 1222: _J , _R3
c_code local build_ref_1222
	ret_reg &ref[1222]
	call_c   Dyam_Create_Binary(I(4),V(9),V(95))
	move_ret ref[1222]
	c_ret

;; TERM 1221: guard{goal=> _H, plus=> _P3, minus=> fail}
c_code local build_ref_1221
	ret_reg &ref[1221]
	call_c   build_ref_1308()
	call_c   build_ref_330()
	call_c   Dyam_Term_Start(&ref[1308],3)
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(93))
	call_c   Dyam_Term_Arg(&ref[330])
	call_c   Dyam_Term_End()
	move_ret ref[1221]
	c_ret

;; TERM 1225: _F = _G
c_code local build_ref_1225
	ret_reg &ref[1225]
	call_c   build_ref_1325()
	call_c   Dyam_Create_Binary(&ref[1325],V(5),V(6))
	move_ret ref[1225]
	c_ret

;; TERM 1031: _J , _I
c_code local build_ref_1031
	ret_reg &ref[1031]
	call_c   Dyam_Create_Binary(I(4),V(9),V(8))
	move_ret ref[1031]
	c_ret

;; TERM 1033: _J ; _I
c_code local build_ref_1033
	ret_reg &ref[1033]
	call_c   Dyam_Create_Binary(I(5),V(9),V(8))
	move_ret ref[1033]
	c_ret

;; TERM 1034: \+ \+ _J xor \+ \+ _I
c_code local build_ref_1034
	ret_reg &ref[1034]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_1411()
	call_c   Dyam_Create_Unary(&ref[1411],V(9))
	move_ret R(0)
	call_c   Dyam_Create_Unary(&ref[1411],R(0))
	move_ret R(0)
	call_c   Dyam_Create_Unary(&ref[1411],V(8))
	move_ret R(1)
	call_c   Dyam_Create_Unary(&ref[1411],R(1))
	move_ret R(1)
	call_c   build_ref_1432()
	call_c   Dyam_Create_Binary(&ref[1432],R(0),R(1))
	move_ret ref[1034]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1432: xor
c_code local build_ref_1432
	ret_reg &ref[1432]
	call_c   Dyam_Create_Atom("xor")
	move_ret ref[1432]
	c_ret

;; TERM 1035: \+ \+ _I xor \+ \+ _J
c_code local build_ref_1035
	ret_reg &ref[1035]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_1411()
	call_c   Dyam_Create_Unary(&ref[1411],V(8))
	move_ret R(0)
	call_c   Dyam_Create_Unary(&ref[1411],R(0))
	move_ret R(0)
	call_c   Dyam_Create_Unary(&ref[1411],V(9))
	move_ret R(1)
	call_c   Dyam_Create_Unary(&ref[1411],R(1))
	move_ret R(1)
	call_c   build_ref_1432()
	call_c   Dyam_Create_Binary(&ref[1432],R(0),R(1))
	move_ret ref[1035]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1032: _D + 1
c_code local build_ref_1032
	ret_reg &ref[1032]
	call_c   build_ref_1369()
	call_c   Dyam_Create_Binary(&ref[1369],V(3),N(1))
	move_ret ref[1032]
	c_ret

;; TERM 1036: _N = _O
c_code local build_ref_1036
	ret_reg &ref[1036]
	call_c   build_ref_1325()
	call_c   Dyam_Create_Binary(&ref[1325],V(13),V(14))
	move_ret ref[1036]
	c_ret

;; TERM 1037: local_var(_O, _P)
c_code local build_ref_1037
	ret_reg &ref[1037]
	call_c   build_ref_1226()
	call_c   Dyam_Create_Binary(&ref[1226],V(14),V(15))
	move_ret ref[1037]
	c_ret

;; TERM 1038: local_var(_N, _Q)
c_code local build_ref_1038
	ret_reg &ref[1038]
	call_c   build_ref_1226()
	call_c   Dyam_Create_Binary(&ref[1226],V(13),V(16))
	move_ret ref[1038]
	c_ret

;; TERM 1039: _N == _O
c_code local build_ref_1039
	ret_reg &ref[1039]
	call_c   build_ref_1433()
	call_c   Dyam_Create_Binary(&ref[1433],V(13),V(14))
	move_ret ref[1039]
	c_ret

;; TERM 1433: ==
c_code local build_ref_1433
	ret_reg &ref[1433]
	call_c   Dyam_Create_Atom("==")
	move_ret ref[1433]
	c_ret

;; TERM 997: tag_node{id=> _N, label=> _O, children=> _P, kind=> foot, adj=> _Q, spine=> _R, top=> _S, bot=> _T, token=> _U, adjleft=> _V, adjright=> _W, adjwrap=> _X}
c_code local build_ref_997
	ret_reg &ref[997]
	call_c   build_ref_1259()
	call_c   build_ref_1155()
	call_c   Dyam_Term_Start(&ref[1259],12)
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(&ref[1155])
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_Arg(V(21))
	call_c   Dyam_Term_Arg(V(22))
	call_c   Dyam_Term_Arg(V(23))
	call_c   Dyam_Term_End()
	move_ret ref[997]
	c_ret

;; TERM 998: tag_node{id=> _Y, label=> _Z, children=> _A1, kind=> _B1, adj=> _C1, spine=> _D1, top=> _E1, bot=> _F1, token=> _G1, adjleft=> _H1, adjright=> _I1, adjwrap=> _J1}
c_code local build_ref_998
	ret_reg &ref[998]
	call_c   build_ref_1259()
	call_c   Dyam_Term_Start(&ref[1259],12)
	call_c   Dyam_Term_Arg(V(24))
	call_c   Dyam_Term_Arg(V(25))
	call_c   Dyam_Term_Arg(V(26))
	call_c   Dyam_Term_Arg(V(27))
	call_c   Dyam_Term_Arg(V(28))
	call_c   Dyam_Term_Arg(V(29))
	call_c   Dyam_Term_Arg(V(30))
	call_c   Dyam_Term_Arg(V(31))
	call_c   Dyam_Term_Arg(V(32))
	call_c   Dyam_Term_Arg(V(33))
	call_c   Dyam_Term_Arg(V(34))
	call_c   Dyam_Term_Arg(V(35))
	call_c   Dyam_Term_End()
	move_ret ref[998]
	c_ret

;; TERM 999: guard{goal=> _K1, plus=> _L1, minus=> _M1}
c_code local build_ref_999
	ret_reg &ref[999]
	call_c   build_ref_1308()
	call_c   Dyam_Term_Start(&ref[1308],3)
	call_c   Dyam_Term_Arg(V(36))
	call_c   Dyam_Term_Arg(V(37))
	call_c   Dyam_Term_Arg(V(38))
	call_c   Dyam_Term_End()
	move_ret ref[999]
	c_ret

;; TERM 991: @*{goal=> _D, vars=> _F, from=> _G, to=> _H, collect_first=> _I, collect_last=> _J, collect_loop=> _K, collect_next=> _L, collect_pred=> _M}
c_code local build_ref_991
	ret_reg &ref[991]
	call_c   build_ref_1297()
	call_c   Dyam_Term_Start(&ref[1297],9)
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_End()
	move_ret ref[991]
	c_ret

;; TERM 992: '$answers'(_D)
c_code local build_ref_992
	ret_reg &ref[992]
	call_c   build_ref_1276()
	call_c   Dyam_Create_Unary(&ref[1276],V(3))
	move_ret ref[992]
	c_ret

;; TERM 993: guard{goal=> _D, plus=> _N, minus=> _O}
c_code local build_ref_993
	ret_reg &ref[993]
	call_c   build_ref_1308()
	call_c   Dyam_Term_Start(&ref[1308],3)
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_End()
	move_ret ref[993]
	c_ret

;; TERM 995: [scan,escape]
c_code local build_ref_995
	ret_reg &ref[995]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1356()
	call_c   Dyam_Create_List(&ref[1356],I(0))
	move_ret R(0)
	call_c   build_ref_1312()
	call_c   Dyam_Create_List(&ref[1312],R(0))
	move_ret ref[995]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 996: strip(_B1)
c_code local build_ref_996
	ret_reg &ref[996]
	call_c   build_ref_1249()
	call_c   Dyam_Create_Unary(&ref[1249],V(27))
	move_ret ref[996]
	c_ret

;; TERM 994: tag_node{id=> _P, label=> _Q, children=> _R, kind=> _S, adj=> _T, spine=> _U, top=> _V, bot=> _W, token=> _X, adjleft=> _Y, adjright=> _Z, adjwrap=> _A1}
c_code local build_ref_994
	ret_reg &ref[994]
	call_c   build_ref_1259()
	call_c   Dyam_Term_Start(&ref[1259],12)
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_Arg(V(21))
	call_c   Dyam_Term_Arg(V(22))
	call_c   Dyam_Term_Arg(V(23))
	call_c   Dyam_Term_Arg(V(24))
	call_c   Dyam_Term_Arg(V(25))
	call_c   Dyam_Term_Arg(V(26))
	call_c   Dyam_Term_End()
	move_ret ref[994]
	c_ret

;; TERM 898: _H , _I
c_code local build_ref_898
	ret_reg &ref[898]
	call_c   Dyam_Create_Binary(I(4),V(7),V(8))
	move_ret ref[898]
	c_ret

;; TERM 900: _G + 1
c_code local build_ref_900
	ret_reg &ref[900]
	call_c   build_ref_1369()
	call_c   Dyam_Create_Binary(&ref[1369],V(6),N(1))
	move_ret ref[900]
	c_ret

;; TERM 899: _H ; _I
c_code local build_ref_899
	ret_reg &ref[899]
	call_c   Dyam_Create_Binary(I(5),V(7),V(8))
	move_ret ref[899]
	c_ret

;; TERM 783: max(_F, _G)
c_code local build_ref_783
	ret_reg &ref[783]
	call_c   build_ref_1434()
	call_c   Dyam_Create_Binary(&ref[1434],V(5),V(6))
	move_ret ref[783]
	c_ret

;; TERM 1434: max
c_code local build_ref_1434
	ret_reg &ref[1434]
	call_c   Dyam_Create_Atom("max")
	move_ret ref[1434]
	c_ret

;; TERM 784: 1 + max(_F, _G)
c_code local build_ref_784
	ret_reg &ref[784]
	call_c   build_ref_1369()
	call_c   build_ref_783()
	call_c   Dyam_Create_Binary(&ref[1369],N(1),&ref[783])
	move_ret ref[784]
	c_ret

;; TERM 785: _D xor _E
c_code local build_ref_785
	ret_reg &ref[785]
	call_c   build_ref_1432()
	call_c   Dyam_Create_Binary(&ref[1432],V(3),V(4))
	move_ret ref[785]
	c_ret

;; TERM 786: \+ \+ _D
c_code local build_ref_786
	ret_reg &ref[786]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1411()
	call_c   Dyam_Create_Unary(&ref[1411],V(3))
	move_ret R(0)
	call_c   Dyam_Create_Unary(&ref[1411],R(0))
	move_ret ref[786]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 779: _D ## _E
c_code local build_ref_779
	ret_reg &ref[779]
	call_c   build_ref_1382()
	call_c   Dyam_Create_Binary(&ref[1382],V(3),V(4))
	move_ret ref[779]
	c_ret

;; TERM 780: tag_node{id=> _F, label=> _G, children=> [_H|_I], kind=> _J, adj=> _K, spine=> _L, top=> _M, bot=> _N, token=> _O, adjleft=> _P, adjright=> _Q, adjwrap=> _R}
c_code local build_ref_780
	ret_reg &ref[780]
	call_c   build_ref_1259()
	call_c   build_ref_236()
	call_c   Dyam_Term_Start(&ref[1259],12)
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(&ref[236])
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_End()
	move_ret ref[780]
	c_ret

;; TERM 782: tag_node{id=> _U, label=> _V, children=> _W, kind=> anchor, adj=> _X, spine=> _Y, top=> _Z, bot=> _A1, token=> _B1, adjleft=> _C1, adjright=> _D1, adjwrap=> _E1}
c_code local build_ref_782
	ret_reg &ref[782]
	call_c   build_ref_1259()
	call_c   build_ref_379()
	call_c   Dyam_Term_Start(&ref[1259],12)
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_Arg(V(21))
	call_c   Dyam_Term_Arg(V(22))
	call_c   Dyam_Term_Arg(&ref[379])
	call_c   Dyam_Term_Arg(V(23))
	call_c   Dyam_Term_Arg(V(24))
	call_c   Dyam_Term_Arg(V(25))
	call_c   Dyam_Term_Arg(V(26))
	call_c   Dyam_Term_Arg(V(27))
	call_c   Dyam_Term_Arg(V(28))
	call_c   Dyam_Term_Arg(V(29))
	call_c   Dyam_Term_Arg(V(30))
	call_c   Dyam_Term_End()
	move_ret ref[782]
	c_ret

;; TERM 781: guard{goal=> _D, plus=> _S, minus=> _T}
c_code local build_ref_781
	ret_reg &ref[781]
	call_c   build_ref_1308()
	call_c   Dyam_Term_Start(&ref[1308],3)
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_End()
	move_ret ref[781]
	c_ret

;; TERM 776: 'warning: guard is a var ~w\n'
c_code local build_ref_776
	ret_reg &ref[776]
	call_c   Dyam_Create_Atom("warning: guard is a var ~w\n")
	move_ret ref[776]
	c_ret

;; TERM 777: _G , _F
c_code local build_ref_777
	ret_reg &ref[777]
	call_c   Dyam_Create_Binary(I(4),V(6),V(5))
	move_ret ref[777]
	c_ret

;; TERM 760: numbervars(_C, _E)
c_code local build_ref_760
	ret_reg &ref[760]
	call_c   build_ref_1435()
	call_c   Dyam_Create_Binary(&ref[1435],V(2),V(4))
	move_ret ref[760]
	c_ret

;; TERM 1435: numbervars
c_code local build_ref_1435
	ret_reg &ref[1435]
	call_c   Dyam_Create_Atom("numbervars")
	move_ret ref[1435]
	c_ret

;; TERM 696: _D , _E , _F
c_code local build_ref_696
	ret_reg &ref[696]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Binary(I(4),V(4),V(5))
	move_ret R(0)
	call_c   Dyam_Create_Binary(I(4),V(3),R(0))
	move_ret ref[696]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 695: (_D , _E) , _F
c_code local build_ref_695
	ret_reg &ref[695]
	call_c   build_ref_341()
	call_c   Dyam_Create_Binary(I(4),&ref[341],V(5))
	move_ret ref[695]
	c_ret

;; TERM 341: _D , _E
c_code local build_ref_341
	ret_reg &ref[341]
	call_c   Dyam_Create_Binary(I(4),V(3),V(4))
	move_ret ref[341]
	c_ret

;; TERM 698: _G = _J
c_code local build_ref_698
	ret_reg &ref[698]
	call_c   build_ref_1325()
	call_c   Dyam_Create_Binary(&ref[1325],V(6),V(9))
	move_ret ref[698]
	c_ret

;; TERM 697: _G = _H , _I = _J
c_code local build_ref_697
	ret_reg &ref[697]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_1325()
	call_c   Dyam_Create_Binary(&ref[1325],V(6),V(7))
	move_ret R(0)
	call_c   Dyam_Create_Binary(&ref[1325],V(8),V(9))
	move_ret R(1)
	call_c   Dyam_Create_Binary(I(4),R(0),R(1))
	move_ret ref[697]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 700: _G = _J , _D
c_code local build_ref_700
	ret_reg &ref[700]
	call_c   build_ref_698()
	call_c   Dyam_Create_Binary(I(4),&ref[698],V(3))
	move_ret ref[700]
	c_ret

;; TERM 699: _G = _H , _I = _J , _D
c_code local build_ref_699
	ret_reg &ref[699]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_1325()
	call_c   Dyam_Create_Binary(&ref[1325],V(6),V(7))
	move_ret R(0)
	call_c   Dyam_Create_Binary(&ref[1325],V(8),V(9))
	move_ret R(1)
	call_c   Dyam_Create_Binary(I(4),R(1),V(3))
	move_ret R(1)
	call_c   Dyam_Create_Binary(I(4),R(0),R(1))
	move_ret ref[699]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 628: tag_il_args(_M, _C, _D, _E, _F, _G, _H, _P)
c_code local build_ref_628
	ret_reg &ref[628]
	call_c   build_ref_1236()
	call_c   Dyam_Term_Start(&ref[1236],8)
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_End()
	move_ret ref[628]
	c_ret

;; TERM 627: '$CLOSURE'('$fun'(228, 0, 1129384860), '$TUPPLE'(35075411420452))
c_code local build_ref_627
	ret_reg &ref[627]
	call_c   build_ref_626()
	call_c   Dyam_Closure_Aux(fun228,&ref[626])
	move_ret ref[627]
	c_ret

;; TERM 621: il_choice
c_code local build_ref_621
	ret_reg &ref[621]
	call_c   Dyam_Create_Atom("il_choice")
	move_ret ref[621]
	c_ret

c_code local build_seed_142
	ret_reg &seed[142]
	call_c   build_ref_48()
	call_c   build_ref_623()
	call_c   Dyam_Seed_Start(&ref[48],&ref[623],I(0),fun3,1)
	call_c   build_ref_624()
	call_c   Dyam_Seed_Add_Comp(&ref[624],&ref[623],0)
	call_c   Dyam_Seed_End()
	move_ret seed[142]
	c_ret

;; TERM 624: '*GUARD*'(body_to_lpda(_B, ((_C == _E -> _C = _D , interleave_choose_first(_Q, _I, _M, _N, _F, _H) ; interleave_choose_alt(_Q, _I, _M, _N, _F)) , true), '*IL-LOOP*'(_N, _P), _J, _K)) :> '$$HOLE$$'
c_code local build_ref_624
	ret_reg &ref[624]
	call_c   build_ref_623()
	call_c   Dyam_Create_Binary(I(9),&ref[623],I(7))
	move_ret ref[624]
	c_ret

;; TERM 623: '*GUARD*'(body_to_lpda(_B, ((_C == _E -> _C = _D , interleave_choose_first(_Q, _I, _M, _N, _F, _H) ; interleave_choose_alt(_Q, _I, _M, _N, _F)) , true), '*IL-LOOP*'(_N, _P), _J, _K))
c_code local build_ref_623
	ret_reg &ref[623]
	call_c   build_ref_48()
	call_c   build_ref_622()
	call_c   Dyam_Create_Unary(&ref[48],&ref[622])
	move_ret ref[623]
	c_ret

;; TERM 622: body_to_lpda(_B, ((_C == _E -> _C = _D , interleave_choose_first(_Q, _I, _M, _N, _F, _H) ; interleave_choose_alt(_Q, _I, _M, _N, _F)) , true), '*IL-LOOP*'(_N, _P), _J, _K)
c_code local build_ref_622
	ret_reg &ref[622]
	call_c   Dyam_Pseudo_Choice(3)
	call_c   build_ref_1433()
	call_c   Dyam_Create_Binary(&ref[1433],V(2),V(4))
	move_ret R(0)
	call_c   build_ref_1325()
	call_c   Dyam_Create_Binary(&ref[1325],V(2),V(3))
	move_ret R(1)
	call_c   build_ref_1436()
	call_c   Dyam_Term_Start(&ref[1436],6)
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret R(2)
	call_c   Dyam_Create_Binary(I(4),R(1),R(2))
	move_ret R(2)
	call_c   build_ref_1292()
	call_c   Dyam_Create_Binary(&ref[1292],R(0),R(2))
	move_ret R(2)
	call_c   build_ref_1437()
	call_c   Dyam_Term_Start(&ref[1437],5)
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   Dyam_Create_Binary(I(5),R(2),R(0))
	move_ret R(0)
	call_c   build_ref_261()
	call_c   Dyam_Create_Binary(I(4),R(0),&ref[261])
	move_ret R(0)
	call_c   build_ref_1438()
	call_c   Dyam_Create_Binary(&ref[1438],V(13),V(15))
	move_ret R(2)
	call_c   build_ref_1319()
	call_c   Dyam_Term_Start(&ref[1319],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(R(2))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_End()
	move_ret ref[622]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1438: '*IL-LOOP*'
c_code local build_ref_1438
	ret_reg &ref[1438]
	call_c   Dyam_Create_Atom("*IL-LOOP*")
	move_ret ref[1438]
	c_ret

;; TERM 1437: interleave_choose_alt
c_code local build_ref_1437
	ret_reg &ref[1437]
	call_c   Dyam_Create_Atom("interleave_choose_alt")
	move_ret ref[1437]
	c_ret

;; TERM 1436: interleave_choose_first
c_code local build_ref_1436
	ret_reg &ref[1436]
	call_c   Dyam_Create_Atom("interleave_choose_first")
	move_ret ref[1436]
	c_ret

long local pool_fun228[3]=[2,build_ref_621,build_seed_142]

pl_code local fun228
	call_c   Dyam_Pool(pool_fun228)
	call_c   Dyam_Allocate(0)
	move     &ref[621], R(0)
	move     0, R(1)
	move     V(16), R(2)
	move     S(5), R(3)
	pl_call  pred_update_counter_2()
	pl_call  fun28(&seed[142],1)
	call_c   Dyam_Deallocate()
	pl_ret

;; TERM 626: '$TUPPLE'(35075411420452)
c_code local build_ref_626
	ret_reg &ref[626]
	call_c   Dyam_Create_Simple_Tupple(0,264085504)
	move_ret ref[626]
	c_ret

;; TERM 620: [_M,_N]
c_code local build_ref_620
	ret_reg &ref[620]
	call_c   Dyam_Create_Tupple(12,13,I(0))
	move_ret ref[620]
	c_ret

;; TERM 629: '*IL-LOOP-WRAP*'(_J)
c_code local build_ref_629
	ret_reg &ref[629]
	call_c   build_ref_1439()
	call_c   Dyam_Create_Unary(&ref[1439],V(9))
	move_ret ref[629]
	c_ret

;; TERM 1439: '*IL-LOOP-WRAP*'
c_code local build_ref_1439
	ret_reg &ref[1439]
	call_c   Dyam_Create_Atom("*IL-LOOP-WRAP*")
	move_ret ref[1439]
	c_ret

;; TERM 607: tag_il_args(_J, _D, _E, _F, _G, _H, _I, _O)
c_code local build_ref_607
	ret_reg &ref[607]
	call_c   build_ref_1236()
	call_c   Dyam_Term_Start(&ref[1236],8)
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_End()
	move_ret ref[607]
	c_ret

;; TERM 606: '$CLOSURE'('$fun'(222, 0, 1129322348), '$TUPPLE'(35075411420088))
c_code local build_ref_606
	ret_reg &ref[606]
	call_c   build_ref_605()
	call_c   Dyam_Closure_Aux(fun222,&ref[605])
	move_ret ref[606]
	c_ret

c_code local build_seed_140
	ret_reg &seed[140]
	call_c   build_ref_48()
	call_c   build_ref_596()
	call_c   Dyam_Seed_Start(&ref[48],&ref[596],I(0),fun3,1)
	call_c   build_ref_597()
	call_c   Dyam_Seed_Add_Comp(&ref[597],&ref[596],0)
	call_c   Dyam_Seed_End()
	move_ret seed[140]
	c_ret

;; TERM 597: '*GUARD*'(body_to_lpda(_B, interleave_start(_S, _N, _K, _L), _M, _P, _Q)) :> '$$HOLE$$'
c_code local build_ref_597
	ret_reg &ref[597]
	call_c   build_ref_596()
	call_c   Dyam_Create_Binary(I(9),&ref[596],I(7))
	move_ret ref[597]
	c_ret

;; TERM 596: '*GUARD*'(body_to_lpda(_B, interleave_start(_S, _N, _K, _L), _M, _P, _Q))
c_code local build_ref_596
	ret_reg &ref[596]
	call_c   build_ref_48()
	call_c   build_ref_595()
	call_c   Dyam_Create_Unary(&ref[48],&ref[595])
	move_ret ref[596]
	c_ret

;; TERM 595: body_to_lpda(_B, interleave_start(_S, _N, _K, _L), _M, _P, _Q)
c_code local build_ref_595
	ret_reg &ref[595]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1440()
	call_c   Dyam_Term_Start(&ref[1440],4)
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_1319()
	call_c   Dyam_Term_Start(&ref[1319],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_End()
	move_ret ref[595]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1440: interleave_start
c_code local build_ref_1440
	ret_reg &ref[1440]
	call_c   Dyam_Create_Atom("interleave_start")
	move_ret ref[1440]
	c_ret

long local pool_fun222[2]=[1,build_seed_140]

pl_code local fun222
	call_c   Dyam_Pool(pool_fun222)
	call_c   Dyam_Allocate(0)
	pl_call  fun28(&seed[140],1)
	call_c   Dyam_Deallocate()
	pl_ret

;; TERM 605: '$TUPPLE'(35075411420088)
c_code local build_ref_605
	ret_reg &ref[605]
	call_c   Dyam_Create_Simple_Tupple(0,134722560)
	move_ret ref[605]
	c_ret

;; TERM 609: '*IL-ALT*'(_N, _C, _O, []) :> _P
c_code local build_ref_609
	ret_reg &ref[609]
	call_c   build_ref_608()
	call_c   Dyam_Create_Binary(I(9),&ref[608],V(15))
	move_ret ref[609]
	c_ret

;; TERM 608: '*IL-ALT*'(_N, _C, _O, [])
c_code local build_ref_608
	ret_reg &ref[608]
	call_c   build_ref_1441()
	call_c   Dyam_Term_Start(&ref[1441],4)
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_End()
	move_ret ref[608]
	c_ret

;; TERM 1441: '*IL-ALT*'
c_code local build_ref_1441
	ret_reg &ref[1441]
	call_c   Dyam_Create_Atom("*IL-ALT*")
	move_ret ref[1441]
	c_ret

;; TERM 601: tag_il_args(_J, _D, _E, _F, _G, _H, _I, _P)
c_code local build_ref_601
	ret_reg &ref[601]
	call_c   build_ref_1236()
	call_c   Dyam_Term_Start(&ref[1236],8)
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_End()
	move_ret ref[601]
	c_ret

;; TERM 600: '$CLOSURE'('$fun'(220, 0, 1129304556), '$TUPPLE'(35075411419968))
c_code local build_ref_600
	ret_reg &ref[600]
	call_c   build_ref_599()
	call_c   Dyam_Closure_Aux(fun220,&ref[599])
	move_ret ref[600]
	c_ret

c_code local build_seed_139
	ret_reg &seed[139]
	call_c   build_ref_48()
	call_c   build_ref_593()
	call_c   Dyam_Seed_Start(&ref[48],&ref[593],I(0),fun3,1)
	call_c   build_ref_594()
	call_c   Dyam_Seed_Add_Comp(&ref[594],&ref[593],0)
	call_c   Dyam_Seed_End()
	move_ret seed[139]
	c_ret

;; TERM 594: '*GUARD*'(body_to_lpda(_B, interleave_register(_T, _O, _K, _L), _M, _Q, _R)) :> '$$HOLE$$'
c_code local build_ref_594
	ret_reg &ref[594]
	call_c   build_ref_593()
	call_c   Dyam_Create_Binary(I(9),&ref[593],I(7))
	move_ret ref[594]
	c_ret

;; TERM 593: '*GUARD*'(body_to_lpda(_B, interleave_register(_T, _O, _K, _L), _M, _Q, _R))
c_code local build_ref_593
	ret_reg &ref[593]
	call_c   build_ref_48()
	call_c   build_ref_592()
	call_c   Dyam_Create_Unary(&ref[48],&ref[592])
	move_ret ref[593]
	c_ret

;; TERM 592: body_to_lpda(_B, interleave_register(_T, _O, _K, _L), _M, _Q, _R)
c_code local build_ref_592
	ret_reg &ref[592]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1442()
	call_c   Dyam_Term_Start(&ref[1442],4)
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_1319()
	call_c   Dyam_Term_Start(&ref[1319],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_End()
	move_ret ref[592]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1442: interleave_register
c_code local build_ref_1442
	ret_reg &ref[1442]
	call_c   Dyam_Create_Atom("interleave_register")
	move_ret ref[1442]
	c_ret

long local pool_fun220[2]=[1,build_seed_139]

pl_code local fun220
	call_c   Dyam_Pool(pool_fun220)
	call_c   Dyam_Allocate(0)
	pl_call  fun28(&seed[139],1)
	call_c   Dyam_Deallocate()
	pl_ret

;; TERM 599: '$TUPPLE'(35075411419968)
c_code local build_ref_599
	ret_reg &ref[599]
	call_c   Dyam_Create_Simple_Tupple(0,134699520)
	move_ret ref[599]
	c_ret

;; TERM 603: '*IL-ALT*'(_O, _C, _P, _N) :> _Q
c_code local build_ref_603
	ret_reg &ref[603]
	call_c   build_ref_602()
	call_c   Dyam_Create_Binary(I(9),&ref[602],V(16))
	move_ret ref[603]
	c_ret

;; TERM 602: '*IL-ALT*'(_O, _C, _P, _N)
c_code local build_ref_602
	ret_reg &ref[602]
	call_c   build_ref_1441()
	call_c   Dyam_Term_Start(&ref[1441],4)
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_End()
	move_ret ref[602]
	c_ret

;; TERM 545: _G , _H
c_code local build_ref_545
	ret_reg &ref[545]
	call_c   Dyam_Create_Binary(I(4),V(6),V(7))
	move_ret ref[545]
	c_ret

;; TERM 551: '*NOOP*'(_K) :> _J
c_code local build_ref_551
	ret_reg &ref[551]
	call_c   build_ref_550()
	call_c   Dyam_Create_Binary(I(9),&ref[550],V(9))
	move_ret ref[551]
	c_ret

;; TERM 550: '*NOOP*'(_K)
c_code local build_ref_550
	ret_reg &ref[550]
	call_c   build_ref_1321()
	call_c   Dyam_Create_Unary(&ref[1321],V(10))
	move_ret ref[550]
	c_ret

;; TERM 546: _G ; _H
c_code local build_ref_546
	ret_reg &ref[546]
	call_c   Dyam_Create_Binary(I(5),V(6),V(7))
	move_ret ref[546]
	c_ret

;; TERM 435: [tag_anchor{family=> _B, coanchors=> _C, equations=> _D}]
c_code local build_ref_435
	ret_reg &ref[435]
	call_c   build_ref_150()
	call_c   Dyam_Create_List(&ref[150],I(0))
	move_ret ref[435]
	c_ret

;; TERM 150: tag_anchor{family=> _B, coanchors=> _C, equations=> _D}
c_code local build_ref_150
	ret_reg &ref[150]
	call_c   build_ref_1241()
	call_c   Dyam_Term_Start(&ref[1241],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[150]
	c_ret

;; TERM 434: 'tag_lemma: unexpected variable as family ~w'
c_code local build_ref_434
	ret_reg &ref[434]
	call_c   Dyam_Create_Atom("tag_lemma: unexpected variable as family ~w")
	move_ret ref[434]
	c_ret

;; TERM 436: tag_anchor{family=> _B, coanchors=> _F, equations=> _G}
c_code local build_ref_436
	ret_reg &ref[436]
	call_c   build_ref_1241()
	call_c   Dyam_Term_Start(&ref[1241],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[436]
	c_ret

;; TERM 437: normalized_tag_lemma(_H, _I, _B, _F, _G) :- _J
c_code local build_ref_437
	ret_reg &ref[437]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1354()
	call_c   Dyam_Term_Start(&ref[1354],5)
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_1353()
	call_c   Dyam_Create_Binary(&ref[1353],R(0),V(9))
	move_ret ref[437]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 444: 'tag_lemma: reference to missing anchor model ~w'
c_code local build_ref_444
	ret_reg &ref[444]
	call_c   Dyam_Create_Atom("tag_lemma: reference to missing anchor model ~w")
	move_ret ref[444]
	c_ret

;; TERM 445: tag_anchor{family=> _K, coanchors=> _C, equations=> _D}
c_code local build_ref_445
	ret_reg &ref[445]
	call_c   build_ref_1241()
	call_c   Dyam_Term_Start(&ref[1241],3)
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[445]
	c_ret

;; TERM 427: '*SA-SUBST-LIGHT*'(_U, _V, _P, _C)
c_code local build_ref_427
	ret_reg &ref[427]
	call_c   build_ref_1350()
	call_c   Dyam_Term_Start(&ref[1350],4)
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_Arg(V(21))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_End()
	move_ret ref[427]
	c_ret

;; TERM 426: light_tabular tag((_W / _X))
c_code local build_ref_426
	ret_reg &ref[426]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1294()
	call_c   Dyam_Create_Binary(&ref[1294],V(22),V(23))
	move_ret R(0)
	call_c   build_ref_1255()
	call_c   Dyam_Create_Unary(&ref[1255],R(0))
	move_ret R(0)
	call_c   build_ref_1338()
	call_c   Dyam_Create_Unary(&ref[1338],R(0))
	move_ret ref[426]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 428: tagfilter_cat((_B ^ _D ^ subst ^ _N ^ (_C , _I , []) ^ _I ^ _Z))
c_code local build_ref_428
	ret_reg &ref[428]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   Dyam_Create_Binary(I(4),V(8),I(0))
	move_ret R(0)
	call_c   Dyam_Create_Binary(I(4),V(2),R(0))
	move_ret R(0)
	call_c   build_ref_1307()
	call_c   Dyam_Create_Binary(&ref[1307],V(8),V(25))
	move_ret R(1)
	call_c   Dyam_Create_Binary(&ref[1307],R(0),R(1))
	move_ret R(1)
	call_c   Dyam_Create_Binary(&ref[1307],V(13),R(1))
	move_ret R(1)
	call_c   build_ref_284()
	call_c   Dyam_Create_Binary(&ref[1307],&ref[284],R(1))
	move_ret R(1)
	call_c   Dyam_Create_Binary(&ref[1307],V(3),R(1))
	move_ret R(1)
	call_c   Dyam_Create_Binary(&ref[1307],V(1),R(1))
	move_ret R(1)
	call_c   build_ref_1349()
	call_c   Dyam_Create_Unary(&ref[1349],R(1))
	move_ret ref[428]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 432: '*SA-SUBST*'(_U, _V, _P, _C)
c_code local build_ref_432
	ret_reg &ref[432]
	call_c   build_ref_1347()
	call_c   Dyam_Term_Start(&ref[1347],4)
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_Arg(V(21))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_End()
	move_ret ref[432]
	c_ret

;; TERM 433: tag_node{id=> _C, label=> _D, children=> _E, kind=> subst, adj=> _F, spine=> _G, top=> _H, bot=> _I, token=> _J, adjleft=> _K, adjright=> _L, adjwrap=> _M}
c_code local build_ref_433
	ret_reg &ref[433]
	call_c   build_ref_1259()
	call_c   build_ref_284()
	call_c   Dyam_Term_Start(&ref[1259],12)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(&ref[284])
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_End()
	move_ret ref[433]
	c_ret

;; TERM 342: _F + 1
c_code local build_ref_342
	ret_reg &ref[342]
	call_c   build_ref_1369()
	call_c   Dyam_Create_Binary(&ref[1369],V(5),N(1))
	move_ret ref[342]
	c_ret

;; TERM 343: _F + _G + 1
c_code local build_ref_343
	ret_reg &ref[343]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1369()
	call_c   Dyam_Create_Binary(&ref[1369],V(5),V(6))
	move_ret R(0)
	call_c   Dyam_Create_Binary(&ref[1369],R(0),N(1))
	move_ret ref[343]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 281: tag_node{id=> _F, label=> _G, children=> _H, kind=> _I, adj=> _J, spine=> _K, top=> _L, bot=> _M, token=> _N, adjleft=> _O, adjright=> _P, adjwrap=> _Q}
c_code local build_ref_281
	ret_reg &ref[281]
	call_c   build_ref_1259()
	call_c   Dyam_Term_Start(&ref[1259],12)
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_End()
	move_ret ref[281]
	c_ret

;; TERM 293: tig(_B, _W)
c_code local build_ref_293
	ret_reg &ref[293]
	call_c   build_ref_1343()
	call_c   Dyam_Create_Binary(&ref[1343],V(1),V(22))
	move_ret ref[293]
	c_ret

;; TERM 287: _C1 , _B1
c_code local build_ref_287
	ret_reg &ref[287]
	call_c   Dyam_Create_Binary(I(4),V(28),V(27))
	move_ret ref[287]
	c_ret

;; TERM 286: record(lctag(_Y, _Z, _B, _A1))
c_code local build_ref_286
	ret_reg &ref[286]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1331()
	call_c   Dyam_Term_Start(&ref[1331],4)
	call_c   Dyam_Term_Arg(V(24))
	call_c   Dyam_Term_Arg(V(25))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(26))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_1443()
	call_c   Dyam_Create_Unary(&ref[1443],R(0))
	move_ret ref[286]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1443: record
c_code local build_ref_1443
	ret_reg &ref[1443]
	call_c   Dyam_Create_Atom("record")
	move_ret ref[1443]
	c_ret

;; TERM 285: lctag(_B, _Y, _Z, _A1)
c_code local build_ref_285
	ret_reg &ref[285]
	call_c   build_ref_1331()
	call_c   Dyam_Term_Start(&ref[1331],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(24))
	call_c   Dyam_Term_Arg(V(25))
	call_c   Dyam_Term_Arg(V(26))
	call_c   Dyam_Term_End()
	move_ret ref[285]
	c_ret

;; TERM 289: lctag(_B, _G1, _H1, _I1)
c_code local build_ref_289
	ret_reg &ref[289]
	call_c   build_ref_1331()
	call_c   Dyam_Term_Start(&ref[1331],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(32))
	call_c   Dyam_Term_Arg(V(33))
	call_c   Dyam_Term_Arg(V(34))
	call_c   Dyam_Term_End()
	move_ret ref[289]
	c_ret

;; TERM 290: record(lctag_ok(_B)) , _F1
c_code local build_ref_290
	ret_reg &ref[290]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1444()
	call_c   Dyam_Create_Unary(&ref[1444],V(1))
	move_ret R(0)
	call_c   build_ref_1443()
	call_c   Dyam_Create_Unary(&ref[1443],R(0))
	move_ret R(0)
	call_c   Dyam_Create_Binary(I(4),R(0),V(31))
	move_ret ref[290]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1444: lctag_ok
c_code local build_ref_1444
	ret_reg &ref[1444]
	call_c   Dyam_Create_Atom("lctag_ok")
	move_ret ref[1444]
	c_ret

;; TERM 291: \+ (_T -> fail ; true)
c_code local build_ref_291
	ret_reg &ref[291]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1292()
	call_c   build_ref_330()
	call_c   Dyam_Create_Binary(&ref[1292],V(19),&ref[330])
	move_ret R(0)
	call_c   build_ref_261()
	call_c   Dyam_Create_Binary(I(5),R(0),&ref[261])
	move_ret R(0)
	call_c   build_ref_1411()
	call_c   Dyam_Create_Unary(&ref[1411],R(0))
	move_ret ref[291]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 292: \+ (_T -> fail ; true) -> _J1 ; fail
c_code local build_ref_292
	ret_reg &ref[292]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1292()
	call_c   build_ref_291()
	call_c   Dyam_Create_Binary(&ref[1292],&ref[291],V(35))
	move_ret R(0)
	call_c   build_ref_330()
	call_c   Dyam_Create_Binary(I(5),R(0),&ref[330])
	move_ret ref[292]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 288: record(loaded_tree(_B)) , record(lctag_map(_G, _W, _B)) , record(lctag_map_reverse(_B, _G, _W)) , _E1
c_code local build_ref_288
	ret_reg &ref[288]
	call_c   Dyam_Pseudo_Choice(3)
	call_c   build_ref_1445()
	call_c   Dyam_Create_Unary(&ref[1445],V(1))
	move_ret R(0)
	call_c   build_ref_1443()
	call_c   Dyam_Create_Unary(&ref[1443],R(0))
	move_ret R(0)
	call_c   build_ref_1446()
	call_c   Dyam_Term_Start(&ref[1446],3)
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(22))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_End()
	move_ret R(1)
	call_c   Dyam_Create_Unary(&ref[1443],R(1))
	move_ret R(1)
	call_c   build_ref_1447()
	call_c   Dyam_Term_Start(&ref[1447],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(22))
	call_c   Dyam_Term_End()
	move_ret R(2)
	call_c   Dyam_Create_Unary(&ref[1443],R(2))
	move_ret R(2)
	call_c   Dyam_Create_Binary(I(4),R(2),V(30))
	move_ret R(2)
	call_c   Dyam_Create_Binary(I(4),R(1),R(2))
	move_ret R(2)
	call_c   Dyam_Create_Binary(I(4),R(0),R(2))
	move_ret ref[288]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 1447: lctag_map_reverse
c_code local build_ref_1447
	ret_reg &ref[1447]
	call_c   Dyam_Create_Atom("lctag_map_reverse")
	move_ret ref[1447]
	c_ret

;; TERM 1446: lctag_map
c_code local build_ref_1446
	ret_reg &ref[1446]
	call_c   Dyam_Create_Atom("lctag_map")
	move_ret ref[1446]
	c_ret

;; TERM 1445: loaded_tree
c_code local build_ref_1445
	ret_reg &ref[1445]
	call_c   Dyam_Create_Atom("loaded_tree")
	move_ret ref[1445]
	c_ret

;; TERM 283: '$VAR'(_V, ['$RANGE',0,1000])
c_code local build_ref_283
	ret_reg &ref[283]
	call_c   build_ref_1448()
	call_c   Dyam_Term_Start(&ref[1448],3)
	call_c   Dyam_Term_Arg(V(21))
	call_c   Dyam_Term_Arg(N(0))
	call_c   Dyam_Term_Arg(N(1000))
	call_c   Dyam_Term_End()
	move_ret ref[283]
	c_ret

;; TERM 1448: '$RANGE'
c_code local build_ref_1448
	ret_reg &ref[1448]
	call_c   Dyam_Create_Atom("$RANGE")
	move_ret ref[1448]
	c_ret

;; TERM 282: '$VAR'(_U, ['$RANGE',0,1000])
c_code local build_ref_282
	ret_reg &ref[282]
	call_c   build_ref_1448()
	call_c   Dyam_Term_Start(&ref[1448],3)
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_Arg(N(0))
	call_c   Dyam_Term_Arg(N(1000))
	call_c   Dyam_Term_End()
	move_ret ref[282]
	c_ret

;; TERM 295: guard{goal=> tag_node{id=> _F, label=> _G, children=> _H, kind=> _I, adj=> _J, spine=> _K, top=> _L, bot=> _M, token=> _N, adjleft=> _O, adjright=> _P, adjwrap=> _Q}, plus=> _R, minus=> _S}
c_code local build_ref_295
	ret_reg &ref[295]
	call_c   build_ref_1308()
	call_c   build_ref_281()
	call_c   Dyam_Term_Start(&ref[1308],3)
	call_c   Dyam_Term_Arg(&ref[281])
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_End()
	move_ret ref[295]
	c_ret

;; TERM 279: [_C1 ^ _Z,_B1,_R] ^ _D1
c_code local build_ref_279
	ret_reg &ref[279]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_1307()
	call_c   Dyam_Create_Binary(&ref[1307],V(28),V(25))
	move_ret R(0)
	call_c   Dyam_Create_List(V(17),I(0))
	move_ret R(1)
	call_c   Dyam_Create_List(V(27),R(1))
	move_ret R(1)
	call_c   Dyam_Create_List(R(0),R(1))
	move_ret R(1)
	call_c   Dyam_Create_Binary(&ref[1307],R(1),V(29))
	move_ret ref[279]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 278: [_Z|_C1]
c_code local build_ref_278
	ret_reg &ref[278]
	call_c   Dyam_Create_List(V(25),V(28))
	move_ret ref[278]
	c_ret

;; TERM 280: _H :> _Z
c_code local build_ref_280
	ret_reg &ref[280]
	call_c   Dyam_Create_Binary(I(9),V(7),V(25))
	move_ret ref[280]
	c_ret

;; TERM 273: [_Q,_R,_S,_T,_U,_V,_W,_X,_F,_G]
c_code local build_ref_273
	ret_reg &ref[273]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Tupple(5,6,I(0))
	move_ret R(0)
	call_c   Dyam_Create_Tupple(16,23,R(0))
	move_ret ref[273]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 237: [_H|_J]
c_code local build_ref_237
	ret_reg &ref[237]
	call_c   Dyam_Create_List(V(7),V(9))
	move_ret ref[237]
	c_ret

;; TERM 228: _E ## _F
c_code local build_ref_228
	ret_reg &ref[228]
	call_c   build_ref_1382()
	call_c   Dyam_Create_Binary(&ref[1382],V(4),V(5))
	move_ret ref[228]
	c_ret

;; TERM 229: [_H|_C]
c_code local build_ref_229
	ret_reg &ref[229]
	call_c   Dyam_Create_List(V(7),V(2))
	move_ret ref[229]
	c_ret

;; TERM 162: tig(_B, _O)
c_code local build_ref_162
	ret_reg &ref[162]
	call_c   build_ref_1343()
	call_c   Dyam_Create_Binary(&ref[1343],V(1),V(14))
	move_ret ref[162]
	c_ret

;; TERM 164: '$VAR'(_R, ['$SET$',adj(no, yes, strict, atmostone, one, sync),62])
c_code local build_ref_164
	ret_reg &ref[164]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1265()
	call_c   build_ref_397()
	call_c   build_ref_161()
	call_c   build_ref_1385()
	call_c   build_ref_1386()
	call_c   build_ref_1387()
	call_c   build_ref_1388()
	call_c   Dyam_Term_Start(&ref[1265],6)
	call_c   Dyam_Term_Arg(&ref[397])
	call_c   Dyam_Term_Arg(&ref[161])
	call_c   Dyam_Term_Arg(&ref[1385])
	call_c   Dyam_Term_Arg(&ref[1386])
	call_c   Dyam_Term_Arg(&ref[1387])
	call_c   Dyam_Term_Arg(&ref[1388])
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   Dyam_Term_Start(I(8),3)
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(N(62))
	call_c   Dyam_Term_End()
	move_ret ref[164]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 163: adjkind((_P / _Q), wrap)
c_code local build_ref_163
	ret_reg &ref[163]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1294()
	call_c   Dyam_Create_Binary(&ref[1294],V(15),V(16))
	move_ret R(0)
	call_c   build_ref_1389()
	call_c   build_ref_294()
	call_c   Dyam_Create_Binary(&ref[1389],R(0),&ref[294])
	move_ret ref[163]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 165: adjkind((_P / _Q), _S)
c_code local build_ref_165
	ret_reg &ref[165]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_1294()
	call_c   Dyam_Create_Binary(&ref[1294],V(15),V(16))
	move_ret R(0)
	call_c   build_ref_1389()
	call_c   Dyam_Create_Binary(&ref[1389],R(0),V(18))
	move_ret ref[165]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 166: adjkind(_T, no)
c_code local build_ref_166
	ret_reg &ref[166]
	call_c   build_ref_1389()
	call_c   build_ref_397()
	call_c   Dyam_Create_Binary(&ref[1389],V(19),&ref[397])
	move_ret ref[166]
	c_ret

;; TERM 149: tag_equation(_B, _E, _K, _L, _D)
c_code local build_ref_149
	ret_reg &ref[149]
	call_c   build_ref_1363()
	call_c   Dyam_Term_Start(&ref[1363],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[149]
	c_ret

;; TERM 151: tag_node{id=> _E, label=> _F, children=> _G, kind=> _H, adj=> _I, spine=> _J, top=> _K, bot=> _L, token=> _M, adjleft=> _N, adjright=> _O, adjwrap=> _P}
c_code local build_ref_151
	ret_reg &ref[151]
	call_c   build_ref_1259()
	call_c   Dyam_Term_Start(&ref[1259],12)
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_End()
	move_ret ref[151]
	c_ret

;; TERM 147: [_D|_F]
c_code local build_ref_147
	ret_reg &ref[147]
	call_c   Dyam_Create_List(V(3),V(5))
	move_ret ref[147]
	c_ret

;; TERM 146: _D ; _E
c_code local build_ref_146
	ret_reg &ref[146]
	call_c   Dyam_Create_Binary(I(5),V(3),V(4))
	move_ret ref[146]
	c_ret

;; TERM 81: local_var(_B, _D)
c_code local build_ref_81
	ret_reg &ref[81]
	call_c   build_ref_1226()
	call_c   Dyam_Create_Binary(&ref[1226],V(1),V(3))
	move_ret ref[81]
	c_ret

;; TERM 82: '$TUPPLE'(_C)
c_code local build_ref_82
	ret_reg &ref[82]
	call_c   Dyam_Create_Unary(I(10),V(2))
	move_ret ref[82]
	c_ret

;; TERM 43: numbervars(_B, _C)
c_code local build_ref_43
	ret_reg &ref[43]
	call_c   build_ref_1435()
	call_c   Dyam_Create_Binary(&ref[1435],V(1),V(2))
	move_ret ref[43]
	c_ret

pl_code local fun12
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Object(R(0))
	call_c   Dyam_Tabule()
	pl_ret

pl_code local fun20
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Object(R(0))
	call_c   Dyam_Tabule()
	pl_ret

pl_code local fun461
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(2),&ref[898])
	fail_ret
fun460:
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(7))
	call_c   Dyam_Reg_Load(4,V(3))
	move     V(9), R(6)
	move     S(5), R(7)
	call_c   Dyam_Reg_Load(8,V(5))
	move     V(10), R(10)
	move     S(5), R(11)
	pl_call  pred_tag_autoloader_6()
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(8))
	call_c   Dyam_Reg_Load(4,V(3))
	move     V(11), R(6)
	move     S(5), R(7)
	call_c   Dyam_Reg_Load(8,V(10))
	call_c   Dyam_Reg_Load(10,V(6))
	pl_call  pred_tag_autoloader_6()
	move     &ref[1198], R(0)
	move     S(5), R(1)
	move     V(12), R(2)
	move     S(5), R(3)
	pl_call  pred_autoloader_simplify_2()
	call_c   Dyam_Reg_Load(0,V(12))
	call_c   Dyam_Reg_Load(2,V(4))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_tag_autoloader_simplify_2()


pl_code local fun463
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(70),&ref[294])
	fail_ret
fun462:
	call_c   Dyam_Unify(V(4),&ref[1213])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret


pl_code local fun464
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Reg_Load(0,V(1))
	move     &ref[1219], R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Load(4,V(3))
	call_c   Dyam_Reg_Load(6,V(4))
	call_c   Dyam_Reg_Load(8,V(5))
	call_c   Dyam_Reg_Load(10,V(6))
	call_c   Dyam_Reg_Deallocate(6)
	pl_jump  pred_tag_autoloader_6()

pl_code local fun465
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(4),&ref[1225])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun466
	call_c   Dyam_Update_Choice(fun465)
	call_c   Dyam_Set_Cut()
	pl_fail

pl_code local fun467
	call_c   Dyam_Update_Choice(fun466)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(2),&ref[1221])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(7))
	call_c   Dyam_Reg_Load(4,V(3))
	move     V(9), R(6)
	move     S(5), R(7)
	call_c   Dyam_Reg_Load(8,V(5))
	call_c   Dyam_Reg_Load(10,V(6))
	pl_call  pred_tag_autoloader_6()
	move     V(94), R(0)
	move     S(5), R(1)
	pl_call  pred_null_tupple_1()
	call_c   Dyam_Reg_Load(0,V(93))
	move     V(95), R(2)
	move     S(5), R(3)
	move     N(0), R(4)
	move     0, R(5)
	call_c   Dyam_Reg_Load(6,V(94))
	move     &ref[259], R(8)
	move     0, R(9)
	pl_call  pred_guard_approx_5()
	call_c   Dyam_Unify(V(4),&ref[1222])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun468
	call_c   Dyam_Update_Choice(fun467)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(2),&ref[1217])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun464)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(85),&ref[1218])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(1))
	move     &ref[1219], R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Load(4,V(3))
	move     V(9), R(6)
	move     S(5), R(7)
	move     V(13), R(8)
	move     S(5), R(9)
	move     V(29), R(10)
	move     S(5), R(11)
	pl_call  pred_tag_autoloader_6()
	call_c   Dyam_Unify(V(4),&ref[1220])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun469
	call_c   Dyam_Update_Choice(fun468)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(2),&ref[1214])
	fail_ret
	pl_call  Object_1(&ref[1215])
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(4),&ref[1216])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun470
	call_c   Dyam_Update_Choice(fun469)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(2),&ref[1210])
	fail_ret
	pl_call  Object_1(&ref[1211])
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun463)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[1212])
	call_c   Dyam_Cut()
	pl_jump  fun462()

pl_code local fun471
	call_c   Dyam_Update_Choice(fun470)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(2),&ref[1208])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(4),&ref[1209])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun472
	call_c   Dyam_Update_Choice(fun471)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(2),&ref[1205])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(3),&ref[1206])
	fail_ret
	call_c   Dyam_Unify(V(4),&ref[1207])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun473
	call_c   Dyam_Update_Choice(fun472)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(2),&ref[1204])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(4),&ref[1203])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun474
	call_c   Dyam_Update_Choice(fun473)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(2),&ref[1202])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(4),&ref[1203])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun475
	call_c   Dyam_Update_Choice(fun474)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(2),&ref[899])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(7))
	call_c   Dyam_Reg_Load(4,V(3))
	move     V(9), R(6)
	move     S(5), R(7)
	call_c   Dyam_Reg_Load(8,V(5))
	call_c   Dyam_Reg_Load(10,V(6))
	pl_call  pred_tag_autoloader_6()
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(8))
	call_c   Dyam_Reg_Load(4,V(3))
	move     V(11), R(6)
	move     S(5), R(7)
	call_c   Dyam_Reg_Load(8,V(5))
	call_c   Dyam_Reg_Load(10,V(6))
	pl_call  pred_tag_autoloader_6()
	move     &ref[1201], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(4))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_autoloader_simplify_2()

pl_code local fun476
	call_c   Dyam_Update_Choice(fun475)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(2),&ref[1199])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(7))
	call_c   Dyam_Reg_Load(4,V(3))
	move     V(9), R(6)
	move     S(5), R(7)
	move     V(13), R(8)
	move     S(5), R(9)
	call_c   Dyam_Reg_Load(10,V(6))
	pl_call  pred_tag_autoloader_6()
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(8))
	call_c   Dyam_Reg_Load(4,V(3))
	move     V(11), R(6)
	move     S(5), R(7)
	move     V(14), R(8)
	move     S(5), R(9)
	call_c   Dyam_Reg_Load(10,V(6))
	pl_call  pred_tag_autoloader_6()
	move     &ref[1200], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(4))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_autoloader_simplify_2()

pl_code local fun477
	call_c   Dyam_Update_Choice(fun476)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Choice(fun461)
	call_c   Dyam_Unify(V(2),&ref[236])
	fail_ret
	pl_jump  fun460()

pl_code local fun423
	call_c   Dyam_Remove_Choice()
	pl_call  fun50(&seed[212],2,V(4))
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun390
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(2),&ref[1031])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun391
	call_c   Dyam_Update_Choice(fun390)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(9),&ref[261])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(2),V(8))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun392
	call_c   Dyam_Update_Choice(fun391)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(9),&ref[330])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(2),&ref[330])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun393
	call_c   Dyam_Update_Choice(fun392)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(8),&ref[261])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(2),V(9))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun395
	call_c   Dyam_Remove_Choice()
	call_c   DYAM_sfol_identical(V(5),&ref[284])
	fail_ret
fun394:
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(2),&ref[1033])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret


pl_code local fun396
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(2),&ref[1035])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun397
	call_c   Dyam_Update_Choice(fun396)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Reg_Load(0,V(9))
	move     V(11), R(2)
	move     S(5), R(3)
	pl_call  pred_guard_length_2()
	call_c   Dyam_Reg_Load(0,V(8))
	move     V(12), R(2)
	move     S(5), R(3)
	pl_call  pred_guard_length_2()
	call_c   DYAM_evpred_lt(V(11),V(12))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(2),&ref[1034])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun398
	call_c   Dyam_Update_Choice(fun397)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Choice(fun395)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_gt(V(3),N(0))
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun394()

pl_code local fun399
	call_c   Dyam_Update_Choice(fun398)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(8),&ref[330])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(2),V(9))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun400
	call_c   Dyam_Update_Choice(fun399)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(9),&ref[330])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(2),V(8))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun401
	call_c   Dyam_Update_Choice(fun400)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(8),&ref[261])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(2),&ref[261])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun405
	call_c   Dyam_Remove_Choice()
fun404:
	call_c   Dyam_Cut()
fun403:
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(2),V(1))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret



pl_code local fun406
	call_c   Dyam_Remove_Choice()
	pl_jump  fun403()

pl_code local fun407
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Reg_Load(0,V(14))
	call_c   Dyam_Reg_Load(2,V(4))
	pl_call  pred_local_var_in_tupple_2()
	call_c   Dyam_Choice(fun406)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[1038])
	call_c   Dyam_Cut()
	pl_fail

pl_code local fun408
	call_c   Dyam_Update_Choice(fun407)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Reg_Load(0,V(13))
	call_c   Dyam_Reg_Load(2,V(4))
	pl_call  pred_local_var_in_tupple_2()
	call_c   Dyam_Choice(fun405)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[1037])
	call_c   Dyam_Cut()
	pl_fail

pl_code local fun410
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(1),&ref[1039])
	fail_ret
fun409:
	call_c   Dyam_Choice(fun408)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Reg_Load(0,V(13))
	call_c   Dyam_Reg_Load(2,V(4))
	pl_call  pred_local_var_in_tupple_2()
	call_c   Dyam_Reg_Load(0,V(14))
	call_c   Dyam_Reg_Load(2,V(4))
	pl_call  pred_local_var_in_tupple_2()
	call_c   Dyam_Cut()
	pl_jump  fun403()


pl_code local fun402
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(2),&ref[261])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun411
	call_c   Dyam_Update_Choice(fun402)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Choice(fun410)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[1036])
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun409()

pl_code local fun412
	call_c   Dyam_Update_Choice(fun411)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(3),N(0))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(2),V(1))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun413
	call_c   Dyam_Update_Choice(fun412)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[330])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(2),V(1))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun414
	call_c   Dyam_Update_Choice(fun413)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[261])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(2),V(1))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun415
	call_c   Dyam_Update_Choice(fun414)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[546])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun402)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_lt(V(3),N(2))
	fail_ret
	call_c   Dyam_Cut()
	call_c   DYAM_evpred_is(V(10),&ref[1032])
	fail_ret
	call_c   Dyam_Reg_Load(0,V(6))
	move     V(9), R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Load(4,V(10))
	call_c   Dyam_Reg_Load(6,V(4))
	call_c   Dyam_Reg_Load(8,V(5))
	pl_call  pred_guard_approx_5()
	call_c   Dyam_Reg_Load(0,V(7))
	move     V(8), R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Load(4,V(10))
	call_c   Dyam_Reg_Load(6,V(4))
	call_c   Dyam_Reg_Load(8,V(5))
	pl_call  pred_guard_approx_5()
	call_c   Dyam_Choice(fun401)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(9),&ref[261])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(2),&ref[261])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun416
	call_c   Dyam_Update_Choice(fun415)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[545])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(7))
	move     V(8), R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Load(4,V(3))
	call_c   Dyam_Reg_Load(6,V(4))
	call_c   Dyam_Reg_Load(8,V(5))
	pl_call  pred_guard_approx_5()
	call_c   Dyam_Reg_Load(0,V(6))
	move     V(9), R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Load(4,V(3))
	call_c   Dyam_Reg_Load(6,V(4))
	call_c   Dyam_Reg_Load(8,V(5))
	pl_call  pred_guard_approx_5()
	call_c   Dyam_Choice(fun393)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(8),&ref[330])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(2),&ref[330])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun377
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Reg_Load(0,V(4))
	call_c   Dyam_Reg_Load(2,V(2))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_tree_foot_2()

pl_code local fun378
	call_c   Dyam_Update_Choice(fun289)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[999])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(36))
	call_c   Dyam_Reg_Load(2,V(2))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_tree_foot_2()

pl_code local fun379
	call_c   Dyam_Update_Choice(fun378)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[998])
	fail_ret
	call_c   Dyam_Cut()
	pl_call  Domain_2(V(36),V(26))
	call_c   Dyam_Reg_Load(0,V(36))
	call_c   Dyam_Reg_Load(2,V(2))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_tree_foot_2()

pl_code local fun380
	call_c   Dyam_Update_Choice(fun379)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[997])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(2),V(1))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun381
	call_c   Dyam_Update_Choice(fun380)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[992])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(3))
	call_c   Dyam_Reg_Load(2,V(2))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_tree_foot_2()

pl_code local fun382
	call_c   Dyam_Update_Choice(fun381)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[341])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun377)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Reg_Load(0,V(3))
	call_c   Dyam_Reg_Load(2,V(2))
	pl_call  pred_tree_foot_2()
	call_c   Dyam_Cut()
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun383
	call_c   Dyam_Update_Choice(fun382)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[146])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun377)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Reg_Load(0,V(3))
	call_c   Dyam_Reg_Load(2,V(2))
	pl_call  pred_tree_foot_2()
	call_c   Dyam_Cut()
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun384
	call_c   Dyam_Update_Choice(fun383)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[779])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun377)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Reg_Load(0,V(3))
	call_c   Dyam_Reg_Load(2,V(2))
	pl_call  pred_tree_foot_2()
	call_c   Dyam_Cut()
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun385
	call_c   Dyam_Update_Choice(fun384)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[991])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(3))
	call_c   Dyam_Reg_Load(2,V(2))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_tree_foot_2()

pl_code local fun386
	call_c   Dyam_Update_Choice(fun385)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[778])
	fail_ret
	call_c   Dyam_Cut()
	pl_fail

pl_code local fun387
	call_c   Dyam_Update_Choice(fun386)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),I(0))
	fail_ret
	call_c   Dyam_Cut()
	pl_fail

pl_code local fun365
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Reg_Load(0,V(2))
	call_c   Dyam_Reg_Load(2,V(1))
	pl_call  pred_tag_compile_info_anchor_2()
fun364:
	call_c   Dyam_Reg_Load(0,V(17))
	call_c   Dyam_Reg_Load(2,V(2))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_tree_info_2()


pl_code local fun366
	call_c   Dyam_Update_Choice(fun365)
	call_c   Dyam_Unify(V(2),&ref[996])
	fail_ret
	pl_jump  fun364()

pl_code local fun367
	call_c   Dyam_Update_Choice(fun35)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[994])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun366)
	call_c   Dyam_Set_Cut()
	pl_call  Domain_2(V(18),&ref[995])
	call_c   Dyam_Cut()
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun368
	call_c   Dyam_Update_Choice(fun367)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[993])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(3))
	call_c   Dyam_Reg_Load(2,V(2))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_tree_info_2()

pl_code local fun369
	call_c   Dyam_Update_Choice(fun368)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[992])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(3))
	call_c   Dyam_Reg_Load(2,V(2))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_tree_info_2()

pl_code local fun370
	call_c   Dyam_Update_Choice(fun369)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[146])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(3))
	call_c   Dyam_Reg_Load(2,V(2))
	pl_call  pred_tree_info_2()
	call_c   Dyam_Reg_Load(0,V(4))
	call_c   Dyam_Reg_Load(2,V(2))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_tree_info_2()

pl_code local fun371
	call_c   Dyam_Update_Choice(fun370)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[341])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(3))
	call_c   Dyam_Reg_Load(2,V(2))
	pl_call  pred_tree_info_2()
	call_c   Dyam_Reg_Load(0,V(4))
	call_c   Dyam_Reg_Load(2,V(2))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_tree_info_2()

pl_code local fun372
	call_c   Dyam_Update_Choice(fun371)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[779])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(3))
	call_c   Dyam_Reg_Load(2,V(2))
	pl_call  pred_tree_info_2()
	call_c   Dyam_Reg_Load(0,V(4))
	call_c   Dyam_Reg_Load(2,V(2))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_tree_info_2()

pl_code local fun373
	call_c   Dyam_Update_Choice(fun372)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[991])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(3))
	call_c   Dyam_Reg_Load(2,V(2))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_tree_info_2()

pl_code local fun374
	call_c   Dyam_Update_Choice(fun373)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[778])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(3))
	call_c   Dyam_Reg_Load(2,V(2))
	pl_call  pred_tree_info_2()
	call_c   Dyam_Reg_Load(0,V(4))
	call_c   Dyam_Reg_Load(2,V(2))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_tree_info_2()

pl_code local fun375
	call_c   Dyam_Update_Choice(fun374)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),I(0))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun349
	call_c   Dyam_Remove_Choice()
	pl_call  fun50(&seed[186],2,V(2))
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun336
	call_c   Dyam_Remove_Choice()
	pl_call  fun50(&seed[184],2,V(6))
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun327
	call_c   Dyam_Remove_Choice()
	pl_call  fun28(&seed[181],1)
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun328
	call_c   Dyam_Update_Choice(fun327)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(2),&ref[330])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(3),V(4))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun329
	call_c   Dyam_Update_Choice(fun328)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(2),&ref[261])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(3),V(4))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun330
	call_c   Dyam_Update_Choice(fun329)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(2),&ref[899])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun204)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_lt(V(6),N(0))
	fail_ret
	call_c   Dyam_Cut()
	call_c   DYAM_evpred_is(V(10),&ref[900])
	fail_ret
	call_c   Dyam_Reg_Load(0,V(3))
	call_c   Dyam_Reg_Load(2,V(4))
	move     V(11), R(4)
	move     S(5), R(5)
	move     V(12), R(6)
	move     S(5), R(7)
	move     V(13), R(8)
	move     S(5), R(9)
	pl_call  pred_handle_choice_5()
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(7))
	call_c   Dyam_Reg_Load(4,V(11))
	call_c   Dyam_Reg_Load(6,V(12))
	call_c   Dyam_Reg_Load(8,V(5))
	call_c   Dyam_Reg_Load(10,V(10))
	pl_call  pred_double_neg_handler_6()
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(8))
	call_c   Dyam_Reg_Load(4,V(11))
	call_c   Dyam_Reg_Load(6,V(13))
	call_c   Dyam_Reg_Load(8,V(5))
	call_c   Dyam_Reg_Load(10,V(10))
	call_c   Dyam_Reg_Deallocate(6)
	pl_jump  pred_double_neg_handler_6()

pl_code local fun303
	call_c   Dyam_Remove_Choice()
	pl_call  fun50(&seed[160],2,V(2))
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun300
	call_c   Dyam_Remove_Choice()
fun299:
	call_c   Dyam_Reg_Load_Ptr(2,V(6))
	fail_ret
	call_c   Dyam_Reg_Load(4,V(2))
	call_c   DyALog_Mutable_Read(R(2),&R(4))
	fail_ret
	call_c   Dyam_Reg_Deallocate(3)
	pl_ret


pl_code local fun293
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(2),N(0))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun294
	call_c   Dyam_Update_Choice(fun293)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[786])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(3))
	call_c   Dyam_Reg_Load(2,V(2))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_guard_depth_2()

pl_code local fun295
	call_c   Dyam_Update_Choice(fun294)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[785])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(3))
	move     V(5), R(2)
	move     S(5), R(3)
	pl_call  pred_guard_depth_2()
	call_c   Dyam_Reg_Load(0,V(4))
	move     V(6), R(2)
	move     S(5), R(3)
	pl_call  pred_guard_depth_2()
	call_c   DYAM_evpred_is(V(2),&ref[784])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun296
	call_c   Dyam_Update_Choice(fun295)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[146])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(3))
	move     V(5), R(2)
	move     S(5), R(3)
	pl_call  pred_guard_depth_2()
	call_c   Dyam_Reg_Load(0,V(4))
	move     V(6), R(2)
	move     S(5), R(3)
	pl_call  pred_guard_depth_2()
	call_c   DYAM_evpred_is(V(2),&ref[784])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun283
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Reg_Load(0,V(4))
	call_c   Dyam_Reg_Load(2,V(2))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_tag_extract_anchor_guards_2()

pl_code local fun285
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(1),&ref[146])
	fail_ret
	pl_jump  fun284()

pl_code local fun286
	call_c   Dyam_Update_Choice(fun285)
	call_c   Dyam_Unify(V(1),&ref[779])
	fail_ret
	pl_jump  fun284()

pl_code local fun287
	call_c   Dyam_Update_Choice(fun286)
	call_c   Dyam_Unify(V(1),&ref[341])
	fail_ret
	pl_jump  fun284()

pl_code local fun288
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Reg_Load(0,V(3))
	call_c   Dyam_Reg_Load(2,V(2))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_tag_extract_anchor_guards_2()

pl_code local fun289
	call_c   Dyam_Remove_Choice()
	pl_fail

pl_code local fun290
	call_c   Dyam_Update_Choice(fun289)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[781])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun288)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(3),&ref[782])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(2),V(18))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun291
	call_c   Dyam_Update_Choice(fun290)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[780])
	fail_ret
	call_c   Dyam_Cut()
	move     &ref[236], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(2))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_tag_extract_anchor_guards_2()

pl_code local fun276
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(2),&ref[777])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun277
	call_c   Dyam_Update_Choice(fun276)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(6),&ref[261])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(2),V(5))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun278
	call_c   Dyam_Update_Choice(fun277)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(6),&ref[330])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(2),&ref[330])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun279
	call_c   Dyam_Update_Choice(fun278)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(5),&ref[261])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(2),V(6))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun280
	call_c   Dyam_Update_Choice(fun251)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[146])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(2),&ref[261])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun281
	call_c   Dyam_Update_Choice(fun280)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[341])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(4))
	move     V(5), R(2)
	move     S(5), R(3)
	pl_call  pred_autoload_guard_approx_2()
	call_c   Dyam_Reg_Load(0,V(3))
	move     V(6), R(2)
	move     S(5), R(3)
	pl_call  pred_autoload_guard_approx_2()
	call_c   Dyam_Choice(fun279)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(5),&ref[330])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(2),&ref[330])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun270
	call_c   Dyam_Remove_Choice()
	call_c   DYAM_Numbervars_3(V(1),N(1),V(3))
	fail_ret
	call_c   Dyam_Reg_Load_Ptr(2,V(4))
	fail_ret
	call_c   Dyam_Reg_Load(4,V(3))
	call_c   DyALog_Mutable_Write(R(2),&R(4),0)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(4))
	fail_ret
	call_c   DYAM_evpred_assert_1(&ref[760])
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun271
	call_c   Dyam_Update_Choice(fun270)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[760])
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load_Ptr(2,V(4))
	fail_ret
	move     V(5), R(4)
	move     S(5), R(5)
	call_c   DyALog_Mutable_Read(R(2),&R(4))
	fail_ret
	call_c   DYAM_Numbervars_3(V(1),V(5),V(3))
	fail_ret
	call_c   Dyam_Reg_Load_Ptr(2,V(4))
	fail_ret
	call_c   Dyam_Reg_Load(4,V(3))
	call_c   DyALog_Mutable_Write(R(2),&R(4),0)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(4))
	fail_ret
	call_c   Dyam_Reg_Deallocate(4)
	pl_ret

pl_code local fun251
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(2),V(1))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun252
	call_c   Dyam_Update_Choice(fun251)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[699])
	fail_ret
	call_c   DYAM_evpred_variable(V(7))
	fail_ret
	call_c   DYAM_sfol_identical(V(7),V(8))
	fail_ret
	call_c   Dyam_Cut()
	move     &ref[700], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(2))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_tag_autoloader_simplify_2()

pl_code local fun253
	call_c   Dyam_Update_Choice(fun252)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[697])
	fail_ret
	call_c   DYAM_evpred_variable(V(7))
	fail_ret
	call_c   DYAM_sfol_identical(V(7),V(8))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(2),&ref[698])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun242
	call_c   Dyam_Remove_Choice()
	pl_call  fun50(&seed[150],2,V(3))
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun218
	call_c   Dyam_Remove_Choice()
	pl_call  fun50(&seed[138],2,V(10))
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun204
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(3),V(4))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun205
	call_c   Dyam_Update_Choice(fun204)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(2),&ref[546])
	fail_ret
	call_c   Dyam_Cut()
	pl_call  fun28(&seed[133],1)
	call_c   Dyam_Reg_Load(0,V(2))
	move     V(10), R(2)
	move     S(5), R(3)
	pl_call  pred_tupple_2()
	call_c   Dyam_Unify(V(4),&ref[551])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun176
	call_c   Dyam_Remove_Choice()
	pl_call  fun50(&seed[127],2,V(5))
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun173
	call_c   Dyam_Remove_Choice()
	pl_call  fun50(&seed[125],2,V(4))
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun170
	call_c   Dyam_Remove_Choice()
	pl_call  fun50(&seed[119],2,V(3))
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun167
	call_c   Dyam_Remove_Choice()
	pl_call  fun50(&seed[116],2,V(3))
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun158
	call_c   Dyam_Remove_Choice()
fun157:
	call_c   Dyam_Unify(V(4),&ref[437])
	fail_ret
	pl_call  fun28(&seed[113],1)
	pl_call  fun28(&seed[114],1)
	call_c   Dyam_Deallocate()
	pl_ret


pl_code local fun160
	call_c   Dyam_Remove_Choice()
fun159:
	call_c   Dyam_Choice(fun158)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(3),I(0))
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun157()


pl_code local fun162
	call_c   Dyam_Remove_Choice()
	move     &ref[444], R(0)
	move     0, R(1)
	move     &ref[148], R(2)
	move     S(5), R(3)
	pl_call  pred_warning_2()
	call_c   Dyam_Unify(V(5),I(0))
	fail_ret
	call_c   Dyam_Unify(V(6),I(0))
	fail_ret
fun161:
	call_c   Dyam_Choice(fun160)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(2),I(0))
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun159()


pl_code local fun163
	call_c   Dyam_Remove_Choice()
	pl_call  Domain_2(V(10),V(1))
	move     &ref[445], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(4))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_tag_compile_lemma_anchor_2()

pl_code local fun164
	call_c   Dyam_Update_Choice(fun163)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_atom(V(1))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun162)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[436])
	call_c   Dyam_Cut()
	pl_jump  fun161()

pl_code local fun153
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(16),V(24))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun155
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(24),&ref[432])
	fail_ret
	pl_jump  fun154()

pl_code local fun129
	call_c   Dyam_Remove_Choice()
	pl_call  fun50(&seed[101],2,V(5))
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun122
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(2),N(1))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun123
	call_c   Dyam_Update_Choice(fun122)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[146])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(4))
	move     V(5), R(2)
	move     S(5), R(3)
	pl_call  pred_guard_length_2()
	call_c   Dyam_Reg_Load(0,V(3))
	move     V(6), R(2)
	move     S(5), R(3)
	pl_call  pred_guard_length_2()
	call_c   DYAM_evpred_is(V(2),&ref[343])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun101
	call_c   Dyam_Remove_Choice()
fun100:
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(22),&ref[284])
	fail_ret
fun99:
	call_c   Dyam_Reg_Load_Ptr(2,V(23))
	fail_ret
	move     &ref[261], R(4)
	move     0, R(5)
	call_c   DyALog_Mutable_Write(R(2),&R(4),0)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(23))
	fail_ret
	call_c   Dyam_Choice(fun98)
	pl_call  Object_1(&ref[285])
	call_c   Dyam_Reg_Load_Ptr(2,V(23))
	fail_ret
	move     V(27), R(4)
	move     S(5), R(5)
	call_c   DyALog_Mutable_Read(R(2),&R(4))
	fail_ret
	call_c   Dyam_Unify(V(28),&ref[286])
	fail_ret
	call_c   Dyam_Reg_Load(0,V(28))
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(29), R(4)
	move     S(5), R(5)
	pl_call  pred_tag_numbervars_3()
	call_c   Dyam_Reg_Load_Ptr(2,V(23))
	fail_ret
	call_c   Dyam_Reg_Load(4,&ref[287])
	call_c   DyALog_Mutable_Write(R(2),&R(4),0)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(23))
	fail_ret
	pl_fail



pl_code local fun95
	call_c   Dyam_Remove_Choice()
fun94:
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(35),&ref[290])
	fail_ret
fun93:
	call_c   Dyam_Choice(fun92)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(35),&ref[261])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(4),&ref[291])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret



pl_code local fun92
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(4),&ref[292])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun96
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(35),V(31))
	fail_ret
	pl_jump  fun93()

pl_code local fun98
	call_c   Dyam_Remove_Choice()
fun97:
	call_c   Dyam_Reg_Load_Ptr(2,V(23))
	fail_ret
	move     V(30), R(4)
	move     S(5), R(5)
	call_c   DyALog_Mutable_Read(R(2),&R(4))
	fail_ret
	call_c   Dyam_Unify(V(31),&ref[288])
	fail_ret
	call_c   Dyam_Choice(fun96)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Choice(fun95)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[289])
	call_c   Dyam_Cut()
	pl_fail


pl_code local fun102
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(22),&ref[294])
	fail_ret
	pl_jump  fun99()

pl_code local fun103
	call_c   Dyam_Update_Choice(fun102)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[293])
	call_c   Dyam_Cut()
	pl_jump  fun99()

pl_code local fun105
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(2),&ref[295])
	fail_ret
	pl_jump  fun104()

pl_code local fun86
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(26),&ref[280])
	fail_ret
	call_c   Dyam_Unify(V(17),V(27))
	fail_ret
	call_c   Dyam_Unify(V(28),I(0))
	fail_ret
fun85:
	pl_call  fun28(&seed[93],1)
	call_c   Dyam_Unify(V(31),V(30))
	fail_ret
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(31))
	call_c   Dyam_Reg_Load(4,V(16))
	call_c   Dyam_Reg_Load(6,V(17))
	call_c   Dyam_Reg_Load(8,V(18))
	call_c   Dyam_Reg_Load(10,V(20))
	call_c   Dyam_Reg_Load(12,V(21))
	call_c   Dyam_Reg_Load(14,V(22))
	call_c   Dyam_Reg_Load(16,V(5))
	call_c   Dyam_Reg_Load(18,V(3))
	call_c   Dyam_Reg_Load(20,V(4))
	call_c   Dyam_Reg_Load(22,V(8))
	move     &ref[278], R(24)
	move     S(5), R(25)
	call_c   Dyam_Reg_Load(26,V(9))
	call_c   Dyam_Reg_Load(28,V(10))
	call_c   Dyam_Reg_Load(30,V(11))
	call_c   Dyam_Reg_Load(32,V(12))
	call_c   Dyam_Reg_Deallocate(17)
	pl_jump  pred_tag_il_register_to_lpda_17()


pl_code local fun87
	call_c   Dyam_Update_Choice(fun86)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(7),&ref[279])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(26),V(29))
	fail_ret
	pl_jump  fun85()

pl_code local fun88
	call_c   Dyam_Remove_Choice()
	move     &ref[273], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(24), R(4)
	move     S(5), R(5)
	pl_call  pred_tag_numbervars_3()
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(17))
	call_c   Dyam_Reg_Load(4,V(19))
	call_c   Dyam_Reg_Load(6,V(16))
	call_c   Dyam_Reg_Load(8,V(21))
	call_c   Dyam_Reg_Load(10,V(23))
	call_c   Dyam_Reg_Load(12,V(20))
	call_c   Dyam_Reg_Load(14,V(6))
	move     V(25), R(16)
	move     S(5), R(17)
	call_c   Dyam_Reg_Load(18,V(10))
	call_c   Dyam_Reg_Load(20,V(11))
	pl_call  pred_tag_il_loop_to_lpda_11()
	call_c   Dyam_Choice(fun87)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(7),&ref[274])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(26),V(25))
	fail_ret
	call_c   Dyam_Unify(V(17),V(27))
	fail_ret
	call_c   Dyam_Unify(V(28),I(0))
	fail_ret
fun84:
	pl_call  fun28(&seed[93],1)
	call_c   Dyam_Unify(V(31),V(30))
	fail_ret
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(31))
	call_c   Dyam_Reg_Load(4,V(16))
	call_c   Dyam_Reg_Load(6,V(17))
	call_c   Dyam_Reg_Load(8,V(18))
	call_c   Dyam_Reg_Load(10,V(20))
	call_c   Dyam_Reg_Load(12,V(21))
	call_c   Dyam_Reg_Load(14,V(22))
	call_c   Dyam_Reg_Load(16,V(5))
	call_c   Dyam_Reg_Load(18,V(3))
	call_c   Dyam_Reg_Load(20,V(4))
	call_c   Dyam_Reg_Load(22,V(8))
	move     &ref[278], R(24)
	move     S(5), R(25)
	call_c   Dyam_Reg_Load(26,V(9))
	call_c   Dyam_Reg_Load(28,V(10))
	call_c   Dyam_Reg_Load(30,V(11))
	call_c   Dyam_Reg_Load(32,V(12))
	call_c   Dyam_Reg_Deallocate(17)
	pl_jump  pred_tag_il_register_to_lpda_17()


pl_code local fun90
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(2),V(14))
	fail_ret
	pl_jump  fun89()

pl_code local fun73
	call_c   Dyam_Remove_Choice()
	pl_call  fun50(&seed[90],2,V(4))
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun70
	call_c   Dyam_Remove_Choice()
	call_c   DYAM_evpred_univ(V(2),&ref[236])
	fail_ret
	call_c   DYAM_evpred_univ(V(3),&ref[237])
	fail_ret
	pl_call  fun28(&seed[87],1)
	pl_call  fun28(&seed[88],1)
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun68
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Reg_Load(0,V(1))
	move     V(7), R(2)
	move     S(5), R(3)
	pl_call  pred_tupple_2()
	call_c   Dyam_Unify(V(3),&ref[229])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun45
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Object(R(0))
	call_c   Dyam_Tabule()
	pl_jump  Schedule_Prolog()

pl_code local fun46
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Choice(fun45)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Subsume(R(0))
	fail_ret
	call_c   Dyam_Cut()
	pl_ret

pl_code local fun50
	call_c   Dyam_Backptr_Trace(R(1),R(2))
	call_c   Dyam_Light_Object(R(0))
	pl_jump  Schedule_Prolog()

pl_code local fun51
	call_c   Dyam_Remove_Choice()
	pl_call  fun50(&seed[79],2,V(5))
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun39
	call_c   Dyam_Remove_Choice()
fun38:
	call_c   Dyam_Choice(fun35)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[166])
	call_c   Dyam_Cut()
	pl_fail


pl_code local fun40
	call_c   Dyam_Update_Choice(fun39)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[165])
	call_c   Dyam_Cut()
	pl_fail

pl_code local fun42
	call_c   Dyam_Remove_Choice()
fun41:
	call_c   DYAM_evpred_functor(V(9),V(15),V(16))
	fail_ret
	call_c   Dyam_Choice(fun40)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[163])
	call_c   Dyam_Unify(V(13),&ref[164])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Deallocate()
	pl_ret


pl_code local fun35
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun32
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(2),&ref[148])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret


;;------------------ INIT POOL ------------

c_code local build_init_pool
	call_c   build_seed_65()
	call_c   build_seed_66()
	call_c   build_seed_68()
	call_c   build_seed_6()
	call_c   build_seed_2()
	call_c   build_seed_5()
	call_c   build_seed_3()
	call_c   build_seed_48()
	call_c   build_seed_4()
	call_c   build_seed_24()
	call_c   build_seed_22()
	call_c   build_seed_0()
	call_c   build_seed_36()
	call_c   build_seed_9()
	call_c   build_seed_10()
	call_c   build_seed_12()
	call_c   build_seed_62()
	call_c   build_seed_13()
	call_c   build_seed_11()
	call_c   build_seed_63()
	call_c   build_seed_64()
	call_c   build_seed_41()
	call_c   build_seed_23()
	call_c   build_seed_61()
	call_c   build_seed_53()
	call_c   build_seed_21()
	call_c   build_seed_51()
	call_c   build_seed_52()
	call_c   build_seed_7()
	call_c   build_seed_60()
	call_c   build_seed_1()
	call_c   build_seed_45()
	call_c   build_seed_38()
	call_c   build_seed_18()
	call_c   build_seed_15()
	call_c   build_seed_17()
	call_c   build_seed_8()
	call_c   build_seed_32()
	call_c   build_seed_44()
	call_c   build_seed_50()
	call_c   build_seed_27()
	call_c   build_seed_57()
	call_c   build_seed_20()
	call_c   build_seed_59()
	call_c   build_seed_31()
	call_c   build_seed_30()
	call_c   build_seed_28()
	call_c   build_seed_54()
	call_c   build_seed_56()
	call_c   build_seed_35()
	call_c   build_seed_26()
	call_c   build_seed_39()
	call_c   build_seed_34()
	call_c   build_seed_29()
	call_c   build_seed_19()
	call_c   build_seed_55()
	call_c   build_seed_58()
	call_c   build_seed_40()
	call_c   build_seed_37()
	call_c   build_seed_16()
	call_c   build_seed_42()
	call_c   build_seed_25()
	call_c   build_seed_43()
	call_c   build_seed_49()
	call_c   build_seed_47()
	call_c   build_seed_14()
	call_c   build_seed_33()
	call_c   build_seed_46()
	call_c   build_seed_211()
	call_c   build_seed_212()
	call_c   build_seed_185()
	call_c   build_seed_186()
	call_c   build_seed_183()
	call_c   build_seed_184()
	call_c   build_seed_181()
	call_c   build_seed_159()
	call_c   build_seed_160()
	call_c   build_seed_149()
	call_c   build_seed_150()
	call_c   build_seed_108()
	call_c   build_seed_138()
	call_c   build_seed_133()
	call_c   build_seed_126()
	call_c   build_seed_127()
	call_c   build_seed_124()
	call_c   build_seed_125()
	call_c   build_seed_118()
	call_c   build_seed_119()
	call_c   build_seed_115()
	call_c   build_seed_116()
	call_c   build_seed_114()
	call_c   build_seed_113()
	call_c   build_seed_112()
	call_c   build_seed_111()
	call_c   build_seed_100()
	call_c   build_seed_101()
	call_c   build_seed_85()
	call_c   build_seed_86()
	call_c   build_seed_93()
	call_c   build_seed_89()
	call_c   build_seed_90()
	call_c   build_seed_88()
	call_c   build_seed_87()
	call_c   build_seed_73()
	call_c   build_seed_79()
	call_c   build_ref_1()
	call_c   build_ref_3()
	call_c   build_ref_4()
	call_c   build_ref_5()
	call_c   build_ref_9()
	call_c   build_ref_8()
	call_c   build_ref_13()
	call_c   build_ref_12()
	call_c   build_ref_14()
	call_c   build_ref_15()
	call_c   build_ref_19()
	call_c   build_ref_18()
	call_c   build_ref_20()
	call_c   build_ref_21()
	call_c   build_ref_24()
	call_c   build_ref_23()
	call_c   build_ref_25()
	call_c   build_ref_26()
	call_c   build_ref_30()
	call_c   build_ref_29()
	call_c   build_ref_33()
	call_c   build_ref_32()
	call_c   build_ref_36()
	call_c   build_ref_35()
	call_c   build_ref_37()
	call_c   build_ref_38()
	call_c   build_ref_39()
	call_c   build_ref_40()
	call_c   build_ref_41()
	call_c   build_ref_42()
	call_c   build_ref_1197()
	call_c   build_ref_1198()
	call_c   build_ref_1200()
	call_c   build_ref_1199()
	call_c   build_ref_1201()
	call_c   build_ref_1202()
	call_c   build_ref_1203()
	call_c   build_ref_1204()
	call_c   build_ref_1207()
	call_c   build_ref_1206()
	call_c   build_ref_1205()
	call_c   build_ref_1209()
	call_c   build_ref_1208()
	call_c   build_ref_1212()
	call_c   build_ref_1213()
	call_c   build_ref_1211()
	call_c   build_ref_1210()
	call_c   build_ref_1216()
	call_c   build_ref_1215()
	call_c   build_ref_1214()
	call_c   build_ref_1220()
	call_c   build_ref_1218()
	call_c   build_ref_1219()
	call_c   build_ref_1217()
	call_c   build_ref_1222()
	call_c   build_ref_259()
	call_c   build_ref_1221()
	call_c   build_ref_1225()
	call_c   build_ref_1031()
	call_c   build_ref_1033()
	call_c   build_ref_1034()
	call_c   build_ref_1035()
	call_c   build_ref_1032()
	call_c   build_ref_1036()
	call_c   build_ref_1037()
	call_c   build_ref_1038()
	call_c   build_ref_1039()
	call_c   build_ref_997()
	call_c   build_ref_998()
	call_c   build_ref_999()
	call_c   build_ref_991()
	call_c   build_ref_992()
	call_c   build_ref_993()
	call_c   build_ref_995()
	call_c   build_ref_996()
	call_c   build_ref_994()
	call_c   build_ref_898()
	call_c   build_ref_900()
	call_c   build_ref_899()
	call_c   build_ref_783()
	call_c   build_ref_784()
	call_c   build_ref_785()
	call_c   build_ref_786()
	call_c   build_ref_778()
	call_c   build_ref_779()
	call_c   build_ref_780()
	call_c   build_ref_782()
	call_c   build_ref_781()
	call_c   build_ref_776()
	call_c   build_ref_330()
	call_c   build_ref_777()
	call_c   build_ref_760()
	call_c   build_ref_696()
	call_c   build_ref_695()
	call_c   build_ref_698()
	call_c   build_ref_697()
	call_c   build_ref_700()
	call_c   build_ref_699()
	call_c   build_ref_628()
	call_c   build_ref_627()
	call_c   build_ref_620()
	call_c   build_ref_629()
	call_c   build_ref_607()
	call_c   build_ref_606()
	call_c   build_ref_609()
	call_c   build_ref_601()
	call_c   build_ref_600()
	call_c   build_ref_603()
	call_c   build_ref_410()
	call_c   build_ref_545()
	call_c   build_ref_551()
	call_c   build_ref_546()
	call_c   build_ref_435()
	call_c   build_ref_434()
	call_c   build_ref_436()
	call_c   build_ref_437()
	call_c   build_ref_444()
	call_c   build_ref_445()
	call_c   build_ref_427()
	call_c   build_ref_426()
	call_c   build_ref_428()
	call_c   build_ref_432()
	call_c   build_ref_433()
	call_c   build_ref_342()
	call_c   build_ref_341()
	call_c   build_ref_343()
	call_c   build_ref_281()
	call_c   build_ref_284()
	call_c   build_ref_293()
	call_c   build_ref_287()
	call_c   build_ref_286()
	call_c   build_ref_285()
	call_c   build_ref_289()
	call_c   build_ref_290()
	call_c   build_ref_291()
	call_c   build_ref_292()
	call_c   build_ref_288()
	call_c   build_ref_261()
	call_c   build_ref_294()
	call_c   build_ref_283()
	call_c   build_ref_282()
	call_c   build_ref_295()
	call_c   build_ref_274()
	call_c   build_ref_279()
	call_c   build_ref_278()
	call_c   build_ref_280()
	call_c   build_ref_273()
	call_c   build_ref_237()
	call_c   build_ref_236()
	call_c   build_ref_228()
	call_c   build_ref_229()
	call_c   build_ref_162()
	call_c   build_ref_161()
	call_c   build_ref_164()
	call_c   build_ref_163()
	call_c   build_ref_165()
	call_c   build_ref_166()
	call_c   build_ref_167()
	call_c   build_ref_149()
	call_c   build_ref_151()
	call_c   build_ref_150()
	call_c   build_ref_147()
	call_c   build_ref_146()
	call_c   build_ref_148()
	call_c   build_ref_81()
	call_c   build_ref_82()
	call_c   build_ref_43()
	c_ret

long local ref[1449]
long local seed[223]

long local _initialization

c_code global initialization_dyalog_tag_5Fcompile
	call_c   Dyam_Check_And_Update_Initialization(_initialization)
	fail_c_ret
	call_c   initialization_dyalog_reader()
	call_c   initialization_dyalog_maker()
	call_c   initialization_dyalog_tag_5Fmaker()
	call_c   initialization_dyalog_tag_5Fnormalize()
	call_c   initialization_dyalog_format()
	call_c   build_init_pool()
	call_c   build_viewers()
	call_c   load()
	c_ret

