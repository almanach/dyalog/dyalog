/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 1998, 2011 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  code.pl -- Code cleaning
 *
 * ----------------------------------------------------------------
 * Description
 * 
 * ----------------------------------------------------------------
 */

:-include 'header.pl'.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Reducing code redirections

:-extensional redirect_jump/1.

:-std_prolog code_optimize/3.
:-std_prolog label_code1/3.
:-std_prolog label_code2/3.
:-std_prolog code_refs/3.
	
redirect_jump(noop).
redirect_jump(fail).
redirect_jump(succeed).
redirect_jump(pl_jump(_,_)).

jump_code(Code,Jump) :-
	code_optimize(Code,Optimized_Code,Length),
	( redirect_jump(Optimized_Code) ->
	    Jump = Optimized_Code
	;
	    label_code2(Optimized_Code,Length,Fun),
	    Jump = pl_jump(Fun,[])
	),
	true
	.

choice_code(Code,C1,choice(Fun) :> C1) :-
	code_optimize(remove_choice :> Code,Optimized_Code,Length),
	( Optimized_Code = pl_jump(Fun,[]) ->
	    jump_decr(Fun)
	;   
	    label_code2(Optimized_Code,Length,Fun)
	),
	true
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Code Registering

label_code(Code,L) :-
        code_optimize(Code,Optimized_Code,Length),
	( Optimized_Code = pl_jump(Fun,[]) ->
	    jump_decr(Fun),
	    L=Optimized_Code
	;   Optimized_Code == noop ->
	    L=noop
	;   
	    label_code2(Optimized_Code,Length,L)
	),
	true
	.

label_code2(Optimized_Code,Length,L) :-
	( var(L) -> L = '$fun'(V,0,M) ; L = '$fun'(V,_,M)),
	(   Length < 20,
	    recorded( code(Length,Optimized_Code,L) )
	xor update_counter(code,V),
	    code_refs(Optimized_Code,LRefs,Ins),
	    mutable(M,'$fun_info'{ code=> Optimized_Code,
				   refs=> LRefs,
				   ins=> Ins
				 }),
	    (	Length >= 20 xor record( code(Length,Optimized_Code,L) ))
	)
	.

jump_inc(Id) :-
	( Id = '$fun'(_,0,_) ->
	    update_counter( jump(Id), _ )
	;   
	    true
	)
	.

jump_value(Id,V) :-
	value_counter( jump(Id), V)
	.

jump_decr(Id) :-
	( recorded( counter(jump(Id),M) ) ->
	    mutable_read(M,V),
	    W is V-1,
	    mutable(M,W)
	;
	    true
	)
	.
