while(<>){
    if ($fun && /^\s*(pl_ret|pl_jump|pl_fail)/) {
	print <<HH ;
    call_c puts("\\nEXIT $fun $ARGV as $1\\n")
    call_c Flush_Output_0()
HH
    $fun = '';
    }
    if (/Dyam_Choice\((\w+)\)/) {
	s/Dyam_Choice\((\w+)\)/Dyam_Choice($1\_FAIL)/;
	print <<HH ;
    call_c puts("\\nSETFAIL $1 $ARGV\\n")
    call_c Flush_Output_0()
HH
    }
    if (/Dyam_Update_Choice\((\w+)\)/) {
	s/Dyam_Update_Choice\((\w+)\)/Dyam_Update_Choice($1\_FAIL)/;
	print <<HH ;
    call_c puts("\\nSETFAIL $1 $ARGV\\n")
    call_c Flush_Output_0()
HH
    }
    if (/Dyam_Deallocate/) {
	print <<HH ;
    call_c puts("\\nDEALLOCATE in $fun $ARGV\\n")
    call_c Flush_Output_0()
HH
    }
    if (/Dyam_Allocate/) {
	print <<HH ;
    call_c puts("\\nALLOCATE in $fun $ARGV\\n")
    call_c Flush_Output_0()
HH
    }
    if (/Dyam_Remove_Choice/) {
	print <<HH ;
    call_c puts("\\nREMOVE in $fun $ARGV\\n")
    call_c Flush_Output_0()
HH
    }
    print;
    if (/^pl_code\s+(local|global)\s+(\w+)/){
	$fun = $2;
	print <<HH ;
    call_c puts("\\nENTER $fun $ARGV\\n")
    pl_jump $fun\_IN()
$fun\_FAIL:
    call_c puts("\\nFAILTO $fun $ARGV\\n")
$fun\_IN:
    call_c Flush_Output_0()
HH
      }

    if (0 && /^c_code\s+(local|global)\s+(\w+)/){
	$fun = $2;
	print <<HH ;
    call_c puts("\\nENTER C $fun\\n")
    call_c Flush_Output_0()
HH
    }
    
    if (/^(\w+)\s*:/){
	$label = $1;
	print <<HH ;
    call_c puts("\\nENTER $fun:$label $ARGV\\n")
    pl_jump $label\_IN()
$label\_FAIL:
    call_c puts("\\nFAILTO $fun:$label $ARGV\\n")
$label\_IN:
    call_c Flush_Output_0()
HH
    }
}
