;; Compiler: DyALog 1.14.0
;; File "tag_dyn.pl"


;;------------------ LOADING ------------

c_code local load
	call_c   Dyam_Loading_Set()
	pl_call  fun18(&seed[24],0)
	pl_call  fun18(&seed[13],0)
	pl_call  fun18(&seed[20],0)
	pl_call  fun18(&seed[15],0)
	pl_call  fun18(&seed[30],0)
	pl_call  fun18(&seed[19],0)
	pl_call  fun18(&seed[9],0)
	pl_call  fun18(&seed[27],0)
	pl_call  fun18(&seed[12],0)
	pl_call  fun18(&seed[31],0)
	pl_call  fun18(&seed[21],0)
	pl_call  fun18(&seed[10],0)
	pl_call  fun18(&seed[25],0)
	pl_call  fun18(&seed[11],0)
	pl_call  fun18(&seed[7],0)
	pl_call  fun18(&seed[17],0)
	pl_call  fun18(&seed[23],0)
	pl_call  fun18(&seed[26],0)
	pl_call  fun18(&seed[0],0)
	pl_call  fun18(&seed[22],0)
	pl_call  fun18(&seed[33],0)
	pl_call  fun18(&seed[28],0)
	pl_call  fun18(&seed[8],0)
	pl_call  fun18(&seed[36],0)
	pl_call  fun18(&seed[35],0)
	pl_call  fun18(&seed[29],0)
	pl_call  fun18(&seed[2],0)
	pl_call  fun18(&seed[1],0)
	pl_call  fun18(&seed[34],0)
	pl_call  fun18(&seed[4],0)
	pl_call  fun18(&seed[3],0)
	pl_call  fun18(&seed[37],0)
	pl_call  fun18(&seed[6],0)
	pl_call  fun18(&seed[14],0)
	pl_call  fun18(&seed[5],0)
	pl_call  fun18(&seed[18],0)
	pl_call  fun18(&seed[16],0)
	pl_call  fun18(&seed[32],0)
	pl_call  fun18(&seed[38],0)
	pl_call  fun2(&seed[39],0)
	call_c   Dyam_Loading_Reset()
	c_ret

pl_code global pred_duplicate_term_4
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(4)
	call_c   Dyam_Choice(fun101)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(1),V(2))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(3),V(4))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code global pred_sa_env_normalize_2
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Reg_Unify(2,&ref[225])
	fail_ret
	call_c   Dyam_Choice(fun61)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_variable(V(1))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun59)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(2),I(0))
	fail_ret
	call_c   Dyam_Unify(V(4),I(0))
	fail_ret
	pl_call  Object_1(&ref[224])
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(1),V(3))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code global pred_add_viewer_2
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(2)
	call_c   DYAM_Numbervars_3(V(1),N(0),V(5))
	fail_ret
	call_c   Dyam_Reg_Load(0,V(1))
	move     V(3), R(2)
	move     S(5), R(3)
	pl_call  pred_label_term_2()
	call_c   Dyam_Reg_Load(0,V(2))
	move     V(4), R(2)
	move     S(5), R(3)
	pl_call  pred_label_term_2()
	move     &ref[38], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Deallocate(1)
	pl_jump  pred_record_without_doublon_1()

pl_code global pred_sa_env_extension_4
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Reg_Unify(2,&ref[25])
	fail_ret
	call_c   Dyam_Reg_Unify(4,V(4))
	fail_ret
	call_c   Dyam_Reg_Unify(6,V(5))
	fail_ret
	call_c   Dyam_Reg_Load(0,V(1))
	move     &ref[24], R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_sa_env_normalize_2()


;;------------------ VIEWERS ------------

c_code local build_viewers
	call_c   Dyam_Load_Viewer(&ref[11],&ref[12])
	call_c   Dyam_Load_Viewer(&ref[7],&ref[8])
	call_c   Dyam_Load_Viewer(&ref[3],&ref[4])
	c_ret

c_code local build_seed_39
	ret_reg &seed[39]
	call_c   build_ref_13()
	call_c   build_ref_14()
	call_c   Dyam_Seed_Start(&ref[13],&ref[14],&ref[14],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[39]
	c_ret

pl_code local fun0
	pl_ret

;; TERM 14: null_escape(sa_escape{start=> [], end=> []})
c_code local build_ref_14
	ret_reg &ref[14]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_466()
	call_c   Dyam_Create_Binary(&ref[466],I(0),I(0))
	move_ret R(0)
	call_c   build_ref_465()
	call_c   Dyam_Create_Unary(&ref[465],R(0))
	move_ret ref[14]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 465: null_escape
c_code local build_ref_465
	ret_reg &ref[465]
	call_c   Dyam_Create_Atom("null_escape")
	move_ret ref[465]
	c_ret

;; TERM 466: sa_escape!'$ft'
c_code local build_ref_466
	ret_reg &ref[466]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_469()
	call_c   Dyam_Create_List(&ref[469],I(0))
	move_ret R(0)
	call_c   build_ref_468()
	call_c   Dyam_Create_List(&ref[468],R(0))
	move_ret R(0)
	call_c   build_ref_467()
	call_c   Dyam_Create_Atom_Module("$ft",&ref[467])
	move_ret ref[466]
	call_c   DYAM_Feature_2(&ref[467],R(0))
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 467: sa_escape
c_code local build_ref_467
	ret_reg &ref[467]
	call_c   Dyam_Create_Atom("sa_escape")
	move_ret ref[467]
	c_ret

;; TERM 468: start
c_code local build_ref_468
	ret_reg &ref[468]
	call_c   Dyam_Create_Atom("start")
	move_ret ref[468]
	c_ret

;; TERM 469: end
c_code local build_ref_469
	ret_reg &ref[469]
	call_c   Dyam_Create_Atom("end")
	move_ret ref[469]
	c_ret

;; TERM 13: '*DATABASE*'
c_code local build_ref_13
	ret_reg &ref[13]
	call_c   Dyam_Create_Atom("*DATABASE*")
	move_ret ref[13]
	c_ret

c_code local build_seed_38
	ret_reg &seed[38]
	call_c   build_ref_15()
	call_c   build_ref_17()
	call_c   Dyam_Seed_Start(&ref[15],&ref[17],I(0),fun8,1)
	call_c   build_ref_19()
	call_c   Dyam_Seed_Add_Comp(&ref[19],fun7,0)
	call_c   Dyam_Seed_End()
	move_ret seed[38]
	c_ret

;; TERM 19: '*CITEM*'('call_tag_subsumption/1', _A)
c_code local build_ref_19
	ret_reg &ref[19]
	call_c   build_ref_18()
	call_c   build_ref_9()
	call_c   Dyam_Create_Binary(&ref[18],&ref[9],V(0))
	move_ret ref[19]
	c_ret

;; TERM 9: 'call_tag_subsumption/1'
c_code local build_ref_9
	ret_reg &ref[9]
	call_c   Dyam_Create_Atom("call_tag_subsumption/1")
	move_ret ref[9]
	c_ret

;; TERM 18: '*CITEM*'
c_code local build_ref_18
	ret_reg &ref[18]
	call_c   Dyam_Create_Atom("*CITEM*")
	move_ret ref[18]
	c_ret

;; TERM 26: yes
c_code local build_ref_26
	ret_reg &ref[26]
	call_c   Dyam_Create_Atom("yes")
	move_ret ref[26]
	c_ret

c_code local build_seed_40
	ret_reg &seed[40]
	call_c   build_ref_0()
	call_c   build_ref_22()
	call_c   Dyam_Seed_Start(&ref[0],&ref[22],&ref[22],fun3,1)
	call_c   build_ref_23()
	call_c   Dyam_Seed_Add_Comp(&ref[23],&ref[22],0)
	call_c   Dyam_Seed_End()
	move_ret seed[40]
	c_ret

pl_code local fun3
	pl_jump  Complete(0,0)

;; TERM 23: '*RITEM*'(_A, return(_B)) :> '$$HOLE$$'
c_code local build_ref_23
	ret_reg &ref[23]
	call_c   build_ref_22()
	call_c   Dyam_Create_Binary(I(9),&ref[22],I(7))
	move_ret ref[23]
	c_ret

;; TERM 22: '*RITEM*'(_A, return(_B))
c_code local build_ref_22
	ret_reg &ref[22]
	call_c   build_ref_0()
	call_c   build_ref_2()
	call_c   Dyam_Create_Binary(&ref[0],V(0),&ref[2])
	move_ret ref[22]
	c_ret

;; TERM 2: return(_B)
c_code local build_ref_2
	ret_reg &ref[2]
	call_c   build_ref_470()
	call_c   Dyam_Create_Unary(&ref[470],V(1))
	move_ret ref[2]
	c_ret

;; TERM 470: return
c_code local build_ref_470
	ret_reg &ref[470]
	call_c   Dyam_Create_Atom("return")
	move_ret ref[470]
	c_ret

;; TERM 0: '*RITEM*'
c_code local build_ref_0
	ret_reg &ref[0]
	call_c   Dyam_Create_Atom("*RITEM*")
	move_ret ref[0]
	c_ret

pl_code local fun2
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Choice(fun1)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Subsume(R(0))
	fail_ret
	call_c   Dyam_Cut()
	pl_ret

long local pool_fun5[2]=[1,build_seed_40]

long local pool_fun6[3]=[65537,build_ref_26,pool_fun5]

pl_code local fun6
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(1),&ref[26])
	fail_ret
fun5:
	call_c   Dyam_Deallocate()
	pl_jump  fun2(&seed[40],2)


;; TERM 20: subsumption(tag(variance))
c_code local build_ref_20
	ret_reg &ref[20]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_472()
	call_c   build_ref_21()
	call_c   Dyam_Create_Unary(&ref[472],&ref[21])
	move_ret R(0)
	call_c   build_ref_471()
	call_c   Dyam_Create_Unary(&ref[471],R(0))
	move_ret ref[20]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 471: subsumption
c_code local build_ref_471
	ret_reg &ref[471]
	call_c   Dyam_Create_Atom("subsumption")
	move_ret ref[471]
	c_ret

;; TERM 21: variance
c_code local build_ref_21
	ret_reg &ref[21]
	call_c   Dyam_Create_Atom("variance")
	move_ret ref[21]
	c_ret

;; TERM 472: tag
c_code local build_ref_472
	ret_reg &ref[472]
	call_c   Dyam_Create_Atom("tag")
	move_ret ref[472]
	c_ret

long local pool_fun7[6]=[131075,build_ref_19,build_ref_20,build_ref_21,pool_fun6,pool_fun5]

pl_code local fun7
	call_c   Dyam_Pool(pool_fun7)
	call_c   Dyam_Unify_Item(&ref[19])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun6)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[20])
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(1),&ref[21])
	fail_ret
	pl_jump  fun5()

pl_code local fun8
	pl_jump  Apply(0,0)

;; TERM 17: '*FIRST*'('call_tag_subsumption/1') :> []
c_code local build_ref_17
	ret_reg &ref[17]
	call_c   build_ref_16()
	call_c   Dyam_Create_Binary(I(9),&ref[16],I(0))
	move_ret ref[17]
	c_ret

;; TERM 16: '*FIRST*'('call_tag_subsumption/1')
c_code local build_ref_16
	ret_reg &ref[16]
	call_c   build_ref_15()
	call_c   build_ref_9()
	call_c   Dyam_Create_Unary(&ref[15],&ref[9])
	move_ret ref[16]
	c_ret

;; TERM 15: '*FIRST*'
c_code local build_ref_15
	ret_reg &ref[15]
	call_c   Dyam_Create_Atom("*FIRST*")
	move_ret ref[15]
	c_ret

c_code local build_seed_32
	ret_reg &seed[32]
	call_c   build_ref_27()
	call_c   build_ref_31()
	call_c   Dyam_Seed_Start(&ref[27],&ref[31],I(0),fun0,1)
	call_c   build_ref_32()
	call_c   Dyam_Seed_Add_Comp(&ref[32],fun11,0)
	call_c   Dyam_Seed_End()
	move_ret seed[32]
	c_ret

;; TERM 32: '*PROLOG-ITEM*'{top=> unfold((_B : _C), _D, _E, _F, _G), cont=> _A}
c_code local build_ref_32
	ret_reg &ref[32]
	call_c   build_ref_473()
	call_c   build_ref_29()
	call_c   Dyam_Create_Binary(&ref[473],&ref[29],V(0))
	move_ret ref[32]
	c_ret

;; TERM 29: unfold((_B : _C), _D, _E, _F, _G)
c_code local build_ref_29
	ret_reg &ref[29]
	call_c   build_ref_474()
	call_c   build_ref_394()
	call_c   Dyam_Term_Start(&ref[474],5)
	call_c   Dyam_Term_Arg(&ref[394])
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[29]
	c_ret

;; TERM 394: _B : _C
c_code local build_ref_394
	ret_reg &ref[394]
	call_c   build_ref_475()
	call_c   Dyam_Create_Binary(&ref[475],V(1),V(2))
	move_ret ref[394]
	c_ret

;; TERM 475: :
c_code local build_ref_475
	ret_reg &ref[475]
	call_c   Dyam_Create_Atom(":")
	move_ret ref[475]
	c_ret

;; TERM 474: unfold
c_code local build_ref_474
	ret_reg &ref[474]
	call_c   Dyam_Create_Atom("unfold")
	move_ret ref[474]
	c_ret

;; TERM 473: '*PROLOG-ITEM*'!'$ft'
c_code local build_ref_473
	ret_reg &ref[473]
	call_c   build_ref_33()
	call_c   Dyam_Create_Atom_Module("$ft",&ref[33])
	move_ret ref[473]
	c_ret

;; TERM 33: '*PROLOG-ITEM*'
c_code local build_ref_33
	ret_reg &ref[33]
	call_c   Dyam_Create_Atom("*PROLOG-ITEM*")
	move_ret ref[33]
	c_ret

c_code local build_seed_41
	ret_reg &seed[41]
	call_c   build_ref_33()
	call_c   build_ref_34()
	call_c   Dyam_Seed_Start(&ref[33],&ref[34],I(0),fun3,1)
	call_c   build_ref_37()
	call_c   Dyam_Seed_Add_Comp(&ref[37],&ref[34],0)
	call_c   Dyam_Seed_End()
	move_ret seed[41]
	c_ret

;; TERM 37: '*PROLOG-FIRST*'(unfold(_C, _D, _E, _F, _G)) :> '$$HOLE$$'
c_code local build_ref_37
	ret_reg &ref[37]
	call_c   build_ref_36()
	call_c   Dyam_Create_Binary(I(9),&ref[36],I(7))
	move_ret ref[37]
	c_ret

;; TERM 36: '*PROLOG-FIRST*'(unfold(_C, _D, _E, _F, _G))
c_code local build_ref_36
	ret_reg &ref[36]
	call_c   build_ref_28()
	call_c   build_ref_35()
	call_c   Dyam_Create_Unary(&ref[28],&ref[35])
	move_ret ref[36]
	c_ret

;; TERM 35: unfold(_C, _D, _E, _F, _G)
c_code local build_ref_35
	ret_reg &ref[35]
	call_c   build_ref_474()
	call_c   Dyam_Term_Start(&ref[474],5)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[35]
	c_ret

;; TERM 28: '*PROLOG-FIRST*'
c_code local build_ref_28
	ret_reg &ref[28]
	call_c   Dyam_Create_Atom("*PROLOG-FIRST*")
	move_ret ref[28]
	c_ret

;; TERM 34: '*PROLOG-ITEM*'{top=> unfold(_C, _D, _E, _F, _G), cont=> _A}
c_code local build_ref_34
	ret_reg &ref[34]
	call_c   build_ref_473()
	call_c   build_ref_35()
	call_c   Dyam_Create_Binary(&ref[473],&ref[35],V(0))
	move_ret ref[34]
	c_ret

pl_code local fun10
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Light_Object(R(0))
	pl_jump  Schedule_Prolog()

long local pool_fun11[3]=[2,build_ref_32,build_seed_41]

pl_code local fun11
	call_c   Dyam_Pool(pool_fun11)
	call_c   Dyam_Unify_Item(&ref[32])
	fail_ret
	pl_jump  fun10(&seed[41],12)

;; TERM 31: '*PROLOG-FIRST*'(unfold((_B : _C), _D, _E, _F, _G)) :> []
c_code local build_ref_31
	ret_reg &ref[31]
	call_c   build_ref_30()
	call_c   Dyam_Create_Binary(I(9),&ref[30],I(0))
	move_ret ref[31]
	c_ret

;; TERM 30: '*PROLOG-FIRST*'(unfold((_B : _C), _D, _E, _F, _G))
c_code local build_ref_30
	ret_reg &ref[30]
	call_c   build_ref_28()
	call_c   build_ref_29()
	call_c   Dyam_Create_Unary(&ref[28],&ref[29])
	move_ret ref[30]
	c_ret

;; TERM 27: '*PROLOG_FIRST*'
c_code local build_ref_27
	ret_reg &ref[27]
	call_c   Dyam_Create_Atom("*PROLOG_FIRST*")
	move_ret ref[27]
	c_ret

c_code local build_seed_16
	ret_reg &seed[16]
	call_c   build_ref_15()
	call_c   build_ref_41()
	call_c   Dyam_Seed_Start(&ref[15],&ref[41],I(0),fun8,1)
	call_c   build_ref_42()
	call_c   Dyam_Seed_Add_Comp(&ref[42],fun26,0)
	call_c   Dyam_Seed_End()
	move_ret seed[16]
	c_ret

;; TERM 42: '*CITEM*'('call_sa_foot_wrapper_install/4'(_B), _A)
c_code local build_ref_42
	ret_reg &ref[42]
	call_c   build_ref_18()
	call_c   build_ref_39()
	call_c   Dyam_Create_Binary(&ref[18],&ref[39],V(0))
	move_ret ref[42]
	c_ret

;; TERM 39: 'call_sa_foot_wrapper_install/4'(_B)
c_code local build_ref_39
	ret_reg &ref[39]
	call_c   build_ref_476()
	call_c   Dyam_Create_Unary(&ref[476],V(1))
	move_ret ref[39]
	c_ret

;; TERM 476: 'call_sa_foot_wrapper_install/4'
c_code local build_ref_476
	ret_reg &ref[476]
	call_c   Dyam_Create_Atom("call_sa_foot_wrapper_install/4")
	move_ret ref[476]
	c_ret

;; TERM 124: 'no tabular wrapper for ~w'
c_code local build_ref_124
	ret_reg &ref[124]
	call_c   Dyam_Create_Atom("no tabular wrapper for ~w")
	move_ret ref[124]
	c_ret

;; TERM 125: [_B]
c_code local build_ref_125
	ret_reg &ref[125]
	call_c   Dyam_Create_List(V(1),I(0))
	move_ret ref[125]
	c_ret

;; TERM 44: null_escape(_K)
c_code local build_ref_44
	ret_reg &ref[44]
	call_c   build_ref_465()
	call_c   Dyam_Create_Unary(&ref[465],V(10))
	move_ret ref[44]
	c_ret

;; TERM 45: [_L|_M]
c_code local build_ref_45
	ret_reg &ref[45]
	call_c   Dyam_Create_List(V(11),V(12))
	move_ret ref[45]
	c_ret

;; TERM 46: [_L|_O]
c_code local build_ref_46
	ret_reg &ref[46]
	call_c   Dyam_Create_List(V(11),V(14))
	move_ret ref[46]
	c_ret

;; TERM 47: [_P|_M]
c_code local build_ref_47
	ret_reg &ref[47]
	call_c   Dyam_Create_List(V(15),V(12))
	move_ret ref[47]
	c_ret

;; TERM 48: sa_env{mspop=> _L, aspop=> _P, holes=> [], escape=> _K}
c_code local build_ref_48
	ret_reg &ref[48]
	call_c   build_ref_477()
	call_c   Dyam_Term_Start(&ref[477],4)
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_End()
	move_ret ref[48]
	c_ret

;; TERM 477: sa_env!'$ft'
c_code local build_ref_477
	ret_reg &ref[477]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_482()
	call_c   Dyam_Create_List(&ref[482],I(0))
	move_ret R(0)
	call_c   build_ref_481()
	call_c   Dyam_Create_List(&ref[481],R(0))
	move_ret R(0)
	call_c   build_ref_480()
	call_c   Dyam_Create_List(&ref[480],R(0))
	move_ret R(0)
	call_c   build_ref_479()
	call_c   Dyam_Create_List(&ref[479],R(0))
	move_ret R(0)
	call_c   build_ref_478()
	call_c   Dyam_Create_Atom_Module("$ft",&ref[478])
	move_ret ref[477]
	call_c   DYAM_Feature_2(&ref[478],R(0))
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 478: sa_env
c_code local build_ref_478
	ret_reg &ref[478]
	call_c   Dyam_Create_Atom("sa_env")
	move_ret ref[478]
	c_ret

;; TERM 479: mspop
c_code local build_ref_479
	ret_reg &ref[479]
	call_c   Dyam_Create_Atom("mspop")
	move_ret ref[479]
	c_ret

;; TERM 480: aspop
c_code local build_ref_480
	ret_reg &ref[480]
	call_c   Dyam_Create_Atom("aspop")
	move_ret ref[480]
	c_ret

;; TERM 481: holes
c_code local build_ref_481
	ret_reg &ref[481]
	call_c   Dyam_Create_Atom("holes")
	move_ret ref[481]
	c_ret

;; TERM 482: escape
c_code local build_ref_482
	ret_reg &ref[482]
	call_c   Dyam_Create_Atom("escape")
	move_ret ref[482]
	c_ret

;; TERM 49: sa_env{mspop=> _T, aspop=> _U, holes=> _V, escape=> _W}
c_code local build_ref_49
	ret_reg &ref[49]
	call_c   build_ref_477()
	call_c   Dyam_Term_Start(&ref[477],4)
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_Arg(V(21))
	call_c   Dyam_Term_Arg(V(22))
	call_c   Dyam_Term_End()
	move_ret ref[49]
	c_ret

c_code local build_seed_44
	ret_reg &seed[44]
	call_c   build_ref_33()
	call_c   build_ref_64()
	call_c   Dyam_Seed_Start(&ref[33],&ref[64],I(0),fun3,1)
	call_c   build_ref_67()
	call_c   Dyam_Seed_Add_Comp(&ref[67],&ref[64],0)
	call_c   Dyam_Seed_End()
	move_ret seed[44]
	c_ret

;; TERM 67: '*PROLOG-FIRST*'(unfold((_H :> deallocate_layer :> deallocate :> succeed), _R, _I, _X, _S)) :> '$$HOLE$$'
c_code local build_ref_67
	ret_reg &ref[67]
	call_c   build_ref_66()
	call_c   Dyam_Create_Binary(I(9),&ref[66],I(7))
	move_ret ref[67]
	c_ret

;; TERM 66: '*PROLOG-FIRST*'(unfold((_H :> deallocate_layer :> deallocate :> succeed), _R, _I, _X, _S))
c_code local build_ref_66
	ret_reg &ref[66]
	call_c   build_ref_28()
	call_c   build_ref_65()
	call_c   Dyam_Create_Unary(&ref[28],&ref[65])
	move_ret ref[66]
	c_ret

;; TERM 65: unfold((_H :> deallocate_layer :> deallocate :> succeed), _R, _I, _X, _S)
c_code local build_ref_65
	ret_reg &ref[65]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_354()
	call_c   Dyam_Create_Binary(I(9),V(7),&ref[354])
	move_ret R(0)
	call_c   build_ref_474()
	call_c   Dyam_Term_Start(&ref[474],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(23))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_End()
	move_ret ref[65]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 354: deallocate_layer :> deallocate :> succeed
c_code local build_ref_354
	ret_reg &ref[354]
	call_c   build_ref_350()
	call_c   build_ref_353()
	call_c   Dyam_Create_Binary(I(9),&ref[350],&ref[353])
	move_ret ref[354]
	c_ret

;; TERM 353: deallocate :> succeed
c_code local build_ref_353
	ret_reg &ref[353]
	call_c   build_ref_351()
	call_c   build_ref_352()
	call_c   Dyam_Create_Binary(I(9),&ref[351],&ref[352])
	move_ret ref[353]
	c_ret

;; TERM 352: succeed
c_code local build_ref_352
	ret_reg &ref[352]
	call_c   Dyam_Create_Atom("succeed")
	move_ret ref[352]
	c_ret

;; TERM 351: deallocate
c_code local build_ref_351
	ret_reg &ref[351]
	call_c   Dyam_Create_Atom("deallocate")
	move_ret ref[351]
	c_ret

;; TERM 350: deallocate_layer
c_code local build_ref_350
	ret_reg &ref[350]
	call_c   Dyam_Create_Atom("deallocate_layer")
	move_ret ref[350]
	c_ret

;; TERM 64: '*PROLOG-ITEM*'{top=> unfold((_H :> deallocate_layer :> deallocate :> succeed), _R, _I, _X, _S), cont=> '$CLOSURE'('$fun'(14, 0, 1138337044), '$TUPPLE'(35163198461500))}
c_code local build_ref_64
	ret_reg &ref[64]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,277454944)
	move_ret R(0)
	call_c   Dyam_Closure_Aux(fun14,R(0))
	move_ret R(0)
	call_c   build_ref_473()
	call_c   build_ref_65()
	call_c   Dyam_Create_Binary(&ref[473],&ref[65],R(0))
	move_ret ref[64]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 50: [_P,_A1,_W|_M]
c_code local build_ref_50
	ret_reg &ref[50]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(22),V(12))
	move_ret R(0)
	call_c   Dyam_Create_List(V(26),R(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(15),R(0))
	move_ret ref[50]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 51: [_B1,_C1,_D1|_E1]
c_code local build_ref_51
	ret_reg &ref[51]
	call_c   Dyam_Create_Tupple(27,29,V(30))
	move_ret ref[51]
	c_ret

;; TERM 52: [_B1|_E1]
c_code local build_ref_52
	ret_reg &ref[52]
	call_c   Dyam_Create_List(V(27),V(30))
	move_ret ref[52]
	c_ret

c_code local build_seed_42
	ret_reg &seed[42]
	call_c   build_ref_53()
	call_c   build_ref_55()
	call_c   Dyam_Seed_Start(&ref[53],&ref[55],I(0),fun3,1)
	call_c   build_ref_56()
	call_c   Dyam_Seed_Add_Comp(&ref[56],&ref[55],0)
	call_c   Dyam_Seed_End()
	move_ret seed[42]
	c_ret

;; TERM 56: '*GUARD*'(std_prolog_unif_args(_N, 0, _G1, _X, _J)) :> '$$HOLE$$'
c_code local build_ref_56
	ret_reg &ref[56]
	call_c   build_ref_55()
	call_c   Dyam_Create_Binary(I(9),&ref[55],I(7))
	move_ret ref[56]
	c_ret

;; TERM 55: '*GUARD*'(std_prolog_unif_args(_N, 0, _G1, _X, _J))
c_code local build_ref_55
	ret_reg &ref[55]
	call_c   build_ref_53()
	call_c   build_ref_54()
	call_c   Dyam_Create_Unary(&ref[53],&ref[54])
	move_ret ref[55]
	c_ret

;; TERM 54: std_prolog_unif_args(_N, 0, _G1, _X, _J)
c_code local build_ref_54
	ret_reg &ref[54]
	call_c   build_ref_483()
	call_c   Dyam_Term_Start(&ref[483],5)
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(N(0))
	call_c   Dyam_Term_Arg(V(32))
	call_c   Dyam_Term_Arg(V(23))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret ref[54]
	c_ret

;; TERM 483: std_prolog_unif_args
c_code local build_ref_483
	ret_reg &ref[483]
	call_c   Dyam_Create_Atom("std_prolog_unif_args")
	move_ret ref[483]
	c_ret

;; TERM 53: '*GUARD*'
c_code local build_ref_53
	ret_reg &ref[53]
	call_c   Dyam_Create_Atom("*GUARD*")
	move_ret ref[53]
	c_ret

;; TERM 60: allocate :> allocate_layer :> _G1
c_code local build_ref_60
	ret_reg &ref[60]
	call_c   build_ref_57()
	call_c   build_ref_59()
	call_c   Dyam_Create_Binary(I(9),&ref[57],&ref[59])
	move_ret ref[60]
	c_ret

;; TERM 59: allocate_layer :> _G1
c_code local build_ref_59
	ret_reg &ref[59]
	call_c   build_ref_58()
	call_c   Dyam_Create_Binary(I(9),&ref[58],V(32))
	move_ret ref[59]
	c_ret

;; TERM 58: allocate_layer
c_code local build_ref_58
	ret_reg &ref[58]
	call_c   Dyam_Create_Atom("allocate_layer")
	move_ret ref[58]
	c_ret

;; TERM 57: allocate
c_code local build_ref_57
	ret_reg &ref[57]
	call_c   Dyam_Create_Atom("allocate")
	move_ret ref[57]
	c_ret

c_code local build_seed_43
	ret_reg &seed[43]
	call_c   build_ref_0()
	call_c   build_ref_62()
	call_c   Dyam_Seed_Start(&ref[0],&ref[62],&ref[62],fun3,1)
	call_c   build_ref_63()
	call_c   Dyam_Seed_Add_Comp(&ref[63],&ref[62],0)
	call_c   Dyam_Seed_End()
	move_ret seed[43]
	c_ret

;; TERM 63: '*RITEM*'(_A, return(_C, _D, _E)) :> '$$HOLE$$'
c_code local build_ref_63
	ret_reg &ref[63]
	call_c   build_ref_62()
	call_c   Dyam_Create_Binary(I(9),&ref[62],I(7))
	move_ret ref[63]
	c_ret

;; TERM 62: '*RITEM*'(_A, return(_C, _D, _E))
c_code local build_ref_62
	ret_reg &ref[62]
	call_c   build_ref_0()
	call_c   build_ref_61()
	call_c   Dyam_Create_Binary(&ref[0],V(0),&ref[61])
	move_ret ref[62]
	c_ret

;; TERM 61: return(_C, _D, _E)
c_code local build_ref_61
	ret_reg &ref[61]
	call_c   build_ref_470()
	call_c   Dyam_Term_Start(&ref[470],3)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[61]
	c_ret

pl_code local fun13
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Choice(fun12)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Subsume(R(0))
	fail_ret
	call_c   Dyam_Cut()
	pl_ret

long local pool_fun14[7]=[6,build_ref_50,build_ref_51,build_ref_52,build_seed_42,build_ref_60,build_seed_43]

pl_code local fun14
	call_c   Dyam_Pool(pool_fun14)
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(22))
	call_c   Dyam_Reg_Load(2,V(5))
	move     V(24), R(4)
	move     S(5), R(5)
	pl_call  pred_tag_numbervars_3()
	call_c   Dyam_Reg_Load(0,V(22))
	move     V(25), R(2)
	move     S(5), R(3)
	pl_call  pred_tupple_2()
	call_c   Dyam_Reg_Load(0,V(25))
	move     V(26), R(2)
	move     S(5), R(3)
	pl_call  pred_oset_tupple_2()
	move     &ref[50], R(0)
	move     S(5), R(1)
	move     &ref[51], R(2)
	move     S(5), R(3)
	move     I(0), R(4)
	move     0, R(5)
	move     V(31), R(6)
	move     S(5), R(7)
	pl_call  pred_duplicate_term_4()
	call_c   Dyam_Unify(V(3),&ref[52])
	fail_ret
	call_c   Dyam_Unify(V(4),V(29))
	fail_ret
	pl_call  fun10(&seed[42],1)
	move     &ref[60], R(0)
	move     S(5), R(1)
	move     V(2), R(2)
	move     S(5), R(3)
	pl_call  pred_label_code_2()
	call_c   Dyam_Reg_Load(0,V(2))
	pl_call  pred_register_init_pool_fun_1()
	call_c   Dyam_Deallocate()
	pl_jump  fun13(&seed[43],2)

long local pool_fun24[8]=[7,build_ref_44,build_ref_45,build_ref_46,build_ref_47,build_ref_48,build_ref_49,build_seed_44]

long local pool_fun25[4]=[65538,build_ref_124,build_ref_125,pool_fun24]

pl_code local fun25
	call_c   Dyam_Remove_Choice()
	move     &ref[124], R(0)
	move     0, R(1)
	move     &ref[125], R(2)
	move     S(5), R(3)
	pl_call  pred_internal_error_2()
fun24:
	call_c   Dyam_Reg_Load(0,V(6))
	move     V(8), R(2)
	move     S(5), R(3)
	pl_call  pred_tupple_2()
	move     V(9), R(0)
	move     S(5), R(1)
	pl_call  pred_null_tupple_1()
	pl_call  Object_1(&ref[44])
	call_c   Dyam_Unify(V(6),&ref[45])
	fail_ret
	call_c   Dyam_Unify(V(13),&ref[46])
	fail_ret
	call_c   Dyam_Unify(V(14),&ref[47])
	fail_ret
	call_c   Dyam_Reg_Load(0,V(15))
	call_c   Dyam_Reg_Load(2,V(5))
	move     V(16), R(4)
	move     S(5), R(5)
	pl_call  pred_tag_numbervars_3()
	call_c   Dyam_Unify(V(17),&ref[48])
	fail_ret
	call_c   Dyam_Unify(V(18),&ref[49])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_jump  fun10(&seed[44],12)


;; TERM 43: '*SA-FOOT-WRAPPER*'(_B, _F, _G, _H)
c_code local build_ref_43
	ret_reg &ref[43]
	call_c   build_ref_484()
	call_c   Dyam_Term_Start(&ref[484],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[43]
	c_ret

;; TERM 484: '*SA-FOOT-WRAPPER*'
c_code local build_ref_484
	ret_reg &ref[484]
	call_c   Dyam_Create_Atom("*SA-FOOT-WRAPPER*")
	move_ret ref[484]
	c_ret

long local pool_fun26[5]=[131074,build_ref_42,build_ref_43,pool_fun25,pool_fun24]

pl_code local fun26
	call_c   Dyam_Pool(pool_fun26)
	call_c   Dyam_Unify_Item(&ref[42])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun25)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[43])
	call_c   Dyam_Cut()
	pl_jump  fun24()

;; TERM 41: '*FIRST*'('call_sa_foot_wrapper_install/4'(_B)) :> []
c_code local build_ref_41
	ret_reg &ref[41]
	call_c   build_ref_40()
	call_c   Dyam_Create_Binary(I(9),&ref[40],I(0))
	move_ret ref[41]
	c_ret

;; TERM 40: '*FIRST*'('call_sa_foot_wrapper_install/4'(_B))
c_code local build_ref_40
	ret_reg &ref[40]
	call_c   build_ref_15()
	call_c   build_ref_39()
	call_c   Dyam_Create_Unary(&ref[15],&ref[39])
	move_ret ref[40]
	c_ret

c_code local build_seed_18
	ret_reg &seed[18]
	call_c   build_ref_27()
	call_c   build_ref_90()
	call_c   Dyam_Seed_Start(&ref[27],&ref[90],I(0),fun0,1)
	call_c   build_ref_91()
	call_c   Dyam_Seed_Add_Comp(&ref[91],fun15,0)
	call_c   Dyam_Seed_End()
	move_ret seed[18]
	c_ret

;; TERM 91: '*PROLOG-ITEM*'{top=> unfold('*SA-PROLOG-LAST*', _B, _C, call('Follow_Cont', [_D]), _B), cont=> _A}
c_code local build_ref_91
	ret_reg &ref[91]
	call_c   build_ref_473()
	call_c   build_ref_88()
	call_c   Dyam_Create_Binary(&ref[473],&ref[88],V(0))
	move_ret ref[91]
	c_ret

;; TERM 88: unfold('*SA-PROLOG-LAST*', _B, _C, call('Follow_Cont', [_D]), _B)
c_code local build_ref_88
	ret_reg &ref[88]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(3),I(0))
	move_ret R(0)
	call_c   build_ref_486()
	call_c   build_ref_487()
	call_c   Dyam_Create_Binary(&ref[486],&ref[487],R(0))
	move_ret R(0)
	call_c   build_ref_474()
	call_c   build_ref_485()
	call_c   Dyam_Term_Start(&ref[474],5)
	call_c   Dyam_Term_Arg(&ref[485])
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_End()
	move_ret ref[88]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 485: '*SA-PROLOG-LAST*'
c_code local build_ref_485
	ret_reg &ref[485]
	call_c   Dyam_Create_Atom("*SA-PROLOG-LAST*")
	move_ret ref[485]
	c_ret

;; TERM 487: 'Follow_Cont'
c_code local build_ref_487
	ret_reg &ref[487]
	call_c   Dyam_Create_Atom("Follow_Cont")
	move_ret ref[487]
	c_ret

;; TERM 486: call
c_code local build_ref_486
	ret_reg &ref[486]
	call_c   Dyam_Create_Atom("call")
	move_ret ref[486]
	c_ret

;; TERM 92: sa_env{mspop=> _E, aspop=> _F, holes=> _G, escape=> _H}
c_code local build_ref_92
	ret_reg &ref[92]
	call_c   build_ref_477()
	call_c   Dyam_Term_Start(&ref[477],4)
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[92]
	c_ret

long local pool_fun15[3]=[2,build_ref_91,build_ref_92]

pl_code local fun15
	call_c   Dyam_Pool(pool_fun15)
	call_c   Dyam_Unify_Item(&ref[91])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(1))
	move     &ref[92], R(2)
	move     S(5), R(3)
	pl_call  pred_sa_env_normalize_2()
	call_c   Dyam_Reg_Load(0,V(4))
	call_c   Dyam_Reg_Load(2,V(3))
	pl_call  pred_label_term_2()
	call_c   Dyam_Deallocate()
	pl_jump  Follow_Cont(V(0))

;; TERM 90: '*PROLOG-FIRST*'(unfold('*SA-PROLOG-LAST*', _B, _C, call('Follow_Cont', [_D]), _B)) :> []
c_code local build_ref_90
	ret_reg &ref[90]
	call_c   build_ref_89()
	call_c   Dyam_Create_Binary(I(9),&ref[89],I(0))
	move_ret ref[90]
	c_ret

;; TERM 89: '*PROLOG-FIRST*'(unfold('*SA-PROLOG-LAST*', _B, _C, call('Follow_Cont', [_D]), _B))
c_code local build_ref_89
	ret_reg &ref[89]
	call_c   build_ref_28()
	call_c   build_ref_88()
	call_c   Dyam_Create_Unary(&ref[28],&ref[88])
	move_ret ref[89]
	c_ret

c_code local build_seed_5
	ret_reg &seed[5]
	call_c   build_ref_102()
	call_c   build_ref_105()
	call_c   Dyam_Seed_Start(&ref[102],&ref[105],I(0),fun0,1)
	call_c   build_ref_104()
	call_c   Dyam_Seed_Add_Comp(&ref[104],fun19,0)
	call_c   Dyam_Seed_End()
	move_ret seed[5]
	c_ret

;; TERM 104: '*GUARD*'(reach_foot([tag_node{id=> _B, label=> _C, children=> _D, kind=> std, adj=> _E, spine=> yes, top=> _F, bot=> _G, token=> _H, adjleft=> _I, adjright=> _J, adjwrap=> _K}|_L], _M))
c_code local build_ref_104
	ret_reg &ref[104]
	call_c   build_ref_53()
	call_c   build_ref_103()
	call_c   Dyam_Create_Unary(&ref[53],&ref[103])
	move_ret ref[104]
	c_ret

;; TERM 103: reach_foot([tag_node{id=> _B, label=> _C, children=> _D, kind=> std, adj=> _E, spine=> yes, top=> _F, bot=> _G, token=> _H, adjleft=> _I, adjright=> _J, adjwrap=> _K}|_L], _M)
c_code local build_ref_103
	ret_reg &ref[103]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_489()
	call_c   build_ref_185()
	call_c   build_ref_26()
	call_c   Dyam_Term_Start(&ref[489],12)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(&ref[185])
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(&ref[26])
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   Dyam_Create_List(R(0),V(11))
	move_ret R(0)
	call_c   build_ref_488()
	call_c   Dyam_Create_Binary(&ref[488],R(0),V(12))
	move_ret ref[103]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 488: reach_foot
c_code local build_ref_488
	ret_reg &ref[488]
	call_c   Dyam_Create_Atom("reach_foot")
	move_ret ref[488]
	c_ret

;; TERM 185: std
c_code local build_ref_185
	ret_reg &ref[185]
	call_c   Dyam_Create_Atom("std")
	move_ret ref[185]
	c_ret

;; TERM 489: tag_node!'$ft'
c_code local build_ref_489
	ret_reg &ref[489]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_502()
	call_c   Dyam_Create_List(&ref[502],I(0))
	move_ret R(0)
	call_c   build_ref_501()
	call_c   Dyam_Create_List(&ref[501],R(0))
	move_ret R(0)
	call_c   build_ref_500()
	call_c   Dyam_Create_List(&ref[500],R(0))
	move_ret R(0)
	call_c   build_ref_499()
	call_c   Dyam_Create_List(&ref[499],R(0))
	move_ret R(0)
	call_c   build_ref_498()
	call_c   Dyam_Create_List(&ref[498],R(0))
	move_ret R(0)
	call_c   build_ref_497()
	call_c   Dyam_Create_List(&ref[497],R(0))
	move_ret R(0)
	call_c   build_ref_496()
	call_c   Dyam_Create_List(&ref[496],R(0))
	move_ret R(0)
	call_c   build_ref_495()
	call_c   Dyam_Create_List(&ref[495],R(0))
	move_ret R(0)
	call_c   build_ref_494()
	call_c   Dyam_Create_List(&ref[494],R(0))
	move_ret R(0)
	call_c   build_ref_493()
	call_c   Dyam_Create_List(&ref[493],R(0))
	move_ret R(0)
	call_c   build_ref_492()
	call_c   Dyam_Create_List(&ref[492],R(0))
	move_ret R(0)
	call_c   build_ref_491()
	call_c   Dyam_Create_List(&ref[491],R(0))
	move_ret R(0)
	call_c   build_ref_490()
	call_c   Dyam_Create_Atom_Module("$ft",&ref[490])
	move_ret ref[489]
	call_c   DYAM_Feature_2(&ref[490],R(0))
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 490: tag_node
c_code local build_ref_490
	ret_reg &ref[490]
	call_c   Dyam_Create_Atom("tag_node")
	move_ret ref[490]
	c_ret

;; TERM 491: id
c_code local build_ref_491
	ret_reg &ref[491]
	call_c   Dyam_Create_Atom("id")
	move_ret ref[491]
	c_ret

;; TERM 492: label
c_code local build_ref_492
	ret_reg &ref[492]
	call_c   Dyam_Create_Atom("label")
	move_ret ref[492]
	c_ret

;; TERM 493: children
c_code local build_ref_493
	ret_reg &ref[493]
	call_c   Dyam_Create_Atom("children")
	move_ret ref[493]
	c_ret

;; TERM 494: kind
c_code local build_ref_494
	ret_reg &ref[494]
	call_c   Dyam_Create_Atom("kind")
	move_ret ref[494]
	c_ret

;; TERM 495: adj
c_code local build_ref_495
	ret_reg &ref[495]
	call_c   Dyam_Create_Atom("adj")
	move_ret ref[495]
	c_ret

;; TERM 496: spine
c_code local build_ref_496
	ret_reg &ref[496]
	call_c   Dyam_Create_Atom("spine")
	move_ret ref[496]
	c_ret

;; TERM 497: top
c_code local build_ref_497
	ret_reg &ref[497]
	call_c   Dyam_Create_Atom("top")
	move_ret ref[497]
	c_ret

;; TERM 498: bot
c_code local build_ref_498
	ret_reg &ref[498]
	call_c   Dyam_Create_Atom("bot")
	move_ret ref[498]
	c_ret

;; TERM 499: token
c_code local build_ref_499
	ret_reg &ref[499]
	call_c   Dyam_Create_Atom("token")
	move_ret ref[499]
	c_ret

;; TERM 500: adjleft
c_code local build_ref_500
	ret_reg &ref[500]
	call_c   Dyam_Create_Atom("adjleft")
	move_ret ref[500]
	c_ret

;; TERM 501: adjright
c_code local build_ref_501
	ret_reg &ref[501]
	call_c   Dyam_Create_Atom("adjright")
	move_ret ref[501]
	c_ret

;; TERM 502: adjwrap
c_code local build_ref_502
	ret_reg &ref[502]
	call_c   Dyam_Create_Atom("adjwrap")
	move_ret ref[502]
	c_ret

c_code local build_seed_48
	ret_reg &seed[48]
	call_c   build_ref_53()
	call_c   build_ref_107()
	call_c   Dyam_Seed_Start(&ref[53],&ref[107],I(0),fun3,1)
	call_c   build_ref_108()
	call_c   Dyam_Seed_Add_Comp(&ref[108],&ref[107],0)
	call_c   Dyam_Seed_End()
	move_ret seed[48]
	c_ret

;; TERM 108: '*GUARD*'(reach_foot(_D, _M)) :> '$$HOLE$$'
c_code local build_ref_108
	ret_reg &ref[108]
	call_c   build_ref_107()
	call_c   Dyam_Create_Binary(I(9),&ref[107],I(7))
	move_ret ref[108]
	c_ret

;; TERM 107: '*GUARD*'(reach_foot(_D, _M))
c_code local build_ref_107
	ret_reg &ref[107]
	call_c   build_ref_53()
	call_c   build_ref_106()
	call_c   Dyam_Create_Unary(&ref[53],&ref[106])
	move_ret ref[107]
	c_ret

;; TERM 106: reach_foot(_D, _M)
c_code local build_ref_106
	ret_reg &ref[106]
	call_c   build_ref_488()
	call_c   Dyam_Create_Binary(&ref[488],V(3),V(12))
	move_ret ref[106]
	c_ret

long local pool_fun19[3]=[2,build_ref_104,build_seed_48]

pl_code local fun19
	call_c   Dyam_Pool(pool_fun19)
	call_c   Dyam_Unify_Item(&ref[104])
	fail_ret
	pl_jump  fun10(&seed[48],1)

;; TERM 105: '*GUARD*'(reach_foot([tag_node{id=> _B, label=> _C, children=> _D, kind=> std, adj=> _E, spine=> yes, top=> _F, bot=> _G, token=> _H, adjleft=> _I, adjright=> _J, adjwrap=> _K}|_L], _M)) :> []
c_code local build_ref_105
	ret_reg &ref[105]
	call_c   build_ref_104()
	call_c   Dyam_Create_Binary(I(9),&ref[104],I(0))
	move_ret ref[105]
	c_ret

;; TERM 102: '*GUARD_CLAUSE*'
c_code local build_ref_102
	ret_reg &ref[102]
	call_c   Dyam_Create_Atom("*GUARD_CLAUSE*")
	move_ret ref[102]
	c_ret

c_code local build_seed_14
	ret_reg &seed[14]
	call_c   build_ref_15()
	call_c   build_ref_70()
	call_c   Dyam_Seed_Start(&ref[15],&ref[70],I(0),fun8,1)
	call_c   build_ref_71()
	call_c   Dyam_Seed_Add_Comp(&ref[71],fun54,0)
	call_c   Dyam_Seed_End()
	move_ret seed[14]
	c_ret

;; TERM 71: '*CITEM*'('call_sa_adj_wrapper_install/2'(_B), _A)
c_code local build_ref_71
	ret_reg &ref[71]
	call_c   build_ref_18()
	call_c   build_ref_68()
	call_c   Dyam_Create_Binary(&ref[18],&ref[68],V(0))
	move_ret ref[71]
	c_ret

;; TERM 68: 'call_sa_adj_wrapper_install/2'(_B)
c_code local build_ref_68
	ret_reg &ref[68]
	call_c   build_ref_503()
	call_c   Dyam_Create_Unary(&ref[503],V(1))
	move_ret ref[68]
	c_ret

;; TERM 503: 'call_sa_adj_wrapper_install/2'
c_code local build_ref_503
	ret_reg &ref[503]
	call_c   Dyam_Create_Atom("call_sa_adj_wrapper_install/2")
	move_ret ref[503]
	c_ret

;; TERM 73: [_D,_E,_F|_G]
c_code local build_ref_73
	ret_reg &ref[73]
	call_c   Dyam_Create_Tupple(3,5,V(6))
	move_ret ref[73]
	c_ret

;; TERM 74: sa_env{mspop=> _D, aspop=> [], holes=> [], escape=> _K}
c_code local build_ref_74
	ret_reg &ref[74]
	call_c   build_ref_477()
	call_c   Dyam_Term_Start(&ref[477],4)
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_End()
	move_ret ref[74]
	c_ret

;; TERM 75: sa_env{mspop=> _E, aspop=> [], holes=> [_O,_P], escape=> _K}
c_code local build_ref_75
	ret_reg &ref[75]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Tupple(14,15,I(0))
	move_ret R(0)
	call_c   build_ref_477()
	call_c   Dyam_Term_Start(&ref[477],4)
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_End()
	move_ret ref[75]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_56
	ret_reg &seed[56]
	call_c   build_ref_33()
	call_c   build_ref_172()
	call_c   Dyam_Seed_Start(&ref[33],&ref[172],I(0),fun3,1)
	call_c   build_ref_175()
	call_c   Dyam_Seed_Add_Comp(&ref[175],&ref[172],0)
	call_c   Dyam_Seed_End()
	move_ret seed[56]
	c_ret

;; TERM 175: '*PROLOG-FIRST*'(unfold((_H :> deallocate_layer :> deallocate :> succeed), _M, _J, _I1, sa_env{mspop=> _J1, aspop=> _K1, holes=> [_O,_P], escape=> _L1})) :> '$$HOLE$$'
c_code local build_ref_175
	ret_reg &ref[175]
	call_c   build_ref_174()
	call_c   Dyam_Create_Binary(I(9),&ref[174],I(7))
	move_ret ref[175]
	c_ret

;; TERM 174: '*PROLOG-FIRST*'(unfold((_H :> deallocate_layer :> deallocate :> succeed), _M, _J, _I1, sa_env{mspop=> _J1, aspop=> _K1, holes=> [_O,_P], escape=> _L1}))
c_code local build_ref_174
	ret_reg &ref[174]
	call_c   build_ref_28()
	call_c   build_ref_173()
	call_c   Dyam_Create_Unary(&ref[28],&ref[173])
	move_ret ref[174]
	c_ret

;; TERM 173: unfold((_H :> deallocate_layer :> deallocate :> succeed), _M, _J, _I1, sa_env{mspop=> _J1, aspop=> _K1, holes=> [_O,_P], escape=> _L1})
c_code local build_ref_173
	ret_reg &ref[173]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_354()
	call_c   Dyam_Create_Binary(I(9),V(7),&ref[354])
	move_ret R(0)
	call_c   Dyam_Create_Tupple(14,15,I(0))
	move_ret R(1)
	call_c   build_ref_477()
	call_c   Dyam_Term_Start(&ref[477],4)
	call_c   Dyam_Term_Arg(V(35))
	call_c   Dyam_Term_Arg(V(36))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(37))
	call_c   Dyam_Term_End()
	move_ret R(1)
	call_c   build_ref_474()
	call_c   Dyam_Term_Start(&ref[474],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(34))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_End()
	move_ret ref[173]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 172: '*PROLOG-ITEM*'{top=> unfold((_H :> deallocate_layer :> deallocate :> succeed), _M, _J, _I1, sa_env{mspop=> _J1, aspop=> _K1, holes=> [_O,_P], escape=> _L1}), cont=> '$CLOSURE'('$fun'(42, 0, 1138594416), '$TUPPLE'(35163198462440))}
c_code local build_ref_172
	ret_reg &ref[172]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Start_Tupple(0,333086720)
	call_c   Dyam_Almost_End_Tupple(29,8388608)
	move_ret R(0)
	call_c   Dyam_Closure_Aux(fun42,R(0))
	move_ret R(0)
	call_c   build_ref_473()
	call_c   build_ref_173()
	call_c   Dyam_Create_Binary(&ref[473],&ref[173],R(0))
	move_ret ref[172]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_55
	ret_reg &seed[55]
	call_c   build_ref_33()
	call_c   build_ref_168()
	call_c   Dyam_Seed_Start(&ref[33],&ref[168],I(0),fun3,1)
	call_c   build_ref_171()
	call_c   Dyam_Seed_Add_Comp(&ref[171],&ref[168],0)
	call_c   Dyam_Seed_End()
	move_ret seed[55]
	c_ret

;; TERM 171: '*PROLOG-FIRST*'(build_closure(_I, _N, _J, _M1, sa_env{mspop=> _N1, aspop=> _O1, holes=> [], escape=> _P1})) :> '$$HOLE$$'
c_code local build_ref_171
	ret_reg &ref[171]
	call_c   build_ref_170()
	call_c   Dyam_Create_Binary(I(9),&ref[170],I(7))
	move_ret ref[171]
	c_ret

;; TERM 170: '*PROLOG-FIRST*'(build_closure(_I, _N, _J, _M1, sa_env{mspop=> _N1, aspop=> _O1, holes=> [], escape=> _P1}))
c_code local build_ref_170
	ret_reg &ref[170]
	call_c   build_ref_28()
	call_c   build_ref_169()
	call_c   Dyam_Create_Unary(&ref[28],&ref[169])
	move_ret ref[170]
	c_ret

;; TERM 169: build_closure(_I, _N, _J, _M1, sa_env{mspop=> _N1, aspop=> _O1, holes=> [], escape=> _P1})
c_code local build_ref_169
	ret_reg &ref[169]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_477()
	call_c   Dyam_Term_Start(&ref[477],4)
	call_c   Dyam_Term_Arg(V(39))
	call_c   Dyam_Term_Arg(V(40))
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(V(41))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_504()
	call_c   Dyam_Term_Start(&ref[504],5)
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(38))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_End()
	move_ret ref[169]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 504: build_closure
c_code local build_ref_504
	ret_reg &ref[504]
	call_c   Dyam_Create_Atom("build_closure")
	move_ret ref[504]
	c_ret

;; TERM 168: '*PROLOG-ITEM*'{top=> build_closure(_I, _N, _J, _M1, sa_env{mspop=> _N1, aspop=> _O1, holes=> [], escape=> _P1}), cont=> '$CLOSURE'('$fun'(41, 0, 1138581672), '$TUPPLE'(35163198462392))}
c_code local build_ref_168
	ret_reg &ref[168]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Start_Tupple(0,331481088)
	call_c   Dyam_Almost_End_Tupple(29,8912896)
	move_ret R(0)
	call_c   Dyam_Closure_Aux(fun41,R(0))
	move_ret R(0)
	call_c   build_ref_473()
	call_c   build_ref_169()
	call_c   Dyam_Create_Binary(&ref[473],&ref[169],R(0))
	move_ret ref[168]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 167: unify(_F, _Q1) :> _I1
c_code local build_ref_167
	ret_reg &ref[167]
	call_c   build_ref_166()
	call_c   Dyam_Create_Binary(I(9),&ref[166],V(34))
	move_ret ref[167]
	c_ret

;; TERM 166: unify(_F, _Q1)
c_code local build_ref_166
	ret_reg &ref[166]
	call_c   build_ref_505()
	call_c   Dyam_Create_Binary(&ref[505],V(5),V(42))
	move_ret ref[166]
	c_ret

;; TERM 505: unify
c_code local build_ref_505
	ret_reg &ref[505]
	call_c   Dyam_Create_Atom("unify")
	move_ret ref[505]
	c_ret

c_code local build_seed_45
	ret_reg &seed[45]
	call_c   build_ref_53()
	call_c   build_ref_86()
	call_c   Dyam_Seed_Start(&ref[53],&ref[86],I(0),fun3,1)
	call_c   build_ref_87()
	call_c   Dyam_Seed_Add_Comp(&ref[87],&ref[86],0)
	call_c   Dyam_Seed_End()
	move_ret seed[45]
	c_ret

;; TERM 87: '*GUARD*'(std_prolog_unif_args([_D,_E,_F|_G], 0, _R1, _H1, _L)) :> '$$HOLE$$'
c_code local build_ref_87
	ret_reg &ref[87]
	call_c   build_ref_86()
	call_c   Dyam_Create_Binary(I(9),&ref[86],I(7))
	move_ret ref[87]
	c_ret

;; TERM 86: '*GUARD*'(std_prolog_unif_args([_D,_E,_F|_G], 0, _R1, _H1, _L))
c_code local build_ref_86
	ret_reg &ref[86]
	call_c   build_ref_53()
	call_c   build_ref_85()
	call_c   Dyam_Create_Unary(&ref[53],&ref[85])
	move_ret ref[86]
	c_ret

;; TERM 85: std_prolog_unif_args([_D,_E,_F|_G], 0, _R1, _H1, _L)
c_code local build_ref_85
	ret_reg &ref[85]
	call_c   build_ref_483()
	call_c   build_ref_73()
	call_c   Dyam_Term_Start(&ref[483],5)
	call_c   Dyam_Term_Arg(&ref[73])
	call_c   Dyam_Term_Arg(N(0))
	call_c   Dyam_Term_Arg(V(43))
	call_c   Dyam_Term_Arg(V(33))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_End()
	move_ret ref[85]
	c_ret

;; TERM 94: allocate :> allocate_layer :> _R1
c_code local build_ref_94
	ret_reg &ref[94]
	call_c   build_ref_57()
	call_c   build_ref_93()
	call_c   Dyam_Create_Binary(I(9),&ref[57],&ref[93])
	move_ret ref[94]
	c_ret

;; TERM 93: allocate_layer :> _R1
c_code local build_ref_93
	ret_reg &ref[93]
	call_c   build_ref_58()
	call_c   Dyam_Create_Binary(I(9),&ref[58],V(43))
	move_ret ref[93]
	c_ret

c_code local build_seed_46
	ret_reg &seed[46]
	call_c   build_ref_0()
	call_c   build_ref_96()
	call_c   Dyam_Seed_Start(&ref[0],&ref[96],&ref[96],fun3,1)
	call_c   build_ref_97()
	call_c   Dyam_Seed_Add_Comp(&ref[97],&ref[96],0)
	call_c   Dyam_Seed_End()
	move_ret seed[46]
	c_ret

;; TERM 97: '*RITEM*'(_A, return(_C)) :> '$$HOLE$$'
c_code local build_ref_97
	ret_reg &ref[97]
	call_c   build_ref_96()
	call_c   Dyam_Create_Binary(I(9),&ref[96],I(7))
	move_ret ref[97]
	c_ret

;; TERM 96: '*RITEM*'(_A, return(_C))
c_code local build_ref_96
	ret_reg &ref[96]
	call_c   build_ref_0()
	call_c   build_ref_95()
	call_c   Dyam_Create_Binary(&ref[0],V(0),&ref[95])
	move_ret ref[96]
	c_ret

;; TERM 95: return(_C)
c_code local build_ref_95
	ret_reg &ref[95]
	call_c   build_ref_470()
	call_c   Dyam_Create_Unary(&ref[470],V(2))
	move_ret ref[95]
	c_ret

long local pool_fun16[4]=[3,build_seed_45,build_ref_94,build_seed_46]

long local pool_fun41[3]=[65537,build_ref_167,pool_fun16]

pl_code local fun41
	call_c   Dyam_Pool(pool_fun41)
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(38))
	move     V(42), R(2)
	move     S(5), R(3)
	pl_call  pred_label_term_2()
	call_c   Dyam_Unify(V(33),&ref[167])
	fail_ret
fun16:
	pl_call  fun10(&seed[45],1)
	move     &ref[94], R(0)
	move     S(5), R(1)
	move     V(2), R(2)
	move     S(5), R(3)
	pl_call  pred_label_code_2()
	call_c   Dyam_Reg_Load(0,V(2))
	pl_call  pred_register_init_pool_fun_1()
	call_c   Dyam_Deallocate()
	pl_jump  fun13(&seed[46],2)


long local pool_fun42[2]=[1,build_seed_55]

pl_code local fun42
	call_c   Dyam_Pool(pool_fun42)
	pl_jump  fun10(&seed[55],12)

long local pool_fun51[2]=[1,build_seed_56]

pl_code local fun51
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Deallocate()
	pl_jump  fun10(&seed[56],12)

;; TERM 76: [_Q|_R]
c_code local build_ref_76
	ret_reg &ref[76]
	call_c   Dyam_Create_List(V(16),V(17))
	move_ret ref[76]
	c_ret

;; TERM 77: [_S|_T]
c_code local build_ref_77
	ret_reg &ref[77]
	call_c   Dyam_Create_List(V(18),V(19))
	move_ret ref[77]
	c_ret

c_code local build_seed_54
	ret_reg &seed[54]
	call_c   build_ref_33()
	call_c   build_ref_162()
	call_c   Dyam_Seed_Start(&ref[33],&ref[162],I(0),fun3,1)
	call_c   build_ref_165()
	call_c   Dyam_Seed_Add_Comp(&ref[165],&ref[162],0)
	call_c   Dyam_Seed_End()
	move_ret seed[54]
	c_ret

;; TERM 165: '*PROLOG-FIRST*'(unfold(_Q, _M, _J, _U, _V)) :> '$$HOLE$$'
c_code local build_ref_165
	ret_reg &ref[165]
	call_c   build_ref_164()
	call_c   Dyam_Create_Binary(I(9),&ref[164],I(7))
	move_ret ref[165]
	c_ret

;; TERM 164: '*PROLOG-FIRST*'(unfold(_Q, _M, _J, _U, _V))
c_code local build_ref_164
	ret_reg &ref[164]
	call_c   build_ref_28()
	call_c   build_ref_163()
	call_c   Dyam_Create_Unary(&ref[28],&ref[163])
	move_ret ref[164]
	c_ret

;; TERM 163: unfold(_Q, _M, _J, _U, _V)
c_code local build_ref_163
	ret_reg &ref[163]
	call_c   build_ref_474()
	call_c   Dyam_Term_Start(&ref[474],5)
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_Arg(V(21))
	call_c   Dyam_Term_End()
	move_ret ref[163]
	c_ret

;; TERM 162: '*PROLOG-ITEM*'{top=> unfold(_Q, _M, _J, _U, _V), cont=> '$CLOSURE'('$fun'(40, 0, 1138570312), '$TUPPLE'(35163198462264))}
c_code local build_ref_162
	ret_reg &ref[162]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,332131072)
	move_ret R(0)
	call_c   Dyam_Closure_Aux(fun40,R(0))
	move_ret R(0)
	call_c   build_ref_473()
	call_c   build_ref_163()
	call_c   Dyam_Create_Binary(&ref[473],&ref[163],R(0))
	move_ret ref[162]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_50
	ret_reg &seed[50]
	call_c   build_ref_33()
	call_c   build_ref_126()
	call_c   Dyam_Seed_Start(&ref[33],&ref[126],I(0),fun3,1)
	call_c   build_ref_129()
	call_c   Dyam_Seed_Add_Comp(&ref[129],&ref[126],0)
	call_c   Dyam_Seed_End()
	move_ret seed[50]
	c_ret

;; TERM 129: '*PROLOG-FIRST*'(unfold(_R, _M, _J, _W, sa_env{mspop=> _X, aspop=> _Y, holes=> [_O,_P], escape=> _Z})) :> '$$HOLE$$'
c_code local build_ref_129
	ret_reg &ref[129]
	call_c   build_ref_128()
	call_c   Dyam_Create_Binary(I(9),&ref[128],I(7))
	move_ret ref[129]
	c_ret

;; TERM 128: '*PROLOG-FIRST*'(unfold(_R, _M, _J, _W, sa_env{mspop=> _X, aspop=> _Y, holes=> [_O,_P], escape=> _Z}))
c_code local build_ref_128
	ret_reg &ref[128]
	call_c   build_ref_28()
	call_c   build_ref_127()
	call_c   Dyam_Create_Unary(&ref[28],&ref[127])
	move_ret ref[128]
	c_ret

;; TERM 127: unfold(_R, _M, _J, _W, sa_env{mspop=> _X, aspop=> _Y, holes=> [_O,_P], escape=> _Z})
c_code local build_ref_127
	ret_reg &ref[127]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Tupple(14,15,I(0))
	move_ret R(0)
	call_c   build_ref_477()
	call_c   Dyam_Term_Start(&ref[477],4)
	call_c   Dyam_Term_Arg(V(23))
	call_c   Dyam_Term_Arg(V(24))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(25))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_474()
	call_c   Dyam_Term_Start(&ref[474],5)
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(22))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_End()
	move_ret ref[127]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 126: '*PROLOG-ITEM*'{top=> unfold(_R, _M, _J, _W, sa_env{mspop=> _X, aspop=> _Y, holes=> [_O,_P], escape=> _Z}), cont=> '$CLOSURE'('$fun'(27, 0, 1138476512), '$TUPPLE'(35163198462032))}
c_code local build_ref_126
	ret_reg &ref[126]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,332038976)
	move_ret R(0)
	call_c   Dyam_Closure_Aux(fun27,R(0))
	move_ret R(0)
	call_c   build_ref_473()
	call_c   build_ref_127()
	call_c   Dyam_Create_Binary(&ref[473],&ref[127],R(0))
	move_ret ref[126]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_47
	ret_reg &seed[47]
	call_c   build_ref_33()
	call_c   build_ref_98()
	call_c   Dyam_Seed_Start(&ref[33],&ref[98],I(0),fun3,1)
	call_c   build_ref_101()
	call_c   Dyam_Seed_Add_Comp(&ref[101],&ref[98],0)
	call_c   Dyam_Seed_End()
	move_ret seed[47]
	c_ret

;; TERM 101: '*PROLOG-FIRST*'(build_closure(_T, _N, _J, _B1, sa_env{mspop=> _C1, aspop=> _D1, holes=> [], escape=> _E1})) :> '$$HOLE$$'
c_code local build_ref_101
	ret_reg &ref[101]
	call_c   build_ref_100()
	call_c   Dyam_Create_Binary(I(9),&ref[100],I(7))
	move_ret ref[101]
	c_ret

;; TERM 100: '*PROLOG-FIRST*'(build_closure(_T, _N, _J, _B1, sa_env{mspop=> _C1, aspop=> _D1, holes=> [], escape=> _E1}))
c_code local build_ref_100
	ret_reg &ref[100]
	call_c   build_ref_28()
	call_c   build_ref_99()
	call_c   Dyam_Create_Unary(&ref[28],&ref[99])
	move_ret ref[100]
	c_ret

;; TERM 99: build_closure(_T, _N, _J, _B1, sa_env{mspop=> _C1, aspop=> _D1, holes=> [], escape=> _E1})
c_code local build_ref_99
	ret_reg &ref[99]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_477()
	call_c   Dyam_Term_Start(&ref[477],4)
	call_c   Dyam_Term_Arg(V(28))
	call_c   Dyam_Term_Arg(V(29))
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(V(30))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_504()
	call_c   Dyam_Term_Start(&ref[504],5)
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(27))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_End()
	move_ret ref[99]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 98: '*PROLOG-ITEM*'{top=> build_closure(_T, _N, _J, _B1, sa_env{mspop=> _C1, aspop=> _D1, holes=> [], escape=> _E1}), cont=> '$CLOSURE'('$fun'(17, 0, 1138397352), '$TUPPLE'(35163198461844))}
c_code local build_ref_98
	ret_reg &ref[98]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,331481414)
	move_ret R(0)
	call_c   Dyam_Closure_Aux(fun17,R(0))
	move_ret R(0)
	call_c   build_ref_473()
	call_c   build_ref_99()
	call_c   Dyam_Create_Binary(&ref[473],&ref[99],R(0))
	move_ret ref[98]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 81: unify(_F, _G1) :> _W :> fail
c_code local build_ref_81
	ret_reg &ref[81]
	call_c   build_ref_78()
	call_c   build_ref_80()
	call_c   Dyam_Create_Binary(I(9),&ref[78],&ref[80])
	move_ret ref[81]
	c_ret

;; TERM 80: _W :> fail
c_code local build_ref_80
	ret_reg &ref[80]
	call_c   build_ref_79()
	call_c   Dyam_Create_Binary(I(9),V(22),&ref[79])
	move_ret ref[80]
	c_ret

;; TERM 79: fail
c_code local build_ref_79
	ret_reg &ref[79]
	call_c   Dyam_Create_Atom("fail")
	move_ret ref[79]
	c_ret

;; TERM 78: unify(_F, _G1)
c_code local build_ref_78
	ret_reg &ref[78]
	call_c   build_ref_505()
	call_c   Dyam_Create_Binary(&ref[505],V(5),V(32))
	move_ret ref[78]
	c_ret

;; TERM 84: unify(_F, _F1) :> _U :> fail
c_code local build_ref_84
	ret_reg &ref[84]
	call_c   build_ref_82()
	call_c   build_ref_83()
	call_c   Dyam_Create_Binary(I(9),&ref[82],&ref[83])
	move_ret ref[84]
	c_ret

;; TERM 83: _U :> fail
c_code local build_ref_83
	ret_reg &ref[83]
	call_c   build_ref_79()
	call_c   Dyam_Create_Binary(I(9),V(20),&ref[79])
	move_ret ref[83]
	c_ret

;; TERM 82: unify(_F, _F1)
c_code local build_ref_82
	ret_reg &ref[82]
	call_c   build_ref_505()
	call_c   Dyam_Create_Binary(&ref[505],V(5),V(31))
	move_ret ref[82]
	c_ret

long local pool_fun17[4]=[65538,build_ref_81,build_ref_84,pool_fun16]

pl_code local fun17
	call_c   Dyam_Pool(pool_fun17)
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(26))
	move     V(31), R(2)
	move     S(5), R(3)
	pl_call  pred_label_term_2()
	call_c   Dyam_Reg_Load(0,V(27))
	move     V(32), R(2)
	move     S(5), R(3)
	pl_call  pred_label_term_2()
	move     &ref[81], R(0)
	move     S(5), R(1)
	move     &ref[84], R(2)
	move     S(5), R(3)
	move     V(33), R(4)
	move     S(5), R(5)
	pl_call  pred_choice_code_3()
	pl_jump  fun16()

long local pool_fun27[2]=[1,build_seed_47]

pl_code local fun27
	call_c   Dyam_Pool(pool_fun27)
	call_c   Dyam_Unify(V(26),V(4))
	fail_ret
	pl_jump  fun10(&seed[47],12)

long local pool_fun40[2]=[1,build_seed_50]

pl_code local fun40
	call_c   Dyam_Pool(pool_fun40)
	pl_jump  fun10(&seed[50],12)

long local pool_fun52[9]=[65543,build_ref_73,build_ref_44,build_ref_74,build_ref_75,build_ref_76,build_ref_77,build_seed_54,pool_fun51]

long local pool_fun53[4]=[65538,build_ref_124,build_ref_125,pool_fun52]

pl_code local fun53
	call_c   Dyam_Remove_Choice()
	move     &ref[124], R(0)
	move     0, R(1)
	move     &ref[125], R(2)
	move     S(5), R(3)
	pl_call  pred_internal_error_2()
fun52:
	move     &ref[73], R(0)
	move     S(5), R(1)
	move     V(9), R(2)
	move     S(5), R(3)
	pl_call  pred_tupple_2()
	pl_call  Object_1(&ref[44])
	move     V(11), R(0)
	move     S(5), R(1)
	pl_call  pred_null_tupple_1()
	call_c   Dyam_Unify(V(12),&ref[74])
	fail_ret
	call_c   Dyam_Unify(V(13),&ref[75])
	fail_ret
	call_c   Dyam_Choice(fun51)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(7),&ref[76])
	fail_ret
	call_c   Dyam_Unify(V(8),&ref[77])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Deallocate()
	pl_jump  fun10(&seed[54],12)


;; TERM 72: '*SA-ADJ-WRAPPER*'(_B, [_D,_E,_F|_G], _H, _I)
c_code local build_ref_72
	ret_reg &ref[72]
	call_c   build_ref_506()
	call_c   build_ref_73()
	call_c   Dyam_Term_Start(&ref[506],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(&ref[73])
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_End()
	move_ret ref[72]
	c_ret

;; TERM 506: '*SA-ADJ-WRAPPER*'
c_code local build_ref_506
	ret_reg &ref[506]
	call_c   Dyam_Create_Atom("*SA-ADJ-WRAPPER*")
	move_ret ref[506]
	c_ret

long local pool_fun54[5]=[131074,build_ref_71,build_ref_72,pool_fun53,pool_fun52]

pl_code local fun54
	call_c   Dyam_Pool(pool_fun54)
	call_c   Dyam_Unify_Item(&ref[71])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun53)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[72])
	call_c   Dyam_Cut()
	pl_jump  fun52()

;; TERM 70: '*FIRST*'('call_sa_adj_wrapper_install/2'(_B)) :> []
c_code local build_ref_70
	ret_reg &ref[70]
	call_c   build_ref_69()
	call_c   Dyam_Create_Binary(I(9),&ref[69],I(0))
	move_ret ref[70]
	c_ret

;; TERM 69: '*FIRST*'('call_sa_adj_wrapper_install/2'(_B))
c_code local build_ref_69
	ret_reg &ref[69]
	call_c   build_ref_15()
	call_c   build_ref_68()
	call_c   Dyam_Create_Unary(&ref[15],&ref[68])
	move_ret ref[69]
	c_ret

c_code local build_seed_6
	ret_reg &seed[6]
	call_c   build_ref_102()
	call_c   build_ref_111()
	call_c   Dyam_Seed_Start(&ref[102],&ref[111],I(0),fun0,1)
	call_c   build_ref_110()
	call_c   Dyam_Seed_Add_Comp(&ref[110],fun20,0)
	call_c   Dyam_Seed_End()
	move_ret seed[6]
	c_ret

;; TERM 110: '*GUARD*'(reach_foot([tag_node{id=> _B, label=> _C, children=> _D, kind=> _E, adj=> _F, spine=> no, top=> _G, bot=> _H, token=> _I, adjleft=> _J, adjright=> _K, adjwrap=> _L}|_M], _N))
c_code local build_ref_110
	ret_reg &ref[110]
	call_c   build_ref_53()
	call_c   build_ref_109()
	call_c   Dyam_Create_Unary(&ref[53],&ref[109])
	move_ret ref[110]
	c_ret

;; TERM 109: reach_foot([tag_node{id=> _B, label=> _C, children=> _D, kind=> _E, adj=> _F, spine=> no, top=> _G, bot=> _H, token=> _I, adjleft=> _J, adjright=> _K, adjwrap=> _L}|_M], _N)
c_code local build_ref_109
	ret_reg &ref[109]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_489()
	call_c   build_ref_507()
	call_c   Dyam_Term_Start(&ref[489],12)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(&ref[507])
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   Dyam_Create_List(R(0),V(12))
	move_ret R(0)
	call_c   build_ref_488()
	call_c   Dyam_Create_Binary(&ref[488],R(0),V(13))
	move_ret ref[109]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 507: no
c_code local build_ref_507
	ret_reg &ref[507]
	call_c   Dyam_Create_Atom("no")
	move_ret ref[507]
	c_ret

c_code local build_seed_49
	ret_reg &seed[49]
	call_c   build_ref_53()
	call_c   build_ref_113()
	call_c   Dyam_Seed_Start(&ref[53],&ref[113],I(0),fun3,1)
	call_c   build_ref_114()
	call_c   Dyam_Seed_Add_Comp(&ref[114],&ref[113],0)
	call_c   Dyam_Seed_End()
	move_ret seed[49]
	c_ret

;; TERM 114: '*GUARD*'(reach_foot(_M, _N)) :> '$$HOLE$$'
c_code local build_ref_114
	ret_reg &ref[114]
	call_c   build_ref_113()
	call_c   Dyam_Create_Binary(I(9),&ref[113],I(7))
	move_ret ref[114]
	c_ret

;; TERM 113: '*GUARD*'(reach_foot(_M, _N))
c_code local build_ref_113
	ret_reg &ref[113]
	call_c   build_ref_53()
	call_c   build_ref_112()
	call_c   Dyam_Create_Unary(&ref[53],&ref[112])
	move_ret ref[113]
	c_ret

;; TERM 112: reach_foot(_M, _N)
c_code local build_ref_112
	ret_reg &ref[112]
	call_c   build_ref_488()
	call_c   Dyam_Create_Binary(&ref[488],V(12),V(13))
	move_ret ref[112]
	c_ret

long local pool_fun20[3]=[2,build_ref_110,build_seed_49]

pl_code local fun20
	call_c   Dyam_Pool(pool_fun20)
	call_c   Dyam_Unify_Item(&ref[110])
	fail_ret
	pl_jump  fun10(&seed[49],1)

;; TERM 111: '*GUARD*'(reach_foot([tag_node{id=> _B, label=> _C, children=> _D, kind=> _E, adj=> _F, spine=> no, top=> _G, bot=> _H, token=> _I, adjleft=> _J, adjright=> _K, adjwrap=> _L}|_M], _N)) :> []
c_code local build_ref_111
	ret_reg &ref[111]
	call_c   build_ref_110()
	call_c   Dyam_Create_Binary(I(9),&ref[110],I(0))
	move_ret ref[111]
	c_ret

c_code local build_seed_37
	ret_reg &seed[37]
	call_c   build_ref_102()
	call_c   build_ref_117()
	call_c   Dyam_Seed_Start(&ref[102],&ref[117],I(0),fun0,1)
	call_c   build_ref_116()
	call_c   Dyam_Seed_Add_Comp(&ref[116],fun21,0)
	call_c   Dyam_Seed_End()
	move_ret seed[37]
	c_ret

;; TERM 116: '*GUARD*'(seed_model(seed{model=> '*CAI*'{current=> _B}, id=> _C, anchor=> _D, subs_comp=> _E, code=> _F, go=> _G, tabule=> yes, schedule=> std, subsume=> _H, model_ref=> _I, subs_comp_ref=> _J, c_type=> '*CAI*', c_type_ref=> _K, out_env=> _L, appinfo=> _M, tail_flag=> _N}))
c_code local build_ref_116
	ret_reg &ref[116]
	call_c   build_ref_53()
	call_c   build_ref_115()
	call_c   Dyam_Create_Unary(&ref[53],&ref[115])
	move_ret ref[116]
	c_ret

;; TERM 115: seed_model(seed{model=> '*CAI*'{current=> _B}, id=> _C, anchor=> _D, subs_comp=> _E, code=> _F, go=> _G, tabule=> yes, schedule=> std, subsume=> _H, model_ref=> _I, subs_comp_ref=> _J, c_type=> '*CAI*', c_type_ref=> _K, out_env=> _L, appinfo=> _M, tail_flag=> _N})
c_code local build_ref_115
	ret_reg &ref[115]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_509()
	call_c   build_ref_320()
	call_c   build_ref_26()
	call_c   build_ref_185()
	call_c   build_ref_510()
	call_c   Dyam_Term_Start(&ref[509],16)
	call_c   Dyam_Term_Arg(&ref[320])
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(&ref[26])
	call_c   Dyam_Term_Arg(&ref[185])
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(&ref[510])
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_508()
	call_c   Dyam_Create_Unary(&ref[508],R(0))
	move_ret ref[115]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 508: seed_model
c_code local build_ref_508
	ret_reg &ref[508]
	call_c   Dyam_Create_Atom("seed_model")
	move_ret ref[508]
	c_ret

;; TERM 510: '*CAI*'
c_code local build_ref_510
	ret_reg &ref[510]
	call_c   Dyam_Create_Atom("*CAI*")
	move_ret ref[510]
	c_ret

;; TERM 320: '*CAI*'{current=> _B}
c_code local build_ref_320
	ret_reg &ref[320]
	call_c   build_ref_511()
	call_c   Dyam_Create_Unary(&ref[511],V(1))
	move_ret ref[320]
	c_ret

;; TERM 511: '*CAI*'!'$ft'
c_code local build_ref_511
	ret_reg &ref[511]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_512()
	call_c   Dyam_Create_List(&ref[512],I(0))
	move_ret R(0)
	call_c   build_ref_510()
	call_c   Dyam_Create_Atom_Module("$ft",&ref[510])
	move_ret ref[511]
	call_c   DYAM_Feature_2(&ref[510],R(0))
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 512: current
c_code local build_ref_512
	ret_reg &ref[512]
	call_c   Dyam_Create_Atom("current")
	move_ret ref[512]
	c_ret

;; TERM 509: seed!'$ft'
c_code local build_ref_509
	ret_reg &ref[509]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_528()
	call_c   Dyam_Create_List(&ref[528],I(0))
	move_ret R(0)
	call_c   build_ref_527()
	call_c   Dyam_Create_List(&ref[527],R(0))
	move_ret R(0)
	call_c   build_ref_526()
	call_c   Dyam_Create_List(&ref[526],R(0))
	move_ret R(0)
	call_c   build_ref_525()
	call_c   Dyam_Create_List(&ref[525],R(0))
	move_ret R(0)
	call_c   build_ref_524()
	call_c   Dyam_Create_List(&ref[524],R(0))
	move_ret R(0)
	call_c   build_ref_523()
	call_c   Dyam_Create_List(&ref[523],R(0))
	move_ret R(0)
	call_c   build_ref_522()
	call_c   Dyam_Create_List(&ref[522],R(0))
	move_ret R(0)
	call_c   build_ref_521()
	call_c   Dyam_Create_List(&ref[521],R(0))
	move_ret R(0)
	call_c   build_ref_520()
	call_c   Dyam_Create_List(&ref[520],R(0))
	move_ret R(0)
	call_c   build_ref_519()
	call_c   Dyam_Create_List(&ref[519],R(0))
	move_ret R(0)
	call_c   build_ref_518()
	call_c   Dyam_Create_List(&ref[518],R(0))
	move_ret R(0)
	call_c   build_ref_517()
	call_c   Dyam_Create_List(&ref[517],R(0))
	move_ret R(0)
	call_c   build_ref_516()
	call_c   Dyam_Create_List(&ref[516],R(0))
	move_ret R(0)
	call_c   build_ref_515()
	call_c   Dyam_Create_List(&ref[515],R(0))
	move_ret R(0)
	call_c   build_ref_491()
	call_c   Dyam_Create_List(&ref[491],R(0))
	move_ret R(0)
	call_c   build_ref_514()
	call_c   Dyam_Create_List(&ref[514],R(0))
	move_ret R(0)
	call_c   build_ref_513()
	call_c   Dyam_Create_Atom_Module("$ft",&ref[513])
	move_ret ref[509]
	call_c   DYAM_Feature_2(&ref[513],R(0))
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 513: seed
c_code local build_ref_513
	ret_reg &ref[513]
	call_c   Dyam_Create_Atom("seed")
	move_ret ref[513]
	c_ret

;; TERM 514: model
c_code local build_ref_514
	ret_reg &ref[514]
	call_c   Dyam_Create_Atom("model")
	move_ret ref[514]
	c_ret

;; TERM 515: anchor
c_code local build_ref_515
	ret_reg &ref[515]
	call_c   Dyam_Create_Atom("anchor")
	move_ret ref[515]
	c_ret

;; TERM 516: subs_comp
c_code local build_ref_516
	ret_reg &ref[516]
	call_c   Dyam_Create_Atom("subs_comp")
	move_ret ref[516]
	c_ret

;; TERM 517: code
c_code local build_ref_517
	ret_reg &ref[517]
	call_c   Dyam_Create_Atom("code")
	move_ret ref[517]
	c_ret

;; TERM 518: go
c_code local build_ref_518
	ret_reg &ref[518]
	call_c   Dyam_Create_Atom("go")
	move_ret ref[518]
	c_ret

;; TERM 519: tabule
c_code local build_ref_519
	ret_reg &ref[519]
	call_c   Dyam_Create_Atom("tabule")
	move_ret ref[519]
	c_ret

;; TERM 520: schedule
c_code local build_ref_520
	ret_reg &ref[520]
	call_c   Dyam_Create_Atom("schedule")
	move_ret ref[520]
	c_ret

;; TERM 521: subsume
c_code local build_ref_521
	ret_reg &ref[521]
	call_c   Dyam_Create_Atom("subsume")
	move_ret ref[521]
	c_ret

;; TERM 522: model_ref
c_code local build_ref_522
	ret_reg &ref[522]
	call_c   Dyam_Create_Atom("model_ref")
	move_ret ref[522]
	c_ret

;; TERM 523: subs_comp_ref
c_code local build_ref_523
	ret_reg &ref[523]
	call_c   Dyam_Create_Atom("subs_comp_ref")
	move_ret ref[523]
	c_ret

;; TERM 524: c_type
c_code local build_ref_524
	ret_reg &ref[524]
	call_c   Dyam_Create_Atom("c_type")
	move_ret ref[524]
	c_ret

;; TERM 525: c_type_ref
c_code local build_ref_525
	ret_reg &ref[525]
	call_c   Dyam_Create_Atom("c_type_ref")
	move_ret ref[525]
	c_ret

;; TERM 526: out_env
c_code local build_ref_526
	ret_reg &ref[526]
	call_c   Dyam_Create_Atom("out_env")
	move_ret ref[526]
	c_ret

;; TERM 527: appinfo
c_code local build_ref_527
	ret_reg &ref[527]
	call_c   Dyam_Create_Atom("appinfo")
	move_ret ref[527]
	c_ret

;; TERM 528: tail_flag
c_code local build_ref_528
	ret_reg &ref[528]
	call_c   Dyam_Create_Atom("tail_flag")
	move_ret ref[528]
	c_ret

long local pool_fun21[3]=[2,build_ref_116,build_ref_26]

pl_code local fun21
	call_c   Dyam_Pool(pool_fun21)
	call_c   Dyam_Unify_Item(&ref[116])
	fail_ret
	call_c   Dyam_Unify(V(7),&ref[26])
	fail_ret
	pl_ret

;; TERM 117: '*GUARD*'(seed_model(seed{model=> '*CAI*'{current=> _B}, id=> _C, anchor=> _D, subs_comp=> _E, code=> _F, go=> _G, tabule=> yes, schedule=> std, subsume=> _H, model_ref=> _I, subs_comp_ref=> _J, c_type=> '*CAI*', c_type_ref=> _K, out_env=> _L, appinfo=> _M, tail_flag=> _N})) :> []
c_code local build_ref_117
	ret_reg &ref[117]
	call_c   build_ref_116()
	call_c   Dyam_Create_Binary(I(9),&ref[116],I(0))
	move_ret ref[117]
	c_ret

c_code local build_seed_3
	ret_reg &seed[3]
	call_c   build_ref_102()
	call_c   build_ref_120()
	call_c   Dyam_Seed_Start(&ref[102],&ref[120],I(0),fun0,1)
	call_c   build_ref_119()
	call_c   Dyam_Seed_Add_Comp(&ref[119],fun22,0)
	call_c   Dyam_Seed_End()
	move_ret seed[3]
	c_ret

;; TERM 119: '*GUARD*'(seed_model(seed{model=> ('*CFI*'{aspop=> _B, current=> _C} :> _D), id=> _E, anchor=> _F, subs_comp=> _G, code=> _H, go=> _I, tabule=> yes, schedule=> first, subsume=> yes, model_ref=> _J, subs_comp_ref=> _K, c_type=> '*CFI>*', c_type_ref=> _L, out_env=> _M, appinfo=> _N, tail_flag=> flat}))
c_code local build_ref_119
	ret_reg &ref[119]
	call_c   build_ref_53()
	call_c   build_ref_118()
	call_c   Dyam_Create_Unary(&ref[53],&ref[118])
	move_ret ref[119]
	c_ret

;; TERM 118: seed_model(seed{model=> ('*CFI*'{aspop=> _B, current=> _C} :> _D), id=> _E, anchor=> _F, subs_comp=> _G, code=> _H, go=> _I, tabule=> yes, schedule=> first, subsume=> yes, model_ref=> _J, subs_comp_ref=> _K, c_type=> '*CFI>*', c_type_ref=> _L, out_env=> _M, appinfo=> _N, tail_flag=> flat})
c_code local build_ref_118
	ret_reg &ref[118]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_509()
	call_c   build_ref_238()
	call_c   build_ref_26()
	call_c   build_ref_529()
	call_c   build_ref_530()
	call_c   build_ref_531()
	call_c   Dyam_Term_Start(&ref[509],16)
	call_c   Dyam_Term_Arg(&ref[238])
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(&ref[26])
	call_c   Dyam_Term_Arg(&ref[529])
	call_c   Dyam_Term_Arg(&ref[26])
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(&ref[530])
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(&ref[531])
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_508()
	call_c   Dyam_Create_Unary(&ref[508],R(0))
	move_ret ref[118]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 531: flat
c_code local build_ref_531
	ret_reg &ref[531]
	call_c   Dyam_Create_Atom("flat")
	move_ret ref[531]
	c_ret

;; TERM 530: '*CFI>*'
c_code local build_ref_530
	ret_reg &ref[530]
	call_c   Dyam_Create_Atom("*CFI>*")
	move_ret ref[530]
	c_ret

;; TERM 529: first
c_code local build_ref_529
	ret_reg &ref[529]
	call_c   Dyam_Create_Atom("first")
	move_ret ref[529]
	c_ret

;; TERM 238: '*CFI*'{aspop=> _B, current=> _C} :> _D
c_code local build_ref_238
	ret_reg &ref[238]
	call_c   build_ref_237()
	call_c   Dyam_Create_Binary(I(9),&ref[237],V(3))
	move_ret ref[238]
	c_ret

;; TERM 237: '*CFI*'{aspop=> _B, current=> _C}
c_code local build_ref_237
	ret_reg &ref[237]
	call_c   build_ref_532()
	call_c   Dyam_Create_Binary(&ref[532],V(1),V(2))
	move_ret ref[237]
	c_ret

;; TERM 532: '*CFI*'!'$ft'
c_code local build_ref_532
	ret_reg &ref[532]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_512()
	call_c   Dyam_Create_List(&ref[512],I(0))
	move_ret R(0)
	call_c   build_ref_480()
	call_c   Dyam_Create_List(&ref[480],R(0))
	move_ret R(0)
	call_c   build_ref_533()
	call_c   Dyam_Create_Atom_Module("$ft",&ref[533])
	move_ret ref[532]
	call_c   DYAM_Feature_2(&ref[533],R(0))
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 533: '*CFI*'
c_code local build_ref_533
	ret_reg &ref[533]
	call_c   Dyam_Create_Atom("*CFI*")
	move_ret ref[533]
	c_ret

pl_code local fun22
	call_c   build_ref_119()
	call_c   Dyam_Unify_Item(&ref[119])
	fail_ret
	pl_ret

;; TERM 120: '*GUARD*'(seed_model(seed{model=> ('*CFI*'{aspop=> _B, current=> _C} :> _D), id=> _E, anchor=> _F, subs_comp=> _G, code=> _H, go=> _I, tabule=> yes, schedule=> first, subsume=> yes, model_ref=> _J, subs_comp_ref=> _K, c_type=> '*CFI>*', c_type_ref=> _L, out_env=> _M, appinfo=> _N, tail_flag=> flat})) :> []
c_code local build_ref_120
	ret_reg &ref[120]
	call_c   build_ref_119()
	call_c   Dyam_Create_Binary(I(9),&ref[119],I(0))
	move_ret ref[120]
	c_ret

c_code local build_seed_4
	ret_reg &seed[4]
	call_c   build_ref_102()
	call_c   build_ref_123()
	call_c   Dyam_Seed_Start(&ref[102],&ref[123],I(0),fun0,1)
	call_c   build_ref_122()
	call_c   Dyam_Seed_Add_Comp(&ref[122],fun23,0)
	call_c   Dyam_Seed_End()
	move_ret seed[4]
	c_ret

;; TERM 122: '*GUARD*'(seed_model(seed{model=> ('*SA-AUX-FIRST*'(_B) :> _C), id=> _D, anchor=> ('*SA-AUX-FIRST*'(_B) :> []), subs_comp=> _E, code=> _F, go=> _G, tabule=> yes, schedule=> no, subsume=> no, model_ref=> _H, subs_comp_ref=> _I, c_type=> '*SA-AUX-FIRST*', c_type_ref=> _J, out_env=> _K, appinfo=> _L, tail_flag=> flat}))
c_code local build_ref_122
	ret_reg &ref[122]
	call_c   build_ref_53()
	call_c   build_ref_121()
	call_c   Dyam_Create_Unary(&ref[53],&ref[121])
	move_ret ref[122]
	c_ret

;; TERM 121: seed_model(seed{model=> ('*SA-AUX-FIRST*'(_B) :> _C), id=> _D, anchor=> ('*SA-AUX-FIRST*'(_B) :> []), subs_comp=> _E, code=> _F, go=> _G, tabule=> yes, schedule=> no, subsume=> no, model_ref=> _H, subs_comp_ref=> _I, c_type=> '*SA-AUX-FIRST*', c_type_ref=> _J, out_env=> _K, appinfo=> _L, tail_flag=> flat})
c_code local build_ref_121
	ret_reg &ref[121]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_325()
	call_c   Dyam_Create_Binary(I(9),&ref[325],I(0))
	move_ret R(0)
	call_c   build_ref_509()
	call_c   build_ref_326()
	call_c   build_ref_26()
	call_c   build_ref_507()
	call_c   build_ref_534()
	call_c   build_ref_531()
	call_c   Dyam_Term_Start(&ref[509],16)
	call_c   Dyam_Term_Arg(&ref[326])
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(&ref[26])
	call_c   Dyam_Term_Arg(&ref[507])
	call_c   Dyam_Term_Arg(&ref[507])
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(&ref[534])
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(&ref[531])
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_508()
	call_c   Dyam_Create_Unary(&ref[508],R(0))
	move_ret ref[121]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 534: '*SA-AUX-FIRST*'
c_code local build_ref_534
	ret_reg &ref[534]
	call_c   Dyam_Create_Atom("*SA-AUX-FIRST*")
	move_ret ref[534]
	c_ret

;; TERM 326: '*SA-AUX-FIRST*'(_B) :> _C
c_code local build_ref_326
	ret_reg &ref[326]
	call_c   build_ref_325()
	call_c   Dyam_Create_Binary(I(9),&ref[325],V(2))
	move_ret ref[326]
	c_ret

;; TERM 325: '*SA-AUX-FIRST*'(_B)
c_code local build_ref_325
	ret_reg &ref[325]
	call_c   build_ref_534()
	call_c   Dyam_Create_Unary(&ref[534],V(1))
	move_ret ref[325]
	c_ret

pl_code local fun23
	call_c   build_ref_122()
	call_c   Dyam_Unify_Item(&ref[122])
	fail_ret
	pl_ret

;; TERM 123: '*GUARD*'(seed_model(seed{model=> ('*SA-AUX-FIRST*'(_B) :> _C), id=> _D, anchor=> ('*SA-AUX-FIRST*'(_B) :> []), subs_comp=> _E, code=> _F, go=> _G, tabule=> yes, schedule=> no, subsume=> no, model_ref=> _H, subs_comp_ref=> _I, c_type=> '*SA-AUX-FIRST*', c_type_ref=> _J, out_env=> _K, appinfo=> _L, tail_flag=> flat})) :> []
c_code local build_ref_123
	ret_reg &ref[123]
	call_c   build_ref_122()
	call_c   Dyam_Create_Binary(I(9),&ref[122],I(0))
	move_ret ref[123]
	c_ret

c_code local build_seed_34
	ret_reg &seed[34]
	call_c   build_ref_102()
	call_c   build_ref_149()
	call_c   Dyam_Seed_Start(&ref[102],&ref[149],I(0),fun0,1)
	call_c   build_ref_148()
	call_c   Dyam_Seed_Add_Comp(&ref[148],fun35,0)
	call_c   Dyam_Seed_End()
	move_ret seed[34]
	c_ret

;; TERM 148: '*GUARD*'(seed_model(seed{model=> '*CFI*'{aspop=> _B, current=> _C}, id=> _D, anchor=> _E, subs_comp=> _F, code=> _G, go=> _H, tabule=> yes, schedule=> std, subsume=> _I, model_ref=> _J, subs_comp_ref=> _K, c_type=> '*CFI*', c_type_ref=> _L, out_env=> _M, appinfo=> _N, tail_flag=> _O}))
c_code local build_ref_148
	ret_reg &ref[148]
	call_c   build_ref_53()
	call_c   build_ref_147()
	call_c   Dyam_Create_Unary(&ref[53],&ref[147])
	move_ret ref[148]
	c_ret

;; TERM 147: seed_model(seed{model=> '*CFI*'{aspop=> _B, current=> _C}, id=> _D, anchor=> _E, subs_comp=> _F, code=> _G, go=> _H, tabule=> yes, schedule=> std, subsume=> _I, model_ref=> _J, subs_comp_ref=> _K, c_type=> '*CFI*', c_type_ref=> _L, out_env=> _M, appinfo=> _N, tail_flag=> _O})
c_code local build_ref_147
	ret_reg &ref[147]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_509()
	call_c   build_ref_237()
	call_c   build_ref_26()
	call_c   build_ref_185()
	call_c   build_ref_533()
	call_c   Dyam_Term_Start(&ref[509],16)
	call_c   Dyam_Term_Arg(&ref[237])
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(&ref[26])
	call_c   Dyam_Term_Arg(&ref[185])
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(&ref[533])
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_508()
	call_c   Dyam_Create_Unary(&ref[508],R(0))
	move_ret ref[147]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun35[3]=[2,build_ref_148,build_ref_26]

pl_code local fun35
	call_c   Dyam_Pool(pool_fun35)
	call_c   Dyam_Unify_Item(&ref[148])
	fail_ret
	call_c   Dyam_Unify(V(8),&ref[26])
	fail_ret
	pl_ret

;; TERM 149: '*GUARD*'(seed_model(seed{model=> '*CFI*'{aspop=> _B, current=> _C}, id=> _D, anchor=> _E, subs_comp=> _F, code=> _G, go=> _H, tabule=> yes, schedule=> std, subsume=> _I, model_ref=> _J, subs_comp_ref=> _K, c_type=> '*CFI*', c_type_ref=> _L, out_env=> _M, appinfo=> _N, tail_flag=> _O})) :> []
c_code local build_ref_149
	ret_reg &ref[149]
	call_c   build_ref_148()
	call_c   Dyam_Create_Binary(I(9),&ref[148],I(0))
	move_ret ref[149]
	c_ret

c_code local build_seed_1
	ret_reg &seed[1]
	call_c   build_ref_102()
	call_c   build_ref_132()
	call_c   Dyam_Seed_Start(&ref[102],&ref[132],I(0),fun0,1)
	call_c   build_ref_131()
	call_c   Dyam_Seed_Add_Comp(&ref[131],fun28,0)
	call_c   Dyam_Seed_End()
	move_ret seed[1]
	c_ret

;; TERM 131: '*GUARD*'(seed_model(seed{model=> ('*SAFIRST*'(_B) :> _C), id=> _D, anchor=> ('*SAFIRST*'(_B) :> []), subs_comp=> _E, code=> _F, go=> _G, tabule=> yes, schedule=> no, subsume=> no, model_ref=> _H, subs_comp_ref=> _I, c_type=> '*SAFIRST*', c_type_ref=> _J, out_env=> _K, appinfo=> _L, tail_flag=> _M}))
c_code local build_ref_131
	ret_reg &ref[131]
	call_c   build_ref_53()
	call_c   build_ref_130()
	call_c   Dyam_Create_Unary(&ref[53],&ref[130])
	move_ret ref[131]
	c_ret

;; TERM 130: seed_model(seed{model=> ('*SAFIRST*'(_B) :> _C), id=> _D, anchor=> ('*SAFIRST*'(_B) :> []), subs_comp=> _E, code=> _F, go=> _G, tabule=> yes, schedule=> no, subsume=> no, model_ref=> _H, subs_comp_ref=> _I, c_type=> '*SAFIRST*', c_type_ref=> _J, out_env=> _K, appinfo=> _L, tail_flag=> _M})
c_code local build_ref_130
	ret_reg &ref[130]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_332()
	call_c   Dyam_Create_Binary(I(9),&ref[332],I(0))
	move_ret R(0)
	call_c   build_ref_509()
	call_c   build_ref_333()
	call_c   build_ref_26()
	call_c   build_ref_507()
	call_c   build_ref_535()
	call_c   Dyam_Term_Start(&ref[509],16)
	call_c   Dyam_Term_Arg(&ref[333])
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(&ref[26])
	call_c   Dyam_Term_Arg(&ref[507])
	call_c   Dyam_Term_Arg(&ref[507])
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(&ref[535])
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_508()
	call_c   Dyam_Create_Unary(&ref[508],R(0))
	move_ret ref[130]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 535: '*SAFIRST*'
c_code local build_ref_535
	ret_reg &ref[535]
	call_c   Dyam_Create_Atom("*SAFIRST*")
	move_ret ref[535]
	c_ret

;; TERM 333: '*SAFIRST*'(_B) :> _C
c_code local build_ref_333
	ret_reg &ref[333]
	call_c   build_ref_332()
	call_c   Dyam_Create_Binary(I(9),&ref[332],V(2))
	move_ret ref[333]
	c_ret

;; TERM 332: '*SAFIRST*'(_B)
c_code local build_ref_332
	ret_reg &ref[332]
	call_c   build_ref_535()
	call_c   Dyam_Create_Unary(&ref[535],V(1))
	move_ret ref[332]
	c_ret

pl_code local fun28
	call_c   build_ref_131()
	call_c   Dyam_Unify_Item(&ref[131])
	fail_ret
	pl_ret

;; TERM 132: '*GUARD*'(seed_model(seed{model=> ('*SAFIRST*'(_B) :> _C), id=> _D, anchor=> ('*SAFIRST*'(_B) :> []), subs_comp=> _E, code=> _F, go=> _G, tabule=> yes, schedule=> no, subsume=> no, model_ref=> _H, subs_comp_ref=> _I, c_type=> '*SAFIRST*', c_type_ref=> _J, out_env=> _K, appinfo=> _L, tail_flag=> _M})) :> []
c_code local build_ref_132
	ret_reg &ref[132]
	call_c   build_ref_131()
	call_c   Dyam_Create_Binary(I(9),&ref[131],I(0))
	move_ret ref[132]
	c_ret

c_code local build_seed_2
	ret_reg &seed[2]
	call_c   build_ref_102()
	call_c   build_ref_152()
	call_c   Dyam_Seed_Start(&ref[102],&ref[152],I(0),fun0,1)
	call_c   build_ref_151()
	call_c   Dyam_Seed_Add_Comp(&ref[151],fun36,0)
	call_c   Dyam_Seed_End()
	move_ret seed[2]
	c_ret

;; TERM 151: '*GUARD*'(seed_model(seed{model=> ('*RFI*'{mspop=> _B, mspop_adj=> _C, current=> _D} :> _E), id=> _F, anchor=> _G, subs_comp=> _H, code=> _I, go=> _J, tabule=> yes, schedule=> first, subsume=> yes, model_ref=> _K, subs_comp_ref=> _L, c_type=> '*RFI>*', c_type_ref=> _M, out_env=> _N, appinfo=> _O, tail_flag=> flat}))
c_code local build_ref_151
	ret_reg &ref[151]
	call_c   build_ref_53()
	call_c   build_ref_150()
	call_c   Dyam_Create_Unary(&ref[53],&ref[150])
	move_ret ref[151]
	c_ret

;; TERM 150: seed_model(seed{model=> ('*RFI*'{mspop=> _B, mspop_adj=> _C, current=> _D} :> _E), id=> _F, anchor=> _G, subs_comp=> _H, code=> _I, go=> _J, tabule=> yes, schedule=> first, subsume=> yes, model_ref=> _K, subs_comp_ref=> _L, c_type=> '*RFI>*', c_type_ref=> _M, out_env=> _N, appinfo=> _O, tail_flag=> flat})
c_code local build_ref_150
	ret_reg &ref[150]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_509()
	call_c   build_ref_258()
	call_c   build_ref_26()
	call_c   build_ref_529()
	call_c   build_ref_536()
	call_c   build_ref_531()
	call_c   Dyam_Term_Start(&ref[509],16)
	call_c   Dyam_Term_Arg(&ref[258])
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(&ref[26])
	call_c   Dyam_Term_Arg(&ref[529])
	call_c   Dyam_Term_Arg(&ref[26])
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(&ref[536])
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(&ref[531])
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_508()
	call_c   Dyam_Create_Unary(&ref[508],R(0))
	move_ret ref[150]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 536: '*RFI>*'
c_code local build_ref_536
	ret_reg &ref[536]
	call_c   Dyam_Create_Atom("*RFI>*")
	move_ret ref[536]
	c_ret

;; TERM 258: '*RFI*'{mspop=> _B, mspop_adj=> _C, current=> _D} :> _E
c_code local build_ref_258
	ret_reg &ref[258]
	call_c   build_ref_257()
	call_c   Dyam_Create_Binary(I(9),&ref[257],V(4))
	move_ret ref[258]
	c_ret

;; TERM 257: '*RFI*'{mspop=> _B, mspop_adj=> _C, current=> _D}
c_code local build_ref_257
	ret_reg &ref[257]
	call_c   build_ref_537()
	call_c   Dyam_Term_Start(&ref[537],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[257]
	c_ret

;; TERM 537: '*RFI*'!'$ft'
c_code local build_ref_537
	ret_reg &ref[537]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_512()
	call_c   Dyam_Create_List(&ref[512],I(0))
	move_ret R(0)
	call_c   build_ref_539()
	call_c   Dyam_Create_List(&ref[539],R(0))
	move_ret R(0)
	call_c   build_ref_479()
	call_c   Dyam_Create_List(&ref[479],R(0))
	move_ret R(0)
	call_c   build_ref_538()
	call_c   Dyam_Create_Atom_Module("$ft",&ref[538])
	move_ret ref[537]
	call_c   DYAM_Feature_2(&ref[538],R(0))
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 538: '*RFI*'
c_code local build_ref_538
	ret_reg &ref[538]
	call_c   Dyam_Create_Atom("*RFI*")
	move_ret ref[538]
	c_ret

;; TERM 539: mspop_adj
c_code local build_ref_539
	ret_reg &ref[539]
	call_c   Dyam_Create_Atom("mspop_adj")
	move_ret ref[539]
	c_ret

pl_code local fun36
	call_c   build_ref_151()
	call_c   Dyam_Unify_Item(&ref[151])
	fail_ret
	pl_ret

;; TERM 152: '*GUARD*'(seed_model(seed{model=> ('*RFI*'{mspop=> _B, mspop_adj=> _C, current=> _D} :> _E), id=> _F, anchor=> _G, subs_comp=> _H, code=> _I, go=> _J, tabule=> yes, schedule=> first, subsume=> yes, model_ref=> _K, subs_comp_ref=> _L, c_type=> '*RFI>*', c_type_ref=> _M, out_env=> _N, appinfo=> _O, tail_flag=> flat})) :> []
c_code local build_ref_152
	ret_reg &ref[152]
	call_c   build_ref_151()
	call_c   Dyam_Create_Binary(I(9),&ref[151],I(0))
	move_ret ref[152]
	c_ret

c_code local build_seed_29
	ret_reg &seed[29]
	call_c   build_ref_27()
	call_c   build_ref_135()
	call_c   Dyam_Seed_Start(&ref[27],&ref[135],I(0),fun0,1)
	call_c   build_ref_136()
	call_c   Dyam_Seed_Add_Comp(&ref[136],fun56,0)
	call_c   Dyam_Seed_End()
	move_ret seed[29]
	c_ret

;; TERM 136: '*PROLOG-ITEM*'{top=> unfold('*SA-LAST*'(_B), _C, _D, _E, _C), cont=> _A}
c_code local build_ref_136
	ret_reg &ref[136]
	call_c   build_ref_473()
	call_c   build_ref_133()
	call_c   Dyam_Create_Binary(&ref[473],&ref[133],V(0))
	move_ret ref[136]
	c_ret

;; TERM 133: unfold('*SA-LAST*'(_B), _C, _D, _E, _C)
c_code local build_ref_133
	ret_reg &ref[133]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_540()
	call_c   Dyam_Create_Unary(&ref[540],V(1))
	move_ret R(0)
	call_c   build_ref_474()
	call_c   Dyam_Term_Start(&ref[474],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_End()
	move_ret ref[133]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 540: '*SA-LAST*'
c_code local build_ref_540
	ret_reg &ref[540]
	call_c   Dyam_Create_Atom("*SA-LAST*")
	move_ret ref[540]
	c_ret

;; TERM 220: '$CLOSURE'('$fun'(55, 0, 1138709392), '$TUPPLE'(35163198462724))
c_code local build_ref_220
	ret_reg &ref[220]
	call_c   build_ref_219()
	call_c   Dyam_Closure_Aux(fun55,&ref[219])
	move_ret ref[220]
	c_ret

c_code local build_seed_53
	ret_reg &seed[53]
	call_c   build_ref_33()
	call_c   build_ref_143()
	call_c   Dyam_Seed_Start(&ref[33],&ref[143],I(0),fun3,1)
	call_c   build_ref_146()
	call_c   Dyam_Seed_Add_Comp(&ref[146],&ref[143],0)
	call_c   Dyam_Seed_End()
	move_ret seed[53]
	c_ret

;; TERM 146: '*PROLOG-FIRST*'(seed_install(seed{model=> '*RITEM*'(_C, _B), id=> _G, anchor=> _H, subs_comp=> _I, code=> _J, go=> _K, tabule=> _L, schedule=> _M, subsume=> _F, model_ref=> _N, subs_comp_ref=> _O, c_type=> _P, c_type_ref=> _Q, out_env=> _R, appinfo=> _S, tail_flag=> _T}, 2, _E)) :> '$$HOLE$$'
c_code local build_ref_146
	ret_reg &ref[146]
	call_c   build_ref_145()
	call_c   Dyam_Create_Binary(I(9),&ref[145],I(7))
	move_ret ref[146]
	c_ret

;; TERM 145: '*PROLOG-FIRST*'(seed_install(seed{model=> '*RITEM*'(_C, _B), id=> _G, anchor=> _H, subs_comp=> _I, code=> _J, go=> _K, tabule=> _L, schedule=> _M, subsume=> _F, model_ref=> _N, subs_comp_ref=> _O, c_type=> _P, c_type_ref=> _Q, out_env=> _R, appinfo=> _S, tail_flag=> _T}, 2, _E))
c_code local build_ref_145
	ret_reg &ref[145]
	call_c   build_ref_28()
	call_c   build_ref_144()
	call_c   Dyam_Create_Unary(&ref[28],&ref[144])
	move_ret ref[145]
	c_ret

;; TERM 144: seed_install(seed{model=> '*RITEM*'(_C, _B), id=> _G, anchor=> _H, subs_comp=> _I, code=> _J, go=> _K, tabule=> _L, schedule=> _M, subsume=> _F, model_ref=> _N, subs_comp_ref=> _O, c_type=> _P, c_type_ref=> _Q, out_env=> _R, appinfo=> _S, tail_flag=> _T}, 2, _E)
c_code local build_ref_144
	ret_reg &ref[144]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_0()
	call_c   Dyam_Create_Binary(&ref[0],V(2),V(1))
	move_ret R(0)
	call_c   build_ref_509()
	call_c   Dyam_Term_Start(&ref[509],16)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_541()
	call_c   Dyam_Term_Start(&ref[541],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(N(2))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[144]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 541: seed_install
c_code local build_ref_541
	ret_reg &ref[541]
	call_c   Dyam_Create_Atom("seed_install")
	move_ret ref[541]
	c_ret

;; TERM 143: '*PROLOG-ITEM*'{top=> seed_install(seed{model=> '*RITEM*'(_C, _B), id=> _G, anchor=> _H, subs_comp=> _I, code=> _J, go=> _K, tabule=> _L, schedule=> _M, subsume=> _F, model_ref=> _N, subs_comp_ref=> _O, c_type=> _P, c_type_ref=> _Q, out_env=> _R, appinfo=> _S, tail_flag=> _T}, 2, _E), cont=> _A}
c_code local build_ref_143
	ret_reg &ref[143]
	call_c   build_ref_473()
	call_c   build_ref_144()
	call_c   Dyam_Create_Binary(&ref[473],&ref[144],V(0))
	move_ret ref[143]
	c_ret

long local pool_fun55[2]=[1,build_seed_53]

pl_code local fun55
	call_c   Dyam_Pool(pool_fun55)
	pl_jump  fun10(&seed[53],12)

;; TERM 219: '$TUPPLE'(35163198462724)
c_code local build_ref_219
	ret_reg &ref[219]
	call_c   Dyam_Create_Simple_Tupple(0,494927872)
	move_ret ref[219]
	c_ret

long local pool_fun56[3]=[2,build_ref_136,build_ref_220]

pl_code local fun56
	call_c   Dyam_Pool(pool_fun56)
	call_c   Dyam_Unify_Item(&ref[136])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[220], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	move     V(5), R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Deallocate(3)
fun34:
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Reg_Bind(2,V(3))
	call_c   Dyam_Reg_Bind(4,V(2))
	call_c   Dyam_Choice(fun33)
	pl_call  fun30(&seed[51],1)
	pl_fail


;; TERM 135: '*PROLOG-FIRST*'(unfold('*SA-LAST*'(_B), _C, _D, _E, _C)) :> []
c_code local build_ref_135
	ret_reg &ref[135]
	call_c   build_ref_134()
	call_c   Dyam_Create_Binary(I(9),&ref[134],I(0))
	move_ret ref[135]
	c_ret

;; TERM 134: '*PROLOG-FIRST*'(unfold('*SA-LAST*'(_B), _C, _D, _E, _C))
c_code local build_ref_134
	ret_reg &ref[134]
	call_c   build_ref_28()
	call_c   build_ref_133()
	call_c   Dyam_Create_Unary(&ref[28],&ref[133])
	move_ret ref[134]
	c_ret

c_code local build_seed_35
	ret_reg &seed[35]
	call_c   build_ref_102()
	call_c   build_ref_155()
	call_c   Dyam_Seed_Start(&ref[102],&ref[155],I(0),fun0,1)
	call_c   build_ref_154()
	call_c   Dyam_Seed_Add_Comp(&ref[154],fun37,0)
	call_c   Dyam_Seed_End()
	move_ret seed[35]
	c_ret

;; TERM 154: '*GUARD*'(seed_model(seed{model=> '*RFI*'{mspop=> _B, mspop_adj=> _C, current=> _D}, id=> _E, anchor=> _F, subs_comp=> _G, code=> _H, go=> _I, tabule=> yes, schedule=> std, subsume=> _J, model_ref=> _K, subs_comp_ref=> _L, c_type=> '*RFI*', c_type_ref=> _M, out_env=> _N, appinfo=> _O, tail_flag=> _P}))
c_code local build_ref_154
	ret_reg &ref[154]
	call_c   build_ref_53()
	call_c   build_ref_153()
	call_c   Dyam_Create_Unary(&ref[53],&ref[153])
	move_ret ref[154]
	c_ret

;; TERM 153: seed_model(seed{model=> '*RFI*'{mspop=> _B, mspop_adj=> _C, current=> _D}, id=> _E, anchor=> _F, subs_comp=> _G, code=> _H, go=> _I, tabule=> yes, schedule=> std, subsume=> _J, model_ref=> _K, subs_comp_ref=> _L, c_type=> '*RFI*', c_type_ref=> _M, out_env=> _N, appinfo=> _O, tail_flag=> _P})
c_code local build_ref_153
	ret_reg &ref[153]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_509()
	call_c   build_ref_257()
	call_c   build_ref_26()
	call_c   build_ref_185()
	call_c   build_ref_538()
	call_c   Dyam_Term_Start(&ref[509],16)
	call_c   Dyam_Term_Arg(&ref[257])
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(&ref[26])
	call_c   Dyam_Term_Arg(&ref[185])
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(&ref[538])
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_508()
	call_c   Dyam_Create_Unary(&ref[508],R(0))
	move_ret ref[153]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun37[3]=[2,build_ref_154,build_ref_26]

pl_code local fun37
	call_c   Dyam_Pool(pool_fun37)
	call_c   Dyam_Unify_Item(&ref[154])
	fail_ret
	call_c   Dyam_Unify(V(9),&ref[26])
	fail_ret
	pl_ret

;; TERM 155: '*GUARD*'(seed_model(seed{model=> '*RFI*'{mspop=> _B, mspop_adj=> _C, current=> _D}, id=> _E, anchor=> _F, subs_comp=> _G, code=> _H, go=> _I, tabule=> yes, schedule=> std, subsume=> _J, model_ref=> _K, subs_comp_ref=> _L, c_type=> '*RFI*', c_type_ref=> _M, out_env=> _N, appinfo=> _O, tail_flag=> _P})) :> []
c_code local build_ref_155
	ret_reg &ref[155]
	call_c   build_ref_154()
	call_c   Dyam_Create_Binary(I(9),&ref[154],I(0))
	move_ret ref[155]
	c_ret

c_code local build_seed_36
	ret_reg &seed[36]
	call_c   build_ref_102()
	call_c   build_ref_158()
	call_c   Dyam_Seed_Start(&ref[102],&ref[158],I(0),fun0,1)
	call_c   build_ref_157()
	call_c   Dyam_Seed_Add_Comp(&ref[157],fun38,0)
	call_c   Dyam_Seed_End()
	move_ret seed[36]
	c_ret

;; TERM 157: '*GUARD*'(seed_model(seed{model=> '*RAI*'{xstart=> _B, xend=> _C, aspop=> _D, current=> _E}, id=> _F, anchor=> _G, subs_comp=> _H, code=> _I, go=> _J, tabule=> yes, schedule=> std, subsume=> _K, model_ref=> _L, subs_comp_ref=> _M, c_type=> '*RAI*', c_type_ref=> _N, out_env=> _O, appinfo=> _P, tail_flag=> _Q}))
c_code local build_ref_157
	ret_reg &ref[157]
	call_c   build_ref_53()
	call_c   build_ref_156()
	call_c   Dyam_Create_Unary(&ref[53],&ref[156])
	move_ret ref[157]
	c_ret

;; TERM 156: seed_model(seed{model=> '*RAI*'{xstart=> _B, xend=> _C, aspop=> _D, current=> _E}, id=> _F, anchor=> _G, subs_comp=> _H, code=> _I, go=> _J, tabule=> yes, schedule=> std, subsume=> _K, model_ref=> _L, subs_comp_ref=> _M, c_type=> '*RAI*', c_type_ref=> _N, out_env=> _O, appinfo=> _P, tail_flag=> _Q})
c_code local build_ref_156
	ret_reg &ref[156]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_509()
	call_c   build_ref_290()
	call_c   build_ref_26()
	call_c   build_ref_185()
	call_c   build_ref_542()
	call_c   Dyam_Term_Start(&ref[509],16)
	call_c   Dyam_Term_Arg(&ref[290])
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(&ref[26])
	call_c   Dyam_Term_Arg(&ref[185])
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(&ref[542])
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_508()
	call_c   Dyam_Create_Unary(&ref[508],R(0))
	move_ret ref[156]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 542: '*RAI*'
c_code local build_ref_542
	ret_reg &ref[542]
	call_c   Dyam_Create_Atom("*RAI*")
	move_ret ref[542]
	c_ret

;; TERM 290: '*RAI*'{xstart=> _B, xend=> _C, aspop=> _D, current=> _E}
c_code local build_ref_290
	ret_reg &ref[290]
	call_c   build_ref_543()
	call_c   Dyam_Term_Start(&ref[543],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[290]
	c_ret

;; TERM 543: '*RAI*'!'$ft'
c_code local build_ref_543
	ret_reg &ref[543]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_512()
	call_c   Dyam_Create_List(&ref[512],I(0))
	move_ret R(0)
	call_c   build_ref_480()
	call_c   Dyam_Create_List(&ref[480],R(0))
	move_ret R(0)
	call_c   build_ref_545()
	call_c   Dyam_Create_List(&ref[545],R(0))
	move_ret R(0)
	call_c   build_ref_544()
	call_c   Dyam_Create_List(&ref[544],R(0))
	move_ret R(0)
	call_c   build_ref_542()
	call_c   Dyam_Create_Atom_Module("$ft",&ref[542])
	move_ret ref[543]
	call_c   DYAM_Feature_2(&ref[542],R(0))
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 544: xstart
c_code local build_ref_544
	ret_reg &ref[544]
	call_c   Dyam_Create_Atom("xstart")
	move_ret ref[544]
	c_ret

;; TERM 545: xend
c_code local build_ref_545
	ret_reg &ref[545]
	call_c   Dyam_Create_Atom("xend")
	move_ret ref[545]
	c_ret

long local pool_fun38[3]=[2,build_ref_157,build_ref_26]

pl_code local fun38
	call_c   Dyam_Pool(pool_fun38)
	call_c   Dyam_Unify_Item(&ref[157])
	fail_ret
	call_c   Dyam_Unify(V(10),&ref[26])
	fail_ret
	pl_ret

;; TERM 158: '*GUARD*'(seed_model(seed{model=> '*RAI*'{xstart=> _B, xend=> _C, aspop=> _D, current=> _E}, id=> _F, anchor=> _G, subs_comp=> _H, code=> _I, go=> _J, tabule=> yes, schedule=> std, subsume=> _K, model_ref=> _L, subs_comp_ref=> _M, c_type=> '*RAI*', c_type_ref=> _N, out_env=> _O, appinfo=> _P, tail_flag=> _Q})) :> []
c_code local build_ref_158
	ret_reg &ref[158]
	call_c   build_ref_157()
	call_c   Dyam_Create_Binary(I(9),&ref[157],I(0))
	move_ret ref[158]
	c_ret

c_code local build_seed_8
	ret_reg &seed[8]
	call_c   build_ref_102()
	call_c   build_ref_178()
	call_c   Dyam_Seed_Start(&ref[102],&ref[178],I(0),fun0,1)
	call_c   build_ref_177()
	call_c   Dyam_Seed_Add_Comp(&ref[177],fun43,0)
	call_c   Dyam_Seed_End()
	move_ret seed[8]
	c_ret

;; TERM 177: '*GUARD*'(tag_viewer(subst, _B, _C, _D, _E, _F))
c_code local build_ref_177
	ret_reg &ref[177]
	call_c   build_ref_53()
	call_c   build_ref_176()
	call_c   Dyam_Create_Unary(&ref[53],&ref[176])
	move_ret ref[177]
	c_ret

;; TERM 176: tag_viewer(subst, _B, _C, _D, _E, _F)
c_code local build_ref_176
	ret_reg &ref[176]
	call_c   build_ref_546()
	call_c   build_ref_547()
	call_c   Dyam_Term_Start(&ref[546],6)
	call_c   Dyam_Term_Arg(&ref[547])
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[176]
	c_ret

;; TERM 547: subst
c_code local build_ref_547
	ret_reg &ref[547]
	call_c   Dyam_Create_Atom("subst")
	move_ret ref[547]
	c_ret

;; TERM 546: tag_viewer
c_code local build_ref_546
	ret_reg &ref[546]
	call_c   Dyam_Create_Atom("tag_viewer")
	move_ret ref[546]
	c_ret

;; TERM 179: '*RITEM*'(_E, _F)
c_code local build_ref_179
	ret_reg &ref[179]
	call_c   build_ref_0()
	call_c   Dyam_Create_Binary(&ref[0],V(4),V(5))
	move_ret ref[179]
	c_ret

;; TERM 180: _B(_C,_D)
c_code local build_ref_180
	ret_reg &ref[180]
	call_c   Dyam_Term_Start(I(3),3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[180]
	c_ret

;; TERM 181: viewer(_H, _I)
c_code local build_ref_181
	ret_reg &ref[181]
	call_c   build_ref_548()
	call_c   Dyam_Create_Binary(&ref[548],V(7),V(8))
	move_ret ref[181]
	c_ret

;; TERM 548: viewer
c_code local build_ref_548
	ret_reg &ref[548]
	call_c   Dyam_Create_Atom("viewer")
	move_ret ref[548]
	c_ret

long local pool_fun43[5]=[4,build_ref_177,build_ref_179,build_ref_180,build_ref_181]

pl_code local fun43
	call_c   Dyam_Pool(pool_fun43)
	call_c   Dyam_Unify_Item(&ref[177])
	fail_ret
	call_c   Dyam_Unify(V(6),&ref[179])
	fail_ret
	call_c   DYAM_Numbervars_3(V(6),N(0),V(9))
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(6))
	move     V(7), R(2)
	move     S(5), R(3)
	pl_call  pred_label_term_2()
	move     &ref[180], R(0)
	move     S(5), R(1)
	move     V(8), R(2)
	move     S(5), R(3)
	pl_call  pred_label_term_2()
	move     &ref[181], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Deallocate(1)
	pl_jump  pred_record_without_doublon_1()

;; TERM 178: '*GUARD*'(tag_viewer(subst, _B, _C, _D, _E, _F)) :> []
c_code local build_ref_178
	ret_reg &ref[178]
	call_c   build_ref_177()
	call_c   Dyam_Create_Binary(I(9),&ref[177],I(0))
	move_ret ref[178]
	c_ret

c_code local build_seed_28
	ret_reg &seed[28]
	call_c   build_ref_102()
	call_c   build_ref_184()
	call_c   Dyam_Seed_Start(&ref[102],&ref[184],I(0),fun0,1)
	call_c   build_ref_183()
	call_c   Dyam_Seed_Add_Comp(&ref[183],fun48,0)
	call_c   Dyam_Seed_End()
	move_ret seed[28]
	c_ret

;; TERM 183: '*GUARD*'(seed_model(seed{model=> '*SACITEM*'(_B), id=> _C, anchor=> _D, subs_comp=> _E, code=> _F, go=> _G, tabule=> yes, schedule=> _H, subsume=> _I, model_ref=> _J, subs_comp_ref=> _K, c_type=> '*SACITEM*', c_type_ref=> _L, out_env=> _M, appinfo=> _N, tail_flag=> _O}))
c_code local build_ref_183
	ret_reg &ref[183]
	call_c   build_ref_53()
	call_c   build_ref_182()
	call_c   Dyam_Create_Unary(&ref[53],&ref[182])
	move_ret ref[183]
	c_ret

;; TERM 182: seed_model(seed{model=> '*SACITEM*'(_B), id=> _C, anchor=> _D, subs_comp=> _E, code=> _F, go=> _G, tabule=> yes, schedule=> _H, subsume=> _I, model_ref=> _J, subs_comp_ref=> _K, c_type=> '*SACITEM*', c_type_ref=> _L, out_env=> _M, appinfo=> _N, tail_flag=> _O})
c_code local build_ref_182
	ret_reg &ref[182]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_509()
	call_c   build_ref_331()
	call_c   build_ref_26()
	call_c   build_ref_549()
	call_c   Dyam_Term_Start(&ref[509],16)
	call_c   Dyam_Term_Arg(&ref[331])
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(&ref[26])
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(&ref[549])
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_508()
	call_c   Dyam_Create_Unary(&ref[508],R(0))
	move_ret ref[182]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 549: '*SACITEM*'
c_code local build_ref_549
	ret_reg &ref[549]
	call_c   Dyam_Create_Atom("*SACITEM*")
	move_ret ref[549]
	c_ret

;; TERM 331: '*SACITEM*'(_B)
c_code local build_ref_331
	ret_reg &ref[331]
	call_c   build_ref_549()
	call_c   Dyam_Create_Unary(&ref[549],V(1))
	move_ret ref[331]
	c_ret

pl_code local fun45
	call_c   Dyam_Remove_Choice()
fun44:
	call_c   Dyam_Deallocate()
	pl_ret


long local pool_fun46[2]=[1,build_ref_26]

pl_code local fun47
	call_c   Dyam_Remove_Choice()
fun46:
	call_c   Dyam_Choice(fun45)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(8),&ref[26])
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun44()


long local pool_fun48[5]=[131074,build_ref_183,build_ref_185,pool_fun46,pool_fun46]

pl_code local fun48
	call_c   Dyam_Pool(pool_fun48)
	call_c   Dyam_Unify_Item(&ref[183])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun47)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(7),&ref[185])
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun46()

;; TERM 184: '*GUARD*'(seed_model(seed{model=> '*SACITEM*'(_B), id=> _C, anchor=> _D, subs_comp=> _E, code=> _F, go=> _G, tabule=> yes, schedule=> _H, subsume=> _I, model_ref=> _J, subs_comp_ref=> _K, c_type=> '*SACITEM*', c_type_ref=> _L, out_env=> _M, appinfo=> _N, tail_flag=> _O})) :> []
c_code local build_ref_184
	ret_reg &ref[184]
	call_c   build_ref_183()
	call_c   Dyam_Create_Binary(I(9),&ref[183],I(0))
	move_ret ref[184]
	c_ret

c_code local build_seed_33
	ret_reg &seed[33]
	call_c   build_ref_102()
	call_c   build_ref_188()
	call_c   Dyam_Seed_Start(&ref[102],&ref[188],I(0),fun0,1)
	call_c   build_ref_187()
	call_c   Dyam_Seed_Add_Comp(&ref[187],fun50,0)
	call_c   Dyam_Seed_End()
	move_ret seed[33]
	c_ret

;; TERM 187: '*GUARD*'(seed_model(seed{model=> ('*RAI*'{xstart=> _B, xend=> _C, aspop=> _D, current=> _E} :> _F), id=> _G, anchor=> _H, subs_comp=> _I, code=> _J, go=> _K, tabule=> yes, schedule=> first, subsume=> _L, model_ref=> _M, subs_comp_ref=> _N, c_type=> '*RAI>*', c_type_ref=> _O, out_env=> _P, appinfo=> _Q, tail_flag=> flat}))
c_code local build_ref_187
	ret_reg &ref[187]
	call_c   build_ref_53()
	call_c   build_ref_186()
	call_c   Dyam_Create_Unary(&ref[53],&ref[186])
	move_ret ref[187]
	c_ret

;; TERM 186: seed_model(seed{model=> ('*RAI*'{xstart=> _B, xend=> _C, aspop=> _D, current=> _E} :> _F), id=> _G, anchor=> _H, subs_comp=> _I, code=> _J, go=> _K, tabule=> yes, schedule=> first, subsume=> _L, model_ref=> _M, subs_comp_ref=> _N, c_type=> '*RAI>*', c_type_ref=> _O, out_env=> _P, appinfo=> _Q, tail_flag=> flat})
c_code local build_ref_186
	ret_reg &ref[186]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_509()
	call_c   build_ref_291()
	call_c   build_ref_26()
	call_c   build_ref_529()
	call_c   build_ref_550()
	call_c   build_ref_531()
	call_c   Dyam_Term_Start(&ref[509],16)
	call_c   Dyam_Term_Arg(&ref[291])
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(&ref[26])
	call_c   Dyam_Term_Arg(&ref[529])
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(&ref[550])
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(&ref[531])
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_508()
	call_c   Dyam_Create_Unary(&ref[508],R(0))
	move_ret ref[186]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 550: '*RAI>*'
c_code local build_ref_550
	ret_reg &ref[550]
	call_c   Dyam_Create_Atom("*RAI>*")
	move_ret ref[550]
	c_ret

;; TERM 291: '*RAI*'{xstart=> _B, xend=> _C, aspop=> _D, current=> _E} :> _F
c_code local build_ref_291
	ret_reg &ref[291]
	call_c   build_ref_290()
	call_c   Dyam_Create_Binary(I(9),&ref[290],V(5))
	move_ret ref[291]
	c_ret

;; TERM 192: '$CLOSURE'('$fun'(49, 0, 1138631896), '$TUPPLE'(0))
c_code local build_ref_192
	ret_reg &ref[192]
	call_c   Dyam_Closure_Aux(fun49,I(0))
	move_ret ref[192]
	c_ret

pl_code local fun49
	pl_ret

long local pool_fun50[3]=[2,build_ref_187,build_ref_192]

pl_code local fun50
	call_c   Dyam_Pool(pool_fun50)
	call_c   Dyam_Unify_Item(&ref[187])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[192], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Load(4,V(11))
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  fun34()

;; TERM 188: '*GUARD*'(seed_model(seed{model=> ('*RAI*'{xstart=> _B, xend=> _C, aspop=> _D, current=> _E} :> _F), id=> _G, anchor=> _H, subs_comp=> _I, code=> _J, go=> _K, tabule=> yes, schedule=> first, subsume=> _L, model_ref=> _M, subs_comp_ref=> _N, c_type=> '*RAI>*', c_type_ref=> _O, out_env=> _P, appinfo=> _Q, tail_flag=> flat})) :> []
c_code local build_ref_188
	ret_reg &ref[188]
	call_c   build_ref_187()
	call_c   Dyam_Create_Binary(I(9),&ref[187],I(0))
	move_ret ref[188]
	c_ret

c_code local build_seed_22
	ret_reg &seed[22]
	call_c   build_ref_27()
	call_c   build_ref_203()
	call_c   Dyam_Seed_Start(&ref[27],&ref[203],I(0),fun0,1)
	call_c   build_ref_204()
	call_c   Dyam_Seed_Add_Comp(&ref[204],fun63,0)
	call_c   Dyam_Seed_End()
	move_ret seed[22]
	c_ret

;; TERM 204: '*PROLOG-ITEM*'{top=> unfold('*SA-AUX-LAST*'(_B), sa_env{mspop=> [], aspop=> _C, holes=> [], escape=> sa_escape{start=> _D, end=> _E}}, _F, _G, sa_env{mspop=> [], aspop=> _C, holes=> [], escape=> sa_escape{start=> _D, end=> _E}}), cont=> _A}
c_code local build_ref_204
	ret_reg &ref[204]
	call_c   build_ref_473()
	call_c   build_ref_201()
	call_c   Dyam_Create_Binary(&ref[473],&ref[201],V(0))
	move_ret ref[204]
	c_ret

;; TERM 201: unfold('*SA-AUX-LAST*'(_B), sa_env{mspop=> [], aspop=> _C, holes=> [], escape=> sa_escape{start=> _D, end=> _E}}, _F, _G, sa_env{mspop=> [], aspop=> _C, holes=> [], escape=> sa_escape{start=> _D, end=> _E}})
c_code local build_ref_201
	ret_reg &ref[201]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_551()
	call_c   Dyam_Create_Unary(&ref[551],V(1))
	move_ret R(0)
	call_c   build_ref_466()
	call_c   Dyam_Create_Binary(&ref[466],V(3),V(4))
	move_ret R(1)
	call_c   build_ref_477()
	call_c   Dyam_Term_Start(&ref[477],4)
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_End()
	move_ret R(1)
	call_c   build_ref_474()
	call_c   Dyam_Term_Start(&ref[474],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_End()
	move_ret ref[201]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 551: '*SA-AUX-LAST*'
c_code local build_ref_551
	ret_reg &ref[551]
	call_c   Dyam_Create_Atom("*SA-AUX-LAST*")
	move_ret ref[551]
	c_ret

c_code local build_seed_58
	ret_reg &seed[58]
	call_c   build_ref_33()
	call_c   build_ref_205()
	call_c   Dyam_Seed_Start(&ref[33],&ref[205],I(0),fun3,1)
	call_c   build_ref_208()
	call_c   Dyam_Seed_Add_Comp(&ref[208],&ref[205],0)
	call_c   Dyam_Seed_End()
	move_ret seed[58]
	c_ret

;; TERM 208: '*PROLOG-FIRST*'(seed_install(seed{model=> '*RAI*'{xstart=> _D, xend=> _E, aspop=> _C, current=> _B}, id=> _H, anchor=> _I, subs_comp=> _J, code=> _K, go=> _L, tabule=> _M, schedule=> _N, subsume=> _O, model_ref=> _P, subs_comp_ref=> _Q, c_type=> _R, c_type_ref=> _S, out_env=> _T, appinfo=> _U, tail_flag=> _V}, 2, _G)) :> '$$HOLE$$'
c_code local build_ref_208
	ret_reg &ref[208]
	call_c   build_ref_207()
	call_c   Dyam_Create_Binary(I(9),&ref[207],I(7))
	move_ret ref[208]
	c_ret

;; TERM 207: '*PROLOG-FIRST*'(seed_install(seed{model=> '*RAI*'{xstart=> _D, xend=> _E, aspop=> _C, current=> _B}, id=> _H, anchor=> _I, subs_comp=> _J, code=> _K, go=> _L, tabule=> _M, schedule=> _N, subsume=> _O, model_ref=> _P, subs_comp_ref=> _Q, c_type=> _R, c_type_ref=> _S, out_env=> _T, appinfo=> _U, tail_flag=> _V}, 2, _G))
c_code local build_ref_207
	ret_reg &ref[207]
	call_c   build_ref_28()
	call_c   build_ref_206()
	call_c   Dyam_Create_Unary(&ref[28],&ref[206])
	move_ret ref[207]
	c_ret

;; TERM 206: seed_install(seed{model=> '*RAI*'{xstart=> _D, xend=> _E, aspop=> _C, current=> _B}, id=> _H, anchor=> _I, subs_comp=> _J, code=> _K, go=> _L, tabule=> _M, schedule=> _N, subsume=> _O, model_ref=> _P, subs_comp_ref=> _Q, c_type=> _R, c_type_ref=> _S, out_env=> _T, appinfo=> _U, tail_flag=> _V}, 2, _G)
c_code local build_ref_206
	ret_reg &ref[206]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_543()
	call_c   Dyam_Term_Start(&ref[543],4)
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_509()
	call_c   Dyam_Term_Start(&ref[509],16)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_Arg(V(21))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_541()
	call_c   Dyam_Term_Start(&ref[541],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(N(2))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[206]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 205: '*PROLOG-ITEM*'{top=> seed_install(seed{model=> '*RAI*'{xstart=> _D, xend=> _E, aspop=> _C, current=> _B}, id=> _H, anchor=> _I, subs_comp=> _J, code=> _K, go=> _L, tabule=> _M, schedule=> _N, subsume=> _O, model_ref=> _P, subs_comp_ref=> _Q, c_type=> _R, c_type_ref=> _S, out_env=> _T, appinfo=> _U, tail_flag=> _V}, 2, _G), cont=> _A}
c_code local build_ref_205
	ret_reg &ref[205]
	call_c   build_ref_473()
	call_c   build_ref_206()
	call_c   Dyam_Create_Binary(&ref[473],&ref[206],V(0))
	move_ret ref[205]
	c_ret

long local pool_fun63[3]=[2,build_ref_204,build_seed_58]

pl_code local fun63
	call_c   Dyam_Pool(pool_fun63)
	call_c   Dyam_Unify_Item(&ref[204])
	fail_ret
	pl_jump  fun10(&seed[58],12)

;; TERM 203: '*PROLOG-FIRST*'(unfold('*SA-AUX-LAST*'(_B), sa_env{mspop=> [], aspop=> _C, holes=> [], escape=> sa_escape{start=> _D, end=> _E}}, _F, _G, sa_env{mspop=> [], aspop=> _C, holes=> [], escape=> sa_escape{start=> _D, end=> _E}})) :> []
c_code local build_ref_203
	ret_reg &ref[203]
	call_c   build_ref_202()
	call_c   Dyam_Create_Binary(I(9),&ref[202],I(0))
	move_ret ref[203]
	c_ret

;; TERM 202: '*PROLOG-FIRST*'(unfold('*SA-AUX-LAST*'(_B), sa_env{mspop=> [], aspop=> _C, holes=> [], escape=> sa_escape{start=> _D, end=> _E}}, _F, _G, sa_env{mspop=> [], aspop=> _C, holes=> [], escape=> sa_escape{start=> _D, end=> _E}}))
c_code local build_ref_202
	ret_reg &ref[202]
	call_c   build_ref_28()
	call_c   build_ref_201()
	call_c   Dyam_Create_Unary(&ref[28],&ref[201])
	move_ret ref[202]
	c_ret

c_code local build_seed_0
	ret_reg &seed[0]
	call_c   build_ref_102()
	call_c   build_ref_161()
	call_c   Dyam_Seed_Start(&ref[102],&ref[161],I(0),fun0,1)
	call_c   build_ref_160()
	call_c   Dyam_Seed_Add_Comp(&ref[160],fun39,0)
	call_c   Dyam_Seed_End()
	move_ret seed[0]
	c_ret

;; TERM 160: '*GUARD*'(reach_foot([tag_node{id=> _B, label=> _C, children=> _D, kind=> foot, adj=> _E, spine=> _F, top=> _G, bot=> _H, token=> _I, adjleft=> _J, adjright=> _K, adjwrap=> _L}|_M], tag_node{id=> _B, label=> _C, children=> _D, kind=> foot, adj=> _E, spine=> _F, top=> _G, bot=> _H, token=> _I, adjleft=> _J, adjright=> _K, adjwrap=> _L}))
c_code local build_ref_160
	ret_reg &ref[160]
	call_c   build_ref_53()
	call_c   build_ref_159()
	call_c   Dyam_Create_Unary(&ref[53],&ref[159])
	move_ret ref[160]
	c_ret

;; TERM 159: reach_foot([tag_node{id=> _B, label=> _C, children=> _D, kind=> foot, adj=> _E, spine=> _F, top=> _G, bot=> _H, token=> _I, adjleft=> _J, adjright=> _K, adjwrap=> _L}|_M], tag_node{id=> _B, label=> _C, children=> _D, kind=> foot, adj=> _E, spine=> _F, top=> _G, bot=> _H, token=> _I, adjleft=> _J, adjright=> _K, adjwrap=> _L})
c_code local build_ref_159
	ret_reg &ref[159]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_489()
	call_c   build_ref_552()
	call_c   Dyam_Term_Start(&ref[489],12)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(&ref[552])
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   Dyam_Create_List(R(0),V(12))
	move_ret R(1)
	call_c   build_ref_488()
	call_c   Dyam_Create_Binary(&ref[488],R(1),R(0))
	move_ret ref[159]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 552: foot
c_code local build_ref_552
	ret_reg &ref[552]
	call_c   Dyam_Create_Atom("foot")
	move_ret ref[552]
	c_ret

pl_code local fun39
	call_c   build_ref_160()
	call_c   Dyam_Unify_Item(&ref[160])
	fail_ret
	pl_ret

;; TERM 161: '*GUARD*'(reach_foot([tag_node{id=> _B, label=> _C, children=> _D, kind=> foot, adj=> _E, spine=> _F, top=> _G, bot=> _H, token=> _I, adjleft=> _J, adjright=> _K, adjwrap=> _L}|_M], tag_node{id=> _B, label=> _C, children=> _D, kind=> foot, adj=> _E, spine=> _F, top=> _G, bot=> _H, token=> _I, adjleft=> _J, adjright=> _K, adjwrap=> _L})) :> []
c_code local build_ref_161
	ret_reg &ref[161]
	call_c   build_ref_160()
	call_c   Dyam_Create_Binary(I(9),&ref[160],I(0))
	move_ret ref[161]
	c_ret

c_code local build_seed_26
	ret_reg &seed[26]
	call_c   build_ref_27()
	call_c   build_ref_195()
	call_c   Dyam_Seed_Start(&ref[27],&ref[195],I(0),fun0,1)
	call_c   build_ref_196()
	call_c   Dyam_Seed_Add_Comp(&ref[196],fun74,0)
	call_c   Dyam_Seed_End()
	move_ret seed[26]
	c_ret

;; TERM 196: '*PROLOG-ITEM*'{top=> unfold('*SA-PSEUDO-SUBST*'(_B, _C, _D, _E), _F, _G, _H, _I), cont=> _A}
c_code local build_ref_196
	ret_reg &ref[196]
	call_c   build_ref_473()
	call_c   build_ref_193()
	call_c   Dyam_Create_Binary(&ref[473],&ref[193],V(0))
	move_ret ref[196]
	c_ret

;; TERM 193: unfold('*SA-PSEUDO-SUBST*'(_B, _C, _D, _E), _F, _G, _H, _I)
c_code local build_ref_193
	ret_reg &ref[193]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_553()
	call_c   Dyam_Term_Start(&ref[553],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_474()
	call_c   Dyam_Term_Start(&ref[474],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_End()
	move_ret ref[193]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 553: '*SA-PSEUDO-SUBST*'
c_code local build_ref_553
	ret_reg &ref[553]
	call_c   Dyam_Create_Atom("*SA-PSEUDO-SUBST*")
	move_ret ref[553]
	c_ret

c_code local build_seed_57
	ret_reg &seed[57]
	call_c   build_ref_33()
	call_c   build_ref_197()
	call_c   Dyam_Seed_Start(&ref[33],&ref[197],I(0),fun3,1)
	call_c   build_ref_200()
	call_c   Dyam_Seed_Add_Comp(&ref[200],&ref[197],0)
	call_c   Dyam_Seed_End()
	move_ret seed[57]
	c_ret

;; TERM 200: '*PROLOG-FIRST*'(seed_install(seed{model=> ('*RITEM*'(_B, _C) :> _D(_F)(_G)), id=> _J, anchor=> _K, subs_comp=> _L, code=> _M, go=> _N, tabule=> _O, schedule=> _P, subsume=> _Q, model_ref=> _R, subs_comp_ref=> _S, c_type=> _T, c_type_ref=> _U, out_env=> [_I], appinfo=> _V, tail_flag=> flat}, [2,_E], _H)) :> '$$HOLE$$'
c_code local build_ref_200
	ret_reg &ref[200]
	call_c   build_ref_199()
	call_c   Dyam_Create_Binary(I(9),&ref[199],I(7))
	move_ret ref[200]
	c_ret

;; TERM 199: '*PROLOG-FIRST*'(seed_install(seed{model=> ('*RITEM*'(_B, _C) :> _D(_F)(_G)), id=> _J, anchor=> _K, subs_comp=> _L, code=> _M, go=> _N, tabule=> _O, schedule=> _P, subsume=> _Q, model_ref=> _R, subs_comp_ref=> _S, c_type=> _T, c_type_ref=> _U, out_env=> [_I], appinfo=> _V, tail_flag=> flat}, [2,_E], _H))
c_code local build_ref_199
	ret_reg &ref[199]
	call_c   build_ref_28()
	call_c   build_ref_198()
	call_c   Dyam_Create_Unary(&ref[28],&ref[198])
	move_ret ref[199]
	c_ret

;; TERM 198: seed_install(seed{model=> ('*RITEM*'(_B, _C) :> _D(_F)(_G)), id=> _J, anchor=> _K, subs_comp=> _L, code=> _M, go=> _N, tabule=> _O, schedule=> _P, subsume=> _Q, model_ref=> _R, subs_comp_ref=> _S, c_type=> _T, c_type_ref=> _U, out_env=> [_I], appinfo=> _V, tail_flag=> flat}, [2,_E], _H)
c_code local build_ref_198
	ret_reg &ref[198]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_0()
	call_c   Dyam_Create_Binary(&ref[0],V(1),V(2))
	move_ret R(0)
	call_c   Dyam_Create_Binary(I(3),V(3),V(5))
	move_ret R(1)
	call_c   Dyam_Create_Binary(I(3),R(1),V(6))
	move_ret R(1)
	call_c   Dyam_Create_Binary(I(9),R(0),R(1))
	move_ret R(1)
	call_c   Dyam_Create_List(V(8),I(0))
	move_ret R(0)
	call_c   build_ref_509()
	call_c   build_ref_531()
	call_c   Dyam_Term_Start(&ref[509],16)
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(21))
	call_c   Dyam_Term_Arg(&ref[531])
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   Dyam_Create_List(V(4),I(0))
	move_ret R(1)
	call_c   Dyam_Create_List(N(2),R(1))
	move_ret R(1)
	call_c   build_ref_541()
	call_c   Dyam_Term_Start(&ref[541],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[198]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 197: '*PROLOG-ITEM*'{top=> seed_install(seed{model=> ('*RITEM*'(_B, _C) :> _D(_F)(_G)), id=> _J, anchor=> _K, subs_comp=> _L, code=> _M, go=> _N, tabule=> _O, schedule=> _P, subsume=> _Q, model_ref=> _R, subs_comp_ref=> _S, c_type=> _T, c_type_ref=> _U, out_env=> [_I], appinfo=> _V, tail_flag=> flat}, [2,_E], _H), cont=> _A}
c_code local build_ref_197
	ret_reg &ref[197]
	call_c   build_ref_473()
	call_c   build_ref_198()
	call_c   Dyam_Create_Binary(&ref[473],&ref[198],V(0))
	move_ret ref[197]
	c_ret

long local pool_fun74[3]=[2,build_ref_196,build_seed_57]

pl_code local fun74
	call_c   Dyam_Pool(pool_fun74)
	call_c   Dyam_Unify_Item(&ref[196])
	fail_ret
	pl_jump  fun10(&seed[57],12)

;; TERM 195: '*PROLOG-FIRST*'(unfold('*SA-PSEUDO-SUBST*'(_B, _C, _D, _E), _F, _G, _H, _I)) :> []
c_code local build_ref_195
	ret_reg &ref[195]
	call_c   build_ref_194()
	call_c   Dyam_Create_Binary(I(9),&ref[194],I(0))
	move_ret ref[195]
	c_ret

;; TERM 194: '*PROLOG-FIRST*'(unfold('*SA-PSEUDO-SUBST*'(_B, _C, _D, _E), _F, _G, _H, _I))
c_code local build_ref_194
	ret_reg &ref[194]
	call_c   build_ref_28()
	call_c   build_ref_193()
	call_c   Dyam_Create_Unary(&ref[28],&ref[193])
	move_ret ref[194]
	c_ret

c_code local build_seed_23
	ret_reg &seed[23]
	call_c   build_ref_27()
	call_c   build_ref_211()
	call_c   Dyam_Seed_Start(&ref[27],&ref[211],I(0),fun0,1)
	call_c   build_ref_212()
	call_c   Dyam_Seed_Add_Comp(&ref[212],fun58,0)
	call_c   Dyam_Seed_End()
	move_ret seed[23]
	c_ret

;; TERM 212: '*PROLOG-ITEM*'{top=> unfold('*SA-CUT-LAST*'(_B, _C, _D), _E, _F, _G, _E), cont=> _A}
c_code local build_ref_212
	ret_reg &ref[212]
	call_c   build_ref_473()
	call_c   build_ref_209()
	call_c   Dyam_Create_Binary(&ref[473],&ref[209],V(0))
	move_ret ref[212]
	c_ret

;; TERM 209: unfold('*SA-CUT-LAST*'(_B, _C, _D), _E, _F, _G, _E)
c_code local build_ref_209
	ret_reg &ref[209]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_554()
	call_c   Dyam_Term_Start(&ref[554],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_474()
	call_c   Dyam_Term_Start(&ref[474],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[209]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 554: '*SA-CUT-LAST*'
c_code local build_ref_554
	ret_reg &ref[554]
	call_c   Dyam_Create_Atom("*SA-CUT-LAST*")
	move_ret ref[554]
	c_ret

;; TERM 213: [return,_C|_H]
c_code local build_ref_213
	ret_reg &ref[213]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(2),V(7))
	move_ret R(0)
	call_c   build_ref_470()
	call_c   Dyam_Create_List(&ref[470],R(0))
	move_ret ref[213]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 223: '$CLOSURE'('$fun'(57, 0, 1138719320), '$TUPPLE'(35163198462824))
c_code local build_ref_223
	ret_reg &ref[223]
	call_c   build_ref_222()
	call_c   Dyam_Closure_Aux(fun57,&ref[222])
	move_ret ref[223]
	c_ret

c_code local build_seed_59
	ret_reg &seed[59]
	call_c   build_ref_33()
	call_c   build_ref_214()
	call_c   Dyam_Seed_Start(&ref[33],&ref[214],I(0),fun3,1)
	call_c   build_ref_217()
	call_c   Dyam_Seed_Add_Comp(&ref[217],&ref[214],0)
	call_c   Dyam_Seed_End()
	move_ret seed[59]
	c_ret

;; TERM 217: '*PROLOG-FIRST*'(seed_install(seed{model=> '*RITEM*'(_E, _I), id=> _K, anchor=> _L, subs_comp=> _M, code=> _N, go=> _O, tabule=> _P, schedule=> _Q, subsume=> _J, model_ref=> _R, subs_comp_ref=> _S, c_type=> _T, c_type_ref=> _U, out_env=> _V, appinfo=> _W, tail_flag=> _X}, 12, _G)) :> '$$HOLE$$'
c_code local build_ref_217
	ret_reg &ref[217]
	call_c   build_ref_216()
	call_c   Dyam_Create_Binary(I(9),&ref[216],I(7))
	move_ret ref[217]
	c_ret

;; TERM 216: '*PROLOG-FIRST*'(seed_install(seed{model=> '*RITEM*'(_E, _I), id=> _K, anchor=> _L, subs_comp=> _M, code=> _N, go=> _O, tabule=> _P, schedule=> _Q, subsume=> _J, model_ref=> _R, subs_comp_ref=> _S, c_type=> _T, c_type_ref=> _U, out_env=> _V, appinfo=> _W, tail_flag=> _X}, 12, _G))
c_code local build_ref_216
	ret_reg &ref[216]
	call_c   build_ref_28()
	call_c   build_ref_215()
	call_c   Dyam_Create_Unary(&ref[28],&ref[215])
	move_ret ref[216]
	c_ret

;; TERM 215: seed_install(seed{model=> '*RITEM*'(_E, _I), id=> _K, anchor=> _L, subs_comp=> _M, code=> _N, go=> _O, tabule=> _P, schedule=> _Q, subsume=> _J, model_ref=> _R, subs_comp_ref=> _S, c_type=> _T, c_type_ref=> _U, out_env=> _V, appinfo=> _W, tail_flag=> _X}, 12, _G)
c_code local build_ref_215
	ret_reg &ref[215]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_0()
	call_c   Dyam_Create_Binary(&ref[0],V(4),V(8))
	move_ret R(0)
	call_c   build_ref_509()
	call_c   Dyam_Term_Start(&ref[509],16)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_Arg(V(21))
	call_c   Dyam_Term_Arg(V(22))
	call_c   Dyam_Term_Arg(V(23))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_541()
	call_c   Dyam_Term_Start(&ref[541],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(N(12))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[215]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 214: '*PROLOG-ITEM*'{top=> seed_install(seed{model=> '*RITEM*'(_E, _I), id=> _K, anchor=> _L, subs_comp=> _M, code=> _N, go=> _O, tabule=> _P, schedule=> _Q, subsume=> _J, model_ref=> _R, subs_comp_ref=> _S, c_type=> _T, c_type_ref=> _U, out_env=> _V, appinfo=> _W, tail_flag=> _X}, 12, _G), cont=> _A}
c_code local build_ref_214
	ret_reg &ref[214]
	call_c   build_ref_473()
	call_c   build_ref_215()
	call_c   Dyam_Create_Binary(&ref[473],&ref[215],V(0))
	move_ret ref[214]
	c_ret

long local pool_fun57[2]=[1,build_seed_59]

pl_code local fun57
	call_c   Dyam_Pool(pool_fun57)
	pl_jump  fun10(&seed[59],12)

;; TERM 222: '$TUPPLE'(35163198462824)
c_code local build_ref_222
	ret_reg &ref[222]
	call_c   Dyam_Create_Simple_Tupple(0,290979840)
	move_ret ref[222]
	c_ret

long local pool_fun58[4]=[3,build_ref_212,build_ref_213,build_ref_223]

pl_code local fun58
	call_c   Dyam_Pool(pool_fun58)
	call_c   Dyam_Unify_Item(&ref[212])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(3))
	move     V(7), R(2)
	move     S(5), R(3)
	pl_call  pred_oset_tupple_2()
	call_c   DYAM_evpred_univ(V(8),&ref[213])
	fail_ret
	move     &ref[223], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	move     V(9), R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  fun34()

;; TERM 211: '*PROLOG-FIRST*'(unfold('*SA-CUT-LAST*'(_B, _C, _D), _E, _F, _G, _E)) :> []
c_code local build_ref_211
	ret_reg &ref[211]
	call_c   build_ref_210()
	call_c   Dyam_Create_Binary(I(9),&ref[210],I(0))
	move_ret ref[211]
	c_ret

;; TERM 210: '*PROLOG-FIRST*'(unfold('*SA-CUT-LAST*'(_B, _C, _D), _E, _F, _G, _E))
c_code local build_ref_210
	ret_reg &ref[210]
	call_c   build_ref_28()
	call_c   build_ref_209()
	call_c   Dyam_Create_Unary(&ref[28],&ref[209])
	move_ret ref[210]
	c_ret

c_code local build_seed_17
	ret_reg &seed[17]
	call_c   build_ref_27()
	call_c   build_ref_241()
	call_c   Dyam_Seed_Start(&ref[27],&ref[241],I(0),fun0,1)
	call_c   build_ref_242()
	call_c   Dyam_Seed_Add_Comp(&ref[242],fun68,0)
	call_c   Dyam_Seed_End()
	move_ret seed[17]
	c_ret

;; TERM 242: '*PROLOG-ITEM*'{top=> unfold('*SA-PROLOG-LAST*'(_B), _C, _D, (unify(_E, _F) :> call('Follow_Cont', [_G])), _H), cont=> _A}
c_code local build_ref_242
	ret_reg &ref[242]
	call_c   build_ref_473()
	call_c   build_ref_239()
	call_c   Dyam_Create_Binary(&ref[473],&ref[239],V(0))
	move_ret ref[242]
	c_ret

;; TERM 239: unfold('*SA-PROLOG-LAST*'(_B), _C, _D, (unify(_E, _F) :> call('Follow_Cont', [_G])), _H)
c_code local build_ref_239
	ret_reg &ref[239]
	call_c   Dyam_Pseudo_Choice(3)
	call_c   build_ref_485()
	call_c   Dyam_Create_Unary(&ref[485],V(1))
	move_ret R(0)
	call_c   build_ref_505()
	call_c   Dyam_Create_Binary(&ref[505],V(4),V(5))
	move_ret R(1)
	call_c   Dyam_Create_List(V(6),I(0))
	move_ret R(2)
	call_c   build_ref_486()
	call_c   build_ref_487()
	call_c   Dyam_Create_Binary(&ref[486],&ref[487],R(2))
	move_ret R(2)
	call_c   Dyam_Create_Binary(I(9),R(1),R(2))
	move_ret R(2)
	call_c   build_ref_474()
	call_c   Dyam_Term_Start(&ref[474],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(R(2))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[239]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 243: sa_env{mspop=> _I(_E,_J), aspop=> _K, holes=> _L, escape=> _M}
c_code local build_ref_243
	ret_reg &ref[243]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Term_Start(I(3),3)
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_477()
	call_c   Dyam_Term_Start(&ref[477],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_End()
	move_ret ref[243]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 244: sa_env{mspop=> _J, aspop=> _K, holes=> _L, escape=> _M}
c_code local build_ref_244
	ret_reg &ref[244]
	call_c   build_ref_477()
	call_c   Dyam_Term_Start(&ref[477],4)
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_End()
	move_ret ref[244]
	c_ret

c_code local build_seed_61
	ret_reg &seed[61]
	call_c   build_ref_33()
	call_c   build_ref_245()
	call_c   Dyam_Seed_Start(&ref[33],&ref[245],I(0),fun3,1)
	call_c   build_ref_248()
	call_c   Dyam_Seed_Add_Comp(&ref[248],&ref[245],0)
	call_c   Dyam_Seed_End()
	move_ret seed[61]
	c_ret

;; TERM 248: '*PROLOG-FIRST*'(build_closure(_B, _N, _D, _O, _H)) :> '$$HOLE$$'
c_code local build_ref_248
	ret_reg &ref[248]
	call_c   build_ref_247()
	call_c   Dyam_Create_Binary(I(9),&ref[247],I(7))
	move_ret ref[248]
	c_ret

;; TERM 247: '*PROLOG-FIRST*'(build_closure(_B, _N, _D, _O, _H))
c_code local build_ref_247
	ret_reg &ref[247]
	call_c   build_ref_28()
	call_c   build_ref_246()
	call_c   Dyam_Create_Unary(&ref[28],&ref[246])
	move_ret ref[247]
	c_ret

;; TERM 246: build_closure(_B, _N, _D, _O, _H)
c_code local build_ref_246
	ret_reg &ref[246]
	call_c   build_ref_504()
	call_c   Dyam_Term_Start(&ref[504],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[246]
	c_ret

;; TERM 245: '*PROLOG-ITEM*'{top=> build_closure(_B, _N, _D, _O, _H), cont=> '$CLOSURE'('$fun'(67, 0, 1138786712), '$TUPPLE'(35163198463176))}
c_code local build_ref_245
	ret_reg &ref[245]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,282083328)
	move_ret R(0)
	call_c   Dyam_Closure_Aux(fun67,R(0))
	move_ret R(0)
	call_c   build_ref_473()
	call_c   build_ref_246()
	call_c   Dyam_Create_Binary(&ref[473],&ref[246],R(0))
	move_ret ref[245]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

pl_code local fun67
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(14))
	call_c   Dyam_Reg_Load(2,V(5))
	pl_call  pred_label_term_2()
	call_c   Dyam_Reg_Load(0,V(8))
	call_c   Dyam_Reg_Load(2,V(6))
	pl_call  pred_label_term_2()
	call_c   Dyam_Deallocate()
	pl_jump  Follow_Cont(V(0))

long local pool_fun68[5]=[4,build_ref_242,build_ref_243,build_ref_244,build_seed_61]

pl_code local fun68
	call_c   Dyam_Pool(pool_fun68)
	call_c   Dyam_Unify_Item(&ref[242])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(2))
	move     &ref[243], R(2)
	move     S(5), R(3)
	pl_call  pred_sa_env_normalize_2()
	move     V(13), R(0)
	move     S(5), R(1)
	move     &ref[244], R(2)
	move     S(5), R(3)
	pl_call  pred_sa_env_normalize_2()
	call_c   Dyam_Deallocate()
	pl_jump  fun10(&seed[61],12)

;; TERM 241: '*PROLOG-FIRST*'(unfold('*SA-PROLOG-LAST*'(_B), _C, _D, (unify(_E, _F) :> call('Follow_Cont', [_G])), _H)) :> []
c_code local build_ref_241
	ret_reg &ref[241]
	call_c   build_ref_240()
	call_c   Dyam_Create_Binary(I(9),&ref[240],I(0))
	move_ret ref[241]
	c_ret

;; TERM 240: '*PROLOG-FIRST*'(unfold('*SA-PROLOG-LAST*'(_B), _C, _D, (unify(_E, _F) :> call('Follow_Cont', [_G])), _H))
c_code local build_ref_240
	ret_reg &ref[240]
	call_c   build_ref_28()
	call_c   build_ref_239()
	call_c   Dyam_Create_Unary(&ref[28],&ref[239])
	move_ret ref[240]
	c_ret

c_code local build_seed_7
	ret_reg &seed[7]
	call_c   build_ref_102()
	call_c   build_ref_261()
	call_c   Dyam_Seed_Start(&ref[102],&ref[261],I(0),fun0,1)
	call_c   build_ref_260()
	call_c   Dyam_Seed_Add_Comp(&ref[260],fun71,0)
	call_c   Dyam_Seed_End()
	move_ret seed[7]
	c_ret

;; TERM 260: '*GUARD*'(tag_viewer(top, _B, _C, _D, _E, _F))
c_code local build_ref_260
	ret_reg &ref[260]
	call_c   build_ref_53()
	call_c   build_ref_259()
	call_c   Dyam_Create_Unary(&ref[53],&ref[259])
	move_ret ref[260]
	c_ret

;; TERM 259: tag_viewer(top, _B, _C, _D, _E, _F)
c_code local build_ref_259
	ret_reg &ref[259]
	call_c   build_ref_546()
	call_c   build_ref_497()
	call_c   Dyam_Term_Start(&ref[546],6)
	call_c   Dyam_Term_Arg(&ref[497])
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[259]
	c_ret

c_code local build_seed_63
	ret_reg &seed[63]
	call_c   build_ref_53()
	call_c   build_ref_263()
	call_c   Dyam_Seed_Start(&ref[53],&ref[263],I(0),fun3,1)
	call_c   build_ref_264()
	call_c   Dyam_Seed_Add_Comp(&ref[264],&ref[263],0)
	call_c   Dyam_Seed_End()
	move_ret seed[63]
	c_ret

;; TERM 264: '*GUARD*'(make_tag_bot_callret(_I, _J, _K, _L, _M)) :> '$$HOLE$$'
c_code local build_ref_264
	ret_reg &ref[264]
	call_c   build_ref_263()
	call_c   Dyam_Create_Binary(I(9),&ref[263],I(7))
	move_ret ref[264]
	c_ret

;; TERM 263: '*GUARD*'(make_tag_bot_callret(_I, _J, _K, _L, _M))
c_code local build_ref_263
	ret_reg &ref[263]
	call_c   build_ref_53()
	call_c   build_ref_262()
	call_c   Dyam_Create_Unary(&ref[53],&ref[262])
	move_ret ref[263]
	c_ret

;; TERM 262: make_tag_bot_callret(_I, _J, _K, _L, _M)
c_code local build_ref_262
	ret_reg &ref[262]
	call_c   build_ref_555()
	call_c   Dyam_Term_Start(&ref[555],5)
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_End()
	move_ret ref[262]
	c_ret

;; TERM 555: make_tag_bot_callret
c_code local build_ref_555
	ret_reg &ref[555]
	call_c   Dyam_Create_Atom("make_tag_bot_callret")
	move_ret ref[555]
	c_ret

;; TERM 265: '*RAI*'{xstart=> _L, xend=> _M, aspop=> _E, current=> _F}
c_code local build_ref_265
	ret_reg &ref[265]
	call_c   build_ref_543()
	call_c   Dyam_Term_Start(&ref[543],4)
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[265]
	c_ret

;; TERM 268: _B(_C,_D) * _I(_J,_K)
c_code local build_ref_268
	ret_reg &ref[268]
	call_c   build_ref_266()
	call_c   build_ref_180()
	call_c   build_ref_267()
	call_c   Dyam_Create_Binary(&ref[266],&ref[180],&ref[267])
	move_ret ref[268]
	c_ret

;; TERM 267: _I(_J,_K)
c_code local build_ref_267
	ret_reg &ref[267]
	call_c   Dyam_Term_Start(I(3),3)
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_End()
	move_ret ref[267]
	c_ret

;; TERM 266: *
c_code local build_ref_266
	ret_reg &ref[266]
	call_c   Dyam_Create_Atom("*")
	move_ret ref[266]
	c_ret

;; TERM 269: viewer(_O, _P)
c_code local build_ref_269
	ret_reg &ref[269]
	call_c   build_ref_548()
	call_c   Dyam_Create_Binary(&ref[548],V(14),V(15))
	move_ret ref[269]
	c_ret

long local pool_fun71[6]=[5,build_ref_260,build_seed_63,build_ref_265,build_ref_268,build_ref_269]

pl_code local fun71
	call_c   Dyam_Pool(pool_fun71)
	call_c   Dyam_Unify_Item(&ref[260])
	fail_ret
	call_c   DYAM_evpred_functor(V(1),V(6),V(7))
	fail_ret
	call_c   DYAM_evpred_functor(V(8),V(6),V(7))
	fail_ret
	call_c   Dyam_Allocate(0)
	pl_call  fun10(&seed[63],1)
	call_c   Dyam_Unify(V(13),&ref[265])
	fail_ret
	call_c   DYAM_Numbervars_3(V(13),N(0),V(16))
	fail_ret
	call_c   Dyam_Reg_Load(0,V(13))
	move     V(14), R(2)
	move     S(5), R(3)
	pl_call  pred_label_term_2()
	move     &ref[268], R(0)
	move     S(5), R(1)
	move     V(15), R(2)
	move     S(5), R(3)
	pl_call  pred_label_term_2()
	move     &ref[269], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Deallocate(1)
	pl_jump  pred_record_without_doublon_1()

;; TERM 261: '*GUARD*'(tag_viewer(top, _B, _C, _D, _E, _F)) :> []
c_code local build_ref_261
	ret_reg &ref[261]
	call_c   build_ref_260()
	call_c   Dyam_Create_Binary(I(9),&ref[260],I(0))
	move_ret ref[261]
	c_ret

c_code local build_seed_11
	ret_reg &seed[11]
	call_c   build_ref_27()
	call_c   build_ref_229()
	call_c   Dyam_Seed_Start(&ref[27],&ref[229],I(0),fun0,1)
	call_c   build_ref_230()
	call_c   Dyam_Seed_Add_Comp(&ref[230],fun66,0)
	call_c   Dyam_Seed_End()
	move_ret seed[11]
	c_ret

;; TERM 230: '*PROLOG-ITEM*'{top=> app_model(application{item=> '*CFI*'{aspop=> _B, current=> _C}, trans=> ('*CFI*'{aspop=> _B, current=> _C} :> _D), item_comp=> '*CFI*'{aspop=> _B, current=> _C}, code=> _E, item_id=> _F, trans_id=> _G, mode=> _H, restrict=> _I, out_env=> _J, key=> _K}), cont=> _A}
c_code local build_ref_230
	ret_reg &ref[230]
	call_c   build_ref_473()
	call_c   build_ref_227()
	call_c   Dyam_Create_Binary(&ref[473],&ref[227],V(0))
	move_ret ref[230]
	c_ret

;; TERM 227: app_model(application{item=> '*CFI*'{aspop=> _B, current=> _C}, trans=> ('*CFI*'{aspop=> _B, current=> _C} :> _D), item_comp=> '*CFI*'{aspop=> _B, current=> _C}, code=> _E, item_id=> _F, trans_id=> _G, mode=> _H, restrict=> _I, out_env=> _J, key=> _K})
c_code local build_ref_227
	ret_reg &ref[227]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_557()
	call_c   build_ref_237()
	call_c   build_ref_238()
	call_c   Dyam_Term_Start(&ref[557],10)
	call_c   Dyam_Term_Arg(&ref[237])
	call_c   Dyam_Term_Arg(&ref[238])
	call_c   Dyam_Term_Arg(&ref[237])
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_556()
	call_c   Dyam_Create_Unary(&ref[556],R(0))
	move_ret ref[227]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 556: app_model
c_code local build_ref_556
	ret_reg &ref[556]
	call_c   Dyam_Create_Atom("app_model")
	move_ret ref[556]
	c_ret

;; TERM 557: application!'$ft'
c_code local build_ref_557
	ret_reg &ref[557]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_564()
	call_c   Dyam_Create_List(&ref[564],I(0))
	move_ret R(0)
	call_c   build_ref_526()
	call_c   Dyam_Create_List(&ref[526],R(0))
	move_ret R(0)
	call_c   build_ref_563()
	call_c   Dyam_Create_List(&ref[563],R(0))
	move_ret R(0)
	call_c   build_ref_562()
	call_c   Dyam_Create_List(&ref[562],R(0))
	move_ret R(0)
	call_c   build_ref_561()
	call_c   Dyam_Create_List(&ref[561],R(0))
	move_ret R(0)
	call_c   build_ref_560()
	call_c   Dyam_Create_List(&ref[560],R(0))
	move_ret R(0)
	call_c   build_ref_517()
	call_c   Dyam_Create_List(&ref[517],R(0))
	move_ret R(0)
	call_c   build_ref_559()
	call_c   Dyam_Create_List(&ref[559],R(0))
	move_ret R(0)
	call_c   build_ref_231()
	call_c   Dyam_Create_List(&ref[231],R(0))
	move_ret R(0)
	call_c   build_ref_236()
	call_c   Dyam_Create_List(&ref[236],R(0))
	move_ret R(0)
	call_c   build_ref_558()
	call_c   Dyam_Create_Atom_Module("$ft",&ref[558])
	move_ret ref[557]
	call_c   DYAM_Feature_2(&ref[558],R(0))
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 558: application
c_code local build_ref_558
	ret_reg &ref[558]
	call_c   Dyam_Create_Atom("application")
	move_ret ref[558]
	c_ret

;; TERM 236: item
c_code local build_ref_236
	ret_reg &ref[236]
	call_c   Dyam_Create_Atom("item")
	move_ret ref[236]
	c_ret

;; TERM 231: trans
c_code local build_ref_231
	ret_reg &ref[231]
	call_c   Dyam_Create_Atom("trans")
	move_ret ref[231]
	c_ret

;; TERM 559: item_comp
c_code local build_ref_559
	ret_reg &ref[559]
	call_c   Dyam_Create_Atom("item_comp")
	move_ret ref[559]
	c_ret

;; TERM 560: item_id
c_code local build_ref_560
	ret_reg &ref[560]
	call_c   Dyam_Create_Atom("item_id")
	move_ret ref[560]
	c_ret

;; TERM 561: trans_id
c_code local build_ref_561
	ret_reg &ref[561]
	call_c   Dyam_Create_Atom("trans_id")
	move_ret ref[561]
	c_ret

;; TERM 562: mode
c_code local build_ref_562
	ret_reg &ref[562]
	call_c   Dyam_Create_Atom("mode")
	move_ret ref[562]
	c_ret

;; TERM 563: restrict
c_code local build_ref_563
	ret_reg &ref[563]
	call_c   Dyam_Create_Atom("restrict")
	move_ret ref[563]
	c_ret

;; TERM 564: key
c_code local build_ref_564
	ret_reg &ref[564]
	call_c   Dyam_Create_Atom("key")
	move_ret ref[564]
	c_ret

pl_code local fun64
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Deallocate()
	pl_jump  Follow_Cont(V(0))

long local pool_fun65[3]=[2,build_ref_236,build_ref_238]

pl_code local fun65
	call_c   Dyam_Update_Choice(fun64)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(7),&ref[236])
	fail_ret
	call_c   Dyam_Cut()
	move     &ref[238], R(0)
	move     S(5), R(1)
	pl_call  pred_holes_1()
	call_c   Dyam_Unify(V(9),I(0))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_jump  Follow_Cont(V(0))

c_code local build_seed_60
	ret_reg &seed[60]
	call_c   build_ref_33()
	call_c   build_ref_232()
	call_c   Dyam_Seed_Start(&ref[33],&ref[232],I(0),fun3,1)
	call_c   build_ref_235()
	call_c   Dyam_Seed_Add_Comp(&ref[235],&ref[232],0)
	call_c   Dyam_Seed_End()
	move_ret seed[60]
	c_ret

;; TERM 235: '*PROLOG-FIRST*'(unfold_std_trans(('*CFI*'{aspop=> _B, current=> _C} :> _D), _E, _J)) :> '$$HOLE$$'
c_code local build_ref_235
	ret_reg &ref[235]
	call_c   build_ref_234()
	call_c   Dyam_Create_Binary(I(9),&ref[234],I(7))
	move_ret ref[235]
	c_ret

;; TERM 234: '*PROLOG-FIRST*'(unfold_std_trans(('*CFI*'{aspop=> _B, current=> _C} :> _D), _E, _J))
c_code local build_ref_234
	ret_reg &ref[234]
	call_c   build_ref_28()
	call_c   build_ref_233()
	call_c   Dyam_Create_Unary(&ref[28],&ref[233])
	move_ret ref[234]
	c_ret

;; TERM 233: unfold_std_trans(('*CFI*'{aspop=> _B, current=> _C} :> _D), _E, _J)
c_code local build_ref_233
	ret_reg &ref[233]
	call_c   build_ref_565()
	call_c   build_ref_238()
	call_c   Dyam_Term_Start(&ref[565],3)
	call_c   Dyam_Term_Arg(&ref[238])
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret ref[233]
	c_ret

;; TERM 565: unfold_std_trans
c_code local build_ref_565
	ret_reg &ref[565]
	call_c   Dyam_Create_Atom("unfold_std_trans")
	move_ret ref[565]
	c_ret

;; TERM 232: '*PROLOG-ITEM*'{top=> unfold_std_trans(('*CFI*'{aspop=> _B, current=> _C} :> _D), _E, _J), cont=> _A}
c_code local build_ref_232
	ret_reg &ref[232]
	call_c   build_ref_473()
	call_c   build_ref_233()
	call_c   Dyam_Create_Binary(&ref[473],&ref[233],V(0))
	move_ret ref[232]
	c_ret

long local pool_fun66[5]=[65539,build_ref_230,build_ref_231,build_seed_60,pool_fun65]

pl_code local fun66
	call_c   Dyam_Pool(pool_fun66)
	call_c   Dyam_Unify_Item(&ref[230])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun65)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(7),&ref[231])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Deallocate()
	pl_jump  fun10(&seed[60],12)

;; TERM 229: '*PROLOG-FIRST*'(app_model(application{item=> '*CFI*'{aspop=> _B, current=> _C}, trans=> ('*CFI*'{aspop=> _B, current=> _C} :> _D), item_comp=> '*CFI*'{aspop=> _B, current=> _C}, code=> _E, item_id=> _F, trans_id=> _G, mode=> _H, restrict=> _I, out_env=> _J, key=> _K})) :> []
c_code local build_ref_229
	ret_reg &ref[229]
	call_c   build_ref_228()
	call_c   Dyam_Create_Binary(I(9),&ref[228],I(0))
	move_ret ref[229]
	c_ret

;; TERM 228: '*PROLOG-FIRST*'(app_model(application{item=> '*CFI*'{aspop=> _B, current=> _C}, trans=> ('*CFI*'{aspop=> _B, current=> _C} :> _D), item_comp=> '*CFI*'{aspop=> _B, current=> _C}, code=> _E, item_id=> _F, trans_id=> _G, mode=> _H, restrict=> _I, out_env=> _J, key=> _K}))
c_code local build_ref_228
	ret_reg &ref[228]
	call_c   build_ref_28()
	call_c   build_ref_227()
	call_c   Dyam_Create_Unary(&ref[28],&ref[227])
	move_ret ref[228]
	c_ret

c_code local build_seed_25
	ret_reg &seed[25]
	call_c   build_ref_27()
	call_c   build_ref_272()
	call_c   Dyam_Seed_Start(&ref[27],&ref[272],I(0),fun0,1)
	call_c   build_ref_273()
	call_c   Dyam_Seed_Add_Comp(&ref[273],fun73,0)
	call_c   Dyam_Seed_End()
	move_ret seed[25]
	c_ret

;; TERM 273: '*PROLOG-ITEM*'{top=> unfold('*SA-SUBST-ALT*'(_B, _C, _D, _E), _F, _G, (_H :> _I), _J), cont=> _A}
c_code local build_ref_273
	ret_reg &ref[273]
	call_c   build_ref_473()
	call_c   build_ref_270()
	call_c   Dyam_Create_Binary(&ref[473],&ref[270],V(0))
	move_ret ref[273]
	c_ret

;; TERM 270: unfold('*SA-SUBST-ALT*'(_B, _C, _D, _E), _F, _G, (_H :> _I), _J)
c_code local build_ref_270
	ret_reg &ref[270]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_566()
	call_c   Dyam_Term_Start(&ref[566],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   Dyam_Create_Binary(I(9),V(7),V(8))
	move_ret R(1)
	call_c   build_ref_474()
	call_c   Dyam_Term_Start(&ref[474],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret ref[270]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 566: '*SA-SUBST-ALT*'
c_code local build_ref_566
	ret_reg &ref[566]
	call_c   Dyam_Create_Atom("*SA-SUBST-ALT*")
	move_ret ref[566]
	c_ret

c_code local build_seed_65
	ret_reg &seed[65]
	call_c   build_ref_33()
	call_c   build_ref_278()
	call_c   Dyam_Seed_Start(&ref[33],&ref[278],I(0),fun3,1)
	call_c   build_ref_281()
	call_c   Dyam_Seed_Add_Comp(&ref[281],&ref[278],0)
	call_c   Dyam_Seed_End()
	move_ret seed[65]
	c_ret

;; TERM 281: '*PROLOG-FIRST*'(seed_install(seed{model=> '*SACITEM*'(_B), id=> _K, anchor=> _L, subs_comp=> _M, code=> _N, go=> _O, tabule=> _P, schedule=> _Q, subsume=> _R, model_ref=> _S, subs_comp_ref=> _T, c_type=> _U, c_type_ref=> _V, out_env=> _W, appinfo=> _X, tail_flag=> _Y}, 1, _H)) :> '$$HOLE$$'
c_code local build_ref_281
	ret_reg &ref[281]
	call_c   build_ref_280()
	call_c   Dyam_Create_Binary(I(9),&ref[280],I(7))
	move_ret ref[281]
	c_ret

;; TERM 280: '*PROLOG-FIRST*'(seed_install(seed{model=> '*SACITEM*'(_B), id=> _K, anchor=> _L, subs_comp=> _M, code=> _N, go=> _O, tabule=> _P, schedule=> _Q, subsume=> _R, model_ref=> _S, subs_comp_ref=> _T, c_type=> _U, c_type_ref=> _V, out_env=> _W, appinfo=> _X, tail_flag=> _Y}, 1, _H))
c_code local build_ref_280
	ret_reg &ref[280]
	call_c   build_ref_28()
	call_c   build_ref_279()
	call_c   Dyam_Create_Unary(&ref[28],&ref[279])
	move_ret ref[280]
	c_ret

;; TERM 279: seed_install(seed{model=> '*SACITEM*'(_B), id=> _K, anchor=> _L, subs_comp=> _M, code=> _N, go=> _O, tabule=> _P, schedule=> _Q, subsume=> _R, model_ref=> _S, subs_comp_ref=> _T, c_type=> _U, c_type_ref=> _V, out_env=> _W, appinfo=> _X, tail_flag=> _Y}, 1, _H)
c_code local build_ref_279
	ret_reg &ref[279]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_509()
	call_c   build_ref_331()
	call_c   Dyam_Term_Start(&ref[509],16)
	call_c   Dyam_Term_Arg(&ref[331])
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_Arg(V(21))
	call_c   Dyam_Term_Arg(V(22))
	call_c   Dyam_Term_Arg(V(23))
	call_c   Dyam_Term_Arg(V(24))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_541()
	call_c   Dyam_Term_Start(&ref[541],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(N(1))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[279]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 278: '*PROLOG-ITEM*'{top=> seed_install(seed{model=> '*SACITEM*'(_B), id=> _K, anchor=> _L, subs_comp=> _M, code=> _N, go=> _O, tabule=> _P, schedule=> _Q, subsume=> _R, model_ref=> _S, subs_comp_ref=> _T, c_type=> _U, c_type_ref=> _V, out_env=> _W, appinfo=> _X, tail_flag=> _Y}, 1, _H), cont=> '$CLOSURE'('$fun'(72, 0, 1138858848), '$TUPPLE'(35163198463356))}
c_code local build_ref_278
	ret_reg &ref[278]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,534249472)
	move_ret R(0)
	call_c   Dyam_Closure_Aux(fun72,R(0))
	move_ret R(0)
	call_c   build_ref_473()
	call_c   build_ref_279()
	call_c   Dyam_Create_Binary(&ref[473],&ref[279],R(0))
	move_ret ref[278]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_64
	ret_reg &seed[64]
	call_c   build_ref_33()
	call_c   build_ref_274()
	call_c   Dyam_Seed_Start(&ref[33],&ref[274],I(0),fun3,1)
	call_c   build_ref_277()
	call_c   Dyam_Seed_Add_Comp(&ref[277],&ref[274],0)
	call_c   Dyam_Seed_End()
	move_ret seed[64]
	c_ret

;; TERM 277: '*PROLOG-FIRST*'(seed_install(seed{model=> ('*RITEM*'(_B, _C) :> _D(_F)(_G)), id=> _Z, anchor=> _A1, subs_comp=> _B1, code=> _C1, go=> _D1, tabule=> _E1, schedule=> _F1, subsume=> no, model_ref=> _G1, subs_comp_ref=> _H1, c_type=> _I1, c_type_ref=> _J1, out_env=> [_J], appinfo=> _K1, tail_flag=> flat}, [2,_E], _I)) :> '$$HOLE$$'
c_code local build_ref_277
	ret_reg &ref[277]
	call_c   build_ref_276()
	call_c   Dyam_Create_Binary(I(9),&ref[276],I(7))
	move_ret ref[277]
	c_ret

;; TERM 276: '*PROLOG-FIRST*'(seed_install(seed{model=> ('*RITEM*'(_B, _C) :> _D(_F)(_G)), id=> _Z, anchor=> _A1, subs_comp=> _B1, code=> _C1, go=> _D1, tabule=> _E1, schedule=> _F1, subsume=> no, model_ref=> _G1, subs_comp_ref=> _H1, c_type=> _I1, c_type_ref=> _J1, out_env=> [_J], appinfo=> _K1, tail_flag=> flat}, [2,_E], _I))
c_code local build_ref_276
	ret_reg &ref[276]
	call_c   build_ref_28()
	call_c   build_ref_275()
	call_c   Dyam_Create_Unary(&ref[28],&ref[275])
	move_ret ref[276]
	c_ret

;; TERM 275: seed_install(seed{model=> ('*RITEM*'(_B, _C) :> _D(_F)(_G)), id=> _Z, anchor=> _A1, subs_comp=> _B1, code=> _C1, go=> _D1, tabule=> _E1, schedule=> _F1, subsume=> no, model_ref=> _G1, subs_comp_ref=> _H1, c_type=> _I1, c_type_ref=> _J1, out_env=> [_J], appinfo=> _K1, tail_flag=> flat}, [2,_E], _I)
c_code local build_ref_275
	ret_reg &ref[275]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_0()
	call_c   Dyam_Create_Binary(&ref[0],V(1),V(2))
	move_ret R(0)
	call_c   Dyam_Create_Binary(I(3),V(3),V(5))
	move_ret R(1)
	call_c   Dyam_Create_Binary(I(3),R(1),V(6))
	move_ret R(1)
	call_c   Dyam_Create_Binary(I(9),R(0),R(1))
	move_ret R(1)
	call_c   Dyam_Create_List(V(9),I(0))
	move_ret R(0)
	call_c   build_ref_509()
	call_c   build_ref_507()
	call_c   build_ref_531()
	call_c   Dyam_Term_Start(&ref[509],16)
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(25))
	call_c   Dyam_Term_Arg(V(26))
	call_c   Dyam_Term_Arg(V(27))
	call_c   Dyam_Term_Arg(V(28))
	call_c   Dyam_Term_Arg(V(29))
	call_c   Dyam_Term_Arg(V(30))
	call_c   Dyam_Term_Arg(V(31))
	call_c   Dyam_Term_Arg(&ref[507])
	call_c   Dyam_Term_Arg(V(32))
	call_c   Dyam_Term_Arg(V(33))
	call_c   Dyam_Term_Arg(V(34))
	call_c   Dyam_Term_Arg(V(35))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(36))
	call_c   Dyam_Term_Arg(&ref[531])
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   Dyam_Create_List(V(4),I(0))
	move_ret R(1)
	call_c   Dyam_Create_List(N(2),R(1))
	move_ret R(1)
	call_c   build_ref_541()
	call_c   Dyam_Term_Start(&ref[541],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_End()
	move_ret ref[275]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 274: '*PROLOG-ITEM*'{top=> seed_install(seed{model=> ('*RITEM*'(_B, _C) :> _D(_F)(_G)), id=> _Z, anchor=> _A1, subs_comp=> _B1, code=> _C1, go=> _D1, tabule=> _E1, schedule=> _F1, subsume=> no, model_ref=> _G1, subs_comp_ref=> _H1, c_type=> _I1, c_type_ref=> _J1, out_env=> [_J], appinfo=> _K1, tail_flag=> flat}, [2,_E], _I), cont=> _A}
c_code local build_ref_274
	ret_reg &ref[274]
	call_c   build_ref_473()
	call_c   build_ref_275()
	call_c   Dyam_Create_Binary(&ref[473],&ref[275],V(0))
	move_ret ref[274]
	c_ret

long local pool_fun72[2]=[1,build_seed_64]

pl_code local fun72
	call_c   Dyam_Pool(pool_fun72)
	pl_jump  fun10(&seed[64],12)

long local pool_fun73[3]=[2,build_ref_273,build_seed_65]

pl_code local fun73
	call_c   Dyam_Pool(pool_fun73)
	call_c   Dyam_Unify_Item(&ref[273])
	fail_ret
	pl_jump  fun10(&seed[65],12)

;; TERM 272: '*PROLOG-FIRST*'(unfold('*SA-SUBST-ALT*'(_B, _C, _D, _E), _F, _G, (_H :> _I), _J)) :> []
c_code local build_ref_272
	ret_reg &ref[272]
	call_c   build_ref_271()
	call_c   Dyam_Create_Binary(I(9),&ref[271],I(0))
	move_ret ref[272]
	c_ret

;; TERM 271: '*PROLOG-FIRST*'(unfold('*SA-SUBST-ALT*'(_B, _C, _D, _E), _F, _G, (_H :> _I), _J))
c_code local build_ref_271
	ret_reg &ref[271]
	call_c   build_ref_28()
	call_c   build_ref_270()
	call_c   Dyam_Create_Unary(&ref[28],&ref[270])
	move_ret ref[271]
	c_ret

c_code local build_seed_10
	ret_reg &seed[10]
	call_c   build_ref_27()
	call_c   build_ref_251()
	call_c   Dyam_Seed_Start(&ref[27],&ref[251],I(0),fun0,1)
	call_c   build_ref_252()
	call_c   Dyam_Seed_Add_Comp(&ref[252],fun70,0)
	call_c   Dyam_Seed_End()
	move_ret seed[10]
	c_ret

;; TERM 252: '*PROLOG-ITEM*'{top=> app_model(application{item=> '*RFI*'{mspop=> _B, mspop_adj=> _C, current=> _D}, trans=> ('*RFI*'{mspop=> _B, mspop_adj=> _C, current=> _D} :> _E), item_comp=> '*RFI*'{mspop=> _B, mspop_adj=> _C, current=> _D}, code=> _F, item_id=> _G, trans_id=> _H, mode=> _I, restrict=> _J, out_env=> _K, key=> _L}), cont=> _A}
c_code local build_ref_252
	ret_reg &ref[252]
	call_c   build_ref_473()
	call_c   build_ref_249()
	call_c   Dyam_Create_Binary(&ref[473],&ref[249],V(0))
	move_ret ref[252]
	c_ret

;; TERM 249: app_model(application{item=> '*RFI*'{mspop=> _B, mspop_adj=> _C, current=> _D}, trans=> ('*RFI*'{mspop=> _B, mspop_adj=> _C, current=> _D} :> _E), item_comp=> '*RFI*'{mspop=> _B, mspop_adj=> _C, current=> _D}, code=> _F, item_id=> _G, trans_id=> _H, mode=> _I, restrict=> _J, out_env=> _K, key=> _L})
c_code local build_ref_249
	ret_reg &ref[249]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_557()
	call_c   build_ref_257()
	call_c   build_ref_258()
	call_c   Dyam_Term_Start(&ref[557],10)
	call_c   Dyam_Term_Arg(&ref[257])
	call_c   Dyam_Term_Arg(&ref[258])
	call_c   Dyam_Term_Arg(&ref[257])
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_556()
	call_c   Dyam_Create_Unary(&ref[556],R(0))
	move_ret ref[249]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun69[3]=[2,build_ref_236,build_ref_258]

pl_code local fun69
	call_c   Dyam_Update_Choice(fun64)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(8),&ref[236])
	fail_ret
	call_c   Dyam_Cut()
	move     &ref[258], R(0)
	move     S(5), R(1)
	pl_call  pred_holes_1()
	call_c   Dyam_Deallocate()
	pl_jump  Follow_Cont(V(0))

c_code local build_seed_62
	ret_reg &seed[62]
	call_c   build_ref_33()
	call_c   build_ref_253()
	call_c   Dyam_Seed_Start(&ref[33],&ref[253],I(0),fun3,1)
	call_c   build_ref_256()
	call_c   Dyam_Seed_Add_Comp(&ref[256],&ref[253],0)
	call_c   Dyam_Seed_End()
	move_ret seed[62]
	c_ret

;; TERM 256: '*PROLOG-FIRST*'(unfold_std_trans(('*RFI*'{mspop=> _B, mspop_adj=> _C, current=> _D} :> _E), _F, _K)) :> '$$HOLE$$'
c_code local build_ref_256
	ret_reg &ref[256]
	call_c   build_ref_255()
	call_c   Dyam_Create_Binary(I(9),&ref[255],I(7))
	move_ret ref[256]
	c_ret

;; TERM 255: '*PROLOG-FIRST*'(unfold_std_trans(('*RFI*'{mspop=> _B, mspop_adj=> _C, current=> _D} :> _E), _F, _K))
c_code local build_ref_255
	ret_reg &ref[255]
	call_c   build_ref_28()
	call_c   build_ref_254()
	call_c   Dyam_Create_Unary(&ref[28],&ref[254])
	move_ret ref[255]
	c_ret

;; TERM 254: unfold_std_trans(('*RFI*'{mspop=> _B, mspop_adj=> _C, current=> _D} :> _E), _F, _K)
c_code local build_ref_254
	ret_reg &ref[254]
	call_c   build_ref_565()
	call_c   build_ref_258()
	call_c   Dyam_Term_Start(&ref[565],3)
	call_c   Dyam_Term_Arg(&ref[258])
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_End()
	move_ret ref[254]
	c_ret

;; TERM 253: '*PROLOG-ITEM*'{top=> unfold_std_trans(('*RFI*'{mspop=> _B, mspop_adj=> _C, current=> _D} :> _E), _F, _K), cont=> _A}
c_code local build_ref_253
	ret_reg &ref[253]
	call_c   build_ref_473()
	call_c   build_ref_254()
	call_c   Dyam_Create_Binary(&ref[473],&ref[254],V(0))
	move_ret ref[253]
	c_ret

long local pool_fun70[5]=[65539,build_ref_252,build_ref_231,build_seed_62,pool_fun69]

pl_code local fun70
	call_c   Dyam_Pool(pool_fun70)
	call_c   Dyam_Unify_Item(&ref[252])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun69)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(8),&ref[231])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Deallocate()
	pl_jump  fun10(&seed[62],12)

;; TERM 251: '*PROLOG-FIRST*'(app_model(application{item=> '*RFI*'{mspop=> _B, mspop_adj=> _C, current=> _D}, trans=> ('*RFI*'{mspop=> _B, mspop_adj=> _C, current=> _D} :> _E), item_comp=> '*RFI*'{mspop=> _B, mspop_adj=> _C, current=> _D}, code=> _F, item_id=> _G, trans_id=> _H, mode=> _I, restrict=> _J, out_env=> _K, key=> _L})) :> []
c_code local build_ref_251
	ret_reg &ref[251]
	call_c   build_ref_250()
	call_c   Dyam_Create_Binary(I(9),&ref[250],I(0))
	move_ret ref[251]
	c_ret

;; TERM 250: '*PROLOG-FIRST*'(app_model(application{item=> '*RFI*'{mspop=> _B, mspop_adj=> _C, current=> _D}, trans=> ('*RFI*'{mspop=> _B, mspop_adj=> _C, current=> _D} :> _E), item_comp=> '*RFI*'{mspop=> _B, mspop_adj=> _C, current=> _D}, code=> _F, item_id=> _G, trans_id=> _H, mode=> _I, restrict=> _J, out_env=> _K, key=> _L}))
c_code local build_ref_250
	ret_reg &ref[250]
	call_c   build_ref_28()
	call_c   build_ref_249()
	call_c   Dyam_Create_Unary(&ref[28],&ref[249])
	move_ret ref[250]
	c_ret

c_code local build_seed_21
	ret_reg &seed[21]
	call_c   build_ref_27()
	call_c   build_ref_294()
	call_c   Dyam_Seed_Start(&ref[27],&ref[294],I(0),fun0,1)
	call_c   build_ref_295()
	call_c   Dyam_Seed_Add_Comp(&ref[295],fun78,0)
	call_c   Dyam_Seed_End()
	move_ret seed[21]
	c_ret

;; TERM 295: '*PROLOG-ITEM*'{top=> unfold('*SA-ADJ-FIRST*'(_B, _C, _D), _E, _F, (_G :> _H), _I), cont=> _A}
c_code local build_ref_295
	ret_reg &ref[295]
	call_c   build_ref_473()
	call_c   build_ref_292()
	call_c   Dyam_Create_Binary(&ref[473],&ref[292],V(0))
	move_ret ref[295]
	c_ret

;; TERM 292: unfold('*SA-ADJ-FIRST*'(_B, _C, _D), _E, _F, (_G :> _H), _I)
c_code local build_ref_292
	ret_reg &ref[292]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_567()
	call_c   Dyam_Term_Start(&ref[567],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   Dyam_Create_Binary(I(9),V(6),V(7))
	move_ret R(1)
	call_c   build_ref_474()
	call_c   Dyam_Term_Start(&ref[474],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_End()
	move_ret ref[292]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 567: '*SA-ADJ-FIRST*'
c_code local build_ref_567
	ret_reg &ref[567]
	call_c   Dyam_Create_Atom("*SA-ADJ-FIRST*")
	move_ret ref[567]
	c_ret

c_code local build_seed_68
	ret_reg &seed[68]
	call_c   build_ref_33()
	call_c   build_ref_300()
	call_c   Dyam_Seed_Start(&ref[33],&ref[300],I(0),fun3,1)
	call_c   build_ref_303()
	call_c   Dyam_Seed_Add_Comp(&ref[303],&ref[300],0)
	call_c   Dyam_Seed_End()
	move_ret seed[68]
	c_ret

;; TERM 303: '*PROLOG-FIRST*'(seed_install(seed{model=> '*CAI*'{current=> _B}, id=> _J, anchor=> _K, subs_comp=> _L, code=> _M, go=> _N, tabule=> _O, schedule=> _P, subsume=> _Q, model_ref=> _R, subs_comp_ref=> _S, c_type=> _T, c_type_ref=> _U, out_env=> _V, appinfo=> _W, tail_flag=> _X}, 1, _G)) :> '$$HOLE$$'
c_code local build_ref_303
	ret_reg &ref[303]
	call_c   build_ref_302()
	call_c   Dyam_Create_Binary(I(9),&ref[302],I(7))
	move_ret ref[303]
	c_ret

;; TERM 302: '*PROLOG-FIRST*'(seed_install(seed{model=> '*CAI*'{current=> _B}, id=> _J, anchor=> _K, subs_comp=> _L, code=> _M, go=> _N, tabule=> _O, schedule=> _P, subsume=> _Q, model_ref=> _R, subs_comp_ref=> _S, c_type=> _T, c_type_ref=> _U, out_env=> _V, appinfo=> _W, tail_flag=> _X}, 1, _G))
c_code local build_ref_302
	ret_reg &ref[302]
	call_c   build_ref_28()
	call_c   build_ref_301()
	call_c   Dyam_Create_Unary(&ref[28],&ref[301])
	move_ret ref[302]
	c_ret

;; TERM 301: seed_install(seed{model=> '*CAI*'{current=> _B}, id=> _J, anchor=> _K, subs_comp=> _L, code=> _M, go=> _N, tabule=> _O, schedule=> _P, subsume=> _Q, model_ref=> _R, subs_comp_ref=> _S, c_type=> _T, c_type_ref=> _U, out_env=> _V, appinfo=> _W, tail_flag=> _X}, 1, _G)
c_code local build_ref_301
	ret_reg &ref[301]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_509()
	call_c   build_ref_320()
	call_c   Dyam_Term_Start(&ref[509],16)
	call_c   Dyam_Term_Arg(&ref[320])
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_Arg(V(21))
	call_c   Dyam_Term_Arg(V(22))
	call_c   Dyam_Term_Arg(V(23))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_541()
	call_c   Dyam_Term_Start(&ref[541],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(N(1))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[301]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 300: '*PROLOG-ITEM*'{top=> seed_install(seed{model=> '*CAI*'{current=> _B}, id=> _J, anchor=> _K, subs_comp=> _L, code=> _M, go=> _N, tabule=> _O, schedule=> _P, subsume=> _Q, model_ref=> _R, subs_comp_ref=> _S, c_type=> _T, c_type_ref=> _U, out_env=> _V, appinfo=> _W, tail_flag=> _X}, 1, _G), cont=> '$CLOSURE'('$fun'(77, 0, 1138907136), '$TUPPLE'(35163198463496))}
c_code local build_ref_300
	ret_reg &ref[300]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,531628032)
	move_ret R(0)
	call_c   Dyam_Closure_Aux(fun77,R(0))
	move_ret R(0)
	call_c   build_ref_473()
	call_c   build_ref_301()
	call_c   Dyam_Create_Binary(&ref[473],&ref[301],R(0))
	move_ret ref[300]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_67
	ret_reg &seed[67]
	call_c   build_ref_33()
	call_c   build_ref_296()
	call_c   Dyam_Seed_Start(&ref[33],&ref[296],I(0),fun3,1)
	call_c   build_ref_299()
	call_c   Dyam_Seed_Add_Comp(&ref[299],&ref[296],0)
	call_c   Dyam_Seed_End()
	move_ret seed[67]
	c_ret

;; TERM 299: '*PROLOG-FIRST*'(seed_install(seed{model=> ('*CFI*'{aspop=> _B, current=> _C} :> _D(_Y)(_F)), id=> _Z, anchor=> _A1, subs_comp=> _B1, code=> _C1, go=> _D1, tabule=> _E1, schedule=> _F1, subsume=> _G1, model_ref=> _H1, subs_comp_ref=> _I1, c_type=> _J1, c_type_ref=> _K1, out_env=> [_I], appinfo=> _L1, tail_flag=> _M1}, 2, _H)) :> '$$HOLE$$'
c_code local build_ref_299
	ret_reg &ref[299]
	call_c   build_ref_298()
	call_c   Dyam_Create_Binary(I(9),&ref[298],I(7))
	move_ret ref[299]
	c_ret

;; TERM 298: '*PROLOG-FIRST*'(seed_install(seed{model=> ('*CFI*'{aspop=> _B, current=> _C} :> _D(_Y)(_F)), id=> _Z, anchor=> _A1, subs_comp=> _B1, code=> _C1, go=> _D1, tabule=> _E1, schedule=> _F1, subsume=> _G1, model_ref=> _H1, subs_comp_ref=> _I1, c_type=> _J1, c_type_ref=> _K1, out_env=> [_I], appinfo=> _L1, tail_flag=> _M1}, 2, _H))
c_code local build_ref_298
	ret_reg &ref[298]
	call_c   build_ref_28()
	call_c   build_ref_297()
	call_c   Dyam_Create_Unary(&ref[28],&ref[297])
	move_ret ref[298]
	c_ret

;; TERM 297: seed_install(seed{model=> ('*CFI*'{aspop=> _B, current=> _C} :> _D(_Y)(_F)), id=> _Z, anchor=> _A1, subs_comp=> _B1, code=> _C1, go=> _D1, tabule=> _E1, schedule=> _F1, subsume=> _G1, model_ref=> _H1, subs_comp_ref=> _I1, c_type=> _J1, c_type_ref=> _K1, out_env=> [_I], appinfo=> _L1, tail_flag=> _M1}, 2, _H)
c_code local build_ref_297
	ret_reg &ref[297]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   Dyam_Create_Binary(I(3),V(3),V(24))
	move_ret R(0)
	call_c   Dyam_Create_Binary(I(3),R(0),V(5))
	move_ret R(0)
	call_c   build_ref_237()
	call_c   Dyam_Create_Binary(I(9),&ref[237],R(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(8),I(0))
	move_ret R(1)
	call_c   build_ref_509()
	call_c   Dyam_Term_Start(&ref[509],16)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(25))
	call_c   Dyam_Term_Arg(V(26))
	call_c   Dyam_Term_Arg(V(27))
	call_c   Dyam_Term_Arg(V(28))
	call_c   Dyam_Term_Arg(V(29))
	call_c   Dyam_Term_Arg(V(30))
	call_c   Dyam_Term_Arg(V(31))
	call_c   Dyam_Term_Arg(V(32))
	call_c   Dyam_Term_Arg(V(33))
	call_c   Dyam_Term_Arg(V(34))
	call_c   Dyam_Term_Arg(V(35))
	call_c   Dyam_Term_Arg(V(36))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(37))
	call_c   Dyam_Term_Arg(V(38))
	call_c   Dyam_Term_End()
	move_ret R(1)
	call_c   build_ref_541()
	call_c   Dyam_Term_Start(&ref[541],3)
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(N(2))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[297]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 296: '*PROLOG-ITEM*'{top=> seed_install(seed{model=> ('*CFI*'{aspop=> _B, current=> _C} :> _D(_Y)(_F)), id=> _Z, anchor=> _A1, subs_comp=> _B1, code=> _C1, go=> _D1, tabule=> _E1, schedule=> _F1, subsume=> _G1, model_ref=> _H1, subs_comp_ref=> _I1, c_type=> _J1, c_type_ref=> _K1, out_env=> [_I], appinfo=> _L1, tail_flag=> _M1}, 2, _H), cont=> _A}
c_code local build_ref_296
	ret_reg &ref[296]
	call_c   build_ref_473()
	call_c   build_ref_297()
	call_c   Dyam_Create_Binary(&ref[473],&ref[297],V(0))
	move_ret ref[296]
	c_ret

long local pool_fun77[2]=[1,build_seed_67]

pl_code local fun77
	call_c   Dyam_Pool(pool_fun77)
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(4))
	move     V(24), R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Load(4,V(2))
	call_c   Dyam_Reg_Load(6,V(1))
	pl_call  pred_sa_env_extension_4()
	call_c   Dyam_Deallocate()
	pl_jump  fun10(&seed[67],12)

long local pool_fun78[3]=[2,build_ref_295,build_seed_68]

pl_code local fun78
	call_c   Dyam_Pool(pool_fun78)
	call_c   Dyam_Unify_Item(&ref[295])
	fail_ret
	pl_jump  fun10(&seed[68],12)

;; TERM 294: '*PROLOG-FIRST*'(unfold('*SA-ADJ-FIRST*'(_B, _C, _D), _E, _F, (_G :> _H), _I)) :> []
c_code local build_ref_294
	ret_reg &ref[294]
	call_c   build_ref_293()
	call_c   Dyam_Create_Binary(I(9),&ref[293],I(0))
	move_ret ref[294]
	c_ret

;; TERM 293: '*PROLOG-FIRST*'(unfold('*SA-ADJ-FIRST*'(_B, _C, _D), _E, _F, (_G :> _H), _I))
c_code local build_ref_293
	ret_reg &ref[293]
	call_c   build_ref_28()
	call_c   build_ref_292()
	call_c   Dyam_Create_Unary(&ref[28],&ref[292])
	move_ret ref[293]
	c_ret

c_code local build_seed_31
	ret_reg &seed[31]
	call_c   build_ref_27()
	call_c   build_ref_306()
	call_c   Dyam_Seed_Start(&ref[27],&ref[306],I(0),fun0,1)
	call_c   build_ref_307()
	call_c   Dyam_Seed_Add_Comp(&ref[307],fun81,0)
	call_c   Dyam_Seed_End()
	move_ret seed[31]
	c_ret

;; TERM 307: '*PROLOG-ITEM*'{top=> unfold('*SA-SUBST*'(_B, _C, _D, _E), _F, _G, (_H :> _I), _J), cont=> _A}
c_code local build_ref_307
	ret_reg &ref[307]
	call_c   build_ref_473()
	call_c   build_ref_304()
	call_c   Dyam_Create_Binary(&ref[473],&ref[304],V(0))
	move_ret ref[307]
	c_ret

;; TERM 304: unfold('*SA-SUBST*'(_B, _C, _D, _E), _F, _G, (_H :> _I), _J)
c_code local build_ref_304
	ret_reg &ref[304]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_568()
	call_c   Dyam_Term_Start(&ref[568],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   Dyam_Create_Binary(I(9),V(7),V(8))
	move_ret R(1)
	call_c   build_ref_474()
	call_c   Dyam_Term_Start(&ref[474],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret ref[304]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 568: '*SA-SUBST*'
c_code local build_ref_568
	ret_reg &ref[568]
	call_c   Dyam_Create_Atom("*SA-SUBST*")
	move_ret ref[568]
	c_ret

c_code local build_seed_70
	ret_reg &seed[70]
	call_c   build_ref_33()
	call_c   build_ref_315()
	call_c   Dyam_Seed_Start(&ref[33],&ref[315],I(0),fun3,1)
	call_c   build_ref_281()
	call_c   Dyam_Seed_Add_Comp(&ref[281],&ref[315],0)
	call_c   Dyam_Seed_End()
	move_ret seed[70]
	c_ret

;; TERM 315: '*PROLOG-ITEM*'{top=> seed_install(seed{model=> '*SACITEM*'(_B), id=> _K, anchor=> _L, subs_comp=> _M, code=> _N, go=> _O, tabule=> _P, schedule=> _Q, subsume=> _R, model_ref=> _S, subs_comp_ref=> _T, c_type=> _U, c_type_ref=> _V, out_env=> _W, appinfo=> _X, tail_flag=> _Y}, 1, _H), cont=> '$CLOSURE'('$fun'(80, 0, 1139009520), '$TUPPLE'(35163198463356))}
c_code local build_ref_315
	ret_reg &ref[315]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,534249472)
	move_ret R(0)
	call_c   Dyam_Closure_Aux(fun80,R(0))
	move_ret R(0)
	call_c   build_ref_473()
	call_c   build_ref_279()
	call_c   Dyam_Create_Binary(&ref[473],&ref[279],R(0))
	move_ret ref[315]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 314: '$CLOSURE'('$fun'(79, 0, 1139000956), '$TUPPLE'(35163198463580))
c_code local build_ref_314
	ret_reg &ref[314]
	call_c   build_ref_313()
	call_c   Dyam_Closure_Aux(fun79,&ref[313])
	move_ret ref[314]
	c_ret

c_code local build_seed_69
	ret_reg &seed[69]
	call_c   build_ref_33()
	call_c   build_ref_308()
	call_c   Dyam_Seed_Start(&ref[33],&ref[308],I(0),fun3,1)
	call_c   build_ref_311()
	call_c   Dyam_Seed_Add_Comp(&ref[311],&ref[308],0)
	call_c   Dyam_Seed_End()
	move_ret seed[69]
	c_ret

;; TERM 311: '*PROLOG-FIRST*'(seed_install(seed{model=> ('*RITEM*'(_B, _C) :> _D(_F)(_G)), id=> _A1, anchor=> _B1, subs_comp=> _C1, code=> _D1, go=> _E1, tabule=> _F1, schedule=> _G1, subsume=> _Z, model_ref=> _H1, subs_comp_ref=> _I1, c_type=> _J1, c_type_ref=> _K1, out_env=> [_J], appinfo=> _L1, tail_flag=> flat}, [2,_E], _I)) :> '$$HOLE$$'
c_code local build_ref_311
	ret_reg &ref[311]
	call_c   build_ref_310()
	call_c   Dyam_Create_Binary(I(9),&ref[310],I(7))
	move_ret ref[311]
	c_ret

;; TERM 310: '*PROLOG-FIRST*'(seed_install(seed{model=> ('*RITEM*'(_B, _C) :> _D(_F)(_G)), id=> _A1, anchor=> _B1, subs_comp=> _C1, code=> _D1, go=> _E1, tabule=> _F1, schedule=> _G1, subsume=> _Z, model_ref=> _H1, subs_comp_ref=> _I1, c_type=> _J1, c_type_ref=> _K1, out_env=> [_J], appinfo=> _L1, tail_flag=> flat}, [2,_E], _I))
c_code local build_ref_310
	ret_reg &ref[310]
	call_c   build_ref_28()
	call_c   build_ref_309()
	call_c   Dyam_Create_Unary(&ref[28],&ref[309])
	move_ret ref[310]
	c_ret

;; TERM 309: seed_install(seed{model=> ('*RITEM*'(_B, _C) :> _D(_F)(_G)), id=> _A1, anchor=> _B1, subs_comp=> _C1, code=> _D1, go=> _E1, tabule=> _F1, schedule=> _G1, subsume=> _Z, model_ref=> _H1, subs_comp_ref=> _I1, c_type=> _J1, c_type_ref=> _K1, out_env=> [_J], appinfo=> _L1, tail_flag=> flat}, [2,_E], _I)
c_code local build_ref_309
	ret_reg &ref[309]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_0()
	call_c   Dyam_Create_Binary(&ref[0],V(1),V(2))
	move_ret R(0)
	call_c   Dyam_Create_Binary(I(3),V(3),V(5))
	move_ret R(1)
	call_c   Dyam_Create_Binary(I(3),R(1),V(6))
	move_ret R(1)
	call_c   Dyam_Create_Binary(I(9),R(0),R(1))
	move_ret R(1)
	call_c   Dyam_Create_List(V(9),I(0))
	move_ret R(0)
	call_c   build_ref_509()
	call_c   build_ref_531()
	call_c   Dyam_Term_Start(&ref[509],16)
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(26))
	call_c   Dyam_Term_Arg(V(27))
	call_c   Dyam_Term_Arg(V(28))
	call_c   Dyam_Term_Arg(V(29))
	call_c   Dyam_Term_Arg(V(30))
	call_c   Dyam_Term_Arg(V(31))
	call_c   Dyam_Term_Arg(V(32))
	call_c   Dyam_Term_Arg(V(25))
	call_c   Dyam_Term_Arg(V(33))
	call_c   Dyam_Term_Arg(V(34))
	call_c   Dyam_Term_Arg(V(35))
	call_c   Dyam_Term_Arg(V(36))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(37))
	call_c   Dyam_Term_Arg(&ref[531])
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   Dyam_Create_List(V(4),I(0))
	move_ret R(1)
	call_c   Dyam_Create_List(N(2),R(1))
	move_ret R(1)
	call_c   build_ref_541()
	call_c   Dyam_Term_Start(&ref[541],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_End()
	move_ret ref[309]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 308: '*PROLOG-ITEM*'{top=> seed_install(seed{model=> ('*RITEM*'(_B, _C) :> _D(_F)(_G)), id=> _A1, anchor=> _B1, subs_comp=> _C1, code=> _D1, go=> _E1, tabule=> _F1, schedule=> _G1, subsume=> _Z, model_ref=> _H1, subs_comp_ref=> _I1, c_type=> _J1, c_type_ref=> _K1, out_env=> [_J], appinfo=> _L1, tail_flag=> flat}, [2,_E], _I), cont=> _A}
c_code local build_ref_308
	ret_reg &ref[308]
	call_c   build_ref_473()
	call_c   build_ref_309()
	call_c   Dyam_Create_Binary(&ref[473],&ref[309],V(0))
	move_ret ref[308]
	c_ret

long local pool_fun79[2]=[1,build_seed_69]

pl_code local fun79
	call_c   Dyam_Pool(pool_fun79)
	pl_jump  fun10(&seed[69],12)

;; TERM 313: '$TUPPLE'(35163198463580)
c_code local build_ref_313
	ret_reg &ref[313]
	call_c   Dyam_Create_Simple_Tupple(0,534249480)
	move_ret ref[313]
	c_ret

long local pool_fun80[2]=[1,build_ref_314]

pl_code local fun80
	call_c   Dyam_Pool(pool_fun80)
	call_c   Dyam_Allocate(0)
	move     &ref[314], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	move     V(25), R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  fun34()

long local pool_fun81[3]=[2,build_ref_307,build_seed_70]

pl_code local fun81
	call_c   Dyam_Pool(pool_fun81)
	call_c   Dyam_Unify_Item(&ref[307])
	fail_ret
	pl_jump  fun10(&seed[70],12)

;; TERM 306: '*PROLOG-FIRST*'(unfold('*SA-SUBST*'(_B, _C, _D, _E), _F, _G, (_H :> _I), _J)) :> []
c_code local build_ref_306
	ret_reg &ref[306]
	call_c   build_ref_305()
	call_c   Dyam_Create_Binary(I(9),&ref[305],I(0))
	move_ret ref[306]
	c_ret

;; TERM 305: '*PROLOG-FIRST*'(unfold('*SA-SUBST*'(_B, _C, _D, _E), _F, _G, (_H :> _I), _J))
c_code local build_ref_305
	ret_reg &ref[305]
	call_c   build_ref_28()
	call_c   build_ref_304()
	call_c   Dyam_Create_Unary(&ref[28],&ref[304])
	move_ret ref[305]
	c_ret

c_code local build_seed_12
	ret_reg &seed[12]
	call_c   build_ref_27()
	call_c   build_ref_318()
	call_c   Dyam_Seed_Start(&ref[27],&ref[318],I(0),fun0,1)
	call_c   build_ref_319()
	call_c   Dyam_Seed_Add_Comp(&ref[319],fun83,0)
	call_c   Dyam_Seed_End()
	move_ret seed[12]
	c_ret

;; TERM 319: '*PROLOG-ITEM*'{top=> app_model(application{item=> '*CAI*'{current=> _B}, trans=> ('*SA-AUX-FIRST*'(_B) :> _C), item_comp=> '*CAI*'{current=> _B}, code=> (item_unify(_D) :> allocate :> _E), item_id=> _F, trans_id=> _G, mode=> _H, restrict=> _I, out_env=> _J, key=> _K}), cont=> _A}
c_code local build_ref_319
	ret_reg &ref[319]
	call_c   build_ref_473()
	call_c   build_ref_316()
	call_c   Dyam_Create_Binary(&ref[473],&ref[316],V(0))
	move_ret ref[319]
	c_ret

;; TERM 316: app_model(application{item=> '*CAI*'{current=> _B}, trans=> ('*SA-AUX-FIRST*'(_B) :> _C), item_comp=> '*CAI*'{current=> _B}, code=> (item_unify(_D) :> allocate :> _E), item_id=> _F, trans_id=> _G, mode=> _H, restrict=> _I, out_env=> _J, key=> _K})
c_code local build_ref_316
	ret_reg &ref[316]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_569()
	call_c   Dyam_Create_Unary(&ref[569],V(3))
	move_ret R(0)
	call_c   build_ref_57()
	call_c   Dyam_Create_Binary(I(9),&ref[57],V(4))
	move_ret R(1)
	call_c   Dyam_Create_Binary(I(9),R(0),R(1))
	move_ret R(1)
	call_c   build_ref_557()
	call_c   build_ref_320()
	call_c   build_ref_326()
	call_c   Dyam_Term_Start(&ref[557],10)
	call_c   Dyam_Term_Arg(&ref[320])
	call_c   Dyam_Term_Arg(&ref[326])
	call_c   Dyam_Term_Arg(&ref[320])
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_End()
	move_ret R(1)
	call_c   build_ref_556()
	call_c   Dyam_Create_Unary(&ref[556],R(1))
	move_ret ref[316]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 569: item_unify
c_code local build_ref_569
	ret_reg &ref[569]
	call_c   Dyam_Create_Atom("item_unify")
	move_ret ref[569]
	c_ret

long local pool_fun82[3]=[2,build_ref_236,build_ref_326]

pl_code local fun82
	call_c   Dyam_Update_Choice(fun64)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(7),&ref[236])
	fail_ret
	call_c   Dyam_Cut()
	move     &ref[326], R(0)
	move     S(5), R(1)
	pl_call  pred_holes_1()
	call_c   Dyam_Unify(V(9),I(0))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_jump  Follow_Cont(V(0))

c_code local build_seed_71
	ret_reg &seed[71]
	call_c   build_ref_33()
	call_c   build_ref_321()
	call_c   Dyam_Seed_Start(&ref[33],&ref[321],I(0),fun3,1)
	call_c   build_ref_324()
	call_c   Dyam_Seed_Add_Comp(&ref[324],&ref[321],0)
	call_c   Dyam_Seed_End()
	move_ret seed[71]
	c_ret

;; TERM 324: '*PROLOG-FIRST*'(unfold((_C :> deallocate :> succeed), _B, _L, _E, _J)) :> '$$HOLE$$'
c_code local build_ref_324
	ret_reg &ref[324]
	call_c   build_ref_323()
	call_c   Dyam_Create_Binary(I(9),&ref[323],I(7))
	move_ret ref[324]
	c_ret

;; TERM 323: '*PROLOG-FIRST*'(unfold((_C :> deallocate :> succeed), _B, _L, _E, _J))
c_code local build_ref_323
	ret_reg &ref[323]
	call_c   build_ref_28()
	call_c   build_ref_322()
	call_c   Dyam_Create_Unary(&ref[28],&ref[322])
	move_ret ref[323]
	c_ret

;; TERM 322: unfold((_C :> deallocate :> succeed), _B, _L, _E, _J)
c_code local build_ref_322
	ret_reg &ref[322]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_353()
	call_c   Dyam_Create_Binary(I(9),V(2),&ref[353])
	move_ret R(0)
	call_c   build_ref_474()
	call_c   Dyam_Term_Start(&ref[474],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret ref[322]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 321: '*PROLOG-ITEM*'{top=> unfold((_C :> deallocate :> succeed), _B, _L, _E, _J), cont=> _A}
c_code local build_ref_321
	ret_reg &ref[321]
	call_c   build_ref_473()
	call_c   build_ref_322()
	call_c   Dyam_Create_Binary(&ref[473],&ref[322],V(0))
	move_ret ref[321]
	c_ret

long local pool_fun83[6]=[65540,build_ref_319,build_ref_231,build_ref_320,build_seed_71,pool_fun82]

pl_code local fun83
	call_c   Dyam_Pool(pool_fun83)
	call_c   Dyam_Unify_Item(&ref[319])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun82)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(7),&ref[231])
	fail_ret
	call_c   Dyam_Cut()
	move     &ref[320], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(3))
	pl_call  pred_label_term_2()
	call_c   Dyam_Reg_Load(0,V(1))
	move     V(11), R(2)
	move     S(5), R(3)
	pl_call  pred_tupple_2()
	call_c   Dyam_Deallocate()
	pl_jump  fun10(&seed[71],12)

;; TERM 318: '*PROLOG-FIRST*'(app_model(application{item=> '*CAI*'{current=> _B}, trans=> ('*SA-AUX-FIRST*'(_B) :> _C), item_comp=> '*CAI*'{current=> _B}, code=> (item_unify(_D) :> allocate :> _E), item_id=> _F, trans_id=> _G, mode=> _H, restrict=> _I, out_env=> _J, key=> _K})) :> []
c_code local build_ref_318
	ret_reg &ref[318]
	call_c   build_ref_317()
	call_c   Dyam_Create_Binary(I(9),&ref[317],I(0))
	move_ret ref[318]
	c_ret

;; TERM 317: '*PROLOG-FIRST*'(app_model(application{item=> '*CAI*'{current=> _B}, trans=> ('*SA-AUX-FIRST*'(_B) :> _C), item_comp=> '*CAI*'{current=> _B}, code=> (item_unify(_D) :> allocate :> _E), item_id=> _F, trans_id=> _G, mode=> _H, restrict=> _I, out_env=> _J, key=> _K}))
c_code local build_ref_317
	ret_reg &ref[317]
	call_c   build_ref_28()
	call_c   build_ref_316()
	call_c   Dyam_Create_Unary(&ref[28],&ref[316])
	move_ret ref[317]
	c_ret

c_code local build_seed_27
	ret_reg &seed[27]
	call_c   build_ref_27()
	call_c   build_ref_329()
	call_c   Dyam_Seed_Start(&ref[27],&ref[329],I(0),fun0,1)
	call_c   build_ref_330()
	call_c   Dyam_Seed_Add_Comp(&ref[330],fun85,0)
	call_c   Dyam_Seed_End()
	move_ret seed[27]
	c_ret

;; TERM 330: '*PROLOG-ITEM*'{top=> app_model(application{item=> '*SACITEM*'(_B), trans=> ('*SAFIRST*'(_B) :> _C), item_comp=> '*SACITEM*'(_B), code=> (item_unify(_D) :> allocate :> _E), item_id=> _F, trans_id=> _G, mode=> _H, restrict=> _I, out_env=> _J, key=> _K}), cont=> _A}
c_code local build_ref_330
	ret_reg &ref[330]
	call_c   build_ref_473()
	call_c   build_ref_327()
	call_c   Dyam_Create_Binary(&ref[473],&ref[327],V(0))
	move_ret ref[330]
	c_ret

;; TERM 327: app_model(application{item=> '*SACITEM*'(_B), trans=> ('*SAFIRST*'(_B) :> _C), item_comp=> '*SACITEM*'(_B), code=> (item_unify(_D) :> allocate :> _E), item_id=> _F, trans_id=> _G, mode=> _H, restrict=> _I, out_env=> _J, key=> _K})
c_code local build_ref_327
	ret_reg &ref[327]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_569()
	call_c   Dyam_Create_Unary(&ref[569],V(3))
	move_ret R(0)
	call_c   build_ref_57()
	call_c   Dyam_Create_Binary(I(9),&ref[57],V(4))
	move_ret R(1)
	call_c   Dyam_Create_Binary(I(9),R(0),R(1))
	move_ret R(1)
	call_c   build_ref_557()
	call_c   build_ref_331()
	call_c   build_ref_333()
	call_c   Dyam_Term_Start(&ref[557],10)
	call_c   Dyam_Term_Arg(&ref[331])
	call_c   Dyam_Term_Arg(&ref[333])
	call_c   Dyam_Term_Arg(&ref[331])
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_End()
	move_ret R(1)
	call_c   build_ref_556()
	call_c   Dyam_Create_Unary(&ref[556],R(1))
	move_ret ref[327]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun84[3]=[2,build_ref_236,build_ref_333]

pl_code local fun84
	call_c   Dyam_Update_Choice(fun64)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(7),&ref[236])
	fail_ret
	call_c   Dyam_Cut()
	move     &ref[333], R(0)
	move     S(5), R(1)
	pl_call  pred_holes_1()
	call_c   Dyam_Unify(V(9),I(0))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_jump  Follow_Cont(V(0))

long local pool_fun85[6]=[65540,build_ref_330,build_ref_231,build_ref_331,build_seed_71,pool_fun84]

pl_code local fun85
	call_c   Dyam_Pool(pool_fun85)
	call_c   Dyam_Unify_Item(&ref[330])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun84)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(7),&ref[231])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(1))
	move     V(11), R(2)
	move     S(5), R(3)
	pl_call  pred_tupple_2()
	move     &ref[331], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(3))
	pl_call  pred_label_term_2()
	call_c   Dyam_Deallocate()
	pl_jump  fun10(&seed[71],12)

;; TERM 329: '*PROLOG-FIRST*'(app_model(application{item=> '*SACITEM*'(_B), trans=> ('*SAFIRST*'(_B) :> _C), item_comp=> '*SACITEM*'(_B), code=> (item_unify(_D) :> allocate :> _E), item_id=> _F, trans_id=> _G, mode=> _H, restrict=> _I, out_env=> _J, key=> _K})) :> []
c_code local build_ref_329
	ret_reg &ref[329]
	call_c   build_ref_328()
	call_c   Dyam_Create_Binary(I(9),&ref[328],I(0))
	move_ret ref[329]
	c_ret

;; TERM 328: '*PROLOG-FIRST*'(app_model(application{item=> '*SACITEM*'(_B), trans=> ('*SAFIRST*'(_B) :> _C), item_comp=> '*SACITEM*'(_B), code=> (item_unify(_D) :> allocate :> _E), item_id=> _F, trans_id=> _G, mode=> _H, restrict=> _I, out_env=> _J, key=> _K}))
c_code local build_ref_328
	ret_reg &ref[328]
	call_c   build_ref_28()
	call_c   build_ref_327()
	call_c   Dyam_Create_Unary(&ref[28],&ref[327])
	move_ret ref[328]
	c_ret

c_code local build_seed_9
	ret_reg &seed[9]
	call_c   build_ref_27()
	call_c   build_ref_284()
	call_c   Dyam_Seed_Start(&ref[27],&ref[284],I(0),fun0,1)
	call_c   build_ref_285()
	call_c   Dyam_Seed_Add_Comp(&ref[285],fun76,0)
	call_c   Dyam_Seed_End()
	move_ret seed[9]
	c_ret

;; TERM 285: '*PROLOG-ITEM*'{top=> app_model(application{item=> '*RAI*'{xstart=> _B, xend=> _C, aspop=> _D, current=> _E}, trans=> ('*RAI*'{xstart=> _B, xend=> _C, aspop=> _D, current=> _E} :> _F), item_comp=> '*RAI*'{xstart=> _B, xend=> _C, aspop=> _D, current=> _E}, code=> _G, item_id=> _H, trans_id=> _I, mode=> _J, restrict=> _K, out_env=> _L, key=> _M}), cont=> _A}
c_code local build_ref_285
	ret_reg &ref[285]
	call_c   build_ref_473()
	call_c   build_ref_282()
	call_c   Dyam_Create_Binary(&ref[473],&ref[282],V(0))
	move_ret ref[285]
	c_ret

;; TERM 282: app_model(application{item=> '*RAI*'{xstart=> _B, xend=> _C, aspop=> _D, current=> _E}, trans=> ('*RAI*'{xstart=> _B, xend=> _C, aspop=> _D, current=> _E} :> _F), item_comp=> '*RAI*'{xstart=> _B, xend=> _C, aspop=> _D, current=> _E}, code=> _G, item_id=> _H, trans_id=> _I, mode=> _J, restrict=> _K, out_env=> _L, key=> _M})
c_code local build_ref_282
	ret_reg &ref[282]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_557()
	call_c   build_ref_290()
	call_c   build_ref_291()
	call_c   Dyam_Term_Start(&ref[557],10)
	call_c   Dyam_Term_Arg(&ref[290])
	call_c   Dyam_Term_Arg(&ref[291])
	call_c   Dyam_Term_Arg(&ref[290])
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_556()
	call_c   Dyam_Create_Unary(&ref[556],R(0))
	move_ret ref[282]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun75[3]=[2,build_ref_236,build_ref_291]

pl_code local fun75
	call_c   Dyam_Update_Choice(fun64)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(9),&ref[236])
	fail_ret
	call_c   Dyam_Cut()
	move     &ref[291], R(0)
	move     S(5), R(1)
	pl_call  pred_holes_1()
	call_c   Dyam_Deallocate()
	pl_jump  Follow_Cont(V(0))

c_code local build_seed_66
	ret_reg &seed[66]
	call_c   build_ref_33()
	call_c   build_ref_286()
	call_c   Dyam_Seed_Start(&ref[33],&ref[286],I(0),fun3,1)
	call_c   build_ref_289()
	call_c   Dyam_Seed_Add_Comp(&ref[289],&ref[286],0)
	call_c   Dyam_Seed_End()
	move_ret seed[66]
	c_ret

;; TERM 289: '*PROLOG-FIRST*'(unfold_std_trans(('*RAI*'{xstart=> _B, xend=> _C, aspop=> _D, current=> _E} :> _F), _G, _L)) :> '$$HOLE$$'
c_code local build_ref_289
	ret_reg &ref[289]
	call_c   build_ref_288()
	call_c   Dyam_Create_Binary(I(9),&ref[288],I(7))
	move_ret ref[289]
	c_ret

;; TERM 288: '*PROLOG-FIRST*'(unfold_std_trans(('*RAI*'{xstart=> _B, xend=> _C, aspop=> _D, current=> _E} :> _F), _G, _L))
c_code local build_ref_288
	ret_reg &ref[288]
	call_c   build_ref_28()
	call_c   build_ref_287()
	call_c   Dyam_Create_Unary(&ref[28],&ref[287])
	move_ret ref[288]
	c_ret

;; TERM 287: unfold_std_trans(('*RAI*'{xstart=> _B, xend=> _C, aspop=> _D, current=> _E} :> _F), _G, _L)
c_code local build_ref_287
	ret_reg &ref[287]
	call_c   build_ref_565()
	call_c   build_ref_291()
	call_c   Dyam_Term_Start(&ref[565],3)
	call_c   Dyam_Term_Arg(&ref[291])
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_End()
	move_ret ref[287]
	c_ret

;; TERM 286: '*PROLOG-ITEM*'{top=> unfold_std_trans(('*RAI*'{xstart=> _B, xend=> _C, aspop=> _D, current=> _E} :> _F), _G, _L), cont=> _A}
c_code local build_ref_286
	ret_reg &ref[286]
	call_c   build_ref_473()
	call_c   build_ref_287()
	call_c   Dyam_Create_Binary(&ref[473],&ref[287],V(0))
	move_ret ref[286]
	c_ret

long local pool_fun76[5]=[65539,build_ref_285,build_ref_231,build_seed_66,pool_fun75]

pl_code local fun76
	call_c   Dyam_Pool(pool_fun76)
	call_c   Dyam_Unify_Item(&ref[285])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun75)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(9),&ref[231])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Deallocate()
	pl_jump  fun10(&seed[66],12)

;; TERM 284: '*PROLOG-FIRST*'(app_model(application{item=> '*RAI*'{xstart=> _B, xend=> _C, aspop=> _D, current=> _E}, trans=> ('*RAI*'{xstart=> _B, xend=> _C, aspop=> _D, current=> _E} :> _F), item_comp=> '*RAI*'{xstart=> _B, xend=> _C, aspop=> _D, current=> _E}, code=> _G, item_id=> _H, trans_id=> _I, mode=> _J, restrict=> _K, out_env=> _L, key=> _M})) :> []
c_code local build_ref_284
	ret_reg &ref[284]
	call_c   build_ref_283()
	call_c   Dyam_Create_Binary(I(9),&ref[283],I(0))
	move_ret ref[284]
	c_ret

;; TERM 283: '*PROLOG-FIRST*'(app_model(application{item=> '*RAI*'{xstart=> _B, xend=> _C, aspop=> _D, current=> _E}, trans=> ('*RAI*'{xstart=> _B, xend=> _C, aspop=> _D, current=> _E} :> _F), item_comp=> '*RAI*'{xstart=> _B, xend=> _C, aspop=> _D, current=> _E}, code=> _G, item_id=> _H, trans_id=> _I, mode=> _J, restrict=> _K, out_env=> _L, key=> _M}))
c_code local build_ref_283
	ret_reg &ref[283]
	call_c   build_ref_28()
	call_c   build_ref_282()
	call_c   Dyam_Create_Unary(&ref[28],&ref[282])
	move_ret ref[283]
	c_ret

c_code local build_seed_19
	ret_reg &seed[19]
	call_c   build_ref_27()
	call_c   build_ref_336()
	call_c   Dyam_Seed_Start(&ref[27],&ref[336],I(0),fun0,1)
	call_c   build_ref_337()
	call_c   Dyam_Seed_Add_Comp(&ref[337],fun87,0)
	call_c   Dyam_Seed_End()
	move_ret seed[19]
	c_ret

;; TERM 337: '*PROLOG-ITEM*'{top=> unfold('*SA-FOOT*'(_B, _C, _D), _E, _F, (_G :> _H), _I), cont=> _A}
c_code local build_ref_337
	ret_reg &ref[337]
	call_c   build_ref_473()
	call_c   build_ref_334()
	call_c   Dyam_Create_Binary(&ref[473],&ref[334],V(0))
	move_ret ref[337]
	c_ret

;; TERM 334: unfold('*SA-FOOT*'(_B, _C, _D), _E, _F, (_G :> _H), _I)
c_code local build_ref_334
	ret_reg &ref[334]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_570()
	call_c   Dyam_Term_Start(&ref[570],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   Dyam_Create_Binary(I(9),V(6),V(7))
	move_ret R(1)
	call_c   build_ref_474()
	call_c   Dyam_Term_Start(&ref[474],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_End()
	move_ret ref[334]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 570: '*SA-FOOT*'
c_code local build_ref_570
	ret_reg &ref[570]
	call_c   Dyam_Create_Atom("*SA-FOOT*")
	move_ret ref[570]
	c_ret

c_code local build_seed_73
	ret_reg &seed[73]
	call_c   build_ref_33()
	call_c   build_ref_342()
	call_c   Dyam_Seed_Start(&ref[33],&ref[342],I(0),fun3,1)
	call_c   build_ref_345()
	call_c   Dyam_Seed_Add_Comp(&ref[345],&ref[342],0)
	call_c   Dyam_Seed_End()
	move_ret seed[73]
	c_ret

;; TERM 345: '*PROLOG-FIRST*'(seed_install(seed{model=> '*CFI*'{aspop=> _K, current=> _B}, id=> _N, anchor=> _O, subs_comp=> _P, code=> _Q, go=> _R, tabule=> _S, schedule=> _T, subsume=> _U, model_ref=> _V, subs_comp_ref=> _W, c_type=> _X, c_type_ref=> _Y, out_env=> _Z, appinfo=> _A1, tail_flag=> _B1}, 1, _G)) :> '$$HOLE$$'
c_code local build_ref_345
	ret_reg &ref[345]
	call_c   build_ref_344()
	call_c   Dyam_Create_Binary(I(9),&ref[344],I(7))
	move_ret ref[345]
	c_ret

;; TERM 344: '*PROLOG-FIRST*'(seed_install(seed{model=> '*CFI*'{aspop=> _K, current=> _B}, id=> _N, anchor=> _O, subs_comp=> _P, code=> _Q, go=> _R, tabule=> _S, schedule=> _T, subsume=> _U, model_ref=> _V, subs_comp_ref=> _W, c_type=> _X, c_type_ref=> _Y, out_env=> _Z, appinfo=> _A1, tail_flag=> _B1}, 1, _G))
c_code local build_ref_344
	ret_reg &ref[344]
	call_c   build_ref_28()
	call_c   build_ref_343()
	call_c   Dyam_Create_Unary(&ref[28],&ref[343])
	move_ret ref[344]
	c_ret

;; TERM 343: seed_install(seed{model=> '*CFI*'{aspop=> _K, current=> _B}, id=> _N, anchor=> _O, subs_comp=> _P, code=> _Q, go=> _R, tabule=> _S, schedule=> _T, subsume=> _U, model_ref=> _V, subs_comp_ref=> _W, c_type=> _X, c_type_ref=> _Y, out_env=> _Z, appinfo=> _A1, tail_flag=> _B1}, 1, _G)
c_code local build_ref_343
	ret_reg &ref[343]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_532()
	call_c   Dyam_Create_Binary(&ref[532],V(10),V(1))
	move_ret R(0)
	call_c   build_ref_509()
	call_c   Dyam_Term_Start(&ref[509],16)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_Arg(V(21))
	call_c   Dyam_Term_Arg(V(22))
	call_c   Dyam_Term_Arg(V(23))
	call_c   Dyam_Term_Arg(V(24))
	call_c   Dyam_Term_Arg(V(25))
	call_c   Dyam_Term_Arg(V(26))
	call_c   Dyam_Term_Arg(V(27))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_541()
	call_c   Dyam_Term_Start(&ref[541],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(N(1))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[343]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 342: '*PROLOG-ITEM*'{top=> seed_install(seed{model=> '*CFI*'{aspop=> _K, current=> _B}, id=> _N, anchor=> _O, subs_comp=> _P, code=> _Q, go=> _R, tabule=> _S, schedule=> _T, subsume=> _U, model_ref=> _V, subs_comp_ref=> _W, c_type=> _X, c_type_ref=> _Y, out_env=> _Z, appinfo=> _A1, tail_flag=> _B1}, 1, _G), cont=> '$CLOSURE'('$fun'(86, 0, 1139065976), '$TUPPLE'(35163198981152))}
c_code local build_ref_342
	ret_reg &ref[342]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,515768320)
	move_ret R(0)
	call_c   Dyam_Closure_Aux(fun86,R(0))
	move_ret R(0)
	call_c   build_ref_473()
	call_c   build_ref_343()
	call_c   Dyam_Create_Binary(&ref[473],&ref[343],R(0))
	move_ret ref[342]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_72
	ret_reg &seed[72]
	call_c   build_ref_33()
	call_c   build_ref_338()
	call_c   Dyam_Seed_Start(&ref[33],&ref[338],I(0),fun3,1)
	call_c   build_ref_341()
	call_c   Dyam_Seed_Add_Comp(&ref[341],&ref[338],0)
	call_c   Dyam_Seed_End()
	move_ret seed[72]
	c_ret

;; TERM 341: '*PROLOG-FIRST*'(seed_install(seed{model=> ('*RFI*'{mspop=> _B, mspop_adj=> _K, current=> _C} :> _D(sa_env{mspop=> _J, aspop=> _K, holes=> _L, escape=> sa_escape{start=> _B, end=> _C}})(_F)), id=> _C1, anchor=> _D1, subs_comp=> _E1, code=> _F1, go=> _G1, tabule=> _H1, schedule=> _I1, subsume=> _J1, model_ref=> _K1, subs_comp_ref=> _L1, c_type=> _M1, c_type_ref=> _N1, out_env=> [_I], appinfo=> _O1, tail_flag=> flat}, 2, _H)) :> '$$HOLE$$'
c_code local build_ref_341
	ret_reg &ref[341]
	call_c   build_ref_340()
	call_c   Dyam_Create_Binary(I(9),&ref[340],I(7))
	move_ret ref[341]
	c_ret

;; TERM 340: '*PROLOG-FIRST*'(seed_install(seed{model=> ('*RFI*'{mspop=> _B, mspop_adj=> _K, current=> _C} :> _D(sa_env{mspop=> _J, aspop=> _K, holes=> _L, escape=> sa_escape{start=> _B, end=> _C}})(_F)), id=> _C1, anchor=> _D1, subs_comp=> _E1, code=> _F1, go=> _G1, tabule=> _H1, schedule=> _I1, subsume=> _J1, model_ref=> _K1, subs_comp_ref=> _L1, c_type=> _M1, c_type_ref=> _N1, out_env=> [_I], appinfo=> _O1, tail_flag=> flat}, 2, _H))
c_code local build_ref_340
	ret_reg &ref[340]
	call_c   build_ref_28()
	call_c   build_ref_339()
	call_c   Dyam_Create_Unary(&ref[28],&ref[339])
	move_ret ref[340]
	c_ret

;; TERM 339: seed_install(seed{model=> ('*RFI*'{mspop=> _B, mspop_adj=> _K, current=> _C} :> _D(sa_env{mspop=> _J, aspop=> _K, holes=> _L, escape=> sa_escape{start=> _B, end=> _C}})(_F)), id=> _C1, anchor=> _D1, subs_comp=> _E1, code=> _F1, go=> _G1, tabule=> _H1, schedule=> _I1, subsume=> _J1, model_ref=> _K1, subs_comp_ref=> _L1, c_type=> _M1, c_type_ref=> _N1, out_env=> [_I], appinfo=> _O1, tail_flag=> flat}, 2, _H)
c_code local build_ref_339
	ret_reg &ref[339]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_537()
	call_c   Dyam_Term_Start(&ref[537],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_466()
	call_c   Dyam_Create_Binary(&ref[466],V(1),V(2))
	move_ret R(1)
	call_c   build_ref_477()
	call_c   Dyam_Term_Start(&ref[477],4)
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_End()
	move_ret R(1)
	call_c   Dyam_Create_Binary(I(3),V(3),R(1))
	move_ret R(1)
	call_c   Dyam_Create_Binary(I(3),R(1),V(5))
	move_ret R(1)
	call_c   Dyam_Create_Binary(I(9),R(0),R(1))
	move_ret R(1)
	call_c   Dyam_Create_List(V(8),I(0))
	move_ret R(0)
	call_c   build_ref_509()
	call_c   build_ref_531()
	call_c   Dyam_Term_Start(&ref[509],16)
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(28))
	call_c   Dyam_Term_Arg(V(29))
	call_c   Dyam_Term_Arg(V(30))
	call_c   Dyam_Term_Arg(V(31))
	call_c   Dyam_Term_Arg(V(32))
	call_c   Dyam_Term_Arg(V(33))
	call_c   Dyam_Term_Arg(V(34))
	call_c   Dyam_Term_Arg(V(35))
	call_c   Dyam_Term_Arg(V(36))
	call_c   Dyam_Term_Arg(V(37))
	call_c   Dyam_Term_Arg(V(38))
	call_c   Dyam_Term_Arg(V(39))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(40))
	call_c   Dyam_Term_Arg(&ref[531])
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_541()
	call_c   Dyam_Term_Start(&ref[541],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(N(2))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[339]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 338: '*PROLOG-ITEM*'{top=> seed_install(seed{model=> ('*RFI*'{mspop=> _B, mspop_adj=> _K, current=> _C} :> _D(sa_env{mspop=> _J, aspop=> _K, holes=> _L, escape=> sa_escape{start=> _B, end=> _C}})(_F)), id=> _C1, anchor=> _D1, subs_comp=> _E1, code=> _F1, go=> _G1, tabule=> _H1, schedule=> _I1, subsume=> _J1, model_ref=> _K1, subs_comp_ref=> _L1, c_type=> _M1, c_type_ref=> _N1, out_env=> [_I], appinfo=> _O1, tail_flag=> flat}, 2, _H), cont=> _A}
c_code local build_ref_338
	ret_reg &ref[338]
	call_c   build_ref_473()
	call_c   build_ref_339()
	call_c   Dyam_Create_Binary(&ref[473],&ref[339],V(0))
	move_ret ref[338]
	c_ret

long local pool_fun86[2]=[1,build_seed_72]

pl_code local fun86
	call_c   Dyam_Pool(pool_fun86)
	pl_jump  fun10(&seed[72],12)

long local pool_fun87[4]=[3,build_ref_337,build_ref_244,build_seed_73]

pl_code local fun87
	call_c   Dyam_Pool(pool_fun87)
	call_c   Dyam_Unify_Item(&ref[337])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(4))
	move     &ref[244], R(2)
	move     S(5), R(3)
	pl_call  pred_sa_env_normalize_2()
	call_c   Dyam_Deallocate()
	pl_jump  fun10(&seed[73],12)

;; TERM 336: '*PROLOG-FIRST*'(unfold('*SA-FOOT*'(_B, _C, _D), _E, _F, (_G :> _H), _I)) :> []
c_code local build_ref_336
	ret_reg &ref[336]
	call_c   build_ref_335()
	call_c   Dyam_Create_Binary(I(9),&ref[335],I(0))
	move_ret ref[336]
	c_ret

;; TERM 335: '*PROLOG-FIRST*'(unfold('*SA-FOOT*'(_B, _C, _D), _E, _F, (_G :> _H), _I))
c_code local build_ref_335
	ret_reg &ref[335]
	call_c   build_ref_28()
	call_c   build_ref_334()
	call_c   Dyam_Create_Unary(&ref[28],&ref[334])
	move_ret ref[335]
	c_ret

c_code local build_seed_30
	ret_reg &seed[30]
	call_c   build_ref_27()
	call_c   build_ref_348()
	call_c   Dyam_Seed_Start(&ref[27],&ref[348],I(0),fun0,1)
	call_c   build_ref_349()
	call_c   Dyam_Seed_Add_Comp(&ref[349],fun91,0)
	call_c   Dyam_Seed_End()
	move_ret seed[30]
	c_ret

;; TERM 349: '*PROLOG-ITEM*'{top=> unfold('*SA-SUBST-LIGHT*'(_B, _C, _D, _E), _F, _G, (_H :> _I), _J), cont=> _A}
c_code local build_ref_349
	ret_reg &ref[349]
	call_c   build_ref_473()
	call_c   build_ref_346()
	call_c   Dyam_Create_Binary(&ref[473],&ref[346],V(0))
	move_ret ref[349]
	c_ret

;; TERM 346: unfold('*SA-SUBST-LIGHT*'(_B, _C, _D, _E), _F, _G, (_H :> _I), _J)
c_code local build_ref_346
	ret_reg &ref[346]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_571()
	call_c   Dyam_Term_Start(&ref[571],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   Dyam_Create_Binary(I(9),V(7),V(8))
	move_ret R(1)
	call_c   build_ref_474()
	call_c   Dyam_Term_Start(&ref[474],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret ref[346]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 571: '*SA-SUBST-LIGHT*'
c_code local build_ref_571
	ret_reg &ref[571]
	call_c   Dyam_Create_Atom("*SA-SUBST-LIGHT*")
	move_ret ref[571]
	c_ret

c_code local build_seed_75
	ret_reg &seed[75]
	call_c   build_ref_33()
	call_c   build_ref_364()
	call_c   Dyam_Seed_Start(&ref[33],&ref[364],I(0),fun3,1)
	call_c   build_ref_367()
	call_c   Dyam_Seed_Add_Comp(&ref[367],&ref[364],0)
	call_c   Dyam_Seed_End()
	move_ret seed[75]
	c_ret

;; TERM 367: '*PROLOG-FIRST*'(seed_install(seed{model=> '*SACITEM*'(_B), id=> _K, anchor=> _L, subs_comp=> _M, code=> _N, go=> _O, tabule=> _P, schedule=> prolog, subsume=> _Q, model_ref=> _R, subs_comp_ref=> _S, c_type=> _T, c_type_ref=> _U, out_env=> _V, appinfo=> _W, tail_flag=> _X}, 1, _H)) :> '$$HOLE$$'
c_code local build_ref_367
	ret_reg &ref[367]
	call_c   build_ref_366()
	call_c   Dyam_Create_Binary(I(9),&ref[366],I(7))
	move_ret ref[367]
	c_ret

;; TERM 366: '*PROLOG-FIRST*'(seed_install(seed{model=> '*SACITEM*'(_B), id=> _K, anchor=> _L, subs_comp=> _M, code=> _N, go=> _O, tabule=> _P, schedule=> prolog, subsume=> _Q, model_ref=> _R, subs_comp_ref=> _S, c_type=> _T, c_type_ref=> _U, out_env=> _V, appinfo=> _W, tail_flag=> _X}, 1, _H))
c_code local build_ref_366
	ret_reg &ref[366]
	call_c   build_ref_28()
	call_c   build_ref_365()
	call_c   Dyam_Create_Unary(&ref[28],&ref[365])
	move_ret ref[366]
	c_ret

;; TERM 365: seed_install(seed{model=> '*SACITEM*'(_B), id=> _K, anchor=> _L, subs_comp=> _M, code=> _N, go=> _O, tabule=> _P, schedule=> prolog, subsume=> _Q, model_ref=> _R, subs_comp_ref=> _S, c_type=> _T, c_type_ref=> _U, out_env=> _V, appinfo=> _W, tail_flag=> _X}, 1, _H)
c_code local build_ref_365
	ret_reg &ref[365]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_509()
	call_c   build_ref_331()
	call_c   build_ref_572()
	call_c   Dyam_Term_Start(&ref[509],16)
	call_c   Dyam_Term_Arg(&ref[331])
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(&ref[572])
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_Arg(V(21))
	call_c   Dyam_Term_Arg(V(22))
	call_c   Dyam_Term_Arg(V(23))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_541()
	call_c   Dyam_Term_Start(&ref[541],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(N(1))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[365]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 572: prolog
c_code local build_ref_572
	ret_reg &ref[572]
	call_c   Dyam_Create_Atom("prolog")
	move_ret ref[572]
	c_ret

;; TERM 364: '*PROLOG-ITEM*'{top=> seed_install(seed{model=> '*SACITEM*'(_B), id=> _K, anchor=> _L, subs_comp=> _M, code=> _N, go=> _O, tabule=> _P, schedule=> prolog, subsume=> _Q, model_ref=> _R, subs_comp_ref=> _S, c_type=> _T, c_type_ref=> _U, out_env=> _V, appinfo=> _W, tail_flag=> _X}, 1, _H), cont=> '$CLOSURE'('$fun'(90, 0, 1139106548), '$TUPPLE'(35163198981536))}
c_code local build_ref_364
	ret_reg &ref[364]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,536346624)
	move_ret R(0)
	call_c   Dyam_Closure_Aux(fun90,R(0))
	move_ret R(0)
	call_c   build_ref_473()
	call_c   build_ref_365()
	call_c   Dyam_Create_Binary(&ref[473],&ref[365],R(0))
	move_ret ref[364]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 363: '$CLOSURE'('$fun'(89, 0, 1139096960), '$TUPPLE'(35163198981464))
c_code local build_ref_363
	ret_reg &ref[363]
	call_c   build_ref_362()
	call_c   Dyam_Closure_Aux(fun89,&ref[362])
	move_ret ref[363]
	c_ret

c_code local build_seed_74
	ret_reg &seed[74]
	call_c   build_ref_33()
	call_c   build_ref_357()
	call_c   Dyam_Seed_Start(&ref[33],&ref[357],I(0),fun3,1)
	call_c   build_ref_360()
	call_c   Dyam_Seed_Add_Comp(&ref[360],&ref[357],0)
	call_c   Dyam_Seed_End()
	move_ret seed[74]
	c_ret

;; TERM 360: '*PROLOG-FIRST*'(seed_install(seed{model=> ('*RITEM*'(_B, _C) :> _D(_F)(_G)), id=> _Z, anchor=> _A1, subs_comp=> _B1, code=> _C1, go=> _D1, tabule=> light, schedule=> prolog, subsume=> _Y, model_ref=> _E1, subs_comp_ref=> _F1, c_type=> _G1, c_type_ref=> _H1, out_env=> [_J], appinfo=> _I1, tail_flag=> flat}, [2,_E], _I)) :> '$$HOLE$$'
c_code local build_ref_360
	ret_reg &ref[360]
	call_c   build_ref_359()
	call_c   Dyam_Create_Binary(I(9),&ref[359],I(7))
	move_ret ref[360]
	c_ret

;; TERM 359: '*PROLOG-FIRST*'(seed_install(seed{model=> ('*RITEM*'(_B, _C) :> _D(_F)(_G)), id=> _Z, anchor=> _A1, subs_comp=> _B1, code=> _C1, go=> _D1, tabule=> light, schedule=> prolog, subsume=> _Y, model_ref=> _E1, subs_comp_ref=> _F1, c_type=> _G1, c_type_ref=> _H1, out_env=> [_J], appinfo=> _I1, tail_flag=> flat}, [2,_E], _I))
c_code local build_ref_359
	ret_reg &ref[359]
	call_c   build_ref_28()
	call_c   build_ref_358()
	call_c   Dyam_Create_Unary(&ref[28],&ref[358])
	move_ret ref[359]
	c_ret

;; TERM 358: seed_install(seed{model=> ('*RITEM*'(_B, _C) :> _D(_F)(_G)), id=> _Z, anchor=> _A1, subs_comp=> _B1, code=> _C1, go=> _D1, tabule=> light, schedule=> prolog, subsume=> _Y, model_ref=> _E1, subs_comp_ref=> _F1, c_type=> _G1, c_type_ref=> _H1, out_env=> [_J], appinfo=> _I1, tail_flag=> flat}, [2,_E], _I)
c_code local build_ref_358
	ret_reg &ref[358]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_0()
	call_c   Dyam_Create_Binary(&ref[0],V(1),V(2))
	move_ret R(0)
	call_c   Dyam_Create_Binary(I(3),V(3),V(5))
	move_ret R(1)
	call_c   Dyam_Create_Binary(I(3),R(1),V(6))
	move_ret R(1)
	call_c   Dyam_Create_Binary(I(9),R(0),R(1))
	move_ret R(1)
	call_c   Dyam_Create_List(V(9),I(0))
	move_ret R(0)
	call_c   build_ref_509()
	call_c   build_ref_573()
	call_c   build_ref_572()
	call_c   build_ref_531()
	call_c   Dyam_Term_Start(&ref[509],16)
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(25))
	call_c   Dyam_Term_Arg(V(26))
	call_c   Dyam_Term_Arg(V(27))
	call_c   Dyam_Term_Arg(V(28))
	call_c   Dyam_Term_Arg(V(29))
	call_c   Dyam_Term_Arg(&ref[573])
	call_c   Dyam_Term_Arg(&ref[572])
	call_c   Dyam_Term_Arg(V(24))
	call_c   Dyam_Term_Arg(V(30))
	call_c   Dyam_Term_Arg(V(31))
	call_c   Dyam_Term_Arg(V(32))
	call_c   Dyam_Term_Arg(V(33))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(34))
	call_c   Dyam_Term_Arg(&ref[531])
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   Dyam_Create_List(V(4),I(0))
	move_ret R(1)
	call_c   Dyam_Create_List(N(2),R(1))
	move_ret R(1)
	call_c   build_ref_541()
	call_c   Dyam_Term_Start(&ref[541],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_End()
	move_ret ref[358]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 573: light
c_code local build_ref_573
	ret_reg &ref[573]
	call_c   Dyam_Create_Atom("light")
	move_ret ref[573]
	c_ret

;; TERM 357: '*PROLOG-ITEM*'{top=> seed_install(seed{model=> ('*RITEM*'(_B, _C) :> _D(_F)(_G)), id=> _Z, anchor=> _A1, subs_comp=> _B1, code=> _C1, go=> _D1, tabule=> light, schedule=> prolog, subsume=> _Y, model_ref=> _E1, subs_comp_ref=> _F1, c_type=> _G1, c_type_ref=> _H1, out_env=> [_J], appinfo=> _I1, tail_flag=> flat}, [2,_E], _I), cont=> '$CLOSURE'('$fun'(88, 0, 1139090900), '$TUPPLE'(35163198981424))}
c_code local build_ref_357
	ret_reg &ref[357]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,271581184)
	move_ret R(0)
	call_c   Dyam_Closure_Aux(fun88,R(0))
	move_ret R(0)
	call_c   build_ref_473()
	call_c   build_ref_358()
	call_c   Dyam_Create_Binary(&ref[473],&ref[358],R(0))
	move_ret ref[357]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 355: _I :> deallocate_layer :> deallocate :> succeed
c_code local build_ref_355
	ret_reg &ref[355]
	call_c   build_ref_354()
	call_c   Dyam_Create_Binary(I(9),V(8),&ref[354])
	move_ret ref[355]
	c_ret

;; TERM 356: _H :> fail
c_code local build_ref_356
	ret_reg &ref[356]
	call_c   build_ref_79()
	call_c   Dyam_Create_Binary(I(9),V(7),&ref[79])
	move_ret ref[356]
	c_ret

long local pool_fun88[3]=[2,build_ref_355,build_ref_356]

pl_code local fun88
	call_c   Dyam_Pool(pool_fun88)
	call_c   Dyam_Allocate(0)
	move     &ref[355], R(0)
	move     S(5), R(1)
	move     &ref[356], R(2)
	move     S(5), R(3)
	move     V(35), R(4)
	move     S(5), R(5)
	pl_call  pred_choice_code_3()
	call_c   Dyam_Deallocate()
	pl_jump  Follow_Cont(V(0))

long local pool_fun89[2]=[1,build_seed_74]

pl_code local fun89
	call_c   Dyam_Pool(pool_fun89)
	pl_jump  fun10(&seed[74],12)

;; TERM 362: '$TUPPLE'(35163198981464)
c_code local build_ref_362
	ret_reg &ref[362]
	call_c   Dyam_Create_Simple_Tupple(0,536346640)
	move_ret ref[362]
	c_ret

long local pool_fun90[2]=[1,build_ref_363]

pl_code local fun90
	call_c   Dyam_Pool(pool_fun90)
	call_c   Dyam_Allocate(0)
	move     &ref[363], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	move     V(24), R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  fun34()

long local pool_fun91[3]=[2,build_ref_349,build_seed_75]

pl_code local fun91
	call_c   Dyam_Pool(pool_fun91)
	call_c   Dyam_Unify_Item(&ref[349])
	fail_ret
	pl_jump  fun10(&seed[75],12)

;; TERM 348: '*PROLOG-FIRST*'(unfold('*SA-SUBST-LIGHT*'(_B, _C, _D, _E), _F, _G, (_H :> _I), _J)) :> []
c_code local build_ref_348
	ret_reg &ref[348]
	call_c   build_ref_347()
	call_c   Dyam_Create_Binary(I(9),&ref[347],I(0))
	move_ret ref[348]
	c_ret

;; TERM 347: '*PROLOG-FIRST*'(unfold('*SA-SUBST-LIGHT*'(_B, _C, _D, _E), _F, _G, (_H :> _I), _J))
c_code local build_ref_347
	ret_reg &ref[347]
	call_c   build_ref_28()
	call_c   build_ref_346()
	call_c   Dyam_Create_Unary(&ref[28],&ref[346])
	move_ret ref[347]
	c_ret

c_code local build_seed_15
	ret_reg &seed[15]
	call_c   build_ref_27()
	call_c   build_ref_370()
	call_c   Dyam_Seed_Start(&ref[27],&ref[370],I(0),fun0,1)
	call_c   build_ref_371()
	call_c   Dyam_Seed_Add_Comp(&ref[371],fun97,0)
	call_c   Dyam_Seed_End()
	move_ret seed[15]
	c_ret

;; TERM 371: '*PROLOG-ITEM*'{top=> unfold('*SA-FOOT-WRAPPER-CALL*'(_B, _C, _D), _E, _F, _G, _H), cont=> _A}
c_code local build_ref_371
	ret_reg &ref[371]
	call_c   build_ref_473()
	call_c   build_ref_368()
	call_c   Dyam_Create_Binary(&ref[473],&ref[368],V(0))
	move_ret ref[371]
	c_ret

;; TERM 368: unfold('*SA-FOOT-WRAPPER-CALL*'(_B, _C, _D), _E, _F, _G, _H)
c_code local build_ref_368
	ret_reg &ref[368]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_574()
	call_c   Dyam_Term_Start(&ref[574],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_474()
	call_c   Dyam_Term_Start(&ref[474],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[368]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 574: '*SA-FOOT-WRAPPER-CALL*'
c_code local build_ref_574
	ret_reg &ref[574]
	call_c   Dyam_Create_Atom("*SA-FOOT-WRAPPER-CALL*")
	move_ret ref[574]
	c_ret

;; TERM 372: sa_env{mspop=> _I, aspop=> _J, holes=> _K, escape=> _L}
c_code local build_ref_372
	ret_reg &ref[372]
	call_c   build_ref_477()
	call_c   Dyam_Term_Start(&ref[477],4)
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_End()
	move_ret ref[372]
	c_ret

;; TERM 393: '$CLOSURE'('$fun'(96, 0, 1139179688), '$TUPPLE'(35163198725020))
c_code local build_ref_393
	ret_reg &ref[393]
	call_c   build_ref_392()
	call_c   Dyam_Closure_Aux(fun96,&ref[392])
	move_ret ref[393]
	c_ret

c_code local build_seed_79
	ret_reg &seed[79]
	call_c   build_ref_33()
	call_c   build_ref_387()
	call_c   Dyam_Seed_Start(&ref[33],&ref[387],I(0),fun3,1)
	call_c   build_ref_390()
	call_c   Dyam_Seed_Add_Comp(&ref[390],&ref[387],0)
	call_c   Dyam_Seed_End()
	move_ret seed[79]
	c_ret

;; TERM 390: '*PROLOG-FIRST*'(build_closure(_D, sa_env{mspop=> _I, aspop=> _J, holes=> _K, escape=> _N}, _O, _P, _H)) :> '$$HOLE$$'
c_code local build_ref_390
	ret_reg &ref[390]
	call_c   build_ref_389()
	call_c   Dyam_Create_Binary(I(9),&ref[389],I(7))
	move_ret ref[390]
	c_ret

;; TERM 389: '*PROLOG-FIRST*'(build_closure(_D, sa_env{mspop=> _I, aspop=> _J, holes=> _K, escape=> _N}, _O, _P, _H))
c_code local build_ref_389
	ret_reg &ref[389]
	call_c   build_ref_28()
	call_c   build_ref_388()
	call_c   Dyam_Create_Unary(&ref[28],&ref[388])
	move_ret ref[389]
	c_ret

;; TERM 388: build_closure(_D, sa_env{mspop=> _I, aspop=> _J, holes=> _K, escape=> _N}, _O, _P, _H)
c_code local build_ref_388
	ret_reg &ref[388]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_477()
	call_c   Dyam_Term_Start(&ref[477],4)
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_504()
	call_c   Dyam_Term_Start(&ref[504],5)
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[388]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 387: '*PROLOG-ITEM*'{top=> build_closure(_D, sa_env{mspop=> _I, aspop=> _J, holes=> _K, escape=> _N}, _O, _P, _H), cont=> '$CLOSURE'('$fun'(95, 0, 1139170736), '$TUPPLE'(35163198724984))}
c_code local build_ref_387
	ret_reg &ref[387]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,365502464)
	move_ret R(0)
	call_c   Dyam_Closure_Aux(fun95,R(0))
	move_ret R(0)
	call_c   build_ref_473()
	call_c   build_ref_388()
	call_c   Dyam_Create_Binary(&ref[473],&ref[388],R(0))
	move_ret ref[387]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 381: [_J|_C]
c_code local build_ref_381
	ret_reg &ref[381]
	call_c   Dyam_Create_List(V(9),V(2))
	move_ret ref[381]
	c_ret

;; TERM 382: _Q + 1
c_code local build_ref_382
	ret_reg &ref[382]
	call_c   build_ref_575()
	call_c   Dyam_Create_Binary(&ref[575],V(16),N(1))
	move_ret ref[382]
	c_ret

;; TERM 575: +
c_code local build_ref_575
	ret_reg &ref[575]
	call_c   Dyam_Create_Atom("+")
	move_ret ref[575]
	c_ret

;; TERM 383: [_E,_P,_J|_C]
c_code local build_ref_383
	ret_reg &ref[383]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_381()
	call_c   Dyam_Create_List(V(15),&ref[381])
	move_ret R(0)
	call_c   Dyam_Create_List(V(4),R(0))
	move_ret ref[383]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_78
	ret_reg &seed[78]
	call_c   build_ref_53()
	call_c   build_ref_385()
	call_c   Dyam_Seed_Start(&ref[53],&ref[385],I(0),fun3,1)
	call_c   build_ref_386()
	call_c   Dyam_Seed_Add_Comp(&ref[386],&ref[385],0)
	call_c   Dyam_Seed_End()
	move_ret seed[78]
	c_ret

;; TERM 386: '*GUARD*'(std_prolog_load_args_alt([_P,_J|_C], 0, _G, (call(_M, []) :> reg_reset(_R) :> noop), _F, _S)) :> '$$HOLE$$'
c_code local build_ref_386
	ret_reg &ref[386]
	call_c   build_ref_385()
	call_c   Dyam_Create_Binary(I(9),&ref[385],I(7))
	move_ret ref[386]
	c_ret

;; TERM 385: '*GUARD*'(std_prolog_load_args_alt([_P,_J|_C], 0, _G, (call(_M, []) :> reg_reset(_R) :> noop), _F, _S))
c_code local build_ref_385
	ret_reg &ref[385]
	call_c   build_ref_53()
	call_c   build_ref_384()
	call_c   Dyam_Create_Unary(&ref[53],&ref[384])
	move_ret ref[385]
	c_ret

;; TERM 384: std_prolog_load_args_alt([_P,_J|_C], 0, _G, (call(_M, []) :> reg_reset(_R) :> noop), _F, _S)
c_code local build_ref_384
	ret_reg &ref[384]
	call_c   Dyam_Pseudo_Choice(3)
	call_c   build_ref_381()
	call_c   Dyam_Create_List(V(15),&ref[381])
	move_ret R(0)
	call_c   build_ref_486()
	call_c   Dyam_Create_Binary(&ref[486],V(12),I(0))
	move_ret R(1)
	call_c   build_ref_577()
	call_c   Dyam_Create_Unary(&ref[577],V(17))
	move_ret R(2)
	call_c   build_ref_578()
	call_c   Dyam_Create_Binary(I(9),R(2),&ref[578])
	move_ret R(2)
	call_c   Dyam_Create_Binary(I(9),R(1),R(2))
	move_ret R(2)
	call_c   build_ref_576()
	call_c   Dyam_Term_Start(&ref[576],6)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(N(0))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(R(2))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_End()
	move_ret ref[384]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 576: std_prolog_load_args_alt
c_code local build_ref_576
	ret_reg &ref[576]
	call_c   Dyam_Create_Atom("std_prolog_load_args_alt")
	move_ret ref[576]
	c_ret

;; TERM 578: noop
c_code local build_ref_578
	ret_reg &ref[578]
	call_c   Dyam_Create_Atom("noop")
	move_ret ref[578]
	c_ret

;; TERM 577: reg_reset
c_code local build_ref_577
	ret_reg &ref[577]
	call_c   Dyam_Create_Atom("reg_reset")
	move_ret ref[577]
	c_ret

long local pool_fun95[5]=[4,build_ref_381,build_ref_382,build_ref_383,build_seed_78]

pl_code local fun95
	call_c   Dyam_Pool(pool_fun95)
	call_c   DYAM_evpred_length(&ref[381],V(16))
	fail_ret
	call_c   DYAM_evpred_is(V(17),&ref[382])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[383], R(0)
	move     S(5), R(1)
	move     V(18), R(2)
	move     S(5), R(3)
	pl_call  pred_duplicate_vars_2()
	pl_call  fun10(&seed[78],1)
	call_c   Dyam_Deallocate()
	pl_jump  Follow_Cont(V(0))

long local pool_fun96[2]=[1,build_seed_79]

pl_code local fun96
	call_c   Dyam_Pool(pool_fun96)
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(2))
	call_c   Dyam_Reg_Load(2,V(5))
	move     V(14), R(4)
	move     S(5), R(5)
	pl_call  pred_extend_tupple_3()
	call_c   Dyam_Deallocate()
	pl_jump  fun10(&seed[79],12)

;; TERM 392: '$TUPPLE'(35163198725020)
c_code local build_ref_392
	ret_reg &ref[392]
	call_c   Dyam_Create_Simple_Tupple(0,402489344)
	move_ret ref[392]
	c_ret

long local pool_fun97[5]=[4,build_ref_371,build_ref_372,build_ref_393,build_ref_381]

pl_code local fun97
	call_c   Dyam_Pool(pool_fun97)
	call_c   Dyam_Unify_Item(&ref[371])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(4))
	move     &ref[372], R(2)
	move     S(5), R(3)
	pl_call  pred_sa_env_normalize_2()
	move     &ref[393], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Load(4,V(1))
	move     V(12), R(6)
	move     S(5), R(7)
	move     &ref[381], R(8)
	move     S(5), R(9)
	move     V(13), R(10)
	move     S(5), R(11)
	call_c   Dyam_Reg_Deallocate(6)
fun94:
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Reg_Bind(2,V(6))
	call_c   Dyam_Multi_Reg_Bind(4,2,4)
	pl_call  fun13(&seed[76],1)
	pl_call  fun93(&seed[77],2,V(6))
	call_c   Dyam_Deallocate()
	pl_ret


;; TERM 370: '*PROLOG-FIRST*'(unfold('*SA-FOOT-WRAPPER-CALL*'(_B, _C, _D), _E, _F, _G, _H)) :> []
c_code local build_ref_370
	ret_reg &ref[370]
	call_c   build_ref_369()
	call_c   Dyam_Create_Binary(I(9),&ref[369],I(0))
	move_ret ref[370]
	c_ret

;; TERM 369: '*PROLOG-FIRST*'(unfold('*SA-FOOT-WRAPPER-CALL*'(_B, _C, _D), _E, _F, _G, _H))
c_code local build_ref_369
	ret_reg &ref[369]
	call_c   build_ref_28()
	call_c   build_ref_368()
	call_c   Dyam_Create_Unary(&ref[28],&ref[368])
	move_ret ref[369]
	c_ret

c_code local build_seed_20
	ret_reg &seed[20]
	call_c   build_ref_27()
	call_c   build_ref_402()
	call_c   Dyam_Seed_Start(&ref[27],&ref[402],I(0),fun0,1)
	call_c   build_ref_403()
	call_c   Dyam_Seed_Add_Comp(&ref[403],fun104,0)
	call_c   Dyam_Seed_End()
	move_ret seed[20]
	c_ret

;; TERM 403: '*PROLOG-ITEM*'{top=> unfold('*SA-ADJ-LAST*'(_B, _C, _D, _E), sa_env{mspop=> _F, aspop=> _G, holes=> [_H,_I|_J], escape=> _K}, _L, (_M :> _N), _O), cont=> _A}
c_code local build_ref_403
	ret_reg &ref[403]
	call_c   build_ref_473()
	call_c   build_ref_400()
	call_c   Dyam_Create_Binary(&ref[473],&ref[400],V(0))
	move_ret ref[403]
	c_ret

;; TERM 400: unfold('*SA-ADJ-LAST*'(_B, _C, _D, _E), sa_env{mspop=> _F, aspop=> _G, holes=> [_H,_I|_J], escape=> _K}, _L, (_M :> _N), _O)
c_code local build_ref_400
	ret_reg &ref[400]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_579()
	call_c   Dyam_Term_Start(&ref[579],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   Dyam_Create_Binary(I(9),V(12),V(13))
	move_ret R(1)
	call_c   build_ref_474()
	call_c   build_ref_404()
	call_c   Dyam_Term_Start(&ref[474],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[404])
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_End()
	move_ret ref[400]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 404: sa_env{mspop=> _F, aspop=> _G, holes=> [_H,_I|_J], escape=> _K}
c_code local build_ref_404
	ret_reg &ref[404]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Tupple(7,8,V(9))
	move_ret R(0)
	call_c   build_ref_477()
	call_c   Dyam_Term_Start(&ref[477],4)
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_End()
	move_ret ref[404]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 579: '*SA-ADJ-LAST*'
c_code local build_ref_579
	ret_reg &ref[579]
	call_c   Dyam_Create_Atom("*SA-ADJ-LAST*")
	move_ret ref[579]
	c_ret

c_code local build_seed_81
	ret_reg &seed[81]
	call_c   build_ref_33()
	call_c   build_ref_409()
	call_c   Dyam_Seed_Start(&ref[33],&ref[409],I(0),fun3,1)
	call_c   build_ref_412()
	call_c   Dyam_Seed_Add_Comp(&ref[412],&ref[409],0)
	call_c   Dyam_Seed_End()
	move_ret seed[81]
	c_ret

;; TERM 412: '*PROLOG-FIRST*'(seed_install(seed{model=> '*RFI*'{mspop=> _H, mspop_adj=> _I, current=> _C}, id=> _P, anchor=> _Q, subs_comp=> _R, code=> _S, go=> _T, tabule=> _U, schedule=> _V, subsume=> _W, model_ref=> _X, subs_comp_ref=> _Y, c_type=> _Z, c_type_ref=> _A1, out_env=> _B1, appinfo=> _C1, tail_flag=> _D1}, 1, _M)) :> '$$HOLE$$'
c_code local build_ref_412
	ret_reg &ref[412]
	call_c   build_ref_411()
	call_c   Dyam_Create_Binary(I(9),&ref[411],I(7))
	move_ret ref[412]
	c_ret

;; TERM 411: '*PROLOG-FIRST*'(seed_install(seed{model=> '*RFI*'{mspop=> _H, mspop_adj=> _I, current=> _C}, id=> _P, anchor=> _Q, subs_comp=> _R, code=> _S, go=> _T, tabule=> _U, schedule=> _V, subsume=> _W, model_ref=> _X, subs_comp_ref=> _Y, c_type=> _Z, c_type_ref=> _A1, out_env=> _B1, appinfo=> _C1, tail_flag=> _D1}, 1, _M))
c_code local build_ref_411
	ret_reg &ref[411]
	call_c   build_ref_28()
	call_c   build_ref_410()
	call_c   Dyam_Create_Unary(&ref[28],&ref[410])
	move_ret ref[411]
	c_ret

;; TERM 410: seed_install(seed{model=> '*RFI*'{mspop=> _H, mspop_adj=> _I, current=> _C}, id=> _P, anchor=> _Q, subs_comp=> _R, code=> _S, go=> _T, tabule=> _U, schedule=> _V, subsume=> _W, model_ref=> _X, subs_comp_ref=> _Y, c_type=> _Z, c_type_ref=> _A1, out_env=> _B1, appinfo=> _C1, tail_flag=> _D1}, 1, _M)
c_code local build_ref_410
	ret_reg &ref[410]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_537()
	call_c   Dyam_Term_Start(&ref[537],3)
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_509()
	call_c   Dyam_Term_Start(&ref[509],16)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_Arg(V(21))
	call_c   Dyam_Term_Arg(V(22))
	call_c   Dyam_Term_Arg(V(23))
	call_c   Dyam_Term_Arg(V(24))
	call_c   Dyam_Term_Arg(V(25))
	call_c   Dyam_Term_Arg(V(26))
	call_c   Dyam_Term_Arg(V(27))
	call_c   Dyam_Term_Arg(V(28))
	call_c   Dyam_Term_Arg(V(29))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_541()
	call_c   Dyam_Term_Start(&ref[541],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(N(1))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_End()
	move_ret ref[410]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 409: '*PROLOG-ITEM*'{top=> seed_install(seed{model=> '*RFI*'{mspop=> _H, mspop_adj=> _I, current=> _C}, id=> _P, anchor=> _Q, subs_comp=> _R, code=> _S, go=> _T, tabule=> _U, schedule=> _V, subsume=> _W, model_ref=> _X, subs_comp_ref=> _Y, c_type=> _Z, c_type_ref=> _A1, out_env=> _B1, appinfo=> _C1, tail_flag=> _D1}, 1, _M), cont=> '$CLOSURE'('$fun'(103, 0, 1139243896), '$TUPPLE'(35163198463840))}
c_code local build_ref_409
	ret_reg &ref[409]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,536788992)
	move_ret R(0)
	call_c   Dyam_Closure_Aux(fun103,R(0))
	move_ret R(0)
	call_c   build_ref_473()
	call_c   build_ref_410()
	call_c   Dyam_Create_Binary(&ref[473],&ref[410],R(0))
	move_ret ref[409]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_80
	ret_reg &seed[80]
	call_c   build_ref_33()
	call_c   build_ref_405()
	call_c   Dyam_Seed_Start(&ref[33],&ref[405],I(0),fun3,1)
	call_c   build_ref_408()
	call_c   Dyam_Seed_Add_Comp(&ref[408],&ref[405],0)
	call_c   Dyam_Seed_End()
	move_ret seed[80]
	c_ret

;; TERM 408: '*PROLOG-FIRST*'(seed_install(seed{model=> ('*RAI*'{xstart=> _H, xend=> _C, aspop=> _I, current=> _B} :> _D(_E1)(_L)), id=> _H1, anchor=> _I1, subs_comp=> _J1, code=> _K1, go=> _L1, tabule=> _M1, schedule=> _N1, subsume=> _O1, model_ref=> _P1, subs_comp_ref=> _Q1, c_type=> _R1, c_type_ref=> _S1, out_env=> [_O], appinfo=> _T1, tail_flag=> _U1}, [2,_E], _N)) :> '$$HOLE$$'
c_code local build_ref_408
	ret_reg &ref[408]
	call_c   build_ref_407()
	call_c   Dyam_Create_Binary(I(9),&ref[407],I(7))
	move_ret ref[408]
	c_ret

;; TERM 407: '*PROLOG-FIRST*'(seed_install(seed{model=> ('*RAI*'{xstart=> _H, xend=> _C, aspop=> _I, current=> _B} :> _D(_E1)(_L)), id=> _H1, anchor=> _I1, subs_comp=> _J1, code=> _K1, go=> _L1, tabule=> _M1, schedule=> _N1, subsume=> _O1, model_ref=> _P1, subs_comp_ref=> _Q1, c_type=> _R1, c_type_ref=> _S1, out_env=> [_O], appinfo=> _T1, tail_flag=> _U1}, [2,_E], _N))
c_code local build_ref_407
	ret_reg &ref[407]
	call_c   build_ref_28()
	call_c   build_ref_406()
	call_c   Dyam_Create_Unary(&ref[28],&ref[406])
	move_ret ref[407]
	c_ret

;; TERM 406: seed_install(seed{model=> ('*RAI*'{xstart=> _H, xend=> _C, aspop=> _I, current=> _B} :> _D(_E1)(_L)), id=> _H1, anchor=> _I1, subs_comp=> _J1, code=> _K1, go=> _L1, tabule=> _M1, schedule=> _N1, subsume=> _O1, model_ref=> _P1, subs_comp_ref=> _Q1, c_type=> _R1, c_type_ref=> _S1, out_env=> [_O], appinfo=> _T1, tail_flag=> _U1}, [2,_E], _N)
c_code local build_ref_406
	ret_reg &ref[406]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_543()
	call_c   Dyam_Term_Start(&ref[543],4)
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   Dyam_Create_Binary(I(3),V(3),V(30))
	move_ret R(1)
	call_c   Dyam_Create_Binary(I(3),R(1),V(11))
	move_ret R(1)
	call_c   Dyam_Create_Binary(I(9),R(0),R(1))
	move_ret R(1)
	call_c   Dyam_Create_List(V(14),I(0))
	move_ret R(0)
	call_c   build_ref_509()
	call_c   Dyam_Term_Start(&ref[509],16)
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(33))
	call_c   Dyam_Term_Arg(V(34))
	call_c   Dyam_Term_Arg(V(35))
	call_c   Dyam_Term_Arg(V(36))
	call_c   Dyam_Term_Arg(V(37))
	call_c   Dyam_Term_Arg(V(38))
	call_c   Dyam_Term_Arg(V(39))
	call_c   Dyam_Term_Arg(V(40))
	call_c   Dyam_Term_Arg(V(41))
	call_c   Dyam_Term_Arg(V(42))
	call_c   Dyam_Term_Arg(V(43))
	call_c   Dyam_Term_Arg(V(44))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(45))
	call_c   Dyam_Term_Arg(V(46))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   Dyam_Create_List(V(4),I(0))
	move_ret R(1)
	call_c   Dyam_Create_List(N(2),R(1))
	move_ret R(1)
	call_c   build_ref_541()
	call_c   Dyam_Term_Start(&ref[541],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_End()
	move_ret ref[406]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 405: '*PROLOG-ITEM*'{top=> seed_install(seed{model=> ('*RAI*'{xstart=> _H, xend=> _C, aspop=> _I, current=> _B} :> _D(_E1)(_L)), id=> _H1, anchor=> _I1, subs_comp=> _J1, code=> _K1, go=> _L1, tabule=> _M1, schedule=> _N1, subsume=> _O1, model_ref=> _P1, subs_comp_ref=> _Q1, c_type=> _R1, c_type_ref=> _S1, out_env=> [_O], appinfo=> _T1, tail_flag=> _U1}, [2,_E], _N), cont=> _A}
c_code local build_ref_405
	ret_reg &ref[405]
	call_c   build_ref_473()
	call_c   build_ref_406()
	call_c   Dyam_Create_Binary(&ref[473],&ref[406],V(0))
	move_ret ref[405]
	c_ret

long local pool_fun103[3]=[2,build_ref_404,build_seed_80]

pl_code local fun103
	call_c   Dyam_Pool(pool_fun103)
	call_c   Dyam_Allocate(0)
	move     V(30), R(0)
	move     S(5), R(1)
	move     &ref[404], R(2)
	move     S(5), R(3)
	move     V(31), R(4)
	move     S(5), R(5)
	move     V(32), R(6)
	move     S(5), R(7)
	pl_call  pred_sa_env_extension_4()
	call_c   Dyam_Deallocate()
	pl_jump  fun10(&seed[80],12)

long local pool_fun104[3]=[2,build_ref_403,build_seed_81]

pl_code local fun104
	call_c   Dyam_Pool(pool_fun104)
	call_c   Dyam_Unify_Item(&ref[403])
	fail_ret
	pl_jump  fun10(&seed[81],12)

;; TERM 402: '*PROLOG-FIRST*'(unfold('*SA-ADJ-LAST*'(_B, _C, _D, _E), sa_env{mspop=> _F, aspop=> _G, holes=> [_H,_I|_J], escape=> _K}, _L, (_M :> _N), _O)) :> []
c_code local build_ref_402
	ret_reg &ref[402]
	call_c   build_ref_401()
	call_c   Dyam_Create_Binary(I(9),&ref[401],I(0))
	move_ret ref[402]
	c_ret

;; TERM 401: '*PROLOG-FIRST*'(unfold('*SA-ADJ-LAST*'(_B, _C, _D, _E), sa_env{mspop=> _F, aspop=> _G, holes=> [_H,_I|_J], escape=> _K}, _L, (_M :> _N), _O))
c_code local build_ref_401
	ret_reg &ref[401]
	call_c   build_ref_28()
	call_c   build_ref_400()
	call_c   Dyam_Create_Unary(&ref[28],&ref[400])
	move_ret ref[401]
	c_ret

c_code local build_seed_13
	ret_reg &seed[13]
	call_c   build_ref_27()
	call_c   build_ref_415()
	call_c   Dyam_Seed_Start(&ref[27],&ref[415],I(0),fun0,1)
	call_c   build_ref_416()
	call_c   Dyam_Seed_Add_Comp(&ref[416],fun109,0)
	call_c   Dyam_Seed_End()
	move_ret seed[13]
	c_ret

;; TERM 416: '*PROLOG-ITEM*'{top=> unfold('*SA-ADJ-WRAPPER-CALL*'(_B, _C, _D, _E, _F), _G, _H, _I, _J), cont=> _A}
c_code local build_ref_416
	ret_reg &ref[416]
	call_c   build_ref_473()
	call_c   build_ref_413()
	call_c   Dyam_Create_Binary(&ref[473],&ref[413],V(0))
	move_ret ref[416]
	c_ret

;; TERM 413: unfold('*SA-ADJ-WRAPPER-CALL*'(_B, _C, _D, _E, _F), _G, _H, _I, _J)
c_code local build_ref_413
	ret_reg &ref[413]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_580()
	call_c   Dyam_Term_Start(&ref[580],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_474()
	call_c   Dyam_Term_Start(&ref[474],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret ref[413]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 580: '*SA-ADJ-WRAPPER-CALL*'
c_code local build_ref_580
	ret_reg &ref[580]
	call_c   Dyam_Create_Atom("*SA-ADJ-WRAPPER-CALL*")
	move_ret ref[580]
	c_ret

;; TERM 417: sa_env{mspop=> _K, aspop=> _L, holes=> _M, escape=> _N}
c_code local build_ref_417
	ret_reg &ref[417]
	call_c   build_ref_477()
	call_c   Dyam_Term_Start(&ref[477],4)
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_End()
	move_ret ref[417]
	c_ret

;; TERM 438: '$CLOSURE'('$fun'(108, 0, 1139304496), '$TUPPLE'(35163198464080))
c_code local build_ref_438
	ret_reg &ref[438]
	call_c   build_ref_437()
	call_c   Dyam_Closure_Aux(fun108,&ref[437])
	move_ret ref[438]
	c_ret

c_code local build_seed_85
	ret_reg &seed[85]
	call_c   build_ref_33()
	call_c   build_ref_432()
	call_c   Dyam_Seed_Start(&ref[33],&ref[432],I(0),fun3,1)
	call_c   build_ref_435()
	call_c   Dyam_Seed_Add_Comp(&ref[435],&ref[432],0)
	call_c   Dyam_Seed_End()
	move_ret seed[85]
	c_ret

;; TERM 435: '*PROLOG-FIRST*'(build_closure(_E, sa_env{mspop=> _D(_F,_K), aspop=> _L, holes=> _M, escape=> _N}, _P, _Q, _J)) :> '$$HOLE$$'
c_code local build_ref_435
	ret_reg &ref[435]
	call_c   build_ref_434()
	call_c   Dyam_Create_Binary(I(9),&ref[434],I(7))
	move_ret ref[435]
	c_ret

;; TERM 434: '*PROLOG-FIRST*'(build_closure(_E, sa_env{mspop=> _D(_F,_K), aspop=> _L, holes=> _M, escape=> _N}, _P, _Q, _J))
c_code local build_ref_434
	ret_reg &ref[434]
	call_c   build_ref_28()
	call_c   build_ref_433()
	call_c   Dyam_Create_Unary(&ref[28],&ref[433])
	move_ret ref[434]
	c_ret

;; TERM 433: build_closure(_E, sa_env{mspop=> _D(_F,_K), aspop=> _L, holes=> _M, escape=> _N}, _P, _Q, _J)
c_code local build_ref_433
	ret_reg &ref[433]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Term_Start(I(3),3)
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_477()
	call_c   Dyam_Term_Start(&ref[477],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_504()
	call_c   Dyam_Term_Start(&ref[504],5)
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret ref[433]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 432: '*PROLOG-ITEM*'{top=> build_closure(_E, sa_env{mspop=> _D(_F,_K), aspop=> _L, holes=> _M, escape=> _N}, _P, _Q, _J), cont=> '$CLOSURE'('$fun'(107, 0, 1139298156), '$TUPPLE'(35163198464044))}
c_code local build_ref_432
	ret_reg &ref[432]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,384847872)
	move_ret R(0)
	call_c   Dyam_Closure_Aux(fun107,R(0))
	move_ret R(0)
	call_c   build_ref_473()
	call_c   build_ref_433()
	call_c   Dyam_Create_Binary(&ref[473],&ref[433],R(0))
	move_ret ref[432]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 426: '$CLOSURE'(_R, _S)
c_code local build_ref_426
	ret_reg &ref[426]
	call_c   build_ref_189()
	call_c   Dyam_Create_Binary(&ref[189],V(17),V(18))
	move_ret ref[426]
	c_ret

;; TERM 189: '$CLOSURE'
c_code local build_ref_189
	ret_reg &ref[189]
	call_c   Dyam_Create_Atom("$CLOSURE")
	move_ret ref[189]
	c_ret

;; TERM 427: _V + 3
c_code local build_ref_427
	ret_reg &ref[427]
	call_c   build_ref_575()
	call_c   Dyam_Create_Binary(&ref[575],V(21),N(3))
	move_ret ref[427]
	c_ret

;; TERM 428: [_G,_Q,_F,_D|_C]
c_code local build_ref_428
	ret_reg &ref[428]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(3),V(2))
	move_ret R(0)
	call_c   Dyam_Create_List(V(5),R(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(16),R(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(6),R(0))
	move_ret ref[428]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_84
	ret_reg &seed[84]
	call_c   build_ref_53()
	call_c   build_ref_430()
	call_c   Dyam_Seed_Start(&ref[53],&ref[430],I(0),fun3,1)
	call_c   build_ref_431()
	call_c   Dyam_Seed_Add_Comp(&ref[431],&ref[430],0)
	call_c   Dyam_Seed_End()
	move_ret seed[84]
	c_ret

;; TERM 431: '*GUARD*'(std_prolog_load_args_alt([_Q,_F,_D|_C], 0, _I, (call(_O, []) :> reg_reset(_W) :> noop), _H, _X)) :> '$$HOLE$$'
c_code local build_ref_431
	ret_reg &ref[431]
	call_c   build_ref_430()
	call_c   Dyam_Create_Binary(I(9),&ref[430],I(7))
	move_ret ref[431]
	c_ret

;; TERM 430: '*GUARD*'(std_prolog_load_args_alt([_Q,_F,_D|_C], 0, _I, (call(_O, []) :> reg_reset(_W) :> noop), _H, _X))
c_code local build_ref_430
	ret_reg &ref[430]
	call_c   build_ref_53()
	call_c   build_ref_429()
	call_c   Dyam_Create_Unary(&ref[53],&ref[429])
	move_ret ref[430]
	c_ret

;; TERM 429: std_prolog_load_args_alt([_Q,_F,_D|_C], 0, _I, (call(_O, []) :> reg_reset(_W) :> noop), _H, _X)
c_code local build_ref_429
	ret_reg &ref[429]
	call_c   Dyam_Pseudo_Choice(3)
	call_c   Dyam_Create_List(V(3),V(2))
	move_ret R(0)
	call_c   Dyam_Create_List(V(5),R(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(16),R(0))
	move_ret R(0)
	call_c   build_ref_486()
	call_c   Dyam_Create_Binary(&ref[486],V(14),I(0))
	move_ret R(1)
	call_c   build_ref_577()
	call_c   Dyam_Create_Unary(&ref[577],V(22))
	move_ret R(2)
	call_c   build_ref_578()
	call_c   Dyam_Create_Binary(I(9),R(2),&ref[578])
	move_ret R(2)
	call_c   Dyam_Create_Binary(I(9),R(1),R(2))
	move_ret R(2)
	call_c   build_ref_576()
	call_c   Dyam_Term_Start(&ref[576],6)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(N(0))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(R(2))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(23))
	call_c   Dyam_Term_End()
	move_ret ref[429]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun107[5]=[4,build_ref_426,build_ref_427,build_ref_428,build_seed_84]

pl_code local fun107
	call_c   Dyam_Pool(pool_fun107)
	call_c   Dyam_Unify(V(16),&ref[426])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(18))
	move     V(19), R(2)
	move     S(5), R(3)
	pl_call  pred_oset_tupple_2()
	call_c   Dyam_Reg_Load(0,V(7))
	move     V(20), R(2)
	move     S(5), R(3)
	pl_call  pred_oset_tupple_2()
	call_c   DYAM_evpred_length(V(2),V(21))
	fail_ret
	call_c   DYAM_evpred_is(V(22),&ref[427])
	fail_ret
	move     &ref[428], R(0)
	move     S(5), R(1)
	move     V(23), R(2)
	move     S(5), R(3)
	pl_call  pred_duplicate_vars_2()
	pl_call  fun10(&seed[84],1)
	call_c   Dyam_Deallocate()
	pl_jump  Follow_Cont(V(0))

long local pool_fun108[2]=[1,build_seed_85]

pl_code local fun108
	call_c   Dyam_Pool(pool_fun108)
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(2))
	call_c   Dyam_Reg_Load(2,V(7))
	move     V(15), R(4)
	move     S(5), R(5)
	pl_call  pred_extend_tupple_3()
	call_c   Dyam_Deallocate()
	pl_jump  fun10(&seed[85],12)

;; TERM 437: '$TUPPLE'(35163198464080)
c_code local build_ref_437
	ret_reg &ref[437]
	call_c   Dyam_Create_Simple_Tupple(0,402636800)
	move_ret ref[437]
	c_ret

long local pool_fun109[4]=[3,build_ref_416,build_ref_417,build_ref_438]

pl_code local fun109
	call_c   Dyam_Pool(pool_fun109)
	call_c   Dyam_Unify_Item(&ref[416])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(6))
	move     &ref[417], R(2)
	move     S(5), R(3)
	pl_call  pred_sa_env_normalize_2()
	move     &ref[438], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Load(4,V(1))
	move     V(14), R(6)
	move     S(5), R(7)
	call_c   Dyam_Reg_Deallocate(4)
fun106:
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Reg_Bind(2,V(4))
	call_c   Dyam_Multi_Reg_Bind(4,2,2)
	pl_call  fun13(&seed[82],1)
	pl_call  fun93(&seed[83],2,V(4))
	call_c   Dyam_Deallocate()
	pl_ret


;; TERM 415: '*PROLOG-FIRST*'(unfold('*SA-ADJ-WRAPPER-CALL*'(_B, _C, _D, _E, _F), _G, _H, _I, _J)) :> []
c_code local build_ref_415
	ret_reg &ref[415]
	call_c   build_ref_414()
	call_c   Dyam_Create_Binary(I(9),&ref[414],I(0))
	move_ret ref[415]
	c_ret

;; TERM 414: '*PROLOG-FIRST*'(unfold('*SA-ADJ-WRAPPER-CALL*'(_B, _C, _D, _E, _F), _G, _H, _I, _J))
c_code local build_ref_414
	ret_reg &ref[414]
	call_c   build_ref_28()
	call_c   build_ref_413()
	call_c   Dyam_Create_Unary(&ref[28],&ref[413])
	move_ret ref[414]
	c_ret

c_code local build_seed_24
	ret_reg &seed[24]
	call_c   build_ref_27()
	call_c   build_ref_441()
	call_c   Dyam_Seed_Start(&ref[27],&ref[441],I(0),fun0,1)
	call_c   build_ref_442()
	call_c   Dyam_Seed_Add_Comp(&ref[442],fun114,0)
	call_c   Dyam_Seed_End()
	move_ret seed[24]
	c_ret

;; TERM 442: '*PROLOG-ITEM*'{top=> unfold('*SA-CUTTER*'(_B, _C, _D, _E, '*SA-CUT-LAST*'(_B, _F, _G), _H), _I, _J, (_K :> _L), _M), cont=> _A}
c_code local build_ref_442
	ret_reg &ref[442]
	call_c   build_ref_473()
	call_c   build_ref_439()
	call_c   Dyam_Create_Binary(&ref[473],&ref[439],V(0))
	move_ret ref[442]
	c_ret

;; TERM 439: unfold('*SA-CUTTER*'(_B, _C, _D, _E, '*SA-CUT-LAST*'(_B, _F, _G), _H), _I, _J, (_K :> _L), _M)
c_code local build_ref_439
	ret_reg &ref[439]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_554()
	call_c   Dyam_Term_Start(&ref[554],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_581()
	call_c   Dyam_Term_Start(&ref[581],6)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   Dyam_Create_Binary(I(9),V(10),V(11))
	move_ret R(1)
	call_c   build_ref_474()
	call_c   Dyam_Term_Start(&ref[474],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_End()
	move_ret ref[439]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 581: '*SA-CUTTER*'
c_code local build_ref_581
	ret_reg &ref[581]
	call_c   Dyam_Create_Atom("*SA-CUTTER*")
	move_ret ref[581]
	c_ret

;; TERM 443: [_B,_C|_O]
c_code local build_ref_443
	ret_reg &ref[443]
	call_c   Dyam_Create_Tupple(1,2,V(14))
	move_ret ref[443]
	c_ret

;; TERM 444: [return,_F|_P]
c_code local build_ref_444
	ret_reg &ref[444]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(5),V(15))
	move_ret R(0)
	call_c   build_ref_470()
	call_c   Dyam_Create_List(&ref[470],R(0))
	move_ret ref[444]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 464: '$CLOSURE'('$fun'(113, 0, 1139361248), '$TUPPLE'(35163198464472))
c_code local build_ref_464
	ret_reg &ref[464]
	call_c   build_ref_463()
	call_c   Dyam_Closure_Aux(fun113,&ref[463])
	move_ret ref[464]
	c_ret

c_code local build_seed_88
	ret_reg &seed[88]
	call_c   build_ref_33()
	call_c   build_ref_458()
	call_c   Dyam_Seed_Start(&ref[33],&ref[458],I(0),fun3,1)
	call_c   build_ref_461()
	call_c   Dyam_Seed_Add_Comp(&ref[461],&ref[458],0)
	call_c   Dyam_Seed_End()
	move_ret seed[88]
	c_ret

;; TERM 461: '*PROLOG-FIRST*'(seed_install(seed{model=> ('*RITEM*'(_Q, _R) :> _H(_I)(_J)), id=> _U, anchor=> _V, subs_comp=> _W, code=> _X, go=> _Y, tabule=> _Z, schedule=> _A1, subsume=> _T, model_ref=> _B1, subs_comp_ref=> _C1, c_type=> _D1, c_type_ref=> _E1, out_env=> [_M], appinfo=> _F1, tail_flag=> flat}, 2, _L)) :> '$$HOLE$$'
c_code local build_ref_461
	ret_reg &ref[461]
	call_c   build_ref_460()
	call_c   Dyam_Create_Binary(I(9),&ref[460],I(7))
	move_ret ref[461]
	c_ret

;; TERM 460: '*PROLOG-FIRST*'(seed_install(seed{model=> ('*RITEM*'(_Q, _R) :> _H(_I)(_J)), id=> _U, anchor=> _V, subs_comp=> _W, code=> _X, go=> _Y, tabule=> _Z, schedule=> _A1, subsume=> _T, model_ref=> _B1, subs_comp_ref=> _C1, c_type=> _D1, c_type_ref=> _E1, out_env=> [_M], appinfo=> _F1, tail_flag=> flat}, 2, _L))
c_code local build_ref_460
	ret_reg &ref[460]
	call_c   build_ref_28()
	call_c   build_ref_459()
	call_c   Dyam_Create_Unary(&ref[28],&ref[459])
	move_ret ref[460]
	c_ret

;; TERM 459: seed_install(seed{model=> ('*RITEM*'(_Q, _R) :> _H(_I)(_J)), id=> _U, anchor=> _V, subs_comp=> _W, code=> _X, go=> _Y, tabule=> _Z, schedule=> _A1, subsume=> _T, model_ref=> _B1, subs_comp_ref=> _C1, c_type=> _D1, c_type_ref=> _E1, out_env=> [_M], appinfo=> _F1, tail_flag=> flat}, 2, _L)
c_code local build_ref_459
	ret_reg &ref[459]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   Dyam_Create_Binary(I(3),V(7),V(8))
	move_ret R(0)
	call_c   Dyam_Create_Binary(I(3),R(0),V(9))
	move_ret R(0)
	call_c   build_ref_448()
	call_c   Dyam_Create_Binary(I(9),&ref[448],R(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(12),I(0))
	move_ret R(1)
	call_c   build_ref_509()
	call_c   build_ref_531()
	call_c   Dyam_Term_Start(&ref[509],16)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_Arg(V(21))
	call_c   Dyam_Term_Arg(V(22))
	call_c   Dyam_Term_Arg(V(23))
	call_c   Dyam_Term_Arg(V(24))
	call_c   Dyam_Term_Arg(V(25))
	call_c   Dyam_Term_Arg(V(26))
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_Arg(V(27))
	call_c   Dyam_Term_Arg(V(28))
	call_c   Dyam_Term_Arg(V(29))
	call_c   Dyam_Term_Arg(V(30))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(31))
	call_c   Dyam_Term_Arg(&ref[531])
	call_c   Dyam_Term_End()
	move_ret R(1)
	call_c   build_ref_541()
	call_c   Dyam_Term_Start(&ref[541],3)
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(N(2))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_End()
	move_ret ref[459]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 448: '*RITEM*'(_Q, _R)
c_code local build_ref_448
	ret_reg &ref[448]
	call_c   build_ref_0()
	call_c   Dyam_Create_Binary(&ref[0],V(16),V(17))
	move_ret ref[448]
	c_ret

;; TERM 458: '*PROLOG-ITEM*'{top=> seed_install(seed{model=> ('*RITEM*'(_Q, _R) :> _H(_I)(_J)), id=> _U, anchor=> _V, subs_comp=> _W, code=> _X, go=> _Y, tabule=> _Z, schedule=> _A1, subsume=> _T, model_ref=> _B1, subs_comp_ref=> _C1, c_type=> _D1, c_type_ref=> _E1, out_env=> [_M], appinfo=> _F1, tail_flag=> flat}, 2, _L), cont=> '$CLOSURE'('$fun'(112, 0, 1139357236), '$TUPPLE'(35163198464432))}
c_code local build_ref_458
	ret_reg &ref[458]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,495196160)
	move_ret R(0)
	call_c   Dyam_Closure_Aux(fun112,R(0))
	move_ret R(0)
	call_c   build_ref_473()
	call_c   build_ref_459()
	call_c   Dyam_Create_Binary(&ref[473],&ref[459],R(0))
	move_ret ref[458]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_87
	ret_reg &seed[87]
	call_c   build_ref_33()
	call_c   build_ref_454()
	call_c   Dyam_Seed_Start(&ref[33],&ref[454],I(0),fun3,1)
	call_c   build_ref_457()
	call_c   Dyam_Seed_Add_Comp(&ref[457],&ref[454],0)
	call_c   Dyam_Seed_End()
	move_ret seed[87]
	c_ret

;; TERM 457: '*PROLOG-FIRST*'(seed_install(seed{model=> '*CITEM*'(_Q, _Q), id=> _G1, anchor=> _H1, subs_comp=> _I1, code=> _J1, go=> _K1, tabule=> _L1, schedule=> _M1, subsume=> _N1, model_ref=> _O1, subs_comp_ref=> _P1, c_type=> _Q1, c_type_ref=> _R1, out_env=> _S1, appinfo=> _T1, tail_flag=> _U1}, 1, _K)) :> '$$HOLE$$'
c_code local build_ref_457
	ret_reg &ref[457]
	call_c   build_ref_456()
	call_c   Dyam_Create_Binary(I(9),&ref[456],I(7))
	move_ret ref[457]
	c_ret

;; TERM 456: '*PROLOG-FIRST*'(seed_install(seed{model=> '*CITEM*'(_Q, _Q), id=> _G1, anchor=> _H1, subs_comp=> _I1, code=> _J1, go=> _K1, tabule=> _L1, schedule=> _M1, subsume=> _N1, model_ref=> _O1, subs_comp_ref=> _P1, c_type=> _Q1, c_type_ref=> _R1, out_env=> _S1, appinfo=> _T1, tail_flag=> _U1}, 1, _K))
c_code local build_ref_456
	ret_reg &ref[456]
	call_c   build_ref_28()
	call_c   build_ref_455()
	call_c   Dyam_Create_Unary(&ref[28],&ref[455])
	move_ret ref[456]
	c_ret

;; TERM 455: seed_install(seed{model=> '*CITEM*'(_Q, _Q), id=> _G1, anchor=> _H1, subs_comp=> _I1, code=> _J1, go=> _K1, tabule=> _L1, schedule=> _M1, subsume=> _N1, model_ref=> _O1, subs_comp_ref=> _P1, c_type=> _Q1, c_type_ref=> _R1, out_env=> _S1, appinfo=> _T1, tail_flag=> _U1}, 1, _K)
c_code local build_ref_455
	ret_reg &ref[455]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_18()
	call_c   Dyam_Create_Binary(&ref[18],V(16),V(16))
	move_ret R(0)
	call_c   build_ref_509()
	call_c   Dyam_Term_Start(&ref[509],16)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(32))
	call_c   Dyam_Term_Arg(V(33))
	call_c   Dyam_Term_Arg(V(34))
	call_c   Dyam_Term_Arg(V(35))
	call_c   Dyam_Term_Arg(V(36))
	call_c   Dyam_Term_Arg(V(37))
	call_c   Dyam_Term_Arg(V(38))
	call_c   Dyam_Term_Arg(V(39))
	call_c   Dyam_Term_Arg(V(40))
	call_c   Dyam_Term_Arg(V(41))
	call_c   Dyam_Term_Arg(V(42))
	call_c   Dyam_Term_Arg(V(43))
	call_c   Dyam_Term_Arg(V(44))
	call_c   Dyam_Term_Arg(V(45))
	call_c   Dyam_Term_Arg(V(46))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_541()
	call_c   Dyam_Term_Start(&ref[541],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(N(1))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_End()
	move_ret ref[455]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 454: '*PROLOG-ITEM*'{top=> seed_install(seed{model=> '*CITEM*'(_Q, _Q), id=> _G1, anchor=> _H1, subs_comp=> _I1, code=> _J1, go=> _K1, tabule=> _L1, schedule=> _M1, subsume=> _N1, model_ref=> _O1, subs_comp_ref=> _P1, c_type=> _Q1, c_type_ref=> _R1, out_env=> _S1, appinfo=> _T1, tail_flag=> _U1}, 1, _K), cont=> '$CLOSURE'('$fun'(111, 0, 1139344852), '$TUPPLE'(35163198464384))}
c_code local build_ref_454
	ret_reg &ref[454]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,494934016)
	move_ret R(0)
	call_c   Dyam_Closure_Aux(fun111,R(0))
	move_ret R(0)
	call_c   build_ref_473()
	call_c   build_ref_455()
	call_c   Dyam_Create_Binary(&ref[473],&ref[455],R(0))
	move_ret ref[454]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 446: '*FIRST*'(_Q) :> _E
c_code local build_ref_446
	ret_reg &ref[446]
	call_c   build_ref_445()
	call_c   Dyam_Create_Binary(I(9),&ref[445],V(4))
	move_ret ref[446]
	c_ret

;; TERM 445: '*FIRST*'(_Q)
c_code local build_ref_445
	ret_reg &ref[445]
	call_c   build_ref_15()
	call_c   Dyam_Create_Unary(&ref[15],V(16))
	move_ret ref[445]
	c_ret

c_code local build_seed_86
	ret_reg &seed[86]
	call_c   build_ref_33()
	call_c   build_ref_450()
	call_c   Dyam_Seed_Start(&ref[33],&ref[450],I(0),fun3,1)
	call_c   build_ref_453()
	call_c   Dyam_Seed_Add_Comp(&ref[453],&ref[450],0)
	call_c   Dyam_Seed_End()
	move_ret seed[86]
	c_ret

;; TERM 453: '*PROLOG-FIRST*'(unfold(_W1, [], _X1, _Y1, _Z1)) :> '$$HOLE$$'
c_code local build_ref_453
	ret_reg &ref[453]
	call_c   build_ref_452()
	call_c   Dyam_Create_Binary(I(9),&ref[452],I(7))
	move_ret ref[453]
	c_ret

;; TERM 452: '*PROLOG-FIRST*'(unfold(_W1, [], _X1, _Y1, _Z1))
c_code local build_ref_452
	ret_reg &ref[452]
	call_c   build_ref_28()
	call_c   build_ref_451()
	call_c   Dyam_Create_Unary(&ref[28],&ref[451])
	move_ret ref[452]
	c_ret

;; TERM 451: unfold(_W1, [], _X1, _Y1, _Z1)
c_code local build_ref_451
	ret_reg &ref[451]
	call_c   build_ref_474()
	call_c   Dyam_Term_Start(&ref[474],5)
	call_c   Dyam_Term_Arg(V(48))
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(V(49))
	call_c   Dyam_Term_Arg(V(50))
	call_c   Dyam_Term_Arg(V(51))
	call_c   Dyam_Term_End()
	move_ret ref[451]
	c_ret

;; TERM 450: '*PROLOG-ITEM*'{top=> unfold(_W1, [], _X1, _Y1, _Z1), cont=> '$CLOSURE'('$fun'(110, 0, 1139334660), '$TUPPLE'(35163198464336))}
c_code local build_ref_450
	ret_reg &ref[450]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Start_Tupple(0,478156800)
	call_c   Dyam_Almost_End_Tupple(29,128)
	move_ret R(0)
	call_c   Dyam_Closure_Aux(fun110,R(0))
	move_ret R(0)
	call_c   build_ref_473()
	call_c   build_ref_451()
	call_c   Dyam_Create_Binary(&ref[473],&ref[451],R(0))
	move_ret ref[450]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 447: load_init(_Y1)
c_code local build_ref_447
	ret_reg &ref[447]
	call_c   build_ref_582()
	call_c   Dyam_Create_Unary(&ref[582],V(50))
	move_ret ref[447]
	c_ret

;; TERM 582: load_init
c_code local build_ref_582
	ret_reg &ref[582]
	call_c   Dyam_Create_Atom("load_init")
	move_ret ref[582]
	c_ret

;; TERM 449: cutter(_C, _F, _B)
c_code local build_ref_449
	ret_reg &ref[449]
	call_c   build_ref_583()
	call_c   Dyam_Term_Start(&ref[583],3)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_End()
	move_ret ref[449]
	c_ret

;; TERM 583: cutter
c_code local build_ref_583
	ret_reg &ref[583]
	call_c   Dyam_Create_Atom("cutter")
	move_ret ref[583]
	c_ret

long local pool_fun110[4]=[3,build_ref_447,build_ref_448,build_ref_449]

pl_code local fun110
	call_c   Dyam_Pool(pool_fun110)
	call_c   DYAM_evpred_assert_1(&ref[447])
	call_c   Dyam_Allocate(0)
	move     &ref[448], R(0)
	move     S(5), R(1)
	move     &ref[449], R(2)
	move     S(5), R(3)
	pl_call  pred_add_viewer_2()
	call_c   Dyam_Deallocate()
	pl_jump  Follow_Cont(V(0))

long local pool_fun111[3]=[2,build_ref_446,build_seed_86]

pl_code local fun111
	call_c   Dyam_Pool(pool_fun111)
	call_c   Dyam_Unify(V(47),&ref[446])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(47))
	move     V(48), R(2)
	move     S(5), R(3)
	pl_call  pred_trans_unfold_2()
	move     V(49), R(0)
	move     S(5), R(1)
	pl_call  pred_null_tupple_1()
	call_c   Dyam_Deallocate()
	pl_jump  fun10(&seed[86],12)

long local pool_fun112[2]=[1,build_seed_87]

pl_code local fun112
	call_c   Dyam_Pool(pool_fun112)
	pl_jump  fun10(&seed[87],12)

long local pool_fun113[2]=[1,build_seed_88]

pl_code local fun113
	call_c   Dyam_Pool(pool_fun113)
	pl_jump  fun10(&seed[88],12)

;; TERM 463: '$TUPPLE'(35163198464472)
c_code local build_ref_463
	ret_reg &ref[463]
	call_c   Dyam_Create_Simple_Tupple(0,499063296)
	move_ret ref[463]
	c_ret

long local pool_fun114[5]=[4,build_ref_442,build_ref_443,build_ref_444,build_ref_464]

pl_code local fun114
	call_c   Dyam_Pool(pool_fun114)
	call_c   Dyam_Unify_Item(&ref[442])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(2))
	call_c   Dyam_Reg_Load(2,V(3))
	move     V(13), R(4)
	move     S(5), R(5)
	pl_call  pred_extend_tupple_3()
	call_c   Dyam_Reg_Load(0,V(3))
	move     V(14), R(2)
	move     S(5), R(3)
	pl_call  pred_oset_tupple_2()
	call_c   Dyam_Reg_Load(0,V(6))
	move     V(15), R(2)
	move     S(5), R(3)
	pl_call  pred_oset_tupple_2()
	call_c   DYAM_evpred_univ(V(16),&ref[443])
	fail_ret
	call_c   DYAM_evpred_univ(V(17),&ref[444])
	fail_ret
	call_c   Dyam_Unify(V(18),V(16))
	fail_ret
	move     &ref[464], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	move     V(19), R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  fun34()

;; TERM 441: '*PROLOG-FIRST*'(unfold('*SA-CUTTER*'(_B, _C, _D, _E, '*SA-CUT-LAST*'(_B, _F, _G), _H), _I, _J, (_K :> _L), _M)) :> []
c_code local build_ref_441
	ret_reg &ref[441]
	call_c   build_ref_440()
	call_c   Dyam_Create_Binary(I(9),&ref[440],I(0))
	move_ret ref[441]
	c_ret

;; TERM 440: '*PROLOG-FIRST*'(unfold('*SA-CUTTER*'(_B, _C, _D, _E, '*SA-CUT-LAST*'(_B, _F, _G), _H), _I, _J, (_K :> _L), _M))
c_code local build_ref_440
	ret_reg &ref[440]
	call_c   build_ref_28()
	call_c   build_ref_439()
	call_c   Dyam_Create_Unary(&ref[28],&ref[439])
	move_ret ref[440]
	c_ret

c_code local build_seed_83
	ret_reg &seed[83]
	call_c   build_ref_139()
	call_c   build_ref_425()
	call_c   Dyam_Seed_Start(&ref[139],&ref[425],I(0),fun8,1)
	call_c   build_ref_423()
	call_c   Dyam_Seed_Add_Comp(&ref[423],fun105,0)
	call_c   Dyam_Seed_End()
	move_ret seed[83]
	c_ret

;; TERM 423: '*RITEM*'('call_sa_adj_wrapper_install/2'(_C), return(_D))
c_code local build_ref_423
	ret_reg &ref[423]
	call_c   build_ref_0()
	call_c   build_ref_418()
	call_c   build_ref_422()
	call_c   Dyam_Create_Binary(&ref[0],&ref[418],&ref[422])
	move_ret ref[423]
	c_ret

;; TERM 422: return(_D)
c_code local build_ref_422
	ret_reg &ref[422]
	call_c   build_ref_470()
	call_c   Dyam_Create_Unary(&ref[470],V(3))
	move_ret ref[422]
	c_ret

;; TERM 418: 'call_sa_adj_wrapper_install/2'(_C)
c_code local build_ref_418
	ret_reg &ref[418]
	call_c   build_ref_503()
	call_c   Dyam_Create_Unary(&ref[503],V(2))
	move_ret ref[418]
	c_ret

pl_code local fun105
	call_c   build_ref_423()
	call_c   Dyam_Unify_Item(&ref[423])
	fail_ret
	pl_jump  Follow_Cont(V(1))

;; TERM 425: '*RITEM*'('call_sa_adj_wrapper_install/2'(_C), return(_D)) :> tag_dyn(2, '$TUPPLE'(35163198460760))
c_code local build_ref_425
	ret_reg &ref[425]
	call_c   build_ref_423()
	call_c   build_ref_424()
	call_c   Dyam_Create_Binary(I(9),&ref[423],&ref[424])
	move_ret ref[425]
	c_ret

;; TERM 424: tag_dyn(2, '$TUPPLE'(35163198460760))
c_code local build_ref_424
	ret_reg &ref[424]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,134217728)
	move_ret R(0)
	call_c   build_ref_584()
	call_c   Dyam_Create_Binary(&ref[584],N(2),R(0))
	move_ret ref[424]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 584: tag_dyn
c_code local build_ref_584
	ret_reg &ref[584]
	call_c   Dyam_Create_Atom("tag_dyn")
	move_ret ref[584]
	c_ret

;; TERM 139: '*CURNEXT*'
c_code local build_ref_139
	ret_reg &ref[139]
	call_c   Dyam_Create_Atom("*CURNEXT*")
	move_ret ref[139]
	c_ret

c_code local build_seed_82
	ret_reg &seed[82]
	call_c   build_ref_18()
	call_c   build_ref_419()
	call_c   Dyam_Seed_Start(&ref[18],&ref[419],&ref[419],fun3,1)
	call_c   build_ref_421()
	call_c   Dyam_Seed_Add_Comp(&ref[421],&ref[419],0)
	call_c   Dyam_Seed_End()
	move_ret seed[82]
	c_ret

;; TERM 421: '*FIRST*'('call_sa_adj_wrapper_install/2'(_C)) :> '$$HOLE$$'
c_code local build_ref_421
	ret_reg &ref[421]
	call_c   build_ref_420()
	call_c   Dyam_Create_Binary(I(9),&ref[420],I(7))
	move_ret ref[421]
	c_ret

;; TERM 420: '*FIRST*'('call_sa_adj_wrapper_install/2'(_C))
c_code local build_ref_420
	ret_reg &ref[420]
	call_c   build_ref_15()
	call_c   build_ref_418()
	call_c   Dyam_Create_Unary(&ref[15],&ref[418])
	move_ret ref[420]
	c_ret

;; TERM 419: '*CITEM*'('call_sa_adj_wrapper_install/2'(_C), 'call_sa_adj_wrapper_install/2'(_C))
c_code local build_ref_419
	ret_reg &ref[419]
	call_c   build_ref_18()
	call_c   build_ref_418()
	call_c   Dyam_Create_Binary(&ref[18],&ref[418],&ref[418])
	move_ret ref[419]
	c_ret

c_code local build_seed_77
	ret_reg &seed[77]
	call_c   build_ref_139()
	call_c   build_ref_380()
	call_c   Dyam_Seed_Start(&ref[139],&ref[380],I(0),fun8,1)
	call_c   build_ref_378()
	call_c   Dyam_Seed_Add_Comp(&ref[378],fun92,0)
	call_c   Dyam_Seed_End()
	move_ret seed[77]
	c_ret

;; TERM 378: '*RITEM*'('call_sa_foot_wrapper_install/4'(_C), return(_D, _E, _F))
c_code local build_ref_378
	ret_reg &ref[378]
	call_c   build_ref_0()
	call_c   build_ref_373()
	call_c   build_ref_377()
	call_c   Dyam_Create_Binary(&ref[0],&ref[373],&ref[377])
	move_ret ref[378]
	c_ret

;; TERM 377: return(_D, _E, _F)
c_code local build_ref_377
	ret_reg &ref[377]
	call_c   build_ref_470()
	call_c   Dyam_Term_Start(&ref[470],3)
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[377]
	c_ret

;; TERM 373: 'call_sa_foot_wrapper_install/4'(_C)
c_code local build_ref_373
	ret_reg &ref[373]
	call_c   build_ref_476()
	call_c   Dyam_Create_Unary(&ref[476],V(2))
	move_ret ref[373]
	c_ret

pl_code local fun92
	call_c   build_ref_378()
	call_c   Dyam_Unify_Item(&ref[378])
	fail_ret
	pl_jump  Follow_Cont(V(1))

;; TERM 380: '*RITEM*'('call_sa_foot_wrapper_install/4'(_C), return(_D, _E, _F)) :> tag_dyn(1, '$TUPPLE'(35163198460760))
c_code local build_ref_380
	ret_reg &ref[380]
	call_c   build_ref_378()
	call_c   build_ref_379()
	call_c   Dyam_Create_Binary(I(9),&ref[378],&ref[379])
	move_ret ref[380]
	c_ret

;; TERM 379: tag_dyn(1, '$TUPPLE'(35163198460760))
c_code local build_ref_379
	ret_reg &ref[379]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,134217728)
	move_ret R(0)
	call_c   build_ref_584()
	call_c   Dyam_Create_Binary(&ref[584],N(1),R(0))
	move_ret ref[379]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_76
	ret_reg &seed[76]
	call_c   build_ref_18()
	call_c   build_ref_374()
	call_c   Dyam_Seed_Start(&ref[18],&ref[374],&ref[374],fun3,1)
	call_c   build_ref_376()
	call_c   Dyam_Seed_Add_Comp(&ref[376],&ref[374],0)
	call_c   Dyam_Seed_End()
	move_ret seed[76]
	c_ret

;; TERM 376: '*FIRST*'('call_sa_foot_wrapper_install/4'(_C)) :> '$$HOLE$$'
c_code local build_ref_376
	ret_reg &ref[376]
	call_c   build_ref_375()
	call_c   Dyam_Create_Binary(I(9),&ref[375],I(7))
	move_ret ref[376]
	c_ret

;; TERM 375: '*FIRST*'('call_sa_foot_wrapper_install/4'(_C))
c_code local build_ref_375
	ret_reg &ref[375]
	call_c   build_ref_15()
	call_c   build_ref_373()
	call_c   Dyam_Create_Unary(&ref[15],&ref[373])
	move_ret ref[375]
	c_ret

;; TERM 374: '*CITEM*'('call_sa_foot_wrapper_install/4'(_C), 'call_sa_foot_wrapper_install/4'(_C))
c_code local build_ref_374
	ret_reg &ref[374]
	call_c   build_ref_18()
	call_c   build_ref_373()
	call_c   Dyam_Create_Binary(&ref[18],&ref[373],&ref[373])
	move_ret ref[374]
	c_ret

c_code local build_seed_51
	ret_reg &seed[51]
	call_c   build_ref_18()
	call_c   build_ref_137()
	call_c   Dyam_Seed_Start(&ref[18],&ref[137],&ref[137],fun3,1)
	call_c   build_ref_138()
	call_c   Dyam_Seed_Add_Comp(&ref[138],&ref[137],0)
	call_c   Dyam_Seed_End()
	move_ret seed[51]
	c_ret

;; TERM 138: '*FIRST*'('call_tag_subsumption/1') :> '$$HOLE$$'
c_code local build_ref_138
	ret_reg &ref[138]
	call_c   build_ref_16()
	call_c   Dyam_Create_Binary(I(9),&ref[16],I(7))
	move_ret ref[138]
	c_ret

;; TERM 137: '*CITEM*'('call_tag_subsumption/1', 'call_tag_subsumption/1')
c_code local build_ref_137
	ret_reg &ref[137]
	call_c   build_ref_18()
	call_c   build_ref_9()
	call_c   Dyam_Create_Binary(&ref[18],&ref[9],&ref[9])
	move_ret ref[137]
	c_ret

c_code local build_seed_52
	ret_reg &seed[52]
	call_c   build_ref_139()
	call_c   build_ref_142()
	call_c   Dyam_Seed_Start(&ref[139],&ref[142],I(0),fun8,1)
	call_c   build_ref_140()
	call_c   Dyam_Seed_Add_Comp(&ref[140],fun31,0)
	call_c   Dyam_Seed_End()
	move_ret seed[52]
	c_ret

;; TERM 140: '*RITEM*'('call_tag_subsumption/1', return(_C))
c_code local build_ref_140
	ret_reg &ref[140]
	call_c   build_ref_0()
	call_c   build_ref_9()
	call_c   build_ref_95()
	call_c   Dyam_Create_Binary(&ref[0],&ref[9],&ref[95])
	move_ret ref[140]
	c_ret

pl_code local fun31
	call_c   build_ref_140()
	call_c   Dyam_Unify_Item(&ref[140])
	fail_ret
	pl_jump  Follow_Cont(V(1))

;; TERM 142: '*RITEM*'('call_tag_subsumption/1', return(_C)) :> tag_dyn(0, '$TUPPLE'(35163198460760))
c_code local build_ref_142
	ret_reg &ref[142]
	call_c   build_ref_140()
	call_c   build_ref_141()
	call_c   Dyam_Create_Binary(I(9),&ref[140],&ref[141])
	move_ret ref[142]
	c_ret

;; TERM 141: tag_dyn(0, '$TUPPLE'(35163198460760))
c_code local build_ref_141
	ret_reg &ref[141]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,134217728)
	move_ret R(0)
	call_c   build_ref_584()
	call_c   Dyam_Create_Binary(&ref[584],N(0),R(0))
	move_ret ref[141]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 4: sa_adj_wrapper_install(_A, _B)
c_code local build_ref_4
	ret_reg &ref[4]
	call_c   build_ref_585()
	call_c   Dyam_Create_Binary(&ref[585],V(0),V(1))
	move_ret ref[4]
	c_ret

;; TERM 585: sa_adj_wrapper_install
c_code local build_ref_585
	ret_reg &ref[585]
	call_c   Dyam_Create_Atom("sa_adj_wrapper_install")
	move_ret ref[585]
	c_ret

;; TERM 3: '*RITEM*'('call_sa_adj_wrapper_install/2'(_A), return(_B))
c_code local build_ref_3
	ret_reg &ref[3]
	call_c   build_ref_0()
	call_c   build_ref_1()
	call_c   build_ref_2()
	call_c   Dyam_Create_Binary(&ref[0],&ref[1],&ref[2])
	move_ret ref[3]
	c_ret

;; TERM 1: 'call_sa_adj_wrapper_install/2'(_A)
c_code local build_ref_1
	ret_reg &ref[1]
	call_c   build_ref_503()
	call_c   Dyam_Create_Unary(&ref[503],V(0))
	move_ret ref[1]
	c_ret

;; TERM 8: sa_foot_wrapper_install(_A, _B, _C, _D)
c_code local build_ref_8
	ret_reg &ref[8]
	call_c   build_ref_586()
	call_c   Dyam_Term_Start(&ref[586],4)
	call_c   Dyam_Term_Arg(V(0))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[8]
	c_ret

;; TERM 586: sa_foot_wrapper_install
c_code local build_ref_586
	ret_reg &ref[586]
	call_c   Dyam_Create_Atom("sa_foot_wrapper_install")
	move_ret ref[586]
	c_ret

;; TERM 7: '*RITEM*'('call_sa_foot_wrapper_install/4'(_A), return(_B, _C, _D))
c_code local build_ref_7
	ret_reg &ref[7]
	call_c   build_ref_0()
	call_c   build_ref_5()
	call_c   build_ref_6()
	call_c   Dyam_Create_Binary(&ref[0],&ref[5],&ref[6])
	move_ret ref[7]
	c_ret

;; TERM 6: return(_B, _C, _D)
c_code local build_ref_6
	ret_reg &ref[6]
	call_c   build_ref_470()
	call_c   Dyam_Term_Start(&ref[470],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[6]
	c_ret

;; TERM 5: 'call_sa_foot_wrapper_install/4'(_A)
c_code local build_ref_5
	ret_reg &ref[5]
	call_c   build_ref_476()
	call_c   Dyam_Create_Unary(&ref[476],V(0))
	move_ret ref[5]
	c_ret

;; TERM 12: tag_subsumption(_A)
c_code local build_ref_12
	ret_reg &ref[12]
	call_c   build_ref_587()
	call_c   Dyam_Create_Unary(&ref[587],V(0))
	move_ret ref[12]
	c_ret

;; TERM 587: tag_subsumption
c_code local build_ref_587
	ret_reg &ref[587]
	call_c   Dyam_Create_Atom("tag_subsumption")
	move_ret ref[587]
	c_ret

;; TERM 11: '*RITEM*'('call_tag_subsumption/1', return(_A))
c_code local build_ref_11
	ret_reg &ref[11]
	call_c   build_ref_0()
	call_c   build_ref_9()
	call_c   build_ref_10()
	call_c   Dyam_Create_Binary(&ref[0],&ref[9],&ref[10])
	move_ret ref[11]
	c_ret

;; TERM 10: return(_A)
c_code local build_ref_10
	ret_reg &ref[10]
	call_c   build_ref_470()
	call_c   Dyam_Create_Unary(&ref[470],V(0))
	move_ret ref[10]
	c_ret

;; TERM 395: [_B : _C|_D]
c_code local build_ref_395
	ret_reg &ref[395]
	call_c   build_ref_394()
	call_c   Dyam_Create_List(&ref[394],V(3))
	move_ret ref[395]
	c_ret

;; TERM 397: [_H|_I]
c_code local build_ref_397
	ret_reg &ref[397]
	call_c   Dyam_Create_List(V(7),V(8))
	move_ret ref[397]
	c_ret

;; TERM 396: [_F|_G]
c_code local build_ref_396
	ret_reg &ref[396]
	call_c   Dyam_Create_List(V(5),V(6))
	move_ret ref[396]
	c_ret

;; TERM 399: [_K|_N]
c_code local build_ref_399
	ret_reg &ref[399]
	call_c   Dyam_Create_List(V(10),V(13))
	move_ret ref[399]
	c_ret

;; TERM 398: [_K|_L]
c_code local build_ref_398
	ret_reg &ref[398]
	call_c   Dyam_Create_List(V(10),V(11))
	move_ret ref[398]
	c_ret

;; TERM 226: sa_env{mspop=> _G, aspop=> _H, holes=> _I, escape=> _J}
c_code local build_ref_226
	ret_reg &ref[226]
	call_c   build_ref_477()
	call_c   Dyam_Term_Start(&ref[477],4)
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret ref[226]
	c_ret

;; TERM 224: null_escape(_F)
c_code local build_ref_224
	ret_reg &ref[224]
	call_c   build_ref_465()
	call_c   Dyam_Create_Unary(&ref[465],V(5))
	move_ret ref[224]
	c_ret

;; TERM 225: sa_env{mspop=> _C, aspop=> _D, holes=> _E, escape=> _F}
c_code local build_ref_225
	ret_reg &ref[225]
	call_c   build_ref_477()
	call_c   Dyam_Term_Start(&ref[477],4)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[225]
	c_ret

;; TERM 38: viewer(_D, _E)
c_code local build_ref_38
	ret_reg &ref[38]
	call_c   build_ref_548()
	call_c   Dyam_Create_Binary(&ref[548],V(3),V(4))
	move_ret ref[38]
	c_ret

;; TERM 24: sa_env{mspop=> _C, aspop=> _D, holes=> _G, escape=> _H}
c_code local build_ref_24
	ret_reg &ref[24]
	call_c   build_ref_477()
	call_c   Dyam_Term_Start(&ref[477],4)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[24]
	c_ret

;; TERM 25: sa_env{mspop=> _C, aspop=> _D, holes=> [_E,_F|_G], escape=> _H}
c_code local build_ref_25
	ret_reg &ref[25]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Tupple(4,5,V(6))
	move_ret R(0)
	call_c   build_ref_477()
	call_c   Dyam_Term_Start(&ref[477],4)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[25]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

pl_code local fun1
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Object(R(0))
	call_c   Dyam_Tabule()
	pl_ret

pl_code local fun18
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Object(R(0))
	call_c   Dyam_Tabule()
	pl_ret

pl_code local fun98
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(4),&ref[395])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun99
	call_c   Dyam_Remove_Choice()
	call_c   DYAM_evpred_univ(V(1),&ref[398])
	fail_ret
	call_c   DYAM_evpred_functor(V(1),V(10),V(12))
	fail_ret
	call_c   DYAM_evpred_functor(V(2),V(10),V(12))
	fail_ret
	call_c   DYAM_evpred_univ(V(2),&ref[399])
	fail_ret
	call_c   Dyam_Reg_Load(0,V(11))
	call_c   Dyam_Reg_Load(2,V(13))
	call_c   Dyam_Reg_Load(4,V(3))
	call_c   Dyam_Reg_Load(6,V(4))
	call_c   Dyam_Reg_Deallocate(4)
	pl_jump  pred_duplicate_term_4()

pl_code local fun100
	call_c   Dyam_Update_Choice(fun99)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[396])
	fail_ret
	call_c   Dyam_Unify(V(2),&ref[397])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(5))
	call_c   Dyam_Reg_Load(2,V(7))
	call_c   Dyam_Reg_Load(4,V(3))
	move     V(9), R(6)
	move     S(5), R(7)
	pl_call  pred_duplicate_term_4()
	call_c   Dyam_Reg_Load(0,V(6))
	call_c   Dyam_Reg_Load(2,V(8))
	call_c   Dyam_Reg_Load(4,V(9))
	call_c   Dyam_Reg_Load(6,V(4))
	call_c   Dyam_Reg_Deallocate(4)
	pl_jump  pred_duplicate_term_4()

pl_code local fun101
	call_c   Dyam_Update_Choice(fun100)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Reg_Load(0,V(1))
	pl_call  pred_check_var_1()
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun98)
	call_c   Dyam_Set_Cut()
	pl_call  Domain_2(&ref[394],V(3))
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(4),V(3))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun93
	call_c   Dyam_Backptr_Trace(R(1),R(2))
	call_c   Dyam_Object(R(0))
	call_c   Dyam_Tabule()
	call_c   Dyam_Now()
	pl_ret

pl_code local fun12
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Object(R(0))
	call_c   Dyam_Tabule()
	call_c   Dyam_Schedule()
	pl_ret

pl_code local fun59
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(1),&ref[225])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun60
	call_c   Dyam_Remove_Choice()
	pl_call  Object_1(&ref[224])
	call_c   Dyam_Unify(V(2),I(0))
	fail_ret
	call_c   Dyam_Unify(V(3),V(1))
	fail_ret
	call_c   Dyam_Unify(V(4),I(0))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun61
	call_c   Dyam_Update_Choice(fun60)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[226])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(1),&ref[225])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun29
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Object(R(0))
	call_c   Dyam_Tabule()
	pl_jump  Schedule_Prolog()

pl_code local fun30
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Choice(fun29)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Subsume(R(0))
	fail_ret
	call_c   Dyam_Cut()
	pl_ret

pl_code local fun32
	call_c   Dyam_Backptr_Trace(R(1),R(2))
	call_c   Dyam_Light_Object(R(0))
	pl_jump  Schedule_Prolog()

pl_code local fun33
	call_c   Dyam_Remove_Choice()
	pl_call  fun32(&seed[52],2,V(3))
	call_c   Dyam_Deallocate()
	pl_ret


;;------------------ INIT POOL ------------

c_code local build_init_pool
	call_c   build_seed_39()
	call_c   build_seed_38()
	call_c   build_seed_32()
	call_c   build_seed_16()
	call_c   build_seed_18()
	call_c   build_seed_5()
	call_c   build_seed_14()
	call_c   build_seed_6()
	call_c   build_seed_37()
	call_c   build_seed_3()
	call_c   build_seed_4()
	call_c   build_seed_34()
	call_c   build_seed_1()
	call_c   build_seed_2()
	call_c   build_seed_29()
	call_c   build_seed_35()
	call_c   build_seed_36()
	call_c   build_seed_8()
	call_c   build_seed_28()
	call_c   build_seed_33()
	call_c   build_seed_22()
	call_c   build_seed_0()
	call_c   build_seed_26()
	call_c   build_seed_23()
	call_c   build_seed_17()
	call_c   build_seed_7()
	call_c   build_seed_11()
	call_c   build_seed_25()
	call_c   build_seed_10()
	call_c   build_seed_21()
	call_c   build_seed_31()
	call_c   build_seed_12()
	call_c   build_seed_27()
	call_c   build_seed_9()
	call_c   build_seed_19()
	call_c   build_seed_30()
	call_c   build_seed_15()
	call_c   build_seed_20()
	call_c   build_seed_13()
	call_c   build_seed_24()
	call_c   build_seed_83()
	call_c   build_seed_82()
	call_c   build_seed_77()
	call_c   build_seed_76()
	call_c   build_seed_51()
	call_c   build_seed_52()
	call_c   build_ref_4()
	call_c   build_ref_3()
	call_c   build_ref_8()
	call_c   build_ref_7()
	call_c   build_ref_12()
	call_c   build_ref_11()
	call_c   build_ref_394()
	call_c   build_ref_395()
	call_c   build_ref_397()
	call_c   build_ref_396()
	call_c   build_ref_399()
	call_c   build_ref_398()
	call_c   build_ref_226()
	call_c   build_ref_224()
	call_c   build_ref_225()
	call_c   build_ref_38()
	call_c   build_ref_24()
	call_c   build_ref_25()
	c_ret

long local ref[588]
long local seed[89]

long local _initialization

c_code global initialization_dyalog_tag_5Fdyn
	call_c   Dyam_Check_And_Update_Initialization(_initialization)
	fail_c_ret
	call_c   initialization_dyalog_maker()
	call_c   initialization_dyalog_tag_5Fmaker()
	call_c   initialization_dyalog_format()
	call_c   build_init_pool()
	call_c   build_viewers()
	call_c   load()
	c_ret

