;; Compiler: DyALog 1.14.0
;; File "code_emit.pl"


;;------------------ LOADING ------------

c_code local load
	call_c   Dyam_Loading_Set()
	pl_call  fun36(&seed[16],0)
	pl_call  fun36(&seed[9],0)
	pl_call  fun36(&seed[3],0)
	pl_call  fun36(&seed[8],0)
	pl_call  fun36(&seed[18],0)
	pl_call  fun36(&seed[17],0)
	pl_call  fun36(&seed[5],0)
	pl_call  fun36(&seed[6],0)
	pl_call  fun36(&seed[4],0)
	pl_call  fun36(&seed[15],0)
	pl_call  fun36(&seed[7],0)
	pl_call  fun36(&seed[14],0)
	pl_call  fun36(&seed[10],0)
	pl_call  fun36(&seed[13],0)
	pl_call  fun36(&seed[12],0)
	pl_call  fun36(&seed[11],0)
	pl_call  fun36(&seed[2],0)
	pl_call  fun36(&seed[0],0)
	pl_call  fun36(&seed[1],0)
	pl_call  fun17(&seed[106],0)
	pl_call  fun17(&seed[105],0)
	pl_call  fun17(&seed[104],0)
	pl_call  fun17(&seed[103],0)
	pl_call  fun17(&seed[102],0)
	pl_call  fun17(&seed[101],0)
	pl_call  fun17(&seed[100],0)
	pl_call  fun17(&seed[99],0)
	pl_call  fun17(&seed[98],0)
	pl_call  fun17(&seed[97],0)
	pl_call  fun17(&seed[96],0)
	pl_call  fun17(&seed[95],0)
	pl_call  fun17(&seed[94],0)
	pl_call  fun17(&seed[93],0)
	pl_call  fun17(&seed[92],0)
	pl_call  fun17(&seed[91],0)
	pl_call  fun17(&seed[90],0)
	pl_call  fun17(&seed[89],0)
	pl_call  fun17(&seed[88],0)
	pl_call  fun17(&seed[87],0)
	pl_call  fun17(&seed[86],0)
	pl_call  fun17(&seed[85],0)
	pl_call  fun17(&seed[84],0)
	pl_call  fun17(&seed[83],0)
	pl_call  fun17(&seed[82],0)
	pl_call  fun17(&seed[81],0)
	pl_call  fun17(&seed[80],0)
	pl_call  fun17(&seed[79],0)
	pl_call  fun17(&seed[78],0)
	pl_call  fun17(&seed[77],0)
	pl_call  fun17(&seed[76],0)
	pl_call  fun17(&seed[75],0)
	pl_call  fun17(&seed[74],0)
	pl_call  fun17(&seed[73],0)
	pl_call  fun17(&seed[72],0)
	pl_call  fun17(&seed[71],0)
	pl_call  fun17(&seed[70],0)
	pl_call  fun17(&seed[69],0)
	pl_call  fun17(&seed[68],0)
	pl_call  fun17(&seed[67],0)
	pl_call  fun17(&seed[66],0)
	pl_call  fun17(&seed[65],0)
	pl_call  fun17(&seed[64],0)
	pl_call  fun17(&seed[63],0)
	pl_call  fun17(&seed[62],0)
	pl_call  fun17(&seed[61],0)
	pl_call  fun17(&seed[60],0)
	pl_call  fun17(&seed[59],0)
	pl_call  fun17(&seed[56],0)
	pl_call  fun17(&seed[55],0)
	pl_call  fun17(&seed[58],0)
	pl_call  fun17(&seed[57],0)
	pl_call  fun17(&seed[54],0)
	pl_call  fun17(&seed[53],0)
	pl_call  fun17(&seed[52],0)
	pl_call  fun17(&seed[51],0)
	pl_call  fun17(&seed[50],0)
	pl_call  fun17(&seed[49],0)
	pl_call  fun17(&seed[45],0)
	pl_call  fun17(&seed[44],0)
	pl_call  fun17(&seed[43],0)
	pl_call  fun17(&seed[42],0)
	pl_call  fun17(&seed[41],0)
	pl_call  fun17(&seed[40],0)
	pl_call  fun17(&seed[39],0)
	pl_call  fun17(&seed[38],0)
	pl_call  fun17(&seed[48],0)
	pl_call  fun17(&seed[47],0)
	pl_call  fun17(&seed[46],0)
	pl_call  fun17(&seed[37],0)
	pl_call  fun17(&seed[36],0)
	pl_call  fun17(&seed[35],0)
	pl_call  fun17(&seed[34],0)
	pl_call  fun17(&seed[33],0)
	pl_call  fun17(&seed[32],0)
	pl_call  fun17(&seed[31],0)
	pl_call  fun17(&seed[30],0)
	pl_call  fun17(&seed[29],0)
	pl_call  fun17(&seed[28],0)
	pl_call  fun17(&seed[27],0)
	pl_call  fun17(&seed[26],0)
	pl_call  fun17(&seed[25],0)
	pl_call  fun17(&seed[24],0)
	pl_call  fun17(&seed[23],0)
	pl_call  fun17(&seed[22],0)
	pl_call  fun17(&seed[21],0)
	pl_call  fun17(&seed[20],0)
	pl_call  fun17(&seed[19],0)
	call_c   Dyam_Loading_Reset()
	c_ret

pl_code global pred_emit_code_elem_1
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Choice(fun125)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(1),&ref[136])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Deallocate()
	pl_ret

pl_code global pred_emit_globals_2
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(2)
	call_c   Dyam_Choice(fun116)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[261])
	fail_ret
	call_c   Dyam_Choice(fun109)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[262])
	call_c   Dyam_Cut()
	pl_fail

pl_code global pred_code_refs_add_3
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(3)
	call_c   Dyam_Choice(fun106)
	call_c   Dyam_Set_Cut()
	pl_call  Domain_2(V(1),&ref[256])
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load_Ptr(2,V(2))
	fail_ret
	move     V(11), R(4)
	move     S(5), R(5)
	call_c   DyALog_Mutable_Read(R(2),&R(4))
	fail_ret
	call_c   Dyam_Choice(fun105)
	call_c   Dyam_Set_Cut()
	pl_call  Domain_2(V(1),V(11))
	call_c   Dyam_Cut()
fun104:
	call_c   Dyam_Choice(fun11)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[257])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load_Ptr(2,V(14))
	fail_ret
	call_c   Dyam_Reg_Load(4,&ref[258])
	call_c   DyALog_Mutable_Read(R(2),&R(4))
	fail_ret
	call_c   Dyam_Reg_Load(0,V(16))
	call_c   Dyam_Reg_Load(2,V(3))
	pl_call  pred_code_refs_add_ins_2()
	call_c   Dyam_Reg_Load(0,V(17))
	call_c   Dyam_Reg_Load(2,V(3))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_code_refs_add_ins_2()


pl_code global pred_code_optimize_allocate_4
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(4)
	call_c   Dyam_Choice(fun102)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[251])
	fail_ret
	call_c   Dyam_Cut()
	call_c   DYAM_evpred_is(V(4),V(3))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code global pred_emit_globals_in_init_pool_1
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Choice(fun91)
	call_c   Dyam_Set_Cut()
	pl_call  Domain_2(V(1),&ref[235])
	call_c   Dyam_Cut()
	move     &ref[236], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Deallocate(1)
	pl_jump  pred_record_without_doublon_1()

pl_code global pred_emit_function_1
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Unify(0,&ref[215])
	fail_ret
	call_c   Dyam_Reg_Load_Ptr(2,V(3))
	fail_ret
	call_c   Dyam_Reg_Load(4,&ref[213])
	call_c   DyALog_Mutable_Read(R(2),&R(4))
	fail_ret
	call_c   Dyam_Choice(fun88)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(2),N(0))
	fail_ret
	move     &ref[215], R(0)
	move     S(5), R(1)
	move     V(7), R(2)
	move     S(5), R(3)
	pl_call  pred_jump_value_2()
	call_c   DYAM_evpred_gt(V(7),N(0))
	fail_ret
	call_c   Dyam_Choice(fun87)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[227])
	call_c   Dyam_Cut()
	pl_fail

pl_code global pred_register_init_pool_fun_1
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Unify(0,&ref[215])
	fail_ret
	call_c   Dyam_Choice(fun74)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[214])
	call_c   Dyam_Cut()
	call_c   Dyam_Deallocate()
	pl_ret

pl_code global pred_code_refs_3
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(3)
	call_c   Dyam_Reg_Load_Ptr(2,V(4))
	fail_ret
	move     I(0), R(4)
	move     0, R(5)
	call_c   DyALog_Mutable_Write(R(2),&R(4),1)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(4))
	fail_ret
	call_c   Dyam_Reg_Load_Ptr(2,V(5))
	fail_ret
	move     I(0), R(4)
	move     0, R(5)
	call_c   DyALog_Mutable_Write(R(2),&R(4),1)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(5))
	fail_ret
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(4))
	call_c   Dyam_Reg_Load(4,V(5))
	pl_call  pred_code_refs_aux_3()
	call_c   Dyam_Reg_Load_Ptr(2,V(4))
	fail_ret
	call_c   Dyam_Reg_Load(4,V(2))
	call_c   DyALog_Mutable_Read(R(2),&R(4))
	fail_ret
	call_c   Dyam_Reg_Load_Ptr(2,V(5))
	fail_ret
	call_c   Dyam_Reg_Load(4,V(3))
	call_c   DyALog_Mutable_Read(R(2),&R(4))
	fail_ret
	call_c   Dyam_Reg_Deallocate(3)
	pl_ret

pl_code global pred_emit_inlined_function_1
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Unify(0,&ref[215])
	fail_ret
	call_c   Dyam_Reg_Load_Ptr(2,V(3))
	fail_ret
	call_c   Dyam_Reg_Load(4,&ref[213])
	call_c   DyALog_Mutable_Read(R(2),&R(4))
	fail_ret
	call_c   Dyam_Reg_Load_Ptr(2,V(3))
	fail_ret
	move     I(0), R(4)
	move     0, R(5)
	call_c   DyALog_Mutable_Write(R(2),&R(4),0)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(3))
	fail_ret
	call_c   Dyam_Choice(fun70)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[214])
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(5))
	pl_call  pred_emit_globals_in_init_pool_1()
fun69:
	call_c   Dyam_Reg_Load(0,V(4))
	pl_call  pred_emit_code_aux_1()
	call_c   DYAM_Nl_0()
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret


pl_code global pred_code_refs_add_ins_2
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(2)
	call_c   Dyam_Choice(fun11)
	pl_call  Domain_2(V(3),V(1))
	call_c   Dyam_Reg_Load_Ptr(2,V(2))
	fail_ret
	move     V(4), R(4)
	move     S(5), R(5)
	call_c   DyALog_Mutable_Read(R(2),&R(4))
	fail_ret
	call_c   Dyam_Choice(fun67)
	call_c   Dyam_Set_Cut()
	pl_call  Domain_2(V(3),V(4))
	call_c   Dyam_Cut()
	pl_fail

pl_code global pred_emit_pool_4
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(2)
	call_c   Dyam_Reg_Bind(6,V(4))
	call_c   Dyam_Choice(fun65)
	pl_call  Domain_2(V(5),V(1))
	call_c   Dyam_Choice(fun64)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Choice(fun63)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(5),&ref[189])
	fail_ret
	call_c   Dyam_Cut()
	pl_fail

pl_code global pred_code_refs_aux_3
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(3)
	call_c   Dyam_Choice(fun48)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[184])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun47)
	call_c   Dyam_Reg_Load(0,V(5))
	call_c   Dyam_Reg_Load(2,V(2))
	call_c   Dyam_Reg_Load(4,V(3))
	pl_call  pred_code_refs_aux_3()
	pl_fail

pl_code global pred_emit_init_code_1
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Reg_Load(0,V(1))
	move     V(2), R(2)
	move     S(5), R(3)
	move     V(3), R(4)
	move     S(5), R(5)
	pl_call  pred_code_optimize_3()
	call_c   Dyam_Reg_Load(0,V(2))
	move     V(4), R(2)
	move     S(5), R(3)
	move     V(5), R(4)
	move     S(5), R(5)
	pl_call  pred_code_refs_3()
	call_c   Dyam_Reg_Load(0,V(4))
	pl_call  pred_emit_globals_in_init_pool_1()
	call_c   Dyam_Reg_Load(0,V(2))
	call_c   Dyam_Reg_Deallocate(1)
	pl_jump  pred_emit_code_aux_1()

pl_code global pred_emit_code_aux_1
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Choice(fun39)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[174])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(2))
	pl_call  pred_emit_code_elem_1()
	call_c   Dyam_Reg_Load(0,V(3))
	call_c   Dyam_Reg_Deallocate(1)
	pl_jump  pred_emit_code_aux_1()

pl_code global pred_emit_move_3
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(3)
	call_c   DYAM_evpred_is(V(4),&ref[172])
	fail_ret
	call_c   Dyam_Reg_Load(0,V(2))
	move     &ref[111], R(2)
	move     S(5), R(3)
	pl_call  pred_ma_emit_move_2()
	call_c   Dyam_Reg_Load(0,V(3))
	move     &ref[173], R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_ma_emit_move_2()

pl_code global pred_code_optimize_3
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(3)
	call_c   Dyam_Choice(fun34)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_notvar(V(1))
	fail_ret
	call_c   Dyam_Cut()
fun33:
	call_c   Dyam_Choice(fun32)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[131])
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(4))
	call_c   Dyam_Reg_Deallocate(1)
	pl_jump  pred_jump_inc_1()


pl_code global pred_inlinable_1
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Unify(0,&ref[116])
	fail_ret
	call_c   Dyam_Choice(fun13)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Reg_Load_Ptr(2,V(2))
	fail_ret
	move     I(0), R(4)
	move     0, R(5)
	call_c   DyALog_Mutable_Read(R(2),&R(4))
	fail_ret
	call_c   Dyam_Cut()
	pl_fail

pl_code global pred_register_global_2
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(2)
	call_c   DYAM_evpred_assert_1(&ref[24])
	call_c   Dyam_Deallocate()
	pl_ret


;;------------------ VIEWERS ------------

c_code local build_viewers
	c_ret

c_code local build_seed_19
	ret_reg &seed[19]
	call_c   build_ref_0()
	call_c   build_ref_1()
	call_c   Dyam_Seed_Start(&ref[0],&ref[1],&ref[1],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[19]
	c_ret

pl_code local fun0
	pl_ret

;; TERM 1: inst(forget_current_item, 'Dyam_Forget_Current_Item', [])
c_code local build_ref_1
	ret_reg &ref[1]
	call_c   build_ref_277()
	call_c   build_ref_278()
	call_c   build_ref_279()
	call_c   Dyam_Term_Start(&ref[277],3)
	call_c   Dyam_Term_Arg(&ref[278])
	call_c   Dyam_Term_Arg(&ref[279])
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_End()
	move_ret ref[1]
	c_ret

;; TERM 279: 'Dyam_Forget_Current_Item'
c_code local build_ref_279
	ret_reg &ref[279]
	call_c   Dyam_Create_Atom("Dyam_Forget_Current_Item")
	move_ret ref[279]
	c_ret

;; TERM 278: forget_current_item
c_code local build_ref_278
	ret_reg &ref[278]
	call_c   Dyam_Create_Atom("forget_current_item")
	move_ret ref[278]
	c_ret

;; TERM 277: inst
c_code local build_ref_277
	ret_reg &ref[277]
	call_c   Dyam_Create_Atom("inst")
	move_ret ref[277]
	c_ret

;; TERM 0: '*DATABASE*'
c_code local build_ref_0
	ret_reg &ref[0]
	call_c   Dyam_Create_Atom("*DATABASE*")
	move_ret ref[0]
	c_ret

c_code local build_seed_20
	ret_reg &seed[20]
	call_c   build_ref_0()
	call_c   build_ref_2()
	call_c   Dyam_Seed_Start(&ref[0],&ref[2],&ref[2],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[20]
	c_ret

;; TERM 2: inst(allocate_layer, 'Dyam_Reg_Allocate_Layer', [])
c_code local build_ref_2
	ret_reg &ref[2]
	call_c   build_ref_277()
	call_c   build_ref_280()
	call_c   build_ref_281()
	call_c   Dyam_Term_Start(&ref[277],3)
	call_c   Dyam_Term_Arg(&ref[280])
	call_c   Dyam_Term_Arg(&ref[281])
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_End()
	move_ret ref[2]
	c_ret

;; TERM 281: 'Dyam_Reg_Allocate_Layer'
c_code local build_ref_281
	ret_reg &ref[281]
	call_c   Dyam_Create_Atom("Dyam_Reg_Allocate_Layer")
	move_ret ref[281]
	c_ret

;; TERM 280: allocate_layer
c_code local build_ref_280
	ret_reg &ref[280]
	call_c   Dyam_Create_Atom("allocate_layer")
	move_ret ref[280]
	c_ret

c_code local build_seed_21
	ret_reg &seed[21]
	call_c   build_ref_0()
	call_c   build_ref_3()
	call_c   Dyam_Seed_Start(&ref[0],&ref[3],&ref[3],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[21]
	c_ret

;; TERM 3: inst(deallocate_alt, 'Dyam_Deallocate_Alt', [])
c_code local build_ref_3
	ret_reg &ref[3]
	call_c   build_ref_277()
	call_c   build_ref_282()
	call_c   build_ref_283()
	call_c   Dyam_Term_Start(&ref[277],3)
	call_c   Dyam_Term_Arg(&ref[282])
	call_c   Dyam_Term_Arg(&ref[283])
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_End()
	move_ret ref[3]
	c_ret

;; TERM 283: 'Dyam_Deallocate_Alt'
c_code local build_ref_283
	ret_reg &ref[283]
	call_c   Dyam_Create_Atom("Dyam_Deallocate_Alt")
	move_ret ref[283]
	c_ret

;; TERM 282: deallocate_alt
c_code local build_ref_282
	ret_reg &ref[282]
	call_c   Dyam_Create_Atom("deallocate_alt")
	move_ret ref[282]
	c_ret

c_code local build_seed_22
	ret_reg &seed[22]
	call_c   build_ref_0()
	call_c   build_ref_4()
	call_c   Dyam_Seed_Start(&ref[0],&ref[4],&ref[4],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[22]
	c_ret

;; TERM 4: inst(deallocate, 'Dyam_Deallocate', [])
c_code local build_ref_4
	ret_reg &ref[4]
	call_c   build_ref_277()
	call_c   build_ref_142()
	call_c   build_ref_284()
	call_c   Dyam_Term_Start(&ref[277],3)
	call_c   Dyam_Term_Arg(&ref[142])
	call_c   Dyam_Term_Arg(&ref[284])
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_End()
	move_ret ref[4]
	c_ret

;; TERM 284: 'Dyam_Deallocate'
c_code local build_ref_284
	ret_reg &ref[284]
	call_c   Dyam_Create_Atom("Dyam_Deallocate")
	move_ret ref[284]
	c_ret

;; TERM 142: deallocate
c_code local build_ref_142
	ret_reg &ref[142]
	call_c   Dyam_Create_Atom("deallocate")
	move_ret ref[142]
	c_ret

c_code local build_seed_23
	ret_reg &seed[23]
	call_c   build_ref_0()
	call_c   build_ref_5()
	call_c   Dyam_Seed_Start(&ref[0],&ref[5],&ref[5],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[23]
	c_ret

;; TERM 5: inst(setcut, 'Dyam_Set_Cut', [])
c_code local build_ref_5
	ret_reg &ref[5]
	call_c   build_ref_277()
	call_c   build_ref_285()
	call_c   build_ref_286()
	call_c   Dyam_Term_Start(&ref[277],3)
	call_c   Dyam_Term_Arg(&ref[285])
	call_c   Dyam_Term_Arg(&ref[286])
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_End()
	move_ret ref[5]
	c_ret

;; TERM 286: 'Dyam_Set_Cut'
c_code local build_ref_286
	ret_reg &ref[286]
	call_c   Dyam_Create_Atom("Dyam_Set_Cut")
	move_ret ref[286]
	c_ret

;; TERM 285: setcut
c_code local build_ref_285
	ret_reg &ref[285]
	call_c   Dyam_Create_Atom("setcut")
	move_ret ref[285]
	c_ret

c_code local build_seed_24
	ret_reg &seed[24]
	call_c   build_ref_0()
	call_c   build_ref_6()
	call_c   Dyam_Seed_Start(&ref[0],&ref[6],&ref[6],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[24]
	c_ret

;; TERM 6: inst(cut, 'Dyam_Cut', [])
c_code local build_ref_6
	ret_reg &ref[6]
	call_c   build_ref_277()
	call_c   build_ref_287()
	call_c   build_ref_288()
	call_c   Dyam_Term_Start(&ref[277],3)
	call_c   Dyam_Term_Arg(&ref[287])
	call_c   Dyam_Term_Arg(&ref[288])
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_End()
	move_ret ref[6]
	c_ret

;; TERM 288: 'Dyam_Cut'
c_code local build_ref_288
	ret_reg &ref[288]
	call_c   Dyam_Create_Atom("Dyam_Cut")
	move_ret ref[288]
	c_ret

;; TERM 287: cut
c_code local build_ref_287
	ret_reg &ref[287]
	call_c   Dyam_Create_Atom("cut")
	move_ret ref[287]
	c_ret

c_code local build_seed_25
	ret_reg &seed[25]
	call_c   build_ref_0()
	call_c   build_ref_7()
	call_c   Dyam_Seed_Start(&ref[0],&ref[7],&ref[7],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[25]
	c_ret

;; TERM 7: inst(remove_choice, 'Dyam_Remove_Choice', [])
c_code local build_ref_7
	ret_reg &ref[7]
	call_c   build_ref_277()
	call_c   build_ref_152()
	call_c   build_ref_289()
	call_c   Dyam_Term_Start(&ref[277],3)
	call_c   Dyam_Term_Arg(&ref[152])
	call_c   Dyam_Term_Arg(&ref[289])
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_End()
	move_ret ref[7]
	c_ret

;; TERM 289: 'Dyam_Remove_Choice'
c_code local build_ref_289
	ret_reg &ref[289]
	call_c   Dyam_Create_Atom("Dyam_Remove_Choice")
	move_ret ref[289]
	c_ret

;; TERM 152: remove_choice
c_code local build_ref_152
	ret_reg &ref[152]
	call_c   Dyam_Create_Atom("remove_choice")
	move_ret ref[152]
	c_ret

c_code local build_seed_26
	ret_reg &seed[26]
	call_c   build_ref_0()
	call_c   build_ref_8()
	call_c   Dyam_Seed_Start(&ref[0],&ref[8],&ref[8],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[26]
	c_ret

;; TERM 8: inst(answer, 'Dyam_Answer', [])
c_code local build_ref_8
	ret_reg &ref[8]
	call_c   build_ref_277()
	call_c   build_ref_290()
	call_c   build_ref_291()
	call_c   Dyam_Term_Start(&ref[277],3)
	call_c   Dyam_Term_Arg(&ref[290])
	call_c   Dyam_Term_Arg(&ref[291])
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_End()
	move_ret ref[8]
	c_ret

;; TERM 291: 'Dyam_Answer'
c_code local build_ref_291
	ret_reg &ref[291]
	call_c   Dyam_Create_Atom("Dyam_Answer")
	move_ret ref[291]
	c_ret

;; TERM 290: answer
c_code local build_ref_290
	ret_reg &ref[290]
	call_c   Dyam_Create_Atom("answer")
	move_ret ref[290]
	c_ret

c_code local build_seed_27
	ret_reg &seed[27]
	call_c   build_ref_0()
	call_c   build_ref_9()
	call_c   Dyam_Seed_Start(&ref[0],&ref[9],&ref[9],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[27]
	c_ret

;; TERM 9: inst(schedule_last, 'Dyam_Schedule_Last', [])
c_code local build_ref_9
	ret_reg &ref[9]
	call_c   build_ref_277()
	call_c   build_ref_292()
	call_c   build_ref_293()
	call_c   Dyam_Term_Start(&ref[277],3)
	call_c   Dyam_Term_Arg(&ref[292])
	call_c   Dyam_Term_Arg(&ref[293])
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_End()
	move_ret ref[9]
	c_ret

;; TERM 293: 'Dyam_Schedule_Last'
c_code local build_ref_293
	ret_reg &ref[293]
	call_c   Dyam_Create_Atom("Dyam_Schedule_Last")
	move_ret ref[293]
	c_ret

;; TERM 292: schedule_last
c_code local build_ref_292
	ret_reg &ref[292]
	call_c   Dyam_Create_Atom("schedule_last")
	move_ret ref[292]
	c_ret

c_code local build_seed_28
	ret_reg &seed[28]
	call_c   build_ref_0()
	call_c   build_ref_10()
	call_c   Dyam_Seed_Start(&ref[0],&ref[10],&ref[10],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[28]
	c_ret

;; TERM 10: inst(schedule_first, 'Dyam_Now', [])
c_code local build_ref_10
	ret_reg &ref[10]
	call_c   build_ref_277()
	call_c   build_ref_294()
	call_c   build_ref_295()
	call_c   Dyam_Term_Start(&ref[277],3)
	call_c   Dyam_Term_Arg(&ref[294])
	call_c   Dyam_Term_Arg(&ref[295])
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_End()
	move_ret ref[10]
	c_ret

;; TERM 295: 'Dyam_Now'
c_code local build_ref_295
	ret_reg &ref[295]
	call_c   Dyam_Create_Atom("Dyam_Now")
	move_ret ref[295]
	c_ret

;; TERM 294: schedule_first
c_code local build_ref_294
	ret_reg &ref[294]
	call_c   Dyam_Create_Atom("schedule_first")
	move_ret ref[294]
	c_ret

c_code local build_seed_29
	ret_reg &seed[29]
	call_c   build_ref_0()
	call_c   build_ref_11()
	call_c   Dyam_Seed_Start(&ref[0],&ref[11],&ref[11],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[29]
	c_ret

;; TERM 11: inst(schedule, 'Dyam_Schedule', [])
c_code local build_ref_11
	ret_reg &ref[11]
	call_c   build_ref_277()
	call_c   build_ref_296()
	call_c   build_ref_297()
	call_c   Dyam_Term_Start(&ref[277],3)
	call_c   Dyam_Term_Arg(&ref[296])
	call_c   Dyam_Term_Arg(&ref[297])
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_End()
	move_ret ref[11]
	c_ret

;; TERM 297: 'Dyam_Schedule'
c_code local build_ref_297
	ret_reg &ref[297]
	call_c   Dyam_Create_Atom("Dyam_Schedule")
	move_ret ref[297]
	c_ret

;; TERM 296: schedule
c_code local build_ref_296
	ret_reg &ref[296]
	call_c   Dyam_Create_Atom("schedule")
	move_ret ref[296]
	c_ret

c_code local build_seed_30
	ret_reg &seed[30]
	call_c   build_ref_0()
	call_c   build_ref_12()
	call_c   Dyam_Seed_Start(&ref[0],&ref[12],&ref[12],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[30]
	c_ret

;; TERM 12: inst(tabule, 'Dyam_Tabule', [])
c_code local build_ref_12
	ret_reg &ref[12]
	call_c   build_ref_277()
	call_c   build_ref_298()
	call_c   build_ref_299()
	call_c   Dyam_Term_Start(&ref[277],3)
	call_c   Dyam_Term_Arg(&ref[298])
	call_c   Dyam_Term_Arg(&ref[299])
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_End()
	move_ret ref[12]
	c_ret

;; TERM 299: 'Dyam_Tabule'
c_code local build_ref_299
	ret_reg &ref[299]
	call_c   Dyam_Create_Atom("Dyam_Tabule")
	move_ret ref[299]
	c_ret

;; TERM 298: tabule
c_code local build_ref_298
	ret_reg &ref[298]
	call_c   Dyam_Create_Atom("tabule")
	move_ret ref[298]
	c_ret

c_code local build_seed_31
	ret_reg &seed[31]
	call_c   build_ref_0()
	call_c   build_ref_13()
	call_c   Dyam_Seed_Start(&ref[0],&ref[13],&ref[13],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[31]
	c_ret

;; TERM 13: allow_allocate_inversion(variance(_B))
c_code local build_ref_13
	ret_reg &ref[13]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_301()
	call_c   Dyam_Create_Unary(&ref[301],V(1))
	move_ret R(0)
	call_c   build_ref_300()
	call_c   Dyam_Create_Unary(&ref[300],R(0))
	move_ret ref[13]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 300: allow_allocate_inversion
c_code local build_ref_300
	ret_reg &ref[300]
	call_c   Dyam_Create_Atom("allow_allocate_inversion")
	move_ret ref[300]
	c_ret

;; TERM 301: variance
c_code local build_ref_301
	ret_reg &ref[301]
	call_c   Dyam_Create_Atom("variance")
	move_ret ref[301]
	c_ret

c_code local build_seed_32
	ret_reg &seed[32]
	call_c   build_ref_0()
	call_c   build_ref_14()
	call_c   Dyam_Seed_Start(&ref[0],&ref[14],&ref[14],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[32]
	c_ret

;; TERM 14: allow_allocate_inversion(subsume(_B))
c_code local build_ref_14
	ret_reg &ref[14]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_302()
	call_c   Dyam_Create_Unary(&ref[302],V(1))
	move_ret R(0)
	call_c   build_ref_300()
	call_c   Dyam_Create_Unary(&ref[300],R(0))
	move_ret ref[14]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 302: subsume
c_code local build_ref_302
	ret_reg &ref[302]
	call_c   Dyam_Create_Atom("subsume")
	move_ret ref[302]
	c_ret

c_code local build_seed_33
	ret_reg &seed[33]
	call_c   build_ref_0()
	call_c   build_ref_15()
	call_c   Dyam_Seed_Start(&ref[0],&ref[15],&ref[15],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[33]
	c_ret

;; TERM 15: allow_allocate_inversion(item_unify(_B))
c_code local build_ref_15
	ret_reg &ref[15]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_303()
	call_c   Dyam_Create_Unary(&ref[303],V(1))
	move_ret R(0)
	call_c   build_ref_300()
	call_c   Dyam_Create_Unary(&ref[300],R(0))
	move_ret ref[15]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 303: item_unify
c_code local build_ref_303
	ret_reg &ref[303]
	call_c   Dyam_Create_Atom("item_unify")
	move_ret ref[303]
	c_ret

c_code local build_seed_34
	ret_reg &seed[34]
	call_c   build_ref_0()
	call_c   build_ref_21()
	call_c   Dyam_Seed_Start(&ref[0],&ref[21],&ref[21],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[34]
	c_ret

;; TERM 21: allow_allocate_inversion(unify(_B, _C))
c_code local build_ref_21
	ret_reg &ref[21]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_304()
	call_c   Dyam_Create_Binary(&ref[304],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_300()
	call_c   Dyam_Create_Unary(&ref[300],R(0))
	move_ret ref[21]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 304: unify
c_code local build_ref_304
	ret_reg &ref[304]
	call_c   Dyam_Create_Atom("unify")
	move_ret ref[304]
	c_ret

c_code local build_seed_35
	ret_reg &seed[35]
	call_c   build_ref_0()
	call_c   build_ref_22()
	call_c   Dyam_Seed_Start(&ref[0],&ref[22],&ref[22],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[35]
	c_ret

;; TERM 22: allow_allocate_inversion(foreign_void_return(_B, _C))
c_code local build_ref_22
	ret_reg &ref[22]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_305()
	call_c   Dyam_Create_Binary(&ref[305],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_300()
	call_c   Dyam_Create_Unary(&ref[300],R(0))
	move_ret ref[22]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 305: foreign_void_return
c_code local build_ref_305
	ret_reg &ref[305]
	call_c   Dyam_Create_Atom("foreign_void_return")
	move_ret ref[305]
	c_ret

c_code local build_seed_36
	ret_reg &seed[36]
	call_c   build_ref_0()
	call_c   build_ref_23()
	call_c   Dyam_Seed_Start(&ref[0],&ref[23],&ref[23],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[36]
	c_ret

;; TERM 23: allow_allocate_inversion(foreign(_B, _C))
c_code local build_ref_23
	ret_reg &ref[23]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_306()
	call_c   Dyam_Create_Binary(&ref[306],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_300()
	call_c   Dyam_Create_Unary(&ref[300],R(0))
	move_ret ref[23]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 306: foreign
c_code local build_ref_306
	ret_reg &ref[306]
	call_c   Dyam_Create_Atom("foreign")
	move_ret ref[306]
	c_ret

c_code local build_seed_37
	ret_reg &seed[37]
	call_c   build_ref_0()
	call_c   build_ref_28()
	call_c   Dyam_Seed_Start(&ref[0],&ref[28],&ref[28],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[37]
	c_ret

;; TERM 28: inst(allocate, 'Dyam_Allocate', [c(0)])
c_code local build_ref_28
	ret_reg &ref[28]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_110()
	call_c   Dyam_Create_List(&ref[110],I(0))
	move_ret R(0)
	call_c   build_ref_277()
	call_c   build_ref_138()
	call_c   build_ref_307()
	call_c   Dyam_Term_Start(&ref[277],3)
	call_c   Dyam_Term_Arg(&ref[138])
	call_c   Dyam_Term_Arg(&ref[307])
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_End()
	move_ret ref[28]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 307: 'Dyam_Allocate'
c_code local build_ref_307
	ret_reg &ref[307]
	call_c   Dyam_Create_Atom("Dyam_Allocate")
	move_ret ref[307]
	c_ret

;; TERM 138: allocate
c_code local build_ref_138
	ret_reg &ref[138]
	call_c   Dyam_Create_Atom("allocate")
	move_ret ref[138]
	c_ret

;; TERM 110: c(0)
c_code local build_ref_110
	ret_reg &ref[110]
	call_c   build_ref_308()
	call_c   Dyam_Create_Unary(&ref[308],N(0))
	move_ret ref[110]
	c_ret

;; TERM 308: c
c_code local build_ref_308
	ret_reg &ref[308]
	call_c   Dyam_Create_Atom("c")
	move_ret ref[308]
	c_ret

c_code local build_seed_46
	ret_reg &seed[46]
	call_c   build_ref_0()
	call_c   build_ref_40()
	call_c   Dyam_Seed_Start(&ref[0],&ref[40],&ref[40],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[46]
	c_ret

;; TERM 40: stop_inst((fail :> _B), fail, [], 1)
c_code local build_ref_40
	ret_reg &ref[40]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_161()
	call_c   Dyam_Create_Binary(I(9),&ref[161],V(1))
	move_ret R(0)
	call_c   build_ref_309()
	call_c   Dyam_Term_Start(&ref[309],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[161])
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(N(1))
	call_c   Dyam_Term_End()
	move_ret ref[40]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 309: stop_inst
c_code local build_ref_309
	ret_reg &ref[309]
	call_c   Dyam_Create_Atom("stop_inst")
	move_ret ref[309]
	c_ret

;; TERM 161: fail
c_code local build_ref_161
	ret_reg &ref[161]
	call_c   Dyam_Create_Atom("fail")
	move_ret ref[161]
	c_ret

c_code local build_seed_47
	ret_reg &seed[47]
	call_c   build_ref_0()
	call_c   build_ref_41()
	call_c   Dyam_Seed_Start(&ref[0],&ref[41],&ref[41],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[47]
	c_ret

;; TERM 41: stop_inst((return :> _B), return, [], 1)
c_code local build_ref_41
	ret_reg &ref[41]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_310()
	call_c   Dyam_Create_Binary(I(9),&ref[310],V(1))
	move_ret R(0)
	call_c   build_ref_309()
	call_c   Dyam_Term_Start(&ref[309],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[310])
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(N(1))
	call_c   Dyam_Term_End()
	move_ret ref[41]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 310: return
c_code local build_ref_310
	ret_reg &ref[310]
	call_c   Dyam_Create_Atom("return")
	move_ret ref[310]
	c_ret

c_code local build_seed_48
	ret_reg &seed[48]
	call_c   build_ref_0()
	call_c   build_ref_42()
	call_c   Dyam_Seed_Start(&ref[0],&ref[42],&ref[42],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[48]
	c_ret

;; TERM 42: stop_inst((succeed :> _B), succeed, [], 1)
c_code local build_ref_42
	ret_reg &ref[42]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_143()
	call_c   Dyam_Create_Binary(I(9),&ref[143],V(1))
	move_ret R(0)
	call_c   build_ref_309()
	call_c   Dyam_Term_Start(&ref[309],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[143])
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(N(1))
	call_c   Dyam_Term_End()
	move_ret ref[42]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 143: succeed
c_code local build_ref_143
	ret_reg &ref[143]
	call_c   Dyam_Create_Atom("succeed")
	move_ret ref[143]
	c_ret

c_code local build_seed_38
	ret_reg &seed[38]
	call_c   build_ref_0()
	call_c   build_ref_32()
	call_c   Dyam_Seed_Start(&ref[0],&ref[32],&ref[32],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[38]
	c_ret

;; TERM 32: inst(update_choice(_B), 'Dyam_Update_Choice', [_B])
c_code local build_ref_32
	ret_reg &ref[32]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_311()
	call_c   Dyam_Create_Unary(&ref[311],V(1))
	move_ret R(0)
	call_c   build_ref_277()
	call_c   build_ref_312()
	call_c   build_ref_276()
	call_c   Dyam_Term_Start(&ref[277],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[312])
	call_c   Dyam_Term_Arg(&ref[276])
	call_c   Dyam_Term_End()
	move_ret ref[32]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 276: [_B]
c_code local build_ref_276
	ret_reg &ref[276]
	call_c   Dyam_Create_List(V(1),I(0))
	move_ret ref[276]
	c_ret

;; TERM 312: 'Dyam_Update_Choice'
c_code local build_ref_312
	ret_reg &ref[312]
	call_c   Dyam_Create_Atom("Dyam_Update_Choice")
	move_ret ref[312]
	c_ret

;; TERM 311: update_choice
c_code local build_ref_311
	ret_reg &ref[311]
	call_c   Dyam_Create_Atom("update_choice")
	move_ret ref[311]
	c_ret

c_code local build_seed_39
	ret_reg &seed[39]
	call_c   build_ref_0()
	call_c   build_ref_33()
	call_c   Dyam_Seed_Start(&ref[0],&ref[33],&ref[33],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[39]
	c_ret

;; TERM 33: inst(choice(_B), 'Dyam_Choice', [_B])
c_code local build_ref_33
	ret_reg &ref[33]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_313()
	call_c   Dyam_Create_Unary(&ref[313],V(1))
	move_ret R(0)
	call_c   build_ref_277()
	call_c   build_ref_314()
	call_c   build_ref_276()
	call_c   Dyam_Term_Start(&ref[277],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[314])
	call_c   Dyam_Term_Arg(&ref[276])
	call_c   Dyam_Term_End()
	move_ret ref[33]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 314: 'Dyam_Choice'
c_code local build_ref_314
	ret_reg &ref[314]
	call_c   Dyam_Create_Atom("Dyam_Choice")
	move_ret ref[314]
	c_ret

;; TERM 313: choice
c_code local build_ref_313
	ret_reg &ref[313]
	call_c   Dyam_Create_Atom("choice")
	move_ret ref[313]
	c_ret

c_code local build_seed_40
	ret_reg &seed[40]
	call_c   build_ref_0()
	call_c   build_ref_34()
	call_c   Dyam_Seed_Start(&ref[0],&ref[34],&ref[34],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[40]
	c_ret

;; TERM 34: inst(light_object(_B), 'Dyam_Light_Object', [_B])
c_code local build_ref_34
	ret_reg &ref[34]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_315()
	call_c   Dyam_Create_Unary(&ref[315],V(1))
	move_ret R(0)
	call_c   build_ref_277()
	call_c   build_ref_316()
	call_c   build_ref_276()
	call_c   Dyam_Term_Start(&ref[277],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[316])
	call_c   Dyam_Term_Arg(&ref[276])
	call_c   Dyam_Term_End()
	move_ret ref[34]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 316: 'Dyam_Light_Object'
c_code local build_ref_316
	ret_reg &ref[316]
	call_c   Dyam_Create_Atom("Dyam_Light_Object")
	move_ret ref[316]
	c_ret

;; TERM 315: light_object
c_code local build_ref_315
	ret_reg &ref[315]
	call_c   Dyam_Create_Atom("light_object")
	move_ret ref[315]
	c_ret

c_code local build_seed_41
	ret_reg &seed[41]
	call_c   build_ref_0()
	call_c   build_ref_35()
	call_c   Dyam_Seed_Start(&ref[0],&ref[35],&ref[35],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[41]
	c_ret

;; TERM 35: inst(object(_B), 'Dyam_Object', [_B])
c_code local build_ref_35
	ret_reg &ref[35]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_317()
	call_c   Dyam_Create_Unary(&ref[317],V(1))
	move_ret R(0)
	call_c   build_ref_277()
	call_c   build_ref_318()
	call_c   build_ref_276()
	call_c   Dyam_Term_Start(&ref[277],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[318])
	call_c   Dyam_Term_Arg(&ref[276])
	call_c   Dyam_Term_End()
	move_ret ref[35]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 318: 'Dyam_Object'
c_code local build_ref_318
	ret_reg &ref[318]
	call_c   Dyam_Create_Atom("Dyam_Object")
	move_ret ref[318]
	c_ret

;; TERM 317: object
c_code local build_ref_317
	ret_reg &ref[317]
	call_c   Dyam_Create_Atom("object")
	move_ret ref[317]
	c_ret

c_code local build_seed_42
	ret_reg &seed[42]
	call_c   build_ref_0()
	call_c   build_ref_36()
	call_c   Dyam_Seed_Start(&ref[0],&ref[36],&ref[36],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[42]
	c_ret

;; TERM 36: inst(backptr(_B), 'Dyam_Backptr', [_B])
c_code local build_ref_36
	ret_reg &ref[36]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_319()
	call_c   Dyam_Create_Unary(&ref[319],V(1))
	move_ret R(0)
	call_c   build_ref_277()
	call_c   build_ref_320()
	call_c   build_ref_276()
	call_c   Dyam_Term_Start(&ref[277],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[320])
	call_c   Dyam_Term_Arg(&ref[276])
	call_c   Dyam_Term_End()
	move_ret ref[36]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 320: 'Dyam_Backptr'
c_code local build_ref_320
	ret_reg &ref[320]
	call_c   Dyam_Create_Atom("Dyam_Backptr")
	move_ret ref[320]
	c_ret

;; TERM 319: backptr
c_code local build_ref_319
	ret_reg &ref[319]
	call_c   Dyam_Create_Atom("backptr")
	move_ret ref[319]
	c_ret

c_code local build_seed_43
	ret_reg &seed[43]
	call_c   build_ref_0()
	call_c   build_ref_37()
	call_c   Dyam_Seed_Start(&ref[0],&ref[37],&ref[37],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[43]
	c_ret

;; TERM 37: test_inst(variance(_B), 'Dyam_Variance', [_B])
c_code local build_ref_37
	ret_reg &ref[37]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_301()
	call_c   Dyam_Create_Unary(&ref[301],V(1))
	move_ret R(0)
	call_c   build_ref_321()
	call_c   build_ref_322()
	call_c   build_ref_276()
	call_c   Dyam_Term_Start(&ref[321],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[322])
	call_c   Dyam_Term_Arg(&ref[276])
	call_c   Dyam_Term_End()
	move_ret ref[37]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 322: 'Dyam_Variance'
c_code local build_ref_322
	ret_reg &ref[322]
	call_c   Dyam_Create_Atom("Dyam_Variance")
	move_ret ref[322]
	c_ret

;; TERM 321: test_inst
c_code local build_ref_321
	ret_reg &ref[321]
	call_c   Dyam_Create_Atom("test_inst")
	move_ret ref[321]
	c_ret

c_code local build_seed_44
	ret_reg &seed[44]
	call_c   build_ref_0()
	call_c   build_ref_38()
	call_c   Dyam_Seed_Start(&ref[0],&ref[38],&ref[38],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[44]
	c_ret

;; TERM 38: test_inst(subsume(_B), 'Dyam_Subsume', [_B])
c_code local build_ref_38
	ret_reg &ref[38]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_302()
	call_c   Dyam_Create_Unary(&ref[302],V(1))
	move_ret R(0)
	call_c   build_ref_321()
	call_c   build_ref_323()
	call_c   build_ref_276()
	call_c   Dyam_Term_Start(&ref[321],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[323])
	call_c   Dyam_Term_Arg(&ref[276])
	call_c   Dyam_Term_End()
	move_ret ref[38]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 323: 'Dyam_Subsume'
c_code local build_ref_323
	ret_reg &ref[323]
	call_c   Dyam_Create_Atom("Dyam_Subsume")
	move_ret ref[323]
	c_ret

c_code local build_seed_45
	ret_reg &seed[45]
	call_c   build_ref_0()
	call_c   build_ref_39()
	call_c   Dyam_Seed_Start(&ref[0],&ref[39],&ref[39],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[45]
	c_ret

;; TERM 39: test_inst(item_unify(_B), 'Dyam_Unify_Item', [_B])
c_code local build_ref_39
	ret_reg &ref[39]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_303()
	call_c   Dyam_Create_Unary(&ref[303],V(1))
	move_ret R(0)
	call_c   build_ref_321()
	call_c   build_ref_324()
	call_c   build_ref_276()
	call_c   Dyam_Term_Start(&ref[321],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[324])
	call_c   Dyam_Term_Arg(&ref[276])
	call_c   Dyam_Term_End()
	move_ret ref[39]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 324: 'Dyam_Unify_Item'
c_code local build_ref_324
	ret_reg &ref[324]
	call_c   Dyam_Create_Atom("Dyam_Unify_Item")
	move_ret ref[324]
	c_ret

c_code local build_seed_49
	ret_reg &seed[49]
	call_c   build_ref_0()
	call_c   build_ref_52()
	call_c   Dyam_Seed_Start(&ref[0],&ref[52],&ref[52],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[49]
	c_ret

;; TERM 52: inst(mem_load_nil(_B), 'Dyam_Mem_Load_Nil', [c(_B)])
c_code local build_ref_52
	ret_reg &ref[52]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_325()
	call_c   Dyam_Create_Unary(&ref[325],V(1))
	move_ret R(0)
	call_c   build_ref_308()
	call_c   Dyam_Create_Unary(&ref[308],V(1))
	move_ret R(1)
	call_c   Dyam_Create_List(R(1),I(0))
	move_ret R(1)
	call_c   build_ref_277()
	call_c   build_ref_326()
	call_c   Dyam_Term_Start(&ref[277],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[326])
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_End()
	move_ret ref[52]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 326: 'Dyam_Mem_Load_Nil'
c_code local build_ref_326
	ret_reg &ref[326]
	call_c   Dyam_Create_Atom("Dyam_Mem_Load_Nil")
	move_ret ref[326]
	c_ret

;; TERM 325: mem_load_nil
c_code local build_ref_325
	ret_reg &ref[325]
	call_c   Dyam_Create_Atom("mem_load_nil")
	move_ret ref[325]
	c_ret

c_code local build_seed_50
	ret_reg &seed[50]
	call_c   build_ref_0()
	call_c   build_ref_53()
	call_c   Dyam_Seed_Start(&ref[0],&ref[53],&ref[53],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[50]
	c_ret

;; TERM 53: inst(reg_deallocate_alt(_B), 'Dyam_Reg_Deallocate_Alt', [c(_B)])
c_code local build_ref_53
	ret_reg &ref[53]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_327()
	call_c   Dyam_Create_Unary(&ref[327],V(1))
	move_ret R(0)
	call_c   build_ref_308()
	call_c   Dyam_Create_Unary(&ref[308],V(1))
	move_ret R(1)
	call_c   Dyam_Create_List(R(1),I(0))
	move_ret R(1)
	call_c   build_ref_277()
	call_c   build_ref_328()
	call_c   Dyam_Term_Start(&ref[277],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[328])
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_End()
	move_ret ref[53]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 328: 'Dyam_Reg_Deallocate_Alt'
c_code local build_ref_328
	ret_reg &ref[328]
	call_c   Dyam_Create_Atom("Dyam_Reg_Deallocate_Alt")
	move_ret ref[328]
	c_ret

;; TERM 327: reg_deallocate_alt
c_code local build_ref_327
	ret_reg &ref[327]
	call_c   Dyam_Create_Atom("reg_deallocate_alt")
	move_ret ref[327]
	c_ret

c_code local build_seed_51
	ret_reg &seed[51]
	call_c   build_ref_0()
	call_c   build_ref_54()
	call_c   Dyam_Seed_Start(&ref[0],&ref[54],&ref[54],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[51]
	c_ret

;; TERM 54: inst(reg_deallocate(_B), 'Dyam_Reg_Deallocate', [c(_B)])
c_code local build_ref_54
	ret_reg &ref[54]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_329()
	call_c   Dyam_Create_Unary(&ref[329],V(1))
	move_ret R(0)
	call_c   build_ref_308()
	call_c   Dyam_Create_Unary(&ref[308],V(1))
	move_ret R(1)
	call_c   Dyam_Create_List(R(1),I(0))
	move_ret R(1)
	call_c   build_ref_277()
	call_c   build_ref_330()
	call_c   Dyam_Term_Start(&ref[277],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[330])
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_End()
	move_ret ref[54]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 330: 'Dyam_Reg_Deallocate'
c_code local build_ref_330
	ret_reg &ref[330]
	call_c   Dyam_Create_Atom("Dyam_Reg_Deallocate")
	move_ret ref[330]
	c_ret

;; TERM 329: reg_deallocate
c_code local build_ref_329
	ret_reg &ref[329]
	call_c   Dyam_Create_Atom("reg_deallocate")
	move_ret ref[329]
	c_ret

c_code local build_seed_52
	ret_reg &seed[52]
	call_c   build_ref_0()
	call_c   build_ref_55()
	call_c   Dyam_Seed_Start(&ref[0],&ref[55],&ref[55],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[52]
	c_ret

;; TERM 55: inst(reg_multi_bind_zero_one(_B), 'Dyam_Multi_Reg_Bind_ZeroOne', [c(_B)])
c_code local build_ref_55
	ret_reg &ref[55]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_331()
	call_c   Dyam_Create_Unary(&ref[331],V(1))
	move_ret R(0)
	call_c   build_ref_308()
	call_c   Dyam_Create_Unary(&ref[308],V(1))
	move_ret R(1)
	call_c   Dyam_Create_List(R(1),I(0))
	move_ret R(1)
	call_c   build_ref_277()
	call_c   build_ref_332()
	call_c   Dyam_Term_Start(&ref[277],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[332])
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_End()
	move_ret ref[55]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 332: 'Dyam_Multi_Reg_Bind_ZeroOne'
c_code local build_ref_332
	ret_reg &ref[332]
	call_c   Dyam_Create_Atom("Dyam_Multi_Reg_Bind_ZeroOne")
	move_ret ref[332]
	c_ret

;; TERM 331: reg_multi_bind_zero_one
c_code local build_ref_331
	ret_reg &ref[331]
	call_c   Dyam_Create_Atom("reg_multi_bind_zero_one")
	move_ret ref[331]
	c_ret

c_code local build_seed_53
	ret_reg &seed[53]
	call_c   build_ref_0()
	call_c   build_ref_56()
	call_c   Dyam_Seed_Start(&ref[0],&ref[56],&ref[56],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[53]
	c_ret

;; TERM 56: test_inst(reg_alt_unif_nil(_B), 'Dyam_Reg_Alt_Unify_Nil', [c(_B)])
c_code local build_ref_56
	ret_reg &ref[56]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_333()
	call_c   Dyam_Create_Unary(&ref[333],V(1))
	move_ret R(0)
	call_c   build_ref_308()
	call_c   Dyam_Create_Unary(&ref[308],V(1))
	move_ret R(1)
	call_c   Dyam_Create_List(R(1),I(0))
	move_ret R(1)
	call_c   build_ref_321()
	call_c   build_ref_334()
	call_c   Dyam_Term_Start(&ref[321],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[334])
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_End()
	move_ret ref[56]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 334: 'Dyam_Reg_Alt_Unify_Nil'
c_code local build_ref_334
	ret_reg &ref[334]
	call_c   Dyam_Create_Atom("Dyam_Reg_Alt_Unify_Nil")
	move_ret ref[334]
	c_ret

;; TERM 333: reg_alt_unif_nil
c_code local build_ref_333
	ret_reg &ref[333]
	call_c   Dyam_Create_Atom("reg_alt_unif_nil")
	move_ret ref[333]
	c_ret

c_code local build_seed_54
	ret_reg &seed[54]
	call_c   build_ref_0()
	call_c   build_ref_57()
	call_c   Dyam_Seed_Start(&ref[0],&ref[57],&ref[57],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[54]
	c_ret

;; TERM 57: test_inst(reg_unif_nil(_B), 'Dyam_Reg_Unify_Nil', [c(_B)])
c_code local build_ref_57
	ret_reg &ref[57]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_335()
	call_c   Dyam_Create_Unary(&ref[335],V(1))
	move_ret R(0)
	call_c   build_ref_308()
	call_c   Dyam_Create_Unary(&ref[308],V(1))
	move_ret R(1)
	call_c   Dyam_Create_List(R(1),I(0))
	move_ret R(1)
	call_c   build_ref_321()
	call_c   build_ref_336()
	call_c   Dyam_Term_Start(&ref[321],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[336])
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_End()
	move_ret ref[57]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 336: 'Dyam_Reg_Unify_Nil'
c_code local build_ref_336
	ret_reg &ref[336]
	call_c   Dyam_Create_Atom("Dyam_Reg_Unify_Nil")
	move_ret ref[336]
	c_ret

;; TERM 335: reg_unif_nil
c_code local build_ref_335
	ret_reg &ref[335]
	call_c   Dyam_Create_Atom("reg_unif_nil")
	move_ret ref[335]
	c_ret

c_code local build_seed_57
	ret_reg &seed[57]
	call_c   build_ref_0()
	call_c   build_ref_60()
	call_c   Dyam_Seed_Start(&ref[0],&ref[60],&ref[60],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[57]
	c_ret

;; TERM 60: allow_allocate_inversion(unify(_B, _C, _D, _E))
c_code local build_ref_60
	ret_reg &ref[60]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_304()
	call_c   Dyam_Term_Start(&ref[304],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_300()
	call_c   Dyam_Create_Unary(&ref[300],R(0))
	move_ret ref[60]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_58
	ret_reg &seed[58]
	call_c   build_ref_0()
	call_c   build_ref_61()
	call_c   Dyam_Seed_Start(&ref[0],&ref[61],&ref[61],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[58]
	c_ret

;; TERM 61: allow_allocate_inversion(c_call(_B, _C, _D, _E))
c_code local build_ref_61
	ret_reg &ref[61]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_337()
	call_c   Dyam_Term_Start(&ref[337],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_300()
	call_c   Dyam_Create_Unary(&ref[300],R(0))
	move_ret ref[61]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 337: c_call
c_code local build_ref_337
	ret_reg &ref[337]
	call_c   Dyam_Create_Atom("c_call")
	move_ret ref[337]
	c_ret

c_code local build_seed_55
	ret_reg &seed[55]
	call_c   build_ref_0()
	call_c   build_ref_58()
	call_c   Dyam_Seed_Start(&ref[0],&ref[58],&ref[58],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[55]
	c_ret

;; TERM 58: inst(foreign_void_return(_B, _C), ('DYAM_' + _B), _C)
c_code local build_ref_58
	ret_reg &ref[58]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_305()
	call_c   Dyam_Create_Binary(&ref[305],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_338()
	call_c   build_ref_339()
	call_c   Dyam_Create_Binary(&ref[338],&ref[339],V(1))
	move_ret R(1)
	call_c   build_ref_277()
	call_c   Dyam_Term_Start(&ref[277],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_End()
	move_ret ref[58]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 339: 'DYAM_'
c_code local build_ref_339
	ret_reg &ref[339]
	call_c   Dyam_Create_Atom("DYAM_")
	move_ret ref[339]
	c_ret

;; TERM 338: +
c_code local build_ref_338
	ret_reg &ref[338]
	call_c   Dyam_Create_Atom("+")
	move_ret ref[338]
	c_ret

c_code local build_seed_56
	ret_reg &seed[56]
	call_c   build_ref_0()
	call_c   build_ref_59()
	call_c   Dyam_Seed_Start(&ref[0],&ref[59],&ref[59],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[56]
	c_ret

;; TERM 59: test_inst(foreign(_B, _C), ('DYAM_' + _B), _C)
c_code local build_ref_59
	ret_reg &ref[59]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_306()
	call_c   Dyam_Create_Binary(&ref[306],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_338()
	call_c   build_ref_339()
	call_c   Dyam_Create_Binary(&ref[338],&ref[339],V(1))
	move_ret R(1)
	call_c   build_ref_321()
	call_c   Dyam_Term_Start(&ref[321],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_End()
	move_ret ref[59]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_59
	ret_reg &seed[59]
	call_c   build_ref_0()
	call_c   build_ref_68()
	call_c   Dyam_Seed_Start(&ref[0],&ref[68],&ref[68],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[59]
	c_ret

;; TERM 68: inst(indexingkey(_B, _C), 'Dyam_Compute_IndexingKey', [_B,_C])
c_code local build_ref_68
	ret_reg &ref[68]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_340()
	call_c   Dyam_Create_Binary(&ref[340],V(1),V(2))
	move_ret R(0)
	call_c   Dyam_Create_Tupple(1,2,I(0))
	move_ret R(1)
	call_c   build_ref_277()
	call_c   build_ref_341()
	call_c   Dyam_Term_Start(&ref[277],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[341])
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_End()
	move_ret ref[68]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 341: 'Dyam_Compute_IndexingKey'
c_code local build_ref_341
	ret_reg &ref[341]
	call_c   Dyam_Create_Atom("Dyam_Compute_IndexingKey")
	move_ret ref[341]
	c_ret

;; TERM 340: indexingkey
c_code local build_ref_340
	ret_reg &ref[340]
	call_c   Dyam_Create_Atom("indexingkey")
	move_ret ref[340]
	c_ret

c_code local build_seed_60
	ret_reg &seed[60]
	call_c   build_ref_0()
	call_c   build_ref_69()
	call_c   Dyam_Seed_Start(&ref[0],&ref[69],&ref[69],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[60]
	c_ret

;; TERM 69: inst(backptr(_B, _C), 'Dyam_Backptr_Trace', [_B,_C])
c_code local build_ref_69
	ret_reg &ref[69]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_319()
	call_c   Dyam_Create_Binary(&ref[319],V(1),V(2))
	move_ret R(0)
	call_c   Dyam_Create_Tupple(1,2,I(0))
	move_ret R(1)
	call_c   build_ref_277()
	call_c   build_ref_342()
	call_c   Dyam_Term_Start(&ref[277],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[342])
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_End()
	move_ret ref[69]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 342: 'Dyam_Backptr_Trace'
c_code local build_ref_342
	ret_reg &ref[342]
	call_c   Dyam_Create_Atom("Dyam_Backptr_Trace")
	move_ret ref[342]
	c_ret

c_code local build_seed_61
	ret_reg &seed[61]
	call_c   build_ref_0()
	call_c   build_ref_70()
	call_c   Dyam_Seed_Start(&ref[0],&ref[70],&ref[70],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[61]
	c_ret

;; TERM 70: test_inst(unify(_B, _C), 'Dyam_Unify', [_B,_C])
c_code local build_ref_70
	ret_reg &ref[70]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_304()
	call_c   Dyam_Create_Binary(&ref[304],V(1),V(2))
	move_ret R(0)
	call_c   Dyam_Create_Tupple(1,2,I(0))
	move_ret R(1)
	call_c   build_ref_321()
	call_c   build_ref_343()
	call_c   Dyam_Term_Start(&ref[321],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[343])
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_End()
	move_ret ref[70]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 343: 'Dyam_Unify'
c_code local build_ref_343
	ret_reg &ref[343]
	call_c   Dyam_Create_Atom("Dyam_Unify")
	move_ret ref[343]
	c_ret

c_code local build_seed_62
	ret_reg &seed[62]
	call_c   build_ref_0()
	call_c   build_ref_71()
	call_c   Dyam_Seed_Start(&ref[0],&ref[71],&ref[71],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[62]
	c_ret

;; TERM 71: test_inst((_B == _C), 'Dyam_Bind', [_B,_C])
c_code local build_ref_71
	ret_reg &ref[71]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_344()
	call_c   Dyam_Create_Binary(&ref[344],V(1),V(2))
	move_ret R(0)
	call_c   Dyam_Create_Tupple(1,2,I(0))
	move_ret R(1)
	call_c   build_ref_321()
	call_c   build_ref_345()
	call_c   Dyam_Term_Start(&ref[321],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[345])
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_End()
	move_ret ref[71]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 345: 'Dyam_Bind'
c_code local build_ref_345
	ret_reg &ref[345]
	call_c   Dyam_Create_Atom("Dyam_Bind")
	move_ret ref[345]
	c_ret

;; TERM 344: ==
c_code local build_ref_344
	ret_reg &ref[344]
	call_c   Dyam_Create_Atom("==")
	move_ret ref[344]
	c_ret

c_code local build_seed_63
	ret_reg &seed[63]
	call_c   build_ref_0()
	call_c   build_ref_72()
	call_c   Dyam_Seed_Start(&ref[0],&ref[72],&ref[72],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[63]
	c_ret

;; TERM 72: inst(reg_alt_bind(_B, _C), 'Dyam_Reg_Alt_Bind', [c(_B),_C])
c_code local build_ref_72
	ret_reg &ref[72]
	call_c   Dyam_Pseudo_Choice(3)
	call_c   build_ref_346()
	call_c   Dyam_Create_Binary(&ref[346],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_308()
	call_c   Dyam_Create_Unary(&ref[308],V(1))
	move_ret R(1)
	call_c   Dyam_Create_List(V(2),I(0))
	move_ret R(2)
	call_c   Dyam_Create_List(R(1),R(2))
	move_ret R(2)
	call_c   build_ref_277()
	call_c   build_ref_347()
	call_c   Dyam_Term_Start(&ref[277],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[347])
	call_c   Dyam_Term_Arg(R(2))
	call_c   Dyam_Term_End()
	move_ret ref[72]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 347: 'Dyam_Reg_Alt_Bind'
c_code local build_ref_347
	ret_reg &ref[347]
	call_c   Dyam_Create_Atom("Dyam_Reg_Alt_Bind")
	move_ret ref[347]
	c_ret

;; TERM 346: reg_alt_bind
c_code local build_ref_346
	ret_reg &ref[346]
	call_c   Dyam_Create_Atom("reg_alt_bind")
	move_ret ref[346]
	c_ret

c_code local build_seed_64
	ret_reg &seed[64]
	call_c   build_ref_0()
	call_c   build_ref_73()
	call_c   Dyam_Seed_Start(&ref[0],&ref[73],&ref[73],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[64]
	c_ret

;; TERM 73: inst(mem_load_var(_B, _C), 'Dyam_Mem_Load_Var', [c(_B),_C])
c_code local build_ref_73
	ret_reg &ref[73]
	call_c   Dyam_Pseudo_Choice(3)
	call_c   build_ref_348()
	call_c   Dyam_Create_Binary(&ref[348],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_308()
	call_c   Dyam_Create_Unary(&ref[308],V(1))
	move_ret R(1)
	call_c   Dyam_Create_List(V(2),I(0))
	move_ret R(2)
	call_c   Dyam_Create_List(R(1),R(2))
	move_ret R(2)
	call_c   build_ref_277()
	call_c   build_ref_349()
	call_c   Dyam_Term_Start(&ref[277],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[349])
	call_c   Dyam_Term_Arg(R(2))
	call_c   Dyam_Term_End()
	move_ret ref[73]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 349: 'Dyam_Mem_Load_Var'
c_code local build_ref_349
	ret_reg &ref[349]
	call_c   Dyam_Create_Atom("Dyam_Mem_Load_Var")
	move_ret ref[349]
	c_ret

;; TERM 348: mem_load_var
c_code local build_ref_348
	ret_reg &ref[348]
	call_c   Dyam_Create_Atom("mem_load_var")
	move_ret ref[348]
	c_ret

c_code local build_seed_65
	ret_reg &seed[65]
	call_c   build_ref_0()
	call_c   build_ref_74()
	call_c   Dyam_Seed_Start(&ref[0],&ref[74],&ref[74],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[65]
	c_ret

;; TERM 74: inst(mem_load_cst(_B, _C), 'Dyam_Mem_Load_Cst', [c(_B),_C])
c_code local build_ref_74
	ret_reg &ref[74]
	call_c   Dyam_Pseudo_Choice(3)
	call_c   build_ref_350()
	call_c   Dyam_Create_Binary(&ref[350],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_308()
	call_c   Dyam_Create_Unary(&ref[308],V(1))
	move_ret R(1)
	call_c   Dyam_Create_List(V(2),I(0))
	move_ret R(2)
	call_c   Dyam_Create_List(R(1),R(2))
	move_ret R(2)
	call_c   build_ref_277()
	call_c   build_ref_351()
	call_c   Dyam_Term_Start(&ref[277],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[351])
	call_c   Dyam_Term_Arg(R(2))
	call_c   Dyam_Term_End()
	move_ret ref[74]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 351: 'Dyam_Mem_Load_Cst'
c_code local build_ref_351
	ret_reg &ref[351]
	call_c   Dyam_Create_Atom("Dyam_Mem_Load_Cst")
	move_ret ref[351]
	c_ret

;; TERM 350: mem_load_cst
c_code local build_ref_350
	ret_reg &ref[350]
	call_c   Dyam_Create_Atom("mem_load_cst")
	move_ret ref[350]
	c_ret

c_code local build_seed_66
	ret_reg &seed[66]
	call_c   build_ref_0()
	call_c   build_ref_75()
	call_c   Dyam_Seed_Start(&ref[0],&ref[75],&ref[75],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[66]
	c_ret

;; TERM 75: inst(mem_load(_B, _C), 'Dyam_Mem_Load', [c(_B),_C])
c_code local build_ref_75
	ret_reg &ref[75]
	call_c   Dyam_Pseudo_Choice(3)
	call_c   build_ref_352()
	call_c   Dyam_Create_Binary(&ref[352],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_308()
	call_c   Dyam_Create_Unary(&ref[308],V(1))
	move_ret R(1)
	call_c   Dyam_Create_List(V(2),I(0))
	move_ret R(2)
	call_c   Dyam_Create_List(R(1),R(2))
	move_ret R(2)
	call_c   build_ref_277()
	call_c   build_ref_353()
	call_c   Dyam_Term_Start(&ref[277],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[353])
	call_c   Dyam_Term_Arg(R(2))
	call_c   Dyam_Term_End()
	move_ret ref[75]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 353: 'Dyam_Mem_Load'
c_code local build_ref_353
	ret_reg &ref[353]
	call_c   Dyam_Create_Atom("Dyam_Mem_Load")
	move_ret ref[353]
	c_ret

;; TERM 352: mem_load
c_code local build_ref_352
	ret_reg &ref[352]
	call_c   Dyam_Create_Atom("mem_load")
	move_ret ref[352]
	c_ret

c_code local build_seed_67
	ret_reg &seed[67]
	call_c   build_ref_0()
	call_c   build_ref_76()
	call_c   Dyam_Seed_Start(&ref[0],&ref[76],&ref[76],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[67]
	c_ret

;; TERM 76: inst(reg_bind(_B, _C), 'Dyam_Reg_Bind', [c(_B),_C])
c_code local build_ref_76
	ret_reg &ref[76]
	call_c   Dyam_Pseudo_Choice(3)
	call_c   build_ref_354()
	call_c   Dyam_Create_Binary(&ref[354],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_308()
	call_c   Dyam_Create_Unary(&ref[308],V(1))
	move_ret R(1)
	call_c   Dyam_Create_List(V(2),I(0))
	move_ret R(2)
	call_c   Dyam_Create_List(R(1),R(2))
	move_ret R(2)
	call_c   build_ref_277()
	call_c   build_ref_355()
	call_c   Dyam_Term_Start(&ref[277],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[355])
	call_c   Dyam_Term_Arg(R(2))
	call_c   Dyam_Term_End()
	move_ret ref[76]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 355: 'Dyam_Reg_Bind'
c_code local build_ref_355
	ret_reg &ref[355]
	call_c   Dyam_Create_Atom("Dyam_Reg_Bind")
	move_ret ref[355]
	c_ret

;; TERM 354: reg_bind
c_code local build_ref_354
	ret_reg &ref[354]
	call_c   Dyam_Create_Atom("reg_bind")
	move_ret ref[354]
	c_ret

c_code local build_seed_68
	ret_reg &seed[68]
	call_c   build_ref_0()
	call_c   build_ref_77()
	call_c   Dyam_Seed_Start(&ref[0],&ref[77],&ref[77],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[68]
	c_ret

;; TERM 77: inst(reg_load(_B, _C), 'Dyam_Reg_Load', [c(_B),_C])
c_code local build_ref_77
	ret_reg &ref[77]
	call_c   Dyam_Pseudo_Choice(3)
	call_c   build_ref_356()
	call_c   Dyam_Create_Binary(&ref[356],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_308()
	call_c   Dyam_Create_Unary(&ref[308],V(1))
	move_ret R(1)
	call_c   Dyam_Create_List(V(2),I(0))
	move_ret R(2)
	call_c   Dyam_Create_List(R(1),R(2))
	move_ret R(2)
	call_c   build_ref_277()
	call_c   build_ref_357()
	call_c   Dyam_Term_Start(&ref[277],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[357])
	call_c   Dyam_Term_Arg(R(2))
	call_c   Dyam_Term_End()
	move_ret ref[77]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 357: 'Dyam_Reg_Load'
c_code local build_ref_357
	ret_reg &ref[357]
	call_c   Dyam_Create_Atom("Dyam_Reg_Load")
	move_ret ref[357]
	c_ret

;; TERM 356: reg_load
c_code local build_ref_356
	ret_reg &ref[356]
	call_c   Dyam_Create_Atom("reg_load")
	move_ret ref[356]
	c_ret

c_code local build_seed_69
	ret_reg &seed[69]
	call_c   build_ref_0()
	call_c   build_ref_78()
	call_c   Dyam_Seed_Start(&ref[0],&ref[78],&ref[78],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[69]
	c_ret

;; TERM 78: test_inst(reg_alt_unif_cst(_B, _C), 'Dyam_Reg_Alt_Unify_Cst', [c(_B),_C])
c_code local build_ref_78
	ret_reg &ref[78]
	call_c   Dyam_Pseudo_Choice(3)
	call_c   build_ref_358()
	call_c   Dyam_Create_Binary(&ref[358],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_308()
	call_c   Dyam_Create_Unary(&ref[308],V(1))
	move_ret R(1)
	call_c   Dyam_Create_List(V(2),I(0))
	move_ret R(2)
	call_c   Dyam_Create_List(R(1),R(2))
	move_ret R(2)
	call_c   build_ref_321()
	call_c   build_ref_359()
	call_c   Dyam_Term_Start(&ref[321],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[359])
	call_c   Dyam_Term_Arg(R(2))
	call_c   Dyam_Term_End()
	move_ret ref[78]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 359: 'Dyam_Reg_Alt_Unify_Cst'
c_code local build_ref_359
	ret_reg &ref[359]
	call_c   Dyam_Create_Atom("Dyam_Reg_Alt_Unify_Cst")
	move_ret ref[359]
	c_ret

;; TERM 358: reg_alt_unif_cst
c_code local build_ref_358
	ret_reg &ref[358]
	call_c   Dyam_Create_Atom("reg_alt_unif_cst")
	move_ret ref[358]
	c_ret

c_code local build_seed_70
	ret_reg &seed[70]
	call_c   build_ref_0()
	call_c   build_ref_79()
	call_c   Dyam_Seed_Start(&ref[0],&ref[79],&ref[79],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[70]
	c_ret

;; TERM 79: test_inst(reg_alt_unif(_B, _C), 'Dyam_Reg_Alt_Unify', [c(_B),_C])
c_code local build_ref_79
	ret_reg &ref[79]
	call_c   Dyam_Pseudo_Choice(3)
	call_c   build_ref_360()
	call_c   Dyam_Create_Binary(&ref[360],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_308()
	call_c   Dyam_Create_Unary(&ref[308],V(1))
	move_ret R(1)
	call_c   Dyam_Create_List(V(2),I(0))
	move_ret R(2)
	call_c   Dyam_Create_List(R(1),R(2))
	move_ret R(2)
	call_c   build_ref_321()
	call_c   build_ref_361()
	call_c   Dyam_Term_Start(&ref[321],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[361])
	call_c   Dyam_Term_Arg(R(2))
	call_c   Dyam_Term_End()
	move_ret ref[79]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 361: 'Dyam_Reg_Alt_Unify'
c_code local build_ref_361
	ret_reg &ref[361]
	call_c   Dyam_Create_Atom("Dyam_Reg_Alt_Unify")
	move_ret ref[361]
	c_ret

;; TERM 360: reg_alt_unif
c_code local build_ref_360
	ret_reg &ref[360]
	call_c   Dyam_Create_Atom("reg_alt_unif")
	move_ret ref[360]
	c_ret

c_code local build_seed_71
	ret_reg &seed[71]
	call_c   build_ref_0()
	call_c   build_ref_80()
	call_c   Dyam_Seed_Start(&ref[0],&ref[80],&ref[80],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[71]
	c_ret

;; TERM 80: test_inst(mem_load_output(_B, _C), 'Dyam_Mem_Load_Output', [c(_B),_C])
c_code local build_ref_80
	ret_reg &ref[80]
	call_c   Dyam_Pseudo_Choice(3)
	call_c   build_ref_362()
	call_c   Dyam_Create_Binary(&ref[362],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_308()
	call_c   Dyam_Create_Unary(&ref[308],V(1))
	move_ret R(1)
	call_c   Dyam_Create_List(V(2),I(0))
	move_ret R(2)
	call_c   Dyam_Create_List(R(1),R(2))
	move_ret R(2)
	call_c   build_ref_321()
	call_c   build_ref_363()
	call_c   Dyam_Term_Start(&ref[321],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[363])
	call_c   Dyam_Term_Arg(R(2))
	call_c   Dyam_Term_End()
	move_ret ref[80]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 363: 'Dyam_Mem_Load_Output'
c_code local build_ref_363
	ret_reg &ref[363]
	call_c   Dyam_Create_Atom("Dyam_Mem_Load_Output")
	move_ret ref[363]
	c_ret

;; TERM 362: mem_load_output
c_code local build_ref_362
	ret_reg &ref[362]
	call_c   Dyam_Create_Atom("mem_load_output")
	move_ret ref[362]
	c_ret

c_code local build_seed_72
	ret_reg &seed[72]
	call_c   build_ref_0()
	call_c   build_ref_81()
	call_c   Dyam_Seed_Start(&ref[0],&ref[81],&ref[81],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[72]
	c_ret

;; TERM 81: test_inst(mem_load_input(_B, _C), 'Dyam_Mem_Load_Input', [c(_B),_C])
c_code local build_ref_81
	ret_reg &ref[81]
	call_c   Dyam_Pseudo_Choice(3)
	call_c   build_ref_364()
	call_c   Dyam_Create_Binary(&ref[364],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_308()
	call_c   Dyam_Create_Unary(&ref[308],V(1))
	move_ret R(1)
	call_c   Dyam_Create_List(V(2),I(0))
	move_ret R(2)
	call_c   Dyam_Create_List(R(1),R(2))
	move_ret R(2)
	call_c   build_ref_321()
	call_c   build_ref_365()
	call_c   Dyam_Term_Start(&ref[321],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[365])
	call_c   Dyam_Term_Arg(R(2))
	call_c   Dyam_Term_End()
	move_ret ref[81]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 365: 'Dyam_Mem_Load_Input'
c_code local build_ref_365
	ret_reg &ref[365]
	call_c   Dyam_Create_Atom("Dyam_Mem_Load_Input")
	move_ret ref[365]
	c_ret

;; TERM 364: mem_load_input
c_code local build_ref_364
	ret_reg &ref[364]
	call_c   Dyam_Create_Atom("mem_load_input")
	move_ret ref[364]
	c_ret

c_code local build_seed_73
	ret_reg &seed[73]
	call_c   build_ref_0()
	call_c   build_ref_82()
	call_c   Dyam_Seed_Start(&ref[0],&ref[82],&ref[82],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[73]
	c_ret

;; TERM 82: test_inst(mem_load_string(_B, _C), 'Dyam_Mem_Load_String', [c(_B),_C])
c_code local build_ref_82
	ret_reg &ref[82]
	call_c   Dyam_Pseudo_Choice(3)
	call_c   build_ref_366()
	call_c   Dyam_Create_Binary(&ref[366],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_308()
	call_c   Dyam_Create_Unary(&ref[308],V(1))
	move_ret R(1)
	call_c   Dyam_Create_List(V(2),I(0))
	move_ret R(2)
	call_c   Dyam_Create_List(R(1),R(2))
	move_ret R(2)
	call_c   build_ref_321()
	call_c   build_ref_367()
	call_c   Dyam_Term_Start(&ref[321],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[367])
	call_c   Dyam_Term_Arg(R(2))
	call_c   Dyam_Term_End()
	move_ret ref[82]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 367: 'Dyam_Mem_Load_String'
c_code local build_ref_367
	ret_reg &ref[367]
	call_c   Dyam_Create_Atom("Dyam_Mem_Load_String")
	move_ret ref[367]
	c_ret

;; TERM 366: mem_load_string
c_code local build_ref_366
	ret_reg &ref[366]
	call_c   Dyam_Create_Atom("mem_load_string")
	move_ret ref[366]
	c_ret

c_code local build_seed_74
	ret_reg &seed[74]
	call_c   build_ref_0()
	call_c   build_ref_83()
	call_c   Dyam_Seed_Start(&ref[0],&ref[83],&ref[83],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[74]
	c_ret

;; TERM 83: test_inst(mem_load_char(_B, _C), 'Dyam_Mem_Load_Char', [c(_B),_C])
c_code local build_ref_83
	ret_reg &ref[83]
	call_c   Dyam_Pseudo_Choice(3)
	call_c   build_ref_368()
	call_c   Dyam_Create_Binary(&ref[368],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_308()
	call_c   Dyam_Create_Unary(&ref[308],V(1))
	move_ret R(1)
	call_c   Dyam_Create_List(V(2),I(0))
	move_ret R(2)
	call_c   Dyam_Create_List(R(1),R(2))
	move_ret R(2)
	call_c   build_ref_321()
	call_c   build_ref_369()
	call_c   Dyam_Term_Start(&ref[321],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[369])
	call_c   Dyam_Term_Arg(R(2))
	call_c   Dyam_Term_End()
	move_ret ref[83]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 369: 'Dyam_Mem_Load_Char'
c_code local build_ref_369
	ret_reg &ref[369]
	call_c   Dyam_Create_Atom("Dyam_Mem_Load_Char")
	move_ret ref[369]
	c_ret

;; TERM 368: mem_load_char
c_code local build_ref_368
	ret_reg &ref[368]
	call_c   Dyam_Create_Atom("mem_load_char")
	move_ret ref[368]
	c_ret

c_code local build_seed_75
	ret_reg &seed[75]
	call_c   build_ref_0()
	call_c   build_ref_84()
	call_c   Dyam_Seed_Start(&ref[0],&ref[84],&ref[84],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[75]
	c_ret

;; TERM 84: test_inst(mem_load_boolean(_B, _C), 'Dyam_Mem_Load_Boolean', [c(_B),_C])
c_code local build_ref_84
	ret_reg &ref[84]
	call_c   Dyam_Pseudo_Choice(3)
	call_c   build_ref_370()
	call_c   Dyam_Create_Binary(&ref[370],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_308()
	call_c   Dyam_Create_Unary(&ref[308],V(1))
	move_ret R(1)
	call_c   Dyam_Create_List(V(2),I(0))
	move_ret R(2)
	call_c   Dyam_Create_List(R(1),R(2))
	move_ret R(2)
	call_c   build_ref_321()
	call_c   build_ref_371()
	call_c   Dyam_Term_Start(&ref[321],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[371])
	call_c   Dyam_Term_Arg(R(2))
	call_c   Dyam_Term_End()
	move_ret ref[84]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 371: 'Dyam_Mem_Load_Boolean'
c_code local build_ref_371
	ret_reg &ref[371]
	call_c   Dyam_Create_Atom("Dyam_Mem_Load_Boolean")
	move_ret ref[371]
	c_ret

;; TERM 370: mem_load_boolean
c_code local build_ref_370
	ret_reg &ref[370]
	call_c   Dyam_Create_Atom("mem_load_boolean")
	move_ret ref[370]
	c_ret

c_code local build_seed_76
	ret_reg &seed[76]
	call_c   build_ref_0()
	call_c   build_ref_85()
	call_c   Dyam_Seed_Start(&ref[0],&ref[85],&ref[85],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[76]
	c_ret

;; TERM 85: test_inst(mem_load_ptr(_B, _C), 'Dyam_Mem_Load_Ptr', [c(_B),_C])
c_code local build_ref_85
	ret_reg &ref[85]
	call_c   Dyam_Pseudo_Choice(3)
	call_c   build_ref_372()
	call_c   Dyam_Create_Binary(&ref[372],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_308()
	call_c   Dyam_Create_Unary(&ref[308],V(1))
	move_ret R(1)
	call_c   Dyam_Create_List(V(2),I(0))
	move_ret R(2)
	call_c   Dyam_Create_List(R(1),R(2))
	move_ret R(2)
	call_c   build_ref_321()
	call_c   build_ref_373()
	call_c   Dyam_Term_Start(&ref[321],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[373])
	call_c   Dyam_Term_Arg(R(2))
	call_c   Dyam_Term_End()
	move_ret ref[85]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 373: 'Dyam_Mem_Load_Ptr'
c_code local build_ref_373
	ret_reg &ref[373]
	call_c   Dyam_Create_Atom("Dyam_Mem_Load_Ptr")
	move_ret ref[373]
	c_ret

;; TERM 372: mem_load_ptr
c_code local build_ref_372
	ret_reg &ref[372]
	call_c   Dyam_Create_Atom("mem_load_ptr")
	move_ret ref[372]
	c_ret

c_code local build_seed_77
	ret_reg &seed[77]
	call_c   build_ref_0()
	call_c   build_ref_86()
	call_c   Dyam_Seed_Start(&ref[0],&ref[86],&ref[86],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[77]
	c_ret

;; TERM 86: test_inst(mem_load_float(_B, _C), 'Dyam_Mem_Load_Float', [c(_B),_C])
c_code local build_ref_86
	ret_reg &ref[86]
	call_c   Dyam_Pseudo_Choice(3)
	call_c   build_ref_374()
	call_c   Dyam_Create_Binary(&ref[374],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_308()
	call_c   Dyam_Create_Unary(&ref[308],V(1))
	move_ret R(1)
	call_c   Dyam_Create_List(V(2),I(0))
	move_ret R(2)
	call_c   Dyam_Create_List(R(1),R(2))
	move_ret R(2)
	call_c   build_ref_321()
	call_c   build_ref_375()
	call_c   Dyam_Term_Start(&ref[321],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[375])
	call_c   Dyam_Term_Arg(R(2))
	call_c   Dyam_Term_End()
	move_ret ref[86]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 375: 'Dyam_Mem_Load_Float'
c_code local build_ref_375
	ret_reg &ref[375]
	call_c   Dyam_Create_Atom("Dyam_Mem_Load_Float")
	move_ret ref[375]
	c_ret

;; TERM 374: mem_load_float
c_code local build_ref_374
	ret_reg &ref[374]
	call_c   Dyam_Create_Atom("mem_load_float")
	move_ret ref[374]
	c_ret

c_code local build_seed_78
	ret_reg &seed[78]
	call_c   build_ref_0()
	call_c   build_ref_87()
	call_c   Dyam_Seed_Start(&ref[0],&ref[87],&ref[87],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[78]
	c_ret

;; TERM 87: test_inst(mem_load_number(_B, _C), 'Dyam_Mem_Load_Number', [c(_B),_C])
c_code local build_ref_87
	ret_reg &ref[87]
	call_c   Dyam_Pseudo_Choice(3)
	call_c   build_ref_376()
	call_c   Dyam_Create_Binary(&ref[376],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_308()
	call_c   Dyam_Create_Unary(&ref[308],V(1))
	move_ret R(1)
	call_c   Dyam_Create_List(V(2),I(0))
	move_ret R(2)
	call_c   Dyam_Create_List(R(1),R(2))
	move_ret R(2)
	call_c   build_ref_321()
	call_c   build_ref_377()
	call_c   Dyam_Term_Start(&ref[321],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[377])
	call_c   Dyam_Term_Arg(R(2))
	call_c   Dyam_Term_End()
	move_ret ref[87]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 377: 'Dyam_Mem_Load_Number'
c_code local build_ref_377
	ret_reg &ref[377]
	call_c   Dyam_Create_Atom("Dyam_Mem_Load_Number")
	move_ret ref[377]
	c_ret

;; TERM 376: mem_load_number
c_code local build_ref_376
	ret_reg &ref[376]
	call_c   Dyam_Create_Atom("mem_load_number")
	move_ret ref[376]
	c_ret

c_code local build_seed_79
	ret_reg &seed[79]
	call_c   build_ref_0()
	call_c   build_ref_88()
	call_c   Dyam_Seed_Start(&ref[0],&ref[88],&ref[88],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[79]
	c_ret

;; TERM 88: test_inst(reg_unif_c_output(_B, _C), 'Dyam_Reg_Unify_C_Output', [c(_B),_C])
c_code local build_ref_88
	ret_reg &ref[88]
	call_c   Dyam_Pseudo_Choice(3)
	call_c   build_ref_378()
	call_c   Dyam_Create_Binary(&ref[378],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_308()
	call_c   Dyam_Create_Unary(&ref[308],V(1))
	move_ret R(1)
	call_c   Dyam_Create_List(V(2),I(0))
	move_ret R(2)
	call_c   Dyam_Create_List(R(1),R(2))
	move_ret R(2)
	call_c   build_ref_321()
	call_c   build_ref_379()
	call_c   Dyam_Term_Start(&ref[321],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[379])
	call_c   Dyam_Term_Arg(R(2))
	call_c   Dyam_Term_End()
	move_ret ref[88]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 379: 'Dyam_Reg_Unify_C_Output'
c_code local build_ref_379
	ret_reg &ref[379]
	call_c   Dyam_Create_Atom("Dyam_Reg_Unify_C_Output")
	move_ret ref[379]
	c_ret

;; TERM 378: reg_unif_c_output
c_code local build_ref_378
	ret_reg &ref[378]
	call_c   Dyam_Create_Atom("reg_unif_c_output")
	move_ret ref[378]
	c_ret

c_code local build_seed_80
	ret_reg &seed[80]
	call_c   build_ref_0()
	call_c   build_ref_89()
	call_c   Dyam_Seed_Start(&ref[0],&ref[89],&ref[89],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[80]
	c_ret

;; TERM 89: test_inst(reg_unif_c_input(_B, _C), 'Dyam_Reg_Unify_C_Input', [c(_B),_C])
c_code local build_ref_89
	ret_reg &ref[89]
	call_c   Dyam_Pseudo_Choice(3)
	call_c   build_ref_380()
	call_c   Dyam_Create_Binary(&ref[380],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_308()
	call_c   Dyam_Create_Unary(&ref[308],V(1))
	move_ret R(1)
	call_c   Dyam_Create_List(V(2),I(0))
	move_ret R(2)
	call_c   Dyam_Create_List(R(1),R(2))
	move_ret R(2)
	call_c   build_ref_321()
	call_c   build_ref_381()
	call_c   Dyam_Term_Start(&ref[321],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[381])
	call_c   Dyam_Term_Arg(R(2))
	call_c   Dyam_Term_End()
	move_ret ref[89]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 381: 'Dyam_Reg_Unify_C_Input'
c_code local build_ref_381
	ret_reg &ref[381]
	call_c   Dyam_Create_Atom("Dyam_Reg_Unify_C_Input")
	move_ret ref[381]
	c_ret

;; TERM 380: reg_unif_c_input
c_code local build_ref_380
	ret_reg &ref[380]
	call_c   Dyam_Create_Atom("reg_unif_c_input")
	move_ret ref[380]
	c_ret

c_code local build_seed_81
	ret_reg &seed[81]
	call_c   build_ref_0()
	call_c   build_ref_90()
	call_c   Dyam_Seed_Start(&ref[0],&ref[90],&ref[90],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[81]
	c_ret

;; TERM 90: test_inst(reg_unif_c_string(_B, _C), 'Dyam_Reg_Unify_C_String', [c(_B),_C])
c_code local build_ref_90
	ret_reg &ref[90]
	call_c   Dyam_Pseudo_Choice(3)
	call_c   build_ref_382()
	call_c   Dyam_Create_Binary(&ref[382],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_308()
	call_c   Dyam_Create_Unary(&ref[308],V(1))
	move_ret R(1)
	call_c   Dyam_Create_List(V(2),I(0))
	move_ret R(2)
	call_c   Dyam_Create_List(R(1),R(2))
	move_ret R(2)
	call_c   build_ref_321()
	call_c   build_ref_383()
	call_c   Dyam_Term_Start(&ref[321],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[383])
	call_c   Dyam_Term_Arg(R(2))
	call_c   Dyam_Term_End()
	move_ret ref[90]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 383: 'Dyam_Reg_Unify_C_String'
c_code local build_ref_383
	ret_reg &ref[383]
	call_c   Dyam_Create_Atom("Dyam_Reg_Unify_C_String")
	move_ret ref[383]
	c_ret

;; TERM 382: reg_unif_c_string
c_code local build_ref_382
	ret_reg &ref[382]
	call_c   Dyam_Create_Atom("reg_unif_c_string")
	move_ret ref[382]
	c_ret

c_code local build_seed_82
	ret_reg &seed[82]
	call_c   build_ref_0()
	call_c   build_ref_91()
	call_c   Dyam_Seed_Start(&ref[0],&ref[91],&ref[91],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[82]
	c_ret

;; TERM 91: test_inst(reg_unif_c_char(_B, _C), 'Dyam_Reg_Unify_C_Char', [c(_B),_C])
c_code local build_ref_91
	ret_reg &ref[91]
	call_c   Dyam_Pseudo_Choice(3)
	call_c   build_ref_384()
	call_c   Dyam_Create_Binary(&ref[384],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_308()
	call_c   Dyam_Create_Unary(&ref[308],V(1))
	move_ret R(1)
	call_c   Dyam_Create_List(V(2),I(0))
	move_ret R(2)
	call_c   Dyam_Create_List(R(1),R(2))
	move_ret R(2)
	call_c   build_ref_321()
	call_c   build_ref_385()
	call_c   Dyam_Term_Start(&ref[321],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[385])
	call_c   Dyam_Term_Arg(R(2))
	call_c   Dyam_Term_End()
	move_ret ref[91]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 385: 'Dyam_Reg_Unify_C_Char'
c_code local build_ref_385
	ret_reg &ref[385]
	call_c   Dyam_Create_Atom("Dyam_Reg_Unify_C_Char")
	move_ret ref[385]
	c_ret

;; TERM 384: reg_unif_c_char
c_code local build_ref_384
	ret_reg &ref[384]
	call_c   Dyam_Create_Atom("reg_unif_c_char")
	move_ret ref[384]
	c_ret

c_code local build_seed_83
	ret_reg &seed[83]
	call_c   build_ref_0()
	call_c   build_ref_92()
	call_c   Dyam_Seed_Start(&ref[0],&ref[92],&ref[92],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[83]
	c_ret

;; TERM 92: test_inst(reg_unif_c_boolean(_B, _C), 'Dyam_Reg_Unify_C_Boolean', [c(_B),_C])
c_code local build_ref_92
	ret_reg &ref[92]
	call_c   Dyam_Pseudo_Choice(3)
	call_c   build_ref_386()
	call_c   Dyam_Create_Binary(&ref[386],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_308()
	call_c   Dyam_Create_Unary(&ref[308],V(1))
	move_ret R(1)
	call_c   Dyam_Create_List(V(2),I(0))
	move_ret R(2)
	call_c   Dyam_Create_List(R(1),R(2))
	move_ret R(2)
	call_c   build_ref_321()
	call_c   build_ref_387()
	call_c   Dyam_Term_Start(&ref[321],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[387])
	call_c   Dyam_Term_Arg(R(2))
	call_c   Dyam_Term_End()
	move_ret ref[92]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 387: 'Dyam_Reg_Unify_C_Boolean'
c_code local build_ref_387
	ret_reg &ref[387]
	call_c   Dyam_Create_Atom("Dyam_Reg_Unify_C_Boolean")
	move_ret ref[387]
	c_ret

;; TERM 386: reg_unif_c_boolean
c_code local build_ref_386
	ret_reg &ref[386]
	call_c   Dyam_Create_Atom("reg_unif_c_boolean")
	move_ret ref[386]
	c_ret

c_code local build_seed_84
	ret_reg &seed[84]
	call_c   build_ref_0()
	call_c   build_ref_93()
	call_c   Dyam_Seed_Start(&ref[0],&ref[93],&ref[93],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[84]
	c_ret

;; TERM 93: test_inst(reg_unif_c_ptr(_B, _C), 'Dyam_Reg_Unify_C_Ptr', [c(_B),_C])
c_code local build_ref_93
	ret_reg &ref[93]
	call_c   Dyam_Pseudo_Choice(3)
	call_c   build_ref_388()
	call_c   Dyam_Create_Binary(&ref[388],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_308()
	call_c   Dyam_Create_Unary(&ref[308],V(1))
	move_ret R(1)
	call_c   Dyam_Create_List(V(2),I(0))
	move_ret R(2)
	call_c   Dyam_Create_List(R(1),R(2))
	move_ret R(2)
	call_c   build_ref_321()
	call_c   build_ref_389()
	call_c   Dyam_Term_Start(&ref[321],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[389])
	call_c   Dyam_Term_Arg(R(2))
	call_c   Dyam_Term_End()
	move_ret ref[93]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 389: 'Dyam_Reg_Unify_C_Ptr'
c_code local build_ref_389
	ret_reg &ref[389]
	call_c   Dyam_Create_Atom("Dyam_Reg_Unify_C_Ptr")
	move_ret ref[389]
	c_ret

;; TERM 388: reg_unif_c_ptr
c_code local build_ref_388
	ret_reg &ref[388]
	call_c   Dyam_Create_Atom("reg_unif_c_ptr")
	move_ret ref[388]
	c_ret

c_code local build_seed_85
	ret_reg &seed[85]
	call_c   build_ref_0()
	call_c   build_ref_94()
	call_c   Dyam_Seed_Start(&ref[0],&ref[94],&ref[94],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[85]
	c_ret

;; TERM 94: test_inst(reg_unif_c_float(_B, _C), 'Dyam_Reg_Unify_C_Float', [c(_B),_C])
c_code local build_ref_94
	ret_reg &ref[94]
	call_c   Dyam_Pseudo_Choice(3)
	call_c   build_ref_390()
	call_c   Dyam_Create_Binary(&ref[390],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_308()
	call_c   Dyam_Create_Unary(&ref[308],V(1))
	move_ret R(1)
	call_c   Dyam_Create_List(V(2),I(0))
	move_ret R(2)
	call_c   Dyam_Create_List(R(1),R(2))
	move_ret R(2)
	call_c   build_ref_321()
	call_c   build_ref_391()
	call_c   Dyam_Term_Start(&ref[321],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[391])
	call_c   Dyam_Term_Arg(R(2))
	call_c   Dyam_Term_End()
	move_ret ref[94]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 391: 'Dyam_Reg_Unify_C_Float'
c_code local build_ref_391
	ret_reg &ref[391]
	call_c   Dyam_Create_Atom("Dyam_Reg_Unify_C_Float")
	move_ret ref[391]
	c_ret

;; TERM 390: reg_unif_c_float
c_code local build_ref_390
	ret_reg &ref[390]
	call_c   Dyam_Create_Atom("reg_unif_c_float")
	move_ret ref[390]
	c_ret

c_code local build_seed_86
	ret_reg &seed[86]
	call_c   build_ref_0()
	call_c   build_ref_95()
	call_c   Dyam_Seed_Start(&ref[0],&ref[95],&ref[95],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[86]
	c_ret

;; TERM 95: test_inst(reg_unif_c_number(_B, _C), 'Dyam_Reg_Unify_C_Number', [c(_B),_C])
c_code local build_ref_95
	ret_reg &ref[95]
	call_c   Dyam_Pseudo_Choice(3)
	call_c   build_ref_392()
	call_c   Dyam_Create_Binary(&ref[392],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_308()
	call_c   Dyam_Create_Unary(&ref[308],V(1))
	move_ret R(1)
	call_c   Dyam_Create_List(V(2),I(0))
	move_ret R(2)
	call_c   Dyam_Create_List(R(1),R(2))
	move_ret R(2)
	call_c   build_ref_321()
	call_c   build_ref_393()
	call_c   Dyam_Term_Start(&ref[321],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[393])
	call_c   Dyam_Term_Arg(R(2))
	call_c   Dyam_Term_End()
	move_ret ref[95]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 393: 'Dyam_Reg_Unify_C_Number'
c_code local build_ref_393
	ret_reg &ref[393]
	call_c   Dyam_Create_Atom("Dyam_Reg_Unify_C_Number")
	move_ret ref[393]
	c_ret

;; TERM 392: reg_unif_c_number
c_code local build_ref_392
	ret_reg &ref[392]
	call_c   Dyam_Create_Atom("reg_unif_c_number")
	move_ret ref[392]
	c_ret

c_code local build_seed_87
	ret_reg &seed[87]
	call_c   build_ref_0()
	call_c   build_ref_96()
	call_c   Dyam_Seed_Start(&ref[0],&ref[96],&ref[96],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[87]
	c_ret

;; TERM 96: test_inst(reg_load_output(_B, _C), 'Dyam_Reg_Load_Output', [c(_B),_C])
c_code local build_ref_96
	ret_reg &ref[96]
	call_c   Dyam_Pseudo_Choice(3)
	call_c   build_ref_394()
	call_c   Dyam_Create_Binary(&ref[394],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_308()
	call_c   Dyam_Create_Unary(&ref[308],V(1))
	move_ret R(1)
	call_c   Dyam_Create_List(V(2),I(0))
	move_ret R(2)
	call_c   Dyam_Create_List(R(1),R(2))
	move_ret R(2)
	call_c   build_ref_321()
	call_c   build_ref_395()
	call_c   Dyam_Term_Start(&ref[321],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[395])
	call_c   Dyam_Term_Arg(R(2))
	call_c   Dyam_Term_End()
	move_ret ref[96]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 395: 'Dyam_Reg_Load_Output'
c_code local build_ref_395
	ret_reg &ref[395]
	call_c   Dyam_Create_Atom("Dyam_Reg_Load_Output")
	move_ret ref[395]
	c_ret

;; TERM 394: reg_load_output
c_code local build_ref_394
	ret_reg &ref[394]
	call_c   Dyam_Create_Atom("reg_load_output")
	move_ret ref[394]
	c_ret

c_code local build_seed_88
	ret_reg &seed[88]
	call_c   build_ref_0()
	call_c   build_ref_97()
	call_c   Dyam_Seed_Start(&ref[0],&ref[97],&ref[97],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[88]
	c_ret

;; TERM 97: test_inst(reg_load_input(_B, _C), 'Dyam_Reg_Load_Input', [c(_B),_C])
c_code local build_ref_97
	ret_reg &ref[97]
	call_c   Dyam_Pseudo_Choice(3)
	call_c   build_ref_396()
	call_c   Dyam_Create_Binary(&ref[396],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_308()
	call_c   Dyam_Create_Unary(&ref[308],V(1))
	move_ret R(1)
	call_c   Dyam_Create_List(V(2),I(0))
	move_ret R(2)
	call_c   Dyam_Create_List(R(1),R(2))
	move_ret R(2)
	call_c   build_ref_321()
	call_c   build_ref_397()
	call_c   Dyam_Term_Start(&ref[321],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[397])
	call_c   Dyam_Term_Arg(R(2))
	call_c   Dyam_Term_End()
	move_ret ref[97]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 397: 'Dyam_Reg_Load_Input'
c_code local build_ref_397
	ret_reg &ref[397]
	call_c   Dyam_Create_Atom("Dyam_Reg_Load_Input")
	move_ret ref[397]
	c_ret

;; TERM 396: reg_load_input
c_code local build_ref_396
	ret_reg &ref[396]
	call_c   Dyam_Create_Atom("reg_load_input")
	move_ret ref[396]
	c_ret

c_code local build_seed_89
	ret_reg &seed[89]
	call_c   build_ref_0()
	call_c   build_ref_98()
	call_c   Dyam_Seed_Start(&ref[0],&ref[98],&ref[98],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[89]
	c_ret

;; TERM 98: test_inst(reg_load_string(_B, _C), 'Dyam_Reg_Load_String', [c(_B),_C])
c_code local build_ref_98
	ret_reg &ref[98]
	call_c   Dyam_Pseudo_Choice(3)
	call_c   build_ref_398()
	call_c   Dyam_Create_Binary(&ref[398],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_308()
	call_c   Dyam_Create_Unary(&ref[308],V(1))
	move_ret R(1)
	call_c   Dyam_Create_List(V(2),I(0))
	move_ret R(2)
	call_c   Dyam_Create_List(R(1),R(2))
	move_ret R(2)
	call_c   build_ref_321()
	call_c   build_ref_399()
	call_c   Dyam_Term_Start(&ref[321],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[399])
	call_c   Dyam_Term_Arg(R(2))
	call_c   Dyam_Term_End()
	move_ret ref[98]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 399: 'Dyam_Reg_Load_String'
c_code local build_ref_399
	ret_reg &ref[399]
	call_c   Dyam_Create_Atom("Dyam_Reg_Load_String")
	move_ret ref[399]
	c_ret

;; TERM 398: reg_load_string
c_code local build_ref_398
	ret_reg &ref[398]
	call_c   Dyam_Create_Atom("reg_load_string")
	move_ret ref[398]
	c_ret

c_code local build_seed_90
	ret_reg &seed[90]
	call_c   build_ref_0()
	call_c   build_ref_99()
	call_c   Dyam_Seed_Start(&ref[0],&ref[99],&ref[99],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[90]
	c_ret

;; TERM 99: test_inst(reg_load_char(_B, _C), 'Dyam_Reg_Load_Char', [c(_B),_C])
c_code local build_ref_99
	ret_reg &ref[99]
	call_c   Dyam_Pseudo_Choice(3)
	call_c   build_ref_400()
	call_c   Dyam_Create_Binary(&ref[400],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_308()
	call_c   Dyam_Create_Unary(&ref[308],V(1))
	move_ret R(1)
	call_c   Dyam_Create_List(V(2),I(0))
	move_ret R(2)
	call_c   Dyam_Create_List(R(1),R(2))
	move_ret R(2)
	call_c   build_ref_321()
	call_c   build_ref_401()
	call_c   Dyam_Term_Start(&ref[321],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[401])
	call_c   Dyam_Term_Arg(R(2))
	call_c   Dyam_Term_End()
	move_ret ref[99]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 401: 'Dyam_Reg_Load_Char'
c_code local build_ref_401
	ret_reg &ref[401]
	call_c   Dyam_Create_Atom("Dyam_Reg_Load_Char")
	move_ret ref[401]
	c_ret

;; TERM 400: reg_load_char
c_code local build_ref_400
	ret_reg &ref[400]
	call_c   Dyam_Create_Atom("reg_load_char")
	move_ret ref[400]
	c_ret

c_code local build_seed_91
	ret_reg &seed[91]
	call_c   build_ref_0()
	call_c   build_ref_100()
	call_c   Dyam_Seed_Start(&ref[0],&ref[100],&ref[100],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[91]
	c_ret

;; TERM 100: test_inst(reg_load_boolean(_B, _C), 'Dyam_Reg_Load_Boolean', [c(_B),_C])
c_code local build_ref_100
	ret_reg &ref[100]
	call_c   Dyam_Pseudo_Choice(3)
	call_c   build_ref_402()
	call_c   Dyam_Create_Binary(&ref[402],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_308()
	call_c   Dyam_Create_Unary(&ref[308],V(1))
	move_ret R(1)
	call_c   Dyam_Create_List(V(2),I(0))
	move_ret R(2)
	call_c   Dyam_Create_List(R(1),R(2))
	move_ret R(2)
	call_c   build_ref_321()
	call_c   build_ref_403()
	call_c   Dyam_Term_Start(&ref[321],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[403])
	call_c   Dyam_Term_Arg(R(2))
	call_c   Dyam_Term_End()
	move_ret ref[100]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 403: 'Dyam_Reg_Load_Boolean'
c_code local build_ref_403
	ret_reg &ref[403]
	call_c   Dyam_Create_Atom("Dyam_Reg_Load_Boolean")
	move_ret ref[403]
	c_ret

;; TERM 402: reg_load_boolean
c_code local build_ref_402
	ret_reg &ref[402]
	call_c   Dyam_Create_Atom("reg_load_boolean")
	move_ret ref[402]
	c_ret

c_code local build_seed_92
	ret_reg &seed[92]
	call_c   build_ref_0()
	call_c   build_ref_101()
	call_c   Dyam_Seed_Start(&ref[0],&ref[101],&ref[101],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[92]
	c_ret

;; TERM 101: test_inst(reg_load_ptr(_B, _C), 'Dyam_Reg_Load_Ptr', [c(_B),_C])
c_code local build_ref_101
	ret_reg &ref[101]
	call_c   Dyam_Pseudo_Choice(3)
	call_c   build_ref_404()
	call_c   Dyam_Create_Binary(&ref[404],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_308()
	call_c   Dyam_Create_Unary(&ref[308],V(1))
	move_ret R(1)
	call_c   Dyam_Create_List(V(2),I(0))
	move_ret R(2)
	call_c   Dyam_Create_List(R(1),R(2))
	move_ret R(2)
	call_c   build_ref_321()
	call_c   build_ref_405()
	call_c   Dyam_Term_Start(&ref[321],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[405])
	call_c   Dyam_Term_Arg(R(2))
	call_c   Dyam_Term_End()
	move_ret ref[101]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 405: 'Dyam_Reg_Load_Ptr'
c_code local build_ref_405
	ret_reg &ref[405]
	call_c   Dyam_Create_Atom("Dyam_Reg_Load_Ptr")
	move_ret ref[405]
	c_ret

;; TERM 404: reg_load_ptr
c_code local build_ref_404
	ret_reg &ref[404]
	call_c   Dyam_Create_Atom("reg_load_ptr")
	move_ret ref[404]
	c_ret

c_code local build_seed_93
	ret_reg &seed[93]
	call_c   build_ref_0()
	call_c   build_ref_102()
	call_c   Dyam_Seed_Start(&ref[0],&ref[102],&ref[102],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[93]
	c_ret

;; TERM 102: test_inst(reg_load_float(_B, _C), 'Dyam_Reg_Load_Float', [c(_B),_C])
c_code local build_ref_102
	ret_reg &ref[102]
	call_c   Dyam_Pseudo_Choice(3)
	call_c   build_ref_406()
	call_c   Dyam_Create_Binary(&ref[406],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_308()
	call_c   Dyam_Create_Unary(&ref[308],V(1))
	move_ret R(1)
	call_c   Dyam_Create_List(V(2),I(0))
	move_ret R(2)
	call_c   Dyam_Create_List(R(1),R(2))
	move_ret R(2)
	call_c   build_ref_321()
	call_c   build_ref_407()
	call_c   Dyam_Term_Start(&ref[321],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[407])
	call_c   Dyam_Term_Arg(R(2))
	call_c   Dyam_Term_End()
	move_ret ref[102]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 407: 'Dyam_Reg_Load_Float'
c_code local build_ref_407
	ret_reg &ref[407]
	call_c   Dyam_Create_Atom("Dyam_Reg_Load_Float")
	move_ret ref[407]
	c_ret

;; TERM 406: reg_load_float
c_code local build_ref_406
	ret_reg &ref[406]
	call_c   Dyam_Create_Atom("reg_load_float")
	move_ret ref[406]
	c_ret

c_code local build_seed_94
	ret_reg &seed[94]
	call_c   build_ref_0()
	call_c   build_ref_103()
	call_c   Dyam_Seed_Start(&ref[0],&ref[103],&ref[103],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[94]
	c_ret

;; TERM 103: test_inst(reg_load_number(_B, _C), 'Dyam_Reg_Load_Number', [c(_B),_C])
c_code local build_ref_103
	ret_reg &ref[103]
	call_c   Dyam_Pseudo_Choice(3)
	call_c   build_ref_408()
	call_c   Dyam_Create_Binary(&ref[408],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_308()
	call_c   Dyam_Create_Unary(&ref[308],V(1))
	move_ret R(1)
	call_c   Dyam_Create_List(V(2),I(0))
	move_ret R(2)
	call_c   Dyam_Create_List(R(1),R(2))
	move_ret R(2)
	call_c   build_ref_321()
	call_c   build_ref_409()
	call_c   Dyam_Term_Start(&ref[321],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[409])
	call_c   Dyam_Term_Arg(R(2))
	call_c   Dyam_Term_End()
	move_ret ref[103]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 409: 'Dyam_Reg_Load_Number'
c_code local build_ref_409
	ret_reg &ref[409]
	call_c   Dyam_Create_Atom("Dyam_Reg_Load_Number")
	move_ret ref[409]
	c_ret

;; TERM 408: reg_load_number
c_code local build_ref_408
	ret_reg &ref[408]
	call_c   Dyam_Create_Atom("reg_load_number")
	move_ret ref[408]
	c_ret

c_code local build_seed_95
	ret_reg &seed[95]
	call_c   build_ref_0()
	call_c   build_ref_104()
	call_c   Dyam_Seed_Start(&ref[0],&ref[104],&ref[104],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[95]
	c_ret

;; TERM 104: test_inst(reg_unif_cst(_B, _C), 'Dyam_Reg_Unify_Cst', [c(_B),_C])
c_code local build_ref_104
	ret_reg &ref[104]
	call_c   Dyam_Pseudo_Choice(3)
	call_c   build_ref_410()
	call_c   Dyam_Create_Binary(&ref[410],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_308()
	call_c   Dyam_Create_Unary(&ref[308],V(1))
	move_ret R(1)
	call_c   Dyam_Create_List(V(2),I(0))
	move_ret R(2)
	call_c   Dyam_Create_List(R(1),R(2))
	move_ret R(2)
	call_c   build_ref_321()
	call_c   build_ref_411()
	call_c   Dyam_Term_Start(&ref[321],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[411])
	call_c   Dyam_Term_Arg(R(2))
	call_c   Dyam_Term_End()
	move_ret ref[104]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 411: 'Dyam_Reg_Unify_Cst'
c_code local build_ref_411
	ret_reg &ref[411]
	call_c   Dyam_Create_Atom("Dyam_Reg_Unify_Cst")
	move_ret ref[411]
	c_ret

;; TERM 410: reg_unif_cst
c_code local build_ref_410
	ret_reg &ref[410]
	call_c   Dyam_Create_Atom("reg_unif_cst")
	move_ret ref[410]
	c_ret

c_code local build_seed_96
	ret_reg &seed[96]
	call_c   build_ref_0()
	call_c   build_ref_105()
	call_c   Dyam_Seed_Start(&ref[0],&ref[105],&ref[105],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[96]
	c_ret

;; TERM 105: test_inst(reg_unif(_B, _C), 'Dyam_Reg_Unify', [c(_B),_C])
c_code local build_ref_105
	ret_reg &ref[105]
	call_c   Dyam_Pseudo_Choice(3)
	call_c   build_ref_412()
	call_c   Dyam_Create_Binary(&ref[412],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_308()
	call_c   Dyam_Create_Unary(&ref[308],V(1))
	move_ret R(1)
	call_c   Dyam_Create_List(V(2),I(0))
	move_ret R(2)
	call_c   Dyam_Create_List(R(1),R(2))
	move_ret R(2)
	call_c   build_ref_321()
	call_c   build_ref_413()
	call_c   Dyam_Term_Start(&ref[321],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[413])
	call_c   Dyam_Term_Arg(R(2))
	call_c   Dyam_Term_End()
	move_ret ref[105]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 413: 'Dyam_Reg_Unify'
c_code local build_ref_413
	ret_reg &ref[413]
	call_c   Dyam_Create_Atom("Dyam_Reg_Unify")
	move_ret ref[413]
	c_ret

;; TERM 412: reg_unif
c_code local build_ref_412
	ret_reg &ref[412]
	call_c   Dyam_Create_Atom("reg_unif")
	move_ret ref[412]
	c_ret

c_code local build_seed_97
	ret_reg &seed[97]
	call_c   build_ref_0()
	call_c   build_ref_106()
	call_c   Dyam_Seed_Start(&ref[0],&ref[106],&ref[106],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[97]
	c_ret

;; TERM 106: stop_inst((call(_B, _C) :> succeed), pl_jump(_B, _C), _B, 1)
c_code local build_ref_106
	ret_reg &ref[106]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_414()
	call_c   Dyam_Create_Binary(&ref[414],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_143()
	call_c   Dyam_Create_Binary(I(9),R(0),&ref[143])
	move_ret R(0)
	call_c   build_ref_415()
	call_c   Dyam_Create_Binary(&ref[415],V(1),V(2))
	move_ret R(1)
	call_c   build_ref_309()
	call_c   Dyam_Term_Start(&ref[309],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(N(1))
	call_c   Dyam_Term_End()
	move_ret ref[106]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 415: pl_jump
c_code local build_ref_415
	ret_reg &ref[415]
	call_c   Dyam_Create_Atom("pl_jump")
	move_ret ref[415]
	c_ret

;; TERM 414: call
c_code local build_ref_414
	ret_reg &ref[414]
	call_c   Dyam_Create_Atom("call")
	move_ret ref[414]
	c_ret

c_code local build_seed_98
	ret_reg &seed[98]
	call_c   build_ref_0()
	call_c   build_ref_112()
	call_c   Dyam_Seed_Start(&ref[0],&ref[112],&ref[112],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[98]
	c_ret

;; TERM 112: inst(reg_multi_bind_zero(_B, _C), 'Dyam_Multi_Reg_Bind_Zero', [c(_B),c(_C)])
c_code local build_ref_112
	ret_reg &ref[112]
	call_c   Dyam_Pseudo_Choice(3)
	call_c   build_ref_416()
	call_c   Dyam_Create_Binary(&ref[416],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_308()
	call_c   Dyam_Create_Unary(&ref[308],V(1))
	move_ret R(1)
	call_c   Dyam_Create_Unary(&ref[308],V(2))
	move_ret R(2)
	call_c   Dyam_Create_List(R(2),I(0))
	move_ret R(2)
	call_c   Dyam_Create_List(R(1),R(2))
	move_ret R(2)
	call_c   build_ref_277()
	call_c   build_ref_417()
	call_c   Dyam_Term_Start(&ref[277],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[417])
	call_c   Dyam_Term_Arg(R(2))
	call_c   Dyam_Term_End()
	move_ret ref[112]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 417: 'Dyam_Multi_Reg_Bind_Zero'
c_code local build_ref_417
	ret_reg &ref[417]
	call_c   Dyam_Create_Atom("Dyam_Multi_Reg_Bind_Zero")
	move_ret ref[417]
	c_ret

;; TERM 416: reg_multi_bind_zero
c_code local build_ref_416
	ret_reg &ref[416]
	call_c   Dyam_Create_Atom("reg_multi_bind_zero")
	move_ret ref[416]
	c_ret

c_code local build_seed_99
	ret_reg &seed[99]
	call_c   build_ref_0()
	call_c   build_ref_113()
	call_c   Dyam_Seed_Start(&ref[0],&ref[113],&ref[113],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[99]
	c_ret

;; TERM 113: pl_inst(completion(_B, _C), 'Complete', [c(_B),c(_C)])
c_code local build_ref_113
	ret_reg &ref[113]
	call_c   Dyam_Pseudo_Choice(3)
	call_c   build_ref_419()
	call_c   Dyam_Create_Binary(&ref[419],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_308()
	call_c   Dyam_Create_Unary(&ref[308],V(1))
	move_ret R(1)
	call_c   Dyam_Create_Unary(&ref[308],V(2))
	move_ret R(2)
	call_c   Dyam_Create_List(R(2),I(0))
	move_ret R(2)
	call_c   Dyam_Create_List(R(1),R(2))
	move_ret R(2)
	call_c   build_ref_418()
	call_c   build_ref_420()
	call_c   Dyam_Term_Start(&ref[418],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[420])
	call_c   Dyam_Term_Arg(R(2))
	call_c   Dyam_Term_End()
	move_ret ref[113]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 420: 'Complete'
c_code local build_ref_420
	ret_reg &ref[420]
	call_c   Dyam_Create_Atom("Complete")
	move_ret ref[420]
	c_ret

;; TERM 418: pl_inst
c_code local build_ref_418
	ret_reg &ref[418]
	call_c   Dyam_Create_Atom("pl_inst")
	move_ret ref[418]
	c_ret

;; TERM 419: completion
c_code local build_ref_419
	ret_reg &ref[419]
	call_c   Dyam_Create_Atom("completion")
	move_ret ref[419]
	c_ret

c_code local build_seed_100
	ret_reg &seed[100]
	call_c   build_ref_0()
	call_c   build_ref_114()
	call_c   Dyam_Seed_Start(&ref[0],&ref[114],&ref[114],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[100]
	c_ret

;; TERM 114: pl_inst(direct(_B, _C), 'Apply', [c(_B),c(_C)])
c_code local build_ref_114
	ret_reg &ref[114]
	call_c   Dyam_Pseudo_Choice(3)
	call_c   build_ref_421()
	call_c   Dyam_Create_Binary(&ref[421],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_308()
	call_c   Dyam_Create_Unary(&ref[308],V(1))
	move_ret R(1)
	call_c   Dyam_Create_Unary(&ref[308],V(2))
	move_ret R(2)
	call_c   Dyam_Create_List(R(2),I(0))
	move_ret R(2)
	call_c   Dyam_Create_List(R(1),R(2))
	move_ret R(2)
	call_c   build_ref_418()
	call_c   build_ref_422()
	call_c   Dyam_Term_Start(&ref[418],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[422])
	call_c   Dyam_Term_Arg(R(2))
	call_c   Dyam_Term_End()
	move_ret ref[114]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 422: 'Apply'
c_code local build_ref_422
	ret_reg &ref[422]
	call_c   Dyam_Create_Atom("Apply")
	move_ret ref[422]
	c_ret

;; TERM 421: direct
c_code local build_ref_421
	ret_reg &ref[421]
	call_c   Dyam_Create_Atom("direct")
	move_ret ref[421]
	c_ret

c_code local build_seed_101
	ret_reg &seed[101]
	call_c   build_ref_0()
	call_c   build_ref_115()
	call_c   Dyam_Seed_Start(&ref[0],&ref[115],&ref[115],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[101]
	c_ret

;; TERM 115: stop_inst((pl_jump(_B, _C) :> _D), pl_jump(_B, _C), _B, 1)
c_code local build_ref_115
	ret_reg &ref[115]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_415()
	call_c   Dyam_Create_Binary(&ref[415],V(1),V(2))
	move_ret R(0)
	call_c   Dyam_Create_Binary(I(9),R(0),V(3))
	move_ret R(1)
	call_c   build_ref_309()
	call_c   Dyam_Term_Start(&ref[309],4)
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(N(1))
	call_c   Dyam_Term_End()
	move_ret ref[115]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_102
	ret_reg &seed[102]
	call_c   build_ref_0()
	call_c   build_ref_121()
	call_c   Dyam_Seed_Start(&ref[0],&ref[121],&ref[121],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[102]
	c_ret

;; TERM 121: stop_inst((reg_reset(_B) :> noop :> deallocate :> succeed), (reg_deallocate(_B) :> succeed), [], 2)
c_code local build_ref_121
	ret_reg &ref[121]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_423()
	call_c   Dyam_Create_Unary(&ref[423],V(1))
	move_ret R(0)
	call_c   build_ref_136()
	call_c   build_ref_144()
	call_c   Dyam_Create_Binary(I(9),&ref[136],&ref[144])
	move_ret R(1)
	call_c   Dyam_Create_Binary(I(9),R(0),R(1))
	move_ret R(1)
	call_c   build_ref_329()
	call_c   Dyam_Create_Unary(&ref[329],V(1))
	move_ret R(0)
	call_c   build_ref_143()
	call_c   Dyam_Create_Binary(I(9),R(0),&ref[143])
	move_ret R(0)
	call_c   build_ref_309()
	call_c   Dyam_Term_Start(&ref[309],4)
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(N(2))
	call_c   Dyam_Term_End()
	move_ret ref[121]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 144: deallocate :> succeed
c_code local build_ref_144
	ret_reg &ref[144]
	call_c   build_ref_142()
	call_c   build_ref_143()
	call_c   Dyam_Create_Binary(I(9),&ref[142],&ref[143])
	move_ret ref[144]
	c_ret

;; TERM 136: noop
c_code local build_ref_136
	ret_reg &ref[136]
	call_c   Dyam_Create_Atom("noop")
	move_ret ref[136]
	c_ret

;; TERM 423: reg_reset
c_code local build_ref_423
	ret_reg &ref[423]
	call_c   Dyam_Create_Atom("reg_reset")
	move_ret ref[423]
	c_ret

c_code local build_seed_103
	ret_reg &seed[103]
	call_c   build_ref_0()
	call_c   build_ref_122()
	call_c   Dyam_Seed_Start(&ref[0],&ref[122],&ref[122],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[103]
	c_ret

;; TERM 122: stop_inst((reg_reset(_B) :> deallocate_layer :> deallocate :> succeed), (reg_deallocate(_B) :> succeed), [], 2)
c_code local build_ref_122
	ret_reg &ref[122]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_423()
	call_c   Dyam_Create_Unary(&ref[423],V(1))
	move_ret R(0)
	call_c   build_ref_424()
	call_c   build_ref_144()
	call_c   Dyam_Create_Binary(I(9),&ref[424],&ref[144])
	move_ret R(1)
	call_c   Dyam_Create_Binary(I(9),R(0),R(1))
	move_ret R(1)
	call_c   build_ref_329()
	call_c   Dyam_Create_Unary(&ref[329],V(1))
	move_ret R(0)
	call_c   build_ref_143()
	call_c   Dyam_Create_Binary(I(9),R(0),&ref[143])
	move_ret R(0)
	call_c   build_ref_309()
	call_c   Dyam_Term_Start(&ref[309],4)
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(N(2))
	call_c   Dyam_Term_End()
	move_ret ref[122]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 424: deallocate_layer
c_code local build_ref_424
	ret_reg &ref[424]
	call_c   Dyam_Create_Atom("deallocate_layer")
	move_ret ref[424]
	c_ret

c_code local build_seed_104
	ret_reg &seed[104]
	call_c   build_ref_0()
	call_c   build_ref_130()
	call_c   Dyam_Seed_Start(&ref[0],&ref[130],&ref[130],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[104]
	c_ret

;; TERM 130: stop_inst((call(_B, _C) :> deallocate :> succeed), (deallocate :> pl_jump(_B, _C)), _B, 2)
c_code local build_ref_130
	ret_reg &ref[130]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_414()
	call_c   Dyam_Create_Binary(&ref[414],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_144()
	call_c   Dyam_Create_Binary(I(9),R(0),&ref[144])
	move_ret R(0)
	call_c   build_ref_415()
	call_c   Dyam_Create_Binary(&ref[415],V(1),V(2))
	move_ret R(1)
	call_c   build_ref_142()
	call_c   Dyam_Create_Binary(I(9),&ref[142],R(1))
	move_ret R(1)
	call_c   build_ref_309()
	call_c   Dyam_Term_Start(&ref[309],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(N(2))
	call_c   Dyam_Term_End()
	move_ret ref[130]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_105
	ret_reg &seed[105]
	call_c   build_ref_0()
	call_c   build_ref_167()
	call_c   Dyam_Seed_Start(&ref[0],&ref[167],&ref[167],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[105]
	c_ret

;; TERM 167: inst(reg_multi_bind(_B, _C, _D), 'Dyam_Multi_Reg_Bind', [c(_B),c(_C),c(_D)])
c_code local build_ref_167
	ret_reg &ref[167]
	call_c   Dyam_Pseudo_Choice(4)
	call_c   build_ref_425()
	call_c   Dyam_Term_Start(&ref[425],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_308()
	call_c   Dyam_Create_Unary(&ref[308],V(1))
	move_ret R(1)
	call_c   Dyam_Create_Unary(&ref[308],V(2))
	move_ret R(2)
	call_c   Dyam_Create_Unary(&ref[308],V(3))
	move_ret R(3)
	call_c   Dyam_Create_List(R(3),I(0))
	move_ret R(3)
	call_c   Dyam_Create_List(R(2),R(3))
	move_ret R(3)
	call_c   Dyam_Create_List(R(1),R(3))
	move_ret R(3)
	call_c   build_ref_277()
	call_c   build_ref_426()
	call_c   Dyam_Term_Start(&ref[277],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[426])
	call_c   Dyam_Term_Arg(R(3))
	call_c   Dyam_Term_End()
	move_ret ref[167]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 426: 'Dyam_Multi_Reg_Bind'
c_code local build_ref_426
	ret_reg &ref[426]
	call_c   Dyam_Create_Atom("Dyam_Multi_Reg_Bind")
	move_ret ref[426]
	c_ret

;; TERM 425: reg_multi_bind
c_code local build_ref_425
	ret_reg &ref[425]
	call_c   Dyam_Create_Atom("reg_multi_bind")
	move_ret ref[425]
	c_ret

c_code local build_seed_106
	ret_reg &seed[106]
	call_c   build_ref_0()
	call_c   build_ref_168()
	call_c   Dyam_Seed_Start(&ref[0],&ref[168],&ref[168],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[106]
	c_ret

;; TERM 168: test_inst(unify(_B, _C, _D, _E), sfol_unify, [_B,_C,_D,_E])
c_code local build_ref_168
	ret_reg &ref[168]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_304()
	call_c   Dyam_Term_Start(&ref[304],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   Dyam_Create_Tupple(1,4,I(0))
	move_ret R(1)
	call_c   build_ref_321()
	call_c   build_ref_427()
	call_c   Dyam_Term_Start(&ref[321],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[427])
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_End()
	move_ret ref[168]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 427: sfol_unify
c_code local build_ref_427
	ret_reg &ref[427]
	call_c   Dyam_Create_Atom("sfol_unify")
	move_ret ref[427]
	c_ret

c_code local build_seed_1
	ret_reg &seed[1]
	call_c   build_ref_16()
	call_c   build_ref_20()
	call_c   Dyam_Seed_Start(&ref[16],&ref[20],I(0),fun0,1)
	call_c   build_ref_19()
	call_c   Dyam_Seed_Add_Comp(&ref[19],fun1,0)
	call_c   Dyam_Seed_End()
	move_ret seed[1]
	c_ret

;; TERM 19: '*GUARD*'(special_inst(deallocate_layer))
c_code local build_ref_19
	ret_reg &ref[19]
	call_c   build_ref_17()
	call_c   build_ref_18()
	call_c   Dyam_Create_Unary(&ref[17],&ref[18])
	move_ret ref[19]
	c_ret

;; TERM 18: special_inst(deallocate_layer)
c_code local build_ref_18
	ret_reg &ref[18]
	call_c   build_ref_428()
	call_c   build_ref_424()
	call_c   Dyam_Create_Unary(&ref[428],&ref[424])
	move_ret ref[18]
	c_ret

;; TERM 428: special_inst
c_code local build_ref_428
	ret_reg &ref[428]
	call_c   Dyam_Create_Atom("special_inst")
	move_ret ref[428]
	c_ret

;; TERM 17: '*GUARD*'
c_code local build_ref_17
	ret_reg &ref[17]
	call_c   Dyam_Create_Atom("*GUARD*")
	move_ret ref[17]
	c_ret

pl_code local fun1
	call_c   build_ref_19()
	call_c   Dyam_Unify_Item(&ref[19])
	fail_ret
	pl_ret

;; TERM 20: '*GUARD*'(special_inst(deallocate_layer)) :> []
c_code local build_ref_20
	ret_reg &ref[20]
	call_c   build_ref_19()
	call_c   Dyam_Create_Binary(I(9),&ref[19],I(0))
	move_ret ref[20]
	c_ret

;; TERM 16: '*GUARD_CLAUSE*'
c_code local build_ref_16
	ret_reg &ref[16]
	call_c   Dyam_Create_Atom("*GUARD_CLAUSE*")
	move_ret ref[16]
	c_ret

c_code local build_seed_0
	ret_reg &seed[0]
	call_c   build_ref_16()
	call_c   build_ref_27()
	call_c   Dyam_Seed_Start(&ref[16],&ref[27],I(0),fun0,1)
	call_c   build_ref_26()
	call_c   Dyam_Seed_Add_Comp(&ref[26],fun3,0)
	call_c   Dyam_Seed_End()
	move_ret seed[0]
	c_ret

;; TERM 26: '*GUARD*'(special_inst(reg_reset(_B)))
c_code local build_ref_26
	ret_reg &ref[26]
	call_c   build_ref_17()
	call_c   build_ref_25()
	call_c   Dyam_Create_Unary(&ref[17],&ref[25])
	move_ret ref[26]
	c_ret

;; TERM 25: special_inst(reg_reset(_B))
c_code local build_ref_25
	ret_reg &ref[25]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_423()
	call_c   Dyam_Create_Unary(&ref[423],V(1))
	move_ret R(0)
	call_c   build_ref_428()
	call_c   Dyam_Create_Unary(&ref[428],R(0))
	move_ret ref[25]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

pl_code local fun3
	call_c   build_ref_26()
	call_c   Dyam_Unify_Item(&ref[26])
	fail_ret
	pl_ret

;; TERM 27: '*GUARD*'(special_inst(reg_reset(_B))) :> []
c_code local build_ref_27
	ret_reg &ref[27]
	call_c   build_ref_26()
	call_c   Dyam_Create_Binary(I(9),&ref[26],I(0))
	move_ret ref[27]
	c_ret

c_code local build_seed_2
	ret_reg &seed[2]
	call_c   build_ref_16()
	call_c   build_ref_31()
	call_c   Dyam_Seed_Start(&ref[16],&ref[31],I(0),fun0,1)
	call_c   build_ref_30()
	call_c   Dyam_Seed_Add_Comp(&ref[30],fun4,0)
	call_c   Dyam_Seed_End()
	move_ret seed[2]
	c_ret

;; TERM 30: '*GUARD*'(decompose_refs([], [], []))
c_code local build_ref_30
	ret_reg &ref[30]
	call_c   build_ref_17()
	call_c   build_ref_29()
	call_c   Dyam_Create_Unary(&ref[17],&ref[29])
	move_ret ref[30]
	c_ret

;; TERM 29: decompose_refs([], [], [])
c_code local build_ref_29
	ret_reg &ref[29]
	call_c   build_ref_429()
	call_c   Dyam_Term_Start(&ref[429],3)
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_End()
	move_ret ref[29]
	c_ret

;; TERM 429: decompose_refs
c_code local build_ref_429
	ret_reg &ref[429]
	call_c   Dyam_Create_Atom("decompose_refs")
	move_ret ref[429]
	c_ret

pl_code local fun4
	call_c   build_ref_30()
	call_c   Dyam_Unify_Item(&ref[30])
	fail_ret
	pl_ret

;; TERM 31: '*GUARD*'(decompose_refs([], [], [])) :> []
c_code local build_ref_31
	ret_reg &ref[31]
	call_c   build_ref_30()
	call_c   Dyam_Create_Binary(I(9),&ref[30],I(0))
	move_ret ref[31]
	c_ret

c_code local build_seed_11
	ret_reg &seed[11]
	call_c   build_ref_16()
	call_c   build_ref_45()
	call_c   Dyam_Seed_Start(&ref[16],&ref[45],I(0),fun0,1)
	call_c   build_ref_44()
	call_c   Dyam_Seed_Add_Comp(&ref[44],fun5,0)
	call_c   Dyam_Seed_End()
	move_ret seed[11]
	c_ret

;; TERM 44: '*GUARD*'(special_inst(fail))
c_code local build_ref_44
	ret_reg &ref[44]
	call_c   build_ref_17()
	call_c   build_ref_43()
	call_c   Dyam_Create_Unary(&ref[17],&ref[43])
	move_ret ref[44]
	c_ret

;; TERM 43: special_inst(fail)
c_code local build_ref_43
	ret_reg &ref[43]
	call_c   build_ref_428()
	call_c   build_ref_161()
	call_c   Dyam_Create_Unary(&ref[428],&ref[161])
	move_ret ref[43]
	c_ret

pl_code local fun5
	call_c   build_ref_44()
	call_c   Dyam_Unify_Item(&ref[44])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Deallocate(0)
	pl_jump  pred_ma_emit_pl_fail_0()

;; TERM 45: '*GUARD*'(special_inst(fail)) :> []
c_code local build_ref_45
	ret_reg &ref[45]
	call_c   build_ref_44()
	call_c   Dyam_Create_Binary(I(9),&ref[44],I(0))
	move_ret ref[45]
	c_ret

c_code local build_seed_12
	ret_reg &seed[12]
	call_c   build_ref_16()
	call_c   build_ref_48()
	call_c   Dyam_Seed_Start(&ref[16],&ref[48],I(0),fun0,1)
	call_c   build_ref_47()
	call_c   Dyam_Seed_Add_Comp(&ref[47],fun6,0)
	call_c   Dyam_Seed_End()
	move_ret seed[12]
	c_ret

;; TERM 47: '*GUARD*'(special_inst(succeed))
c_code local build_ref_47
	ret_reg &ref[47]
	call_c   build_ref_17()
	call_c   build_ref_46()
	call_c   Dyam_Create_Unary(&ref[17],&ref[46])
	move_ret ref[47]
	c_ret

;; TERM 46: special_inst(succeed)
c_code local build_ref_46
	ret_reg &ref[46]
	call_c   build_ref_428()
	call_c   build_ref_143()
	call_c   Dyam_Create_Unary(&ref[428],&ref[143])
	move_ret ref[46]
	c_ret

pl_code local fun6
	call_c   build_ref_47()
	call_c   Dyam_Unify_Item(&ref[47])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Deallocate(0)
	pl_jump  pred_ma_emit_pl_ret_0()

;; TERM 48: '*GUARD*'(special_inst(succeed)) :> []
c_code local build_ref_48
	ret_reg &ref[48]
	call_c   build_ref_47()
	call_c   Dyam_Create_Binary(I(9),&ref[47],I(0))
	move_ret ref[48]
	c_ret

c_code local build_seed_13
	ret_reg &seed[13]
	call_c   build_ref_16()
	call_c   build_ref_51()
	call_c   Dyam_Seed_Start(&ref[16],&ref[51],I(0),fun0,1)
	call_c   build_ref_50()
	call_c   Dyam_Seed_Add_Comp(&ref[50],fun7,0)
	call_c   Dyam_Seed_End()
	move_ret seed[13]
	c_ret

;; TERM 50: '*GUARD*'(special_inst(return))
c_code local build_ref_50
	ret_reg &ref[50]
	call_c   build_ref_17()
	call_c   build_ref_49()
	call_c   Dyam_Create_Unary(&ref[17],&ref[49])
	move_ret ref[50]
	c_ret

;; TERM 49: special_inst(return)
c_code local build_ref_49
	ret_reg &ref[49]
	call_c   build_ref_428()
	call_c   build_ref_310()
	call_c   Dyam_Create_Unary(&ref[428],&ref[310])
	move_ret ref[49]
	c_ret

pl_code local fun7
	call_c   build_ref_50()
	call_c   Dyam_Unify_Item(&ref[50])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Deallocate(0)
	pl_jump  pred_ma_emit_ret_0()

;; TERM 51: '*GUARD*'(special_inst(return)) :> []
c_code local build_ref_51
	ret_reg &ref[51]
	call_c   build_ref_50()
	call_c   Dyam_Create_Binary(I(9),&ref[50],I(0))
	move_ret ref[51]
	c_ret

c_code local build_seed_10
	ret_reg &seed[10]
	call_c   build_ref_16()
	call_c   build_ref_64()
	call_c   Dyam_Seed_Start(&ref[16],&ref[64],I(0),fun0,1)
	call_c   build_ref_63()
	call_c   Dyam_Seed_Add_Comp(&ref[63],fun8,0)
	call_c   Dyam_Seed_End()
	move_ret seed[10]
	c_ret

;; TERM 63: '*GUARD*'(special_inst(jump(_B)))
c_code local build_ref_63
	ret_reg &ref[63]
	call_c   build_ref_17()
	call_c   build_ref_62()
	call_c   Dyam_Create_Unary(&ref[17],&ref[62])
	move_ret ref[63]
	c_ret

;; TERM 62: special_inst(jump(_B))
c_code local build_ref_62
	ret_reg &ref[62]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_430()
	call_c   Dyam_Create_Unary(&ref[430],V(1))
	move_ret R(0)
	call_c   build_ref_428()
	call_c   Dyam_Create_Unary(&ref[428],R(0))
	move_ret ref[62]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 430: jump
c_code local build_ref_430
	ret_reg &ref[430]
	call_c   Dyam_Create_Atom("jump")
	move_ret ref[430]
	c_ret

pl_code local fun8
	call_c   build_ref_63()
	call_c   Dyam_Unify_Item(&ref[63])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Deallocate(1)
	pl_jump  pred_ma_emit_jump_1()

;; TERM 64: '*GUARD*'(special_inst(jump(_B))) :> []
c_code local build_ref_64
	ret_reg &ref[64]
	call_c   build_ref_63()
	call_c   Dyam_Create_Binary(I(9),&ref[63],I(0))
	move_ret ref[64]
	c_ret

c_code local build_seed_14
	ret_reg &seed[14]
	call_c   build_ref_16()
	call_c   build_ref_67()
	call_c   Dyam_Seed_Start(&ref[16],&ref[67],I(0),fun0,1)
	call_c   build_ref_66()
	call_c   Dyam_Seed_Add_Comp(&ref[66],fun9,0)
	call_c   Dyam_Seed_End()
	move_ret seed[14]
	c_ret

;; TERM 66: '*GUARD*'(special_inst(label(_B)))
c_code local build_ref_66
	ret_reg &ref[66]
	call_c   build_ref_17()
	call_c   build_ref_65()
	call_c   Dyam_Create_Unary(&ref[17],&ref[65])
	move_ret ref[66]
	c_ret

;; TERM 65: special_inst(label(_B))
c_code local build_ref_65
	ret_reg &ref[65]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_431()
	call_c   Dyam_Create_Unary(&ref[431],V(1))
	move_ret R(0)
	call_c   build_ref_428()
	call_c   Dyam_Create_Unary(&ref[428],R(0))
	move_ret ref[65]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 431: label
c_code local build_ref_431
	ret_reg &ref[431]
	call_c   Dyam_Create_Atom("label")
	move_ret ref[431]
	c_ret

pl_code local fun9
	call_c   build_ref_66()
	call_c   Dyam_Unify_Item(&ref[66])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Deallocate(1)
	pl_jump  pred_ma_emit_label_1()

;; TERM 67: '*GUARD*'(special_inst(label(_B))) :> []
c_code local build_ref_67
	ret_reg &ref[67]
	call_c   build_ref_66()
	call_c   Dyam_Create_Binary(I(9),&ref[66],I(0))
	move_ret ref[67]
	c_ret

c_code local build_seed_7
	ret_reg &seed[7]
	call_c   build_ref_16()
	call_c   build_ref_109()
	call_c   Dyam_Seed_Start(&ref[16],&ref[109],I(0),fun0,1)
	call_c   build_ref_108()
	call_c   Dyam_Seed_Add_Comp(&ref[108],fun10,0)
	call_c   Dyam_Seed_End()
	move_ret seed[7]
	c_ret

;; TERM 108: '*GUARD*'(special_inst(reg_zero(_B)))
c_code local build_ref_108
	ret_reg &ref[108]
	call_c   build_ref_17()
	call_c   build_ref_107()
	call_c   Dyam_Create_Unary(&ref[17],&ref[107])
	move_ret ref[108]
	c_ret

;; TERM 107: special_inst(reg_zero(_B))
c_code local build_ref_107
	ret_reg &ref[107]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_432()
	call_c   Dyam_Create_Unary(&ref[432],V(1))
	move_ret R(0)
	call_c   build_ref_428()
	call_c   Dyam_Create_Unary(&ref[428],R(0))
	move_ret ref[107]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 432: reg_zero
c_code local build_ref_432
	ret_reg &ref[432]
	call_c   Dyam_Create_Atom("reg_zero")
	move_ret ref[432]
	c_ret

;; TERM 111: '$reg'(_B)
c_code local build_ref_111
	ret_reg &ref[111]
	call_c   build_ref_433()
	call_c   Dyam_Create_Unary(&ref[433],V(1))
	move_ret ref[111]
	c_ret

;; TERM 433: '$reg'
c_code local build_ref_433
	ret_reg &ref[433]
	call_c   Dyam_Create_Atom("$reg")
	move_ret ref[433]
	c_ret

long local pool_fun10[4]=[3,build_ref_108,build_ref_110,build_ref_111]

pl_code local fun10
	call_c   Dyam_Pool(pool_fun10)
	call_c   Dyam_Unify_Item(&ref[108])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[110], R(0)
	move     S(5), R(1)
	move     &ref[111], R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_ma_emit_move_2()

;; TERM 109: '*GUARD*'(special_inst(reg_zero(_B))) :> []
c_code local build_ref_109
	ret_reg &ref[109]
	call_c   build_ref_108()
	call_c   Dyam_Create_Binary(I(9),&ref[108],I(0))
	move_ret ref[109]
	c_ret

c_code local build_seed_15
	ret_reg &seed[15]
	call_c   build_ref_16()
	call_c   build_ref_120()
	call_c   Dyam_Seed_Start(&ref[16],&ref[120],I(0),fun0,1)
	call_c   build_ref_119()
	call_c   Dyam_Seed_Add_Comp(&ref[119],fun15,0)
	call_c   Dyam_Seed_End()
	move_ret seed[15]
	c_ret

;; TERM 119: '*GUARD*'(special_inst(call(_B, _C)))
c_code local build_ref_119
	ret_reg &ref[119]
	call_c   build_ref_17()
	call_c   build_ref_118()
	call_c   Dyam_Create_Unary(&ref[17],&ref[118])
	move_ret ref[119]
	c_ret

;; TERM 118: special_inst(call(_B, _C))
c_code local build_ref_118
	ret_reg &ref[118]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_414()
	call_c   Dyam_Create_Binary(&ref[414],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_428()
	call_c   Dyam_Create_Unary(&ref[428],R(0))
	move_ret ref[118]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

pl_code local fun15
	call_c   build_ref_119()
	call_c   Dyam_Unify_Item(&ref[119])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(2))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_ma_emit_pl_call_2()

;; TERM 120: '*GUARD*'(special_inst(call(_B, _C))) :> []
c_code local build_ref_120
	ret_reg &ref[120]
	call_c   build_ref_119()
	call_c   Dyam_Create_Binary(I(9),&ref[119],I(0))
	move_ret ref[120]
	c_ret

c_code local build_seed_4
	ret_reg &seed[4]
	call_c   build_ref_16()
	call_c   build_ref_125()
	call_c   Dyam_Seed_Start(&ref[16],&ref[125],I(0),fun0,1)
	call_c   build_ref_124()
	call_c   Dyam_Seed_Add_Comp(&ref[124],fun18,0)
	call_c   Dyam_Seed_End()
	move_ret seed[4]
	c_ret

;; TERM 124: '*GUARD*'(special_inst(reg_load_var(_B, _C)))
c_code local build_ref_124
	ret_reg &ref[124]
	call_c   build_ref_17()
	call_c   build_ref_123()
	call_c   Dyam_Create_Unary(&ref[17],&ref[123])
	move_ret ref[124]
	c_ret

;; TERM 123: special_inst(reg_load_var(_B, _C))
c_code local build_ref_123
	ret_reg &ref[123]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_434()
	call_c   Dyam_Create_Binary(&ref[434],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_428()
	call_c   Dyam_Create_Unary(&ref[428],R(0))
	move_ret ref[123]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 434: reg_load_var
c_code local build_ref_434
	ret_reg &ref[434]
	call_c   Dyam_Create_Atom("reg_load_var")
	move_ret ref[434]
	c_ret

;; TERM 126: '$system'(trans_k)
c_code local build_ref_126
	ret_reg &ref[126]
	call_c   build_ref_435()
	call_c   build_ref_436()
	call_c   Dyam_Create_Unary(&ref[435],&ref[436])
	move_ret ref[126]
	c_ret

;; TERM 436: trans_k
c_code local build_ref_436
	ret_reg &ref[436]
	call_c   Dyam_Create_Atom("trans_k")
	move_ret ref[436]
	c_ret

;; TERM 435: '$system'
c_code local build_ref_435
	ret_reg &ref[435]
	call_c   Dyam_Create_Atom("$system")
	move_ret ref[435]
	c_ret

long local pool_fun18[3]=[2,build_ref_124,build_ref_126]

pl_code local fun18
	call_c   Dyam_Pool(pool_fun18)
	call_c   Dyam_Unify_Item(&ref[124])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(2))
	move     &ref[126], R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  pred_emit_move_3()

;; TERM 125: '*GUARD*'(special_inst(reg_load_var(_B, _C))) :> []
c_code local build_ref_125
	ret_reg &ref[125]
	call_c   build_ref_124()
	call_c   Dyam_Create_Binary(I(9),&ref[124],I(0))
	move_ret ref[125]
	c_ret

c_code local build_seed_6
	ret_reg &seed[6]
	call_c   build_ref_16()
	call_c   build_ref_129()
	call_c   Dyam_Seed_Start(&ref[16],&ref[129],I(0),fun0,1)
	call_c   build_ref_128()
	call_c   Dyam_Seed_Add_Comp(&ref[128],fun19,0)
	call_c   Dyam_Seed_End()
	move_ret seed[6]
	c_ret

;; TERM 128: '*GUARD*'(special_inst(reg_load_cst(_B, _C)))
c_code local build_ref_128
	ret_reg &ref[128]
	call_c   build_ref_17()
	call_c   build_ref_127()
	call_c   Dyam_Create_Unary(&ref[17],&ref[127])
	move_ret ref[128]
	c_ret

;; TERM 127: special_inst(reg_load_cst(_B, _C))
c_code local build_ref_127
	ret_reg &ref[127]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_437()
	call_c   Dyam_Create_Binary(&ref[437],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_428()
	call_c   Dyam_Create_Unary(&ref[428],R(0))
	move_ret ref[127]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 437: reg_load_cst
c_code local build_ref_437
	ret_reg &ref[437]
	call_c   Dyam_Create_Atom("reg_load_cst")
	move_ret ref[437]
	c_ret

long local pool_fun19[3]=[2,build_ref_128,build_ref_110]

pl_code local fun19
	call_c   Dyam_Pool(pool_fun19)
	call_c   Dyam_Unify_Item(&ref[128])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(2))
	move     &ref[110], R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  pred_emit_move_3()

;; TERM 129: '*GUARD*'(special_inst(reg_load_cst(_B, _C))) :> []
c_code local build_ref_129
	ret_reg &ref[129]
	call_c   build_ref_128()
	call_c   Dyam_Create_Binary(I(9),&ref[128],I(0))
	move_ret ref[129]
	c_ret

c_code local build_seed_5
	ret_reg &seed[5]
	call_c   build_ref_16()
	call_c   build_ref_171()
	call_c   Dyam_Seed_Start(&ref[16],&ref[171],I(0),fun0,1)
	call_c   build_ref_170()
	call_c   Dyam_Seed_Add_Comp(&ref[170],fun37,0)
	call_c   Dyam_Seed_End()
	move_ret seed[5]
	c_ret

;; TERM 170: '*GUARD*'(special_inst(reg_load_nil(_B)))
c_code local build_ref_170
	ret_reg &ref[170]
	call_c   build_ref_17()
	call_c   build_ref_169()
	call_c   Dyam_Create_Unary(&ref[17],&ref[169])
	move_ret ref[170]
	c_ret

;; TERM 169: special_inst(reg_load_nil(_B))
c_code local build_ref_169
	ret_reg &ref[169]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_438()
	call_c   Dyam_Create_Unary(&ref[438],V(1))
	move_ret R(0)
	call_c   build_ref_428()
	call_c   Dyam_Create_Unary(&ref[428],R(0))
	move_ret ref[169]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 438: reg_load_nil
c_code local build_ref_438
	ret_reg &ref[438]
	call_c   Dyam_Create_Atom("reg_load_nil")
	move_ret ref[438]
	c_ret

long local pool_fun37[3]=[2,build_ref_170,build_ref_110]

pl_code local fun37
	call_c   Dyam_Pool(pool_fun37)
	call_c   Dyam_Unify_Item(&ref[170])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     I(0), R(0)
	move     0, R(1)
	move     V(2), R(2)
	move     S(5), R(3)
	pl_call  pred_label_term_2()
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(2))
	move     &ref[110], R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  pred_emit_move_3()

;; TERM 171: '*GUARD*'(special_inst(reg_load_nil(_B))) :> []
c_code local build_ref_171
	ret_reg &ref[171]
	call_c   build_ref_170()
	call_c   Dyam_Create_Binary(I(9),&ref[170],I(0))
	move_ret ref[171]
	c_ret

c_code local build_seed_17
	ret_reg &seed[17]
	call_c   build_ref_16()
	call_c   build_ref_177()
	call_c   Dyam_Seed_Start(&ref[16],&ref[177],I(0),fun0,1)
	call_c   build_ref_176()
	call_c   Dyam_Seed_Add_Comp(&ref[176],fun44,0)
	call_c   Dyam_Seed_End()
	move_ret seed[17]
	c_ret

;; TERM 176: '*GUARD*'(decompose_refs(['$seed'(_B, _C)|_D], [build_seed_ + _B|_E], _F))
c_code local build_ref_176
	ret_reg &ref[176]
	call_c   build_ref_17()
	call_c   build_ref_175()
	call_c   Dyam_Create_Unary(&ref[17],&ref[175])
	move_ret ref[176]
	c_ret

;; TERM 175: decompose_refs(['$seed'(_B, _C)|_D], [build_seed_ + _B|_E], _F)
c_code local build_ref_175
	ret_reg &ref[175]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_439()
	call_c   Dyam_Create_Binary(&ref[439],V(1),V(2))
	move_ret R(0)
	call_c   Dyam_Create_List(R(0),V(3))
	move_ret R(0)
	call_c   build_ref_338()
	call_c   build_ref_440()
	call_c   Dyam_Create_Binary(&ref[338],&ref[440],V(1))
	move_ret R(1)
	call_c   Dyam_Create_List(R(1),V(4))
	move_ret R(1)
	call_c   build_ref_429()
	call_c   Dyam_Term_Start(&ref[429],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[175]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 440: build_seed_
c_code local build_ref_440
	ret_reg &ref[440]
	call_c   Dyam_Create_Atom("build_seed_")
	move_ret ref[440]
	c_ret

;; TERM 439: '$seed'
c_code local build_ref_439
	ret_reg &ref[439]
	call_c   Dyam_Create_Atom("$seed")
	move_ret ref[439]
	c_ret

c_code local build_seed_107
	ret_reg &seed[107]
	call_c   build_ref_17()
	call_c   build_ref_179()
	call_c   Dyam_Seed_Start(&ref[17],&ref[179],I(0),fun42,1)
	call_c   build_ref_180()
	call_c   Dyam_Seed_Add_Comp(&ref[180],&ref[179],0)
	call_c   Dyam_Seed_End()
	move_ret seed[107]
	c_ret

pl_code local fun42
	pl_jump  Complete(0,0)

;; TERM 180: '*GUARD*'(decompose_refs(_D, _E, _F)) :> '$$HOLE$$'
c_code local build_ref_180
	ret_reg &ref[180]
	call_c   build_ref_179()
	call_c   Dyam_Create_Binary(I(9),&ref[179],I(7))
	move_ret ref[180]
	c_ret

;; TERM 179: '*GUARD*'(decompose_refs(_D, _E, _F))
c_code local build_ref_179
	ret_reg &ref[179]
	call_c   build_ref_17()
	call_c   build_ref_178()
	call_c   Dyam_Create_Unary(&ref[17],&ref[178])
	move_ret ref[179]
	c_ret

;; TERM 178: decompose_refs(_D, _E, _F)
c_code local build_ref_178
	ret_reg &ref[178]
	call_c   build_ref_429()
	call_c   Dyam_Term_Start(&ref[429],3)
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[178]
	c_ret

pl_code local fun43
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Light_Object(R(0))
	pl_jump  Schedule_Prolog()

long local pool_fun44[3]=[2,build_ref_176,build_seed_107]

pl_code local fun44
	call_c   Dyam_Pool(pool_fun44)
	call_c   Dyam_Unify_Item(&ref[176])
	fail_ret
	pl_jump  fun43(&seed[107],1)

;; TERM 177: '*GUARD*'(decompose_refs(['$seed'(_B, _C)|_D], [build_seed_ + _B|_E], _F)) :> []
c_code local build_ref_177
	ret_reg &ref[177]
	call_c   build_ref_176()
	call_c   Dyam_Create_Binary(I(9),&ref[176],I(0))
	move_ret ref[177]
	c_ret

c_code local build_seed_18
	ret_reg &seed[18]
	call_c   build_ref_16()
	call_c   build_ref_183()
	call_c   Dyam_Seed_Start(&ref[16],&ref[183],I(0),fun0,1)
	call_c   build_ref_182()
	call_c   Dyam_Seed_Add_Comp(&ref[182],fun45,0)
	call_c   Dyam_Seed_End()
	move_ret seed[18]
	c_ret

;; TERM 182: '*GUARD*'(decompose_refs(['$term'(_B, _C)|_D], [build_ref_ + _B|_E], _F))
c_code local build_ref_182
	ret_reg &ref[182]
	call_c   build_ref_17()
	call_c   build_ref_181()
	call_c   Dyam_Create_Unary(&ref[17],&ref[181])
	move_ret ref[182]
	c_ret

;; TERM 181: decompose_refs(['$term'(_B, _C)|_D], [build_ref_ + _B|_E], _F)
c_code local build_ref_181
	ret_reg &ref[181]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_441()
	call_c   Dyam_Create_Binary(&ref[441],V(1),V(2))
	move_ret R(0)
	call_c   Dyam_Create_List(R(0),V(3))
	move_ret R(0)
	call_c   build_ref_338()
	call_c   build_ref_442()
	call_c   Dyam_Create_Binary(&ref[338],&ref[442],V(1))
	move_ret R(1)
	call_c   Dyam_Create_List(R(1),V(4))
	move_ret R(1)
	call_c   build_ref_429()
	call_c   Dyam_Term_Start(&ref[429],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[181]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 442: build_ref_
c_code local build_ref_442
	ret_reg &ref[442]
	call_c   Dyam_Create_Atom("build_ref_")
	move_ret ref[442]
	c_ret

;; TERM 441: '$term'
c_code local build_ref_441
	ret_reg &ref[441]
	call_c   Dyam_Create_Atom("$term")
	move_ret ref[441]
	c_ret

long local pool_fun45[3]=[2,build_ref_182,build_seed_107]

pl_code local fun45
	call_c   Dyam_Pool(pool_fun45)
	call_c   Dyam_Unify_Item(&ref[182])
	fail_ret
	pl_jump  fun43(&seed[107],1)

;; TERM 183: '*GUARD*'(decompose_refs(['$term'(_B, _C)|_D], [build_ref_ + _B|_E], _F)) :> []
c_code local build_ref_183
	ret_reg &ref[183]
	call_c   build_ref_182()
	call_c   Dyam_Create_Binary(I(9),&ref[182],I(0))
	move_ret ref[183]
	c_ret

c_code local build_seed_8
	ret_reg &seed[8]
	call_c   build_ref_16()
	call_c   build_ref_187()
	call_c   Dyam_Seed_Start(&ref[16],&ref[187],I(0),fun0,1)
	call_c   build_ref_186()
	call_c   Dyam_Seed_Add_Comp(&ref[186],fun54,0)
	call_c   Dyam_Seed_End()
	move_ret seed[8]
	c_ret

;; TERM 186: '*GUARD*'(special_inst(c_call(_B, _C, _D, _E)))
c_code local build_ref_186
	ret_reg &ref[186]
	call_c   build_ref_17()
	call_c   build_ref_185()
	call_c   Dyam_Create_Unary(&ref[17],&ref[185])
	move_ret ref[186]
	c_ret

;; TERM 185: special_inst(c_call(_B, _C, _D, _E))
c_code local build_ref_185
	ret_reg &ref[185]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_337()
	call_c   Dyam_Term_Start(&ref[337],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_428()
	call_c   Dyam_Create_Unary(&ref[428],R(0))
	move_ret ref[185]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

pl_code local fun51
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Reg_Load(0,V(4))
	pl_call  pred_ma_emit_move_ret_1()
fun50:
	call_c   Dyam_Deallocate()
	pl_ret


;; TERM 188: none
c_code local build_ref_188
	ret_reg &ref[188]
	call_c   Dyam_Create_Atom("none")
	move_ret ref[188]
	c_ret

long local pool_fun52[2]=[1,build_ref_188]

pl_code local fun53
	call_c   Dyam_Remove_Choice()
	pl_call  pred_ma_emit_fail_ret_0()
fun52:
	call_c   Dyam_Choice(fun51)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(4),&ref[188])
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun50()


long local pool_fun54[5]=[131074,build_ref_186,build_ref_188,pool_fun52,pool_fun52]

pl_code local fun54
	call_c   Dyam_Pool(pool_fun54)
	call_c   Dyam_Unify_Item(&ref[186])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(2))
	pl_call  pred_ma_emit_call_c_2()
	call_c   Dyam_Choice(fun53)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(3),&ref[188])
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun52()

;; TERM 187: '*GUARD*'(special_inst(c_call(_B, _C, _D, _E))) :> []
c_code local build_ref_187
	ret_reg &ref[187]
	call_c   build_ref_186()
	call_c   Dyam_Create_Binary(I(9),&ref[186],I(0))
	move_ret ref[187]
	c_ret

c_code local build_seed_3
	ret_reg &seed[3]
	call_c   build_ref_16()
	call_c   build_ref_218()
	call_c   Dyam_Seed_Start(&ref[16],&ref[218],I(0),fun0,1)
	call_c   build_ref_217()
	call_c   Dyam_Seed_Add_Comp(&ref[217],fun73,0)
	call_c   Dyam_Seed_End()
	move_ret seed[3]
	c_ret

;; TERM 217: '*GUARD*'(special_inst(closure(_B, _C, _D)))
c_code local build_ref_217
	ret_reg &ref[217]
	call_c   build_ref_17()
	call_c   build_ref_216()
	call_c   Dyam_Create_Unary(&ref[17],&ref[216])
	move_ret ref[217]
	c_ret

;; TERM 216: special_inst(closure(_B, _C, _D))
c_code local build_ref_216
	ret_reg &ref[216]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_443()
	call_c   Dyam_Term_Start(&ref[443],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_428()
	call_c   Dyam_Create_Unary(&ref[428],R(0))
	move_ret ref[216]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 443: closure
c_code local build_ref_443
	ret_reg &ref[443]
	call_c   Dyam_Create_Atom("closure")
	move_ret ref[443]
	c_ret

;; TERM 219: closure_label
c_code local build_ref_219
	ret_reg &ref[219]
	call_c   Dyam_Create_Atom("closure_label")
	move_ret ref[219]
	c_ret

;; TERM 220: '\tjmp_reg  ~a, Lclosure~w\n'
c_code local build_ref_220
	ret_reg &ref[220]
	call_c   Dyam_Create_Atom("\tjmp_reg  ~a, Lclosure~w\n")
	move_ret ref[220]
	c_ret

;; TERM 221: [_B,_E]
c_code local build_ref_221
	ret_reg &ref[221]
	call_c   build_ref_225()
	call_c   Dyam_Create_List(V(1),&ref[225])
	move_ret ref[221]
	c_ret

;; TERM 225: [_E]
c_code local build_ref_225
	ret_reg &ref[225]
	call_c   Dyam_Create_List(V(4),I(0))
	move_ret ref[225]
	c_ret

;; TERM 222: 'Dyam_Closure_Aux'
c_code local build_ref_222
	ret_reg &ref[222]
	call_c   Dyam_Create_Atom("Dyam_Closure_Aux")
	move_ret ref[222]
	c_ret

;; TERM 223: [_C,_D]
c_code local build_ref_223
	ret_reg &ref[223]
	call_c   Dyam_Create_Tupple(2,3,I(0))
	move_ret ref[223]
	c_ret

;; TERM 224: 'Lclosure~w:\n'
c_code local build_ref_224
	ret_reg &ref[224]
	call_c   Dyam_Create_Atom("Lclosure~w:\n")
	move_ret ref[224]
	c_ret

long local pool_fun73[9]=[8,build_ref_217,build_ref_219,build_ref_220,build_ref_221,build_ref_222,build_ref_223,build_ref_224,build_ref_225]

pl_code local fun73
	call_c   Dyam_Pool(pool_fun73)
	call_c   Dyam_Unify_Item(&ref[217])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[219], R(0)
	move     0, R(1)
	move     V(4), R(2)
	move     S(5), R(3)
	pl_call  pred_update_counter_2()
	move     &ref[220], R(0)
	move     0, R(1)
	move     &ref[221], R(2)
	move     S(5), R(3)
	pl_call  pred_format_2()
	move     &ref[222], R(0)
	move     0, R(1)
	move     &ref[223], R(2)
	move     S(5), R(3)
	pl_call  pred_ma_emit_call_c_2()
	call_c   Dyam_Reg_Load(0,V(1))
	pl_call  pred_ma_emit_move_ret_1()
	move     &ref[224], R(0)
	move     0, R(1)
	move     &ref[225], R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_format_2()

;; TERM 218: '*GUARD*'(special_inst(closure(_B, _C, _D))) :> []
c_code local build_ref_218
	ret_reg &ref[218]
	call_c   build_ref_217()
	call_c   Dyam_Create_Binary(I(9),&ref[217],I(0))
	move_ret ref[218]
	c_ret

c_code local build_seed_9
	ret_reg &seed[9]
	call_c   build_ref_16()
	call_c   build_ref_240()
	call_c   Dyam_Seed_Start(&ref[16],&ref[240],I(0),fun0,1)
	call_c   build_ref_239()
	call_c   Dyam_Seed_Add_Comp(&ref[239],fun95,0)
	call_c   Dyam_Seed_End()
	move_ret seed[9]
	c_ret

;; TERM 239: '*GUARD*'(special_inst(pl_jump(_B, _C)))
c_code local build_ref_239
	ret_reg &ref[239]
	call_c   build_ref_17()
	call_c   build_ref_238()
	call_c   Dyam_Create_Unary(&ref[17],&ref[238])
	move_ret ref[239]
	c_ret

;; TERM 238: special_inst(pl_jump(_B, _C))
c_code local build_ref_238
	ret_reg &ref[238]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_415()
	call_c   Dyam_Create_Binary(&ref[415],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_428()
	call_c   Dyam_Create_Unary(&ref[428],R(0))
	move_ret ref[238]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

pl_code local fun93
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(2))
	pl_call  pred_ma_emit_pl_jump_2()
	pl_jump  fun50()

pl_code local fun94
	call_c   Dyam_Update_Choice(fun93)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(2),I(0))
	fail_ret
	call_c   Dyam_Reg_Load(0,V(1))
	pl_call  pred_inlinable_1()
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(1))
	pl_call  pred_ma_emit_label_1()
	call_c   Dyam_Reg_Load(0,V(1))
	pl_call  pred_emit_inlined_function_1()
	pl_jump  fun50()

long local pool_fun95[2]=[1,build_ref_239]

pl_code local fun95
	call_c   Dyam_Pool(pool_fun95)
	call_c   Dyam_Unify_Item(&ref[239])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun94)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Reg_Load(0,V(1))
	move     N(0), R(2)
	move     0, R(3)
	pl_call  pred_reg_value_2()
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(2))
	pl_call  pred_ma_emit_pl_jump_2()
	pl_jump  fun50()

;; TERM 240: '*GUARD*'(special_inst(pl_jump(_B, _C))) :> []
c_code local build_ref_240
	ret_reg &ref[240]
	call_c   build_ref_239()
	call_c   Dyam_Create_Binary(I(9),&ref[239],I(0))
	move_ret ref[240]
	c_ret

c_code local build_seed_16
	ret_reg &seed[16]
	call_c   build_ref_16()
	call_c   build_ref_243()
	call_c   Dyam_Seed_Start(&ref[16],&ref[243],I(0),fun0,1)
	call_c   build_ref_242()
	call_c   Dyam_Seed_Add_Comp(&ref[242],fun99,0)
	call_c   Dyam_Seed_End()
	move_ret seed[16]
	c_ret

;; TERM 242: '*GUARD*'(decompose_refs(['$fun'(_B, _C, _D)|_E], _F, _G))
c_code local build_ref_242
	ret_reg &ref[242]
	call_c   build_ref_17()
	call_c   build_ref_241()
	call_c   Dyam_Create_Unary(&ref[17],&ref[241])
	move_ret ref[242]
	c_ret

;; TERM 241: decompose_refs(['$fun'(_B, _C, _D)|_E], _F, _G)
c_code local build_ref_241
	ret_reg &ref[241]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_215()
	call_c   Dyam_Create_List(&ref[215],V(4))
	move_ret R(0)
	call_c   build_ref_429()
	call_c   Dyam_Term_Start(&ref[429],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[241]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 215: '$fun'(_B, _C, _D)
c_code local build_ref_215
	ret_reg &ref[215]
	call_c   build_ref_444()
	call_c   Dyam_Term_Start(&ref[444],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[215]
	c_ret

;; TERM 444: '$fun'
c_code local build_ref_444
	ret_reg &ref[444]
	call_c   Dyam_Create_Atom("$fun")
	move_ret ref[444]
	c_ret

c_code local build_seed_111
	ret_reg &seed[111]
	call_c   build_ref_17()
	call_c   build_ref_248()
	call_c   Dyam_Seed_Start(&ref[17],&ref[248],I(0),fun42,1)
	call_c   build_ref_249()
	call_c   Dyam_Seed_Add_Comp(&ref[249],&ref[248],0)
	call_c   Dyam_Seed_End()
	move_ret seed[111]
	c_ret

;; TERM 249: '*GUARD*'(decompose_refs(_E, _J, _K)) :> '$$HOLE$$'
c_code local build_ref_249
	ret_reg &ref[249]
	call_c   build_ref_248()
	call_c   Dyam_Create_Binary(I(9),&ref[248],I(7))
	move_ret ref[249]
	c_ret

;; TERM 248: '*GUARD*'(decompose_refs(_E, _J, _K))
c_code local build_ref_248
	ret_reg &ref[248]
	call_c   build_ref_17()
	call_c   build_ref_247()
	call_c   Dyam_Create_Unary(&ref[17],&ref[247])
	move_ret ref[248]
	c_ret

;; TERM 247: decompose_refs(_E, _J, _K)
c_code local build_ref_247
	ret_reg &ref[247]
	call_c   build_ref_429()
	call_c   Dyam_Term_Start(&ref[429],3)
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_End()
	move_ret ref[247]
	c_ret

long local pool_fun96[2]=[1,build_seed_111]

pl_code local fun98
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(5),V(9))
	fail_ret
	call_c   Dyam_Unify(V(6),V(10))
	fail_ret
fun96:
	call_c   Dyam_Deallocate()
	pl_jump  fun43(&seed[111],1)


;; TERM 244: '$pool'('$fun'(_B, _C, _D), _H)
c_code local build_ref_244
	ret_reg &ref[244]
	call_c   build_ref_445()
	call_c   build_ref_215()
	call_c   Dyam_Create_Binary(&ref[445],&ref[215],V(7))
	move_ret ref[244]
	c_ret

;; TERM 445: '$pool'
c_code local build_ref_445
	ret_reg &ref[445]
	call_c   Dyam_Create_Atom("$pool")
	move_ret ref[445]
	c_ret

;; TERM 250: [_H|_J]
c_code local build_ref_250
	ret_reg &ref[250]
	call_c   Dyam_Create_List(V(7),V(9))
	move_ret ref[250]
	c_ret

long local pool_fun97[3]=[65537,build_ref_250,pool_fun96]

pl_code local fun97
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(5),&ref[250])
	fail_ret
	call_c   Dyam_Unify(V(6),V(10))
	fail_ret
	pl_jump  fun96()

;; TERM 245: pool_ + _I
c_code local build_ref_245
	ret_reg &ref[245]
	call_c   build_ref_338()
	call_c   build_ref_446()
	call_c   Dyam_Create_Binary(&ref[338],&ref[446],V(8))
	move_ret ref[245]
	c_ret

;; TERM 446: pool_
c_code local build_ref_446
	ret_reg &ref[446]
	call_c   Dyam_Create_Atom("pool_")
	move_ret ref[446]
	c_ret

;; TERM 246: [_H|_K]
c_code local build_ref_246
	ret_reg &ref[246]
	call_c   Dyam_Create_List(V(7),V(10))
	move_ret ref[246]
	c_ret

long local pool_fun99[8]=[196612,build_ref_242,build_ref_244,build_ref_245,build_ref_246,pool_fun96,pool_fun97,pool_fun96]

pl_code local fun99
	call_c   Dyam_Pool(pool_fun99)
	call_c   Dyam_Unify_Item(&ref[242])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun98)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[244])
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun97)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(7),&ref[245])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(5),V(9))
	fail_ret
	call_c   Dyam_Unify(V(6),&ref[246])
	fail_ret
	pl_jump  fun96()

;; TERM 243: '*GUARD*'(decompose_refs(['$fun'(_B, _C, _D)|_E], _F, _G)) :> []
c_code local build_ref_243
	ret_reg &ref[243]
	call_c   build_ref_242()
	call_c   Dyam_Create_Binary(I(9),&ref[242],I(0))
	move_ret ref[243]
	c_ret

c_code local build_seed_112
	ret_reg &seed[112]
	call_c   build_ref_17()
	call_c   build_ref_273()
	call_c   Dyam_Seed_Start(&ref[17],&ref[273],I(0),fun42,1)
	call_c   build_ref_274()
	call_c   Dyam_Seed_Add_Comp(&ref[274],&ref[273],0)
	call_c   Dyam_Seed_End()
	move_ret seed[112]
	c_ret

;; TERM 274: '*GUARD*'(special_inst(_B)) :> '$$HOLE$$'
c_code local build_ref_274
	ret_reg &ref[274]
	call_c   build_ref_273()
	call_c   Dyam_Create_Binary(I(9),&ref[273],I(7))
	move_ret ref[274]
	c_ret

;; TERM 273: '*GUARD*'(special_inst(_B))
c_code local build_ref_273
	ret_reg &ref[273]
	call_c   build_ref_17()
	call_c   build_ref_272()
	call_c   Dyam_Create_Unary(&ref[17],&ref[272])
	move_ret ref[273]
	c_ret

;; TERM 272: special_inst(_B)
c_code local build_ref_272
	ret_reg &ref[272]
	call_c   build_ref_428()
	call_c   Dyam_Create_Unary(&ref[428],V(1))
	move_ret ref[272]
	c_ret

c_code local build_seed_108
	ret_reg &seed[108]
	call_c   build_ref_17()
	call_c   build_ref_191()
	call_c   Dyam_Seed_Start(&ref[17],&ref[191],I(0),fun42,1)
	call_c   build_ref_192()
	call_c   Dyam_Seed_Add_Comp(&ref[192],&ref[191],0)
	call_c   Dyam_Seed_End()
	move_ret seed[108]
	c_ret

;; TERM 192: '*GUARD*'(data_emit(_F)) :> '$$HOLE$$'
c_code local build_ref_192
	ret_reg &ref[192]
	call_c   build_ref_191()
	call_c   Dyam_Create_Binary(I(9),&ref[191],I(7))
	move_ret ref[192]
	c_ret

;; TERM 191: '*GUARD*'(data_emit(_F))
c_code local build_ref_191
	ret_reg &ref[191]
	call_c   build_ref_17()
	call_c   build_ref_190()
	call_c   Dyam_Create_Unary(&ref[17],&ref[190])
	move_ret ref[191]
	c_ret

;; TERM 190: data_emit(_F)
c_code local build_ref_190
	ret_reg &ref[190]
	call_c   build_ref_447()
	call_c   Dyam_Create_Unary(&ref[447],V(5))
	move_ret ref[190]
	c_ret

;; TERM 447: data_emit
c_code local build_ref_447
	ret_reg &ref[447]
	call_c   Dyam_Create_Atom("data_emit")
	move_ret ref[447]
	c_ret

c_code local build_seed_110
	ret_reg &seed[110]
	call_c   build_ref_17()
	call_c   build_ref_206()
	call_c   Dyam_Seed_Start(&ref[17],&ref[206],I(0),fun42,1)
	call_c   build_ref_207()
	call_c   Dyam_Seed_Add_Comp(&ref[207],&ref[206],0)
	call_c   Dyam_Seed_End()
	move_ret seed[110]
	c_ret

;; TERM 207: '*GUARD*'(append(_P, _Q, _U)) :> '$$HOLE$$'
c_code local build_ref_207
	ret_reg &ref[207]
	call_c   build_ref_206()
	call_c   Dyam_Create_Binary(I(9),&ref[206],I(7))
	move_ret ref[207]
	c_ret

;; TERM 206: '*GUARD*'(append(_P, _Q, _U))
c_code local build_ref_206
	ret_reg &ref[206]
	call_c   build_ref_17()
	call_c   build_ref_205()
	call_c   Dyam_Create_Unary(&ref[17],&ref[205])
	move_ret ref[206]
	c_ret

;; TERM 205: append(_P, _Q, _U)
c_code local build_ref_205
	ret_reg &ref[205]
	call_c   build_ref_448()
	call_c   Dyam_Term_Start(&ref[448],3)
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_End()
	move_ret ref[205]
	c_ret

;; TERM 448: append
c_code local build_ref_448
	ret_reg &ref[448]
	call_c   Dyam_Create_Atom("append")
	move_ret ref[448]
	c_ret

c_code local build_seed_109
	ret_reg &seed[109]
	call_c   build_ref_17()
	call_c   build_ref_201()
	call_c   Dyam_Seed_Start(&ref[17],&ref[201],I(0),fun42,1)
	call_c   build_ref_202()
	call_c   Dyam_Seed_Add_Comp(&ref[202],&ref[201],0)
	call_c   Dyam_Seed_End()
	move_ret seed[109]
	c_ret

;; TERM 202: '*GUARD*'(decompose_refs(_B, _P, _Q)) :> '$$HOLE$$'
c_code local build_ref_202
	ret_reg &ref[202]
	call_c   build_ref_201()
	call_c   Dyam_Create_Binary(I(9),&ref[201],I(7))
	move_ret ref[202]
	c_ret

;; TERM 201: '*GUARD*'(decompose_refs(_B, _P, _Q))
c_code local build_ref_201
	ret_reg &ref[201]
	call_c   build_ref_17()
	call_c   build_ref_200()
	call_c   Dyam_Create_Unary(&ref[17],&ref[200])
	move_ret ref[201]
	c_ret

;; TERM 200: decompose_refs(_B, _P, _Q)
c_code local build_ref_200
	ret_reg &ref[200]
	call_c   build_ref_429()
	call_c   Dyam_Term_Start(&ref[429],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_End()
	move_ret ref[200]
	c_ret

;; TERM 266: noop(_C)
c_code local build_ref_266
	ret_reg &ref[266]
	call_c   build_ref_136()
	call_c   Dyam_Create_Unary(&ref[136],V(2))
	move_ret ref[266]
	c_ret

;; TERM 267: notest(_D, _E)
c_code local build_ref_267
	ret_reg &ref[267]
	call_c   build_ref_449()
	call_c   Dyam_Create_Binary(&ref[449],V(3),V(4))
	move_ret ref[267]
	c_ret

;; TERM 449: notest
c_code local build_ref_449
	ret_reg &ref[449]
	call_c   Dyam_Create_Atom("notest")
	move_ret ref[449]
	c_ret

;; TERM 268: failtest(_D, _E)
c_code local build_ref_268
	ret_reg &ref[268]
	call_c   build_ref_450()
	call_c   Dyam_Create_Binary(&ref[450],V(3),V(4))
	move_ret ref[268]
	c_ret

;; TERM 450: failtest
c_code local build_ref_450
	ret_reg &ref[450]
	call_c   Dyam_Create_Atom("failtest")
	move_ret ref[450]
	c_ret

;; TERM 269: test_inst(_B, _D, _E)
c_code local build_ref_269
	ret_reg &ref[269]
	call_c   build_ref_321()
	call_c   Dyam_Term_Start(&ref[321],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[269]
	c_ret

;; TERM 270: inst(_B, _D, _E)
c_code local build_ref_270
	ret_reg &ref[270]
	call_c   build_ref_277()
	call_c   Dyam_Term_Start(&ref[277],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[270]
	c_ret

;; TERM 271: pl_inst(_B, _D, _E)
c_code local build_ref_271
	ret_reg &ref[271]
	call_c   build_ref_418()
	call_c   Dyam_Term_Start(&ref[418],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[271]
	c_ret

;; TERM 275: 'Not a dyam instruction ~w'
c_code local build_ref_275
	ret_reg &ref[275]
	call_c   Dyam_Create_Atom("Not a dyam instruction ~w")
	move_ret ref[275]
	c_ret

;; TERM 263: build_ref_ + _D
c_code local build_ref_263
	ret_reg &ref[263]
	call_c   build_ref_338()
	call_c   build_ref_442()
	call_c   Dyam_Create_Binary(&ref[338],&ref[442],V(3))
	move_ret ref[263]
	c_ret

;; TERM 261: '$term'(_D, _E)
c_code local build_ref_261
	ret_reg &ref[261]
	call_c   build_ref_441()
	call_c   Dyam_Create_Binary(&ref[441],V(3),V(4))
	move_ret ref[261]
	c_ret

;; TERM 265: build_seed_ + _D
c_code local build_ref_265
	ret_reg &ref[265]
	call_c   build_ref_338()
	call_c   build_ref_440()
	call_c   Dyam_Create_Binary(&ref[338],&ref[440],V(3))
	move_ret ref[265]
	c_ret

;; TERM 264: '$seed'(_D, _F)
c_code local build_ref_264
	ret_reg &ref[264]
	call_c   build_ref_439()
	call_c   Dyam_Create_Binary(&ref[439],V(3),V(5))
	move_ret ref[264]
	c_ret

;; TERM 262: global(_C, _B)
c_code local build_ref_262
	ret_reg &ref[262]
	call_c   build_ref_451()
	call_c   Dyam_Create_Binary(&ref[451],V(2),V(1))
	move_ret ref[262]
	c_ret

;; TERM 451: global
c_code local build_ref_451
	ret_reg &ref[451]
	call_c   Dyam_Create_Atom("global")
	move_ret ref[451]
	c_ret

;; TERM 258: '$fun_info'{code=> _P, refs=> _Q, ins=> _R}
c_code local build_ref_258
	ret_reg &ref[258]
	call_c   build_ref_452()
	call_c   Dyam_Term_Start(&ref[452],3)
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_End()
	move_ret ref[258]
	c_ret

;; TERM 452: '$fun_info'!'$ft'
c_code local build_ref_452
	ret_reg &ref[452]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_456()
	call_c   Dyam_Create_List(&ref[456],I(0))
	move_ret R(0)
	call_c   build_ref_455()
	call_c   Dyam_Create_List(&ref[455],R(0))
	move_ret R(0)
	call_c   build_ref_454()
	call_c   Dyam_Create_List(&ref[454],R(0))
	move_ret R(0)
	call_c   build_ref_453()
	call_c   Dyam_Create_Atom_Module("$ft",&ref[453])
	move_ret ref[452]
	call_c   DYAM_Feature_2(&ref[453],R(0))
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 453: '$fun_info'
c_code local build_ref_453
	ret_reg &ref[453]
	call_c   Dyam_Create_Atom("$fun_info")
	move_ret ref[453]
	c_ret

;; TERM 454: code
c_code local build_ref_454
	ret_reg &ref[454]
	call_c   Dyam_Create_Atom("code")
	move_ret ref[454]
	c_ret

;; TERM 455: refs
c_code local build_ref_455
	ret_reg &ref[455]
	call_c   Dyam_Create_Atom("refs")
	move_ret ref[455]
	c_ret

;; TERM 456: ins
c_code local build_ref_456
	ret_reg &ref[456]
	call_c   Dyam_Create_Atom("ins")
	move_ret ref[456]
	c_ret

;; TERM 257: '$fun'(_M, _N, _O)
c_code local build_ref_257
	ret_reg &ref[257]
	call_c   build_ref_444()
	call_c   Dyam_Term_Start(&ref[444],3)
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_End()
	move_ret ref[257]
	c_ret

;; TERM 259: [_B|_L]
c_code local build_ref_259
	ret_reg &ref[259]
	call_c   Dyam_Create_List(V(1),V(11))
	move_ret ref[259]
	c_ret

;; TERM 256: ['$term'(_E, _F),'$seed'(_G, _H),'$fun'(_I, _J, _K)]
c_code local build_ref_256
	ret_reg &ref[256]
	call_c   Dyam_Pseudo_Choice(3)
	call_c   build_ref_441()
	call_c   Dyam_Create_Binary(&ref[441],V(4),V(5))
	move_ret R(0)
	call_c   build_ref_439()
	call_c   Dyam_Create_Binary(&ref[439],V(6),V(7))
	move_ret R(1)
	call_c   build_ref_444()
	call_c   Dyam_Term_Start(&ref[444],3)
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_End()
	move_ret R(2)
	call_c   Dyam_Create_List(R(2),I(0))
	move_ret R(2)
	call_c   Dyam_Create_List(R(1),R(2))
	move_ret R(2)
	call_c   Dyam_Create_List(R(0),R(2))
	move_ret ref[256]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 260: [_S|_T]
c_code local build_ref_260
	ret_reg &ref[260]
	call_c   Dyam_Create_List(V(18),V(19))
	move_ret ref[260]
	c_ret

;; TERM 251: deallocate :> _C
c_code local build_ref_251
	ret_reg &ref[251]
	call_c   build_ref_142()
	call_c   Dyam_Create_Binary(I(9),&ref[142],V(2))
	move_ret ref[251]
	c_ret

;; TERM 254: _F :> _H
c_code local build_ref_254
	ret_reg &ref[254]
	call_c   Dyam_Create_Binary(I(9),V(5),V(7))
	move_ret ref[254]
	c_ret

;; TERM 253: allow_allocate_inversion(_F)
c_code local build_ref_253
	ret_reg &ref[253]
	call_c   build_ref_300()
	call_c   Dyam_Create_Unary(&ref[300],V(5))
	move_ret ref[253]
	c_ret

;; TERM 252: _D + 1
c_code local build_ref_252
	ret_reg &ref[252]
	call_c   build_ref_338()
	call_c   Dyam_Create_Binary(&ref[338],V(3),N(1))
	move_ret ref[252]
	c_ret

;; TERM 255: allocate :> _B
c_code local build_ref_255
	ret_reg &ref[255]
	call_c   build_ref_138()
	call_c   Dyam_Create_Binary(I(9),&ref[138],V(1))
	move_ret ref[255]
	c_ret

;; TERM 236: init_pool(_B)
c_code local build_ref_236
	ret_reg &ref[236]
	call_c   build_ref_457()
	call_c   Dyam_Create_Unary(&ref[457],V(1))
	move_ret ref[236]
	c_ret

;; TERM 457: init_pool
c_code local build_ref_457
	ret_reg &ref[457]
	call_c   Dyam_Create_Atom("init_pool")
	move_ret ref[457]
	c_ret

;; TERM 235: ['$term'(_C, _D),'$seed'(_E, _F)]
c_code local build_ref_235
	ret_reg &ref[235]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_441()
	call_c   Dyam_Create_Binary(&ref[441],V(2),V(3))
	move_ret R(0)
	call_c   build_ref_439()
	call_c   Dyam_Create_Binary(&ref[439],V(4),V(5))
	move_ret R(1)
	call_c   Dyam_Create_List(R(1),I(0))
	move_ret R(1)
	call_c   Dyam_Create_List(R(0),R(1))
	move_ret ref[235]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 237: [_J|_K]
c_code local build_ref_237
	ret_reg &ref[237]
	call_c   Dyam_Create_List(V(9),V(10))
	move_ret ref[237]
	c_ret

;; TERM 227: named_function('$fun'(_B, _C, _D), _I, global)
c_code local build_ref_227
	ret_reg &ref[227]
	call_c   build_ref_458()
	call_c   build_ref_215()
	call_c   build_ref_451()
	call_c   Dyam_Term_Start(&ref[458],3)
	call_c   Dyam_Term_Arg(&ref[215])
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(&ref[451])
	call_c   Dyam_Term_End()
	move_ret ref[227]
	c_ret

;; TERM 458: named_function
c_code local build_ref_458
	ret_reg &ref[458]
	call_c   Dyam_Create_Atom("named_function")
	move_ret ref[458]
	c_ret

;; TERM 228: true
c_code local build_ref_228
	ret_reg &ref[228]
	call_c   Dyam_Create_Atom("true")
	move_ret ref[228]
	c_ret

;; TERM 229: named_function('$fun'(_B, _C, _D), _I, _L)
c_code local build_ref_229
	ret_reg &ref[229]
	call_c   build_ref_458()
	call_c   build_ref_215()
	call_c   Dyam_Term_Start(&ref[458],3)
	call_c   Dyam_Term_Arg(&ref[215])
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_End()
	move_ret ref[229]
	c_ret

;; TERM 230: '$derived_fun'('$fun'(_B, _C, _D))
c_code local build_ref_230
	ret_reg &ref[230]
	call_c   build_ref_459()
	call_c   build_ref_215()
	call_c   Dyam_Create_Unary(&ref[459],&ref[215])
	move_ret ref[230]
	c_ret

;; TERM 459: '$derived_fun'
c_code local build_ref_459
	ret_reg &ref[459]
	call_c   Dyam_Create_Atom("$derived_fun")
	move_ret ref[459]
	c_ret

;; TERM 233: ['$label'(_K)]
c_code local build_ref_233
	ret_reg &ref[233]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_460()
	call_c   Dyam_Create_Unary(&ref[460],V(10))
	move_ret R(0)
	call_c   Dyam_Create_List(R(0),I(0))
	move_ret ref[233]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 460: '$label'
c_code local build_ref_460
	ret_reg &ref[460]
	call_c   Dyam_Create_Atom("$label")
	move_ret ref[460]
	c_ret

;; TERM 232: 'Dyam_Pool'
c_code local build_ref_232
	ret_reg &ref[232]
	call_c   Dyam_Create_Atom("Dyam_Pool")
	move_ret ref[232]
	c_ret

;; TERM 231: pool_ + _M
c_code local build_ref_231
	ret_reg &ref[231]
	call_c   build_ref_338()
	call_c   build_ref_446()
	call_c   Dyam_Create_Binary(&ref[338],&ref[446],V(12))
	move_ret ref[231]
	c_ret

;; TERM 234: false
c_code local build_ref_234
	ret_reg &ref[234]
	call_c   Dyam_Create_Atom("false")
	move_ret ref[234]
	c_ret

;; TERM 226: '$fun'(_H, _I, _J)
c_code local build_ref_226
	ret_reg &ref[226]
	call_c   build_ref_444()
	call_c   Dyam_Term_Start(&ref[444],3)
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret ref[226]
	c_ret

;; TERM 214: init_pool('$fun'(_B, _C, _D))
c_code local build_ref_214
	ret_reg &ref[214]
	call_c   build_ref_457()
	call_c   build_ref_215()
	call_c   Dyam_Create_Unary(&ref[457],&ref[215])
	move_ret ref[214]
	c_ret

;; TERM 213: '$fun_info'{code=> _E, refs=> _F, ins=> _G}
c_code local build_ref_213
	ret_reg &ref[213]
	call_c   build_ref_452()
	call_c   Dyam_Term_Start(&ref[452],3)
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[213]
	c_ret

;; TERM 212: [_D|_E]
c_code local build_ref_212
	ret_reg &ref[212]
	call_c   Dyam_Create_List(V(3),V(4))
	move_ret ref[212]
	c_ret

;; TERM 189: '$fun'(_G, _H, _I)
c_code local build_ref_189
	ret_reg &ref[189]
	call_c   build_ref_444()
	call_c   Dyam_Term_Start(&ref[444],3)
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_End()
	move_ret ref[189]
	c_ret

;; TERM 211: '$derived_fun'(_F)
c_code local build_ref_211
	ret_reg &ref[211]
	call_c   build_ref_459()
	call_c   Dyam_Create_Unary(&ref[459],V(5))
	move_ret ref[211]
	c_ret

;; TERM 194: '$pool'('$fun'(_J, _K, _L), _E)
c_code local build_ref_194
	ret_reg &ref[194]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_444()
	call_c   Dyam_Term_Start(&ref[444],3)
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_445()
	call_c   Dyam_Create_Binary(&ref[445],R(0),V(4))
	move_ret ref[194]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 193: ['$fun'(_J, _K, _L)]
c_code local build_ref_193
	ret_reg &ref[193]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_444()
	call_c   Dyam_Term_Start(&ref[444],3)
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   Dyam_Create_List(R(0),I(0))
	move_ret ref[193]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 197: build_ref_ + _M
c_code local build_ref_197
	ret_reg &ref[197]
	call_c   build_ref_338()
	call_c   build_ref_442()
	call_c   Dyam_Create_Binary(&ref[338],&ref[442],V(12))
	move_ret ref[197]
	c_ret

;; TERM 196: ['$term'(_M, _N)]
c_code local build_ref_196
	ret_reg &ref[196]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_441()
	call_c   Dyam_Create_Binary(&ref[441],V(12),V(13))
	move_ret R(0)
	call_c   Dyam_Create_List(R(0),I(0))
	move_ret ref[196]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 199: build_seed + _M
c_code local build_ref_199
	ret_reg &ref[199]
	call_c   build_ref_338()
	call_c   build_ref_461()
	call_c   Dyam_Create_Binary(&ref[338],&ref[461],V(12))
	move_ret ref[199]
	c_ret

;; TERM 461: build_seed
c_code local build_ref_461
	ret_reg &ref[461]
	call_c   Dyam_Create_Atom("build_seed")
	move_ret ref[461]
	c_ret

;; TERM 198: ['$seed'(_M, _O)]
c_code local build_ref_198
	ret_reg &ref[198]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_439()
	call_c   Dyam_Create_Binary(&ref[439],V(12),V(14))
	move_ret R(0)
	call_c   Dyam_Create_List(R(0),I(0))
	move_ret ref[198]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 210: [_V|_U]
c_code local build_ref_210
	ret_reg &ref[210]
	call_c   Dyam_Create_List(V(21),V(20))
	move_ret ref[210]
	c_ret

;; TERM 209: local
c_code local build_ref_209
	ret_reg &ref[209]
	call_c   Dyam_Create_Atom("local")
	move_ret ref[209]
	c_ret

;; TERM 208: _R + _S << 16
c_code local build_ref_208
	ret_reg &ref[208]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_462()
	call_c   Dyam_Create_Binary(&ref[462],V(18),N(16))
	move_ret R(0)
	call_c   build_ref_338()
	call_c   Dyam_Create_Binary(&ref[338],V(17),R(0))
	move_ret ref[208]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 462: <<
c_code local build_ref_462
	ret_reg &ref[462]
	call_c   Dyam_Create_Atom("<<")
	move_ret ref[462]
	c_ret

;; TERM 195: '$pool'(_C, _E)
c_code local build_ref_195
	ret_reg &ref[195]
	call_c   build_ref_445()
	call_c   Dyam_Create_Binary(&ref[445],V(2),V(4))
	move_ret ref[195]
	c_ret

;; TERM 204: pool_ + _C
c_code local build_ref_204
	ret_reg &ref[204]
	call_c   build_ref_338()
	call_c   build_ref_446()
	call_c   Dyam_Create_Binary(&ref[338],&ref[446],V(2))
	move_ret ref[204]
	c_ret

;; TERM 203: _R + _S + 1
c_code local build_ref_203
	ret_reg &ref[203]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_338()
	call_c   Dyam_Create_Binary(&ref[338],V(17),V(18))
	move_ret R(0)
	call_c   Dyam_Create_Binary(&ref[338],R(0),N(1))
	move_ret ref[203]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 184: _E :> _F
c_code local build_ref_184
	ret_reg &ref[184]
	call_c   Dyam_Create_Binary(I(9),V(4),V(5))
	move_ret ref[184]
	c_ret

;; TERM 174: _C :> _D
c_code local build_ref_174
	ret_reg &ref[174]
	call_c   Dyam_Create_Binary(I(9),V(2),V(3))
	move_ret ref[174]
	c_ret

;; TERM 173: '$reg'(_E)
c_code local build_ref_173
	ret_reg &ref[173]
	call_c   build_ref_433()
	call_c   Dyam_Create_Unary(&ref[433],V(4))
	move_ret ref[173]
	c_ret

;; TERM 172: _B + 1
c_code local build_ref_172
	ret_reg &ref[172]
	call_c   build_ref_338()
	call_c   Dyam_Create_Binary(&ref[338],V(1),N(1))
	move_ret ref[172]
	c_ret

;; TERM 131: stop_inst(_B, _C, _E, _D)
c_code local build_ref_131
	ret_reg &ref[131]
	call_c   build_ref_309()
	call_c   Dyam_Term_Start(&ref[309],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[131]
	c_ret

;; TERM 135: _F :> _G :> _H
c_code local build_ref_135
	ret_reg &ref[135]
	call_c   build_ref_134()
	call_c   Dyam_Create_Binary(I(9),V(5),&ref[134])
	move_ret ref[135]
	c_ret

;; TERM 134: _G :> _H
c_code local build_ref_134
	ret_reg &ref[134]
	call_c   Dyam_Create_Binary(I(9),V(6),V(7))
	move_ret ref[134]
	c_ret

;; TERM 133: (_F :> _G) :> _H
c_code local build_ref_133
	ret_reg &ref[133]
	call_c   build_ref_132()
	call_c   Dyam_Create_Binary(I(9),&ref[132],V(7))
	move_ret ref[133]
	c_ret

;; TERM 132: _F :> _G
c_code local build_ref_132
	ret_reg &ref[132]
	call_c   Dyam_Create_Binary(I(9),V(5),V(6))
	move_ret ref[132]
	c_ret

;; TERM 137: noop :> _F
c_code local build_ref_137
	ret_reg &ref[137]
	call_c   build_ref_136()
	call_c   Dyam_Create_Binary(I(9),&ref[136],V(5))
	move_ret ref[137]
	c_ret

;; TERM 139: allocate :> _F
c_code local build_ref_139
	ret_reg &ref[139]
	call_c   build_ref_138()
	call_c   Dyam_Create_Binary(I(9),&ref[138],V(5))
	move_ret ref[139]
	c_ret

;; TERM 146: deallocate :> pl_jump(_E, _K)
c_code local build_ref_146
	ret_reg &ref[146]
	call_c   build_ref_142()
	call_c   build_ref_145()
	call_c   Dyam_Create_Binary(I(9),&ref[142],&ref[145])
	move_ret ref[146]
	c_ret

;; TERM 145: pl_jump(_E, _K)
c_code local build_ref_145
	ret_reg &ref[145]
	call_c   build_ref_415()
	call_c   Dyam_Create_Binary(&ref[415],V(4),V(10))
	move_ret ref[145]
	c_ret

;; TERM 149: reg_deallocate(_M) :> pl_jump(_E, _K)
c_code local build_ref_149
	ret_reg &ref[149]
	call_c   build_ref_147()
	call_c   build_ref_145()
	call_c   Dyam_Create_Binary(I(9),&ref[147],&ref[145])
	move_ret ref[149]
	c_ret

;; TERM 147: reg_deallocate(_M)
c_code local build_ref_147
	ret_reg &ref[147]
	call_c   build_ref_329()
	call_c   Dyam_Create_Unary(&ref[329],V(12))
	move_ret ref[147]
	c_ret

;; TERM 148: reg_deallocate(_M) :> succeed
c_code local build_ref_148
	ret_reg &ref[148]
	call_c   build_ref_147()
	call_c   build_ref_143()
	call_c   Dyam_Create_Binary(I(9),&ref[147],&ref[143])
	move_ret ref[148]
	c_ret

;; TERM 150: call(_E, _K) :> _I
c_code local build_ref_150
	ret_reg &ref[150]
	call_c   build_ref_140()
	call_c   Dyam_Create_Binary(I(9),&ref[140],V(8))
	move_ret ref[150]
	c_ret

;; TERM 140: call(_E, _K)
c_code local build_ref_140
	ret_reg &ref[140]
	call_c   build_ref_414()
	call_c   Dyam_Create_Binary(&ref[414],V(4),V(10))
	move_ret ref[140]
	c_ret

;; TERM 141: call(_E, _K) :> _F
c_code local build_ref_141
	ret_reg &ref[141]
	call_c   build_ref_140()
	call_c   Dyam_Create_Binary(I(9),&ref[140],V(5))
	move_ret ref[141]
	c_ret

;; TERM 157: update_choice(_E) :> _G
c_code local build_ref_157
	ret_reg &ref[157]
	call_c   build_ref_156()
	call_c   Dyam_Create_Binary(I(9),&ref[156],V(6))
	move_ret ref[157]
	c_ret

;; TERM 156: update_choice(_E)
c_code local build_ref_156
	ret_reg &ref[156]
	call_c   build_ref_311()
	call_c   Dyam_Create_Unary(&ref[311],V(4))
	move_ret ref[156]
	c_ret

;; TERM 155: choice(_E) :> _G
c_code local build_ref_155
	ret_reg &ref[155]
	call_c   build_ref_154()
	call_c   Dyam_Create_Binary(I(9),&ref[154],V(6))
	move_ret ref[155]
	c_ret

;; TERM 154: choice(_E)
c_code local build_ref_154
	ret_reg &ref[154]
	call_c   build_ref_313()
	call_c   Dyam_Create_Unary(&ref[313],V(4))
	move_ret ref[154]
	c_ret

;; TERM 158: remove_choice :> _I
c_code local build_ref_158
	ret_reg &ref[158]
	call_c   build_ref_152()
	call_c   Dyam_Create_Binary(I(9),&ref[152],V(8))
	move_ret ref[158]
	c_ret

;; TERM 153: remove_choice :> _F
c_code local build_ref_153
	ret_reg &ref[153]
	call_c   build_ref_152()
	call_c   Dyam_Create_Binary(I(9),&ref[152],V(5))
	move_ret ref[153]
	c_ret

;; TERM 160: call(_E, _K) :> _G
c_code local build_ref_160
	ret_reg &ref[160]
	call_c   build_ref_140()
	call_c   Dyam_Create_Binary(I(9),&ref[140],V(6))
	move_ret ref[160]
	c_ret

;; TERM 159: pl_inst(_F, _E, _K)
c_code local build_ref_159
	ret_reg &ref[159]
	call_c   build_ref_418()
	call_c   Dyam_Term_Start(&ref[418],3)
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_End()
	move_ret ref[159]
	c_ret

;; TERM 164: notest(_E, _K) :> fail
c_code local build_ref_164
	ret_reg &ref[164]
	call_c   build_ref_163()
	call_c   build_ref_161()
	call_c   Dyam_Create_Binary(I(9),&ref[163],&ref[161])
	move_ret ref[164]
	c_ret

;; TERM 163: notest(_E, _K)
c_code local build_ref_163
	ret_reg &ref[163]
	call_c   build_ref_449()
	call_c   Dyam_Create_Binary(&ref[449],V(4),V(10))
	move_ret ref[163]
	c_ret

;; TERM 162: test_inst(_F, _E, _K)
c_code local build_ref_162
	ret_reg &ref[162]
	call_c   build_ref_321()
	call_c   Dyam_Term_Start(&ref[321],3)
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_End()
	move_ret ref[162]
	c_ret

;; TERM 151: _L + 1
c_code local build_ref_151
	ret_reg &ref[151]
	call_c   build_ref_338()
	call_c   Dyam_Create_Binary(&ref[338],V(11),N(1))
	move_ret ref[151]
	c_ret

;; TERM 165: _F :> _N
c_code local build_ref_165
	ret_reg &ref[165]
	call_c   Dyam_Create_Binary(I(9),V(5),V(13))
	move_ret ref[165]
	c_ret

;; TERM 166: 'Code optimize'
c_code local build_ref_166
	ret_reg &ref[166]
	call_c   Dyam_Create_Atom("Code optimize")
	move_ret ref[166]
	c_ret

;; TERM 117: named_function('$fun'(_B, 0, _C), _E, global)
c_code local build_ref_117
	ret_reg &ref[117]
	call_c   build_ref_458()
	call_c   build_ref_116()
	call_c   build_ref_451()
	call_c   Dyam_Term_Start(&ref[458],3)
	call_c   Dyam_Term_Arg(&ref[116])
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(&ref[451])
	call_c   Dyam_Term_End()
	move_ret ref[117]
	c_ret

;; TERM 116: '$fun'(_B, 0, _C)
c_code local build_ref_116
	ret_reg &ref[116]
	call_c   build_ref_444()
	call_c   Dyam_Term_Start(&ref[444],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(N(0))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_End()
	move_ret ref[116]
	c_ret

;; TERM 24: global(_B, _C)
c_code local build_ref_24
	ret_reg &ref[24]
	call_c   build_ref_451()
	call_c   Dyam_Create_Binary(&ref[451],V(1),V(2))
	move_ret ref[24]
	c_ret

pl_code local fun16
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Object(R(0))
	call_c   Dyam_Tabule()
	pl_ret

pl_code local fun17
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Choice(fun16)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Subsume(R(0))
	fail_ret
	call_c   Dyam_Cut()
	pl_ret

pl_code local fun36
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Object(R(0))
	call_c   Dyam_Tabule()
	pl_ret

pl_code local fun118
	call_c   Dyam_Remove_Choice()
	move     &ref[275], R(0)
	move     0, R(1)
	move     &ref[276], R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_internal_error_2()

pl_code local fun119
	call_c   Dyam_Update_Choice(fun118)
	call_c   Dyam_Set_Cut()
	pl_call  fun43(&seed[112],1)
	call_c   Dyam_Cut()
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun120
	call_c   Dyam_Update_Choice(fun119)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[271])
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(3))
	call_c   Dyam_Reg_Load(2,V(4))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_ma_emit_pl_call_2()

pl_code local fun121
	call_c   Dyam_Update_Choice(fun120)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[270])
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(3))
	call_c   Dyam_Reg_Load(2,V(4))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_ma_emit_call_c_2()

pl_code local fun122
	call_c   Dyam_Update_Choice(fun121)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[269])
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(3))
	call_c   Dyam_Reg_Load(2,V(4))
	pl_call  pred_ma_emit_call_c_2()
	call_c   Dyam_Reg_Deallocate(0)
	pl_jump  pred_ma_emit_fail_ret_0()

pl_code local fun123
	call_c   Dyam_Update_Choice(fun122)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[268])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(3))
	call_c   Dyam_Reg_Load(2,V(4))
	pl_call  pred_ma_emit_call_c_2()
	call_c   Dyam_Reg_Deallocate(0)
	pl_jump  pred_ma_emit_true_ret_0()

pl_code local fun124
	call_c   Dyam_Update_Choice(fun123)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[267])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(3))
	call_c   Dyam_Reg_Load(2,V(4))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_ma_emit_call_c_2()

pl_code local fun125
	call_c   Dyam_Update_Choice(fun124)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[266])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun109
	call_c   Dyam_Remove_Choice()
fun108:
	call_c   Dyam_Cut()
	move     &ref[263], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	pl_call  pred_ma_emit_call_c_2()
	call_c   Dyam_Reg_Load(0,V(2))
	call_c   Dyam_Reg_Load(2,V(1))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_register_global_2()


pl_code local fun111
	call_c   Dyam_Remove_Choice()
fun110:
	call_c   Dyam_Cut()
	move     &ref[265], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	pl_call  pred_ma_emit_call_c_2()
	call_c   Dyam_Reg_Load(0,V(2))
	call_c   Dyam_Reg_Load(2,V(1))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_register_global_2()


pl_code local fun113
	call_c   Dyam_Remove_Choice()
fun112:
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(2))
	call_c   Dyam_Reg_Load(2,V(1))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_register_global_2()


pl_code local fun114
	call_c   Dyam_Update_Choice(fun11)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[237])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun11)
	pl_call  Domain_2(V(11),V(1))
	call_c   Dyam_Reg_Load(0,V(11))
	call_c   Dyam_Reg_Load(2,V(2))
	pl_call  pred_emit_globals_2()
	pl_fail

pl_code local fun115
	call_c   Dyam_Update_Choice(fun114)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[189])
	fail_ret
	call_c   Dyam_Choice(fun113)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[262])
	call_c   Dyam_Cut()
	pl_fail

pl_code local fun116
	call_c   Dyam_Update_Choice(fun115)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[264])
	fail_ret
	call_c   Dyam_Choice(fun111)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[262])
	call_c   Dyam_Cut()
	pl_fail

pl_code local fun105
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Reg_Load_Ptr(2,V(2))
	fail_ret
	call_c   Dyam_Reg_Load(4,&ref[259])
	call_c   DyALog_Mutable_Write(R(2),&R(4),0)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(2))
	fail_ret
	pl_jump  fun104()

pl_code local fun106
	call_c   Dyam_Update_Choice(fun11)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[260])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun11)
	pl_call  Domain_2(V(20),V(1))
	call_c   Dyam_Reg_Load(0,V(20))
	call_c   Dyam_Reg_Load(2,V(2))
	call_c   Dyam_Reg_Load(4,V(3))
	pl_call  pred_code_refs_add_3()
	pl_fail

pl_code local fun100
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(2),&ref[255])
	fail_ret
	call_c   DYAM_evpred_is(V(4),&ref[252])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun101
	call_c   Dyam_Update_Choice(fun100)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[132])
	fail_ret
	pl_call  Object_1(&ref[253])
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(2),&ref[254])
	fail_ret
	call_c   Dyam_Reg_Load(0,V(6))
	call_c   Dyam_Reg_Load(2,V(7))
	call_c   Dyam_Reg_Load(4,V(3))
	call_c   Dyam_Reg_Load(6,V(4))
	call_c   Dyam_Reg_Deallocate(4)
	pl_jump  pred_code_optimize_allocate_4()

pl_code local fun102
	call_c   Dyam_Update_Choice(fun101)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(1),&ref[161])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(2),&ref[161])
	fail_ret
	call_c   DYAM_evpred_is(V(4),&ref[252])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun90
	call_c   Dyam_Update_Choice(fun11)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[237])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun11)
	pl_call  Domain_2(V(11),V(1))
	call_c   Dyam_Reg_Load(0,V(11))
	pl_call  pred_emit_globals_in_init_pool_1()
	pl_fail

pl_code local fun91
	call_c   Dyam_Update_Choice(fun90)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[189])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Deallocate(1)
	pl_jump  pred_register_init_pool_fun_1()

pl_code local fun87
	call_c   Dyam_Remove_Choice()
fun86:
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(9),&ref[228])
	fail_ret
fun85:
	call_c   Dyam_Choice(fun84)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[214])
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun83)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(9),&ref[228])
	fail_ret
	call_c   Dyam_Cut()
fun82:
	call_c   Dyam_Choice(fun81)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(9),&ref[228])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Deallocate()
	pl_ret




pl_code local fun83
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Reg_Load(0,V(5))
	pl_call  pred_emit_globals_in_init_pool_1()
	pl_jump  fun82()

pl_code local fun76
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Reg_Load(0,V(10))
	move     I(0), R(2)
	move     0, R(3)
	pl_call  pred_ma_emit_call_c_2()
	pl_jump  fun69()

pl_code local fun78
	call_c   Dyam_Remove_Choice()
fun77:
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun76)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(10),&ref[231])
	fail_ret
	call_c   Dyam_Cut()
	move     &ref[232], R(0)
	move     0, R(1)
	move     &ref[233], R(2)
	move     S(5), R(3)
	pl_call  pred_ma_emit_call_c_2()
	pl_jump  fun69()


pl_code local fun80
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(11),&ref[209])
	fail_ret
fun79:
	call_c   Dyam_Reg_Load(0,V(11))
	move     &ref[215], R(2)
	move     S(5), R(3)
	pl_call  pred_ma_emit_pl_code_2()
	call_c   Dyam_Choice(fun70)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_notvar(V(10))
	fail_ret
	call_c   Dyam_Choice(fun78)
	call_c   Dyam_Set_Cut()
	pl_call  Object_2(&ref[230],V(13))
	call_c   DYAM_evpred_retract(&ref[230])
	fail_ret
	call_c   Dyam_Cut()
	pl_fail


pl_code local fun81
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Reg_Load_Ptr(2,V(3))
	fail_ret
	move     I(0), R(4)
	move     0, R(5)
	call_c   DyALog_Mutable_Write(R(2),&R(4),0)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(3))
	fail_ret
	call_c   Dyam_Choice(fun80)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[229])
	call_c   Dyam_Cut()
	pl_jump  fun79()

pl_code local fun84
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Reg_Load(0,V(5))
	move     &ref[215], R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Load(4,V(6))
	move     V(10), R(6)
	move     S(5), R(7)
	pl_call  pred_emit_pool_4()
	pl_jump  fun82()

pl_code local fun88
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(9),&ref[234])
	fail_ret
	pl_jump  fun85()

pl_code local fun74
	call_c   Dyam_Remove_Choice()
	call_c   DYAM_evpred_assert_1(&ref[214])
	call_c   Dyam_Reg_Load_Ptr(2,V(3))
	fail_ret
	call_c   Dyam_Reg_Load(4,&ref[213])
	call_c   DyALog_Mutable_Read(R(2),&R(4))
	fail_ret
	call_c   Dyam_Reg_Load(0,V(5))
	pl_call  pred_emit_globals_in_init_pool_1()
	call_c   Dyam_Choice(fun11)
	pl_call  Domain_2(&ref[226],V(5))
	move     &ref[226], R(0)
	move     S(5), R(1)
	pl_call  pred_register_init_pool_fun_1()
	pl_fail

pl_code local fun70
	call_c   Dyam_Remove_Choice()
	pl_jump  fun69()

pl_code local fun67
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Reg_Load_Ptr(2,V(2))
	fail_ret
	call_c   Dyam_Reg_Load(4,&ref[212])
	call_c   DyALog_Mutable_Write(R(2),&R(4),0)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(2))
	fail_ret
	pl_fail

pl_code local fun63
	call_c   Dyam_Remove_Choice()
fun62:
	call_c   Dyam_Cut()
fun61:
	pl_call  fun43(&seed[108],1)
	pl_fail



pl_code local fun64
	call_c   Dyam_Remove_Choice()
	move     &ref[211], R(0)
	move     S(5), R(1)
	pl_call  pred_record_without_doublon_1()
	pl_jump  fun61()

pl_code local fun55
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(4),&ref[204])
	fail_ret
	move     &ref[195], R(0)
	move     S(5), R(1)
	pl_call  pred_record_without_doublon_1()
	pl_call  fun43(&seed[110],1)
	call_c   DYAM_evpred_is(V(21),&ref[208])
	fail_ret
	move     &ref[209], R(0)
	move     0, R(1)
	call_c   Dyam_Reg_Load(2,V(4))
	call_c   Dyam_Reg_Load(4,V(19))
	move     &ref[210], R(6)
	move     S(5), R(7)
	pl_call  pred_ma_emit_table_4()
	call_c   DYAM_Nl_0()
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun56
	call_c   Dyam_Remove_Choice()
	pl_call  fun43(&seed[109],1)
	call_c   DYAM_evpred_length(V(15),V(17))
	fail_ret
	call_c   DYAM_evpred_length(V(16),V(18))
	fail_ret
	call_c   DYAM_evpred_is(V(19),&ref[203])
	fail_ret
	call_c   Dyam_Choice(fun55)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(19),N(1))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun57
	call_c   Dyam_Update_Choice(fun56)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[198])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(4),&ref[199])
	fail_ret
	move     &ref[195], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Deallocate(1)
	pl_jump  pred_record_without_doublon_1()

pl_code local fun58
	call_c   Dyam_Update_Choice(fun57)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[196])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(4),&ref[197])
	fail_ret
	move     &ref[195], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Deallocate(1)
	pl_jump  pred_record_without_doublon_1()

pl_code local fun59
	call_c   Dyam_Update_Choice(fun58)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[193])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun11)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[194])
	call_c   Dyam_Cut()
	move     &ref[195], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Deallocate(1)
	pl_jump  pred_record_without_doublon_1()

pl_code local fun65
	call_c   Dyam_Remove_Choice()
fun60:
	call_c   Dyam_Choice(fun59)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(1),I(0))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Deallocate()
	pl_ret


pl_code local fun47
	call_c   Dyam_Remove_Choice()
fun46:
	call_c   Dyam_Choice(fun11)
	call_c   Dyam_Reg_Load(0,V(4))
	move     V(6), R(2)
	move     S(5), R(3)
	move     V(7), R(4)
	move     S(5), R(5)
	pl_call  pred_foreign_arg_3()
	call_c   Dyam_Reg_Load(0,V(7))
	call_c   Dyam_Reg_Load(2,V(2))
	call_c   Dyam_Reg_Load(4,V(3))
	pl_call  pred_code_refs_add_3()
	pl_fail


pl_code local fun48
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(1),V(4))
	fail_ret
	pl_jump  fun46()

pl_code local fun39
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Deallocate(1)
	pl_jump  pred_emit_code_elem_1()

pl_code local fun20
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(2),&ref[150])
	fail_ret
	call_c   DYAM_evpred_is(V(3),&ref[151])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun21
	call_c   Dyam_Update_Choice(fun20)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(8),&ref[143])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(2),&ref[145])
	fail_ret
	call_c   Dyam_Unify(V(3),N(1))
	fail_ret
	call_c   Dyam_Reg_Load(0,V(4))
	call_c   Dyam_Reg_Deallocate(1)
	pl_jump  pred_jump_inc_1()

pl_code local fun22
	call_c   Dyam_Update_Choice(fun21)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(8),&ref[148])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(2),&ref[149])
	fail_ret
	call_c   Dyam_Unify(V(3),N(2))
	fail_ret
	call_c   Dyam_Reg_Load(0,V(4))
	call_c   Dyam_Reg_Deallocate(1)
	pl_jump  pred_jump_inc_1()

pl_code local fun23
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(2),&ref[158])
	fail_ret
	call_c   DYAM_evpred_is(V(3),&ref[151])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun24
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(2),&ref[165])
	fail_ret
	call_c   DYAM_evpred_is(V(3),&ref[151])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun25
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(2),V(1))
	fail_ret
	call_c   Dyam_Unify(V(3),N(1))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun26
	call_c   Dyam_Update_Choice(fun25)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[132])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(6))
	move     V(13), R(2)
	move     S(5), R(3)
	move     V(11), R(4)
	move     S(5), R(5)
	pl_call  pred_code_optimize_3()
	call_c   Dyam_Choice(fun24)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(13),&ref[161])
	fail_ret
	pl_call  Object_1(&ref[162])
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(2),&ref[164])
	fail_ret
	call_c   Dyam_Unify(V(3),N(2))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun27
	call_c   Dyam_Update_Choice(fun26)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[132])
	fail_ret
	pl_call  Object_1(&ref[159])
	call_c   Dyam_Cut()
	move     &ref[160], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(2))
	call_c   Dyam_Reg_Load(4,V(3))
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  pred_code_optimize_3()

pl_code local fun28
	call_c   Dyam_Update_Choice(fun27)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[153])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(5))
	move     V(8), R(2)
	move     S(5), R(3)
	move     V(11), R(4)
	move     S(5), R(5)
	pl_call  pred_code_optimize_3()
	call_c   Dyam_Choice(fun23)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(8),&ref[155])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(2),&ref[157])
	fail_ret
	call_c   DYAM_evpred_is(V(3),V(11))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun29
	call_c   Dyam_Update_Choice(fun28)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[141])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(5))
	move     V(8), R(2)
	move     S(5), R(3)
	move     V(11), R(4)
	move     S(5), R(5)
	pl_call  pred_code_optimize_3()
	call_c   Dyam_Choice(fun22)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(8),&ref[144])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(2),&ref[146])
	fail_ret
	call_c   Dyam_Unify(V(3),N(2))
	fail_ret
	call_c   Dyam_Reg_Load(0,V(4))
	call_c   Dyam_Reg_Deallocate(1)
	pl_jump  pred_jump_inc_1()

pl_code local fun30
	call_c   Dyam_Update_Choice(fun29)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[139])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(5))
	move     V(8), R(2)
	move     S(5), R(3)
	move     V(9), R(4)
	move     S(5), R(5)
	pl_call  pred_code_optimize_3()
	call_c   Dyam_Reg_Load(0,V(8))
	call_c   Dyam_Reg_Load(2,V(2))
	call_c   Dyam_Reg_Load(4,V(9))
	call_c   Dyam_Reg_Load(6,V(3))
	call_c   Dyam_Reg_Deallocate(4)
	pl_jump  pred_code_optimize_allocate_4()

pl_code local fun31
	call_c   Dyam_Update_Choice(fun30)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[137])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(5))
	call_c   Dyam_Reg_Load(2,V(2))
	call_c   Dyam_Reg_Load(4,V(3))
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  pred_code_optimize_3()

pl_code local fun32
	call_c   Dyam_Update_Choice(fun31)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[133])
	fail_ret
	call_c   Dyam_Cut()
	move     &ref[135], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(2))
	call_c   Dyam_Reg_Load(4,V(3))
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  pred_code_optimize_3()

pl_code local fun34
	call_c   Dyam_Remove_Choice()
	move     &ref[166], R(0)
	move     0, R(1)
	move     I(0), R(2)
	move     0, R(3)
	pl_call  pred_internal_error_2()
	pl_jump  fun33()

pl_code local fun11
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun13
	call_c   Dyam_Remove_Choice()
fun12:
	move     &ref[116], R(0)
	move     S(5), R(1)
	move     V(3), R(2)
	move     S(5), R(3)
	pl_call  pred_jump_value_2()
	call_c   DYAM_evpred_gt(V(3),N(0))
	fail_ret
	call_c   Dyam_Choice(fun11)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[117])
	call_c   Dyam_Cut()
	pl_fail



;;------------------ INIT POOL ------------

c_code local build_init_pool
	call_c   build_seed_19()
	call_c   build_seed_20()
	call_c   build_seed_21()
	call_c   build_seed_22()
	call_c   build_seed_23()
	call_c   build_seed_24()
	call_c   build_seed_25()
	call_c   build_seed_26()
	call_c   build_seed_27()
	call_c   build_seed_28()
	call_c   build_seed_29()
	call_c   build_seed_30()
	call_c   build_seed_31()
	call_c   build_seed_32()
	call_c   build_seed_33()
	call_c   build_seed_34()
	call_c   build_seed_35()
	call_c   build_seed_36()
	call_c   build_seed_37()
	call_c   build_seed_46()
	call_c   build_seed_47()
	call_c   build_seed_48()
	call_c   build_seed_38()
	call_c   build_seed_39()
	call_c   build_seed_40()
	call_c   build_seed_41()
	call_c   build_seed_42()
	call_c   build_seed_43()
	call_c   build_seed_44()
	call_c   build_seed_45()
	call_c   build_seed_49()
	call_c   build_seed_50()
	call_c   build_seed_51()
	call_c   build_seed_52()
	call_c   build_seed_53()
	call_c   build_seed_54()
	call_c   build_seed_57()
	call_c   build_seed_58()
	call_c   build_seed_55()
	call_c   build_seed_56()
	call_c   build_seed_59()
	call_c   build_seed_60()
	call_c   build_seed_61()
	call_c   build_seed_62()
	call_c   build_seed_63()
	call_c   build_seed_64()
	call_c   build_seed_65()
	call_c   build_seed_66()
	call_c   build_seed_67()
	call_c   build_seed_68()
	call_c   build_seed_69()
	call_c   build_seed_70()
	call_c   build_seed_71()
	call_c   build_seed_72()
	call_c   build_seed_73()
	call_c   build_seed_74()
	call_c   build_seed_75()
	call_c   build_seed_76()
	call_c   build_seed_77()
	call_c   build_seed_78()
	call_c   build_seed_79()
	call_c   build_seed_80()
	call_c   build_seed_81()
	call_c   build_seed_82()
	call_c   build_seed_83()
	call_c   build_seed_84()
	call_c   build_seed_85()
	call_c   build_seed_86()
	call_c   build_seed_87()
	call_c   build_seed_88()
	call_c   build_seed_89()
	call_c   build_seed_90()
	call_c   build_seed_91()
	call_c   build_seed_92()
	call_c   build_seed_93()
	call_c   build_seed_94()
	call_c   build_seed_95()
	call_c   build_seed_96()
	call_c   build_seed_97()
	call_c   build_seed_98()
	call_c   build_seed_99()
	call_c   build_seed_100()
	call_c   build_seed_101()
	call_c   build_seed_102()
	call_c   build_seed_103()
	call_c   build_seed_104()
	call_c   build_seed_105()
	call_c   build_seed_106()
	call_c   build_seed_1()
	call_c   build_seed_0()
	call_c   build_seed_2()
	call_c   build_seed_11()
	call_c   build_seed_12()
	call_c   build_seed_13()
	call_c   build_seed_10()
	call_c   build_seed_14()
	call_c   build_seed_7()
	call_c   build_seed_15()
	call_c   build_seed_4()
	call_c   build_seed_6()
	call_c   build_seed_5()
	call_c   build_seed_17()
	call_c   build_seed_18()
	call_c   build_seed_8()
	call_c   build_seed_3()
	call_c   build_seed_9()
	call_c   build_seed_16()
	call_c   build_seed_112()
	call_c   build_seed_108()
	call_c   build_seed_110()
	call_c   build_seed_109()
	call_c   build_ref_136()
	call_c   build_ref_266()
	call_c   build_ref_267()
	call_c   build_ref_268()
	call_c   build_ref_269()
	call_c   build_ref_270()
	call_c   build_ref_271()
	call_c   build_ref_276()
	call_c   build_ref_275()
	call_c   build_ref_263()
	call_c   build_ref_261()
	call_c   build_ref_265()
	call_c   build_ref_264()
	call_c   build_ref_262()
	call_c   build_ref_258()
	call_c   build_ref_257()
	call_c   build_ref_259()
	call_c   build_ref_256()
	call_c   build_ref_260()
	call_c   build_ref_251()
	call_c   build_ref_254()
	call_c   build_ref_253()
	call_c   build_ref_252()
	call_c   build_ref_255()
	call_c   build_ref_236()
	call_c   build_ref_235()
	call_c   build_ref_237()
	call_c   build_ref_227()
	call_c   build_ref_228()
	call_c   build_ref_229()
	call_c   build_ref_230()
	call_c   build_ref_233()
	call_c   build_ref_232()
	call_c   build_ref_231()
	call_c   build_ref_234()
	call_c   build_ref_226()
	call_c   build_ref_214()
	call_c   build_ref_213()
	call_c   build_ref_215()
	call_c   build_ref_212()
	call_c   build_ref_189()
	call_c   build_ref_211()
	call_c   build_ref_194()
	call_c   build_ref_193()
	call_c   build_ref_197()
	call_c   build_ref_196()
	call_c   build_ref_199()
	call_c   build_ref_198()
	call_c   build_ref_210()
	call_c   build_ref_209()
	call_c   build_ref_208()
	call_c   build_ref_195()
	call_c   build_ref_204()
	call_c   build_ref_203()
	call_c   build_ref_184()
	call_c   build_ref_174()
	call_c   build_ref_173()
	call_c   build_ref_111()
	call_c   build_ref_172()
	call_c   build_ref_131()
	call_c   build_ref_135()
	call_c   build_ref_133()
	call_c   build_ref_137()
	call_c   build_ref_139()
	call_c   build_ref_146()
	call_c   build_ref_144()
	call_c   build_ref_149()
	call_c   build_ref_148()
	call_c   build_ref_145()
	call_c   build_ref_143()
	call_c   build_ref_150()
	call_c   build_ref_141()
	call_c   build_ref_157()
	call_c   build_ref_155()
	call_c   build_ref_158()
	call_c   build_ref_153()
	call_c   build_ref_160()
	call_c   build_ref_159()
	call_c   build_ref_164()
	call_c   build_ref_162()
	call_c   build_ref_161()
	call_c   build_ref_151()
	call_c   build_ref_165()
	call_c   build_ref_132()
	call_c   build_ref_166()
	call_c   build_ref_117()
	call_c   build_ref_116()
	call_c   build_ref_24()
	c_ret

long local ref[463]
long local seed[113]

long local _initialization

c_code global initialization_dyalog_code_5Femit
	call_c   Dyam_Check_And_Update_Initialization(_initialization)
	fail_c_ret
	call_c   initialization_dyalog_format()
	call_c   build_init_pool()
	call_c   build_viewers()
	call_c   load()
	c_ret

