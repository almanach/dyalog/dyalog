;; Compiler: DyALog 1.14.0
;; File "toto.tag"


;;------------------ LOADING ------------

c_code local load
	call_c   Dyam_Loading_Set()
	pl_call  fun202()
	pl_call  fun197()
	pl_call  fun192()
	pl_call  fun122()
	pl_call  fun117()
	pl_call  fun112()
	pl_call  fun107()
	pl_call  fun102()
	pl_call  fun14(&seed[12],0)
	pl_call  fun14(&seed[2],0)
	pl_call  fun14(&seed[1],0)
	pl_call  fun14(&seed[3],0)
	pl_call  fun14(&seed[0],0)
	pl_call  fun6(&seed[13],0)
	pl_call  fun3(&seed[26],0)
	pl_call  fun3(&seed[25],0)
	pl_call  fun3(&seed[24],0)
	pl_call  fun3(&seed[23],0)
	pl_call  fun3(&seed[22],0)
	pl_call  fun3(&seed[21],0)
	pl_call  fun3(&seed[20],0)
	pl_call  fun3(&seed[19],0)
	pl_call  fun3(&seed[18],0)
	pl_call  fun3(&seed[17],0)
	pl_call  fun3(&seed[16],0)
	pl_call  fun3(&seed[15],0)
	pl_call  fun3(&seed[14],0)
	call_c   Dyam_Loading_Reset()
	c_ret

pl_code global pred_anchor_7
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind(2,2,4)
	call_c   Dyam_Reg_Unify(10,V(3))
	fail_ret
	pl_call  Object_1(&ref[156])
	pl_call  Object_1(&ref[157])
	call_c   Dyam_Deallocate()
	pl_ret

pl_code global pred_tag_family_load_7
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind(10,6,2)
	call_c   DYAM_Term_Range_3(V(7),V(8),V(6))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret


;;------------------ VIEWERS ------------

c_code local build_viewers
	call_c   Dyam_Load_Viewer(&ref[100],&ref[103])
	call_c   Dyam_Load_Viewer(&ref[97],&ref[99])
	call_c   Dyam_Load_Viewer(&ref[95],&ref[96])
	call_c   Dyam_Load_Viewer(&ref[76],&ref[79])
	call_c   Dyam_Load_Viewer(&ref[67],&ref[70])
	call_c   Dyam_Load_Viewer(&ref[63],&ref[66])
	call_c   Dyam_Load_Viewer(&ref[51],&ref[54])
	call_c   Dyam_Load_Viewer(&ref[50],&ref[47])
	call_c   Dyam_Load_Viewer(&ref[45],&ref[48])
	call_c   Dyam_Load_Viewer(&ref[36],&ref[38])
	call_c   Dyam_Load_Viewer(&ref[34],&ref[35])
	call_c   Dyam_Load_Viewer(&ref[15],&ref[18])
	call_c   Dyam_Load_Viewer(&ref[10],&ref[14])
	call_c   Dyam_Load_Viewer(&ref[9],&ref[8])
	call_c   Dyam_Load_Viewer(&ref[7],&ref[6])
	call_c   Dyam_Load_Viewer(&ref[5],&ref[4])
	call_c   Dyam_Load_Viewer(&ref[3],&ref[1])
	c_ret


;;------------------ DIRECTIVES ------------

c_code global main_directive_initialization
	call_c   build_init_pool()
	call_c   DYAM_Feature_2(&ref[566],&ref[567])
	c_ret

c_code local build_seed_14
	ret_reg &seed[14]
	call_c   build_ref_118()
	call_c   build_ref_119()
	call_c   Dyam_Seed_Start(&ref[118],&ref[119],&ref[119],fun1,0)
	call_c   Dyam_Seed_End()
	move_ret seed[14]
	c_ret

pl_code local fun1
	pl_ret

;; TERM 119: token_cat(a, grosse)
c_code local build_ref_119
	ret_reg &ref[119]
	call_c   build_ref_568()
	call_c   build_ref_20()
	call_c   build_ref_569()
	call_c   Dyam_Create_Binary(&ref[568],&ref[20],&ref[569])
	move_ret ref[119]
	c_ret

;; TERM 569: grosse
c_code local build_ref_569
	ret_reg &ref[569]
	call_c   Dyam_Create_Atom("grosse")
	move_ret ref[569]
	c_ret

;; TERM 20: a
c_code local build_ref_20
	ret_reg &ref[20]
	call_c   Dyam_Create_Atom("a")
	move_ret ref[20]
	c_ret

;; TERM 568: token_cat
c_code local build_ref_568
	ret_reg &ref[568]
	call_c   Dyam_Create_Atom("token_cat")
	move_ret ref[568]
	c_ret

;; TERM 118: '*DATABASE*'
c_code local build_ref_118
	ret_reg &ref[118]
	call_c   Dyam_Create_Atom("*DATABASE*")
	move_ret ref[118]
	c_ret

c_code local build_seed_15
	ret_reg &seed[15]
	call_c   build_ref_118()
	call_c   build_ref_120()
	call_c   Dyam_Seed_Start(&ref[118],&ref[120],&ref[120],fun1,0)
	call_c   Dyam_Seed_End()
	move_ret seed[15]
	c_ret

;; TERM 120: token_cat(a, rouge)
c_code local build_ref_120
	ret_reg &ref[120]
	call_c   build_ref_568()
	call_c   build_ref_20()
	call_c   build_ref_570()
	call_c   Dyam_Create_Binary(&ref[568],&ref[20],&ref[570])
	move_ret ref[120]
	c_ret

;; TERM 570: rouge
c_code local build_ref_570
	ret_reg &ref[570]
	call_c   Dyam_Create_Atom("rouge")
	move_ret ref[570]
	c_ret

c_code local build_seed_16
	ret_reg &seed[16]
	call_c   build_ref_118()
	call_c   build_ref_121()
	call_c   Dyam_Seed_Start(&ref[118],&ref[121],&ref[121],fun1,0)
	call_c   Dyam_Seed_End()
	move_ret seed[16]
	c_ret

;; TERM 121: token_cat(a, belle)
c_code local build_ref_121
	ret_reg &ref[121]
	call_c   build_ref_568()
	call_c   build_ref_20()
	call_c   build_ref_571()
	call_c   Dyam_Create_Binary(&ref[568],&ref[20],&ref[571])
	move_ret ref[121]
	c_ret

;; TERM 571: belle
c_code local build_ref_571
	ret_reg &ref[571]
	call_c   Dyam_Create_Atom("belle")
	move_ret ref[571]
	c_ret

c_code local build_seed_17
	ret_reg &seed[17]
	call_c   build_ref_118()
	call_c   build_ref_122()
	call_c   Dyam_Seed_Start(&ref[118],&ref[122],&ref[122],fun1,0)
	call_c   Dyam_Seed_End()
	move_ret seed[17]
	c_ret

;; TERM 122: token_cat(v, mange)
c_code local build_ref_122
	ret_reg &ref[122]
	call_c   build_ref_568()
	call_c   build_ref_106()
	call_c   build_ref_572()
	call_c   Dyam_Create_Binary(&ref[568],&ref[106],&ref[572])
	move_ret ref[122]
	c_ret

;; TERM 572: mange
c_code local build_ref_572
	ret_reg &ref[572]
	call_c   Dyam_Create_Atom("mange")
	move_ret ref[572]
	c_ret

;; TERM 106: v
c_code local build_ref_106
	ret_reg &ref[106]
	call_c   Dyam_Create_Atom("v")
	move_ret ref[106]
	c_ret

c_code local build_seed_18
	ret_reg &seed[18]
	call_c   build_ref_118()
	call_c   build_ref_123()
	call_c   Dyam_Seed_Start(&ref[118],&ref[123],&ref[123],fun1,0)
	call_c   Dyam_Seed_End()
	move_ret seed[18]
	c_ret

;; TERM 123: token_cat(p, avec)
c_code local build_ref_123
	ret_reg &ref[123]
	call_c   build_ref_568()
	call_c   build_ref_57()
	call_c   build_ref_573()
	call_c   Dyam_Create_Binary(&ref[568],&ref[57],&ref[573])
	move_ret ref[123]
	c_ret

;; TERM 573: avec
c_code local build_ref_573
	ret_reg &ref[573]
	call_c   Dyam_Create_Atom("avec")
	move_ret ref[573]
	c_ret

;; TERM 57: p
c_code local build_ref_57
	ret_reg &ref[57]
	call_c   Dyam_Create_Atom("p")
	move_ret ref[57]
	c_ret

c_code local build_seed_19
	ret_reg &seed[19]
	call_c   build_ref_118()
	call_c   build_ref_124()
	call_c   Dyam_Seed_Start(&ref[118],&ref[124],&ref[124],fun1,0)
	call_c   Dyam_Seed_End()
	move_ret seed[19]
	c_ret

;; TERM 124: token_cat(d, la)
c_code local build_ref_124
	ret_reg &ref[124]
	call_c   build_ref_568()
	call_c   build_ref_39()
	call_c   build_ref_574()
	call_c   Dyam_Create_Binary(&ref[568],&ref[39],&ref[574])
	move_ret ref[124]
	c_ret

;; TERM 574: la
c_code local build_ref_574
	ret_reg &ref[574]
	call_c   Dyam_Create_Atom("la")
	move_ret ref[574]
	c_ret

;; TERM 39: d
c_code local build_ref_39
	ret_reg &ref[39]
	call_c   Dyam_Create_Atom("d")
	move_ret ref[39]
	c_ret

c_code local build_seed_20
	ret_reg &seed[20]
	call_c   build_ref_118()
	call_c   build_ref_125()
	call_c   Dyam_Seed_Start(&ref[118],&ref[125],&ref[125],fun1,0)
	call_c   Dyam_Seed_End()
	move_ret seed[20]
	c_ret

;; TERM 125: token_cat(d, le)
c_code local build_ref_125
	ret_reg &ref[125]
	call_c   build_ref_568()
	call_c   build_ref_39()
	call_c   build_ref_575()
	call_c   Dyam_Create_Binary(&ref[568],&ref[39],&ref[575])
	move_ret ref[125]
	c_ret

;; TERM 575: le
c_code local build_ref_575
	ret_reg &ref[575]
	call_c   Dyam_Create_Atom("le")
	move_ret ref[575]
	c_ret

c_code local build_seed_21
	ret_reg &seed[21]
	call_c   build_ref_118()
	call_c   build_ref_126()
	call_c   Dyam_Seed_Start(&ref[118],&ref[126],&ref[126],fun1,0)
	call_c   Dyam_Seed_End()
	move_ret seed[21]
	c_ret

;; TERM 126: token_cat(d, une)
c_code local build_ref_126
	ret_reg &ref[126]
	call_c   build_ref_568()
	call_c   build_ref_39()
	call_c   build_ref_576()
	call_c   Dyam_Create_Binary(&ref[568],&ref[39],&ref[576])
	move_ret ref[126]
	c_ret

;; TERM 576: une
c_code local build_ref_576
	ret_reg &ref[576]
	call_c   Dyam_Create_Atom("une")
	move_ret ref[576]
	c_ret

c_code local build_seed_22
	ret_reg &seed[22]
	call_c   build_ref_118()
	call_c   build_ref_127()
	call_c   Dyam_Seed_Start(&ref[118],&ref[127],&ref[127],fun1,0)
	call_c   Dyam_Seed_End()
	move_ret seed[22]
	c_ret

;; TERM 127: token_cat(d, un)
c_code local build_ref_127
	ret_reg &ref[127]
	call_c   build_ref_568()
	call_c   build_ref_39()
	call_c   build_ref_577()
	call_c   Dyam_Create_Binary(&ref[568],&ref[39],&ref[577])
	move_ret ref[127]
	c_ret

;; TERM 577: un
c_code local build_ref_577
	ret_reg &ref[577]
	call_c   Dyam_Create_Atom("un")
	move_ret ref[577]
	c_ret

c_code local build_seed_23
	ret_reg &seed[23]
	call_c   build_ref_118()
	call_c   build_ref_128()
	call_c   Dyam_Seed_Start(&ref[118],&ref[128],&ref[128],fun1,0)
	call_c   Dyam_Seed_End()
	move_ret seed[23]
	c_ret

;; TERM 128: token_cat(n, poire)
c_code local build_ref_128
	ret_reg &ref[128]
	call_c   build_ref_568()
	call_c   build_ref_87()
	call_c   build_ref_578()
	call_c   Dyam_Create_Binary(&ref[568],&ref[87],&ref[578])
	move_ret ref[128]
	c_ret

;; TERM 578: poire
c_code local build_ref_578
	ret_reg &ref[578]
	call_c   Dyam_Create_Atom("poire")
	move_ret ref[578]
	c_ret

;; TERM 87: n
c_code local build_ref_87
	ret_reg &ref[87]
	call_c   Dyam_Create_Atom("n")
	move_ret ref[87]
	c_ret

c_code local build_seed_24
	ret_reg &seed[24]
	call_c   build_ref_118()
	call_c   build_ref_129()
	call_c   Dyam_Seed_Start(&ref[118],&ref[129],&ref[129],fun1,0)
	call_c   Dyam_Seed_End()
	move_ret seed[24]
	c_ret

;; TERM 129: token_cat(n, pomme)
c_code local build_ref_129
	ret_reg &ref[129]
	call_c   build_ref_568()
	call_c   build_ref_87()
	call_c   build_ref_579()
	call_c   Dyam_Create_Binary(&ref[568],&ref[87],&ref[579])
	move_ret ref[129]
	c_ret

;; TERM 579: pomme
c_code local build_ref_579
	ret_reg &ref[579]
	call_c   Dyam_Create_Atom("pomme")
	move_ret ref[579]
	c_ret

c_code local build_seed_25
	ret_reg &seed[25]
	call_c   build_ref_118()
	call_c   build_ref_130()
	call_c   Dyam_Seed_Start(&ref[118],&ref[130],&ref[130],fun1,0)
	call_c   Dyam_Seed_End()
	move_ret seed[25]
	c_ret

;; TERM 130: token_cat(pn, 'Pierre')
c_code local build_ref_130
	ret_reg &ref[130]
	call_c   build_ref_568()
	call_c   build_ref_81()
	call_c   build_ref_580()
	call_c   Dyam_Create_Binary(&ref[568],&ref[81],&ref[580])
	move_ret ref[130]
	c_ret

;; TERM 580: 'Pierre'
c_code local build_ref_580
	ret_reg &ref[580]
	call_c   Dyam_Create_Atom("Pierre")
	move_ret ref[580]
	c_ret

;; TERM 81: pn
c_code local build_ref_81
	ret_reg &ref[81]
	call_c   Dyam_Create_Atom("pn")
	move_ret ref[81]
	c_ret

c_code local build_seed_26
	ret_reg &seed[26]
	call_c   build_ref_118()
	call_c   build_ref_131()
	call_c   Dyam_Seed_Start(&ref[118],&ref[131],&ref[131],fun1,0)
	call_c   Dyam_Seed_End()
	move_ret seed[26]
	c_ret

;; TERM 131: token_cat(pn, 'Paul')
c_code local build_ref_131
	ret_reg &ref[131]
	call_c   build_ref_568()
	call_c   build_ref_81()
	call_c   build_ref_581()
	call_c   Dyam_Create_Binary(&ref[568],&ref[81],&ref[581])
	move_ret ref[131]
	c_ret

;; TERM 581: 'Paul'
c_code local build_ref_581
	ret_reg &ref[581]
	call_c   Dyam_Create_Atom("Paul")
	move_ret ref[581]
	c_ret

c_code local build_seed_13
	ret_reg &seed[13]
	call_c   build_ref_112()
	call_c   build_ref_114()
	call_c   Dyam_Seed_Start(&ref[112],&ref[114],&ref[114],fun0,1)
	call_c   build_ref_117()
	call_c   Dyam_Seed_Add_Comp(&ref[117],&ref[114],0)
	call_c   Dyam_Seed_End()
	move_ret seed[13]
	c_ret

pl_code local fun0
	pl_jump  Complete(0,0)

;; TERM 117: '*FIRST*'(start) :> '$$HOLE$$'
c_code local build_ref_117
	ret_reg &ref[117]
	call_c   build_ref_116()
	call_c   Dyam_Create_Binary(I(9),&ref[116],I(7))
	move_ret ref[117]
	c_ret

;; TERM 116: '*FIRST*'(start)
c_code local build_ref_116
	ret_reg &ref[116]
	call_c   build_ref_115()
	call_c   build_ref_113()
	call_c   Dyam_Create_Unary(&ref[115],&ref[113])
	move_ret ref[116]
	c_ret

;; TERM 113: start
c_code local build_ref_113
	ret_reg &ref[113]
	call_c   Dyam_Create_Atom("start")
	move_ret ref[113]
	c_ret

;; TERM 115: '*FIRST*'
c_code local build_ref_115
	ret_reg &ref[115]
	call_c   Dyam_Create_Atom("*FIRST*")
	move_ret ref[115]
	c_ret

;; TERM 114: '*CITEM*'(start, start)
c_code local build_ref_114
	ret_reg &ref[114]
	call_c   build_ref_112()
	call_c   build_ref_113()
	call_c   Dyam_Create_Binary(&ref[112],&ref[113],&ref[113])
	move_ret ref[114]
	c_ret

;; TERM 112: '*CITEM*'
c_code local build_ref_112
	ret_reg &ref[112]
	call_c   Dyam_Create_Atom("*CITEM*")
	move_ret ref[112]
	c_ret

c_code local build_seed_0
	ret_reg &seed[0]
	call_c   build_ref_115()
	call_c   build_ref_133()
	call_c   Dyam_Seed_Start(&ref[115],&ref[133],I(0),fun9,1)
	call_c   build_ref_134()
	call_c   Dyam_Seed_Add_Comp(&ref[134],fun12,0)
	call_c   Dyam_Seed_End()
	move_ret seed[0]
	c_ret

;; TERM 134: '*CITEM*'(verbose!adj, _A)
c_code local build_ref_134
	ret_reg &ref[134]
	call_c   build_ref_112()
	call_c   build_ref_1()
	call_c   Dyam_Create_Binary(&ref[112],&ref[1],V(0))
	move_ret ref[134]
	c_ret

;; TERM 1: verbose!adj
c_code local build_ref_1
	ret_reg &ref[1]
	call_c   build_ref_582()
	call_c   Dyam_Create_Atom_Module("adj",&ref[582])
	move_ret ref[1]
	c_ret

;; TERM 582: verbose
c_code local build_ref_582
	ret_reg &ref[582]
	call_c   Dyam_Create_Atom("verbose")
	move_ret ref[582]
	c_ret

c_code local build_seed_27
	ret_reg &seed[27]
	call_c   build_ref_0()
	call_c   build_ref_135()
	call_c   Dyam_Seed_Start(&ref[0],&ref[135],&ref[135],fun0,1)
	call_c   build_ref_136()
	call_c   Dyam_Seed_Add_Comp(&ref[136],&ref[135],0)
	call_c   Dyam_Seed_End()
	move_ret seed[27]
	c_ret

;; TERM 136: '*RITEM*'(_A, voidret) :> '$$HOLE$$'
c_code local build_ref_136
	ret_reg &ref[136]
	call_c   build_ref_135()
	call_c   Dyam_Create_Binary(I(9),&ref[135],I(7))
	move_ret ref[136]
	c_ret

;; TERM 135: '*RITEM*'(_A, voidret)
c_code local build_ref_135
	ret_reg &ref[135]
	call_c   build_ref_0()
	call_c   build_ref_2()
	call_c   Dyam_Create_Binary(&ref[0],V(0),&ref[2])
	move_ret ref[135]
	c_ret

;; TERM 2: voidret
c_code local build_ref_2
	ret_reg &ref[2]
	call_c   Dyam_Create_Atom("voidret")
	move_ret ref[2]
	c_ret

;; TERM 0: '*RITEM*'
c_code local build_ref_0
	ret_reg &ref[0]
	call_c   Dyam_Create_Atom("*RITEM*")
	move_ret ref[0]
	c_ret

pl_code local fun6
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Choice(fun5)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Subsume(R(0))
	fail_ret
	call_c   Dyam_Cut()
	pl_ret

long local pool_fun12[3]=[2,build_ref_134,build_seed_27]

pl_code local fun12
	call_c   Dyam_Pool(pool_fun12)
	call_c   Dyam_Unify_Item(&ref[134])
	fail_ret
	pl_jump  fun6(&seed[27],2)

pl_code local fun9
	pl_jump  Apply(0,0)

;; TERM 133: '*FIRST*'(verbose!adj) :> []
c_code local build_ref_133
	ret_reg &ref[133]
	call_c   build_ref_132()
	call_c   Dyam_Create_Binary(I(9),&ref[132],I(0))
	move_ret ref[133]
	c_ret

;; TERM 132: '*FIRST*'(verbose!adj)
c_code local build_ref_132
	ret_reg &ref[132]
	call_c   build_ref_115()
	call_c   build_ref_1()
	call_c   Dyam_Create_Unary(&ref[115],&ref[1])
	move_ret ref[132]
	c_ret

c_code local build_seed_3
	ret_reg &seed[3]
	call_c   build_ref_115()
	call_c   build_ref_139()
	call_c   Dyam_Seed_Start(&ref[115],&ref[139],I(0),fun9,1)
	call_c   build_ref_140()
	call_c   Dyam_Seed_Add_Comp(&ref[140],fun11,0)
	call_c   Dyam_Seed_End()
	move_ret seed[3]
	c_ret

;; TERM 140: '*CITEM*'(verbose!struct(_B, _C), _A)
c_code local build_ref_140
	ret_reg &ref[140]
	call_c   build_ref_112()
	call_c   build_ref_137()
	call_c   Dyam_Create_Binary(&ref[112],&ref[137],V(0))
	move_ret ref[140]
	c_ret

;; TERM 137: verbose!struct(_B, _C)
c_code local build_ref_137
	ret_reg &ref[137]
	call_c   build_ref_583()
	call_c   Dyam_Create_Binary(&ref[583],V(1),V(2))
	move_ret ref[137]
	c_ret

;; TERM 583: verbose!struct
c_code local build_ref_583
	ret_reg &ref[583]
	call_c   build_ref_582()
	call_c   Dyam_Create_Atom_Module("struct",&ref[582])
	move_ret ref[583]
	c_ret

long local pool_fun11[3]=[2,build_ref_140,build_seed_27]

pl_code local fun11
	call_c   Dyam_Pool(pool_fun11)
	call_c   Dyam_Unify_Item(&ref[140])
	fail_ret
	pl_jump  fun6(&seed[27],2)

;; TERM 139: '*FIRST*'(verbose!struct(_B, _C)) :> []
c_code local build_ref_139
	ret_reg &ref[139]
	call_c   build_ref_138()
	call_c   Dyam_Create_Binary(I(9),&ref[138],I(0))
	move_ret ref[139]
	c_ret

;; TERM 138: '*FIRST*'(verbose!struct(_B, _C))
c_code local build_ref_138
	ret_reg &ref[138]
	call_c   build_ref_115()
	call_c   build_ref_137()
	call_c   Dyam_Create_Unary(&ref[115],&ref[137])
	move_ret ref[138]
	c_ret

c_code local build_seed_1
	ret_reg &seed[1]
	call_c   build_ref_115()
	call_c   build_ref_143()
	call_c   Dyam_Seed_Start(&ref[115],&ref[143],I(0),fun9,1)
	call_c   build_ref_144()
	call_c   Dyam_Seed_Add_Comp(&ref[144],fun10,0)
	call_c   Dyam_Seed_End()
	move_ret seed[1]
	c_ret

;; TERM 144: '*CITEM*'(verbose!lexical([_B|_C], _D, _E, lex, _B), _A)
c_code local build_ref_144
	ret_reg &ref[144]
	call_c   build_ref_112()
	call_c   build_ref_141()
	call_c   Dyam_Create_Binary(&ref[112],&ref[141],V(0))
	move_ret ref[144]
	c_ret

;; TERM 141: verbose!lexical([_B|_C], _D, _E, lex, _B)
c_code local build_ref_141
	ret_reg &ref[141]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(1),V(2))
	move_ret R(0)
	call_c   build_ref_584()
	call_c   build_ref_585()
	call_c   Dyam_Term_Start(&ref[584],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(&ref[585])
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_End()
	move_ret ref[141]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 585: lex
c_code local build_ref_585
	ret_reg &ref[585]
	call_c   Dyam_Create_Atom("lex")
	move_ret ref[585]
	c_ret

;; TERM 584: verbose!lexical
c_code local build_ref_584
	ret_reg &ref[584]
	call_c   build_ref_582()
	call_c   Dyam_Create_Atom_Module("lexical",&ref[582])
	move_ret ref[584]
	c_ret

long local pool_fun10[3]=[2,build_ref_144,build_seed_27]

pl_code local fun10
	call_c   Dyam_Pool(pool_fun10)
	call_c   Dyam_Unify_Item(&ref[144])
	fail_ret
	pl_jump  fun6(&seed[27],2)

;; TERM 143: '*FIRST*'(verbose!lexical([_B|_C], _D, _E, lex, _B)) :> []
c_code local build_ref_143
	ret_reg &ref[143]
	call_c   build_ref_142()
	call_c   Dyam_Create_Binary(I(9),&ref[142],I(0))
	move_ret ref[143]
	c_ret

;; TERM 142: '*FIRST*'(verbose!lexical([_B|_C], _D, _E, lex, _B))
c_code local build_ref_142
	ret_reg &ref[142]
	call_c   build_ref_115()
	call_c   build_ref_141()
	call_c   Dyam_Create_Unary(&ref[115],&ref[141])
	move_ret ref[142]
	c_ret

c_code local build_seed_2
	ret_reg &seed[2]
	call_c   build_ref_115()
	call_c   build_ref_154()
	call_c   Dyam_Seed_Start(&ref[115],&ref[154],I(0),fun9,1)
	call_c   build_ref_155()
	call_c   Dyam_Seed_Add_Comp(&ref[155],fun8,0)
	call_c   Dyam_Seed_End()
	move_ret seed[2]
	c_ret

;; TERM 155: '*CITEM*'(verbose!anchor(_B, _C, _D, _E, _F, _G, _H), _A)
c_code local build_ref_155
	ret_reg &ref[155]
	call_c   build_ref_112()
	call_c   build_ref_152()
	call_c   Dyam_Create_Binary(&ref[112],&ref[152],V(0))
	move_ret ref[155]
	c_ret

;; TERM 152: verbose!anchor(_B, _C, _D, _E, _F, _G, _H)
c_code local build_ref_152
	ret_reg &ref[152]
	call_c   build_ref_586()
	call_c   Dyam_Term_Start(&ref[586],7)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[152]
	c_ret

;; TERM 586: verbose!anchor
c_code local build_ref_586
	ret_reg &ref[586]
	call_c   build_ref_582()
	call_c   Dyam_Create_Atom_Module("anchor",&ref[582])
	move_ret ref[586]
	c_ret

long local pool_fun8[3]=[2,build_ref_155,build_seed_27]

pl_code local fun8
	call_c   Dyam_Pool(pool_fun8)
	call_c   Dyam_Unify_Item(&ref[155])
	fail_ret
	pl_jump  fun6(&seed[27],2)

;; TERM 154: '*FIRST*'(verbose!anchor(_B, _C, _D, _E, _F, _G, _H)) :> []
c_code local build_ref_154
	ret_reg &ref[154]
	call_c   build_ref_153()
	call_c   Dyam_Create_Binary(I(9),&ref[153],I(0))
	move_ret ref[154]
	c_ret

;; TERM 153: '*FIRST*'(verbose!anchor(_B, _C, _D, _E, _F, _G, _H))
c_code local build_ref_153
	ret_reg &ref[153]
	call_c   build_ref_115()
	call_c   build_ref_152()
	call_c   Dyam_Create_Unary(&ref[115],&ref[152])
	move_ret ref[153]
	c_ret

c_code local build_seed_12
	ret_reg &seed[12]
	call_c   build_ref_115()
	call_c   build_ref_145()
	call_c   Dyam_Seed_Start(&ref[115],&ref[145],I(0),fun9,1)
	call_c   build_ref_146()
	call_c   Dyam_Seed_Add_Comp(&ref[146],fun20,0)
	call_c   Dyam_Seed_End()
	move_ret seed[12]
	c_ret

;; TERM 146: '*CITEM*'(start, _A)
c_code local build_ref_146
	ret_reg &ref[146]
	call_c   build_ref_112()
	call_c   build_ref_113()
	call_c   Dyam_Create_Binary(&ref[112],&ref[113],V(0))
	move_ret ref[146]
	c_ret

;; TERM 147: 'N'(_B)
c_code local build_ref_147
	ret_reg &ref[147]
	call_c   build_ref_587()
	call_c   Dyam_Create_Unary(&ref[587],V(1))
	move_ret ref[147]
	c_ret

;; TERM 587: 'N'
c_code local build_ref_587
	ret_reg &ref[587]
	call_c   Dyam_Create_Atom("N")
	move_ret ref[587]
	c_ret

;; TERM 169: '$CLOSURE'('$fun'(19, 0, 1167921524), '$TUPPLE'(35169792164796))
c_code local build_ref_169
	ret_reg &ref[169]
	call_c   build_ref_168()
	call_c   Dyam_Closure_Aux(fun19,&ref[168])
	move_ret ref[169]
	c_ret

c_code local build_seed_30
	ret_reg &seed[30]
	call_c   build_ref_164()
	call_c   build_ref_165()
	call_c   Dyam_Seed_Start(&ref[164],&ref[165],I(0),fun1,0)
	call_c   Dyam_Seed_End()
	move_ret seed[30]
	c_ret

;; TERM 165: '*ANSWER*'{answer=> ['N' = _B]}
c_code local build_ref_165
	ret_reg &ref[165]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_589()
	call_c   build_ref_590()
	call_c   Dyam_Create_Binary(&ref[589],&ref[590],V(1))
	move_ret R(0)
	call_c   Dyam_Create_List(R(0),I(0))
	move_ret R(0)
	call_c   build_ref_588()
	call_c   Dyam_Create_Unary(&ref[588],R(0))
	move_ret ref[165]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 588: '*ANSWER*'!'$ft'
c_code local build_ref_588
	ret_reg &ref[588]
	call_c   build_ref_164()
	call_c   Dyam_Create_Atom_Module("$ft",&ref[164])
	move_ret ref[588]
	c_ret

;; TERM 164: '*ANSWER*'
c_code local build_ref_164
	ret_reg &ref[164]
	call_c   Dyam_Create_Atom("*ANSWER*")
	move_ret ref[164]
	c_ret

;; TERM 590: 'N'
c_code local build_ref_590
	ret_reg &ref[590]
	call_c   Dyam_Create_Atom("N")
	move_ret ref[590]
	c_ret

;; TERM 589: =
c_code local build_ref_589
	ret_reg &ref[589]
	call_c   Dyam_Create_Atom("=")
	move_ret ref[589]
	c_ret

pl_code local fun18
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Object(R(0))
	call_c   Dyam_Tabule()
	call_c   Dyam_Answer()
	pl_ret

long local pool_fun19[2]=[1,build_seed_30]

pl_code local fun19
	call_c   Dyam_Pool(pool_fun19)
	pl_jump  fun18(&seed[30],2)

;; TERM 168: '$TUPPLE'(35169792164796)
c_code local build_ref_168
	ret_reg &ref[168]
	call_c   Dyam_Create_Simple_Tupple(0,402653184)
	move_ret ref[168]
	c_ret

long local pool_fun20[4]=[3,build_ref_146,build_ref_147,build_ref_169]

pl_code local fun20
	call_c   Dyam_Pool(pool_fun20)
	call_c   Dyam_Unify_Item(&ref[146])
	fail_ret
	call_c   Dyam_Allocate(0)
	pl_call  Object_1(&ref[147])
	move     &ref[169], R(0)
	move     S(5), R(1)
	move     N(0), R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Load(4,V(1))
	move     N(0), R(6)
	move     0, R(7)
	move     N(36), R(8)
	move     0, R(9)
	call_c   Dyam_Reg_Deallocate(5)
fun17:
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(5)
	pl_call  fun6(&seed[28],1)
	pl_call  fun16(&seed[29],2,V(4))
	call_c   Dyam_Deallocate()
	pl_ret


;; TERM 145: '*FIRST*'(start) :> []
c_code local build_ref_145
	ret_reg &ref[145]
	call_c   build_ref_116()
	call_c   Dyam_Create_Binary(I(9),&ref[116],I(0))
	move_ret ref[145]
	c_ret

c_code local build_seed_6
	ret_reg &seed[6]
	call_c   build_ref_170()
	call_c   build_ref_172()
	call_c   Dyam_Seed_Start(&ref[170],&ref[172],I(0),fun9,1)
	call_c   build_ref_173()
	call_c   Dyam_Seed_Add_Comp(&ref[173],fun38,0)
	call_c   Dyam_Seed_End()
	move_ret seed[6]
	c_ret

;; TERM 173: '*SACITEM*'('call_d/2'(_B))
c_code local build_ref_173
	ret_reg &ref[173]
	call_c   build_ref_148()
	call_c   build_ref_208()
	call_c   Dyam_Create_Unary(&ref[148],&ref[208])
	move_ret ref[173]
	c_ret

;; TERM 208: 'call_d/2'(_B)
c_code local build_ref_208
	ret_reg &ref[208]
	call_c   build_ref_591()
	call_c   Dyam_Create_Unary(&ref[591],V(1))
	move_ret ref[208]
	c_ret

;; TERM 591: 'call_d/2'
c_code local build_ref_591
	ret_reg &ref[591]
	call_c   Dyam_Create_Atom("call_d/2")
	move_ret ref[591]
	c_ret

;; TERM 148: '*SACITEM*'
c_code local build_ref_148
	ret_reg &ref[148]
	call_c   Dyam_Create_Atom("*SACITEM*")
	move_ret ref[148]
	c_ret

;; TERM 229: '$CLOSURE'('$fun'(37, 0, 1168110336), '$TUPPLE'(35169792165580))
c_code local build_ref_229
	ret_reg &ref[229]
	call_c   build_ref_228()
	call_c   Dyam_Closure_Aux(fun37,&ref[228])
	move_ret ref[229]
	c_ret

;; TERM 226: '$CLOSURE'('$fun'(36, 0, 1168095940), '$TUPPLE'(35169792165376))
c_code local build_ref_226
	ret_reg &ref[226]
	call_c   build_ref_225()
	call_c   Dyam_Closure_Aux(fun36,&ref[225])
	move_ret ref[226]
	c_ret

;; TERM 193: tag_anchor{family=> d, coanchors=> _N, equations=> _O}
c_code local build_ref_193
	ret_reg &ref[193]
	call_c   build_ref_592()
	call_c   build_ref_39()
	call_c   Dyam_Term_Start(&ref[592],3)
	call_c   Dyam_Term_Arg(&ref[39])
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_End()
	move_ret ref[193]
	c_ret

;; TERM 592: tag_anchor!'$ft'
c_code local build_ref_592
	ret_reg &ref[592]
	call_c   build_ref_593()
	call_c   Dyam_Create_Atom_Module("$ft",&ref[593])
	move_ret ref[592]
	c_ret

;; TERM 593: tag_anchor
c_code local build_ref_593
	ret_reg &ref[593]
	call_c   Dyam_Create_Atom("tag_anchor")
	move_ret ref[593]
	c_ret

;; TERM 222: '$CLOSURE'('$fun'(35, 0, 1168089880), '$TUPPLE'(35169792165292))
c_code local build_ref_222
	ret_reg &ref[222]
	call_c   build_ref_221()
	call_c   Dyam_Closure_Aux(fun35,&ref[221])
	move_ret ref[222]
	c_ret

;; TERM 219: '$CLOSURE'('$fun'(34, 0, 1168084460), '$TUPPLE'(35169792165272))
c_code local build_ref_219
	ret_reg &ref[219]
	call_c   build_ref_218()
	call_c   Dyam_Closure_Aux(fun34,&ref[218])
	move_ret ref[219]
	c_ret

;; TERM 216: '$CLOSURE'('$fun'(33, 0, 1168074836), '$TUPPLE'(35169792164304))
c_code local build_ref_216
	ret_reg &ref[216]
	call_c   build_ref_213()
	call_c   Dyam_Closure_Aux(fun33,&ref[213])
	move_ret ref[216]
	c_ret

;; TERM 214: '$CLOSURE'('$fun'(32, 0, 1168075700), '$TUPPLE'(35169792164304))
c_code local build_ref_214
	ret_reg &ref[214]
	call_c   build_ref_213()
	call_c   Dyam_Closure_Aux(fun32,&ref[213])
	move_ret ref[214]
	c_ret

c_code local build_seed_39
	ret_reg &seed[39]
	call_c   build_ref_0()
	call_c   build_ref_210()
	call_c   Dyam_Seed_Start(&ref[0],&ref[210],&ref[210],fun0,1)
	call_c   build_ref_211()
	call_c   Dyam_Seed_Add_Comp(&ref[211],&ref[210],0)
	call_c   Dyam_Seed_End()
	move_ret seed[39]
	c_ret

;; TERM 211: '*RITEM*'('call_d/2'(_B), return(_C)) :> '$$HOLE$$'
c_code local build_ref_211
	ret_reg &ref[211]
	call_c   build_ref_210()
	call_c   Dyam_Create_Binary(I(9),&ref[210],I(7))
	move_ret ref[211]
	c_ret

;; TERM 210: '*RITEM*'('call_d/2'(_B), return(_C))
c_code local build_ref_210
	ret_reg &ref[210]
	call_c   build_ref_0()
	call_c   build_ref_208()
	call_c   build_ref_209()
	call_c   Dyam_Create_Binary(&ref[0],&ref[208],&ref[209])
	move_ret ref[210]
	c_ret

;; TERM 209: return(_C)
c_code local build_ref_209
	ret_reg &ref[209]
	call_c   build_ref_594()
	call_c   Dyam_Create_Unary(&ref[594],V(2))
	move_ret ref[209]
	c_ret

;; TERM 594: return
c_code local build_ref_594
	ret_reg &ref[594]
	call_c   Dyam_Create_Atom("return")
	move_ret ref[594]
	c_ret

long local pool_fun32[2]=[1,build_seed_39]

pl_code local fun32
	call_c   Dyam_Pool(pool_fun32)
	pl_jump  fun6(&seed[39],2)

;; TERM 213: '$TUPPLE'(35169792164304)
c_code local build_ref_213
	ret_reg &ref[213]
	call_c   Dyam_Create_Simple_Tupple(0,201326592)
	move_ret ref[213]
	c_ret

long local pool_fun33[3]=[2,build_ref_214,build_ref_39]

pl_code local fun33
	call_c   Dyam_Pool(pool_fun33)
	call_c   Dyam_Allocate(0)
	move     &ref[214], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	move     &ref[39], R(4)
	move     0, R(5)
	move     &ref[39], R(6)
	move     0, R(7)
	call_c   Dyam_Reg_Deallocate(4)
fun31:
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Reg_Bind(2,V(4))
	call_c   Dyam_Multi_Reg_Bind(4,2,2)
	pl_call  fun6(&seed[37],1)
	pl_call  fun28(&seed[38],2,V(4))
	call_c   Dyam_Deallocate()
	pl_ret


pl_code local fun34
	call_c   build_ref_216()
	call_c   Dyam_Unify(V(7),&ref[216])
	fail_ret
	pl_jump  Follow_Cont(V(6))

;; TERM 218: '$TUPPLE'(35169792165272)
c_code local build_ref_218
	ret_reg &ref[218]
	call_c   Dyam_Create_Simple_Tupple(0,207618048)
	move_ret ref[218]
	c_ret

pl_code local fun35
	call_c   build_ref_219()
	call_c   Dyam_Unify(V(11),&ref[219])
	fail_ret
	pl_jump  Follow_Cont(V(10))

;; TERM 221: '$TUPPLE'(35169792165292)
c_code local build_ref_221
	ret_reg &ref[221]
	call_c   Dyam_Create_Simple_Tupple(0,208011264)
	move_ret ref[221]
	c_ret

;; TERM 223: verbose!anchor(_D, _I, _J, d, d, _M, tag_anchor{family=> d, coanchors=> _N, equations=> _O})
c_code local build_ref_223
	ret_reg &ref[223]
	call_c   build_ref_586()
	call_c   build_ref_39()
	call_c   build_ref_193()
	call_c   Dyam_Term_Start(&ref[586],7)
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(&ref[39])
	call_c   Dyam_Term_Arg(&ref[39])
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(&ref[193])
	call_c   Dyam_Term_End()
	move_ret ref[223]
	c_ret

long local pool_fun36[5]=[4,build_ref_193,build_ref_39,build_ref_222,build_ref_223]

pl_code local fun36
	call_c   Dyam_Pool(pool_fun36)
	call_c   Dyam_Allocate(0)
	move     &ref[193], R(0)
	move     S(5), R(1)
	move     V(3), R(2)
	move     S(5), R(3)
	move     &ref[39], R(4)
	move     0, R(5)
	call_c   Dyam_Reg_Load(6,V(8))
	call_c   Dyam_Reg_Load(8,V(9))
	move     &ref[39], R(10)
	move     0, R(11)
	move     V(12), R(12)
	move     S(5), R(13)
	pl_call  pred_anchor_7()
	move     &ref[222], R(0)
	move     S(5), R(1)
	move     &ref[223], R(2)
	move     S(5), R(3)
	move     N(0), R(4)
	move     0, R(5)
	call_c   Dyam_Reg_Deallocate(3)
fun29:
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Reg_Unify(2,&ref[194])
	fail_ret
	call_c   Dyam_Reg_Bind(4,V(9))
	pl_call  fun6(&seed[35],1)
	pl_call  fun28(&seed[36],2,V(9))
	call_c   Dyam_Deallocate()
	pl_ret


;; TERM 225: '$TUPPLE'(35169792165376)
c_code local build_ref_225
	ret_reg &ref[225]
	call_c   Dyam_Create_Simple_Tupple(0,209584128)
	move_ret ref[225]
	c_ret

;; TERM 174: +
c_code local build_ref_174
	ret_reg &ref[174]
	call_c   Dyam_Create_Atom("+")
	move_ret ref[174]
	c_ret

long local pool_fun37[4]=[3,build_ref_226,build_ref_174,build_ref_39]

pl_code local fun37
	call_c   Dyam_Pool(pool_fun37)
	call_c   Dyam_Allocate(0)
	move     &ref[226], R(0)
	move     S(5), R(1)
	move     V(11), R(2)
	move     S(5), R(3)
	move     V(10), R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Load(6,V(4))
	move     V(8), R(8)
	move     S(5), R(9)
	move     N(0), R(10)
	move     0, R(11)
	move     &ref[174], R(12)
	move     0, R(13)
	move     &ref[39], R(14)
	move     0, R(15)
	call_c   Dyam_Reg_Load(16,V(5))
	move     V(9), R(18)
	move     S(5), R(19)
	call_c   Dyam_Reg_Deallocate(10)
fun26:
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(10)
	call_c   Dyam_Choice(fun25)
	call_c   Dyam_Unify(V(3),V(2))
	fail_ret
	call_c   Dyam_Unify(V(4),V(5))
	fail_ret
	call_c   Dyam_Unify(V(9),V(10))
	fail_ret
	pl_call  Follow_Cont(V(1))
	pl_fail


;; TERM 228: '$TUPPLE'(35169792165580)
c_code local build_ref_228
	ret_reg &ref[228]
	call_c   Dyam_Create_Simple_Tupple(0,232783872)
	move_ret ref[228]
	c_ret

long local pool_fun38[5]=[4,build_ref_173,build_ref_229,build_ref_174,build_ref_39]

pl_code local fun38
	call_c   Dyam_Pool(pool_fun38)
	call_c   Dyam_Unify_Item(&ref[173])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[229], R(0)
	move     S(5), R(1)
	move     V(7), R(2)
	move     S(5), R(3)
	move     V(6), R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Load(6,V(1))
	move     V(4), R(8)
	move     S(5), R(9)
	move     N(1), R(10)
	move     0, R(11)
	move     &ref[174], R(12)
	move     0, R(13)
	move     &ref[39], R(14)
	move     0, R(15)
	move     V(2), R(16)
	move     S(5), R(17)
	move     V(5), R(18)
	move     S(5), R(19)
	call_c   Dyam_Reg_Deallocate(10)
	pl_jump  fun26()

;; TERM 172: '*SAFIRST*'('call_d/2'(_B)) :> []
c_code local build_ref_172
	ret_reg &ref[172]
	call_c   build_ref_171()
	call_c   Dyam_Create_Binary(I(9),&ref[171],I(0))
	move_ret ref[172]
	c_ret

;; TERM 171: '*SAFIRST*'('call_d/2'(_B))
c_code local build_ref_171
	ret_reg &ref[171]
	call_c   build_ref_170()
	call_c   build_ref_208()
	call_c   Dyam_Create_Unary(&ref[170],&ref[208])
	move_ret ref[171]
	c_ret

;; TERM 170: '*SAFIRST*'
c_code local build_ref_170
	ret_reg &ref[170]
	call_c   Dyam_Create_Atom("*SAFIRST*")
	move_ret ref[170]
	c_ret

c_code local build_seed_9
	ret_reg &seed[9]
	call_c   build_ref_170()
	call_c   build_ref_231()
	call_c   Dyam_Seed_Start(&ref[170],&ref[231],I(0),fun9,1)
	call_c   build_ref_232()
	call_c   Dyam_Seed_Add_Comp(&ref[232],fun55,0)
	call_c   Dyam_Seed_End()
	move_ret seed[9]
	c_ret

;; TERM 232: '*SACITEM*'('call_np/2'(_B))
c_code local build_ref_232
	ret_reg &ref[232]
	call_c   build_ref_148()
	call_c   build_ref_260()
	call_c   Dyam_Create_Unary(&ref[148],&ref[260])
	move_ret ref[232]
	c_ret

;; TERM 260: 'call_np/2'(_B)
c_code local build_ref_260
	ret_reg &ref[260]
	call_c   build_ref_595()
	call_c   Dyam_Create_Unary(&ref[595],V(1))
	move_ret ref[260]
	c_ret

;; TERM 595: 'call_np/2'
c_code local build_ref_595
	ret_reg &ref[595]
	call_c   Dyam_Create_Atom("call_np/2")
	move_ret ref[595]
	c_ret

;; TERM 275: '$CLOSURE'('$fun'(54, 0, 1168256684), '$TUPPLE'(35169792165580))
c_code local build_ref_275
	ret_reg &ref[275]
	call_c   build_ref_228()
	call_c   Dyam_Closure_Aux(fun54,&ref[228])
	move_ret ref[275]
	c_ret

;; TERM 273: '$CLOSURE'('$fun'(53, 0, 1168242964), '$TUPPLE'(35169792165376))
c_code local build_ref_273
	ret_reg &ref[273]
	call_c   build_ref_225()
	call_c   Dyam_Closure_Aux(fun53,&ref[225])
	move_ret ref[273]
	c_ret

;; TERM 259: tag_anchor{family=> np, coanchors=> _N, equations=> _O}
c_code local build_ref_259
	ret_reg &ref[259]
	call_c   build_ref_592()
	call_c   build_ref_80()
	call_c   Dyam_Term_Start(&ref[592],3)
	call_c   Dyam_Term_Arg(&ref[80])
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_End()
	move_ret ref[259]
	c_ret

;; TERM 80: np
c_code local build_ref_80
	ret_reg &ref[80]
	call_c   Dyam_Create_Atom("np")
	move_ret ref[80]
	c_ret

;; TERM 270: '$CLOSURE'('$fun'(52, 0, 1168234520), '$TUPPLE'(35169792165292))
c_code local build_ref_270
	ret_reg &ref[270]
	call_c   build_ref_221()
	call_c   Dyam_Closure_Aux(fun52,&ref[221])
	move_ret ref[270]
	c_ret

;; TERM 268: '$CLOSURE'('$fun'(51, 0, 1168235060), '$TUPPLE'(35169792165272))
c_code local build_ref_268
	ret_reg &ref[268]
	call_c   build_ref_218()
	call_c   Dyam_Closure_Aux(fun51,&ref[218])
	move_ret ref[268]
	c_ret

;; TERM 266: '$CLOSURE'('$fun'(50, 0, 1168235432), '$TUPPLE'(35169792164304))
c_code local build_ref_266
	ret_reg &ref[266]
	call_c   build_ref_213()
	call_c   Dyam_Closure_Aux(fun50,&ref[213])
	move_ret ref[266]
	c_ret

;; TERM 264: '$CLOSURE'('$fun'(49, 0, 1168226796), '$TUPPLE'(35169792164304))
c_code local build_ref_264
	ret_reg &ref[264]
	call_c   build_ref_213()
	call_c   Dyam_Closure_Aux(fun49,&ref[213])
	move_ret ref[264]
	c_ret

c_code local build_seed_48
	ret_reg &seed[48]
	call_c   build_ref_0()
	call_c   build_ref_261()
	call_c   Dyam_Seed_Start(&ref[0],&ref[261],&ref[261],fun0,1)
	call_c   build_ref_262()
	call_c   Dyam_Seed_Add_Comp(&ref[262],&ref[261],0)
	call_c   Dyam_Seed_End()
	move_ret seed[48]
	c_ret

;; TERM 262: '*RITEM*'('call_np/2'(_B), return(_C)) :> '$$HOLE$$'
c_code local build_ref_262
	ret_reg &ref[262]
	call_c   build_ref_261()
	call_c   Dyam_Create_Binary(I(9),&ref[261],I(7))
	move_ret ref[262]
	c_ret

;; TERM 261: '*RITEM*'('call_np/2'(_B), return(_C))
c_code local build_ref_261
	ret_reg &ref[261]
	call_c   build_ref_0()
	call_c   build_ref_260()
	call_c   build_ref_209()
	call_c   Dyam_Create_Binary(&ref[0],&ref[260],&ref[209])
	move_ret ref[261]
	c_ret

long local pool_fun49[2]=[1,build_seed_48]

pl_code local fun49
	call_c   Dyam_Pool(pool_fun49)
	pl_jump  fun6(&seed[48],2)

long local pool_fun50[4]=[3,build_ref_264,build_ref_81,build_ref_80]

pl_code local fun50
	call_c   Dyam_Pool(pool_fun50)
	call_c   Dyam_Allocate(0)
	move     &ref[264], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	move     &ref[81], R(4)
	move     0, R(5)
	move     &ref[80], R(6)
	move     0, R(7)
	call_c   Dyam_Reg_Deallocate(4)
	pl_jump  fun31()

pl_code local fun51
	call_c   build_ref_266()
	call_c   Dyam_Unify(V(7),&ref[266])
	fail_ret
	pl_jump  Follow_Cont(V(6))

pl_code local fun52
	call_c   build_ref_268()
	call_c   Dyam_Unify(V(11),&ref[268])
	fail_ret
	pl_jump  Follow_Cont(V(10))

;; TERM 271: verbose!anchor(_D, _I, _J, pn, pn, _M, tag_anchor{family=> np, coanchors=> _N, equations=> _O})
c_code local build_ref_271
	ret_reg &ref[271]
	call_c   build_ref_586()
	call_c   build_ref_81()
	call_c   build_ref_259()
	call_c   Dyam_Term_Start(&ref[586],7)
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(&ref[81])
	call_c   Dyam_Term_Arg(&ref[81])
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(&ref[259])
	call_c   Dyam_Term_End()
	move_ret ref[271]
	c_ret

long local pool_fun53[5]=[4,build_ref_259,build_ref_81,build_ref_270,build_ref_271]

pl_code local fun53
	call_c   Dyam_Pool(pool_fun53)
	call_c   Dyam_Allocate(0)
	move     &ref[259], R(0)
	move     S(5), R(1)
	move     V(3), R(2)
	move     S(5), R(3)
	move     &ref[81], R(4)
	move     0, R(5)
	call_c   Dyam_Reg_Load(6,V(8))
	call_c   Dyam_Reg_Load(8,V(9))
	move     &ref[81], R(10)
	move     0, R(11)
	move     V(12), R(12)
	move     S(5), R(13)
	pl_call  pred_anchor_7()
	move     &ref[270], R(0)
	move     S(5), R(1)
	move     &ref[271], R(2)
	move     S(5), R(3)
	move     N(0), R(4)
	move     0, R(5)
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  fun29()

long local pool_fun54[4]=[3,build_ref_273,build_ref_174,build_ref_81]

pl_code local fun54
	call_c   Dyam_Pool(pool_fun54)
	call_c   Dyam_Allocate(0)
	move     &ref[273], R(0)
	move     S(5), R(1)
	move     V(11), R(2)
	move     S(5), R(3)
	move     V(10), R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Load(6,V(4))
	move     V(8), R(8)
	move     S(5), R(9)
	move     N(0), R(10)
	move     0, R(11)
	move     &ref[174], R(12)
	move     0, R(13)
	move     &ref[81], R(14)
	move     0, R(15)
	call_c   Dyam_Reg_Load(16,V(5))
	move     V(9), R(18)
	move     S(5), R(19)
	call_c   Dyam_Reg_Deallocate(10)
fun48:
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(10)
	call_c   Dyam_Choice(fun47)
	call_c   Dyam_Unify(V(3),V(2))
	fail_ret
	call_c   Dyam_Unify(V(4),V(5))
	fail_ret
	call_c   Dyam_Unify(V(9),V(10))
	fail_ret
	pl_call  Follow_Cont(V(1))
	pl_fail


long local pool_fun55[5]=[4,build_ref_232,build_ref_275,build_ref_174,build_ref_81]

pl_code local fun55
	call_c   Dyam_Pool(pool_fun55)
	call_c   Dyam_Unify_Item(&ref[232])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[275], R(0)
	move     S(5), R(1)
	move     V(7), R(2)
	move     S(5), R(3)
	move     V(6), R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Load(6,V(1))
	move     V(4), R(8)
	move     S(5), R(9)
	move     N(1), R(10)
	move     0, R(11)
	move     &ref[174], R(12)
	move     0, R(13)
	move     &ref[81], R(14)
	move     0, R(15)
	move     V(2), R(16)
	move     S(5), R(17)
	move     V(5), R(18)
	move     S(5), R(19)
	call_c   Dyam_Reg_Deallocate(10)
fun43:
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(10)
	call_c   Dyam_Choice(fun42)
	call_c   Dyam_Unify(V(3),V(2))
	fail_ret
	call_c   Dyam_Unify(V(4),V(5))
	fail_ret
	call_c   Dyam_Unify(V(9),V(10))
	fail_ret
	pl_call  Follow_Cont(V(1))
	pl_fail


;; TERM 231: '*SAFIRST*'('call_np/2'(_B)) :> []
c_code local build_ref_231
	ret_reg &ref[231]
	call_c   build_ref_230()
	call_c   Dyam_Create_Binary(I(9),&ref[230],I(0))
	move_ret ref[231]
	c_ret

;; TERM 230: '*SAFIRST*'('call_np/2'(_B))
c_code local build_ref_230
	ret_reg &ref[230]
	call_c   build_ref_170()
	call_c   build_ref_260()
	call_c   Dyam_Create_Unary(&ref[170],&ref[260])
	move_ret ref[230]
	c_ret

c_code local build_seed_10
	ret_reg &seed[10]
	call_c   build_ref_170()
	call_c   build_ref_231()
	call_c   Dyam_Seed_Start(&ref[170],&ref[231],I(0),fun9,1)
	call_c   build_ref_232()
	call_c   Dyam_Seed_Add_Comp(&ref[232],fun92,0)
	call_c   Dyam_Seed_End()
	move_ret seed[10]
	c_ret

;; TERM 382: '$CLOSURE'('$fun'(91, 0, 1168588196), '$TUPPLE'(35169792165580))
c_code local build_ref_382
	ret_reg &ref[382]
	call_c   build_ref_228()
	call_c   Dyam_Closure_Aux(fun91,&ref[228])
	move_ret ref[382]
	c_ret

;; TERM 380: '$CLOSURE'('$fun'(90, 0, 1168588748), '$TUPPLE'(35169792167232))
c_code local build_ref_380
	ret_reg &ref[380]
	call_c   build_ref_337()
	call_c   Dyam_Closure_Aux(fun90,&ref[337])
	move_ret ref[380]
	c_ret

;; TERM 378: '$CLOSURE'('$fun'(89, 0, 1168576052), '$TUPPLE'(35169792168228))
c_code local build_ref_378
	ret_reg &ref[378]
	call_c   build_ref_377()
	call_c   Dyam_Closure_Aux(fun89,&ref[377])
	move_ret ref[378]
	c_ret

;; TERM 367: tag_anchor{family=> np, coanchors=> _O, equations=> _P}
c_code local build_ref_367
	ret_reg &ref[367]
	call_c   build_ref_592()
	call_c   build_ref_80()
	call_c   Dyam_Term_Start(&ref[592],3)
	call_c   Dyam_Term_Arg(&ref[80])
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_End()
	move_ret ref[367]
	c_ret

;; TERM 374: '$CLOSURE'('$fun'(88, 0, 1168569860), '$TUPPLE'(35169792168144))
c_code local build_ref_374
	ret_reg &ref[374]
	call_c   build_ref_373()
	call_c   Dyam_Closure_Aux(fun88,&ref[373])
	move_ret ref[374]
	c_ret

;; TERM 371: '$CLOSURE'('$fun'(87, 0, 1168555044), '$TUPPLE'(35169792165272))
c_code local build_ref_371
	ret_reg &ref[371]
	call_c   build_ref_218()
	call_c   Dyam_Closure_Aux(fun87,&ref[218])
	move_ret ref[371]
	c_ret

;; TERM 369: '$CLOSURE'('$fun'(86, 0, 1168555416), '$TUPPLE'(35169792164304))
c_code local build_ref_369
	ret_reg &ref[369]
	call_c   build_ref_213()
	call_c   Dyam_Closure_Aux(fun86,&ref[213])
	move_ret ref[369]
	c_ret

;; TERM 88: dn
c_code local build_ref_88
	ret_reg &ref[88]
	call_c   Dyam_Create_Atom("dn")
	move_ret ref[88]
	c_ret

long local pool_fun86[4]=[3,build_ref_264,build_ref_88,build_ref_80]

pl_code local fun86
	call_c   Dyam_Pool(pool_fun86)
	call_c   Dyam_Allocate(0)
	move     &ref[264], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	move     &ref[88], R(4)
	move     0, R(5)
	move     &ref[80], R(6)
	move     0, R(7)
	call_c   Dyam_Reg_Deallocate(4)
	pl_jump  fun31()

pl_code local fun87
	call_c   build_ref_369()
	call_c   Dyam_Unify(V(7),&ref[369])
	fail_ret
	pl_jump  Follow_Cont(V(6))

pl_code local fun88
	call_c   build_ref_371()
	call_c   Dyam_Unify(V(12),&ref[371])
	fail_ret
	pl_jump  Follow_Cont(V(11))

;; TERM 373: '$TUPPLE'(35169792168144)
c_code local build_ref_373
	ret_reg &ref[373]
	call_c   Dyam_Create_Simple_Tupple(0,207814656)
	move_ret ref[373]
	c_ret

;; TERM 375: verbose!anchor(_D, _J, _K, dn, n, _N, tag_anchor{family=> np, coanchors=> _O, equations=> _P})
c_code local build_ref_375
	ret_reg &ref[375]
	call_c   build_ref_586()
	call_c   build_ref_88()
	call_c   build_ref_87()
	call_c   build_ref_367()
	call_c   Dyam_Term_Start(&ref[586],7)
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(&ref[88])
	call_c   Dyam_Term_Arg(&ref[87])
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(&ref[367])
	call_c   Dyam_Term_End()
	move_ret ref[375]
	c_ret

long local pool_fun89[5]=[4,build_ref_367,build_ref_87,build_ref_374,build_ref_375]

pl_code local fun89
	call_c   Dyam_Pool(pool_fun89)
	call_c   Dyam_Allocate(0)
	move     &ref[367], R(0)
	move     S(5), R(1)
	move     V(3), R(2)
	move     S(5), R(3)
	move     &ref[87], R(4)
	move     0, R(5)
	call_c   Dyam_Reg_Load(6,V(9))
	call_c   Dyam_Reg_Load(8,V(10))
	move     &ref[87], R(10)
	move     0, R(11)
	move     V(13), R(12)
	move     S(5), R(13)
	pl_call  pred_anchor_7()
	move     &ref[374], R(0)
	move     S(5), R(1)
	move     &ref[375], R(2)
	move     S(5), R(3)
	move     N(1), R(4)
	move     0, R(5)
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  fun29()

;; TERM 377: '$TUPPLE'(35169792168228)
c_code local build_ref_377
	ret_reg &ref[377]
	call_c   Dyam_Create_Simple_Tupple(0,208601088)
	move_ret ref[377]
	c_ret

long local pool_fun90[4]=[3,build_ref_378,build_ref_174,build_ref_88]

pl_code local fun90
	call_c   Dyam_Pool(pool_fun90)
	call_c   Dyam_Allocate(0)
	move     &ref[378], R(0)
	move     S(5), R(1)
	move     V(12), R(2)
	move     S(5), R(3)
	move     V(11), R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Load(6,V(8))
	move     V(9), R(8)
	move     S(5), R(9)
	move     N(1), R(10)
	move     0, R(11)
	move     &ref[174], R(12)
	move     0, R(13)
	move     &ref[88], R(14)
	move     0, R(15)
	call_c   Dyam_Reg_Load(16,V(5))
	move     V(10), R(18)
	move     S(5), R(19)
	call_c   Dyam_Reg_Deallocate(10)
fun60:
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(10)
	call_c   Dyam_Choice(fun59)
	call_c   Dyam_Unify(V(3),V(2))
	fail_ret
	call_c   Dyam_Unify(V(4),V(5))
	fail_ret
	call_c   Dyam_Unify(V(9),V(10))
	fail_ret
	pl_call  Follow_Cont(V(1))
	pl_fail


;; TERM 337: '$TUPPLE'(35169792167232)
c_code local build_ref_337
	ret_reg &ref[337]
	call_c   Dyam_Create_Simple_Tupple(0,217055232)
	move_ret ref[337]
	c_ret

long local pool_fun91[3]=[2,build_ref_380,build_ref_88]

pl_code local fun91
	call_c   Dyam_Pool(pool_fun91)
	call_c   Dyam_Allocate(0)
	move     &ref[380], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(4))
	move     V(8), R(4)
	move     S(5), R(5)
	move     N(0), R(6)
	move     0, R(7)
	move     &ref[88], R(8)
	move     0, R(9)
	call_c   Dyam_Reg_Deallocate(5)
fun85:
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(5)
	pl_call  fun6(&seed[61],1)
	pl_call  fun16(&seed[62],2,V(4))
	call_c   Dyam_Deallocate()
	pl_ret


long local pool_fun92[5]=[4,build_ref_232,build_ref_382,build_ref_174,build_ref_88]

pl_code local fun92
	call_c   Dyam_Pool(pool_fun92)
	call_c   Dyam_Unify_Item(&ref[232])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[382], R(0)
	move     S(5), R(1)
	move     V(7), R(2)
	move     S(5), R(3)
	move     V(6), R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Load(6,V(1))
	move     V(4), R(8)
	move     S(5), R(9)
	move     N(2), R(10)
	move     0, R(11)
	move     &ref[174], R(12)
	move     0, R(13)
	move     &ref[88], R(14)
	move     0, R(15)
	move     V(2), R(16)
	move     S(5), R(17)
	move     V(5), R(18)
	move     S(5), R(19)
	call_c   Dyam_Reg_Deallocate(10)
	pl_jump  fun43()

c_code local build_seed_4
	ret_reg &seed[4]
	call_c   build_ref_276()
	call_c   build_ref_278()
	call_c   Dyam_Seed_Start(&ref[276],&ref[278],I(0),fun9,1)
	call_c   build_ref_279()
	call_c   Dyam_Seed_Add_Comp(&ref[279],fun75,0)
	call_c   Dyam_Seed_End()
	move_ret seed[4]
	c_ret

;; TERM 279: '*CAI*'{current=> 'call_n/2'(_B)}
c_code local build_ref_279
	ret_reg &ref[279]
	call_c   build_ref_596()
	call_c   build_ref_335()
	call_c   Dyam_Create_Unary(&ref[596],&ref[335])
	move_ret ref[279]
	c_ret

;; TERM 335: 'call_n/2'(_B)
c_code local build_ref_335
	ret_reg &ref[335]
	call_c   build_ref_597()
	call_c   Dyam_Create_Unary(&ref[597],V(1))
	move_ret ref[335]
	c_ret

;; TERM 597: 'call_n/2'
c_code local build_ref_597
	ret_reg &ref[597]
	call_c   Dyam_Create_Atom("call_n/2")
	move_ret ref[597]
	c_ret

;; TERM 596: '*CAI*'!'$ft'
c_code local build_ref_596
	ret_reg &ref[596]
	call_c   build_ref_175()
	call_c   Dyam_Create_Atom_Module("$ft",&ref[175])
	move_ret ref[596]
	c_ret

;; TERM 175: '*CAI*'
c_code local build_ref_175
	ret_reg &ref[175]
	call_c   Dyam_Create_Atom("*CAI*")
	move_ret ref[175]
	c_ret

;; TERM 338: '$CLOSURE'('$fun'(74, 0, 1168451436), '$TUPPLE'(35169792167232))
c_code local build_ref_338
	ret_reg &ref[338]
	call_c   build_ref_337()
	call_c   Dyam_Closure_Aux(fun74,&ref[337])
	move_ret ref[338]
	c_ret

;; TERM 334: '$CLOSURE'('$fun'(73, 0, 1168440932), '$TUPPLE'(35169792167116))
c_code local build_ref_334
	ret_reg &ref[334]
	call_c   build_ref_333()
	call_c   Dyam_Closure_Aux(fun73,&ref[333])
	move_ret ref[334]
	c_ret

;; TERM 331: '$CLOSURE'('$fun'(72, 0, 1168426272), '$TUPPLE'(35169792166920))
c_code local build_ref_331
	ret_reg &ref[331]
	call_c   build_ref_330()
	call_c   Dyam_Closure_Aux(fun72,&ref[330])
	move_ret ref[331]
	c_ret

;; TERM 313: tag_anchor{family=> a, coanchors=> _P, equations=> _Q}
c_code local build_ref_313
	ret_reg &ref[313]
	call_c   build_ref_592()
	call_c   build_ref_20()
	call_c   Dyam_Term_Start(&ref[592],3)
	call_c   Dyam_Term_Arg(&ref[20])
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_End()
	move_ret ref[313]
	c_ret

;; TERM 327: '$CLOSURE'('$fun'(71, 0, 1168420080), '$TUPPLE'(35169792166836))
c_code local build_ref_327
	ret_reg &ref[327]
	call_c   build_ref_326()
	call_c   Dyam_Closure_Aux(fun71,&ref[326])
	move_ret ref[327]
	c_ret

;; TERM 324: '$CLOSURE'('$fun'(70, 0, 1168420752), '$TUPPLE'(35169792166816))
c_code local build_ref_324
	ret_reg &ref[324]
	call_c   build_ref_323()
	call_c   Dyam_Closure_Aux(fun70,&ref[323])
	move_ret ref[324]
	c_ret

;; TERM 321: '$CLOSURE'('$fun'(69, 0, 1168409864), '$TUPPLE'(35169792166760))
c_code local build_ref_321
	ret_reg &ref[321]
	call_c   build_ref_318()
	call_c   Dyam_Closure_Aux(fun69,&ref[318])
	move_ret ref[321]
	c_ret

;; TERM 319: '$CLOSURE'('$fun'(68, 0, 1168410524), '$TUPPLE'(35169792166760))
c_code local build_ref_319
	ret_reg &ref[319]
	call_c   build_ref_318()
	call_c   Dyam_Closure_Aux(fun68,&ref[318])
	move_ret ref[319]
	c_ret

c_code local build_seed_59
	ret_reg &seed[59]
	call_c   build_ref_314()
	call_c   build_ref_315()
	call_c   Dyam_Seed_Start(&ref[314],&ref[315],&ref[315],fun0,1)
	call_c   build_ref_316()
	call_c   Dyam_Seed_Add_Comp(&ref[316],&ref[315],0)
	call_c   Dyam_Seed_End()
	move_ret seed[59]
	c_ret

;; TERM 316: '*RAI*'{xstart=> 'call_n/2'(_F), xend=> return(_J), aspop=> 'call_n/2'(_B), current=> return(_C)} :> '$$HOLE$$'
c_code local build_ref_316
	ret_reg &ref[316]
	call_c   build_ref_315()
	call_c   Dyam_Create_Binary(I(9),&ref[315],I(7))
	move_ret ref[316]
	c_ret

;; TERM 315: '*RAI*'{xstart=> 'call_n/2'(_F), xend=> return(_J), aspop=> 'call_n/2'(_B), current=> return(_C)}
c_code local build_ref_315
	ret_reg &ref[315]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_597()
	call_c   Dyam_Create_Unary(&ref[597],V(5))
	move_ret R(0)
	call_c   build_ref_594()
	call_c   Dyam_Create_Unary(&ref[594],V(9))
	move_ret R(1)
	call_c   build_ref_598()
	call_c   build_ref_335()
	call_c   build_ref_209()
	call_c   Dyam_Term_Start(&ref[598],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(&ref[335])
	call_c   Dyam_Term_Arg(&ref[209])
	call_c   Dyam_Term_End()
	move_ret ref[315]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 598: '*RAI*'!'$ft'
c_code local build_ref_598
	ret_reg &ref[598]
	call_c   build_ref_314()
	call_c   Dyam_Create_Atom_Module("$ft",&ref[314])
	move_ret ref[598]
	c_ret

;; TERM 314: '*RAI*'
c_code local build_ref_314
	ret_reg &ref[314]
	call_c   Dyam_Create_Atom("*RAI*")
	move_ret ref[314]
	c_ret

long local pool_fun68[2]=[1,build_seed_59]

pl_code local fun68
	call_c   Dyam_Pool(pool_fun68)
	pl_jump  fun6(&seed[59],2)

;; TERM 318: '$TUPPLE'(35169792166760)
c_code local build_ref_318
	ret_reg &ref[318]
	call_c   Dyam_Create_Simple_Tupple(0,210239488)
	move_ret ref[318]
	c_ret

;; TERM 21: na
c_code local build_ref_21
	ret_reg &ref[21]
	call_c   Dyam_Create_Atom("na")
	move_ret ref[21]
	c_ret

long local pool_fun69[4]=[3,build_ref_319,build_ref_21,build_ref_20]

pl_code local fun69
	call_c   Dyam_Pool(pool_fun69)
	call_c   Dyam_Allocate(0)
	move     &ref[319], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	move     &ref[21], R(4)
	move     0, R(5)
	move     &ref[20], R(6)
	move     0, R(7)
	call_c   Dyam_Reg_Deallocate(4)
	pl_jump  fun31()

pl_code local fun70
	call_c   build_ref_321()
	call_c   Dyam_Unify(V(8),&ref[321])
	fail_ret
	pl_jump  Follow_Cont(V(7))

;; TERM 323: '$TUPPLE'(35169792166816)
c_code local build_ref_323
	ret_reg &ref[323]
	call_c   Dyam_Create_Simple_Tupple(0,213385216)
	move_ret ref[323]
	c_ret

pl_code local fun71
	call_c   build_ref_324()
	call_c   Dyam_Unify(V(13),&ref[324])
	fail_ret
	pl_jump  Follow_Cont(V(12))

;; TERM 326: '$TUPPLE'(35169792166836)
c_code local build_ref_326
	ret_reg &ref[326]
	call_c   Dyam_Create_Simple_Tupple(0,213483520)
	move_ret ref[326]
	c_ret

;; TERM 328: verbose!anchor(_D, _K, _L, na, a, _O, tag_anchor{family=> a, coanchors=> _P, equations=> _Q})
c_code local build_ref_328
	ret_reg &ref[328]
	call_c   build_ref_586()
	call_c   build_ref_21()
	call_c   build_ref_20()
	call_c   build_ref_313()
	call_c   Dyam_Term_Start(&ref[586],7)
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(&ref[21])
	call_c   Dyam_Term_Arg(&ref[20])
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(&ref[313])
	call_c   Dyam_Term_End()
	move_ret ref[328]
	c_ret

long local pool_fun72[5]=[4,build_ref_313,build_ref_20,build_ref_327,build_ref_328]

pl_code local fun72
	call_c   Dyam_Pool(pool_fun72)
	call_c   Dyam_Allocate(0)
	move     &ref[313], R(0)
	move     S(5), R(1)
	move     V(3), R(2)
	move     S(5), R(3)
	move     &ref[20], R(4)
	move     0, R(5)
	call_c   Dyam_Reg_Load(6,V(10))
	call_c   Dyam_Reg_Load(8,V(11))
	move     &ref[20], R(10)
	move     0, R(11)
	move     V(14), R(12)
	move     S(5), R(13)
	pl_call  pred_anchor_7()
	move     &ref[327], R(0)
	move     S(5), R(1)
	move     &ref[328], R(2)
	move     S(5), R(3)
	move     N(1), R(4)
	move     0, R(5)
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  fun29()

;; TERM 330: '$TUPPLE'(35169792166920)
c_code local build_ref_330
	ret_reg &ref[330]
	call_c   Dyam_Create_Simple_Tupple(0,213876736)
	move_ret ref[330]
	c_ret

long local pool_fun73[4]=[3,build_ref_331,build_ref_174,build_ref_21]

pl_code local fun73
	call_c   Dyam_Pool(pool_fun73)
	call_c   Dyam_Allocate(0)
	move     &ref[331], R(0)
	move     S(5), R(1)
	move     V(13), R(2)
	move     S(5), R(3)
	move     V(12), R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Load(6,V(9))
	move     V(10), R(8)
	move     S(5), R(9)
	move     N(1), R(10)
	move     0, R(11)
	move     &ref[174], R(12)
	move     0, R(13)
	move     &ref[21], R(14)
	move     0, R(15)
	call_c   Dyam_Reg_Load(16,V(6))
	move     V(11), R(18)
	move     S(5), R(19)
	call_c   Dyam_Reg_Deallocate(10)
fun67:
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(10)
	call_c   Dyam_Choice(fun66)
	call_c   Dyam_Unify(V(3),V(2))
	fail_ret
	call_c   Dyam_Unify(V(4),V(5))
	fail_ret
	call_c   Dyam_Unify(V(9),V(10))
	fail_ret
	pl_call  Follow_Cont(V(1))
	pl_fail


;; TERM 333: '$TUPPLE'(35169792167116)
c_code local build_ref_333
	ret_reg &ref[333]
	call_c   Dyam_Create_Simple_Tupple(0,217579520)
	move_ret ref[333]
	c_ret

long local pool_fun74[3]=[2,build_ref_334,build_ref_335]

pl_code local fun74
	call_c   Dyam_Pool(pool_fun74)
	call_c   Dyam_Allocate(0)
	move     &ref[334], R(0)
	move     S(5), R(1)
	move     &ref[335], R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Load(4,V(5))
	move     V(9), R(6)
	move     S(5), R(7)
	call_c   Dyam_Reg_Deallocate(4)
fun62:
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Reg_Bind(2,V(4))
	call_c   Dyam_Multi_Reg_Bind(4,2,2)
	pl_call  fun6(&seed[53],1)
	pl_call  fun22(&seed[54],2)
	call_c   Dyam_Deallocate()
	pl_ret


long local pool_fun75[4]=[3,build_ref_279,build_ref_338,build_ref_21]

pl_code local fun75
	call_c   Dyam_Pool(pool_fun75)
	call_c   Dyam_Unify_Item(&ref[279])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[338], R(0)
	move     S(5), R(1)
	move     V(8), R(2)
	move     S(5), R(3)
	move     V(7), R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Load(6,V(1))
	move     V(5), R(8)
	move     S(5), R(9)
	move     N(2), R(10)
	move     0, R(11)
	move     0, R(12)
	move     &ref[21], R(14)
	move     0, R(15)
	move     V(2), R(16)
	move     S(5), R(17)
	move     V(6), R(18)
	move     S(5), R(19)
	call_c   Dyam_Reg_Deallocate(10)
	pl_jump  fun60()

;; TERM 278: '*SA-AUX-FIRST*'('call_n/2'(_B)) :> []
c_code local build_ref_278
	ret_reg &ref[278]
	call_c   build_ref_277()
	call_c   Dyam_Create_Binary(I(9),&ref[277],I(0))
	move_ret ref[278]
	c_ret

;; TERM 277: '*SA-AUX-FIRST*'('call_n/2'(_B))
c_code local build_ref_277
	ret_reg &ref[277]
	call_c   build_ref_276()
	call_c   build_ref_335()
	call_c   Dyam_Create_Unary(&ref[276],&ref[335])
	move_ret ref[277]
	c_ret

;; TERM 276: '*SA-AUX-FIRST*'
c_code local build_ref_276
	ret_reg &ref[276]
	call_c   Dyam_Create_Atom("*SA-AUX-FIRST*")
	move_ret ref[276]
	c_ret

c_code local build_seed_5
	ret_reg &seed[5]
	call_c   build_ref_276()
	call_c   build_ref_278()
	call_c   Dyam_Seed_Start(&ref[276],&ref[278],I(0),fun9,1)
	call_c   build_ref_279()
	call_c   Dyam_Seed_Add_Comp(&ref[279],fun83,0)
	call_c   Dyam_Seed_End()
	move_ret seed[5]
	c_ret

;; TERM 359: '$CLOSURE'('$fun'(82, 0, 1168517924), '$TUPPLE'(35169792167232))
c_code local build_ref_359
	ret_reg &ref[359]
	call_c   build_ref_337()
	call_c   Dyam_Closure_Aux(fun82,&ref[337])
	move_ret ref[359]
	c_ret

;; TERM 357: '$CLOSURE'('$fun'(81, 0, 1168502156), '$TUPPLE'(35169792167676))
c_code local build_ref_357
	ret_reg &ref[357]
	call_c   build_ref_356()
	call_c   Dyam_Closure_Aux(fun81,&ref[356])
	move_ret ref[357]
	c_ret

;; TERM 353: '$CLOSURE'('$fun'(80, 0, 1168494820), '$TUPPLE'(35169792167600))
c_code local build_ref_353
	ret_reg &ref[353]
	call_c   build_ref_352()
	call_c   Dyam_Closure_Aux(fun80,&ref[352])
	move_ret ref[353]
	c_ret

;; TERM 350: '$CLOSURE'('$fun'(79, 0, 1168495192), '$TUPPLE'(35169792167488))
c_code local build_ref_350
	ret_reg &ref[350]
	call_c   build_ref_347()
	call_c   Dyam_Closure_Aux(fun79,&ref[347])
	move_ret ref[350]
	c_ret

;; TERM 348: '$CLOSURE'('$fun'(78, 0, 1168485664), '$TUPPLE'(35169792167488))
c_code local build_ref_348
	ret_reg &ref[348]
	call_c   build_ref_347()
	call_c   Dyam_Closure_Aux(fun78,&ref[347])
	move_ret ref[348]
	c_ret

;; TERM 345: '$CLOSURE'('$fun'(77, 0, 1168486036), '$TUPPLE'(35169792167432))
c_code local build_ref_345
	ret_reg &ref[345]
	call_c   build_ref_342()
	call_c   Dyam_Closure_Aux(fun77,&ref[342])
	move_ret ref[345]
	c_ret

;; TERM 343: '$CLOSURE'('$fun'(76, 0, 1168479532), '$TUPPLE'(35169792167432))
c_code local build_ref_343
	ret_reg &ref[343]
	call_c   build_ref_342()
	call_c   Dyam_Closure_Aux(fun76,&ref[342])
	move_ret ref[343]
	c_ret

c_code local build_seed_60
	ret_reg &seed[60]
	call_c   build_ref_314()
	call_c   build_ref_339()
	call_c   Dyam_Seed_Start(&ref[314],&ref[339],&ref[339],fun0,1)
	call_c   build_ref_340()
	call_c   Dyam_Seed_Add_Comp(&ref[340],&ref[339],0)
	call_c   Dyam_Seed_End()
	move_ret seed[60]
	c_ret

;; TERM 340: '*RAI*'{xstart=> 'call_n/2'(_J), xend=> return(_G), aspop=> 'call_n/2'(_B), current=> return(_C)} :> '$$HOLE$$'
c_code local build_ref_340
	ret_reg &ref[340]
	call_c   build_ref_339()
	call_c   Dyam_Create_Binary(I(9),&ref[339],I(7))
	move_ret ref[340]
	c_ret

;; TERM 339: '*RAI*'{xstart=> 'call_n/2'(_J), xend=> return(_G), aspop=> 'call_n/2'(_B), current=> return(_C)}
c_code local build_ref_339
	ret_reg &ref[339]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_597()
	call_c   Dyam_Create_Unary(&ref[597],V(9))
	move_ret R(0)
	call_c   build_ref_594()
	call_c   Dyam_Create_Unary(&ref[594],V(6))
	move_ret R(1)
	call_c   build_ref_598()
	call_c   build_ref_335()
	call_c   build_ref_209()
	call_c   Dyam_Term_Start(&ref[598],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(&ref[335])
	call_c   Dyam_Term_Arg(&ref[209])
	call_c   Dyam_Term_End()
	move_ret ref[339]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun76[2]=[1,build_seed_60]

pl_code local fun76
	call_c   Dyam_Pool(pool_fun76)
	pl_jump  fun6(&seed[60],2)

;; TERM 342: '$TUPPLE'(35169792167432)
c_code local build_ref_342
	ret_reg &ref[342]
	call_c   Dyam_Create_Simple_Tupple(0,206045184)
	move_ret ref[342]
	c_ret

;; TERM 27: an
c_code local build_ref_27
	ret_reg &ref[27]
	call_c   Dyam_Create_Atom("an")
	move_ret ref[27]
	c_ret

long local pool_fun77[4]=[3,build_ref_343,build_ref_27,build_ref_20]

pl_code local fun77
	call_c   Dyam_Pool(pool_fun77)
	call_c   Dyam_Allocate(0)
	move     &ref[343], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	move     &ref[27], R(4)
	move     0, R(5)
	move     &ref[20], R(6)
	move     0, R(7)
	call_c   Dyam_Reg_Deallocate(4)
	pl_jump  fun31()

pl_code local fun78
	call_c   build_ref_345()
	call_c   Dyam_Unify(V(8),&ref[345])
	fail_ret
	pl_jump  Follow_Cont(V(7))

;; TERM 347: '$TUPPLE'(35169792167488)
c_code local build_ref_347
	ret_reg &ref[347]
	call_c   Dyam_Create_Simple_Tupple(0,209190912)
	move_ret ref[347]
	c_ret

long local pool_fun79[3]=[2,build_ref_348,build_ref_335]

pl_code local fun79
	call_c   Dyam_Pool(pool_fun79)
	call_c   Dyam_Allocate(0)
	move     &ref[348], R(0)
	move     S(5), R(1)
	move     &ref[335], R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Load(4,V(9))
	call_c   Dyam_Reg_Load(6,V(6))
	call_c   Dyam_Reg_Deallocate(4)
	pl_jump  fun62()

pl_code local fun80
	call_c   build_ref_350()
	call_c   Dyam_Unify(V(13),&ref[350])
	fail_ret
	pl_jump  Follow_Cont(V(12))

;; TERM 352: '$TUPPLE'(35169792167600)
c_code local build_ref_352
	ret_reg &ref[352]
	call_c   Dyam_Create_Simple_Tupple(0,209289216)
	move_ret ref[352]
	c_ret

;; TERM 354: verbose!anchor(_D, _K, _L, an, a, _O, tag_anchor{family=> a, coanchors=> _P, equations=> _Q})
c_code local build_ref_354
	ret_reg &ref[354]
	call_c   build_ref_586()
	call_c   build_ref_27()
	call_c   build_ref_20()
	call_c   build_ref_313()
	call_c   Dyam_Term_Start(&ref[586],7)
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(&ref[27])
	call_c   Dyam_Term_Arg(&ref[20])
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(&ref[313])
	call_c   Dyam_Term_End()
	move_ret ref[354]
	c_ret

long local pool_fun81[5]=[4,build_ref_313,build_ref_20,build_ref_353,build_ref_354]

pl_code local fun81
	call_c   Dyam_Pool(pool_fun81)
	call_c   Dyam_Allocate(0)
	move     &ref[313], R(0)
	move     S(5), R(1)
	move     V(3), R(2)
	move     S(5), R(3)
	move     &ref[20], R(4)
	move     0, R(5)
	call_c   Dyam_Reg_Load(6,V(10))
	call_c   Dyam_Reg_Load(8,V(11))
	move     &ref[20], R(10)
	move     0, R(11)
	move     V(14), R(12)
	move     S(5), R(13)
	pl_call  pred_anchor_7()
	move     &ref[353], R(0)
	move     S(5), R(1)
	move     &ref[354], R(2)
	move     S(5), R(3)
	move     N(0), R(4)
	move     0, R(5)
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  fun29()

;; TERM 356: '$TUPPLE'(35169792167676)
c_code local build_ref_356
	ret_reg &ref[356]
	call_c   Dyam_Create_Simple_Tupple(0,209682432)
	move_ret ref[356]
	c_ret

long local pool_fun82[4]=[3,build_ref_357,build_ref_174,build_ref_27]

pl_code local fun82
	call_c   Dyam_Pool(pool_fun82)
	call_c   Dyam_Allocate(0)
	move     &ref[357], R(0)
	move     S(5), R(1)
	move     V(13), R(2)
	move     S(5), R(3)
	move     V(12), R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Load(6,V(5))
	move     V(10), R(8)
	move     S(5), R(9)
	move     N(0), R(10)
	move     0, R(11)
	move     &ref[174], R(12)
	move     0, R(13)
	move     &ref[27], R(14)
	move     0, R(15)
	move     V(9), R(16)
	move     S(5), R(17)
	move     V(11), R(18)
	move     S(5), R(19)
	call_c   Dyam_Reg_Deallocate(10)
	pl_jump  fun67()

long local pool_fun83[4]=[3,build_ref_279,build_ref_359,build_ref_27]

pl_code local fun83
	call_c   Dyam_Pool(pool_fun83)
	call_c   Dyam_Unify_Item(&ref[279])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[359], R(0)
	move     S(5), R(1)
	move     V(8), R(2)
	move     S(5), R(3)
	move     V(7), R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Load(6,V(1))
	move     V(5), R(8)
	move     S(5), R(9)
	move     N(2), R(10)
	move     0, R(11)
	move     0, R(12)
	move     &ref[27], R(14)
	move     0, R(15)
	move     V(2), R(16)
	move     S(5), R(17)
	move     V(6), R(18)
	move     S(5), R(19)
	call_c   Dyam_Reg_Deallocate(10)
	pl_jump  fun60()

c_code local build_seed_7
	ret_reg &seed[7]
	call_c   build_ref_276()
	call_c   build_ref_384()
	call_c   Dyam_Seed_Start(&ref[276],&ref[384],I(0),fun9,1)
	call_c   build_ref_385()
	call_c   Dyam_Seed_Add_Comp(&ref[385],fun142,0)
	call_c   Dyam_Seed_End()
	move_ret seed[7]
	c_ret

;; TERM 385: '*CAI*'{current=> 'call_np/2'(_B)}
c_code local build_ref_385
	ret_reg &ref[385]
	call_c   build_ref_596()
	call_c   build_ref_260()
	call_c   Dyam_Create_Unary(&ref[596],&ref[260])
	move_ret ref[385]
	c_ret

;; TERM 437: '$CLOSURE'('$fun'(141, 0, 1168935812), '$TUPPLE'(35169792167232))
c_code local build_ref_437
	ret_reg &ref[437]
	call_c   build_ref_337()
	call_c   Dyam_Closure_Aux(fun141,&ref[337])
	move_ret ref[437]
	c_ret

;; TERM 435: '$CLOSURE'('$fun'(140, 0, 1168916900), '$TUPPLE'(35169792167116))
c_code local build_ref_435
	ret_reg &ref[435]
	call_c   build_ref_333()
	call_c   Dyam_Closure_Aux(fun140,&ref[333])
	move_ret ref[435]
	c_ret

;; TERM 433: '$CLOSURE'('$fun'(139, 0, 1168917428), '$TUPPLE'(35169792166920))
c_code local build_ref_433
	ret_reg &ref[433]
	call_c   build_ref_330()
	call_c   Dyam_Closure_Aux(fun139,&ref[330])
	move_ret ref[433]
	c_ret

;; TERM 431: '$CLOSURE'('$fun'(138, 0, 1168905840), '$TUPPLE'(35169792169332))
c_code local build_ref_431
	ret_reg &ref[431]
	call_c   build_ref_430()
	call_c   Dyam_Closure_Aux(fun138,&ref[430])
	move_ret ref[431]
	c_ret

;; TERM 404: tag_anchor{family=> nppn, coanchors=> _U, equations=> _V}
c_code local build_ref_404
	ret_reg &ref[404]
	call_c   build_ref_592()
	call_c   build_ref_56()
	call_c   Dyam_Term_Start(&ref[592],3)
	call_c   Dyam_Term_Arg(&ref[56])
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_Arg(V(21))
	call_c   Dyam_Term_End()
	move_ret ref[404]
	c_ret

;; TERM 56: nppn
c_code local build_ref_56
	ret_reg &ref[56]
	call_c   Dyam_Create_Atom("nppn")
	move_ret ref[56]
	c_ret

;; TERM 427: '$CLOSURE'('$fun'(137, 0, 1168897600), '$TUPPLE'(35169792169248))
c_code local build_ref_427
	ret_reg &ref[427]
	call_c   build_ref_426()
	call_c   Dyam_Closure_Aux(fun137,&ref[426])
	move_ret ref[427]
	c_ret

;; TERM 424: '$CLOSURE'('$fun'(136, 0, 1168886796), '$TUPPLE'(35169792169208))
c_code local build_ref_424
	ret_reg &ref[424]
	call_c   build_ref_423()
	call_c   Dyam_Closure_Aux(fun136,&ref[423])
	move_ret ref[424]
	c_ret

;; TERM 421: '$CLOSURE'('$fun'(135, 0, 1168887516), '$TUPPLE'(35169792166836))
c_code local build_ref_421
	ret_reg &ref[421]
	call_c   build_ref_326()
	call_c   Dyam_Closure_Aux(fun135,&ref[326])
	move_ret ref[421]
	c_ret

;; TERM 419: '$CLOSURE'('$fun'(134, 0, 1168878844), '$TUPPLE'(35169792166816))
c_code local build_ref_419
	ret_reg &ref[419]
	call_c   build_ref_323()
	call_c   Dyam_Closure_Aux(fun134,&ref[323])
	move_ret ref[419]
	c_ret

;; TERM 417: '$CLOSURE'('$fun'(133, 0, 1168879216), '$TUPPLE'(35169792166760))
c_code local build_ref_417
	ret_reg &ref[417]
	call_c   build_ref_318()
	call_c   Dyam_Closure_Aux(fun133,&ref[318])
	move_ret ref[417]
	c_ret

;; TERM 415: '$CLOSURE'('$fun'(132, 0, 1168873652), '$TUPPLE'(35169792166760))
c_code local build_ref_415
	ret_reg &ref[415]
	call_c   build_ref_318()
	call_c   Dyam_Closure_Aux(fun132,&ref[318])
	move_ret ref[415]
	c_ret

c_code local build_seed_71
	ret_reg &seed[71]
	call_c   build_ref_314()
	call_c   build_ref_412()
	call_c   Dyam_Seed_Start(&ref[314],&ref[412],&ref[412],fun0,1)
	call_c   build_ref_413()
	call_c   Dyam_Seed_Add_Comp(&ref[413],&ref[412],0)
	call_c   Dyam_Seed_End()
	move_ret seed[71]
	c_ret

;; TERM 413: '*RAI*'{xstart=> 'call_np/2'(_F), xend=> return(_J), aspop=> 'call_np/2'(_B), current=> return(_C)} :> '$$HOLE$$'
c_code local build_ref_413
	ret_reg &ref[413]
	call_c   build_ref_412()
	call_c   Dyam_Create_Binary(I(9),&ref[412],I(7))
	move_ret ref[413]
	c_ret

;; TERM 412: '*RAI*'{xstart=> 'call_np/2'(_F), xend=> return(_J), aspop=> 'call_np/2'(_B), current=> return(_C)}
c_code local build_ref_412
	ret_reg &ref[412]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_595()
	call_c   Dyam_Create_Unary(&ref[595],V(5))
	move_ret R(0)
	call_c   build_ref_594()
	call_c   Dyam_Create_Unary(&ref[594],V(9))
	move_ret R(1)
	call_c   build_ref_598()
	call_c   build_ref_260()
	call_c   build_ref_209()
	call_c   Dyam_Term_Start(&ref[598],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(&ref[260])
	call_c   Dyam_Term_Arg(&ref[209])
	call_c   Dyam_Term_End()
	move_ret ref[412]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun132[2]=[1,build_seed_71]

pl_code local fun132
	call_c   Dyam_Pool(pool_fun132)
	pl_jump  fun6(&seed[71],2)

long local pool_fun133[3]=[2,build_ref_415,build_ref_56]

pl_code local fun133
	call_c   Dyam_Pool(pool_fun133)
	call_c   Dyam_Allocate(0)
	move     &ref[415], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	move     &ref[56], R(4)
	move     0, R(5)
	move     &ref[56], R(6)
	move     0, R(7)
	call_c   Dyam_Reg_Deallocate(4)
	pl_jump  fun31()

pl_code local fun134
	call_c   build_ref_417()
	call_c   Dyam_Unify(V(8),&ref[417])
	fail_ret
	pl_jump  Follow_Cont(V(7))

pl_code local fun135
	call_c   build_ref_419()
	call_c   Dyam_Unify(V(13),&ref[419])
	fail_ret
	pl_jump  Follow_Cont(V(12))

long local pool_fun136[3]=[2,build_ref_421,build_ref_56]

pl_code local fun136
	call_c   Dyam_Pool(pool_fun136)
	call_c   Dyam_Allocate(0)
	move     &ref[421], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(14))
	call_c   Dyam_Reg_Load(4,V(11))
	move     N(2), R(6)
	move     0, R(7)
	move     &ref[56], R(8)
	move     0, R(9)
	call_c   Dyam_Reg_Deallocate(5)
fun131:
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(5)
	pl_call  fun6(&seed[69],1)
	pl_call  fun16(&seed[70],2,V(4))
	call_c   Dyam_Deallocate()
	pl_ret


;; TERM 423: '$TUPPLE'(35169792169208)
c_code local build_ref_423
	ret_reg &ref[423]
	call_c   Dyam_Create_Simple_Tupple(0,213630976)
	move_ret ref[423]
	c_ret

pl_code local fun137
	call_c   build_ref_424()
	call_c   Dyam_Unify(V(18),&ref[424])
	fail_ret
	pl_jump  Follow_Cont(V(17))

;; TERM 426: '$TUPPLE'(35169792169248)
c_code local build_ref_426
	ret_reg &ref[426]
	call_c   Dyam_Create_Simple_Tupple(0,213634048)
	move_ret ref[426]
	c_ret

;; TERM 428: verbose!anchor(_D, _P, _Q, nppn, p, _T, tag_anchor{family=> nppn, coanchors=> _U, equations=> _V})
c_code local build_ref_428
	ret_reg &ref[428]
	call_c   build_ref_586()
	call_c   build_ref_56()
	call_c   build_ref_57()
	call_c   build_ref_404()
	call_c   Dyam_Term_Start(&ref[586],7)
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(&ref[56])
	call_c   Dyam_Term_Arg(&ref[57])
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_Arg(&ref[404])
	call_c   Dyam_Term_End()
	move_ret ref[428]
	c_ret

long local pool_fun138[5]=[4,build_ref_404,build_ref_57,build_ref_427,build_ref_428]

pl_code local fun138
	call_c   Dyam_Pool(pool_fun138)
	call_c   Dyam_Allocate(0)
	move     &ref[404], R(0)
	move     S(5), R(1)
	move     V(3), R(2)
	move     S(5), R(3)
	move     &ref[57], R(4)
	move     0, R(5)
	call_c   Dyam_Reg_Load(6,V(15))
	call_c   Dyam_Reg_Load(8,V(16))
	move     &ref[57], R(10)
	move     0, R(11)
	move     V(19), R(12)
	move     S(5), R(13)
	pl_call  pred_anchor_7()
	move     &ref[427], R(0)
	move     S(5), R(1)
	move     &ref[428], R(2)
	move     S(5), R(3)
	move     N(1), R(4)
	move     0, R(5)
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  fun29()

;; TERM 430: '$TUPPLE'(35169792169332)
c_code local build_ref_430
	ret_reg &ref[430]
	call_c   Dyam_Create_Simple_Tupple(0,213646336)
	move_ret ref[430]
	c_ret

long local pool_fun139[4]=[3,build_ref_431,build_ref_174,build_ref_56]

pl_code local fun139
	call_c   Dyam_Pool(pool_fun139)
	call_c   Dyam_Allocate(0)
	move     &ref[431], R(0)
	move     S(5), R(1)
	move     V(18), R(2)
	move     S(5), R(3)
	move     V(17), R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Load(6,V(10))
	move     V(15), R(8)
	move     S(5), R(9)
	move     N(1), R(10)
	move     0, R(11)
	move     &ref[174], R(12)
	move     0, R(13)
	move     &ref[56], R(14)
	move     0, R(15)
	move     V(14), R(16)
	move     S(5), R(17)
	move     V(16), R(18)
	move     S(5), R(19)
	call_c   Dyam_Reg_Deallocate(10)
fun129:
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(10)
	call_c   Dyam_Choice(fun128)
	call_c   Dyam_Unify(V(3),V(2))
	fail_ret
	call_c   Dyam_Unify(V(4),V(5))
	fail_ret
	call_c   Dyam_Unify(V(9),V(10))
	fail_ret
	pl_call  Follow_Cont(V(1))
	pl_fail


long local pool_fun140[4]=[3,build_ref_433,build_ref_174,build_ref_56]

pl_code local fun140
	call_c   Dyam_Pool(pool_fun140)
	call_c   Dyam_Allocate(0)
	move     &ref[433], R(0)
	move     S(5), R(1)
	move     V(13), R(2)
	move     S(5), R(3)
	move     V(12), R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Load(6,V(9))
	move     V(10), R(8)
	move     S(5), R(9)
	move     N(3), R(10)
	move     0, R(11)
	move     &ref[174], R(12)
	move     0, R(13)
	move     &ref[56], R(14)
	move     0, R(15)
	call_c   Dyam_Reg_Load(16,V(6))
	move     V(11), R(18)
	move     S(5), R(19)
	call_c   Dyam_Reg_Deallocate(10)
	pl_jump  fun43()

long local pool_fun141[3]=[2,build_ref_435,build_ref_260]

pl_code local fun141
	call_c   Dyam_Pool(pool_fun141)
	call_c   Dyam_Allocate(0)
	move     &ref[435], R(0)
	move     S(5), R(1)
	move     &ref[260], R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Load(4,V(5))
	move     V(9), R(6)
	move     S(5), R(7)
	call_c   Dyam_Reg_Deallocate(4)
fun124:
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Reg_Bind(2,V(4))
	call_c   Dyam_Multi_Reg_Bind(4,2,2)
	pl_call  fun6(&seed[63],1)
	pl_call  fun22(&seed[64],2)
	call_c   Dyam_Deallocate()
	pl_ret


long local pool_fun142[4]=[3,build_ref_385,build_ref_437,build_ref_56]

pl_code local fun142
	call_c   Dyam_Pool(pool_fun142)
	call_c   Dyam_Unify_Item(&ref[385])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[437], R(0)
	move     S(5), R(1)
	move     V(8), R(2)
	move     S(5), R(3)
	move     V(7), R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Load(6,V(1))
	move     V(5), R(8)
	move     S(5), R(9)
	move     N(4), R(10)
	move     0, R(11)
	move     0, R(12)
	move     &ref[56], R(14)
	move     0, R(15)
	move     V(2), R(16)
	move     S(5), R(17)
	move     V(6), R(18)
	move     S(5), R(19)
	call_c   Dyam_Reg_Deallocate(10)
	pl_jump  fun43()

;; TERM 384: '*SA-AUX-FIRST*'('call_np/2'(_B)) :> []
c_code local build_ref_384
	ret_reg &ref[384]
	call_c   build_ref_383()
	call_c   Dyam_Create_Binary(I(9),&ref[383],I(0))
	move_ret ref[384]
	c_ret

;; TERM 383: '*SA-AUX-FIRST*'('call_np/2'(_B))
c_code local build_ref_383
	ret_reg &ref[383]
	call_c   build_ref_276()
	call_c   build_ref_260()
	call_c   Dyam_Create_Unary(&ref[276],&ref[260])
	move_ret ref[383]
	c_ret

c_code local build_seed_8
	ret_reg &seed[8]
	call_c   build_ref_276()
	call_c   build_ref_439()
	call_c   Dyam_Seed_Start(&ref[276],&ref[439],I(0),fun9,1)
	call_c   build_ref_440()
	call_c   Dyam_Seed_Add_Comp(&ref[440],fun165,0)
	call_c   Dyam_Seed_End()
	move_ret seed[8]
	c_ret

;; TERM 440: '*CAI*'{current=> 'call_vp/2'(_B)}
c_code local build_ref_440
	ret_reg &ref[440]
	call_c   build_ref_596()
	call_c   build_ref_494()
	call_c   Dyam_Create_Unary(&ref[596],&ref[494])
	move_ret ref[440]
	c_ret

;; TERM 494: 'call_vp/2'(_B)
c_code local build_ref_494
	ret_reg &ref[494]
	call_c   build_ref_599()
	call_c   Dyam_Create_Unary(&ref[599],V(1))
	move_ret ref[494]
	c_ret

;; TERM 599: 'call_vp/2'
c_code local build_ref_599
	ret_reg &ref[599]
	call_c   Dyam_Create_Atom("call_vp/2")
	move_ret ref[599]
	c_ret

;; TERM 496: '$CLOSURE'('$fun'(164, 0, 1169119532), '$TUPPLE'(35169792167232))
c_code local build_ref_496
	ret_reg &ref[496]
	call_c   build_ref_337()
	call_c   Dyam_Closure_Aux(fun164,&ref[337])
	move_ret ref[496]
	c_ret

;; TERM 493: '$CLOSURE'('$fun'(163, 0, 1169120204), '$TUPPLE'(35169792167116))
c_code local build_ref_493
	ret_reg &ref[493]
	call_c   build_ref_333()
	call_c   Dyam_Closure_Aux(fun163,&ref[333])
	move_ret ref[493]
	c_ret

;; TERM 491: '$CLOSURE'('$fun'(162, 0, 1169107508), '$TUPPLE'(35169792166920))
c_code local build_ref_491
	ret_reg &ref[491]
	call_c   build_ref_330()
	call_c   Dyam_Closure_Aux(fun162,&ref[330])
	move_ret ref[491]
	c_ret

;; TERM 489: '$CLOSURE'('$fun'(161, 0, 1169093704), '$TUPPLE'(35169792169332))
c_code local build_ref_489
	ret_reg &ref[489]
	call_c   build_ref_430()
	call_c   Dyam_Closure_Aux(fun161,&ref[430])
	move_ret ref[489]
	c_ret

;; TERM 472: tag_anchor{family=> vppn, coanchors=> _U, equations=> _V}
c_code local build_ref_472
	ret_reg &ref[472]
	call_c   build_ref_592()
	call_c   build_ref_71()
	call_c   Dyam_Term_Start(&ref[592],3)
	call_c   Dyam_Term_Arg(&ref[71])
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_Arg(V(21))
	call_c   Dyam_Term_End()
	move_ret ref[472]
	c_ret

;; TERM 71: vppn
c_code local build_ref_71
	ret_reg &ref[71]
	call_c   Dyam_Create_Atom("vppn")
	move_ret ref[71]
	c_ret

;; TERM 486: '$CLOSURE'('$fun'(160, 0, 1169094472), '$TUPPLE'(35169792169248))
c_code local build_ref_486
	ret_reg &ref[486]
	call_c   build_ref_426()
	call_c   Dyam_Closure_Aux(fun160,&ref[426])
	move_ret ref[486]
	c_ret

;; TERM 484: '$CLOSURE'('$fun'(159, 0, 1169085632), '$TUPPLE'(35169792169208))
c_code local build_ref_484
	ret_reg &ref[484]
	call_c   build_ref_423()
	call_c   Dyam_Closure_Aux(fun159,&ref[423])
	move_ret ref[484]
	c_ret

;; TERM 482: '$CLOSURE'('$fun'(158, 0, 1169086268), '$TUPPLE'(35169792166836))
c_code local build_ref_482
	ret_reg &ref[482]
	call_c   build_ref_326()
	call_c   Dyam_Closure_Aux(fun158,&ref[326])
	move_ret ref[482]
	c_ret

;; TERM 480: '$CLOSURE'('$fun'(157, 0, 1169078620), '$TUPPLE'(35169792166816))
c_code local build_ref_480
	ret_reg &ref[480]
	call_c   build_ref_323()
	call_c   Dyam_Closure_Aux(fun157,&ref[323])
	move_ret ref[480]
	c_ret

;; TERM 478: '$CLOSURE'('$fun'(156, 0, 1169078992), '$TUPPLE'(35169792166760))
c_code local build_ref_478
	ret_reg &ref[478]
	call_c   build_ref_318()
	call_c   Dyam_Closure_Aux(fun156,&ref[318])
	move_ret ref[478]
	c_ret

;; TERM 476: '$CLOSURE'('$fun'(155, 0, 1169070356), '$TUPPLE'(35169792166760))
c_code local build_ref_476
	ret_reg &ref[476]
	call_c   build_ref_318()
	call_c   Dyam_Closure_Aux(fun155,&ref[318])
	move_ret ref[476]
	c_ret

c_code local build_seed_82
	ret_reg &seed[82]
	call_c   build_ref_314()
	call_c   build_ref_473()
	call_c   Dyam_Seed_Start(&ref[314],&ref[473],&ref[473],fun0,1)
	call_c   build_ref_474()
	call_c   Dyam_Seed_Add_Comp(&ref[474],&ref[473],0)
	call_c   Dyam_Seed_End()
	move_ret seed[82]
	c_ret

;; TERM 474: '*RAI*'{xstart=> 'call_vp/2'(_F), xend=> return(_J), aspop=> 'call_vp/2'(_B), current=> return(_C)} :> '$$HOLE$$'
c_code local build_ref_474
	ret_reg &ref[474]
	call_c   build_ref_473()
	call_c   Dyam_Create_Binary(I(9),&ref[473],I(7))
	move_ret ref[474]
	c_ret

;; TERM 473: '*RAI*'{xstart=> 'call_vp/2'(_F), xend=> return(_J), aspop=> 'call_vp/2'(_B), current=> return(_C)}
c_code local build_ref_473
	ret_reg &ref[473]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_599()
	call_c   Dyam_Create_Unary(&ref[599],V(5))
	move_ret R(0)
	call_c   build_ref_594()
	call_c   Dyam_Create_Unary(&ref[594],V(9))
	move_ret R(1)
	call_c   build_ref_598()
	call_c   build_ref_494()
	call_c   build_ref_209()
	call_c   Dyam_Term_Start(&ref[598],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(&ref[494])
	call_c   Dyam_Term_Arg(&ref[209])
	call_c   Dyam_Term_End()
	move_ret ref[473]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun155[2]=[1,build_seed_82]

pl_code local fun155
	call_c   Dyam_Pool(pool_fun155)
	pl_jump  fun6(&seed[82],2)

long local pool_fun156[3]=[2,build_ref_476,build_ref_71]

pl_code local fun156
	call_c   Dyam_Pool(pool_fun156)
	call_c   Dyam_Allocate(0)
	move     &ref[476], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	move     &ref[71], R(4)
	move     0, R(5)
	move     &ref[71], R(6)
	move     0, R(7)
	call_c   Dyam_Reg_Deallocate(4)
	pl_jump  fun31()

pl_code local fun157
	call_c   build_ref_478()
	call_c   Dyam_Unify(V(8),&ref[478])
	fail_ret
	pl_jump  Follow_Cont(V(7))

pl_code local fun158
	call_c   build_ref_480()
	call_c   Dyam_Unify(V(13),&ref[480])
	fail_ret
	pl_jump  Follow_Cont(V(12))

long local pool_fun159[3]=[2,build_ref_482,build_ref_71]

pl_code local fun159
	call_c   Dyam_Pool(pool_fun159)
	call_c   Dyam_Allocate(0)
	move     &ref[482], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(14))
	call_c   Dyam_Reg_Load(4,V(11))
	move     N(2), R(6)
	move     0, R(7)
	move     &ref[71], R(8)
	move     0, R(9)
	call_c   Dyam_Reg_Deallocate(5)
	pl_jump  fun131()

pl_code local fun160
	call_c   build_ref_484()
	call_c   Dyam_Unify(V(18),&ref[484])
	fail_ret
	pl_jump  Follow_Cont(V(17))

;; TERM 487: verbose!anchor(_D, _P, _Q, vppn, p, _T, tag_anchor{family=> vppn, coanchors=> _U, equations=> _V})
c_code local build_ref_487
	ret_reg &ref[487]
	call_c   build_ref_586()
	call_c   build_ref_71()
	call_c   build_ref_57()
	call_c   build_ref_472()
	call_c   Dyam_Term_Start(&ref[586],7)
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(&ref[71])
	call_c   Dyam_Term_Arg(&ref[57])
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_Arg(&ref[472])
	call_c   Dyam_Term_End()
	move_ret ref[487]
	c_ret

long local pool_fun161[5]=[4,build_ref_472,build_ref_57,build_ref_486,build_ref_487]

pl_code local fun161
	call_c   Dyam_Pool(pool_fun161)
	call_c   Dyam_Allocate(0)
	move     &ref[472], R(0)
	move     S(5), R(1)
	move     V(3), R(2)
	move     S(5), R(3)
	move     &ref[57], R(4)
	move     0, R(5)
	call_c   Dyam_Reg_Load(6,V(15))
	call_c   Dyam_Reg_Load(8,V(16))
	move     &ref[57], R(10)
	move     0, R(11)
	move     V(19), R(12)
	move     S(5), R(13)
	pl_call  pred_anchor_7()
	move     &ref[486], R(0)
	move     S(5), R(1)
	move     &ref[487], R(2)
	move     S(5), R(3)
	move     N(1), R(4)
	move     0, R(5)
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  fun29()

long local pool_fun162[4]=[3,build_ref_489,build_ref_174,build_ref_71]

pl_code local fun162
	call_c   Dyam_Pool(pool_fun162)
	call_c   Dyam_Allocate(0)
	move     &ref[489], R(0)
	move     S(5), R(1)
	move     V(18), R(2)
	move     S(5), R(3)
	move     V(17), R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Load(6,V(10))
	move     V(15), R(8)
	move     S(5), R(9)
	move     N(1), R(10)
	move     0, R(11)
	move     &ref[174], R(12)
	move     0, R(13)
	move     &ref[71], R(14)
	move     0, R(15)
	move     V(14), R(16)
	move     S(5), R(17)
	move     V(16), R(18)
	move     S(5), R(19)
	call_c   Dyam_Reg_Deallocate(10)
	pl_jump  fun129()

long local pool_fun163[4]=[3,build_ref_491,build_ref_174,build_ref_71]

pl_code local fun163
	call_c   Dyam_Pool(pool_fun163)
	call_c   Dyam_Allocate(0)
	move     &ref[491], R(0)
	move     S(5), R(1)
	move     V(13), R(2)
	move     S(5), R(3)
	move     V(12), R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Load(6,V(9))
	move     V(10), R(8)
	move     S(5), R(9)
	move     N(3), R(10)
	move     0, R(11)
	move     &ref[174], R(12)
	move     0, R(13)
	move     &ref[71], R(14)
	move     0, R(15)
	call_c   Dyam_Reg_Load(16,V(6))
	move     V(11), R(18)
	move     S(5), R(19)
	call_c   Dyam_Reg_Deallocate(10)
fun154:
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(10)
	call_c   Dyam_Choice(fun153)
	call_c   Dyam_Unify(V(3),V(2))
	fail_ret
	call_c   Dyam_Unify(V(4),V(5))
	fail_ret
	call_c   Dyam_Unify(V(9),V(10))
	fail_ret
	pl_call  Follow_Cont(V(1))
	pl_fail


long local pool_fun164[3]=[2,build_ref_493,build_ref_494]

pl_code local fun164
	call_c   Dyam_Pool(pool_fun164)
	call_c   Dyam_Allocate(0)
	move     &ref[493], R(0)
	move     S(5), R(1)
	move     &ref[494], R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Load(4,V(5))
	move     V(9), R(6)
	move     S(5), R(7)
	call_c   Dyam_Reg_Deallocate(4)
fun149:
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Reg_Bind(2,V(4))
	call_c   Dyam_Multi_Reg_Bind(4,2,2)
	pl_call  fun6(&seed[76],1)
	pl_call  fun22(&seed[77],2)
	call_c   Dyam_Deallocate()
	pl_ret


long local pool_fun165[4]=[3,build_ref_440,build_ref_496,build_ref_71]

pl_code local fun165
	call_c   Dyam_Pool(pool_fun165)
	call_c   Dyam_Unify_Item(&ref[440])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[496], R(0)
	move     S(5), R(1)
	move     V(8), R(2)
	move     S(5), R(3)
	move     V(7), R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Load(6,V(1))
	move     V(5), R(8)
	move     S(5), R(9)
	move     N(4), R(10)
	move     0, R(11)
	move     0, R(12)
	move     &ref[71], R(14)
	move     0, R(15)
	move     V(2), R(16)
	move     S(5), R(17)
	move     V(6), R(18)
	move     S(5), R(19)
	call_c   Dyam_Reg_Deallocate(10)
fun147:
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(10)
	call_c   Dyam_Choice(fun146)
	call_c   Dyam_Unify(V(3),V(2))
	fail_ret
	call_c   Dyam_Unify(V(4),V(5))
	fail_ret
	call_c   Dyam_Unify(V(9),V(10))
	fail_ret
	pl_call  Follow_Cont(V(1))
	pl_fail


;; TERM 439: '*SA-AUX-FIRST*'('call_vp/2'(_B)) :> []
c_code local build_ref_439
	ret_reg &ref[439]
	call_c   build_ref_438()
	call_c   Dyam_Create_Binary(I(9),&ref[438],I(0))
	move_ret ref[439]
	c_ret

;; TERM 438: '*SA-AUX-FIRST*'('call_vp/2'(_B))
c_code local build_ref_438
	ret_reg &ref[438]
	call_c   build_ref_276()
	call_c   build_ref_494()
	call_c   Dyam_Create_Unary(&ref[276],&ref[494])
	move_ret ref[438]
	c_ret

c_code local build_seed_11
	ret_reg &seed[11]
	call_c   build_ref_170()
	call_c   build_ref_498()
	call_c   Dyam_Seed_Start(&ref[170],&ref[498],I(0),fun9,1)
	call_c   build_ref_499()
	call_c   Dyam_Seed_Add_Comp(&ref[499],fun187,0)
	call_c   Dyam_Seed_End()
	move_ret seed[11]
	c_ret

;; TERM 499: '*SACITEM*'('call_s/2'(_B))
c_code local build_ref_499
	ret_reg &ref[499]
	call_c   build_ref_148()
	call_c   build_ref_519()
	call_c   Dyam_Create_Unary(&ref[148],&ref[519])
	move_ret ref[499]
	c_ret

;; TERM 519: 'call_s/2'(_B)
c_code local build_ref_519
	ret_reg &ref[519]
	call_c   build_ref_600()
	call_c   Dyam_Create_Unary(&ref[600],V(1))
	move_ret ref[519]
	c_ret

;; TERM 600: 'call_s/2'
c_code local build_ref_600
	ret_reg &ref[600]
	call_c   Dyam_Create_Atom("call_s/2")
	move_ret ref[600]
	c_ret

;; TERM 565: '$CLOSURE'('$fun'(186, 0, 1169337052), '$TUPPLE'(35169792171988))
c_code local build_ref_565
	ret_reg &ref[565]
	call_c   build_ref_564()
	call_c   Dyam_Closure_Aux(fun186,&ref[564])
	move_ret ref[565]
	c_ret

;; TERM 562: '$CLOSURE'('$fun'(185, 0, 1169316056), '$TUPPLE'(35169792171904))
c_code local build_ref_562
	ret_reg &ref[562]
	call_c   build_ref_561()
	call_c   Dyam_Closure_Aux(fun185,&ref[561])
	move_ret ref[562]
	c_ret

;; TERM 513: xa
c_code local build_ref_513
	ret_reg &ref[513]
	call_c   Dyam_Create_Atom("xa")
	move_ret ref[513]
	c_ret

c_code local build_seed_87
	ret_reg &seed[87]
	call_c   build_ref_158()
	call_c   build_ref_518()
	call_c   Dyam_Seed_Start(&ref[158],&ref[518],&ref[518],fun9,1)
	call_c   build_ref_516()
	call_c   Dyam_Seed_Add_Comp(&ref[516],fun173,0)
	call_c   Dyam_Seed_End()
	move_ret seed[87]
	c_ret

;; TERM 516: '*RITEM*'('_cutter0'(_O), return(_K))
c_code local build_ref_516
	ret_reg &ref[516]
	call_c   build_ref_0()
	call_c   build_ref_514()
	call_c   build_ref_515()
	call_c   Dyam_Create_Binary(&ref[0],&ref[514],&ref[515])
	move_ret ref[516]
	c_ret

;; TERM 515: return(_K)
c_code local build_ref_515
	ret_reg &ref[515]
	call_c   build_ref_594()
	call_c   Dyam_Create_Unary(&ref[594],V(10))
	move_ret ref[515]
	c_ret

;; TERM 514: '_cutter0'(_O)
c_code local build_ref_514
	ret_reg &ref[514]
	call_c   build_ref_601()
	call_c   Dyam_Create_Unary(&ref[601],V(14))
	move_ret ref[514]
	c_ret

;; TERM 601: '_cutter0'
c_code local build_ref_601
	ret_reg &ref[601]
	call_c   Dyam_Create_Atom("_cutter0")
	move_ret ref[601]
	c_ret

;; TERM 525: '$CLOSURE'('$fun'(172, 0, 1169219412), '$TUPPLE'(35169792164304))
c_code local build_ref_525
	ret_reg &ref[525]
	call_c   build_ref_213()
	call_c   Dyam_Closure_Aux(fun172,&ref[213])
	move_ret ref[525]
	c_ret

;; TERM 523: '$CLOSURE'('$fun'(171, 0, 1169211800), '$TUPPLE'(35169792164304))
c_code local build_ref_523
	ret_reg &ref[523]
	call_c   build_ref_213()
	call_c   Dyam_Closure_Aux(fun171,&ref[213])
	move_ret ref[523]
	c_ret

c_code local build_seed_88
	ret_reg &seed[88]
	call_c   build_ref_0()
	call_c   build_ref_520()
	call_c   Dyam_Seed_Start(&ref[0],&ref[520],&ref[520],fun0,1)
	call_c   build_ref_521()
	call_c   Dyam_Seed_Add_Comp(&ref[521],&ref[520],0)
	call_c   Dyam_Seed_End()
	move_ret seed[88]
	c_ret

;; TERM 521: '*RITEM*'('call_s/2'(_B), return(_C)) :> '$$HOLE$$'
c_code local build_ref_521
	ret_reg &ref[521]
	call_c   build_ref_520()
	call_c   Dyam_Create_Binary(I(9),&ref[520],I(7))
	move_ret ref[521]
	c_ret

;; TERM 520: '*RITEM*'('call_s/2'(_B), return(_C))
c_code local build_ref_520
	ret_reg &ref[520]
	call_c   build_ref_0()
	call_c   build_ref_519()
	call_c   build_ref_209()
	call_c   Dyam_Create_Binary(&ref[0],&ref[519],&ref[209])
	move_ret ref[520]
	c_ret

long local pool_fun171[2]=[1,build_seed_88]

pl_code local fun171
	call_c   Dyam_Pool(pool_fun171)
	pl_jump  fun6(&seed[88],2)

;; TERM 105: tn1
c_code local build_ref_105
	ret_reg &ref[105]
	call_c   Dyam_Create_Atom("tn1")
	move_ret ref[105]
	c_ret

long local pool_fun172[3]=[2,build_ref_523,build_ref_105]

pl_code local fun172
	call_c   Dyam_Pool(pool_fun172)
	call_c   Dyam_Allocate(0)
	move     &ref[523], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	move     &ref[105], R(4)
	move     0, R(5)
	move     &ref[105], R(6)
	move     0, R(7)
	call_c   Dyam_Reg_Deallocate(4)
	pl_jump  fun31()

long local pool_fun173[3]=[2,build_ref_516,build_ref_525]

pl_code local fun173
	call_c   Dyam_Pool(pool_fun173)
	call_c   Dyam_Unify_Item(&ref[516])
	fail_ret
	call_c   Dyam_Unify(V(12),&ref[525])
	fail_ret
	pl_jump  Follow_Cont(V(11))

;; TERM 518: '*RITEM*'('_cutter0'(_O), return(_K)) :> '$FUNTUPPLE'(toto_26, '$TUPPLE'(35169792171044))
c_code local build_ref_518
	ret_reg &ref[518]
	call_c   build_ref_516()
	call_c   build_ref_517()
	call_c   Dyam_Create_Binary(I(9),&ref[516],&ref[517])
	move_ret ref[518]
	c_ret

;; TERM 517: '$FUNTUPPLE'(toto_26, '$TUPPLE'(35169792171044))
c_code local build_ref_517
	ret_reg &ref[517]
	call_c   build_ref_602()
	call_c   Dyam_Create_Fun_With_Tupple(&ref[602],0,201523200)
	move_ret ref[517]
	c_ret

;; TERM 602: toto_26
c_code local build_ref_602
	ret_reg &ref[602]
	call_c   Dyam_Create_Atom("toto_26")
	move_ret ref[602]
	c_ret

;; TERM 158: '*CURNEXT*'
c_code local build_ref_158
	ret_reg &ref[158]
	call_c   Dyam_Create_Atom("*CURNEXT*")
	move_ret ref[158]
	c_ret

pl_code local fun22
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Choice(fun15)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Subsume(R(0))
	fail_ret
	call_c   Dyam_Cut()
	pl_ret

;; TERM 559: '$CLOSURE'('$fun'(184, 0, 1169316668), '$TUPPLE'(35169792171732))
c_code local build_ref_559
	ret_reg &ref[559]
	call_c   build_ref_558()
	call_c   Dyam_Closure_Aux(fun184,&ref[558])
	move_ret ref[559]
	c_ret

;; TERM 556: '$CLOSURE'('$fun'(183, 0, 1169305080), '$TUPPLE'(35169792171520))
c_code local build_ref_556
	ret_reg &ref[556]
	call_c   build_ref_555()
	call_c   Dyam_Closure_Aux(fun183,&ref[555])
	move_ret ref[556]
	c_ret

;; TERM 539: tag_anchor{family=> tn1, coanchors=> _Z, equations=> _A1}
c_code local build_ref_539
	ret_reg &ref[539]
	call_c   build_ref_592()
	call_c   build_ref_105()
	call_c   Dyam_Term_Start(&ref[592],3)
	call_c   Dyam_Term_Arg(&ref[105])
	call_c   Dyam_Term_Arg(V(25))
	call_c   Dyam_Term_Arg(V(26))
	call_c   Dyam_Term_End()
	move_ret ref[539]
	c_ret

;; TERM 552: '$CLOSURE'('$fun'(182, 0, 1169294840), '$TUPPLE'(35169792171436))
c_code local build_ref_552
	ret_reg &ref[552]
	call_c   build_ref_551()
	call_c   Dyam_Closure_Aux(fun182,&ref[551])
	move_ret ref[552]
	c_ret

;; TERM 549: '$CLOSURE'('$fun'(181, 0, 1169295296), '$TUPPLE'(35169792171396))
c_code local build_ref_549
	ret_reg &ref[549]
	call_c   build_ref_548()
	call_c   Dyam_Closure_Aux(fun181,&ref[548])
	move_ret ref[549]
	c_ret

;; TERM 546: '$CLOSURE'('$fun'(180, 0, 1169285780), '$TUPPLE'(35169792171332))
c_code local build_ref_546
	ret_reg &ref[546]
	call_c   build_ref_545()
	call_c   Dyam_Closure_Aux(fun180,&ref[545])
	move_ret ref[546]
	c_ret

;; TERM 543: '$CLOSURE'('$fun'(179, 0, 1169279192), '$TUPPLE'(35169792171056))
c_code local build_ref_543
	ret_reg &ref[543]
	call_c   build_ref_542()
	call_c   Dyam_Closure_Aux(fun179,&ref[542])
	move_ret ref[543]
	c_ret

c_code local build_seed_93
	ret_reg &seed[93]
	call_c   build_ref_0()
	call_c   build_ref_516()
	call_c   Dyam_Seed_Start(&ref[0],&ref[516],&ref[516],fun0,1)
	call_c   build_ref_540()
	call_c   Dyam_Seed_Add_Comp(&ref[540],&ref[516],0)
	call_c   Dyam_Seed_End()
	move_ret seed[93]
	c_ret

;; TERM 540: '*RITEM*'('_cutter0'(_O), return(_K)) :> '$$HOLE$$'
c_code local build_ref_540
	ret_reg &ref[540]
	call_c   build_ref_516()
	call_c   Dyam_Create_Binary(I(9),&ref[516],I(7))
	move_ret ref[540]
	c_ret

long local pool_fun179[2]=[1,build_seed_93]

pl_code local fun179
	call_c   Dyam_Pool(pool_fun179)
	pl_jump  fun6(&seed[93],12)

;; TERM 542: '$TUPPLE'(35169792171056)
c_code local build_ref_542
	ret_reg &ref[542]
	call_c   Dyam_Create_Simple_Tupple(0,278528)
	move_ret ref[542]
	c_ret

pl_code local fun180
	call_c   build_ref_543()
	call_c   Dyam_Unify(V(18),&ref[543])
	fail_ret
	pl_jump  Follow_Cont(V(17))

;; TERM 545: '$TUPPLE'(35169792171332)
c_code local build_ref_545
	ret_reg &ref[545]
	call_c   Dyam_Create_Simple_Tupple(0,281600)
	move_ret ref[545]
	c_ret

long local pool_fun181[3]=[2,build_ref_546,build_ref_105]

pl_code local fun181
	call_c   Dyam_Pool(pool_fun181)
	call_c   Dyam_Allocate(0)
	move     &ref[546], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(19))
	call_c   Dyam_Reg_Load(4,V(16))
	move     N(2), R(6)
	move     0, R(7)
	move     &ref[105], R(8)
	move     0, R(9)
	call_c   Dyam_Reg_Deallocate(5)
	pl_jump  fun131()

;; TERM 548: '$TUPPLE'(35169792171396)
c_code local build_ref_548
	ret_reg &ref[548]
	call_c   Dyam_Create_Simple_Tupple(0,286208)
	move_ret ref[548]
	c_ret

pl_code local fun182
	call_c   build_ref_549()
	call_c   Dyam_Unify(V(23),&ref[549])
	fail_ret
	pl_jump  Follow_Cont(V(22))

;; TERM 551: '$TUPPLE'(35169792171436)
c_code local build_ref_551
	ret_reg &ref[551]
	call_c   Dyam_Create_Simple_Tupple(0,286304)
	move_ret ref[551]
	c_ret

;; TERM 553: verbose!anchor(_I, _U, _V, tn1, v, _Y, tag_anchor{family=> tn1, coanchors=> _Z, equations=> _A1})
c_code local build_ref_553
	ret_reg &ref[553]
	call_c   build_ref_586()
	call_c   build_ref_105()
	call_c   build_ref_106()
	call_c   build_ref_539()
	call_c   Dyam_Term_Start(&ref[586],7)
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_Arg(V(21))
	call_c   Dyam_Term_Arg(&ref[105])
	call_c   Dyam_Term_Arg(&ref[106])
	call_c   Dyam_Term_Arg(V(24))
	call_c   Dyam_Term_Arg(&ref[539])
	call_c   Dyam_Term_End()
	move_ret ref[553]
	c_ret

long local pool_fun183[5]=[4,build_ref_539,build_ref_106,build_ref_552,build_ref_553]

pl_code local fun183
	call_c   Dyam_Pool(pool_fun183)
	call_c   Dyam_Allocate(0)
	move     &ref[539], R(0)
	move     S(5), R(1)
	move     V(8), R(2)
	move     S(5), R(3)
	move     &ref[106], R(4)
	move     0, R(5)
	call_c   Dyam_Reg_Load(6,V(20))
	call_c   Dyam_Reg_Load(8,V(21))
	move     &ref[106], R(10)
	move     0, R(11)
	move     V(24), R(12)
	move     S(5), R(13)
	pl_call  pred_anchor_7()
	move     &ref[552], R(0)
	move     S(5), R(1)
	move     &ref[553], R(2)
	move     S(5), R(3)
	move     N(1), R(4)
	move     0, R(5)
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  fun29()

;; TERM 555: '$TUPPLE'(35169792171520)
c_code local build_ref_555
	ret_reg &ref[555]
	call_c   Dyam_Create_Simple_Tupple(0,286688)
	move_ret ref[555]
	c_ret

long local pool_fun184[4]=[3,build_ref_556,build_ref_174,build_ref_105]

pl_code local fun184
	call_c   Dyam_Pool(pool_fun184)
	call_c   Dyam_Allocate(0)
	move     &ref[556], R(0)
	move     S(5), R(1)
	move     V(23), R(2)
	move     S(5), R(3)
	move     V(22), R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Load(6,V(15))
	move     V(20), R(8)
	move     S(5), R(9)
	move     N(1), R(10)
	move     0, R(11)
	move     &ref[174], R(12)
	move     0, R(13)
	move     &ref[105], R(14)
	move     0, R(15)
	move     V(19), R(16)
	move     S(5), R(17)
	move     V(21), R(18)
	move     S(5), R(19)
	call_c   Dyam_Reg_Deallocate(10)
fun178:
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(10)
	call_c   Dyam_Choice(fun177)
	call_c   Dyam_Unify(V(3),V(2))
	fail_ret
	call_c   Dyam_Unify(V(4),V(5))
	fail_ret
	call_c   Dyam_Unify(V(9),V(10))
	fail_ret
	pl_call  Follow_Cont(V(1))
	pl_fail


;; TERM 558: '$TUPPLE'(35169792171732)
c_code local build_ref_558
	ret_reg &ref[558]
	call_c   Dyam_Create_Simple_Tupple(0,293888)
	move_ret ref[558]
	c_ret

long local pool_fun185[6]=[5,build_ref_513,build_seed_87,build_ref_559,build_ref_174,build_ref_105]

pl_code local fun185
	call_c   Dyam_Pool(pool_fun185)
	call_c   Dyam_Unify(V(13),V(14))
	fail_ret
	call_c   Dyam_Unify(V(3),&ref[513])
	fail_ret
	call_c   Dyam_Allocate(0)
	pl_call  fun22(&seed[87],2)
	call_c   Dyam_Forget_Current_Trans()
	move     &ref[559], R(0)
	move     S(5), R(1)
	move     V(18), R(2)
	move     S(5), R(3)
	move     V(17), R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Load(6,V(14))
	move     V(15), R(8)
	move     S(5), R(9)
	move     N(3), R(10)
	move     0, R(11)
	move     &ref[174], R(12)
	move     0, R(13)
	move     &ref[105], R(14)
	move     0, R(15)
	call_c   Dyam_Reg_Load(16,V(10))
	move     V(16), R(18)
	move     S(5), R(19)
	pl_call  fun147()
	call_c   Dyam_Deallocate()
	pl_ret

;; TERM 561: '$TUPPLE'(35169792171904)
c_code local build_ref_561
	ret_reg &ref[561]
	call_c   Dyam_Create_Simple_Tupple(0,201818112)
	move_ret ref[561]
	c_ret

long local pool_fun186[3]=[2,build_ref_562,build_ref_105]

pl_code local fun186
	call_c   Dyam_Pool(pool_fun186)
	call_c   Dyam_Allocate(0)
	move     &ref[562], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(9))
	move     V(13), R(4)
	move     S(5), R(5)
	move     N(0), R(6)
	move     0, R(7)
	move     &ref[105], R(8)
	move     0, R(9)
	call_c   Dyam_Reg_Deallocate(5)
	pl_jump  fun131()

;; TERM 564: '$TUPPLE'(35169792171988)
c_code local build_ref_564
	ret_reg &ref[564]
	call_c   Dyam_Create_Simple_Tupple(0,202309632)
	move_ret ref[564]
	c_ret

long local pool_fun187[5]=[4,build_ref_499,build_ref_565,build_ref_174,build_ref_105]

pl_code local fun187
	call_c   Dyam_Pool(pool_fun187)
	call_c   Dyam_Unify_Item(&ref[499])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[565], R(0)
	move     S(5), R(1)
	move     V(12), R(2)
	move     S(5), R(3)
	move     V(11), R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Load(6,V(1))
	move     V(9), R(8)
	move     S(5), R(9)
	move     N(4), R(10)
	move     0, R(11)
	move     &ref[174], R(12)
	move     0, R(13)
	move     &ref[105], R(14)
	move     0, R(15)
	move     V(2), R(16)
	move     S(5), R(17)
	move     V(10), R(18)
	move     S(5), R(19)
	call_c   Dyam_Reg_Deallocate(10)
fun170:
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(10)
	call_c   Dyam_Choice(fun169)
	call_c   Dyam_Unify(V(3),V(2))
	fail_ret
	call_c   Dyam_Unify(V(4),V(5))
	fail_ret
	call_c   Dyam_Unify(V(9),V(10))
	fail_ret
	pl_call  Follow_Cont(V(1))
	pl_fail


;; TERM 498: '*SAFIRST*'('call_s/2'(_B)) :> []
c_code local build_ref_498
	ret_reg &ref[498]
	call_c   build_ref_497()
	call_c   Dyam_Create_Binary(I(9),&ref[497],I(0))
	move_ret ref[498]
	c_ret

;; TERM 497: '*SAFIRST*'('call_s/2'(_B))
c_code local build_ref_497
	ret_reg &ref[497]
	call_c   build_ref_170()
	call_c   build_ref_519()
	call_c   Dyam_Create_Unary(&ref[170],&ref[519])
	move_ret ref[497]
	c_ret

c_code local build_seed_90
	ret_reg &seed[90]
	call_c   build_ref_179()
	call_c   build_ref_531()
	call_c   Dyam_Seed_Start(&ref[179],&ref[531],&ref[531],fun9,1)
	call_c   build_ref_529()
	call_c   Dyam_Seed_Add_Comp(&ref[529],fun174,0)
	call_c   Dyam_Seed_End()
	move_ret seed[90]
	c_ret

;; TERM 529: '*CFI*'{aspop=> 'call_v/2'(_E), current=> 'call_v/2'(_F)}
c_code local build_ref_529
	ret_reg &ref[529]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_604()
	call_c   Dyam_Create_Unary(&ref[604],V(4))
	move_ret R(0)
	call_c   Dyam_Create_Unary(&ref[604],V(5))
	move_ret R(1)
	call_c   build_ref_603()
	call_c   Dyam_Create_Binary(&ref[603],R(0),R(1))
	move_ret ref[529]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 603: '*CFI*'!'$ft'
c_code local build_ref_603
	ret_reg &ref[603]
	call_c   build_ref_293()
	call_c   Dyam_Create_Atom_Module("$ft",&ref[293])
	move_ret ref[603]
	c_ret

;; TERM 293: '*CFI*'
c_code local build_ref_293
	ret_reg &ref[293]
	call_c   Dyam_Create_Atom("*CFI*")
	move_ret ref[293]
	c_ret

;; TERM 604: 'call_v/2'
c_code local build_ref_604
	ret_reg &ref[604]
	call_c   Dyam_Create_Atom("call_v/2")
	move_ret ref[604]
	c_ret

pl_code local fun174
	call_c   build_ref_529()
	call_c   Dyam_Unify_Item(&ref[529])
	fail_ret
	pl_jump  Follow_Cont(V(1))

;; TERM 531: '*CFI*'{aspop=> 'call_v/2'(_E), current=> 'call_v/2'(_F)} :> '$FUNTUPPLE'(toto_27, '$TUPPLE'(35169791645148))
c_code local build_ref_531
	ret_reg &ref[531]
	call_c   build_ref_529()
	call_c   build_ref_530()
	call_c   Dyam_Create_Binary(I(9),&ref[529],&ref[530])
	move_ret ref[531]
	c_ret

;; TERM 530: '$FUNTUPPLE'(toto_27, '$TUPPLE'(35169791645148))
c_code local build_ref_530
	ret_reg &ref[530]
	call_c   build_ref_605()
	call_c   Dyam_Create_Fun_With_Tupple(&ref[605],0,134217728)
	move_ret ref[530]
	c_ret

;; TERM 605: toto_27
c_code local build_ref_605
	ret_reg &ref[605]
	call_c   Dyam_Create_Atom("toto_27")
	move_ret ref[605]
	c_ret

;; TERM 179: '*CFI>*'
c_code local build_ref_179
	ret_reg &ref[179]
	call_c   Dyam_Create_Atom("*CFI>*")
	move_ret ref[179]
	c_ret

c_code local build_seed_89
	ret_reg &seed[89]
	call_c   build_ref_175()
	call_c   build_ref_526()
	call_c   Dyam_Seed_Start(&ref[175],&ref[526],&ref[526],fun0,1)
	call_c   build_ref_528()
	call_c   Dyam_Seed_Add_Comp(&ref[528],&ref[526],0)
	call_c   Dyam_Seed_End()
	move_ret seed[89]
	c_ret

;; TERM 528: '*SA-AUX-FIRST*'('call_v/2'(_E)) :> '$$HOLE$$'
c_code local build_ref_528
	ret_reg &ref[528]
	call_c   build_ref_527()
	call_c   Dyam_Create_Binary(I(9),&ref[527],I(7))
	move_ret ref[528]
	c_ret

;; TERM 527: '*SA-AUX-FIRST*'('call_v/2'(_E))
c_code local build_ref_527
	ret_reg &ref[527]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_604()
	call_c   Dyam_Create_Unary(&ref[604],V(4))
	move_ret R(0)
	call_c   build_ref_276()
	call_c   Dyam_Create_Unary(&ref[276],R(0))
	move_ret ref[527]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 526: '*CAI*'{current=> 'call_v/2'(_E)}
c_code local build_ref_526
	ret_reg &ref[526]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_604()
	call_c   Dyam_Create_Unary(&ref[604],V(4))
	move_ret R(0)
	call_c   build_ref_596()
	call_c   Dyam_Create_Unary(&ref[596],R(0))
	move_ret ref[526]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_84
	ret_reg &seed[84]
	call_c   build_ref_179()
	call_c   build_ref_505()
	call_c   Dyam_Seed_Start(&ref[179],&ref[505],&ref[505],fun9,1)
	call_c   build_ref_503()
	call_c   Dyam_Seed_Add_Comp(&ref[503],fun166,0)
	call_c   Dyam_Seed_End()
	move_ret seed[84]
	c_ret

;; TERM 503: '*CFI*'{aspop=> 'call_s/2'(_E), current=> 'call_s/2'(_F)}
c_code local build_ref_503
	ret_reg &ref[503]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_600()
	call_c   Dyam_Create_Unary(&ref[600],V(4))
	move_ret R(0)
	call_c   Dyam_Create_Unary(&ref[600],V(5))
	move_ret R(1)
	call_c   build_ref_603()
	call_c   Dyam_Create_Binary(&ref[603],R(0),R(1))
	move_ret ref[503]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

pl_code local fun166
	call_c   build_ref_503()
	call_c   Dyam_Unify_Item(&ref[503])
	fail_ret
	pl_jump  Follow_Cont(V(1))

;; TERM 505: '*CFI*'{aspop=> 'call_s/2'(_E), current=> 'call_s/2'(_F)} :> '$FUNTUPPLE'(toto_24, '$TUPPLE'(35169791645148))
c_code local build_ref_505
	ret_reg &ref[505]
	call_c   build_ref_503()
	call_c   build_ref_504()
	call_c   Dyam_Create_Binary(I(9),&ref[503],&ref[504])
	move_ret ref[505]
	c_ret

;; TERM 504: '$FUNTUPPLE'(toto_24, '$TUPPLE'(35169791645148))
c_code local build_ref_504
	ret_reg &ref[504]
	call_c   build_ref_606()
	call_c   Dyam_Create_Fun_With_Tupple(&ref[606],0,134217728)
	move_ret ref[504]
	c_ret

;; TERM 606: toto_24
c_code local build_ref_606
	ret_reg &ref[606]
	call_c   Dyam_Create_Atom("toto_24")
	move_ret ref[606]
	c_ret

c_code local build_seed_83
	ret_reg &seed[83]
	call_c   build_ref_175()
	call_c   build_ref_500()
	call_c   Dyam_Seed_Start(&ref[175],&ref[500],&ref[500],fun0,1)
	call_c   build_ref_502()
	call_c   Dyam_Seed_Add_Comp(&ref[502],&ref[500],0)
	call_c   Dyam_Seed_End()
	move_ret seed[83]
	c_ret

;; TERM 502: '*SA-AUX-FIRST*'('call_s/2'(_E)) :> '$$HOLE$$'
c_code local build_ref_502
	ret_reg &ref[502]
	call_c   build_ref_501()
	call_c   Dyam_Create_Binary(I(9),&ref[501],I(7))
	move_ret ref[502]
	c_ret

;; TERM 501: '*SA-AUX-FIRST*'('call_s/2'(_E))
c_code local build_ref_501
	ret_reg &ref[501]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_600()
	call_c   Dyam_Create_Unary(&ref[600],V(4))
	move_ret R(0)
	call_c   build_ref_276()
	call_c   Dyam_Create_Unary(&ref[276],R(0))
	move_ret ref[501]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 500: '*CAI*'{current=> 'call_s/2'(_E)}
c_code local build_ref_500
	ret_reg &ref[500]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_600()
	call_c   Dyam_Create_Unary(&ref[600],V(4))
	move_ret R(0)
	call_c   build_ref_596()
	call_c   Dyam_Create_Unary(&ref[596],R(0))
	move_ret ref[500]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_79
	ret_reg &seed[79]
	call_c   build_ref_179()
	call_c   build_ref_464()
	call_c   Dyam_Seed_Start(&ref[179],&ref[464],&ref[464],fun9,1)
	call_c   build_ref_462()
	call_c   Dyam_Seed_Add_Comp(&ref[462],fun150,0)
	call_c   Dyam_Seed_End()
	move_ret seed[79]
	c_ret

;; TERM 462: '*CFI*'{aspop=> 'call_pp/2'(_E), current=> 'call_pp/2'(_F)}
c_code local build_ref_462
	ret_reg &ref[462]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_607()
	call_c   Dyam_Create_Unary(&ref[607],V(4))
	move_ret R(0)
	call_c   Dyam_Create_Unary(&ref[607],V(5))
	move_ret R(1)
	call_c   build_ref_603()
	call_c   Dyam_Create_Binary(&ref[603],R(0),R(1))
	move_ret ref[462]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 607: 'call_pp/2'
c_code local build_ref_607
	ret_reg &ref[607]
	call_c   Dyam_Create_Atom("call_pp/2")
	move_ret ref[607]
	c_ret

pl_code local fun150
	call_c   build_ref_462()
	call_c   Dyam_Unify_Item(&ref[462])
	fail_ret
	pl_jump  Follow_Cont(V(1))

;; TERM 464: '*CFI*'{aspop=> 'call_pp/2'(_E), current=> 'call_pp/2'(_F)} :> '$FUNTUPPLE'(toto_22, '$TUPPLE'(35169791645148))
c_code local build_ref_464
	ret_reg &ref[464]
	call_c   build_ref_462()
	call_c   build_ref_463()
	call_c   Dyam_Create_Binary(I(9),&ref[462],&ref[463])
	move_ret ref[464]
	c_ret

;; TERM 463: '$FUNTUPPLE'(toto_22, '$TUPPLE'(35169791645148))
c_code local build_ref_463
	ret_reg &ref[463]
	call_c   build_ref_608()
	call_c   Dyam_Create_Fun_With_Tupple(&ref[608],0,134217728)
	move_ret ref[463]
	c_ret

;; TERM 608: toto_22
c_code local build_ref_608
	ret_reg &ref[608]
	call_c   Dyam_Create_Atom("toto_22")
	move_ret ref[608]
	c_ret

c_code local build_seed_78
	ret_reg &seed[78]
	call_c   build_ref_175()
	call_c   build_ref_459()
	call_c   Dyam_Seed_Start(&ref[175],&ref[459],&ref[459],fun0,1)
	call_c   build_ref_461()
	call_c   Dyam_Seed_Add_Comp(&ref[461],&ref[459],0)
	call_c   Dyam_Seed_End()
	move_ret seed[78]
	c_ret

;; TERM 461: '*SA-AUX-FIRST*'('call_pp/2'(_E)) :> '$$HOLE$$'
c_code local build_ref_461
	ret_reg &ref[461]
	call_c   build_ref_460()
	call_c   Dyam_Create_Binary(I(9),&ref[460],I(7))
	move_ret ref[461]
	c_ret

;; TERM 460: '*SA-AUX-FIRST*'('call_pp/2'(_E))
c_code local build_ref_460
	ret_reg &ref[460]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_607()
	call_c   Dyam_Create_Unary(&ref[607],V(4))
	move_ret R(0)
	call_c   build_ref_276()
	call_c   Dyam_Create_Unary(&ref[276],R(0))
	move_ret ref[460]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 459: '*CAI*'{current=> 'call_pp/2'(_E)}
c_code local build_ref_459
	ret_reg &ref[459]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_607()
	call_c   Dyam_Create_Unary(&ref[607],V(4))
	move_ret R(0)
	call_c   build_ref_596()
	call_c   Dyam_Create_Unary(&ref[596],R(0))
	move_ret ref[459]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_77
	ret_reg &seed[77]
	call_c   build_ref_296()
	call_c   build_ref_458()
	call_c   Dyam_Seed_Start(&ref[296],&ref[458],&ref[458],fun9,1)
	call_c   build_ref_456()
	call_c   Dyam_Seed_Add_Comp(&ref[456],fun148,0)
	call_c   Dyam_Seed_End()
	move_ret seed[77]
	c_ret

;; TERM 456: '*RFI*'{mspop=> 'call_vp/2'(_C), mspop_adj=> _E, current=> return(_D)}
c_code local build_ref_456
	ret_reg &ref[456]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_599()
	call_c   Dyam_Create_Unary(&ref[599],V(2))
	move_ret R(0)
	call_c   build_ref_609()
	call_c   build_ref_160()
	call_c   Dyam_Term_Start(&ref[609],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(&ref[160])
	call_c   Dyam_Term_End()
	move_ret ref[456]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 160: return(_D)
c_code local build_ref_160
	ret_reg &ref[160]
	call_c   build_ref_594()
	call_c   Dyam_Create_Unary(&ref[594],V(3))
	move_ret ref[160]
	c_ret

;; TERM 609: '*RFI*'!'$ft'
c_code local build_ref_609
	ret_reg &ref[609]
	call_c   build_ref_183()
	call_c   Dyam_Create_Atom_Module("$ft",&ref[183])
	move_ret ref[609]
	c_ret

;; TERM 183: '*RFI*'
c_code local build_ref_183
	ret_reg &ref[183]
	call_c   Dyam_Create_Atom("*RFI*")
	move_ret ref[183]
	c_ret

pl_code local fun148
	call_c   build_ref_456()
	call_c   Dyam_Unify_Item(&ref[456])
	fail_ret
	pl_jump  Follow_Cont(V(1))

;; TERM 458: '*RFI*'{mspop=> 'call_vp/2'(_C), mspop_adj=> _E, current=> return(_D)} :> '$FUNTUPPLE'(toto_21, '$TUPPLE'(35169791645148))
c_code local build_ref_458
	ret_reg &ref[458]
	call_c   build_ref_456()
	call_c   build_ref_457()
	call_c   Dyam_Create_Binary(I(9),&ref[456],&ref[457])
	move_ret ref[458]
	c_ret

;; TERM 457: '$FUNTUPPLE'(toto_21, '$TUPPLE'(35169791645148))
c_code local build_ref_457
	ret_reg &ref[457]
	call_c   build_ref_610()
	call_c   Dyam_Create_Fun_With_Tupple(&ref[610],0,134217728)
	move_ret ref[457]
	c_ret

;; TERM 610: toto_21
c_code local build_ref_610
	ret_reg &ref[610]
	call_c   Dyam_Create_Atom("toto_21")
	move_ret ref[610]
	c_ret

;; TERM 296: '*RFI>*'
c_code local build_ref_296
	ret_reg &ref[296]
	call_c   Dyam_Create_Atom("*RFI>*")
	move_ret ref[296]
	c_ret

c_code local build_seed_76
	ret_reg &seed[76]
	call_c   build_ref_293()
	call_c   build_ref_454()
	call_c   Dyam_Seed_Start(&ref[293],&ref[454],&ref[454],fun0,1)
	call_c   build_ref_455()
	call_c   Dyam_Seed_Add_Comp(&ref[455],&ref[454],0)
	call_c   Dyam_Seed_End()
	move_ret seed[76]
	c_ret

;; TERM 455: '*CFI*'{aspop=> _E, current=> 'call_vp/2'(_C)} :> '$$HOLE$$'
c_code local build_ref_455
	ret_reg &ref[455]
	call_c   build_ref_454()
	call_c   Dyam_Create_Binary(I(9),&ref[454],I(7))
	move_ret ref[455]
	c_ret

;; TERM 454: '*CFI*'{aspop=> _E, current=> 'call_vp/2'(_C)}
c_code local build_ref_454
	ret_reg &ref[454]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_599()
	call_c   Dyam_Create_Unary(&ref[599],V(2))
	move_ret R(0)
	call_c   build_ref_603()
	call_c   Dyam_Create_Binary(&ref[603],V(4),R(0))
	move_ret ref[454]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_73
	ret_reg &seed[73]
	call_c   build_ref_179()
	call_c   build_ref_446()
	call_c   Dyam_Seed_Start(&ref[179],&ref[446],&ref[446],fun9,1)
	call_c   build_ref_444()
	call_c   Dyam_Seed_Add_Comp(&ref[444],fun143,0)
	call_c   Dyam_Seed_End()
	move_ret seed[73]
	c_ret

;; TERM 444: '*CFI*'{aspop=> 'call_vp/2'(_E), current=> 'call_vp/2'(_F)}
c_code local build_ref_444
	ret_reg &ref[444]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_599()
	call_c   Dyam_Create_Unary(&ref[599],V(4))
	move_ret R(0)
	call_c   Dyam_Create_Unary(&ref[599],V(5))
	move_ret R(1)
	call_c   build_ref_603()
	call_c   Dyam_Create_Binary(&ref[603],R(0),R(1))
	move_ret ref[444]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

pl_code local fun143
	call_c   build_ref_444()
	call_c   Dyam_Unify_Item(&ref[444])
	fail_ret
	pl_jump  Follow_Cont(V(1))

;; TERM 446: '*CFI*'{aspop=> 'call_vp/2'(_E), current=> 'call_vp/2'(_F)} :> '$FUNTUPPLE'(toto_19, '$TUPPLE'(35169791645148))
c_code local build_ref_446
	ret_reg &ref[446]
	call_c   build_ref_444()
	call_c   build_ref_445()
	call_c   Dyam_Create_Binary(I(9),&ref[444],&ref[445])
	move_ret ref[446]
	c_ret

;; TERM 445: '$FUNTUPPLE'(toto_19, '$TUPPLE'(35169791645148))
c_code local build_ref_445
	ret_reg &ref[445]
	call_c   build_ref_611()
	call_c   Dyam_Create_Fun_With_Tupple(&ref[611],0,134217728)
	move_ret ref[445]
	c_ret

;; TERM 611: toto_19
c_code local build_ref_611
	ret_reg &ref[611]
	call_c   Dyam_Create_Atom("toto_19")
	move_ret ref[611]
	c_ret

c_code local build_seed_72
	ret_reg &seed[72]
	call_c   build_ref_175()
	call_c   build_ref_441()
	call_c   Dyam_Seed_Start(&ref[175],&ref[441],&ref[441],fun0,1)
	call_c   build_ref_443()
	call_c   Dyam_Seed_Add_Comp(&ref[443],&ref[441],0)
	call_c   Dyam_Seed_End()
	move_ret seed[72]
	c_ret

;; TERM 443: '*SA-AUX-FIRST*'('call_vp/2'(_E)) :> '$$HOLE$$'
c_code local build_ref_443
	ret_reg &ref[443]
	call_c   build_ref_442()
	call_c   Dyam_Create_Binary(I(9),&ref[442],I(7))
	move_ret ref[443]
	c_ret

;; TERM 442: '*SA-AUX-FIRST*'('call_vp/2'(_E))
c_code local build_ref_442
	ret_reg &ref[442]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_599()
	call_c   Dyam_Create_Unary(&ref[599],V(4))
	move_ret R(0)
	call_c   build_ref_276()
	call_c   Dyam_Create_Unary(&ref[276],R(0))
	move_ret ref[442]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 441: '*CAI*'{current=> 'call_vp/2'(_E)}
c_code local build_ref_441
	ret_reg &ref[441]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_599()
	call_c   Dyam_Create_Unary(&ref[599],V(4))
	move_ret R(0)
	call_c   build_ref_596()
	call_c   Dyam_Create_Unary(&ref[596],R(0))
	move_ret ref[441]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_70
	ret_reg &seed[70]
	call_c   build_ref_158()
	call_c   build_ref_411()
	call_c   Dyam_Seed_Start(&ref[158],&ref[411],&ref[411],fun9,1)
	call_c   build_ref_409()
	call_c   Dyam_Seed_Add_Comp(&ref[409],fun130,0)
	call_c   Dyam_Seed_End()
	move_ret seed[70]
	c_ret

;; TERM 409: '*RITEM*'('call_np/2'(_C), return(_D))
c_code local build_ref_409
	ret_reg &ref[409]
	call_c   build_ref_0()
	call_c   build_ref_408()
	call_c   build_ref_160()
	call_c   Dyam_Create_Binary(&ref[0],&ref[408],&ref[160])
	move_ret ref[409]
	c_ret

;; TERM 408: 'call_np/2'(_C)
c_code local build_ref_408
	ret_reg &ref[408]
	call_c   build_ref_595()
	call_c   Dyam_Create_Unary(&ref[595],V(2))
	move_ret ref[408]
	c_ret

pl_code local fun130
	call_c   build_ref_409()
	call_c   Dyam_Unify_Item(&ref[409])
	fail_ret
	pl_jump  Follow_Cont(V(1))

;; TERM 411: '*RITEM*'('call_np/2'(_C), return(_D)) :> '$FUNTUPPLE'(toto_18, '$TUPPLE'(35169791645148))
c_code local build_ref_411
	ret_reg &ref[411]
	call_c   build_ref_409()
	call_c   build_ref_410()
	call_c   Dyam_Create_Binary(I(9),&ref[409],&ref[410])
	move_ret ref[411]
	c_ret

;; TERM 410: '$FUNTUPPLE'(toto_18, '$TUPPLE'(35169791645148))
c_code local build_ref_410
	ret_reg &ref[410]
	call_c   build_ref_612()
	call_c   Dyam_Create_Fun_With_Tupple(&ref[612],0,134217728)
	move_ret ref[410]
	c_ret

;; TERM 612: toto_18
c_code local build_ref_612
	ret_reg &ref[612]
	call_c   Dyam_Create_Atom("toto_18")
	move_ret ref[612]
	c_ret

c_code local build_seed_69
	ret_reg &seed[69]
	call_c   build_ref_148()
	call_c   build_ref_405()
	call_c   Dyam_Seed_Start(&ref[148],&ref[405],&ref[405],fun0,1)
	call_c   build_ref_407()
	call_c   Dyam_Seed_Add_Comp(&ref[407],&ref[405],0)
	call_c   Dyam_Seed_End()
	move_ret seed[69]
	c_ret

;; TERM 407: '*SAFIRST*'('call_np/2'(_C)) :> '$$HOLE$$'
c_code local build_ref_407
	ret_reg &ref[407]
	call_c   build_ref_406()
	call_c   Dyam_Create_Binary(I(9),&ref[406],I(7))
	move_ret ref[407]
	c_ret

;; TERM 406: '*SAFIRST*'('call_np/2'(_C))
c_code local build_ref_406
	ret_reg &ref[406]
	call_c   build_ref_170()
	call_c   build_ref_408()
	call_c   Dyam_Create_Unary(&ref[170],&ref[408])
	move_ret ref[406]
	c_ret

;; TERM 405: '*SACITEM*'('call_np/2'(_C))
c_code local build_ref_405
	ret_reg &ref[405]
	call_c   build_ref_148()
	call_c   build_ref_408()
	call_c   Dyam_Create_Unary(&ref[148],&ref[408])
	move_ret ref[405]
	c_ret

c_code local build_seed_66
	ret_reg &seed[66]
	call_c   build_ref_179()
	call_c   build_ref_396()
	call_c   Dyam_Seed_Start(&ref[179],&ref[396],&ref[396],fun9,1)
	call_c   build_ref_394()
	call_c   Dyam_Seed_Add_Comp(&ref[394],fun125,0)
	call_c   Dyam_Seed_End()
	move_ret seed[66]
	c_ret

;; TERM 394: '*CFI*'{aspop=> 'call_p/2'(_E), current=> 'call_p/2'(_F)}
c_code local build_ref_394
	ret_reg &ref[394]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_613()
	call_c   Dyam_Create_Unary(&ref[613],V(4))
	move_ret R(0)
	call_c   Dyam_Create_Unary(&ref[613],V(5))
	move_ret R(1)
	call_c   build_ref_603()
	call_c   Dyam_Create_Binary(&ref[603],R(0),R(1))
	move_ret ref[394]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 613: 'call_p/2'
c_code local build_ref_613
	ret_reg &ref[613]
	call_c   Dyam_Create_Atom("call_p/2")
	move_ret ref[613]
	c_ret

pl_code local fun125
	call_c   build_ref_394()
	call_c   Dyam_Unify_Item(&ref[394])
	fail_ret
	pl_jump  Follow_Cont(V(1))

;; TERM 396: '*CFI*'{aspop=> 'call_p/2'(_E), current=> 'call_p/2'(_F)} :> '$FUNTUPPLE'(toto_16, '$TUPPLE'(35169791645148))
c_code local build_ref_396
	ret_reg &ref[396]
	call_c   build_ref_394()
	call_c   build_ref_395()
	call_c   Dyam_Create_Binary(I(9),&ref[394],&ref[395])
	move_ret ref[396]
	c_ret

;; TERM 395: '$FUNTUPPLE'(toto_16, '$TUPPLE'(35169791645148))
c_code local build_ref_395
	ret_reg &ref[395]
	call_c   build_ref_614()
	call_c   Dyam_Create_Fun_With_Tupple(&ref[614],0,134217728)
	move_ret ref[395]
	c_ret

;; TERM 614: toto_16
c_code local build_ref_614
	ret_reg &ref[614]
	call_c   Dyam_Create_Atom("toto_16")
	move_ret ref[614]
	c_ret

c_code local build_seed_65
	ret_reg &seed[65]
	call_c   build_ref_175()
	call_c   build_ref_391()
	call_c   Dyam_Seed_Start(&ref[175],&ref[391],&ref[391],fun0,1)
	call_c   build_ref_393()
	call_c   Dyam_Seed_Add_Comp(&ref[393],&ref[391],0)
	call_c   Dyam_Seed_End()
	move_ret seed[65]
	c_ret

;; TERM 393: '*SA-AUX-FIRST*'('call_p/2'(_E)) :> '$$HOLE$$'
c_code local build_ref_393
	ret_reg &ref[393]
	call_c   build_ref_392()
	call_c   Dyam_Create_Binary(I(9),&ref[392],I(7))
	move_ret ref[393]
	c_ret

;; TERM 392: '*SA-AUX-FIRST*'('call_p/2'(_E))
c_code local build_ref_392
	ret_reg &ref[392]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_613()
	call_c   Dyam_Create_Unary(&ref[613],V(4))
	move_ret R(0)
	call_c   build_ref_276()
	call_c   Dyam_Create_Unary(&ref[276],R(0))
	move_ret ref[392]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 391: '*CAI*'{current=> 'call_p/2'(_E)}
c_code local build_ref_391
	ret_reg &ref[391]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_613()
	call_c   Dyam_Create_Unary(&ref[613],V(4))
	move_ret R(0)
	call_c   build_ref_596()
	call_c   Dyam_Create_Unary(&ref[596],R(0))
	move_ret ref[391]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_64
	ret_reg &seed[64]
	call_c   build_ref_296()
	call_c   build_ref_390()
	call_c   Dyam_Seed_Start(&ref[296],&ref[390],&ref[390],fun9,1)
	call_c   build_ref_388()
	call_c   Dyam_Seed_Add_Comp(&ref[388],fun123,0)
	call_c   Dyam_Seed_End()
	move_ret seed[64]
	c_ret

;; TERM 388: '*RFI*'{mspop=> 'call_np/2'(_C), mspop_adj=> _E, current=> return(_D)}
c_code local build_ref_388
	ret_reg &ref[388]
	call_c   build_ref_609()
	call_c   build_ref_408()
	call_c   build_ref_160()
	call_c   Dyam_Term_Start(&ref[609],3)
	call_c   Dyam_Term_Arg(&ref[408])
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(&ref[160])
	call_c   Dyam_Term_End()
	move_ret ref[388]
	c_ret

pl_code local fun123
	call_c   build_ref_388()
	call_c   Dyam_Unify_Item(&ref[388])
	fail_ret
	pl_jump  Follow_Cont(V(1))

;; TERM 390: '*RFI*'{mspop=> 'call_np/2'(_C), mspop_adj=> _E, current=> return(_D)} :> '$FUNTUPPLE'(toto_15, '$TUPPLE'(35169791645148))
c_code local build_ref_390
	ret_reg &ref[390]
	call_c   build_ref_388()
	call_c   build_ref_389()
	call_c   Dyam_Create_Binary(I(9),&ref[388],&ref[389])
	move_ret ref[390]
	c_ret

;; TERM 389: '$FUNTUPPLE'(toto_15, '$TUPPLE'(35169791645148))
c_code local build_ref_389
	ret_reg &ref[389]
	call_c   build_ref_615()
	call_c   Dyam_Create_Fun_With_Tupple(&ref[615],0,134217728)
	move_ret ref[389]
	c_ret

;; TERM 615: toto_15
c_code local build_ref_615
	ret_reg &ref[615]
	call_c   Dyam_Create_Atom("toto_15")
	move_ret ref[615]
	c_ret

c_code local build_seed_63
	ret_reg &seed[63]
	call_c   build_ref_293()
	call_c   build_ref_386()
	call_c   Dyam_Seed_Start(&ref[293],&ref[386],&ref[386],fun0,1)
	call_c   build_ref_387()
	call_c   Dyam_Seed_Add_Comp(&ref[387],&ref[386],0)
	call_c   Dyam_Seed_End()
	move_ret seed[63]
	c_ret

;; TERM 387: '*CFI*'{aspop=> _E, current=> 'call_np/2'(_C)} :> '$$HOLE$$'
c_code local build_ref_387
	ret_reg &ref[387]
	call_c   build_ref_386()
	call_c   Dyam_Create_Binary(I(9),&ref[386],I(7))
	move_ret ref[387]
	c_ret

;; TERM 386: '*CFI*'{aspop=> _E, current=> 'call_np/2'(_C)}
c_code local build_ref_386
	ret_reg &ref[386]
	call_c   build_ref_603()
	call_c   build_ref_408()
	call_c   Dyam_Create_Binary(&ref[603],V(4),&ref[408])
	move_ret ref[386]
	c_ret

c_code local build_seed_62
	ret_reg &seed[62]
	call_c   build_ref_158()
	call_c   build_ref_366()
	call_c   Dyam_Seed_Start(&ref[158],&ref[366],&ref[366],fun9,1)
	call_c   build_ref_364()
	call_c   Dyam_Seed_Add_Comp(&ref[364],fun84,0)
	call_c   Dyam_Seed_End()
	move_ret seed[62]
	c_ret

;; TERM 364: '*RITEM*'('call_d/2'(_C), return(_D))
c_code local build_ref_364
	ret_reg &ref[364]
	call_c   build_ref_0()
	call_c   build_ref_363()
	call_c   build_ref_160()
	call_c   Dyam_Create_Binary(&ref[0],&ref[363],&ref[160])
	move_ret ref[364]
	c_ret

;; TERM 363: 'call_d/2'(_C)
c_code local build_ref_363
	ret_reg &ref[363]
	call_c   build_ref_591()
	call_c   Dyam_Create_Unary(&ref[591],V(2))
	move_ret ref[363]
	c_ret

pl_code local fun84
	call_c   build_ref_364()
	call_c   Dyam_Unify_Item(&ref[364])
	fail_ret
	pl_jump  Follow_Cont(V(1))

;; TERM 366: '*RITEM*'('call_d/2'(_C), return(_D)) :> '$FUNTUPPLE'(toto_14, '$TUPPLE'(35169791645148))
c_code local build_ref_366
	ret_reg &ref[366]
	call_c   build_ref_364()
	call_c   build_ref_365()
	call_c   Dyam_Create_Binary(I(9),&ref[364],&ref[365])
	move_ret ref[366]
	c_ret

;; TERM 365: '$FUNTUPPLE'(toto_14, '$TUPPLE'(35169791645148))
c_code local build_ref_365
	ret_reg &ref[365]
	call_c   build_ref_616()
	call_c   Dyam_Create_Fun_With_Tupple(&ref[616],0,134217728)
	move_ret ref[365]
	c_ret

;; TERM 616: toto_14
c_code local build_ref_616
	ret_reg &ref[616]
	call_c   Dyam_Create_Atom("toto_14")
	move_ret ref[616]
	c_ret

c_code local build_seed_61
	ret_reg &seed[61]
	call_c   build_ref_148()
	call_c   build_ref_360()
	call_c   Dyam_Seed_Start(&ref[148],&ref[360],&ref[360],fun0,1)
	call_c   build_ref_362()
	call_c   Dyam_Seed_Add_Comp(&ref[362],&ref[360],0)
	call_c   Dyam_Seed_End()
	move_ret seed[61]
	c_ret

;; TERM 362: '*SAFIRST*'('call_d/2'(_C)) :> '$$HOLE$$'
c_code local build_ref_362
	ret_reg &ref[362]
	call_c   build_ref_361()
	call_c   Dyam_Create_Binary(I(9),&ref[361],I(7))
	move_ret ref[362]
	c_ret

;; TERM 361: '*SAFIRST*'('call_d/2'(_C))
c_code local build_ref_361
	ret_reg &ref[361]
	call_c   build_ref_170()
	call_c   build_ref_363()
	call_c   Dyam_Create_Unary(&ref[170],&ref[363])
	move_ret ref[361]
	c_ret

;; TERM 360: '*SACITEM*'('call_d/2'(_C))
c_code local build_ref_360
	ret_reg &ref[360]
	call_c   build_ref_148()
	call_c   build_ref_363()
	call_c   Dyam_Create_Unary(&ref[148],&ref[363])
	move_ret ref[360]
	c_ret

c_code local build_seed_56
	ret_reg &seed[56]
	call_c   build_ref_179()
	call_c   build_ref_305()
	call_c   Dyam_Seed_Start(&ref[179],&ref[305],&ref[305],fun9,1)
	call_c   build_ref_303()
	call_c   Dyam_Seed_Add_Comp(&ref[303],fun63,0)
	call_c   Dyam_Seed_End()
	move_ret seed[56]
	c_ret

;; TERM 303: '*CFI*'{aspop=> 'call_a/2'(_E), current=> 'call_a/2'(_F)}
c_code local build_ref_303
	ret_reg &ref[303]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_617()
	call_c   Dyam_Create_Unary(&ref[617],V(4))
	move_ret R(0)
	call_c   Dyam_Create_Unary(&ref[617],V(5))
	move_ret R(1)
	call_c   build_ref_603()
	call_c   Dyam_Create_Binary(&ref[603],R(0),R(1))
	move_ret ref[303]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 617: 'call_a/2'
c_code local build_ref_617
	ret_reg &ref[617]
	call_c   Dyam_Create_Atom("call_a/2")
	move_ret ref[617]
	c_ret

pl_code local fun63
	call_c   build_ref_303()
	call_c   Dyam_Unify_Item(&ref[303])
	fail_ret
	pl_jump  Follow_Cont(V(1))

;; TERM 305: '*CFI*'{aspop=> 'call_a/2'(_E), current=> 'call_a/2'(_F)} :> '$FUNTUPPLE'(toto_12, '$TUPPLE'(35169791645148))
c_code local build_ref_305
	ret_reg &ref[305]
	call_c   build_ref_303()
	call_c   build_ref_304()
	call_c   Dyam_Create_Binary(I(9),&ref[303],&ref[304])
	move_ret ref[305]
	c_ret

;; TERM 304: '$FUNTUPPLE'(toto_12, '$TUPPLE'(35169791645148))
c_code local build_ref_304
	ret_reg &ref[304]
	call_c   build_ref_618()
	call_c   Dyam_Create_Fun_With_Tupple(&ref[618],0,134217728)
	move_ret ref[304]
	c_ret

;; TERM 618: toto_12
c_code local build_ref_618
	ret_reg &ref[618]
	call_c   Dyam_Create_Atom("toto_12")
	move_ret ref[618]
	c_ret

c_code local build_seed_55
	ret_reg &seed[55]
	call_c   build_ref_175()
	call_c   build_ref_300()
	call_c   Dyam_Seed_Start(&ref[175],&ref[300],&ref[300],fun0,1)
	call_c   build_ref_302()
	call_c   Dyam_Seed_Add_Comp(&ref[302],&ref[300],0)
	call_c   Dyam_Seed_End()
	move_ret seed[55]
	c_ret

;; TERM 302: '*SA-AUX-FIRST*'('call_a/2'(_E)) :> '$$HOLE$$'
c_code local build_ref_302
	ret_reg &ref[302]
	call_c   build_ref_301()
	call_c   Dyam_Create_Binary(I(9),&ref[301],I(7))
	move_ret ref[302]
	c_ret

;; TERM 301: '*SA-AUX-FIRST*'('call_a/2'(_E))
c_code local build_ref_301
	ret_reg &ref[301]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_617()
	call_c   Dyam_Create_Unary(&ref[617],V(4))
	move_ret R(0)
	call_c   build_ref_276()
	call_c   Dyam_Create_Unary(&ref[276],R(0))
	move_ret ref[301]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 300: '*CAI*'{current=> 'call_a/2'(_E)}
c_code local build_ref_300
	ret_reg &ref[300]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_617()
	call_c   Dyam_Create_Unary(&ref[617],V(4))
	move_ret R(0)
	call_c   build_ref_596()
	call_c   Dyam_Create_Unary(&ref[596],R(0))
	move_ret ref[300]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_54
	ret_reg &seed[54]
	call_c   build_ref_296()
	call_c   build_ref_299()
	call_c   Dyam_Seed_Start(&ref[296],&ref[299],&ref[299],fun9,1)
	call_c   build_ref_297()
	call_c   Dyam_Seed_Add_Comp(&ref[297],fun61,0)
	call_c   Dyam_Seed_End()
	move_ret seed[54]
	c_ret

;; TERM 297: '*RFI*'{mspop=> 'call_n/2'(_C), mspop_adj=> _E, current=> return(_D)}
c_code local build_ref_297
	ret_reg &ref[297]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_597()
	call_c   Dyam_Create_Unary(&ref[597],V(2))
	move_ret R(0)
	call_c   build_ref_609()
	call_c   build_ref_160()
	call_c   Dyam_Term_Start(&ref[609],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(&ref[160])
	call_c   Dyam_Term_End()
	move_ret ref[297]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

pl_code local fun61
	call_c   build_ref_297()
	call_c   Dyam_Unify_Item(&ref[297])
	fail_ret
	pl_jump  Follow_Cont(V(1))

;; TERM 299: '*RFI*'{mspop=> 'call_n/2'(_C), mspop_adj=> _E, current=> return(_D)} :> '$FUNTUPPLE'(toto_11, '$TUPPLE'(35169791645148))
c_code local build_ref_299
	ret_reg &ref[299]
	call_c   build_ref_297()
	call_c   build_ref_298()
	call_c   Dyam_Create_Binary(I(9),&ref[297],&ref[298])
	move_ret ref[299]
	c_ret

;; TERM 298: '$FUNTUPPLE'(toto_11, '$TUPPLE'(35169791645148))
c_code local build_ref_298
	ret_reg &ref[298]
	call_c   build_ref_619()
	call_c   Dyam_Create_Fun_With_Tupple(&ref[619],0,134217728)
	move_ret ref[298]
	c_ret

;; TERM 619: toto_11
c_code local build_ref_619
	ret_reg &ref[619]
	call_c   Dyam_Create_Atom("toto_11")
	move_ret ref[619]
	c_ret

c_code local build_seed_53
	ret_reg &seed[53]
	call_c   build_ref_293()
	call_c   build_ref_294()
	call_c   Dyam_Seed_Start(&ref[293],&ref[294],&ref[294],fun0,1)
	call_c   build_ref_295()
	call_c   Dyam_Seed_Add_Comp(&ref[295],&ref[294],0)
	call_c   Dyam_Seed_End()
	move_ret seed[53]
	c_ret

;; TERM 295: '*CFI*'{aspop=> _E, current=> 'call_n/2'(_C)} :> '$$HOLE$$'
c_code local build_ref_295
	ret_reg &ref[295]
	call_c   build_ref_294()
	call_c   Dyam_Create_Binary(I(9),&ref[294],I(7))
	move_ret ref[295]
	c_ret

;; TERM 294: '*CFI*'{aspop=> _E, current=> 'call_n/2'(_C)}
c_code local build_ref_294
	ret_reg &ref[294]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_597()
	call_c   Dyam_Create_Unary(&ref[597],V(2))
	move_ret R(0)
	call_c   build_ref_603()
	call_c   Dyam_Create_Binary(&ref[603],V(4),R(0))
	move_ret ref[294]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_50
	ret_reg &seed[50]
	call_c   build_ref_179()
	call_c   build_ref_285()
	call_c   Dyam_Seed_Start(&ref[179],&ref[285],&ref[285],fun9,1)
	call_c   build_ref_283()
	call_c   Dyam_Seed_Add_Comp(&ref[283],fun56,0)
	call_c   Dyam_Seed_End()
	move_ret seed[50]
	c_ret

;; TERM 283: '*CFI*'{aspop=> 'call_n/2'(_E), current=> 'call_n/2'(_F)}
c_code local build_ref_283
	ret_reg &ref[283]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_597()
	call_c   Dyam_Create_Unary(&ref[597],V(4))
	move_ret R(0)
	call_c   Dyam_Create_Unary(&ref[597],V(5))
	move_ret R(1)
	call_c   build_ref_603()
	call_c   Dyam_Create_Binary(&ref[603],R(0),R(1))
	move_ret ref[283]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

pl_code local fun56
	call_c   build_ref_283()
	call_c   Dyam_Unify_Item(&ref[283])
	fail_ret
	pl_jump  Follow_Cont(V(1))

;; TERM 285: '*CFI*'{aspop=> 'call_n/2'(_E), current=> 'call_n/2'(_F)} :> '$FUNTUPPLE'(toto_9, '$TUPPLE'(35169791645148))
c_code local build_ref_285
	ret_reg &ref[285]
	call_c   build_ref_283()
	call_c   build_ref_284()
	call_c   Dyam_Create_Binary(I(9),&ref[283],&ref[284])
	move_ret ref[285]
	c_ret

;; TERM 284: '$FUNTUPPLE'(toto_9, '$TUPPLE'(35169791645148))
c_code local build_ref_284
	ret_reg &ref[284]
	call_c   build_ref_620()
	call_c   Dyam_Create_Fun_With_Tupple(&ref[620],0,134217728)
	move_ret ref[284]
	c_ret

;; TERM 620: toto_9
c_code local build_ref_620
	ret_reg &ref[620]
	call_c   Dyam_Create_Atom("toto_9")
	move_ret ref[620]
	c_ret

c_code local build_seed_49
	ret_reg &seed[49]
	call_c   build_ref_175()
	call_c   build_ref_280()
	call_c   Dyam_Seed_Start(&ref[175],&ref[280],&ref[280],fun0,1)
	call_c   build_ref_282()
	call_c   Dyam_Seed_Add_Comp(&ref[282],&ref[280],0)
	call_c   Dyam_Seed_End()
	move_ret seed[49]
	c_ret

;; TERM 282: '*SA-AUX-FIRST*'('call_n/2'(_E)) :> '$$HOLE$$'
c_code local build_ref_282
	ret_reg &ref[282]
	call_c   build_ref_281()
	call_c   Dyam_Create_Binary(I(9),&ref[281],I(7))
	move_ret ref[282]
	c_ret

;; TERM 281: '*SA-AUX-FIRST*'('call_n/2'(_E))
c_code local build_ref_281
	ret_reg &ref[281]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_597()
	call_c   Dyam_Create_Unary(&ref[597],V(4))
	move_ret R(0)
	call_c   build_ref_276()
	call_c   Dyam_Create_Unary(&ref[276],R(0))
	move_ret ref[281]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 280: '*CAI*'{current=> 'call_n/2'(_E)}
c_code local build_ref_280
	ret_reg &ref[280]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_597()
	call_c   Dyam_Create_Unary(&ref[597],V(4))
	move_ret R(0)
	call_c   build_ref_596()
	call_c   Dyam_Create_Unary(&ref[596],R(0))
	move_ret ref[280]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_45
	ret_reg &seed[45]
	call_c   build_ref_179()
	call_c   build_ref_251()
	call_c   Dyam_Seed_Start(&ref[179],&ref[251],&ref[251],fun9,1)
	call_c   build_ref_249()
	call_c   Dyam_Seed_Add_Comp(&ref[249],fun44,0)
	call_c   Dyam_Seed_End()
	move_ret seed[45]
	c_ret

;; TERM 249: '*CFI*'{aspop=> 'call_pn/2'(_E), current=> 'call_pn/2'(_F)}
c_code local build_ref_249
	ret_reg &ref[249]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_621()
	call_c   Dyam_Create_Unary(&ref[621],V(4))
	move_ret R(0)
	call_c   Dyam_Create_Unary(&ref[621],V(5))
	move_ret R(1)
	call_c   build_ref_603()
	call_c   Dyam_Create_Binary(&ref[603],R(0),R(1))
	move_ret ref[249]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 621: 'call_pn/2'
c_code local build_ref_621
	ret_reg &ref[621]
	call_c   Dyam_Create_Atom("call_pn/2")
	move_ret ref[621]
	c_ret

pl_code local fun44
	call_c   build_ref_249()
	call_c   Dyam_Unify_Item(&ref[249])
	fail_ret
	pl_jump  Follow_Cont(V(1))

;; TERM 251: '*CFI*'{aspop=> 'call_pn/2'(_E), current=> 'call_pn/2'(_F)} :> '$FUNTUPPLE'(toto_7, '$TUPPLE'(35169791645148))
c_code local build_ref_251
	ret_reg &ref[251]
	call_c   build_ref_249()
	call_c   build_ref_250()
	call_c   Dyam_Create_Binary(I(9),&ref[249],&ref[250])
	move_ret ref[251]
	c_ret

;; TERM 250: '$FUNTUPPLE'(toto_7, '$TUPPLE'(35169791645148))
c_code local build_ref_250
	ret_reg &ref[250]
	call_c   build_ref_622()
	call_c   Dyam_Create_Fun_With_Tupple(&ref[622],0,134217728)
	move_ret ref[250]
	c_ret

;; TERM 622: toto_7
c_code local build_ref_622
	ret_reg &ref[622]
	call_c   Dyam_Create_Atom("toto_7")
	move_ret ref[622]
	c_ret

c_code local build_seed_44
	ret_reg &seed[44]
	call_c   build_ref_175()
	call_c   build_ref_246()
	call_c   Dyam_Seed_Start(&ref[175],&ref[246],&ref[246],fun0,1)
	call_c   build_ref_248()
	call_c   Dyam_Seed_Add_Comp(&ref[248],&ref[246],0)
	call_c   Dyam_Seed_End()
	move_ret seed[44]
	c_ret

;; TERM 248: '*SA-AUX-FIRST*'('call_pn/2'(_E)) :> '$$HOLE$$'
c_code local build_ref_248
	ret_reg &ref[248]
	call_c   build_ref_247()
	call_c   Dyam_Create_Binary(I(9),&ref[247],I(7))
	move_ret ref[248]
	c_ret

;; TERM 247: '*SA-AUX-FIRST*'('call_pn/2'(_E))
c_code local build_ref_247
	ret_reg &ref[247]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_621()
	call_c   Dyam_Create_Unary(&ref[621],V(4))
	move_ret R(0)
	call_c   build_ref_276()
	call_c   Dyam_Create_Unary(&ref[276],R(0))
	move_ret ref[247]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 246: '*CAI*'{current=> 'call_pn/2'(_E)}
c_code local build_ref_246
	ret_reg &ref[246]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_621()
	call_c   Dyam_Create_Unary(&ref[621],V(4))
	move_ret R(0)
	call_c   build_ref_596()
	call_c   Dyam_Create_Unary(&ref[596],R(0))
	move_ret ref[246]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_41
	ret_reg &seed[41]
	call_c   build_ref_179()
	call_c   build_ref_238()
	call_c   Dyam_Seed_Start(&ref[179],&ref[238],&ref[238],fun9,1)
	call_c   build_ref_236()
	call_c   Dyam_Seed_Add_Comp(&ref[236],fun39,0)
	call_c   Dyam_Seed_End()
	move_ret seed[41]
	c_ret

;; TERM 236: '*CFI*'{aspop=> 'call_np/2'(_E), current=> 'call_np/2'(_F)}
c_code local build_ref_236
	ret_reg &ref[236]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_595()
	call_c   Dyam_Create_Unary(&ref[595],V(4))
	move_ret R(0)
	call_c   Dyam_Create_Unary(&ref[595],V(5))
	move_ret R(1)
	call_c   build_ref_603()
	call_c   Dyam_Create_Binary(&ref[603],R(0),R(1))
	move_ret ref[236]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

pl_code local fun39
	call_c   build_ref_236()
	call_c   Dyam_Unify_Item(&ref[236])
	fail_ret
	pl_jump  Follow_Cont(V(1))

;; TERM 238: '*CFI*'{aspop=> 'call_np/2'(_E), current=> 'call_np/2'(_F)} :> '$FUNTUPPLE'(toto_5, '$TUPPLE'(35169791645148))
c_code local build_ref_238
	ret_reg &ref[238]
	call_c   build_ref_236()
	call_c   build_ref_237()
	call_c   Dyam_Create_Binary(I(9),&ref[236],&ref[237])
	move_ret ref[238]
	c_ret

;; TERM 237: '$FUNTUPPLE'(toto_5, '$TUPPLE'(35169791645148))
c_code local build_ref_237
	ret_reg &ref[237]
	call_c   build_ref_623()
	call_c   Dyam_Create_Fun_With_Tupple(&ref[623],0,134217728)
	move_ret ref[237]
	c_ret

;; TERM 623: toto_5
c_code local build_ref_623
	ret_reg &ref[623]
	call_c   Dyam_Create_Atom("toto_5")
	move_ret ref[623]
	c_ret

c_code local build_seed_40
	ret_reg &seed[40]
	call_c   build_ref_175()
	call_c   build_ref_233()
	call_c   Dyam_Seed_Start(&ref[175],&ref[233],&ref[233],fun0,1)
	call_c   build_ref_235()
	call_c   Dyam_Seed_Add_Comp(&ref[235],&ref[233],0)
	call_c   Dyam_Seed_End()
	move_ret seed[40]
	c_ret

;; TERM 235: '*SA-AUX-FIRST*'('call_np/2'(_E)) :> '$$HOLE$$'
c_code local build_ref_235
	ret_reg &ref[235]
	call_c   build_ref_234()
	call_c   Dyam_Create_Binary(I(9),&ref[234],I(7))
	move_ret ref[235]
	c_ret

;; TERM 234: '*SA-AUX-FIRST*'('call_np/2'(_E))
c_code local build_ref_234
	ret_reg &ref[234]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_595()
	call_c   Dyam_Create_Unary(&ref[595],V(4))
	move_ret R(0)
	call_c   build_ref_276()
	call_c   Dyam_Create_Unary(&ref[276],R(0))
	move_ret ref[234]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 233: '*CAI*'{current=> 'call_np/2'(_E)}
c_code local build_ref_233
	ret_reg &ref[233]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_595()
	call_c   Dyam_Create_Unary(&ref[595],V(4))
	move_ret R(0)
	call_c   build_ref_596()
	call_c   Dyam_Create_Unary(&ref[596],R(0))
	move_ret ref[233]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_38
	ret_reg &seed[38]
	call_c   build_ref_158()
	call_c   build_ref_207()
	call_c   Dyam_Seed_Start(&ref[158],&ref[207],I(0),fun9,1)
	call_c   build_ref_205()
	call_c   Dyam_Seed_Add_Comp(&ref[205],fun30,0)
	call_c   Dyam_Seed_End()
	move_ret seed[38]
	c_ret

;; TERM 205: '*RITEM*'(verbose!struct(_C, _D), voidret)
c_code local build_ref_205
	ret_reg &ref[205]
	call_c   build_ref_0()
	call_c   build_ref_201()
	call_c   build_ref_2()
	call_c   Dyam_Create_Binary(&ref[0],&ref[201],&ref[2])
	move_ret ref[205]
	c_ret

;; TERM 201: verbose!struct(_C, _D)
c_code local build_ref_201
	ret_reg &ref[201]
	call_c   build_ref_583()
	call_c   Dyam_Create_Binary(&ref[583],V(2),V(3))
	move_ret ref[201]
	c_ret

pl_code local fun30
	call_c   build_ref_205()
	call_c   Dyam_Unify_Item(&ref[205])
	fail_ret
	pl_jump  Follow_Cont(V(1))

;; TERM 207: '*RITEM*'(verbose!struct(_C, _D), voidret) :> toto(4, '$TUPPLE'(35169791645148))
c_code local build_ref_207
	ret_reg &ref[207]
	call_c   build_ref_205()
	call_c   build_ref_206()
	call_c   Dyam_Create_Binary(I(9),&ref[205],&ref[206])
	move_ret ref[207]
	c_ret

;; TERM 206: toto(4, '$TUPPLE'(35169791645148))
c_code local build_ref_206
	ret_reg &ref[206]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,134217728)
	move_ret R(0)
	call_c   build_ref_624()
	call_c   Dyam_Create_Binary(&ref[624],N(4),R(0))
	move_ret ref[206]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 624: toto
c_code local build_ref_624
	ret_reg &ref[624]
	call_c   Dyam_Create_Atom("toto")
	move_ret ref[624]
	c_ret

c_code local build_seed_37
	ret_reg &seed[37]
	call_c   build_ref_112()
	call_c   build_ref_202()
	call_c   Dyam_Seed_Start(&ref[112],&ref[202],&ref[202],fun0,1)
	call_c   build_ref_204()
	call_c   Dyam_Seed_Add_Comp(&ref[204],&ref[202],0)
	call_c   Dyam_Seed_End()
	move_ret seed[37]
	c_ret

;; TERM 204: '*FIRST*'(verbose!struct(_C, _D)) :> '$$HOLE$$'
c_code local build_ref_204
	ret_reg &ref[204]
	call_c   build_ref_203()
	call_c   Dyam_Create_Binary(I(9),&ref[203],I(7))
	move_ret ref[204]
	c_ret

;; TERM 203: '*FIRST*'(verbose!struct(_C, _D))
c_code local build_ref_203
	ret_reg &ref[203]
	call_c   build_ref_115()
	call_c   build_ref_201()
	call_c   Dyam_Create_Unary(&ref[115],&ref[201])
	move_ret ref[203]
	c_ret

;; TERM 202: '*CITEM*'(verbose!struct(_C, _D), verbose!struct(_C, _D))
c_code local build_ref_202
	ret_reg &ref[202]
	call_c   build_ref_112()
	call_c   build_ref_201()
	call_c   Dyam_Create_Binary(&ref[112],&ref[201],&ref[201])
	move_ret ref[202]
	c_ret

c_code local build_seed_36
	ret_reg &seed[36]
	call_c   build_ref_158()
	call_c   build_ref_200()
	call_c   Dyam_Seed_Start(&ref[158],&ref[200],I(0),fun9,1)
	call_c   build_ref_198()
	call_c   Dyam_Seed_Add_Comp(&ref[198],fun27,0)
	call_c   Dyam_Seed_End()
	move_ret seed[36]
	c_ret

;; TERM 198: '*RITEM*'(verbose!anchor(_C, _D, _E, _F, _G, _H, _I), voidret)
c_code local build_ref_198
	ret_reg &ref[198]
	call_c   build_ref_0()
	call_c   build_ref_194()
	call_c   build_ref_2()
	call_c   Dyam_Create_Binary(&ref[0],&ref[194],&ref[2])
	move_ret ref[198]
	c_ret

;; TERM 194: verbose!anchor(_C, _D, _E, _F, _G, _H, _I)
c_code local build_ref_194
	ret_reg &ref[194]
	call_c   build_ref_586()
	call_c   Dyam_Term_Start(&ref[586],7)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_End()
	move_ret ref[194]
	c_ret

pl_code local fun27
	call_c   build_ref_198()
	call_c   Dyam_Unify_Item(&ref[198])
	fail_ret
	pl_jump  Follow_Cont(V(1))

;; TERM 200: '*RITEM*'(verbose!anchor(_C, _D, _E, _F, _G, _H, _I), voidret) :> toto(3, '$TUPPLE'(35169791645148))
c_code local build_ref_200
	ret_reg &ref[200]
	call_c   build_ref_198()
	call_c   build_ref_199()
	call_c   Dyam_Create_Binary(I(9),&ref[198],&ref[199])
	move_ret ref[200]
	c_ret

;; TERM 199: toto(3, '$TUPPLE'(35169791645148))
c_code local build_ref_199
	ret_reg &ref[199]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,134217728)
	move_ret R(0)
	call_c   build_ref_624()
	call_c   Dyam_Create_Binary(&ref[624],N(3),R(0))
	move_ret ref[199]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_35
	ret_reg &seed[35]
	call_c   build_ref_112()
	call_c   build_ref_195()
	call_c   Dyam_Seed_Start(&ref[112],&ref[195],&ref[195],fun0,1)
	call_c   build_ref_197()
	call_c   Dyam_Seed_Add_Comp(&ref[197],&ref[195],0)
	call_c   Dyam_Seed_End()
	move_ret seed[35]
	c_ret

;; TERM 197: '*FIRST*'(verbose!anchor(_C, _D, _E, _F, _G, _H, _I)) :> '$$HOLE$$'
c_code local build_ref_197
	ret_reg &ref[197]
	call_c   build_ref_196()
	call_c   Dyam_Create_Binary(I(9),&ref[196],I(7))
	move_ret ref[197]
	c_ret

;; TERM 196: '*FIRST*'(verbose!anchor(_C, _D, _E, _F, _G, _H, _I))
c_code local build_ref_196
	ret_reg &ref[196]
	call_c   build_ref_115()
	call_c   build_ref_194()
	call_c   Dyam_Create_Unary(&ref[115],&ref[194])
	move_ret ref[196]
	c_ret

;; TERM 195: '*CITEM*'(verbose!anchor(_C, _D, _E, _F, _G, _H, _I), verbose!anchor(_C, _D, _E, _F, _G, _H, _I))
c_code local build_ref_195
	ret_reg &ref[195]
	call_c   build_ref_112()
	call_c   build_ref_194()
	call_c   Dyam_Create_Binary(&ref[112],&ref[194],&ref[194])
	move_ret ref[195]
	c_ret

c_code local build_seed_32
	ret_reg &seed[32]
	call_c   build_ref_179()
	call_c   build_ref_182()
	call_c   Dyam_Seed_Start(&ref[179],&ref[182],&ref[182],fun9,1)
	call_c   build_ref_180()
	call_c   Dyam_Seed_Add_Comp(&ref[180],fun21,0)
	call_c   Dyam_Seed_End()
	move_ret seed[32]
	c_ret

;; TERM 180: '*CFI*'{aspop=> 'call_d/2'(_E), current=> 'call_d/2'(_F)}
c_code local build_ref_180
	ret_reg &ref[180]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_591()
	call_c   Dyam_Create_Unary(&ref[591],V(4))
	move_ret R(0)
	call_c   Dyam_Create_Unary(&ref[591],V(5))
	move_ret R(1)
	call_c   build_ref_603()
	call_c   Dyam_Create_Binary(&ref[603],R(0),R(1))
	move_ret ref[180]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

pl_code local fun21
	call_c   build_ref_180()
	call_c   Dyam_Unify_Item(&ref[180])
	fail_ret
	pl_jump  Follow_Cont(V(1))

;; TERM 182: '*CFI*'{aspop=> 'call_d/2'(_E), current=> 'call_d/2'(_F)} :> '$FUNTUPPLE'(toto_1, '$TUPPLE'(35169791645148))
c_code local build_ref_182
	ret_reg &ref[182]
	call_c   build_ref_180()
	call_c   build_ref_181()
	call_c   Dyam_Create_Binary(I(9),&ref[180],&ref[181])
	move_ret ref[182]
	c_ret

;; TERM 181: '$FUNTUPPLE'(toto_1, '$TUPPLE'(35169791645148))
c_code local build_ref_181
	ret_reg &ref[181]
	call_c   build_ref_625()
	call_c   Dyam_Create_Fun_With_Tupple(&ref[625],0,134217728)
	move_ret ref[181]
	c_ret

;; TERM 625: toto_1
c_code local build_ref_625
	ret_reg &ref[625]
	call_c   Dyam_Create_Atom("toto_1")
	move_ret ref[625]
	c_ret

c_code local build_seed_31
	ret_reg &seed[31]
	call_c   build_ref_175()
	call_c   build_ref_176()
	call_c   Dyam_Seed_Start(&ref[175],&ref[176],&ref[176],fun0,1)
	call_c   build_ref_178()
	call_c   Dyam_Seed_Add_Comp(&ref[178],&ref[176],0)
	call_c   Dyam_Seed_End()
	move_ret seed[31]
	c_ret

;; TERM 178: '*SA-AUX-FIRST*'('call_d/2'(_E)) :> '$$HOLE$$'
c_code local build_ref_178
	ret_reg &ref[178]
	call_c   build_ref_177()
	call_c   Dyam_Create_Binary(I(9),&ref[177],I(7))
	move_ret ref[178]
	c_ret

;; TERM 177: '*SA-AUX-FIRST*'('call_d/2'(_E))
c_code local build_ref_177
	ret_reg &ref[177]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_591()
	call_c   Dyam_Create_Unary(&ref[591],V(4))
	move_ret R(0)
	call_c   build_ref_276()
	call_c   Dyam_Create_Unary(&ref[276],R(0))
	move_ret ref[177]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 176: '*CAI*'{current=> 'call_d/2'(_E)}
c_code local build_ref_176
	ret_reg &ref[176]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_591()
	call_c   Dyam_Create_Unary(&ref[591],V(4))
	move_ret R(0)
	call_c   build_ref_596()
	call_c   Dyam_Create_Unary(&ref[596],R(0))
	move_ret ref[176]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_29
	ret_reg &seed[29]
	call_c   build_ref_158()
	call_c   build_ref_163()
	call_c   Dyam_Seed_Start(&ref[158],&ref[163],&ref[163],fun9,1)
	call_c   build_ref_161()
	call_c   Dyam_Seed_Add_Comp(&ref[161],fun13,0)
	call_c   Dyam_Seed_End()
	move_ret seed[29]
	c_ret

;; TERM 161: '*RITEM*'('call_s/2'(_C), return(_D))
c_code local build_ref_161
	ret_reg &ref[161]
	call_c   build_ref_0()
	call_c   build_ref_159()
	call_c   build_ref_160()
	call_c   Dyam_Create_Binary(&ref[0],&ref[159],&ref[160])
	move_ret ref[161]
	c_ret

;; TERM 159: 'call_s/2'(_C)
c_code local build_ref_159
	ret_reg &ref[159]
	call_c   build_ref_600()
	call_c   Dyam_Create_Unary(&ref[600],V(2))
	move_ret ref[159]
	c_ret

pl_code local fun13
	call_c   build_ref_161()
	call_c   Dyam_Unify_Item(&ref[161])
	fail_ret
	pl_jump  Follow_Cont(V(1))

;; TERM 163: '*RITEM*'('call_s/2'(_C), return(_D)) :> '$FUNTUPPLE'(toto_0, '$TUPPLE'(35169791645148))
c_code local build_ref_163
	ret_reg &ref[163]
	call_c   build_ref_161()
	call_c   build_ref_162()
	call_c   Dyam_Create_Binary(I(9),&ref[161],&ref[162])
	move_ret ref[163]
	c_ret

;; TERM 162: '$FUNTUPPLE'(toto_0, '$TUPPLE'(35169791645148))
c_code local build_ref_162
	ret_reg &ref[162]
	call_c   build_ref_626()
	call_c   Dyam_Create_Fun_With_Tupple(&ref[626],0,134217728)
	move_ret ref[162]
	c_ret

;; TERM 626: toto_0
c_code local build_ref_626
	ret_reg &ref[626]
	call_c   Dyam_Create_Atom("toto_0")
	move_ret ref[626]
	c_ret

c_code local build_seed_28
	ret_reg &seed[28]
	call_c   build_ref_148()
	call_c   build_ref_149()
	call_c   Dyam_Seed_Start(&ref[148],&ref[149],&ref[149],fun0,1)
	call_c   build_ref_151()
	call_c   Dyam_Seed_Add_Comp(&ref[151],&ref[149],0)
	call_c   Dyam_Seed_End()
	move_ret seed[28]
	c_ret

;; TERM 151: '*SAFIRST*'('call_s/2'(_C)) :> '$$HOLE$$'
c_code local build_ref_151
	ret_reg &ref[151]
	call_c   build_ref_150()
	call_c   Dyam_Create_Binary(I(9),&ref[150],I(7))
	move_ret ref[151]
	c_ret

;; TERM 150: '*SAFIRST*'('call_s/2'(_C))
c_code local build_ref_150
	ret_reg &ref[150]
	call_c   build_ref_170()
	call_c   build_ref_159()
	call_c   Dyam_Create_Unary(&ref[170],&ref[159])
	move_ret ref[150]
	c_ret

;; TERM 149: '*SACITEM*'('call_s/2'(_C))
c_code local build_ref_149
	ret_reg &ref[149]
	call_c   build_ref_148()
	call_c   build_ref_159()
	call_c   Dyam_Create_Unary(&ref[148],&ref[159])
	move_ret ref[149]
	c_ret

;; TERM 567: [module,file,preds]
c_code local build_ref_567
	ret_reg &ref[567]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_629()
	call_c   Dyam_Create_List(&ref[629],I(0))
	move_ret R(0)
	call_c   build_ref_628()
	call_c   Dyam_Create_List(&ref[628],R(0))
	move_ret R(0)
	call_c   build_ref_627()
	call_c   Dyam_Create_List(&ref[627],R(0))
	move_ret ref[567]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 627: module
c_code local build_ref_627
	ret_reg &ref[627]
	call_c   Dyam_Create_Atom("module")
	move_ret ref[627]
	c_ret

;; TERM 628: file
c_code local build_ref_628
	ret_reg &ref[628]
	call_c   Dyam_Create_Atom("file")
	move_ret ref[628]
	c_ret

;; TERM 629: preds
c_code local build_ref_629
	ret_reg &ref[629]
	call_c   Dyam_Create_Atom("preds")
	move_ret ref[629]
	c_ret

;; TERM 566: import
c_code local build_ref_566
	ret_reg &ref[566]
	call_c   Dyam_Create_Atom("import")
	move_ret ref[566]
	c_ret

;; TERM 3: '*RITEM*'(verbose!adj, voidret)
c_code local build_ref_3
	ret_reg &ref[3]
	call_c   build_ref_0()
	call_c   build_ref_1()
	call_c   build_ref_2()
	call_c   Dyam_Create_Binary(&ref[0],&ref[1],&ref[2])
	move_ret ref[3]
	c_ret

;; TERM 4: verbose!lexical(_A, _B, _C, _D, _E)
c_code local build_ref_4
	ret_reg &ref[4]
	call_c   build_ref_584()
	call_c   Dyam_Term_Start(&ref[584],5)
	call_c   Dyam_Term_Arg(V(0))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[4]
	c_ret

;; TERM 5: '*RITEM*'(verbose!lexical(_A, _B, _C, _D, _E), voidret)
c_code local build_ref_5
	ret_reg &ref[5]
	call_c   build_ref_0()
	call_c   build_ref_4()
	call_c   build_ref_2()
	call_c   Dyam_Create_Binary(&ref[0],&ref[4],&ref[2])
	move_ret ref[5]
	c_ret

;; TERM 6: verbose!anchor(_A, _B, _C, _D, _E, _F, _G)
c_code local build_ref_6
	ret_reg &ref[6]
	call_c   build_ref_586()
	call_c   Dyam_Term_Start(&ref[586],7)
	call_c   Dyam_Term_Arg(V(0))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[6]
	c_ret

;; TERM 7: '*RITEM*'(verbose!anchor(_A, _B, _C, _D, _E, _F, _G), voidret)
c_code local build_ref_7
	ret_reg &ref[7]
	call_c   build_ref_0()
	call_c   build_ref_6()
	call_c   build_ref_2()
	call_c   Dyam_Create_Binary(&ref[0],&ref[6],&ref[2])
	move_ret ref[7]
	c_ret

;; TERM 8: verbose!struct(_A, _B)
c_code local build_ref_8
	ret_reg &ref[8]
	call_c   build_ref_583()
	call_c   Dyam_Create_Binary(&ref[583],V(0),V(1))
	move_ret ref[8]
	c_ret

;; TERM 9: '*RITEM*'(verbose!struct(_A, _B), voidret)
c_code local build_ref_9
	ret_reg &ref[9]
	call_c   build_ref_0()
	call_c   build_ref_8()
	call_c   build_ref_2()
	call_c   Dyam_Create_Binary(&ref[0],&ref[8],&ref[2])
	move_ret ref[9]
	c_ret

;; TERM 14: n(_C,_D) * n(_A,_B)
c_code local build_ref_14
	ret_reg &ref[14]
	call_c   build_ref_11()
	call_c   build_ref_12()
	call_c   build_ref_13()
	call_c   Dyam_Create_Binary(&ref[11],&ref[12],&ref[13])
	move_ret ref[14]
	c_ret

;; TERM 13: n(_A,_B)
c_code local build_ref_13
	ret_reg &ref[13]
	call_c   build_ref_87()
	call_c   Dyam_Term_Start(I(3),3)
	call_c   Dyam_Term_Arg(&ref[87])
	call_c   Dyam_Term_Arg(V(0))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_End()
	move_ret ref[13]
	c_ret

;; TERM 12: n(_C,_D)
c_code local build_ref_12
	ret_reg &ref[12]
	call_c   build_ref_87()
	call_c   Dyam_Term_Start(I(3),3)
	call_c   Dyam_Term_Arg(&ref[87])
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[12]
	c_ret

;; TERM 11: *
c_code local build_ref_11
	ret_reg &ref[11]
	call_c   Dyam_Create_Atom("*")
	move_ret ref[11]
	c_ret

;; TERM 10: '*RAI*'{xstart=> 'call_n/2'(_A), xend=> return(_B), aspop=> 'call_n/2'(_C), current=> return(_D)}
c_code local build_ref_10
	ret_reg &ref[10]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_597()
	call_c   Dyam_Create_Unary(&ref[597],V(0))
	move_ret R(0)
	call_c   Dyam_Create_Unary(&ref[597],V(2))
	move_ret R(1)
	call_c   build_ref_598()
	call_c   build_ref_33()
	call_c   build_ref_160()
	call_c   Dyam_Term_Start(&ref[598],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[33])
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(&ref[160])
	call_c   Dyam_Term_End()
	move_ret ref[10]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 33: return(_B)
c_code local build_ref_33
	ret_reg &ref[33]
	call_c   build_ref_594()
	call_c   Dyam_Create_Unary(&ref[594],V(1))
	move_ret ref[33]
	c_ret

;; TERM 18: a(_C,_D) * a(_A,_B)
c_code local build_ref_18
	ret_reg &ref[18]
	call_c   build_ref_11()
	call_c   build_ref_16()
	call_c   build_ref_17()
	call_c   Dyam_Create_Binary(&ref[11],&ref[16],&ref[17])
	move_ret ref[18]
	c_ret

;; TERM 17: a(_A,_B)
c_code local build_ref_17
	ret_reg &ref[17]
	call_c   build_ref_20()
	call_c   Dyam_Term_Start(I(3),3)
	call_c   Dyam_Term_Arg(&ref[20])
	call_c   Dyam_Term_Arg(V(0))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_End()
	move_ret ref[17]
	c_ret

;; TERM 16: a(_C,_D)
c_code local build_ref_16
	ret_reg &ref[16]
	call_c   build_ref_20()
	call_c   Dyam_Term_Start(I(3),3)
	call_c   Dyam_Term_Arg(&ref[20])
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[16]
	c_ret

;; TERM 15: '*RAI*'{xstart=> 'call_a/2'(_A), xend=> return(_B), aspop=> 'call_a/2'(_C), current=> return(_D)}
c_code local build_ref_15
	ret_reg &ref[15]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_617()
	call_c   Dyam_Create_Unary(&ref[617],V(0))
	move_ret R(0)
	call_c   Dyam_Create_Unary(&ref[617],V(2))
	move_ret R(1)
	call_c   build_ref_598()
	call_c   build_ref_33()
	call_c   build_ref_160()
	call_c   Dyam_Term_Start(&ref[598],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[33])
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(&ref[160])
	call_c   Dyam_Term_End()
	move_ret ref[15]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 35: d(_A,_B)
c_code local build_ref_35
	ret_reg &ref[35]
	call_c   build_ref_39()
	call_c   Dyam_Term_Start(I(3),3)
	call_c   Dyam_Term_Arg(&ref[39])
	call_c   Dyam_Term_Arg(V(0))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_End()
	move_ret ref[35]
	c_ret

;; TERM 34: '*RITEM*'('call_d/2'(_A), return(_B))
c_code local build_ref_34
	ret_reg &ref[34]
	call_c   build_ref_0()
	call_c   build_ref_32()
	call_c   build_ref_33()
	call_c   Dyam_Create_Binary(&ref[0],&ref[32],&ref[33])
	move_ret ref[34]
	c_ret

;; TERM 32: 'call_d/2'(_A)
c_code local build_ref_32
	ret_reg &ref[32]
	call_c   build_ref_591()
	call_c   Dyam_Create_Unary(&ref[591],V(0))
	move_ret ref[32]
	c_ret

;; TERM 38: d(_C,_D) * d(_A,_B)
c_code local build_ref_38
	ret_reg &ref[38]
	call_c   build_ref_11()
	call_c   build_ref_37()
	call_c   build_ref_35()
	call_c   Dyam_Create_Binary(&ref[11],&ref[37],&ref[35])
	move_ret ref[38]
	c_ret

;; TERM 37: d(_C,_D)
c_code local build_ref_37
	ret_reg &ref[37]
	call_c   build_ref_39()
	call_c   Dyam_Term_Start(I(3),3)
	call_c   Dyam_Term_Arg(&ref[39])
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[37]
	c_ret

;; TERM 36: '*RAI*'{xstart=> 'call_d/2'(_A), xend=> return(_B), aspop=> 'call_d/2'(_C), current=> return(_D)}
c_code local build_ref_36
	ret_reg &ref[36]
	call_c   build_ref_598()
	call_c   build_ref_32()
	call_c   build_ref_33()
	call_c   build_ref_363()
	call_c   build_ref_160()
	call_c   Dyam_Term_Start(&ref[598],4)
	call_c   Dyam_Term_Arg(&ref[32])
	call_c   Dyam_Term_Arg(&ref[33])
	call_c   Dyam_Term_Arg(&ref[363])
	call_c   Dyam_Term_Arg(&ref[160])
	call_c   Dyam_Term_End()
	move_ret ref[36]
	c_ret

;; TERM 48: np(_C,_D) * np(_A,_B)
c_code local build_ref_48
	ret_reg &ref[48]
	call_c   build_ref_11()
	call_c   build_ref_46()
	call_c   build_ref_47()
	call_c   Dyam_Create_Binary(&ref[11],&ref[46],&ref[47])
	move_ret ref[48]
	c_ret

;; TERM 47: np(_A,_B)
c_code local build_ref_47
	ret_reg &ref[47]
	call_c   build_ref_80()
	call_c   Dyam_Term_Start(I(3),3)
	call_c   Dyam_Term_Arg(&ref[80])
	call_c   Dyam_Term_Arg(V(0))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_End()
	move_ret ref[47]
	c_ret

;; TERM 46: np(_C,_D)
c_code local build_ref_46
	ret_reg &ref[46]
	call_c   build_ref_80()
	call_c   Dyam_Term_Start(I(3),3)
	call_c   Dyam_Term_Arg(&ref[80])
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[46]
	c_ret

;; TERM 45: '*RAI*'{xstart=> 'call_np/2'(_A), xend=> return(_B), aspop=> 'call_np/2'(_C), current=> return(_D)}
c_code local build_ref_45
	ret_reg &ref[45]
	call_c   build_ref_598()
	call_c   build_ref_49()
	call_c   build_ref_33()
	call_c   build_ref_408()
	call_c   build_ref_160()
	call_c   Dyam_Term_Start(&ref[598],4)
	call_c   Dyam_Term_Arg(&ref[49])
	call_c   Dyam_Term_Arg(&ref[33])
	call_c   Dyam_Term_Arg(&ref[408])
	call_c   Dyam_Term_Arg(&ref[160])
	call_c   Dyam_Term_End()
	move_ret ref[45]
	c_ret

;; TERM 49: 'call_np/2'(_A)
c_code local build_ref_49
	ret_reg &ref[49]
	call_c   build_ref_595()
	call_c   Dyam_Create_Unary(&ref[595],V(0))
	move_ret ref[49]
	c_ret

;; TERM 50: '*RITEM*'('call_np/2'(_A), return(_B))
c_code local build_ref_50
	ret_reg &ref[50]
	call_c   build_ref_0()
	call_c   build_ref_49()
	call_c   build_ref_33()
	call_c   Dyam_Create_Binary(&ref[0],&ref[49],&ref[33])
	move_ret ref[50]
	c_ret

;; TERM 54: p(_C,_D) * p(_A,_B)
c_code local build_ref_54
	ret_reg &ref[54]
	call_c   build_ref_11()
	call_c   build_ref_52()
	call_c   build_ref_53()
	call_c   Dyam_Create_Binary(&ref[11],&ref[52],&ref[53])
	move_ret ref[54]
	c_ret

;; TERM 53: p(_A,_B)
c_code local build_ref_53
	ret_reg &ref[53]
	call_c   build_ref_57()
	call_c   Dyam_Term_Start(I(3),3)
	call_c   Dyam_Term_Arg(&ref[57])
	call_c   Dyam_Term_Arg(V(0))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_End()
	move_ret ref[53]
	c_ret

;; TERM 52: p(_C,_D)
c_code local build_ref_52
	ret_reg &ref[52]
	call_c   build_ref_57()
	call_c   Dyam_Term_Start(I(3),3)
	call_c   Dyam_Term_Arg(&ref[57])
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[52]
	c_ret

;; TERM 51: '*RAI*'{xstart=> 'call_p/2'(_A), xend=> return(_B), aspop=> 'call_p/2'(_C), current=> return(_D)}
c_code local build_ref_51
	ret_reg &ref[51]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_613()
	call_c   Dyam_Create_Unary(&ref[613],V(0))
	move_ret R(0)
	call_c   Dyam_Create_Unary(&ref[613],V(2))
	move_ret R(1)
	call_c   build_ref_598()
	call_c   build_ref_33()
	call_c   build_ref_160()
	call_c   Dyam_Term_Start(&ref[598],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[33])
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(&ref[160])
	call_c   Dyam_Term_End()
	move_ret ref[51]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 66: vp(_C,_D) * vp(_A,_B)
c_code local build_ref_66
	ret_reg &ref[66]
	call_c   build_ref_11()
	call_c   build_ref_64()
	call_c   build_ref_65()
	call_c   Dyam_Create_Binary(&ref[11],&ref[64],&ref[65])
	move_ret ref[66]
	c_ret

;; TERM 65: vp(_A,_B)
c_code local build_ref_65
	ret_reg &ref[65]
	call_c   build_ref_630()
	call_c   Dyam_Term_Start(I(3),3)
	call_c   Dyam_Term_Arg(&ref[630])
	call_c   Dyam_Term_Arg(V(0))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_End()
	move_ret ref[65]
	c_ret

;; TERM 630: vp
c_code local build_ref_630
	ret_reg &ref[630]
	call_c   Dyam_Create_Atom("vp")
	move_ret ref[630]
	c_ret

;; TERM 64: vp(_C,_D)
c_code local build_ref_64
	ret_reg &ref[64]
	call_c   build_ref_630()
	call_c   Dyam_Term_Start(I(3),3)
	call_c   Dyam_Term_Arg(&ref[630])
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[64]
	c_ret

;; TERM 63: '*RAI*'{xstart=> 'call_vp/2'(_A), xend=> return(_B), aspop=> 'call_vp/2'(_C), current=> return(_D)}
c_code local build_ref_63
	ret_reg &ref[63]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_599()
	call_c   Dyam_Create_Unary(&ref[599],V(0))
	move_ret R(0)
	call_c   Dyam_Create_Unary(&ref[599],V(2))
	move_ret R(1)
	call_c   build_ref_598()
	call_c   build_ref_33()
	call_c   build_ref_160()
	call_c   Dyam_Term_Start(&ref[598],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[33])
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(&ref[160])
	call_c   Dyam_Term_End()
	move_ret ref[63]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 70: pp(_C,_D) * pp(_A,_B)
c_code local build_ref_70
	ret_reg &ref[70]
	call_c   build_ref_11()
	call_c   build_ref_68()
	call_c   build_ref_69()
	call_c   Dyam_Create_Binary(&ref[11],&ref[68],&ref[69])
	move_ret ref[70]
	c_ret

;; TERM 69: pp(_A,_B)
c_code local build_ref_69
	ret_reg &ref[69]
	call_c   build_ref_631()
	call_c   Dyam_Term_Start(I(3),3)
	call_c   Dyam_Term_Arg(&ref[631])
	call_c   Dyam_Term_Arg(V(0))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_End()
	move_ret ref[69]
	c_ret

;; TERM 631: pp
c_code local build_ref_631
	ret_reg &ref[631]
	call_c   Dyam_Create_Atom("pp")
	move_ret ref[631]
	c_ret

;; TERM 68: pp(_C,_D)
c_code local build_ref_68
	ret_reg &ref[68]
	call_c   build_ref_631()
	call_c   Dyam_Term_Start(I(3),3)
	call_c   Dyam_Term_Arg(&ref[631])
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[68]
	c_ret

;; TERM 67: '*RAI*'{xstart=> 'call_pp/2'(_A), xend=> return(_B), aspop=> 'call_pp/2'(_C), current=> return(_D)}
c_code local build_ref_67
	ret_reg &ref[67]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_607()
	call_c   Dyam_Create_Unary(&ref[607],V(0))
	move_ret R(0)
	call_c   Dyam_Create_Unary(&ref[607],V(2))
	move_ret R(1)
	call_c   build_ref_598()
	call_c   build_ref_33()
	call_c   build_ref_160()
	call_c   Dyam_Term_Start(&ref[598],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[33])
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(&ref[160])
	call_c   Dyam_Term_End()
	move_ret ref[67]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 79: pn(_C,_D) * pn(_A,_B)
c_code local build_ref_79
	ret_reg &ref[79]
	call_c   build_ref_11()
	call_c   build_ref_77()
	call_c   build_ref_78()
	call_c   Dyam_Create_Binary(&ref[11],&ref[77],&ref[78])
	move_ret ref[79]
	c_ret

;; TERM 78: pn(_A,_B)
c_code local build_ref_78
	ret_reg &ref[78]
	call_c   build_ref_81()
	call_c   Dyam_Term_Start(I(3),3)
	call_c   Dyam_Term_Arg(&ref[81])
	call_c   Dyam_Term_Arg(V(0))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_End()
	move_ret ref[78]
	c_ret

;; TERM 77: pn(_C,_D)
c_code local build_ref_77
	ret_reg &ref[77]
	call_c   build_ref_81()
	call_c   Dyam_Term_Start(I(3),3)
	call_c   Dyam_Term_Arg(&ref[81])
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[77]
	c_ret

;; TERM 76: '*RAI*'{xstart=> 'call_pn/2'(_A), xend=> return(_B), aspop=> 'call_pn/2'(_C), current=> return(_D)}
c_code local build_ref_76
	ret_reg &ref[76]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_621()
	call_c   Dyam_Create_Unary(&ref[621],V(0))
	move_ret R(0)
	call_c   Dyam_Create_Unary(&ref[621],V(2))
	move_ret R(1)
	call_c   build_ref_598()
	call_c   build_ref_33()
	call_c   build_ref_160()
	call_c   Dyam_Term_Start(&ref[598],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[33])
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(&ref[160])
	call_c   Dyam_Term_End()
	move_ret ref[76]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 96: s(_A,_B)
c_code local build_ref_96
	ret_reg &ref[96]
	call_c   build_ref_632()
	call_c   Dyam_Term_Start(I(3),3)
	call_c   Dyam_Term_Arg(&ref[632])
	call_c   Dyam_Term_Arg(V(0))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_End()
	move_ret ref[96]
	c_ret

;; TERM 632: s
c_code local build_ref_632
	ret_reg &ref[632]
	call_c   Dyam_Create_Atom("s")
	move_ret ref[632]
	c_ret

;; TERM 95: '*RITEM*'('call_s/2'(_A), return(_B))
c_code local build_ref_95
	ret_reg &ref[95]
	call_c   build_ref_0()
	call_c   build_ref_94()
	call_c   build_ref_33()
	call_c   Dyam_Create_Binary(&ref[0],&ref[94],&ref[33])
	move_ret ref[95]
	c_ret

;; TERM 94: 'call_s/2'(_A)
c_code local build_ref_94
	ret_reg &ref[94]
	call_c   build_ref_600()
	call_c   Dyam_Create_Unary(&ref[600],V(0))
	move_ret ref[94]
	c_ret

;; TERM 99: s(_C,_D) * s(_A,_B)
c_code local build_ref_99
	ret_reg &ref[99]
	call_c   build_ref_11()
	call_c   build_ref_98()
	call_c   build_ref_96()
	call_c   Dyam_Create_Binary(&ref[11],&ref[98],&ref[96])
	move_ret ref[99]
	c_ret

;; TERM 98: s(_C,_D)
c_code local build_ref_98
	ret_reg &ref[98]
	call_c   build_ref_632()
	call_c   Dyam_Term_Start(I(3),3)
	call_c   Dyam_Term_Arg(&ref[632])
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[98]
	c_ret

;; TERM 97: '*RAI*'{xstart=> 'call_s/2'(_A), xend=> return(_B), aspop=> 'call_s/2'(_C), current=> return(_D)}
c_code local build_ref_97
	ret_reg &ref[97]
	call_c   build_ref_598()
	call_c   build_ref_94()
	call_c   build_ref_33()
	call_c   build_ref_159()
	call_c   build_ref_160()
	call_c   Dyam_Term_Start(&ref[598],4)
	call_c   Dyam_Term_Arg(&ref[94])
	call_c   Dyam_Term_Arg(&ref[33])
	call_c   Dyam_Term_Arg(&ref[159])
	call_c   Dyam_Term_Arg(&ref[160])
	call_c   Dyam_Term_End()
	move_ret ref[97]
	c_ret

;; TERM 103: v(_C,_D) * v(_A,_B)
c_code local build_ref_103
	ret_reg &ref[103]
	call_c   build_ref_11()
	call_c   build_ref_101()
	call_c   build_ref_102()
	call_c   Dyam_Create_Binary(&ref[11],&ref[101],&ref[102])
	move_ret ref[103]
	c_ret

;; TERM 102: v(_A,_B)
c_code local build_ref_102
	ret_reg &ref[102]
	call_c   build_ref_106()
	call_c   Dyam_Term_Start(I(3),3)
	call_c   Dyam_Term_Arg(&ref[106])
	call_c   Dyam_Term_Arg(V(0))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_End()
	move_ret ref[102]
	c_ret

;; TERM 101: v(_C,_D)
c_code local build_ref_101
	ret_reg &ref[101]
	call_c   build_ref_106()
	call_c   Dyam_Term_Start(I(3),3)
	call_c   Dyam_Term_Arg(&ref[106])
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[101]
	c_ret

;; TERM 100: '*RAI*'{xstart=> 'call_v/2'(_A), xend=> return(_B), aspop=> 'call_v/2'(_C), current=> return(_D)}
c_code local build_ref_100
	ret_reg &ref[100]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_604()
	call_c   Dyam_Create_Unary(&ref[604],V(0))
	move_ret R(0)
	call_c   Dyam_Create_Unary(&ref[604],V(2))
	move_ret R(1)
	call_c   build_ref_598()
	call_c   build_ref_33()
	call_c   build_ref_160()
	call_c   Dyam_Term_Start(&ref[598],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[33])
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(&ref[160])
	call_c   Dyam_Term_End()
	move_ret ref[100]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 44: lctag_map_reverse(d, d, subst)
c_code local build_ref_44
	ret_reg &ref[44]
	call_c   build_ref_633()
	call_c   build_ref_39()
	call_c   build_ref_634()
	call_c   Dyam_Term_Start(&ref[633],3)
	call_c   Dyam_Term_Arg(&ref[39])
	call_c   Dyam_Term_Arg(&ref[39])
	call_c   Dyam_Term_Arg(&ref[634])
	call_c   Dyam_Term_End()
	move_ret ref[44]
	c_ret

;; TERM 634: subst
c_code local build_ref_634
	ret_reg &ref[634]
	call_c   Dyam_Create_Atom("subst")
	move_ret ref[634]
	c_ret

;; TERM 633: lctag_map_reverse
c_code local build_ref_633
	ret_reg &ref[633]
	call_c   Dyam_Create_Atom("lctag_map_reverse")
	move_ret ref[633]
	c_ret

;; TERM 43: lctag_map(d, subst, d)
c_code local build_ref_43
	ret_reg &ref[43]
	call_c   build_ref_635()
	call_c   build_ref_39()
	call_c   build_ref_634()
	call_c   Dyam_Term_Start(&ref[635],3)
	call_c   Dyam_Term_Arg(&ref[39])
	call_c   Dyam_Term_Arg(&ref[634])
	call_c   Dyam_Term_Arg(&ref[39])
	call_c   Dyam_Term_End()
	move_ret ref[43]
	c_ret

;; TERM 635: lctag_map
c_code local build_ref_635
	ret_reg &ref[635]
	call_c   Dyam_Create_Atom("lctag_map")
	move_ret ref[635]
	c_ret

;; TERM 42: loaded_tree(d)
c_code local build_ref_42
	ret_reg &ref[42]
	call_c   build_ref_636()
	call_c   build_ref_39()
	call_c   Dyam_Create_Unary(&ref[636],&ref[39])
	move_ret ref[42]
	c_ret

;; TERM 636: loaded_tree
c_code local build_ref_636
	ret_reg &ref[636]
	call_c   Dyam_Create_Atom("loaded_tree")
	move_ret ref[636]
	c_ret

;; TERM 41: lctag_ok(d)
c_code local build_ref_41
	ret_reg &ref[41]
	call_c   build_ref_637()
	call_c   build_ref_39()
	call_c   Dyam_Create_Unary(&ref[637],&ref[39])
	move_ret ref[41]
	c_ret

;; TERM 637: lctag_ok
c_code local build_ref_637
	ret_reg &ref[637]
	call_c   Dyam_Create_Atom("lctag_ok")
	move_ret ref[637]
	c_ret

;; TERM 40: '$VAR'(_P, ['$RANGE',0,1000])
c_code local build_ref_40
	ret_reg &ref[40]
	call_c   build_ref_638()
	call_c   Dyam_Term_Start(&ref[638],3)
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(N(0))
	call_c   Dyam_Term_Arg(N(1000))
	call_c   Dyam_Term_End()
	move_ret ref[40]
	c_ret

;; TERM 638: '$RANGE'
c_code local build_ref_638
	ret_reg &ref[638]
	call_c   Dyam_Create_Atom("$RANGE")
	move_ret ref[638]
	c_ret

;; TERM 85: lctag_map_reverse(pn, np, subst)
c_code local build_ref_85
	ret_reg &ref[85]
	call_c   build_ref_633()
	call_c   build_ref_81()
	call_c   build_ref_80()
	call_c   build_ref_634()
	call_c   Dyam_Term_Start(&ref[633],3)
	call_c   Dyam_Term_Arg(&ref[81])
	call_c   Dyam_Term_Arg(&ref[80])
	call_c   Dyam_Term_Arg(&ref[634])
	call_c   Dyam_Term_End()
	move_ret ref[85]
	c_ret

;; TERM 84: lctag_map(np, subst, pn)
c_code local build_ref_84
	ret_reg &ref[84]
	call_c   build_ref_635()
	call_c   build_ref_80()
	call_c   build_ref_634()
	call_c   build_ref_81()
	call_c   Dyam_Term_Start(&ref[635],3)
	call_c   Dyam_Term_Arg(&ref[80])
	call_c   Dyam_Term_Arg(&ref[634])
	call_c   Dyam_Term_Arg(&ref[81])
	call_c   Dyam_Term_End()
	move_ret ref[84]
	c_ret

;; TERM 83: loaded_tree(pn)
c_code local build_ref_83
	ret_reg &ref[83]
	call_c   build_ref_636()
	call_c   build_ref_81()
	call_c   Dyam_Create_Unary(&ref[636],&ref[81])
	move_ret ref[83]
	c_ret

;; TERM 82: lctag_ok(pn)
c_code local build_ref_82
	ret_reg &ref[82]
	call_c   build_ref_637()
	call_c   build_ref_81()
	call_c   Dyam_Create_Unary(&ref[637],&ref[81])
	move_ret ref[82]
	c_ret

;; TERM 89: '$VAR'(_T, ['$RANGE',0,1000])
c_code local build_ref_89
	ret_reg &ref[89]
	call_c   build_ref_638()
	call_c   Dyam_Term_Start(&ref[638],3)
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_Arg(N(0))
	call_c   Dyam_Term_Arg(N(1000))
	call_c   Dyam_Term_End()
	move_ret ref[89]
	c_ret

;; TERM 86: '$VAR'(_Q, ['$RANGE',0,1000])
c_code local build_ref_86
	ret_reg &ref[86]
	call_c   build_ref_638()
	call_c   Dyam_Term_Start(&ref[638],3)
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(N(0))
	call_c   Dyam_Term_Arg(N(1000))
	call_c   Dyam_Term_End()
	move_ret ref[86]
	c_ret

;; TERM 93: lctag_map_reverse(dn, np, subst)
c_code local build_ref_93
	ret_reg &ref[93]
	call_c   build_ref_633()
	call_c   build_ref_88()
	call_c   build_ref_80()
	call_c   build_ref_634()
	call_c   Dyam_Term_Start(&ref[633],3)
	call_c   Dyam_Term_Arg(&ref[88])
	call_c   Dyam_Term_Arg(&ref[80])
	call_c   Dyam_Term_Arg(&ref[634])
	call_c   Dyam_Term_End()
	move_ret ref[93]
	c_ret

;; TERM 92: lctag_map(np, subst, dn)
c_code local build_ref_92
	ret_reg &ref[92]
	call_c   build_ref_635()
	call_c   build_ref_80()
	call_c   build_ref_634()
	call_c   build_ref_88()
	call_c   Dyam_Term_Start(&ref[635],3)
	call_c   Dyam_Term_Arg(&ref[80])
	call_c   Dyam_Term_Arg(&ref[634])
	call_c   Dyam_Term_Arg(&ref[88])
	call_c   Dyam_Term_End()
	move_ret ref[92]
	c_ret

;; TERM 91: loaded_tree(dn)
c_code local build_ref_91
	ret_reg &ref[91]
	call_c   build_ref_636()
	call_c   build_ref_88()
	call_c   Dyam_Create_Unary(&ref[636],&ref[88])
	move_ret ref[91]
	c_ret

;; TERM 90: lctag_ok(dn)
c_code local build_ref_90
	ret_reg &ref[90]
	call_c   build_ref_637()
	call_c   build_ref_88()
	call_c   Dyam_Create_Unary(&ref[637],&ref[88])
	move_ret ref[90]
	c_ret

;; TERM 26: lctag_map_reverse(na, n, wrap)
c_code local build_ref_26
	ret_reg &ref[26]
	call_c   build_ref_633()
	call_c   build_ref_21()
	call_c   build_ref_87()
	call_c   build_ref_639()
	call_c   Dyam_Term_Start(&ref[633],3)
	call_c   Dyam_Term_Arg(&ref[21])
	call_c   Dyam_Term_Arg(&ref[87])
	call_c   Dyam_Term_Arg(&ref[639])
	call_c   Dyam_Term_End()
	move_ret ref[26]
	c_ret

;; TERM 639: wrap
c_code local build_ref_639
	ret_reg &ref[639]
	call_c   Dyam_Create_Atom("wrap")
	move_ret ref[639]
	c_ret

;; TERM 25: lctag_map(n, wrap, na)
c_code local build_ref_25
	ret_reg &ref[25]
	call_c   build_ref_635()
	call_c   build_ref_87()
	call_c   build_ref_639()
	call_c   build_ref_21()
	call_c   Dyam_Term_Start(&ref[635],3)
	call_c   Dyam_Term_Arg(&ref[87])
	call_c   Dyam_Term_Arg(&ref[639])
	call_c   Dyam_Term_Arg(&ref[21])
	call_c   Dyam_Term_End()
	move_ret ref[25]
	c_ret

;; TERM 24: loaded_tree(na)
c_code local build_ref_24
	ret_reg &ref[24]
	call_c   build_ref_636()
	call_c   build_ref_21()
	call_c   Dyam_Create_Unary(&ref[636],&ref[21])
	move_ret ref[24]
	c_ret

;; TERM 23: lctag_ok(na)
c_code local build_ref_23
	ret_reg &ref[23]
	call_c   build_ref_637()
	call_c   build_ref_21()
	call_c   Dyam_Create_Unary(&ref[637],&ref[21])
	move_ret ref[23]
	c_ret

;; TERM 22: '$VAR'(_U, ['$RANGE',0,1000])
c_code local build_ref_22
	ret_reg &ref[22]
	call_c   build_ref_638()
	call_c   Dyam_Term_Start(&ref[638],3)
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_Arg(N(0))
	call_c   Dyam_Term_Arg(N(1000))
	call_c   Dyam_Term_End()
	move_ret ref[22]
	c_ret

;; TERM 19: '$VAR'(_R, ['$RANGE',0,1000])
c_code local build_ref_19
	ret_reg &ref[19]
	call_c   build_ref_638()
	call_c   Dyam_Term_Start(&ref[638],3)
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(N(0))
	call_c   Dyam_Term_Arg(N(1000))
	call_c   Dyam_Term_End()
	move_ret ref[19]
	c_ret

;; TERM 31: lctag_map_reverse(an, n, wrap)
c_code local build_ref_31
	ret_reg &ref[31]
	call_c   build_ref_633()
	call_c   build_ref_27()
	call_c   build_ref_87()
	call_c   build_ref_639()
	call_c   Dyam_Term_Start(&ref[633],3)
	call_c   Dyam_Term_Arg(&ref[27])
	call_c   Dyam_Term_Arg(&ref[87])
	call_c   Dyam_Term_Arg(&ref[639])
	call_c   Dyam_Term_End()
	move_ret ref[31]
	c_ret

;; TERM 30: lctag_map(n, wrap, an)
c_code local build_ref_30
	ret_reg &ref[30]
	call_c   build_ref_635()
	call_c   build_ref_87()
	call_c   build_ref_639()
	call_c   build_ref_27()
	call_c   Dyam_Term_Start(&ref[635],3)
	call_c   Dyam_Term_Arg(&ref[87])
	call_c   Dyam_Term_Arg(&ref[639])
	call_c   Dyam_Term_Arg(&ref[27])
	call_c   Dyam_Term_End()
	move_ret ref[30]
	c_ret

;; TERM 29: loaded_tree(an)
c_code local build_ref_29
	ret_reg &ref[29]
	call_c   build_ref_636()
	call_c   build_ref_27()
	call_c   Dyam_Create_Unary(&ref[636],&ref[27])
	move_ret ref[29]
	c_ret

;; TERM 28: lctag_ok(an)
c_code local build_ref_28
	ret_reg &ref[28]
	call_c   build_ref_637()
	call_c   build_ref_27()
	call_c   Dyam_Create_Unary(&ref[637],&ref[27])
	move_ret ref[28]
	c_ret

;; TERM 62: lctag_map_reverse(nppn, np, wrap)
c_code local build_ref_62
	ret_reg &ref[62]
	call_c   build_ref_633()
	call_c   build_ref_56()
	call_c   build_ref_80()
	call_c   build_ref_639()
	call_c   Dyam_Term_Start(&ref[633],3)
	call_c   Dyam_Term_Arg(&ref[56])
	call_c   Dyam_Term_Arg(&ref[80])
	call_c   Dyam_Term_Arg(&ref[639])
	call_c   Dyam_Term_End()
	move_ret ref[62]
	c_ret

;; TERM 61: lctag_map(np, wrap, nppn)
c_code local build_ref_61
	ret_reg &ref[61]
	call_c   build_ref_635()
	call_c   build_ref_80()
	call_c   build_ref_639()
	call_c   build_ref_56()
	call_c   Dyam_Term_Start(&ref[635],3)
	call_c   Dyam_Term_Arg(&ref[80])
	call_c   Dyam_Term_Arg(&ref[639])
	call_c   Dyam_Term_Arg(&ref[56])
	call_c   Dyam_Term_End()
	move_ret ref[61]
	c_ret

;; TERM 60: loaded_tree(nppn)
c_code local build_ref_60
	ret_reg &ref[60]
	call_c   build_ref_636()
	call_c   build_ref_56()
	call_c   Dyam_Create_Unary(&ref[636],&ref[56])
	move_ret ref[60]
	c_ret

;; TERM 59: lctag_ok(nppn)
c_code local build_ref_59
	ret_reg &ref[59]
	call_c   build_ref_637()
	call_c   build_ref_56()
	call_c   Dyam_Create_Unary(&ref[637],&ref[56])
	move_ret ref[59]
	c_ret

;; TERM 58: '$VAR'(_A1, ['$RANGE',0,1000])
c_code local build_ref_58
	ret_reg &ref[58]
	call_c   build_ref_638()
	call_c   Dyam_Term_Start(&ref[638],3)
	call_c   Dyam_Term_Arg(V(26))
	call_c   Dyam_Term_Arg(N(0))
	call_c   Dyam_Term_Arg(N(1000))
	call_c   Dyam_Term_End()
	move_ret ref[58]
	c_ret

;; TERM 55: '$VAR'(_W, ['$RANGE',0,1000])
c_code local build_ref_55
	ret_reg &ref[55]
	call_c   build_ref_638()
	call_c   Dyam_Term_Start(&ref[638],3)
	call_c   Dyam_Term_Arg(V(22))
	call_c   Dyam_Term_Arg(N(0))
	call_c   Dyam_Term_Arg(N(1000))
	call_c   Dyam_Term_End()
	move_ret ref[55]
	c_ret

;; TERM 75: lctag_map_reverse(vppn, vp, wrap)
c_code local build_ref_75
	ret_reg &ref[75]
	call_c   build_ref_633()
	call_c   build_ref_71()
	call_c   build_ref_630()
	call_c   build_ref_639()
	call_c   Dyam_Term_Start(&ref[633],3)
	call_c   Dyam_Term_Arg(&ref[71])
	call_c   Dyam_Term_Arg(&ref[630])
	call_c   Dyam_Term_Arg(&ref[639])
	call_c   Dyam_Term_End()
	move_ret ref[75]
	c_ret

;; TERM 74: lctag_map(vp, wrap, vppn)
c_code local build_ref_74
	ret_reg &ref[74]
	call_c   build_ref_635()
	call_c   build_ref_630()
	call_c   build_ref_639()
	call_c   build_ref_71()
	call_c   Dyam_Term_Start(&ref[635],3)
	call_c   Dyam_Term_Arg(&ref[630])
	call_c   Dyam_Term_Arg(&ref[639])
	call_c   Dyam_Term_Arg(&ref[71])
	call_c   Dyam_Term_End()
	move_ret ref[74]
	c_ret

;; TERM 73: loaded_tree(vppn)
c_code local build_ref_73
	ret_reg &ref[73]
	call_c   build_ref_636()
	call_c   build_ref_71()
	call_c   Dyam_Create_Unary(&ref[636],&ref[71])
	move_ret ref[73]
	c_ret

;; TERM 72: lctag_ok(vppn)
c_code local build_ref_72
	ret_reg &ref[72]
	call_c   build_ref_637()
	call_c   build_ref_71()
	call_c   Dyam_Create_Unary(&ref[637],&ref[71])
	move_ret ref[72]
	c_ret

;; TERM 107: '$VAR'(_F1, ['$RANGE',0,1000])
c_code local build_ref_107
	ret_reg &ref[107]
	call_c   build_ref_638()
	call_c   Dyam_Term_Start(&ref[638],3)
	call_c   Dyam_Term_Arg(V(31))
	call_c   Dyam_Term_Arg(N(0))
	call_c   Dyam_Term_Arg(N(1000))
	call_c   Dyam_Term_End()
	move_ret ref[107]
	c_ret

;; TERM 104: '$VAR'(_B1, ['$RANGE',0,1000])
c_code local build_ref_104
	ret_reg &ref[104]
	call_c   build_ref_638()
	call_c   Dyam_Term_Start(&ref[638],3)
	call_c   Dyam_Term_Arg(V(27))
	call_c   Dyam_Term_Arg(N(0))
	call_c   Dyam_Term_Arg(N(1000))
	call_c   Dyam_Term_End()
	move_ret ref[104]
	c_ret

;; TERM 111: lctag_map_reverse(tn1, s, subst)
c_code local build_ref_111
	ret_reg &ref[111]
	call_c   build_ref_633()
	call_c   build_ref_105()
	call_c   build_ref_632()
	call_c   build_ref_634()
	call_c   Dyam_Term_Start(&ref[633],3)
	call_c   Dyam_Term_Arg(&ref[105])
	call_c   Dyam_Term_Arg(&ref[632])
	call_c   Dyam_Term_Arg(&ref[634])
	call_c   Dyam_Term_End()
	move_ret ref[111]
	c_ret

;; TERM 110: lctag_map(s, subst, tn1)
c_code local build_ref_110
	ret_reg &ref[110]
	call_c   build_ref_635()
	call_c   build_ref_632()
	call_c   build_ref_634()
	call_c   build_ref_105()
	call_c   Dyam_Term_Start(&ref[635],3)
	call_c   Dyam_Term_Arg(&ref[632])
	call_c   Dyam_Term_Arg(&ref[634])
	call_c   Dyam_Term_Arg(&ref[105])
	call_c   Dyam_Term_End()
	move_ret ref[110]
	c_ret

;; TERM 109: loaded_tree(tn1)
c_code local build_ref_109
	ret_reg &ref[109]
	call_c   build_ref_636()
	call_c   build_ref_105()
	call_c   Dyam_Create_Unary(&ref[636],&ref[105])
	move_ret ref[109]
	c_ret

;; TERM 108: lctag_ok(tn1)
c_code local build_ref_108
	ret_reg &ref[108]
	call_c   build_ref_637()
	call_c   build_ref_105()
	call_c   Dyam_Create_Unary(&ref[637],&ref[105])
	move_ret ref[108]
	c_ret

;; TERM 538: '$CLOSURE'('$fun'(176, 0, 1169258940), '$TUPPLE'(35169792164988))
c_code local build_ref_538
	ret_reg &ref[538]
	call_c   build_ref_191()
	call_c   Dyam_Closure_Aux(fun176,&ref[191])
	move_ret ref[538]
	c_ret

c_code local build_seed_91
	ret_reg &seed[91]
	call_c   build_ref_183()
	call_c   build_ref_532()
	call_c   Dyam_Seed_Start(&ref[183],&ref[532],&ref[532],fun0,1)
	call_c   build_ref_533()
	call_c   Dyam_Seed_Add_Comp(&ref[533],&ref[532],0)
	call_c   Dyam_Seed_End()
	move_ret seed[91]
	c_ret

;; TERM 533: '*RFI*'{mspop=> 'call_v/2'(_F), mspop_adj=> 'call_v/2'(_E), current=> return(_K)} :> '$$HOLE$$'
c_code local build_ref_533
	ret_reg &ref[533]
	call_c   build_ref_532()
	call_c   Dyam_Create_Binary(I(9),&ref[532],I(7))
	move_ret ref[533]
	c_ret

;; TERM 532: '*RFI*'{mspop=> 'call_v/2'(_F), mspop_adj=> 'call_v/2'(_E), current=> return(_K)}
c_code local build_ref_532
	ret_reg &ref[532]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_604()
	call_c   Dyam_Create_Unary(&ref[604],V(5))
	move_ret R(0)
	call_c   Dyam_Create_Unary(&ref[604],V(4))
	move_ret R(1)
	call_c   build_ref_609()
	call_c   build_ref_515()
	call_c   Dyam_Term_Start(&ref[609],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(&ref[515])
	call_c   Dyam_Term_End()
	move_ret ref[532]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_92
	ret_reg &seed[92]
	call_c   build_ref_186()
	call_c   build_ref_536()
	call_c   Dyam_Seed_Start(&ref[186],&ref[536],&ref[536],fun9,1)
	call_c   build_ref_534()
	call_c   Dyam_Seed_Add_Comp(&ref[534],fun175,0)
	call_c   Dyam_Seed_End()
	move_ret seed[92]
	c_ret

;; TERM 534: '*RAI*'{xstart=> 'call_v/2'(_F), xend=> return(_K), aspop=> 'call_v/2'(_E), current=> return(_J)}
c_code local build_ref_534
	ret_reg &ref[534]
	call_c   Dyam_Pseudo_Choice(3)
	call_c   build_ref_604()
	call_c   Dyam_Create_Unary(&ref[604],V(5))
	move_ret R(0)
	call_c   Dyam_Create_Unary(&ref[604],V(4))
	move_ret R(1)
	call_c   build_ref_594()
	call_c   Dyam_Create_Unary(&ref[594],V(9))
	move_ret R(2)
	call_c   build_ref_598()
	call_c   build_ref_515()
	call_c   Dyam_Term_Start(&ref[598],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[515])
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(R(2))
	call_c   Dyam_Term_End()
	move_ret ref[534]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

pl_code local fun175
	call_c   build_ref_534()
	call_c   Dyam_Unify_Item(&ref[534])
	fail_ret
	pl_jump  Follow_Cont(V(2))

;; TERM 536: '*RAI*'{xstart=> 'call_v/2'(_F), xend=> return(_K), aspop=> 'call_v/2'(_E), current=> return(_J)} :> '$FUNTUPPLE'(toto_28, '$TUPPLE'(35169792164100))
c_code local build_ref_536
	ret_reg &ref[536]
	call_c   build_ref_534()
	call_c   build_ref_535()
	call_c   Dyam_Create_Binary(I(9),&ref[534],&ref[535])
	move_ret ref[536]
	c_ret

;; TERM 535: '$FUNTUPPLE'(toto_28, '$TUPPLE'(35169792164100))
c_code local build_ref_535
	ret_reg &ref[535]
	call_c   build_ref_640()
	call_c   Dyam_Create_Fun_With_Tupple(&ref[640],0,67108864)
	move_ret ref[535]
	c_ret

;; TERM 640: toto_28
c_code local build_ref_640
	ret_reg &ref[640]
	call_c   Dyam_Create_Atom("toto_28")
	move_ret ref[640]
	c_ret

;; TERM 186: '*RAI>*'
c_code local build_ref_186
	ret_reg &ref[186]
	call_c   Dyam_Create_Atom("*RAI>*")
	move_ret ref[186]
	c_ret

pl_code local fun16
	call_c   Dyam_Backptr_Trace(R(1),R(2))
	call_c   Dyam_Choice(fun15)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Subsume(R(0))
	fail_ret
	call_c   Dyam_Cut()
	pl_ret

long local pool_fun176[3]=[2,build_seed_91,build_seed_92]

pl_code local fun176
	call_c   Dyam_Pool(pool_fun176)
	call_c   Dyam_Allocate(0)
	pl_call  fun6(&seed[91],1)
	call_c   Dyam_Deallocate()
	pl_jump  fun16(&seed[92],2,V(6))

;; TERM 191: '$TUPPLE'(35169792164988)
c_code local build_ref_191
	ret_reg &ref[191]
	call_c   Dyam_Create_Simple_Tupple(0,97255424)
	move_ret ref[191]
	c_ret

;; TERM 512: '$CLOSURE'('$fun'(168, 0, 1169180080), '$TUPPLE'(35169792164988))
c_code local build_ref_512
	ret_reg &ref[512]
	call_c   build_ref_191()
	call_c   Dyam_Closure_Aux(fun168,&ref[191])
	move_ret ref[512]
	c_ret

c_code local build_seed_85
	ret_reg &seed[85]
	call_c   build_ref_183()
	call_c   build_ref_506()
	call_c   Dyam_Seed_Start(&ref[183],&ref[506],&ref[506],fun0,1)
	call_c   build_ref_507()
	call_c   Dyam_Seed_Add_Comp(&ref[507],&ref[506],0)
	call_c   Dyam_Seed_End()
	move_ret seed[85]
	c_ret

;; TERM 507: '*RFI*'{mspop=> 'call_s/2'(_F), mspop_adj=> 'call_s/2'(_E), current=> return(_K)} :> '$$HOLE$$'
c_code local build_ref_507
	ret_reg &ref[507]
	call_c   build_ref_506()
	call_c   Dyam_Create_Binary(I(9),&ref[506],I(7))
	move_ret ref[507]
	c_ret

;; TERM 506: '*RFI*'{mspop=> 'call_s/2'(_F), mspop_adj=> 'call_s/2'(_E), current=> return(_K)}
c_code local build_ref_506
	ret_reg &ref[506]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_600()
	call_c   Dyam_Create_Unary(&ref[600],V(5))
	move_ret R(0)
	call_c   Dyam_Create_Unary(&ref[600],V(4))
	move_ret R(1)
	call_c   build_ref_609()
	call_c   build_ref_515()
	call_c   Dyam_Term_Start(&ref[609],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(&ref[515])
	call_c   Dyam_Term_End()
	move_ret ref[506]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_86
	ret_reg &seed[86]
	call_c   build_ref_186()
	call_c   build_ref_510()
	call_c   Dyam_Seed_Start(&ref[186],&ref[510],&ref[510],fun9,1)
	call_c   build_ref_508()
	call_c   Dyam_Seed_Add_Comp(&ref[508],fun167,0)
	call_c   Dyam_Seed_End()
	move_ret seed[86]
	c_ret

;; TERM 508: '*RAI*'{xstart=> 'call_s/2'(_F), xend=> return(_K), aspop=> 'call_s/2'(_E), current=> return(_J)}
c_code local build_ref_508
	ret_reg &ref[508]
	call_c   Dyam_Pseudo_Choice(3)
	call_c   build_ref_600()
	call_c   Dyam_Create_Unary(&ref[600],V(5))
	move_ret R(0)
	call_c   Dyam_Create_Unary(&ref[600],V(4))
	move_ret R(1)
	call_c   build_ref_594()
	call_c   Dyam_Create_Unary(&ref[594],V(9))
	move_ret R(2)
	call_c   build_ref_598()
	call_c   build_ref_515()
	call_c   Dyam_Term_Start(&ref[598],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[515])
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(R(2))
	call_c   Dyam_Term_End()
	move_ret ref[508]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

pl_code local fun167
	call_c   build_ref_508()
	call_c   Dyam_Unify_Item(&ref[508])
	fail_ret
	pl_jump  Follow_Cont(V(2))

;; TERM 510: '*RAI*'{xstart=> 'call_s/2'(_F), xend=> return(_K), aspop=> 'call_s/2'(_E), current=> return(_J)} :> '$FUNTUPPLE'(toto_25, '$TUPPLE'(35169792164100))
c_code local build_ref_510
	ret_reg &ref[510]
	call_c   build_ref_508()
	call_c   build_ref_509()
	call_c   Dyam_Create_Binary(I(9),&ref[508],&ref[509])
	move_ret ref[510]
	c_ret

;; TERM 509: '$FUNTUPPLE'(toto_25, '$TUPPLE'(35169792164100))
c_code local build_ref_509
	ret_reg &ref[509]
	call_c   build_ref_641()
	call_c   Dyam_Create_Fun_With_Tupple(&ref[641],0,67108864)
	move_ret ref[509]
	c_ret

;; TERM 641: toto_25
c_code local build_ref_641
	ret_reg &ref[641]
	call_c   Dyam_Create_Atom("toto_25")
	move_ret ref[641]
	c_ret

long local pool_fun168[3]=[2,build_seed_85,build_seed_86]

pl_code local fun168
	call_c   Dyam_Pool(pool_fun168)
	call_c   Dyam_Allocate(0)
	pl_call  fun6(&seed[85],1)
	call_c   Dyam_Deallocate()
	pl_jump  fun16(&seed[86],2,V(6))

;; TERM 471: '$CLOSURE'('$fun'(152, 0, 1169050572), '$TUPPLE'(35169792164988))
c_code local build_ref_471
	ret_reg &ref[471]
	call_c   build_ref_191()
	call_c   Dyam_Closure_Aux(fun152,&ref[191])
	move_ret ref[471]
	c_ret

c_code local build_seed_80
	ret_reg &seed[80]
	call_c   build_ref_183()
	call_c   build_ref_465()
	call_c   Dyam_Seed_Start(&ref[183],&ref[465],&ref[465],fun0,1)
	call_c   build_ref_466()
	call_c   Dyam_Seed_Add_Comp(&ref[466],&ref[465],0)
	call_c   Dyam_Seed_End()
	move_ret seed[80]
	c_ret

;; TERM 466: '*RFI*'{mspop=> 'call_pp/2'(_F), mspop_adj=> 'call_pp/2'(_E), current=> return(_K)} :> '$$HOLE$$'
c_code local build_ref_466
	ret_reg &ref[466]
	call_c   build_ref_465()
	call_c   Dyam_Create_Binary(I(9),&ref[465],I(7))
	move_ret ref[466]
	c_ret

;; TERM 465: '*RFI*'{mspop=> 'call_pp/2'(_F), mspop_adj=> 'call_pp/2'(_E), current=> return(_K)}
c_code local build_ref_465
	ret_reg &ref[465]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_607()
	call_c   Dyam_Create_Unary(&ref[607],V(5))
	move_ret R(0)
	call_c   Dyam_Create_Unary(&ref[607],V(4))
	move_ret R(1)
	call_c   build_ref_609()
	call_c   build_ref_515()
	call_c   Dyam_Term_Start(&ref[609],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(&ref[515])
	call_c   Dyam_Term_End()
	move_ret ref[465]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_81
	ret_reg &seed[81]
	call_c   build_ref_186()
	call_c   build_ref_469()
	call_c   Dyam_Seed_Start(&ref[186],&ref[469],&ref[469],fun9,1)
	call_c   build_ref_467()
	call_c   Dyam_Seed_Add_Comp(&ref[467],fun151,0)
	call_c   Dyam_Seed_End()
	move_ret seed[81]
	c_ret

;; TERM 467: '*RAI*'{xstart=> 'call_pp/2'(_F), xend=> return(_K), aspop=> 'call_pp/2'(_E), current=> return(_J)}
c_code local build_ref_467
	ret_reg &ref[467]
	call_c   Dyam_Pseudo_Choice(3)
	call_c   build_ref_607()
	call_c   Dyam_Create_Unary(&ref[607],V(5))
	move_ret R(0)
	call_c   Dyam_Create_Unary(&ref[607],V(4))
	move_ret R(1)
	call_c   build_ref_594()
	call_c   Dyam_Create_Unary(&ref[594],V(9))
	move_ret R(2)
	call_c   build_ref_598()
	call_c   build_ref_515()
	call_c   Dyam_Term_Start(&ref[598],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[515])
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(R(2))
	call_c   Dyam_Term_End()
	move_ret ref[467]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

pl_code local fun151
	call_c   build_ref_467()
	call_c   Dyam_Unify_Item(&ref[467])
	fail_ret
	pl_jump  Follow_Cont(V(2))

;; TERM 469: '*RAI*'{xstart=> 'call_pp/2'(_F), xend=> return(_K), aspop=> 'call_pp/2'(_E), current=> return(_J)} :> '$FUNTUPPLE'(toto_23, '$TUPPLE'(35169792164100))
c_code local build_ref_469
	ret_reg &ref[469]
	call_c   build_ref_467()
	call_c   build_ref_468()
	call_c   Dyam_Create_Binary(I(9),&ref[467],&ref[468])
	move_ret ref[469]
	c_ret

;; TERM 468: '$FUNTUPPLE'(toto_23, '$TUPPLE'(35169792164100))
c_code local build_ref_468
	ret_reg &ref[468]
	call_c   build_ref_642()
	call_c   Dyam_Create_Fun_With_Tupple(&ref[642],0,67108864)
	move_ret ref[468]
	c_ret

;; TERM 642: toto_23
c_code local build_ref_642
	ret_reg &ref[642]
	call_c   Dyam_Create_Atom("toto_23")
	move_ret ref[642]
	c_ret

long local pool_fun152[3]=[2,build_seed_80,build_seed_81]

pl_code local fun152
	call_c   Dyam_Pool(pool_fun152)
	call_c   Dyam_Allocate(0)
	pl_call  fun6(&seed[80],1)
	call_c   Dyam_Deallocate()
	pl_jump  fun16(&seed[81],2,V(6))

;; TERM 453: '$CLOSURE'('$fun'(145, 0, 1168983124), '$TUPPLE'(35169792164988))
c_code local build_ref_453
	ret_reg &ref[453]
	call_c   build_ref_191()
	call_c   Dyam_Closure_Aux(fun145,&ref[191])
	move_ret ref[453]
	c_ret

c_code local build_seed_74
	ret_reg &seed[74]
	call_c   build_ref_183()
	call_c   build_ref_447()
	call_c   Dyam_Seed_Start(&ref[183],&ref[447],&ref[447],fun0,1)
	call_c   build_ref_448()
	call_c   Dyam_Seed_Add_Comp(&ref[448],&ref[447],0)
	call_c   Dyam_Seed_End()
	move_ret seed[74]
	c_ret

;; TERM 448: '*RFI*'{mspop=> 'call_vp/2'(_F), mspop_adj=> 'call_vp/2'(_E), current=> return(_K)} :> '$$HOLE$$'
c_code local build_ref_448
	ret_reg &ref[448]
	call_c   build_ref_447()
	call_c   Dyam_Create_Binary(I(9),&ref[447],I(7))
	move_ret ref[448]
	c_ret

;; TERM 447: '*RFI*'{mspop=> 'call_vp/2'(_F), mspop_adj=> 'call_vp/2'(_E), current=> return(_K)}
c_code local build_ref_447
	ret_reg &ref[447]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_599()
	call_c   Dyam_Create_Unary(&ref[599],V(5))
	move_ret R(0)
	call_c   Dyam_Create_Unary(&ref[599],V(4))
	move_ret R(1)
	call_c   build_ref_609()
	call_c   build_ref_515()
	call_c   Dyam_Term_Start(&ref[609],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(&ref[515])
	call_c   Dyam_Term_End()
	move_ret ref[447]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_75
	ret_reg &seed[75]
	call_c   build_ref_186()
	call_c   build_ref_451()
	call_c   Dyam_Seed_Start(&ref[186],&ref[451],&ref[451],fun9,1)
	call_c   build_ref_449()
	call_c   Dyam_Seed_Add_Comp(&ref[449],fun144,0)
	call_c   Dyam_Seed_End()
	move_ret seed[75]
	c_ret

;; TERM 449: '*RAI*'{xstart=> 'call_vp/2'(_F), xend=> return(_K), aspop=> 'call_vp/2'(_E), current=> return(_J)}
c_code local build_ref_449
	ret_reg &ref[449]
	call_c   Dyam_Pseudo_Choice(3)
	call_c   build_ref_599()
	call_c   Dyam_Create_Unary(&ref[599],V(5))
	move_ret R(0)
	call_c   Dyam_Create_Unary(&ref[599],V(4))
	move_ret R(1)
	call_c   build_ref_594()
	call_c   Dyam_Create_Unary(&ref[594],V(9))
	move_ret R(2)
	call_c   build_ref_598()
	call_c   build_ref_515()
	call_c   Dyam_Term_Start(&ref[598],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[515])
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(R(2))
	call_c   Dyam_Term_End()
	move_ret ref[449]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

pl_code local fun144
	call_c   build_ref_449()
	call_c   Dyam_Unify_Item(&ref[449])
	fail_ret
	pl_jump  Follow_Cont(V(2))

;; TERM 451: '*RAI*'{xstart=> 'call_vp/2'(_F), xend=> return(_K), aspop=> 'call_vp/2'(_E), current=> return(_J)} :> '$FUNTUPPLE'(toto_20, '$TUPPLE'(35169792164100))
c_code local build_ref_451
	ret_reg &ref[451]
	call_c   build_ref_449()
	call_c   build_ref_450()
	call_c   Dyam_Create_Binary(I(9),&ref[449],&ref[450])
	move_ret ref[451]
	c_ret

;; TERM 450: '$FUNTUPPLE'(toto_20, '$TUPPLE'(35169792164100))
c_code local build_ref_450
	ret_reg &ref[450]
	call_c   build_ref_643()
	call_c   Dyam_Create_Fun_With_Tupple(&ref[643],0,67108864)
	move_ret ref[450]
	c_ret

;; TERM 643: toto_20
c_code local build_ref_643
	ret_reg &ref[643]
	call_c   Dyam_Create_Atom("toto_20")
	move_ret ref[643]
	c_ret

long local pool_fun145[3]=[2,build_seed_74,build_seed_75]

pl_code local fun145
	call_c   Dyam_Pool(pool_fun145)
	call_c   Dyam_Allocate(0)
	pl_call  fun6(&seed[74],1)
	call_c   Dyam_Deallocate()
	pl_jump  fun16(&seed[75],2,V(6))

;; TERM 403: '$CLOSURE'('$fun'(127, 0, 1168831124), '$TUPPLE'(35169792164988))
c_code local build_ref_403
	ret_reg &ref[403]
	call_c   build_ref_191()
	call_c   Dyam_Closure_Aux(fun127,&ref[191])
	move_ret ref[403]
	c_ret

c_code local build_seed_67
	ret_reg &seed[67]
	call_c   build_ref_183()
	call_c   build_ref_397()
	call_c   Dyam_Seed_Start(&ref[183],&ref[397],&ref[397],fun0,1)
	call_c   build_ref_398()
	call_c   Dyam_Seed_Add_Comp(&ref[398],&ref[397],0)
	call_c   Dyam_Seed_End()
	move_ret seed[67]
	c_ret

;; TERM 398: '*RFI*'{mspop=> 'call_p/2'(_F), mspop_adj=> 'call_p/2'(_E), current=> return(_K)} :> '$$HOLE$$'
c_code local build_ref_398
	ret_reg &ref[398]
	call_c   build_ref_397()
	call_c   Dyam_Create_Binary(I(9),&ref[397],I(7))
	move_ret ref[398]
	c_ret

;; TERM 397: '*RFI*'{mspop=> 'call_p/2'(_F), mspop_adj=> 'call_p/2'(_E), current=> return(_K)}
c_code local build_ref_397
	ret_reg &ref[397]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_613()
	call_c   Dyam_Create_Unary(&ref[613],V(5))
	move_ret R(0)
	call_c   Dyam_Create_Unary(&ref[613],V(4))
	move_ret R(1)
	call_c   build_ref_609()
	call_c   build_ref_515()
	call_c   Dyam_Term_Start(&ref[609],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(&ref[515])
	call_c   Dyam_Term_End()
	move_ret ref[397]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_68
	ret_reg &seed[68]
	call_c   build_ref_186()
	call_c   build_ref_401()
	call_c   Dyam_Seed_Start(&ref[186],&ref[401],&ref[401],fun9,1)
	call_c   build_ref_399()
	call_c   Dyam_Seed_Add_Comp(&ref[399],fun126,0)
	call_c   Dyam_Seed_End()
	move_ret seed[68]
	c_ret

;; TERM 399: '*RAI*'{xstart=> 'call_p/2'(_F), xend=> return(_K), aspop=> 'call_p/2'(_E), current=> return(_J)}
c_code local build_ref_399
	ret_reg &ref[399]
	call_c   Dyam_Pseudo_Choice(3)
	call_c   build_ref_613()
	call_c   Dyam_Create_Unary(&ref[613],V(5))
	move_ret R(0)
	call_c   Dyam_Create_Unary(&ref[613],V(4))
	move_ret R(1)
	call_c   build_ref_594()
	call_c   Dyam_Create_Unary(&ref[594],V(9))
	move_ret R(2)
	call_c   build_ref_598()
	call_c   build_ref_515()
	call_c   Dyam_Term_Start(&ref[598],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[515])
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(R(2))
	call_c   Dyam_Term_End()
	move_ret ref[399]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

pl_code local fun126
	call_c   build_ref_399()
	call_c   Dyam_Unify_Item(&ref[399])
	fail_ret
	pl_jump  Follow_Cont(V(2))

;; TERM 401: '*RAI*'{xstart=> 'call_p/2'(_F), xend=> return(_K), aspop=> 'call_p/2'(_E), current=> return(_J)} :> '$FUNTUPPLE'(toto_17, '$TUPPLE'(35169792164100))
c_code local build_ref_401
	ret_reg &ref[401]
	call_c   build_ref_399()
	call_c   build_ref_400()
	call_c   Dyam_Create_Binary(I(9),&ref[399],&ref[400])
	move_ret ref[401]
	c_ret

;; TERM 400: '$FUNTUPPLE'(toto_17, '$TUPPLE'(35169792164100))
c_code local build_ref_400
	ret_reg &ref[400]
	call_c   build_ref_644()
	call_c   Dyam_Create_Fun_With_Tupple(&ref[644],0,67108864)
	move_ret ref[400]
	c_ret

;; TERM 644: toto_17
c_code local build_ref_644
	ret_reg &ref[644]
	call_c   Dyam_Create_Atom("toto_17")
	move_ret ref[644]
	c_ret

long local pool_fun127[3]=[2,build_seed_67,build_seed_68]

pl_code local fun127
	call_c   Dyam_Pool(pool_fun127)
	call_c   Dyam_Allocate(0)
	pl_call  fun6(&seed[67],1)
	call_c   Dyam_Deallocate()
	pl_jump  fun16(&seed[68],2,V(6))

;; TERM 312: '$CLOSURE'('$fun'(65, 0, 1168385684), '$TUPPLE'(35169792164988))
c_code local build_ref_312
	ret_reg &ref[312]
	call_c   build_ref_191()
	call_c   Dyam_Closure_Aux(fun65,&ref[191])
	move_ret ref[312]
	c_ret

c_code local build_seed_57
	ret_reg &seed[57]
	call_c   build_ref_183()
	call_c   build_ref_306()
	call_c   Dyam_Seed_Start(&ref[183],&ref[306],&ref[306],fun0,1)
	call_c   build_ref_307()
	call_c   Dyam_Seed_Add_Comp(&ref[307],&ref[306],0)
	call_c   Dyam_Seed_End()
	move_ret seed[57]
	c_ret

;; TERM 307: '*RFI*'{mspop=> 'call_a/2'(_F), mspop_adj=> 'call_a/2'(_E), current=> return(_K)} :> '$$HOLE$$'
c_code local build_ref_307
	ret_reg &ref[307]
	call_c   build_ref_306()
	call_c   Dyam_Create_Binary(I(9),&ref[306],I(7))
	move_ret ref[307]
	c_ret

;; TERM 306: '*RFI*'{mspop=> 'call_a/2'(_F), mspop_adj=> 'call_a/2'(_E), current=> return(_K)}
c_code local build_ref_306
	ret_reg &ref[306]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_617()
	call_c   Dyam_Create_Unary(&ref[617],V(5))
	move_ret R(0)
	call_c   Dyam_Create_Unary(&ref[617],V(4))
	move_ret R(1)
	call_c   build_ref_609()
	call_c   build_ref_515()
	call_c   Dyam_Term_Start(&ref[609],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(&ref[515])
	call_c   Dyam_Term_End()
	move_ret ref[306]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_58
	ret_reg &seed[58]
	call_c   build_ref_186()
	call_c   build_ref_310()
	call_c   Dyam_Seed_Start(&ref[186],&ref[310],&ref[310],fun9,1)
	call_c   build_ref_308()
	call_c   Dyam_Seed_Add_Comp(&ref[308],fun64,0)
	call_c   Dyam_Seed_End()
	move_ret seed[58]
	c_ret

;; TERM 308: '*RAI*'{xstart=> 'call_a/2'(_F), xend=> return(_K), aspop=> 'call_a/2'(_E), current=> return(_J)}
c_code local build_ref_308
	ret_reg &ref[308]
	call_c   Dyam_Pseudo_Choice(3)
	call_c   build_ref_617()
	call_c   Dyam_Create_Unary(&ref[617],V(5))
	move_ret R(0)
	call_c   Dyam_Create_Unary(&ref[617],V(4))
	move_ret R(1)
	call_c   build_ref_594()
	call_c   Dyam_Create_Unary(&ref[594],V(9))
	move_ret R(2)
	call_c   build_ref_598()
	call_c   build_ref_515()
	call_c   Dyam_Term_Start(&ref[598],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[515])
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(R(2))
	call_c   Dyam_Term_End()
	move_ret ref[308]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

pl_code local fun64
	call_c   build_ref_308()
	call_c   Dyam_Unify_Item(&ref[308])
	fail_ret
	pl_jump  Follow_Cont(V(2))

;; TERM 310: '*RAI*'{xstart=> 'call_a/2'(_F), xend=> return(_K), aspop=> 'call_a/2'(_E), current=> return(_J)} :> '$FUNTUPPLE'(toto_13, '$TUPPLE'(35169792164100))
c_code local build_ref_310
	ret_reg &ref[310]
	call_c   build_ref_308()
	call_c   build_ref_309()
	call_c   Dyam_Create_Binary(I(9),&ref[308],&ref[309])
	move_ret ref[310]
	c_ret

;; TERM 309: '$FUNTUPPLE'(toto_13, '$TUPPLE'(35169792164100))
c_code local build_ref_309
	ret_reg &ref[309]
	call_c   build_ref_645()
	call_c   Dyam_Create_Fun_With_Tupple(&ref[645],0,67108864)
	move_ret ref[309]
	c_ret

;; TERM 645: toto_13
c_code local build_ref_645
	ret_reg &ref[645]
	call_c   Dyam_Create_Atom("toto_13")
	move_ret ref[645]
	c_ret

long local pool_fun65[3]=[2,build_seed_57,build_seed_58]

pl_code local fun65
	call_c   Dyam_Pool(pool_fun65)
	call_c   Dyam_Allocate(0)
	pl_call  fun6(&seed[57],1)
	call_c   Dyam_Deallocate()
	pl_jump  fun16(&seed[58],2,V(6))

;; TERM 292: '$CLOSURE'('$fun'(58, 0, 1168311004), '$TUPPLE'(35169792164988))
c_code local build_ref_292
	ret_reg &ref[292]
	call_c   build_ref_191()
	call_c   Dyam_Closure_Aux(fun58,&ref[191])
	move_ret ref[292]
	c_ret

c_code local build_seed_51
	ret_reg &seed[51]
	call_c   build_ref_183()
	call_c   build_ref_286()
	call_c   Dyam_Seed_Start(&ref[183],&ref[286],&ref[286],fun0,1)
	call_c   build_ref_287()
	call_c   Dyam_Seed_Add_Comp(&ref[287],&ref[286],0)
	call_c   Dyam_Seed_End()
	move_ret seed[51]
	c_ret

;; TERM 287: '*RFI*'{mspop=> 'call_n/2'(_F), mspop_adj=> 'call_n/2'(_E), current=> return(_K)} :> '$$HOLE$$'
c_code local build_ref_287
	ret_reg &ref[287]
	call_c   build_ref_286()
	call_c   Dyam_Create_Binary(I(9),&ref[286],I(7))
	move_ret ref[287]
	c_ret

;; TERM 286: '*RFI*'{mspop=> 'call_n/2'(_F), mspop_adj=> 'call_n/2'(_E), current=> return(_K)}
c_code local build_ref_286
	ret_reg &ref[286]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_597()
	call_c   Dyam_Create_Unary(&ref[597],V(5))
	move_ret R(0)
	call_c   Dyam_Create_Unary(&ref[597],V(4))
	move_ret R(1)
	call_c   build_ref_609()
	call_c   build_ref_515()
	call_c   Dyam_Term_Start(&ref[609],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(&ref[515])
	call_c   Dyam_Term_End()
	move_ret ref[286]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_52
	ret_reg &seed[52]
	call_c   build_ref_186()
	call_c   build_ref_290()
	call_c   Dyam_Seed_Start(&ref[186],&ref[290],&ref[290],fun9,1)
	call_c   build_ref_288()
	call_c   Dyam_Seed_Add_Comp(&ref[288],fun57,0)
	call_c   Dyam_Seed_End()
	move_ret seed[52]
	c_ret

;; TERM 288: '*RAI*'{xstart=> 'call_n/2'(_F), xend=> return(_K), aspop=> 'call_n/2'(_E), current=> return(_J)}
c_code local build_ref_288
	ret_reg &ref[288]
	call_c   Dyam_Pseudo_Choice(3)
	call_c   build_ref_597()
	call_c   Dyam_Create_Unary(&ref[597],V(5))
	move_ret R(0)
	call_c   Dyam_Create_Unary(&ref[597],V(4))
	move_ret R(1)
	call_c   build_ref_594()
	call_c   Dyam_Create_Unary(&ref[594],V(9))
	move_ret R(2)
	call_c   build_ref_598()
	call_c   build_ref_515()
	call_c   Dyam_Term_Start(&ref[598],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[515])
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(R(2))
	call_c   Dyam_Term_End()
	move_ret ref[288]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

pl_code local fun57
	call_c   build_ref_288()
	call_c   Dyam_Unify_Item(&ref[288])
	fail_ret
	pl_jump  Follow_Cont(V(2))

;; TERM 290: '*RAI*'{xstart=> 'call_n/2'(_F), xend=> return(_K), aspop=> 'call_n/2'(_E), current=> return(_J)} :> '$FUNTUPPLE'(toto_10, '$TUPPLE'(35169792164100))
c_code local build_ref_290
	ret_reg &ref[290]
	call_c   build_ref_288()
	call_c   build_ref_289()
	call_c   Dyam_Create_Binary(I(9),&ref[288],&ref[289])
	move_ret ref[290]
	c_ret

;; TERM 289: '$FUNTUPPLE'(toto_10, '$TUPPLE'(35169792164100))
c_code local build_ref_289
	ret_reg &ref[289]
	call_c   build_ref_646()
	call_c   Dyam_Create_Fun_With_Tupple(&ref[646],0,67108864)
	move_ret ref[289]
	c_ret

;; TERM 646: toto_10
c_code local build_ref_646
	ret_reg &ref[646]
	call_c   Dyam_Create_Atom("toto_10")
	move_ret ref[646]
	c_ret

long local pool_fun58[3]=[2,build_seed_51,build_seed_52]

pl_code local fun58
	call_c   Dyam_Pool(pool_fun58)
	call_c   Dyam_Allocate(0)
	pl_call  fun6(&seed[51],1)
	call_c   Dyam_Deallocate()
	pl_jump  fun16(&seed[52],2,V(6))

;; TERM 258: '$CLOSURE'('$fun'(46, 0, 1168204884), '$TUPPLE'(35169792164988))
c_code local build_ref_258
	ret_reg &ref[258]
	call_c   build_ref_191()
	call_c   Dyam_Closure_Aux(fun46,&ref[191])
	move_ret ref[258]
	c_ret

c_code local build_seed_46
	ret_reg &seed[46]
	call_c   build_ref_183()
	call_c   build_ref_252()
	call_c   Dyam_Seed_Start(&ref[183],&ref[252],&ref[252],fun0,1)
	call_c   build_ref_253()
	call_c   Dyam_Seed_Add_Comp(&ref[253],&ref[252],0)
	call_c   Dyam_Seed_End()
	move_ret seed[46]
	c_ret

;; TERM 253: '*RFI*'{mspop=> 'call_pn/2'(_F), mspop_adj=> 'call_pn/2'(_E), current=> return(_K)} :> '$$HOLE$$'
c_code local build_ref_253
	ret_reg &ref[253]
	call_c   build_ref_252()
	call_c   Dyam_Create_Binary(I(9),&ref[252],I(7))
	move_ret ref[253]
	c_ret

;; TERM 252: '*RFI*'{mspop=> 'call_pn/2'(_F), mspop_adj=> 'call_pn/2'(_E), current=> return(_K)}
c_code local build_ref_252
	ret_reg &ref[252]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_621()
	call_c   Dyam_Create_Unary(&ref[621],V(5))
	move_ret R(0)
	call_c   Dyam_Create_Unary(&ref[621],V(4))
	move_ret R(1)
	call_c   build_ref_609()
	call_c   build_ref_515()
	call_c   Dyam_Term_Start(&ref[609],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(&ref[515])
	call_c   Dyam_Term_End()
	move_ret ref[252]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_47
	ret_reg &seed[47]
	call_c   build_ref_186()
	call_c   build_ref_256()
	call_c   Dyam_Seed_Start(&ref[186],&ref[256],&ref[256],fun9,1)
	call_c   build_ref_254()
	call_c   Dyam_Seed_Add_Comp(&ref[254],fun45,0)
	call_c   Dyam_Seed_End()
	move_ret seed[47]
	c_ret

;; TERM 254: '*RAI*'{xstart=> 'call_pn/2'(_F), xend=> return(_K), aspop=> 'call_pn/2'(_E), current=> return(_J)}
c_code local build_ref_254
	ret_reg &ref[254]
	call_c   Dyam_Pseudo_Choice(3)
	call_c   build_ref_621()
	call_c   Dyam_Create_Unary(&ref[621],V(5))
	move_ret R(0)
	call_c   Dyam_Create_Unary(&ref[621],V(4))
	move_ret R(1)
	call_c   build_ref_594()
	call_c   Dyam_Create_Unary(&ref[594],V(9))
	move_ret R(2)
	call_c   build_ref_598()
	call_c   build_ref_515()
	call_c   Dyam_Term_Start(&ref[598],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[515])
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(R(2))
	call_c   Dyam_Term_End()
	move_ret ref[254]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

pl_code local fun45
	call_c   build_ref_254()
	call_c   Dyam_Unify_Item(&ref[254])
	fail_ret
	pl_jump  Follow_Cont(V(2))

;; TERM 256: '*RAI*'{xstart=> 'call_pn/2'(_F), xend=> return(_K), aspop=> 'call_pn/2'(_E), current=> return(_J)} :> '$FUNTUPPLE'(toto_8, '$TUPPLE'(35169792164100))
c_code local build_ref_256
	ret_reg &ref[256]
	call_c   build_ref_254()
	call_c   build_ref_255()
	call_c   Dyam_Create_Binary(I(9),&ref[254],&ref[255])
	move_ret ref[256]
	c_ret

;; TERM 255: '$FUNTUPPLE'(toto_8, '$TUPPLE'(35169792164100))
c_code local build_ref_255
	ret_reg &ref[255]
	call_c   build_ref_647()
	call_c   Dyam_Create_Fun_With_Tupple(&ref[647],0,67108864)
	move_ret ref[255]
	c_ret

;; TERM 647: toto_8
c_code local build_ref_647
	ret_reg &ref[647]
	call_c   Dyam_Create_Atom("toto_8")
	move_ret ref[647]
	c_ret

long local pool_fun46[3]=[2,build_seed_46,build_seed_47]

pl_code local fun46
	call_c   Dyam_Pool(pool_fun46)
	call_c   Dyam_Allocate(0)
	pl_call  fun6(&seed[46],1)
	call_c   Dyam_Deallocate()
	pl_jump  fun16(&seed[47],2,V(6))

;; TERM 245: '$CLOSURE'('$fun'(41, 0, 1168160128), '$TUPPLE'(35169792164988))
c_code local build_ref_245
	ret_reg &ref[245]
	call_c   build_ref_191()
	call_c   Dyam_Closure_Aux(fun41,&ref[191])
	move_ret ref[245]
	c_ret

c_code local build_seed_42
	ret_reg &seed[42]
	call_c   build_ref_183()
	call_c   build_ref_239()
	call_c   Dyam_Seed_Start(&ref[183],&ref[239],&ref[239],fun0,1)
	call_c   build_ref_240()
	call_c   Dyam_Seed_Add_Comp(&ref[240],&ref[239],0)
	call_c   Dyam_Seed_End()
	move_ret seed[42]
	c_ret

;; TERM 240: '*RFI*'{mspop=> 'call_np/2'(_F), mspop_adj=> 'call_np/2'(_E), current=> return(_K)} :> '$$HOLE$$'
c_code local build_ref_240
	ret_reg &ref[240]
	call_c   build_ref_239()
	call_c   Dyam_Create_Binary(I(9),&ref[239],I(7))
	move_ret ref[240]
	c_ret

;; TERM 239: '*RFI*'{mspop=> 'call_np/2'(_F), mspop_adj=> 'call_np/2'(_E), current=> return(_K)}
c_code local build_ref_239
	ret_reg &ref[239]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_595()
	call_c   Dyam_Create_Unary(&ref[595],V(5))
	move_ret R(0)
	call_c   Dyam_Create_Unary(&ref[595],V(4))
	move_ret R(1)
	call_c   build_ref_609()
	call_c   build_ref_515()
	call_c   Dyam_Term_Start(&ref[609],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(&ref[515])
	call_c   Dyam_Term_End()
	move_ret ref[239]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_43
	ret_reg &seed[43]
	call_c   build_ref_186()
	call_c   build_ref_243()
	call_c   Dyam_Seed_Start(&ref[186],&ref[243],&ref[243],fun9,1)
	call_c   build_ref_241()
	call_c   Dyam_Seed_Add_Comp(&ref[241],fun40,0)
	call_c   Dyam_Seed_End()
	move_ret seed[43]
	c_ret

;; TERM 241: '*RAI*'{xstart=> 'call_np/2'(_F), xend=> return(_K), aspop=> 'call_np/2'(_E), current=> return(_J)}
c_code local build_ref_241
	ret_reg &ref[241]
	call_c   Dyam_Pseudo_Choice(3)
	call_c   build_ref_595()
	call_c   Dyam_Create_Unary(&ref[595],V(5))
	move_ret R(0)
	call_c   Dyam_Create_Unary(&ref[595],V(4))
	move_ret R(1)
	call_c   build_ref_594()
	call_c   Dyam_Create_Unary(&ref[594],V(9))
	move_ret R(2)
	call_c   build_ref_598()
	call_c   build_ref_515()
	call_c   Dyam_Term_Start(&ref[598],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[515])
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(R(2))
	call_c   Dyam_Term_End()
	move_ret ref[241]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

pl_code local fun40
	call_c   build_ref_241()
	call_c   Dyam_Unify_Item(&ref[241])
	fail_ret
	pl_jump  Follow_Cont(V(2))

;; TERM 243: '*RAI*'{xstart=> 'call_np/2'(_F), xend=> return(_K), aspop=> 'call_np/2'(_E), current=> return(_J)} :> '$FUNTUPPLE'(toto_6, '$TUPPLE'(35169792164100))
c_code local build_ref_243
	ret_reg &ref[243]
	call_c   build_ref_241()
	call_c   build_ref_242()
	call_c   Dyam_Create_Binary(I(9),&ref[241],&ref[242])
	move_ret ref[243]
	c_ret

;; TERM 242: '$FUNTUPPLE'(toto_6, '$TUPPLE'(35169792164100))
c_code local build_ref_242
	ret_reg &ref[242]
	call_c   build_ref_648()
	call_c   Dyam_Create_Fun_With_Tupple(&ref[648],0,67108864)
	move_ret ref[242]
	c_ret

;; TERM 648: toto_6
c_code local build_ref_648
	ret_reg &ref[648]
	call_c   Dyam_Create_Atom("toto_6")
	move_ret ref[648]
	c_ret

long local pool_fun41[3]=[2,build_seed_42,build_seed_43]

pl_code local fun41
	call_c   Dyam_Pool(pool_fun41)
	call_c   Dyam_Allocate(0)
	pl_call  fun6(&seed[42],1)
	call_c   Dyam_Deallocate()
	pl_jump  fun16(&seed[43],2,V(6))

;; TERM 192: '$CLOSURE'('$fun'(24, 0, 1167992324), '$TUPPLE'(35169792164988))
c_code local build_ref_192
	ret_reg &ref[192]
	call_c   build_ref_191()
	call_c   Dyam_Closure_Aux(fun24,&ref[191])
	move_ret ref[192]
	c_ret

c_code local build_seed_33
	ret_reg &seed[33]
	call_c   build_ref_183()
	call_c   build_ref_184()
	call_c   Dyam_Seed_Start(&ref[183],&ref[184],&ref[184],fun0,1)
	call_c   build_ref_185()
	call_c   Dyam_Seed_Add_Comp(&ref[185],&ref[184],0)
	call_c   Dyam_Seed_End()
	move_ret seed[33]
	c_ret

;; TERM 185: '*RFI*'{mspop=> 'call_d/2'(_F), mspop_adj=> 'call_d/2'(_E), current=> return(_K)} :> '$$HOLE$$'
c_code local build_ref_185
	ret_reg &ref[185]
	call_c   build_ref_184()
	call_c   Dyam_Create_Binary(I(9),&ref[184],I(7))
	move_ret ref[185]
	c_ret

;; TERM 184: '*RFI*'{mspop=> 'call_d/2'(_F), mspop_adj=> 'call_d/2'(_E), current=> return(_K)}
c_code local build_ref_184
	ret_reg &ref[184]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_591()
	call_c   Dyam_Create_Unary(&ref[591],V(5))
	move_ret R(0)
	call_c   Dyam_Create_Unary(&ref[591],V(4))
	move_ret R(1)
	call_c   build_ref_609()
	call_c   build_ref_515()
	call_c   Dyam_Term_Start(&ref[609],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(&ref[515])
	call_c   Dyam_Term_End()
	move_ret ref[184]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_34
	ret_reg &seed[34]
	call_c   build_ref_186()
	call_c   build_ref_189()
	call_c   Dyam_Seed_Start(&ref[186],&ref[189],&ref[189],fun9,1)
	call_c   build_ref_187()
	call_c   Dyam_Seed_Add_Comp(&ref[187],fun23,0)
	call_c   Dyam_Seed_End()
	move_ret seed[34]
	c_ret

;; TERM 187: '*RAI*'{xstart=> 'call_d/2'(_F), xend=> return(_K), aspop=> 'call_d/2'(_E), current=> return(_J)}
c_code local build_ref_187
	ret_reg &ref[187]
	call_c   Dyam_Pseudo_Choice(3)
	call_c   build_ref_591()
	call_c   Dyam_Create_Unary(&ref[591],V(5))
	move_ret R(0)
	call_c   Dyam_Create_Unary(&ref[591],V(4))
	move_ret R(1)
	call_c   build_ref_594()
	call_c   Dyam_Create_Unary(&ref[594],V(9))
	move_ret R(2)
	call_c   build_ref_598()
	call_c   build_ref_515()
	call_c   Dyam_Term_Start(&ref[598],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[515])
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(R(2))
	call_c   Dyam_Term_End()
	move_ret ref[187]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

pl_code local fun23
	call_c   build_ref_187()
	call_c   Dyam_Unify_Item(&ref[187])
	fail_ret
	pl_jump  Follow_Cont(V(2))

;; TERM 189: '*RAI*'{xstart=> 'call_d/2'(_F), xend=> return(_K), aspop=> 'call_d/2'(_E), current=> return(_J)} :> '$FUNTUPPLE'(toto_2, '$TUPPLE'(35169792164100))
c_code local build_ref_189
	ret_reg &ref[189]
	call_c   build_ref_187()
	call_c   build_ref_188()
	call_c   Dyam_Create_Binary(I(9),&ref[187],&ref[188])
	move_ret ref[189]
	c_ret

;; TERM 188: '$FUNTUPPLE'(toto_2, '$TUPPLE'(35169792164100))
c_code local build_ref_188
	ret_reg &ref[188]
	call_c   build_ref_649()
	call_c   Dyam_Create_Fun_With_Tupple(&ref[649],0,67108864)
	move_ret ref[188]
	c_ret

;; TERM 649: toto_2
c_code local build_ref_649
	ret_reg &ref[649]
	call_c   Dyam_Create_Atom("toto_2")
	move_ret ref[649]
	c_ret

long local pool_fun24[3]=[2,build_seed_33,build_seed_34]

pl_code local fun24
	call_c   Dyam_Pool(pool_fun24)
	call_c   Dyam_Allocate(0)
	pl_call  fun6(&seed[33],1)
	call_c   Dyam_Deallocate()
	pl_jump  fun16(&seed[34],2,V(6))

;; TERM 157: token_cat(_D, _C)
c_code local build_ref_157
	ret_reg &ref[157]
	call_c   build_ref_568()
	call_c   Dyam_Create_Binary(&ref[568],V(3),V(2))
	move_ret ref[157]
	c_ret

;; TERM 156: 'C'(_E, _C, _F)
c_code local build_ref_156
	ret_reg &ref[156]
	call_c   build_ref_650()
	call_c   Dyam_Term_Start(&ref[650],3)
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[156]
	c_ret

;; TERM 650: 'C'
c_code local build_ref_650
	ret_reg &ref[650]
	call_c   Dyam_Create_Atom("C")
	move_ret ref[650]
	c_ret

pl_code local fun2
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Object(R(0))
	call_c   Dyam_Tabule()
	pl_ret

pl_code local fun3
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Choice(fun2)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Subsume(R(0))
	fail_ret
	call_c   Dyam_Cut()
	pl_ret

pl_code local fun98
	call_c   Dyam_Remove_Choice()
fun95:
	call_c   Dyam_Cut()
	call_c   DYAM_evpred_assert_1(&ref[41])
	call_c   DYAM_evpred_assert_1(&ref[42])
	call_c   DYAM_evpred_assert_1(&ref[43])
	call_c   DYAM_evpred_assert_1(&ref[44])
fun94:
	call_c   Dyam_Cut()
fun93:
	pl_call  fun14(&seed[6],0)
	pl_fail




pl_code local fun102
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun101)
	call_c   Dyam_Choice(fun99)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Choice(fun99)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Choice(fun98)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Choice(fun97)
	call_c   Dyam_Set_Cut()
	move     &ref[39], R(0)
	move     0, R(1)
	move     &ref[39], R(2)
	move     0, R(3)
	move     &ref[39], R(4)
	move     0, R(5)
	move     V(3), R(6)
	move     S(5), R(7)
	move     &ref[39], R(8)
	move     0, R(9)
	move     &ref[40], R(10)
	move     S(5), R(11)
	move     V(16), R(12)
	move     S(5), R(13)
	pl_call  pred_tag_family_load_7()
	call_c   DYAM_Term_Range_3(V(16),N(1000),&ref[19])
	fail_ret
	call_c   Dyam_Cut()
	pl_fail

pl_code local fun106
	call_c   Dyam_Remove_Choice()
fun105:
	call_c   Dyam_Cut()
	call_c   DYAM_evpred_assert_1(&ref[82])
	call_c   DYAM_evpred_assert_1(&ref[83])
	call_c   DYAM_evpred_assert_1(&ref[84])
	call_c   DYAM_evpred_assert_1(&ref[85])
fun104:
	call_c   Dyam_Cut()
fun103:
	pl_call  fun14(&seed[9],0)
	pl_fail




pl_code local fun107
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun101)
	call_c   Dyam_Choice(fun99)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Choice(fun99)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Choice(fun106)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Choice(fun97)
	call_c   Dyam_Set_Cut()
	move     &ref[80], R(0)
	move     0, R(1)
	move     &ref[81], R(2)
	move     0, R(3)
	move     &ref[81], R(4)
	move     0, R(5)
	move     V(3), R(6)
	move     S(5), R(7)
	move     &ref[81], R(8)
	move     0, R(9)
	move     &ref[40], R(10)
	move     S(5), R(11)
	move     V(16), R(12)
	move     S(5), R(13)
	pl_call  pred_tag_family_load_7()
	call_c   DYAM_Term_Range_3(V(16),N(1000),&ref[19])
	fail_ret
	call_c   Dyam_Cut()
	pl_fail

pl_code local fun111
	call_c   Dyam_Remove_Choice()
fun110:
	call_c   Dyam_Cut()
	call_c   DYAM_evpred_assert_1(&ref[90])
	call_c   DYAM_evpred_assert_1(&ref[91])
	call_c   DYAM_evpred_assert_1(&ref[92])
	call_c   DYAM_evpred_assert_1(&ref[93])
fun109:
	call_c   Dyam_Cut()
fun108:
	pl_call  fun14(&seed[10],0)
	pl_fail




pl_code local fun112
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun101)
	call_c   Dyam_Choice(fun99)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Choice(fun99)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Choice(fun111)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Choice(fun97)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(&ref[86],V(17))
	fail_ret
	move     &ref[80], R(0)
	move     0, R(1)
	move     &ref[87], R(2)
	move     0, R(3)
	move     &ref[87], R(4)
	move     0, R(5)
	move     V(3), R(6)
	move     S(5), R(7)
	move     &ref[88], R(8)
	move     0, R(9)
	call_c   Dyam_Reg_Load(10,V(17))
	move     V(18), R(12)
	move     S(5), R(13)
	pl_call  pred_tag_family_load_7()
	call_c   DYAM_Term_Range_3(V(18),N(1000),&ref[89])
	fail_ret
	call_c   Dyam_Cut()
	pl_fail

pl_code local fun116
	call_c   Dyam_Remove_Choice()
fun115:
	call_c   Dyam_Cut()
	call_c   DYAM_evpred_assert_1(&ref[23])
	call_c   DYAM_evpred_assert_1(&ref[24])
	call_c   DYAM_evpred_assert_1(&ref[25])
	call_c   DYAM_evpred_assert_1(&ref[26])
fun114:
	call_c   Dyam_Cut()
fun113:
	pl_call  fun14(&seed[4],0)
	pl_fail




pl_code local fun117
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun101)
	call_c   Dyam_Choice(fun99)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Choice(fun99)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Choice(fun116)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Choice(fun97)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(&ref[19],V(18))
	fail_ret
	move     &ref[20], R(0)
	move     0, R(1)
	move     &ref[20], R(2)
	move     0, R(3)
	move     &ref[20], R(4)
	move     0, R(5)
	move     V(3), R(6)
	move     S(5), R(7)
	move     &ref[21], R(8)
	move     0, R(9)
	call_c   Dyam_Reg_Load(10,V(18))
	move     V(19), R(12)
	move     S(5), R(13)
	pl_call  pred_tag_family_load_7()
	call_c   DYAM_Term_Range_3(V(19),N(1000),&ref[22])
	fail_ret
	call_c   Dyam_Cut()
	pl_fail

pl_code local fun121
	call_c   Dyam_Remove_Choice()
fun120:
	call_c   Dyam_Cut()
	call_c   DYAM_evpred_assert_1(&ref[28])
	call_c   DYAM_evpred_assert_1(&ref[29])
	call_c   DYAM_evpred_assert_1(&ref[30])
	call_c   DYAM_evpred_assert_1(&ref[31])
fun119:
	call_c   Dyam_Cut()
fun118:
	pl_call  fun14(&seed[5],0)
	pl_fail




pl_code local fun122
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun101)
	call_c   Dyam_Choice(fun99)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Choice(fun99)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Choice(fun121)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Choice(fun97)
	call_c   Dyam_Set_Cut()
	move     &ref[20], R(0)
	move     0, R(1)
	move     &ref[20], R(2)
	move     0, R(3)
	move     &ref[20], R(4)
	move     0, R(5)
	move     V(3), R(6)
	move     S(5), R(7)
	move     &ref[27], R(8)
	move     0, R(9)
	move     &ref[19], R(10)
	move     S(5), R(11)
	move     V(18), R(12)
	move     S(5), R(13)
	pl_call  pred_tag_family_load_7()
	call_c   DYAM_Term_Range_3(V(18),N(1000),V(19))
	fail_ret
	call_c   Dyam_Unify(V(19),&ref[22])
	fail_ret
	call_c   Dyam_Cut()
	pl_fail

pl_code local fun191
	call_c   Dyam_Remove_Choice()
fun190:
	call_c   Dyam_Cut()
	call_c   DYAM_evpred_assert_1(&ref[59])
	call_c   DYAM_evpred_assert_1(&ref[60])
	call_c   DYAM_evpred_assert_1(&ref[61])
	call_c   DYAM_evpred_assert_1(&ref[62])
fun189:
	call_c   Dyam_Cut()
fun188:
	pl_call  fun14(&seed[7],0)
	pl_fail




pl_code local fun192
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun101)
	call_c   Dyam_Choice(fun99)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Choice(fun99)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Choice(fun191)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Choice(fun97)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(&ref[55],V(23))
	fail_ret
	move     &ref[56], R(0)
	move     0, R(1)
	move     &ref[57], R(2)
	move     0, R(3)
	move     &ref[57], R(4)
	move     0, R(5)
	move     V(3), R(6)
	move     S(5), R(7)
	move     &ref[56], R(8)
	move     0, R(9)
	call_c   Dyam_Reg_Load(10,V(23))
	move     V(24), R(12)
	move     S(5), R(13)
	pl_call  pred_tag_family_load_7()
	call_c   DYAM_Term_Range_3(V(24),N(1000),V(25))
	fail_ret
	call_c   Dyam_Unify(V(25),&ref[58])
	fail_ret
	call_c   Dyam_Cut()
	pl_fail

pl_code local fun196
	call_c   Dyam_Remove_Choice()
fun195:
	call_c   Dyam_Cut()
	call_c   DYAM_evpred_assert_1(&ref[72])
	call_c   DYAM_evpred_assert_1(&ref[73])
	call_c   DYAM_evpred_assert_1(&ref[74])
	call_c   DYAM_evpred_assert_1(&ref[75])
fun194:
	call_c   Dyam_Cut()
fun193:
	pl_call  fun14(&seed[8],0)
	pl_fail




pl_code local fun197
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun101)
	call_c   Dyam_Choice(fun99)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Choice(fun99)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Choice(fun196)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Choice(fun97)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(&ref[55],V(23))
	fail_ret
	move     &ref[71], R(0)
	move     0, R(1)
	move     &ref[57], R(2)
	move     0, R(3)
	move     &ref[57], R(4)
	move     0, R(5)
	move     V(3), R(6)
	move     S(5), R(7)
	move     &ref[71], R(8)
	move     0, R(9)
	call_c   Dyam_Reg_Load(10,V(23))
	move     V(24), R(12)
	move     S(5), R(13)
	pl_call  pred_tag_family_load_7()
	call_c   DYAM_Term_Range_3(V(24),N(1000),V(25))
	fail_ret
	call_c   Dyam_Unify(V(25),&ref[58])
	fail_ret
	call_c   Dyam_Cut()
	pl_fail

pl_code local fun97
	call_c   Dyam_Remove_Choice()
fun96:
	call_c   Dyam_Cut()
	pl_fail


pl_code local fun14
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Object(R(0))
	call_c   Dyam_Tabule()
	pl_ret

pl_code local fun201
	call_c   Dyam_Remove_Choice()
fun200:
	call_c   Dyam_Cut()
	call_c   DYAM_evpred_assert_1(&ref[108])
	call_c   DYAM_evpred_assert_1(&ref[109])
	call_c   DYAM_evpred_assert_1(&ref[110])
	call_c   DYAM_evpred_assert_1(&ref[111])
fun199:
	call_c   Dyam_Cut()
fun198:
	pl_call  fun14(&seed[11],0)
	pl_fail




pl_code local fun99
	call_c   Dyam_Remove_Choice()
	pl_fail

pl_code local fun101
	call_c   Dyam_Remove_Choice()
fun100:
	call_c   Dyam_Deallocate()
	pl_ret


pl_code local fun202
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun101)
	call_c   Dyam_Choice(fun99)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Choice(fun99)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Choice(fun201)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Choice(fun97)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(&ref[104],V(28))
	fail_ret
	move     &ref[105], R(0)
	move     0, R(1)
	move     &ref[106], R(2)
	move     0, R(3)
	move     &ref[106], R(4)
	move     0, R(5)
	move     V(8), R(6)
	move     S(5), R(7)
	move     &ref[105], R(8)
	move     0, R(9)
	call_c   Dyam_Reg_Load(10,V(28))
	move     V(29), R(12)
	move     S(5), R(13)
	pl_call  pred_tag_family_load_7()
	call_c   DYAM_Term_Range_3(V(29),N(1000),V(30))
	fail_ret
	call_c   Dyam_Unify(V(30),&ref[107])
	fail_ret
	call_c   Dyam_Cut()
	pl_fail

pl_code local fun177
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(3),&ref[538])
	fail_ret
	call_c   Dyam_Unify(V(7),&ref[174])
	fail_ret
	pl_call  fun6(&seed[89],1)
	pl_call  fun22(&seed[90],2)
	pl_fail

pl_code local fun169
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(3),&ref[512])
	fail_ret
	call_c   Dyam_Unify(V(7),&ref[174])
	fail_ret
	pl_call  fun6(&seed[83],1)
	pl_call  fun22(&seed[84],2)
	pl_fail

pl_code local fun153
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(3),&ref[471])
	fail_ret
	call_c   Dyam_Unify(V(7),&ref[174])
	fail_ret
	pl_call  fun6(&seed[78],1)
	pl_call  fun22(&seed[79],2)
	pl_fail

pl_code local fun146
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(3),&ref[453])
	fail_ret
	call_c   Dyam_Unify(V(7),&ref[174])
	fail_ret
	pl_call  fun6(&seed[72],1)
	pl_call  fun22(&seed[73],2)
	pl_fail

pl_code local fun128
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(3),&ref[403])
	fail_ret
	call_c   Dyam_Unify(V(7),&ref[174])
	fail_ret
	pl_call  fun6(&seed[65],1)
	pl_call  fun22(&seed[66],2)
	pl_fail

pl_code local fun66
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(3),&ref[312])
	fail_ret
	call_c   Dyam_Unify(V(7),&ref[174])
	fail_ret
	pl_call  fun6(&seed[55],1)
	pl_call  fun22(&seed[56],2)
	pl_fail

pl_code local fun59
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(3),&ref[292])
	fail_ret
	call_c   Dyam_Unify(V(7),&ref[174])
	fail_ret
	pl_call  fun6(&seed[49],1)
	pl_call  fun22(&seed[50],2)
	pl_fail

pl_code local fun47
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(3),&ref[258])
	fail_ret
	call_c   Dyam_Unify(V(7),&ref[174])
	fail_ret
	pl_call  fun6(&seed[44],1)
	pl_call  fun22(&seed[45],2)
	pl_fail

pl_code local fun42
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(3),&ref[245])
	fail_ret
	call_c   Dyam_Unify(V(7),&ref[174])
	fail_ret
	pl_call  fun6(&seed[40],1)
	pl_call  fun22(&seed[41],2)
	pl_fail

pl_code local fun28
	call_c   Dyam_Backptr_Trace(R(1),R(2))
	call_c   Dyam_Object(R(0))
	call_c   Dyam_Tabule()
	call_c   Dyam_Now()
	pl_ret

pl_code local fun25
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(3),&ref[192])
	fail_ret
	call_c   Dyam_Unify(V(7),&ref[174])
	fail_ret
	pl_call  fun6(&seed[31],1)
	pl_call  fun22(&seed[32],2)
	pl_fail

pl_code local fun15
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Object(R(0))
	call_c   Dyam_Tabule()
	call_c   Dyam_Now()
	pl_ret

pl_code local fun5
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Object(R(0))
	call_c   Dyam_Tabule()
	call_c   Dyam_Schedule()
	pl_ret


;;------------------ INIT POOL ------------

c_code local build_init_pool
	call_c   build_seed_14()
	call_c   build_seed_15()
	call_c   build_seed_16()
	call_c   build_seed_17()
	call_c   build_seed_18()
	call_c   build_seed_19()
	call_c   build_seed_20()
	call_c   build_seed_21()
	call_c   build_seed_22()
	call_c   build_seed_23()
	call_c   build_seed_24()
	call_c   build_seed_25()
	call_c   build_seed_26()
	call_c   build_seed_13()
	call_c   build_seed_0()
	call_c   build_seed_3()
	call_c   build_seed_1()
	call_c   build_seed_2()
	call_c   build_seed_12()
	call_c   build_seed_6()
	call_c   build_seed_9()
	call_c   build_seed_10()
	call_c   build_seed_4()
	call_c   build_seed_5()
	call_c   build_seed_7()
	call_c   build_seed_8()
	call_c   build_seed_11()
	call_c   build_seed_90()
	call_c   build_seed_89()
	call_c   build_seed_84()
	call_c   build_seed_83()
	call_c   build_seed_79()
	call_c   build_seed_78()
	call_c   build_seed_77()
	call_c   build_seed_76()
	call_c   build_seed_73()
	call_c   build_seed_72()
	call_c   build_seed_70()
	call_c   build_seed_69()
	call_c   build_seed_66()
	call_c   build_seed_65()
	call_c   build_seed_64()
	call_c   build_seed_63()
	call_c   build_seed_62()
	call_c   build_seed_61()
	call_c   build_seed_56()
	call_c   build_seed_55()
	call_c   build_seed_54()
	call_c   build_seed_53()
	call_c   build_seed_50()
	call_c   build_seed_49()
	call_c   build_seed_45()
	call_c   build_seed_44()
	call_c   build_seed_41()
	call_c   build_seed_40()
	call_c   build_seed_38()
	call_c   build_seed_37()
	call_c   build_seed_36()
	call_c   build_seed_35()
	call_c   build_seed_32()
	call_c   build_seed_31()
	call_c   build_seed_29()
	call_c   build_seed_28()
	call_c   build_ref_567()
	call_c   build_ref_566()
	call_c   build_ref_1()
	call_c   build_ref_3()
	call_c   build_ref_4()
	call_c   build_ref_5()
	call_c   build_ref_6()
	call_c   build_ref_7()
	call_c   build_ref_8()
	call_c   build_ref_9()
	call_c   build_ref_14()
	call_c   build_ref_10()
	call_c   build_ref_18()
	call_c   build_ref_15()
	call_c   build_ref_35()
	call_c   build_ref_34()
	call_c   build_ref_38()
	call_c   build_ref_36()
	call_c   build_ref_48()
	call_c   build_ref_45()
	call_c   build_ref_47()
	call_c   build_ref_50()
	call_c   build_ref_54()
	call_c   build_ref_51()
	call_c   build_ref_66()
	call_c   build_ref_63()
	call_c   build_ref_70()
	call_c   build_ref_67()
	call_c   build_ref_79()
	call_c   build_ref_76()
	call_c   build_ref_96()
	call_c   build_ref_95()
	call_c   build_ref_99()
	call_c   build_ref_97()
	call_c   build_ref_103()
	call_c   build_ref_100()
	call_c   build_ref_39()
	call_c   build_ref_44()
	call_c   build_ref_43()
	call_c   build_ref_42()
	call_c   build_ref_41()
	call_c   build_ref_40()
	call_c   build_ref_81()
	call_c   build_ref_85()
	call_c   build_ref_84()
	call_c   build_ref_83()
	call_c   build_ref_82()
	call_c   build_ref_89()
	call_c   build_ref_88()
	call_c   build_ref_87()
	call_c   build_ref_80()
	call_c   build_ref_86()
	call_c   build_ref_93()
	call_c   build_ref_92()
	call_c   build_ref_91()
	call_c   build_ref_90()
	call_c   build_ref_21()
	call_c   build_ref_26()
	call_c   build_ref_25()
	call_c   build_ref_24()
	call_c   build_ref_23()
	call_c   build_ref_22()
	call_c   build_ref_19()
	call_c   build_ref_27()
	call_c   build_ref_20()
	call_c   build_ref_31()
	call_c   build_ref_30()
	call_c   build_ref_29()
	call_c   build_ref_28()
	call_c   build_ref_56()
	call_c   build_ref_62()
	call_c   build_ref_61()
	call_c   build_ref_60()
	call_c   build_ref_59()
	call_c   build_ref_58()
	call_c   build_ref_71()
	call_c   build_ref_57()
	call_c   build_ref_55()
	call_c   build_ref_75()
	call_c   build_ref_74()
	call_c   build_ref_73()
	call_c   build_ref_72()
	call_c   build_ref_107()
	call_c   build_ref_105()
	call_c   build_ref_106()
	call_c   build_ref_104()
	call_c   build_ref_111()
	call_c   build_ref_110()
	call_c   build_ref_109()
	call_c   build_ref_108()
	call_c   build_ref_538()
	call_c   build_ref_512()
	call_c   build_ref_471()
	call_c   build_ref_453()
	call_c   build_ref_403()
	call_c   build_ref_312()
	call_c   build_ref_292()
	call_c   build_ref_258()
	call_c   build_ref_245()
	call_c   build_ref_194()
	call_c   build_ref_174()
	call_c   build_ref_192()
	call_c   build_ref_157()
	call_c   build_ref_156()
	c_ret

long local ref[651]
long local seed[94]

c_code global main_initialization
	call_c   build_viewers()
	call_c   load()
	c_ret

