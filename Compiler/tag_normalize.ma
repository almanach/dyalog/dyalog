;; Compiler: DyALog 1.14.0
;; File "tag_normalize.pl"


;;------------------ LOADING ------------

c_code local load
	call_c   Dyam_Loading_Set()
	pl_call  fun15(&seed[24],0)
	pl_call  fun15(&seed[22],0)
	pl_call  fun15(&seed[21],0)
	pl_call  fun15(&seed[16],0)
	pl_call  fun15(&seed[17],0)
	pl_call  fun15(&seed[23],0)
	pl_call  fun15(&seed[13],0)
	pl_call  fun15(&seed[19],0)
	pl_call  fun15(&seed[15],0)
	pl_call  fun15(&seed[9],0)
	pl_call  fun15(&seed[20],0)
	pl_call  fun15(&seed[12],0)
	pl_call  fun15(&seed[11],0)
	pl_call  fun15(&seed[10],0)
	pl_call  fun15(&seed[8],0)
	pl_call  fun15(&seed[14],0)
	pl_call  fun15(&seed[3],0)
	pl_call  fun15(&seed[18],0)
	pl_call  fun15(&seed[4],0)
	pl_call  fun15(&seed[2],0)
	pl_call  fun15(&seed[7],0)
	pl_call  fun15(&seed[1],0)
	pl_call  fun15(&seed[0],0)
	pl_call  fun15(&seed[6],0)
	pl_call  fun15(&seed[5],0)
	call_c   Dyam_Loading_Reset()
	c_ret

pl_code global pred_guard_use_factor_4
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(4)
	call_c   Dyam_Choice(fun150)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[215])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(5))
	call_c   Dyam_Reg_Load(2,V(2))
	move     V(7), R(4)
	move     S(5), R(5)
	move     V(8), R(6)
	move     S(5), R(7)
	pl_call  pred_guard_use_factor_4()
	call_c   Dyam_Reg_Load(0,V(6))
	call_c   Dyam_Reg_Load(2,V(2))
	move     V(9), R(4)
	move     S(5), R(5)
	move     V(10), R(6)
	move     S(5), R(7)
	pl_call  pred_guard_use_factor_4()
	call_c   Dyam_Choice(fun143)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(7),&ref[94])
	fail_ret
	call_c   Dyam_Unify(V(9),&ref[94])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(3),&ref[94])
	fail_ret
	call_c   Dyam_Reg_Load(0,V(8))
	call_c   Dyam_Reg_Load(2,V(10))
	call_c   Dyam_Reg_Load(4,V(4))
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  pred_guard_and_combine_3()

pl_code global pred_guard_simplify_4
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(4)
	call_c   Dyam_Choice(fun139)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[215])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(5))
	move     V(7), R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Load(4,V(3))
	move     V(8), R(6)
	move     S(5), R(7)
	pl_call  pred_guard_simplify_4()
	call_c   Dyam_Reg_Load(0,V(6))
	move     V(9), R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Load(4,V(8))
	call_c   Dyam_Reg_Load(6,V(4))
	pl_call  pred_guard_simplify_4()
	call_c   Dyam_Unify(V(2),&ref[216])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code global pred_guard_linearize_2
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(2)
	call_c   Dyam_Choice(fun133)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[205])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(3))
	move     V(6), R(2)
	move     S(5), R(3)
	pl_call  pred_guard_linearize_2()
	move     &ref[206], R(0)
	move     S(5), R(1)
	move     V(7), R(2)
	move     S(5), R(3)
	pl_call  pred_guard_linearize_2()
	call_c   Dyam_Unify(V(2),&ref[207])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code global pred_guard_push_disjunctions_2
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(2)
	call_c   Dyam_Choice(fun128)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[196])
	fail_ret
	pl_call  Domain_2(V(5),&ref[197])
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(2),&ref[198])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code global pred_feature_check_3
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(3)
	call_c   Dyam_Choice(fun123)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[190])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Deallocate()
	pl_ret

pl_code global pred_guard_factors_aux_3
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(3)
	call_c   Dyam_Choice(fun82)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(2),I(0))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(3),&ref[157])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code global pred_factor_max_3
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(3)
	call_c   Dyam_Choice(fun76)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[155])
	fail_ret
	call_c   Dyam_Cut()
	call_c   DYAM_evpred_gt(V(3),N(1))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code global pred_factor_sort_add_4
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(4)
	call_c   Dyam_Choice(fun65)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(3),I(0))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(4),&ref[129])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code global pred_factor_sort_2
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(2)
	call_c   Dyam_Choice(fun53)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),I(0))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(2),I(0))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code global pred_guard_and_combine_3
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(3)
	call_c   Dyam_Choice(fun40)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[93])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(3),V(2))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code global pred_guard_or_combine_3
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(3)
	call_c   Dyam_Choice(fun35)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[93])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(3),&ref[93])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code global pred_guard_display_3
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(3)
	move     &ref[87], R(0)
	move     S(5), R(1)
	move     &ref[88], R(2)
	move     S(5), R(3)
	move     I(0), R(4)
	move     0, R(5)
	move     V(7), R(6)
	move     S(5), R(7)
	pl_call  pred_guard_simplify_4()
	move     &ref[89], R(0)
	move     0, R(1)
	move     &ref[90], R(2)
	move     S(5), R(3)
	pl_call  pred_format_2()
	move     &ref[91], R(0)
	move     0, R(1)
	move     &ref[92], R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_format_2()

pl_code global pred_guard_lookup_3
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Reg_Unify(2,&ref[75])
	fail_ret
	call_c   Dyam_Reg_Bind(4,V(5))
	call_c   Dyam_Choice(fun23)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(2),V(1))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(3),V(5))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code global pred_set_spine_2
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(2)
	call_c   Dyam_Choice(fun13)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[42])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(2),&ref[43])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code global pred_normalize_label_2
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(2)
	call_c   Dyam_Choice(fun10)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_variable(V(1))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(2),&ref[41])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret


;;------------------ VIEWERS ------------

c_code local build_viewers
	call_c   Dyam_Load_Viewer(&ref[7],&ref[8])
	call_c   Dyam_Load_Viewer(&ref[3],&ref[4])
	c_ret

c_code local build_seed_5
	ret_reg &seed[5]
	call_c   build_ref_9()
	call_c   build_ref_13()
	call_c   Dyam_Seed_Start(&ref[9],&ref[13],I(0),fun1,1)
	call_c   build_ref_12()
	call_c   Dyam_Seed_Add_Comp(&ref[12],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[5]
	c_ret

;; TERM 12: '*GUARD*'(guard_factorize(fail, fail))
c_code local build_ref_12
	ret_reg &ref[12]
	call_c   build_ref_10()
	call_c   build_ref_11()
	call_c   Dyam_Create_Unary(&ref[10],&ref[11])
	move_ret ref[12]
	c_ret

;; TERM 11: guard_factorize(fail, fail)
c_code local build_ref_11
	ret_reg &ref[11]
	call_c   build_ref_382()
	call_c   build_ref_94()
	call_c   Dyam_Create_Binary(&ref[382],&ref[94],&ref[94])
	move_ret ref[11]
	c_ret

;; TERM 94: fail
c_code local build_ref_94
	ret_reg &ref[94]
	call_c   Dyam_Create_Atom("fail")
	move_ret ref[94]
	c_ret

;; TERM 382: guard_factorize
c_code local build_ref_382
	ret_reg &ref[382]
	call_c   Dyam_Create_Atom("guard_factorize")
	move_ret ref[382]
	c_ret

;; TERM 10: '*GUARD*'
c_code local build_ref_10
	ret_reg &ref[10]
	call_c   Dyam_Create_Atom("*GUARD*")
	move_ret ref[10]
	c_ret

pl_code local fun0
	call_c   build_ref_12()
	call_c   Dyam_Unify_Item(&ref[12])
	fail_ret
	pl_ret

pl_code local fun1
	pl_ret

;; TERM 13: '*GUARD*'(guard_factorize(fail, fail)) :> []
c_code local build_ref_13
	ret_reg &ref[13]
	call_c   build_ref_12()
	call_c   Dyam_Create_Binary(I(9),&ref[12],I(0))
	move_ret ref[13]
	c_ret

;; TERM 9: '*GUARD_CLAUSE*'
c_code local build_ref_9
	ret_reg &ref[9]
	call_c   Dyam_Create_Atom("*GUARD_CLAUSE*")
	move_ret ref[9]
	c_ret

c_code local build_seed_6
	ret_reg &seed[6]
	call_c   build_ref_9()
	call_c   build_ref_16()
	call_c   Dyam_Seed_Start(&ref[9],&ref[16],I(0),fun1,1)
	call_c   build_ref_15()
	call_c   Dyam_Seed_Add_Comp(&ref[15],fun2,0)
	call_c   Dyam_Seed_End()
	move_ret seed[6]
	c_ret

;; TERM 15: '*GUARD*'(guard_factorize(true, true))
c_code local build_ref_15
	ret_reg &ref[15]
	call_c   build_ref_10()
	call_c   build_ref_14()
	call_c   Dyam_Create_Unary(&ref[10],&ref[14])
	move_ret ref[15]
	c_ret

;; TERM 14: guard_factorize(true, true)
c_code local build_ref_14
	ret_reg &ref[14]
	call_c   build_ref_382()
	call_c   build_ref_93()
	call_c   Dyam_Create_Binary(&ref[382],&ref[93],&ref[93])
	move_ret ref[14]
	c_ret

;; TERM 93: true
c_code local build_ref_93
	ret_reg &ref[93]
	call_c   Dyam_Create_Atom("true")
	move_ret ref[93]
	c_ret

pl_code local fun2
	call_c   build_ref_15()
	call_c   Dyam_Unify_Item(&ref[15])
	fail_ret
	pl_ret

;; TERM 16: '*GUARD*'(guard_factorize(true, true)) :> []
c_code local build_ref_16
	ret_reg &ref[16]
	call_c   build_ref_15()
	call_c   Dyam_Create_Binary(I(9),&ref[15],I(0))
	move_ret ref[16]
	c_ret

c_code local build_seed_0
	ret_reg &seed[0]
	call_c   build_ref_9()
	call_c   build_ref_19()
	call_c   Dyam_Seed_Start(&ref[9],&ref[19],I(0),fun1,1)
	call_c   build_ref_18()
	call_c   Dyam_Seed_Add_Comp(&ref[18],fun3,0)
	call_c   Dyam_Seed_End()
	move_ret seed[0]
	c_ret

;; TERM 18: '*GUARD*'(guard_factors(fail, _B, _B))
c_code local build_ref_18
	ret_reg &ref[18]
	call_c   build_ref_10()
	call_c   build_ref_17()
	call_c   Dyam_Create_Unary(&ref[10],&ref[17])
	move_ret ref[18]
	c_ret

;; TERM 17: guard_factors(fail, _B, _B)
c_code local build_ref_17
	ret_reg &ref[17]
	call_c   build_ref_383()
	call_c   build_ref_94()
	call_c   Dyam_Term_Start(&ref[383],3)
	call_c   Dyam_Term_Arg(&ref[94])
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_End()
	move_ret ref[17]
	c_ret

;; TERM 383: guard_factors
c_code local build_ref_383
	ret_reg &ref[383]
	call_c   Dyam_Create_Atom("guard_factors")
	move_ret ref[383]
	c_ret

pl_code local fun3
	call_c   build_ref_18()
	call_c   Dyam_Unify_Item(&ref[18])
	fail_ret
	pl_ret

;; TERM 19: '*GUARD*'(guard_factors(fail, _B, _B)) :> []
c_code local build_ref_19
	ret_reg &ref[19]
	call_c   build_ref_18()
	call_c   Dyam_Create_Binary(I(9),&ref[18],I(0))
	move_ret ref[19]
	c_ret

c_code local build_seed_1
	ret_reg &seed[1]
	call_c   build_ref_9()
	call_c   build_ref_22()
	call_c   Dyam_Seed_Start(&ref[9],&ref[22],I(0),fun1,1)
	call_c   build_ref_21()
	call_c   Dyam_Seed_Add_Comp(&ref[21],fun4,0)
	call_c   Dyam_Seed_End()
	move_ret seed[1]
	c_ret

;; TERM 21: '*GUARD*'(guard_factors(true, _B, _B))
c_code local build_ref_21
	ret_reg &ref[21]
	call_c   build_ref_10()
	call_c   build_ref_20()
	call_c   Dyam_Create_Unary(&ref[10],&ref[20])
	move_ret ref[21]
	c_ret

;; TERM 20: guard_factors(true, _B, _B)
c_code local build_ref_20
	ret_reg &ref[20]
	call_c   build_ref_383()
	call_c   build_ref_93()
	call_c   Dyam_Term_Start(&ref[383],3)
	call_c   Dyam_Term_Arg(&ref[93])
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_End()
	move_ret ref[20]
	c_ret

pl_code local fun4
	call_c   build_ref_21()
	call_c   Dyam_Unify_Item(&ref[21])
	fail_ret
	pl_ret

;; TERM 22: '*GUARD*'(guard_factors(true, _B, _B)) :> []
c_code local build_ref_22
	ret_reg &ref[22]
	call_c   build_ref_21()
	call_c   Dyam_Create_Binary(I(9),&ref[21],I(0))
	move_ret ref[22]
	c_ret

c_code local build_seed_7
	ret_reg &seed[7]
	call_c   build_ref_9()
	call_c   build_ref_25()
	call_c   Dyam_Seed_Start(&ref[9],&ref[25],I(0),fun1,1)
	call_c   build_ref_24()
	call_c   Dyam_Seed_Add_Comp(&ref[24],fun5,0)
	call_c   Dyam_Seed_End()
	move_ret seed[7]
	c_ret

;; TERM 24: '*GUARD*'(normalize_args([], [], _B, _B, _C))
c_code local build_ref_24
	ret_reg &ref[24]
	call_c   build_ref_10()
	call_c   build_ref_23()
	call_c   Dyam_Create_Unary(&ref[10],&ref[23])
	move_ret ref[24]
	c_ret

;; TERM 23: normalize_args([], [], _B, _B, _C)
c_code local build_ref_23
	ret_reg &ref[23]
	call_c   build_ref_384()
	call_c   Dyam_Term_Start(&ref[384],5)
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_End()
	move_ret ref[23]
	c_ret

;; TERM 384: normalize_args
c_code local build_ref_384
	ret_reg &ref[384]
	call_c   Dyam_Create_Atom("normalize_args")
	move_ret ref[384]
	c_ret

pl_code local fun5
	call_c   build_ref_24()
	call_c   Dyam_Unify_Item(&ref[24])
	fail_ret
	pl_ret

;; TERM 25: '*GUARD*'(normalize_args([], [], _B, _B, _C)) :> []
c_code local build_ref_25
	ret_reg &ref[25]
	call_c   build_ref_24()
	call_c   Dyam_Create_Binary(I(9),&ref[24],I(0))
	move_ret ref[25]
	c_ret

c_code local build_seed_2
	ret_reg &seed[2]
	call_c   build_ref_9()
	call_c   build_ref_28()
	call_c   Dyam_Seed_Start(&ref[9],&ref[28],I(0),fun1,1)
	call_c   build_ref_27()
	call_c   Dyam_Seed_Add_Comp(&ref[27],fun6,0)
	call_c   Dyam_Seed_End()
	move_ret seed[2]
	c_ret

;; TERM 27: '*GUARD*'(guard_factorize((_B == _C), (_B == _C)))
c_code local build_ref_27
	ret_reg &ref[27]
	call_c   build_ref_10()
	call_c   build_ref_26()
	call_c   Dyam_Create_Unary(&ref[10],&ref[26])
	move_ret ref[27]
	c_ret

;; TERM 26: guard_factorize((_B == _C), (_B == _C))
c_code local build_ref_26
	ret_reg &ref[26]
	call_c   build_ref_382()
	call_c   build_ref_58()
	call_c   Dyam_Create_Binary(&ref[382],&ref[58],&ref[58])
	move_ret ref[26]
	c_ret

;; TERM 58: _B == _C
c_code local build_ref_58
	ret_reg &ref[58]
	call_c   build_ref_385()
	call_c   Dyam_Create_Binary(&ref[385],V(1),V(2))
	move_ret ref[58]
	c_ret

;; TERM 385: ==
c_code local build_ref_385
	ret_reg &ref[385]
	call_c   Dyam_Create_Atom("==")
	move_ret ref[385]
	c_ret

pl_code local fun6
	call_c   build_ref_27()
	call_c   Dyam_Unify_Item(&ref[27])
	fail_ret
	pl_ret

;; TERM 28: '*GUARD*'(guard_factorize((_B == _C), (_B == _C))) :> []
c_code local build_ref_28
	ret_reg &ref[28]
	call_c   build_ref_27()
	call_c   Dyam_Create_Binary(I(9),&ref[27],I(0))
	move_ret ref[28]
	c_ret

c_code local build_seed_4
	ret_reg &seed[4]
	call_c   build_ref_9()
	call_c   build_ref_31()
	call_c   Dyam_Seed_Start(&ref[9],&ref[31],I(0),fun1,1)
	call_c   build_ref_30()
	call_c   Dyam_Seed_Add_Comp(&ref[30],fun7,0)
	call_c   Dyam_Seed_End()
	move_ret seed[4]
	c_ret

;; TERM 30: '*GUARD*'(guard_factorize((_B = _C), (_B = _C)))
c_code local build_ref_30
	ret_reg &ref[30]
	call_c   build_ref_10()
	call_c   build_ref_29()
	call_c   Dyam_Create_Unary(&ref[10],&ref[29])
	move_ret ref[30]
	c_ret

;; TERM 29: guard_factorize((_B = _C), (_B = _C))
c_code local build_ref_29
	ret_reg &ref[29]
	call_c   build_ref_382()
	call_c   build_ref_62()
	call_c   Dyam_Create_Binary(&ref[382],&ref[62],&ref[62])
	move_ret ref[29]
	c_ret

;; TERM 62: _B = _C
c_code local build_ref_62
	ret_reg &ref[62]
	call_c   build_ref_386()
	call_c   Dyam_Create_Binary(&ref[386],V(1),V(2))
	move_ret ref[62]
	c_ret

;; TERM 386: =
c_code local build_ref_386
	ret_reg &ref[386]
	call_c   Dyam_Create_Atom("=")
	move_ret ref[386]
	c_ret

pl_code local fun7
	call_c   build_ref_30()
	call_c   Dyam_Unify_Item(&ref[30])
	fail_ret
	pl_ret

;; TERM 31: '*GUARD*'(guard_factorize((_B = _C), (_B = _C))) :> []
c_code local build_ref_31
	ret_reg &ref[31]
	call_c   build_ref_30()
	call_c   Dyam_Create_Binary(I(9),&ref[30],I(0))
	move_ret ref[31]
	c_ret

c_code local build_seed_18
	ret_reg &seed[18]
	call_c   build_ref_9()
	call_c   build_ref_37()
	call_c   Dyam_Seed_Start(&ref[9],&ref[37],I(0),fun1,1)
	call_c   build_ref_36()
	call_c   Dyam_Seed_Add_Comp(&ref[36],fun22,0)
	call_c   Dyam_Seed_End()
	move_ret seed[18]
	c_ret

;; TERM 36: '*GUARD*'(normalize((tree _B), _C, _D))
c_code local build_ref_36
	ret_reg &ref[36]
	call_c   build_ref_10()
	call_c   build_ref_35()
	call_c   Dyam_Create_Unary(&ref[10],&ref[35])
	move_ret ref[36]
	c_ret

;; TERM 35: normalize((tree _B), _C, _D)
c_code local build_ref_35
	ret_reg &ref[35]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_388()
	call_c   Dyam_Create_Unary(&ref[388],V(1))
	move_ret R(0)
	call_c   build_ref_387()
	call_c   Dyam_Term_Start(&ref[387],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[35]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 387: normalize
c_code local build_ref_387
	ret_reg &ref[387]
	call_c   Dyam_Create_Atom("normalize")
	move_ret ref[387]
	c_ret

;; TERM 388: tree
c_code local build_ref_388
	ret_reg &ref[388]
	call_c   Dyam_Create_Atom("tree")
	move_ret ref[388]
	c_ret

c_code local build_seed_25
	ret_reg &seed[25]
	call_c   build_ref_10()
	call_c   build_ref_39()
	call_c   Dyam_Seed_Start(&ref[10],&ref[39],I(0),fun9,1)
	call_c   build_ref_40()
	call_c   Dyam_Seed_Add_Comp(&ref[40],&ref[39],0)
	call_c   Dyam_Seed_End()
	move_ret seed[25]
	c_ret

pl_code local fun9
	pl_jump  Complete(0,0)

;; TERM 40: '*GUARD*'(normalize(_B, _C, nospine, nospine, _D)) :> '$$HOLE$$'
c_code local build_ref_40
	ret_reg &ref[40]
	call_c   build_ref_39()
	call_c   Dyam_Create_Binary(I(9),&ref[39],I(7))
	move_ret ref[40]
	c_ret

;; TERM 39: '*GUARD*'(normalize(_B, _C, nospine, nospine, _D))
c_code local build_ref_39
	ret_reg &ref[39]
	call_c   build_ref_10()
	call_c   build_ref_38()
	call_c   Dyam_Create_Unary(&ref[10],&ref[38])
	move_ret ref[39]
	c_ret

;; TERM 38: normalize(_B, _C, nospine, nospine, _D)
c_code local build_ref_38
	ret_reg &ref[38]
	call_c   build_ref_387()
	call_c   build_ref_42()
	call_c   Dyam_Term_Start(&ref[387],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(&ref[42])
	call_c   Dyam_Term_Arg(&ref[42])
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[38]
	c_ret

;; TERM 42: nospine
c_code local build_ref_42
	ret_reg &ref[42]
	call_c   Dyam_Create_Atom("nospine")
	move_ret ref[42]
	c_ret

pl_code local fun16
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Light_Object(R(0))
	pl_jump  Schedule_Prolog()

long local pool_fun22[3]=[2,build_ref_36,build_seed_25]

pl_code local fun22
	call_c   Dyam_Pool(pool_fun22)
	call_c   Dyam_Unify_Item(&ref[36])
	fail_ret
	pl_jump  fun16(&seed[25],1)

;; TERM 37: '*GUARD*'(normalize((tree _B), _C, _D)) :> []
c_code local build_ref_37
	ret_reg &ref[37]
	call_c   build_ref_36()
	call_c   Dyam_Create_Binary(I(9),&ref[36],I(0))
	move_ret ref[37]
	c_ret

c_code local build_seed_3
	ret_reg &seed[3]
	call_c   build_ref_9()
	call_c   build_ref_34()
	call_c   Dyam_Seed_Start(&ref[9],&ref[34],I(0),fun1,1)
	call_c   build_ref_33()
	call_c   Dyam_Seed_Add_Comp(&ref[33],fun8,0)
	call_c   Dyam_Seed_End()
	move_ret seed[3]
	c_ret

;; TERM 33: '*GUARD*'(guard_factorize((\+ \+ _B = _C), (\+ \+ _B = _C)))
c_code local build_ref_33
	ret_reg &ref[33]
	call_c   build_ref_10()
	call_c   build_ref_32()
	call_c   Dyam_Create_Unary(&ref[10],&ref[32])
	move_ret ref[33]
	c_ret

;; TERM 32: guard_factorize((\+ \+ _B = _C), (\+ \+ _B = _C))
c_code local build_ref_32
	ret_reg &ref[32]
	call_c   build_ref_382()
	call_c   build_ref_86()
	call_c   Dyam_Create_Binary(&ref[382],&ref[86],&ref[86])
	move_ret ref[32]
	c_ret

;; TERM 86: \+ \+ _B = _C
c_code local build_ref_86
	ret_reg &ref[86]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_389()
	call_c   build_ref_62()
	call_c   Dyam_Create_Unary(&ref[389],&ref[62])
	move_ret R(0)
	call_c   Dyam_Create_Unary(&ref[389],R(0))
	move_ret ref[86]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 389: \+
c_code local build_ref_389
	ret_reg &ref[389]
	call_c   Dyam_Create_Atom("\\+")
	move_ret ref[389]
	c_ret

pl_code local fun8
	call_c   build_ref_33()
	call_c   Dyam_Unify_Item(&ref[33])
	fail_ret
	pl_ret

;; TERM 34: '*GUARD*'(guard_factorize((\+ \+ _B = _C), (\+ \+ _B = _C))) :> []
c_code local build_ref_34
	ret_reg &ref[34]
	call_c   build_ref_33()
	call_c   Dyam_Create_Binary(I(9),&ref[33],I(0))
	move_ret ref[34]
	c_ret

c_code local build_seed_14
	ret_reg &seed[14]
	call_c   build_ref_9()
	call_c   build_ref_48()
	call_c   Dyam_Seed_Start(&ref[9],&ref[48],I(0),fun1,1)
	call_c   build_ref_47()
	call_c   Dyam_Seed_Add_Comp(&ref[47],fun17,0)
	call_c   Dyam_Seed_End()
	move_ret seed[14]
	c_ret

;; TERM 47: '*GUARD*'(guard_factorize((_B , _C), (_D , _E)))
c_code local build_ref_47
	ret_reg &ref[47]
	call_c   build_ref_10()
	call_c   build_ref_46()
	call_c   Dyam_Create_Unary(&ref[10],&ref[46])
	move_ret ref[47]
	c_ret

;; TERM 46: guard_factorize((_B , _C), (_D , _E))
c_code local build_ref_46
	ret_reg &ref[46]
	call_c   build_ref_382()
	call_c   build_ref_96()
	call_c   build_ref_213()
	call_c   Dyam_Create_Binary(&ref[382],&ref[96],&ref[213])
	move_ret ref[46]
	c_ret

;; TERM 213: _D , _E
c_code local build_ref_213
	ret_reg &ref[213]
	call_c   Dyam_Create_Binary(I(4),V(3),V(4))
	move_ret ref[213]
	c_ret

;; TERM 96: _B , _C
c_code local build_ref_96
	ret_reg &ref[96]
	call_c   Dyam_Create_Binary(I(4),V(1),V(2))
	move_ret ref[96]
	c_ret

c_code local build_seed_26
	ret_reg &seed[26]
	call_c   build_ref_10()
	call_c   build_ref_50()
	call_c   Dyam_Seed_Start(&ref[10],&ref[50],I(0),fun9,1)
	call_c   build_ref_51()
	call_c   Dyam_Seed_Add_Comp(&ref[51],&ref[50],0)
	call_c   Dyam_Seed_End()
	move_ret seed[26]
	c_ret

;; TERM 51: '*GUARD*'(guard_factorize(_B, _D)) :> '$$HOLE$$'
c_code local build_ref_51
	ret_reg &ref[51]
	call_c   build_ref_50()
	call_c   Dyam_Create_Binary(I(9),&ref[50],I(7))
	move_ret ref[51]
	c_ret

;; TERM 50: '*GUARD*'(guard_factorize(_B, _D))
c_code local build_ref_50
	ret_reg &ref[50]
	call_c   build_ref_10()
	call_c   build_ref_49()
	call_c   Dyam_Create_Unary(&ref[10],&ref[49])
	move_ret ref[50]
	c_ret

;; TERM 49: guard_factorize(_B, _D)
c_code local build_ref_49
	ret_reg &ref[49]
	call_c   build_ref_382()
	call_c   Dyam_Create_Binary(&ref[382],V(1),V(3))
	move_ret ref[49]
	c_ret

c_code local build_seed_27
	ret_reg &seed[27]
	call_c   build_ref_10()
	call_c   build_ref_53()
	call_c   Dyam_Seed_Start(&ref[10],&ref[53],I(0),fun9,1)
	call_c   build_ref_54()
	call_c   Dyam_Seed_Add_Comp(&ref[54],&ref[53],0)
	call_c   Dyam_Seed_End()
	move_ret seed[27]
	c_ret

;; TERM 54: '*GUARD*'(guard_factorize(_C, _E)) :> '$$HOLE$$'
c_code local build_ref_54
	ret_reg &ref[54]
	call_c   build_ref_53()
	call_c   Dyam_Create_Binary(I(9),&ref[53],I(7))
	move_ret ref[54]
	c_ret

;; TERM 53: '*GUARD*'(guard_factorize(_C, _E))
c_code local build_ref_53
	ret_reg &ref[53]
	call_c   build_ref_10()
	call_c   build_ref_52()
	call_c   Dyam_Create_Unary(&ref[10],&ref[52])
	move_ret ref[53]
	c_ret

;; TERM 52: guard_factorize(_C, _E)
c_code local build_ref_52
	ret_reg &ref[52]
	call_c   build_ref_382()
	call_c   Dyam_Create_Binary(&ref[382],V(2),V(4))
	move_ret ref[52]
	c_ret

long local pool_fun17[4]=[3,build_ref_47,build_seed_26,build_seed_27]

pl_code local fun17
	call_c   Dyam_Pool(pool_fun17)
	call_c   Dyam_Unify_Item(&ref[47])
	fail_ret
	call_c   Dyam_Allocate(0)
	pl_call  fun16(&seed[26],1)
	call_c   Dyam_Deallocate()
	pl_jump  fun16(&seed[27],1)

;; TERM 48: '*GUARD*'(guard_factorize((_B , _C), (_D , _E))) :> []
c_code local build_ref_48
	ret_reg &ref[48]
	call_c   build_ref_47()
	call_c   Dyam_Create_Binary(I(9),&ref[47],I(0))
	move_ret ref[48]
	c_ret

c_code local build_seed_8
	ret_reg &seed[8]
	call_c   build_ref_9()
	call_c   build_ref_57()
	call_c   Dyam_Seed_Start(&ref[9],&ref[57],I(0),fun1,1)
	call_c   build_ref_56()
	call_c   Dyam_Seed_Add_Comp(&ref[56],fun18,0)
	call_c   Dyam_Seed_End()
	move_ret seed[8]
	c_ret

;; TERM 56: '*GUARD*'(guard_factors((_B == _C), _D, _E))
c_code local build_ref_56
	ret_reg &ref[56]
	call_c   build_ref_10()
	call_c   build_ref_55()
	call_c   Dyam_Create_Unary(&ref[10],&ref[55])
	move_ret ref[56]
	c_ret

;; TERM 55: guard_factors((_B == _C), _D, _E)
c_code local build_ref_55
	ret_reg &ref[55]
	call_c   build_ref_383()
	call_c   build_ref_58()
	call_c   Dyam_Term_Start(&ref[383],3)
	call_c   Dyam_Term_Arg(&ref[58])
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[55]
	c_ret

long local pool_fun18[3]=[2,build_ref_56,build_ref_58]

pl_code local fun18
	call_c   Dyam_Pool(pool_fun18)
	call_c   Dyam_Unify_Item(&ref[56])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[58], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(3))
	call_c   Dyam_Reg_Load(4,V(4))
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  pred_guard_factors_aux_3()

;; TERM 57: '*GUARD*'(guard_factors((_B == _C), _D, _E)) :> []
c_code local build_ref_57
	ret_reg &ref[57]
	call_c   build_ref_56()
	call_c   Dyam_Create_Binary(I(9),&ref[56],I(0))
	move_ret ref[57]
	c_ret

c_code local build_seed_10
	ret_reg &seed[10]
	call_c   build_ref_9()
	call_c   build_ref_61()
	call_c   Dyam_Seed_Start(&ref[9],&ref[61],I(0),fun1,1)
	call_c   build_ref_60()
	call_c   Dyam_Seed_Add_Comp(&ref[60],fun19,0)
	call_c   Dyam_Seed_End()
	move_ret seed[10]
	c_ret

;; TERM 60: '*GUARD*'(guard_factors((_B = _C), _D, _E))
c_code local build_ref_60
	ret_reg &ref[60]
	call_c   build_ref_10()
	call_c   build_ref_59()
	call_c   Dyam_Create_Unary(&ref[10],&ref[59])
	move_ret ref[60]
	c_ret

;; TERM 59: guard_factors((_B = _C), _D, _E)
c_code local build_ref_59
	ret_reg &ref[59]
	call_c   build_ref_383()
	call_c   build_ref_62()
	call_c   Dyam_Term_Start(&ref[383],3)
	call_c   Dyam_Term_Arg(&ref[62])
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[59]
	c_ret

long local pool_fun19[3]=[2,build_ref_60,build_ref_62]

pl_code local fun19
	call_c   Dyam_Pool(pool_fun19)
	call_c   Dyam_Unify_Item(&ref[60])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[62], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(3))
	call_c   Dyam_Reg_Load(4,V(4))
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  pred_guard_factors_aux_3()

;; TERM 61: '*GUARD*'(guard_factors((_B = _C), _D, _E)) :> []
c_code local build_ref_61
	ret_reg &ref[61]
	call_c   build_ref_60()
	call_c   Dyam_Create_Binary(I(9),&ref[60],I(0))
	move_ret ref[61]
	c_ret

c_code local build_seed_11
	ret_reg &seed[11]
	call_c   build_ref_9()
	call_c   build_ref_65()
	call_c   Dyam_Seed_Start(&ref[9],&ref[65],I(0),fun1,1)
	call_c   build_ref_64()
	call_c   Dyam_Seed_Add_Comp(&ref[64],fun20,0)
	call_c   Dyam_Seed_End()
	move_ret seed[11]
	c_ret

;; TERM 64: '*GUARD*'(guard_factors((_B , _C), _D, _E))
c_code local build_ref_64
	ret_reg &ref[64]
	call_c   build_ref_10()
	call_c   build_ref_63()
	call_c   Dyam_Create_Unary(&ref[10],&ref[63])
	move_ret ref[64]
	c_ret

;; TERM 63: guard_factors((_B , _C), _D, _E)
c_code local build_ref_63
	ret_reg &ref[63]
	call_c   build_ref_383()
	call_c   build_ref_96()
	call_c   Dyam_Term_Start(&ref[383],3)
	call_c   Dyam_Term_Arg(&ref[96])
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[63]
	c_ret

c_code local build_seed_28
	ret_reg &seed[28]
	call_c   build_ref_10()
	call_c   build_ref_67()
	call_c   Dyam_Seed_Start(&ref[10],&ref[67],I(0),fun9,1)
	call_c   build_ref_68()
	call_c   Dyam_Seed_Add_Comp(&ref[68],&ref[67],0)
	call_c   Dyam_Seed_End()
	move_ret seed[28]
	c_ret

;; TERM 68: '*GUARD*'(guard_factors(_B, _D, _F)) :> '$$HOLE$$'
c_code local build_ref_68
	ret_reg &ref[68]
	call_c   build_ref_67()
	call_c   Dyam_Create_Binary(I(9),&ref[67],I(7))
	move_ret ref[68]
	c_ret

;; TERM 67: '*GUARD*'(guard_factors(_B, _D, _F))
c_code local build_ref_67
	ret_reg &ref[67]
	call_c   build_ref_10()
	call_c   build_ref_66()
	call_c   Dyam_Create_Unary(&ref[10],&ref[66])
	move_ret ref[67]
	c_ret

;; TERM 66: guard_factors(_B, _D, _F)
c_code local build_ref_66
	ret_reg &ref[66]
	call_c   build_ref_383()
	call_c   Dyam_Term_Start(&ref[383],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[66]
	c_ret

c_code local build_seed_29
	ret_reg &seed[29]
	call_c   build_ref_10()
	call_c   build_ref_70()
	call_c   Dyam_Seed_Start(&ref[10],&ref[70],I(0),fun9,1)
	call_c   build_ref_71()
	call_c   Dyam_Seed_Add_Comp(&ref[71],&ref[70],0)
	call_c   Dyam_Seed_End()
	move_ret seed[29]
	c_ret

;; TERM 71: '*GUARD*'(guard_factors(_C, _F, _E)) :> '$$HOLE$$'
c_code local build_ref_71
	ret_reg &ref[71]
	call_c   build_ref_70()
	call_c   Dyam_Create_Binary(I(9),&ref[70],I(7))
	move_ret ref[71]
	c_ret

;; TERM 70: '*GUARD*'(guard_factors(_C, _F, _E))
c_code local build_ref_70
	ret_reg &ref[70]
	call_c   build_ref_10()
	call_c   build_ref_69()
	call_c   Dyam_Create_Unary(&ref[10],&ref[69])
	move_ret ref[70]
	c_ret

;; TERM 69: guard_factors(_C, _F, _E)
c_code local build_ref_69
	ret_reg &ref[69]
	call_c   build_ref_383()
	call_c   Dyam_Term_Start(&ref[383],3)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[69]
	c_ret

long local pool_fun20[4]=[3,build_ref_64,build_seed_28,build_seed_29]

pl_code local fun20
	call_c   Dyam_Pool(pool_fun20)
	call_c   Dyam_Unify_Item(&ref[64])
	fail_ret
	call_c   Dyam_Allocate(0)
	pl_call  fun16(&seed[28],1)
	call_c   Dyam_Deallocate()
	pl_jump  fun16(&seed[29],1)

;; TERM 65: '*GUARD*'(guard_factors((_B , _C), _D, _E)) :> []
c_code local build_ref_65
	ret_reg &ref[65]
	call_c   build_ref_64()
	call_c   Dyam_Create_Binary(I(9),&ref[64],I(0))
	move_ret ref[65]
	c_ret

c_code local build_seed_12
	ret_reg &seed[12]
	call_c   build_ref_9()
	call_c   build_ref_74()
	call_c   Dyam_Seed_Start(&ref[9],&ref[74],I(0),fun1,1)
	call_c   build_ref_73()
	call_c   Dyam_Seed_Add_Comp(&ref[73],fun21,0)
	call_c   Dyam_Seed_End()
	move_ret seed[12]
	c_ret

;; TERM 73: '*GUARD*'(guard_factors((_B ; _C), _D, _E))
c_code local build_ref_73
	ret_reg &ref[73]
	call_c   build_ref_10()
	call_c   build_ref_72()
	call_c   Dyam_Create_Unary(&ref[10],&ref[72])
	move_ret ref[73]
	c_ret

;; TERM 72: guard_factors((_B ; _C), _D, _E)
c_code local build_ref_72
	ret_reg &ref[72]
	call_c   build_ref_383()
	call_c   build_ref_95()
	call_c   Dyam_Term_Start(&ref[383],3)
	call_c   Dyam_Term_Arg(&ref[95])
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[72]
	c_ret

;; TERM 95: _B ; _C
c_code local build_ref_95
	ret_reg &ref[95]
	call_c   Dyam_Create_Binary(I(5),V(1),V(2))
	move_ret ref[95]
	c_ret

long local pool_fun21[4]=[3,build_ref_73,build_seed_28,build_seed_29]

pl_code local fun21
	call_c   Dyam_Pool(pool_fun21)
	call_c   Dyam_Unify_Item(&ref[73])
	fail_ret
	call_c   Dyam_Allocate(0)
	pl_call  fun16(&seed[28],1)
	call_c   Dyam_Deallocate()
	pl_jump  fun16(&seed[29],1)

;; TERM 74: '*GUARD*'(guard_factors((_B ; _C), _D, _E)) :> []
c_code local build_ref_74
	ret_reg &ref[74]
	call_c   build_ref_73()
	call_c   Dyam_Create_Binary(I(9),&ref[73],I(0))
	move_ret ref[74]
	c_ret

c_code local build_seed_20
	ret_reg &seed[20]
	call_c   build_ref_9()
	call_c   build_ref_78()
	call_c   Dyam_Seed_Start(&ref[9],&ref[78],I(0),fun1,1)
	call_c   build_ref_77()
	call_c   Dyam_Seed_Add_Comp(&ref[77],fun29,0)
	call_c   Dyam_Seed_End()
	move_ret seed[20]
	c_ret

;; TERM 77: '*GUARD*'(name_tag_node(_B, _C))
c_code local build_ref_77
	ret_reg &ref[77]
	call_c   build_ref_10()
	call_c   build_ref_76()
	call_c   Dyam_Create_Unary(&ref[10],&ref[76])
	move_ret ref[77]
	c_ret

;; TERM 76: name_tag_node(_B, _C)
c_code local build_ref_76
	ret_reg &ref[76]
	call_c   build_ref_390()
	call_c   Dyam_Create_Binary(&ref[390],V(1),V(2))
	move_ret ref[76]
	c_ret

;; TERM 390: name_tag_node
c_code local build_ref_390
	ret_reg &ref[390]
	call_c   Dyam_Create_Atom("name_tag_node")
	move_ret ref[390]
	c_ret

;; TERM 82: tag_nodeid(_C)
c_code local build_ref_82
	ret_reg &ref[82]
	call_c   build_ref_391()
	call_c   Dyam_Create_Unary(&ref[391],V(2))
	move_ret ref[82]
	c_ret

;; TERM 391: tag_nodeid
c_code local build_ref_391
	ret_reg &ref[391]
	call_c   Dyam_Create_Atom("tag_nodeid")
	move_ret ref[391]
	c_ret

;; TERM 79: tag_node_id(_C, _B)
c_code local build_ref_79
	ret_reg &ref[79]
	call_c   build_ref_392()
	call_c   Dyam_Create_Binary(&ref[392],V(2),V(1))
	move_ret ref[79]
	c_ret

;; TERM 392: tag_node_id
c_code local build_ref_392
	ret_reg &ref[392]
	call_c   Dyam_Create_Atom("tag_node_id")
	move_ret ref[392]
	c_ret

long local pool_fun26[2]=[1,build_ref_79]

pl_code local fun26
	call_c   Dyam_Remove_Choice()
	call_c   DYAM_evpred_assert_1(&ref[79])
fun25:
	call_c   Dyam_Deallocate()
	pl_ret


;; TERM 80: 'duplicate local name ~w for tag node in tree ~w'
c_code local build_ref_80
	ret_reg &ref[80]
	call_c   Dyam_Create_Atom("duplicate local name ~w for tag node in tree ~w")
	move_ret ref[80]
	c_ret

;; TERM 81: [_B,_C]
c_code local build_ref_81
	ret_reg &ref[81]
	call_c   Dyam_Create_Tupple(1,2,I(0))
	move_ret ref[81]
	c_ret

long local pool_fun27[5]=[65539,build_ref_79,build_ref_80,build_ref_81,pool_fun26]

long local pool_fun28[3]=[65537,build_ref_82,pool_fun27]

pl_code local fun28
	call_c   Dyam_Remove_Choice()
	move     &ref[82], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(1))
	pl_call  pred_update_counter_2()
fun27:
	call_c   Dyam_Choice(fun26)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[79])
	call_c   Dyam_Cut()
	move     &ref[80], R(0)
	move     0, R(1)
	move     &ref[81], R(2)
	move     S(5), R(3)
	pl_call  pred_error_2()
	pl_jump  fun25()


long local pool_fun29[4]=[131073,build_ref_77,pool_fun28,pool_fun27]

pl_code local fun29
	call_c   Dyam_Pool(pool_fun29)
	call_c   Dyam_Unify_Item(&ref[77])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun28)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_notvar(V(1))
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun27()

;; TERM 78: '*GUARD*'(name_tag_node(_B, _C)) :> []
c_code local build_ref_78
	ret_reg &ref[78]
	call_c   build_ref_77()
	call_c   Dyam_Create_Binary(I(9),&ref[77],I(0))
	move_ret ref[78]
	c_ret

c_code local build_seed_9
	ret_reg &seed[9]
	call_c   build_ref_9()
	call_c   build_ref_85()
	call_c   Dyam_Seed_Start(&ref[9],&ref[85],I(0),fun1,1)
	call_c   build_ref_84()
	call_c   Dyam_Seed_Add_Comp(&ref[84],fun30,0)
	call_c   Dyam_Seed_End()
	move_ret seed[9]
	c_ret

;; TERM 84: '*GUARD*'(guard_factors((\+ \+ _B = _C), _D, _E))
c_code local build_ref_84
	ret_reg &ref[84]
	call_c   build_ref_10()
	call_c   build_ref_83()
	call_c   Dyam_Create_Unary(&ref[10],&ref[83])
	move_ret ref[84]
	c_ret

;; TERM 83: guard_factors((\+ \+ _B = _C), _D, _E)
c_code local build_ref_83
	ret_reg &ref[83]
	call_c   build_ref_383()
	call_c   build_ref_86()
	call_c   Dyam_Term_Start(&ref[383],3)
	call_c   Dyam_Term_Arg(&ref[86])
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[83]
	c_ret

long local pool_fun30[3]=[2,build_ref_84,build_ref_86]

pl_code local fun30
	call_c   Dyam_Pool(pool_fun30)
	call_c   Dyam_Unify_Item(&ref[84])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[86], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(3))
	call_c   Dyam_Reg_Load(4,V(4))
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  pred_guard_factors_aux_3()

;; TERM 85: '*GUARD*'(guard_factors((\+ \+ _B = _C), _D, _E)) :> []
c_code local build_ref_85
	ret_reg &ref[85]
	call_c   build_ref_84()
	call_c   Dyam_Create_Binary(I(9),&ref[84],I(0))
	move_ret ref[85]
	c_ret

c_code local build_seed_15
	ret_reg &seed[15]
	call_c   build_ref_97()
	call_c   build_ref_100()
	call_c   Dyam_Seed_Start(&ref[97],&ref[100],I(0),fun50,1)
	call_c   build_ref_102()
	call_c   Dyam_Seed_Add_Comp(&ref[102],fun49,0)
	call_c   Dyam_Seed_End()
	move_ret seed[15]
	c_ret

;; TERM 102: '*CITEM*'('call_term_module_shift/5'(_B, tag((_C / _D))), _A)
c_code local build_ref_102
	ret_reg &ref[102]
	call_c   build_ref_101()
	call_c   build_ref_98()
	call_c   Dyam_Create_Binary(&ref[101],&ref[98],V(0))
	move_ret ref[102]
	c_ret

;; TERM 98: 'call_term_module_shift/5'(_B, tag((_C / _D)))
c_code local build_ref_98
	ret_reg &ref[98]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_395()
	call_c   Dyam_Create_Binary(&ref[395],V(2),V(3))
	move_ret R(0)
	call_c   build_ref_394()
	call_c   Dyam_Create_Unary(&ref[394],R(0))
	move_ret R(0)
	call_c   build_ref_393()
	call_c   Dyam_Create_Binary(&ref[393],V(1),R(0))
	move_ret ref[98]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 393: 'call_term_module_shift/5'
c_code local build_ref_393
	ret_reg &ref[393]
	call_c   Dyam_Create_Atom("call_term_module_shift/5")
	move_ret ref[393]
	c_ret

;; TERM 394: tag
c_code local build_ref_394
	ret_reg &ref[394]
	call_c   Dyam_Create_Atom("tag")
	move_ret ref[394]
	c_ret

;; TERM 395: /
c_code local build_ref_395
	ret_reg &ref[395]
	call_c   Dyam_Create_Atom("/")
	move_ret ref[395]
	c_ret

;; TERM 101: '*CITEM*'
c_code local build_ref_101
	ret_reg &ref[101]
	call_c   Dyam_Create_Atom("*CITEM*")
	move_ret ref[101]
	c_ret

;; TERM 104: [_C|_J]
c_code local build_ref_104
	ret_reg &ref[104]
	call_c   Dyam_Create_List(V(2),V(9))
	move_ret ref[104]
	c_ret

;; TERM 105: [_E|_J]
c_code local build_ref_105
	ret_reg &ref[105]
	call_c   Dyam_Create_List(V(4),V(9))
	move_ret ref[105]
	c_ret

c_code local build_seed_30
	ret_reg &seed[30]
	call_c   build_ref_0()
	call_c   build_ref_107()
	call_c   Dyam_Seed_Start(&ref[0],&ref[107],&ref[107],fun9,1)
	call_c   build_ref_108()
	call_c   Dyam_Seed_Add_Comp(&ref[108],&ref[107],0)
	call_c   Dyam_Seed_End()
	move_ret seed[30]
	c_ret

;; TERM 108: '*RITEM*'(_A, return(tag((_E / _D)), _F, _G)) :> '$$HOLE$$'
c_code local build_ref_108
	ret_reg &ref[108]
	call_c   build_ref_107()
	call_c   Dyam_Create_Binary(I(9),&ref[107],I(7))
	move_ret ref[108]
	c_ret

;; TERM 107: '*RITEM*'(_A, return(tag((_E / _D)), _F, _G))
c_code local build_ref_107
	ret_reg &ref[107]
	call_c   build_ref_0()
	call_c   build_ref_106()
	call_c   Dyam_Create_Binary(&ref[0],V(0),&ref[106])
	move_ret ref[107]
	c_ret

;; TERM 106: return(tag((_E / _D)), _F, _G)
c_code local build_ref_106
	ret_reg &ref[106]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_395()
	call_c   Dyam_Create_Binary(&ref[395],V(4),V(3))
	move_ret R(0)
	call_c   build_ref_394()
	call_c   Dyam_Create_Unary(&ref[394],R(0))
	move_ret R(0)
	call_c   build_ref_396()
	call_c   Dyam_Term_Start(&ref[396],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[106]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 396: return
c_code local build_ref_396
	ret_reg &ref[396]
	call_c   Dyam_Create_Atom("return")
	move_ret ref[396]
	c_ret

;; TERM 0: '*RITEM*'
c_code local build_ref_0
	ret_reg &ref[0]
	call_c   Dyam_Create_Atom("*RITEM*")
	move_ret ref[0]
	c_ret

pl_code local fun42
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Object(R(0))
	call_c   Dyam_Tabule()
	pl_ret

pl_code local fun43
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Choice(fun42)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Subsume(R(0))
	fail_ret
	call_c   Dyam_Cut()
	pl_ret

long local pool_fun44[4]=[3,build_ref_104,build_ref_105,build_seed_30]

pl_code local fun47
	call_c   Dyam_Remove_Choice()
fun46:
	call_c   DYAM_evpred_atom_to_module(V(2),I(0),V(8))
	fail_ret
	call_c   Dyam_Unify(V(7),V(1))
	fail_ret
fun44:
	call_c   DYAM_evpred_atom_to_module(V(4),V(7),V(2))
	fail_ret
	call_c   DYAM_evpred_length(V(9),V(3))
	fail_ret
	call_c   DYAM_evpred_univ(V(5),&ref[104])
	fail_ret
	call_c   DYAM_evpred_univ(V(6),&ref[105])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_jump  fun43(&seed[30],2)



pl_code local fun48
	call_c   Dyam_Update_Choice(fun47)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(1),I(0))
	fail_ret
	call_c   Dyam_Cut()
	pl_fail

;; TERM 103: module_import(tag((_C / _D)), _H)
c_code local build_ref_103
	ret_reg &ref[103]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_395()
	call_c   Dyam_Create_Binary(&ref[395],V(2),V(3))
	move_ret R(0)
	call_c   build_ref_394()
	call_c   Dyam_Create_Unary(&ref[394],R(0))
	move_ret R(0)
	call_c   build_ref_397()
	call_c   Dyam_Create_Binary(&ref[397],R(0),V(7))
	move_ret ref[103]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 397: module_import
c_code local build_ref_397
	ret_reg &ref[397]
	call_c   Dyam_Create_Atom("module_import")
	move_ret ref[397]
	c_ret

pl_code local fun45
	call_c   Dyam_Remove_Choice()
	pl_jump  fun44()

long local pool_fun49[5]=[131074,build_ref_102,build_ref_103,pool_fun44,pool_fun44]

pl_code local fun49
	call_c   Dyam_Pool(pool_fun49)
	call_c   Dyam_Unify_Item(&ref[102])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun48)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[103])
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun45)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(7),I(0))
	fail_ret
	call_c   Dyam_Cut()
	pl_fail

pl_code local fun50
	pl_jump  Apply(0,0)

;; TERM 100: '*FIRST*'('call_term_module_shift/5'(_B, tag((_C / _D)))) :> []
c_code local build_ref_100
	ret_reg &ref[100]
	call_c   build_ref_99()
	call_c   Dyam_Create_Binary(I(9),&ref[99],I(0))
	move_ret ref[100]
	c_ret

;; TERM 99: '*FIRST*'('call_term_module_shift/5'(_B, tag((_C / _D))))
c_code local build_ref_99
	ret_reg &ref[99]
	call_c   build_ref_97()
	call_c   build_ref_98()
	call_c   Dyam_Create_Unary(&ref[97],&ref[98])
	move_ret ref[99]
	c_ret

;; TERM 97: '*FIRST*'
c_code local build_ref_97
	ret_reg &ref[97]
	call_c   Dyam_Create_Atom("*FIRST*")
	move_ret ref[97]
	c_ret

c_code local build_seed_19
	ret_reg &seed[19]
	call_c   build_ref_97()
	call_c   build_ref_112()
	call_c   Dyam_Seed_Start(&ref[97],&ref[112],I(0),fun50,1)
	call_c   build_ref_113()
	call_c   Dyam_Seed_Add_Comp(&ref[113],fun63,0)
	call_c   Dyam_Seed_End()
	move_ret seed[19]
	c_ret

;; TERM 113: '*CITEM*'('call_normalize/2'(tag_tree{family=> _B, name=> _C, tree=> _D}), _A)
c_code local build_ref_113
	ret_reg &ref[113]
	call_c   build_ref_101()
	call_c   build_ref_110()
	call_c   Dyam_Create_Binary(&ref[101],&ref[110],V(0))
	move_ret ref[113]
	c_ret

;; TERM 110: 'call_normalize/2'(tag_tree{family=> _B, name=> _C, tree=> _D})
c_code local build_ref_110
	ret_reg &ref[110]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_399()
	call_c   Dyam_Term_Start(&ref[399],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_398()
	call_c   Dyam_Create_Unary(&ref[398],R(0))
	move_ret ref[110]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 398: 'call_normalize/2'
c_code local build_ref_398
	ret_reg &ref[398]
	call_c   Dyam_Create_Atom("call_normalize/2")
	move_ret ref[398]
	c_ret

;; TERM 399: tag_tree!'$ft'
c_code local build_ref_399
	ret_reg &ref[399]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_388()
	call_c   Dyam_Create_List(&ref[388],I(0))
	move_ret R(0)
	call_c   build_ref_402()
	call_c   Dyam_Create_List(&ref[402],R(0))
	move_ret R(0)
	call_c   build_ref_401()
	call_c   Dyam_Create_List(&ref[401],R(0))
	move_ret R(0)
	call_c   build_ref_400()
	call_c   Dyam_Create_Atom_Module("$ft",&ref[400])
	move_ret ref[399]
	call_c   DYAM_Feature_2(&ref[400],R(0))
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 400: tag_tree
c_code local build_ref_400
	ret_reg &ref[400]
	call_c   Dyam_Create_Atom("tag_tree")
	move_ret ref[400]
	c_ret

;; TERM 401: family
c_code local build_ref_401
	ret_reg &ref[401]
	call_c   Dyam_Create_Atom("family")
	move_ret ref[401]
	c_ret

;; TERM 402: name
c_code local build_ref_402
	ret_reg &ref[402]
	call_c   Dyam_Create_Atom("name")
	move_ret ref[402]
	c_ret

;; TERM 127: tag_tree(_C, _B)
c_code local build_ref_127
	ret_reg &ref[127]
	call_c   build_ref_400()
	call_c   Dyam_Create_Binary(&ref[400],V(2),V(1))
	move_ret ref[127]
	c_ret

c_code local build_seed_31
	ret_reg &seed[31]
	call_c   build_ref_10()
	call_c   build_ref_121()
	call_c   Dyam_Seed_Start(&ref[10],&ref[121],I(0),fun9,1)
	call_c   build_ref_122()
	call_c   Dyam_Seed_Add_Comp(&ref[122],&ref[121],0)
	call_c   Dyam_Seed_End()
	move_ret seed[31]
	c_ret

;; TERM 122: '*GUARD*'(normalize(_D, _E, _C)) :> '$$HOLE$$'
c_code local build_ref_122
	ret_reg &ref[122]
	call_c   build_ref_121()
	call_c   Dyam_Create_Binary(I(9),&ref[121],I(7))
	move_ret ref[122]
	c_ret

;; TERM 121: '*GUARD*'(normalize(_D, _E, _C))
c_code local build_ref_121
	ret_reg &ref[121]
	call_c   build_ref_10()
	call_c   build_ref_120()
	call_c   Dyam_Create_Unary(&ref[10],&ref[120])
	move_ret ref[121]
	c_ret

;; TERM 120: normalize(_D, _E, _C)
c_code local build_ref_120
	ret_reg &ref[120]
	call_c   build_ref_387()
	call_c   Dyam_Term_Start(&ref[387],3)
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_End()
	move_ret ref[120]
	c_ret

;; TERM 123: tag_node_id(_C, _N)
c_code local build_ref_123
	ret_reg &ref[123]
	call_c   build_ref_392()
	call_c   Dyam_Create_Binary(&ref[392],V(2),V(13))
	move_ret ref[123]
	c_ret

c_code local build_seed_32
	ret_reg &seed[32]
	call_c   build_ref_0()
	call_c   build_ref_125()
	call_c   Dyam_Seed_Start(&ref[0],&ref[125],&ref[125],fun9,1)
	call_c   build_ref_126()
	call_c   Dyam_Seed_Add_Comp(&ref[126],&ref[125],0)
	call_c   Dyam_Seed_End()
	move_ret seed[32]
	c_ret

;; TERM 126: '*RITEM*'(_A, return(_E)) :> '$$HOLE$$'
c_code local build_ref_126
	ret_reg &ref[126]
	call_c   build_ref_125()
	call_c   Dyam_Create_Binary(I(9),&ref[125],I(7))
	move_ret ref[126]
	c_ret

;; TERM 125: '*RITEM*'(_A, return(_E))
c_code local build_ref_125
	ret_reg &ref[125]
	call_c   build_ref_0()
	call_c   build_ref_124()
	call_c   Dyam_Create_Binary(&ref[0],V(0),&ref[124])
	move_ret ref[125]
	c_ret

;; TERM 124: return(_E)
c_code local build_ref_124
	ret_reg &ref[124]
	call_c   build_ref_396()
	call_c   Dyam_Create_Unary(&ref[396],V(4))
	move_ret ref[124]
	c_ret

long local pool_fun55[4]=[3,build_seed_31,build_ref_123,build_seed_32]

long local pool_fun56[3]=[65537,build_ref_127,pool_fun55]

pl_code local fun56
	call_c   Dyam_Remove_Choice()
	call_c   DYAM_evpred_assert_1(&ref[127])
fun55:
	pl_call  fun16(&seed[31],1)
	call_c   DYAM_evpred_retract(&ref[123])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_jump  fun43(&seed[32],2)


;; TERM 117: tag_tree(_C, _M)
c_code local build_ref_117
	ret_reg &ref[117]
	call_c   build_ref_400()
	call_c   Dyam_Create_Binary(&ref[400],V(2),V(12))
	move_ret ref[117]
	c_ret

;; TERM 118: 'two TAG trees with same name ~w\n'
c_code local build_ref_118
	ret_reg &ref[118]
	call_c   Dyam_Create_Atom("two TAG trees with same name ~w\n")
	move_ret ref[118]
	c_ret

;; TERM 119: [_C]
c_code local build_ref_119
	ret_reg &ref[119]
	call_c   Dyam_Create_List(V(2),I(0))
	move_ret ref[119]
	c_ret

long local pool_fun57[6]=[131075,build_ref_117,build_ref_118,build_ref_119,pool_fun56,pool_fun55]

pl_code local fun58
	call_c   Dyam_Remove_Choice()
fun57:
	call_c   Dyam_Choice(fun56)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[117])
	call_c   Dyam_Cut()
	move     &ref[118], R(0)
	move     0, R(1)
	move     &ref[119], R(2)
	move     S(5), R(3)
	pl_call  pred_error_2()
	pl_jump  fun55()


;; TERM 115: tag_trees
c_code local build_ref_115
	ret_reg &ref[115]
	call_c   Dyam_Create_Atom("tag_trees")
	move_ret ref[115]
	c_ret

;; TERM 116: [0't,0'r,0'e,0'e|_K]
c_code local build_ref_116
	ret_reg &ref[116]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(C(101),V(10))
	move_ret R(0)
	call_c   Dyam_Create_List(C(101),R(0))
	move_ret R(0)
	call_c   Dyam_Create_List(C(114),R(0))
	move_ret R(0)
	call_c   Dyam_Create_List(C(116),R(0))
	move_ret ref[116]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun59[5]=[131074,build_ref_115,build_ref_116,pool_fun57,pool_fun57]

pl_code local fun62
	call_c   Dyam_Remove_Choice()
fun59:
	call_c   Dyam_Choice(fun58)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_variable(V(2))
	fail_ret
	call_c   Dyam_Cut()
	move     &ref[115], R(0)
	move     0, R(1)
	move     V(9), R(2)
	move     S(5), R(3)
	pl_call  pred_update_counter_2()
	call_c   DYAM_evpred_number_to_chars(V(9),V(10))
	fail_ret
	call_c   DYAM_evpred_atom_to_chars(V(11),&ref[116])
	fail_ret
	call_c   Dyam_Reg_Load(0,V(11))
	call_c   Dyam_Reg_Load(2,V(2))
	pl_call  pred_default_module_2()
	pl_jump  fun57()


pl_code local fun60
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(1),V(2))
	fail_ret
	pl_jump  fun59()

;; TERM 128: main_file(_F, info(_G, _B, _H, _I))
c_code local build_ref_128
	ret_reg &ref[128]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_404()
	call_c   Dyam_Term_Start(&ref[404],4)
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_403()
	call_c   Dyam_Create_Binary(&ref[403],V(5),R(0))
	move_ret ref[128]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 403: main_file
c_code local build_ref_403
	ret_reg &ref[403]
	call_c   Dyam_Create_Atom("main_file")
	move_ret ref[403]
	c_ret

;; TERM 404: info
c_code local build_ref_404
	ret_reg &ref[404]
	call_c   Dyam_Create_Atom("info")
	move_ret ref[404]
	c_ret

long local pool_fun61[4]=[131073,build_ref_128,pool_fun59,pool_fun59]

pl_code local fun61
	call_c   Dyam_Update_Choice(fun60)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_variable(V(2))
	fail_ret
	call_c   Dyam_Cut()
	pl_call  Object_1(&ref[128])
	pl_jump  fun59()

;; TERM 114: tag_family(_B)
c_code local build_ref_114
	ret_reg &ref[114]
	call_c   build_ref_405()
	call_c   Dyam_Create_Unary(&ref[405],V(1))
	move_ret ref[114]
	c_ret

;; TERM 405: tag_family
c_code local build_ref_405
	ret_reg &ref[405]
	call_c   Dyam_Create_Atom("tag_family")
	move_ret ref[405]
	c_ret

long local pool_fun63[6]=[196610,build_ref_113,build_ref_114,pool_fun59,pool_fun61,pool_fun59]

pl_code local fun63
	call_c   Dyam_Pool(pool_fun63)
	call_c   Dyam_Unify_Item(&ref[113])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun62)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_variable(V(1))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun61)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[114])
	call_c   Dyam_Cut()
	pl_jump  fun59()

;; TERM 112: '*FIRST*'('call_normalize/2'(tag_tree{family=> _B, name=> _C, tree=> _D})) :> []
c_code local build_ref_112
	ret_reg &ref[112]
	call_c   build_ref_111()
	call_c   Dyam_Create_Binary(I(9),&ref[111],I(0))
	move_ret ref[112]
	c_ret

;; TERM 111: '*FIRST*'('call_normalize/2'(tag_tree{family=> _B, name=> _C, tree=> _D}))
c_code local build_ref_111
	ret_reg &ref[111]
	call_c   build_ref_97()
	call_c   build_ref_110()
	call_c   Dyam_Create_Unary(&ref[97],&ref[110])
	move_ret ref[111]
	c_ret

c_code local build_seed_13
	ret_reg &seed[13]
	call_c   build_ref_9()
	call_c   build_ref_135()
	call_c   Dyam_Seed_Start(&ref[9],&ref[135],I(0),fun1,1)
	call_c   build_ref_134()
	call_c   Dyam_Seed_Add_Comp(&ref[134],fun70,0)
	call_c   Dyam_Seed_End()
	move_ret seed[13]
	c_ret

;; TERM 134: '*GUARD*'(guard_factorize((_B ; _C), _D))
c_code local build_ref_134
	ret_reg &ref[134]
	call_c   build_ref_10()
	call_c   build_ref_133()
	call_c   Dyam_Create_Unary(&ref[10],&ref[133])
	move_ret ref[134]
	c_ret

;; TERM 133: guard_factorize((_B ; _C), _D)
c_code local build_ref_133
	ret_reg &ref[133]
	call_c   build_ref_382()
	call_c   build_ref_95()
	call_c   Dyam_Create_Binary(&ref[382],&ref[95],V(3))
	move_ret ref[133]
	c_ret

c_code local build_seed_33
	ret_reg &seed[33]
	call_c   build_ref_10()
	call_c   build_ref_137()
	call_c   Dyam_Seed_Start(&ref[10],&ref[137],I(0),fun9,1)
	call_c   build_ref_138()
	call_c   Dyam_Seed_Add_Comp(&ref[138],&ref[137],0)
	call_c   Dyam_Seed_End()
	move_ret seed[33]
	c_ret

;; TERM 138: '*GUARD*'(guard_factors((_B ; _C), [], _E)) :> '$$HOLE$$'
c_code local build_ref_138
	ret_reg &ref[138]
	call_c   build_ref_137()
	call_c   Dyam_Create_Binary(I(9),&ref[137],I(7))
	move_ret ref[138]
	c_ret

;; TERM 137: '*GUARD*'(guard_factors((_B ; _C), [], _E))
c_code local build_ref_137
	ret_reg &ref[137]
	call_c   build_ref_10()
	call_c   build_ref_136()
	call_c   Dyam_Create_Unary(&ref[10],&ref[136])
	move_ret ref[137]
	c_ret

;; TERM 136: guard_factors((_B ; _C), [], _E)
c_code local build_ref_136
	ret_reg &ref[136]
	call_c   build_ref_383()
	call_c   build_ref_95()
	call_c   Dyam_Term_Start(&ref[383],3)
	call_c   Dyam_Term_Arg(&ref[95])
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[136]
	c_ret

long local pool_fun69[2]=[1,build_ref_95]

pl_code local fun69
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(3),&ref[95])
	fail_ret
	pl_jump  fun25()

;; TERM 139: _F : _G
c_code local build_ref_139
	ret_reg &ref[139]
	call_c   build_ref_406()
	call_c   Dyam_Create_Binary(&ref[406],V(5),V(6))
	move_ret ref[139]
	c_ret

;; TERM 406: :
c_code local build_ref_406
	ret_reg &ref[406]
	call_c   Dyam_Create_Atom(":")
	move_ret ref[406]
	c_ret

long local pool_fun67[2]=[1,build_ref_95]

pl_code local fun68
	call_c   Dyam_Remove_Choice()
fun67:
	call_c   Dyam_Cut()
	move     &ref[95], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(5))
	move     V(9), R(4)
	move     S(5), R(5)
	move     V(10), R(6)
	move     S(5), R(7)
	pl_call  pred_guard_use_factor_4()
	call_c   Dyam_Reg_Load(0,V(5))
	call_c   Dyam_Reg_Load(2,V(9))
	move     V(11), R(4)
	move     S(5), R(5)
	pl_call  pred_guard_and_combine_3()
	call_c   Dyam_Reg_Load(0,V(11))
	call_c   Dyam_Reg_Load(2,V(10))
	call_c   Dyam_Reg_Load(4,V(3))
	pl_call  pred_guard_or_combine_3()
	pl_jump  fun25()


;; TERM 140: _H : _I
c_code local build_ref_140
	ret_reg &ref[140]
	call_c   build_ref_406()
	call_c   Dyam_Create_Binary(&ref[406],V(7),V(8))
	move_ret ref[140]
	c_ret

long local pool_fun70[7]=[131076,build_ref_134,build_seed_33,build_ref_139,build_ref_140,pool_fun69,pool_fun67]

pl_code local fun70
	call_c   Dyam_Pool(pool_fun70)
	call_c   Dyam_Unify_Item(&ref[134])
	fail_ret
	call_c   Dyam_Allocate(0)
	pl_call  fun16(&seed[33],1)
	call_c   Dyam_Choice(fun69)
	call_c   Dyam_Set_Cut()
	pl_call  Domain_2(&ref[139],V(4))
	call_c   DYAM_evpred_gt(V(6),N(1))
	fail_ret
	call_c   Dyam_Choice(fun68)
	call_c   Dyam_Set_Cut()
	pl_call  Domain_2(&ref[140],V(4))
	call_c   DYAM_evpred_gt(V(8),V(6))
	fail_ret
	call_c   Dyam_Cut()
	pl_fail

;; TERM 135: '*GUARD*'(guard_factorize((_B ; _C), _D)) :> []
c_code local build_ref_135
	ret_reg &ref[135]
	call_c   build_ref_134()
	call_c   Dyam_Create_Binary(I(9),&ref[134],I(0))
	move_ret ref[135]
	c_ret

c_code local build_seed_23
	ret_reg &seed[23]
	call_c   build_ref_9()
	call_c   build_ref_143()
	call_c   Dyam_Seed_Start(&ref[9],&ref[143],I(0),fun1,1)
	call_c   build_ref_142()
	call_c   Dyam_Seed_Add_Comp(&ref[142],fun73,0)
	call_c   Dyam_Seed_End()
	move_ret seed[23]
	c_ret

;; TERM 142: '*GUARD*'(normalize_args([_B|_C], _D, _E, _F, _G))
c_code local build_ref_142
	ret_reg &ref[142]
	call_c   build_ref_10()
	call_c   build_ref_141()
	call_c   Dyam_Create_Unary(&ref[10],&ref[141])
	move_ret ref[142]
	c_ret

;; TERM 141: normalize_args([_B|_C], _D, _E, _F, _G)
c_code local build_ref_141
	ret_reg &ref[141]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(1),V(2))
	move_ret R(0)
	call_c   build_ref_384()
	call_c   Dyam_Term_Start(&ref[384],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[141]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_34
	ret_reg &seed[34]
	call_c   build_ref_10()
	call_c   build_ref_145()
	call_c   Dyam_Seed_Start(&ref[10],&ref[145],I(0),fun9,1)
	call_c   build_ref_146()
	call_c   Dyam_Seed_Add_Comp(&ref[146],&ref[145],0)
	call_c   Dyam_Seed_End()
	move_ret seed[34]
	c_ret

;; TERM 146: '*GUARD*'(normalize(_B, _H, _E, _I, _G)) :> '$$HOLE$$'
c_code local build_ref_146
	ret_reg &ref[146]
	call_c   build_ref_145()
	call_c   Dyam_Create_Binary(I(9),&ref[145],I(7))
	move_ret ref[146]
	c_ret

;; TERM 145: '*GUARD*'(normalize(_B, _H, _E, _I, _G))
c_code local build_ref_145
	ret_reg &ref[145]
	call_c   build_ref_10()
	call_c   build_ref_144()
	call_c   Dyam_Create_Unary(&ref[10],&ref[144])
	move_ret ref[145]
	c_ret

;; TERM 144: normalize(_B, _H, _E, _I, _G)
c_code local build_ref_144
	ret_reg &ref[144]
	call_c   build_ref_387()
	call_c   Dyam_Term_Start(&ref[387],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[144]
	c_ret

c_code local build_seed_35
	ret_reg &seed[35]
	call_c   build_ref_10()
	call_c   build_ref_148()
	call_c   Dyam_Seed_Start(&ref[10],&ref[148],I(0),fun9,1)
	call_c   build_ref_149()
	call_c   Dyam_Seed_Add_Comp(&ref[149],&ref[148],0)
	call_c   Dyam_Seed_End()
	move_ret seed[35]
	c_ret

;; TERM 149: '*GUARD*'(normalize_args(_C, _J, _I, _F, _G)) :> '$$HOLE$$'
c_code local build_ref_149
	ret_reg &ref[149]
	call_c   build_ref_148()
	call_c   Dyam_Create_Binary(I(9),&ref[148],I(7))
	move_ret ref[149]
	c_ret

;; TERM 148: '*GUARD*'(normalize_args(_C, _J, _I, _F, _G))
c_code local build_ref_148
	ret_reg &ref[148]
	call_c   build_ref_10()
	call_c   build_ref_147()
	call_c   Dyam_Create_Unary(&ref[10],&ref[147])
	move_ret ref[148]
	c_ret

;; TERM 147: normalize_args(_C, _J, _I, _F, _G)
c_code local build_ref_147
	ret_reg &ref[147]
	call_c   build_ref_384()
	call_c   Dyam_Term_Start(&ref[384],5)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[147]
	c_ret

;; TERM 154: [_H|_J]
c_code local build_ref_154
	ret_reg &ref[154]
	call_c   Dyam_Create_List(V(7),V(9))
	move_ret ref[154]
	c_ret

long local pool_fun71[2]=[1,build_ref_154]

pl_code local fun71
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(3),&ref[154])
	fail_ret
	pl_jump  fun25()

;; TERM 150: [_K|_L]
c_code local build_ref_150
	ret_reg &ref[150]
	call_c   Dyam_Create_List(V(10),V(11))
	move_ret ref[150]
	c_ret

c_code local build_seed_36
	ret_reg &seed[36]
	call_c   build_ref_10()
	call_c   build_ref_152()
	call_c   Dyam_Seed_Start(&ref[10],&ref[152],I(0),fun9,1)
	call_c   build_ref_153()
	call_c   Dyam_Seed_Add_Comp(&ref[153],&ref[152],0)
	call_c   Dyam_Seed_End()
	move_ret seed[36]
	c_ret

;; TERM 153: '*GUARD*'(append(_H, _J, _D)) :> '$$HOLE$$'
c_code local build_ref_153
	ret_reg &ref[153]
	call_c   build_ref_152()
	call_c   Dyam_Create_Binary(I(9),&ref[152],I(7))
	move_ret ref[153]
	c_ret

;; TERM 152: '*GUARD*'(append(_H, _J, _D))
c_code local build_ref_152
	ret_reg &ref[152]
	call_c   build_ref_10()
	call_c   build_ref_151()
	call_c   Dyam_Create_Unary(&ref[10],&ref[151])
	move_ret ref[152]
	c_ret

;; TERM 151: append(_H, _J, _D)
c_code local build_ref_151
	ret_reg &ref[151]
	call_c   build_ref_407()
	call_c   Dyam_Term_Start(&ref[407],3)
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[151]
	c_ret

;; TERM 407: append
c_code local build_ref_407
	ret_reg &ref[407]
	call_c   Dyam_Create_Atom("append")
	move_ret ref[407]
	c_ret

long local pool_fun72[4]=[65538,build_ref_150,build_seed_36,pool_fun71]

pl_code local fun72
	call_c   Dyam_Update_Choice(fun71)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(7),&ref[150])
	fail_ret
	call_c   Dyam_Cut()
	pl_call  fun16(&seed[36],1)
	pl_jump  fun25()

long local pool_fun73[5]=[65539,build_ref_142,build_seed_34,build_seed_35,pool_fun72]

pl_code local fun73
	call_c   Dyam_Pool(pool_fun73)
	call_c   Dyam_Unify_Item(&ref[142])
	fail_ret
	call_c   Dyam_Allocate(0)
	pl_call  fun16(&seed[34],1)
	pl_call  fun16(&seed[35],1)
	call_c   Dyam_Choice(fun72)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(7),I(0))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(3),V(9))
	fail_ret
	pl_jump  fun25()

;; TERM 143: '*GUARD*'(normalize_args([_B|_C], _D, _E, _F, _G)) :> []
c_code local build_ref_143
	ret_reg &ref[143]
	call_c   build_ref_142()
	call_c   Dyam_Create_Binary(I(9),&ref[142],I(0))
	move_ret ref[143]
	c_ret

c_code local build_seed_17
	ret_reg &seed[17]
	call_c   build_ref_9()
	call_c   build_ref_165()
	call_c   Dyam_Seed_Start(&ref[9],&ref[165],I(0),fun1,1)
	call_c   build_ref_164()
	call_c   Dyam_Seed_Add_Comp(&ref[164],fun85,0)
	call_c   Dyam_Seed_End()
	move_ret seed[17]
	c_ret

;; TERM 164: '*GUARD*'(normalize((auxtree _B), tag_node{id=> _C, label=> _D, children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}, _O))
c_code local build_ref_164
	ret_reg &ref[164]
	call_c   build_ref_10()
	call_c   build_ref_163()
	call_c   Dyam_Create_Unary(&ref[10],&ref[163])
	move_ret ref[164]
	c_ret

;; TERM 163: normalize((auxtree _B), tag_node{id=> _C, label=> _D, children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}, _O)
c_code local build_ref_163
	ret_reg &ref[163]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_408()
	call_c   Dyam_Create_Unary(&ref[408],V(1))
	move_ret R(0)
	call_c   build_ref_409()
	call_c   Dyam_Term_Start(&ref[409],12)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_End()
	move_ret R(1)
	call_c   build_ref_387()
	call_c   Dyam_Term_Start(&ref[387],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_End()
	move_ret ref[163]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 409: tag_node!'$ft'
c_code local build_ref_409
	ret_reg &ref[409]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_422()
	call_c   Dyam_Create_List(&ref[422],I(0))
	move_ret R(0)
	call_c   build_ref_421()
	call_c   Dyam_Create_List(&ref[421],R(0))
	move_ret R(0)
	call_c   build_ref_420()
	call_c   Dyam_Create_List(&ref[420],R(0))
	move_ret R(0)
	call_c   build_ref_419()
	call_c   Dyam_Create_List(&ref[419],R(0))
	move_ret R(0)
	call_c   build_ref_418()
	call_c   Dyam_Create_List(&ref[418],R(0))
	move_ret R(0)
	call_c   build_ref_417()
	call_c   Dyam_Create_List(&ref[417],R(0))
	move_ret R(0)
	call_c   build_ref_416()
	call_c   Dyam_Create_List(&ref[416],R(0))
	move_ret R(0)
	call_c   build_ref_415()
	call_c   Dyam_Create_List(&ref[415],R(0))
	move_ret R(0)
	call_c   build_ref_414()
	call_c   Dyam_Create_List(&ref[414],R(0))
	move_ret R(0)
	call_c   build_ref_413()
	call_c   Dyam_Create_List(&ref[413],R(0))
	move_ret R(0)
	call_c   build_ref_412()
	call_c   Dyam_Create_List(&ref[412],R(0))
	move_ret R(0)
	call_c   build_ref_411()
	call_c   Dyam_Create_List(&ref[411],R(0))
	move_ret R(0)
	call_c   build_ref_410()
	call_c   Dyam_Create_Atom_Module("$ft",&ref[410])
	move_ret ref[409]
	call_c   DYAM_Feature_2(&ref[410],R(0))
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 410: tag_node
c_code local build_ref_410
	ret_reg &ref[410]
	call_c   Dyam_Create_Atom("tag_node")
	move_ret ref[410]
	c_ret

;; TERM 411: id
c_code local build_ref_411
	ret_reg &ref[411]
	call_c   Dyam_Create_Atom("id")
	move_ret ref[411]
	c_ret

;; TERM 412: label
c_code local build_ref_412
	ret_reg &ref[412]
	call_c   Dyam_Create_Atom("label")
	move_ret ref[412]
	c_ret

;; TERM 413: children
c_code local build_ref_413
	ret_reg &ref[413]
	call_c   Dyam_Create_Atom("children")
	move_ret ref[413]
	c_ret

;; TERM 414: kind
c_code local build_ref_414
	ret_reg &ref[414]
	call_c   Dyam_Create_Atom("kind")
	move_ret ref[414]
	c_ret

;; TERM 415: adj
c_code local build_ref_415
	ret_reg &ref[415]
	call_c   Dyam_Create_Atom("adj")
	move_ret ref[415]
	c_ret

;; TERM 416: spine
c_code local build_ref_416
	ret_reg &ref[416]
	call_c   Dyam_Create_Atom("spine")
	move_ret ref[416]
	c_ret

;; TERM 417: top
c_code local build_ref_417
	ret_reg &ref[417]
	call_c   Dyam_Create_Atom("top")
	move_ret ref[417]
	c_ret

;; TERM 418: bot
c_code local build_ref_418
	ret_reg &ref[418]
	call_c   Dyam_Create_Atom("bot")
	move_ret ref[418]
	c_ret

;; TERM 419: token
c_code local build_ref_419
	ret_reg &ref[419]
	call_c   Dyam_Create_Atom("token")
	move_ret ref[419]
	c_ret

;; TERM 420: adjleft
c_code local build_ref_420
	ret_reg &ref[420]
	call_c   Dyam_Create_Atom("adjleft")
	move_ret ref[420]
	c_ret

;; TERM 421: adjright
c_code local build_ref_421
	ret_reg &ref[421]
	call_c   Dyam_Create_Atom("adjright")
	move_ret ref[421]
	c_ret

;; TERM 422: adjwrap
c_code local build_ref_422
	ret_reg &ref[422]
	call_c   Dyam_Create_Atom("adjwrap")
	move_ret ref[422]
	c_ret

;; TERM 408: auxtree
c_code local build_ref_408
	ret_reg &ref[408]
	call_c   Dyam_Create_Atom("auxtree")
	move_ret ref[408]
	c_ret

c_code local build_seed_37
	ret_reg &seed[37]
	call_c   build_ref_10()
	call_c   build_ref_167()
	call_c   Dyam_Seed_Start(&ref[10],&ref[167],I(0),fun9,1)
	call_c   build_ref_168()
	call_c   Dyam_Seed_Add_Comp(&ref[168],&ref[167],0)
	call_c   Dyam_Seed_End()
	move_ret seed[37]
	c_ret

;; TERM 168: '*GUARD*'(normalize(_B, tag_node{id=> _C, label=> _D, children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}, left(_D), _P, _O)) :> '$$HOLE$$'
c_code local build_ref_168
	ret_reg &ref[168]
	call_c   build_ref_167()
	call_c   Dyam_Create_Binary(I(9),&ref[167],I(7))
	move_ret ref[168]
	c_ret

;; TERM 167: '*GUARD*'(normalize(_B, tag_node{id=> _C, label=> _D, children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}, left(_D), _P, _O))
c_code local build_ref_167
	ret_reg &ref[167]
	call_c   build_ref_10()
	call_c   build_ref_166()
	call_c   Dyam_Create_Unary(&ref[10],&ref[166])
	move_ret ref[167]
	c_ret

;; TERM 166: normalize(_B, tag_node{id=> _C, label=> _D, children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}, left(_D), _P, _O)
c_code local build_ref_166
	ret_reg &ref[166]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_409()
	call_c   Dyam_Term_Start(&ref[409],12)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_45()
	call_c   Dyam_Create_Unary(&ref[45],V(3))
	move_ret R(1)
	call_c   build_ref_387()
	call_c   Dyam_Term_Start(&ref[387],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_End()
	move_ret ref[166]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 45: left
c_code local build_ref_45
	ret_reg &ref[45]
	call_c   Dyam_Create_Atom("left")
	move_ret ref[45]
	c_ret

;; TERM 170: 'missing foot node in ~w\n'
c_code local build_ref_170
	ret_reg &ref[170]
	call_c   Dyam_Create_Atom("missing foot node in ~w\n")
	move_ret ref[170]
	c_ret

;; TERM 171: [_B]
c_code local build_ref_171
	ret_reg &ref[171]
	call_c   Dyam_Create_List(V(1),I(0))
	move_ret ref[171]
	c_ret

long local pool_fun84[3]=[2,build_ref_170,build_ref_171]

pl_code local fun84
	call_c   Dyam_Remove_Choice()
	move     &ref[170], R(0)
	move     0, R(1)
	move     &ref[171], R(2)
	move     S(5), R(3)
	pl_call  pred_error_2()
	pl_jump  fun25()

;; TERM 169: yes
c_code local build_ref_169
	ret_reg &ref[169]
	call_c   Dyam_Create_Atom("yes")
	move_ret ref[169]
	c_ret

;; TERM 44: right
c_code local build_ref_44
	ret_reg &ref[44]
	call_c   Dyam_Create_Atom("right")
	move_ret ref[44]
	c_ret

long local pool_fun85[6]=[65540,build_ref_164,build_seed_37,build_ref_169,build_ref_44,pool_fun84]

pl_code local fun85
	call_c   Dyam_Pool(pool_fun85)
	call_c   Dyam_Unify_Item(&ref[164])
	fail_ret
	call_c   Dyam_Allocate(0)
	pl_call  fun16(&seed[37],1)
	call_c   Dyam_Choice(fun84)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(7),&ref[169])
	fail_ret
	call_c   DYAM_sfol_identical(V(15),&ref[44])
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun25()

;; TERM 165: '*GUARD*'(normalize((auxtree _B), tag_node{id=> _C, label=> _D, children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}, _O)) :> []
c_code local build_ref_165
	ret_reg &ref[165]
	call_c   build_ref_164()
	call_c   Dyam_Create_Binary(I(9),&ref[164],I(0))
	move_ret ref[165]
	c_ret

c_code local build_seed_16
	ret_reg &seed[16]
	call_c   build_ref_9()
	call_c   build_ref_174()
	call_c   Dyam_Seed_Start(&ref[9],&ref[174],I(0),fun1,1)
	call_c   build_ref_173()
	call_c   Dyam_Seed_Add_Comp(&ref[173],fun86,0)
	call_c   Dyam_Seed_End()
	move_ret seed[16]
	c_ret

;; TERM 173: '*GUARD*'(normalize((spinetree _B), (spinetree tag_node{id=> _C, label=> _D, children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}), _O))
c_code local build_ref_173
	ret_reg &ref[173]
	call_c   build_ref_10()
	call_c   build_ref_172()
	call_c   Dyam_Create_Unary(&ref[10],&ref[172])
	move_ret ref[173]
	c_ret

;; TERM 172: normalize((spinetree _B), (spinetree tag_node{id=> _C, label=> _D, children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}), _O)
c_code local build_ref_172
	ret_reg &ref[172]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_423()
	call_c   Dyam_Create_Unary(&ref[423],V(1))
	move_ret R(0)
	call_c   build_ref_409()
	call_c   Dyam_Term_Start(&ref[409],12)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_End()
	move_ret R(1)
	call_c   Dyam_Create_Unary(&ref[423],R(1))
	move_ret R(1)
	call_c   build_ref_387()
	call_c   Dyam_Term_Start(&ref[387],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_End()
	move_ret ref[172]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 423: spinetree
c_code local build_ref_423
	ret_reg &ref[423]
	call_c   Dyam_Create_Atom("spinetree")
	move_ret ref[423]
	c_ret

long local pool_fun86[6]=[65540,build_ref_173,build_seed_37,build_ref_169,build_ref_44,pool_fun84]

pl_code local fun86
	call_c   Dyam_Pool(pool_fun86)
	call_c   Dyam_Unify_Item(&ref[173])
	fail_ret
	call_c   Dyam_Allocate(0)
	pl_call  fun16(&seed[37],1)
	call_c   Dyam_Choice(fun84)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(7),&ref[169])
	fail_ret
	call_c   DYAM_sfol_identical(V(15),&ref[44])
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun25()

;; TERM 174: '*GUARD*'(normalize((spinetree _B), (spinetree tag_node{id=> _C, label=> _D, children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}), _O)) :> []
c_code local build_ref_174
	ret_reg &ref[174]
	call_c   build_ref_173()
	call_c   Dyam_Create_Binary(I(9),&ref[173],I(0))
	move_ret ref[174]
	c_ret

c_code local build_seed_21
	ret_reg &seed[21]
	call_c   build_ref_9()
	call_c   build_ref_177()
	call_c   Dyam_Seed_Start(&ref[9],&ref[177],I(0),fun1,1)
	call_c   build_ref_176()
	call_c   Dyam_Seed_Add_Comp(&ref[176],fun117,0)
	call_c   Dyam_Seed_End()
	move_ret seed[21]
	c_ret

;; TERM 176: '*GUARD*'(misc_check(tag_node{id=> _B, label=> _C, children=> _D, kind=> _E, adj=> _F, spine=> _G, top=> _H, bot=> _I, token=> _J, adjleft=> _K, adjright=> _L, adjwrap=> _M}, _N, _O))
c_code local build_ref_176
	ret_reg &ref[176]
	call_c   build_ref_10()
	call_c   build_ref_175()
	call_c   Dyam_Create_Unary(&ref[10],&ref[175])
	move_ret ref[176]
	c_ret

;; TERM 175: misc_check(tag_node{id=> _B, label=> _C, children=> _D, kind=> _E, adj=> _F, spine=> _G, top=> _H, bot=> _I, token=> _J, adjleft=> _K, adjright=> _L, adjwrap=> _M}, _N, _O)
c_code local build_ref_175
	ret_reg &ref[175]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_409()
	call_c   Dyam_Term_Start(&ref[409],12)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_424()
	call_c   Dyam_Term_Start(&ref[424],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_End()
	move_ret ref[175]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 424: misc_check
c_code local build_ref_424
	ret_reg &ref[424]
	call_c   Dyam_Create_Atom("misc_check")
	move_ret ref[424]
	c_ret

c_code local build_seed_38
	ret_reg &seed[38]
	call_c   build_ref_10()
	call_c   build_ref_179()
	call_c   Dyam_Seed_Start(&ref[10],&ref[179],I(0),fun9,1)
	call_c   build_ref_180()
	call_c   Dyam_Seed_Add_Comp(&ref[180],&ref[179],0)
	call_c   Dyam_Seed_End()
	move_ret seed[38]
	c_ret

;; TERM 180: '*GUARD*'(name_tag_node(_B, _O)) :> '$$HOLE$$'
c_code local build_ref_180
	ret_reg &ref[180]
	call_c   build_ref_179()
	call_c   Dyam_Create_Binary(I(9),&ref[179],I(7))
	move_ret ref[180]
	c_ret

;; TERM 179: '*GUARD*'(name_tag_node(_B, _O))
c_code local build_ref_179
	ret_reg &ref[179]
	call_c   build_ref_10()
	call_c   build_ref_178()
	call_c   Dyam_Create_Unary(&ref[10],&ref[178])
	move_ret ref[179]
	c_ret

;; TERM 178: name_tag_node(_B, _O)
c_code local build_ref_178
	ret_reg &ref[178]
	call_c   build_ref_390()
	call_c   Dyam_Create_Binary(&ref[390],V(1),V(14))
	move_ret ref[178]
	c_ret

;; TERM 182: 'Token info ~w useless in tree for node ~w'
c_code local build_ref_182
	ret_reg &ref[182]
	call_c   Dyam_Create_Atom("Token info ~w useless in tree for node ~w")
	move_ret ref[182]
	c_ret

;; TERM 183: [_J,_O,_N]
c_code local build_ref_183
	ret_reg &ref[183]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(13),I(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(14),R(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(9),R(0))
	move_ret ref[183]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun87[3]=[2,build_ref_182,build_ref_183]

pl_code local fun87
	call_c   Dyam_Remove_Choice()
	move     &ref[182], R(0)
	move     0, R(1)
	move     &ref[183], R(2)
	move     S(5), R(3)
	pl_call  pred_warning_2()
	pl_jump  fun25()

long local pool_fun88[2]=[65536,pool_fun87]

pl_code local fun88
	call_c   Dyam_Update_Choice(fun87)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(9),I(0))
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun25()

;; TERM 181: [anchor,coanchor]
c_code local build_ref_181
	ret_reg &ref[181]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_349()
	call_c   Dyam_Create_List(&ref[349],I(0))
	move_ret R(0)
	call_c   build_ref_343()
	call_c   Dyam_Create_List(&ref[343],R(0))
	move_ret ref[181]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 343: anchor
c_code local build_ref_343
	ret_reg &ref[343]
	call_c   Dyam_Create_Atom("anchor")
	move_ret ref[343]
	c_ret

;; TERM 349: coanchor
c_code local build_ref_349
	ret_reg &ref[349]
	call_c   Dyam_Create_Atom("coanchor")
	move_ret ref[349]
	c_ret

long local pool_fun89[3]=[65537,build_ref_181,pool_fun88]

pl_code local fun90
	call_c   Dyam_Remove_Choice()
fun89:
	call_c   Dyam_Choice(fun88)
	call_c   Dyam_Set_Cut()
	pl_call  Domain_2(V(4),&ref[181])
	call_c   Dyam_Cut()
	pl_jump  fun25()


long local pool_fun91[3]=[131072,pool_fun89,pool_fun89]

pl_code local fun92
	call_c   Dyam_Remove_Choice()
fun91:
	call_c   Dyam_Choice(fun90)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(3),I(0))
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun89()


;; TERM 43: no
c_code local build_ref_43
	ret_reg &ref[43]
	call_c   Dyam_Create_Atom("no")
	move_ret ref[43]
	c_ret

long local pool_fun93[5]=[131074,build_seed_38,build_ref_43,pool_fun91,pool_fun91]

pl_code local fun94
	call_c   Dyam_Remove_Choice()
fun93:
	pl_call  fun16(&seed[38],1)
	call_c   Dyam_Choice(fun92)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(6),&ref[43])
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun91()


long local pool_fun101[3]=[131072,pool_fun93,pool_fun93]

pl_code local fun102
	call_c   Dyam_Remove_Choice()
fun101:
	call_c   Dyam_Choice(fun94)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(12),V(17))
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun93()


long local pool_fun103[3]=[131072,pool_fun101,pool_fun101]

pl_code local fun104
	call_c   Dyam_Remove_Choice()
fun103:
	call_c   Dyam_Choice(fun102)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(11),V(16))
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun101()


long local pool_fun107[4]=[131073,build_ref_169,pool_fun103,pool_fun103]

pl_code local fun107
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(5),&ref[169])
	fail_ret
	call_c   Dyam_Choice(fun104)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(10),V(15))
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun103()

;; TERM 186: strict
c_code local build_ref_186
	ret_reg &ref[186]
	call_c   Dyam_Create_Atom("strict")
	move_ret ref[186]
	c_ret

long local pool_fun105[4]=[131073,build_ref_186,pool_fun103,pool_fun103]

pl_code local fun106
	call_c   Dyam_Remove_Choice()
fun105:
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(5),&ref[186])
	fail_ret
	call_c   Dyam_Choice(fun104)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(10),V(15))
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun103()


long local pool_fun108[3]=[131072,pool_fun107,pool_fun105]

pl_code local fun108
	call_c   Dyam_Update_Choice(fun107)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Choice(fun106)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(7),V(8))
	fail_ret
	call_c   Dyam_Cut()
	pl_fail

long local pool_fun109[5]=[196609,build_ref_186,pool_fun108,pool_fun103,pool_fun103]

pl_code local fun109
	call_c   Dyam_Update_Choice(fun108)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(5),&ref[186])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun104)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(10),V(15))
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun103()

;; TERM 184: 'Conflicting features top ~w and bot ~w on node ~w'
c_code local build_ref_184
	ret_reg &ref[184]
	call_c   Dyam_Create_Atom("Conflicting features top ~w and bot ~w on node ~w")
	move_ret ref[184]
	c_ret

;; TERM 185: [_H,_I,_N]
c_code local build_ref_185
	ret_reg &ref[185]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(13),I(0))
	move_ret R(0)
	call_c   Dyam_Create_Tupple(7,8,R(0))
	move_ret ref[185]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun95[4]=[131073,build_ref_43,pool_fun93,pool_fun93]

pl_code local fun96
	call_c   Dyam_Remove_Choice()
fun95:
	call_c   Dyam_Choice(fun94)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(12),&ref[43])
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun93()


long local pool_fun97[4]=[131073,build_ref_43,pool_fun95,pool_fun95]

pl_code local fun98
	call_c   Dyam_Remove_Choice()
fun97:
	call_c   Dyam_Choice(fun96)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(11),&ref[43])
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun95()


long local pool_fun99[4]=[131073,build_ref_43,pool_fun97,pool_fun97]

long local pool_fun100[4]=[65538,build_ref_184,build_ref_185,pool_fun99]

pl_code local fun100
	call_c   Dyam_Remove_Choice()
	move     &ref[184], R(0)
	move     0, R(1)
	move     &ref[185], R(2)
	move     S(5), R(3)
	pl_call  pred_error_2()
fun99:
	call_c   Dyam_Choice(fun98)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(10),&ref[43])
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun97()


long local pool_fun110[5]=[196609,build_ref_43,pool_fun109,pool_fun100,pool_fun99]

long local pool_fun111[3]=[65537,build_ref_169,pool_fun110]

pl_code local fun111
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(17),&ref[169])
	fail_ret
fun110:
	call_c   Dyam_Choice(fun109)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(5),&ref[43])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun100)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(7),V(8))
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun99()


;; TERM 189: adjrestr((_S / _T), wrap, _R)
c_code local build_ref_189
	ret_reg &ref[189]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_395()
	call_c   Dyam_Create_Binary(&ref[395],V(18),V(19))
	move_ret R(0)
	call_c   build_ref_425()
	call_c   build_ref_426()
	call_c   Dyam_Term_Start(&ref[425],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[426])
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_End()
	move_ret ref[189]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 426: wrap
c_code local build_ref_426
	ret_reg &ref[426]
	call_c   Dyam_Create_Atom("wrap")
	move_ret ref[426]
	c_ret

;; TERM 425: adjrestr
c_code local build_ref_425
	ret_reg &ref[425]
	call_c   Dyam_Create_Atom("adjrestr")
	move_ret ref[425]
	c_ret

long local pool_fun112[4]=[131073,build_ref_189,pool_fun111,pool_fun110]

long local pool_fun113[3]=[65537,build_ref_169,pool_fun112]

pl_code local fun113
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(16),&ref[169])
	fail_ret
fun112:
	call_c   Dyam_Choice(fun111)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[189])
	call_c   Dyam_Cut()
	pl_jump  fun110()


;; TERM 188: adjrestr((_S / _T), right, _Q)
c_code local build_ref_188
	ret_reg &ref[188]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_395()
	call_c   Dyam_Create_Binary(&ref[395],V(18),V(19))
	move_ret R(0)
	call_c   build_ref_425()
	call_c   build_ref_44()
	call_c   Dyam_Term_Start(&ref[425],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[44])
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_End()
	move_ret ref[188]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun114[4]=[131073,build_ref_188,pool_fun113,pool_fun112]

long local pool_fun115[3]=[65537,build_ref_169,pool_fun114]

pl_code local fun115
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(15),&ref[169])
	fail_ret
fun114:
	call_c   Dyam_Choice(fun113)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[188])
	call_c   Dyam_Cut()
	pl_jump  fun112()


;; TERM 187: adjrestr((_S / _T), left, _P)
c_code local build_ref_187
	ret_reg &ref[187]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_395()
	call_c   Dyam_Create_Binary(&ref[395],V(18),V(19))
	move_ret R(0)
	call_c   build_ref_425()
	call_c   build_ref_45()
	call_c   Dyam_Term_Start(&ref[425],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[45])
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_End()
	move_ret ref[187]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun116[4]=[131073,build_ref_187,pool_fun115,pool_fun114]

pl_code local fun116
	call_c   Dyam_Remove_Choice()
	call_c   DYAM_evpred_functor(V(7),V(18),V(19))
	fail_ret
	call_c   Dyam_Choice(fun115)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[187])
	call_c   Dyam_Cut()
	pl_jump  fun114()

long local pool_fun117[5]=[131074,build_ref_176,build_ref_169,pool_fun116,pool_fun110]

pl_code local fun117
	call_c   Dyam_Pool(pool_fun117)
	call_c   Dyam_Unify_Item(&ref[176])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(2))
	call_c   Dyam_Reg_Load(2,V(7))
	call_c   Dyam_Reg_Load(4,V(8))
	pl_call  pred_feature_check_3()
	call_c   Dyam_Choice(fun116)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_variable(V(7))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(15),&ref[169])
	fail_ret
	call_c   Dyam_Unify(V(16),&ref[169])
	fail_ret
	call_c   Dyam_Unify(V(17),&ref[169])
	fail_ret
	pl_jump  fun110()

;; TERM 177: '*GUARD*'(misc_check(tag_node{id=> _B, label=> _C, children=> _D, kind=> _E, adj=> _F, spine=> _G, top=> _H, bot=> _I, token=> _J, adjleft=> _K, adjright=> _L, adjwrap=> _M}, _N, _O)) :> []
c_code local build_ref_177
	ret_reg &ref[177]
	call_c   build_ref_176()
	call_c   Dyam_Create_Binary(I(9),&ref[176],I(0))
	move_ret ref[177]
	c_ret

c_code local build_seed_22
	ret_reg &seed[22]
	call_c   build_ref_9()
	call_c   build_ref_226()
	call_c   Dyam_Seed_Start(&ref[9],&ref[226],I(0),fun1,1)
	call_c   build_ref_225()
	call_c   Dyam_Seed_Add_Comp(&ref[225],fun175,0)
	call_c   Dyam_Seed_End()
	move_ret seed[22]
	c_ret

;; TERM 225: '*GUARD*'(feature_normalize(_B, tag_node{id=> _C, label=> _D, children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}, _O))
c_code local build_ref_225
	ret_reg &ref[225]
	call_c   build_ref_10()
	call_c   build_ref_224()
	call_c   Dyam_Create_Unary(&ref[10],&ref[224])
	move_ret ref[225]
	c_ret

;; TERM 224: feature_normalize(_B, tag_node{id=> _C, label=> _D, children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}, _O)
c_code local build_ref_224
	ret_reg &ref[224]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_409()
	call_c   Dyam_Term_Start(&ref[409],12)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_427()
	call_c   Dyam_Term_Start(&ref[427],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_End()
	move_ret ref[224]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 427: feature_normalize
c_code local build_ref_427
	ret_reg &ref[427]
	call_c   Dyam_Create_Atom("feature_normalize")
	move_ret ref[427]
	c_ret

;; TERM 261: 'not a valid feature ~w at ~w'
c_code local build_ref_261
	ret_reg &ref[261]
	call_c   Dyam_Create_Atom("not a valid feature ~w at ~w")
	move_ret ref[261]
	c_ret

;; TERM 262: [_B,_O]
c_code local build_ref_262
	ret_reg &ref[262]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(14),I(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(1),R(0))
	move_ret ref[262]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun167[3]=[2,build_ref_261,build_ref_262]

pl_code local fun167
	call_c   Dyam_Remove_Choice()
	move     &ref[261], R(0)
	move     0, R(1)
	move     &ref[262], R(2)
	move     S(5), R(3)
	pl_call  pred_error_2()
	pl_jump  fun25()

;; TERM 256: adjwrap = _U
c_code local build_ref_256
	ret_reg &ref[256]
	call_c   build_ref_386()
	call_c   build_ref_422()
	call_c   Dyam_Create_Binary(&ref[386],&ref[422],V(20))
	move_ret ref[256]
	c_ret

;; TERM 259: 'adjwrap conflict ~w with ~w at ~w'
c_code local build_ref_259
	ret_reg &ref[259]
	call_c   Dyam_Create_Atom("adjwrap conflict ~w with ~w at ~w")
	move_ret ref[259]
	c_ret

;; TERM 260: [_U,_N,_O]
c_code local build_ref_260
	ret_reg &ref[260]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Tupple(13,14,I(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(20),R(0))
	move_ret ref[260]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 258: 'bad adjwrap value ~w at ~w'
c_code local build_ref_258
	ret_reg &ref[258]
	call_c   Dyam_Create_Atom("bad adjwrap value ~w at ~w")
	move_ret ref[258]
	c_ret

;; TERM 249: [_U,_O]
c_code local build_ref_249
	ret_reg &ref[249]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(14),I(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(20),R(0))
	move_ret ref[249]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun164[3]=[2,build_ref_258,build_ref_249]

pl_code local fun164
	call_c   Dyam_Remove_Choice()
	move     &ref[258], R(0)
	move     0, R(1)
	move     &ref[249], R(2)
	move     S(5), R(3)
	pl_call  pred_error_2()
	pl_jump  fun25()

;; TERM 257: '$VAR'(_Y, ['$SET$',adj(no, yes, strict, atmostone, one, sync),63])
c_code local build_ref_257
	ret_reg &ref[257]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_415()
	call_c   build_ref_43()
	call_c   build_ref_169()
	call_c   build_ref_186()
	call_c   build_ref_428()
	call_c   build_ref_429()
	call_c   build_ref_430()
	call_c   Dyam_Term_Start(&ref[415],6)
	call_c   Dyam_Term_Arg(&ref[43])
	call_c   Dyam_Term_Arg(&ref[169])
	call_c   Dyam_Term_Arg(&ref[186])
	call_c   Dyam_Term_Arg(&ref[428])
	call_c   Dyam_Term_Arg(&ref[429])
	call_c   Dyam_Term_Arg(&ref[430])
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   Dyam_Term_Start(I(8),3)
	call_c   Dyam_Term_Arg(V(24))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(N(63))
	call_c   Dyam_Term_End()
	move_ret ref[257]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 430: sync
c_code local build_ref_430
	ret_reg &ref[430]
	call_c   Dyam_Create_Atom("sync")
	move_ret ref[430]
	c_ret

;; TERM 429: one
c_code local build_ref_429
	ret_reg &ref[429]
	call_c   Dyam_Create_Atom("one")
	move_ret ref[429]
	c_ret

;; TERM 428: atmostone
c_code local build_ref_428
	ret_reg &ref[428]
	call_c   Dyam_Create_Atom("atmostone")
	move_ret ref[428]
	c_ret

long local pool_fun165[3]=[65537,build_ref_257,pool_fun164]

long local pool_fun166[4]=[65538,build_ref_259,build_ref_260,pool_fun165]

pl_code local fun166
	call_c   Dyam_Remove_Choice()
	move     &ref[259], R(0)
	move     0, R(1)
	move     &ref[260], R(2)
	move     S(5), R(3)
	pl_call  pred_error_2()
fun165:
	call_c   Dyam_Choice(fun164)
	call_c   Dyam_Set_Cut()
	pl_call  Domain_2(V(20),&ref[257])
	call_c   Dyam_Cut()
	pl_jump  fun25()


long local pool_fun168[5]=[196609,build_ref_256,pool_fun167,pool_fun166,pool_fun165]

pl_code local fun168
	call_c   Dyam_Update_Choice(fun167)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[256])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun166)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(13),V(20))
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun165()

;; TERM 252: adjright = _U
c_code local build_ref_252
	ret_reg &ref[252]
	call_c   build_ref_386()
	call_c   build_ref_421()
	call_c   Dyam_Create_Binary(&ref[386],&ref[421],V(20))
	move_ret ref[252]
	c_ret

;; TERM 255: 'adjright conflict ~w with ~w at ~w'
c_code local build_ref_255
	ret_reg &ref[255]
	call_c   Dyam_Create_Atom("adjright conflict ~w with ~w at ~w")
	move_ret ref[255]
	c_ret

;; TERM 251: [_U,_L,_O]
c_code local build_ref_251
	ret_reg &ref[251]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(14),I(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(11),R(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(20),R(0))
	move_ret ref[251]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 254: 'bad adjright value ~w at ~w'
c_code local build_ref_254
	ret_reg &ref[254]
	call_c   Dyam_Create_Atom("bad adjright value ~w at ~w")
	move_ret ref[254]
	c_ret

long local pool_fun161[3]=[2,build_ref_254,build_ref_249]

pl_code local fun161
	call_c   Dyam_Remove_Choice()
	move     &ref[254], R(0)
	move     0, R(1)
	move     &ref[249], R(2)
	move     S(5), R(3)
	pl_call  pred_error_2()
	pl_jump  fun25()

;; TERM 253: '$VAR'(_X, ['$SET$',adj(no, yes, strict, atmostone, one, sync),63])
c_code local build_ref_253
	ret_reg &ref[253]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_415()
	call_c   build_ref_43()
	call_c   build_ref_169()
	call_c   build_ref_186()
	call_c   build_ref_428()
	call_c   build_ref_429()
	call_c   build_ref_430()
	call_c   Dyam_Term_Start(&ref[415],6)
	call_c   Dyam_Term_Arg(&ref[43])
	call_c   Dyam_Term_Arg(&ref[169])
	call_c   Dyam_Term_Arg(&ref[186])
	call_c   Dyam_Term_Arg(&ref[428])
	call_c   Dyam_Term_Arg(&ref[429])
	call_c   Dyam_Term_Arg(&ref[430])
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   Dyam_Term_Start(I(8),3)
	call_c   Dyam_Term_Arg(V(23))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(N(63))
	call_c   Dyam_Term_End()
	move_ret ref[253]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun162[3]=[65537,build_ref_253,pool_fun161]

long local pool_fun163[4]=[65538,build_ref_255,build_ref_251,pool_fun162]

pl_code local fun163
	call_c   Dyam_Remove_Choice()
	move     &ref[255], R(0)
	move     0, R(1)
	move     &ref[251], R(2)
	move     S(5), R(3)
	pl_call  pred_error_2()
fun162:
	call_c   Dyam_Choice(fun161)
	call_c   Dyam_Set_Cut()
	pl_call  Domain_2(V(20),&ref[253])
	call_c   Dyam_Cut()
	pl_jump  fun25()


long local pool_fun169[5]=[196609,build_ref_252,pool_fun168,pool_fun163,pool_fun162]

pl_code local fun169
	call_c   Dyam_Update_Choice(fun168)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[252])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun163)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(12),V(20))
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun162()

;; TERM 246: adjleft = _U
c_code local build_ref_246
	ret_reg &ref[246]
	call_c   build_ref_386()
	call_c   build_ref_420()
	call_c   Dyam_Create_Binary(&ref[386],&ref[420],V(20))
	move_ret ref[246]
	c_ret

;; TERM 250: 'adjleft conflict ~w with ~w at ~w'
c_code local build_ref_250
	ret_reg &ref[250]
	call_c   Dyam_Create_Atom("adjleft conflict ~w with ~w at ~w")
	move_ret ref[250]
	c_ret

;; TERM 248: 'bad adjleft value ~w at ~w'
c_code local build_ref_248
	ret_reg &ref[248]
	call_c   Dyam_Create_Atom("bad adjleft value ~w at ~w")
	move_ret ref[248]
	c_ret

long local pool_fun158[3]=[2,build_ref_248,build_ref_249]

pl_code local fun158
	call_c   Dyam_Remove_Choice()
	move     &ref[248], R(0)
	move     0, R(1)
	move     &ref[249], R(2)
	move     S(5), R(3)
	pl_call  pred_error_2()
	pl_jump  fun25()

;; TERM 247: '$VAR'(_W, ['$SET$',adj(no, yes, strict, atmostone, one, sync),63])
c_code local build_ref_247
	ret_reg &ref[247]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_415()
	call_c   build_ref_43()
	call_c   build_ref_169()
	call_c   build_ref_186()
	call_c   build_ref_428()
	call_c   build_ref_429()
	call_c   build_ref_430()
	call_c   Dyam_Term_Start(&ref[415],6)
	call_c   Dyam_Term_Arg(&ref[43])
	call_c   Dyam_Term_Arg(&ref[169])
	call_c   Dyam_Term_Arg(&ref[186])
	call_c   Dyam_Term_Arg(&ref[428])
	call_c   Dyam_Term_Arg(&ref[429])
	call_c   Dyam_Term_Arg(&ref[430])
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   Dyam_Term_Start(I(8),3)
	call_c   Dyam_Term_Arg(V(22))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(N(63))
	call_c   Dyam_Term_End()
	move_ret ref[247]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun159[3]=[65537,build_ref_247,pool_fun158]

long local pool_fun160[4]=[65538,build_ref_250,build_ref_251,pool_fun159]

pl_code local fun160
	call_c   Dyam_Remove_Choice()
	move     &ref[250], R(0)
	move     0, R(1)
	move     &ref[251], R(2)
	move     S(5), R(3)
	pl_call  pred_error_2()
fun159:
	call_c   Dyam_Choice(fun158)
	call_c   Dyam_Set_Cut()
	pl_call  Domain_2(V(20),&ref[247])
	call_c   Dyam_Cut()
	pl_jump  fun25()


long local pool_fun170[5]=[196609,build_ref_246,pool_fun169,pool_fun160,pool_fun159]

pl_code local fun170
	call_c   Dyam_Update_Choice(fun169)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[246])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun160)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(11),V(20))
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun159()

;; TERM 243: token = _U
c_code local build_ref_243
	ret_reg &ref[243]
	call_c   build_ref_386()
	call_c   build_ref_419()
	call_c   Dyam_Create_Binary(&ref[386],&ref[419],V(20))
	move_ret ref[243]
	c_ret

;; TERM 244: 'node token conflict ~w with ~w at ~w'
c_code local build_ref_244
	ret_reg &ref[244]
	call_c   Dyam_Create_Atom("node token conflict ~w with ~w at ~w")
	move_ret ref[244]
	c_ret

;; TERM 245: [_U,_K,_O]
c_code local build_ref_245
	ret_reg &ref[245]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(14),I(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(10),R(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(20),R(0))
	move_ret ref[245]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun157[3]=[2,build_ref_244,build_ref_245]

pl_code local fun157
	call_c   Dyam_Remove_Choice()
	move     &ref[244], R(0)
	move     0, R(1)
	move     &ref[245], R(2)
	move     S(5), R(3)
	pl_call  pred_error_2()
	pl_jump  fun25()

long local pool_fun171[4]=[131073,build_ref_243,pool_fun170,pool_fun157]

pl_code local fun171
	call_c   Dyam_Update_Choice(fun170)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[243])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun157)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(20),V(10))
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun25()

;; TERM 240: id = _U
c_code local build_ref_240
	ret_reg &ref[240]
	call_c   build_ref_386()
	call_c   build_ref_411()
	call_c   Dyam_Create_Binary(&ref[386],&ref[411],V(20))
	move_ret ref[240]
	c_ret

;; TERM 241: 'node id conflict ~w with ~w at ~w'
c_code local build_ref_241
	ret_reg &ref[241]
	call_c   Dyam_Create_Atom("node id conflict ~w with ~w at ~w")
	move_ret ref[241]
	c_ret

;; TERM 242: [_U,_C,_O]
c_code local build_ref_242
	ret_reg &ref[242]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(14),I(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(2),R(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(20),R(0))
	move_ret ref[242]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun156[3]=[2,build_ref_241,build_ref_242]

pl_code local fun156
	call_c   Dyam_Remove_Choice()
	move     &ref[241], R(0)
	move     0, R(1)
	move     &ref[242], R(2)
	move     S(5), R(3)
	pl_call  pred_error_2()
	pl_jump  fun25()

long local pool_fun172[4]=[131073,build_ref_240,pool_fun171,pool_fun156]

pl_code local fun172
	call_c   Dyam_Update_Choice(fun171)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[240])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun156)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(20),V(2))
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun25()

;; TERM 238: bot = _P
c_code local build_ref_238
	ret_reg &ref[238]
	call_c   build_ref_386()
	call_c   build_ref_418()
	call_c   Dyam_Create_Binary(&ref[386],&ref[418],V(15))
	move_ret ref[238]
	c_ret

;; TERM 235: tag((_R / _S))
c_code local build_ref_235
	ret_reg &ref[235]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_395()
	call_c   Dyam_Create_Binary(&ref[395],V(17),V(18))
	move_ret R(0)
	call_c   build_ref_394()
	call_c   Dyam_Create_Unary(&ref[394],R(0))
	move_ret ref[235]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 239: 'bot feature conflict ~w with ~w at ~w'
c_code local build_ref_239
	ret_reg &ref[239]
	call_c   Dyam_Create_Atom("bot feature conflict ~w with ~w at ~w")
	move_ret ref[239]
	c_ret

;; TERM 237: [_U,_I,_O]
c_code local build_ref_237
	ret_reg &ref[237]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(14),I(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(8),R(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(20),R(0))
	move_ret ref[237]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun154[3]=[2,build_ref_239,build_ref_237]

pl_code local fun154
	call_c   Dyam_Remove_Choice()
	move     &ref[239], R(0)
	move     0, R(1)
	move     &ref[237], R(2)
	move     S(5), R(3)
	pl_call  pred_error_2()
	pl_jump  fun25()

long local pool_fun155[3]=[65537,build_ref_235,pool_fun154]

pl_code local fun155
	call_c   Dyam_Remove_Choice()
	call_c   DYAM_evpred_functor(V(15),V(17),V(18))
	fail_ret
	move     &ref[235], R(0)
	move     S(5), R(1)
	move     V(21), R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Load(4,V(15))
	move     V(20), R(6)
	move     S(5), R(7)
	pl_call  pred_term_current_module_shift_4()
	call_c   Dyam_Choice(fun154)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(20),V(9))
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun25()

long local pool_fun173[4]=[131073,build_ref_238,pool_fun172,pool_fun155]

pl_code local fun173
	call_c   Dyam_Update_Choice(fun172)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[238])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun155)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_variable(V(15))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(9),V(15))
	fail_ret
	pl_jump  fun25()

;; TERM 234: top = _P
c_code local build_ref_234
	ret_reg &ref[234]
	call_c   build_ref_386()
	call_c   build_ref_417()
	call_c   Dyam_Create_Binary(&ref[386],&ref[417],V(15))
	move_ret ref[234]
	c_ret

;; TERM 236: 'top feature conflict ~w with ~w at ~w'
c_code local build_ref_236
	ret_reg &ref[236]
	call_c   Dyam_Create_Atom("top feature conflict ~w with ~w at ~w")
	move_ret ref[236]
	c_ret

long local pool_fun152[3]=[2,build_ref_236,build_ref_237]

pl_code local fun152
	call_c   Dyam_Remove_Choice()
	move     &ref[236], R(0)
	move     0, R(1)
	move     &ref[237], R(2)
	move     S(5), R(3)
	pl_call  pred_error_2()
	pl_jump  fun25()

long local pool_fun153[3]=[65537,build_ref_235,pool_fun152]

pl_code local fun153
	call_c   Dyam_Remove_Choice()
	call_c   DYAM_evpred_functor(V(15),V(17),V(18))
	fail_ret
	move     &ref[235], R(0)
	move     S(5), R(1)
	move     V(19), R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Load(4,V(15))
	move     V(20), R(6)
	move     S(5), R(7)
	pl_call  pred_term_current_module_shift_4()
	call_c   Dyam_Choice(fun152)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(20),V(8))
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun25()

long local pool_fun174[4]=[131073,build_ref_234,pool_fun173,pool_fun153]

pl_code local fun174
	call_c   Dyam_Update_Choice(fun173)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[234])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun153)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_variable(V(15))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(8),V(15))
	fail_ret
	pl_jump  fun25()

;; TERM 227: _P and _Q
c_code local build_ref_227
	ret_reg &ref[227]
	call_c   build_ref_431()
	call_c   Dyam_Create_Binary(&ref[431],V(15),V(16))
	move_ret ref[227]
	c_ret

;; TERM 431: and
c_code local build_ref_431
	ret_reg &ref[431]
	call_c   Dyam_Create_Atom("and")
	move_ret ref[431]
	c_ret

c_code local build_seed_40
	ret_reg &seed[40]
	call_c   build_ref_10()
	call_c   build_ref_229()
	call_c   Dyam_Seed_Start(&ref[10],&ref[229],I(0),fun9,1)
	call_c   build_ref_230()
	call_c   Dyam_Seed_Add_Comp(&ref[230],&ref[229],0)
	call_c   Dyam_Seed_End()
	move_ret seed[40]
	c_ret

;; TERM 230: '*GUARD*'(feature_normalize(_P, tag_node{id=> _C, label=> _D, children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}, _O)) :> '$$HOLE$$'
c_code local build_ref_230
	ret_reg &ref[230]
	call_c   build_ref_229()
	call_c   Dyam_Create_Binary(I(9),&ref[229],I(7))
	move_ret ref[230]
	c_ret

;; TERM 229: '*GUARD*'(feature_normalize(_P, tag_node{id=> _C, label=> _D, children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}, _O))
c_code local build_ref_229
	ret_reg &ref[229]
	call_c   build_ref_10()
	call_c   build_ref_228()
	call_c   Dyam_Create_Unary(&ref[10],&ref[228])
	move_ret ref[229]
	c_ret

;; TERM 228: feature_normalize(_P, tag_node{id=> _C, label=> _D, children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}, _O)
c_code local build_ref_228
	ret_reg &ref[228]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_409()
	call_c   Dyam_Term_Start(&ref[409],12)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_427()
	call_c   Dyam_Term_Start(&ref[427],3)
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_End()
	move_ret ref[228]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_41
	ret_reg &seed[41]
	call_c   build_ref_10()
	call_c   build_ref_232()
	call_c   Dyam_Seed_Start(&ref[10],&ref[232],I(0),fun9,1)
	call_c   build_ref_233()
	call_c   Dyam_Seed_Add_Comp(&ref[233],&ref[232],0)
	call_c   Dyam_Seed_End()
	move_ret seed[41]
	c_ret

;; TERM 233: '*GUARD*'(feature_normalize(_Q, tag_node{id=> _C, label=> _D, children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}, _O)) :> '$$HOLE$$'
c_code local build_ref_233
	ret_reg &ref[233]
	call_c   build_ref_232()
	call_c   Dyam_Create_Binary(I(9),&ref[232],I(7))
	move_ret ref[233]
	c_ret

;; TERM 232: '*GUARD*'(feature_normalize(_Q, tag_node{id=> _C, label=> _D, children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}, _O))
c_code local build_ref_232
	ret_reg &ref[232]
	call_c   build_ref_10()
	call_c   build_ref_231()
	call_c   Dyam_Create_Unary(&ref[10],&ref[231])
	move_ret ref[232]
	c_ret

;; TERM 231: feature_normalize(_Q, tag_node{id=> _C, label=> _D, children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}, _O)
c_code local build_ref_231
	ret_reg &ref[231]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_409()
	call_c   Dyam_Term_Start(&ref[409],12)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_427()
	call_c   Dyam_Term_Start(&ref[427],3)
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_End()
	move_ret ref[231]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun175[6]=[65540,build_ref_225,build_ref_227,build_seed_40,build_seed_41,pool_fun174]

pl_code local fun175
	call_c   Dyam_Pool(pool_fun175)
	call_c   Dyam_Unify_Item(&ref[225])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun174)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[227])
	fail_ret
	call_c   Dyam_Cut()
	pl_call  fun16(&seed[40],1)
	pl_call  fun16(&seed[41],1)
	pl_jump  fun25()

;; TERM 226: '*GUARD*'(feature_normalize(_B, tag_node{id=> _C, label=> _D, children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}, _O)) :> []
c_code local build_ref_226
	ret_reg &ref[226]
	call_c   build_ref_225()
	call_c   Dyam_Create_Binary(I(9),&ref[225],I(0))
	move_ret ref[226]
	c_ret

c_code local build_seed_24
	ret_reg &seed[24]
	call_c   build_ref_9()
	call_c   build_ref_265()
	call_c   Dyam_Seed_Start(&ref[9],&ref[265],I(0),fun1,1)
	call_c   build_ref_264()
	call_c   Dyam_Seed_Add_Comp(&ref[264],fun254,0)
	call_c   Dyam_Seed_End()
	move_ret seed[24]
	c_ret

;; TERM 264: '*GUARD*'(normalize(_B, _C, _D, _E, _F))
c_code local build_ref_264
	ret_reg &ref[264]
	call_c   build_ref_10()
	call_c   build_ref_263()
	call_c   Dyam_Create_Unary(&ref[10],&ref[263])
	move_ret ref[264]
	c_ret

;; TERM 263: normalize(_B, _C, _D, _E, _F)
c_code local build_ref_263
	ret_reg &ref[263]
	call_c   build_ref_387()
	call_c   Dyam_Term_Start(&ref[387],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[263]
	c_ret

;; TERM 266: tag_node{id=> _H, label=> _I, children=> _J, kind=> _K, adj=> '$VAR'(_L, ['$SET$',adj(no, yes, strict, atmostone, one, sync),63]), spine=> _M, top=> _N, bot=> _O, token=> _P, adjleft=> '$VAR'(_Q, ['$SET$',adj(no, yes, strict, atmostone, one, sync),63]), adjright=> '$VAR'(_R, ['$SET$',adj(no, yes, strict, atmostone, one, sync),63]), adjwrap=> '$VAR'(_S, ['$SET$',adj(no, yes, strict, atmostone, one, sync),63])}
c_code local build_ref_266
	ret_reg &ref[266]
	call_c   Dyam_Pseudo_Choice(3)
	call_c   build_ref_415()
	call_c   build_ref_43()
	call_c   build_ref_169()
	call_c   build_ref_186()
	call_c   build_ref_428()
	call_c   build_ref_429()
	call_c   build_ref_430()
	call_c   Dyam_Term_Start(&ref[415],6)
	call_c   Dyam_Term_Arg(&ref[43])
	call_c   Dyam_Term_Arg(&ref[169])
	call_c   Dyam_Term_Arg(&ref[186])
	call_c   Dyam_Term_Arg(&ref[428])
	call_c   Dyam_Term_Arg(&ref[429])
	call_c   Dyam_Term_Arg(&ref[430])
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   Dyam_Term_Start(I(8),3)
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(N(63))
	call_c   Dyam_Term_End()
	move_ret R(1)
	call_c   Dyam_Term_Start(I(8),3)
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(N(63))
	call_c   Dyam_Term_End()
	move_ret R(2)
	call_c   Dyam_Term_Start(I(8),3)
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(N(63))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_409()
	call_c   build_ref_268()
	call_c   Dyam_Term_Start(&ref[409],12)
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(&ref[268])
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(R(2))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_End()
	move_ret ref[266]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 268: '$VAR'(_L, ['$SET$',adj(no, yes, strict, atmostone, one, sync),63])
c_code local build_ref_268
	ret_reg &ref[268]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_415()
	call_c   build_ref_43()
	call_c   build_ref_169()
	call_c   build_ref_186()
	call_c   build_ref_428()
	call_c   build_ref_429()
	call_c   build_ref_430()
	call_c   Dyam_Term_Start(&ref[415],6)
	call_c   Dyam_Term_Arg(&ref[43])
	call_c   Dyam_Term_Arg(&ref[169])
	call_c   Dyam_Term_Arg(&ref[186])
	call_c   Dyam_Term_Arg(&ref[428])
	call_c   Dyam_Term_Arg(&ref[429])
	call_c   Dyam_Term_Arg(&ref[430])
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   Dyam_Term_Start(I(8),3)
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(N(63))
	call_c   Dyam_Term_End()
	move_ret ref[268]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 380: std
c_code local build_ref_380
	ret_reg &ref[380]
	call_c   Dyam_Create_Atom("std")
	move_ret ref[380]
	c_ret

;; TERM 381: [_I|_W]
c_code local build_ref_381
	ret_reg &ref[381]
	call_c   Dyam_Create_List(V(8),V(22))
	move_ret ref[381]
	c_ret

c_code local build_seed_43
	ret_reg &seed[43]
	call_c   build_ref_10()
	call_c   build_ref_281()
	call_c   Dyam_Seed_Start(&ref[10],&ref[281],I(0),fun9,1)
	call_c   build_ref_282()
	call_c   Dyam_Seed_Add_Comp(&ref[282],&ref[281],0)
	call_c   Dyam_Seed_End()
	move_ret seed[43]
	c_ret

;; TERM 282: '*GUARD*'(normalize_args(_W, _J, _D, _E, _F)) :> '$$HOLE$$'
c_code local build_ref_282
	ret_reg &ref[282]
	call_c   build_ref_281()
	call_c   Dyam_Create_Binary(I(9),&ref[281],I(7))
	move_ret ref[282]
	c_ret

;; TERM 281: '*GUARD*'(normalize_args(_W, _J, _D, _E, _F))
c_code local build_ref_281
	ret_reg &ref[281]
	call_c   build_ref_10()
	call_c   build_ref_280()
	call_c   Dyam_Create_Unary(&ref[10],&ref[280])
	move_ret ref[281]
	c_ret

;; TERM 280: normalize_args(_W, _J, _D, _E, _F)
c_code local build_ref_280
	ret_reg &ref[280]
	call_c   build_ref_384()
	call_c   Dyam_Term_Start(&ref[384],5)
	call_c   Dyam_Term_Arg(V(22))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[280]
	c_ret

c_code local build_seed_42
	ret_reg &seed[42]
	call_c   build_ref_10()
	call_c   build_ref_271()
	call_c   Dyam_Seed_Start(&ref[10],&ref[271],I(0),fun9,1)
	call_c   build_ref_272()
	call_c   Dyam_Seed_Add_Comp(&ref[272],&ref[271],0)
	call_c   Dyam_Seed_End()
	move_ret seed[42]
	c_ret

;; TERM 272: '*GUARD*'(misc_check(_C, _B, _F)) :> '$$HOLE$$'
c_code local build_ref_272
	ret_reg &ref[272]
	call_c   build_ref_271()
	call_c   Dyam_Create_Binary(I(9),&ref[271],I(7))
	move_ret ref[272]
	c_ret

;; TERM 271: '*GUARD*'(misc_check(_C, _B, _F))
c_code local build_ref_271
	ret_reg &ref[271]
	call_c   build_ref_10()
	call_c   build_ref_270()
	call_c   Dyam_Create_Unary(&ref[10],&ref[270])
	move_ret ref[271]
	c_ret

;; TERM 270: misc_check(_C, _B, _F)
c_code local build_ref_270
	ret_reg &ref[270]
	call_c   build_ref_424()
	call_c   Dyam_Term_Start(&ref[424],3)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[270]
	c_ret

;; TERM 273: tag_anchor / 2
c_code local build_ref_273
	ret_reg &ref[273]
	call_c   build_ref_395()
	call_c   build_ref_432()
	call_c   Dyam_Create_Binary(&ref[395],&ref[432],N(2))
	move_ret ref[273]
	c_ret

;; TERM 432: tag_anchor
c_code local build_ref_432
	ret_reg &ref[432]
	call_c   Dyam_Create_Atom("tag_anchor")
	move_ret ref[432]
	c_ret

;; TERM 274: tag_node_id / 2
c_code local build_ref_274
	ret_reg &ref[274]
	call_c   build_ref_395()
	call_c   build_ref_392()
	call_c   Dyam_Create_Binary(&ref[395],&ref[392],N(2))
	move_ret ref[274]
	c_ret

long local pool_fun176[3]=[2,build_ref_273,build_ref_274]

long local pool_fun182[3]=[65537,build_seed_42,pool_fun176]

pl_code local fun185
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Reg_Load(0,V(3))
	call_c   Dyam_Reg_Load(2,V(12))
	pl_call  pred_set_spine_2()
fun182:
	pl_call  fun16(&seed[42],1)
fun176:
	move     &ref[273], R(0)
	move     S(5), R(1)
	pl_call  pred_abolish_1()
	move     &ref[274], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Deallocate(1)
	pl_jump  pred_abolish_1()



long local pool_fun183[3]=[65537,build_ref_169,pool_fun182]

pl_code local fun184
	call_c   Dyam_Remove_Choice()
fun183:
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(12),&ref[169])
	fail_ret
	pl_jump  fun182()


long local pool_fun231[6]=[131075,build_ref_380,build_ref_381,build_seed_43,pool_fun182,pool_fun183]

pl_code local fun231
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(10),&ref[380])
	fail_ret
	call_c   Dyam_Unify(V(2),V(6))
	fail_ret
	call_c   DYAM_evpred_univ(V(1),&ref[381])
	fail_ret
	pl_call  fun16(&seed[43],1)
	call_c   Dyam_Choice(fun185)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Choice(fun184)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(3),V(4))
	fail_ret
	call_c   Dyam_Cut()
	pl_fail

;; TERM 379: '$pos'(_U2)
c_code local build_ref_379
	ret_reg &ref[379]
	call_c   build_ref_433()
	call_c   Dyam_Create_Unary(&ref[433],V(72))
	move_ret ref[379]
	c_ret

;; TERM 433: '$pos'
c_code local build_ref_433
	ret_reg &ref[433]
	call_c   Dyam_Create_Atom("$pos")
	move_ret ref[433]
	c_ret

long local pool_fun232[4]=[131073,build_ref_379,pool_fun231,pool_fun176]

pl_code local fun232
	call_c   Dyam_Update_Choice(fun231)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[379])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(2),V(1))
	fail_ret
	call_c   Dyam_Unify(V(3),V(4))
	fail_ret
	pl_jump  fun176()

;; TERM 377: '$protect'(_X)
c_code local build_ref_377
	ret_reg &ref[377]
	call_c   build_ref_434()
	call_c   Dyam_Create_Unary(&ref[434],V(23))
	move_ret ref[377]
	c_ret

;; TERM 434: '$protect'
c_code local build_ref_434
	ret_reg &ref[434]
	call_c   Dyam_Create_Atom("$protect")
	move_ret ref[434]
	c_ret

;; TERM 378: '$protect'(_Z)
c_code local build_ref_378
	ret_reg &ref[378]
	call_c   build_ref_434()
	call_c   Dyam_Create_Unary(&ref[434],V(25))
	move_ret ref[378]
	c_ret

c_code local build_seed_44
	ret_reg &seed[44]
	call_c   build_ref_10()
	call_c   build_ref_286()
	call_c   Dyam_Seed_Start(&ref[10],&ref[286],I(0),fun9,1)
	call_c   build_ref_287()
	call_c   Dyam_Seed_Add_Comp(&ref[287],&ref[286],0)
	call_c   Dyam_Seed_End()
	move_ret seed[44]
	c_ret

;; TERM 287: '*GUARD*'(normalize(_X, _Z, _D, _E, _F)) :> '$$HOLE$$'
c_code local build_ref_287
	ret_reg &ref[287]
	call_c   build_ref_286()
	call_c   Dyam_Create_Binary(I(9),&ref[286],I(7))
	move_ret ref[287]
	c_ret

;; TERM 286: '*GUARD*'(normalize(_X, _Z, _D, _E, _F))
c_code local build_ref_286
	ret_reg &ref[286]
	call_c   build_ref_10()
	call_c   build_ref_285()
	call_c   Dyam_Create_Unary(&ref[10],&ref[285])
	move_ret ref[286]
	c_ret

;; TERM 285: normalize(_X, _Z, _D, _E, _F)
c_code local build_ref_285
	ret_reg &ref[285]
	call_c   build_ref_387()
	call_c   Dyam_Term_Start(&ref[387],5)
	call_c   Dyam_Term_Arg(V(23))
	call_c   Dyam_Term_Arg(V(25))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[285]
	c_ret

long local pool_fun233[6]=[131075,build_ref_377,build_ref_378,build_seed_44,pool_fun232,pool_fun176]

pl_code local fun233
	call_c   Dyam_Update_Choice(fun232)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[377])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(2),&ref[378])
	fail_ret
	pl_call  fun16(&seed[44],1)
	pl_jump  fun176()

;; TERM 375: '$answers'(_X)
c_code local build_ref_375
	ret_reg &ref[375]
	call_c   build_ref_435()
	call_c   Dyam_Create_Unary(&ref[435],V(23))
	move_ret ref[375]
	c_ret

;; TERM 435: '$answers'
c_code local build_ref_435
	ret_reg &ref[435]
	call_c   Dyam_Create_Atom("$answers")
	move_ret ref[435]
	c_ret

;; TERM 376: '$answers'(_Z)
c_code local build_ref_376
	ret_reg &ref[376]
	call_c   build_ref_435()
	call_c   Dyam_Create_Unary(&ref[435],V(25))
	move_ret ref[376]
	c_ret

long local pool_fun234[6]=[131075,build_ref_375,build_ref_376,build_seed_44,pool_fun233,pool_fun176]

pl_code local fun234
	call_c   Dyam_Update_Choice(fun233)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[375])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(2),&ref[376])
	fail_ret
	pl_call  fun16(&seed[44],1)
	pl_jump  fun176()

;; TERM 372: {_I}
c_code local build_ref_372
	ret_reg &ref[372]
	call_c   Dyam_Create_Unary(I(2),V(8))
	move_ret ref[372]
	c_ret

;; TERM 275: 'Not a valid TAG adjonction node ~w'
c_code local build_ref_275
	ret_reg &ref[275]
	call_c   Dyam_Create_Atom("Not a valid TAG adjonction node ~w")
	move_ret ref[275]
	c_ret

;; TERM 373: escape
c_code local build_ref_373
	ret_reg &ref[373]
	call_c   Dyam_Create_Atom("escape")
	move_ret ref[373]
	c_ret

;; TERM 374: 'Features on escape  node'
c_code local build_ref_374
	ret_reg &ref[374]
	call_c   Dyam_Create_Atom("Features on escape  node")
	move_ret ref[374]
	c_ret

long local pool_fun228[4]=[65538,build_ref_374,build_ref_171,pool_fun176]

pl_code local fun228
	call_c   Dyam_Remove_Choice()
	move     &ref[374], R(0)
	move     0, R(1)
	move     &ref[171], R(2)
	move     S(5), R(3)
	pl_call  pred_error_2()
	pl_jump  fun176()

long local pool_fun229[4]=[131073,build_ref_373,pool_fun228,pool_fun176]

long local pool_fun230[4]=[65538,build_ref_275,build_ref_171,pool_fun229]

pl_code local fun230
	call_c   Dyam_Remove_Choice()
	move     &ref[275], R(0)
	move     0, R(1)
	move     &ref[171], R(2)
	move     S(5), R(3)
	pl_call  pred_error_2()
fun229:
	call_c   Dyam_Unify(V(10),&ref[373])
	fail_ret
	call_c   Dyam_Unify(V(9),I(0))
	fail_ret
	call_c   Dyam_Reg_Load(0,V(3))
	call_c   Dyam_Reg_Load(2,V(12))
	pl_call  pred_set_spine_2()
	call_c   Dyam_Unify(V(3),V(4))
	fail_ret
	call_c   Dyam_Unify(V(7),I(0))
	fail_ret
	call_c   Dyam_Choice(fun228)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(13),I(0))
	fail_ret
	call_c   Dyam_Unify(V(14),I(0))
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun176()


long local pool_fun235[7]=[196611,build_ref_372,build_ref_43,build_ref_268,pool_fun234,pool_fun230,pool_fun229]

pl_code local fun235
	call_c   Dyam_Update_Choice(fun234)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[372])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(2),V(6))
	fail_ret
	call_c   Dyam_Choice(fun230)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(&ref[268],&ref[43])
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun229()

;; TERM 269: subst
c_code local build_ref_269
	ret_reg &ref[269]
	call_c   Dyam_Create_Atom("subst")
	move_ret ref[269]
	c_ret

long local pool_fun226[4]=[65538,build_ref_269,build_seed_42,pool_fun176]

long local pool_fun227[4]=[65538,build_ref_275,build_ref_171,pool_fun226]

pl_code local fun227
	call_c   Dyam_Remove_Choice()
	move     &ref[275], R(0)
	move     0, R(1)
	move     &ref[171], R(2)
	move     S(5), R(3)
	pl_call  pred_error_2()
fun226:
	call_c   Dyam_Unify(V(10),&ref[269])
	fail_ret
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(8))
	pl_call  pred_normalize_label_2()
	call_c   Dyam_Unify(V(3),V(4))
	fail_ret
	call_c   Dyam_Reg_Load(0,V(3))
	call_c   Dyam_Reg_Load(2,V(12))
	pl_call  pred_set_spine_2()
	pl_call  fun16(&seed[42],1)
	pl_jump  fun176()


long local pool_fun236[6]=[196610,build_ref_43,build_ref_268,pool_fun235,pool_fun227,pool_fun226]

pl_code local fun236
	call_c   Dyam_Update_Choice(fun235)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_atomic(V(1))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(2),V(6))
	fail_ret
	call_c   Dyam_Choice(fun227)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(&ref[268],&ref[43])
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun226()

;; TERM 371: '$skip'
c_code local build_ref_371
	ret_reg &ref[371]
	call_c   Dyam_Create_Atom("$skip")
	move_ret ref[371]
	c_ret

long local pool_fun237[4]=[131073,build_ref_371,pool_fun236,pool_fun176]

pl_code local fun237
	call_c   Dyam_Update_Choice(fun236)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[371])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(2),V(1))
	fail_ret
	call_c   Dyam_Unify(V(3),V(4))
	fail_ret
	pl_jump  fun176()

;; TERM 370: [_S2|_T2]
c_code local build_ref_370
	ret_reg &ref[370]
	call_c   Dyam_Create_List(V(70),V(71))
	move_ret ref[370]
	c_ret

;; TERM 368: scan
c_code local build_ref_368
	ret_reg &ref[368]
	call_c   Dyam_Create_Atom("scan")
	move_ret ref[368]
	c_ret

;; TERM 369: 'Features on scan node'
c_code local build_ref_369
	ret_reg &ref[369]
	call_c   Dyam_Create_Atom("Features on scan node")
	move_ret ref[369]
	c_ret

long local pool_fun219[4]=[65538,build_ref_369,build_ref_171,pool_fun176]

pl_code local fun219
	call_c   Dyam_Remove_Choice()
	move     &ref[369], R(0)
	move     0, R(1)
	move     &ref[171], R(2)
	move     S(5), R(3)
	pl_call  pred_error_2()
	pl_jump  fun176()

long local pool_fun220[3]=[131072,pool_fun219,pool_fun176]

pl_code local fun221
	call_c   Dyam_Remove_Choice()
fun220:
	call_c   Dyam_Choice(fun219)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(13),I(0))
	fail_ret
	call_c   Dyam_Unify(V(14),I(0))
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun176()


long local pool_fun222[4]=[131073,build_ref_368,pool_fun220,pool_fun220]

long local pool_fun223[4]=[65538,build_ref_275,build_ref_171,pool_fun222]

pl_code local fun223
	call_c   Dyam_Remove_Choice()
	move     &ref[275], R(0)
	move     0, R(1)
	move     &ref[171], R(2)
	move     S(5), R(3)
	pl_call  pred_error_2()
fun222:
	call_c   Dyam_Unify(V(10),&ref[368])
	fail_ret
	call_c   Dyam_Unify(V(8),V(1))
	fail_ret
	call_c   Dyam_Unify(V(9),I(0))
	fail_ret
	call_c   Dyam_Reg_Load(0,V(3))
	call_c   Dyam_Reg_Load(2,V(12))
	pl_call  pred_set_spine_2()
	call_c   Dyam_Unify(V(3),V(4))
	fail_ret
	call_c   Dyam_Choice(fun221)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(7),I(0))
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun220()


long local pool_fun224[5]=[131074,build_ref_43,build_ref_268,pool_fun223,pool_fun222]

long local pool_fun225[3]=[65537,build_ref_370,pool_fun224]

pl_code local fun225
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(1),&ref[370])
	fail_ret
fun224:
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(2),V(6))
	fail_ret
	call_c   Dyam_Choice(fun223)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(&ref[268],&ref[43])
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun222()


long local pool_fun238[4]=[196608,pool_fun237,pool_fun225,pool_fun224]

pl_code local fun238
	call_c   Dyam_Update_Choice(fun237)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Choice(fun225)
	call_c   Dyam_Unify(V(1),I(0))
	fail_ret
	pl_jump  fun224()

;; TERM 366: = _V
c_code local build_ref_366
	ret_reg &ref[366]
	call_c   build_ref_386()
	call_c   Dyam_Create_Unary(&ref[386],V(21))
	move_ret ref[366]
	c_ret

;; TERM 367: substfoot
c_code local build_ref_367
	ret_reg &ref[367]
	call_c   Dyam_Create_Atom("substfoot")
	move_ret ref[367]
	c_ret

;; TERM 365: 'Not a valid TAG foot ~w'
c_code local build_ref_365
	ret_reg &ref[365]
	call_c   Dyam_Create_Atom("Not a valid TAG foot ~w")
	move_ret ref[365]
	c_ret

;; TERM 363: 'Not a valid TAG adjonction node ~w (~w)'
c_code local build_ref_363
	ret_reg &ref[363]
	call_c   Dyam_Create_Atom("Not a valid TAG adjonction node ~w (~w)")
	move_ret ref[363]
	c_ret

;; TERM 364: [_B,'$VAR'(_L, ['$SET$',adj(no, yes, strict, atmostone, one, sync),63])]
c_code local build_ref_364
	ret_reg &ref[364]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_268()
	call_c   Dyam_Create_List(&ref[268],I(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(1),R(0))
	move_ret ref[364]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 361: 'Not a valid TAG foot ~w ~w ~w'
c_code local build_ref_361
	ret_reg &ref[361]
	call_c   Dyam_Create_Atom("Not a valid TAG foot ~w ~w ~w")
	move_ret ref[361]
	c_ret

;; TERM 362: [_B,_D,_E]
c_code local build_ref_362
	ret_reg &ref[362]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Tupple(3,4,I(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(1),R(0))
	move_ret ref[362]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun209[4]=[65538,build_ref_169,build_seed_42,pool_fun176]

long local pool_fun210[4]=[65538,build_ref_361,build_ref_362,pool_fun209]

pl_code local fun210
	call_c   Dyam_Remove_Choice()
	move     &ref[361], R(0)
	move     0, R(1)
	move     &ref[362], R(2)
	move     S(5), R(3)
	pl_call  pred_error_2()
fun209:
	call_c   Dyam_Unify(V(12),&ref[169])
	fail_ret
	pl_call  fun16(&seed[42],1)
	pl_jump  fun176()


;; TERM 360: left(_R2)
c_code local build_ref_360
	ret_reg &ref[360]
	call_c   build_ref_45()
	call_c   Dyam_Create_Unary(&ref[45],V(69))
	move_ret ref[360]
	c_ret

long local pool_fun211[5]=[131074,build_ref_360,build_ref_44,pool_fun210,pool_fun209]

pl_code local fun211
	call_c   Dyam_Update_Choice(fun210)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(3),&ref[360])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(4),&ref[44])
	fail_ret
	pl_jump  fun209()

;; TERM 359: 'more than one foot ~w: ~w'
c_code local build_ref_359
	ret_reg &ref[359]
	call_c   Dyam_Create_Atom("more than one foot ~w: ~w")
	move_ret ref[359]
	c_ret

;; TERM 346: [_F,_B]
c_code local build_ref_346
	ret_reg &ref[346]
	call_c   build_ref_171()
	call_c   Dyam_Create_List(V(5),&ref[171])
	move_ret ref[346]
	c_ret

long local pool_fun212[6]=[131075,build_ref_44,build_ref_359,build_ref_346,pool_fun211,pool_fun209]

pl_code local fun212
	call_c   Dyam_Update_Choice(fun211)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(3),&ref[44])
	fail_ret
	call_c   Dyam_Cut()
	move     &ref[359], R(0)
	move     0, R(1)
	move     &ref[346], R(2)
	move     S(5), R(3)
	pl_call  pred_error_2()
	pl_jump  fun209()

;; TERM 358: 'foot ~w in elementary tree'
c_code local build_ref_358
	ret_reg &ref[358]
	call_c   Dyam_Create_Atom("foot ~w in elementary tree")
	move_ret ref[358]
	c_ret

long local pool_fun213[6]=[131075,build_ref_42,build_ref_358,build_ref_171,pool_fun212,pool_fun209]

long local pool_fun214[4]=[65538,build_ref_363,build_ref_364,pool_fun213]

pl_code local fun214
	call_c   Dyam_Remove_Choice()
	move     &ref[363], R(0)
	move     0, R(1)
	move     &ref[364], R(2)
	move     S(5), R(3)
	pl_call  pred_error_2()
fun213:
	call_c   Dyam_Choice(fun212)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(3),&ref[42])
	fail_ret
	call_c   Dyam_Cut()
	move     &ref[358], R(0)
	move     0, R(1)
	move     &ref[171], R(2)
	move     S(5), R(3)
	pl_call  pred_error_2()
	pl_jump  fun209()


long local pool_fun215[5]=[131074,build_ref_43,build_ref_268,pool_fun214,pool_fun213]

long local pool_fun216[4]=[65538,build_ref_365,build_ref_171,pool_fun215]

pl_code local fun216
	call_c   Dyam_Remove_Choice()
	move     &ref[365], R(0)
	move     0, R(1)
	move     &ref[171], R(2)
	move     S(5), R(3)
	pl_call  pred_error_2()
fun215:
	call_c   Dyam_Choice(fun214)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(&ref[268],&ref[43])
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun213()


long local pool_fun217[3]=[131072,pool_fun216,pool_fun215]

long local pool_fun218[4]=[65538,build_ref_366,build_ref_367,pool_fun217]

pl_code local fun218
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(1),&ref[366])
	fail_ret
	call_c   Dyam_Unify(V(10),&ref[367])
	fail_ret
fun217:
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(2),V(6))
	fail_ret
	call_c   Dyam_Choice(fun216)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Reg_Load(0,V(21))
	call_c   Dyam_Reg_Load(2,V(8))
	pl_call  pred_normalize_label_2()
	call_c   Dyam_Cut()
	pl_jump  fun215()


;; TERM 356: * _V
c_code local build_ref_356
	ret_reg &ref[356]
	call_c   build_ref_355()
	call_c   Dyam_Create_Unary(&ref[355],V(21))
	move_ret ref[356]
	c_ret

;; TERM 355: *
c_code local build_ref_355
	ret_reg &ref[355]
	call_c   Dyam_Create_Atom("*")
	move_ret ref[355]
	c_ret

;; TERM 357: foot
c_code local build_ref_357
	ret_reg &ref[357]
	call_c   Dyam_Create_Atom("foot")
	move_ret ref[357]
	c_ret

long local pool_fun239[6]=[196610,build_ref_356,build_ref_357,pool_fun238,pool_fun218,pool_fun217]

pl_code local fun239
	call_c   Dyam_Update_Choice(fun238)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Choice(fun218)
	call_c   Dyam_Unify(V(1),&ref[356])
	fail_ret
	call_c   Dyam_Unify(V(10),&ref[357])
	fail_ret
	pl_jump  fun217()

;; TERM 351: _Q2 at _N2
c_code local build_ref_351
	ret_reg &ref[351]
	call_c   build_ref_436()
	call_c   Dyam_Create_Binary(&ref[436],V(68),V(65))
	move_ret ref[351]
	c_ret

;; TERM 436: at
c_code local build_ref_436
	ret_reg &ref[436]
	call_c   Dyam_Create_Atom("at")
	move_ret ref[436]
	c_ret

c_code local build_seed_54
	ret_reg &seed[54]
	call_c   build_ref_10()
	call_c   build_ref_353()
	call_c   Dyam_Seed_Start(&ref[10],&ref[353],I(0),fun9,1)
	call_c   build_ref_354()
	call_c   Dyam_Seed_Add_Comp(&ref[354],&ref[353],0)
	call_c   Dyam_Seed_End()
	move_ret seed[54]
	c_ret

;; TERM 354: '*GUARD*'(feature_normalize(_Q2, _C, _N2)) :> '$$HOLE$$'
c_code local build_ref_354
	ret_reg &ref[354]
	call_c   build_ref_353()
	call_c   Dyam_Create_Binary(I(9),&ref[353],I(7))
	move_ret ref[354]
	c_ret

;; TERM 353: '*GUARD*'(feature_normalize(_Q2, _C, _N2))
c_code local build_ref_353
	ret_reg &ref[353]
	call_c   build_ref_10()
	call_c   build_ref_352()
	call_c   Dyam_Create_Unary(&ref[10],&ref[352])
	move_ret ref[353]
	c_ret

;; TERM 352: feature_normalize(_Q2, _C, _N2)
c_code local build_ref_352
	ret_reg &ref[352]
	call_c   build_ref_427()
	call_c   Dyam_Term_Start(&ref[427],3)
	call_c   Dyam_Term_Arg(V(68))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(65))
	call_c   Dyam_Term_End()
	move_ret ref[352]
	c_ret

c_code local build_seed_53
	ret_reg &seed[53]
	call_c   build_ref_10()
	call_c   build_ref_334()
	call_c   Dyam_Seed_Start(&ref[10],&ref[334],I(0),fun9,1)
	call_c   build_ref_335()
	call_c   Dyam_Seed_Add_Comp(&ref[335],&ref[334],0)
	call_c   Dyam_Seed_End()
	move_ret seed[53]
	c_ret

;; TERM 335: '*GUARD*'(normalize(_N2, _C, _D, _E, _F)) :> '$$HOLE$$'
c_code local build_ref_335
	ret_reg &ref[335]
	call_c   build_ref_334()
	call_c   Dyam_Create_Binary(I(9),&ref[334],I(7))
	move_ret ref[335]
	c_ret

;; TERM 334: '*GUARD*'(normalize(_N2, _C, _D, _E, _F))
c_code local build_ref_334
	ret_reg &ref[334]
	call_c   build_ref_10()
	call_c   build_ref_333()
	call_c   Dyam_Create_Unary(&ref[10],&ref[333])
	move_ret ref[334]
	c_ret

;; TERM 333: normalize(_N2, _C, _D, _E, _F)
c_code local build_ref_333
	ret_reg &ref[333]
	call_c   build_ref_387()
	call_c   Dyam_Term_Start(&ref[387],5)
	call_c   Dyam_Term_Arg(V(65))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[333]
	c_ret

long local pool_fun240[6]=[131075,build_ref_351,build_seed_54,build_seed_53,pool_fun239,pool_fun176]

pl_code local fun240
	call_c   Dyam_Update_Choice(fun239)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[351])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(2),V(6))
	fail_ret
	pl_call  fun16(&seed[54],1)
	pl_call  fun16(&seed[53],1)
	pl_jump  fun176()

;; TERM 348: <=> _V
c_code local build_ref_348
	ret_reg &ref[348]
	call_c   build_ref_437()
	call_c   Dyam_Create_Unary(&ref[437],V(21))
	move_ret ref[348]
	c_ret

;; TERM 437: <=>
c_code local build_ref_437
	ret_reg &ref[437]
	call_c   Dyam_Create_Atom("<=>")
	move_ret ref[437]
	c_ret

;; TERM 350: 'Not a valid TAG coanchor ~w'
c_code local build_ref_350
	ret_reg &ref[350]
	call_c   Dyam_Create_Atom("Not a valid TAG coanchor ~w")
	move_ret ref[350]
	c_ret

long local pool_fun207[4]=[65538,build_ref_349,build_seed_42,pool_fun176]

long local pool_fun208[4]=[65538,build_ref_350,build_ref_171,pool_fun207]

pl_code local fun208
	call_c   Dyam_Remove_Choice()
	move     &ref[350], R(0)
	move     0, R(1)
	move     &ref[171], R(2)
	move     S(5), R(3)
	pl_call  pred_error_2()
fun207:
	call_c   Dyam_Unify(V(10),&ref[349])
	fail_ret
	call_c   Dyam_Unify(V(3),V(4))
	fail_ret
	call_c   Dyam_Reg_Load(0,V(3))
	call_c   Dyam_Reg_Load(2,V(12))
	pl_call  pred_set_spine_2()
	pl_call  fun16(&seed[42],1)
	pl_jump  fun176()


long local pool_fun241[5]=[196609,build_ref_348,pool_fun240,pool_fun208,pool_fun207]

pl_code local fun241
	call_c   Dyam_Update_Choice(fun240)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[348])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(2),V(6))
	fail_ret
	call_c   Dyam_Choice(fun208)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Reg_Load(0,V(21))
	call_c   Dyam_Reg_Load(2,V(8))
	pl_call  pred_normalize_label_2()
	call_c   Dyam_Cut()
	pl_jump  fun207()

;; TERM 341: <> _V
c_code local build_ref_341
	ret_reg &ref[341]
	call_c   build_ref_438()
	call_c   Dyam_Create_Unary(&ref[438],V(21))
	move_ret ref[341]
	c_ret

;; TERM 438: <>
c_code local build_ref_438
	ret_reg &ref[438]
	call_c   Dyam_Create_Atom("<>")
	move_ret ref[438]
	c_ret

;; TERM 347: 'Not a valid TAG anchor ~w'
c_code local build_ref_347
	ret_reg &ref[347]
	call_c   Dyam_Create_Atom("Not a valid TAG anchor ~w")
	move_ret ref[347]
	c_ret

;; TERM 345: 'More than one anchor in tree ~w: ~w'
c_code local build_ref_345
	ret_reg &ref[345]
	call_c   Dyam_Create_Atom("More than one anchor in tree ~w: ~w")
	move_ret ref[345]
	c_ret

;; TERM 344: tag_anchor(_F, _H)
c_code local build_ref_344
	ret_reg &ref[344]
	call_c   build_ref_432()
	call_c   Dyam_Create_Binary(&ref[432],V(5),V(7))
	move_ret ref[344]
	c_ret

long local pool_fun201[5]=[65539,build_ref_343,build_seed_42,build_ref_344,pool_fun176]

long local pool_fun204[4]=[65538,build_ref_345,build_ref_346,pool_fun201]

pl_code local fun204
	call_c   Dyam_Remove_Choice()
	move     &ref[345], R(0)
	move     0, R(1)
	move     &ref[346], R(2)
	move     S(5), R(3)
	pl_call  pred_error_2()
fun201:
	call_c   Dyam_Unify(V(10),&ref[343])
	fail_ret
	call_c   Dyam_Unify(V(3),V(4))
	fail_ret
	call_c   Dyam_Reg_Load(0,V(3))
	call_c   Dyam_Reg_Load(2,V(12))
	pl_call  pred_set_spine_2()
	pl_call  fun16(&seed[42],1)
	call_c   DYAM_evpred_assert_1(&ref[344])
	pl_jump  fun176()


pl_code local fun203
	call_c   Dyam_Remove_Choice()
fun202:
	call_c   Dyam_Cut()
	pl_jump  fun201()


;; TERM 342: tag_anchor(_F, _P2)
c_code local build_ref_342
	ret_reg &ref[342]
	call_c   build_ref_432()
	call_c   Dyam_Create_Binary(&ref[432],V(5),V(67))
	move_ret ref[342]
	c_ret

long local pool_fun205[4]=[131073,build_ref_342,pool_fun204,pool_fun201]

long local pool_fun206[4]=[65538,build_ref_347,build_ref_171,pool_fun205]

pl_code local fun206
	call_c   Dyam_Remove_Choice()
	move     &ref[347], R(0)
	move     0, R(1)
	move     &ref[171], R(2)
	move     S(5), R(3)
	pl_call  pred_error_2()
fun205:
	call_c   Dyam_Choice(fun204)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Choice(fun203)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[342])
	call_c   Dyam_Cut()
	pl_fail


long local pool_fun242[5]=[196609,build_ref_341,pool_fun241,pool_fun206,pool_fun205]

pl_code local fun242
	call_c   Dyam_Update_Choice(fun241)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[341])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(2),V(6))
	fail_ret
	call_c   Dyam_Choice(fun206)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Reg_Load(0,V(21))
	call_c   Dyam_Reg_Load(2,V(8))
	pl_call  pred_normalize_label_2()
	call_c   Dyam_Cut()
	pl_jump  fun205()

;; TERM 340: - _N2
c_code local build_ref_340
	ret_reg &ref[340]
	call_c   build_ref_439()
	call_c   Dyam_Create_Unary(&ref[439],V(65))
	move_ret ref[340]
	c_ret

;; TERM 439: -
c_code local build_ref_439
	ret_reg &ref[439]
	call_c   Dyam_Create_Atom("-")
	move_ret ref[439]
	c_ret

;; TERM 336: 'Multiple incompatible marks on ~w'
c_code local build_ref_336
	ret_reg &ref[336]
	call_c   Dyam_Create_Atom("Multiple incompatible marks on ~w")
	move_ret ref[336]
	c_ret

long local pool_fun198[3]=[65537,build_seed_53,pool_fun176]

long local pool_fun199[4]=[65538,build_ref_336,build_ref_171,pool_fun198]

pl_code local fun199
	call_c   Dyam_Remove_Choice()
	move     &ref[336], R(0)
	move     0, R(1)
	move     &ref[171], R(2)
	move     S(5), R(3)
	pl_call  pred_error_2()
fun198:
	pl_call  fun16(&seed[53],1)
	pl_jump  fun176()


long local pool_fun243[7]=[196611,build_ref_340,build_ref_43,build_ref_268,pool_fun242,pool_fun199,pool_fun198]

pl_code local fun243
	call_c   Dyam_Update_Choice(fun242)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[340])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(2),V(6))
	fail_ret
	call_c   Dyam_Choice(fun199)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(&ref[268],&ref[43])
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun198()

;; TERM 337: + _N2
c_code local build_ref_337
	ret_reg &ref[337]
	call_c   build_ref_440()
	call_c   Dyam_Create_Unary(&ref[440],V(65))
	move_ret ref[337]
	c_ret

;; TERM 440: +
c_code local build_ref_440
	ret_reg &ref[440]
	call_c   Dyam_Create_Atom("+")
	move_ret ref[440]
	c_ret

;; TERM 339: 'Conflict on TAG modifiers ~w'
c_code local build_ref_339
	ret_reg &ref[339]
	call_c   Dyam_Create_Atom("Conflict on TAG modifiers ~w")
	move_ret ref[339]
	c_ret

long local pool_fun200[4]=[65538,build_ref_339,build_ref_171,pool_fun198]

pl_code local fun200
	call_c   Dyam_Remove_Choice()
	move     &ref[339], R(0)
	move     0, R(1)
	move     &ref[171], R(2)
	move     S(5), R(3)
	pl_call  pred_error_2()
	pl_jump  fun198()

;; TERM 338: '$VAR'(_O2, ['$SET$',adj(no, yes, strict, atmostone, one, sync),62])
c_code local build_ref_338
	ret_reg &ref[338]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_415()
	call_c   build_ref_43()
	call_c   build_ref_169()
	call_c   build_ref_186()
	call_c   build_ref_428()
	call_c   build_ref_429()
	call_c   build_ref_430()
	call_c   Dyam_Term_Start(&ref[415],6)
	call_c   Dyam_Term_Arg(&ref[43])
	call_c   Dyam_Term_Arg(&ref[169])
	call_c   Dyam_Term_Arg(&ref[186])
	call_c   Dyam_Term_Arg(&ref[428])
	call_c   Dyam_Term_Arg(&ref[429])
	call_c   Dyam_Term_Arg(&ref[430])
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   Dyam_Term_Start(I(8),3)
	call_c   Dyam_Term_Arg(V(66))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(N(62))
	call_c   Dyam_Term_End()
	move_ret ref[338]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun244[7]=[196611,build_ref_337,build_ref_338,build_ref_268,pool_fun243,pool_fun200,pool_fun198]

pl_code local fun244
	call_c   Dyam_Update_Choice(fun243)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[337])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(2),V(6))
	fail_ret
	call_c   Dyam_Choice(fun200)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(&ref[268],&ref[338])
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun198()

;; TERM 332: ++ _N2
c_code local build_ref_332
	ret_reg &ref[332]
	call_c   build_ref_441()
	call_c   Dyam_Create_Unary(&ref[441],V(65))
	move_ret ref[332]
	c_ret

;; TERM 441: ++
c_code local build_ref_441
	ret_reg &ref[441]
	call_c   Dyam_Create_Atom("++")
	move_ret ref[441]
	c_ret

long local pool_fun245[7]=[196611,build_ref_332,build_ref_186,build_ref_268,pool_fun244,pool_fun199,pool_fun198]

pl_code local fun245
	call_c   Dyam_Update_Choice(fun244)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[332])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(2),V(6))
	fail_ret
	call_c   Dyam_Choice(fun199)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(&ref[268],&ref[186])
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun198()

;; TERM 330: _X ## _Y
c_code local build_ref_330
	ret_reg &ref[330]
	call_c   build_ref_442()
	call_c   Dyam_Create_Binary(&ref[442],V(23),V(24))
	move_ret ref[330]
	c_ret

;; TERM 442: ##
c_code local build_ref_442
	ret_reg &ref[442]
	call_c   Dyam_Create_Atom("##")
	move_ret ref[442]
	c_ret

c_code local build_seed_48
	ret_reg &seed[48]
	call_c   build_ref_10()
	call_c   build_ref_305()
	call_c   Dyam_Seed_Start(&ref[10],&ref[305],I(0),fun9,1)
	call_c   build_ref_306()
	call_c   Dyam_Seed_Add_Comp(&ref[306],&ref[305],0)
	call_c   Dyam_Seed_End()
	move_ret seed[48]
	c_ret

;; TERM 306: '*GUARD*'(normalize(_X, _Z, _D, _H1, _F)) :> '$$HOLE$$'
c_code local build_ref_306
	ret_reg &ref[306]
	call_c   build_ref_305()
	call_c   Dyam_Create_Binary(I(9),&ref[305],I(7))
	move_ret ref[306]
	c_ret

;; TERM 305: '*GUARD*'(normalize(_X, _Z, _D, _H1, _F))
c_code local build_ref_305
	ret_reg &ref[305]
	call_c   build_ref_10()
	call_c   build_ref_304()
	call_c   Dyam_Create_Unary(&ref[10],&ref[304])
	move_ret ref[305]
	c_ret

;; TERM 304: normalize(_X, _Z, _D, _H1, _F)
c_code local build_ref_304
	ret_reg &ref[304]
	call_c   build_ref_387()
	call_c   Dyam_Term_Start(&ref[387],5)
	call_c   Dyam_Term_Arg(V(23))
	call_c   Dyam_Term_Arg(V(25))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(33))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[304]
	c_ret

c_code local build_seed_49
	ret_reg &seed[49]
	call_c   build_ref_10()
	call_c   build_ref_308()
	call_c   Dyam_Seed_Start(&ref[10],&ref[308],I(0),fun9,1)
	call_c   build_ref_309()
	call_c   Dyam_Seed_Add_Comp(&ref[309],&ref[308],0)
	call_c   Dyam_Seed_End()
	move_ret seed[49]
	c_ret

;; TERM 309: '*GUARD*'(normalize(_Y, _A1, _H1, _E, _F)) :> '$$HOLE$$'
c_code local build_ref_309
	ret_reg &ref[309]
	call_c   build_ref_308()
	call_c   Dyam_Create_Binary(I(9),&ref[308],I(7))
	move_ret ref[309]
	c_ret

;; TERM 308: '*GUARD*'(normalize(_Y, _A1, _H1, _E, _F))
c_code local build_ref_308
	ret_reg &ref[308]
	call_c   build_ref_10()
	call_c   build_ref_307()
	call_c   Dyam_Create_Unary(&ref[10],&ref[307])
	move_ret ref[308]
	c_ret

;; TERM 307: normalize(_Y, _A1, _H1, _E, _F)
c_code local build_ref_307
	ret_reg &ref[307]
	call_c   build_ref_387()
	call_c   Dyam_Term_Start(&ref[387],5)
	call_c   Dyam_Term_Arg(V(24))
	call_c   Dyam_Term_Arg(V(26))
	call_c   Dyam_Term_Arg(V(33))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[307]
	c_ret

;; TERM 331: _Z ## _A1
c_code local build_ref_331
	ret_reg &ref[331]
	call_c   build_ref_442()
	call_c   Dyam_Create_Binary(&ref[442],V(25),V(26))
	move_ret ref[331]
	c_ret

long local pool_fun246[7]=[131076,build_ref_330,build_seed_48,build_seed_49,build_ref_331,pool_fun245,pool_fun176]

pl_code local fun246
	call_c   Dyam_Update_Choice(fun245)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[330])
	fail_ret
	call_c   Dyam_Cut()
	pl_call  fun16(&seed[48],1)
	pl_call  fun16(&seed[49],1)
	call_c   Dyam_Unify(V(2),&ref[331])
	fail_ret
	pl_jump  fun176()

;; TERM 325: @*{goal=> _W1, vars=> _X1, from=> _Y1, to=> _Z1, collect_first=> _A2, collect_last=> _B2, collect_loop=> _C2, collect_next=> _D2, collect_pred=> _E2}
c_code local build_ref_325
	ret_reg &ref[325]
	call_c   build_ref_443()
	call_c   Dyam_Term_Start(&ref[443],9)
	call_c   Dyam_Term_Arg(V(48))
	call_c   Dyam_Term_Arg(V(49))
	call_c   Dyam_Term_Arg(V(50))
	call_c   Dyam_Term_Arg(V(51))
	call_c   Dyam_Term_Arg(V(52))
	call_c   Dyam_Term_Arg(V(53))
	call_c   Dyam_Term_Arg(V(54))
	call_c   Dyam_Term_Arg(V(55))
	call_c   Dyam_Term_Arg(V(56))
	call_c   Dyam_Term_End()
	move_ret ref[325]
	c_ret

;; TERM 443: @*!'$ft'
c_code local build_ref_443
	ret_reg &ref[443]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_453()
	call_c   Dyam_Create_List(&ref[453],I(0))
	move_ret R(0)
	call_c   build_ref_452()
	call_c   Dyam_Create_List(&ref[452],R(0))
	move_ret R(0)
	call_c   build_ref_451()
	call_c   Dyam_Create_List(&ref[451],R(0))
	move_ret R(0)
	call_c   build_ref_450()
	call_c   Dyam_Create_List(&ref[450],R(0))
	move_ret R(0)
	call_c   build_ref_449()
	call_c   Dyam_Create_List(&ref[449],R(0))
	move_ret R(0)
	call_c   build_ref_448()
	call_c   Dyam_Create_List(&ref[448],R(0))
	move_ret R(0)
	call_c   build_ref_447()
	call_c   Dyam_Create_List(&ref[447],R(0))
	move_ret R(0)
	call_c   build_ref_446()
	call_c   Dyam_Create_List(&ref[446],R(0))
	move_ret R(0)
	call_c   build_ref_445()
	call_c   Dyam_Create_List(&ref[445],R(0))
	move_ret R(0)
	call_c   build_ref_444()
	call_c   Dyam_Create_Atom_Module("$ft",&ref[444])
	move_ret ref[443]
	call_c   DYAM_Feature_2(&ref[444],R(0))
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 444: @*
c_code local build_ref_444
	ret_reg &ref[444]
	call_c   Dyam_Create_Atom("@*")
	move_ret ref[444]
	c_ret

;; TERM 445: goal
c_code local build_ref_445
	ret_reg &ref[445]
	call_c   Dyam_Create_Atom("goal")
	move_ret ref[445]
	c_ret

;; TERM 446: vars
c_code local build_ref_446
	ret_reg &ref[446]
	call_c   Dyam_Create_Atom("vars")
	move_ret ref[446]
	c_ret

;; TERM 447: from
c_code local build_ref_447
	ret_reg &ref[447]
	call_c   Dyam_Create_Atom("from")
	move_ret ref[447]
	c_ret

;; TERM 448: to
c_code local build_ref_448
	ret_reg &ref[448]
	call_c   Dyam_Create_Atom("to")
	move_ret ref[448]
	c_ret

;; TERM 449: collect_first
c_code local build_ref_449
	ret_reg &ref[449]
	call_c   Dyam_Create_Atom("collect_first")
	move_ret ref[449]
	c_ret

;; TERM 450: collect_last
c_code local build_ref_450
	ret_reg &ref[450]
	call_c   Dyam_Create_Atom("collect_last")
	move_ret ref[450]
	c_ret

;; TERM 451: collect_loop
c_code local build_ref_451
	ret_reg &ref[451]
	call_c   Dyam_Create_Atom("collect_loop")
	move_ret ref[451]
	c_ret

;; TERM 452: collect_next
c_code local build_ref_452
	ret_reg &ref[452]
	call_c   Dyam_Create_Atom("collect_next")
	move_ret ref[452]
	c_ret

;; TERM 453: collect_pred
c_code local build_ref_453
	ret_reg &ref[453]
	call_c   Dyam_Create_Atom("collect_pred")
	move_ret ref[453]
	c_ret

;; TERM 326: @*{goal=> _X, vars=> _F2, from=> _G2, to=> _H2, collect_first=> _I2, collect_last=> _J2, collect_loop=> _K2, collect_next=> _L2, collect_pred=> _M2}
c_code local build_ref_326
	ret_reg &ref[326]
	call_c   build_ref_443()
	call_c   Dyam_Term_Start(&ref[443],9)
	call_c   Dyam_Term_Arg(V(23))
	call_c   Dyam_Term_Arg(V(57))
	call_c   Dyam_Term_Arg(V(58))
	call_c   Dyam_Term_Arg(V(59))
	call_c   Dyam_Term_Arg(V(60))
	call_c   Dyam_Term_Arg(V(61))
	call_c   Dyam_Term_Arg(V(62))
	call_c   Dyam_Term_Arg(V(63))
	call_c   Dyam_Term_Arg(V(64))
	call_c   Dyam_Term_End()
	move_ret ref[326]
	c_ret

;; TERM 327: @*{goal=> _Z, vars=> _F2, from=> _G2, to=> _H2, collect_first=> _I2, collect_last=> _J2, collect_loop=> _K2, collect_next=> _L2, collect_pred=> _M2}
c_code local build_ref_327
	ret_reg &ref[327]
	call_c   build_ref_443()
	call_c   Dyam_Term_Start(&ref[443],9)
	call_c   Dyam_Term_Arg(V(25))
	call_c   Dyam_Term_Arg(V(57))
	call_c   Dyam_Term_Arg(V(58))
	call_c   Dyam_Term_Arg(V(59))
	call_c   Dyam_Term_Arg(V(60))
	call_c   Dyam_Term_Arg(V(61))
	call_c   Dyam_Term_Arg(V(62))
	call_c   Dyam_Term_Arg(V(63))
	call_c   Dyam_Term_Arg(V(64))
	call_c   Dyam_Term_End()
	move_ret ref[327]
	c_ret

;; TERM 328: 'Kleene star not allowed around TAG spine nodes: ~w\n'
c_code local build_ref_328
	ret_reg &ref[328]
	call_c   Dyam_Create_Atom("Kleene star not allowed around TAG spine nodes: ~w\n")
	move_ret ref[328]
	c_ret

;; TERM 329: [_X]
c_code local build_ref_329
	ret_reg &ref[329]
	call_c   Dyam_Create_List(V(23),I(0))
	move_ret ref[329]
	c_ret

long local pool_fun197[4]=[65538,build_ref_328,build_ref_329,pool_fun176]

pl_code local fun197
	call_c   Dyam_Remove_Choice()
	move     &ref[328], R(0)
	move     0, R(1)
	move     &ref[329], R(2)
	move     S(5), R(3)
	pl_call  pred_error_2()
	pl_jump  fun176()

long local pool_fun247[8]=[196612,build_ref_325,build_ref_326,build_ref_327,build_seed_44,pool_fun246,pool_fun197,pool_fun176]

pl_code local fun247
	call_c   Dyam_Update_Choice(fun246)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[325])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(1))
	move     &ref[326], R(2)
	move     S(5), R(3)
	pl_call  pred_kleene_normalize_2()
	call_c   Dyam_Unify(V(2),&ref[327])
	fail_ret
	pl_call  fun16(&seed[44],1)
	call_c   Dyam_Choice(fun197)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(3),V(4))
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun176()

;; TERM 321: _X @*
c_code local build_ref_321
	ret_reg &ref[321]
	call_c   build_ref_444()
	call_c   Dyam_Create_Unary(&ref[444],V(23))
	move_ret ref[321]
	c_ret

c_code local build_seed_52
	ret_reg &seed[52]
	call_c   build_ref_10()
	call_c   build_ref_323()
	call_c   Dyam_Seed_Start(&ref[10],&ref[323],I(0),fun9,1)
	call_c   build_ref_324()
	call_c   Dyam_Seed_Add_Comp(&ref[324],&ref[323],0)
	call_c   Dyam_Seed_End()
	move_ret seed[52]
	c_ret

;; TERM 324: '*GUARD*'(normalize(@*{goal=> _X, vars=> _O1, from=> _P1, to=> _Q1, collect_first=> _R1, collect_last=> _S1, collect_loop=> _T1, collect_next=> _U1, collect_pred=> _V1}, _C, _D, _E, _F)) :> '$$HOLE$$'
c_code local build_ref_324
	ret_reg &ref[324]
	call_c   build_ref_323()
	call_c   Dyam_Create_Binary(I(9),&ref[323],I(7))
	move_ret ref[324]
	c_ret

;; TERM 323: '*GUARD*'(normalize(@*{goal=> _X, vars=> _O1, from=> _P1, to=> _Q1, collect_first=> _R1, collect_last=> _S1, collect_loop=> _T1, collect_next=> _U1, collect_pred=> _V1}, _C, _D, _E, _F))
c_code local build_ref_323
	ret_reg &ref[323]
	call_c   build_ref_10()
	call_c   build_ref_322()
	call_c   Dyam_Create_Unary(&ref[10],&ref[322])
	move_ret ref[323]
	c_ret

;; TERM 322: normalize(@*{goal=> _X, vars=> _O1, from=> _P1, to=> _Q1, collect_first=> _R1, collect_last=> _S1, collect_loop=> _T1, collect_next=> _U1, collect_pred=> _V1}, _C, _D, _E, _F)
c_code local build_ref_322
	ret_reg &ref[322]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_443()
	call_c   Dyam_Term_Start(&ref[443],9)
	call_c   Dyam_Term_Arg(V(23))
	call_c   Dyam_Term_Arg(V(40))
	call_c   Dyam_Term_Arg(V(41))
	call_c   Dyam_Term_Arg(V(42))
	call_c   Dyam_Term_Arg(V(43))
	call_c   Dyam_Term_Arg(V(44))
	call_c   Dyam_Term_Arg(V(45))
	call_c   Dyam_Term_Arg(V(46))
	call_c   Dyam_Term_Arg(V(47))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_387()
	call_c   Dyam_Term_Start(&ref[387],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[322]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun248[5]=[131074,build_ref_321,build_seed_52,pool_fun247,pool_fun176]

pl_code local fun248
	call_c   Dyam_Update_Choice(fun247)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[321])
	fail_ret
	call_c   Dyam_Cut()
	pl_call  fun16(&seed[52],1)
	pl_jump  fun176()

;; TERM 303: _X , _Y
c_code local build_ref_303
	ret_reg &ref[303]
	call_c   Dyam_Create_Binary(I(4),V(23),V(24))
	move_ret ref[303]
	c_ret

;; TERM 320: [_Z,_A1]
c_code local build_ref_320
	ret_reg &ref[320]
	call_c   Dyam_Create_Tupple(25,26,I(0))
	move_ret ref[320]
	c_ret

long local pool_fun194[3]=[65537,build_ref_320,pool_fun176]

pl_code local fun194
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(2),&ref[320])
	fail_ret
	pl_jump  fun176()

;; TERM 316: [_M1|_N1]
c_code local build_ref_316
	ret_reg &ref[316]
	call_c   Dyam_Create_List(V(38),V(39))
	move_ret ref[316]
	c_ret

c_code local build_seed_51
	ret_reg &seed[51]
	call_c   build_ref_10()
	call_c   build_ref_318()
	call_c   Dyam_Seed_Start(&ref[10],&ref[318],I(0),fun9,1)
	call_c   build_ref_319()
	call_c   Dyam_Seed_Add_Comp(&ref[319],&ref[318],0)
	call_c   Dyam_Seed_End()
	move_ret seed[51]
	c_ret

;; TERM 319: '*GUARD*'(append(_Z, [_A1], _C)) :> '$$HOLE$$'
c_code local build_ref_319
	ret_reg &ref[319]
	call_c   build_ref_318()
	call_c   Dyam_Create_Binary(I(9),&ref[318],I(7))
	move_ret ref[319]
	c_ret

;; TERM 318: '*GUARD*'(append(_Z, [_A1], _C))
c_code local build_ref_318
	ret_reg &ref[318]
	call_c   build_ref_10()
	call_c   build_ref_317()
	call_c   Dyam_Create_Unary(&ref[10],&ref[317])
	move_ret ref[318]
	c_ret

;; TERM 317: append(_Z, [_A1], _C)
c_code local build_ref_317
	ret_reg &ref[317]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(26),I(0))
	move_ret R(0)
	call_c   build_ref_407()
	call_c   Dyam_Term_Start(&ref[407],3)
	call_c   Dyam_Term_Arg(V(25))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_End()
	move_ret ref[317]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun195[5]=[131074,build_ref_316,build_seed_51,pool_fun194,pool_fun176]

pl_code local fun195
	call_c   Dyam_Update_Choice(fun194)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(25),&ref[316])
	fail_ret
	call_c   Dyam_Cut()
	pl_call  fun16(&seed[51],1)
	pl_jump  fun176()

;; TERM 310: [_I1|_J1]
c_code local build_ref_310
	ret_reg &ref[310]
	call_c   Dyam_Create_List(V(34),V(35))
	move_ret ref[310]
	c_ret

;; TERM 315: [_Z|_A1]
c_code local build_ref_315
	ret_reg &ref[315]
	call_c   Dyam_Create_List(V(25),V(26))
	move_ret ref[315]
	c_ret

long local pool_fun193[3]=[65537,build_ref_315,pool_fun176]

pl_code local fun193
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(2),&ref[315])
	fail_ret
	pl_jump  fun176()

;; TERM 311: [_K1|_L1]
c_code local build_ref_311
	ret_reg &ref[311]
	call_c   Dyam_Create_List(V(36),V(37))
	move_ret ref[311]
	c_ret

c_code local build_seed_50
	ret_reg &seed[50]
	call_c   build_ref_10()
	call_c   build_ref_313()
	call_c   Dyam_Seed_Start(&ref[10],&ref[313],I(0),fun9,1)
	call_c   build_ref_314()
	call_c   Dyam_Seed_Add_Comp(&ref[314],&ref[313],0)
	call_c   Dyam_Seed_End()
	move_ret seed[50]
	c_ret

;; TERM 314: '*GUARD*'(append(_Z, _A1, _C)) :> '$$HOLE$$'
c_code local build_ref_314
	ret_reg &ref[314]
	call_c   build_ref_313()
	call_c   Dyam_Create_Binary(I(9),&ref[313],I(7))
	move_ret ref[314]
	c_ret

;; TERM 313: '*GUARD*'(append(_Z, _A1, _C))
c_code local build_ref_313
	ret_reg &ref[313]
	call_c   build_ref_10()
	call_c   build_ref_312()
	call_c   Dyam_Create_Unary(&ref[10],&ref[312])
	move_ret ref[313]
	c_ret

;; TERM 312: append(_Z, _A1, _C)
c_code local build_ref_312
	ret_reg &ref[312]
	call_c   build_ref_407()
	call_c   Dyam_Term_Start(&ref[407],3)
	call_c   Dyam_Term_Arg(V(25))
	call_c   Dyam_Term_Arg(V(26))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_End()
	move_ret ref[312]
	c_ret

long local pool_fun196[7]=[196611,build_ref_310,build_ref_311,build_seed_50,pool_fun195,pool_fun193,pool_fun176]

pl_code local fun196
	call_c   Dyam_Update_Choice(fun195)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(26),&ref[310])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun193)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(25),&ref[311])
	fail_ret
	call_c   Dyam_Cut()
	pl_call  fun16(&seed[50],1)
	pl_jump  fun176()

long local pool_fun249[7]=[196611,build_ref_303,build_seed_48,build_seed_49,pool_fun248,pool_fun196,pool_fun176]

pl_code local fun249
	call_c   Dyam_Update_Choice(fun248)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[303])
	fail_ret
	call_c   Dyam_Cut()
	pl_call  fun16(&seed[48],1)
	pl_call  fun16(&seed[49],1)
	call_c   Dyam_Choice(fun196)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(26),I(0))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(2),V(25))
	fail_ret
	pl_jump  fun176()

;; TERM 301: _X ?
c_code local build_ref_301
	ret_reg &ref[301]
	call_c   build_ref_454()
	call_c   Dyam_Create_Unary(&ref[454],V(23))
	move_ret ref[301]
	c_ret

;; TERM 454: ?
c_code local build_ref_454
	ret_reg &ref[454]
	call_c   Dyam_Create_Atom("?")
	move_ret ref[454]
	c_ret

;; TERM 302: guard{goal=> _Z, plus=> true, minus=> true}
c_code local build_ref_302
	ret_reg &ref[302]
	call_c   build_ref_455()
	call_c   build_ref_93()
	call_c   Dyam_Term_Start(&ref[455],3)
	call_c   Dyam_Term_Arg(V(25))
	call_c   Dyam_Term_Arg(&ref[93])
	call_c   Dyam_Term_Arg(&ref[93])
	call_c   Dyam_Term_End()
	move_ret ref[302]
	c_ret

;; TERM 455: guard!'$ft'
c_code local build_ref_455
	ret_reg &ref[455]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_458()
	call_c   Dyam_Create_List(&ref[458],I(0))
	move_ret R(0)
	call_c   build_ref_457()
	call_c   Dyam_Create_List(&ref[457],R(0))
	move_ret R(0)
	call_c   build_ref_445()
	call_c   Dyam_Create_List(&ref[445],R(0))
	move_ret R(0)
	call_c   build_ref_456()
	call_c   Dyam_Create_Atom_Module("$ft",&ref[456])
	move_ret ref[455]
	call_c   DYAM_Feature_2(&ref[456],R(0))
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 456: guard
c_code local build_ref_456
	ret_reg &ref[456]
	call_c   Dyam_Create_Atom("guard")
	move_ret ref[456]
	c_ret

;; TERM 457: plus
c_code local build_ref_457
	ret_reg &ref[457]
	call_c   Dyam_Create_Atom("plus")
	move_ret ref[457]
	c_ret

;; TERM 458: minus
c_code local build_ref_458
	ret_reg &ref[458]
	call_c   Dyam_Create_Atom("minus")
	move_ret ref[458]
	c_ret

long local pool_fun250[6]=[131075,build_ref_301,build_seed_44,build_ref_302,pool_fun249,pool_fun176]

pl_code local fun250
	call_c   Dyam_Update_Choice(fun249)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[301])
	fail_ret
	call_c   Dyam_Cut()
	pl_call  fun16(&seed[44],1)
	call_c   Dyam_Unify(V(2),&ref[302])
	fail_ret
	pl_jump  fun176()

;; TERM 293: guard{goal=> _X, plus=> _C1, minus=> _D1}
c_code local build_ref_293
	ret_reg &ref[293]
	call_c   build_ref_455()
	call_c   Dyam_Term_Start(&ref[455],3)
	call_c   Dyam_Term_Arg(V(23))
	call_c   Dyam_Term_Arg(V(28))
	call_c   Dyam_Term_Arg(V(29))
	call_c   Dyam_Term_End()
	move_ret ref[293]
	c_ret

c_code local build_seed_47
	ret_reg &seed[47]
	call_c   build_ref_10()
	call_c   build_ref_298()
	call_c   Dyam_Seed_Start(&ref[10],&ref[298],I(0),fun9,1)
	call_c   build_ref_299()
	call_c   Dyam_Seed_Add_Comp(&ref[299],&ref[298],0)
	call_c   Dyam_Seed_End()
	move_ret seed[47]
	c_ret

;; TERM 299: '*GUARD*'(guard_factorize(_C1, _E1)) :> '$$HOLE$$'
c_code local build_ref_299
	ret_reg &ref[299]
	call_c   build_ref_298()
	call_c   Dyam_Create_Binary(I(9),&ref[298],I(7))
	move_ret ref[299]
	c_ret

;; TERM 298: '*GUARD*'(guard_factorize(_C1, _E1))
c_code local build_ref_298
	ret_reg &ref[298]
	call_c   build_ref_10()
	call_c   build_ref_297()
	call_c   Dyam_Create_Unary(&ref[10],&ref[297])
	move_ret ref[298]
	c_ret

;; TERM 297: guard_factorize(_C1, _E1)
c_code local build_ref_297
	ret_reg &ref[297]
	call_c   build_ref_382()
	call_c   Dyam_Create_Binary(&ref[382],V(28),V(30))
	move_ret ref[297]
	c_ret

;; TERM 300: guard{goal=> _Z, plus=> _G1, minus=> _D1}
c_code local build_ref_300
	ret_reg &ref[300]
	call_c   build_ref_455()
	call_c   Dyam_Term_Start(&ref[455],3)
	call_c   Dyam_Term_Arg(V(25))
	call_c   Dyam_Term_Arg(V(32))
	call_c   Dyam_Term_Arg(V(29))
	call_c   Dyam_Term_End()
	move_ret ref[300]
	c_ret

long local pool_fun187[4]=[65538,build_seed_47,build_ref_300,pool_fun176]

pl_code local fun187
	call_c   Dyam_Remove_Choice()
	pl_call  fun16(&seed[47],1)
	call_c   Dyam_Reg_Load(0,V(30))
	move     V(31), R(2)
	move     S(5), R(3)
	pl_call  pred_guard_linearize_2()
	call_c   Dyam_Reg_Load(0,V(31))
	move     V(32), R(2)
	move     S(5), R(3)
	pl_call  pred_guard_push_disjunctions_2()
	call_c   Dyam_Unify(V(2),&ref[300])
	fail_ret
	pl_jump  fun176()

long local pool_fun188[5]=[131074,build_ref_93,build_ref_94,pool_fun187,pool_fun176]

pl_code local fun188
	call_c   Dyam_Update_Choice(fun187)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(28),&ref[93])
	fail_ret
	call_c   Dyam_Unify(V(29),&ref[94])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(2),V(25))
	fail_ret
	pl_jump  fun176()

long local pool_fun189[3]=[65537,build_seed_44,pool_fun188]

pl_code local fun190
	call_c   Dyam_Remove_Choice()
fun189:
	pl_call  fun16(&seed[44],1)
	call_c   Dyam_Choice(fun188)
	call_c   Dyam_Set_Cut()
	pl_fail


long local pool_fun191[4]=[131073,build_ref_94,pool_fun189,pool_fun189]

pl_code local fun192
	call_c   Dyam_Remove_Choice()
fun191:
	call_c   Dyam_Choice(fun190)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(29),&ref[94])
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun189()


long local pool_fun251[6]=[196610,build_ref_293,build_ref_93,pool_fun250,pool_fun191,pool_fun191]

pl_code local fun251
	call_c   Dyam_Update_Choice(fun250)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[293])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun192)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(28),&ref[93])
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun191()

;; TERM 283: _X ; _Y
c_code local build_ref_283
	ret_reg &ref[283]
	call_c   Dyam_Create_Binary(I(5),V(23),V(24))
	move_ret ref[283]
	c_ret

;; TERM 284: _Z ; _A1
c_code local build_ref_284
	ret_reg &ref[284]
	call_c   Dyam_Create_Binary(I(5),V(25),V(26))
	move_ret ref[284]
	c_ret

;; TERM 291: 'Incompatible TAG alternatives on ~w\n'
c_code local build_ref_291
	ret_reg &ref[291]
	call_c   Dyam_Create_Atom("Incompatible TAG alternatives on ~w\n")
	move_ret ref[291]
	c_ret

;; TERM 292: [_B1]
c_code local build_ref_292
	ret_reg &ref[292]
	call_c   Dyam_Create_List(V(27),I(0))
	move_ret ref[292]
	c_ret

long local pool_fun186[4]=[65538,build_ref_291,build_ref_292,pool_fun176]

pl_code local fun186
	call_c   Dyam_Remove_Choice()
	move     &ref[291], R(0)
	move     0, R(1)
	move     &ref[292], R(2)
	move     S(5), R(3)
	pl_call  pred_error_2()
	pl_jump  fun176()

c_code local build_seed_45
	ret_reg &seed[45]
	call_c   build_ref_10()
	call_c   build_ref_289()
	call_c   Dyam_Seed_Start(&ref[10],&ref[289],I(0),fun9,1)
	call_c   build_ref_290()
	call_c   Dyam_Seed_Add_Comp(&ref[290],&ref[289],0)
	call_c   Dyam_Seed_End()
	move_ret seed[45]
	c_ret

;; TERM 290: '*GUARD*'(normalize(_Y, _A1, _D, _E, _F)) :> '$$HOLE$$'
c_code local build_ref_290
	ret_reg &ref[290]
	call_c   build_ref_289()
	call_c   Dyam_Create_Binary(I(9),&ref[289],I(7))
	move_ret ref[290]
	c_ret

;; TERM 289: '*GUARD*'(normalize(_Y, _A1, _D, _E, _F))
c_code local build_ref_289
	ret_reg &ref[289]
	call_c   build_ref_10()
	call_c   build_ref_288()
	call_c   Dyam_Create_Unary(&ref[10],&ref[288])
	move_ret ref[289]
	c_ret

;; TERM 288: normalize(_Y, _A1, _D, _E, _F)
c_code local build_ref_288
	ret_reg &ref[288]
	call_c   build_ref_387()
	call_c   Dyam_Term_Start(&ref[387],5)
	call_c   Dyam_Term_Arg(V(24))
	call_c   Dyam_Term_Arg(V(26))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[288]
	c_ret

long local pool_fun252[8]=[196612,build_ref_283,build_ref_284,build_seed_44,build_seed_45,pool_fun251,pool_fun186,pool_fun176]

pl_code local fun252
	call_c   Dyam_Update_Choice(fun251)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[283])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(2),&ref[284])
	fail_ret
	call_c   Dyam_Choice(fun186)
	call_c   Dyam_Set_Cut()
	pl_call  fun16(&seed[44],1)
	pl_call  fun16(&seed[45],1)
	call_c   Dyam_Cut()
	pl_jump  fun176()

;; TERM 278: [apply,_V|_W]
c_code local build_ref_278
	ret_reg &ref[278]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(21),V(22))
	move_ret R(0)
	call_c   Dyam_Create_List(I(3),R(0))
	move_ret ref[278]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 279: '$call'(_V)
c_code local build_ref_279
	ret_reg &ref[279]
	call_c   build_ref_459()
	call_c   Dyam_Create_Unary(&ref[459],V(21))
	move_ret ref[279]
	c_ret

;; TERM 459: '$call'
c_code local build_ref_459
	ret_reg &ref[459]
	call_c   Dyam_Create_Atom("$call")
	move_ret ref[459]
	c_ret

long local pool_fun253[7]=[196611,build_ref_278,build_ref_279,build_seed_43,pool_fun252,pool_fun182,pool_fun183]

pl_code local fun253
	call_c   Dyam_Update_Choice(fun252)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_univ(V(1),&ref[278])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(2),V(6))
	fail_ret
	call_c   Dyam_Unify(V(8),&ref[279])
	fail_ret
	pl_call  fun16(&seed[43],1)
	call_c   Dyam_Choice(fun185)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Choice(fun184)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(3),V(4))
	fail_ret
	call_c   Dyam_Cut()
	pl_fail

;; TERM 277: '$call'(_U)
c_code local build_ref_277
	ret_reg &ref[277]
	call_c   build_ref_459()
	call_c   Dyam_Create_Unary(&ref[459],V(20))
	move_ret ref[277]
	c_ret

long local pool_fun177[4]=[65538,build_ref_269,build_seed_42,pool_fun176]

long local pool_fun178[4]=[65538,build_ref_275,build_ref_171,pool_fun177]

pl_code local fun178
	call_c   Dyam_Remove_Choice()
	move     &ref[275], R(0)
	move     0, R(1)
	move     &ref[171], R(2)
	move     S(5), R(3)
	pl_call  pred_error_2()
fun177:
	call_c   Dyam_Unify(V(10),&ref[269])
	fail_ret
	call_c   Dyam_Unify(V(3),V(4))
	fail_ret
	call_c   Dyam_Reg_Load(0,V(3))
	call_c   Dyam_Reg_Load(2,V(12))
	pl_call  pred_set_spine_2()
	pl_call  fun16(&seed[42],1)
	pl_jump  fun176()


long local pool_fun179[5]=[131074,build_ref_43,build_ref_268,pool_fun178,pool_fun177]

long local pool_fun180[3]=[65537,build_ref_277,pool_fun179]

pl_code local fun180
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(1),&ref[277])
	fail_ret
	call_c   Dyam_Unify(V(8),V(1))
	fail_ret
fun179:
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(2),V(6))
	fail_ret
	call_c   Dyam_Choice(fun178)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(&ref[268],&ref[43])
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun177()


;; TERM 276: _K43235671
c_code local build_ref_276
	ret_reg &ref[276]
	call_c   Dyam_Create_Unary(I(6),V(19))
	move_ret ref[276]
	c_ret

;; TERM 267: '$call'(_B)
c_code local build_ref_267
	ret_reg &ref[267]
	call_c   build_ref_459()
	call_c   Dyam_Create_Unary(&ref[459],V(1))
	move_ret ref[267]
	c_ret

long local pool_fun181[5]=[131074,build_ref_276,build_ref_267,pool_fun180,pool_fun179]

pl_code local fun181
	call_c   Dyam_Update_Choice(fun180)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[276])
	fail_ret
	call_c   Dyam_Unify(V(8),&ref[267])
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun179()

long local pool_fun254[7]=[196611,build_ref_264,build_ref_266,build_ref_267,pool_fun253,pool_fun181,pool_fun179]

pl_code local fun254
	call_c   Dyam_Pool(pool_fun254)
	call_c   Dyam_Unify_Item(&ref[264])
	fail_ret
	call_c   Dyam_Unify(V(6),&ref[266])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun253)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Choice(fun181)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_variable(V(1))
	fail_ret
	call_c   Dyam_Unify(V(8),&ref[267])
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun179()

;; TERM 265: '*GUARD*'(normalize(_B, _C, _D, _E, _F)) :> []
c_code local build_ref_265
	ret_reg &ref[265]
	call_c   build_ref_264()
	call_c   Dyam_Create_Binary(I(9),&ref[264],I(0))
	move_ret ref[265]
	c_ret

c_code local build_seed_39
	ret_reg &seed[39]
	call_c   build_ref_10()
	call_c   build_ref_221()
	call_c   Dyam_Seed_Start(&ref[10],&ref[221],I(0),fun9,1)
	call_c   build_ref_222()
	call_c   Dyam_Seed_Add_Comp(&ref[222],&ref[221],0)
	call_c   Dyam_Seed_End()
	move_ret seed[39]
	c_ret

;; TERM 222: '*GUARD*'(guard_factorize(_P, _D)) :> '$$HOLE$$'
c_code local build_ref_222
	ret_reg &ref[222]
	call_c   build_ref_221()
	call_c   Dyam_Create_Binary(I(9),&ref[221],I(7))
	move_ret ref[222]
	c_ret

;; TERM 221: '*GUARD*'(guard_factorize(_P, _D))
c_code local build_ref_221
	ret_reg &ref[221]
	call_c   build_ref_10()
	call_c   build_ref_220()
	call_c   Dyam_Create_Unary(&ref[10],&ref[220])
	move_ret ref[221]
	c_ret

;; TERM 220: guard_factorize(_P, _D)
c_code local build_ref_220
	ret_reg &ref[220]
	call_c   build_ref_382()
	call_c   Dyam_Create_Binary(&ref[382],V(15),V(3))
	move_ret ref[220]
	c_ret

;; TERM 4: term_module_shift(_A, _B, _C, _D, _E)
c_code local build_ref_4
	ret_reg &ref[4]
	call_c   build_ref_460()
	call_c   Dyam_Term_Start(&ref[460],5)
	call_c   Dyam_Term_Arg(V(0))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[4]
	c_ret

;; TERM 460: term_module_shift
c_code local build_ref_460
	ret_reg &ref[460]
	call_c   Dyam_Create_Atom("term_module_shift")
	move_ret ref[460]
	c_ret

;; TERM 3: '*RITEM*'('call_term_module_shift/5'(_A, _B), return(_C, _D, _E))
c_code local build_ref_3
	ret_reg &ref[3]
	call_c   build_ref_0()
	call_c   build_ref_1()
	call_c   build_ref_2()
	call_c   Dyam_Create_Binary(&ref[0],&ref[1],&ref[2])
	move_ret ref[3]
	c_ret

;; TERM 2: return(_C, _D, _E)
c_code local build_ref_2
	ret_reg &ref[2]
	call_c   build_ref_396()
	call_c   Dyam_Term_Start(&ref[396],3)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[2]
	c_ret

;; TERM 1: 'call_term_module_shift/5'(_A, _B)
c_code local build_ref_1
	ret_reg &ref[1]
	call_c   build_ref_393()
	call_c   Dyam_Create_Binary(&ref[393],V(0),V(1))
	move_ret ref[1]
	c_ret

;; TERM 8: normalize(_A, _B)
c_code local build_ref_8
	ret_reg &ref[8]
	call_c   build_ref_387()
	call_c   Dyam_Create_Binary(&ref[387],V(0),V(1))
	move_ret ref[8]
	c_ret

;; TERM 7: '*RITEM*'('call_normalize/2'(_A), return(_B))
c_code local build_ref_7
	ret_reg &ref[7]
	call_c   build_ref_0()
	call_c   build_ref_5()
	call_c   build_ref_6()
	call_c   Dyam_Create_Binary(&ref[0],&ref[5],&ref[6])
	move_ret ref[7]
	c_ret

;; TERM 6: return(_B)
c_code local build_ref_6
	ret_reg &ref[6]
	call_c   build_ref_396()
	call_c   Dyam_Create_Unary(&ref[396],V(1))
	move_ret ref[6]
	c_ret

;; TERM 5: 'call_normalize/2'(_A)
c_code local build_ref_5
	ret_reg &ref[5]
	call_c   build_ref_398()
	call_c   Dyam_Create_Unary(&ref[398],V(0))
	move_ret ref[5]
	c_ret

;; TERM 219: _Q ; _R
c_code local build_ref_219
	ret_reg &ref[219]
	call_c   Dyam_Create_Binary(I(5),V(16),V(17))
	move_ret ref[219]
	c_ret

;; TERM 223: _S ; _T
c_code local build_ref_223
	ret_reg &ref[223]
	call_c   Dyam_Create_Binary(I(5),V(18),V(19))
	move_ret ref[223]
	c_ret

;; TERM 216: _H , _J
c_code local build_ref_216
	ret_reg &ref[216]
	call_c   Dyam_Create_Binary(I(4),V(7),V(9))
	move_ret ref[216]
	c_ret

;; TERM 215: _F , _G
c_code local build_ref_215
	ret_reg &ref[215]
	call_c   Dyam_Create_Binary(I(4),V(5),V(6))
	move_ret ref[215]
	c_ret

;; TERM 218: _H ; _J
c_code local build_ref_218
	ret_reg &ref[218]
	call_c   Dyam_Create_Binary(I(5),V(7),V(9))
	move_ret ref[218]
	c_ret

;; TERM 217: _F ; _G
c_code local build_ref_217
	ret_reg &ref[217]
	call_c   Dyam_Create_Binary(I(5),V(5),V(6))
	move_ret ref[217]
	c_ret

;; TERM 207: _G , _H
c_code local build_ref_207
	ret_reg &ref[207]
	call_c   Dyam_Create_Binary(I(4),V(6),V(7))
	move_ret ref[207]
	c_ret

;; TERM 206: _E , _F
c_code local build_ref_206
	ret_reg &ref[206]
	call_c   Dyam_Create_Binary(I(4),V(4),V(5))
	move_ret ref[206]
	c_ret

;; TERM 205: (_D , _E) , _F
c_code local build_ref_205
	ret_reg &ref[205]
	call_c   build_ref_213()
	call_c   Dyam_Create_Binary(I(4),&ref[213],V(5))
	move_ret ref[205]
	c_ret

;; TERM 210: _G ; _H
c_code local build_ref_210
	ret_reg &ref[210]
	call_c   Dyam_Create_Binary(I(5),V(6),V(7))
	move_ret ref[210]
	c_ret

;; TERM 209: _E ; _F
c_code local build_ref_209
	ret_reg &ref[209]
	call_c   Dyam_Create_Binary(I(5),V(4),V(5))
	move_ret ref[209]
	c_ret

;; TERM 208: (_D ; _E) ; _F
c_code local build_ref_208
	ret_reg &ref[208]
	call_c   build_ref_211()
	call_c   Dyam_Create_Binary(I(5),&ref[211],V(5))
	move_ret ref[208]
	c_ret

;; TERM 211: _D ; _E
c_code local build_ref_211
	ret_reg &ref[211]
	call_c   Dyam_Create_Binary(I(5),V(3),V(4))
	move_ret ref[211]
	c_ret

;; TERM 212: _G ; _I
c_code local build_ref_212
	ret_reg &ref[212]
	call_c   Dyam_Create_Binary(I(5),V(6),V(8))
	move_ret ref[212]
	c_ret

;; TERM 214: _G , _I
c_code local build_ref_214
	ret_reg &ref[214]
	call_c   Dyam_Create_Binary(I(4),V(6),V(8))
	move_ret ref[214]
	c_ret

;; TERM 198: _F , (_D ; _E)
c_code local build_ref_198
	ret_reg &ref[198]
	call_c   build_ref_211()
	call_c   Dyam_Create_Binary(I(4),V(5),&ref[211])
	move_ret ref[198]
	c_ret

;; TERM 197: [_G = _H,\+ \+ _I = _J,_K == _L]
c_code local build_ref_197
	ret_reg &ref[197]
	call_c   Dyam_Pseudo_Choice(3)
	call_c   build_ref_386()
	call_c   Dyam_Create_Binary(&ref[386],V(6),V(7))
	move_ret R(0)
	call_c   Dyam_Create_Binary(&ref[386],V(8),V(9))
	move_ret R(1)
	call_c   build_ref_389()
	call_c   Dyam_Create_Unary(&ref[389],R(1))
	move_ret R(1)
	call_c   Dyam_Create_Unary(&ref[389],R(1))
	move_ret R(1)
	call_c   build_ref_385()
	call_c   Dyam_Create_Binary(&ref[385],V(10),V(11))
	move_ret R(2)
	call_c   Dyam_Create_List(R(2),I(0))
	move_ret R(2)
	call_c   Dyam_Create_List(R(1),R(2))
	move_ret R(2)
	call_c   Dyam_Create_List(R(0),R(2))
	move_ret ref[197]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 196: (_D ; _E) , _F
c_code local build_ref_196
	ret_reg &ref[196]
	call_c   build_ref_211()
	call_c   Dyam_Create_Binary(I(4),&ref[211],V(5))
	move_ret ref[196]
	c_ret

;; TERM 202: _F , _T
c_code local build_ref_202
	ret_reg &ref[202]
	call_c   Dyam_Create_Binary(I(4),V(5),V(19))
	move_ret ref[202]
	c_ret

;; TERM 201: (_D ; _E) , _M
c_code local build_ref_201
	ret_reg &ref[201]
	call_c   build_ref_211()
	call_c   Dyam_Create_Binary(I(4),&ref[211],V(12))
	move_ret ref[201]
	c_ret

;; TERM 200: [_N = _O,\+ \+ _P = _Q,_R == _S]
c_code local build_ref_200
	ret_reg &ref[200]
	call_c   Dyam_Pseudo_Choice(3)
	call_c   build_ref_386()
	call_c   Dyam_Create_Binary(&ref[386],V(13),V(14))
	move_ret R(0)
	call_c   Dyam_Create_Binary(&ref[386],V(15),V(16))
	move_ret R(1)
	call_c   build_ref_389()
	call_c   Dyam_Create_Unary(&ref[389],R(1))
	move_ret R(1)
	call_c   Dyam_Create_Unary(&ref[389],R(1))
	move_ret R(1)
	call_c   build_ref_385()
	call_c   Dyam_Create_Binary(&ref[385],V(17),V(18))
	move_ret R(2)
	call_c   Dyam_Create_List(R(2),I(0))
	move_ret R(2)
	call_c   Dyam_Create_List(R(1),R(2))
	move_ret R(2)
	call_c   Dyam_Create_List(R(0),R(2))
	move_ret ref[200]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 199: (_D ; _E) , _F , _M
c_code local build_ref_199
	ret_reg &ref[199]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Binary(I(4),V(5),V(12))
	move_ret R(0)
	call_c   build_ref_211()
	call_c   Dyam_Create_Binary(I(4),&ref[211],R(0))
	move_ret ref[199]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 204: _U , _W
c_code local build_ref_204
	ret_reg &ref[204]
	call_c   Dyam_Create_Binary(I(4),V(20),V(22))
	move_ret ref[204]
	c_ret

;; TERM 203: _U , _V
c_code local build_ref_203
	ret_reg &ref[203]
	call_c   Dyam_Create_Binary(I(4),V(20),V(21))
	move_ret ref[203]
	c_ret

;; TERM 190: '$call'(_E)
c_code local build_ref_190
	ret_reg &ref[190]
	call_c   build_ref_459()
	call_c   Dyam_Create_Unary(&ref[459],V(4))
	move_ret ref[190]
	c_ret

;; TERM 193: [_C,_D,_B,_F,_G]
c_code local build_ref_193
	ret_reg &ref[193]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Tupple(5,6,I(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(1),R(0))
	move_ret R(0)
	call_c   Dyam_Create_Tupple(2,3,R(0))
	move_ret ref[193]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 192: 'features top ~w or bot ~w for ~w do not unify with defaults top ~w or bot ~w'
c_code local build_ref_192
	ret_reg &ref[192]
	call_c   Dyam_Create_Atom("features top ~w or bot ~w for ~w do not unify with defaults top ~w or bot ~w")
	move_ret ref[192]
	c_ret

;; TERM 191: tag_features(_B, _F, _G)
c_code local build_ref_191
	ret_reg &ref[191]
	call_c   build_ref_461()
	call_c   Dyam_Term_Start(&ref[461],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[191]
	c_ret

;; TERM 461: tag_features
c_code local build_ref_461
	ret_reg &ref[461]
	call_c   Dyam_Create_Atom("tag_features")
	move_ret ref[461]
	c_ret

;; TERM 195: [_C,_D,_B]
c_code local build_ref_195
	ret_reg &ref[195]
	call_c   build_ref_171()
	call_c   Dyam_Create_Tupple(2,3,&ref[171])
	move_ret ref[195]
	c_ret

;; TERM 194: 'features top ~w or bot ~w not built on same non terminal ~w'
c_code local build_ref_194
	ret_reg &ref[194]
	call_c   Dyam_Create_Atom("features top ~w or bot ~w not built on same non terminal ~w")
	move_ret ref[194]
	c_ret

;; TERM 157: [_B : 1]
c_code local build_ref_157
	ret_reg &ref[157]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_406()
	call_c   Dyam_Create_Binary(&ref[406],V(1),N(1))
	move_ret R(0)
	call_c   Dyam_Create_List(R(0),I(0))
	move_ret ref[157]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 158: _B : _E
c_code local build_ref_158
	ret_reg &ref[158]
	call_c   build_ref_406()
	call_c   Dyam_Create_Binary(&ref[406],V(1),V(4))
	move_ret ref[158]
	c_ret

;; TERM 159: [_B : 1|_C]
c_code local build_ref_159
	ret_reg &ref[159]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_406()
	call_c   Dyam_Create_Binary(&ref[406],V(1),N(1))
	move_ret R(0)
	call_c   Dyam_Create_List(R(0),V(2))
	move_ret ref[159]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 161: [_F : _I|_H]
c_code local build_ref_161
	ret_reg &ref[161]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_406()
	call_c   Dyam_Create_Binary(&ref[406],V(5),V(8))
	move_ret R(0)
	call_c   Dyam_Create_List(R(0),V(7))
	move_ret ref[161]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 160: _G + 1
c_code local build_ref_160
	ret_reg &ref[160]
	call_c   build_ref_440()
	call_c   Dyam_Create_Binary(&ref[440],V(6),N(1))
	move_ret ref[160]
	c_ret

;; TERM 162: [_F : _G|_J]
c_code local build_ref_162
	ret_reg &ref[162]
	call_c   build_ref_139()
	call_c   Dyam_Create_List(&ref[139],V(9))
	move_ret ref[162]
	c_ret

;; TERM 155: [_C : _D]
c_code local build_ref_155
	ret_reg &ref[155]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_406()
	call_c   Dyam_Create_Binary(&ref[406],V(2),V(3))
	move_ret R(0)
	call_c   Dyam_Create_List(R(0),I(0))
	move_ret ref[155]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 156: [_E : _F|_G]
c_code local build_ref_156
	ret_reg &ref[156]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_406()
	call_c   Dyam_Create_Binary(&ref[406],V(4),V(5))
	move_ret R(0)
	call_c   Dyam_Create_List(R(0),V(6))
	move_ret ref[156]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 129: [_B : _C]
c_code local build_ref_129
	ret_reg &ref[129]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_406()
	call_c   Dyam_Create_Binary(&ref[406],V(1),V(2))
	move_ret R(0)
	call_c   Dyam_Create_List(R(0),I(0))
	move_ret ref[129]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 131: [_F : _G|_I]
c_code local build_ref_131
	ret_reg &ref[131]
	call_c   build_ref_139()
	call_c   Dyam_Create_List(&ref[139],V(8))
	move_ret ref[131]
	c_ret

;; TERM 132: [_B : _C|_D]
c_code local build_ref_132
	ret_reg &ref[132]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_406()
	call_c   Dyam_Create_Binary(&ref[406],V(1),V(2))
	move_ret R(0)
	call_c   Dyam_Create_List(R(0),V(3))
	move_ret ref[132]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 130: [_F : _G|_H]
c_code local build_ref_130
	ret_reg &ref[130]
	call_c   build_ref_139()
	call_c   Dyam_Create_List(&ref[139],V(7))
	move_ret ref[130]
	c_ret

;; TERM 109: [_D : _E|_F]
c_code local build_ref_109
	ret_reg &ref[109]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_406()
	call_c   Dyam_Create_Binary(&ref[406],V(3),V(4))
	move_ret R(0)
	call_c   Dyam_Create_List(R(0),V(5))
	move_ret ref[109]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 92: [_G]
c_code local build_ref_92
	ret_reg &ref[92]
	call_c   Dyam_Create_List(V(6),I(0))
	move_ret ref[92]
	c_ret

;; TERM 91: '\tgot ~w\n'
c_code local build_ref_91
	ret_reg &ref[91]
	call_c   Dyam_Create_Atom("\tgot ~w\n")
	move_ret ref[91]
	c_ret

;; TERM 90: [_E,_F]
c_code local build_ref_90
	ret_reg &ref[90]
	call_c   Dyam_Create_Tupple(4,5,I(0))
	move_ret ref[90]
	c_ret

;; TERM 89: '\nuse factor ~w on ~w\n'
c_code local build_ref_89
	ret_reg &ref[89]
	call_c   Dyam_Create_Atom("\nuse factor ~w on ~w\n")
	move_ret ref[89]
	c_ret

;; TERM 88: _E , _F , _G
c_code local build_ref_88
	ret_reg &ref[88]
	call_c   build_ref_215()
	call_c   Dyam_Create_Binary(I(4),V(4),&ref[215])
	move_ret ref[88]
	c_ret

;; TERM 87: _B , _C , _D
c_code local build_ref_87
	ret_reg &ref[87]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Binary(I(4),V(2),V(3))
	move_ret R(0)
	call_c   Dyam_Create_Binary(I(4),V(1),R(0))
	move_ret ref[87]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 75: [_C : _D|_E]
c_code local build_ref_75
	ret_reg &ref[75]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_406()
	call_c   Dyam_Create_Binary(&ref[406],V(2),V(3))
	move_ret R(0)
	call_c   Dyam_Create_List(R(0),V(4))
	move_ret ref[75]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 41: '$call'(_C)
c_code local build_ref_41
	ret_reg &ref[41]
	call_c   build_ref_459()
	call_c   Dyam_Create_Unary(&ref[459],V(2))
	move_ret ref[41]
	c_ret

pl_code local fun15
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Object(R(0))
	call_c   Dyam_Tabule()
	pl_ret

pl_code local fun142
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(15),V(3))
	fail_ret
fun141:
	call_c   Dyam_Reg_Load(0,V(8))
	call_c   Dyam_Reg_Load(2,V(10))
	call_c   Dyam_Reg_Load(4,V(4))
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  pred_guard_and_combine_3()


pl_code local fun143
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Reg_Load(0,V(7))
	call_c   Dyam_Reg_Load(2,V(9))
	move     V(11), R(4)
	move     S(5), R(5)
	pl_call  pred_guard_and_combine_3()
	call_c   Dyam_Reg_Load(0,V(7))
	call_c   Dyam_Reg_Load(2,V(10))
	move     V(12), R(4)
	move     S(5), R(5)
	pl_call  pred_guard_and_combine_3()
	call_c   Dyam_Reg_Load(0,V(8))
	call_c   Dyam_Reg_Load(2,V(9))
	move     V(13), R(4)
	move     S(5), R(5)
	pl_call  pred_guard_and_combine_3()
	call_c   Dyam_Reg_Load(0,V(11))
	call_c   Dyam_Reg_Load(2,V(12))
	move     V(14), R(4)
	move     S(5), R(5)
	pl_call  pred_guard_or_combine_3()
	call_c   Dyam_Reg_Load(0,V(13))
	call_c   Dyam_Reg_Load(2,V(14))
	move     V(15), R(4)
	move     S(5), R(5)
	pl_call  pred_guard_or_combine_3()
	call_c   Dyam_Choice(fun142)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(15),&ref[219])
	fail_ret
	call_c   Dyam_Cut()
	pl_call  fun16(&seed[39],1)
	pl_jump  fun141()

pl_code local fun145
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(15),V(3))
	fail_ret
fun144:
	call_c   Dyam_Reg_Load(0,V(8))
	call_c   Dyam_Reg_Load(2,V(10))
	move     V(20), R(4)
	move     S(5), R(5)
	pl_call  pred_guard_or_combine_3()
	call_c   Dyam_Unify(V(4),V(20))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret


pl_code local fun146
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(3),&ref[94])
	fail_ret
	call_c   Dyam_Unify(V(4),V(1))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun147
	call_c   Dyam_Update_Choice(fun146)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(1),V(2))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(3),&ref[93])
	fail_ret
	call_c   Dyam_Unify(V(4),&ref[94])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun148
	call_c   Dyam_Update_Choice(fun147)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[94])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(3),&ref[94])
	fail_ret
	call_c   Dyam_Unify(V(4),&ref[94])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun149
	call_c   Dyam_Update_Choice(fun148)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[93])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(3),&ref[94])
	fail_ret
	call_c   Dyam_Unify(V(4),&ref[93])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun150
	call_c   Dyam_Update_Choice(fun149)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[217])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(5))
	call_c   Dyam_Reg_Load(2,V(2))
	move     V(7), R(4)
	move     S(5), R(5)
	move     V(8), R(6)
	move     S(5), R(7)
	pl_call  pred_guard_use_factor_4()
	call_c   Dyam_Reg_Load(0,V(6))
	call_c   Dyam_Reg_Load(2,V(2))
	move     V(9), R(4)
	move     S(5), R(5)
	move     V(10), R(6)
	move     S(5), R(7)
	pl_call  pred_guard_use_factor_4()
	call_c   Dyam_Reg_Load(0,V(7))
	call_c   Dyam_Reg_Load(2,V(9))
	move     V(15), R(4)
	move     S(5), R(5)
	pl_call  pred_guard_or_combine_3()
	call_c   Dyam_Choice(fun145)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(15),&ref[223])
	fail_ret
	call_c   Dyam_Cut()
	pl_call  fun16(&seed[39],1)
	pl_jump  fun144()

pl_code local fun135
	call_c   Dyam_Remove_Choice()
	call_c   DyALog_Gensym()
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Number(0,V(2))
	fail_ret
	call_c   Dyam_Unify(V(4),&ref[132])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun136
	call_c   Dyam_Update_Choice(fun135)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(3))
	call_c   Dyam_Reg_Load(4,V(2))
	pl_call  pred_guard_lookup_3()
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(3),V(4))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun137
	call_c   Dyam_Update_Choice(fun136)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[94])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(2),V(1))
	fail_ret
	call_c   Dyam_Unify(V(3),V(4))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun138
	call_c   Dyam_Update_Choice(fun137)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[93])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(2),V(1))
	fail_ret
	call_c   Dyam_Unify(V(3),V(4))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun139
	call_c   Dyam_Update_Choice(fun138)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[217])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(5))
	move     V(7), R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Load(4,V(3))
	move     V(8), R(6)
	move     S(5), R(7)
	pl_call  pred_guard_simplify_4()
	call_c   Dyam_Reg_Load(0,V(6))
	move     V(9), R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Load(4,V(8))
	call_c   Dyam_Reg_Load(6,V(4))
	pl_call  pred_guard_simplify_4()
	call_c   Dyam_Unify(V(2),&ref[218])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun130
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(2),V(1))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun131
	call_c   Dyam_Update_Choice(fun130)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[213])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(3))
	move     V(6), R(2)
	move     S(5), R(3)
	pl_call  pred_guard_linearize_2()
	call_c   Dyam_Reg_Load(0,V(4))
	move     V(8), R(2)
	move     S(5), R(3)
	pl_call  pred_guard_linearize_2()
	call_c   Dyam_Unify(V(2),&ref[214])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun132
	call_c   Dyam_Update_Choice(fun131)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[211])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(3))
	move     V(6), R(2)
	move     S(5), R(3)
	pl_call  pred_guard_linearize_2()
	call_c   Dyam_Reg_Load(0,V(4))
	move     V(8), R(2)
	move     S(5), R(3)
	pl_call  pred_guard_linearize_2()
	call_c   Dyam_Unify(V(2),&ref[212])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun133
	call_c   Dyam_Update_Choice(fun132)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[208])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(3))
	move     V(6), R(2)
	move     S(5), R(3)
	pl_call  pred_guard_linearize_2()
	move     &ref[209], R(0)
	move     S(5), R(1)
	move     V(7), R(2)
	move     S(5), R(3)
	pl_call  pred_guard_linearize_2()
	call_c   Dyam_Unify(V(2),&ref[210])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun125
	call_c   Dyam_Remove_Choice()
	move     &ref[204], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(2))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_guard_push_disjunctions_2()

pl_code local fun126
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(1),V(2))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun127
	call_c   Dyam_Update_Choice(fun126)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[203])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(21))
	move     V(22), R(2)
	move     S(5), R(3)
	pl_call  pred_guard_push_disjunctions_2()
	call_c   Dyam_Choice(fun125)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(21),V(22))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(2),V(1))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun128
	call_c   Dyam_Update_Choice(fun127)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[199])
	fail_ret
	pl_call  Domain_2(V(5),&ref[200])
	call_c   Dyam_Cut()
	move     &ref[201], R(0)
	move     S(5), R(1)
	move     V(19), R(2)
	move     S(5), R(3)
	pl_call  pred_guard_push_disjunctions_2()
	call_c   Dyam_Unify(V(2),&ref[202])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun118
	call_c   Dyam_Remove_Choice()
	move     &ref[192], R(0)
	move     0, R(1)
	move     &ref[193], R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_error_2()

pl_code local fun119
	call_c   Dyam_Remove_Choice()
	move     &ref[194], R(0)
	move     0, R(1)
	move     &ref[195], R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_error_2()

pl_code local fun120
	call_c   Dyam_Update_Choice(fun119)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_functor(V(3),V(1),V(7))
	fail_ret
	call_c   DYAM_evpred_functor(V(2),V(1),V(7))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun121
	call_c   Dyam_Update_Choice(fun120)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_functor(V(2),V(1),V(7))
	fail_ret
	call_c   DYAM_evpred_functor(V(3),V(1),V(7))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun122
	call_c   Dyam_Update_Choice(fun121)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),V(2))
	fail_ret
	call_c   Dyam_Unify(V(1),V(3))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun123
	call_c   Dyam_Update_Choice(fun122)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[191])
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun118)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(2),V(5))
	fail_ret
	call_c   Dyam_Unify(V(3),V(6))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun79
	call_c   Dyam_Remove_Choice()
fun78:
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(3),&ref[159])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret


pl_code local fun80
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(7))
	move     V(9), R(4)
	move     S(5), R(5)
	pl_call  pred_guard_factors_aux_3()
	call_c   Dyam_Unify(V(3),&ref[162])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun81
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(2),&ref[130])
	fail_ret
	call_c   Dyam_Choice(fun80)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(5),V(1))
	fail_ret
	call_c   Dyam_Cut()
	call_c   DYAM_evpred_is(V(8),&ref[160])
	fail_ret
	call_c   Dyam_Unify(V(3),&ref[161])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun82
	call_c   Dyam_Update_Choice(fun81)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Choice(fun79)
	call_c   Dyam_Set_Cut()
	pl_call  Domain_2(&ref[158],V(2))
	call_c   Dyam_Cut()
	pl_fail

pl_code local fun74
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(2),V(7))
	fail_ret
	call_c   Dyam_Unify(V(3),V(8))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun75
	call_c   Dyam_Remove_Choice()
	call_c   DYAM_evpred_gt(V(5),N(1))
	fail_ret
	call_c   Dyam_Unify(V(2),V(4))
	fail_ret
	call_c   Dyam_Unify(V(3),V(5))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun76
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(1),&ref[156])
	fail_ret
	call_c   Dyam_Choice(fun75)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Reg_Load(0,V(6))
	move     V(7), R(2)
	move     S(5), R(3)
	move     V(8), R(4)
	move     S(5), R(5)
	pl_call  pred_factor_max_3()
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun74)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_gt(V(5),V(8))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(2),V(4))
	fail_ret
	call_c   Dyam_Unify(V(3),V(8))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun64
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(4),&ref[132])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun65
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(3),&ref[130])
	fail_ret
	call_c   Dyam_Choice(fun64)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_gt(V(6),V(2))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(2))
	call_c   Dyam_Reg_Load(4,V(7))
	move     V(8), R(6)
	move     S(5), R(7)
	pl_call  pred_factor_sort_add_4()
	call_c   Dyam_Unify(V(4),&ref[131])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun51
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Reg_Load(0,V(3))
	call_c   Dyam_Reg_Load(2,V(4))
	call_c   Dyam_Reg_Load(4,V(6))
	call_c   Dyam_Reg_Load(6,V(2))
	call_c   Dyam_Reg_Deallocate(4)
	pl_jump  pred_factor_sort_add_4()

pl_code local fun52
	call_c   Dyam_Remove_Choice()
	pl_fail

pl_code local fun53
	call_c   Dyam_Update_Choice(fun52)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[109])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(5))
	move     V(6), R(2)
	move     S(5), R(3)
	pl_call  pred_factor_sort_2()
	call_c   Dyam_Choice(fun51)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(4),N(1))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(2),V(6))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun37
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(3),&ref[96])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun38
	call_c   Dyam_Update_Choice(fun37)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(2),&ref[94])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(3),&ref[94])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun39
	call_c   Dyam_Update_Choice(fun38)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[94])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(3),&ref[94])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun40
	call_c   Dyam_Update_Choice(fun39)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(2),&ref[93])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(3),V(1))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun32
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(3),&ref[95])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun33
	call_c   Dyam_Update_Choice(fun32)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(2),&ref[94])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(3),V(1))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun34
	call_c   Dyam_Update_Choice(fun33)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[94])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(3),V(2))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun35
	call_c   Dyam_Update_Choice(fun34)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(2),&ref[93])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(3),&ref[93])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun23
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(4))
	call_c   Dyam_Reg_Load(4,V(5))
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  pred_guard_lookup_3()

pl_code local fun12
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(2),&ref[45])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun13
	call_c   Dyam_Update_Choice(fun12)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[44])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(2),&ref[44])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun10
	call_c   Dyam_Remove_Choice()
	call_c   DYAM_evpred_atomic(V(1))
	fail_ret
	call_c   Dyam_Unify(V(2),V(1))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret


;;------------------ INIT POOL ------------

c_code local build_init_pool
	call_c   build_seed_5()
	call_c   build_seed_6()
	call_c   build_seed_0()
	call_c   build_seed_1()
	call_c   build_seed_7()
	call_c   build_seed_2()
	call_c   build_seed_4()
	call_c   build_seed_18()
	call_c   build_seed_3()
	call_c   build_seed_14()
	call_c   build_seed_8()
	call_c   build_seed_10()
	call_c   build_seed_11()
	call_c   build_seed_12()
	call_c   build_seed_20()
	call_c   build_seed_9()
	call_c   build_seed_15()
	call_c   build_seed_19()
	call_c   build_seed_13()
	call_c   build_seed_23()
	call_c   build_seed_17()
	call_c   build_seed_16()
	call_c   build_seed_21()
	call_c   build_seed_22()
	call_c   build_seed_24()
	call_c   build_seed_39()
	call_c   build_ref_4()
	call_c   build_ref_3()
	call_c   build_ref_8()
	call_c   build_ref_7()
	call_c   build_ref_219()
	call_c   build_ref_223()
	call_c   build_ref_216()
	call_c   build_ref_215()
	call_c   build_ref_218()
	call_c   build_ref_217()
	call_c   build_ref_207()
	call_c   build_ref_206()
	call_c   build_ref_205()
	call_c   build_ref_210()
	call_c   build_ref_209()
	call_c   build_ref_208()
	call_c   build_ref_212()
	call_c   build_ref_211()
	call_c   build_ref_214()
	call_c   build_ref_213()
	call_c   build_ref_198()
	call_c   build_ref_197()
	call_c   build_ref_196()
	call_c   build_ref_202()
	call_c   build_ref_201()
	call_c   build_ref_200()
	call_c   build_ref_199()
	call_c   build_ref_204()
	call_c   build_ref_203()
	call_c   build_ref_190()
	call_c   build_ref_193()
	call_c   build_ref_192()
	call_c   build_ref_191()
	call_c   build_ref_195()
	call_c   build_ref_194()
	call_c   build_ref_157()
	call_c   build_ref_158()
	call_c   build_ref_159()
	call_c   build_ref_161()
	call_c   build_ref_160()
	call_c   build_ref_162()
	call_c   build_ref_155()
	call_c   build_ref_156()
	call_c   build_ref_129()
	call_c   build_ref_131()
	call_c   build_ref_132()
	call_c   build_ref_130()
	call_c   build_ref_109()
	call_c   build_ref_96()
	call_c   build_ref_93()
	call_c   build_ref_94()
	call_c   build_ref_95()
	call_c   build_ref_92()
	call_c   build_ref_91()
	call_c   build_ref_90()
	call_c   build_ref_89()
	call_c   build_ref_88()
	call_c   build_ref_87()
	call_c   build_ref_75()
	call_c   build_ref_43()
	call_c   build_ref_42()
	call_c   build_ref_44()
	call_c   build_ref_45()
	call_c   build_ref_41()
	c_ret

long local ref[462]
long local seed[55]

long local _initialization

c_code global initialization_dyalog_tag_5Fnormalize
	call_c   Dyam_Check_And_Update_Initialization(_initialization)
	fail_c_ret
	call_c   initialization_dyalog_format()
	call_c   build_init_pool()
	call_c   build_viewers()
	call_c   load()
	c_ret

