;; Compiler: DyALog 1.14.0
;; File "seed.pl"


;;------------------ LOADING ------------

c_code local load
	call_c   Dyam_Loading_Set()
	pl_call  fun54(&seed[17],0)
	pl_call  fun54(&seed[22],0)
	pl_call  fun54(&seed[23],0)
	pl_call  fun54(&seed[8],0)
	pl_call  fun54(&seed[20],0)
	pl_call  fun54(&seed[10],0)
	pl_call  fun54(&seed[18],0)
	pl_call  fun54(&seed[14],0)
	pl_call  fun54(&seed[9],0)
	pl_call  fun54(&seed[21],0)
	pl_call  fun54(&seed[11],0)
	pl_call  fun54(&seed[7],0)
	pl_call  fun54(&seed[12],0)
	pl_call  fun54(&seed[15],0)
	pl_call  fun54(&seed[5],0)
	pl_call  fun54(&seed[4],0)
	pl_call  fun54(&seed[3],0)
	pl_call  fun54(&seed[19],0)
	pl_call  fun54(&seed[13],0)
	pl_call  fun54(&seed[6],0)
	pl_call  fun54(&seed[2],0)
	pl_call  fun54(&seed[1],0)
	pl_call  fun54(&seed[0],0)
	pl_call  fun54(&seed[16],0)
	pl_call  fun2(&seed[25],0)
	pl_call  fun2(&seed[24],0)
	call_c   Dyam_Loading_Reset()
	c_ret

pl_code global pred_test_schedule_3
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(3)
	call_c   Dyam_Choice(fun89)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_variable(V(1))
	fail_ret
	call_c   Dyam_Cut()
	move     &ref[206], R(0)
	move     0, R(1)
	move     &ref[207], R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_error_2()

pl_code global pred_handle_appinfo_4
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(4)
	call_c   Dyam_Choice(fun26)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),I(0))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(3),&ref[80])
	fail_ret
	call_c   Dyam_Unify(V(4),&ref[81])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret


;;------------------ VIEWERS ------------

c_code local build_viewers
	call_c   Dyam_Load_Viewer(&ref[7],&ref[6])
	call_c   Dyam_Load_Viewer(&ref[5],&ref[4])
	call_c   Dyam_Load_Viewer(&ref[3],&ref[1])
	c_ret

;; TERM 1: gen_application(_A)
c_code local build_ref_1
	ret_reg &ref[1]
	call_c   build_ref_259()
	call_c   Dyam_Create_Unary(&ref[259],V(0))
	move_ret ref[1]
	c_ret

;; TERM 259: gen_application
c_code local build_ref_259
	ret_reg &ref[259]
	call_c   Dyam_Create_Atom("gen_application")
	move_ret ref[259]
	c_ret

;; TERM 3: '*RITEM*'(gen_application(_A), voidret)
c_code local build_ref_3
	ret_reg &ref[3]
	call_c   build_ref_0()
	call_c   build_ref_1()
	call_c   build_ref_2()
	call_c   Dyam_Create_Binary(&ref[0],&ref[1],&ref[2])
	move_ret ref[3]
	c_ret

;; TERM 2: voidret
c_code local build_ref_2
	ret_reg &ref[2]
	call_c   Dyam_Create_Atom("voidret")
	move_ret ref[2]
	c_ret

;; TERM 0: '*RITEM*'
c_code local build_ref_0
	ret_reg &ref[0]
	call_c   Dyam_Create_Atom("*RITEM*")
	move_ret ref[0]
	c_ret

;; TERM 4: special_app_model(_A)
c_code local build_ref_4
	ret_reg &ref[4]
	call_c   build_ref_260()
	call_c   Dyam_Create_Unary(&ref[260],V(0))
	move_ret ref[4]
	c_ret

;; TERM 260: special_app_model
c_code local build_ref_260
	ret_reg &ref[260]
	call_c   Dyam_Create_Atom("special_app_model")
	move_ret ref[260]
	c_ret

;; TERM 5: '*RITEM*'(special_app_model(_A), voidret)
c_code local build_ref_5
	ret_reg &ref[5]
	call_c   build_ref_0()
	call_c   build_ref_4()
	call_c   build_ref_2()
	call_c   Dyam_Create_Binary(&ref[0],&ref[4],&ref[2])
	move_ret ref[5]
	c_ret

;; TERM 6: seed(_A, _B)
c_code local build_ref_6
	ret_reg &ref[6]
	call_c   build_ref_261()
	call_c   Dyam_Create_Binary(&ref[261],V(0),V(1))
	move_ret ref[6]
	c_ret

;; TERM 261: seed
c_code local build_ref_261
	ret_reg &ref[261]
	call_c   Dyam_Create_Atom("seed")
	move_ret ref[261]
	c_ret

;; TERM 7: '*RITEM*'(seed(_A, _B), voidret)
c_code local build_ref_7
	ret_reg &ref[7]
	call_c   build_ref_0()
	call_c   build_ref_6()
	call_c   build_ref_2()
	call_c   Dyam_Create_Binary(&ref[0],&ref[6],&ref[2])
	move_ret ref[7]
	c_ret

;; TERM 207: [_B,_C]
c_code local build_ref_207
	ret_reg &ref[207]
	call_c   Dyam_Create_Tupple(1,2,I(0))
	move_ret ref[207]
	c_ret

;; TERM 206: 'variable in test schedule ~w ~w'
c_code local build_ref_206
	ret_reg &ref[206]
	call_c   Dyam_Create_Atom("variable in test schedule ~w ~w")
	move_ret ref[206]
	c_ret

;; TERM 209: schedule :> succeed
c_code local build_ref_209
	ret_reg &ref[209]
	call_c   build_ref_208()
	call_c   build_ref_128()
	call_c   Dyam_Create_Binary(I(9),&ref[208],&ref[128])
	move_ret ref[209]
	c_ret

;; TERM 128: succeed
c_code local build_ref_128
	ret_reg &ref[128]
	call_c   Dyam_Create_Atom("succeed")
	move_ret ref[128]
	c_ret

;; TERM 208: schedule
c_code local build_ref_208
	ret_reg &ref[208]
	call_c   Dyam_Create_Atom("schedule")
	move_ret ref[208]
	c_ret

;; TERM 97: std
c_code local build_ref_97
	ret_reg &ref[97]
	call_c   Dyam_Create_Atom("std")
	move_ret ref[97]
	c_ret

;; TERM 212: schedule_last :> succeed
c_code local build_ref_212
	ret_reg &ref[212]
	call_c   build_ref_211()
	call_c   build_ref_128()
	call_c   Dyam_Create_Binary(I(9),&ref[211],&ref[128])
	move_ret ref[212]
	c_ret

;; TERM 211: schedule_last
c_code local build_ref_211
	ret_reg &ref[211]
	call_c   Dyam_Create_Atom("schedule_last")
	move_ret ref[211]
	c_ret

;; TERM 210: last
c_code local build_ref_210
	ret_reg &ref[210]
	call_c   Dyam_Create_Atom("last")
	move_ret ref[210]
	c_ret

;; TERM 214: schedule_first :> succeed
c_code local build_ref_214
	ret_reg &ref[214]
	call_c   build_ref_213()
	call_c   build_ref_128()
	call_c   Dyam_Create_Binary(I(9),&ref[213],&ref[128])
	move_ret ref[214]
	c_ret

;; TERM 213: schedule_first
c_code local build_ref_213
	ret_reg &ref[213]
	call_c   Dyam_Create_Atom("schedule_first")
	move_ret ref[213]
	c_ret

;; TERM 101: first
c_code local build_ref_101
	ret_reg &ref[101]
	call_c   Dyam_Create_Atom("first")
	move_ret ref[101]
	c_ret

;; TERM 216: call('Schedule_Prolog', []) :> succeed
c_code local build_ref_216
	ret_reg &ref[216]
	call_c   build_ref_215()
	call_c   build_ref_128()
	call_c   Dyam_Create_Binary(I(9),&ref[215],&ref[128])
	move_ret ref[216]
	c_ret

;; TERM 215: call('Schedule_Prolog', [])
c_code local build_ref_215
	ret_reg &ref[215]
	call_c   build_ref_262()
	call_c   build_ref_263()
	call_c   Dyam_Create_Binary(&ref[262],&ref[263],I(0))
	move_ret ref[215]
	c_ret

;; TERM 263: 'Schedule_Prolog'
c_code local build_ref_263
	ret_reg &ref[263]
	call_c   Dyam_Create_Atom("Schedule_Prolog")
	move_ret ref[263]
	c_ret

;; TERM 262: call
c_code local build_ref_262
	ret_reg &ref[262]
	call_c   Dyam_Create_Atom("call")
	move_ret ref[262]
	c_ret

;; TERM 113: prolog
c_code local build_ref_113
	ret_reg &ref[113]
	call_c   Dyam_Create_Atom("prolog")
	move_ret ref[113]
	c_ret

;; TERM 219: answer :> succeed
c_code local build_ref_219
	ret_reg &ref[219]
	call_c   build_ref_218()
	call_c   build_ref_128()
	call_c   Dyam_Create_Binary(I(9),&ref[218],&ref[128])
	move_ret ref[219]
	c_ret

;; TERM 218: answer
c_code local build_ref_218
	ret_reg &ref[218]
	call_c   Dyam_Create_Atom("answer")
	move_ret ref[218]
	c_ret

;; TERM 217: '*ANSWER*'{answer=> _E}
c_code local build_ref_217
	ret_reg &ref[217]
	call_c   build_ref_264()
	call_c   Dyam_Create_Unary(&ref[264],V(4))
	move_ret ref[217]
	c_ret

;; TERM 264: '*ANSWER*'!'$ft'
c_code local build_ref_264
	ret_reg &ref[264]
	call_c   build_ref_265()
	call_c   Dyam_Create_Atom_Module("$ft",&ref[265])
	move_ret ref[264]
	c_ret

;; TERM 265: '*ANSWER*'
c_code local build_ref_265
	ret_reg &ref[265]
	call_c   Dyam_Create_Atom("*ANSWER*")
	move_ret ref[265]
	c_ret

;; TERM 83: key(_F, _G, _D)
c_code local build_ref_83
	ret_reg &ref[83]
	call_c   build_ref_266()
	call_c   Dyam_Term_Start(&ref[266],3)
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[83]
	c_ret

;; TERM 266: key
c_code local build_ref_266
	ret_reg &ref[266]
	call_c   Dyam_Create_Atom("key")
	move_ret ref[266]
	c_ret

;; TERM 82: key(_F, _G, _E)
c_code local build_ref_82
	ret_reg &ref[82]
	call_c   build_ref_266()
	call_c   Dyam_Term_Start(&ref[266],3)
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[82]
	c_ret

;; TERM 81: c(0)
c_code local build_ref_81
	ret_reg &ref[81]
	call_c   build_ref_267()
	call_c   Dyam_Create_Unary(&ref[267],N(0))
	move_ret ref[81]
	c_ret

;; TERM 267: c
c_code local build_ref_267
	ret_reg &ref[267]
	call_c   Dyam_Create_Atom("c")
	move_ret ref[267]
	c_ret

;; TERM 80: noop
c_code local build_ref_80
	ret_reg &ref[80]
	call_c   Dyam_Create_Atom("noop")
	move_ret ref[80]
	c_ret

;; TERM 84: key(_H, [], [])
c_code local build_ref_84
	ret_reg &ref[84]
	call_c   build_ref_266()
	call_c   Dyam_Term_Start(&ref[266],3)
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_End()
	move_ret ref[84]
	c_ret

c_code local build_seed_24
	ret_reg &seed[24]
	call_c   build_ref_8()
	call_c   build_ref_9()
	call_c   Dyam_Seed_Start(&ref[8],&ref[9],&ref[9],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[24]
	c_ret

pl_code local fun0
	pl_ret

;; TERM 9: empty_app_key([])
c_code local build_ref_9
	ret_reg &ref[9]
	call_c   build_ref_268()
	call_c   Dyam_Create_Unary(&ref[268],I(0))
	move_ret ref[9]
	c_ret

;; TERM 268: empty_app_key
c_code local build_ref_268
	ret_reg &ref[268]
	call_c   Dyam_Create_Atom("empty_app_key")
	move_ret ref[268]
	c_ret

;; TERM 8: '*DATABASE*'
c_code local build_ref_8
	ret_reg &ref[8]
	call_c   Dyam_Create_Atom("*DATABASE*")
	move_ret ref[8]
	c_ret

c_code local build_seed_25
	ret_reg &seed[25]
	call_c   build_ref_8()
	call_c   build_ref_10()
	call_c   Dyam_Seed_Start(&ref[8],&ref[10],&ref[10],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[25]
	c_ret

;; TERM 10: empty_app_key(key(_B, [], []))
c_code local build_ref_10
	ret_reg &ref[10]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_266()
	call_c   Dyam_Term_Start(&ref[266],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_268()
	call_c   Dyam_Create_Unary(&ref[268],R(0))
	move_ret ref[10]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_16
	ret_reg &seed[16]
	call_c   build_ref_16()
	call_c   build_ref_20()
	call_c   Dyam_Seed_Start(&ref[16],&ref[20],I(0),fun0,1)
	call_c   build_ref_21()
	call_c   Dyam_Seed_Add_Comp(&ref[21],fun30,0)
	call_c   Dyam_Seed_End()
	move_ret seed[16]
	c_ret

;; TERM 21: '*PROLOG-ITEM*'{top=> component(_B, _C, _D, _E), cont=> _A}
c_code local build_ref_21
	ret_reg &ref[21]
	call_c   build_ref_269()
	call_c   build_ref_18()
	call_c   Dyam_Create_Binary(&ref[269],&ref[18],V(0))
	move_ret ref[21]
	c_ret

;; TERM 18: component(_B, _C, _D, _E)
c_code local build_ref_18
	ret_reg &ref[18]
	call_c   build_ref_270()
	call_c   Dyam_Term_Start(&ref[270],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[18]
	c_ret

;; TERM 270: component
c_code local build_ref_270
	ret_reg &ref[270]
	call_c   Dyam_Create_Atom("component")
	move_ret ref[270]
	c_ret

;; TERM 269: '*PROLOG-ITEM*'!'$ft'
c_code local build_ref_269
	ret_reg &ref[269]
	call_c   build_ref_26()
	call_c   Dyam_Create_Atom_Module("$ft",&ref[26])
	move_ret ref[269]
	c_ret

;; TERM 26: '*PROLOG-ITEM*'
c_code local build_ref_26
	ret_reg &ref[26]
	call_c   Dyam_Create_Atom("*PROLOG-ITEM*")
	move_ret ref[26]
	c_ret

pl_code local fun29
	call_c   Dyam_Remove_Choice()
	pl_fail

;; TERM 22: gen_application(application{item=> _F, trans=> _B, item_comp=> _G, code=> _H, item_id=> _I, trans_id=> _C, mode=> _J, restrict=> _K, out_env=> _L, key=> _M})
c_code local build_ref_22
	ret_reg &ref[22]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_271()
	call_c   Dyam_Term_Start(&ref[271],10)
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_259()
	call_c   Dyam_Create_Unary(&ref[259],R(0))
	move_ret ref[22]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 271: application!'$ft'
c_code local build_ref_271
	ret_reg &ref[271]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_266()
	call_c   Dyam_Create_List(&ref[266],I(0))
	move_ret R(0)
	call_c   build_ref_280()
	call_c   Dyam_Create_List(&ref[280],R(0))
	move_ret R(0)
	call_c   build_ref_279()
	call_c   Dyam_Create_List(&ref[279],R(0))
	move_ret R(0)
	call_c   build_ref_278()
	call_c   Dyam_Create_List(&ref[278],R(0))
	move_ret R(0)
	call_c   build_ref_277()
	call_c   Dyam_Create_List(&ref[277],R(0))
	move_ret R(0)
	call_c   build_ref_276()
	call_c   Dyam_Create_List(&ref[276],R(0))
	move_ret R(0)
	call_c   build_ref_66()
	call_c   Dyam_Create_List(&ref[66],R(0))
	move_ret R(0)
	call_c   build_ref_275()
	call_c   Dyam_Create_List(&ref[275],R(0))
	move_ret R(0)
	call_c   build_ref_274()
	call_c   Dyam_Create_List(&ref[274],R(0))
	move_ret R(0)
	call_c   build_ref_273()
	call_c   Dyam_Create_List(&ref[273],R(0))
	move_ret R(0)
	call_c   build_ref_272()
	call_c   Dyam_Create_Atom_Module("$ft",&ref[272])
	move_ret ref[271]
	call_c   DYAM_Feature_2(&ref[272],R(0))
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 272: application
c_code local build_ref_272
	ret_reg &ref[272]
	call_c   Dyam_Create_Atom("application")
	move_ret ref[272]
	c_ret

;; TERM 273: item
c_code local build_ref_273
	ret_reg &ref[273]
	call_c   Dyam_Create_Atom("item")
	move_ret ref[273]
	c_ret

;; TERM 274: trans
c_code local build_ref_274
	ret_reg &ref[274]
	call_c   Dyam_Create_Atom("trans")
	move_ret ref[274]
	c_ret

;; TERM 275: item_comp
c_code local build_ref_275
	ret_reg &ref[275]
	call_c   Dyam_Create_Atom("item_comp")
	move_ret ref[275]
	c_ret

;; TERM 66: code
c_code local build_ref_66
	ret_reg &ref[66]
	call_c   Dyam_Create_Atom("code")
	move_ret ref[66]
	c_ret

;; TERM 276: item_id
c_code local build_ref_276
	ret_reg &ref[276]
	call_c   Dyam_Create_Atom("item_id")
	move_ret ref[276]
	c_ret

;; TERM 277: trans_id
c_code local build_ref_277
	ret_reg &ref[277]
	call_c   Dyam_Create_Atom("trans_id")
	move_ret ref[277]
	c_ret

;; TERM 278: mode
c_code local build_ref_278
	ret_reg &ref[278]
	call_c   Dyam_Create_Atom("mode")
	move_ret ref[278]
	c_ret

;; TERM 279: restrict
c_code local build_ref_279
	ret_reg &ref[279]
	call_c   Dyam_Create_Atom("restrict")
	move_ret ref[279]
	c_ret

;; TERM 280: out_env
c_code local build_ref_280
	ret_reg &ref[280]
	call_c   Dyam_Create_Atom("out_env")
	move_ret ref[280]
	c_ret

c_code local build_seed_26
	ret_reg &seed[26]
	call_c   build_ref_26()
	call_c   build_ref_27()
	call_c   Dyam_Seed_Start(&ref[26],&ref[27],I(0),fun7,1)
	call_c   build_ref_30()
	call_c   Dyam_Seed_Add_Comp(&ref[30],&ref[27],0)
	call_c   Dyam_Seed_End()
	move_ret seed[26]
	c_ret

pl_code local fun7
	pl_jump  Complete(0,0)

;; TERM 30: '*PROLOG-FIRST*'(app_model(application{item=> _F, trans=> _B, item_comp=> _P, code=> _Q, item_id=> _I, trans_id=> _C, mode=> trans, restrict=> _R, out_env=> _S, key=> _M})) :> '$$HOLE$$'
c_code local build_ref_30
	ret_reg &ref[30]
	call_c   build_ref_29()
	call_c   Dyam_Create_Binary(I(9),&ref[29],I(7))
	move_ret ref[30]
	c_ret

;; TERM 29: '*PROLOG-FIRST*'(app_model(application{item=> _F, trans=> _B, item_comp=> _P, code=> _Q, item_id=> _I, trans_id=> _C, mode=> trans, restrict=> _R, out_env=> _S, key=> _M}))
c_code local build_ref_29
	ret_reg &ref[29]
	call_c   build_ref_17()
	call_c   build_ref_28()
	call_c   Dyam_Create_Unary(&ref[17],&ref[28])
	move_ret ref[29]
	c_ret

;; TERM 28: app_model(application{item=> _F, trans=> _B, item_comp=> _P, code=> _Q, item_id=> _I, trans_id=> _C, mode=> trans, restrict=> _R, out_env=> _S, key=> _M})
c_code local build_ref_28
	ret_reg &ref[28]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_271()
	call_c   build_ref_274()
	call_c   Dyam_Term_Start(&ref[271],10)
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(&ref[274])
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_281()
	call_c   Dyam_Create_Unary(&ref[281],R(0))
	move_ret ref[28]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 281: app_model
c_code local build_ref_281
	ret_reg &ref[281]
	call_c   Dyam_Create_Atom("app_model")
	move_ret ref[281]
	c_ret

;; TERM 17: '*PROLOG-FIRST*'
c_code local build_ref_17
	ret_reg &ref[17]
	call_c   Dyam_Create_Atom("*PROLOG-FIRST*")
	move_ret ref[17]
	c_ret

;; TERM 27: '*PROLOG-ITEM*'{top=> app_model(application{item=> _F, trans=> _B, item_comp=> _P, code=> _Q, item_id=> _I, trans_id=> _C, mode=> trans, restrict=> _R, out_env=> _S, key=> _M}), cont=> '$CLOSURE'('$fun'(6, 0, 1082449184), '$TUPPLE'(35046582125148))}
c_code local build_ref_27
	ret_reg &ref[27]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,378592256)
	move_ret R(0)
	call_c   Dyam_Closure_Aux(fun6,R(0))
	move_ret R(0)
	call_c   build_ref_269()
	call_c   build_ref_28()
	call_c   Dyam_Create_Binary(&ref[269],&ref[28],R(0))
	move_ret ref[27]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 25: direct(_C, _T, _I, _U, _S, _N, _O)
c_code local build_ref_25
	ret_reg &ref[25]
	call_c   build_ref_282()
	call_c   Dyam_Term_Start(&ref[282],7)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_End()
	move_ret ref[25]
	c_ret

;; TERM 282: direct
c_code local build_ref_282
	ret_reg &ref[282]
	call_c   Dyam_Create_Atom("direct")
	move_ret ref[282]
	c_ret

long local pool_fun5[2]=[1,build_ref_25]

pl_code local fun5
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(3),&ref[25])
	fail_ret
fun4:
	call_c   Dyam_Reg_Load(0,V(16))
	call_c   Dyam_Reg_Load(2,V(20))
	pl_call  pred_label_code_2()
	call_c   Dyam_Reg_Load(0,V(5))
	call_c   Dyam_Reg_Load(2,V(19))
	pl_call  pred_label_term_2()
	call_c   Dyam_Deallocate()
	pl_jump  Follow_Cont(V(0))


;; TERM 23: yes
c_code local build_ref_23
	ret_reg &ref[23]
	call_c   Dyam_Create_Atom("yes")
	move_ret ref[23]
	c_ret

;; TERM 24: direct(_T, _U, _S, _N, _O)
c_code local build_ref_24
	ret_reg &ref[24]
	call_c   build_ref_282()
	call_c   Dyam_Term_Start(&ref[282],5)
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_End()
	move_ret ref[24]
	c_ret

long local pool_fun6[4]=[65538,build_ref_23,build_ref_24,pool_fun5]

pl_code local fun6
	call_c   Dyam_Pool(pool_fun6)
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun5)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(17),&ref[23])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(3),&ref[24])
	fail_ret
	pl_jump  fun4()

pl_code local fun20
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Light_Object(R(0))
	pl_jump  Schedule_Prolog()

long local pool_fun28[2]=[1,build_seed_26]

long local pool_fun30[4]=[65538,build_ref_21,build_ref_22,pool_fun28]

pl_code local fun30
	call_c   Dyam_Pool(pool_fun30)
	call_c   Dyam_Unify_Item(&ref[21])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun29)
	call_c   Dyam_Set_Cut()
	pl_call  Callret_2(V(21),&ref[22])
	call_c   Dyam_Cut()
fun28:
	pl_call  Object_1(V(21))
	call_c   Dyam_Reg_Load(0,V(12))
	call_c   Dyam_Reg_Load(2,V(4))
	move     V(13), R(4)
	move     S(5), R(5)
	move     V(14), R(6)
	move     S(5), R(7)
	pl_call  pred_handle_appinfo_4()
	call_c   Dyam_Deallocate()
	pl_jump  fun20(&seed[26],12)


;; TERM 20: '*PROLOG-FIRST*'(component(_B, _C, _D, _E)) :> []
c_code local build_ref_20
	ret_reg &ref[20]
	call_c   build_ref_19()
	call_c   Dyam_Create_Binary(I(9),&ref[19],I(0))
	move_ret ref[20]
	c_ret

;; TERM 19: '*PROLOG-FIRST*'(component(_B, _C, _D, _E))
c_code local build_ref_19
	ret_reg &ref[19]
	call_c   build_ref_17()
	call_c   build_ref_18()
	call_c   Dyam_Create_Unary(&ref[17],&ref[18])
	move_ret ref[19]
	c_ret

;; TERM 16: '*PROLOG_FIRST*'
c_code local build_ref_16
	ret_reg &ref[16]
	call_c   Dyam_Create_Atom("*PROLOG_FIRST*")
	move_ret ref[16]
	c_ret

c_code local build_seed_0
	ret_reg &seed[0]
	call_c   build_ref_11()
	call_c   build_ref_15()
	call_c   Dyam_Seed_Start(&ref[11],&ref[15],I(0),fun0,1)
	call_c   build_ref_14()
	call_c   Dyam_Seed_Add_Comp(&ref[14],fun3,0)
	call_c   Dyam_Seed_End()
	move_ret seed[0]
	c_ret

;; TERM 14: '*GUARD*'(seed_model(seed{model=> '*DATABASE*'(_B), id=> _C, anchor=> _B, subs_comp=> _D, code=> _E, go=> _F, tabule=> yes, schedule=> no, subsume=> yes, model_ref=> _G, subs_comp_ref=> _H, c_type=> '*DATABASE*', c_type_ref=> _I, out_env=> _J, appinfo=> _K, tail_flag=> _L}))
c_code local build_ref_14
	ret_reg &ref[14]
	call_c   build_ref_12()
	call_c   build_ref_13()
	call_c   Dyam_Create_Unary(&ref[12],&ref[13])
	move_ret ref[14]
	c_ret

;; TERM 13: seed_model(seed{model=> '*DATABASE*'(_B), id=> _C, anchor=> _B, subs_comp=> _D, code=> _E, go=> _F, tabule=> yes, schedule=> no, subsume=> yes, model_ref=> _G, subs_comp_ref=> _H, c_type=> '*DATABASE*', c_type_ref=> _I, out_env=> _J, appinfo=> _K, tail_flag=> _L})
c_code local build_ref_13
	ret_reg &ref[13]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_8()
	call_c   Dyam_Create_Unary(&ref[8],V(1))
	move_ret R(0)
	call_c   build_ref_284()
	call_c   build_ref_23()
	call_c   build_ref_140()
	call_c   Dyam_Term_Start(&ref[284],16)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(&ref[23])
	call_c   Dyam_Term_Arg(&ref[140])
	call_c   Dyam_Term_Arg(&ref[23])
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(&ref[8])
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_283()
	call_c   Dyam_Create_Unary(&ref[283],R(0))
	move_ret ref[13]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 283: seed_model
c_code local build_ref_283
	ret_reg &ref[283]
	call_c   Dyam_Create_Atom("seed_model")
	move_ret ref[283]
	c_ret

;; TERM 140: no
c_code local build_ref_140
	ret_reg &ref[140]
	call_c   Dyam_Create_Atom("no")
	move_ret ref[140]
	c_ret

;; TERM 284: seed!'$ft'
c_code local build_ref_284
	ret_reg &ref[284]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_296()
	call_c   Dyam_Create_List(&ref[296],I(0))
	move_ret R(0)
	call_c   build_ref_295()
	call_c   Dyam_Create_List(&ref[295],R(0))
	move_ret R(0)
	call_c   build_ref_280()
	call_c   Dyam_Create_List(&ref[280],R(0))
	move_ret R(0)
	call_c   build_ref_294()
	call_c   Dyam_Create_List(&ref[294],R(0))
	move_ret R(0)
	call_c   build_ref_293()
	call_c   Dyam_Create_List(&ref[293],R(0))
	move_ret R(0)
	call_c   build_ref_292()
	call_c   Dyam_Create_List(&ref[292],R(0))
	move_ret R(0)
	call_c   build_ref_291()
	call_c   Dyam_Create_List(&ref[291],R(0))
	move_ret R(0)
	call_c   build_ref_290()
	call_c   Dyam_Create_List(&ref[290],R(0))
	move_ret R(0)
	call_c   build_ref_208()
	call_c   Dyam_Create_List(&ref[208],R(0))
	move_ret R(0)
	call_c   build_ref_178()
	call_c   Dyam_Create_List(&ref[178],R(0))
	move_ret R(0)
	call_c   build_ref_289()
	call_c   Dyam_Create_List(&ref[289],R(0))
	move_ret R(0)
	call_c   build_ref_66()
	call_c   Dyam_Create_List(&ref[66],R(0))
	move_ret R(0)
	call_c   build_ref_288()
	call_c   Dyam_Create_List(&ref[288],R(0))
	move_ret R(0)
	call_c   build_ref_287()
	call_c   Dyam_Create_List(&ref[287],R(0))
	move_ret R(0)
	call_c   build_ref_286()
	call_c   Dyam_Create_List(&ref[286],R(0))
	move_ret R(0)
	call_c   build_ref_285()
	call_c   Dyam_Create_List(&ref[285],R(0))
	move_ret R(0)
	call_c   build_ref_261()
	call_c   Dyam_Create_Atom_Module("$ft",&ref[261])
	move_ret ref[284]
	call_c   DYAM_Feature_2(&ref[261],R(0))
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 285: model
c_code local build_ref_285
	ret_reg &ref[285]
	call_c   Dyam_Create_Atom("model")
	move_ret ref[285]
	c_ret

;; TERM 286: id
c_code local build_ref_286
	ret_reg &ref[286]
	call_c   Dyam_Create_Atom("id")
	move_ret ref[286]
	c_ret

;; TERM 287: anchor
c_code local build_ref_287
	ret_reg &ref[287]
	call_c   Dyam_Create_Atom("anchor")
	move_ret ref[287]
	c_ret

;; TERM 288: subs_comp
c_code local build_ref_288
	ret_reg &ref[288]
	call_c   Dyam_Create_Atom("subs_comp")
	move_ret ref[288]
	c_ret

;; TERM 289: go
c_code local build_ref_289
	ret_reg &ref[289]
	call_c   Dyam_Create_Atom("go")
	move_ret ref[289]
	c_ret

;; TERM 178: tabule
c_code local build_ref_178
	ret_reg &ref[178]
	call_c   Dyam_Create_Atom("tabule")
	move_ret ref[178]
	c_ret

;; TERM 290: subsume
c_code local build_ref_290
	ret_reg &ref[290]
	call_c   Dyam_Create_Atom("subsume")
	move_ret ref[290]
	c_ret

;; TERM 291: model_ref
c_code local build_ref_291
	ret_reg &ref[291]
	call_c   Dyam_Create_Atom("model_ref")
	move_ret ref[291]
	c_ret

;; TERM 292: subs_comp_ref
c_code local build_ref_292
	ret_reg &ref[292]
	call_c   Dyam_Create_Atom("subs_comp_ref")
	move_ret ref[292]
	c_ret

;; TERM 293: c_type
c_code local build_ref_293
	ret_reg &ref[293]
	call_c   Dyam_Create_Atom("c_type")
	move_ret ref[293]
	c_ret

;; TERM 294: c_type_ref
c_code local build_ref_294
	ret_reg &ref[294]
	call_c   Dyam_Create_Atom("c_type_ref")
	move_ret ref[294]
	c_ret

;; TERM 295: appinfo
c_code local build_ref_295
	ret_reg &ref[295]
	call_c   Dyam_Create_Atom("appinfo")
	move_ret ref[295]
	c_ret

;; TERM 296: tail_flag
c_code local build_ref_296
	ret_reg &ref[296]
	call_c   Dyam_Create_Atom("tail_flag")
	move_ret ref[296]
	c_ret

;; TERM 12: '*GUARD*'
c_code local build_ref_12
	ret_reg &ref[12]
	call_c   Dyam_Create_Atom("*GUARD*")
	move_ret ref[12]
	c_ret

pl_code local fun3
	call_c   build_ref_14()
	call_c   Dyam_Unify_Item(&ref[14])
	fail_ret
	pl_ret

;; TERM 15: '*GUARD*'(seed_model(seed{model=> '*DATABASE*'(_B), id=> _C, anchor=> _B, subs_comp=> _D, code=> _E, go=> _F, tabule=> yes, schedule=> no, subsume=> yes, model_ref=> _G, subs_comp_ref=> _H, c_type=> '*DATABASE*', c_type_ref=> _I, out_env=> _J, appinfo=> _K, tail_flag=> _L})) :> []
c_code local build_ref_15
	ret_reg &ref[15]
	call_c   build_ref_14()
	call_c   Dyam_Create_Binary(I(9),&ref[14],I(0))
	move_ret ref[15]
	c_ret

;; TERM 11: '*GUARD_CLAUSE*'
c_code local build_ref_11
	ret_reg &ref[11]
	call_c   Dyam_Create_Atom("*GUARD_CLAUSE*")
	move_ret ref[11]
	c_ret

c_code local build_seed_1
	ret_reg &seed[1]
	call_c   build_ref_11()
	call_c   build_ref_33()
	call_c   Dyam_Seed_Start(&ref[11],&ref[33],I(0),fun0,1)
	call_c   build_ref_32()
	call_c   Dyam_Seed_Add_Comp(&ref[32],fun8,0)
	call_c   Dyam_Seed_End()
	move_ret seed[1]
	c_ret

;; TERM 32: '*GUARD*'(seed_model(seed{model=> '*ANSWER*'{answer=> _B}, id=> _C, anchor=> _D, subs_comp=> _E, code=> _F, go=> _G, tabule=> yes, schedule=> no, subsume=> no, model_ref=> _H, subs_comp_ref=> _I, c_type=> '*ANSWER*', c_type_ref=> _J, out_env=> _K, appinfo=> _L, tail_flag=> _M}))
c_code local build_ref_32
	ret_reg &ref[32]
	call_c   build_ref_12()
	call_c   build_ref_31()
	call_c   Dyam_Create_Unary(&ref[12],&ref[31])
	move_ret ref[32]
	c_ret

;; TERM 31: seed_model(seed{model=> '*ANSWER*'{answer=> _B}, id=> _C, anchor=> _D, subs_comp=> _E, code=> _F, go=> _G, tabule=> yes, schedule=> no, subsume=> no, model_ref=> _H, subs_comp_ref=> _I, c_type=> '*ANSWER*', c_type_ref=> _J, out_env=> _K, appinfo=> _L, tail_flag=> _M})
c_code local build_ref_31
	ret_reg &ref[31]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_264()
	call_c   Dyam_Create_Unary(&ref[264],V(1))
	move_ret R(0)
	call_c   build_ref_284()
	call_c   build_ref_23()
	call_c   build_ref_140()
	call_c   build_ref_265()
	call_c   Dyam_Term_Start(&ref[284],16)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(&ref[23])
	call_c   Dyam_Term_Arg(&ref[140])
	call_c   Dyam_Term_Arg(&ref[140])
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(&ref[265])
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_283()
	call_c   Dyam_Create_Unary(&ref[283],R(0))
	move_ret ref[31]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

pl_code local fun8
	call_c   build_ref_32()
	call_c   Dyam_Unify_Item(&ref[32])
	fail_ret
	pl_ret

;; TERM 33: '*GUARD*'(seed_model(seed{model=> '*ANSWER*'{answer=> _B}, id=> _C, anchor=> _D, subs_comp=> _E, code=> _F, go=> _G, tabule=> yes, schedule=> no, subsume=> no, model_ref=> _H, subs_comp_ref=> _I, c_type=> '*ANSWER*', c_type_ref=> _J, out_env=> _K, appinfo=> _L, tail_flag=> _M})) :> []
c_code local build_ref_33
	ret_reg &ref[33]
	call_c   build_ref_32()
	call_c   Dyam_Create_Binary(I(9),&ref[32],I(0))
	move_ret ref[33]
	c_ret

c_code local build_seed_2
	ret_reg &seed[2]
	call_c   build_ref_11()
	call_c   build_ref_36()
	call_c   Dyam_Seed_Start(&ref[11],&ref[36],I(0),fun0,1)
	call_c   build_ref_35()
	call_c   Dyam_Seed_Add_Comp(&ref[35],fun9,0)
	call_c   Dyam_Seed_End()
	move_ret seed[2]
	c_ret

;; TERM 35: '*GUARD*'(seed_model(seed{model=> '*GUARD*'(_B), id=> _C, anchor=> _D, subs_comp=> _E, code=> _F, go=> _G, tabule=> light, schedule=> prolog, subsume=> no, model_ref=> _H, subs_comp_ref=> _I, c_type=> '*GUARD*', c_type_ref=> _J, out_env=> _K, appinfo=> _L, tail_flag=> _M}))
c_code local build_ref_35
	ret_reg &ref[35]
	call_c   build_ref_12()
	call_c   build_ref_34()
	call_c   Dyam_Create_Unary(&ref[12],&ref[34])
	move_ret ref[35]
	c_ret

;; TERM 34: seed_model(seed{model=> '*GUARD*'(_B), id=> _C, anchor=> _D, subs_comp=> _E, code=> _F, go=> _G, tabule=> light, schedule=> prolog, subsume=> no, model_ref=> _H, subs_comp_ref=> _I, c_type=> '*GUARD*', c_type_ref=> _J, out_env=> _K, appinfo=> _L, tail_flag=> _M})
c_code local build_ref_34
	ret_reg &ref[34]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_12()
	call_c   Dyam_Create_Unary(&ref[12],V(1))
	move_ret R(0)
	call_c   build_ref_284()
	call_c   build_ref_175()
	call_c   build_ref_113()
	call_c   build_ref_140()
	call_c   Dyam_Term_Start(&ref[284],16)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(&ref[175])
	call_c   Dyam_Term_Arg(&ref[113])
	call_c   Dyam_Term_Arg(&ref[140])
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(&ref[12])
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_283()
	call_c   Dyam_Create_Unary(&ref[283],R(0))
	move_ret ref[34]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 175: light
c_code local build_ref_175
	ret_reg &ref[175]
	call_c   Dyam_Create_Atom("light")
	move_ret ref[175]
	c_ret

pl_code local fun9
	call_c   build_ref_35()
	call_c   Dyam_Unify_Item(&ref[35])
	fail_ret
	pl_ret

;; TERM 36: '*GUARD*'(seed_model(seed{model=> '*GUARD*'(_B), id=> _C, anchor=> _D, subs_comp=> _E, code=> _F, go=> _G, tabule=> light, schedule=> prolog, subsume=> no, model_ref=> _H, subs_comp_ref=> _I, c_type=> '*GUARD*', c_type_ref=> _J, out_env=> _K, appinfo=> _L, tail_flag=> _M})) :> []
c_code local build_ref_36
	ret_reg &ref[36]
	call_c   build_ref_35()
	call_c   Dyam_Create_Binary(I(9),&ref[35],I(0))
	move_ret ref[36]
	c_ret

c_code local build_seed_6
	ret_reg &seed[6]
	call_c   build_ref_11()
	call_c   build_ref_39()
	call_c   Dyam_Seed_Start(&ref[11],&ref[39],I(0),fun0,1)
	call_c   build_ref_38()
	call_c   Dyam_Seed_Add_Comp(&ref[38],fun10,0)
	call_c   Dyam_Seed_End()
	move_ret seed[6]
	c_ret

;; TERM 38: '*GUARD*'(seed_model(seed{model=> '*PROLOG-ITEM*'{top=> _B, cont=> _C}, id=> _D, anchor=> _E, subs_comp=> _F, code=> _G, go=> _H, tabule=> light, schedule=> prolog, subsume=> no, model_ref=> _I, subs_comp_ref=> _J, c_type=> '*PROLOG-ITEM*', c_type_ref=> _K, out_env=> _L, appinfo=> _M, tail_flag=> _N}))
c_code local build_ref_38
	ret_reg &ref[38]
	call_c   build_ref_12()
	call_c   build_ref_37()
	call_c   Dyam_Create_Unary(&ref[12],&ref[37])
	move_ret ref[38]
	c_ret

;; TERM 37: seed_model(seed{model=> '*PROLOG-ITEM*'{top=> _B, cont=> _C}, id=> _D, anchor=> _E, subs_comp=> _F, code=> _G, go=> _H, tabule=> light, schedule=> prolog, subsume=> no, model_ref=> _I, subs_comp_ref=> _J, c_type=> '*PROLOG-ITEM*', c_type_ref=> _K, out_env=> _L, appinfo=> _M, tail_flag=> _N})
c_code local build_ref_37
	ret_reg &ref[37]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_269()
	call_c   Dyam_Create_Binary(&ref[269],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_284()
	call_c   build_ref_175()
	call_c   build_ref_113()
	call_c   build_ref_140()
	call_c   build_ref_26()
	call_c   Dyam_Term_Start(&ref[284],16)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(&ref[175])
	call_c   Dyam_Term_Arg(&ref[113])
	call_c   Dyam_Term_Arg(&ref[140])
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(&ref[26])
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_283()
	call_c   Dyam_Create_Unary(&ref[283],R(0))
	move_ret ref[37]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

pl_code local fun10
	call_c   build_ref_38()
	call_c   Dyam_Unify_Item(&ref[38])
	fail_ret
	pl_ret

;; TERM 39: '*GUARD*'(seed_model(seed{model=> '*PROLOG-ITEM*'{top=> _B, cont=> _C}, id=> _D, anchor=> _E, subs_comp=> _F, code=> _G, go=> _H, tabule=> light, schedule=> prolog, subsume=> no, model_ref=> _I, subs_comp_ref=> _J, c_type=> '*PROLOG-ITEM*', c_type_ref=> _K, out_env=> _L, appinfo=> _M, tail_flag=> _N})) :> []
c_code local build_ref_39
	ret_reg &ref[39]
	call_c   build_ref_38()
	call_c   Dyam_Create_Binary(I(9),&ref[38],I(0))
	move_ret ref[39]
	c_ret

c_code local build_seed_13
	ret_reg &seed[13]
	call_c   build_ref_40()
	call_c   build_ref_43()
	call_c   Dyam_Seed_Start(&ref[40],&ref[43],I(0),fun14,1)
	call_c   build_ref_45()
	call_c   Dyam_Seed_Add_Comp(&ref[45],fun18,0)
	call_c   Dyam_Seed_End()
	move_ret seed[13]
	c_ret

;; TERM 45: '*CITEM*'(gen_application(application{item=> none, trans=> _B, item_comp=> _C, code=> _D, item_id=> none, trans_id=> 0, mode=> gen, restrict=> _E, out_env=> _F, key=> _G}), _A)
c_code local build_ref_45
	ret_reg &ref[45]
	call_c   build_ref_44()
	call_c   build_ref_41()
	call_c   Dyam_Create_Binary(&ref[44],&ref[41],V(0))
	move_ret ref[45]
	c_ret

;; TERM 41: gen_application(application{item=> none, trans=> _B, item_comp=> _C, code=> _D, item_id=> none, trans_id=> 0, mode=> gen, restrict=> _E, out_env=> _F, key=> _G})
c_code local build_ref_41
	ret_reg &ref[41]
	call_c   build_ref_259()
	call_c   build_ref_61()
	call_c   Dyam_Create_Unary(&ref[259],&ref[61])
	move_ret ref[41]
	c_ret

;; TERM 61: application{item=> none, trans=> _B, item_comp=> _C, code=> _D, item_id=> none, trans_id=> 0, mode=> gen, restrict=> _E, out_env=> _F, key=> _G}
c_code local build_ref_61
	ret_reg &ref[61]
	call_c   build_ref_271()
	call_c   build_ref_297()
	call_c   build_ref_298()
	call_c   Dyam_Term_Start(&ref[271],10)
	call_c   Dyam_Term_Arg(&ref[297])
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(&ref[297])
	call_c   Dyam_Term_Arg(N(0))
	call_c   Dyam_Term_Arg(&ref[298])
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[61]
	c_ret

;; TERM 298: gen
c_code local build_ref_298
	ret_reg &ref[298]
	call_c   Dyam_Create_Atom("gen")
	move_ret ref[298]
	c_ret

;; TERM 297: none
c_code local build_ref_297
	ret_reg &ref[297]
	call_c   Dyam_Create_Atom("none")
	move_ret ref[297]
	c_ret

;; TERM 44: '*CITEM*'
c_code local build_ref_44
	ret_reg &ref[44]
	call_c   Dyam_Create_Atom("*CITEM*")
	move_ret ref[44]
	c_ret

;; TERM 60: '$CLOSURE'('$fun'(17, 0, 1082539404), '$TUPPLE'(35046582649296))
c_code local build_ref_60
	ret_reg &ref[60]
	call_c   build_ref_59()
	call_c   Dyam_Closure_Aux(fun17,&ref[59])
	move_ret ref[60]
	c_ret

;; TERM 54: special_app(_B)
c_code local build_ref_54
	ret_reg &ref[54]
	call_c   build_ref_299()
	call_c   Dyam_Create_Unary(&ref[299],V(1))
	move_ret ref[54]
	c_ret

;; TERM 299: special_app
c_code local build_ref_299
	ret_reg &ref[299]
	call_c   Dyam_Create_Atom("special_app")
	move_ret ref[299]
	c_ret

c_code local build_seed_29
	ret_reg &seed[29]
	call_c   build_ref_0()
	call_c   build_ref_55()
	call_c   Dyam_Seed_Start(&ref[0],&ref[55],&ref[55],fun7,1)
	call_c   build_ref_56()
	call_c   Dyam_Seed_Add_Comp(&ref[56],&ref[55],0)
	call_c   Dyam_Seed_End()
	move_ret seed[29]
	c_ret

;; TERM 56: '*RITEM*'(_A, voidret) :> '$$HOLE$$'
c_code local build_ref_56
	ret_reg &ref[56]
	call_c   build_ref_55()
	call_c   Dyam_Create_Binary(I(9),&ref[55],I(7))
	move_ret ref[56]
	c_ret

;; TERM 55: '*RITEM*'(_A, voidret)
c_code local build_ref_55
	ret_reg &ref[55]
	call_c   build_ref_0()
	call_c   build_ref_2()
	call_c   Dyam_Create_Binary(&ref[0],V(0),&ref[2])
	move_ret ref[55]
	c_ret

pl_code local fun12
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Choice(fun11)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Subsume(R(0))
	fail_ret
	call_c   Dyam_Cut()
	pl_ret

long local pool_fun17[3]=[2,build_ref_54,build_seed_29]

pl_code local fun17
	call_c   Dyam_Pool(pool_fun17)
	call_c   DYAM_evpred_assert_1(&ref[54])
	pl_jump  fun12(&seed[29],2)

;; TERM 59: '$TUPPLE'(35046582649296)
c_code local build_ref_59
	ret_reg &ref[59]
	call_c   Dyam_Create_Simple_Tupple(0,402653184)
	move_ret ref[59]
	c_ret

long local pool_fun18[4]=[3,build_ref_45,build_ref_60,build_ref_61]

pl_code local fun18
	call_c   Dyam_Pool(pool_fun18)
	call_c   Dyam_Unify_Item(&ref[45])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[60], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	move     &ref[61], R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Deallocate(3)
fun16:
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Reg_Bind(2,V(3))
	call_c   Dyam_Reg_Bind(4,V(2))
	pl_call  fun12(&seed[27],1)
	pl_call  fun15(&seed[28],2,V(3))
	call_c   Dyam_Deallocate()
	pl_ret


pl_code local fun14
	pl_jump  Apply(0,0)

;; TERM 43: '*FIRST*'(gen_application(application{item=> none, trans=> _B, item_comp=> _C, code=> _D, item_id=> none, trans_id=> 0, mode=> gen, restrict=> _E, out_env=> _F, key=> _G})) :> []
c_code local build_ref_43
	ret_reg &ref[43]
	call_c   build_ref_42()
	call_c   Dyam_Create_Binary(I(9),&ref[42],I(0))
	move_ret ref[43]
	c_ret

;; TERM 42: '*FIRST*'(gen_application(application{item=> none, trans=> _B, item_comp=> _C, code=> _D, item_id=> none, trans_id=> 0, mode=> gen, restrict=> _E, out_env=> _F, key=> _G}))
c_code local build_ref_42
	ret_reg &ref[42]
	call_c   build_ref_40()
	call_c   build_ref_41()
	call_c   Dyam_Create_Unary(&ref[40],&ref[41])
	move_ret ref[42]
	c_ret

;; TERM 40: '*FIRST*'
c_code local build_ref_40
	ret_reg &ref[40]
	call_c   Dyam_Create_Atom("*FIRST*")
	move_ret ref[40]
	c_ret

c_code local build_seed_19
	ret_reg &seed[19]
	call_c   build_ref_16()
	call_c   build_ref_64()
	call_c   Dyam_Seed_Start(&ref[16],&ref[64],I(0),fun0,1)
	call_c   build_ref_65()
	call_c   Dyam_Seed_Add_Comp(&ref[65],fun21,0)
	call_c   Dyam_Seed_End()
	move_ret seed[19]
	c_ret

;; TERM 65: '*PROLOG-ITEM*'{top=> seed_code(_B, _C, _D, _E, _F), cont=> _A}
c_code local build_ref_65
	ret_reg &ref[65]
	call_c   build_ref_269()
	call_c   build_ref_62()
	call_c   Dyam_Create_Binary(&ref[269],&ref[62],V(0))
	move_ret ref[65]
	c_ret

;; TERM 62: seed_code(_B, _C, _D, _E, _F)
c_code local build_ref_62
	ret_reg &ref[62]
	call_c   build_ref_300()
	call_c   Dyam_Term_Start(&ref[300],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[62]
	c_ret

;; TERM 300: seed_code
c_code local build_ref_300
	ret_reg &ref[300]
	call_c   Dyam_Create_Atom("seed_code")
	move_ret ref[300]
	c_ret

c_code local build_seed_30
	ret_reg &seed[30]
	call_c   build_ref_26()
	call_c   build_ref_67()
	call_c   Dyam_Seed_Start(&ref[26],&ref[67],I(0),fun7,1)
	call_c   build_ref_70()
	call_c   Dyam_Seed_Add_Comp(&ref[70],&ref[67],0)
	call_c   Dyam_Seed_End()
	move_ret seed[30]
	c_ret

;; TERM 70: '*PROLOG-FIRST*'(install_app(_G, _B, _C, _D, _H, _F)) :> '$$HOLE$$'
c_code local build_ref_70
	ret_reg &ref[70]
	call_c   build_ref_69()
	call_c   Dyam_Create_Binary(I(9),&ref[69],I(7))
	move_ret ref[70]
	c_ret

;; TERM 69: '*PROLOG-FIRST*'(install_app(_G, _B, _C, _D, _H, _F))
c_code local build_ref_69
	ret_reg &ref[69]
	call_c   build_ref_17()
	call_c   build_ref_68()
	call_c   Dyam_Create_Unary(&ref[17],&ref[68])
	move_ret ref[69]
	c_ret

;; TERM 68: install_app(_G, _B, _C, _D, _H, _F)
c_code local build_ref_68
	ret_reg &ref[68]
	call_c   build_ref_301()
	call_c   Dyam_Term_Start(&ref[301],6)
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[68]
	c_ret

;; TERM 301: install_app
c_code local build_ref_301
	ret_reg &ref[301]
	call_c   Dyam_Create_Atom("install_app")
	move_ret ref[301]
	c_ret

;; TERM 67: '*PROLOG-ITEM*'{top=> install_app(_G, _B, _C, _D, _H, _F), cont=> '$CLOSURE'('$fun'(19, 0, 1082562388), '$TUPPLE'(35046582649424))}
c_code local build_ref_67
	ret_reg &ref[67]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,287309824)
	move_ret R(0)
	call_c   Dyam_Closure_Aux(fun19,R(0))
	move_ret R(0)
	call_c   build_ref_269()
	call_c   build_ref_68()
	call_c   Dyam_Create_Binary(&ref[269],&ref[68],R(0))
	move_ret ref[67]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

pl_code local fun19
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(7))
	call_c   Dyam_Reg_Load(2,V(4))
	pl_call  pred_label_code_2()
	call_c   Dyam_Deallocate()
	pl_jump  Follow_Cont(V(0))

long local pool_fun21[4]=[3,build_ref_65,build_ref_66,build_seed_30]

pl_code local fun21
	call_c   Dyam_Pool(pool_fun21)
	call_c   Dyam_Unify_Item(&ref[65])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(1))
	move     V(6), R(2)
	move     S(5), R(3)
	pl_call  pred_value_counter_2()
	call_c   DYAM_evpred_functor(V(3),&ref[66],V(6))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_jump  fun20(&seed[30],12)

;; TERM 64: '*PROLOG-FIRST*'(seed_code(_B, _C, _D, _E, _F)) :> []
c_code local build_ref_64
	ret_reg &ref[64]
	call_c   build_ref_63()
	call_c   Dyam_Create_Binary(I(9),&ref[63],I(0))
	move_ret ref[64]
	c_ret

;; TERM 63: '*PROLOG-FIRST*'(seed_code(_B, _C, _D, _E, _F))
c_code local build_ref_63
	ret_reg &ref[63]
	call_c   build_ref_17()
	call_c   build_ref_62()
	call_c   Dyam_Create_Unary(&ref[17],&ref[62])
	move_ret ref[63]
	c_ret

c_code local build_seed_3
	ret_reg &seed[3]
	call_c   build_ref_11()
	call_c   build_ref_73()
	call_c   Dyam_Seed_Start(&ref[11],&ref[73],I(0),fun0,1)
	call_c   build_ref_72()
	call_c   Dyam_Seed_Add_Comp(&ref[72],fun22,0)
	call_c   Dyam_Seed_End()
	move_ret seed[3]
	c_ret

;; TERM 72: '*GUARD*'(seed_model(seed{model=> ('*GUARD*'(_B) :> _C), id=> _D, anchor=> ('*GUARD*'(_B) :> []), subs_comp=> _E, code=> _F, go=> _G, tabule=> yes, schedule=> no, subsume=> no, model_ref=> _H, subs_comp_ref=> _I, c_type=> '*GUARD_CLAUSE*', c_type_ref=> _J, out_env=> _K, appinfo=> _L, tail_flag=> _M}))
c_code local build_ref_72
	ret_reg &ref[72]
	call_c   build_ref_12()
	call_c   build_ref_71()
	call_c   Dyam_Create_Unary(&ref[12],&ref[71])
	move_ret ref[72]
	c_ret

;; TERM 71: seed_model(seed{model=> ('*GUARD*'(_B) :> _C), id=> _D, anchor=> ('*GUARD*'(_B) :> []), subs_comp=> _E, code=> _F, go=> _G, tabule=> yes, schedule=> no, subsume=> no, model_ref=> _H, subs_comp_ref=> _I, c_type=> '*GUARD_CLAUSE*', c_type_ref=> _J, out_env=> _K, appinfo=> _L, tail_flag=> _M})
c_code local build_ref_71
	ret_reg &ref[71]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_12()
	call_c   Dyam_Create_Unary(&ref[12],V(1))
	move_ret R(0)
	call_c   Dyam_Create_Binary(I(9),R(0),V(2))
	move_ret R(1)
	call_c   Dyam_Create_Binary(I(9),R(0),I(0))
	move_ret R(0)
	call_c   build_ref_284()
	call_c   build_ref_23()
	call_c   build_ref_140()
	call_c   build_ref_11()
	call_c   Dyam_Term_Start(&ref[284],16)
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(&ref[23])
	call_c   Dyam_Term_Arg(&ref[140])
	call_c   Dyam_Term_Arg(&ref[140])
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(&ref[11])
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_283()
	call_c   Dyam_Create_Unary(&ref[283],R(0))
	move_ret ref[71]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

pl_code local fun22
	call_c   build_ref_72()
	call_c   Dyam_Unify_Item(&ref[72])
	fail_ret
	pl_ret

;; TERM 73: '*GUARD*'(seed_model(seed{model=> ('*GUARD*'(_B) :> _C), id=> _D, anchor=> ('*GUARD*'(_B) :> []), subs_comp=> _E, code=> _F, go=> _G, tabule=> yes, schedule=> no, subsume=> no, model_ref=> _H, subs_comp_ref=> _I, c_type=> '*GUARD_CLAUSE*', c_type_ref=> _J, out_env=> _K, appinfo=> _L, tail_flag=> _M})) :> []
c_code local build_ref_73
	ret_reg &ref[73]
	call_c   build_ref_72()
	call_c   Dyam_Create_Binary(I(9),&ref[72],I(0))
	move_ret ref[73]
	c_ret

c_code local build_seed_4
	ret_reg &seed[4]
	call_c   build_ref_11()
	call_c   build_ref_76()
	call_c   Dyam_Seed_Start(&ref[11],&ref[76],I(0),fun0,1)
	call_c   build_ref_75()
	call_c   Dyam_Seed_Add_Comp(&ref[75],fun23,0)
	call_c   Dyam_Seed_End()
	move_ret seed[4]
	c_ret

;; TERM 75: '*GUARD*'(seed_model(seed{model=> ('*PROLOG-FIRST*'(_B) :> _C), id=> _D, anchor=> ('*PROLOG-FIRST*'(_B) :> []), subs_comp=> _E, code=> _F, go=> _G, tabule=> yes, schedule=> no, subsume=> no, model_ref=> _H, subs_comp_ref=> _I, c_type=> '*PROLOG_FIRST*', c_type_ref=> _J, out_env=> _K, appinfo=> _L, tail_flag=> _M}))
c_code local build_ref_75
	ret_reg &ref[75]
	call_c   build_ref_12()
	call_c   build_ref_74()
	call_c   Dyam_Create_Unary(&ref[12],&ref[74])
	move_ret ref[75]
	c_ret

;; TERM 74: seed_model(seed{model=> ('*PROLOG-FIRST*'(_B) :> _C), id=> _D, anchor=> ('*PROLOG-FIRST*'(_B) :> []), subs_comp=> _E, code=> _F, go=> _G, tabule=> yes, schedule=> no, subsume=> no, model_ref=> _H, subs_comp_ref=> _I, c_type=> '*PROLOG_FIRST*', c_type_ref=> _J, out_env=> _K, appinfo=> _L, tail_flag=> _M})
c_code local build_ref_74
	ret_reg &ref[74]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_17()
	call_c   Dyam_Create_Unary(&ref[17],V(1))
	move_ret R(0)
	call_c   Dyam_Create_Binary(I(9),R(0),V(2))
	move_ret R(1)
	call_c   Dyam_Create_Binary(I(9),R(0),I(0))
	move_ret R(0)
	call_c   build_ref_284()
	call_c   build_ref_23()
	call_c   build_ref_140()
	call_c   build_ref_16()
	call_c   Dyam_Term_Start(&ref[284],16)
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(&ref[23])
	call_c   Dyam_Term_Arg(&ref[140])
	call_c   Dyam_Term_Arg(&ref[140])
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(&ref[16])
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_283()
	call_c   Dyam_Create_Unary(&ref[283],R(0))
	move_ret ref[74]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

pl_code local fun23
	call_c   build_ref_75()
	call_c   Dyam_Unify_Item(&ref[75])
	fail_ret
	pl_ret

;; TERM 76: '*GUARD*'(seed_model(seed{model=> ('*PROLOG-FIRST*'(_B) :> _C), id=> _D, anchor=> ('*PROLOG-FIRST*'(_B) :> []), subs_comp=> _E, code=> _F, go=> _G, tabule=> yes, schedule=> no, subsume=> no, model_ref=> _H, subs_comp_ref=> _I, c_type=> '*PROLOG_FIRST*', c_type_ref=> _J, out_env=> _K, appinfo=> _L, tail_flag=> _M})) :> []
c_code local build_ref_76
	ret_reg &ref[76]
	call_c   build_ref_75()
	call_c   Dyam_Create_Binary(I(9),&ref[75],I(0))
	move_ret ref[76]
	c_ret

c_code local build_seed_5
	ret_reg &seed[5]
	call_c   build_ref_11()
	call_c   build_ref_79()
	call_c   Dyam_Seed_Start(&ref[11],&ref[79],I(0),fun0,1)
	call_c   build_ref_78()
	call_c   Dyam_Seed_Add_Comp(&ref[78],fun24,0)
	call_c   Dyam_Seed_End()
	move_ret seed[5]
	c_ret

;; TERM 78: '*GUARD*'(seed_model(seed{model=> ('*FIRST*'(_B) :> _C), id=> _D, anchor=> ('*FIRST*'(_B) :> []), subs_comp=> _E, code=> _F, go=> _G, tabule=> yes, schedule=> no, subsume=> no, model_ref=> _H, subs_comp_ref=> _I, c_type=> '*FIRST*', c_type_ref=> _J, out_env=> _K, appinfo=> _L, tail_flag=> _M}))
c_code local build_ref_78
	ret_reg &ref[78]
	call_c   build_ref_12()
	call_c   build_ref_77()
	call_c   Dyam_Create_Unary(&ref[12],&ref[77])
	move_ret ref[78]
	c_ret

;; TERM 77: seed_model(seed{model=> ('*FIRST*'(_B) :> _C), id=> _D, anchor=> ('*FIRST*'(_B) :> []), subs_comp=> _E, code=> _F, go=> _G, tabule=> yes, schedule=> no, subsume=> no, model_ref=> _H, subs_comp_ref=> _I, c_type=> '*FIRST*', c_type_ref=> _J, out_env=> _K, appinfo=> _L, tail_flag=> _M})
c_code local build_ref_77
	ret_reg &ref[77]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_40()
	call_c   Dyam_Create_Unary(&ref[40],V(1))
	move_ret R(0)
	call_c   Dyam_Create_Binary(I(9),R(0),V(2))
	move_ret R(1)
	call_c   Dyam_Create_Binary(I(9),R(0),I(0))
	move_ret R(0)
	call_c   build_ref_284()
	call_c   build_ref_23()
	call_c   build_ref_140()
	call_c   Dyam_Term_Start(&ref[284],16)
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(&ref[23])
	call_c   Dyam_Term_Arg(&ref[140])
	call_c   Dyam_Term_Arg(&ref[140])
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(&ref[40])
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_283()
	call_c   Dyam_Create_Unary(&ref[283],R(0))
	move_ret ref[77]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

pl_code local fun24
	call_c   build_ref_78()
	call_c   Dyam_Unify_Item(&ref[78])
	fail_ret
	pl_ret

;; TERM 79: '*GUARD*'(seed_model(seed{model=> ('*FIRST*'(_B) :> _C), id=> _D, anchor=> ('*FIRST*'(_B) :> []), subs_comp=> _E, code=> _F, go=> _G, tabule=> yes, schedule=> no, subsume=> no, model_ref=> _H, subs_comp_ref=> _I, c_type=> '*FIRST*', c_type_ref=> _J, out_env=> _K, appinfo=> _L, tail_flag=> _M})) :> []
c_code local build_ref_79
	ret_reg &ref[79]
	call_c   build_ref_78()
	call_c   Dyam_Create_Binary(I(9),&ref[78],I(0))
	move_ret ref[79]
	c_ret

c_code local build_seed_15
	ret_reg &seed[15]
	call_c   build_ref_16()
	call_c   build_ref_87()
	call_c   Dyam_Seed_Start(&ref[16],&ref[87],I(0),fun0,1)
	call_c   build_ref_88()
	call_c   Dyam_Seed_Add_Comp(&ref[88],fun33,0)
	call_c   Dyam_Seed_End()
	move_ret seed[15]
	c_ret

;; TERM 88: '*PROLOG-ITEM*'{top=> component(_B, _C, completion(_C, _D, _E, _F, _G, _H), _I), cont=> _A}
c_code local build_ref_88
	ret_reg &ref[88]
	call_c   build_ref_269()
	call_c   build_ref_85()
	call_c   Dyam_Create_Binary(&ref[269],&ref[85],V(0))
	move_ret ref[88]
	c_ret

;; TERM 85: component(_B, _C, completion(_C, _D, _E, _F, _G, _H), _I)
c_code local build_ref_85
	ret_reg &ref[85]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_302()
	call_c   Dyam_Term_Start(&ref[302],6)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_270()
	call_c   Dyam_Term_Start(&ref[270],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_End()
	move_ret ref[85]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 302: completion
c_code local build_ref_302
	ret_reg &ref[302]
	call_c   Dyam_Create_Atom("completion")
	move_ret ref[302]
	c_ret

;; TERM 89: gen_application(application{item=> _B, trans=> _J, item_comp=> _K, code=> _L, item_id=> _C, trans_id=> _E, mode=> _M, restrict=> _N, out_env=> _O, key=> _P})
c_code local build_ref_89
	ret_reg &ref[89]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_271()
	call_c   Dyam_Term_Start(&ref[271],10)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_259()
	call_c   Dyam_Create_Unary(&ref[259],R(0))
	move_ret ref[89]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_31
	ret_reg &seed[31]
	call_c   build_ref_26()
	call_c   build_ref_90()
	call_c   Dyam_Seed_Start(&ref[26],&ref[90],I(0),fun7,1)
	call_c   build_ref_93()
	call_c   Dyam_Seed_Add_Comp(&ref[93],&ref[90],0)
	call_c   Dyam_Seed_End()
	move_ret seed[31]
	c_ret

;; TERM 93: '*PROLOG-FIRST*'(app_model(application{item=> _B, trans=> _J, item_comp=> _Q, code=> _R, item_id=> _C, trans_id=> _E, mode=> item, restrict=> _S, out_env=> _T, key=> _P})) :> '$$HOLE$$'
c_code local build_ref_93
	ret_reg &ref[93]
	call_c   build_ref_92()
	call_c   Dyam_Create_Binary(I(9),&ref[92],I(7))
	move_ret ref[93]
	c_ret

;; TERM 92: '*PROLOG-FIRST*'(app_model(application{item=> _B, trans=> _J, item_comp=> _Q, code=> _R, item_id=> _C, trans_id=> _E, mode=> item, restrict=> _S, out_env=> _T, key=> _P}))
c_code local build_ref_92
	ret_reg &ref[92]
	call_c   build_ref_17()
	call_c   build_ref_91()
	call_c   Dyam_Create_Unary(&ref[17],&ref[91])
	move_ret ref[92]
	c_ret

;; TERM 91: app_model(application{item=> _B, trans=> _J, item_comp=> _Q, code=> _R, item_id=> _C, trans_id=> _E, mode=> item, restrict=> _S, out_env=> _T, key=> _P})
c_code local build_ref_91
	ret_reg &ref[91]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_271()
	call_c   build_ref_273()
	call_c   Dyam_Term_Start(&ref[271],10)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(&ref[273])
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_281()
	call_c   Dyam_Create_Unary(&ref[281],R(0))
	move_ret ref[91]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 90: '*PROLOG-ITEM*'{top=> app_model(application{item=> _B, trans=> _J, item_comp=> _Q, code=> _R, item_id=> _C, trans_id=> _E, mode=> item, restrict=> _S, out_env=> _T, key=> _P}), cont=> '$CLOSURE'('$fun'(31, 0, 1082630744), '$TUPPLE'(35046582649756))}
c_code local build_ref_90
	ret_reg &ref[90]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,310906880)
	move_ret R(0)
	call_c   Dyam_Closure_Aux(fun31,R(0))
	move_ret R(0)
	call_c   build_ref_269()
	call_c   build_ref_91()
	call_c   Dyam_Create_Binary(&ref[269],&ref[91],R(0))
	move_ret ref[90]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

pl_code local fun31
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(16))
	call_c   Dyam_Reg_Load(2,V(5))
	pl_call  pred_label_term_2()
	call_c   Dyam_Reg_Load(0,V(9))
	call_c   Dyam_Reg_Load(2,V(3))
	pl_call  pred_label_term_2()
	call_c   Dyam_Deallocate()
	pl_jump  Follow_Cont(V(0))

long local pool_fun32[2]=[1,build_seed_31]

long local pool_fun33[4]=[65538,build_ref_88,build_ref_89,pool_fun32]

pl_code local fun33
	call_c   Dyam_Pool(pool_fun33)
	call_c   Dyam_Unify_Item(&ref[88])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun29)
	call_c   Dyam_Set_Cut()
	pl_call  Callret_2(V(20),&ref[89])
	call_c   Dyam_Cut()
fun32:
	pl_call  Object_1(V(20))
	call_c   Dyam_Reg_Load(0,V(15))
	call_c   Dyam_Reg_Load(2,V(8))
	call_c   Dyam_Reg_Load(4,V(6))
	call_c   Dyam_Reg_Load(6,V(7))
	pl_call  pred_handle_appinfo_4()
	call_c   Dyam_Deallocate()
	pl_jump  fun20(&seed[31],12)


;; TERM 87: '*PROLOG-FIRST*'(component(_B, _C, completion(_C, _D, _E, _F, _G, _H), _I)) :> []
c_code local build_ref_87
	ret_reg &ref[87]
	call_c   build_ref_86()
	call_c   Dyam_Create_Binary(I(9),&ref[86],I(0))
	move_ret ref[87]
	c_ret

;; TERM 86: '*PROLOG-FIRST*'(component(_B, _C, completion(_C, _D, _E, _F, _G, _H), _I))
c_code local build_ref_86
	ret_reg &ref[86]
	call_c   build_ref_17()
	call_c   build_ref_85()
	call_c   Dyam_Create_Unary(&ref[17],&ref[85])
	move_ret ref[86]
	c_ret

c_code local build_seed_12
	ret_reg &seed[12]
	call_c   build_ref_11()
	call_c   build_ref_96()
	call_c   Dyam_Seed_Start(&ref[11],&ref[96],I(0),fun0,1)
	call_c   build_ref_95()
	call_c   Dyam_Seed_Add_Comp(&ref[95],fun36,0)
	call_c   Dyam_Seed_End()
	move_ret seed[12]
	c_ret

;; TERM 95: '*GUARD*'(seed_model(seed{model=> '*CITEM*'(_B, _C), id=> _D, anchor=> _E, subs_comp=> _F, code=> _G, go=> _H, tabule=> yes, schedule=> _I, subsume=> yes, model_ref=> _J, subs_comp_ref=> _K, c_type=> '*CITEM*', c_type_ref=> _L, out_env=> _M, appinfo=> _N, tail_flag=> _O}))
c_code local build_ref_95
	ret_reg &ref[95]
	call_c   build_ref_12()
	call_c   build_ref_94()
	call_c   Dyam_Create_Unary(&ref[12],&ref[94])
	move_ret ref[95]
	c_ret

;; TERM 94: seed_model(seed{model=> '*CITEM*'(_B, _C), id=> _D, anchor=> _E, subs_comp=> _F, code=> _G, go=> _H, tabule=> yes, schedule=> _I, subsume=> yes, model_ref=> _J, subs_comp_ref=> _K, c_type=> '*CITEM*', c_type_ref=> _L, out_env=> _M, appinfo=> _N, tail_flag=> _O})
c_code local build_ref_94
	ret_reg &ref[94]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_44()
	call_c   Dyam_Create_Binary(&ref[44],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_284()
	call_c   build_ref_23()
	call_c   Dyam_Term_Start(&ref[284],16)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(&ref[23])
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(&ref[23])
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(&ref[44])
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_283()
	call_c   Dyam_Create_Unary(&ref[283],R(0))
	move_ret ref[94]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

pl_code local fun35
	call_c   Dyam_Remove_Choice()
fun34:
	call_c   Dyam_Deallocate()
	pl_ret


long local pool_fun36[3]=[2,build_ref_95,build_ref_97]

pl_code local fun36
	call_c   Dyam_Pool(pool_fun36)
	call_c   Dyam_Unify_Item(&ref[95])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun35)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(8),&ref[97])
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun34()

;; TERM 96: '*GUARD*'(seed_model(seed{model=> '*CITEM*'(_B, _C), id=> _D, anchor=> _E, subs_comp=> _F, code=> _G, go=> _H, tabule=> yes, schedule=> _I, subsume=> yes, model_ref=> _J, subs_comp_ref=> _K, c_type=> '*CITEM*', c_type_ref=> _L, out_env=> _M, appinfo=> _N, tail_flag=> _O})) :> []
c_code local build_ref_96
	ret_reg &ref[96]
	call_c   build_ref_95()
	call_c   Dyam_Create_Binary(I(9),&ref[95],I(0))
	move_ret ref[96]
	c_ret

c_code local build_seed_7
	ret_reg &seed[7]
	call_c   build_ref_11()
	call_c   build_ref_100()
	call_c   Dyam_Seed_Start(&ref[11],&ref[100],I(0),fun0,1)
	call_c   build_ref_99()
	call_c   Dyam_Seed_Add_Comp(&ref[99],fun41,0)
	call_c   Dyam_Seed_End()
	move_ret seed[7]
	c_ret

;; TERM 99: '*GUARD*'(seed_model(seed{model=> ('*CONT*' :> _B), id=> _C, anchor=> _D, subs_comp=> _E, code=> _F, go=> _G, tabule=> _H, schedule=> _I, subsume=> _J, model_ref=> _K, subs_comp_ref=> _L, c_type=> '*CONT*', c_type_ref=> _M, out_env=> _N, appinfo=> _O, tail_flag=> _P}))
c_code local build_ref_99
	ret_reg &ref[99]
	call_c   build_ref_12()
	call_c   build_ref_98()
	call_c   Dyam_Create_Unary(&ref[12],&ref[98])
	move_ret ref[99]
	c_ret

;; TERM 98: seed_model(seed{model=> ('*CONT*' :> _B), id=> _C, anchor=> _D, subs_comp=> _E, code=> _F, go=> _G, tabule=> _H, schedule=> _I, subsume=> _J, model_ref=> _K, subs_comp_ref=> _L, c_type=> '*CONT*', c_type_ref=> _M, out_env=> _N, appinfo=> _O, tail_flag=> _P})
c_code local build_ref_98
	ret_reg &ref[98]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_303()
	call_c   Dyam_Create_Binary(I(9),&ref[303],V(1))
	move_ret R(0)
	call_c   build_ref_284()
	call_c   Dyam_Term_Start(&ref[284],16)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(&ref[303])
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_283()
	call_c   Dyam_Create_Unary(&ref[283],R(0))
	move_ret ref[98]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 303: '*CONT*'
c_code local build_ref_303
	ret_reg &ref[303]
	call_c   Dyam_Create_Atom("*CONT*")
	move_ret ref[303]
	c_ret

long local pool_fun37[2]=[1,build_ref_101]

pl_code local fun38
	call_c   Dyam_Remove_Choice()
fun37:
	call_c   Dyam_Choice(fun35)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(8),&ref[101])
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun34()


long local pool_fun39[4]=[131073,build_ref_23,pool_fun37,pool_fun37]

pl_code local fun40
	call_c   Dyam_Remove_Choice()
fun39:
	call_c   Dyam_Choice(fun38)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(9),&ref[23])
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun37()


long local pool_fun41[5]=[131074,build_ref_99,build_ref_23,pool_fun39,pool_fun39]

pl_code local fun41
	call_c   Dyam_Pool(pool_fun41)
	call_c   Dyam_Unify_Item(&ref[99])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun40)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(7),&ref[23])
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun39()

;; TERM 100: '*GUARD*'(seed_model(seed{model=> ('*CONT*' :> _B), id=> _C, anchor=> _D, subs_comp=> _E, code=> _F, go=> _G, tabule=> _H, schedule=> _I, subsume=> _J, model_ref=> _K, subs_comp_ref=> _L, c_type=> '*CONT*', c_type_ref=> _M, out_env=> _N, appinfo=> _O, tail_flag=> _P})) :> []
c_code local build_ref_100
	ret_reg &ref[100]
	call_c   build_ref_99()
	call_c   Dyam_Create_Binary(I(9),&ref[99],I(0))
	move_ret ref[100]
	c_ret

c_code local build_seed_11
	ret_reg &seed[11]
	call_c   build_ref_11()
	call_c   build_ref_104()
	call_c   Dyam_Seed_Start(&ref[11],&ref[104],I(0),fun0,1)
	call_c   build_ref_103()
	call_c   Dyam_Seed_Add_Comp(&ref[103],fun44,0)
	call_c   Dyam_Seed_End()
	move_ret seed[11]
	c_ret

;; TERM 103: '*GUARD*'(seed_model(seed{model=> '*RITEM*'(_B, _C), id=> _D, anchor=> _E, subs_comp=> _F, code=> _G, go=> _H, tabule=> yes, schedule=> _I, subsume=> _J, model_ref=> _K, subs_comp_ref=> _L, c_type=> '*RITEM*', c_type_ref=> _M, out_env=> _N, appinfo=> _O, tail_flag=> _P}))
c_code local build_ref_103
	ret_reg &ref[103]
	call_c   build_ref_12()
	call_c   build_ref_102()
	call_c   Dyam_Create_Unary(&ref[12],&ref[102])
	move_ret ref[103]
	c_ret

;; TERM 102: seed_model(seed{model=> '*RITEM*'(_B, _C), id=> _D, anchor=> _E, subs_comp=> _F, code=> _G, go=> _H, tabule=> yes, schedule=> _I, subsume=> _J, model_ref=> _K, subs_comp_ref=> _L, c_type=> '*RITEM*', c_type_ref=> _M, out_env=> _N, appinfo=> _O, tail_flag=> _P})
c_code local build_ref_102
	ret_reg &ref[102]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_0()
	call_c   Dyam_Create_Binary(&ref[0],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_284()
	call_c   build_ref_23()
	call_c   Dyam_Term_Start(&ref[284],16)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(&ref[23])
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(&ref[0])
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_283()
	call_c   Dyam_Create_Unary(&ref[283],R(0))
	move_ret ref[102]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun42[2]=[1,build_ref_23]

pl_code local fun43
	call_c   Dyam_Remove_Choice()
fun42:
	call_c   Dyam_Choice(fun35)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(9),&ref[23])
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun34()


long local pool_fun44[5]=[131074,build_ref_103,build_ref_97,pool_fun42,pool_fun42]

pl_code local fun44
	call_c   Dyam_Pool(pool_fun44)
	call_c   Dyam_Unify_Item(&ref[103])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun43)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(8),&ref[97])
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun42()

;; TERM 104: '*GUARD*'(seed_model(seed{model=> '*RITEM*'(_B, _C), id=> _D, anchor=> _E, subs_comp=> _F, code=> _G, go=> _H, tabule=> yes, schedule=> _I, subsume=> _J, model_ref=> _K, subs_comp_ref=> _L, c_type=> '*RITEM*', c_type_ref=> _M, out_env=> _N, appinfo=> _O, tail_flag=> _P})) :> []
c_code local build_ref_104
	ret_reg &ref[104]
	call_c   build_ref_103()
	call_c   Dyam_Create_Binary(I(9),&ref[103],I(0))
	move_ret ref[104]
	c_ret

c_code local build_seed_21
	ret_reg &seed[21]
	call_c   build_ref_11()
	call_c   build_ref_107()
	call_c   Dyam_Seed_Start(&ref[11],&ref[107],I(0),fun0,1)
	call_c   build_ref_106()
	call_c   Dyam_Seed_Add_Comp(&ref[106],fun48,0)
	call_c   Dyam_Seed_End()
	move_ret seed[21]
	c_ret

;; TERM 106: '*GUARD*'(anchor(_B, _C, _D, _E))
c_code local build_ref_106
	ret_reg &ref[106]
	call_c   build_ref_12()
	call_c   build_ref_105()
	call_c   Dyam_Create_Unary(&ref[12],&ref[105])
	move_ret ref[106]
	c_ret

;; TERM 105: anchor(_B, _C, _D, _E)
c_code local build_ref_105
	ret_reg &ref[105]
	call_c   build_ref_287()
	call_c   Dyam_Term_Start(&ref[287],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[105]
	c_ret

pl_code local fun47
	call_c   Dyam_Remove_Choice()
fun45:
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(3))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_label_term_2()


pl_code local fun46
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(1),V(2))
	fail_ret
	pl_jump  fun45()

;; TERM 108: _F :> _G
c_code local build_ref_108
	ret_reg &ref[108]
	call_c   Dyam_Create_Binary(I(9),V(5),V(6))
	move_ret ref[108]
	c_ret

;; TERM 109: _F :> _H
c_code local build_ref_109
	ret_reg &ref[109]
	call_c   Dyam_Create_Binary(I(9),V(5),V(7))
	move_ret ref[109]
	c_ret

long local pool_fun48[4]=[3,build_ref_106,build_ref_108,build_ref_109]

pl_code local fun48
	call_c   Dyam_Pool(pool_fun48)
	call_c   Dyam_Unify_Item(&ref[106])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun47)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_variable(V(1))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun46)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(2),&ref[108])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(6))
	call_c   Dyam_Reg_Load(2,V(5))
	move     V(7), R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Load(6,V(4))
	pl_call  pred_new_gen_tail_4()
	call_c   Dyam_Unify(V(1),&ref[109])
	fail_ret
	pl_jump  fun45()

;; TERM 107: '*GUARD*'(anchor(_B, _C, _D, _E)) :> []
c_code local build_ref_107
	ret_reg &ref[107]
	call_c   build_ref_106()
	call_c   Dyam_Create_Binary(I(9),&ref[106],I(0))
	move_ret ref[107]
	c_ret

c_code local build_seed_9
	ret_reg &seed[9]
	call_c   build_ref_11()
	call_c   build_ref_112()
	call_c   Dyam_Seed_Start(&ref[11],&ref[112],I(0),fun0,1)
	call_c   build_ref_111()
	call_c   Dyam_Seed_Add_Comp(&ref[111],fun51,0)
	call_c   Dyam_Seed_End()
	move_ret seed[9]
	c_ret

;; TERM 111: '*GUARD*'(seed_model(seed{model=> ('*LCFIRST*'(_B) :> _C), id=> _D, anchor=> _E, subs_comp=> _F, code=> _G, go=> _H, tabule=> _I, schedule=> _J, subsume=> no, model_ref=> _K, subs_comp_ref=> _L, c_type=> '*LCFIRST*', c_type_ref=> _M, out_env=> _N, appinfo=> _O, tail_flag=> _P}))
c_code local build_ref_111
	ret_reg &ref[111]
	call_c   build_ref_12()
	call_c   build_ref_110()
	call_c   Dyam_Create_Unary(&ref[12],&ref[110])
	move_ret ref[111]
	c_ret

;; TERM 110: seed_model(seed{model=> ('*LCFIRST*'(_B) :> _C), id=> _D, anchor=> _E, subs_comp=> _F, code=> _G, go=> _H, tabule=> _I, schedule=> _J, subsume=> no, model_ref=> _K, subs_comp_ref=> _L, c_type=> '*LCFIRST*', c_type_ref=> _M, out_env=> _N, appinfo=> _O, tail_flag=> _P})
c_code local build_ref_110
	ret_reg &ref[110]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_304()
	call_c   Dyam_Create_Unary(&ref[304],V(1))
	move_ret R(0)
	call_c   Dyam_Create_Binary(I(9),R(0),V(2))
	move_ret R(0)
	call_c   build_ref_284()
	call_c   build_ref_140()
	call_c   Dyam_Term_Start(&ref[284],16)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(&ref[140])
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(&ref[304])
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_283()
	call_c   Dyam_Create_Unary(&ref[283],R(0))
	move_ret ref[110]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 304: '*LCFIRST*'
c_code local build_ref_304
	ret_reg &ref[304]
	call_c   Dyam_Create_Atom("*LCFIRST*")
	move_ret ref[304]
	c_ret

long local pool_fun49[2]=[1,build_ref_113]

pl_code local fun50
	call_c   Dyam_Remove_Choice()
fun49:
	call_c   Dyam_Choice(fun35)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(9),&ref[113])
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun34()


long local pool_fun51[5]=[131074,build_ref_111,build_ref_23,pool_fun49,pool_fun49]

pl_code local fun51
	call_c   Dyam_Pool(pool_fun51)
	call_c   Dyam_Unify_Item(&ref[111])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun50)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(8),&ref[23])
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun49()

;; TERM 112: '*GUARD*'(seed_model(seed{model=> ('*LCFIRST*'(_B) :> _C), id=> _D, anchor=> _E, subs_comp=> _F, code=> _G, go=> _H, tabule=> _I, schedule=> _J, subsume=> no, model_ref=> _K, subs_comp_ref=> _L, c_type=> '*LCFIRST*', c_type_ref=> _M, out_env=> _N, appinfo=> _O, tail_flag=> _P})) :> []
c_code local build_ref_112
	ret_reg &ref[112]
	call_c   build_ref_111()
	call_c   Dyam_Create_Binary(I(9),&ref[111],I(0))
	move_ret ref[112]
	c_ret

c_code local build_seed_14
	ret_reg &seed[14]
	call_c   build_ref_40()
	call_c   build_ref_116()
	call_c   Dyam_Seed_Start(&ref[40],&ref[116],I(0),fun14,1)
	call_c   build_ref_117()
	call_c   Dyam_Seed_Add_Comp(&ref[117],fun53,0)
	call_c   Dyam_Seed_End()
	move_ret seed[14]
	c_ret

;; TERM 117: '*CITEM*'(gen_application(application{item=> _B, trans=> _C, item_comp=> _D, code=> _E, item_id=> _F, trans_id=> _G, mode=> gen, restrict=> _H, out_env=> _I, key=> _J}), _A)
c_code local build_ref_117
	ret_reg &ref[117]
	call_c   build_ref_44()
	call_c   build_ref_114()
	call_c   Dyam_Create_Binary(&ref[44],&ref[114],V(0))
	move_ret ref[117]
	c_ret

;; TERM 114: gen_application(application{item=> _B, trans=> _C, item_comp=> _D, code=> _E, item_id=> _F, trans_id=> _G, mode=> gen, restrict=> _H, out_env=> _I, key=> _J})
c_code local build_ref_114
	ret_reg &ref[114]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_271()
	call_c   build_ref_298()
	call_c   Dyam_Term_Start(&ref[271],10)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(&ref[298])
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_259()
	call_c   Dyam_Create_Unary(&ref[259],R(0))
	move_ret ref[114]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_32
	ret_reg &seed[32]
	call_c   build_ref_26()
	call_c   build_ref_118()
	call_c   Dyam_Seed_Start(&ref[26],&ref[118],I(0),fun7,1)
	call_c   build_ref_121()
	call_c   Dyam_Seed_Add_Comp(&ref[121],&ref[118],0)
	call_c   Dyam_Seed_End()
	move_ret seed[32]
	c_ret

;; TERM 121: '*PROLOG-FIRST*'(app_model(application{item=> _B, trans=> _C, item_comp=> _D, code=> _E, item_id=> _F, trans_id=> _G, mode=> gen, restrict=> _H, out_env=> _I, key=> _J})) :> '$$HOLE$$'
c_code local build_ref_121
	ret_reg &ref[121]
	call_c   build_ref_120()
	call_c   Dyam_Create_Binary(I(9),&ref[120],I(7))
	move_ret ref[121]
	c_ret

;; TERM 120: '*PROLOG-FIRST*'(app_model(application{item=> _B, trans=> _C, item_comp=> _D, code=> _E, item_id=> _F, trans_id=> _G, mode=> gen, restrict=> _H, out_env=> _I, key=> _J}))
c_code local build_ref_120
	ret_reg &ref[120]
	call_c   build_ref_17()
	call_c   build_ref_119()
	call_c   Dyam_Create_Unary(&ref[17],&ref[119])
	move_ret ref[120]
	c_ret

;; TERM 119: app_model(application{item=> _B, trans=> _C, item_comp=> _D, code=> _E, item_id=> _F, trans_id=> _G, mode=> gen, restrict=> _H, out_env=> _I, key=> _J})
c_code local build_ref_119
	ret_reg &ref[119]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_271()
	call_c   build_ref_298()
	call_c   Dyam_Term_Start(&ref[271],10)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(&ref[298])
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_281()
	call_c   Dyam_Create_Unary(&ref[281],R(0))
	move_ret ref[119]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 118: '*PROLOG-ITEM*'{top=> app_model(application{item=> _B, trans=> _C, item_comp=> _D, code=> _E, item_id=> _F, trans_id=> _G, mode=> gen, restrict=> _H, out_env=> _I, key=> _J}), cont=> '$CLOSURE'('$fun'(52, 0, 1082789272), '$TUPPLE'(35046582650220))}
c_code local build_ref_118
	ret_reg &ref[118]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,482344960)
	move_ret R(0)
	call_c   Dyam_Closure_Aux(fun52,R(0))
	move_ret R(0)
	call_c   build_ref_269()
	call_c   build_ref_119()
	call_c   Dyam_Create_Binary(&ref[269],&ref[119],R(0))
	move_ret ref[118]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun52[2]=[1,build_seed_29]

pl_code local fun52
	call_c   Dyam_Pool(pool_fun52)
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(5))
	pl_call  pred_update_counter_2()
	call_c   Dyam_Reg_Load(0,V(2))
	call_c   Dyam_Reg_Load(2,V(6))
	pl_call  pred_update_counter_2()
	call_c   Dyam_Deallocate()
	pl_jump  fun12(&seed[29],2)

long local pool_fun53[3]=[2,build_ref_117,build_seed_32]

pl_code local fun53
	call_c   Dyam_Pool(pool_fun53)
	call_c   Dyam_Unify_Item(&ref[117])
	fail_ret
	pl_jump  fun20(&seed[32],12)

;; TERM 116: '*FIRST*'(gen_application(application{item=> _B, trans=> _C, item_comp=> _D, code=> _E, item_id=> _F, trans_id=> _G, mode=> gen, restrict=> _H, out_env=> _I, key=> _J})) :> []
c_code local build_ref_116
	ret_reg &ref[116]
	call_c   build_ref_115()
	call_c   Dyam_Create_Binary(I(9),&ref[115],I(0))
	move_ret ref[116]
	c_ret

;; TERM 115: '*FIRST*'(gen_application(application{item=> _B, trans=> _C, item_comp=> _D, code=> _E, item_id=> _F, trans_id=> _G, mode=> gen, restrict=> _H, out_env=> _I, key=> _J}))
c_code local build_ref_115
	ret_reg &ref[115]
	call_c   build_ref_40()
	call_c   build_ref_114()
	call_c   Dyam_Create_Unary(&ref[40],&ref[114])
	move_ret ref[115]
	c_ret

c_code local build_seed_18
	ret_reg &seed[18]
	call_c   build_ref_16()
	call_c   build_ref_124()
	call_c   Dyam_Seed_Start(&ref[16],&ref[124],I(0),fun0,1)
	call_c   build_ref_125()
	call_c   Dyam_Seed_Add_Comp(&ref[125],fun57,0)
	call_c   Dyam_Seed_End()
	move_ret seed[18]
	c_ret

;; TERM 125: '*PROLOG-ITEM*'{top=> install_app(0, _B, _C, _D, _E, _F), cont=> _A}
c_code local build_ref_125
	ret_reg &ref[125]
	call_c   build_ref_269()
	call_c   build_ref_122()
	call_c   Dyam_Create_Binary(&ref[269],&ref[122],V(0))
	move_ret ref[125]
	c_ret

;; TERM 122: install_app(0, _B, _C, _D, _E, _F)
c_code local build_ref_122
	ret_reg &ref[122]
	call_c   build_ref_301()
	call_c   Dyam_Term_Start(&ref[301],6)
	call_c   Dyam_Term_Arg(N(0))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[122]
	c_ret

pl_code local fun56
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(4),&ref[128])
	fail_ret
	call_c   Dyam_Unify(V(5),I(0))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_jump  Follow_Cont(V(0))

;; TERM 135: '$CLOSURE'('$fun'(55, 0, 1082815908), '$TUPPLE'(35046582650284))
c_code local build_ref_135
	ret_reg &ref[135]
	call_c   build_ref_134()
	call_c   Dyam_Closure_Aux(fun55,&ref[134])
	move_ret ref[135]
	c_ret

;; TERM 131: allocate :> _I :> deallocate :> succeed
c_code local build_ref_131
	ret_reg &ref[131]
	call_c   build_ref_126()
	call_c   build_ref_130()
	call_c   Dyam_Create_Binary(I(9),&ref[126],&ref[130])
	move_ret ref[131]
	c_ret

;; TERM 130: _I :> deallocate :> succeed
c_code local build_ref_130
	ret_reg &ref[130]
	call_c   build_ref_129()
	call_c   Dyam_Create_Binary(I(9),V(8),&ref[129])
	move_ret ref[130]
	c_ret

;; TERM 129: deallocate :> succeed
c_code local build_ref_129
	ret_reg &ref[129]
	call_c   build_ref_127()
	call_c   build_ref_128()
	call_c   Dyam_Create_Binary(I(9),&ref[127],&ref[128])
	move_ret ref[129]
	c_ret

;; TERM 127: deallocate
c_code local build_ref_127
	ret_reg &ref[127]
	call_c   Dyam_Create_Atom("deallocate")
	move_ret ref[127]
	c_ret

;; TERM 126: allocate
c_code local build_ref_126
	ret_reg &ref[126]
	call_c   Dyam_Create_Atom("allocate")
	move_ret ref[126]
	c_ret

;; TERM 132: [_F]
c_code local build_ref_132
	ret_reg &ref[132]
	call_c   Dyam_Create_List(V(5),I(0))
	move_ret ref[132]
	c_ret

long local pool_fun55[3]=[2,build_ref_131,build_ref_132]

pl_code local fun55
	call_c   Dyam_Pool(pool_fun55)
	call_c   Dyam_Unify(V(4),&ref[131])
	fail_ret
	call_c   Dyam_Unify(V(5),&ref[132])
	fail_ret
	pl_jump  Follow_Cont(V(0))

;; TERM 134: '$TUPPLE'(35046582650284)
c_code local build_ref_134
	ret_reg &ref[134]
	call_c   Dyam_Create_Simple_Tupple(0,294649856)
	move_ret ref[134]
	c_ret

;; TERM 136: application{item=> _G, trans=> _B, item_comp=> _H, code=> _I, item_id=> _J, trans_id=> 0, mode=> trans, restrict=> _K, out_env=> _L, key=> _M}
c_code local build_ref_136
	ret_reg &ref[136]
	call_c   build_ref_271()
	call_c   build_ref_274()
	call_c   Dyam_Term_Start(&ref[271],10)
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(N(0))
	call_c   Dyam_Term_Arg(&ref[274])
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_End()
	move_ret ref[136]
	c_ret

long local pool_fun57[6]=[5,build_ref_125,build_ref_128,build_ref_54,build_ref_135,build_ref_136]

pl_code local fun57
	call_c   Dyam_Pool(pool_fun57)
	call_c   Dyam_Unify_Item(&ref[125])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun56)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[54])
	call_c   Dyam_Cut()
	move     &ref[135], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	move     &ref[136], R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  fun16()

;; TERM 124: '*PROLOG-FIRST*'(install_app(0, _B, _C, _D, _E, _F)) :> []
c_code local build_ref_124
	ret_reg &ref[124]
	call_c   build_ref_123()
	call_c   Dyam_Create_Binary(I(9),&ref[123],I(0))
	move_ret ref[124]
	c_ret

;; TERM 123: '*PROLOG-FIRST*'(install_app(0, _B, _C, _D, _E, _F))
c_code local build_ref_123
	ret_reg &ref[123]
	call_c   build_ref_17()
	call_c   build_ref_122()
	call_c   Dyam_Create_Unary(&ref[17],&ref[122])
	move_ret ref[123]
	c_ret

c_code local build_seed_10
	ret_reg &seed[10]
	call_c   build_ref_11()
	call_c   build_ref_139()
	call_c   Dyam_Seed_Start(&ref[11],&ref[139],I(0),fun0,1)
	call_c   build_ref_138()
	call_c   Dyam_Seed_Add_Comp(&ref[138],fun62,0)
	call_c   Dyam_Seed_End()
	move_ret seed[10]
	c_ret

;; TERM 138: '*GUARD*'(seed_model(seed{model=> ('*RITEM*'(_B, _C) :> _D), id=> _E, anchor=> _F, subs_comp=> _G, code=> _H, go=> _I, tabule=> _J, schedule=> _K, subsume=> _L, model_ref=> _M, subs_comp_ref=> _N, c_type=> '*CURNEXT*', c_type_ref=> _O, out_env=> _P, appinfo=> _Q, tail_flag=> _R}))
c_code local build_ref_138
	ret_reg &ref[138]
	call_c   build_ref_12()
	call_c   build_ref_137()
	call_c   Dyam_Create_Unary(&ref[12],&ref[137])
	move_ret ref[138]
	c_ret

;; TERM 137: seed_model(seed{model=> ('*RITEM*'(_B, _C) :> _D), id=> _E, anchor=> _F, subs_comp=> _G, code=> _H, go=> _I, tabule=> _J, schedule=> _K, subsume=> _L, model_ref=> _M, subs_comp_ref=> _N, c_type=> '*CURNEXT*', c_type_ref=> _O, out_env=> _P, appinfo=> _Q, tail_flag=> _R})
c_code local build_ref_137
	ret_reg &ref[137]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_0()
	call_c   Dyam_Create_Binary(&ref[0],V(1),V(2))
	move_ret R(0)
	call_c   Dyam_Create_Binary(I(9),R(0),V(3))
	move_ret R(0)
	call_c   build_ref_284()
	call_c   build_ref_50()
	call_c   Dyam_Term_Start(&ref[284],16)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(&ref[50])
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_283()
	call_c   Dyam_Create_Unary(&ref[283],R(0))
	move_ret ref[137]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 50: '*CURNEXT*'
c_code local build_ref_50
	ret_reg &ref[50]
	call_c   Dyam_Create_Atom("*CURNEXT*")
	move_ret ref[50]
	c_ret

long local pool_fun58[2]=[1,build_ref_101]

pl_code local fun59
	call_c   Dyam_Remove_Choice()
fun58:
	call_c   Dyam_Choice(fun35)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(10),&ref[101])
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun34()


long local pool_fun60[4]=[131073,build_ref_23,pool_fun58,pool_fun58]

pl_code local fun61
	call_c   Dyam_Remove_Choice()
fun60:
	call_c   Dyam_Choice(fun59)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(9),&ref[23])
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun58()


long local pool_fun62[5]=[131074,build_ref_138,build_ref_140,pool_fun60,pool_fun60]

pl_code local fun62
	call_c   Dyam_Pool(pool_fun62)
	call_c   Dyam_Unify_Item(&ref[138])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun61)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(11),&ref[140])
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun60()

;; TERM 139: '*GUARD*'(seed_model(seed{model=> ('*RITEM*'(_B, _C) :> _D), id=> _E, anchor=> _F, subs_comp=> _G, code=> _H, go=> _I, tabule=> _J, schedule=> _K, subsume=> _L, model_ref=> _M, subs_comp_ref=> _N, c_type=> '*CURNEXT*', c_type_ref=> _O, out_env=> _P, appinfo=> _Q, tail_flag=> _R})) :> []
c_code local build_ref_139
	ret_reg &ref[139]
	call_c   build_ref_138()
	call_c   Dyam_Create_Binary(I(9),&ref[138],I(0))
	move_ret ref[139]
	c_ret

c_code local build_seed_20
	ret_reg &seed[20]
	call_c   build_ref_11()
	call_c   build_ref_143()
	call_c   Dyam_Seed_Start(&ref[11],&ref[143],I(0),fun0,1)
	call_c   build_ref_142()
	call_c   Dyam_Seed_Add_Comp(&ref[142],fun65,0)
	call_c   Dyam_Seed_End()
	move_ret seed[20]
	c_ret

;; TERM 142: '*GUARD*'(subsumption(_B, _C, _D, _E, _F))
c_code local build_ref_142
	ret_reg &ref[142]
	call_c   build_ref_12()
	call_c   build_ref_141()
	call_c   Dyam_Create_Unary(&ref[12],&ref[141])
	move_ret ref[142]
	c_ret

;; TERM 141: subsumption(_B, _C, _D, _E, _F)
c_code local build_ref_141
	ret_reg &ref[141]
	call_c   build_ref_305()
	call_c   Dyam_Term_Start(&ref[305],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[141]
	c_ret

;; TERM 305: subsumption
c_code local build_ref_305
	ret_reg &ref[305]
	call_c   Dyam_Create_Atom("subsumption")
	move_ret ref[305]
	c_ret

pl_code local fun63
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(1),I(0))
	fail_ret
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(5))
	pl_call  pred_label_term_2()
	pl_jump  fun34()

;; TERM 144: [yes,variance]
c_code local build_ref_144
	ret_reg &ref[144]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_192()
	call_c   Dyam_Create_List(&ref[192],I(0))
	move_ret R(0)
	call_c   build_ref_23()
	call_c   Dyam_Create_List(&ref[23],R(0))
	move_ret ref[144]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 192: variance
c_code local build_ref_192
	ret_reg &ref[192]
	call_c   Dyam_Create_Atom("variance")
	move_ret ref[192]
	c_ret

long local pool_fun64[2]=[1,build_ref_144]

pl_code local fun64
	call_c   Dyam_Update_Choice(fun63)
	call_c   Dyam_Set_Cut()
	pl_call  Domain_2(V(2),&ref[144])
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(1),V(3))
	fail_ret
	call_c   Dyam_Unify(V(5),V(4))
	fail_ret
	pl_jump  fun34()

long local pool_fun65[3]=[65537,build_ref_142,pool_fun64]

pl_code local fun65
	call_c   Dyam_Pool(pool_fun65)
	call_c   Dyam_Unify_Item(&ref[142])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun64)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_notvar(V(1))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(5))
	pl_call  pred_label_term_2()
	pl_jump  fun34()

;; TERM 143: '*GUARD*'(subsumption(_B, _C, _D, _E, _F)) :> []
c_code local build_ref_143
	ret_reg &ref[143]
	call_c   build_ref_142()
	call_c   Dyam_Create_Binary(I(9),&ref[142],I(0))
	move_ret ref[143]
	c_ret

c_code local build_seed_8
	ret_reg &seed[8]
	call_c   build_ref_11()
	call_c   build_ref_147()
	call_c   Dyam_Seed_Start(&ref[11],&ref[147],I(0),fun0,1)
	call_c   build_ref_146()
	call_c   Dyam_Seed_Add_Comp(&ref[146],fun66,0)
	call_c   Dyam_Seed_End()
	move_ret seed[8]
	c_ret

;; TERM 146: '*GUARD*'(seed_model(seed{model=> '*WAIT-CONT*'{rest=> _B, cont=> _C, tupple=> _D}, id=> _E, anchor=> cont(_F), subs_comp=> _G, code=> _H, go=> _I, tabule=> no, schedule=> last, subsume=> no, model_ref=> _J, subs_comp_ref=> _K, c_type=> '*WAIT_CONT*', c_type_ref=> _L, out_env=> _M, appinfo=> _N, tail_flag=> _O}))
c_code local build_ref_146
	ret_reg &ref[146]
	call_c   build_ref_12()
	call_c   build_ref_145()
	call_c   Dyam_Create_Unary(&ref[12],&ref[145])
	move_ret ref[146]
	c_ret

;; TERM 145: seed_model(seed{model=> '*WAIT-CONT*'{rest=> _B, cont=> _C, tupple=> _D}, id=> _E, anchor=> cont(_F), subs_comp=> _G, code=> _H, go=> _I, tabule=> no, schedule=> last, subsume=> no, model_ref=> _J, subs_comp_ref=> _K, c_type=> '*WAIT_CONT*', c_type_ref=> _L, out_env=> _M, appinfo=> _N, tail_flag=> _O})
c_code local build_ref_145
	ret_reg &ref[145]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_284()
	call_c   build_ref_148()
	call_c   build_ref_149()
	call_c   build_ref_140()
	call_c   build_ref_210()
	call_c   build_ref_306()
	call_c   Dyam_Term_Start(&ref[284],16)
	call_c   Dyam_Term_Arg(&ref[148])
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(&ref[149])
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(&ref[140])
	call_c   Dyam_Term_Arg(&ref[210])
	call_c   Dyam_Term_Arg(&ref[140])
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(&ref[306])
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_283()
	call_c   Dyam_Create_Unary(&ref[283],R(0))
	move_ret ref[145]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 306: '*WAIT_CONT*'
c_code local build_ref_306
	ret_reg &ref[306]
	call_c   Dyam_Create_Atom("*WAIT_CONT*")
	move_ret ref[306]
	c_ret

;; TERM 149: cont(_F)
c_code local build_ref_149
	ret_reg &ref[149]
	call_c   build_ref_307()
	call_c   Dyam_Create_Unary(&ref[307],V(5))
	move_ret ref[149]
	c_ret

;; TERM 307: cont
c_code local build_ref_307
	ret_reg &ref[307]
	call_c   Dyam_Create_Atom("cont")
	move_ret ref[307]
	c_ret

;; TERM 148: '*WAIT-CONT*'{rest=> _B, cont=> _C, tupple=> _D}
c_code local build_ref_148
	ret_reg &ref[148]
	call_c   build_ref_308()
	call_c   Dyam_Term_Start(&ref[308],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[148]
	c_ret

;; TERM 308: '*WAIT-CONT*'!'$ft'
c_code local build_ref_308
	ret_reg &ref[308]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_311()
	call_c   Dyam_Create_List(&ref[311],I(0))
	move_ret R(0)
	call_c   build_ref_307()
	call_c   Dyam_Create_List(&ref[307],R(0))
	move_ret R(0)
	call_c   build_ref_310()
	call_c   Dyam_Create_List(&ref[310],R(0))
	move_ret R(0)
	call_c   build_ref_309()
	call_c   Dyam_Create_Atom_Module("$ft",&ref[309])
	move_ret ref[308]
	call_c   DYAM_Feature_2(&ref[309],R(0))
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 309: '*WAIT-CONT*'
c_code local build_ref_309
	ret_reg &ref[309]
	call_c   Dyam_Create_Atom("*WAIT-CONT*")
	move_ret ref[309]
	c_ret

;; TERM 310: rest
c_code local build_ref_310
	ret_reg &ref[310]
	call_c   Dyam_Create_Atom("rest")
	move_ret ref[310]
	c_ret

;; TERM 311: tupple
c_code local build_ref_311
	ret_reg &ref[311]
	call_c   Dyam_Create_Atom("tupple")
	move_ret ref[311]
	c_ret

;; TERM 150: no_copyable(_J)
c_code local build_ref_150
	ret_reg &ref[150]
	call_c   build_ref_312()
	call_c   Dyam_Create_Unary(&ref[312],V(9))
	move_ret ref[150]
	c_ret

;; TERM 312: no_copyable
c_code local build_ref_312
	ret_reg &ref[312]
	call_c   Dyam_Create_Atom("no_copyable")
	move_ret ref[312]
	c_ret

long local pool_fun66[5]=[4,build_ref_146,build_ref_148,build_ref_149,build_ref_150]

pl_code local fun66
	call_c   Dyam_Pool(pool_fun66)
	call_c   Dyam_Unify_Item(&ref[146])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[148], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(5))
	pl_call  pred_tupple_2()
	move     &ref[149], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(9))
	pl_call  pred_label_term_2()
	move     &ref[150], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Deallocate(1)
	pl_jump  pred_record_without_doublon_1()

;; TERM 147: '*GUARD*'(seed_model(seed{model=> '*WAIT-CONT*'{rest=> _B, cont=> _C, tupple=> _D}, id=> _E, anchor=> cont(_F), subs_comp=> _G, code=> _H, go=> _I, tabule=> no, schedule=> last, subsume=> no, model_ref=> _J, subs_comp_ref=> _K, c_type=> '*WAIT_CONT*', c_type_ref=> _L, out_env=> _M, appinfo=> _N, tail_flag=> _O})) :> []
c_code local build_ref_147
	ret_reg &ref[147]
	call_c   build_ref_146()
	call_c   Dyam_Create_Binary(I(9),&ref[146],I(0))
	move_ret ref[147]
	c_ret

c_code local build_seed_23
	ret_reg &seed[23]
	call_c   build_ref_16()
	call_c   build_ref_153()
	call_c   Dyam_Seed_Start(&ref[16],&ref[153],I(0),fun0,1)
	call_c   build_ref_154()
	call_c   Dyam_Seed_Add_Comp(&ref[154],fun83,0)
	call_c   Dyam_Seed_End()
	move_ret seed[23]
	c_ret

;; TERM 154: '*PROLOG-ITEM*'{top=> seed_install(seed{model=> _B, id=> '$seed'(_C, _D), anchor=> _E, subs_comp=> _F, code=> _G, go=> _H, tabule=> _I, schedule=> _J, subsume=> _K, model_ref=> _L, subs_comp_ref=> _M, c_type=> _N, c_type_ref=> _O, out_env=> _P, appinfo=> _Q, tail_flag=> _R}, _S, _T), cont=> _A}
c_code local build_ref_154
	ret_reg &ref[154]
	call_c   build_ref_269()
	call_c   build_ref_151()
	call_c   Dyam_Create_Binary(&ref[269],&ref[151],V(0))
	move_ret ref[154]
	c_ret

;; TERM 151: seed_install(seed{model=> _B, id=> '$seed'(_C, _D), anchor=> _E, subs_comp=> _F, code=> _G, go=> _H, tabule=> _I, schedule=> _J, subsume=> _K, model_ref=> _L, subs_comp_ref=> _M, c_type=> _N, c_type_ref=> _O, out_env=> _P, appinfo=> _Q, tail_flag=> _R}, _S, _T)
c_code local build_ref_151
	ret_reg &ref[151]
	call_c   build_ref_313()
	call_c   build_ref_205()
	call_c   Dyam_Term_Start(&ref[313],3)
	call_c   Dyam_Term_Arg(&ref[205])
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_End()
	move_ret ref[151]
	c_ret

;; TERM 205: seed{model=> _B, id=> '$seed'(_C, _D), anchor=> _E, subs_comp=> _F, code=> _G, go=> _H, tabule=> _I, schedule=> _J, subsume=> _K, model_ref=> _L, subs_comp_ref=> _M, c_type=> _N, c_type_ref=> _O, out_env=> _P, appinfo=> _Q, tail_flag=> _R}
c_code local build_ref_205
	ret_reg &ref[205]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_314()
	call_c   Dyam_Create_Binary(&ref[314],V(2),V(3))
	move_ret R(0)
	call_c   build_ref_284()
	call_c   Dyam_Term_Start(&ref[284],16)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_End()
	move_ret ref[205]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 314: '$seed'
c_code local build_ref_314
	ret_reg &ref[314]
	call_c   Dyam_Create_Atom("$seed")
	move_ret ref[314]
	c_ret

;; TERM 313: seed_install
c_code local build_ref_313
	ret_reg &ref[313]
	call_c   Dyam_Create_Atom("seed_install")
	move_ret ref[313]
	c_ret

c_code local build_seed_33
	ret_reg &seed[33]
	call_c   build_ref_12()
	call_c   build_ref_158()
	call_c   Dyam_Seed_Start(&ref[12],&ref[158],I(0),fun7,1)
	call_c   build_ref_159()
	call_c   Dyam_Seed_Add_Comp(&ref[159],&ref[158],0)
	call_c   Dyam_Seed_End()
	move_ret seed[33]
	c_ret

;; TERM 159: '*GUARD*'(seed_model(seed{model=> _B, id=> '$seed'(_C, _D), anchor=> _E, subs_comp=> _F, code=> _G, go=> _H, tabule=> _I, schedule=> _J, subsume=> _K, model_ref=> _L, subs_comp_ref=> _M, c_type=> _N, c_type_ref=> _O, out_env=> _P, appinfo=> _Q, tail_flag=> _R})) :> '$$HOLE$$'
c_code local build_ref_159
	ret_reg &ref[159]
	call_c   build_ref_158()
	call_c   Dyam_Create_Binary(I(9),&ref[158],I(7))
	move_ret ref[159]
	c_ret

;; TERM 158: '*GUARD*'(seed_model(seed{model=> _B, id=> '$seed'(_C, _D), anchor=> _E, subs_comp=> _F, code=> _G, go=> _H, tabule=> _I, schedule=> _J, subsume=> _K, model_ref=> _L, subs_comp_ref=> _M, c_type=> _N, c_type_ref=> _O, out_env=> _P, appinfo=> _Q, tail_flag=> _R}))
c_code local build_ref_158
	ret_reg &ref[158]
	call_c   build_ref_12()
	call_c   build_ref_157()
	call_c   Dyam_Create_Unary(&ref[12],&ref[157])
	move_ret ref[158]
	c_ret

;; TERM 157: seed_model(seed{model=> _B, id=> '$seed'(_C, _D), anchor=> _E, subs_comp=> _F, code=> _G, go=> _H, tabule=> _I, schedule=> _J, subsume=> _K, model_ref=> _L, subs_comp_ref=> _M, c_type=> _N, c_type_ref=> _O, out_env=> _P, appinfo=> _Q, tail_flag=> _R})
c_code local build_ref_157
	ret_reg &ref[157]
	call_c   build_ref_283()
	call_c   build_ref_205()
	call_c   Dyam_Create_Unary(&ref[283],&ref[205])
	move_ret ref[157]
	c_ret

;; TERM 204: '$CLOSURE'('$fun'(80, 0, 1082995856), '$TUPPLE'(35046582400100))
c_code local build_ref_204
	ret_reg &ref[204]
	call_c   build_ref_203()
	call_c   Dyam_Closure_Aux(fun80,&ref[203])
	move_ret ref[204]
	c_ret

;; TERM 167: '$arg'(0)
c_code local build_ref_167
	ret_reg &ref[167]
	call_c   build_ref_315()
	call_c   Dyam_Create_Unary(&ref[315],N(0))
	move_ret ref[167]
	c_ret

;; TERM 315: '$arg'
c_code local build_ref_315
	ret_reg &ref[315]
	call_c   Dyam_Create_Atom("$arg")
	move_ret ref[315]
	c_ret

;; TERM 168: '$arg'(1)
c_code local build_ref_168
	ret_reg &ref[168]
	call_c   build_ref_315()
	call_c   Dyam_Create_Unary(&ref[315],N(1))
	move_ret ref[168]
	c_ret

;; TERM 198: call(_D1, ['$seed'(_C, _D),c(_Z)])
c_code local build_ref_198
	ret_reg &ref[198]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_314()
	call_c   Dyam_Create_Binary(&ref[314],V(2),V(3))
	move_ret R(0)
	call_c   build_ref_267()
	call_c   Dyam_Create_Unary(&ref[267],V(25))
	move_ret R(1)
	call_c   Dyam_Create_List(R(1),I(0))
	move_ret R(1)
	call_c   Dyam_Create_List(R(0),R(1))
	move_ret R(1)
	call_c   build_ref_262()
	call_c   Dyam_Create_Binary(&ref[262],V(29),R(1))
	move_ret ref[198]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 199: backptr(_Y)
c_code local build_ref_199
	ret_reg &ref[199]
	call_c   build_ref_316()
	call_c   Dyam_Create_Unary(&ref[316],V(24))
	move_ret ref[199]
	c_ret

;; TERM 316: backptr
c_code local build_ref_316
	ret_reg &ref[316]
	call_c   Dyam_Create_Atom("backptr")
	move_ret ref[316]
	c_ret

;; TERM 200: '$fun'(_I1, 2, _J1)
c_code local build_ref_200
	ret_reg &ref[200]
	call_c   build_ref_317()
	call_c   Dyam_Term_Start(&ref[317],3)
	call_c   Dyam_Term_Arg(V(34))
	call_c   Dyam_Term_Arg(N(2))
	call_c   Dyam_Term_Arg(V(35))
	call_c   Dyam_Term_End()
	move_ret ref[200]
	c_ret

;; TERM 317: '$fun'
c_code local build_ref_317
	ret_reg &ref[317]
	call_c   Dyam_Create_Atom("$fun")
	move_ret ref[317]
	c_ret

;; TERM 201: [_X,_Y]
c_code local build_ref_201
	ret_reg &ref[201]
	call_c   Dyam_Create_Tupple(23,24,I(0))
	move_ret ref[201]
	c_ret

;; TERM 197: object(_X) :> _M1
c_code local build_ref_197
	ret_reg &ref[197]
	call_c   build_ref_196()
	call_c   Dyam_Create_Binary(I(9),&ref[196],V(38))
	move_ret ref[197]
	c_ret

;; TERM 196: object(_X)
c_code local build_ref_196
	ret_reg &ref[196]
	call_c   build_ref_318()
	call_c   Dyam_Create_Unary(&ref[318],V(23))
	move_ret ref[196]
	c_ret

;; TERM 318: object
c_code local build_ref_318
	ret_reg &ref[318]
	call_c   Dyam_Create_Atom("object")
	move_ret ref[318]
	c_ret

;; TERM 191: _E1 :> _U1
c_code local build_ref_191
	ret_reg &ref[191]
	call_c   Dyam_Create_Binary(I(9),V(30),V(46))
	move_ret ref[191]
	c_ret

pl_code local fun70
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(46),V(39))
	fail_ret
fun69:
	move     &ref[191], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(29))
	pl_call  pred_label_code_2()
	call_c   Dyam_Deallocate()
	pl_jump  Follow_Cont(V(0))


;; TERM 187: _O1 :> _P1 ^ _Q1
c_code local build_ref_187
	ret_reg &ref[187]
	call_c   build_ref_186()
	call_c   Dyam_Create_Binary(I(9),V(40),&ref[186])
	move_ret ref[187]
	c_ret

;; TERM 186: _P1 ^ _Q1
c_code local build_ref_186
	ret_reg &ref[186]
	call_c   build_ref_319()
	call_c   Dyam_Create_Binary(&ref[319],V(41),V(42))
	move_ret ref[186]
	c_ret

;; TERM 319: ^
c_code local build_ref_319
	ret_reg &ref[319]
	call_c   Dyam_Create_Atom("^")
	move_ret ref[319]
	c_ret

;; TERM 188: _R1 :> _S1
c_code local build_ref_188
	ret_reg &ref[188]
	call_c   Dyam_Create_Binary(I(9),V(43),V(44))
	move_ret ref[188]
	c_ret

;; TERM 190: indexingkey(_P1, _T1) :> _N1
c_code local build_ref_190
	ret_reg &ref[190]
	call_c   build_ref_189()
	call_c   Dyam_Create_Binary(I(9),&ref[189],V(39))
	move_ret ref[190]
	c_ret

;; TERM 189: indexingkey(_P1, _T1)
c_code local build_ref_189
	ret_reg &ref[189]
	call_c   build_ref_320()
	call_c   Dyam_Create_Binary(&ref[320],V(41),V(45))
	move_ret ref[189]
	c_ret

;; TERM 320: indexingkey
c_code local build_ref_320
	ret_reg &ref[320]
	call_c   Dyam_Create_Atom("indexingkey")
	move_ret ref[320]
	c_ret

long local pool_fun71[6]=[5,build_ref_191,build_ref_187,build_ref_188,build_ref_190,build_ref_191]

pl_code local fun72
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(39),V(37))
	fail_ret
fun71:
	call_c   Dyam_Choice(fun70)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[187])
	fail_ret
	call_c   Dyam_Unify(V(5),&ref[188])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(44))
	move     V(45), R(2)
	move     S(5), R(3)
	pl_call  pred_label_term_2()
	call_c   Dyam_Unify(V(46),&ref[190])
	fail_ret
	pl_jump  fun69()


;; TERM 195: setcut :> variance(_X) :> cut :> succeed
c_code local build_ref_195
	ret_reg &ref[195]
	call_c   build_ref_180()
	call_c   build_ref_194()
	call_c   Dyam_Create_Binary(I(9),&ref[180],&ref[194])
	move_ret ref[195]
	c_ret

;; TERM 194: variance(_X) :> cut :> succeed
c_code local build_ref_194
	ret_reg &ref[194]
	call_c   build_ref_193()
	call_c   build_ref_183()
	call_c   Dyam_Create_Binary(I(9),&ref[193],&ref[183])
	move_ret ref[194]
	c_ret

;; TERM 183: cut :> succeed
c_code local build_ref_183
	ret_reg &ref[183]
	call_c   build_ref_182()
	call_c   build_ref_128()
	call_c   Dyam_Create_Binary(I(9),&ref[182],&ref[128])
	move_ret ref[183]
	c_ret

;; TERM 182: cut
c_code local build_ref_182
	ret_reg &ref[182]
	call_c   Dyam_Create_Atom("cut")
	move_ret ref[182]
	c_ret

;; TERM 193: variance(_X)
c_code local build_ref_193
	ret_reg &ref[193]
	call_c   build_ref_192()
	call_c   Dyam_Create_Unary(&ref[192],V(23))
	move_ret ref[193]
	c_ret

;; TERM 180: setcut
c_code local build_ref_180
	ret_reg &ref[180]
	call_c   Dyam_Create_Atom("setcut")
	move_ret ref[180]
	c_ret

long local pool_fun73[5]=[131074,build_ref_192,build_ref_195,pool_fun71,pool_fun71]

pl_code local fun73
	call_c   Dyam_Update_Choice(fun72)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(10),&ref[192])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(37))
	move     &ref[195], R(2)
	move     S(5), R(3)
	move     V(39), R(4)
	move     S(5), R(5)
	pl_call  pred_choice_code_3()
	pl_jump  fun71()

;; TERM 185: setcut :> subsume(_X) :> cut :> succeed
c_code local build_ref_185
	ret_reg &ref[185]
	call_c   build_ref_180()
	call_c   build_ref_184()
	call_c   Dyam_Create_Binary(I(9),&ref[180],&ref[184])
	move_ret ref[185]
	c_ret

;; TERM 184: subsume(_X) :> cut :> succeed
c_code local build_ref_184
	ret_reg &ref[184]
	call_c   build_ref_181()
	call_c   build_ref_183()
	call_c   Dyam_Create_Binary(I(9),&ref[181],&ref[183])
	move_ret ref[184]
	c_ret

;; TERM 181: subsume(_X)
c_code local build_ref_181
	ret_reg &ref[181]
	call_c   build_ref_290()
	call_c   Dyam_Create_Unary(&ref[290],V(23))
	move_ret ref[181]
	c_ret

long local pool_fun74[5]=[131074,build_ref_23,build_ref_185,pool_fun73,pool_fun71]

pl_code local fun75
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(38),V(36))
	fail_ret
fun74:
	call_c   Dyam_Choice(fun73)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(10),&ref[23])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(37))
	move     &ref[185], R(2)
	move     S(5), R(3)
	move     V(39), R(4)
	move     S(5), R(5)
	pl_call  pred_choice_code_3()
	pl_jump  fun71()


;; TERM 179: tabule :> _K1
c_code local build_ref_179
	ret_reg &ref[179]
	call_c   build_ref_178()
	call_c   Dyam_Create_Binary(I(9),&ref[178],V(36))
	move_ret ref[179]
	c_ret

long local pool_fun76[5]=[131074,build_ref_23,build_ref_179,pool_fun74,pool_fun74]

long local pool_fun77[3]=[65537,build_ref_197,pool_fun76]

pl_code local fun77
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(37),&ref[197])
	fail_ret
fun76:
	call_c   Dyam_Choice(fun75)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(8),&ref[23])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(38),&ref[179])
	fail_ret
	pl_jump  fun74()


;; TERM 177: light_object(_X) :> _M1
c_code local build_ref_177
	ret_reg &ref[177]
	call_c   build_ref_176()
	call_c   Dyam_Create_Binary(I(9),&ref[176],V(38))
	move_ret ref[177]
	c_ret

;; TERM 176: light_object(_X)
c_code local build_ref_176
	ret_reg &ref[176]
	call_c   build_ref_321()
	call_c   Dyam_Create_Unary(&ref[321],V(23))
	move_ret ref[176]
	c_ret

;; TERM 321: light_object
c_code local build_ref_321
	ret_reg &ref[321]
	call_c   Dyam_Create_Atom("light_object")
	move_ret ref[321]
	c_ret

long local pool_fun78[5]=[131074,build_ref_175,build_ref_177,pool_fun77,pool_fun76]

long local pool_fun79[6]=[65540,build_ref_198,build_ref_199,build_ref_200,build_ref_201,pool_fun78]

pl_code local fun79
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(18),V(25))
	fail_ret
	call_c   Dyam_Unify(V(19),&ref[198])
	fail_ret
	call_c   Dyam_Unify(V(30),&ref[199])
	fail_ret
	call_c   Dyam_Unify(V(29),&ref[200])
	fail_ret
	call_c   Dyam_Unify(V(33),&ref[201])
	fail_ret
fun78:
	call_c   Dyam_Reg_Load(0,V(9))
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(36), R(4)
	move     S(5), R(5)
	pl_call  pred_test_schedule_3()
	call_c   Dyam_Choice(fun77)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(8),&ref[175])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(37),&ref[177])
	fail_ret
	pl_jump  fun76()


;; TERM 169: [_Z,_A1]
c_code local build_ref_169
	ret_reg &ref[169]
	call_c   Dyam_Create_Tupple(25,26,I(0))
	move_ret ref[169]
	c_ret

;; TERM 170: '$arg'(2)
c_code local build_ref_170
	ret_reg &ref[170]
	call_c   build_ref_315()
	call_c   Dyam_Create_Unary(&ref[315],N(2))
	move_ret ref[170]
	c_ret

;; TERM 171: call(_D1, ['$seed'(_C, _D),c(_Z),_B1])
c_code local build_ref_171
	ret_reg &ref[171]
	call_c   Dyam_Pseudo_Choice(3)
	call_c   build_ref_314()
	call_c   Dyam_Create_Binary(&ref[314],V(2),V(3))
	move_ret R(0)
	call_c   build_ref_267()
	call_c   Dyam_Create_Unary(&ref[267],V(25))
	move_ret R(1)
	call_c   Dyam_Create_List(V(27),I(0))
	move_ret R(2)
	call_c   Dyam_Create_List(R(1),R(2))
	move_ret R(2)
	call_c   Dyam_Create_List(R(0),R(2))
	move_ret R(2)
	call_c   build_ref_262()
	call_c   Dyam_Create_Binary(&ref[262],V(29),R(2))
	move_ret ref[171]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 172: backptr(_Y, _C1)
c_code local build_ref_172
	ret_reg &ref[172]
	call_c   build_ref_316()
	call_c   Dyam_Create_Binary(&ref[316],V(24),V(28))
	move_ret ref[172]
	c_ret

;; TERM 173: '$fun'(_F1, 3, _G1)
c_code local build_ref_173
	ret_reg &ref[173]
	call_c   build_ref_317()
	call_c   Dyam_Term_Start(&ref[317],3)
	call_c   Dyam_Term_Arg(V(31))
	call_c   Dyam_Term_Arg(N(3))
	call_c   Dyam_Term_Arg(V(32))
	call_c   Dyam_Term_End()
	move_ret ref[173]
	c_ret

;; TERM 174: [_X,_Y,_C1]
c_code local build_ref_174
	ret_reg &ref[174]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(28),I(0))
	move_ret R(0)
	call_c   Dyam_Create_Tupple(23,24,R(0))
	move_ret ref[174]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun80[11]=[131080,build_ref_167,build_ref_168,build_ref_169,build_ref_170,build_ref_171,build_ref_172,build_ref_173,build_ref_174,pool_fun79,pool_fun78]

pl_code local fun80
	call_c   Dyam_Pool(pool_fun80)
	call_c   Dyam_Unify(V(23),&ref[167])
	fail_ret
	call_c   Dyam_Unify(V(24),&ref[168])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun79)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(18),&ref[169])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(26))
	move     V(27), R(2)
	move     S(5), R(3)
	pl_call  pred_label_term_2()
	call_c   Dyam_Unify(V(28),&ref[170])
	fail_ret
	call_c   Dyam_Unify(V(19),&ref[171])
	fail_ret
	call_c   Dyam_Unify(V(30),&ref[172])
	fail_ret
	call_c   Dyam_Unify(V(29),&ref[173])
	fail_ret
	call_c   Dyam_Unify(V(33),&ref[174])
	fail_ret
	pl_jump  fun78()

;; TERM 203: '$TUPPLE'(35046582400100)
c_code local build_ref_203
	ret_reg &ref[203]
	call_c   Dyam_Create_Simple_Tupple(0,513541632)
	move_ret ref[203]
	c_ret

long local pool_fun81[4]=[3,build_seed_33,build_ref_204,build_ref_205]

pl_code local fun82
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(22),I(0))
	fail_ret
fun81:
	pl_call  fun20(&seed[33],1)
	move     &ref[204], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Load(4,V(22))
	move     &ref[205], R(6)
	move     S(5), R(7)
	call_c   Dyam_Reg_Deallocate(4)
fun68:
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Reg_Bind(2,V(4))
	call_c   Dyam_Multi_Reg_Bind(4,2,2)
	pl_call  fun12(&seed[34],1)
	pl_call  fun15(&seed[35],2,V(4))
	call_c   Dyam_Deallocate()
	pl_ret



;; TERM 155: _U :> _V
c_code local build_ref_155
	ret_reg &ref[155]
	call_c   Dyam_Create_Binary(I(9),V(20),V(21))
	move_ret ref[155]
	c_ret

;; TERM 156: seeds
c_code local build_ref_156
	ret_reg &ref[156]
	call_c   Dyam_Create_Atom("seeds")
	move_ret ref[156]
	c_ret

long local pool_fun83[6]=[131075,build_ref_154,build_ref_155,build_ref_156,pool_fun81,pool_fun81]

pl_code local fun83
	call_c   Dyam_Pool(pool_fun83)
	call_c   Dyam_Unify_Item(&ref[154])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun82)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[155])
	fail_ret
	call_c   Dyam_Cut()
	move     &ref[156], R(0)
	move     0, R(1)
	call_c   Dyam_Reg_Load(2,V(2))
	pl_call  pred_update_counter_2()
	call_c   Dyam_Unify(V(22),V(2))
	fail_ret
	pl_jump  fun81()

;; TERM 153: '*PROLOG-FIRST*'(seed_install(seed{model=> _B, id=> '$seed'(_C, _D), anchor=> _E, subs_comp=> _F, code=> _G, go=> _H, tabule=> _I, schedule=> _J, subsume=> _K, model_ref=> _L, subs_comp_ref=> _M, c_type=> _N, c_type_ref=> _O, out_env=> _P, appinfo=> _Q, tail_flag=> _R}, _S, _T)) :> []
c_code local build_ref_153
	ret_reg &ref[153]
	call_c   build_ref_152()
	call_c   Dyam_Create_Binary(I(9),&ref[152],I(0))
	move_ret ref[153]
	c_ret

;; TERM 152: '*PROLOG-FIRST*'(seed_install(seed{model=> _B, id=> '$seed'(_C, _D), anchor=> _E, subs_comp=> _F, code=> _G, go=> _H, tabule=> _I, schedule=> _J, subsume=> _K, model_ref=> _L, subs_comp_ref=> _M, c_type=> _N, c_type_ref=> _O, out_env=> _P, appinfo=> _Q, tail_flag=> _R}, _S, _T))
c_code local build_ref_152
	ret_reg &ref[152]
	call_c   build_ref_17()
	call_c   build_ref_151()
	call_c   Dyam_Create_Unary(&ref[17],&ref[151])
	move_ret ref[152]
	c_ret

c_code local build_seed_22
	ret_reg &seed[22]
	call_c   build_ref_40()
	call_c   build_ref_222()
	call_c   Dyam_Seed_Start(&ref[40],&ref[222],I(0),fun14,1)
	call_c   build_ref_223()
	call_c   Dyam_Seed_Add_Comp(&ref[223],fun96,0)
	call_c   Dyam_Seed_End()
	move_ret seed[22]
	c_ret

;; TERM 223: '*CITEM*'(seed(_B, seed{model=> _C, id=> '$seed'(_D, _E), anchor=> _F, subs_comp=> _G, code=> _H, go=> _I, tabule=> _J, schedule=> _K, subsume=> _L, model_ref=> _M, subs_comp_ref=> _N, c_type=> _O, c_type_ref=> _P, out_env=> _Q, appinfo=> _R, tail_flag=> _S}), _A)
c_code local build_ref_223
	ret_reg &ref[223]
	call_c   build_ref_44()
	call_c   build_ref_220()
	call_c   Dyam_Create_Binary(&ref[44],&ref[220],V(0))
	move_ret ref[223]
	c_ret

;; TERM 220: seed(_B, seed{model=> _C, id=> '$seed'(_D, _E), anchor=> _F, subs_comp=> _G, code=> _H, go=> _I, tabule=> _J, schedule=> _K, subsume=> _L, model_ref=> _M, subs_comp_ref=> _N, c_type=> _O, c_type_ref=> _P, out_env=> _Q, appinfo=> _R, tail_flag=> _S})
c_code local build_ref_220
	ret_reg &ref[220]
	call_c   build_ref_261()
	call_c   build_ref_230()
	call_c   Dyam_Create_Binary(&ref[261],V(1),&ref[230])
	move_ret ref[220]
	c_ret

;; TERM 230: seed{model=> _C, id=> '$seed'(_D, _E), anchor=> _F, subs_comp=> _G, code=> _H, go=> _I, tabule=> _J, schedule=> _K, subsume=> _L, model_ref=> _M, subs_comp_ref=> _N, c_type=> _O, c_type_ref=> _P, out_env=> _Q, appinfo=> _R, tail_flag=> _S}
c_code local build_ref_230
	ret_reg &ref[230]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_314()
	call_c   Dyam_Create_Binary(&ref[314],V(3),V(4))
	move_ret R(0)
	call_c   build_ref_284()
	call_c   Dyam_Term_Start(&ref[284],16)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_End()
	move_ret ref[230]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_36
	ret_reg &seed[36]
	call_c   build_ref_12()
	call_c   build_ref_225()
	call_c   Dyam_Seed_Start(&ref[12],&ref[225],I(0),fun7,1)
	call_c   build_ref_226()
	call_c   Dyam_Seed_Add_Comp(&ref[226],&ref[225],0)
	call_c   Dyam_Seed_End()
	move_ret seed[36]
	c_ret

;; TERM 226: '*GUARD*'(anchor(_F, _C, _M, _S)) :> '$$HOLE$$'
c_code local build_ref_226
	ret_reg &ref[226]
	call_c   build_ref_225()
	call_c   Dyam_Create_Binary(I(9),&ref[225],I(7))
	move_ret ref[226]
	c_ret

;; TERM 225: '*GUARD*'(anchor(_F, _C, _M, _S))
c_code local build_ref_225
	ret_reg &ref[225]
	call_c   build_ref_12()
	call_c   build_ref_224()
	call_c   Dyam_Create_Unary(&ref[12],&ref[224])
	move_ret ref[225]
	c_ret

;; TERM 224: anchor(_F, _C, _M, _S)
c_code local build_ref_224
	ret_reg &ref[224]
	call_c   build_ref_287()
	call_c   Dyam_Term_Start(&ref[287],4)
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_End()
	move_ret ref[224]
	c_ret

c_code local build_seed_37
	ret_reg &seed[37]
	call_c   build_ref_12()
	call_c   build_ref_228()
	call_c   Dyam_Seed_Start(&ref[12],&ref[228],I(0),fun7,1)
	call_c   build_ref_229()
	call_c   Dyam_Seed_Add_Comp(&ref[229],&ref[228],0)
	call_c   Dyam_Seed_End()
	move_ret seed[37]
	c_ret

;; TERM 229: '*GUARD*'(subsumption(_G, _L, _F, _M, _N)) :> '$$HOLE$$'
c_code local build_ref_229
	ret_reg &ref[229]
	call_c   build_ref_228()
	call_c   Dyam_Create_Binary(I(9),&ref[228],I(7))
	move_ret ref[229]
	c_ret

;; TERM 228: '*GUARD*'(subsumption(_G, _L, _F, _M, _N))
c_code local build_ref_228
	ret_reg &ref[228]
	call_c   build_ref_12()
	call_c   build_ref_227()
	call_c   Dyam_Create_Unary(&ref[12],&ref[227])
	move_ret ref[228]
	c_ret

;; TERM 227: subsumption(_G, _L, _F, _M, _N)
c_code local build_ref_227
	ret_reg &ref[227]
	call_c   build_ref_305()
	call_c   Dyam_Term_Start(&ref[305],5)
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_End()
	move_ret ref[227]
	c_ret

c_code local build_seed_38
	ret_reg &seed[38]
	call_c   build_ref_26()
	call_c   build_ref_231()
	call_c   Dyam_Seed_Start(&ref[26],&ref[231],I(0),fun7,1)
	call_c   build_ref_234()
	call_c   Dyam_Seed_Add_Comp(&ref[234],&ref[231],0)
	call_c   Dyam_Seed_End()
	move_ret seed[38]
	c_ret

;; TERM 234: '*PROLOG-FIRST*'(seed_code(_C, _R, _H, _I, _Q)) :> '$$HOLE$$'
c_code local build_ref_234
	ret_reg &ref[234]
	call_c   build_ref_233()
	call_c   Dyam_Create_Binary(I(9),&ref[233],I(7))
	move_ret ref[234]
	c_ret

;; TERM 233: '*PROLOG-FIRST*'(seed_code(_C, _R, _H, _I, _Q))
c_code local build_ref_233
	ret_reg &ref[233]
	call_c   build_ref_17()
	call_c   build_ref_232()
	call_c   Dyam_Create_Unary(&ref[17],&ref[232])
	move_ret ref[233]
	c_ret

;; TERM 232: seed_code(_C, _R, _H, _I, _Q)
c_code local build_ref_232
	ret_reg &ref[232]
	call_c   build_ref_300()
	call_c   Dyam_Term_Start(&ref[300],5)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_End()
	move_ret ref[232]
	c_ret

;; TERM 231: '*PROLOG-ITEM*'{top=> seed_code(_C, _R, _H, _I, _Q), cont=> '$CLOSURE'('$fun'(91, 0, 1083115796), '$TUPPLE'(35046582400672))}
c_code local build_ref_231
	ret_reg &ref[231]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,402652160)
	move_ret R(0)
	call_c   Dyam_Closure_Aux(fun91,R(0))
	move_ret R(0)
	call_c   build_ref_269()
	call_c   build_ref_232()
	call_c   Dyam_Create_Binary(&ref[269],&ref[232],R(0))
	move_ret ref[231]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun91[3]=[2,build_ref_230,build_seed_29]

pl_code local fun91
	call_c   Dyam_Pool(pool_fun91)
	call_c   DYAM_Numbervars_3(&ref[230],N(1),V(20))
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load_Ptr(2,V(4))
	fail_ret
	call_c   Dyam_Reg_Load(4,&ref[230])
	call_c   DyALog_Mutable_Write(R(2),&R(4),0)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(4))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_jump  fun12(&seed[29],2)

long local pool_fun92[2]=[1,build_seed_38]

pl_code local fun93
	call_c   Dyam_Remove_Choice()
fun92:
	call_c   Dyam_Deallocate()
	pl_jump  fun20(&seed[38],12)


long local pool_fun94[6]=[131075,build_ref_140,build_seed_36,build_seed_37,pool_fun92,pool_fun92]

pl_code local fun95
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(3),V(1))
	fail_ret
fun94:
	call_c   Dyam_Reg_Load_Ptr(2,V(19))
	fail_ret
	move     &ref[140], R(4)
	move     0, R(5)
	call_c   DyALog_Mutable_Write(R(2),&R(4),0)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(19))
	fail_ret
	pl_call  fun20(&seed[36],1)
	pl_call  fun20(&seed[37],1)
	call_c   Dyam_Choice(fun93)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(17),I(0))
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun92()


long local pool_fun96[5]=[131074,build_ref_223,build_ref_156,pool_fun94,pool_fun94]

pl_code local fun96
	call_c   Dyam_Pool(pool_fun96)
	call_c   Dyam_Unify_Item(&ref[223])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load_Ptr(2,V(4))
	fail_ret
	move     I(0), R(4)
	move     0, R(5)
	call_c   DyALog_Mutable_Write(R(2),&R(4),0)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(4))
	fail_ret
	call_c   Dyam_Reg_Load(0,V(14))
	call_c   Dyam_Reg_Load(2,V(15))
	pl_call  pred_label_term_2()
	call_c   Dyam_Choice(fun95)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(1),I(0))
	fail_ret
	call_c   Dyam_Cut()
	move     &ref[156], R(0)
	move     0, R(1)
	call_c   Dyam_Reg_Load(2,V(3))
	pl_call  pred_update_counter_2()
	pl_jump  fun94()

;; TERM 222: '*FIRST*'(seed(_B, seed{model=> _C, id=> '$seed'(_D, _E), anchor=> _F, subs_comp=> _G, code=> _H, go=> _I, tabule=> _J, schedule=> _K, subsume=> _L, model_ref=> _M, subs_comp_ref=> _N, c_type=> _O, c_type_ref=> _P, out_env=> _Q, appinfo=> _R, tail_flag=> _S})) :> []
c_code local build_ref_222
	ret_reg &ref[222]
	call_c   build_ref_221()
	call_c   Dyam_Create_Binary(I(9),&ref[221],I(0))
	move_ret ref[222]
	c_ret

;; TERM 221: '*FIRST*'(seed(_B, seed{model=> _C, id=> '$seed'(_D, _E), anchor=> _F, subs_comp=> _G, code=> _H, go=> _I, tabule=> _J, schedule=> _K, subsume=> _L, model_ref=> _M, subs_comp_ref=> _N, c_type=> _O, c_type_ref=> _P, out_env=> _Q, appinfo=> _R, tail_flag=> _S}))
c_code local build_ref_221
	ret_reg &ref[221]
	call_c   build_ref_40()
	call_c   build_ref_220()
	call_c   Dyam_Create_Unary(&ref[40],&ref[220])
	move_ret ref[221]
	c_ret

c_code local build_seed_17
	ret_reg &seed[17]
	call_c   build_ref_16()
	call_c   build_ref_237()
	call_c   Dyam_Seed_Start(&ref[16],&ref[237],I(0),fun0,1)
	call_c   build_ref_238()
	call_c   Dyam_Seed_Add_Comp(&ref[238],fun105,0)
	call_c   Dyam_Seed_End()
	move_ret seed[17]
	c_ret

;; TERM 238: '*PROLOG-ITEM*'{top=> install_app(_B, _C, _D, _E, _F, _G), cont=> _A}
c_code local build_ref_238
	ret_reg &ref[238]
	call_c   build_ref_269()
	call_c   build_ref_235()
	call_c   Dyam_Create_Binary(&ref[269],&ref[235],V(0))
	move_ret ref[238]
	c_ret

;; TERM 235: install_app(_B, _C, _D, _E, _F, _G)
c_code local build_ref_235
	ret_reg &ref[235]
	call_c   build_ref_301()
	call_c   Dyam_Term_Start(&ref[301],6)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[235]
	c_ret

;; TERM 239: _B - 1
c_code local build_ref_239
	ret_reg &ref[239]
	call_c   build_ref_322()
	call_c   Dyam_Create_Binary(&ref[322],V(1),N(1))
	move_ret ref[239]
	c_ret

;; TERM 322: -
c_code local build_ref_322
	ret_reg &ref[322]
	call_c   Dyam_Create_Atom("-")
	move_ret ref[322]
	c_ret

c_code local build_seed_40
	ret_reg &seed[40]
	call_c   build_ref_26()
	call_c   build_ref_255()
	call_c   Dyam_Seed_Start(&ref[26],&ref[255],I(0),fun7,1)
	call_c   build_ref_258()
	call_c   Dyam_Seed_Add_Comp(&ref[258],&ref[255],0)
	call_c   Dyam_Seed_End()
	move_ret seed[40]
	c_ret

;; TERM 258: '*PROLOG-FIRST*'(component(_C, _H, _I, _D)) :> '$$HOLE$$'
c_code local build_ref_258
	ret_reg &ref[258]
	call_c   build_ref_257()
	call_c   Dyam_Create_Binary(I(9),&ref[257],I(7))
	move_ret ref[258]
	c_ret

;; TERM 257: '*PROLOG-FIRST*'(component(_C, _H, _I, _D))
c_code local build_ref_257
	ret_reg &ref[257]
	call_c   build_ref_17()
	call_c   build_ref_256()
	call_c   Dyam_Create_Unary(&ref[17],&ref[256])
	move_ret ref[257]
	c_ret

;; TERM 256: component(_C, _H, _I, _D)
c_code local build_ref_256
	ret_reg &ref[256]
	call_c   build_ref_270()
	call_c   Dyam_Term_Start(&ref[270],4)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[256]
	c_ret

;; TERM 255: '*PROLOG-ITEM*'{top=> component(_C, _H, _I, _D), cont=> '$CLOSURE'('$fun'(104, 0, 1083189644), '$TUPPLE'(35046582401040))}
c_code local build_ref_255
	ret_reg &ref[255]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,535822336)
	move_ret R(0)
	call_c   Dyam_Closure_Aux(fun104,R(0))
	move_ret R(0)
	call_c   build_ref_269()
	call_c   build_ref_256()
	call_c   Dyam_Create_Binary(&ref[269],&ref[256],R(0))
	move_ret ref[255]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 254: direct(_J, _L, _Q, _M, _N)
c_code local build_ref_254
	ret_reg &ref[254]
	call_c   build_ref_282()
	call_c   Dyam_Term_Start(&ref[282],5)
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_End()
	move_ret ref[254]
	c_ret

;; TERM 253: [_Q|_P]
c_code local build_ref_253
	ret_reg &ref[253]
	call_c   Dyam_Create_List(V(16),V(15))
	move_ret ref[253]
	c_ret

;; TERM 242: component(_J, _L, _N)
c_code local build_ref_242
	ret_reg &ref[242]
	call_c   build_ref_270()
	call_c   Dyam_Term_Start(&ref[270],3)
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_End()
	move_ret ref[242]
	c_ret

;; TERM 250: allocate :> _M :> deallocate :> _O
c_code local build_ref_250
	ret_reg &ref[250]
	call_c   build_ref_126()
	call_c   build_ref_249()
	call_c   Dyam_Create_Binary(I(9),&ref[126],&ref[249])
	move_ret ref[250]
	c_ret

;; TERM 249: _M :> deallocate :> _O
c_code local build_ref_249
	ret_reg &ref[249]
	call_c   build_ref_248()
	call_c   Dyam_Create_Binary(I(9),V(12),&ref[248])
	move_ret ref[249]
	c_ret

;; TERM 248: deallocate :> _O
c_code local build_ref_248
	ret_reg &ref[248]
	call_c   build_ref_127()
	call_c   Dyam_Create_Binary(I(9),&ref[127],V(14))
	move_ret ref[248]
	c_ret

c_code local build_seed_39
	ret_reg &seed[39]
	call_c   build_ref_26()
	call_c   build_ref_244()
	call_c   Dyam_Seed_Start(&ref[26],&ref[244],I(0),fun7,1)
	call_c   build_ref_247()
	call_c   Dyam_Seed_Add_Comp(&ref[247],&ref[244],0)
	call_c   Dyam_Seed_End()
	move_ret seed[39]
	c_ret

;; TERM 247: '*PROLOG-FIRST*'(install_app(_H, _C, _D, _E, _T, _P)) :> '$$HOLE$$'
c_code local build_ref_247
	ret_reg &ref[247]
	call_c   build_ref_246()
	call_c   Dyam_Create_Binary(I(9),&ref[246],I(7))
	move_ret ref[247]
	c_ret

;; TERM 246: '*PROLOG-FIRST*'(install_app(_H, _C, _D, _E, _T, _P))
c_code local build_ref_246
	ret_reg &ref[246]
	call_c   build_ref_17()
	call_c   build_ref_245()
	call_c   Dyam_Create_Unary(&ref[17],&ref[245])
	move_ret ref[246]
	c_ret

;; TERM 245: install_app(_H, _C, _D, _E, _T, _P)
c_code local build_ref_245
	ret_reg &ref[245]
	call_c   build_ref_301()
	call_c   Dyam_Term_Start(&ref[301],6)
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_End()
	move_ret ref[245]
	c_ret

;; TERM 244: '*PROLOG-ITEM*'{top=> install_app(_H, _C, _D, _E, _T, _P), cont=> '$CLOSURE'('$fun'(97, 0, 1083158296), '$TUPPLE'(35046582400852))}
c_code local build_ref_244
	ret_reg &ref[244]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,276826624)
	move_ret R(0)
	call_c   Dyam_Closure_Aux(fun97,R(0))
	move_ret R(0)
	call_c   build_ref_269()
	call_c   build_ref_245()
	call_c   Dyam_Create_Binary(&ref[269],&ref[245],R(0))
	move_ret ref[244]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 243: _R :> succeed
c_code local build_ref_243
	ret_reg &ref[243]
	call_c   build_ref_128()
	call_c   Dyam_Create_Binary(I(9),V(17),&ref[128])
	move_ret ref[243]
	c_ret

pl_code local fun97
	call_c   build_ref_243()
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(19))
	move     &ref[243], R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Load(4,V(5))
	pl_call  pred_choice_code_3()
	call_c   Dyam_Deallocate()
	pl_jump  Follow_Cont(V(0))

long local pool_fun98[2]=[1,build_seed_39]

pl_code local fun98
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Deallocate()
	pl_jump  fun20(&seed[39],12)

long local pool_fun99[3]=[65537,build_ref_243,pool_fun98]

long local pool_fun100[3]=[65537,build_ref_250,pool_fun99]

pl_code local fun100
	call_c   Dyam_Remove_Choice()
	move     V(18), R(0)
	move     S(5), R(1)
	pl_call  pred_null_tupple_1()
	call_c   Dyam_Unify(V(17),&ref[250])
	fail_ret
fun99:
	call_c   Dyam_Choice(fun98)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(1),N(1))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(5),&ref[243])
	fail_ret
	call_c   Dyam_Unify(V(15),I(0))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_jump  Follow_Cont(V(0))


long local pool_fun101[5]=[131074,build_ref_242,build_ref_80,pool_fun100,pool_fun99]

long local pool_fun102[5]=[65539,build_ref_254,build_ref_80,build_ref_253,pool_fun101]

pl_code local fun102
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(8),&ref[254])
	fail_ret
	call_c   Dyam_Unify(V(14),&ref[80])
	fail_ret
	call_c   Dyam_Unify(V(6),&ref[253])
	fail_ret
fun101:
	call_c   DYAM_evpred_arg(V(1),V(4),&ref[242])
	fail_ret
	call_c   Dyam_Choice(fun100)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(12),&ref[80])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(17),V(14))
	fail_ret
	pl_jump  fun99()


;; TERM 251: direct(_H, _J, _K, _L, _Q, _M, _N)
c_code local build_ref_251
	ret_reg &ref[251]
	call_c   build_ref_282()
	call_c   Dyam_Term_Start(&ref[282],7)
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_End()
	move_ret ref[251]
	c_ret

;; TERM 252: direct(_K, _H)
c_code local build_ref_252
	ret_reg &ref[252]
	call_c   build_ref_282()
	call_c   Dyam_Create_Binary(&ref[282],V(10),V(7))
	move_ret ref[252]
	c_ret

long local pool_fun103[6]=[131075,build_ref_251,build_ref_252,build_ref_253,pool_fun102,pool_fun101]

pl_code local fun103
	call_c   Dyam_Update_Choice(fun102)
	call_c   Dyam_Unify(V(8),&ref[251])
	fail_ret
	call_c   Dyam_Unify(V(14),&ref[252])
	fail_ret
	call_c   Dyam_Unify(V(6),&ref[253])
	fail_ret
	pl_jump  fun101()

;; TERM 240: completion(_H, _J, _K, _L, _M, _N)
c_code local build_ref_240
	ret_reg &ref[240]
	call_c   build_ref_302()
	call_c   Dyam_Term_Start(&ref[302],6)
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_End()
	move_ret ref[240]
	c_ret

;; TERM 241: completion(_K, _H)
c_code local build_ref_241
	ret_reg &ref[241]
	call_c   build_ref_302()
	call_c   Dyam_Create_Binary(&ref[302],V(10),V(7))
	move_ret ref[241]
	c_ret

long local pool_fun104[5]=[131074,build_ref_240,build_ref_241,pool_fun103,pool_fun101]

pl_code local fun104
	call_c   Dyam_Pool(pool_fun104)
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun103)
	call_c   Dyam_Unify(V(8),&ref[240])
	fail_ret
	call_c   Dyam_Unify(V(14),&ref[241])
	fail_ret
	call_c   Dyam_Unify(V(6),V(15))
	fail_ret
	pl_jump  fun101()

long local pool_fun105[4]=[3,build_ref_238,build_ref_239,build_seed_40]

pl_code local fun105
	call_c   Dyam_Pool(pool_fun105)
	call_c   Dyam_Unify_Item(&ref[238])
	fail_ret
	call_c   DYAM_evpred_gt(V(1),N(0))
	fail_ret
	call_c   DYAM_evpred_is(V(7),&ref[239])
	fail_ret
	pl_jump  fun20(&seed[40],12)

;; TERM 237: '*PROLOG-FIRST*'(install_app(_B, _C, _D, _E, _F, _G)) :> []
c_code local build_ref_237
	ret_reg &ref[237]
	call_c   build_ref_236()
	call_c   Dyam_Create_Binary(I(9),&ref[236],I(0))
	move_ret ref[237]
	c_ret

;; TERM 236: '*PROLOG-FIRST*'(install_app(_B, _C, _D, _E, _F, _G))
c_code local build_ref_236
	ret_reg &ref[236]
	call_c   build_ref_17()
	call_c   build_ref_235()
	call_c   Dyam_Create_Unary(&ref[17],&ref[235])
	move_ret ref[236]
	c_ret

c_code local build_seed_35
	ret_reg &seed[35]
	call_c   build_ref_50()
	call_c   build_ref_166()
	call_c   Dyam_Seed_Start(&ref[50],&ref[166],I(0),fun14,1)
	call_c   build_ref_164()
	call_c   Dyam_Seed_Add_Comp(&ref[164],fun67,0)
	call_c   Dyam_Seed_End()
	move_ret seed[35]
	c_ret

;; TERM 164: '*RITEM*'(seed(_C, _D), voidret)
c_code local build_ref_164
	ret_reg &ref[164]
	call_c   build_ref_0()
	call_c   build_ref_160()
	call_c   build_ref_2()
	call_c   Dyam_Create_Binary(&ref[0],&ref[160],&ref[2])
	move_ret ref[164]
	c_ret

;; TERM 160: seed(_C, _D)
c_code local build_ref_160
	ret_reg &ref[160]
	call_c   build_ref_261()
	call_c   Dyam_Create_Binary(&ref[261],V(2),V(3))
	move_ret ref[160]
	c_ret

pl_code local fun67
	call_c   build_ref_164()
	call_c   Dyam_Unify_Item(&ref[164])
	fail_ret
	pl_jump  Follow_Cont(V(1))

;; TERM 166: '*RITEM*'(seed(_C, _D), voidret) :> seed(1, '$TUPPLE'(35046582125244))
c_code local build_ref_166
	ret_reg &ref[166]
	call_c   build_ref_164()
	call_c   build_ref_165()
	call_c   Dyam_Create_Binary(I(9),&ref[164],&ref[165])
	move_ret ref[166]
	c_ret

;; TERM 165: seed(1, '$TUPPLE'(35046582125244))
c_code local build_ref_165
	ret_reg &ref[165]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,134217728)
	move_ret R(0)
	call_c   build_ref_261()
	call_c   Dyam_Create_Binary(&ref[261],N(1),R(0))
	move_ret ref[165]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_34
	ret_reg &seed[34]
	call_c   build_ref_44()
	call_c   build_ref_161()
	call_c   Dyam_Seed_Start(&ref[44],&ref[161],&ref[161],fun7,1)
	call_c   build_ref_163()
	call_c   Dyam_Seed_Add_Comp(&ref[163],&ref[161],0)
	call_c   Dyam_Seed_End()
	move_ret seed[34]
	c_ret

;; TERM 163: '*FIRST*'(seed(_C, _D)) :> '$$HOLE$$'
c_code local build_ref_163
	ret_reg &ref[163]
	call_c   build_ref_162()
	call_c   Dyam_Create_Binary(I(9),&ref[162],I(7))
	move_ret ref[163]
	c_ret

;; TERM 162: '*FIRST*'(seed(_C, _D))
c_code local build_ref_162
	ret_reg &ref[162]
	call_c   build_ref_40()
	call_c   build_ref_160()
	call_c   Dyam_Create_Unary(&ref[40],&ref[160])
	move_ret ref[162]
	c_ret

;; TERM 161: '*CITEM*'(seed(_C, _D), seed(_C, _D))
c_code local build_ref_161
	ret_reg &ref[161]
	call_c   build_ref_44()
	call_c   build_ref_160()
	call_c   Dyam_Create_Binary(&ref[44],&ref[160],&ref[160])
	move_ret ref[161]
	c_ret

c_code local build_seed_28
	ret_reg &seed[28]
	call_c   build_ref_50()
	call_c   build_ref_53()
	call_c   Dyam_Seed_Start(&ref[50],&ref[53],I(0),fun14,1)
	call_c   build_ref_51()
	call_c   Dyam_Seed_Add_Comp(&ref[51],fun13,0)
	call_c   Dyam_Seed_End()
	move_ret seed[28]
	c_ret

;; TERM 51: '*RITEM*'(special_app_model(_C), voidret)
c_code local build_ref_51
	ret_reg &ref[51]
	call_c   build_ref_0()
	call_c   build_ref_46()
	call_c   build_ref_2()
	call_c   Dyam_Create_Binary(&ref[0],&ref[46],&ref[2])
	move_ret ref[51]
	c_ret

;; TERM 46: special_app_model(_C)
c_code local build_ref_46
	ret_reg &ref[46]
	call_c   build_ref_260()
	call_c   Dyam_Create_Unary(&ref[260],V(2))
	move_ret ref[46]
	c_ret

pl_code local fun13
	call_c   build_ref_51()
	call_c   Dyam_Unify_Item(&ref[51])
	fail_ret
	pl_jump  Follow_Cont(V(1))

;; TERM 53: '*RITEM*'(special_app_model(_C), voidret) :> seed(0, '$TUPPLE'(35046582125244))
c_code local build_ref_53
	ret_reg &ref[53]
	call_c   build_ref_51()
	call_c   build_ref_52()
	call_c   Dyam_Create_Binary(I(9),&ref[51],&ref[52])
	move_ret ref[53]
	c_ret

;; TERM 52: seed(0, '$TUPPLE'(35046582125244))
c_code local build_ref_52
	ret_reg &ref[52]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,134217728)
	move_ret R(0)
	call_c   build_ref_261()
	call_c   Dyam_Create_Binary(&ref[261],N(0),R(0))
	move_ret ref[52]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_27
	ret_reg &seed[27]
	call_c   build_ref_44()
	call_c   build_ref_47()
	call_c   Dyam_Seed_Start(&ref[44],&ref[47],&ref[47],fun7,1)
	call_c   build_ref_49()
	call_c   Dyam_Seed_Add_Comp(&ref[49],&ref[47],0)
	call_c   Dyam_Seed_End()
	move_ret seed[27]
	c_ret

;; TERM 49: '*FIRST*'(special_app_model(_C)) :> '$$HOLE$$'
c_code local build_ref_49
	ret_reg &ref[49]
	call_c   build_ref_48()
	call_c   Dyam_Create_Binary(I(9),&ref[48],I(7))
	move_ret ref[49]
	c_ret

;; TERM 48: '*FIRST*'(special_app_model(_C))
c_code local build_ref_48
	ret_reg &ref[48]
	call_c   build_ref_40()
	call_c   build_ref_46()
	call_c   Dyam_Create_Unary(&ref[40],&ref[46])
	move_ret ref[48]
	c_ret

;; TERM 47: '*CITEM*'(special_app_model(_C), special_app_model(_C))
c_code local build_ref_47
	ret_reg &ref[47]
	call_c   build_ref_44()
	call_c   build_ref_46()
	call_c   Dyam_Create_Binary(&ref[44],&ref[46],&ref[46])
	move_ret ref[47]
	c_ret

pl_code local fun1
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Object(R(0))
	call_c   Dyam_Tabule()
	pl_ret

pl_code local fun2
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Choice(fun1)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Subsume(R(0))
	fail_ret
	call_c   Dyam_Cut()
	pl_ret

pl_code local fun54
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Object(R(0))
	call_c   Dyam_Tabule()
	pl_ret

pl_code local fun84
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(3),&ref[128])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun85
	call_c   Dyam_Update_Choice(fun84)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(2),&ref[217])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(3),&ref[219])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun86
	call_c   Dyam_Update_Choice(fun85)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(1),&ref[113])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(3),&ref[216])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun87
	call_c   Dyam_Update_Choice(fun86)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(1),&ref[101])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(3),&ref[214])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun88
	call_c   Dyam_Update_Choice(fun87)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(1),&ref[210])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(3),&ref[212])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun89
	call_c   Dyam_Update_Choice(fun88)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(1),&ref[97])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(3),&ref[209])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun25
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(1),&ref[84])
	fail_ret
	call_c   Dyam_Unify(V(3),&ref[80])
	fail_ret
	call_c   Dyam_Unify(V(4),&ref[81])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun26
	call_c   Dyam_Update_Choice(fun25)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[82])
	fail_ret
	pl_call  Domain_2(&ref[83],V(2))
	call_c   Dyam_Cut()
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun15
	call_c   Dyam_Backptr_Trace(R(1),R(2))
	call_c   Dyam_Object(R(0))
	call_c   Dyam_Tabule()
	call_c   Dyam_Now()
	pl_ret

pl_code local fun11
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Object(R(0))
	call_c   Dyam_Tabule()
	call_c   Dyam_Schedule()
	pl_ret


;;------------------ INIT POOL ------------

c_code local build_init_pool
	call_c   build_ref_1()
	call_c   build_ref_3()
	call_c   build_ref_4()
	call_c   build_ref_5()
	call_c   build_ref_6()
	call_c   build_ref_7()
	call_c   build_ref_207()
	call_c   build_ref_206()
	call_c   build_ref_209()
	call_c   build_ref_97()
	call_c   build_ref_212()
	call_c   build_ref_210()
	call_c   build_ref_214()
	call_c   build_ref_101()
	call_c   build_ref_216()
	call_c   build_ref_113()
	call_c   build_ref_219()
	call_c   build_ref_217()
	call_c   build_ref_128()
	call_c   build_ref_83()
	call_c   build_ref_82()
	call_c   build_ref_81()
	call_c   build_ref_80()
	call_c   build_ref_84()
	call_c   build_seed_24()
	call_c   build_seed_25()
	call_c   build_seed_16()
	call_c   build_seed_0()
	call_c   build_seed_1()
	call_c   build_seed_2()
	call_c   build_seed_6()
	call_c   build_seed_13()
	call_c   build_seed_19()
	call_c   build_seed_3()
	call_c   build_seed_4()
	call_c   build_seed_5()
	call_c   build_seed_15()
	call_c   build_seed_12()
	call_c   build_seed_7()
	call_c   build_seed_11()
	call_c   build_seed_21()
	call_c   build_seed_9()
	call_c   build_seed_14()
	call_c   build_seed_18()
	call_c   build_seed_10()
	call_c   build_seed_20()
	call_c   build_seed_8()
	call_c   build_seed_23()
	call_c   build_seed_22()
	call_c   build_seed_17()
	call_c   build_seed_35()
	call_c   build_seed_34()
	call_c   build_seed_28()
	call_c   build_seed_27()
	c_ret

long local ref[323]
long local seed[41]

long local _initialization

c_code global initialization_dyalog_seed
	call_c   Dyam_Check_And_Update_Initialization(_initialization)
	fail_c_ret
	call_c   initialization_dyalog_format()
	call_c   build_init_pool()
	call_c   build_viewers()
	call_c   load()
	c_ret

