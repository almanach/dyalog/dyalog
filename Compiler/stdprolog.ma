;; Compiler: DyALog 1.14.0
;; File "stdprolog.pl"


;;------------------ LOADING ------------

c_code local load
	call_c   Dyam_Loading_Set()
	pl_call  fun7(&seed[22],0)
	pl_call  fun7(&seed[19],0)
	pl_call  fun7(&seed[17],0)
	pl_call  fun7(&seed[20],0)
	pl_call  fun7(&seed[18],0)
	pl_call  fun7(&seed[8],0)
	pl_call  fun7(&seed[16],0)
	pl_call  fun7(&seed[6],0)
	pl_call  fun7(&seed[15],0)
	pl_call  fun7(&seed[11],0)
	pl_call  fun7(&seed[12],0)
	pl_call  fun7(&seed[21],0)
	pl_call  fun7(&seed[10],0)
	pl_call  fun7(&seed[9],0)
	pl_call  fun7(&seed[7],0)
	pl_call  fun7(&seed[14],0)
	pl_call  fun7(&seed[13],0)
	pl_call  fun7(&seed[5],0)
	pl_call  fun7(&seed[2],0)
	pl_call  fun7(&seed[0],0)
	pl_call  fun7(&seed[3],0)
	pl_call  fun7(&seed[1],0)
	pl_call  fun7(&seed[4],0)
	call_c   Dyam_Loading_Reset()
	c_ret


;;------------------ VIEWERS ------------

c_code local build_viewers
	call_c   Dyam_Load_Viewer(&ref[9],&ref[8])
	call_c   Dyam_Load_Viewer(&ref[7],&ref[6])
	call_c   Dyam_Load_Viewer(&ref[5],&ref[4])
	call_c   Dyam_Load_Viewer(&ref[3],&ref[1])
	c_ret

;; TERM 1: wrapper_install(_A, _B)
c_code local build_ref_1
	ret_reg &ref[1]
	call_c   build_ref_325()
	call_c   Dyam_Create_Binary(&ref[325],V(0),V(1))
	move_ret ref[1]
	c_ret

;; TERM 325: wrapper_install
c_code local build_ref_325
	ret_reg &ref[325]
	call_c   Dyam_Create_Atom("wrapper_install")
	move_ret ref[325]
	c_ret

;; TERM 3: '*RITEM*'(wrapper_install(_A, _B), voidret)
c_code local build_ref_3
	ret_reg &ref[3]
	call_c   build_ref_0()
	call_c   build_ref_1()
	call_c   build_ref_2()
	call_c   Dyam_Create_Binary(&ref[0],&ref[1],&ref[2])
	move_ret ref[3]
	c_ret

;; TERM 2: voidret
c_code local build_ref_2
	ret_reg &ref[2]
	call_c   Dyam_Create_Atom("voidret")
	move_ret ref[2]
	c_ret

;; TERM 0: '*RITEM*'
c_code local build_ref_0
	ret_reg &ref[0]
	call_c   Dyam_Create_Atom("*RITEM*")
	move_ret ref[0]
	c_ret

;; TERM 4: c_module_name(_A, _B)
c_code local build_ref_4
	ret_reg &ref[4]
	call_c   build_ref_326()
	call_c   Dyam_Create_Binary(&ref[326],V(0),V(1))
	move_ret ref[4]
	c_ret

;; TERM 326: c_module_name
c_code local build_ref_326
	ret_reg &ref[326]
	call_c   Dyam_Create_Atom("c_module_name")
	move_ret ref[326]
	c_ret

;; TERM 5: '*RITEM*'(c_module_name(_A, _B), voidret)
c_code local build_ref_5
	ret_reg &ref[5]
	call_c   build_ref_0()
	call_c   build_ref_4()
	call_c   build_ref_2()
	call_c   Dyam_Create_Binary(&ref[0],&ref[4],&ref[2])
	move_ret ref[5]
	c_ret

;; TERM 6: std_prolog_function(_A, _B, _C)
c_code local build_ref_6
	ret_reg &ref[6]
	call_c   build_ref_327()
	call_c   Dyam_Term_Start(&ref[327],3)
	call_c   Dyam_Term_Arg(V(0))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_End()
	move_ret ref[6]
	c_ret

;; TERM 327: std_prolog_function
c_code local build_ref_327
	ret_reg &ref[327]
	call_c   Dyam_Create_Atom("std_prolog_function")
	move_ret ref[327]
	c_ret

;; TERM 7: '*RITEM*'(std_prolog_function(_A, _B, _C), voidret)
c_code local build_ref_7
	ret_reg &ref[7]
	call_c   build_ref_0()
	call_c   build_ref_6()
	call_c   build_ref_2()
	call_c   Dyam_Create_Binary(&ref[0],&ref[6],&ref[2])
	move_ret ref[7]
	c_ret

;; TERM 8: stdprolog_install(_A)
c_code local build_ref_8
	ret_reg &ref[8]
	call_c   build_ref_328()
	call_c   Dyam_Create_Unary(&ref[328],V(0))
	move_ret ref[8]
	c_ret

;; TERM 328: stdprolog_install
c_code local build_ref_328
	ret_reg &ref[328]
	call_c   Dyam_Create_Atom("stdprolog_install")
	move_ret ref[328]
	c_ret

;; TERM 9: '*RITEM*'(stdprolog_install(_A), voidret)
c_code local build_ref_9
	ret_reg &ref[9]
	call_c   build_ref_0()
	call_c   build_ref_8()
	call_c   build_ref_2()
	call_c   Dyam_Create_Binary(&ref[0],&ref[8],&ref[2])
	move_ret ref[9]
	c_ret

c_code local build_seed_4
	ret_reg &seed[4]
	call_c   build_ref_10()
	call_c   build_ref_14()
	call_c   Dyam_Seed_Start(&ref[10],&ref[14],I(0),fun1,1)
	call_c   build_ref_15()
	call_c   Dyam_Seed_Add_Comp(&ref[15],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[4]
	c_ret

;; TERM 15: '*PROLOG-ITEM*'{top=> unfold(deallocate_layer, _B, _C, deallocate_layer, _B), cont=> _A}
c_code local build_ref_15
	ret_reg &ref[15]
	call_c   build_ref_329()
	call_c   build_ref_12()
	call_c   Dyam_Create_Binary(&ref[329],&ref[12],V(0))
	move_ret ref[15]
	c_ret

;; TERM 12: unfold(deallocate_layer, _B, _C, deallocate_layer, _B)
c_code local build_ref_12
	ret_reg &ref[12]
	call_c   build_ref_330()
	call_c   build_ref_331()
	call_c   Dyam_Term_Start(&ref[330],5)
	call_c   Dyam_Term_Arg(&ref[331])
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(&ref[331])
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_End()
	move_ret ref[12]
	c_ret

;; TERM 331: deallocate_layer
c_code local build_ref_331
	ret_reg &ref[331]
	call_c   Dyam_Create_Atom("deallocate_layer")
	move_ret ref[331]
	c_ret

;; TERM 330: unfold
c_code local build_ref_330
	ret_reg &ref[330]
	call_c   Dyam_Create_Atom("unfold")
	move_ret ref[330]
	c_ret

;; TERM 329: '*PROLOG-ITEM*'!'$ft'
c_code local build_ref_329
	ret_reg &ref[329]
	call_c   build_ref_55()
	call_c   Dyam_Create_Atom_Module("$ft",&ref[55])
	move_ret ref[329]
	c_ret

;; TERM 55: '*PROLOG-ITEM*'
c_code local build_ref_55
	ret_reg &ref[55]
	call_c   Dyam_Create_Atom("*PROLOG-ITEM*")
	move_ret ref[55]
	c_ret

pl_code local fun0
	call_c   build_ref_15()
	call_c   Dyam_Unify_Item(&ref[15])
	fail_ret
	pl_jump  Follow_Cont(V(0))

pl_code local fun1
	pl_ret

;; TERM 14: '*PROLOG-FIRST*'(unfold(deallocate_layer, _B, _C, deallocate_layer, _B)) :> []
c_code local build_ref_14
	ret_reg &ref[14]
	call_c   build_ref_13()
	call_c   Dyam_Create_Binary(I(9),&ref[13],I(0))
	move_ret ref[14]
	c_ret

;; TERM 13: '*PROLOG-FIRST*'(unfold(deallocate_layer, _B, _C, deallocate_layer, _B))
c_code local build_ref_13
	ret_reg &ref[13]
	call_c   build_ref_11()
	call_c   build_ref_12()
	call_c   Dyam_Create_Unary(&ref[11],&ref[12])
	move_ret ref[13]
	c_ret

;; TERM 11: '*PROLOG-FIRST*'
c_code local build_ref_11
	ret_reg &ref[11]
	call_c   Dyam_Create_Atom("*PROLOG-FIRST*")
	move_ret ref[11]
	c_ret

;; TERM 10: '*PROLOG_FIRST*'
c_code local build_ref_10
	ret_reg &ref[10]
	call_c   Dyam_Create_Atom("*PROLOG_FIRST*")
	move_ret ref[10]
	c_ret

c_code local build_seed_1
	ret_reg &seed[1]
	call_c   build_ref_16()
	call_c   build_ref_20()
	call_c   Dyam_Seed_Start(&ref[16],&ref[20],I(0),fun1,1)
	call_c   build_ref_19()
	call_c   Dyam_Seed_Add_Comp(&ref[19],fun2,0)
	call_c   Dyam_Seed_End()
	move_ret seed[1]
	c_ret

;; TERM 19: '*GUARD*'(std_prolog_load_args([], _B, _C, _C, _D))
c_code local build_ref_19
	ret_reg &ref[19]
	call_c   build_ref_17()
	call_c   build_ref_18()
	call_c   Dyam_Create_Unary(&ref[17],&ref[18])
	move_ret ref[19]
	c_ret

;; TERM 18: std_prolog_load_args([], _B, _C, _C, _D)
c_code local build_ref_18
	ret_reg &ref[18]
	call_c   build_ref_332()
	call_c   Dyam_Term_Start(&ref[332],5)
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[18]
	c_ret

;; TERM 332: std_prolog_load_args
c_code local build_ref_332
	ret_reg &ref[332]
	call_c   Dyam_Create_Atom("std_prolog_load_args")
	move_ret ref[332]
	c_ret

;; TERM 17: '*GUARD*'
c_code local build_ref_17
	ret_reg &ref[17]
	call_c   Dyam_Create_Atom("*GUARD*")
	move_ret ref[17]
	c_ret

pl_code local fun2
	call_c   build_ref_19()
	call_c   Dyam_Unify_Item(&ref[19])
	fail_ret
	pl_ret

;; TERM 20: '*GUARD*'(std_prolog_load_args([], _B, _C, _C, _D)) :> []
c_code local build_ref_20
	ret_reg &ref[20]
	call_c   build_ref_19()
	call_c   Dyam_Create_Binary(I(9),&ref[19],I(0))
	move_ret ref[20]
	c_ret

;; TERM 16: '*GUARD_CLAUSE*'
c_code local build_ref_16
	ret_reg &ref[16]
	call_c   Dyam_Create_Atom("*GUARD_CLAUSE*")
	move_ret ref[16]
	c_ret

c_code local build_seed_3
	ret_reg &seed[3]
	call_c   build_ref_16()
	call_c   build_ref_23()
	call_c   Dyam_Seed_Start(&ref[16],&ref[23],I(0),fun1,1)
	call_c   build_ref_22()
	call_c   Dyam_Seed_Add_Comp(&ref[22],fun3,0)
	call_c   Dyam_Seed_End()
	move_ret seed[3]
	c_ret

;; TERM 22: '*GUARD*'(std_prolog_unif_args([], _B, _C, _C, _D))
c_code local build_ref_22
	ret_reg &ref[22]
	call_c   build_ref_17()
	call_c   build_ref_21()
	call_c   Dyam_Create_Unary(&ref[17],&ref[21])
	move_ret ref[22]
	c_ret

;; TERM 21: std_prolog_unif_args([], _B, _C, _C, _D)
c_code local build_ref_21
	ret_reg &ref[21]
	call_c   build_ref_333()
	call_c   Dyam_Term_Start(&ref[333],5)
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[21]
	c_ret

;; TERM 333: std_prolog_unif_args
c_code local build_ref_333
	ret_reg &ref[333]
	call_c   Dyam_Create_Atom("std_prolog_unif_args")
	move_ret ref[333]
	c_ret

pl_code local fun3
	call_c   build_ref_22()
	call_c   Dyam_Unify_Item(&ref[22])
	fail_ret
	pl_ret

;; TERM 23: '*GUARD*'(std_prolog_unif_args([], _B, _C, _C, _D)) :> []
c_code local build_ref_23
	ret_reg &ref[23]
	call_c   build_ref_22()
	call_c   Dyam_Create_Binary(I(9),&ref[22],I(0))
	move_ret ref[23]
	c_ret

c_code local build_seed_0
	ret_reg &seed[0]
	call_c   build_ref_16()
	call_c   build_ref_26()
	call_c   Dyam_Seed_Start(&ref[16],&ref[26],I(0),fun1,1)
	call_c   build_ref_25()
	call_c   Dyam_Seed_Add_Comp(&ref[25],fun4,0)
	call_c   Dyam_Seed_End()
	move_ret seed[0]
	c_ret

;; TERM 25: '*GUARD*'(std_prolog_load_args_alt([], _B, _C, _C, _D, _E))
c_code local build_ref_25
	ret_reg &ref[25]
	call_c   build_ref_17()
	call_c   build_ref_24()
	call_c   Dyam_Create_Unary(&ref[17],&ref[24])
	move_ret ref[25]
	c_ret

;; TERM 24: std_prolog_load_args_alt([], _B, _C, _C, _D, _E)
c_code local build_ref_24
	ret_reg &ref[24]
	call_c   build_ref_334()
	call_c   Dyam_Term_Start(&ref[334],6)
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[24]
	c_ret

;; TERM 334: std_prolog_load_args_alt
c_code local build_ref_334
	ret_reg &ref[334]
	call_c   Dyam_Create_Atom("std_prolog_load_args_alt")
	move_ret ref[334]
	c_ret

pl_code local fun4
	call_c   build_ref_25()
	call_c   Dyam_Unify_Item(&ref[25])
	fail_ret
	pl_ret

;; TERM 26: '*GUARD*'(std_prolog_load_args_alt([], _B, _C, _C, _D, _E)) :> []
c_code local build_ref_26
	ret_reg &ref[26]
	call_c   build_ref_25()
	call_c   Dyam_Create_Binary(I(9),&ref[25],I(0))
	move_ret ref[26]
	c_ret

c_code local build_seed_2
	ret_reg &seed[2]
	call_c   build_ref_16()
	call_c   build_ref_29()
	call_c   Dyam_Seed_Start(&ref[16],&ref[29],I(0),fun1,1)
	call_c   build_ref_28()
	call_c   Dyam_Seed_Add_Comp(&ref[28],fun5,0)
	call_c   Dyam_Seed_End()
	move_ret seed[2]
	c_ret

;; TERM 28: '*GUARD*'(std_prolog_unif_args_alt([], _B, _C, _C, _D, _E))
c_code local build_ref_28
	ret_reg &ref[28]
	call_c   build_ref_17()
	call_c   build_ref_27()
	call_c   Dyam_Create_Unary(&ref[17],&ref[27])
	move_ret ref[28]
	c_ret

;; TERM 27: std_prolog_unif_args_alt([], _B, _C, _C, _D, _E)
c_code local build_ref_27
	ret_reg &ref[27]
	call_c   build_ref_335()
	call_c   Dyam_Term_Start(&ref[335],6)
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[27]
	c_ret

;; TERM 335: std_prolog_unif_args_alt
c_code local build_ref_335
	ret_reg &ref[335]
	call_c   Dyam_Create_Atom("std_prolog_unif_args_alt")
	move_ret ref[335]
	c_ret

pl_code local fun5
	call_c   build_ref_28()
	call_c   Dyam_Unify_Item(&ref[28])
	fail_ret
	pl_ret

;; TERM 29: '*GUARD*'(std_prolog_unif_args_alt([], _B, _C, _C, _D, _E)) :> []
c_code local build_ref_29
	ret_reg &ref[29]
	call_c   build_ref_28()
	call_c   Dyam_Create_Binary(I(9),&ref[28],I(0))
	move_ret ref[29]
	c_ret

c_code local build_seed_5
	ret_reg &seed[5]
	call_c   build_ref_16()
	call_c   build_ref_32()
	call_c   Dyam_Seed_Start(&ref[16],&ref[32],I(0),fun1,1)
	call_c   build_ref_31()
	call_c   Dyam_Seed_Add_Comp(&ref[31],fun6,0)
	call_c   Dyam_Seed_End()
	move_ret seed[5]
	c_ret

;; TERM 31: '*GUARD*'(directive(dyalog_ender(_B), _C, _D, _E))
c_code local build_ref_31
	ret_reg &ref[31]
	call_c   build_ref_17()
	call_c   build_ref_30()
	call_c   Dyam_Create_Unary(&ref[17],&ref[30])
	move_ret ref[31]
	c_ret

;; TERM 30: directive(dyalog_ender(_B), _C, _D, _E)
c_code local build_ref_30
	ret_reg &ref[30]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_337()
	call_c   Dyam_Create_Unary(&ref[337],V(1))
	move_ret R(0)
	call_c   build_ref_336()
	call_c   Dyam_Term_Start(&ref[336],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[30]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 336: directive
c_code local build_ref_336
	ret_reg &ref[336]
	call_c   Dyam_Create_Atom("directive")
	move_ret ref[336]
	c_ret

;; TERM 337: dyalog_ender
c_code local build_ref_337
	ret_reg &ref[337]
	call_c   Dyam_Create_Atom("dyalog_ender")
	move_ret ref[337]
	c_ret

;; TERM 33: dyalog_ender(_F)
c_code local build_ref_33
	ret_reg &ref[33]
	call_c   build_ref_337()
	call_c   Dyam_Create_Unary(&ref[337],V(5))
	move_ret ref[33]
	c_ret

;; TERM 34: dyalog_ender((_B / 0))
c_code local build_ref_34
	ret_reg &ref[34]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_338()
	call_c   Dyam_Create_Binary(&ref[338],V(1),N(0))
	move_ret R(0)
	call_c   build_ref_337()
	call_c   Dyam_Create_Unary(&ref[337],R(0))
	move_ret ref[34]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 338: /
c_code local build_ref_338
	ret_reg &ref[338]
	call_c   Dyam_Create_Atom("/")
	move_ret ref[338]
	c_ret

long local pool_fun6[4]=[3,build_ref_31,build_ref_33,build_ref_34]

pl_code local fun6
	call_c   Dyam_Pool(pool_fun6)
	call_c   Dyam_Unify_Item(&ref[31])
	fail_ret
	call_c   DYAM_evpred_retract(&ref[33])
	fail_ret
	call_c   DYAM_evpred_assert_1(&ref[34])
	pl_ret

;; TERM 32: '*GUARD*'(directive(dyalog_ender(_B), _C, _D, _E)) :> []
c_code local build_ref_32
	ret_reg &ref[32]
	call_c   build_ref_31()
	call_c   Dyam_Create_Binary(I(9),&ref[31],I(0))
	move_ret ref[32]
	c_ret

c_code local build_seed_13
	ret_reg &seed[13]
	call_c   build_ref_35()
	call_c   build_ref_38()
	call_c   Dyam_Seed_Start(&ref[35],&ref[38],I(0),fun17,1)
	call_c   build_ref_40()
	call_c   Dyam_Seed_Add_Comp(&ref[40],fun52,0)
	call_c   Dyam_Seed_End()
	move_ret seed[13]
	c_ret

;; TERM 40: '*CITEM*'(wrapper_install(_B, _C), _A)
c_code local build_ref_40
	ret_reg &ref[40]
	call_c   build_ref_39()
	call_c   build_ref_36()
	call_c   Dyam_Create_Binary(&ref[39],&ref[36],V(0))
	move_ret ref[40]
	c_ret

;; TERM 36: wrapper_install(_B, _C)
c_code local build_ref_36
	ret_reg &ref[36]
	call_c   build_ref_325()
	call_c   Dyam_Create_Binary(&ref[325],V(1),V(2))
	move_ret ref[36]
	c_ret

;; TERM 39: '*CITEM*'
c_code local build_ref_39
	ret_reg &ref[39]
	call_c   Dyam_Create_Atom("*CITEM*")
	move_ret ref[39]
	c_ret

;; TERM 138: 'no tabular wrapper for ~w'
c_code local build_ref_138
	ret_reg &ref[138]
	call_c   Dyam_Create_Atom("no tabular wrapper for ~w")
	move_ret ref[138]
	c_ret

;; TERM 139: [_B]
c_code local build_ref_139
	ret_reg &ref[139]
	call_c   Dyam_Create_List(V(1),I(0))
	move_ret ref[139]
	c_ret

;; TERM 42: callret(_G, _H)
c_code local build_ref_42
	ret_reg &ref[42]
	call_c   build_ref_339()
	call_c   Dyam_Create_Binary(&ref[339],V(6),V(7))
	move_ret ref[42]
	c_ret

;; TERM 339: callret
c_code local build_ref_339
	ret_reg &ref[339]
	call_c   Dyam_Create_Atom("callret")
	move_ret ref[339]
	c_ret

;; TERM 43: [_D|_G]
c_code local build_ref_43
	ret_reg &ref[43]
	call_c   Dyam_Create_List(V(3),V(6))
	move_ret ref[43]
	c_ret

c_code local build_seed_25
	ret_reg &seed[25]
	call_c   build_ref_55()
	call_c   build_ref_56()
	call_c   Dyam_Seed_Start(&ref[55],&ref[56],I(0),fun8,1)
	call_c   build_ref_59()
	call_c   Dyam_Seed_Add_Comp(&ref[59],&ref[56],0)
	call_c   Dyam_Seed_End()
	move_ret seed[25]
	c_ret

pl_code local fun8
	pl_jump  Complete(0,0)

;; TERM 59: '*PROLOG-FIRST*'(unfold((_F :> deallocate_layer :> deallocate :> succeed), _D, _I, _K, _L)) :> '$$HOLE$$'
c_code local build_ref_59
	ret_reg &ref[59]
	call_c   build_ref_58()
	call_c   Dyam_Create_Binary(I(9),&ref[58],I(7))
	move_ret ref[59]
	c_ret

;; TERM 58: '*PROLOG-FIRST*'(unfold((_F :> deallocate_layer :> deallocate :> succeed), _D, _I, _K, _L))
c_code local build_ref_58
	ret_reg &ref[58]
	call_c   build_ref_11()
	call_c   build_ref_57()
	call_c   Dyam_Create_Unary(&ref[11],&ref[57])
	move_ret ref[58]
	c_ret

;; TERM 57: unfold((_F :> deallocate_layer :> deallocate :> succeed), _D, _I, _K, _L)
c_code local build_ref_57
	ret_reg &ref[57]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_340()
	call_c   build_ref_341()
	call_c   Dyam_Create_Binary(I(9),&ref[340],&ref[341])
	move_ret R(0)
	call_c   build_ref_331()
	call_c   Dyam_Create_Binary(I(9),&ref[331],R(0))
	move_ret R(0)
	call_c   Dyam_Create_Binary(I(9),V(5),R(0))
	move_ret R(0)
	call_c   build_ref_330()
	call_c   Dyam_Term_Start(&ref[330],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_End()
	move_ret ref[57]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 341: succeed
c_code local build_ref_341
	ret_reg &ref[341]
	call_c   Dyam_Create_Atom("succeed")
	move_ret ref[341]
	c_ret

;; TERM 340: deallocate
c_code local build_ref_340
	ret_reg &ref[340]
	call_c   Dyam_Create_Atom("deallocate")
	move_ret ref[340]
	c_ret

;; TERM 56: '*PROLOG-ITEM*'{top=> unfold((_F :> deallocate_layer :> deallocate :> succeed), _D, _I, _K, _L), cont=> '$CLOSURE'('$fun'(14, 0, 1133192636), '$TUPPLE'(35082884350344))}
c_code local build_ref_56
	ret_reg &ref[56]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,508297216)
	move_ret R(0)
	call_c   Dyam_Closure_Aux(fun14,R(0))
	move_ret R(0)
	call_c   build_ref_329()
	call_c   build_ref_57()
	call_c   Dyam_Create_Binary(&ref[329],&ref[57],R(0))
	move_ret ref[56]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_23
	ret_reg &seed[23]
	call_c   build_ref_17()
	call_c   build_ref_45()
	call_c   Dyam_Seed_Start(&ref[17],&ref[45],I(0),fun8,1)
	call_c   build_ref_46()
	call_c   Dyam_Seed_Add_Comp(&ref[46],&ref[45],0)
	call_c   Dyam_Seed_End()
	move_ret seed[23]
	c_ret

;; TERM 46: '*GUARD*'(std_prolog_unif_args([_D|_G], 0, _M, _K, _J)) :> '$$HOLE$$'
c_code local build_ref_46
	ret_reg &ref[46]
	call_c   build_ref_45()
	call_c   Dyam_Create_Binary(I(9),&ref[45],I(7))
	move_ret ref[46]
	c_ret

;; TERM 45: '*GUARD*'(std_prolog_unif_args([_D|_G], 0, _M, _K, _J))
c_code local build_ref_45
	ret_reg &ref[45]
	call_c   build_ref_17()
	call_c   build_ref_44()
	call_c   Dyam_Create_Unary(&ref[17],&ref[44])
	move_ret ref[45]
	c_ret

;; TERM 44: std_prolog_unif_args([_D|_G], 0, _M, _K, _J)
c_code local build_ref_44
	ret_reg &ref[44]
	call_c   build_ref_333()
	call_c   build_ref_43()
	call_c   Dyam_Term_Start(&ref[333],5)
	call_c   Dyam_Term_Arg(&ref[43])
	call_c   Dyam_Term_Arg(N(0))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret ref[44]
	c_ret

pl_code local fun9
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Light_Object(R(0))
	pl_jump  Schedule_Prolog()

;; TERM 50: allocate :> allocate_layer :> _M
c_code local build_ref_50
	ret_reg &ref[50]
	call_c   build_ref_47()
	call_c   build_ref_49()
	call_c   Dyam_Create_Binary(I(9),&ref[47],&ref[49])
	move_ret ref[50]
	c_ret

;; TERM 49: allocate_layer :> _M
c_code local build_ref_49
	ret_reg &ref[49]
	call_c   build_ref_48()
	call_c   Dyam_Create_Binary(I(9),&ref[48],V(12))
	move_ret ref[49]
	c_ret

;; TERM 48: allocate_layer
c_code local build_ref_48
	ret_reg &ref[48]
	call_c   Dyam_Create_Atom("allocate_layer")
	move_ret ref[48]
	c_ret

;; TERM 47: allocate
c_code local build_ref_47
	ret_reg &ref[47]
	call_c   Dyam_Create_Atom("allocate")
	move_ret ref[47]
	c_ret

c_code local build_seed_24
	ret_reg &seed[24]
	call_c   build_ref_0()
	call_c   build_ref_53()
	call_c   Dyam_Seed_Start(&ref[0],&ref[53],&ref[53],fun8,1)
	call_c   build_ref_54()
	call_c   Dyam_Seed_Add_Comp(&ref[54],&ref[53],0)
	call_c   Dyam_Seed_End()
	move_ret seed[24]
	c_ret

;; TERM 54: '*RITEM*'(_A, voidret) :> '$$HOLE$$'
c_code local build_ref_54
	ret_reg &ref[54]
	call_c   build_ref_53()
	call_c   Dyam_Create_Binary(I(9),&ref[53],I(7))
	move_ret ref[54]
	c_ret

;; TERM 53: '*RITEM*'(_A, voidret)
c_code local build_ref_53
	ret_reg &ref[53]
	call_c   build_ref_0()
	call_c   build_ref_2()
	call_c   Dyam_Create_Binary(&ref[0],V(0),&ref[2])
	move_ret ref[53]
	c_ret

pl_code local fun11
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Choice(fun10)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Subsume(R(0))
	fail_ret
	call_c   Dyam_Cut()
	pl_ret

long local pool_fun12[2]=[1,build_seed_24]

pl_code local fun13
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(14),V(2))
	fail_ret
fun12:
	call_c   Dyam_Deallocate()
	pl_jump  fun11(&seed[24],2)


;; TERM 51: wrapper_function_name(_B, _C)
c_code local build_ref_51
	ret_reg &ref[51]
	call_c   build_ref_342()
	call_c   Dyam_Create_Binary(&ref[342],V(1),V(2))
	move_ret ref[51]
	c_ret

;; TERM 342: wrapper_function_name
c_code local build_ref_342
	ret_reg &ref[342]
	call_c   Dyam_Create_Atom("wrapper_function_name")
	move_ret ref[342]
	c_ret

;; TERM 52: named_function(_O, _C, local)
c_code local build_ref_52
	ret_reg &ref[52]
	call_c   build_ref_343()
	call_c   build_ref_344()
	call_c   Dyam_Term_Start(&ref[343],3)
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(&ref[344])
	call_c   Dyam_Term_End()
	move_ret ref[52]
	c_ret

;; TERM 344: local
c_code local build_ref_344
	ret_reg &ref[344]
	call_c   Dyam_Create_Atom("local")
	move_ret ref[344]
	c_ret

;; TERM 343: named_function
c_code local build_ref_343
	ret_reg &ref[343]
	call_c   Dyam_Create_Atom("named_function")
	move_ret ref[343]
	c_ret

long local pool_fun14[7]=[131076,build_seed_23,build_ref_50,build_ref_51,build_ref_52,pool_fun12,pool_fun12]

pl_code local fun14
	call_c   Dyam_Pool(pool_fun14)
	call_c   Dyam_Allocate(0)
	pl_call  fun9(&seed[23],1)
	call_c   Dyam_Unify(V(13),&ref[50])
	fail_ret
	call_c   Dyam_Reg_Load(0,V(13))
	move     V(14), R(2)
	move     S(5), R(3)
	pl_call  pred_label_code_2()
	call_c   Dyam_Reg_Load(0,V(14))
	pl_call  pred_register_init_pool_fun_1()
	call_c   Dyam_Choice(fun13)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[51])
	call_c   Dyam_Cut()
	call_c   DYAM_evpred_assert_1(&ref[52])
	pl_jump  fun12()

long local pool_fun50[4]=[3,build_ref_42,build_ref_43,build_seed_25]

long local pool_fun51[4]=[65538,build_ref_138,build_ref_139,pool_fun50]

pl_code local fun51
	call_c   Dyam_Remove_Choice()
	move     &ref[138], R(0)
	move     0, R(1)
	move     &ref[139], R(2)
	move     S(5), R(3)
	pl_call  pred_internal_error_2()
fun50:
	call_c   Dyam_Unify(V(4),&ref[42])
	fail_ret
	move     &ref[43], R(0)
	move     S(5), R(1)
	move     V(8), R(2)
	move     S(5), R(3)
	pl_call  pred_tupple_2()
	move     V(9), R(0)
	move     S(5), R(1)
	pl_call  pred_null_tupple_1()
	call_c   Dyam_Deallocate()
	pl_jump  fun9(&seed[25],12)


;; TERM 41: '*WRAPPER*'(_B, [_D|_E], _F)
c_code local build_ref_41
	ret_reg &ref[41]
	call_c   build_ref_345()
	call_c   build_ref_60()
	call_c   Dyam_Term_Start(&ref[345],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(&ref[60])
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[41]
	c_ret

;; TERM 60: [_D|_E]
c_code local build_ref_60
	ret_reg &ref[60]
	call_c   Dyam_Create_List(V(3),V(4))
	move_ret ref[60]
	c_ret

;; TERM 345: '*WRAPPER*'
c_code local build_ref_345
	ret_reg &ref[345]
	call_c   Dyam_Create_Atom("*WRAPPER*")
	move_ret ref[345]
	c_ret

long local pool_fun52[5]=[131074,build_ref_40,build_ref_41,pool_fun51,pool_fun50]

pl_code local fun52
	call_c   Dyam_Pool(pool_fun52)
	call_c   Dyam_Unify_Item(&ref[40])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun51)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[41])
	call_c   Dyam_Cut()
	pl_jump  fun50()

pl_code local fun17
	pl_jump  Apply(0,0)

;; TERM 38: '*FIRST*'(wrapper_install(_B, _C)) :> []
c_code local build_ref_38
	ret_reg &ref[38]
	call_c   build_ref_37()
	call_c   Dyam_Create_Binary(I(9),&ref[37],I(0))
	move_ret ref[38]
	c_ret

;; TERM 37: '*FIRST*'(wrapper_install(_B, _C))
c_code local build_ref_37
	ret_reg &ref[37]
	call_c   build_ref_35()
	call_c   build_ref_36()
	call_c   Dyam_Create_Unary(&ref[35],&ref[36])
	move_ret ref[37]
	c_ret

;; TERM 35: '*FIRST*'
c_code local build_ref_35
	ret_reg &ref[35]
	call_c   Dyam_Create_Atom("*FIRST*")
	move_ret ref[35]
	c_ret

c_code local build_seed_14
	ret_reg &seed[14]
	call_c   build_ref_35()
	call_c   build_ref_38()
	call_c   Dyam_Seed_Start(&ref[35],&ref[38],I(0),fun17,1)
	call_c   build_ref_40()
	call_c   Dyam_Seed_Add_Comp(&ref[40],fun57,0)
	call_c   Dyam_Seed_End()
	move_ret seed[14]
	c_ret

c_code local build_seed_27
	ret_reg &seed[27]
	call_c   build_ref_55()
	call_c   build_ref_64()
	call_c   Dyam_Seed_Start(&ref[55],&ref[64],I(0),fun8,1)
	call_c   build_ref_59()
	call_c   Dyam_Seed_Add_Comp(&ref[59],&ref[64],0)
	call_c   Dyam_Seed_End()
	move_ret seed[27]
	c_ret

;; TERM 64: '*PROLOG-ITEM*'{top=> unfold((_F :> deallocate_layer :> deallocate :> succeed), _D, _I, _K, _L), cont=> '$CLOSURE'('$fun'(15, 0, 1133218828), '$TUPPLE'(35082884350516))}
c_code local build_ref_64
	ret_reg &ref[64]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,520880128)
	move_ret R(0)
	call_c   Dyam_Closure_Aux(fun15,R(0))
	move_ret R(0)
	call_c   build_ref_329()
	call_c   build_ref_57()
	call_c   Dyam_Create_Binary(&ref[329],&ref[57],R(0))
	move_ret ref[64]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_26
	ret_reg &seed[26]
	call_c   build_ref_17()
	call_c   build_ref_62()
	call_c   Dyam_Seed_Start(&ref[17],&ref[62],I(0),fun8,1)
	call_c   build_ref_63()
	call_c   Dyam_Seed_Add_Comp(&ref[63],&ref[62],0)
	call_c   Dyam_Seed_End()
	move_ret seed[26]
	c_ret

;; TERM 63: '*GUARD*'(std_prolog_unif_args([_D|_E], 0, _M, _K, _J)) :> '$$HOLE$$'
c_code local build_ref_63
	ret_reg &ref[63]
	call_c   build_ref_62()
	call_c   Dyam_Create_Binary(I(9),&ref[62],I(7))
	move_ret ref[63]
	c_ret

;; TERM 62: '*GUARD*'(std_prolog_unif_args([_D|_E], 0, _M, _K, _J))
c_code local build_ref_62
	ret_reg &ref[62]
	call_c   build_ref_17()
	call_c   build_ref_61()
	call_c   Dyam_Create_Unary(&ref[17],&ref[61])
	move_ret ref[62]
	c_ret

;; TERM 61: std_prolog_unif_args([_D|_E], 0, _M, _K, _J)
c_code local build_ref_61
	ret_reg &ref[61]
	call_c   build_ref_333()
	call_c   build_ref_60()
	call_c   Dyam_Term_Start(&ref[333],5)
	call_c   Dyam_Term_Arg(&ref[60])
	call_c   Dyam_Term_Arg(N(0))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret ref[61]
	c_ret

long local pool_fun15[7]=[131076,build_seed_26,build_ref_50,build_ref_51,build_ref_52,pool_fun12,pool_fun12]

pl_code local fun15
	call_c   Dyam_Pool(pool_fun15)
	call_c   Dyam_Allocate(0)
	pl_call  fun9(&seed[26],1)
	call_c   Dyam_Unify(V(13),&ref[50])
	fail_ret
	call_c   Dyam_Reg_Load(0,V(13))
	move     V(14), R(2)
	move     S(5), R(3)
	pl_call  pred_label_code_2()
	call_c   Dyam_Reg_Load(0,V(14))
	pl_call  pred_register_init_pool_fun_1()
	call_c   Dyam_Choice(fun13)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[51])
	call_c   Dyam_Cut()
	call_c   DYAM_evpred_assert_1(&ref[52])
	pl_jump  fun12()

long local pool_fun53[3]=[2,build_ref_60,build_seed_27]

pl_code local fun54
	call_c   Dyam_Remove_Choice()
fun53:
	move     &ref[60], R(0)
	move     S(5), R(1)
	move     V(8), R(2)
	move     S(5), R(3)
	pl_call  pred_tupple_2()
	move     V(9), R(0)
	move     S(5), R(1)
	pl_call  pred_null_tupple_1()
	call_c   Dyam_Deallocate()
	pl_jump  fun9(&seed[27],12)


long local pool_fun55[3]=[65537,build_ref_42,pool_fun53]

long local pool_fun56[4]=[65538,build_ref_138,build_ref_139,pool_fun55]

pl_code local fun56
	call_c   Dyam_Remove_Choice()
	move     &ref[138], R(0)
	move     0, R(1)
	move     &ref[139], R(2)
	move     S(5), R(3)
	pl_call  pred_internal_error_2()
fun55:
	call_c   Dyam_Choice(fun54)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(4),&ref[42])
	fail_ret
	call_c   Dyam_Cut()
	pl_fail


long local pool_fun57[5]=[131074,build_ref_40,build_ref_41,pool_fun56,pool_fun55]

pl_code local fun57
	call_c   Dyam_Pool(pool_fun57)
	call_c   Dyam_Unify_Item(&ref[40])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun56)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[41])
	call_c   Dyam_Cut()
	pl_jump  fun55()

c_code local build_seed_7
	ret_reg &seed[7]
	call_c   build_ref_10()
	call_c   build_ref_67()
	call_c   Dyam_Seed_Start(&ref[10],&ref[67],I(0),fun1,1)
	call_c   build_ref_68()
	call_c   Dyam_Seed_Add_Comp(&ref[68],fun35,0)
	call_c   Dyam_Seed_End()
	move_ret seed[7]
	c_ret

;; TERM 68: '*PROLOG-ITEM*'{top=> unfold('*WRAPPER-INNER-CALL*'(_B, _C, _D), _E, _F, _G, _H), cont=> _A}
c_code local build_ref_68
	ret_reg &ref[68]
	call_c   build_ref_329()
	call_c   build_ref_65()
	call_c   Dyam_Create_Binary(&ref[329],&ref[65],V(0))
	move_ret ref[68]
	c_ret

;; TERM 65: unfold('*WRAPPER-INNER-CALL*'(_B, _C, _D), _E, _F, _G, _H)
c_code local build_ref_65
	ret_reg &ref[65]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_346()
	call_c   Dyam_Term_Start(&ref[346],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_330()
	call_c   Dyam_Term_Start(&ref[330],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[65]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 346: '*WRAPPER-INNER-CALL*'
c_code local build_ref_346
	ret_reg &ref[346]
	call_c   Dyam_Create_Atom("*WRAPPER-INNER-CALL*")
	move_ret ref[346]
	c_ret

;; TERM 124: wrapper
c_code local build_ref_124
	ret_reg &ref[124]
	call_c   Dyam_Create_Atom("wrapper")
	move_ret ref[124]
	c_ret

;; TERM 125: '_looping_wrapper~w'
c_code local build_ref_125
	ret_reg &ref[125]
	call_c   Dyam_Create_Atom("_looping_wrapper~w")
	move_ret ref[125]
	c_ret

;; TERM 126: [_L]
c_code local build_ref_126
	ret_reg &ref[126]
	call_c   Dyam_Create_List(V(11),I(0))
	move_ret ref[126]
	c_ret

;; TERM 70: wrapper_function_name(_B, _K)
c_code local build_ref_70
	ret_reg &ref[70]
	call_c   build_ref_342()
	call_c   Dyam_Create_Binary(&ref[342],V(1),V(10))
	move_ret ref[70]
	c_ret

c_code local build_seed_36
	ret_reg &seed[36]
	call_c   build_ref_55()
	call_c   build_ref_120()
	call_c   Dyam_Seed_Start(&ref[55],&ref[120],I(0),fun8,1)
	call_c   build_ref_123()
	call_c   Dyam_Seed_Add_Comp(&ref[123],&ref[120],0)
	call_c   Dyam_Seed_End()
	move_ret seed[36]
	c_ret

;; TERM 123: '*PROLOG-FIRST*'(build_closure(_D, _E, _M, _N, _H)) :> '$$HOLE$$'
c_code local build_ref_123
	ret_reg &ref[123]
	call_c   build_ref_122()
	call_c   Dyam_Create_Binary(I(9),&ref[122],I(7))
	move_ret ref[123]
	c_ret

;; TERM 122: '*PROLOG-FIRST*'(build_closure(_D, _E, _M, _N, _H))
c_code local build_ref_122
	ret_reg &ref[122]
	call_c   build_ref_11()
	call_c   build_ref_121()
	call_c   Dyam_Create_Unary(&ref[11],&ref[121])
	move_ret ref[122]
	c_ret

;; TERM 121: build_closure(_D, _E, _M, _N, _H)
c_code local build_ref_121
	ret_reg &ref[121]
	call_c   build_ref_347()
	call_c   Dyam_Term_Start(&ref[347],5)
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[121]
	c_ret

;; TERM 347: build_closure
c_code local build_ref_347
	ret_reg &ref[347]
	call_c   Dyam_Create_Atom("build_closure")
	move_ret ref[347]
	c_ret

;; TERM 120: '*PROLOG-ITEM*'{top=> build_closure(_D, _E, _M, _N, _H), cont=> '$CLOSURE'('$fun'(29, 0, 1133390748), '$TUPPLE'(35082884089152))}
c_code local build_ref_120
	ret_reg &ref[120]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,365199360)
	move_ret R(0)
	call_c   Dyam_Closure_Aux(fun29,R(0))
	move_ret R(0)
	call_c   build_ref_329()
	call_c   build_ref_121()
	call_c   Dyam_Create_Binary(&ref[329],&ref[121],R(0))
	move_ret ref[120]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 72: [_E,_N|_C]
c_code local build_ref_72
	ret_reg &ref[72]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(13),V(2))
	move_ret R(0)
	call_c   Dyam_Create_List(V(4),R(0))
	move_ret ref[72]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 73: _P + 1
c_code local build_ref_73
	ret_reg &ref[73]
	call_c   build_ref_348()
	call_c   Dyam_Create_Binary(&ref[348],V(15),N(1))
	move_ret ref[73]
	c_ret

;; TERM 348: +
c_code local build_ref_348
	ret_reg &ref[348]
	call_c   Dyam_Create_Atom("+")
	move_ret ref[348]
	c_ret

c_code local build_seed_28
	ret_reg &seed[28]
	call_c   build_ref_17()
	call_c   build_ref_75()
	call_c   Dyam_Seed_Start(&ref[17],&ref[75],I(0),fun8,1)
	call_c   build_ref_76()
	call_c   Dyam_Seed_Add_Comp(&ref[76],&ref[75],0)
	call_c   Dyam_Seed_End()
	move_ret seed[28]
	c_ret

;; TERM 76: '*GUARD*'(std_prolog_load_args_alt([_N|_C], 0, _G, (call(_K, []) :> reg_reset(_Q) :> noop), _F, _O)) :> '$$HOLE$$'
c_code local build_ref_76
	ret_reg &ref[76]
	call_c   build_ref_75()
	call_c   Dyam_Create_Binary(I(9),&ref[75],I(7))
	move_ret ref[76]
	c_ret

;; TERM 75: '*GUARD*'(std_prolog_load_args_alt([_N|_C], 0, _G, (call(_K, []) :> reg_reset(_Q) :> noop), _F, _O))
c_code local build_ref_75
	ret_reg &ref[75]
	call_c   build_ref_17()
	call_c   build_ref_74()
	call_c   Dyam_Create_Unary(&ref[17],&ref[74])
	move_ret ref[75]
	c_ret

;; TERM 74: std_prolog_load_args_alt([_N|_C], 0, _G, (call(_K, []) :> reg_reset(_Q) :> noop), _F, _O)
c_code local build_ref_74
	ret_reg &ref[74]
	call_c   Dyam_Pseudo_Choice(3)
	call_c   Dyam_Create_List(V(13),V(2))
	move_ret R(0)
	call_c   build_ref_349()
	call_c   Dyam_Create_Binary(&ref[349],V(10),I(0))
	move_ret R(1)
	call_c   build_ref_350()
	call_c   Dyam_Create_Unary(&ref[350],V(16))
	move_ret R(2)
	call_c   build_ref_302()
	call_c   Dyam_Create_Binary(I(9),R(2),&ref[302])
	move_ret R(2)
	call_c   Dyam_Create_Binary(I(9),R(1),R(2))
	move_ret R(2)
	call_c   build_ref_334()
	call_c   Dyam_Term_Start(&ref[334],6)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(N(0))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(R(2))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_End()
	move_ret ref[74]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 302: noop
c_code local build_ref_302
	ret_reg &ref[302]
	call_c   Dyam_Create_Atom("noop")
	move_ret ref[302]
	c_ret

;; TERM 350: reg_reset
c_code local build_ref_350
	ret_reg &ref[350]
	call_c   Dyam_Create_Atom("reg_reset")
	move_ret ref[350]
	c_ret

;; TERM 349: call
c_code local build_ref_349
	ret_reg &ref[349]
	call_c   Dyam_Create_Atom("call")
	move_ret ref[349]
	c_ret

long local pool_fun28[4]=[3,build_ref_72,build_ref_73,build_seed_28]

pl_code local fun29
	call_c   Dyam_Pool(pool_fun28)
	call_c   Dyam_Allocate(0)
fun28:
	move     &ref[72], R(0)
	move     S(5), R(1)
	move     V(14), R(2)
	move     S(5), R(3)
	pl_call  pred_duplicate_vars_2()
	call_c   DYAM_evpred_length(V(2),V(15))
	fail_ret
	call_c   DYAM_evpred_is(V(16),&ref[73])
	fail_ret
	pl_call  fun9(&seed[28],1)
	call_c   Dyam_Deallocate()
	pl_jump  Follow_Cont(V(0))


long local pool_fun30[2]=[1,build_seed_36]

pl_code local fun30
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Deallocate()
	pl_jump  fun9(&seed[36],12)

;; TERM 71: '*PROLOG-LAST*'
c_code local build_ref_71
	ret_reg &ref[71]
	call_c   Dyam_Create_Atom("*PROLOG-LAST*")
	move_ret ref[71]
	c_ret

long local pool_fun31[4]=[131073,build_ref_71,pool_fun30,pool_fun28]

long local pool_fun32[6]=[65540,build_ref_124,build_ref_125,build_ref_126,build_ref_70,pool_fun31]

pl_code local fun32
	call_c   Dyam_Remove_Choice()
	move     &ref[124], R(0)
	move     0, R(1)
	move     V(11), R(2)
	move     S(5), R(3)
	pl_call  pred_update_counter_2()
	move     &ref[125], R(0)
	move     0, R(1)
	move     &ref[126], R(2)
	move     S(5), R(3)
	move     V(10), R(4)
	move     S(5), R(5)
	pl_call  pred_name_builder_3()
	call_c   DYAM_evpred_assert_1(&ref[70])
fun31:
	call_c   Dyam_Reg_Load(0,V(2))
	call_c   Dyam_Reg_Load(2,V(5))
	move     V(12), R(4)
	move     S(5), R(5)
	pl_call  pred_extend_tupple_3()
	call_c   Dyam_Choice(fun30)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(3),&ref[71])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(13),V(4))
	fail_ret
	call_c   Dyam_Unify(V(7),V(4))
	fail_ret
	pl_jump  fun28()


long local pool_fun33[4]=[131073,build_ref_70,pool_fun32,pool_fun31]

pl_code local fun34
	call_c   Dyam_Remove_Choice()
fun33:
	call_c   Dyam_Choice(fun32)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[70])
	call_c   Dyam_Cut()
	pl_jump  fun31()


;; TERM 69: callret(_I, _J)
c_code local build_ref_69
	ret_reg &ref[69]
	call_c   build_ref_339()
	call_c   Dyam_Create_Binary(&ref[339],V(8),V(9))
	move_ret ref[69]
	c_ret

long local pool_fun35[4]=[65538,build_ref_68,build_ref_69,pool_fun33]

pl_code local fun35
	call_c   Dyam_Pool(pool_fun35)
	call_c   Dyam_Unify_Item(&ref[68])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun34)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(2),&ref[69])
	fail_ret
	call_c   Dyam_Cut()
	pl_fail

;; TERM 67: '*PROLOG-FIRST*'(unfold('*WRAPPER-INNER-CALL*'(_B, _C, _D), _E, _F, _G, _H)) :> []
c_code local build_ref_67
	ret_reg &ref[67]
	call_c   build_ref_66()
	call_c   Dyam_Create_Binary(I(9),&ref[66],I(0))
	move_ret ref[67]
	c_ret

;; TERM 66: '*PROLOG-FIRST*'(unfold('*WRAPPER-INNER-CALL*'(_B, _C, _D), _E, _F, _G, _H))
c_code local build_ref_66
	ret_reg &ref[66]
	call_c   build_ref_11()
	call_c   build_ref_65()
	call_c   Dyam_Create_Unary(&ref[11],&ref[65])
	move_ret ref[66]
	c_ret

c_code local build_seed_9
	ret_reg &seed[9]
	call_c   build_ref_10()
	call_c   build_ref_79()
	call_c   Dyam_Seed_Start(&ref[10],&ref[79],I(0),fun1,1)
	call_c   build_ref_80()
	call_c   Dyam_Seed_Add_Comp(&ref[80],fun42,0)
	call_c   Dyam_Seed_End()
	move_ret seed[9]
	c_ret

;; TERM 80: '*PROLOG-ITEM*'{top=> unfold('*WRAPPER-CALL-ALT*'(_B, _C, _D), _E, _F, _G, _H), cont=> _A}
c_code local build_ref_80
	ret_reg &ref[80]
	call_c   build_ref_329()
	call_c   build_ref_77()
	call_c   Dyam_Create_Binary(&ref[329],&ref[77],V(0))
	move_ret ref[80]
	c_ret

;; TERM 77: unfold('*WRAPPER-CALL-ALT*'(_B, _C, _D), _E, _F, _G, _H)
c_code local build_ref_77
	ret_reg &ref[77]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_351()
	call_c   Dyam_Term_Start(&ref[351],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_330()
	call_c   Dyam_Term_Start(&ref[330],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[77]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 351: '*WRAPPER-CALL-ALT*'
c_code local build_ref_351
	ret_reg &ref[351]
	call_c   Dyam_Create_Atom("*WRAPPER-CALL-ALT*")
	move_ret ref[351]
	c_ret

;; TERM 134: '$CLOSURE'('$fun'(39, 0, 1133447204), '$TUPPLE'(35082884089388))
c_code local build_ref_134
	ret_reg &ref[134]
	call_c   build_ref_133()
	call_c   Dyam_Closure_Aux(fun39,&ref[133])
	move_ret ref[134]
	c_ret

c_code local build_seed_37
	ret_reg &seed[37]
	call_c   build_ref_55()
	call_c   build_ref_128()
	call_c   Dyam_Seed_Start(&ref[55],&ref[128],I(0),fun8,1)
	call_c   build_ref_131()
	call_c   Dyam_Seed_Add_Comp(&ref[131],&ref[128],0)
	call_c   Dyam_Seed_End()
	move_ret seed[37]
	c_ret

;; TERM 131: '*PROLOG-FIRST*'(build_closure(_D, _E, _L, _M, _H)) :> '$$HOLE$$'
c_code local build_ref_131
	ret_reg &ref[131]
	call_c   build_ref_130()
	call_c   Dyam_Create_Binary(I(9),&ref[130],I(7))
	move_ret ref[131]
	c_ret

;; TERM 130: '*PROLOG-FIRST*'(build_closure(_D, _E, _L, _M, _H))
c_code local build_ref_130
	ret_reg &ref[130]
	call_c   build_ref_11()
	call_c   build_ref_129()
	call_c   Dyam_Create_Unary(&ref[11],&ref[129])
	move_ret ref[130]
	c_ret

;; TERM 129: build_closure(_D, _E, _L, _M, _H)
c_code local build_ref_129
	ret_reg &ref[129]
	call_c   build_ref_347()
	call_c   Dyam_Term_Start(&ref[347],5)
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[129]
	c_ret

;; TERM 128: '*PROLOG-ITEM*'{top=> build_closure(_D, _E, _L, _M, _H), cont=> '$CLOSURE'('$fun'(37, 0, 1133438288), '$TUPPLE'(35082884089352))}
c_code local build_ref_128
	ret_reg &ref[128]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,365363200)
	move_ret R(0)
	call_c   Dyam_Closure_Aux(fun37,R(0))
	move_ret R(0)
	call_c   build_ref_329()
	call_c   build_ref_129()
	call_c   Dyam_Create_Binary(&ref[329],&ref[129],R(0))
	move_ret ref[128]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 127: '$CLOSURE'(_O, _N)
c_code local build_ref_127
	ret_reg &ref[127]
	call_c   build_ref_116()
	call_c   Dyam_Create_Binary(&ref[116],V(14),V(13))
	move_ret ref[127]
	c_ret

;; TERM 116: '$CLOSURE'
c_code local build_ref_116
	ret_reg &ref[116]
	call_c   Dyam_Create_Atom("$CLOSURE")
	move_ret ref[116]
	c_ret

;; TERM 89: [_E,_M|_C]
c_code local build_ref_89
	ret_reg &ref[89]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(12),V(2))
	move_ret R(0)
	call_c   Dyam_Create_List(V(4),R(0))
	move_ret ref[89]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 90: _Q + 1
c_code local build_ref_90
	ret_reg &ref[90]
	call_c   build_ref_348()
	call_c   Dyam_Create_Binary(&ref[348],V(16),N(1))
	move_ret ref[90]
	c_ret

c_code local build_seed_31
	ret_reg &seed[31]
	call_c   build_ref_17()
	call_c   build_ref_92()
	call_c   Dyam_Seed_Start(&ref[17],&ref[92],I(0),fun8,1)
	call_c   build_ref_93()
	call_c   Dyam_Seed_Add_Comp(&ref[93],&ref[92],0)
	call_c   Dyam_Seed_End()
	move_ret seed[31]
	c_ret

;; TERM 93: '*GUARD*'(std_prolog_load_args_alt([_M|_C], 0, _G, (call(_K, []) :> reg_reset(_R) :> noop), _F, _P)) :> '$$HOLE$$'
c_code local build_ref_93
	ret_reg &ref[93]
	call_c   build_ref_92()
	call_c   Dyam_Create_Binary(I(9),&ref[92],I(7))
	move_ret ref[93]
	c_ret

;; TERM 92: '*GUARD*'(std_prolog_load_args_alt([_M|_C], 0, _G, (call(_K, []) :> reg_reset(_R) :> noop), _F, _P))
c_code local build_ref_92
	ret_reg &ref[92]
	call_c   build_ref_17()
	call_c   build_ref_91()
	call_c   Dyam_Create_Unary(&ref[17],&ref[91])
	move_ret ref[92]
	c_ret

;; TERM 91: std_prolog_load_args_alt([_M|_C], 0, _G, (call(_K, []) :> reg_reset(_R) :> noop), _F, _P)
c_code local build_ref_91
	ret_reg &ref[91]
	call_c   Dyam_Pseudo_Choice(3)
	call_c   Dyam_Create_List(V(12),V(2))
	move_ret R(0)
	call_c   build_ref_349()
	call_c   Dyam_Create_Binary(&ref[349],V(10),I(0))
	move_ret R(1)
	call_c   build_ref_350()
	call_c   Dyam_Create_Unary(&ref[350],V(17))
	move_ret R(2)
	call_c   build_ref_302()
	call_c   Dyam_Create_Binary(I(9),R(2),&ref[302])
	move_ret R(2)
	call_c   Dyam_Create_Binary(I(9),R(1),R(2))
	move_ret R(2)
	call_c   build_ref_334()
	call_c   Dyam_Term_Start(&ref[334],6)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(N(0))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(R(2))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_End()
	move_ret ref[91]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun36[4]=[3,build_ref_89,build_ref_90,build_seed_31]

long local pool_fun37[3]=[65537,build_ref_127,pool_fun36]

pl_code local fun37
	call_c   Dyam_Pool(pool_fun37)
	call_c   Dyam_Unify(V(12),&ref[127])
	fail_ret
	call_c   Dyam_Allocate(0)
fun36:
	move     &ref[89], R(0)
	move     S(5), R(1)
	move     V(15), R(2)
	move     S(5), R(3)
	pl_call  pred_duplicate_vars_2()
	call_c   DYAM_evpred_length(V(2),V(16))
	fail_ret
	call_c   DYAM_evpred_is(V(17),&ref[90])
	fail_ret
	pl_call  fun9(&seed[31],1)
	call_c   Dyam_Reg_Load(0,V(11))
	move     V(18), R(2)
	move     S(5), R(3)
	pl_call  pred_oset_tupple_2()
	call_c   Dyam_Reg_Load(0,V(13))
	move     V(19), R(2)
	move     S(5), R(3)
	pl_call  pred_oset_tupple_2()
	call_c   Dyam_Reg_Load(0,V(12))
	move     V(20), R(2)
	move     S(5), R(3)
	pl_call  pred_label_term_2()
	call_c   Dyam_Deallocate()
	pl_jump  Follow_Cont(V(0))


long local pool_fun38[2]=[1,build_seed_37]

pl_code local fun38
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Deallocate()
	pl_jump  fun9(&seed[37],12)

long local pool_fun39[4]=[131073,build_ref_71,pool_fun38,pool_fun36]

pl_code local fun39
	call_c   Dyam_Pool(pool_fun39)
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(2))
	call_c   Dyam_Reg_Load(2,V(5))
	move     V(11), R(4)
	move     S(5), R(5)
	pl_call  pred_extend_tupple_3()
	call_c   Dyam_Choice(fun38)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(3),&ref[71])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(12),V(4))
	fail_ret
	call_c   Dyam_Unify(V(7),V(4))
	fail_ret
	move     V(13), R(0)
	move     S(5), R(1)
	pl_call  pred_null_tupple_1()
	pl_jump  fun36()

;; TERM 133: '$TUPPLE'(35082884089388)
c_code local build_ref_133
	ret_reg &ref[133]
	call_c   Dyam_Create_Simple_Tupple(0,400818176)
	move_ret ref[133]
	c_ret

long local pool_fun40[2]=[1,build_ref_134]

pl_code local fun41
	call_c   Dyam_Remove_Choice()
fun40:
	move     &ref[134], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Load(4,V(1))
	move     V(10), R(6)
	move     S(5), R(7)
	call_c   Dyam_Reg_Deallocate(4)
fun19:
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Reg_Bind(2,V(4))
	call_c   Dyam_Multi_Reg_Bind(4,2,2)
	pl_call  fun11(&seed[29],1)
	pl_call  fun18(&seed[30],2,V(4))
	call_c   Dyam_Deallocate()
	pl_ret



long local pool_fun42[4]=[65538,build_ref_80,build_ref_69,pool_fun40]

pl_code local fun42
	call_c   Dyam_Pool(pool_fun42)
	call_c   Dyam_Unify_Item(&ref[80])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun41)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(2),&ref[69])
	fail_ret
	call_c   Dyam_Cut()
	pl_fail

;; TERM 79: '*PROLOG-FIRST*'(unfold('*WRAPPER-CALL-ALT*'(_B, _C, _D), _E, _F, _G, _H)) :> []
c_code local build_ref_79
	ret_reg &ref[79]
	call_c   build_ref_78()
	call_c   Dyam_Create_Binary(I(9),&ref[78],I(0))
	move_ret ref[79]
	c_ret

;; TERM 78: '*PROLOG-FIRST*'(unfold('*WRAPPER-CALL-ALT*'(_B, _C, _D), _E, _F, _G, _H))
c_code local build_ref_78
	ret_reg &ref[78]
	call_c   build_ref_11()
	call_c   build_ref_77()
	call_c   Dyam_Create_Unary(&ref[11],&ref[77])
	move_ret ref[78]
	c_ret

c_code local build_seed_10
	ret_reg &seed[10]
	call_c   build_ref_10()
	call_c   build_ref_96()
	call_c   Dyam_Seed_Start(&ref[10],&ref[96],I(0),fun1,1)
	call_c   build_ref_97()
	call_c   Dyam_Seed_Add_Comp(&ref[97],fun49,0)
	call_c   Dyam_Seed_End()
	move_ret seed[10]
	c_ret

;; TERM 97: '*PROLOG-ITEM*'{top=> unfold('*WRAPPER-CALL*'(_B, _C, _D), _E, _F, _G, _H), cont=> _A}
c_code local build_ref_97
	ret_reg &ref[97]
	call_c   build_ref_329()
	call_c   build_ref_94()
	call_c   Dyam_Create_Binary(&ref[329],&ref[94],V(0))
	move_ret ref[97]
	c_ret

;; TERM 94: unfold('*WRAPPER-CALL*'(_B, _C, _D), _E, _F, _G, _H)
c_code local build_ref_94
	ret_reg &ref[94]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_352()
	call_c   Dyam_Term_Start(&ref[352],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_330()
	call_c   Dyam_Term_Start(&ref[330],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[94]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 352: '*WRAPPER-CALL*'
c_code local build_ref_352
	ret_reg &ref[352]
	call_c   Dyam_Create_Atom("*WRAPPER-CALL*")
	move_ret ref[352]
	c_ret

;; TERM 137: '$CLOSURE'('$fun'(46, 0, 1133477936), '$TUPPLE'(35082884089388))
c_code local build_ref_137
	ret_reg &ref[137]
	call_c   build_ref_133()
	call_c   Dyam_Closure_Aux(fun46,&ref[133])
	move_ret ref[137]
	c_ret

c_code local build_seed_38
	ret_reg &seed[38]
	call_c   build_ref_55()
	call_c   build_ref_135()
	call_c   Dyam_Seed_Start(&ref[55],&ref[135],I(0),fun8,1)
	call_c   build_ref_131()
	call_c   Dyam_Seed_Add_Comp(&ref[131],&ref[135],0)
	call_c   Dyam_Seed_End()
	move_ret seed[38]
	c_ret

;; TERM 135: '*PROLOG-ITEM*'{top=> build_closure(_D, _E, _L, _M, _H), cont=> '$CLOSURE'('$fun'(44, 0, 1133478644), '$TUPPLE'(35082884089520))}
c_code local build_ref_135
	ret_reg &ref[135]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,365232128)
	move_ret R(0)
	call_c   Dyam_Closure_Aux(fun44,R(0))
	move_ret R(0)
	call_c   build_ref_329()
	call_c   build_ref_129()
	call_c   Dyam_Create_Binary(&ref[329],&ref[129],R(0))
	move_ret ref[135]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 98: _O + 1
c_code local build_ref_98
	ret_reg &ref[98]
	call_c   build_ref_348()
	call_c   Dyam_Create_Binary(&ref[348],V(14),N(1))
	move_ret ref[98]
	c_ret

c_code local build_seed_32
	ret_reg &seed[32]
	call_c   build_ref_17()
	call_c   build_ref_100()
	call_c   Dyam_Seed_Start(&ref[17],&ref[100],I(0),fun8,1)
	call_c   build_ref_101()
	call_c   Dyam_Seed_Add_Comp(&ref[101],&ref[100],0)
	call_c   Dyam_Seed_End()
	move_ret seed[32]
	c_ret

;; TERM 101: '*GUARD*'(std_prolog_load_args_alt([_M|_C], 0, _G, (call(_K, []) :> reg_reset(_P) :> noop), _F, _N)) :> '$$HOLE$$'
c_code local build_ref_101
	ret_reg &ref[101]
	call_c   build_ref_100()
	call_c   Dyam_Create_Binary(I(9),&ref[100],I(7))
	move_ret ref[101]
	c_ret

;; TERM 100: '*GUARD*'(std_prolog_load_args_alt([_M|_C], 0, _G, (call(_K, []) :> reg_reset(_P) :> noop), _F, _N))
c_code local build_ref_100
	ret_reg &ref[100]
	call_c   build_ref_17()
	call_c   build_ref_99()
	call_c   Dyam_Create_Unary(&ref[17],&ref[99])
	move_ret ref[100]
	c_ret

;; TERM 99: std_prolog_load_args_alt([_M|_C], 0, _G, (call(_K, []) :> reg_reset(_P) :> noop), _F, _N)
c_code local build_ref_99
	ret_reg &ref[99]
	call_c   Dyam_Pseudo_Choice(3)
	call_c   Dyam_Create_List(V(12),V(2))
	move_ret R(0)
	call_c   build_ref_349()
	call_c   Dyam_Create_Binary(&ref[349],V(10),I(0))
	move_ret R(1)
	call_c   build_ref_350()
	call_c   Dyam_Create_Unary(&ref[350],V(15))
	move_ret R(2)
	call_c   build_ref_302()
	call_c   Dyam_Create_Binary(I(9),R(2),&ref[302])
	move_ret R(2)
	call_c   Dyam_Create_Binary(I(9),R(1),R(2))
	move_ret R(2)
	call_c   build_ref_334()
	call_c   Dyam_Term_Start(&ref[334],6)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(N(0))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(R(2))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_End()
	move_ret ref[99]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun43[4]=[3,build_ref_89,build_ref_98,build_seed_32]

pl_code local fun44
	call_c   Dyam_Pool(pool_fun43)
	call_c   Dyam_Allocate(0)
fun43:
	move     &ref[89], R(0)
	move     S(5), R(1)
	move     V(13), R(2)
	move     S(5), R(3)
	pl_call  pred_duplicate_vars_2()
	call_c   DYAM_evpred_length(V(2),V(14))
	fail_ret
	call_c   DYAM_evpred_is(V(15),&ref[98])
	fail_ret
	pl_call  fun9(&seed[32],1)
	call_c   Dyam_Deallocate()
	pl_jump  Follow_Cont(V(0))


long local pool_fun45[2]=[1,build_seed_38]

pl_code local fun45
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Deallocate()
	pl_jump  fun9(&seed[38],12)

long local pool_fun46[4]=[131073,build_ref_71,pool_fun45,pool_fun43]

pl_code local fun46
	call_c   Dyam_Pool(pool_fun46)
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(2))
	call_c   Dyam_Reg_Load(2,V(5))
	move     V(11), R(4)
	move     S(5), R(5)
	pl_call  pred_extend_tupple_3()
	call_c   Dyam_Choice(fun45)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(3),&ref[71])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(12),V(4))
	fail_ret
	call_c   Dyam_Unify(V(7),V(4))
	fail_ret
	pl_jump  fun43()

long local pool_fun47[2]=[1,build_ref_137]

pl_code local fun48
	call_c   Dyam_Remove_Choice()
fun47:
	move     &ref[137], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Load(4,V(1))
	move     V(10), R(6)
	move     S(5), R(7)
	call_c   Dyam_Reg_Deallocate(4)
	pl_jump  fun19()


long local pool_fun49[4]=[65538,build_ref_97,build_ref_69,pool_fun47]

pl_code local fun49
	call_c   Dyam_Pool(pool_fun49)
	call_c   Dyam_Unify_Item(&ref[97])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun48)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(2),&ref[69])
	fail_ret
	call_c   Dyam_Cut()
	pl_fail

;; TERM 96: '*PROLOG-FIRST*'(unfold('*WRAPPER-CALL*'(_B, _C, _D), _E, _F, _G, _H)) :> []
c_code local build_ref_96
	ret_reg &ref[96]
	call_c   build_ref_95()
	call_c   Dyam_Create_Binary(I(9),&ref[95],I(0))
	move_ret ref[96]
	c_ret

;; TERM 95: '*PROLOG-FIRST*'(unfold('*WRAPPER-CALL*'(_B, _C, _D), _E, _F, _G, _H))
c_code local build_ref_95
	ret_reg &ref[95]
	call_c   build_ref_11()
	call_c   build_ref_94()
	call_c   Dyam_Create_Unary(&ref[11],&ref[94])
	move_ret ref[95]
	c_ret

c_code local build_seed_21
	ret_reg &seed[21]
	call_c   build_ref_10()
	call_c   build_ref_104()
	call_c   Dyam_Seed_Start(&ref[10],&ref[104],I(0),fun1,1)
	call_c   build_ref_105()
	call_c   Dyam_Seed_Add_Comp(&ref[105],fun27,0)
	call_c   Dyam_Seed_End()
	move_ret seed[21]
	c_ret

;; TERM 105: '*PROLOG-ITEM*'{top=> unfold('*STD-PROLOG-CALL*'((_B / _C), _D), _E, _F, _G, _E), cont=> _A}
c_code local build_ref_105
	ret_reg &ref[105]
	call_c   build_ref_329()
	call_c   build_ref_102()
	call_c   Dyam_Create_Binary(&ref[329],&ref[102],V(0))
	move_ret ref[105]
	c_ret

;; TERM 102: unfold('*STD-PROLOG-CALL*'((_B / _C), _D), _E, _F, _G, _E)
c_code local build_ref_102
	ret_reg &ref[102]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_338()
	call_c   Dyam_Create_Binary(&ref[338],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_353()
	call_c   Dyam_Create_Binary(&ref[353],R(0),V(3))
	move_ret R(0)
	call_c   build_ref_330()
	call_c   Dyam_Term_Start(&ref[330],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[102]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 353: '*STD-PROLOG-CALL*'
c_code local build_ref_353
	ret_reg &ref[353]
	call_c   Dyam_Create_Atom("*STD-PROLOG-CALL*")
	move_ret ref[353]
	c_ret

;; TERM 119: '$CLOSURE'('$fun'(26, 0, 1133370416), '$TUPPLE'(35082884089004))
c_code local build_ref_119
	ret_reg &ref[119]
	call_c   build_ref_118()
	call_c   Dyam_Closure_Aux(fun26,&ref[118])
	move_ret ref[119]
	c_ret

c_code local build_seed_35
	ret_reg &seed[35]
	call_c   build_ref_17()
	call_c   build_ref_114()
	call_c   Dyam_Seed_Start(&ref[17],&ref[114],I(0),fun8,1)
	call_c   build_ref_115()
	call_c   Dyam_Seed_Add_Comp(&ref[115],&ref[114],0)
	call_c   Dyam_Seed_End()
	move_ret seed[35]
	c_ret

;; TERM 115: '*GUARD*'(std_prolog_load_args(_D, 0, _G, (call(_H, []) :> reg_reset(_C)), _F)) :> '$$HOLE$$'
c_code local build_ref_115
	ret_reg &ref[115]
	call_c   build_ref_114()
	call_c   Dyam_Create_Binary(I(9),&ref[114],I(7))
	move_ret ref[115]
	c_ret

;; TERM 114: '*GUARD*'(std_prolog_load_args(_D, 0, _G, (call(_H, []) :> reg_reset(_C)), _F))
c_code local build_ref_114
	ret_reg &ref[114]
	call_c   build_ref_17()
	call_c   build_ref_113()
	call_c   Dyam_Create_Unary(&ref[17],&ref[113])
	move_ret ref[114]
	c_ret

;; TERM 113: std_prolog_load_args(_D, 0, _G, (call(_H, []) :> reg_reset(_C)), _F)
c_code local build_ref_113
	ret_reg &ref[113]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_349()
	call_c   Dyam_Create_Binary(&ref[349],V(7),I(0))
	move_ret R(0)
	call_c   build_ref_350()
	call_c   Dyam_Create_Unary(&ref[350],V(2))
	move_ret R(1)
	call_c   Dyam_Create_Binary(I(9),R(0),R(1))
	move_ret R(1)
	call_c   build_ref_332()
	call_c   Dyam_Term_Start(&ref[332],5)
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(N(0))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[113]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun26[2]=[1,build_seed_35]

pl_code local fun26
	call_c   Dyam_Pool(pool_fun26)
	call_c   Dyam_Allocate(0)
	pl_call  fun9(&seed[35],1)
	call_c   Dyam_Deallocate()
	pl_jump  Follow_Cont(V(0))

;; TERM 118: '$TUPPLE'(35082884089004)
c_code local build_ref_118
	ret_reg &ref[118]
	call_c   Dyam_Create_Simple_Tupple(0,383778816)
	move_ret ref[118]
	c_ret

long local pool_fun27[3]=[2,build_ref_105,build_ref_119]

pl_code local fun27
	call_c   Dyam_Pool(pool_fun27)
	call_c   Dyam_Unify_Item(&ref[105])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[119], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Load(4,V(1))
	call_c   Dyam_Reg_Load(6,V(2))
	move     V(7), R(8)
	move     S(5), R(9)
	call_c   Dyam_Reg_Deallocate(5)
fun25:
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Reg_Bind(2,V(5))
	call_c   Dyam_Multi_Reg_Bind(4,2,3)
	call_c   Dyam_Choice(fun24)
	pl_call  fun21(&seed[33],1)
	pl_fail


;; TERM 104: '*PROLOG-FIRST*'(unfold('*STD-PROLOG-CALL*'((_B / _C), _D), _E, _F, _G, _E)) :> []
c_code local build_ref_104
	ret_reg &ref[104]
	call_c   build_ref_103()
	call_c   Dyam_Create_Binary(I(9),&ref[103],I(0))
	move_ret ref[104]
	c_ret

;; TERM 103: '*PROLOG-FIRST*'(unfold('*STD-PROLOG-CALL*'((_B / _C), _D), _E, _F, _G, _E))
c_code local build_ref_103
	ret_reg &ref[103]
	call_c   build_ref_11()
	call_c   build_ref_102()
	call_c   Dyam_Create_Unary(&ref[11],&ref[102])
	move_ret ref[103]
	c_ret

c_code local build_seed_12
	ret_reg &seed[12]
	call_c   build_ref_10()
	call_c   build_ref_142()
	call_c   Dyam_Seed_Start(&ref[10],&ref[142],I(0),fun1,1)
	call_c   build_ref_143()
	call_c   Dyam_Seed_Add_Comp(&ref[143],fun58,0)
	call_c   Dyam_Seed_End()
	move_ret seed[12]
	c_ret

;; TERM 143: '*PROLOG-ITEM*'{top=> unfold('*RETARGS*'(_B), _C, _D, _E, _C), cont=> _A}
c_code local build_ref_143
	ret_reg &ref[143]
	call_c   build_ref_329()
	call_c   build_ref_140()
	call_c   Dyam_Create_Binary(&ref[329],&ref[140],V(0))
	move_ret ref[143]
	c_ret

;; TERM 140: unfold('*RETARGS*'(_B), _C, _D, _E, _C)
c_code local build_ref_140
	ret_reg &ref[140]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_354()
	call_c   Dyam_Create_Unary(&ref[354],V(1))
	move_ret R(0)
	call_c   build_ref_330()
	call_c   Dyam_Term_Start(&ref[330],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_End()
	move_ret ref[140]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 354: '*RETARGS*'
c_code local build_ref_354
	ret_reg &ref[354]
	call_c   Dyam_Create_Atom("*RETARGS*")
	move_ret ref[354]
	c_ret

;; TERM 144: _G + 1
c_code local build_ref_144
	ret_reg &ref[144]
	call_c   build_ref_348()
	call_c   Dyam_Create_Binary(&ref[348],V(6),N(1))
	move_ret ref[144]
	c_ret

c_code local build_seed_39
	ret_reg &seed[39]
	call_c   build_ref_17()
	call_c   build_ref_146()
	call_c   Dyam_Seed_Start(&ref[17],&ref[146],I(0),fun8,1)
	call_c   build_ref_147()
	call_c   Dyam_Seed_Add_Comp(&ref[147],&ref[146],0)
	call_c   Dyam_Seed_End()
	move_ret seed[39]
	c_ret

;; TERM 147: '*GUARD*'(std_prolog_load_args(_B, 1, _E, (call('Follow_Cont', [_F]) :> reg_reset(_H)), _D)) :> '$$HOLE$$'
c_code local build_ref_147
	ret_reg &ref[147]
	call_c   build_ref_146()
	call_c   Dyam_Create_Binary(I(9),&ref[146],I(7))
	move_ret ref[147]
	c_ret

;; TERM 146: '*GUARD*'(std_prolog_load_args(_B, 1, _E, (call('Follow_Cont', [_F]) :> reg_reset(_H)), _D))
c_code local build_ref_146
	ret_reg &ref[146]
	call_c   build_ref_17()
	call_c   build_ref_145()
	call_c   Dyam_Create_Unary(&ref[17],&ref[145])
	move_ret ref[146]
	c_ret

;; TERM 145: std_prolog_load_args(_B, 1, _E, (call('Follow_Cont', [_F]) :> reg_reset(_H)), _D)
c_code local build_ref_145
	ret_reg &ref[145]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   Dyam_Create_List(V(5),I(0))
	move_ret R(0)
	call_c   build_ref_349()
	call_c   build_ref_355()
	call_c   Dyam_Create_Binary(&ref[349],&ref[355],R(0))
	move_ret R(0)
	call_c   build_ref_350()
	call_c   Dyam_Create_Unary(&ref[350],V(7))
	move_ret R(1)
	call_c   Dyam_Create_Binary(I(9),R(0),R(1))
	move_ret R(1)
	call_c   build_ref_332()
	call_c   Dyam_Term_Start(&ref[332],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(N(1))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[145]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 355: 'Follow_Cont'
c_code local build_ref_355
	ret_reg &ref[355]
	call_c   Dyam_Create_Atom("Follow_Cont")
	move_ret ref[355]
	c_ret

long local pool_fun58[4]=[3,build_ref_143,build_ref_144,build_seed_39]

pl_code local fun58
	call_c   Dyam_Pool(pool_fun58)
	call_c   Dyam_Unify_Item(&ref[143])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(2))
	move     V(5), R(2)
	move     S(5), R(3)
	pl_call  pred_label_term_2()
	call_c   DYAM_evpred_length(V(1),V(6))
	fail_ret
	call_c   DYAM_evpred_is(V(7),&ref[144])
	fail_ret
	pl_call  fun9(&seed[39],1)
	call_c   Dyam_Deallocate()
	pl_jump  Follow_Cont(V(0))

;; TERM 142: '*PROLOG-FIRST*'(unfold('*RETARGS*'(_B), _C, _D, _E, _C)) :> []
c_code local build_ref_142
	ret_reg &ref[142]
	call_c   build_ref_141()
	call_c   Dyam_Create_Binary(I(9),&ref[141],I(0))
	move_ret ref[142]
	c_ret

;; TERM 141: '*PROLOG-FIRST*'(unfold('*RETARGS*'(_B), _C, _D, _E, _C))
c_code local build_ref_141
	ret_reg &ref[141]
	call_c   build_ref_11()
	call_c   build_ref_140()
	call_c   Dyam_Create_Unary(&ref[11],&ref[140])
	move_ret ref[141]
	c_ret

c_code local build_seed_11
	ret_reg &seed[11]
	call_c   build_ref_10()
	call_c   build_ref_150()
	call_c   Dyam_Seed_Start(&ref[10],&ref[150],I(0),fun1,1)
	call_c   build_ref_151()
	call_c   Dyam_Seed_Add_Comp(&ref[151],fun60,0)
	call_c   Dyam_Seed_End()
	move_ret seed[11]
	c_ret

;; TERM 151: '*PROLOG-ITEM*'{top=> unfold(load_args(_B, _C), _D, _E, _F, _G), cont=> _A}
c_code local build_ref_151
	ret_reg &ref[151]
	call_c   build_ref_329()
	call_c   build_ref_148()
	call_c   Dyam_Create_Binary(&ref[329],&ref[148],V(0))
	move_ret ref[151]
	c_ret

;; TERM 148: unfold(load_args(_B, _C), _D, _E, _F, _G)
c_code local build_ref_148
	ret_reg &ref[148]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_356()
	call_c   Dyam_Create_Binary(&ref[356],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_330()
	call_c   Dyam_Term_Start(&ref[330],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[148]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 356: load_args
c_code local build_ref_356
	ret_reg &ref[356]
	call_c   Dyam_Create_Atom("load_args")
	move_ret ref[356]
	c_ret

c_code local build_seed_41
	ret_reg &seed[41]
	call_c   build_ref_55()
	call_c   build_ref_155()
	call_c   Dyam_Seed_Start(&ref[55],&ref[155],I(0),fun8,1)
	call_c   build_ref_158()
	call_c   Dyam_Seed_Add_Comp(&ref[158],&ref[155],0)
	call_c   Dyam_Seed_End()
	move_ret seed[41]
	c_ret

;; TERM 158: '*PROLOG-FIRST*'(unfold(_C, _D, _I, _J, _G)) :> '$$HOLE$$'
c_code local build_ref_158
	ret_reg &ref[158]
	call_c   build_ref_157()
	call_c   Dyam_Create_Binary(I(9),&ref[157],I(7))
	move_ret ref[158]
	c_ret

;; TERM 157: '*PROLOG-FIRST*'(unfold(_C, _D, _I, _J, _G))
c_code local build_ref_157
	ret_reg &ref[157]
	call_c   build_ref_11()
	call_c   build_ref_156()
	call_c   Dyam_Create_Unary(&ref[11],&ref[156])
	move_ret ref[157]
	c_ret

;; TERM 156: unfold(_C, _D, _I, _J, _G)
c_code local build_ref_156
	ret_reg &ref[156]
	call_c   build_ref_330()
	call_c   Dyam_Term_Start(&ref[330],5)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[156]
	c_ret

;; TERM 155: '*PROLOG-ITEM*'{top=> unfold(_C, _D, _I, _J, _G), cont=> '$CLOSURE'('$fun'(59, 0, 1133556772), '$TUPPLE'(35082884089776))}
c_code local build_ref_155
	ret_reg &ref[155]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,413663232)
	move_ret R(0)
	call_c   Dyam_Closure_Aux(fun59,R(0))
	move_ret R(0)
	call_c   build_ref_329()
	call_c   build_ref_156()
	call_c   Dyam_Create_Binary(&ref[329],&ref[156],R(0))
	move_ret ref[155]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_40
	ret_reg &seed[40]
	call_c   build_ref_17()
	call_c   build_ref_153()
	call_c   Dyam_Seed_Start(&ref[17],&ref[153],I(0),fun8,1)
	call_c   build_ref_154()
	call_c   Dyam_Seed_Add_Comp(&ref[154],&ref[153],0)
	call_c   Dyam_Seed_End()
	move_ret seed[40]
	c_ret

;; TERM 154: '*GUARD*'(std_prolog_load_args(_B, 0, _F, _J, _H)) :> '$$HOLE$$'
c_code local build_ref_154
	ret_reg &ref[154]
	call_c   build_ref_153()
	call_c   Dyam_Create_Binary(I(9),&ref[153],I(7))
	move_ret ref[154]
	c_ret

;; TERM 153: '*GUARD*'(std_prolog_load_args(_B, 0, _F, _J, _H))
c_code local build_ref_153
	ret_reg &ref[153]
	call_c   build_ref_17()
	call_c   build_ref_152()
	call_c   Dyam_Create_Unary(&ref[17],&ref[152])
	move_ret ref[153]
	c_ret

;; TERM 152: std_prolog_load_args(_B, 0, _F, _J, _H)
c_code local build_ref_152
	ret_reg &ref[152]
	call_c   build_ref_332()
	call_c   Dyam_Term_Start(&ref[332],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(N(0))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[152]
	c_ret

long local pool_fun59[2]=[1,build_seed_40]

pl_code local fun59
	call_c   Dyam_Pool(pool_fun59)
	call_c   Dyam_Allocate(0)
	pl_call  fun9(&seed[40],1)
	call_c   Dyam_Deallocate()
	pl_jump  Follow_Cont(V(0))

long local pool_fun60[3]=[2,build_ref_151,build_seed_41]

pl_code local fun60
	call_c   Dyam_Pool(pool_fun60)
	call_c   Dyam_Unify_Item(&ref[151])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(3))
	call_c   Dyam_Reg_Load(2,V(4))
	move     V(7), R(4)
	move     S(5), R(5)
	pl_call  pred_extend_tupple_3()
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(4))
	move     V(8), R(4)
	move     S(5), R(5)
	pl_call  pred_extend_tupple_3()
	call_c   Dyam_Deallocate()
	pl_jump  fun9(&seed[41],12)

;; TERM 150: '*PROLOG-FIRST*'(unfold(load_args(_B, _C), _D, _E, _F, _G)) :> []
c_code local build_ref_150
	ret_reg &ref[150]
	call_c   build_ref_149()
	call_c   Dyam_Create_Binary(I(9),&ref[149],I(0))
	move_ret ref[150]
	c_ret

;; TERM 149: '*PROLOG-FIRST*'(unfold(load_args(_B, _C), _D, _E, _F, _G))
c_code local build_ref_149
	ret_reg &ref[149]
	call_c   build_ref_11()
	call_c   build_ref_148()
	call_c   Dyam_Create_Unary(&ref[11],&ref[148])
	move_ret ref[149]
	c_ret

c_code local build_seed_15
	ret_reg &seed[15]
	call_c   build_ref_35()
	call_c   build_ref_161()
	call_c   Dyam_Seed_Start(&ref[35],&ref[161],I(0),fun17,1)
	call_c   build_ref_162()
	call_c   Dyam_Seed_Add_Comp(&ref[162],fun70,0)
	call_c   Dyam_Seed_End()
	move_ret seed[15]
	c_ret

;; TERM 162: '*CITEM*'(c_module_name(_B, _C), _A)
c_code local build_ref_162
	ret_reg &ref[162]
	call_c   build_ref_39()
	call_c   build_ref_159()
	call_c   Dyam_Create_Binary(&ref[39],&ref[159],V(0))
	move_ret ref[162]
	c_ret

;; TERM 159: c_module_name(_B, _C)
c_code local build_ref_159
	ret_reg &ref[159]
	call_c   build_ref_326()
	call_c   Dyam_Create_Binary(&ref[326],V(1),V(2))
	move_ret ref[159]
	c_ret

pl_code local fun68
	call_c   Dyam_Remove_Choice()
	pl_fail

;; TERM 175: '$CLOSURE'('$fun'(67, 0, 1133619884), '$TUPPLE'(35082884089908))
c_code local build_ref_175
	ret_reg &ref[175]
	call_c   build_ref_174()
	call_c   Dyam_Closure_Aux(fun67,&ref[174])
	move_ret ref[175]
	c_ret

;; TERM 171: '~w~w__'
c_code local build_ref_171
	ret_reg &ref[171]
	call_c   Dyam_Create_Atom("~w~w__")
	move_ret ref[171]
	c_ret

;; TERM 172: [_F,_E]
c_code local build_ref_172
	ret_reg &ref[172]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(4),I(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(5),R(0))
	move_ret ref[172]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_42
	ret_reg &seed[42]
	call_c   build_ref_0()
	call_c   build_ref_53()
	call_c   Dyam_Seed_Start(&ref[0],&ref[53],&ref[53],fun8,1)
	call_c   build_ref_54()
	call_c   Dyam_Seed_Add_Comp(&ref[54],&ref[53],0)
	call_c   Dyam_Seed_End()
	move_ret seed[42]
	c_ret

pl_code local fun61
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Object(R(0))
	call_c   Dyam_Tabule()
	pl_ret

pl_code local fun62
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Choice(fun61)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Subsume(R(0))
	fail_ret
	call_c   Dyam_Cut()
	pl_ret

long local pool_fun63[2]=[1,build_seed_42]

long local pool_fun67[4]=[65538,build_ref_171,build_ref_172,pool_fun63]

pl_code local fun67
	call_c   Dyam_Pool(pool_fun67)
	call_c   Dyam_Allocate(0)
	move     &ref[171], R(0)
	move     0, R(1)
	move     &ref[172], R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Load(4,V(2))
	pl_call  pred_name_builder_3()
fun63:
	call_c   Dyam_Deallocate()
	pl_jump  fun62(&seed[42],2)


;; TERM 174: '$TUPPLE'(35082884089908)
c_code local build_ref_174
	ret_reg &ref[174]
	call_c   Dyam_Create_Simple_Tupple(0,360710144)
	move_ret ref[174]
	c_ret

long local pool_fun69[2]=[1,build_ref_175]

pl_code local fun69
	call_c   Dyam_Update_Choice(fun68)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_atom_to_module(V(1),V(3),V(4))
	fail_ret
	call_c   Dyam_Cut()
	move     &ref[175], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Load(4,V(3))
	move     V(5), R(6)
	move     S(5), R(7)
	call_c   Dyam_Reg_Deallocate(4)
fun66:
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Reg_Bind(2,V(4))
	call_c   Dyam_Multi_Reg_Bind(4,2,2)
	call_c   Dyam_Choice(fun65)
	pl_call  fun21(&seed[43],1)
	pl_fail


;; TERM 163: ''
c_code local build_ref_163
	ret_reg &ref[163]
	call_c   Dyam_Create_Atom("")
	move_ret ref[163]
	c_ret

long local pool_fun70[5]=[131074,build_ref_162,build_ref_163,pool_fun69,pool_fun63]

pl_code local fun70
	call_c   Dyam_Pool(pool_fun70)
	call_c   Dyam_Unify_Item(&ref[162])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun69)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(1),I(0))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(2),&ref[163])
	fail_ret
	pl_jump  fun63()

;; TERM 161: '*FIRST*'(c_module_name(_B, _C)) :> []
c_code local build_ref_161
	ret_reg &ref[161]
	call_c   build_ref_160()
	call_c   Dyam_Create_Binary(I(9),&ref[160],I(0))
	move_ret ref[161]
	c_ret

;; TERM 160: '*FIRST*'(c_module_name(_B, _C))
c_code local build_ref_160
	ret_reg &ref[160]
	call_c   build_ref_35()
	call_c   build_ref_159()
	call_c   Dyam_Create_Unary(&ref[35],&ref[159])
	move_ret ref[160]
	c_ret

c_code local build_seed_6
	ret_reg &seed[6]
	call_c   build_ref_10()
	call_c   build_ref_178()
	call_c   Dyam_Seed_Start(&ref[10],&ref[178],I(0),fun1,1)
	call_c   build_ref_179()
	call_c   Dyam_Seed_Add_Comp(&ref[179],fun76,0)
	call_c   Dyam_Seed_End()
	move_ret seed[6]
	c_ret

;; TERM 179: '*PROLOG-ITEM*'{top=> unfold('*WRAPPER-INNER-CALL*'(_B, callret(_C, _D), _E), _F, _G, _H, _I), cont=> _A}
c_code local build_ref_179
	ret_reg &ref[179]
	call_c   build_ref_329()
	call_c   build_ref_176()
	call_c   Dyam_Create_Binary(&ref[329],&ref[176],V(0))
	move_ret ref[179]
	c_ret

;; TERM 176: unfold('*WRAPPER-INNER-CALL*'(_B, callret(_C, _D), _E), _F, _G, _H, _I)
c_code local build_ref_176
	ret_reg &ref[176]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_339()
	call_c   Dyam_Create_Binary(&ref[339],V(2),V(3))
	move_ret R(0)
	call_c   build_ref_346()
	call_c   Dyam_Term_Start(&ref[346],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_330()
	call_c   Dyam_Term_Start(&ref[330],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_End()
	move_ret ref[176]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 189: [_K]
c_code local build_ref_189
	ret_reg &ref[189]
	call_c   Dyam_Create_List(V(10),I(0))
	move_ret ref[189]
	c_ret

;; TERM 180: wrapper_function_name(_B, _J)
c_code local build_ref_180
	ret_reg &ref[180]
	call_c   build_ref_342()
	call_c   Dyam_Create_Binary(&ref[342],V(1),V(9))
	move_ret ref[180]
	c_ret

c_code local build_seed_46
	ret_reg &seed[46]
	call_c   build_ref_55()
	call_c   build_ref_185()
	call_c   Dyam_Seed_Start(&ref[55],&ref[185],I(0),fun8,1)
	call_c   build_ref_188()
	call_c   Dyam_Seed_Add_Comp(&ref[188],&ref[185],0)
	call_c   Dyam_Seed_End()
	move_ret seed[46]
	c_ret

;; TERM 188: '*PROLOG-FIRST*'(build_closure(_E, _F, _L, _M, _I)) :> '$$HOLE$$'
c_code local build_ref_188
	ret_reg &ref[188]
	call_c   build_ref_187()
	call_c   Dyam_Create_Binary(I(9),&ref[187],I(7))
	move_ret ref[188]
	c_ret

;; TERM 187: '*PROLOG-FIRST*'(build_closure(_E, _F, _L, _M, _I))
c_code local build_ref_187
	ret_reg &ref[187]
	call_c   build_ref_11()
	call_c   build_ref_186()
	call_c   Dyam_Create_Unary(&ref[11],&ref[186])
	move_ret ref[187]
	c_ret

;; TERM 186: build_closure(_E, _F, _L, _M, _I)
c_code local build_ref_186
	ret_reg &ref[186]
	call_c   build_ref_347()
	call_c   Dyam_Term_Start(&ref[347],5)
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_End()
	move_ret ref[186]
	c_ret

;; TERM 185: '*PROLOG-ITEM*'{top=> build_closure(_E, _F, _L, _M, _I), cont=> '$CLOSURE'('$fun'(72, 0, 1133658072), '$TUPPLE'(35082884090152))}
c_code local build_ref_185
	ret_reg &ref[185]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,384368640)
	move_ret R(0)
	call_c   Dyam_Closure_Aux(fun72,R(0))
	move_ret R(0)
	call_c   build_ref_329()
	call_c   build_ref_186()
	call_c   Dyam_Create_Binary(&ref[329],&ref[186],R(0))
	move_ret ref[185]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 181: [_F,_M|_C]
c_code local build_ref_181
	ret_reg &ref[181]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(12),V(2))
	move_ret R(0)
	call_c   Dyam_Create_List(V(5),R(0))
	move_ret ref[181]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_45
	ret_reg &seed[45]
	call_c   build_ref_17()
	call_c   build_ref_183()
	call_c   Dyam_Seed_Start(&ref[17],&ref[183],I(0),fun8,1)
	call_c   build_ref_184()
	call_c   Dyam_Seed_Add_Comp(&ref[184],&ref[183],0)
	call_c   Dyam_Seed_End()
	move_ret seed[45]
	c_ret

;; TERM 184: '*GUARD*'(std_prolog_load_args_alt([_M|_C], 0, _H, (call(_J, []) :> reg_reset(_P) :> noop), _G, _N)) :> '$$HOLE$$'
c_code local build_ref_184
	ret_reg &ref[184]
	call_c   build_ref_183()
	call_c   Dyam_Create_Binary(I(9),&ref[183],I(7))
	move_ret ref[184]
	c_ret

;; TERM 183: '*GUARD*'(std_prolog_load_args_alt([_M|_C], 0, _H, (call(_J, []) :> reg_reset(_P) :> noop), _G, _N))
c_code local build_ref_183
	ret_reg &ref[183]
	call_c   build_ref_17()
	call_c   build_ref_182()
	call_c   Dyam_Create_Unary(&ref[17],&ref[182])
	move_ret ref[183]
	c_ret

;; TERM 182: std_prolog_load_args_alt([_M|_C], 0, _H, (call(_J, []) :> reg_reset(_P) :> noop), _G, _N)
c_code local build_ref_182
	ret_reg &ref[182]
	call_c   Dyam_Pseudo_Choice(3)
	call_c   Dyam_Create_List(V(12),V(2))
	move_ret R(0)
	call_c   build_ref_349()
	call_c   Dyam_Create_Binary(&ref[349],V(9),I(0))
	move_ret R(1)
	call_c   build_ref_350()
	call_c   Dyam_Create_Unary(&ref[350],V(15))
	move_ret R(2)
	call_c   build_ref_302()
	call_c   Dyam_Create_Binary(I(9),R(2),&ref[302])
	move_ret R(2)
	call_c   Dyam_Create_Binary(I(9),R(1),R(2))
	move_ret R(2)
	call_c   build_ref_334()
	call_c   Dyam_Term_Start(&ref[334],6)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(N(0))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(R(2))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_End()
	move_ret ref[182]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun71[4]=[3,build_ref_181,build_ref_98,build_seed_45]

pl_code local fun72
	call_c   Dyam_Pool(pool_fun71)
	call_c   Dyam_Allocate(0)
fun71:
	move     &ref[181], R(0)
	move     S(5), R(1)
	move     V(13), R(2)
	move     S(5), R(3)
	pl_call  pred_duplicate_vars_2()
	call_c   DYAM_evpred_length(V(3),V(14))
	fail_ret
	call_c   DYAM_evpred_is(V(15),&ref[98])
	fail_ret
	pl_call  fun9(&seed[45],1)
	call_c   Dyam_Deallocate()
	pl_jump  Follow_Cont(V(0))


long local pool_fun73[2]=[1,build_seed_46]

pl_code local fun73
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Deallocate()
	pl_jump  fun9(&seed[46],12)

long local pool_fun74[4]=[131073,build_ref_71,pool_fun73,pool_fun71]

long local pool_fun75[6]=[65540,build_ref_124,build_ref_125,build_ref_189,build_ref_180,pool_fun74]

pl_code local fun75
	call_c   Dyam_Remove_Choice()
	move     &ref[124], R(0)
	move     0, R(1)
	move     V(10), R(2)
	move     S(5), R(3)
	pl_call  pred_update_counter_2()
	move     &ref[125], R(0)
	move     0, R(1)
	move     &ref[189], R(2)
	move     S(5), R(3)
	move     V(9), R(4)
	move     S(5), R(5)
	pl_call  pred_name_builder_3()
	call_c   DYAM_evpred_assert_1(&ref[180])
fun74:
	call_c   Dyam_Reg_Load(0,V(2))
	call_c   Dyam_Reg_Load(2,V(6))
	move     V(11), R(4)
	move     S(5), R(5)
	pl_call  pred_extend_tupple_3()
	call_c   Dyam_Choice(fun73)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(4),&ref[71])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(12),V(5))
	fail_ret
	call_c   Dyam_Unify(V(8),V(5))
	fail_ret
	pl_jump  fun71()


long local pool_fun76[5]=[131074,build_ref_179,build_ref_180,pool_fun75,pool_fun74]

pl_code local fun76
	call_c   Dyam_Pool(pool_fun76)
	call_c   Dyam_Unify_Item(&ref[179])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun75)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[180])
	call_c   Dyam_Cut()
	pl_jump  fun74()

;; TERM 178: '*PROLOG-FIRST*'(unfold('*WRAPPER-INNER-CALL*'(_B, callret(_C, _D), _E), _F, _G, _H, _I)) :> []
c_code local build_ref_178
	ret_reg &ref[178]
	call_c   build_ref_177()
	call_c   Dyam_Create_Binary(I(9),&ref[177],I(0))
	move_ret ref[178]
	c_ret

;; TERM 177: '*PROLOG-FIRST*'(unfold('*WRAPPER-INNER-CALL*'(_B, callret(_C, _D), _E), _F, _G, _H, _I))
c_code local build_ref_177
	ret_reg &ref[177]
	call_c   build_ref_11()
	call_c   build_ref_176()
	call_c   Dyam_Create_Unary(&ref[11],&ref[176])
	move_ret ref[177]
	c_ret

c_code local build_seed_16
	ret_reg &seed[16]
	call_c   build_ref_35()
	call_c   build_ref_192()
	call_c   Dyam_Seed_Start(&ref[35],&ref[192],I(0),fun17,1)
	call_c   build_ref_193()
	call_c   Dyam_Seed_Add_Comp(&ref[193],fun79,0)
	call_c   Dyam_Seed_End()
	move_ret seed[16]
	c_ret

;; TERM 193: '*CITEM*'(std_prolog_function(_B, _C, _D), _A)
c_code local build_ref_193
	ret_reg &ref[193]
	call_c   build_ref_39()
	call_c   build_ref_190()
	call_c   Dyam_Create_Binary(&ref[39],&ref[190],V(0))
	move_ret ref[193]
	c_ret

;; TERM 190: std_prolog_function(_B, _C, _D)
c_code local build_ref_190
	ret_reg &ref[190]
	call_c   build_ref_327()
	call_c   Dyam_Term_Start(&ref[327],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[190]
	c_ret

;; TERM 201: '$CLOSURE'('$fun'(78, 0, 1133693384), '$TUPPLE'(35082884090280))
c_code local build_ref_201
	ret_reg &ref[201]
	call_c   build_ref_200()
	call_c   Dyam_Closure_Aux(fun78,&ref[200])
	move_ret ref[201]
	c_ret

;; TERM 197: 'pred_~w~w_~w'
c_code local build_ref_197
	ret_reg &ref[197]
	call_c   Dyam_Create_Atom("pred_~w~w_~w")
	move_ret ref[197]
	c_ret

;; TERM 198: [_G,_F,_C]
c_code local build_ref_198
	ret_reg &ref[198]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(2),I(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(5),R(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(6),R(0))
	move_ret ref[198]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun77[4]=[65538,build_ref_197,build_ref_198,pool_fun63]

pl_code local fun77
	call_c   Dyam_Remove_Choice()
	move     &ref[197], R(0)
	move     0, R(1)
	move     &ref[198], R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Load(4,V(3))
	pl_call  pred_name_builder_3()
	pl_jump  fun63()

;; TERM 194: '$ft'
c_code local build_ref_194
	ret_reg &ref[194]
	call_c   Dyam_Create_Atom("$ft")
	move_ret ref[194]
	c_ret

;; TERM 195: 'pred_~w__ft_~w'
c_code local build_ref_195
	ret_reg &ref[195]
	call_c   Dyam_Create_Atom("pred_~w__ft_~w")
	move_ret ref[195]
	c_ret

;; TERM 196: [_G,_C]
c_code local build_ref_196
	ret_reg &ref[196]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(2),I(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(6),R(0))
	move_ret ref[196]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun78[6]=[131075,build_ref_194,build_ref_195,build_ref_196,pool_fun77,pool_fun63]

pl_code local fun78
	call_c   Dyam_Pool(pool_fun78)
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun77)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(5),&ref[194])
	fail_ret
	call_c   Dyam_Cut()
	move     &ref[195], R(0)
	move     0, R(1)
	move     &ref[196], R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Load(4,V(3))
	pl_call  pred_name_builder_3()
	pl_jump  fun63()

;; TERM 200: '$TUPPLE'(35082884090280)
c_code local build_ref_200
	ret_reg &ref[200]
	call_c   Dyam_Create_Simple_Tupple(0,381681664)
	move_ret ref[200]
	c_ret

long local pool_fun79[3]=[2,build_ref_193,build_ref_201]

pl_code local fun79
	call_c   Dyam_Pool(pool_fun79)
	call_c   Dyam_Unify_Item(&ref[193])
	fail_ret
	call_c   DYAM_evpred_atom_to_module(V(1),V(4),V(5))
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[201], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Load(4,V(4))
	move     V(6), R(6)
	move     S(5), R(7)
	call_c   Dyam_Reg_Deallocate(4)
	pl_jump  fun66()

;; TERM 192: '*FIRST*'(std_prolog_function(_B, _C, _D)) :> []
c_code local build_ref_192
	ret_reg &ref[192]
	call_c   build_ref_191()
	call_c   Dyam_Create_Binary(I(9),&ref[191],I(0))
	move_ret ref[192]
	c_ret

;; TERM 191: '*FIRST*'(std_prolog_function(_B, _C, _D))
c_code local build_ref_191
	ret_reg &ref[191]
	call_c   build_ref_35()
	call_c   build_ref_190()
	call_c   Dyam_Create_Unary(&ref[35],&ref[190])
	move_ret ref[191]
	c_ret

c_code local build_seed_8
	ret_reg &seed[8]
	call_c   build_ref_10()
	call_c   build_ref_204()
	call_c   Dyam_Seed_Start(&ref[10],&ref[204],I(0),fun1,1)
	call_c   build_ref_205()
	call_c   Dyam_Seed_Add_Comp(&ref[205],fun84,0)
	call_c   Dyam_Seed_End()
	move_ret seed[8]
	c_ret

;; TERM 205: '*PROLOG-ITEM*'{top=> unfold('*WRAPPER-CALL-ALT*'(_B, callret(_C, _D), _E), _F, _G, _H, _I), cont=> _A}
c_code local build_ref_205
	ret_reg &ref[205]
	call_c   build_ref_329()
	call_c   build_ref_202()
	call_c   Dyam_Create_Binary(&ref[329],&ref[202],V(0))
	move_ret ref[205]
	c_ret

;; TERM 202: unfold('*WRAPPER-CALL-ALT*'(_B, callret(_C, _D), _E), _F, _G, _H, _I)
c_code local build_ref_202
	ret_reg &ref[202]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_339()
	call_c   Dyam_Create_Binary(&ref[339],V(2),V(3))
	move_ret R(0)
	call_c   build_ref_351()
	call_c   Dyam_Term_Start(&ref[351],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_330()
	call_c   Dyam_Term_Start(&ref[330],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_End()
	move_ret ref[202]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 217: '$CLOSURE'('$fun'(83, 0, 1133744812), '$TUPPLE'(35082884090540))
c_code local build_ref_217
	ret_reg &ref[217]
	call_c   build_ref_216()
	call_c   Dyam_Closure_Aux(fun83,&ref[216])
	move_ret ref[217]
	c_ret

c_code local build_seed_48
	ret_reg &seed[48]
	call_c   build_ref_55()
	call_c   build_ref_211()
	call_c   Dyam_Seed_Start(&ref[55],&ref[211],I(0),fun8,1)
	call_c   build_ref_214()
	call_c   Dyam_Seed_Add_Comp(&ref[214],&ref[211],0)
	call_c   Dyam_Seed_End()
	move_ret seed[48]
	c_ret

;; TERM 214: '*PROLOG-FIRST*'(build_args_closure(_E, callret(_C, _D), _F, _K, _L, _I)) :> '$$HOLE$$'
c_code local build_ref_214
	ret_reg &ref[214]
	call_c   build_ref_213()
	call_c   Dyam_Create_Binary(I(9),&ref[213],I(7))
	move_ret ref[214]
	c_ret

;; TERM 213: '*PROLOG-FIRST*'(build_args_closure(_E, callret(_C, _D), _F, _K, _L, _I))
c_code local build_ref_213
	ret_reg &ref[213]
	call_c   build_ref_11()
	call_c   build_ref_212()
	call_c   Dyam_Create_Unary(&ref[11],&ref[212])
	move_ret ref[213]
	c_ret

;; TERM 212: build_args_closure(_E, callret(_C, _D), _F, _K, _L, _I)
c_code local build_ref_212
	ret_reg &ref[212]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_339()
	call_c   Dyam_Create_Binary(&ref[339],V(2),V(3))
	move_ret R(0)
	call_c   build_ref_357()
	call_c   Dyam_Term_Start(&ref[357],6)
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_End()
	move_ret ref[212]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 357: build_args_closure
c_code local build_ref_357
	ret_reg &ref[357]
	call_c   Dyam_Create_Atom("build_args_closure")
	move_ret ref[357]
	c_ret

;; TERM 211: '*PROLOG-ITEM*'{top=> build_args_closure(_E, callret(_C, _D), _F, _K, _L, _I), cont=> '$CLOSURE'('$fun'(81, 0, 1133729320), '$TUPPLE'(35082884090496))}
c_code local build_ref_211
	ret_reg &ref[211]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,350879744)
	move_ret R(0)
	call_c   Dyam_Closure_Aux(fun81,R(0))
	move_ret R(0)
	call_c   build_ref_329()
	call_c   build_ref_212()
	call_c   Dyam_Create_Binary(&ref[329],&ref[212],R(0))
	move_ret ref[211]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 210: '$CLOSURE'(_N, _M)
c_code local build_ref_210
	ret_reg &ref[210]
	call_c   build_ref_116()
	call_c   Dyam_Create_Binary(&ref[116],V(13),V(12))
	move_ret ref[210]
	c_ret

;; TERM 206: [_F,_L|_C]
c_code local build_ref_206
	ret_reg &ref[206]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(11),V(2))
	move_ret R(0)
	call_c   Dyam_Create_List(V(5),R(0))
	move_ret ref[206]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_47
	ret_reg &seed[47]
	call_c   build_ref_17()
	call_c   build_ref_208()
	call_c   Dyam_Seed_Start(&ref[17],&ref[208],I(0),fun8,1)
	call_c   build_ref_209()
	call_c   Dyam_Seed_Add_Comp(&ref[209],&ref[208],0)
	call_c   Dyam_Seed_End()
	move_ret seed[47]
	c_ret

;; TERM 209: '*GUARD*'(std_prolog_load_args_alt([_L|_C], 0, _H, (call(_J, []) :> reg_reset(_Q) :> noop), _G, _O)) :> '$$HOLE$$'
c_code local build_ref_209
	ret_reg &ref[209]
	call_c   build_ref_208()
	call_c   Dyam_Create_Binary(I(9),&ref[208],I(7))
	move_ret ref[209]
	c_ret

;; TERM 208: '*GUARD*'(std_prolog_load_args_alt([_L|_C], 0, _H, (call(_J, []) :> reg_reset(_Q) :> noop), _G, _O))
c_code local build_ref_208
	ret_reg &ref[208]
	call_c   build_ref_17()
	call_c   build_ref_207()
	call_c   Dyam_Create_Unary(&ref[17],&ref[207])
	move_ret ref[208]
	c_ret

;; TERM 207: std_prolog_load_args_alt([_L|_C], 0, _H, (call(_J, []) :> reg_reset(_Q) :> noop), _G, _O)
c_code local build_ref_207
	ret_reg &ref[207]
	call_c   Dyam_Pseudo_Choice(3)
	call_c   Dyam_Create_List(V(11),V(2))
	move_ret R(0)
	call_c   build_ref_349()
	call_c   Dyam_Create_Binary(&ref[349],V(9),I(0))
	move_ret R(1)
	call_c   build_ref_350()
	call_c   Dyam_Create_Unary(&ref[350],V(16))
	move_ret R(2)
	call_c   build_ref_302()
	call_c   Dyam_Create_Binary(I(9),R(2),&ref[302])
	move_ret R(2)
	call_c   Dyam_Create_Binary(I(9),R(1),R(2))
	move_ret R(2)
	call_c   build_ref_334()
	call_c   Dyam_Term_Start(&ref[334],6)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(N(0))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(R(2))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_End()
	move_ret ref[207]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun80[4]=[3,build_ref_206,build_ref_73,build_seed_47]

long local pool_fun81[3]=[65537,build_ref_210,pool_fun80]

pl_code local fun81
	call_c   Dyam_Pool(pool_fun81)
	call_c   Dyam_Unify(V(11),&ref[210])
	fail_ret
	call_c   Dyam_Allocate(0)
fun80:
	move     &ref[206], R(0)
	move     S(5), R(1)
	move     V(14), R(2)
	move     S(5), R(3)
	pl_call  pred_duplicate_vars_2()
	call_c   DYAM_evpred_length(V(2),V(15))
	fail_ret
	call_c   DYAM_evpred_is(V(16),&ref[73])
	fail_ret
	pl_call  fun9(&seed[47],1)
	call_c   Dyam_Reg_Load(0,V(11))
	move     V(17), R(2)
	move     S(5), R(3)
	pl_call  pred_label_term_2()
	call_c   Dyam_Deallocate()
	pl_jump  Follow_Cont(V(0))


long local pool_fun82[2]=[1,build_seed_48]

pl_code local fun82
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Deallocate()
	pl_jump  fun9(&seed[48],12)

long local pool_fun83[4]=[131073,build_ref_71,pool_fun82,pool_fun80]

pl_code local fun83
	call_c   Dyam_Pool(pool_fun83)
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(2))
	call_c   Dyam_Reg_Load(2,V(6))
	move     V(10), R(4)
	move     S(5), R(5)
	pl_call  pred_extend_tupple_3()
	call_c   Dyam_Choice(fun82)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(4),&ref[71])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(11),V(5))
	fail_ret
	call_c   Dyam_Unify(V(8),V(5))
	fail_ret
	move     V(12), R(0)
	move     S(5), R(1)
	pl_call  pred_null_tupple_1()
	pl_jump  fun80()

;; TERM 216: '$TUPPLE'(35082884090540)
c_code local build_ref_216
	ret_reg &ref[216]
	call_c   Dyam_Create_Simple_Tupple(0,402128896)
	move_ret ref[216]
	c_ret

long local pool_fun84[3]=[2,build_ref_205,build_ref_217]

pl_code local fun84
	call_c   Dyam_Pool(pool_fun84)
	call_c   Dyam_Unify_Item(&ref[205])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[217], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Load(4,V(1))
	move     V(9), R(6)
	move     S(5), R(7)
	call_c   Dyam_Reg_Deallocate(4)
	pl_jump  fun19()

;; TERM 204: '*PROLOG-FIRST*'(unfold('*WRAPPER-CALL-ALT*'(_B, callret(_C, _D), _E), _F, _G, _H, _I)) :> []
c_code local build_ref_204
	ret_reg &ref[204]
	call_c   build_ref_203()
	call_c   Dyam_Create_Binary(I(9),&ref[203],I(0))
	move_ret ref[204]
	c_ret

;; TERM 203: '*PROLOG-FIRST*'(unfold('*WRAPPER-CALL-ALT*'(_B, callret(_C, _D), _E), _F, _G, _H, _I))
c_code local build_ref_203
	ret_reg &ref[203]
	call_c   build_ref_11()
	call_c   build_ref_202()
	call_c   Dyam_Create_Unary(&ref[11],&ref[202])
	move_ret ref[203]
	c_ret

c_code local build_seed_18
	ret_reg &seed[18]
	call_c   build_ref_16()
	call_c   build_ref_220()
	call_c   Dyam_Seed_Start(&ref[16],&ref[220],I(0),fun1,1)
	call_c   build_ref_219()
	call_c   Dyam_Seed_Add_Comp(&ref[219],fun89,0)
	call_c   Dyam_Seed_End()
	move_ret seed[18]
	c_ret

;; TERM 219: '*GUARD*'(std_prolog_load_args([_B|_C], _D, (_E :> _F), _G, _H))
c_code local build_ref_219
	ret_reg &ref[219]
	call_c   build_ref_17()
	call_c   build_ref_218()
	call_c   Dyam_Create_Unary(&ref[17],&ref[218])
	move_ret ref[219]
	c_ret

;; TERM 218: std_prolog_load_args([_B|_C], _D, (_E :> _F), _G, _H)
c_code local build_ref_218
	ret_reg &ref[218]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   Dyam_Create_List(V(1),V(2))
	move_ret R(0)
	call_c   Dyam_Create_Binary(I(9),V(4),V(5))
	move_ret R(1)
	call_c   build_ref_332()
	call_c   Dyam_Term_Start(&ref[332],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[218]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 221: _D + 1
c_code local build_ref_221
	ret_reg &ref[221]
	call_c   build_ref_348()
	call_c   Dyam_Create_Binary(&ref[348],V(3),N(1))
	move_ret ref[221]
	c_ret

;; TERM 223: _D * 2
c_code local build_ref_223
	ret_reg &ref[223]
	call_c   build_ref_222()
	call_c   Dyam_Create_Binary(&ref[222],V(3),N(2))
	move_ret ref[223]
	c_ret

;; TERM 222: *
c_code local build_ref_222
	ret_reg &ref[222]
	call_c   Dyam_Create_Atom("*")
	move_ret ref[222]
	c_ret

;; TERM 230: reg_load_var(_J, _K)
c_code local build_ref_230
	ret_reg &ref[230]
	call_c   build_ref_358()
	call_c   Dyam_Create_Binary(&ref[358],V(9),V(10))
	move_ret ref[230]
	c_ret

;; TERM 358: reg_load_var
c_code local build_ref_358
	ret_reg &ref[358]
	call_c   Dyam_Create_Atom("reg_load_var")
	move_ret ref[358]
	c_ret

c_code local build_seed_49
	ret_reg &seed[49]
	call_c   build_ref_17()
	call_c   build_ref_226()
	call_c   Dyam_Seed_Start(&ref[17],&ref[226],I(0),fun8,1)
	call_c   build_ref_227()
	call_c   Dyam_Seed_Add_Comp(&ref[227],&ref[226],0)
	call_c   Dyam_Seed_End()
	move_ret seed[49]
	c_ret

;; TERM 227: '*GUARD*'(std_prolog_load_args(_C, _I, _F, _G, _H)) :> '$$HOLE$$'
c_code local build_ref_227
	ret_reg &ref[227]
	call_c   build_ref_226()
	call_c   Dyam_Create_Binary(I(9),&ref[226],I(7))
	move_ret ref[227]
	c_ret

;; TERM 226: '*GUARD*'(std_prolog_load_args(_C, _I, _F, _G, _H))
c_code local build_ref_226
	ret_reg &ref[226]
	call_c   build_ref_17()
	call_c   build_ref_225()
	call_c   Dyam_Create_Unary(&ref[17],&ref[225])
	move_ret ref[226]
	c_ret

;; TERM 225: std_prolog_load_args(_C, _I, _F, _G, _H)
c_code local build_ref_225
	ret_reg &ref[225]
	call_c   build_ref_332()
	call_c   Dyam_Term_Start(&ref[332],5)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[225]
	c_ret

long local pool_fun85[2]=[1,build_seed_49]

long local pool_fun86[3]=[65537,build_ref_230,pool_fun85]

pl_code local fun86
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(4),&ref[230])
	fail_ret
fun85:
	call_c   Dyam_Deallocate()
	pl_jump  fun9(&seed[49],1)


;; TERM 229: reg_load(_J, _K)
c_code local build_ref_229
	ret_reg &ref[229]
	call_c   build_ref_359()
	call_c   Dyam_Create_Binary(&ref[359],V(9),V(10))
	move_ret ref[229]
	c_ret

;; TERM 359: reg_load
c_code local build_ref_359
	ret_reg &ref[359]
	call_c   Dyam_Create_Atom("reg_load")
	move_ret ref[359]
	c_ret

long local pool_fun87[4]=[131073,build_ref_229,pool_fun86,pool_fun85]

pl_code local fun87
	call_c   Dyam_Update_Choice(fun86)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(7))
	pl_call  pred_var_in_tupple_2()
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(4),&ref[229])
	fail_ret
	pl_jump  fun85()

;; TERM 228: reg_load_cst(_J, _K)
c_code local build_ref_228
	ret_reg &ref[228]
	call_c   build_ref_360()
	call_c   Dyam_Create_Binary(&ref[360],V(9),V(10))
	move_ret ref[228]
	c_ret

;; TERM 360: reg_load_cst
c_code local build_ref_360
	ret_reg &ref[360]
	call_c   Dyam_Create_Atom("reg_load_cst")
	move_ret ref[360]
	c_ret

long local pool_fun88[4]=[131073,build_ref_228,pool_fun87,pool_fun85]

pl_code local fun88
	call_c   Dyam_Update_Choice(fun87)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_atomic(V(1))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(4),&ref[228])
	fail_ret
	pl_jump  fun85()

;; TERM 224: reg_load_nil(_J)
c_code local build_ref_224
	ret_reg &ref[224]
	call_c   build_ref_361()
	call_c   Dyam_Create_Unary(&ref[361],V(9))
	move_ret ref[224]
	c_ret

;; TERM 361: reg_load_nil
c_code local build_ref_361
	ret_reg &ref[361]
	call_c   Dyam_Create_Atom("reg_load_nil")
	move_ret ref[361]
	c_ret

long local pool_fun89[7]=[131076,build_ref_219,build_ref_221,build_ref_223,build_ref_224,pool_fun88,pool_fun85]

pl_code local fun89
	call_c   Dyam_Pool(pool_fun89)
	call_c   Dyam_Unify_Item(&ref[219])
	fail_ret
	call_c   DYAM_evpred_is(V(8),&ref[221])
	fail_ret
	call_c   DYAM_evpred_is(V(9),&ref[223])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(1))
	move     V(10), R(2)
	move     S(5), R(3)
	pl_call  pred_label_term_2()
	call_c   Dyam_Choice(fun88)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(1),I(0))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(4),&ref[224])
	fail_ret
	pl_jump  fun85()

;; TERM 220: '*GUARD*'(std_prolog_load_args([_B|_C], _D, (_E :> _F), _G, _H)) :> []
c_code local build_ref_220
	ret_reg &ref[220]
	call_c   build_ref_219()
	call_c   Dyam_Create_Binary(I(9),&ref[219],I(0))
	move_ret ref[220]
	c_ret

c_code local build_seed_20
	ret_reg &seed[20]
	call_c   build_ref_16()
	call_c   build_ref_233()
	call_c   Dyam_Seed_Start(&ref[16],&ref[233],I(0),fun1,1)
	call_c   build_ref_232()
	call_c   Dyam_Seed_Add_Comp(&ref[232],fun103,0)
	call_c   Dyam_Seed_End()
	move_ret seed[20]
	c_ret

;; TERM 232: '*GUARD*'(std_prolog_unif_args([_B|_C], _D, _E, _F, _G))
c_code local build_ref_232
	ret_reg &ref[232]
	call_c   build_ref_17()
	call_c   build_ref_231()
	call_c   Dyam_Create_Unary(&ref[17],&ref[231])
	move_ret ref[232]
	c_ret

;; TERM 231: std_prolog_unif_args([_B|_C], _D, _E, _F, _G)
c_code local build_ref_231
	ret_reg &ref[231]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(1),V(2))
	move_ret R(0)
	call_c   build_ref_333()
	call_c   Dyam_Term_Start(&ref[333],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[231]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 262: reg_unif(_I, _J)
c_code local build_ref_262
	ret_reg &ref[262]
	call_c   build_ref_362()
	call_c   Dyam_Create_Binary(&ref[362],V(8),V(9))
	move_ret ref[262]
	c_ret

;; TERM 362: reg_unif
c_code local build_ref_362
	ret_reg &ref[362]
	call_c   Dyam_Create_Atom("reg_unif")
	move_ret ref[362]
	c_ret

c_code local build_seed_50
	ret_reg &seed[50]
	call_c   build_ref_17()
	call_c   build_ref_236()
	call_c   Dyam_Seed_Start(&ref[17],&ref[236],I(0),fun8,1)
	call_c   build_ref_237()
	call_c   Dyam_Seed_Add_Comp(&ref[237],&ref[236],0)
	call_c   Dyam_Seed_End()
	move_ret seed[50]
	c_ret

;; TERM 237: '*GUARD*'(std_prolog_unif_args(_C, _H, _M, _F, _L)) :> '$$HOLE$$'
c_code local build_ref_237
	ret_reg &ref[237]
	call_c   build_ref_236()
	call_c   Dyam_Create_Binary(I(9),&ref[236],I(7))
	move_ret ref[237]
	c_ret

;; TERM 236: '*GUARD*'(std_prolog_unif_args(_C, _H, _M, _F, _L))
c_code local build_ref_236
	ret_reg &ref[236]
	call_c   build_ref_17()
	call_c   build_ref_235()
	call_c   Dyam_Create_Unary(&ref[17],&ref[235])
	move_ret ref[236]
	c_ret

;; TERM 235: std_prolog_unif_args(_C, _H, _M, _F, _L)
c_code local build_ref_235
	ret_reg &ref[235]
	call_c   build_ref_333()
	call_c   Dyam_Term_Start(&ref[333],5)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_End()
	move_ret ref[235]
	c_ret

;; TERM 260: _K :> _M
c_code local build_ref_260
	ret_reg &ref[260]
	call_c   Dyam_Create_Binary(I(9),V(10),V(12))
	move_ret ref[260]
	c_ret

long local pool_fun95[2]=[1,build_ref_260]

pl_code local fun95
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(4),&ref[260])
	fail_ret
fun90:
	call_c   Dyam_Deallocate()
	pl_ret


;; TERM 252: reg_multi_bind(_N, _Q, _S) :> _P
c_code local build_ref_252
	ret_reg &ref[252]
	call_c   build_ref_251()
	call_c   Dyam_Create_Binary(I(9),&ref[251],V(15))
	move_ret ref[252]
	c_ret

;; TERM 251: reg_multi_bind(_N, _Q, _S)
c_code local build_ref_251
	ret_reg &ref[251]
	call_c   build_ref_363()
	call_c   Dyam_Term_Start(&ref[363],3)
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_End()
	move_ret ref[251]
	c_ret

;; TERM 363: reg_multi_bind
c_code local build_ref_363
	ret_reg &ref[363]
	call_c   Dyam_Create_Atom("reg_multi_bind")
	move_ret ref[363]
	c_ret

;; TERM 240: reg_bind(_I, _J)
c_code local build_ref_240
	ret_reg &ref[240]
	call_c   build_ref_364()
	call_c   Dyam_Create_Binary(&ref[364],V(8),V(9))
	move_ret ref[240]
	c_ret

;; TERM 364: reg_bind
c_code local build_ref_364
	ret_reg &ref[364]
	call_c   Dyam_Create_Atom("reg_bind")
	move_ret ref[364]
	c_ret

;; TERM 241: _I + 2
c_code local build_ref_241
	ret_reg &ref[241]
	call_c   build_ref_348()
	call_c   Dyam_Create_Binary(&ref[348],V(8),N(2))
	move_ret ref[241]
	c_ret

;; TERM 243: _S43569419
c_code local build_ref_243
	ret_reg &ref[243]
	call_c   Dyam_Create_Unary(I(6),V(17))
	move_ret ref[243]
	c_ret

;; TERM 244: _R + 1
c_code local build_ref_244
	ret_reg &ref[244]
	call_c   build_ref_348()
	call_c   Dyam_Create_Binary(&ref[348],V(17),N(1))
	move_ret ref[244]
	c_ret

;; TERM 253: _S + 1
c_code local build_ref_253
	ret_reg &ref[253]
	call_c   build_ref_348()
	call_c   Dyam_Create_Binary(&ref[348],V(18),N(1))
	move_ret ref[253]
	c_ret

;; TERM 259: reg_multi_bind(_I, _R, _T) :> _P
c_code local build_ref_259
	ret_reg &ref[259]
	call_c   build_ref_258()
	call_c   Dyam_Create_Binary(I(9),&ref[258],V(15))
	move_ret ref[259]
	c_ret

;; TERM 258: reg_multi_bind(_I, _R, _T)
c_code local build_ref_258
	ret_reg &ref[258]
	call_c   build_ref_363()
	call_c   Dyam_Term_Start(&ref[363],3)
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_End()
	move_ret ref[258]
	c_ret

long local pool_fun94[2]=[1,build_ref_259]

pl_code local fun94
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(4),&ref[259])
	fail_ret
	pl_jump  fun90()

;; TERM 257: reg_multi_bind_zero(_R, _T) :> _P
c_code local build_ref_257
	ret_reg &ref[257]
	call_c   build_ref_256()
	call_c   Dyam_Create_Binary(I(9),&ref[256],V(15))
	move_ret ref[257]
	c_ret

;; TERM 256: reg_multi_bind_zero(_R, _T)
c_code local build_ref_256
	ret_reg &ref[256]
	call_c   build_ref_365()
	call_c   Dyam_Create_Binary(&ref[365],V(17),V(19))
	move_ret ref[256]
	c_ret

;; TERM 365: reg_multi_bind_zero
c_code local build_ref_365
	ret_reg &ref[365]
	call_c   Dyam_Create_Atom("reg_multi_bind_zero")
	move_ret ref[365]
	c_ret

long local pool_fun93[2]=[1,build_ref_257]

pl_code local fun93
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(4),&ref[257])
	fail_ret
	pl_jump  fun90()

;; TERM 255: reg_multi_bind_zero_one(_T) :> _P
c_code local build_ref_255
	ret_reg &ref[255]
	call_c   build_ref_254()
	call_c   Dyam_Create_Binary(I(9),&ref[254],V(15))
	move_ret ref[255]
	c_ret

;; TERM 254: reg_multi_bind_zero_one(_T)
c_code local build_ref_254
	ret_reg &ref[254]
	call_c   build_ref_366()
	call_c   Dyam_Create_Unary(&ref[366],V(19))
	move_ret ref[254]
	c_ret

;; TERM 366: reg_multi_bind_zero_one
c_code local build_ref_366
	ret_reg &ref[366]
	call_c   Dyam_Create_Atom("reg_multi_bind_zero_one")
	move_ret ref[366]
	c_ret

long local pool_fun96[11]=[196615,build_ref_252,build_ref_240,build_ref_241,build_ref_243,build_ref_244,build_ref_253,build_ref_255,pool_fun95,pool_fun94,pool_fun93]

pl_code local fun96
	call_c   Dyam_Update_Choice(fun95)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(12),&ref[252])
	fail_ret
	call_c   Dyam_Unify(V(10),&ref[240])
	fail_ret
	call_c   DYAM_evpred_is(V(13),&ref[241])
	fail_ret
	call_c   Dyam_Unify(V(9),&ref[243])
	fail_ret
	call_c   DYAM_evpred_is(V(16),&ref[244])
	fail_ret
	call_c   Dyam_Cut()
	call_c   DYAM_evpred_is(V(19),&ref[253])
	fail_ret
	call_c   Dyam_Choice(fun94)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(8),N(0))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun93)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(17),N(1))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(4),&ref[255])
	fail_ret
	pl_jump  fun90()

;; TERM 239: reg_bind(_N, _O) :> _P
c_code local build_ref_239
	ret_reg &ref[239]
	call_c   build_ref_238()
	call_c   Dyam_Create_Binary(I(9),&ref[238],V(15))
	move_ret ref[239]
	c_ret

;; TERM 238: reg_bind(_N, _O)
c_code local build_ref_238
	ret_reg &ref[238]
	call_c   build_ref_364()
	call_c   Dyam_Create_Binary(&ref[364],V(13),V(14))
	move_ret ref[238]
	c_ret

;; TERM 242: _Q43568088
c_code local build_ref_242
	ret_reg &ref[242]
	call_c   Dyam_Create_Unary(I(6),V(16))
	move_ret ref[242]
	c_ret

;; TERM 250: reg_multi_bind(_I, _R, 2) :> _P
c_code local build_ref_250
	ret_reg &ref[250]
	call_c   build_ref_249()
	call_c   Dyam_Create_Binary(I(9),&ref[249],V(15))
	move_ret ref[250]
	c_ret

;; TERM 249: reg_multi_bind(_I, _R, 2)
c_code local build_ref_249
	ret_reg &ref[249]
	call_c   build_ref_363()
	call_c   Dyam_Term_Start(&ref[363],3)
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(N(2))
	call_c   Dyam_Term_End()
	move_ret ref[249]
	c_ret

long local pool_fun92[2]=[1,build_ref_250]

pl_code local fun92
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(4),&ref[250])
	fail_ret
	pl_jump  fun90()

;; TERM 248: reg_multi_bind_zero(_R, 2) :> _P
c_code local build_ref_248
	ret_reg &ref[248]
	call_c   build_ref_247()
	call_c   Dyam_Create_Binary(I(9),&ref[247],V(15))
	move_ret ref[248]
	c_ret

;; TERM 247: reg_multi_bind_zero(_R, 2)
c_code local build_ref_247
	ret_reg &ref[247]
	call_c   build_ref_365()
	call_c   Dyam_Create_Binary(&ref[365],V(17),N(2))
	move_ret ref[247]
	c_ret

long local pool_fun91[2]=[1,build_ref_248]

pl_code local fun91
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(4),&ref[248])
	fail_ret
	pl_jump  fun90()

;; TERM 246: reg_multi_bind_zero_one(2) :> _P
c_code local build_ref_246
	ret_reg &ref[246]
	call_c   build_ref_245()
	call_c   Dyam_Create_Binary(I(9),&ref[245],V(15))
	move_ret ref[246]
	c_ret

;; TERM 245: reg_multi_bind_zero_one(2)
c_code local build_ref_245
	ret_reg &ref[245]
	call_c   build_ref_366()
	call_c   Dyam_Create_Unary(&ref[366],N(2))
	move_ret ref[245]
	c_ret

long local pool_fun97[12]=[196616,build_seed_50,build_ref_239,build_ref_240,build_ref_241,build_ref_242,build_ref_243,build_ref_244,build_ref_246,pool_fun96,pool_fun92,pool_fun91]

long local pool_fun100[3]=[65537,build_ref_262,pool_fun97]

pl_code local fun100
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(10),&ref[262])
	fail_ret
fun97:
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(6))
	move     V(11), R(4)
	move     S(5), R(5)
	pl_call  pred_extend_tupple_3()
	pl_call  fun9(&seed[50],1)
	call_c   Dyam_Choice(fun96)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(12),&ref[239])
	fail_ret
	call_c   Dyam_Unify(V(10),&ref[240])
	fail_ret
	call_c   DYAM_evpred_is(V(13),&ref[241])
	fail_ret
	call_c   Dyam_Unify(V(14),&ref[242])
	fail_ret
	call_c   Dyam_Unify(V(9),&ref[243])
	fail_ret
	call_c   DYAM_evpred_is(V(16),&ref[244])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun92)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(8),N(0))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun91)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(17),N(1))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(4),&ref[246])
	fail_ret
	pl_jump  fun90()


long local pool_fun98[3]=[65537,build_ref_240,pool_fun97]

pl_code local fun99
	call_c   Dyam_Remove_Choice()
fun98:
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(10),&ref[240])
	fail_ret
	pl_jump  fun97()


long local pool_fun101[3]=[131072,pool_fun100,pool_fun98]

pl_code local fun101
	call_c   Dyam_Update_Choice(fun100)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Reg_Load(0,V(1))
	pl_call  pred_check_var_1()
	call_c   Dyam_Choice(fun99)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(6))
	pl_call  pred_var_in_tupple_2()
	call_c   Dyam_Cut()
	pl_fail

;; TERM 261: reg_unif_cst(_I, _J)
c_code local build_ref_261
	ret_reg &ref[261]
	call_c   build_ref_367()
	call_c   Dyam_Create_Binary(&ref[367],V(8),V(9))
	move_ret ref[261]
	c_ret

;; TERM 367: reg_unif_cst
c_code local build_ref_367
	ret_reg &ref[367]
	call_c   Dyam_Create_Atom("reg_unif_cst")
	move_ret ref[367]
	c_ret

long local pool_fun102[4]=[131073,build_ref_261,pool_fun101,pool_fun97]

pl_code local fun102
	call_c   Dyam_Update_Choice(fun101)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_atomic(V(1))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(10),&ref[261])
	fail_ret
	pl_jump  fun97()

;; TERM 234: reg_unif_nil(_I)
c_code local build_ref_234
	ret_reg &ref[234]
	call_c   build_ref_368()
	call_c   Dyam_Create_Unary(&ref[368],V(8))
	move_ret ref[234]
	c_ret

;; TERM 368: reg_unif_nil
c_code local build_ref_368
	ret_reg &ref[368]
	call_c   Dyam_Create_Atom("reg_unif_nil")
	move_ret ref[368]
	c_ret

long local pool_fun103[7]=[131076,build_ref_232,build_ref_221,build_ref_223,build_ref_234,pool_fun102,pool_fun97]

pl_code local fun103
	call_c   Dyam_Pool(pool_fun103)
	call_c   Dyam_Unify_Item(&ref[232])
	fail_ret
	call_c   DYAM_evpred_is(V(7),&ref[221])
	fail_ret
	call_c   DYAM_evpred_is(V(8),&ref[223])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(1))
	move     V(9), R(2)
	move     S(5), R(3)
	pl_call  pred_label_term_2()
	call_c   Dyam_Choice(fun102)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(1),I(0))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(10),&ref[234])
	fail_ret
	pl_jump  fun97()

;; TERM 233: '*GUARD*'(std_prolog_unif_args([_B|_C], _D, _E, _F, _G)) :> []
c_code local build_ref_233
	ret_reg &ref[233]
	call_c   build_ref_232()
	call_c   Dyam_Create_Binary(I(9),&ref[232],I(0))
	move_ret ref[233]
	c_ret

c_code local build_seed_17
	ret_reg &seed[17]
	call_c   build_ref_16()
	call_c   build_ref_265()
	call_c   Dyam_Seed_Start(&ref[16],&ref[265],I(0),fun1,1)
	call_c   build_ref_264()
	call_c   Dyam_Seed_Add_Comp(&ref[264],fun111,0)
	call_c   Dyam_Seed_End()
	move_ret seed[17]
	c_ret

;; TERM 264: '*GUARD*'(std_prolog_load_args_alt([_B|_C], _D, (_E :> _F), _G, _H, _I))
c_code local build_ref_264
	ret_reg &ref[264]
	call_c   build_ref_17()
	call_c   build_ref_263()
	call_c   Dyam_Create_Unary(&ref[17],&ref[263])
	move_ret ref[264]
	c_ret

;; TERM 263: std_prolog_load_args_alt([_B|_C], _D, (_E :> _F), _G, _H, _I)
c_code local build_ref_263
	ret_reg &ref[263]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   Dyam_Create_List(V(1),V(2))
	move_ret R(0)
	call_c   Dyam_Create_Binary(I(9),V(4),V(5))
	move_ret R(1)
	call_c   build_ref_334()
	call_c   Dyam_Term_Start(&ref[334],6)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_End()
	move_ret ref[263]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 273: reg_load_var(_K, _L)
c_code local build_ref_273
	ret_reg &ref[273]
	call_c   build_ref_358()
	call_c   Dyam_Create_Binary(&ref[358],V(10),V(11))
	move_ret ref[273]
	c_ret

c_code local build_seed_51
	ret_reg &seed[51]
	call_c   build_ref_17()
	call_c   build_ref_268()
	call_c   Dyam_Seed_Start(&ref[17],&ref[268],I(0),fun8,1)
	call_c   build_ref_269()
	call_c   Dyam_Seed_Add_Comp(&ref[269],&ref[268],0)
	call_c   Dyam_Seed_End()
	move_ret seed[51]
	c_ret

;; TERM 269: '*GUARD*'(std_prolog_load_args_alt(_C, _J, _F, _G, _H, _I)) :> '$$HOLE$$'
c_code local build_ref_269
	ret_reg &ref[269]
	call_c   build_ref_268()
	call_c   Dyam_Create_Binary(I(9),&ref[268],I(7))
	move_ret ref[269]
	c_ret

;; TERM 268: '*GUARD*'(std_prolog_load_args_alt(_C, _J, _F, _G, _H, _I))
c_code local build_ref_268
	ret_reg &ref[268]
	call_c   build_ref_17()
	call_c   build_ref_267()
	call_c   Dyam_Create_Unary(&ref[17],&ref[267])
	move_ret ref[268]
	c_ret

;; TERM 267: std_prolog_load_args_alt(_C, _J, _F, _G, _H, _I)
c_code local build_ref_267
	ret_reg &ref[267]
	call_c   build_ref_334()
	call_c   Dyam_Term_Start(&ref[334],6)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_End()
	move_ret ref[267]
	c_ret

long local pool_fun104[2]=[1,build_seed_51]

long local pool_fun107[3]=[65537,build_ref_273,pool_fun104]

pl_code local fun107
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(4),&ref[273])
	fail_ret
fun104:
	call_c   Dyam_Deallocate()
	pl_jump  fun9(&seed[51],1)


;; TERM 272: reg_zero(_K)
c_code local build_ref_272
	ret_reg &ref[272]
	call_c   build_ref_369()
	call_c   Dyam_Create_Unary(&ref[369],V(10))
	move_ret ref[272]
	c_ret

;; TERM 369: reg_zero
c_code local build_ref_369
	ret_reg &ref[369]
	call_c   Dyam_Create_Atom("reg_zero")
	move_ret ref[369]
	c_ret

long local pool_fun105[3]=[65537,build_ref_272,pool_fun104]

pl_code local fun106
	call_c   Dyam_Remove_Choice()
fun105:
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(4),&ref[272])
	fail_ret
	pl_jump  fun104()


long local pool_fun108[3]=[131072,pool_fun107,pool_fun105]

pl_code local fun108
	call_c   Dyam_Update_Choice(fun107)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Reg_Load(0,V(1))
	pl_call  pred_check_var_1()
	call_c   Dyam_Choice(fun106)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(8))
	pl_call  pred_var_in_tupple_2()
	call_c   Dyam_Cut()
	pl_fail

;; TERM 271: reg_load(_K, _L)
c_code local build_ref_271
	ret_reg &ref[271]
	call_c   build_ref_359()
	call_c   Dyam_Create_Binary(&ref[359],V(10),V(11))
	move_ret ref[271]
	c_ret

long local pool_fun109[4]=[131073,build_ref_271,pool_fun108,pool_fun104]

pl_code local fun109
	call_c   Dyam_Update_Choice(fun108)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(7))
	pl_call  pred_var_in_tupple_2()
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(4),&ref[271])
	fail_ret
	pl_jump  fun104()

;; TERM 270: reg_load_cst(_K, _L)
c_code local build_ref_270
	ret_reg &ref[270]
	call_c   build_ref_360()
	call_c   Dyam_Create_Binary(&ref[360],V(10),V(11))
	move_ret ref[270]
	c_ret

long local pool_fun110[4]=[131073,build_ref_270,pool_fun109,pool_fun104]

pl_code local fun110
	call_c   Dyam_Update_Choice(fun109)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_atomic(V(1))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(4),&ref[270])
	fail_ret
	pl_jump  fun104()

;; TERM 266: reg_load_nil(_K)
c_code local build_ref_266
	ret_reg &ref[266]
	call_c   build_ref_361()
	call_c   Dyam_Create_Unary(&ref[361],V(10))
	move_ret ref[266]
	c_ret

long local pool_fun111[7]=[131076,build_ref_264,build_ref_221,build_ref_223,build_ref_266,pool_fun110,pool_fun104]

pl_code local fun111
	call_c   Dyam_Pool(pool_fun111)
	call_c   Dyam_Unify_Item(&ref[264])
	fail_ret
	call_c   DYAM_evpred_is(V(9),&ref[221])
	fail_ret
	call_c   DYAM_evpred_is(V(10),&ref[223])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(1))
	move     V(11), R(2)
	move     S(5), R(3)
	pl_call  pred_label_term_2()
	call_c   Dyam_Choice(fun110)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(1),I(0))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(4),&ref[266])
	fail_ret
	pl_jump  fun104()

;; TERM 265: '*GUARD*'(std_prolog_load_args_alt([_B|_C], _D, (_E :> _F), _G, _H, _I)) :> []
c_code local build_ref_265
	ret_reg &ref[265]
	call_c   build_ref_264()
	call_c   Dyam_Create_Binary(I(9),&ref[264],I(0))
	move_ret ref[265]
	c_ret

c_code local build_seed_19
	ret_reg &seed[19]
	call_c   build_ref_16()
	call_c   build_ref_276()
	call_c   Dyam_Seed_Start(&ref[16],&ref[276],I(0),fun1,1)
	call_c   build_ref_275()
	call_c   Dyam_Seed_Add_Comp(&ref[275],fun127,0)
	call_c   Dyam_Seed_End()
	move_ret seed[19]
	c_ret

;; TERM 275: '*GUARD*'(std_prolog_unif_args_alt([_B|_C], _D, _E, _F, _G, _H))
c_code local build_ref_275
	ret_reg &ref[275]
	call_c   build_ref_17()
	call_c   build_ref_274()
	call_c   Dyam_Create_Unary(&ref[17],&ref[274])
	move_ret ref[275]
	c_ret

;; TERM 274: std_prolog_unif_args_alt([_B|_C], _D, _E, _F, _G, _H)
c_code local build_ref_274
	ret_reg &ref[274]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(1),V(2))
	move_ret R(0)
	call_c   build_ref_335()
	call_c   Dyam_Term_Start(&ref[335],6)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[274]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 303: reg_unif(_J, _K)
c_code local build_ref_303
	ret_reg &ref[303]
	call_c   build_ref_362()
	call_c   Dyam_Create_Binary(&ref[362],V(9),V(10))
	move_ret ref[303]
	c_ret

c_code local build_seed_52
	ret_reg &seed[52]
	call_c   build_ref_17()
	call_c   build_ref_279()
	call_c   Dyam_Seed_Start(&ref[17],&ref[279],I(0),fun8,1)
	call_c   build_ref_280()
	call_c   Dyam_Seed_Add_Comp(&ref[280],&ref[279],0)
	call_c   Dyam_Seed_End()
	move_ret seed[52]
	c_ret

;; TERM 280: '*GUARD*'(std_prolog_unif_args_alt(_C, _I, _N, _F, _M, _H)) :> '$$HOLE$$'
c_code local build_ref_280
	ret_reg &ref[280]
	call_c   build_ref_279()
	call_c   Dyam_Create_Binary(I(9),&ref[279],I(7))
	move_ret ref[280]
	c_ret

;; TERM 279: '*GUARD*'(std_prolog_unif_args_alt(_C, _I, _N, _F, _M, _H))
c_code local build_ref_279
	ret_reg &ref[279]
	call_c   build_ref_17()
	call_c   build_ref_278()
	call_c   Dyam_Create_Unary(&ref[17],&ref[278])
	move_ret ref[279]
	c_ret

;; TERM 278: std_prolog_unif_args_alt(_C, _I, _N, _F, _M, _H)
c_code local build_ref_278
	ret_reg &ref[278]
	call_c   build_ref_335()
	call_c   Dyam_Term_Start(&ref[335],6)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[278]
	c_ret

;; TERM 300: _L :> _N
c_code local build_ref_300
	ret_reg &ref[300]
	call_c   Dyam_Create_Binary(I(9),V(11),V(13))
	move_ret ref[300]
	c_ret

long local pool_fun116[2]=[1,build_ref_300]

pl_code local fun116
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(4),&ref[300])
	fail_ret
	pl_jump  fun90()

;; TERM 292: reg_multi_bind(_O, _R, _T) :> _Q
c_code local build_ref_292
	ret_reg &ref[292]
	call_c   build_ref_291()
	call_c   Dyam_Create_Binary(I(9),&ref[291],V(16))
	move_ret ref[292]
	c_ret

;; TERM 291: reg_multi_bind(_O, _R, _T)
c_code local build_ref_291
	ret_reg &ref[291]
	call_c   build_ref_363()
	call_c   Dyam_Term_Start(&ref[363],3)
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_End()
	move_ret ref[291]
	c_ret

;; TERM 283: reg_bind(_J, _K)
c_code local build_ref_283
	ret_reg &ref[283]
	call_c   build_ref_364()
	call_c   Dyam_Create_Binary(&ref[364],V(9),V(10))
	move_ret ref[283]
	c_ret

;; TERM 284: _J + 2
c_code local build_ref_284
	ret_reg &ref[284]
	call_c   build_ref_348()
	call_c   Dyam_Create_Binary(&ref[348],V(9),N(2))
	move_ret ref[284]
	c_ret

;; TERM 285: _C43569403
c_code local build_ref_285
	ret_reg &ref[285]
	call_c   Dyam_Create_Unary(I(6),V(18))
	move_ret ref[285]
	c_ret

;; TERM 293: _T + 1
c_code local build_ref_293
	ret_reg &ref[293]
	call_c   build_ref_348()
	call_c   Dyam_Create_Binary(&ref[348],V(19),N(1))
	move_ret ref[293]
	c_ret

;; TERM 299: reg_multi_bind(_J, _S, _U) :> _Q
c_code local build_ref_299
	ret_reg &ref[299]
	call_c   build_ref_298()
	call_c   Dyam_Create_Binary(I(9),&ref[298],V(16))
	move_ret ref[299]
	c_ret

;; TERM 298: reg_multi_bind(_J, _S, _U)
c_code local build_ref_298
	ret_reg &ref[298]
	call_c   build_ref_363()
	call_c   Dyam_Term_Start(&ref[363],3)
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_End()
	move_ret ref[298]
	c_ret

long local pool_fun115[2]=[1,build_ref_299]

pl_code local fun115
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(4),&ref[299])
	fail_ret
	pl_jump  fun90()

;; TERM 297: reg_multi_bind_zero(_S, _U) :> _Q
c_code local build_ref_297
	ret_reg &ref[297]
	call_c   build_ref_296()
	call_c   Dyam_Create_Binary(I(9),&ref[296],V(16))
	move_ret ref[297]
	c_ret

;; TERM 296: reg_multi_bind_zero(_S, _U)
c_code local build_ref_296
	ret_reg &ref[296]
	call_c   build_ref_365()
	call_c   Dyam_Create_Binary(&ref[365],V(18),V(20))
	move_ret ref[296]
	c_ret

long local pool_fun114[2]=[1,build_ref_297]

pl_code local fun114
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(4),&ref[297])
	fail_ret
	pl_jump  fun90()

;; TERM 295: reg_multi_bind_zero_one(_U) :> _Q
c_code local build_ref_295
	ret_reg &ref[295]
	call_c   build_ref_294()
	call_c   Dyam_Create_Binary(I(9),&ref[294],V(16))
	move_ret ref[295]
	c_ret

;; TERM 294: reg_multi_bind_zero_one(_U)
c_code local build_ref_294
	ret_reg &ref[294]
	call_c   build_ref_366()
	call_c   Dyam_Create_Unary(&ref[366],V(20))
	move_ret ref[294]
	c_ret

long local pool_fun117[11]=[196615,build_ref_292,build_ref_283,build_ref_284,build_ref_285,build_ref_253,build_ref_293,build_ref_295,pool_fun116,pool_fun115,pool_fun114]

pl_code local fun117
	call_c   Dyam_Update_Choice(fun116)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(13),&ref[292])
	fail_ret
	call_c   Dyam_Unify(V(11),&ref[283])
	fail_ret
	call_c   DYAM_evpred_is(V(14),&ref[284])
	fail_ret
	call_c   Dyam_Unify(V(10),&ref[285])
	fail_ret
	call_c   DYAM_evpred_is(V(17),&ref[253])
	fail_ret
	call_c   Dyam_Cut()
	call_c   DYAM_evpred_is(V(20),&ref[293])
	fail_ret
	call_c   Dyam_Choice(fun115)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(9),N(0))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun114)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(18),N(1))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(4),&ref[295])
	fail_ret
	pl_jump  fun90()

;; TERM 282: reg_bind(_O, _P) :> _Q
c_code local build_ref_282
	ret_reg &ref[282]
	call_c   build_ref_281()
	call_c   Dyam_Create_Binary(I(9),&ref[281],V(16))
	move_ret ref[282]
	c_ret

;; TERM 281: reg_bind(_O, _P)
c_code local build_ref_281
	ret_reg &ref[281]
	call_c   build_ref_364()
	call_c   Dyam_Create_Binary(&ref[364],V(14),V(15))
	move_ret ref[281]
	c_ret

;; TERM 290: reg_multi_bind(_J, _S, 2) :> _Q
c_code local build_ref_290
	ret_reg &ref[290]
	call_c   build_ref_289()
	call_c   Dyam_Create_Binary(I(9),&ref[289],V(16))
	move_ret ref[290]
	c_ret

;; TERM 289: reg_multi_bind(_J, _S, 2)
c_code local build_ref_289
	ret_reg &ref[289]
	call_c   build_ref_363()
	call_c   Dyam_Term_Start(&ref[363],3)
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(N(2))
	call_c   Dyam_Term_End()
	move_ret ref[289]
	c_ret

long local pool_fun113[2]=[1,build_ref_290]

pl_code local fun113
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(4),&ref[290])
	fail_ret
	pl_jump  fun90()

;; TERM 288: reg_multi_bind_zero(_S, 2) :> _Q
c_code local build_ref_288
	ret_reg &ref[288]
	call_c   build_ref_287()
	call_c   Dyam_Create_Binary(I(9),&ref[287],V(16))
	move_ret ref[288]
	c_ret

;; TERM 287: reg_multi_bind_zero(_S, 2)
c_code local build_ref_287
	ret_reg &ref[287]
	call_c   build_ref_365()
	call_c   Dyam_Create_Binary(&ref[365],V(18),N(2))
	move_ret ref[287]
	c_ret

long local pool_fun112[2]=[1,build_ref_288]

pl_code local fun112
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(4),&ref[288])
	fail_ret
	pl_jump  fun90()

;; TERM 286: reg_multi_bind_zero_one(2) :> _Q
c_code local build_ref_286
	ret_reg &ref[286]
	call_c   build_ref_245()
	call_c   Dyam_Create_Binary(I(9),&ref[245],V(16))
	move_ret ref[286]
	c_ret

long local pool_fun118[12]=[196616,build_seed_52,build_ref_282,build_ref_283,build_ref_284,build_ref_243,build_ref_285,build_ref_253,build_ref_286,pool_fun117,pool_fun113,pool_fun112]

long local pool_fun123[3]=[65537,build_ref_303,pool_fun118]

pl_code local fun123
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(11),&ref[303])
	fail_ret
fun118:
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(6))
	move     V(12), R(4)
	move     S(5), R(5)
	pl_call  pred_extend_tupple_3()
	pl_call  fun9(&seed[52],1)
	call_c   Dyam_Choice(fun117)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(13),&ref[282])
	fail_ret
	call_c   Dyam_Unify(V(11),&ref[283])
	fail_ret
	call_c   DYAM_evpred_is(V(14),&ref[284])
	fail_ret
	call_c   Dyam_Unify(V(15),&ref[243])
	fail_ret
	call_c   Dyam_Unify(V(10),&ref[285])
	fail_ret
	call_c   DYAM_evpred_is(V(17),&ref[253])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun113)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(9),N(0))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun112)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(18),N(1))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(4),&ref[286])
	fail_ret
	pl_jump  fun90()


long local pool_fun121[3]=[65537,build_ref_283,pool_fun118]

pl_code local fun122
	call_c   Dyam_Remove_Choice()
fun121:
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(11),&ref[283])
	fail_ret
	pl_jump  fun118()


long local pool_fun124[3]=[131072,pool_fun123,pool_fun121]

pl_code local fun124
	call_c   Dyam_Update_Choice(fun123)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Reg_Load(0,V(1))
	pl_call  pred_check_var_1()
	call_c   Dyam_Choice(fun122)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(6))
	pl_call  pred_var_in_tupple_2()
	call_c   Dyam_Cut()
	pl_fail

long local pool_fun119[3]=[65537,build_ref_302,pool_fun118]

pl_code local fun120
	call_c   Dyam_Remove_Choice()
fun119:
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(11),&ref[302])
	fail_ret
	pl_jump  fun118()


long local pool_fun125[3]=[131072,pool_fun124,pool_fun119]

pl_code local fun125
	call_c   Dyam_Update_Choice(fun124)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Reg_Load(0,V(1))
	pl_call  pred_check_var_1()
	call_c   Dyam_Choice(fun120)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(7))
	pl_call  pred_var_in_tupple_2()
	call_c   Dyam_Cut()
	pl_fail

;; TERM 301: reg_unif_cst(_J, _K)
c_code local build_ref_301
	ret_reg &ref[301]
	call_c   build_ref_367()
	call_c   Dyam_Create_Binary(&ref[367],V(9),V(10))
	move_ret ref[301]
	c_ret

long local pool_fun126[4]=[131073,build_ref_301,pool_fun125,pool_fun118]

pl_code local fun126
	call_c   Dyam_Update_Choice(fun125)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_atomic(V(1))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(11),&ref[301])
	fail_ret
	pl_jump  fun118()

;; TERM 277: reg_unif_nil(_J)
c_code local build_ref_277
	ret_reg &ref[277]
	call_c   build_ref_368()
	call_c   Dyam_Create_Unary(&ref[368],V(9))
	move_ret ref[277]
	c_ret

long local pool_fun127[7]=[131076,build_ref_275,build_ref_221,build_ref_223,build_ref_277,pool_fun126,pool_fun118]

pl_code local fun127
	call_c   Dyam_Pool(pool_fun127)
	call_c   Dyam_Unify_Item(&ref[275])
	fail_ret
	call_c   DYAM_evpred_is(V(8),&ref[221])
	fail_ret
	call_c   DYAM_evpred_is(V(9),&ref[223])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(1))
	move     V(10), R(2)
	move     S(5), R(3)
	pl_call  pred_label_term_2()
	call_c   Dyam_Choice(fun126)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(1),I(0))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(11),&ref[277])
	fail_ret
	pl_jump  fun118()

;; TERM 276: '*GUARD*'(std_prolog_unif_args_alt([_B|_C], _D, _E, _F, _G, _H)) :> []
c_code local build_ref_276
	ret_reg &ref[276]
	call_c   build_ref_275()
	call_c   Dyam_Create_Binary(I(9),&ref[275],I(0))
	move_ret ref[276]
	c_ret

c_code local build_seed_22
	ret_reg &seed[22]
	call_c   build_ref_35()
	call_c   build_ref_306()
	call_c   Dyam_Seed_Start(&ref[35],&ref[306],I(0),fun17,1)
	call_c   build_ref_307()
	call_c   Dyam_Seed_Add_Comp(&ref[307],fun131,0)
	call_c   Dyam_Seed_End()
	move_ret seed[22]
	c_ret

;; TERM 307: '*CITEM*'(stdprolog_install('*STD-PROLOG-FIRST*'((_B / _C), _D, _E)), _A)
c_code local build_ref_307
	ret_reg &ref[307]
	call_c   build_ref_39()
	call_c   build_ref_304()
	call_c   Dyam_Create_Binary(&ref[39],&ref[304],V(0))
	move_ret ref[307]
	c_ret

;; TERM 304: stdprolog_install('*STD-PROLOG-FIRST*'((_B / _C), _D, _E))
c_code local build_ref_304
	ret_reg &ref[304]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_338()
	call_c   Dyam_Create_Binary(&ref[338],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_370()
	call_c   Dyam_Term_Start(&ref[370],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_328()
	call_c   Dyam_Create_Unary(&ref[328],R(0))
	move_ret ref[304]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 370: '*STD-PROLOG-FIRST*'
c_code local build_ref_370
	ret_reg &ref[370]
	call_c   Dyam_Create_Atom("*STD-PROLOG-FIRST*")
	move_ret ref[370]
	c_ret

c_code local build_seed_54
	ret_reg &seed[54]
	call_c   build_ref_55()
	call_c   build_ref_321()
	call_c   Dyam_Seed_Start(&ref[55],&ref[321],I(0),fun8,1)
	call_c   build_ref_324()
	call_c   Dyam_Seed_Add_Comp(&ref[324],&ref[321],0)
	call_c   Dyam_Seed_End()
	move_ret seed[54]
	c_ret

;; TERM 324: '*PROLOG-FIRST*'(unfold((_E :> deallocate :> succeed), [], _F, _L, _M)) :> '$$HOLE$$'
c_code local build_ref_324
	ret_reg &ref[324]
	call_c   build_ref_323()
	call_c   Dyam_Create_Binary(I(9),&ref[323],I(7))
	move_ret ref[324]
	c_ret

;; TERM 323: '*PROLOG-FIRST*'(unfold((_E :> deallocate :> succeed), [], _F, _L, _M))
c_code local build_ref_323
	ret_reg &ref[323]
	call_c   build_ref_11()
	call_c   build_ref_322()
	call_c   Dyam_Create_Unary(&ref[11],&ref[322])
	move_ret ref[323]
	c_ret

;; TERM 322: unfold((_E :> deallocate :> succeed), [], _F, _L, _M)
c_code local build_ref_322
	ret_reg &ref[322]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_340()
	call_c   build_ref_341()
	call_c   Dyam_Create_Binary(I(9),&ref[340],&ref[341])
	move_ret R(0)
	call_c   Dyam_Create_Binary(I(9),V(4),R(0))
	move_ret R(0)
	call_c   build_ref_330()
	call_c   Dyam_Term_Start(&ref[330],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_End()
	move_ret ref[322]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 321: '*PROLOG-ITEM*'{top=> unfold((_E :> deallocate :> succeed), [], _F, _L, _M), cont=> '$CLOSURE'('$fun'(130, 0, 1134171232), '$TUPPLE'(35082883835716))}
c_code local build_ref_321
	ret_reg &ref[321]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,507904000)
	move_ret R(0)
	call_c   Dyam_Closure_Aux(fun130,R(0))
	move_ret R(0)
	call_c   build_ref_329()
	call_c   build_ref_322()
	call_c   Dyam_Create_Binary(&ref[329],&ref[322],R(0))
	move_ret ref[321]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_53
	ret_reg &seed[53]
	call_c   build_ref_17()
	call_c   build_ref_309()
	call_c   Dyam_Seed_Start(&ref[17],&ref[309],I(0),fun8,1)
	call_c   build_ref_310()
	call_c   Dyam_Seed_Add_Comp(&ref[310],&ref[309],0)
	call_c   Dyam_Seed_End()
	move_ret seed[53]
	c_ret

;; TERM 310: '*GUARD*'(std_prolog_unif_args_alt(_D, 0, _N, _L, _G, _K)) :> '$$HOLE$$'
c_code local build_ref_310
	ret_reg &ref[310]
	call_c   build_ref_309()
	call_c   Dyam_Create_Binary(I(9),&ref[309],I(7))
	move_ret ref[310]
	c_ret

;; TERM 309: '*GUARD*'(std_prolog_unif_args_alt(_D, 0, _N, _L, _G, _K))
c_code local build_ref_309
	ret_reg &ref[309]
	call_c   build_ref_17()
	call_c   build_ref_308()
	call_c   Dyam_Create_Unary(&ref[17],&ref[308])
	move_ret ref[309]
	c_ret

;; TERM 308: std_prolog_unif_args_alt(_D, 0, _N, _L, _G, _K)
c_code local build_ref_308
	ret_reg &ref[308]
	call_c   build_ref_335()
	call_c   Dyam_Term_Start(&ref[335],6)
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(N(0))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_End()
	move_ret ref[308]
	c_ret

;; TERM 314: noop((_B / _C)) :> allocate :> allocate_layer :> _N
c_code local build_ref_314
	ret_reg &ref[314]
	call_c   build_ref_311()
	call_c   build_ref_313()
	call_c   Dyam_Create_Binary(I(9),&ref[311],&ref[313])
	move_ret ref[314]
	c_ret

;; TERM 313: allocate :> allocate_layer :> _N
c_code local build_ref_313
	ret_reg &ref[313]
	call_c   build_ref_47()
	call_c   build_ref_312()
	call_c   Dyam_Create_Binary(I(9),&ref[47],&ref[312])
	move_ret ref[313]
	c_ret

;; TERM 312: allocate_layer :> _N
c_code local build_ref_312
	ret_reg &ref[312]
	call_c   build_ref_48()
	call_c   Dyam_Create_Binary(I(9),&ref[48],V(13))
	move_ret ref[312]
	c_ret

;; TERM 311: noop((_B / _C))
c_code local build_ref_311
	ret_reg &ref[311]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_338()
	call_c   Dyam_Create_Binary(&ref[338],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_302()
	call_c   Dyam_Create_Unary(&ref[302],R(0))
	move_ret ref[311]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 320: '$CLOSURE'('$fun'(129, 0, 1134172096), '$TUPPLE'(35082883835584))
c_code local build_ref_320
	ret_reg &ref[320]
	call_c   build_ref_319()
	call_c   Dyam_Closure_Aux(fun129,&ref[319])
	move_ret ref[320]
	c_ret

;; TERM 315: named_function(_O, _P, global)
c_code local build_ref_315
	ret_reg &ref[315]
	call_c   build_ref_343()
	call_c   build_ref_371()
	call_c   Dyam_Term_Start(&ref[343],3)
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(&ref[371])
	call_c   Dyam_Term_End()
	move_ret ref[315]
	c_ret

;; TERM 371: global
c_code local build_ref_371
	ret_reg &ref[371]
	call_c   Dyam_Create_Atom("global")
	move_ret ref[371]
	c_ret

pl_code local fun128
	call_c   Dyam_Remove_Choice()
	pl_jump  fun12()

;; TERM 316: dyalog_ender((_B / _C))
c_code local build_ref_316
	ret_reg &ref[316]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_338()
	call_c   Dyam_Create_Binary(&ref[338],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_337()
	call_c   Dyam_Create_Unary(&ref[337],R(0))
	move_ret ref[316]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 317: dyalog_ender((_B / _C), _O)
c_code local build_ref_317
	ret_reg &ref[317]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_338()
	call_c   Dyam_Create_Binary(&ref[338],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_337()
	call_c   Dyam_Create_Binary(&ref[337],R(0),V(14))
	move_ret ref[317]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun129[6]=[131075,build_ref_315,build_ref_316,build_ref_317,pool_fun12,pool_fun12]

pl_code local fun129
	call_c   Dyam_Pool(pool_fun129)
	call_c   DYAM_evpred_assert_1(&ref[315])
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(14))
	pl_call  pred_register_init_pool_fun_1()
	call_c   Dyam_Choice(fun128)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[316])
	call_c   Dyam_Cut()
	call_c   DYAM_evpred_assert_1(&ref[317])
	pl_jump  fun12()

;; TERM 319: '$TUPPLE'(35082883835584)
c_code local build_ref_319
	ret_reg &ref[319]
	call_c   Dyam_Create_Simple_Tupple(0,469786624)
	move_ret ref[319]
	c_ret

long local pool_fun130[4]=[3,build_seed_53,build_ref_314,build_ref_320]

pl_code local fun130
	call_c   Dyam_Pool(pool_fun130)
	call_c   Dyam_Allocate(0)
	pl_call  fun9(&seed[53],1)
	move     &ref[314], R(0)
	move     S(5), R(1)
	move     V(14), R(2)
	move     S(5), R(3)
	pl_call  pred_label_code_2()
	move     &ref[320], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Load(4,V(1))
	call_c   Dyam_Reg_Load(6,V(2))
	move     V(15), R(8)
	move     S(5), R(9)
	call_c   Dyam_Reg_Deallocate(5)
	pl_jump  fun25()

long local pool_fun131[3]=[2,build_ref_307,build_seed_54]

pl_code local fun131
	call_c   Dyam_Pool(pool_fun131)
	call_c   Dyam_Unify_Item(&ref[307])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(3))
	move     V(5), R(2)
	move     S(5), R(3)
	pl_call  pred_tupple_2()
	move     V(6), R(0)
	move     S(5), R(1)
	pl_call  pred_null_tupple_1()
	call_c   Dyam_Reg_Load(0,V(3))
	move     V(7), R(2)
	move     S(5), R(3)
	pl_call  pred_duplicate_vars_2()
	call_c   Dyam_Reg_Load(0,V(4))
	move     V(8), R(2)
	move     S(5), R(3)
	pl_call  pred_tupple_2()
	call_c   Dyam_Reg_Load(0,V(8))
	call_c   Dyam_Reg_Load(2,V(5))
	move     V(9), R(4)
	move     S(5), R(5)
	pl_call  pred_inter_3()
	call_c   Dyam_Reg_Load(0,V(9))
	call_c   Dyam_Reg_Load(2,V(7))
	move     V(10), R(4)
	move     S(5), R(5)
	pl_call  pred_union_3()
	call_c   Dyam_Deallocate()
	pl_jump  fun9(&seed[54],12)

;; TERM 306: '*FIRST*'(stdprolog_install('*STD-PROLOG-FIRST*'((_B / _C), _D, _E))) :> []
c_code local build_ref_306
	ret_reg &ref[306]
	call_c   build_ref_305()
	call_c   Dyam_Create_Binary(I(9),&ref[305],I(0))
	move_ret ref[306]
	c_ret

;; TERM 305: '*FIRST*'(stdprolog_install('*STD-PROLOG-FIRST*'((_B / _C), _D, _E)))
c_code local build_ref_305
	ret_reg &ref[305]
	call_c   build_ref_35()
	call_c   build_ref_304()
	call_c   Dyam_Create_Unary(&ref[35],&ref[304])
	move_ret ref[305]
	c_ret

c_code local build_seed_43
	ret_reg &seed[43]
	call_c   build_ref_39()
	call_c   build_ref_165()
	call_c   Dyam_Seed_Start(&ref[39],&ref[165],&ref[165],fun8,1)
	call_c   build_ref_167()
	call_c   Dyam_Seed_Add_Comp(&ref[167],&ref[165],0)
	call_c   Dyam_Seed_End()
	move_ret seed[43]
	c_ret

;; TERM 167: '*FIRST*'(c_module_name(_C, _D)) :> '$$HOLE$$'
c_code local build_ref_167
	ret_reg &ref[167]
	call_c   build_ref_166()
	call_c   Dyam_Create_Binary(I(9),&ref[166],I(7))
	move_ret ref[167]
	c_ret

;; TERM 166: '*FIRST*'(c_module_name(_C, _D))
c_code local build_ref_166
	ret_reg &ref[166]
	call_c   build_ref_35()
	call_c   build_ref_164()
	call_c   Dyam_Create_Unary(&ref[35],&ref[164])
	move_ret ref[166]
	c_ret

;; TERM 164: c_module_name(_C, _D)
c_code local build_ref_164
	ret_reg &ref[164]
	call_c   build_ref_326()
	call_c   Dyam_Create_Binary(&ref[326],V(2),V(3))
	move_ret ref[164]
	c_ret

;; TERM 165: '*CITEM*'(c_module_name(_C, _D), c_module_name(_C, _D))
c_code local build_ref_165
	ret_reg &ref[165]
	call_c   build_ref_39()
	call_c   build_ref_164()
	call_c   Dyam_Create_Binary(&ref[39],&ref[164],&ref[164])
	move_ret ref[165]
	c_ret

c_code local build_seed_44
	ret_reg &seed[44]
	call_c   build_ref_85()
	call_c   build_ref_170()
	call_c   Dyam_Seed_Start(&ref[85],&ref[170],I(0),fun17,1)
	call_c   build_ref_168()
	call_c   Dyam_Seed_Add_Comp(&ref[168],fun64,0)
	call_c   Dyam_Seed_End()
	move_ret seed[44]
	c_ret

;; TERM 168: '*RITEM*'(c_module_name(_C, _D), voidret)
c_code local build_ref_168
	ret_reg &ref[168]
	call_c   build_ref_0()
	call_c   build_ref_164()
	call_c   build_ref_2()
	call_c   Dyam_Create_Binary(&ref[0],&ref[164],&ref[2])
	move_ret ref[168]
	c_ret

pl_code local fun64
	call_c   build_ref_168()
	call_c   Dyam_Unify_Item(&ref[168])
	fail_ret
	pl_jump  Follow_Cont(V(1))

;; TERM 170: '*RITEM*'(c_module_name(_C, _D), voidret) :> stdprolog(2, '$TUPPLE'(35082884350708))
c_code local build_ref_170
	ret_reg &ref[170]
	call_c   build_ref_168()
	call_c   build_ref_169()
	call_c   Dyam_Create_Binary(I(9),&ref[168],&ref[169])
	move_ret ref[170]
	c_ret

;; TERM 169: stdprolog(2, '$TUPPLE'(35082884350708))
c_code local build_ref_169
	ret_reg &ref[169]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,134217728)
	move_ret R(0)
	call_c   build_ref_372()
	call_c   Dyam_Create_Binary(&ref[372],N(2),R(0))
	move_ret ref[169]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 372: stdprolog
c_code local build_ref_372
	ret_reg &ref[372]
	call_c   Dyam_Create_Atom("stdprolog")
	move_ret ref[372]
	c_ret

;; TERM 85: '*CURNEXT*'
c_code local build_ref_85
	ret_reg &ref[85]
	call_c   Dyam_Create_Atom("*CURNEXT*")
	move_ret ref[85]
	c_ret

c_code local build_seed_33
	ret_reg &seed[33]
	call_c   build_ref_39()
	call_c   build_ref_107()
	call_c   Dyam_Seed_Start(&ref[39],&ref[107],&ref[107],fun8,1)
	call_c   build_ref_109()
	call_c   Dyam_Seed_Add_Comp(&ref[109],&ref[107],0)
	call_c   Dyam_Seed_End()
	move_ret seed[33]
	c_ret

;; TERM 109: '*FIRST*'(std_prolog_function(_C, _D, _E)) :> '$$HOLE$$'
c_code local build_ref_109
	ret_reg &ref[109]
	call_c   build_ref_108()
	call_c   Dyam_Create_Binary(I(9),&ref[108],I(7))
	move_ret ref[109]
	c_ret

;; TERM 108: '*FIRST*'(std_prolog_function(_C, _D, _E))
c_code local build_ref_108
	ret_reg &ref[108]
	call_c   build_ref_35()
	call_c   build_ref_106()
	call_c   Dyam_Create_Unary(&ref[35],&ref[106])
	move_ret ref[108]
	c_ret

;; TERM 106: std_prolog_function(_C, _D, _E)
c_code local build_ref_106
	ret_reg &ref[106]
	call_c   build_ref_327()
	call_c   Dyam_Term_Start(&ref[327],3)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[106]
	c_ret

;; TERM 107: '*CITEM*'(std_prolog_function(_C, _D, _E), std_prolog_function(_C, _D, _E))
c_code local build_ref_107
	ret_reg &ref[107]
	call_c   build_ref_39()
	call_c   build_ref_106()
	call_c   Dyam_Create_Binary(&ref[39],&ref[106],&ref[106])
	move_ret ref[107]
	c_ret

c_code local build_seed_34
	ret_reg &seed[34]
	call_c   build_ref_85()
	call_c   build_ref_112()
	call_c   Dyam_Seed_Start(&ref[85],&ref[112],I(0),fun17,1)
	call_c   build_ref_110()
	call_c   Dyam_Seed_Add_Comp(&ref[110],fun22,0)
	call_c   Dyam_Seed_End()
	move_ret seed[34]
	c_ret

;; TERM 110: '*RITEM*'(std_prolog_function(_C, _D, _E), voidret)
c_code local build_ref_110
	ret_reg &ref[110]
	call_c   build_ref_0()
	call_c   build_ref_106()
	call_c   build_ref_2()
	call_c   Dyam_Create_Binary(&ref[0],&ref[106],&ref[2])
	move_ret ref[110]
	c_ret

pl_code local fun22
	call_c   build_ref_110()
	call_c   Dyam_Unify_Item(&ref[110])
	fail_ret
	pl_jump  Follow_Cont(V(1))

;; TERM 112: '*RITEM*'(std_prolog_function(_C, _D, _E), voidret) :> stdprolog(1, '$TUPPLE'(35082884350708))
c_code local build_ref_112
	ret_reg &ref[112]
	call_c   build_ref_110()
	call_c   build_ref_111()
	call_c   Dyam_Create_Binary(I(9),&ref[110],&ref[111])
	move_ret ref[112]
	c_ret

;; TERM 111: stdprolog(1, '$TUPPLE'(35082884350708))
c_code local build_ref_111
	ret_reg &ref[111]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,134217728)
	move_ret R(0)
	call_c   build_ref_372()
	call_c   Dyam_Create_Binary(&ref[372],N(1),R(0))
	move_ret ref[111]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_30
	ret_reg &seed[30]
	call_c   build_ref_85()
	call_c   build_ref_88()
	call_c   Dyam_Seed_Start(&ref[85],&ref[88],I(0),fun17,1)
	call_c   build_ref_86()
	call_c   Dyam_Seed_Add_Comp(&ref[86],fun16,0)
	call_c   Dyam_Seed_End()
	move_ret seed[30]
	c_ret

;; TERM 86: '*RITEM*'(wrapper_install(_C, _D), voidret)
c_code local build_ref_86
	ret_reg &ref[86]
	call_c   build_ref_0()
	call_c   build_ref_81()
	call_c   build_ref_2()
	call_c   Dyam_Create_Binary(&ref[0],&ref[81],&ref[2])
	move_ret ref[86]
	c_ret

;; TERM 81: wrapper_install(_C, _D)
c_code local build_ref_81
	ret_reg &ref[81]
	call_c   build_ref_325()
	call_c   Dyam_Create_Binary(&ref[325],V(2),V(3))
	move_ret ref[81]
	c_ret

pl_code local fun16
	call_c   build_ref_86()
	call_c   Dyam_Unify_Item(&ref[86])
	fail_ret
	pl_jump  Follow_Cont(V(1))

;; TERM 88: '*RITEM*'(wrapper_install(_C, _D), voidret) :> stdprolog(0, '$TUPPLE'(35082884350708))
c_code local build_ref_88
	ret_reg &ref[88]
	call_c   build_ref_86()
	call_c   build_ref_87()
	call_c   Dyam_Create_Binary(I(9),&ref[86],&ref[87])
	move_ret ref[88]
	c_ret

;; TERM 87: stdprolog(0, '$TUPPLE'(35082884350708))
c_code local build_ref_87
	ret_reg &ref[87]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,134217728)
	move_ret R(0)
	call_c   build_ref_372()
	call_c   Dyam_Create_Binary(&ref[372],N(0),R(0))
	move_ret ref[87]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_29
	ret_reg &seed[29]
	call_c   build_ref_39()
	call_c   build_ref_82()
	call_c   Dyam_Seed_Start(&ref[39],&ref[82],&ref[82],fun8,1)
	call_c   build_ref_84()
	call_c   Dyam_Seed_Add_Comp(&ref[84],&ref[82],0)
	call_c   Dyam_Seed_End()
	move_ret seed[29]
	c_ret

;; TERM 84: '*FIRST*'(wrapper_install(_C, _D)) :> '$$HOLE$$'
c_code local build_ref_84
	ret_reg &ref[84]
	call_c   build_ref_83()
	call_c   Dyam_Create_Binary(I(9),&ref[83],I(7))
	move_ret ref[84]
	c_ret

;; TERM 83: '*FIRST*'(wrapper_install(_C, _D))
c_code local build_ref_83
	ret_reg &ref[83]
	call_c   build_ref_35()
	call_c   build_ref_81()
	call_c   Dyam_Create_Unary(&ref[35],&ref[81])
	move_ret ref[83]
	c_ret

;; TERM 82: '*CITEM*'(wrapper_install(_C, _D), wrapper_install(_C, _D))
c_code local build_ref_82
	ret_reg &ref[82]
	call_c   build_ref_39()
	call_c   build_ref_81()
	call_c   Dyam_Create_Binary(&ref[39],&ref[81],&ref[81])
	move_ret ref[82]
	c_ret

pl_code local fun7
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Object(R(0))
	call_c   Dyam_Tabule()
	pl_ret

pl_code local fun65
	call_c   Dyam_Remove_Choice()
	pl_call  fun23(&seed[44],2,V(4))
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun20
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Object(R(0))
	call_c   Dyam_Tabule()
	pl_jump  Schedule_Prolog()

pl_code local fun21
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Choice(fun20)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Subsume(R(0))
	fail_ret
	call_c   Dyam_Cut()
	pl_ret

pl_code local fun23
	call_c   Dyam_Backptr_Trace(R(1),R(2))
	call_c   Dyam_Light_Object(R(0))
	pl_jump  Schedule_Prolog()

pl_code local fun24
	call_c   Dyam_Remove_Choice()
	pl_call  fun23(&seed[34],2,V(5))
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun18
	call_c   Dyam_Backptr_Trace(R(1),R(2))
	call_c   Dyam_Object(R(0))
	call_c   Dyam_Tabule()
	call_c   Dyam_Now()
	pl_ret

pl_code local fun10
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Object(R(0))
	call_c   Dyam_Tabule()
	call_c   Dyam_Schedule()
	pl_ret


;;------------------ INIT POOL ------------

c_code local build_init_pool
	call_c   build_ref_1()
	call_c   build_ref_3()
	call_c   build_ref_4()
	call_c   build_ref_5()
	call_c   build_ref_6()
	call_c   build_ref_7()
	call_c   build_ref_8()
	call_c   build_ref_9()
	call_c   build_seed_4()
	call_c   build_seed_1()
	call_c   build_seed_3()
	call_c   build_seed_0()
	call_c   build_seed_2()
	call_c   build_seed_5()
	call_c   build_seed_13()
	call_c   build_seed_14()
	call_c   build_seed_7()
	call_c   build_seed_9()
	call_c   build_seed_10()
	call_c   build_seed_21()
	call_c   build_seed_12()
	call_c   build_seed_11()
	call_c   build_seed_15()
	call_c   build_seed_6()
	call_c   build_seed_16()
	call_c   build_seed_8()
	call_c   build_seed_18()
	call_c   build_seed_20()
	call_c   build_seed_17()
	call_c   build_seed_19()
	call_c   build_seed_22()
	call_c   build_seed_43()
	call_c   build_seed_44()
	call_c   build_seed_33()
	call_c   build_seed_34()
	call_c   build_seed_30()
	call_c   build_seed_29()
	c_ret

long local ref[373]
long local seed[55]

long local _initialization

c_code global initialization_dyalog_stdprolog
	call_c   Dyam_Check_And_Update_Initialization(_initialization)
	fail_c_ret
	call_c   initialization_dyalog_reader()
	call_c   initialization_dyalog_oset()
	call_c   initialization_dyalog_format()
	call_c   build_init_pool()
	call_c   build_viewers()
	call_c   load()
	c_ret

