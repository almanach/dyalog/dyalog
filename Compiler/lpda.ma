;; Compiler: DyALog 1.14.0
;; File "lpda.pl"


;;------------------ LOADING ------------

c_code local load
	call_c   Dyam_Loading_Set()
	pl_call  fun23(&seed[16],0)
	pl_call  fun23(&seed[33],0)
	pl_call  fun23(&seed[35],0)
	pl_call  fun23(&seed[17],0)
	pl_call  fun23(&seed[13],0)
	pl_call  fun23(&seed[12],0)
	pl_call  fun23(&seed[41],0)
	pl_call  fun23(&seed[11],0)
	pl_call  fun23(&seed[24],0)
	pl_call  fun23(&seed[8],0)
	pl_call  fun23(&seed[15],0)
	pl_call  fun23(&seed[14],0)
	pl_call  fun23(&seed[31],0)
	pl_call  fun23(&seed[32],0)
	pl_call  fun23(&seed[9],0)
	pl_call  fun23(&seed[10],0)
	pl_call  fun23(&seed[28],0)
	pl_call  fun23(&seed[37],0)
	pl_call  fun23(&seed[36],0)
	pl_call  fun23(&seed[29],0)
	pl_call  fun23(&seed[18],0)
	pl_call  fun23(&seed[26],0)
	pl_call  fun23(&seed[38],0)
	pl_call  fun23(&seed[19],0)
	pl_call  fun23(&seed[23],0)
	pl_call  fun23(&seed[27],0)
	pl_call  fun23(&seed[21],0)
	pl_call  fun23(&seed[25],0)
	pl_call  fun23(&seed[2],0)
	pl_call  fun23(&seed[30],0)
	pl_call  fun23(&seed[20],0)
	pl_call  fun23(&seed[22],0)
	pl_call  fun23(&seed[40],0)
	pl_call  fun23(&seed[3],0)
	pl_call  fun23(&seed[34],0)
	pl_call  fun23(&seed[39],0)
	pl_call  fun23(&seed[1],0)
	pl_call  fun23(&seed[5],0)
	pl_call  fun23(&seed[4],0)
	pl_call  fun23(&seed[42],0)
	pl_call  fun23(&seed[7],0)
	pl_call  fun23(&seed[0],0)
	pl_call  fun23(&seed[43],0)
	pl_call  fun23(&seed[6],0)
	pl_call  fun8(&seed[49],0)
	pl_call  fun8(&seed[48],0)
	pl_call  fun8(&seed[47],0)
	pl_call  fun8(&seed[46],0)
	pl_call  fun8(&seed[45],0)
	pl_call  fun8(&seed[44],0)
	call_c   Dyam_Loading_Reset()
	c_ret

pl_code global pred_destructure_unify_aux_4
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(4)
	call_c   Dyam_Choice(fun147)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(1),V(2))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(4),V(3))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code global pred_handle_choice_5
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(5)
	call_c   Dyam_Choice(fun123)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[335])
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(3),V(1))
	fail_ret
	call_c   Dyam_Unify(V(2),&ref[336])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code global pred_trans_unfold_2
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(2)
	call_c   Dyam_Choice(fun120)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[330])
	fail_ret
	call_c   Dyam_Cut()
	move     &ref[331], R(0)
	move     S(5), R(1)
	move     V(6), R(2)
	move     S(5), R(3)
	pl_call  pred_trans_unfold_2()
	move     &ref[332], R(0)
	move     S(5), R(1)
	move     V(7), R(2)
	move     S(5), R(3)
	pl_call  pred_trans_unfold_2()
	call_c   Dyam_Unify(V(2),&ref[333])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code global pred_try_lco_6
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Reg_Unify_Nil(2)
	fail_ret
	call_c   Dyam_Multi_Reg_Bind(4,2,4)
	pl_call  Object_1(&ref[235])
	call_c   Dyam_Choice(fun71)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[236])
	call_c   DYAM_sfol_identical(&ref[237],V(4))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(5),&ref[238])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code global pred_try_litteral_expansion_4
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(4)
	call_c   Dyam_Reg_Load_Ptr(2,V(2))
	fail_ret
	move     V(5), R(4)
	move     S(5), R(5)
	call_c   DyALog_Mutable_Read(R(2),&R(4))
	fail_ret
	call_c   Dyam_Choice(fun63)
	call_c   Dyam_Set_Cut()
	pl_call  Domain_2(&ref[221],V(5))
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(3))
	call_c   Dyam_Reg_Load(2,V(4))
	move     V(6), R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  pred_my_numbervars_3()

pl_code global pred_xtagop_3
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(3)
	call_c   Dyam_Choice(fun29)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[128])
	call_c   Dyam_Cut()
	call_c   Dyam_Deallocate()
	pl_ret

pl_code global pred_try_compiler_toplevel_2
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Unify(0,&ref[85])
	fail_ret
	call_c   Dyam_Reg_Bind(2,V(2))
	call_c   Dyam_Choice(fun18)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[79])
	fail_ret
	call_c   Dyam_Cut()
fun17:
	call_c   Dyam_Choice(fun16)
	call_c   Dyam_Set_Cut()
	pl_call  fun15(&seed[53],1)
	call_c   Dyam_Cut()
	call_c   Dyam_Deallocate()
	pl_ret


pl_code global pred_registered_predicate_1
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Choice(fun10)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[60])
	call_c   Dyam_Cut()
	call_c   Dyam_Deallocate()
	pl_ret


;;------------------ VIEWERS ------------

c_code local build_viewers
	call_c   Dyam_Load_Viewer(&ref[17],&ref[16])
	call_c   Dyam_Load_Viewer(&ref[14],&ref[15])
	call_c   Dyam_Load_Viewer(&ref[11],&ref[10])
	call_c   Dyam_Load_Viewer(&ref[8],&ref[9])
	call_c   Dyam_Load_Viewer(&ref[5],&ref[4])
	call_c   Dyam_Load_Viewer(&ref[3],&ref[1])
	c_ret

c_code local build_seed_44
	ret_reg &seed[44]
	call_c   build_ref_18()
	call_c   build_ref_19()
	call_c   Dyam_Seed_Start(&ref[18],&ref[19],&ref[19],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[44]
	c_ret

pl_code local fun0
	pl_ret

;; TERM 19: terminal_cont(deallocate_layer)
c_code local build_ref_19
	ret_reg &ref[19]
	call_c   build_ref_572()
	call_c   build_ref_573()
	call_c   Dyam_Create_Unary(&ref[572],&ref[573])
	move_ret ref[19]
	c_ret

;; TERM 573: deallocate_layer
c_code local build_ref_573
	ret_reg &ref[573]
	call_c   Dyam_Create_Atom("deallocate_layer")
	move_ret ref[573]
	c_ret

;; TERM 572: terminal_cont
c_code local build_ref_572
	ret_reg &ref[572]
	call_c   Dyam_Create_Atom("terminal_cont")
	move_ret ref[572]
	c_ret

;; TERM 18: '*DATABASE*'
c_code local build_ref_18
	ret_reg &ref[18]
	call_c   Dyam_Create_Atom("*DATABASE*")
	move_ret ref[18]
	c_ret

c_code local build_seed_45
	ret_reg &seed[45]
	call_c   build_ref_18()
	call_c   build_ref_20()
	call_c   Dyam_Seed_Start(&ref[18],&ref[20],&ref[20],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[45]
	c_ret

;; TERM 20: terminal_cont('*PROLOG-LAST*')
c_code local build_ref_20
	ret_reg &ref[20]
	call_c   build_ref_572()
	call_c   build_ref_426()
	call_c   Dyam_Create_Unary(&ref[572],&ref[426])
	move_ret ref[20]
	c_ret

;; TERM 426: '*PROLOG-LAST*'
c_code local build_ref_426
	ret_reg &ref[426]
	call_c   Dyam_Create_Atom("*PROLOG-LAST*")
	move_ret ref[426]
	c_ret

c_code local build_seed_46
	ret_reg &seed[46]
	call_c   build_ref_18()
	call_c   build_ref_21()
	call_c   Dyam_Seed_Start(&ref[18],&ref[21],&ref[21],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[46]
	c_ret

;; TERM 21: terminal_cont('*SA-PROLOG-LAST*')
c_code local build_ref_21
	ret_reg &ref[21]
	call_c   build_ref_572()
	call_c   build_ref_574()
	call_c   Dyam_Create_Unary(&ref[572],&ref[574])
	move_ret ref[21]
	c_ret

;; TERM 574: '*SA-PROLOG-LAST*'
c_code local build_ref_574
	ret_reg &ref[574]
	call_c   Dyam_Create_Atom("*SA-PROLOG-LAST*")
	move_ret ref[574]
	c_ret

c_code local build_seed_47
	ret_reg &seed[47]
	call_c   build_ref_18()
	call_c   build_ref_22()
	call_c   Dyam_Seed_Start(&ref[18],&ref[22],&ref[22],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[47]
	c_ret

;; TERM 22: lpda_meta_call(dyalog, _B, call!call(_B))
c_code local build_ref_22
	ret_reg &ref[22]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_577()
	call_c   Dyam_Create_Unary(&ref[577],V(1))
	move_ret R(0)
	call_c   build_ref_575()
	call_c   build_ref_576()
	call_c   Dyam_Term_Start(&ref[575],3)
	call_c   Dyam_Term_Arg(&ref[576])
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_End()
	move_ret ref[22]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 576: dyalog
c_code local build_ref_576
	ret_reg &ref[576]
	call_c   Dyam_Create_Atom("dyalog")
	move_ret ref[576]
	c_ret

;; TERM 575: lpda_meta_call
c_code local build_ref_575
	ret_reg &ref[575]
	call_c   Dyam_Create_Atom("lpda_meta_call")
	move_ret ref[575]
	c_ret

;; TERM 577: call!call
c_code local build_ref_577
	ret_reg &ref[577]
	call_c   build_ref_578()
	call_c   Dyam_Create_Atom_Module("call",&ref[578])
	move_ret ref[577]
	c_ret

;; TERM 578: call
c_code local build_ref_578
	ret_reg &ref[578]
	call_c   Dyam_Create_Atom("call")
	move_ret ref[578]
	c_ret

c_code local build_seed_48
	ret_reg &seed[48]
	call_c   build_ref_18()
	call_c   build_ref_23()
	call_c   Dyam_Seed_Start(&ref[18],&ref[23],&ref[23],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[48]
	c_ret

;; TERM 23: lpda_meta_call(prolog, _B, call!call(_B))
c_code local build_ref_23
	ret_reg &ref[23]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_577()
	call_c   Dyam_Create_Unary(&ref[577],V(1))
	move_ret R(0)
	call_c   build_ref_575()
	call_c   build_ref_579()
	call_c   Dyam_Term_Start(&ref[575],3)
	call_c   Dyam_Term_Arg(&ref[579])
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_End()
	move_ret ref[23]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 579: prolog
c_code local build_ref_579
	ret_reg &ref[579]
	call_c   Dyam_Create_Atom("prolog")
	move_ret ref[579]
	c_ret

c_code local build_seed_49
	ret_reg &seed[49]
	call_c   build_ref_18()
	call_c   build_ref_24()
	call_c   Dyam_Seed_Start(&ref[18],&ref[24],&ref[24],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[49]
	c_ret

;; TERM 24: lpda_meta_call(rec_prolog, _B, call!pcall(_B))
c_code local build_ref_24
	ret_reg &ref[24]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_580()
	call_c   Dyam_Create_Unary(&ref[580],V(1))
	move_ret R(0)
	call_c   build_ref_575()
	call_c   build_ref_544()
	call_c   Dyam_Term_Start(&ref[575],3)
	call_c   Dyam_Term_Arg(&ref[544])
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_End()
	move_ret ref[24]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 544: rec_prolog
c_code local build_ref_544
	ret_reg &ref[544]
	call_c   Dyam_Create_Atom("rec_prolog")
	move_ret ref[544]
	c_ret

;; TERM 580: call!pcall
c_code local build_ref_580
	ret_reg &ref[580]
	call_c   build_ref_578()
	call_c   Dyam_Create_Atom_Module("pcall",&ref[578])
	move_ret ref[580]
	c_ret

c_code local build_seed_6
	ret_reg &seed[6]
	call_c   build_ref_25()
	call_c   build_ref_28()
	call_c   Dyam_Seed_Start(&ref[25],&ref[28],I(0),fun6,1)
	call_c   build_ref_30()
	call_c   Dyam_Seed_Add_Comp(&ref[30],fun21,0)
	call_c   Dyam_Seed_End()
	move_ret seed[6]
	c_ret

;; TERM 30: '*CITEM*'(register_predicate(_B), _A)
c_code local build_ref_30
	ret_reg &ref[30]
	call_c   build_ref_29()
	call_c   build_ref_26()
	call_c   Dyam_Create_Binary(&ref[29],&ref[26],V(0))
	move_ret ref[30]
	c_ret

;; TERM 26: register_predicate(_B)
c_code local build_ref_26
	ret_reg &ref[26]
	call_c   build_ref_581()
	call_c   Dyam_Create_Unary(&ref[581],V(1))
	move_ret ref[26]
	c_ret

;; TERM 581: register_predicate
c_code local build_ref_581
	ret_reg &ref[581]
	call_c   Dyam_Create_Atom("register_predicate")
	move_ret ref[581]
	c_ret

;; TERM 29: '*CITEM*'
c_code local build_ref_29
	ret_reg &ref[29]
	call_c   Dyam_Create_Atom("*CITEM*")
	move_ret ref[29]
	c_ret

c_code local build_seed_50
	ret_reg &seed[50]
	call_c   build_ref_0()
	call_c   build_ref_31()
	call_c   Dyam_Seed_Start(&ref[0],&ref[31],&ref[31],fun1,1)
	call_c   build_ref_32()
	call_c   Dyam_Seed_Add_Comp(&ref[32],&ref[31],0)
	call_c   Dyam_Seed_End()
	move_ret seed[50]
	c_ret

pl_code local fun1
	pl_jump  Complete(0,0)

;; TERM 32: '*RITEM*'(_A, voidret) :> '$$HOLE$$'
c_code local build_ref_32
	ret_reg &ref[32]
	call_c   build_ref_31()
	call_c   Dyam_Create_Binary(I(9),&ref[31],I(7))
	move_ret ref[32]
	c_ret

;; TERM 31: '*RITEM*'(_A, voidret)
c_code local build_ref_31
	ret_reg &ref[31]
	call_c   build_ref_0()
	call_c   build_ref_2()
	call_c   Dyam_Create_Binary(&ref[0],V(0),&ref[2])
	move_ret ref[31]
	c_ret

;; TERM 2: voidret
c_code local build_ref_2
	ret_reg &ref[2]
	call_c   Dyam_Create_Atom("voidret")
	move_ret ref[2]
	c_ret

;; TERM 0: '*RITEM*'
c_code local build_ref_0
	ret_reg &ref[0]
	call_c   Dyam_Create_Atom("*RITEM*")
	move_ret ref[0]
	c_ret

pl_code local fun8
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Choice(fun7)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Subsume(R(0))
	fail_ret
	call_c   Dyam_Cut()
	pl_ret

long local pool_fun21[3]=[2,build_ref_30,build_seed_50]

pl_code local fun21
	call_c   Dyam_Pool(pool_fun21)
	call_c   Dyam_Unify_Item(&ref[30])
	fail_ret
	pl_jump  fun8(&seed[50],2)

pl_code local fun6
	pl_jump  Apply(0,0)

;; TERM 28: '*FIRST*'(register_predicate(_B)) :> []
c_code local build_ref_28
	ret_reg &ref[28]
	call_c   build_ref_27()
	call_c   Dyam_Create_Binary(I(9),&ref[27],I(0))
	move_ret ref[28]
	c_ret

;; TERM 27: '*FIRST*'(register_predicate(_B))
c_code local build_ref_27
	ret_reg &ref[27]
	call_c   build_ref_25()
	call_c   build_ref_26()
	call_c   Dyam_Create_Unary(&ref[25],&ref[26])
	move_ret ref[27]
	c_ret

;; TERM 25: '*FIRST*'
c_code local build_ref_25
	ret_reg &ref[25]
	call_c   Dyam_Create_Atom("*FIRST*")
	move_ret ref[25]
	c_ret

c_code local build_seed_43
	ret_reg &seed[43]
	call_c   build_ref_33()
	call_c   build_ref_45()
	call_c   Dyam_Seed_Start(&ref[33],&ref[45],I(0),fun0,1)
	call_c   build_ref_44()
	call_c   Dyam_Seed_Add_Comp(&ref[44],fun3,0)
	call_c   Dyam_Seed_End()
	move_ret seed[43]
	c_ret

;; TERM 44: '*GUARD*'(get_clause(_B))
c_code local build_ref_44
	ret_reg &ref[44]
	call_c   build_ref_34()
	call_c   build_ref_43()
	call_c   Dyam_Create_Unary(&ref[34],&ref[43])
	move_ret ref[44]
	c_ret

;; TERM 43: get_clause(_B)
c_code local build_ref_43
	ret_reg &ref[43]
	call_c   build_ref_582()
	call_c   Dyam_Create_Unary(&ref[582],V(1))
	move_ret ref[43]
	c_ret

;; TERM 582: get_clause
c_code local build_ref_582
	ret_reg &ref[582]
	call_c   Dyam_Create_Atom("get_clause")
	move_ret ref[582]
	c_ret

;; TERM 34: '*GUARD*'
c_code local build_ref_34
	ret_reg &ref[34]
	call_c   Dyam_Create_Atom("*GUARD*")
	move_ret ref[34]
	c_ret

;; TERM 46: '$clause'(_B)
c_code local build_ref_46
	ret_reg &ref[46]
	call_c   build_ref_583()
	call_c   Dyam_Create_Unary(&ref[583],V(1))
	move_ret ref[46]
	c_ret

;; TERM 583: '$clause'
c_code local build_ref_583
	ret_reg &ref[583]
	call_c   Dyam_Create_Atom("$clause")
	move_ret ref[583]
	c_ret

long local pool_fun3[3]=[2,build_ref_44,build_ref_46]

pl_code local fun3
	call_c   Dyam_Pool(pool_fun3)
	call_c   Dyam_Unify_Item(&ref[44])
	fail_ret
	pl_jump  Object_1(&ref[46])

;; TERM 45: '*GUARD*'(get_clause(_B)) :> []
c_code local build_ref_45
	ret_reg &ref[45]
	call_c   build_ref_44()
	call_c   Dyam_Create_Binary(I(9),&ref[44],I(0))
	move_ret ref[45]
	c_ret

;; TERM 33: '*GUARD_CLAUSE*'
c_code local build_ref_33
	ret_reg &ref[33]
	call_c   Dyam_Create_Atom("*GUARD_CLAUSE*")
	move_ret ref[33]
	c_ret

c_code local build_seed_0
	ret_reg &seed[0]
	call_c   build_ref_33()
	call_c   build_ref_37()
	call_c   Dyam_Seed_Start(&ref[33],&ref[37],I(0),fun0,1)
	call_c   build_ref_36()
	call_c   Dyam_Seed_Add_Comp(&ref[36],fun2,0)
	call_c   Dyam_Seed_End()
	move_ret seed[0]
	c_ret

;; TERM 36: '*GUARD*'(destructure_unify_args([], [], _B, _B))
c_code local build_ref_36
	ret_reg &ref[36]
	call_c   build_ref_34()
	call_c   build_ref_35()
	call_c   Dyam_Create_Unary(&ref[34],&ref[35])
	move_ret ref[36]
	c_ret

;; TERM 35: destructure_unify_args([], [], _B, _B)
c_code local build_ref_35
	ret_reg &ref[35]
	call_c   build_ref_584()
	call_c   Dyam_Term_Start(&ref[584],4)
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_End()
	move_ret ref[35]
	c_ret

;; TERM 584: destructure_unify_args
c_code local build_ref_584
	ret_reg &ref[584]
	call_c   Dyam_Create_Atom("destructure_unify_args")
	move_ret ref[584]
	c_ret

pl_code local fun2
	call_c   build_ref_36()
	call_c   Dyam_Unify_Item(&ref[36])
	fail_ret
	pl_ret

;; TERM 37: '*GUARD*'(destructure_unify_args([], [], _B, _B)) :> []
c_code local build_ref_37
	ret_reg &ref[37]
	call_c   build_ref_36()
	call_c   Dyam_Create_Binary(I(9),&ref[36],I(0))
	move_ret ref[37]
	c_ret

c_code local build_seed_7
	ret_reg &seed[7]
	call_c   build_ref_25()
	call_c   build_ref_39()
	call_c   Dyam_Seed_Start(&ref[25],&ref[39],I(0),fun6,1)
	call_c   build_ref_40()
	call_c   Dyam_Seed_Add_Comp(&ref[40],fun20,0)
	call_c   Dyam_Seed_End()
	move_ret seed[7]
	c_ret

;; TERM 40: '*CITEM*'(metacall_initialize, _A)
c_code local build_ref_40
	ret_reg &ref[40]
	call_c   build_ref_29()
	call_c   build_ref_4()
	call_c   Dyam_Create_Binary(&ref[29],&ref[4],V(0))
	move_ret ref[40]
	c_ret

;; TERM 4: metacall_initialize
c_code local build_ref_4
	ret_reg &ref[4]
	call_c   Dyam_Create_Atom("metacall_initialize")
	move_ret ref[4]
	c_ret

;; TERM 41: 'call.pl'
c_code local build_ref_41
	ret_reg &ref[41]
	call_c   Dyam_Create_Atom("call.pl")
	move_ret ref[41]
	c_ret

;; TERM 42: require
c_code local build_ref_42
	ret_reg &ref[42]
	call_c   Dyam_Create_Atom("require")
	move_ret ref[42]
	c_ret

long local pool_fun20[5]=[4,build_ref_40,build_ref_41,build_ref_42,build_seed_50]

pl_code local fun20
	call_c   Dyam_Pool(pool_fun20)
	call_c   Dyam_Unify_Item(&ref[40])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[41], R(0)
	move     0, R(1)
	move     &ref[42], R(2)
	move     0, R(3)
	pl_call  pred_read_files_2()
	call_c   Dyam_Deallocate()
	pl_jump  fun8(&seed[50],2)

;; TERM 39: '*FIRST*'(metacall_initialize) :> []
c_code local build_ref_39
	ret_reg &ref[39]
	call_c   build_ref_38()
	call_c   Dyam_Create_Binary(I(9),&ref[38],I(0))
	move_ret ref[39]
	c_ret

;; TERM 38: '*FIRST*'(metacall_initialize)
c_code local build_ref_38
	ret_reg &ref[38]
	call_c   build_ref_25()
	call_c   build_ref_4()
	call_c   Dyam_Create_Unary(&ref[25],&ref[4])
	move_ret ref[38]
	c_ret

c_code local build_seed_42
	ret_reg &seed[42]
	call_c   build_ref_33()
	call_c   build_ref_52()
	call_c   Dyam_Seed_Start(&ref[33],&ref[52],I(0),fun0,1)
	call_c   build_ref_51()
	call_c   Dyam_Seed_Add_Comp(&ref[51],fun34,0)
	call_c   Dyam_Seed_End()
	move_ret seed[42]
	c_ret

;; TERM 51: '*GUARD*'(get_clause(register_predicate(_B)))
c_code local build_ref_51
	ret_reg &ref[51]
	call_c   build_ref_34()
	call_c   build_ref_50()
	call_c   Dyam_Create_Unary(&ref[34],&ref[50])
	move_ret ref[51]
	c_ret

;; TERM 50: get_clause(register_predicate(_B))
c_code local build_ref_50
	ret_reg &ref[50]
	call_c   build_ref_582()
	call_c   build_ref_26()
	call_c   Dyam_Create_Unary(&ref[582],&ref[26])
	move_ret ref[50]
	c_ret

c_code local build_seed_51
	ret_reg &seed[51]
	call_c   build_ref_53()
	call_c   build_ref_56()
	call_c   Dyam_Seed_Start(&ref[53],&ref[56],I(0),fun6,1)
	call_c   build_ref_54()
	call_c   Dyam_Seed_Add_Comp(&ref[54],fun5,0)
	call_c   Dyam_Seed_End()
	move_ret seed[51]
	c_ret

;; TERM 54: '*RITEM*'(register_predicate(_B), voidret)
c_code local build_ref_54
	ret_reg &ref[54]
	call_c   build_ref_0()
	call_c   build_ref_26()
	call_c   build_ref_2()
	call_c   Dyam_Create_Binary(&ref[0],&ref[26],&ref[2])
	move_ret ref[54]
	c_ret

pl_code local fun5
	call_c   build_ref_54()
	call_c   Dyam_Unify_Item(&ref[54])
	fail_ret
	pl_ret

;; TERM 56: '*RITEM*'(register_predicate(_B), voidret) :> lpda(0, '$TUPPLE'(0))
c_code local build_ref_56
	ret_reg &ref[56]
	call_c   build_ref_54()
	call_c   build_ref_55()
	call_c   Dyam_Create_Binary(I(9),&ref[54],&ref[55])
	move_ret ref[56]
	c_ret

;; TERM 55: lpda(0, '$TUPPLE'(0))
c_code local build_ref_55
	ret_reg &ref[55]
	call_c   build_ref_585()
	call_c   Dyam_Create_Binary(&ref[585],N(0),I(0))
	move_ret ref[55]
	c_ret

;; TERM 585: lpda
c_code local build_ref_585
	ret_reg &ref[585]
	call_c   Dyam_Create_Atom("lpda")
	move_ret ref[585]
	c_ret

;; TERM 53: '*CURNEXT*'
c_code local build_ref_53
	ret_reg &ref[53]
	call_c   Dyam_Create_Atom("*CURNEXT*")
	move_ret ref[53]
	c_ret

pl_code local fun15
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Light_Object(R(0))
	pl_jump  Schedule_Prolog()

long local pool_fun34[3]=[2,build_ref_51,build_seed_51]

pl_code local fun34
	call_c   Dyam_Pool(pool_fun34)
	call_c   Dyam_Unify_Item(&ref[51])
	fail_ret
	pl_jump  fun15(&seed[51],2)

;; TERM 52: '*GUARD*'(get_clause(register_predicate(_B))) :> []
c_code local build_ref_52
	ret_reg &ref[52]
	call_c   build_ref_51()
	call_c   Dyam_Create_Binary(I(9),&ref[51],I(0))
	move_ret ref[52]
	c_ret

c_code local build_seed_4
	ret_reg &seed[4]
	call_c   build_ref_33()
	call_c   build_ref_49()
	call_c   Dyam_Seed_Start(&ref[33],&ref[49],I(0),fun0,1)
	call_c   build_ref_48()
	call_c   Dyam_Seed_Add_Comp(&ref[48],fun4,0)
	call_c   Dyam_Seed_End()
	move_ret seed[4]
	c_ret

;; TERM 48: '*GUARD*'(body_to_lpda_handler(_B, true, _C, _C, _D))
c_code local build_ref_48
	ret_reg &ref[48]
	call_c   build_ref_34()
	call_c   build_ref_47()
	call_c   Dyam_Create_Unary(&ref[34],&ref[47])
	move_ret ref[48]
	c_ret

;; TERM 47: body_to_lpda_handler(_B, true, _C, _C, _D)
c_code local build_ref_47
	ret_reg &ref[47]
	call_c   build_ref_586()
	call_c   build_ref_84()
	call_c   Dyam_Term_Start(&ref[586],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(&ref[84])
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[47]
	c_ret

;; TERM 84: true
c_code local build_ref_84
	ret_reg &ref[84]
	call_c   Dyam_Create_Atom("true")
	move_ret ref[84]
	c_ret

;; TERM 586: body_to_lpda_handler
c_code local build_ref_586
	ret_reg &ref[586]
	call_c   Dyam_Create_Atom("body_to_lpda_handler")
	move_ret ref[586]
	c_ret

pl_code local fun4
	call_c   build_ref_48()
	call_c   Dyam_Unify_Item(&ref[48])
	fail_ret
	pl_ret

;; TERM 49: '*GUARD*'(body_to_lpda_handler(_B, true, _C, _C, _D)) :> []
c_code local build_ref_49
	ret_reg &ref[49]
	call_c   build_ref_48()
	call_c   Dyam_Create_Binary(I(9),&ref[48],I(0))
	move_ret ref[49]
	c_ret

c_code local build_seed_5
	ret_reg &seed[5]
	call_c   build_ref_33()
	call_c   build_ref_59()
	call_c   Dyam_Seed_Start(&ref[33],&ref[59],I(0),fun0,1)
	call_c   build_ref_58()
	call_c   Dyam_Seed_Add_Comp(&ref[58],fun9,0)
	call_c   Dyam_Seed_End()
	move_ret seed[5]
	c_ret

;; TERM 58: '*GUARD*'(body_to_lpda_handler(_B, fail, _C, (fail :> _C), _D))
c_code local build_ref_58
	ret_reg &ref[58]
	call_c   build_ref_34()
	call_c   build_ref_57()
	call_c   Dyam_Create_Unary(&ref[34],&ref[57])
	move_ret ref[58]
	c_ret

;; TERM 57: body_to_lpda_handler(_B, fail, _C, (fail :> _C), _D)
c_code local build_ref_57
	ret_reg &ref[57]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_213()
	call_c   Dyam_Create_Binary(I(9),&ref[213],V(2))
	move_ret R(0)
	call_c   build_ref_586()
	call_c   Dyam_Term_Start(&ref[586],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(&ref[213])
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[57]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 213: fail
c_code local build_ref_213
	ret_reg &ref[213]
	call_c   Dyam_Create_Atom("fail")
	move_ret ref[213]
	c_ret

pl_code local fun9
	call_c   build_ref_58()
	call_c   Dyam_Unify_Item(&ref[58])
	fail_ret
	pl_ret

;; TERM 59: '*GUARD*'(body_to_lpda_handler(_B, fail, _C, (fail :> _C), _D)) :> []
c_code local build_ref_59
	ret_reg &ref[59]
	call_c   build_ref_58()
	call_c   Dyam_Create_Binary(I(9),&ref[58],I(0))
	move_ret ref[59]
	c_ret

c_code local build_seed_1
	ret_reg &seed[1]
	call_c   build_ref_33()
	call_c   build_ref_65()
	call_c   Dyam_Seed_Start(&ref[33],&ref[65],I(0),fun0,1)
	call_c   build_ref_64()
	call_c   Dyam_Seed_Add_Comp(&ref[64],fun12,0)
	call_c   Dyam_Seed_End()
	move_ret seed[1]
	c_ret

;; TERM 64: '*GUARD*'(body_to_lpda_handler(_B, wait, _C, ('*WAIT*'(_C) :> fail), _D))
c_code local build_ref_64
	ret_reg &ref[64]
	call_c   build_ref_34()
	call_c   build_ref_63()
	call_c   Dyam_Create_Unary(&ref[34],&ref[63])
	move_ret ref[64]
	c_ret

;; TERM 63: body_to_lpda_handler(_B, wait, _C, ('*WAIT*'(_C) :> fail), _D)
c_code local build_ref_63
	ret_reg &ref[63]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_588()
	call_c   Dyam_Create_Unary(&ref[588],V(2))
	move_ret R(0)
	call_c   build_ref_213()
	call_c   Dyam_Create_Binary(I(9),R(0),&ref[213])
	move_ret R(0)
	call_c   build_ref_586()
	call_c   build_ref_587()
	call_c   Dyam_Term_Start(&ref[586],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(&ref[587])
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[63]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 587: wait
c_code local build_ref_587
	ret_reg &ref[587]
	call_c   Dyam_Create_Atom("wait")
	move_ret ref[587]
	c_ret

;; TERM 588: '*WAIT*'
c_code local build_ref_588
	ret_reg &ref[588]
	call_c   Dyam_Create_Atom("*WAIT*")
	move_ret ref[588]
	c_ret

pl_code local fun12
	call_c   build_ref_64()
	call_c   Dyam_Unify_Item(&ref[64])
	fail_ret
	pl_ret

;; TERM 65: '*GUARD*'(body_to_lpda_handler(_B, wait, _C, ('*WAIT*'(_C) :> fail), _D)) :> []
c_code local build_ref_65
	ret_reg &ref[65]
	call_c   build_ref_64()
	call_c   Dyam_Create_Binary(I(9),&ref[64],I(0))
	move_ret ref[65]
	c_ret

c_code local build_seed_39
	ret_reg &seed[39]
	call_c   build_ref_33()
	call_c   build_ref_74()
	call_c   Dyam_Seed_Start(&ref[33],&ref[74],I(0),fun0,1)
	call_c   build_ref_73()
	call_c   Dyam_Seed_Add_Comp(&ref[73],fun13,0)
	call_c   Dyam_Seed_End()
	move_ret seed[39]
	c_ret

;; TERM 73: '*GUARD*'(pgm_to_lpda('$query'(_B, _C), '*CITEM*'(start, start)))
c_code local build_ref_73
	ret_reg &ref[73]
	call_c   build_ref_34()
	call_c   build_ref_72()
	call_c   Dyam_Create_Unary(&ref[34],&ref[72])
	move_ret ref[73]
	c_ret

;; TERM 72: pgm_to_lpda('$query'(_B, _C), '*CITEM*'(start, start))
c_code local build_ref_72
	ret_reg &ref[72]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_590()
	call_c   Dyam_Create_Binary(&ref[590],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_29()
	call_c   build_ref_591()
	call_c   Dyam_Create_Binary(&ref[29],&ref[591],&ref[591])
	move_ret R(1)
	call_c   build_ref_589()
	call_c   Dyam_Create_Binary(&ref[589],R(0),R(1))
	move_ret ref[72]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 589: pgm_to_lpda
c_code local build_ref_589
	ret_reg &ref[589]
	call_c   Dyam_Create_Atom("pgm_to_lpda")
	move_ret ref[589]
	c_ret

;; TERM 591: start
c_code local build_ref_591
	ret_reg &ref[591]
	call_c   Dyam_Create_Atom("start")
	move_ret ref[591]
	c_ret

;; TERM 590: '$query'
c_code local build_ref_590
	ret_reg &ref[590]
	call_c   Dyam_Create_Atom("$query")
	move_ret ref[590]
	c_ret

;; TERM 75: main_module
c_code local build_ref_75
	ret_reg &ref[75]
	call_c   Dyam_Create_Atom("main_module")
	move_ret ref[75]
	c_ret

long local pool_fun13[3]=[2,build_ref_73,build_ref_75]

pl_code local fun13
	call_c   Dyam_Pool(pool_fun13)
	call_c   Dyam_Unify_Item(&ref[73])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[75], R(0)
	move     0, R(1)
	call_c   Dyam_Reg_Deallocate(1)
	pl_jump  pred_record_without_doublon_1()

;; TERM 74: '*GUARD*'(pgm_to_lpda('$query'(_B, _C), '*CITEM*'(start, start))) :> []
c_code local build_ref_74
	ret_reg &ref[74]
	call_c   build_ref_73()
	call_c   Dyam_Create_Binary(I(9),&ref[73],I(0))
	move_ret ref[74]
	c_ret

c_code local build_seed_34
	ret_reg &seed[34]
	call_c   build_ref_33()
	call_c   build_ref_68()
	call_c   Dyam_Seed_Start(&ref[33],&ref[68],I(0),fun0,1)
	call_c   build_ref_67()
	call_c   Dyam_Seed_Add_Comp(&ref[67],fun24,0)
	call_c   Dyam_Seed_End()
	move_ret seed[34]
	c_ret

;; TERM 67: '*GUARD*'(pgm_to_lpda((_B :- _C), _D))
c_code local build_ref_67
	ret_reg &ref[67]
	call_c   build_ref_34()
	call_c   build_ref_66()
	call_c   Dyam_Create_Unary(&ref[34],&ref[66])
	move_ret ref[67]
	c_ret

;; TERM 66: pgm_to_lpda((_B :- _C), _D)
c_code local build_ref_66
	ret_reg &ref[66]
	call_c   build_ref_589()
	call_c   build_ref_460()
	call_c   Dyam_Create_Binary(&ref[589],&ref[460],V(3))
	move_ret ref[66]
	c_ret

;; TERM 460: _B :- _C
c_code local build_ref_460
	ret_reg &ref[460]
	call_c   build_ref_592()
	call_c   Dyam_Create_Binary(&ref[592],V(1),V(2))
	move_ret ref[460]
	c_ret

;; TERM 592: :-
c_code local build_ref_592
	ret_reg &ref[592]
	call_c   Dyam_Create_Atom(":-")
	move_ret ref[592]
	c_ret

c_code local build_seed_52
	ret_reg &seed[52]
	call_c   build_ref_34()
	call_c   build_ref_70()
	call_c   Dyam_Seed_Start(&ref[34],&ref[70],I(0),fun1,1)
	call_c   build_ref_71()
	call_c   Dyam_Seed_Add_Comp(&ref[71],&ref[70],0)
	call_c   Dyam_Seed_End()
	move_ret seed[52]
	c_ret

;; TERM 71: '*GUARD*'(clause_to_lpda((_B :- _C), _D)) :> '$$HOLE$$'
c_code local build_ref_71
	ret_reg &ref[71]
	call_c   build_ref_70()
	call_c   Dyam_Create_Binary(I(9),&ref[70],I(7))
	move_ret ref[71]
	c_ret

;; TERM 70: '*GUARD*'(clause_to_lpda((_B :- _C), _D))
c_code local build_ref_70
	ret_reg &ref[70]
	call_c   build_ref_34()
	call_c   build_ref_69()
	call_c   Dyam_Create_Unary(&ref[34],&ref[69])
	move_ret ref[70]
	c_ret

;; TERM 69: clause_to_lpda((_B :- _C), _D)
c_code local build_ref_69
	ret_reg &ref[69]
	call_c   build_ref_593()
	call_c   build_ref_460()
	call_c   Dyam_Create_Binary(&ref[593],&ref[460],V(3))
	move_ret ref[69]
	c_ret

;; TERM 593: clause_to_lpda
c_code local build_ref_593
	ret_reg &ref[593]
	call_c   Dyam_Create_Atom("clause_to_lpda")
	move_ret ref[593]
	c_ret

long local pool_fun24[3]=[2,build_ref_67,build_seed_52]

pl_code local fun24
	call_c   Dyam_Pool(pool_fun24)
	call_c   Dyam_Unify_Item(&ref[67])
	fail_ret
	pl_jump  fun15(&seed[52],1)

;; TERM 68: '*GUARD*'(pgm_to_lpda((_B :- _C), _D)) :> []
c_code local build_ref_68
	ret_reg &ref[68]
	call_c   build_ref_67()
	call_c   Dyam_Create_Binary(I(9),&ref[67],I(0))
	move_ret ref[68]
	c_ret

c_code local build_seed_3
	ret_reg &seed[3]
	call_c   build_ref_33()
	call_c   build_ref_78()
	call_c   Dyam_Seed_Start(&ref[33],&ref[78],I(0),fun0,1)
	call_c   build_ref_77()
	call_c   Dyam_Seed_Add_Comp(&ref[77],fun14,0)
	call_c   Dyam_Seed_End()
	move_ret seed[3]
	c_ret

;; TERM 77: '*GUARD*'(body_to_lpda_handler(_B, '$internal_tail'(_C), _D, ('*INTERNAL_TAIL*'(_C) :> _D), _E))
c_code local build_ref_77
	ret_reg &ref[77]
	call_c   build_ref_34()
	call_c   build_ref_76()
	call_c   Dyam_Create_Unary(&ref[34],&ref[76])
	move_ret ref[77]
	c_ret

;; TERM 76: body_to_lpda_handler(_B, '$internal_tail'(_C), _D, ('*INTERNAL_TAIL*'(_C) :> _D), _E)
c_code local build_ref_76
	ret_reg &ref[76]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_594()
	call_c   Dyam_Create_Unary(&ref[594],V(2))
	move_ret R(0)
	call_c   build_ref_595()
	call_c   Dyam_Create_Unary(&ref[595],V(2))
	move_ret R(1)
	call_c   Dyam_Create_Binary(I(9),R(1),V(3))
	move_ret R(1)
	call_c   build_ref_586()
	call_c   Dyam_Term_Start(&ref[586],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[76]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 595: '*INTERNAL_TAIL*'
c_code local build_ref_595
	ret_reg &ref[595]
	call_c   Dyam_Create_Atom("*INTERNAL_TAIL*")
	move_ret ref[595]
	c_ret

;; TERM 594: '$internal_tail'
c_code local build_ref_594
	ret_reg &ref[594]
	call_c   Dyam_Create_Atom("$internal_tail")
	move_ret ref[594]
	c_ret

pl_code local fun14
	call_c   build_ref_77()
	call_c   Dyam_Unify_Item(&ref[77])
	fail_ret
	pl_ret

;; TERM 78: '*GUARD*'(body_to_lpda_handler(_B, '$internal_tail'(_C), _D, ('*INTERNAL_TAIL*'(_C) :> _D), _E)) :> []
c_code local build_ref_78
	ret_reg &ref[78]
	call_c   build_ref_77()
	call_c   Dyam_Create_Binary(I(9),&ref[77],I(0))
	move_ret ref[78]
	c_ret

c_code local build_seed_40
	ret_reg &seed[40]
	call_c   build_ref_33()
	call_c   build_ref_91()
	call_c   Dyam_Seed_Start(&ref[33],&ref[91],I(0),fun0,1)
	call_c   build_ref_90()
	call_c   Dyam_Seed_Add_Comp(&ref[90],fun25,0)
	call_c   Dyam_Seed_End()
	move_ret seed[40]
	c_ret

;; TERM 90: '*GUARD*'(pgm_to_lpda('$loader'(_B, _C), _D))
c_code local build_ref_90
	ret_reg &ref[90]
	call_c   build_ref_34()
	call_c   build_ref_89()
	call_c   Dyam_Create_Unary(&ref[34],&ref[89])
	move_ret ref[90]
	c_ret

;; TERM 89: pgm_to_lpda('$loader'(_B, _C), _D)
c_code local build_ref_89
	ret_reg &ref[89]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_596()
	call_c   Dyam_Create_Binary(&ref[596],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_589()
	call_c   Dyam_Create_Binary(&ref[589],R(0),V(3))
	move_ret ref[89]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 596: '$loader'
c_code local build_ref_596
	ret_reg &ref[596]
	call_c   Dyam_Create_Atom("$loader")
	move_ret ref[596]
	c_ret

c_code local build_seed_54
	ret_reg &seed[54]
	call_c   build_ref_34()
	call_c   build_ref_93()
	call_c   Dyam_Seed_Start(&ref[34],&ref[93],I(0),fun1,1)
	call_c   build_ref_94()
	call_c   Dyam_Seed_Add_Comp(&ref[94],&ref[93],0)
	call_c   Dyam_Seed_End()
	move_ret seed[54]
	c_ret

;; TERM 94: '*GUARD*'(pgm_to_lpda(_C, _E)) :> '$$HOLE$$'
c_code local build_ref_94
	ret_reg &ref[94]
	call_c   build_ref_93()
	call_c   Dyam_Create_Binary(I(9),&ref[93],I(7))
	move_ret ref[94]
	c_ret

;; TERM 93: '*GUARD*'(pgm_to_lpda(_C, _E))
c_code local build_ref_93
	ret_reg &ref[93]
	call_c   build_ref_34()
	call_c   build_ref_92()
	call_c   Dyam_Create_Unary(&ref[34],&ref[92])
	move_ret ref[93]
	c_ret

;; TERM 92: pgm_to_lpda(_C, _E)
c_code local build_ref_92
	ret_reg &ref[92]
	call_c   build_ref_589()
	call_c   Dyam_Create_Binary(&ref[589],V(2),V(4))
	move_ret ref[92]
	c_ret

c_code local build_seed_55
	ret_reg &seed[55]
	call_c   build_ref_34()
	call_c   build_ref_96()
	call_c   Dyam_Seed_Start(&ref[34],&ref[96],I(0),fun1,1)
	call_c   build_ref_97()
	call_c   Dyam_Seed_Add_Comp(&ref[97],&ref[96],0)
	call_c   Dyam_Seed_End()
	move_ret seed[55]
	c_ret

;; TERM 97: '*GUARD*'(build_cond_loader(_F, _B, _E, _D)) :> '$$HOLE$$'
c_code local build_ref_97
	ret_reg &ref[97]
	call_c   build_ref_96()
	call_c   Dyam_Create_Binary(I(9),&ref[96],I(7))
	move_ret ref[97]
	c_ret

;; TERM 96: '*GUARD*'(build_cond_loader(_F, _B, _E, _D))
c_code local build_ref_96
	ret_reg &ref[96]
	call_c   build_ref_34()
	call_c   build_ref_95()
	call_c   Dyam_Create_Unary(&ref[34],&ref[95])
	move_ret ref[96]
	c_ret

;; TERM 95: build_cond_loader(_F, _B, _E, _D)
c_code local build_ref_95
	ret_reg &ref[95]
	call_c   build_ref_597()
	call_c   Dyam_Term_Start(&ref[597],4)
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[95]
	c_ret

;; TERM 597: build_cond_loader
c_code local build_ref_597
	ret_reg &ref[597]
	call_c   Dyam_Create_Atom("build_cond_loader")
	move_ret ref[597]
	c_ret

long local pool_fun25[4]=[3,build_ref_90,build_seed_54,build_seed_55]

pl_code local fun25
	call_c   Dyam_Pool(pool_fun25)
	call_c   Dyam_Unify_Item(&ref[90])
	fail_ret
	call_c   Dyam_Allocate(0)
	pl_call  fun15(&seed[54],1)
	call_c   Dyam_Deallocate()
	pl_jump  fun15(&seed[55],1)

;; TERM 91: '*GUARD*'(pgm_to_lpda('$loader'(_B, _C), _D)) :> []
c_code local build_ref_91
	ret_reg &ref[91]
	call_c   build_ref_90()
	call_c   Dyam_Create_Binary(I(9),&ref[90],I(0))
	move_ret ref[91]
	c_ret

c_code local build_seed_22
	ret_reg &seed[22]
	call_c   build_ref_33()
	call_c   build_ref_100()
	call_c   Dyam_Seed_Start(&ref[33],&ref[100],I(0),fun0,1)
	call_c   build_ref_99()
	call_c   Dyam_Seed_Add_Comp(&ref[99],fun31,0)
	call_c   Dyam_Seed_End()
	move_ret seed[22]
	c_ret

;; TERM 99: '*GUARD*'(body_to_lpda_handler(_B, rcg_phrase(_C), _D, _E, _F))
c_code local build_ref_99
	ret_reg &ref[99]
	call_c   build_ref_34()
	call_c   build_ref_98()
	call_c   Dyam_Create_Unary(&ref[34],&ref[98])
	move_ret ref[99]
	c_ret

;; TERM 98: body_to_lpda_handler(_B, rcg_phrase(_C), _D, _E, _F)
c_code local build_ref_98
	ret_reg &ref[98]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_598()
	call_c   Dyam_Create_Unary(&ref[598],V(2))
	move_ret R(0)
	call_c   build_ref_586()
	call_c   Dyam_Term_Start(&ref[586],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[98]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 598: rcg_phrase
c_code local build_ref_598
	ret_reg &ref[598]
	call_c   Dyam_Create_Atom("rcg_phrase")
	move_ret ref[598]
	c_ret

c_code local build_seed_56
	ret_reg &seed[56]
	call_c   build_ref_34()
	call_c   build_ref_102()
	call_c   Dyam_Seed_Start(&ref[34],&ref[102],I(0),fun1,1)
	call_c   build_ref_103()
	call_c   Dyam_Seed_Add_Comp(&ref[103],&ref[102],0)
	call_c   Dyam_Seed_End()
	move_ret seed[56]
	c_ret

;; TERM 103: '*GUARD*'(rcg_body_to_lpda(_B, _C, _D, _E, _F)) :> '$$HOLE$$'
c_code local build_ref_103
	ret_reg &ref[103]
	call_c   build_ref_102()
	call_c   Dyam_Create_Binary(I(9),&ref[102],I(7))
	move_ret ref[103]
	c_ret

;; TERM 102: '*GUARD*'(rcg_body_to_lpda(_B, _C, _D, _E, _F))
c_code local build_ref_102
	ret_reg &ref[102]
	call_c   build_ref_34()
	call_c   build_ref_101()
	call_c   Dyam_Create_Unary(&ref[34],&ref[101])
	move_ret ref[102]
	c_ret

;; TERM 101: rcg_body_to_lpda(_B, _C, _D, _E, _F)
c_code local build_ref_101
	ret_reg &ref[101]
	call_c   build_ref_599()
	call_c   Dyam_Term_Start(&ref[599],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[101]
	c_ret

;; TERM 599: rcg_body_to_lpda
c_code local build_ref_599
	ret_reg &ref[599]
	call_c   Dyam_Create_Atom("rcg_body_to_lpda")
	move_ret ref[599]
	c_ret

long local pool_fun31[3]=[2,build_ref_99,build_seed_56]

pl_code local fun31
	call_c   Dyam_Pool(pool_fun31)
	call_c   Dyam_Unify_Item(&ref[99])
	fail_ret
	pl_jump  fun15(&seed[56],1)

;; TERM 100: '*GUARD*'(body_to_lpda_handler(_B, rcg_phrase(_C), _D, _E, _F)) :> []
c_code local build_ref_100
	ret_reg &ref[100]
	call_c   build_ref_99()
	call_c   Dyam_Create_Binary(I(9),&ref[99],I(0))
	move_ret ref[100]
	c_ret

c_code local build_seed_20
	ret_reg &seed[20]
	call_c   build_ref_33()
	call_c   build_ref_106()
	call_c   Dyam_Seed_Start(&ref[33],&ref[106],I(0),fun0,1)
	call_c   build_ref_105()
	call_c   Dyam_Seed_Add_Comp(&ref[105],fun32,0)
	call_c   Dyam_Seed_End()
	move_ret seed[20]
	c_ret

;; TERM 105: '*GUARD*'(body_to_lpda_handler(_B, '$answers'(_C), _D, _E, _F))
c_code local build_ref_105
	ret_reg &ref[105]
	call_c   build_ref_34()
	call_c   build_ref_104()
	call_c   Dyam_Create_Unary(&ref[34],&ref[104])
	move_ret ref[105]
	c_ret

;; TERM 104: body_to_lpda_handler(_B, '$answers'(_C), _D, _E, _F)
c_code local build_ref_104
	ret_reg &ref[104]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_600()
	call_c   Dyam_Create_Unary(&ref[600],V(2))
	move_ret R(0)
	call_c   build_ref_586()
	call_c   Dyam_Term_Start(&ref[586],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[104]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 600: '$answers'
c_code local build_ref_600
	ret_reg &ref[600]
	call_c   Dyam_Create_Atom("$answers")
	move_ret ref[600]
	c_ret

c_code local build_seed_57
	ret_reg &seed[57]
	call_c   build_ref_34()
	call_c   build_ref_108()
	call_c   Dyam_Seed_Start(&ref[34],&ref[108],I(0),fun1,1)
	call_c   build_ref_109()
	call_c   Dyam_Seed_Add_Comp(&ref[109],&ref[108],0)
	call_c   Dyam_Seed_End()
	move_ret seed[57]
	c_ret

;; TERM 109: '*GUARD*'(body_to_lpda(_B, _C, _D, _E, answer(_F))) :> '$$HOLE$$'
c_code local build_ref_109
	ret_reg &ref[109]
	call_c   build_ref_108()
	call_c   Dyam_Create_Binary(I(9),&ref[108],I(7))
	move_ret ref[109]
	c_ret

;; TERM 108: '*GUARD*'(body_to_lpda(_B, _C, _D, _E, answer(_F)))
c_code local build_ref_108
	ret_reg &ref[108]
	call_c   build_ref_34()
	call_c   build_ref_107()
	call_c   Dyam_Create_Unary(&ref[34],&ref[107])
	move_ret ref[108]
	c_ret

;; TERM 107: body_to_lpda(_B, _C, _D, _E, answer(_F))
c_code local build_ref_107
	ret_reg &ref[107]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_602()
	call_c   Dyam_Create_Unary(&ref[602],V(5))
	move_ret R(0)
	call_c   build_ref_601()
	call_c   Dyam_Term_Start(&ref[601],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_End()
	move_ret ref[107]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 601: body_to_lpda
c_code local build_ref_601
	ret_reg &ref[601]
	call_c   Dyam_Create_Atom("body_to_lpda")
	move_ret ref[601]
	c_ret

;; TERM 602: answer
c_code local build_ref_602
	ret_reg &ref[602]
	call_c   Dyam_Create_Atom("answer")
	move_ret ref[602]
	c_ret

long local pool_fun32[3]=[2,build_ref_105,build_seed_57]

pl_code local fun32
	call_c   Dyam_Pool(pool_fun32)
	call_c   Dyam_Unify_Item(&ref[105])
	fail_ret
	pl_jump  fun15(&seed[57],1)

;; TERM 106: '*GUARD*'(body_to_lpda_handler(_B, '$answers'(_C), _D, _E, _F)) :> []
c_code local build_ref_106
	ret_reg &ref[106]
	call_c   build_ref_105()
	call_c   Dyam_Create_Binary(I(9),&ref[105],I(0))
	move_ret ref[106]
	c_ret

c_code local build_seed_30
	ret_reg &seed[30]
	call_c   build_ref_33()
	call_c   build_ref_112()
	call_c   Dyam_Seed_Start(&ref[33],&ref[112],I(0),fun0,1)
	call_c   build_ref_111()
	call_c   Dyam_Seed_Add_Comp(&ref[111],fun26,0)
	call_c   Dyam_Seed_End()
	move_ret seed[30]
	c_ret

;; TERM 111: '*GUARD*'(body_to_lpda_handler(_B, (_C = _D), _E, _F, _G))
c_code local build_ref_111
	ret_reg &ref[111]
	call_c   build_ref_34()
	call_c   build_ref_110()
	call_c   Dyam_Create_Unary(&ref[34],&ref[110])
	move_ret ref[111]
	c_ret

;; TERM 110: body_to_lpda_handler(_B, (_C = _D), _E, _F, _G)
c_code local build_ref_110
	ret_reg &ref[110]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_603()
	call_c   Dyam_Create_Binary(&ref[603],V(2),V(3))
	move_ret R(0)
	call_c   build_ref_586()
	call_c   Dyam_Term_Start(&ref[586],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[110]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 603: =
c_code local build_ref_603
	ret_reg &ref[603]
	call_c   Dyam_Create_Atom("=")
	move_ret ref[603]
	c_ret

c_code local build_seed_58
	ret_reg &seed[58]
	call_c   build_ref_34()
	call_c   build_ref_114()
	call_c   Dyam_Seed_Start(&ref[34],&ref[114],I(0),fun1,1)
	call_c   build_ref_115()
	call_c   Dyam_Seed_Add_Comp(&ref[115],&ref[114],0)
	call_c   Dyam_Seed_End()
	move_ret seed[58]
	c_ret

;; TERM 115: '*GUARD*'(destructure_unify(_C, _D, _E, _F)) :> '$$HOLE$$'
c_code local build_ref_115
	ret_reg &ref[115]
	call_c   build_ref_114()
	call_c   Dyam_Create_Binary(I(9),&ref[114],I(7))
	move_ret ref[115]
	c_ret

;; TERM 114: '*GUARD*'(destructure_unify(_C, _D, _E, _F))
c_code local build_ref_114
	ret_reg &ref[114]
	call_c   build_ref_34()
	call_c   build_ref_113()
	call_c   Dyam_Create_Unary(&ref[34],&ref[113])
	move_ret ref[114]
	c_ret

;; TERM 113: destructure_unify(_C, _D, _E, _F)
c_code local build_ref_113
	ret_reg &ref[113]
	call_c   build_ref_604()
	call_c   Dyam_Term_Start(&ref[604],4)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[113]
	c_ret

;; TERM 604: destructure_unify
c_code local build_ref_604
	ret_reg &ref[604]
	call_c   Dyam_Create_Atom("destructure_unify")
	move_ret ref[604]
	c_ret

long local pool_fun26[3]=[2,build_ref_111,build_seed_58]

pl_code local fun26
	call_c   Dyam_Pool(pool_fun26)
	call_c   Dyam_Unify_Item(&ref[111])
	fail_ret
	pl_jump  fun15(&seed[58],1)

;; TERM 112: '*GUARD*'(body_to_lpda_handler(_B, (_C = _D), _E, _F, _G)) :> []
c_code local build_ref_112
	ret_reg &ref[112]
	call_c   build_ref_111()
	call_c   Dyam_Create_Binary(I(9),&ref[111],I(0))
	move_ret ref[112]
	c_ret

c_code local build_seed_2
	ret_reg &seed[2]
	call_c   build_ref_33()
	call_c   build_ref_88()
	call_c   Dyam_Seed_Start(&ref[33],&ref[88],I(0),fun0,1)
	call_c   build_ref_87()
	call_c   Dyam_Seed_Add_Comp(&ref[87],fun22,0)
	call_c   Dyam_Seed_End()
	move_ret seed[2]
	c_ret

;; TERM 87: '*GUARD*'(body_to_lpda_handler(_B, '$interface'(_C, _D), _E, ('$interface'(_C, _D) :> _E), _F))
c_code local build_ref_87
	ret_reg &ref[87]
	call_c   build_ref_34()
	call_c   build_ref_86()
	call_c   Dyam_Create_Unary(&ref[34],&ref[86])
	move_ret ref[87]
	c_ret

;; TERM 86: body_to_lpda_handler(_B, '$interface'(_C, _D), _E, ('$interface'(_C, _D) :> _E), _F)
c_code local build_ref_86
	ret_reg &ref[86]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_605()
	call_c   Dyam_Create_Binary(&ref[605],V(2),V(3))
	move_ret R(0)
	call_c   Dyam_Create_Binary(I(9),R(0),V(4))
	move_ret R(1)
	call_c   build_ref_586()
	call_c   Dyam_Term_Start(&ref[586],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[86]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 605: '$interface'
c_code local build_ref_605
	ret_reg &ref[605]
	call_c   Dyam_Create_Atom("$interface")
	move_ret ref[605]
	c_ret

pl_code local fun22
	call_c   build_ref_87()
	call_c   Dyam_Unify_Item(&ref[87])
	fail_ret
	pl_ret

;; TERM 88: '*GUARD*'(body_to_lpda_handler(_B, '$interface'(_C, _D), _E, ('$interface'(_C, _D) :> _E), _F)) :> []
c_code local build_ref_88
	ret_reg &ref[88]
	call_c   build_ref_87()
	call_c   Dyam_Create_Binary(I(9),&ref[87],I(0))
	move_ret ref[88]
	c_ret

c_code local build_seed_25
	ret_reg &seed[25]
	call_c   build_ref_33()
	call_c   build_ref_124()
	call_c   Dyam_Seed_Start(&ref[33],&ref[124],I(0),fun0,1)
	call_c   build_ref_123()
	call_c   Dyam_Seed_Add_Comp(&ref[123],fun33,0)
	call_c   Dyam_Seed_End()
	move_ret seed[25]
	c_ret

;; TERM 123: '*GUARD*'(body_to_lpda_handler(_B, '*COMMIT*'(_C), _D, ('*SETCUT*' :> _E), _F))
c_code local build_ref_123
	ret_reg &ref[123]
	call_c   build_ref_34()
	call_c   build_ref_122()
	call_c   Dyam_Create_Unary(&ref[34],&ref[122])
	move_ret ref[123]
	c_ret

;; TERM 122: body_to_lpda_handler(_B, '*COMMIT*'(_C), _D, ('*SETCUT*' :> _E), _F)
c_code local build_ref_122
	ret_reg &ref[122]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_606()
	call_c   Dyam_Create_Unary(&ref[606],V(2))
	move_ret R(0)
	call_c   build_ref_607()
	call_c   Dyam_Create_Binary(I(9),&ref[607],V(4))
	move_ret R(1)
	call_c   build_ref_586()
	call_c   Dyam_Term_Start(&ref[586],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[122]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 607: '*SETCUT*'
c_code local build_ref_607
	ret_reg &ref[607]
	call_c   Dyam_Create_Atom("*SETCUT*")
	move_ret ref[607]
	c_ret

;; TERM 606: '*COMMIT*'
c_code local build_ref_606
	ret_reg &ref[606]
	call_c   Dyam_Create_Atom("*COMMIT*")
	move_ret ref[606]
	c_ret

c_code local build_seed_60
	ret_reg &seed[60]
	call_c   build_ref_34()
	call_c   build_ref_126()
	call_c   Dyam_Seed_Start(&ref[34],&ref[126],I(0),fun1,1)
	call_c   build_ref_127()
	call_c   Dyam_Seed_Add_Comp(&ref[127],&ref[126],0)
	call_c   Dyam_Seed_End()
	move_ret seed[60]
	c_ret

;; TERM 127: '*GUARD*'(body_to_lpda(_B, _C, ('*CUT*' :> _D), _E, rec_prolog)) :> '$$HOLE$$'
c_code local build_ref_127
	ret_reg &ref[127]
	call_c   build_ref_126()
	call_c   Dyam_Create_Binary(I(9),&ref[126],I(7))
	move_ret ref[127]
	c_ret

;; TERM 126: '*GUARD*'(body_to_lpda(_B, _C, ('*CUT*' :> _D), _E, rec_prolog))
c_code local build_ref_126
	ret_reg &ref[126]
	call_c   build_ref_34()
	call_c   build_ref_125()
	call_c   Dyam_Create_Unary(&ref[34],&ref[125])
	move_ret ref[126]
	c_ret

;; TERM 125: body_to_lpda(_B, _C, ('*CUT*' :> _D), _E, rec_prolog)
c_code local build_ref_125
	ret_reg &ref[125]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_608()
	call_c   Dyam_Create_Binary(I(9),&ref[608],V(3))
	move_ret R(0)
	call_c   build_ref_601()
	call_c   build_ref_544()
	call_c   Dyam_Term_Start(&ref[601],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(&ref[544])
	call_c   Dyam_Term_End()
	move_ret ref[125]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 608: '*CUT*'
c_code local build_ref_608
	ret_reg &ref[608]
	call_c   Dyam_Create_Atom("*CUT*")
	move_ret ref[608]
	c_ret

long local pool_fun33[3]=[2,build_ref_123,build_seed_60]

pl_code local fun33
	call_c   Dyam_Pool(pool_fun33)
	call_c   Dyam_Unify_Item(&ref[123])
	fail_ret
	pl_jump  fun15(&seed[60],1)

;; TERM 124: '*GUARD*'(body_to_lpda_handler(_B, '*COMMIT*'(_C), _D, ('*SETCUT*' :> _E), _F)) :> []
c_code local build_ref_124
	ret_reg &ref[124]
	call_c   build_ref_123()
	call_c   Dyam_Create_Binary(I(9),&ref[123],I(0))
	move_ret ref[124]
	c_ret

c_code local build_seed_21
	ret_reg &seed[21]
	call_c   build_ref_33()
	call_c   build_ref_118()
	call_c   Dyam_Seed_Start(&ref[33],&ref[118],I(0),fun0,1)
	call_c   build_ref_117()
	call_c   Dyam_Seed_Add_Comp(&ref[117],fun27,0)
	call_c   Dyam_Seed_End()
	move_ret seed[21]
	c_ret

;; TERM 117: '*GUARD*'(body_to_lpda_handler(_B, wait(_C), _D, ('*WAIT*'(_D) :> _E), _F))
c_code local build_ref_117
	ret_reg &ref[117]
	call_c   build_ref_34()
	call_c   build_ref_116()
	call_c   Dyam_Create_Unary(&ref[34],&ref[116])
	move_ret ref[117]
	c_ret

;; TERM 116: body_to_lpda_handler(_B, wait(_C), _D, ('*WAIT*'(_D) :> _E), _F)
c_code local build_ref_116
	ret_reg &ref[116]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_587()
	call_c   Dyam_Create_Unary(&ref[587],V(2))
	move_ret R(0)
	call_c   build_ref_588()
	call_c   Dyam_Create_Unary(&ref[588],V(3))
	move_ret R(1)
	call_c   Dyam_Create_Binary(I(9),R(1),V(4))
	move_ret R(1)
	call_c   build_ref_586()
	call_c   Dyam_Term_Start(&ref[586],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[116]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_59
	ret_reg &seed[59]
	call_c   build_ref_34()
	call_c   build_ref_120()
	call_c   Dyam_Seed_Start(&ref[34],&ref[120],I(0),fun1,1)
	call_c   build_ref_121()
	call_c   Dyam_Seed_Add_Comp(&ref[121],&ref[120],0)
	call_c   Dyam_Seed_End()
	move_ret seed[59]
	c_ret

;; TERM 121: '*GUARD*'(body_to_lpda(_B, _C, fail, _E, _F)) :> '$$HOLE$$'
c_code local build_ref_121
	ret_reg &ref[121]
	call_c   build_ref_120()
	call_c   Dyam_Create_Binary(I(9),&ref[120],I(7))
	move_ret ref[121]
	c_ret

;; TERM 120: '*GUARD*'(body_to_lpda(_B, _C, fail, _E, _F))
c_code local build_ref_120
	ret_reg &ref[120]
	call_c   build_ref_34()
	call_c   build_ref_119()
	call_c   Dyam_Create_Unary(&ref[34],&ref[119])
	move_ret ref[120]
	c_ret

;; TERM 119: body_to_lpda(_B, _C, fail, _E, _F)
c_code local build_ref_119
	ret_reg &ref[119]
	call_c   build_ref_601()
	call_c   build_ref_213()
	call_c   Dyam_Term_Start(&ref[601],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(&ref[213])
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[119]
	c_ret

long local pool_fun27[3]=[2,build_ref_117,build_seed_59]

pl_code local fun27
	call_c   Dyam_Pool(pool_fun27)
	call_c   Dyam_Unify_Item(&ref[117])
	fail_ret
	pl_jump  fun15(&seed[59],1)

;; TERM 118: '*GUARD*'(body_to_lpda_handler(_B, wait(_C), _D, ('*WAIT*'(_D) :> _E), _F)) :> []
c_code local build_ref_118
	ret_reg &ref[118]
	call_c   build_ref_117()
	call_c   Dyam_Create_Binary(I(9),&ref[117],I(0))
	move_ret ref[118]
	c_ret

c_code local build_seed_27
	ret_reg &seed[27]
	call_c   build_ref_33()
	call_c   build_ref_132()
	call_c   Dyam_Seed_Start(&ref[33],&ref[132],I(0),fun0,1)
	call_c   build_ref_131()
	call_c   Dyam_Seed_Add_Comp(&ref[131],fun57,0)
	call_c   Dyam_Seed_End()
	move_ret seed[27]
	c_ret

;; TERM 131: '*GUARD*'(body_to_lpda_handler(_B, (_C -> _D), _E, _F, _G))
c_code local build_ref_131
	ret_reg &ref[131]
	call_c   build_ref_34()
	call_c   build_ref_130()
	call_c   Dyam_Create_Unary(&ref[34],&ref[130])
	move_ret ref[131]
	c_ret

;; TERM 130: body_to_lpda_handler(_B, (_C -> _D), _E, _F, _G)
c_code local build_ref_130
	ret_reg &ref[130]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_609()
	call_c   Dyam_Create_Binary(&ref[609],V(2),V(3))
	move_ret R(0)
	call_c   build_ref_586()
	call_c   Dyam_Term_Start(&ref[586],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[130]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 609: ->
c_code local build_ref_609
	ret_reg &ref[609]
	call_c   Dyam_Create_Atom("->")
	move_ret ref[609]
	c_ret

c_code local build_seed_61
	ret_reg &seed[61]
	call_c   build_ref_34()
	call_c   build_ref_134()
	call_c   Dyam_Seed_Start(&ref[34],&ref[134],I(0),fun1,1)
	call_c   build_ref_135()
	call_c   Dyam_Seed_Add_Comp(&ref[135],&ref[134],0)
	call_c   Dyam_Seed_End()
	move_ret seed[61]
	c_ret

;; TERM 135: '*GUARD*'(body_to_lpda(_B, (_C -> _D ; fail), _E, _F, _G)) :> '$$HOLE$$'
c_code local build_ref_135
	ret_reg &ref[135]
	call_c   build_ref_134()
	call_c   Dyam_Create_Binary(I(9),&ref[134],I(7))
	move_ret ref[135]
	c_ret

;; TERM 134: '*GUARD*'(body_to_lpda(_B, (_C -> _D ; fail), _E, _F, _G))
c_code local build_ref_134
	ret_reg &ref[134]
	call_c   build_ref_34()
	call_c   build_ref_133()
	call_c   Dyam_Create_Unary(&ref[34],&ref[133])
	move_ret ref[134]
	c_ret

;; TERM 133: body_to_lpda(_B, (_C -> _D ; fail), _E, _F, _G)
c_code local build_ref_133
	ret_reg &ref[133]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_609()
	call_c   Dyam_Create_Binary(&ref[609],V(2),V(3))
	move_ret R(0)
	call_c   build_ref_213()
	call_c   Dyam_Create_Binary(I(5),R(0),&ref[213])
	move_ret R(0)
	call_c   build_ref_601()
	call_c   Dyam_Term_Start(&ref[601],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[133]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun57[3]=[2,build_ref_131,build_seed_61]

pl_code local fun57
	call_c   Dyam_Pool(pool_fun57)
	call_c   Dyam_Unify_Item(&ref[131])
	fail_ret
	pl_jump  fun15(&seed[61],1)

;; TERM 132: '*GUARD*'(body_to_lpda_handler(_B, (_C -> _D), _E, _F, _G)) :> []
c_code local build_ref_132
	ret_reg &ref[132]
	call_c   build_ref_131()
	call_c   Dyam_Create_Binary(I(9),&ref[131],I(0))
	move_ret ref[132]
	c_ret

c_code local build_seed_23
	ret_reg &seed[23]
	call_c   build_ref_33()
	call_c   build_ref_138()
	call_c   Dyam_Seed_Start(&ref[33],&ref[138],I(0),fun0,1)
	call_c   build_ref_137()
	call_c   Dyam_Seed_Add_Comp(&ref[137],fun40,0)
	call_c   Dyam_Seed_End()
	move_ret seed[23]
	c_ret

;; TERM 137: '*GUARD*'(body_to_lpda_handler(_B, tag_phrase(_C, _D, _E), _F, _G, _H))
c_code local build_ref_137
	ret_reg &ref[137]
	call_c   build_ref_34()
	call_c   build_ref_136()
	call_c   Dyam_Create_Unary(&ref[34],&ref[136])
	move_ret ref[137]
	c_ret

;; TERM 136: body_to_lpda_handler(_B, tag_phrase(_C, _D, _E), _F, _G, _H)
c_code local build_ref_136
	ret_reg &ref[136]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_610()
	call_c   Dyam_Term_Start(&ref[610],3)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_586()
	call_c   Dyam_Term_Start(&ref[586],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[136]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 610: tag_phrase
c_code local build_ref_610
	ret_reg &ref[610]
	call_c   Dyam_Create_Atom("tag_phrase")
	move_ret ref[610]
	c_ret

c_code local build_seed_62
	ret_reg &seed[62]
	call_c   build_ref_34()
	call_c   build_ref_140()
	call_c   Dyam_Seed_Start(&ref[34],&ref[140],I(0),fun1,1)
	call_c   build_ref_141()
	call_c   Dyam_Seed_Add_Comp(&ref[141],&ref[140],0)
	call_c   Dyam_Seed_End()
	move_ret seed[62]
	c_ret

;; TERM 141: '*GUARD*'(tag_normalize_and_compile(_B, _C, _D, _E, _F, _G, _H)) :> '$$HOLE$$'
c_code local build_ref_141
	ret_reg &ref[141]
	call_c   build_ref_140()
	call_c   Dyam_Create_Binary(I(9),&ref[140],I(7))
	move_ret ref[141]
	c_ret

;; TERM 140: '*GUARD*'(tag_normalize_and_compile(_B, _C, _D, _E, _F, _G, _H))
c_code local build_ref_140
	ret_reg &ref[140]
	call_c   build_ref_34()
	call_c   build_ref_139()
	call_c   Dyam_Create_Unary(&ref[34],&ref[139])
	move_ret ref[140]
	c_ret

;; TERM 139: tag_normalize_and_compile(_B, _C, _D, _E, _F, _G, _H)
c_code local build_ref_139
	ret_reg &ref[139]
	call_c   build_ref_611()
	call_c   Dyam_Term_Start(&ref[611],7)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[139]
	c_ret

;; TERM 611: tag_normalize_and_compile
c_code local build_ref_611
	ret_reg &ref[611]
	call_c   Dyam_Create_Atom("tag_normalize_and_compile")
	move_ret ref[611]
	c_ret

long local pool_fun40[3]=[2,build_ref_137,build_seed_62]

pl_code local fun40
	call_c   Dyam_Pool(pool_fun40)
	call_c   Dyam_Unify_Item(&ref[137])
	fail_ret
	pl_jump  fun15(&seed[62],1)

;; TERM 138: '*GUARD*'(body_to_lpda_handler(_B, tag_phrase(_C, _D, _E), _F, _G, _H)) :> []
c_code local build_ref_138
	ret_reg &ref[138]
	call_c   build_ref_137()
	call_c   Dyam_Create_Binary(I(9),&ref[137],I(0))
	move_ret ref[138]
	c_ret

c_code local build_seed_19
	ret_reg &seed[19]
	call_c   build_ref_33()
	call_c   build_ref_144()
	call_c   Dyam_Seed_Start(&ref[33],&ref[144],I(0),fun0,1)
	call_c   build_ref_143()
	call_c   Dyam_Seed_Add_Comp(&ref[143],fun35,0)
	call_c   Dyam_Seed_End()
	move_ret seed[19]
	c_ret

;; TERM 143: '*GUARD*'(body_to_lpda_handler(_B, '$toplevel'(_C), _D, _E, _F))
c_code local build_ref_143
	ret_reg &ref[143]
	call_c   build_ref_34()
	call_c   build_ref_142()
	call_c   Dyam_Create_Unary(&ref[34],&ref[142])
	move_ret ref[143]
	c_ret

;; TERM 142: body_to_lpda_handler(_B, '$toplevel'(_C), _D, _E, _F)
c_code local build_ref_142
	ret_reg &ref[142]
	call_c   build_ref_586()
	call_c   build_ref_145()
	call_c   Dyam_Term_Start(&ref[586],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(&ref[145])
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[142]
	c_ret

;; TERM 145: '$toplevel'(_C)
c_code local build_ref_145
	ret_reg &ref[145]
	call_c   build_ref_612()
	call_c   Dyam_Create_Unary(&ref[612],V(2))
	move_ret ref[145]
	c_ret

;; TERM 612: '$toplevel'
c_code local build_ref_612
	ret_reg &ref[612]
	call_c   Dyam_Create_Atom("$toplevel")
	move_ret ref[612]
	c_ret

c_code local build_seed_63
	ret_reg &seed[63]
	call_c   build_ref_34()
	call_c   build_ref_147()
	call_c   Dyam_Seed_Start(&ref[34],&ref[147],I(0),fun1,1)
	call_c   build_ref_148()
	call_c   Dyam_Seed_Add_Comp(&ref[148],&ref[147],0)
	call_c   Dyam_Seed_End()
	move_ret seed[63]
	c_ret

;; TERM 148: '*GUARD*'(body_to_lpda(_B, _G, _D, _E, _F)) :> '$$HOLE$$'
c_code local build_ref_148
	ret_reg &ref[148]
	call_c   build_ref_147()
	call_c   Dyam_Create_Binary(I(9),&ref[147],I(7))
	move_ret ref[148]
	c_ret

;; TERM 147: '*GUARD*'(body_to_lpda(_B, _G, _D, _E, _F))
c_code local build_ref_147
	ret_reg &ref[147]
	call_c   build_ref_34()
	call_c   build_ref_146()
	call_c   Dyam_Create_Unary(&ref[34],&ref[146])
	move_ret ref[147]
	c_ret

;; TERM 146: body_to_lpda(_B, _G, _D, _E, _F)
c_code local build_ref_146
	ret_reg &ref[146]
	call_c   build_ref_601()
	call_c   Dyam_Term_Start(&ref[601],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[146]
	c_ret

long local pool_fun35[4]=[3,build_ref_143,build_ref_145,build_seed_63]

pl_code local fun35
	call_c   Dyam_Pool(pool_fun35)
	call_c   Dyam_Unify_Item(&ref[143])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[145], R(0)
	move     S(5), R(1)
	move     V(6), R(2)
	move     S(5), R(3)
	pl_call  pred_try_compiler_toplevel_2()
	call_c   Dyam_Deallocate()
	pl_jump  fun15(&seed[63],1)

;; TERM 144: '*GUARD*'(body_to_lpda_handler(_B, '$toplevel'(_C), _D, _E, _F)) :> []
c_code local build_ref_144
	ret_reg &ref[144]
	call_c   build_ref_143()
	call_c   Dyam_Create_Binary(I(9),&ref[143],I(0))
	move_ret ref[144]
	c_ret

c_code local build_seed_38
	ret_reg &seed[38]
	call_c   build_ref_33()
	call_c   build_ref_151()
	call_c   Dyam_Seed_Start(&ref[33],&ref[151],I(0),fun0,1)
	call_c   build_ref_150()
	call_c   Dyam_Seed_Add_Comp(&ref[150],fun36,0)
	call_c   Dyam_Seed_End()
	move_ret seed[38]
	c_ret

;; TERM 150: '*GUARD*'(pgm_to_lpda('$query'(_B, _C), ('*FIRST*'(start) :> _D)))
c_code local build_ref_150
	ret_reg &ref[150]
	call_c   build_ref_34()
	call_c   build_ref_149()
	call_c   Dyam_Create_Unary(&ref[34],&ref[149])
	move_ret ref[150]
	c_ret

;; TERM 149: pgm_to_lpda('$query'(_B, _C), ('*FIRST*'(start) :> _D))
c_code local build_ref_149
	ret_reg &ref[149]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_590()
	call_c   Dyam_Create_Binary(&ref[590],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_25()
	call_c   build_ref_591()
	call_c   Dyam_Create_Unary(&ref[25],&ref[591])
	move_ret R(1)
	call_c   Dyam_Create_Binary(I(9),R(1),V(3))
	move_ret R(1)
	call_c   build_ref_589()
	call_c   Dyam_Create_Binary(&ref[589],R(0),R(1))
	move_ret ref[149]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_64
	ret_reg &seed[64]
	call_c   build_ref_34()
	call_c   build_ref_153()
	call_c   Dyam_Seed_Start(&ref[34],&ref[153],I(0),fun1,1)
	call_c   build_ref_154()
	call_c   Dyam_Seed_Add_Comp(&ref[154],&ref[153],0)
	call_c   Dyam_Seed_End()
	move_ret seed[64]
	c_ret

;; TERM 154: '*GUARD*'(body_to_lpda(_E, _B, '*ANSWER*'{answer=> _C}, _D, dyalog)) :> '$$HOLE$$'
c_code local build_ref_154
	ret_reg &ref[154]
	call_c   build_ref_153()
	call_c   Dyam_Create_Binary(I(9),&ref[153],I(7))
	move_ret ref[154]
	c_ret

;; TERM 153: '*GUARD*'(body_to_lpda(_E, _B, '*ANSWER*'{answer=> _C}, _D, dyalog))
c_code local build_ref_153
	ret_reg &ref[153]
	call_c   build_ref_34()
	call_c   build_ref_152()
	call_c   Dyam_Create_Unary(&ref[34],&ref[152])
	move_ret ref[153]
	c_ret

;; TERM 152: body_to_lpda(_E, _B, '*ANSWER*'{answer=> _C}, _D, dyalog)
c_code local build_ref_152
	ret_reg &ref[152]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_613()
	call_c   Dyam_Create_Unary(&ref[613],V(2))
	move_ret R(0)
	call_c   build_ref_601()
	call_c   build_ref_576()
	call_c   Dyam_Term_Start(&ref[601],5)
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(&ref[576])
	call_c   Dyam_Term_End()
	move_ret ref[152]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 613: '*ANSWER*'!'$ft'
c_code local build_ref_613
	ret_reg &ref[613]
	call_c   build_ref_614()
	call_c   Dyam_Create_Atom_Module("$ft",&ref[614])
	move_ret ref[613]
	c_ret

;; TERM 614: '*ANSWER*'
c_code local build_ref_614
	ret_reg &ref[614]
	call_c   Dyam_Create_Atom("*ANSWER*")
	move_ret ref[614]
	c_ret

long local pool_fun36[3]=[2,build_ref_150,build_seed_64]

pl_code local fun36
	call_c   Dyam_Pool(pool_fun36)
	call_c   Dyam_Unify_Item(&ref[150])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(1))
	move     V(4), R(2)
	move     S(5), R(3)
	move     V(5), R(4)
	move     S(5), R(5)
	pl_call  pred_my_numbervars_3()
	call_c   Dyam_Deallocate()
	pl_jump  fun15(&seed[64],1)

;; TERM 151: '*GUARD*'(pgm_to_lpda('$query'(_B, _C), ('*FIRST*'(start) :> _D))) :> []
c_code local build_ref_151
	ret_reg &ref[151]
	call_c   build_ref_150()
	call_c   Dyam_Create_Binary(I(9),&ref[150],I(0))
	move_ret ref[151]
	c_ret

c_code local build_seed_26
	ret_reg &seed[26]
	call_c   build_ref_33()
	call_c   build_ref_164()
	call_c   Dyam_Seed_Start(&ref[33],&ref[164],I(0),fun0,1)
	call_c   build_ref_163()
	call_c   Dyam_Seed_Add_Comp(&ref[163],fun39,0)
	call_c   Dyam_Seed_End()
	move_ret seed[26]
	c_ret

;; TERM 163: '*GUARD*'(body_to_lpda_handler(_B, (_C ; _D), _E, _F, _G))
c_code local build_ref_163
	ret_reg &ref[163]
	call_c   build_ref_34()
	call_c   build_ref_162()
	call_c   Dyam_Create_Unary(&ref[34],&ref[162])
	move_ret ref[163]
	c_ret

;; TERM 162: body_to_lpda_handler(_B, (_C ; _D), _E, _F, _G)
c_code local build_ref_162
	ret_reg &ref[162]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Binary(I(5),V(2),V(3))
	move_ret R(0)
	call_c   build_ref_586()
	call_c   Dyam_Term_Start(&ref[586],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[162]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_66
	ret_reg &seed[66]
	call_c   build_ref_34()
	call_c   build_ref_167()
	call_c   Dyam_Seed_Start(&ref[34],&ref[167],I(0),fun1,1)
	call_c   build_ref_168()
	call_c   Dyam_Seed_Add_Comp(&ref[168],&ref[167],0)
	call_c   Dyam_Seed_End()
	move_ret seed[66]
	c_ret

;; TERM 168: '*GUARD*'(body_to_lpda(_B, _C, _J, _K, _G)) :> '$$HOLE$$'
c_code local build_ref_168
	ret_reg &ref[168]
	call_c   build_ref_167()
	call_c   Dyam_Create_Binary(I(9),&ref[167],I(7))
	move_ret ref[168]
	c_ret

;; TERM 167: '*GUARD*'(body_to_lpda(_B, _C, _J, _K, _G))
c_code local build_ref_167
	ret_reg &ref[167]
	call_c   build_ref_34()
	call_c   build_ref_166()
	call_c   Dyam_Create_Unary(&ref[34],&ref[166])
	move_ret ref[167]
	c_ret

;; TERM 166: body_to_lpda(_B, _C, _J, _K, _G)
c_code local build_ref_166
	ret_reg &ref[166]
	call_c   build_ref_601()
	call_c   Dyam_Term_Start(&ref[601],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[166]
	c_ret

c_code local build_seed_67
	ret_reg &seed[67]
	call_c   build_ref_34()
	call_c   build_ref_170()
	call_c   Dyam_Seed_Start(&ref[34],&ref[170],I(0),fun1,1)
	call_c   build_ref_171()
	call_c   Dyam_Seed_Add_Comp(&ref[171],&ref[170],0)
	call_c   Dyam_Seed_End()
	move_ret seed[67]
	c_ret

;; TERM 171: '*GUARD*'(body_to_lpda(_B, _D, _J, _L, _G)) :> '$$HOLE$$'
c_code local build_ref_171
	ret_reg &ref[171]
	call_c   build_ref_170()
	call_c   Dyam_Create_Binary(I(9),&ref[170],I(7))
	move_ret ref[171]
	c_ret

;; TERM 170: '*GUARD*'(body_to_lpda(_B, _D, _J, _L, _G))
c_code local build_ref_170
	ret_reg &ref[170]
	call_c   build_ref_34()
	call_c   build_ref_169()
	call_c   Dyam_Create_Unary(&ref[34],&ref[169])
	move_ret ref[170]
	c_ret

;; TERM 169: body_to_lpda(_B, _D, _J, _L, _G)
c_code local build_ref_169
	ret_reg &ref[169]
	call_c   build_ref_601()
	call_c   Dyam_Term_Start(&ref[601],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[169]
	c_ret

long local pool_fun37[3]=[2,build_seed_66,build_seed_67]

pl_code local fun38
	call_c   Dyam_Remove_Choice()
fun37:
	call_c   Dyam_Reg_Load(0,V(4))
	call_c   Dyam_Reg_Load(2,V(5))
	move     V(9), R(4)
	move     S(5), R(5)
	move     V(10), R(6)
	move     S(5), R(7)
	move     V(11), R(8)
	move     S(5), R(9)
	pl_call  pred_handle_choice_5()
	pl_call  fun15(&seed[66],1)
	call_c   Dyam_Deallocate()
	pl_jump  fun15(&seed[67],1)


;; TERM 165: _H -> _I
c_code local build_ref_165
	ret_reg &ref[165]
	call_c   build_ref_609()
	call_c   Dyam_Create_Binary(&ref[609],V(7),V(8))
	move_ret ref[165]
	c_ret

long local pool_fun39[4]=[65538,build_ref_163,build_ref_165,pool_fun37]

pl_code local fun39
	call_c   Dyam_Pool(pool_fun39)
	call_c   Dyam_Unify_Item(&ref[163])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun38)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(2),&ref[165])
	fail_ret
	call_c   Dyam_Cut()
	pl_fail

;; TERM 164: '*GUARD*'(body_to_lpda_handler(_B, (_C ; _D), _E, _F, _G)) :> []
c_code local build_ref_164
	ret_reg &ref[164]
	call_c   build_ref_163()
	call_c   Dyam_Create_Binary(I(9),&ref[163],I(0))
	move_ret ref[164]
	c_ret

c_code local build_seed_18
	ret_reg &seed[18]
	call_c   build_ref_33()
	call_c   build_ref_157()
	call_c   Dyam_Seed_Start(&ref[33],&ref[157],I(0),fun0,1)
	call_c   build_ref_156()
	call_c   Dyam_Seed_Add_Comp(&ref[156],fun112,0)
	call_c   Dyam_Seed_End()
	move_ret seed[18]
	c_ret

;; TERM 156: '*GUARD*'(body_to_lpda_handler(_B, (_C @*), _D, _E, _F))
c_code local build_ref_156
	ret_reg &ref[156]
	call_c   build_ref_34()
	call_c   build_ref_155()
	call_c   Dyam_Create_Unary(&ref[34],&ref[155])
	move_ret ref[156]
	c_ret

;; TERM 155: body_to_lpda_handler(_B, (_C @*), _D, _E, _F)
c_code local build_ref_155
	ret_reg &ref[155]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_615()
	call_c   Dyam_Create_Unary(&ref[615],V(2))
	move_ret R(0)
	call_c   build_ref_586()
	call_c   Dyam_Term_Start(&ref[586],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[155]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 615: @*
c_code local build_ref_615
	ret_reg &ref[615]
	call_c   Dyam_Create_Atom("@*")
	move_ret ref[615]
	c_ret

c_code local build_seed_65
	ret_reg &seed[65]
	call_c   build_ref_34()
	call_c   build_ref_160()
	call_c   Dyam_Seed_Start(&ref[34],&ref[160],I(0),fun1,1)
	call_c   build_ref_161()
	call_c   Dyam_Seed_Add_Comp(&ref[161],&ref[160],0)
	call_c   Dyam_Seed_End()
	move_ret seed[65]
	c_ret

;; TERM 161: '*GUARD*'(body_to_lpda_handler(_B, @*{goal=> _H, vars=> _G, from=> _I, to=> _J, collect_first=> _K, collect_last=> _L, collect_loop=> _M, collect_next=> _N, collect_pred=> _O}, _D, _E, _F)) :> '$$HOLE$$'
c_code local build_ref_161
	ret_reg &ref[161]
	call_c   build_ref_160()
	call_c   Dyam_Create_Binary(I(9),&ref[160],I(7))
	move_ret ref[161]
	c_ret

;; TERM 160: '*GUARD*'(body_to_lpda_handler(_B, @*{goal=> _H, vars=> _G, from=> _I, to=> _J, collect_first=> _K, collect_last=> _L, collect_loop=> _M, collect_next=> _N, collect_pred=> _O}, _D, _E, _F))
c_code local build_ref_160
	ret_reg &ref[160]
	call_c   build_ref_34()
	call_c   build_ref_159()
	call_c   Dyam_Create_Unary(&ref[34],&ref[159])
	move_ret ref[160]
	c_ret

;; TERM 159: body_to_lpda_handler(_B, @*{goal=> _H, vars=> _G, from=> _I, to=> _J, collect_first=> _K, collect_last=> _L, collect_loop=> _M, collect_next=> _N, collect_pred=> _O}, _D, _E, _F)
c_code local build_ref_159
	ret_reg &ref[159]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_616()
	call_c   Dyam_Term_Start(&ref[616],9)
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_586()
	call_c   Dyam_Term_Start(&ref[586],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[159]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 616: @*!'$ft'
c_code local build_ref_616
	ret_reg &ref[616]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_625()
	call_c   Dyam_Create_List(&ref[625],I(0))
	move_ret R(0)
	call_c   build_ref_624()
	call_c   Dyam_Create_List(&ref[624],R(0))
	move_ret R(0)
	call_c   build_ref_623()
	call_c   Dyam_Create_List(&ref[623],R(0))
	move_ret R(0)
	call_c   build_ref_622()
	call_c   Dyam_Create_List(&ref[622],R(0))
	move_ret R(0)
	call_c   build_ref_621()
	call_c   Dyam_Create_List(&ref[621],R(0))
	move_ret R(0)
	call_c   build_ref_620()
	call_c   Dyam_Create_List(&ref[620],R(0))
	move_ret R(0)
	call_c   build_ref_619()
	call_c   Dyam_Create_List(&ref[619],R(0))
	move_ret R(0)
	call_c   build_ref_618()
	call_c   Dyam_Create_List(&ref[618],R(0))
	move_ret R(0)
	call_c   build_ref_617()
	call_c   Dyam_Create_List(&ref[617],R(0))
	move_ret R(0)
	call_c   build_ref_615()
	call_c   Dyam_Create_Atom_Module("$ft",&ref[615])
	move_ret ref[616]
	call_c   DYAM_Feature_2(&ref[615],R(0))
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 617: goal
c_code local build_ref_617
	ret_reg &ref[617]
	call_c   Dyam_Create_Atom("goal")
	move_ret ref[617]
	c_ret

;; TERM 618: vars
c_code local build_ref_618
	ret_reg &ref[618]
	call_c   Dyam_Create_Atom("vars")
	move_ret ref[618]
	c_ret

;; TERM 619: from
c_code local build_ref_619
	ret_reg &ref[619]
	call_c   Dyam_Create_Atom("from")
	move_ret ref[619]
	c_ret

;; TERM 620: to
c_code local build_ref_620
	ret_reg &ref[620]
	call_c   Dyam_Create_Atom("to")
	move_ret ref[620]
	c_ret

;; TERM 621: collect_first
c_code local build_ref_621
	ret_reg &ref[621]
	call_c   Dyam_Create_Atom("collect_first")
	move_ret ref[621]
	c_ret

;; TERM 622: collect_last
c_code local build_ref_622
	ret_reg &ref[622]
	call_c   Dyam_Create_Atom("collect_last")
	move_ret ref[622]
	c_ret

;; TERM 623: collect_loop
c_code local build_ref_623
	ret_reg &ref[623]
	call_c   Dyam_Create_Atom("collect_loop")
	move_ret ref[623]
	c_ret

;; TERM 624: collect_next
c_code local build_ref_624
	ret_reg &ref[624]
	call_c   Dyam_Create_Atom("collect_next")
	move_ret ref[624]
	c_ret

;; TERM 625: collect_pred
c_code local build_ref_625
	ret_reg &ref[625]
	call_c   Dyam_Create_Atom("collect_pred")
	move_ret ref[625]
	c_ret

long local pool_fun110[2]=[1,build_seed_65]

pl_code local fun111
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(2),V(7))
	fail_ret
fun110:
	call_c   Dyam_Deallocate()
	pl_jump  fun15(&seed[65],1)


;; TERM 158: _G ^ _H
c_code local build_ref_158
	ret_reg &ref[158]
	call_c   build_ref_626()
	call_c   Dyam_Create_Binary(&ref[626],V(6),V(7))
	move_ret ref[158]
	c_ret

;; TERM 626: ^
c_code local build_ref_626
	ret_reg &ref[626]
	call_c   Dyam_Create_Atom("^")
	move_ret ref[626]
	c_ret

long local pool_fun112[5]=[131074,build_ref_156,build_ref_158,pool_fun110,pool_fun110]

pl_code local fun112
	call_c   Dyam_Pool(pool_fun112)
	call_c   Dyam_Unify_Item(&ref[156])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun111)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(2),&ref[158])
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun110()

;; TERM 157: '*GUARD*'(body_to_lpda_handler(_B, (_C @*), _D, _E, _F)) :> []
c_code local build_ref_157
	ret_reg &ref[157]
	call_c   build_ref_156()
	call_c   Dyam_Create_Binary(I(9),&ref[156],I(0))
	move_ret ref[157]
	c_ret

c_code local build_seed_29
	ret_reg &seed[29]
	call_c   build_ref_33()
	call_c   build_ref_180()
	call_c   Dyam_Seed_Start(&ref[33],&ref[180],I(0),fun0,1)
	call_c   build_ref_179()
	call_c   Dyam_Seed_Add_Comp(&ref[179],fun41,0)
	call_c   Dyam_Seed_End()
	move_ret seed[29]
	c_ret

;; TERM 179: '*GUARD*'(body_to_lpda_handler(_B, (_C , _D), _E, _F, _G))
c_code local build_ref_179
	ret_reg &ref[179]
	call_c   build_ref_34()
	call_c   build_ref_178()
	call_c   Dyam_Create_Unary(&ref[34],&ref[178])
	move_ret ref[179]
	c_ret

;; TERM 178: body_to_lpda_handler(_B, (_C , _D), _E, _F, _G)
c_code local build_ref_178
	ret_reg &ref[178]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Binary(I(4),V(2),V(3))
	move_ret R(0)
	call_c   build_ref_586()
	call_c   Dyam_Term_Start(&ref[586],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[178]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_69
	ret_reg &seed[69]
	call_c   build_ref_34()
	call_c   build_ref_182()
	call_c   Dyam_Seed_Start(&ref[34],&ref[182],I(0),fun1,1)
	call_c   build_ref_183()
	call_c   Dyam_Seed_Add_Comp(&ref[183],&ref[182],0)
	call_c   Dyam_Seed_End()
	move_ret seed[69]
	c_ret

;; TERM 183: '*GUARD*'(body_to_lpda(_B, _D, _E, _H, _G)) :> '$$HOLE$$'
c_code local build_ref_183
	ret_reg &ref[183]
	call_c   build_ref_182()
	call_c   Dyam_Create_Binary(I(9),&ref[182],I(7))
	move_ret ref[183]
	c_ret

;; TERM 182: '*GUARD*'(body_to_lpda(_B, _D, _E, _H, _G))
c_code local build_ref_182
	ret_reg &ref[182]
	call_c   build_ref_34()
	call_c   build_ref_181()
	call_c   Dyam_Create_Unary(&ref[34],&ref[181])
	move_ret ref[182]
	c_ret

;; TERM 181: body_to_lpda(_B, _D, _E, _H, _G)
c_code local build_ref_181
	ret_reg &ref[181]
	call_c   build_ref_601()
	call_c   Dyam_Term_Start(&ref[601],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[181]
	c_ret

c_code local build_seed_70
	ret_reg &seed[70]
	call_c   build_ref_34()
	call_c   build_ref_185()
	call_c   Dyam_Seed_Start(&ref[34],&ref[185],I(0),fun1,1)
	call_c   build_ref_186()
	call_c   Dyam_Seed_Add_Comp(&ref[186],&ref[185],0)
	call_c   Dyam_Seed_End()
	move_ret seed[70]
	c_ret

;; TERM 186: '*GUARD*'(body_to_lpda(_B, _C, _H, _F, _G)) :> '$$HOLE$$'
c_code local build_ref_186
	ret_reg &ref[186]
	call_c   build_ref_185()
	call_c   Dyam_Create_Binary(I(9),&ref[185],I(7))
	move_ret ref[186]
	c_ret

;; TERM 185: '*GUARD*'(body_to_lpda(_B, _C, _H, _F, _G))
c_code local build_ref_185
	ret_reg &ref[185]
	call_c   build_ref_34()
	call_c   build_ref_184()
	call_c   Dyam_Create_Unary(&ref[34],&ref[184])
	move_ret ref[185]
	c_ret

;; TERM 184: body_to_lpda(_B, _C, _H, _F, _G)
c_code local build_ref_184
	ret_reg &ref[184]
	call_c   build_ref_601()
	call_c   Dyam_Term_Start(&ref[601],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[184]
	c_ret

long local pool_fun41[4]=[3,build_ref_179,build_seed_69,build_seed_70]

pl_code local fun41
	call_c   Dyam_Pool(pool_fun41)
	call_c   Dyam_Unify_Item(&ref[179])
	fail_ret
	call_c   Dyam_Allocate(0)
	pl_call  fun15(&seed[69],1)
	call_c   Dyam_Deallocate()
	pl_jump  fun15(&seed[70],1)

;; TERM 180: '*GUARD*'(body_to_lpda_handler(_B, (_C , _D), _E, _F, _G)) :> []
c_code local build_ref_180
	ret_reg &ref[180]
	call_c   build_ref_179()
	call_c   Dyam_Create_Binary(I(9),&ref[179],I(0))
	move_ret ref[180]
	c_ret

c_code local build_seed_36
	ret_reg &seed[36]
	call_c   build_ref_33()
	call_c   build_ref_189()
	call_c   Dyam_Seed_Start(&ref[33],&ref[189],I(0),fun0,1)
	call_c   build_ref_188()
	call_c   Dyam_Seed_Add_Comp(&ref[188],fun51,0)
	call_c   Dyam_Seed_End()
	move_ret seed[36]
	c_ret

;; TERM 188: '*GUARD*'(pgm_to_lpda(register_predicate((_B / _C)), ('*GUARD*'(_D) :> _E)))
c_code local build_ref_188
	ret_reg &ref[188]
	call_c   build_ref_34()
	call_c   build_ref_187()
	call_c   Dyam_Create_Unary(&ref[34],&ref[187])
	move_ret ref[188]
	c_ret

;; TERM 187: pgm_to_lpda(register_predicate((_B / _C)), ('*GUARD*'(_D) :> _E))
c_code local build_ref_187
	ret_reg &ref[187]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_581()
	call_c   build_ref_354()
	call_c   Dyam_Create_Unary(&ref[581],&ref[354])
	move_ret R(0)
	call_c   build_ref_34()
	call_c   Dyam_Create_Unary(&ref[34],V(3))
	move_ret R(1)
	call_c   Dyam_Create_Binary(I(9),R(1),V(4))
	move_ret R(1)
	call_c   build_ref_589()
	call_c   Dyam_Create_Binary(&ref[589],R(0),R(1))
	move_ret ref[187]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 354: _B / _C
c_code local build_ref_354
	ret_reg &ref[354]
	call_c   build_ref_627()
	call_c   Dyam_Create_Binary(&ref[627],V(1),V(2))
	move_ret ref[354]
	c_ret

;; TERM 627: /
c_code local build_ref_627
	ret_reg &ref[627]
	call_c   Dyam_Create_Atom("/")
	move_ret ref[627]
	c_ret

;; TERM 201: public('*default*')
c_code local build_ref_201
	ret_reg &ref[201]
	call_c   build_ref_628()
	call_c   build_ref_629()
	call_c   Dyam_Create_Unary(&ref[628],&ref[629])
	move_ret ref[201]
	c_ret

;; TERM 629: '*default*'
c_code local build_ref_629
	ret_reg &ref[629]
	call_c   Dyam_Create_Atom("*default*")
	move_ret ref[629]
	c_ret

;; TERM 628: public
c_code local build_ref_628
	ret_reg &ref[628]
	call_c   Dyam_Create_Atom("public")
	move_ret ref[628]
	c_ret

;; TERM 200: foreign((_B / _C), _G)
c_code local build_ref_200
	ret_reg &ref[200]
	call_c   build_ref_630()
	call_c   build_ref_354()
	call_c   Dyam_Create_Binary(&ref[630],&ref[354],V(6))
	move_ret ref[200]
	c_ret

;; TERM 630: foreign
c_code local build_ref_630
	ret_reg &ref[630]
	call_c   Dyam_Create_Atom("foreign")
	move_ret ref[630]
	c_ret

;; TERM 192: '$pcall'(_J)
c_code local build_ref_192
	ret_reg &ref[192]
	call_c   build_ref_631()
	call_c   Dyam_Create_Unary(&ref[631],V(9))
	move_ret ref[192]
	c_ret

;; TERM 631: '$pcall'
c_code local build_ref_631
	ret_reg &ref[631]
	call_c   Dyam_Create_Atom("$pcall")
	move_ret ref[631]
	c_ret

;; TERM 196: '$answers'(_F)
c_code local build_ref_196
	ret_reg &ref[196]
	call_c   build_ref_600()
	call_c   Dyam_Create_Unary(&ref[600],V(5))
	move_ret ref[196]
	c_ret

c_code local build_seed_71
	ret_reg &seed[71]
	call_c   build_ref_34()
	call_c   build_ref_194()
	call_c   Dyam_Seed_Start(&ref[34],&ref[194],I(0),fun1,1)
	call_c   build_ref_195()
	call_c   Dyam_Seed_Add_Comp(&ref[195],&ref[194],0)
	call_c   Dyam_Seed_End()
	move_ret seed[71]
	c_ret

;; TERM 195: '*GUARD*'(body_to_lpda(_H, _J, noop, _E, rec_prolog)) :> '$$HOLE$$'
c_code local build_ref_195
	ret_reg &ref[195]
	call_c   build_ref_194()
	call_c   Dyam_Create_Binary(I(9),&ref[194],I(7))
	move_ret ref[195]
	c_ret

;; TERM 194: '*GUARD*'(body_to_lpda(_H, _J, noop, _E, rec_prolog))
c_code local build_ref_194
	ret_reg &ref[194]
	call_c   build_ref_34()
	call_c   build_ref_193()
	call_c   Dyam_Create_Unary(&ref[34],&ref[193])
	move_ret ref[194]
	c_ret

;; TERM 193: body_to_lpda(_H, _J, noop, _E, rec_prolog)
c_code local build_ref_193
	ret_reg &ref[193]
	call_c   build_ref_601()
	call_c   build_ref_322()
	call_c   build_ref_544()
	call_c   Dyam_Term_Start(&ref[601],5)
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(&ref[322])
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(&ref[544])
	call_c   Dyam_Term_End()
	move_ret ref[193]
	c_ret

;; TERM 322: noop
c_code local build_ref_322
	ret_reg &ref[322]
	call_c   Dyam_Create_Atom("noop")
	move_ret ref[322]
	c_ret

long local pool_fun42[2]=[1,build_seed_71]

long local pool_fun43[3]=[65537,build_ref_196,pool_fun42]

pl_code local fun43
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(9),&ref[196])
	fail_ret
fun42:
	call_c   Dyam_Deallocate()
	pl_jump  fun15(&seed[71],1)


long local pool_fun44[4]=[131073,build_ref_192,pool_fun43,pool_fun42]

long local pool_fun45[3]=[65537,build_ref_200,pool_fun44]

pl_code local fun45
	call_c   Dyam_Remove_Choice()
	pl_call  Object_1(&ref[200])
fun44:
	call_c   Dyam_Reg_Load(0,V(5))
	move     V(7), R(2)
	move     S(5), R(3)
	move     V(8), R(4)
	move     S(5), R(5)
	pl_call  pred_my_numbervars_3()
	call_c   Dyam_Unify(V(3),&ref[192])
	fail_ret
	call_c   Dyam_Choice(fun43)
	call_c   Dyam_Unify(V(9),V(5))
	fail_ret
	pl_jump  fun42()


;; TERM 199: extensional _B / _C
c_code local build_ref_199
	ret_reg &ref[199]
	call_c   build_ref_632()
	call_c   build_ref_354()
	call_c   Dyam_Create_Unary(&ref[632],&ref[354])
	move_ret ref[199]
	c_ret

;; TERM 632: extensional
c_code local build_ref_632
	ret_reg &ref[632]
	call_c   Dyam_Create_Atom("extensional")
	move_ret ref[632]
	c_ret

long local pool_fun46[4]=[131073,build_ref_199,pool_fun45,pool_fun44]

pl_code local fun46
	call_c   Dyam_Update_Choice(fun45)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[199])
	call_c   Dyam_Cut()
	pl_jump  fun44()

;; TERM 198: light_tabular _B / _C
c_code local build_ref_198
	ret_reg &ref[198]
	call_c   build_ref_633()
	call_c   build_ref_354()
	call_c   Dyam_Create_Unary(&ref[633],&ref[354])
	move_ret ref[198]
	c_ret

;; TERM 633: light_tabular
c_code local build_ref_633
	ret_reg &ref[633]
	call_c   Dyam_Create_Atom("light_tabular")
	move_ret ref[633]
	c_ret

long local pool_fun47[4]=[131073,build_ref_198,pool_fun46,pool_fun44]

pl_code local fun47
	call_c   Dyam_Update_Choice(fun46)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[198])
	call_c   Dyam_Cut()
	pl_jump  fun44()

;; TERM 197: std_prolog _B / _C
c_code local build_ref_197
	ret_reg &ref[197]
	call_c   build_ref_634()
	call_c   build_ref_354()
	call_c   Dyam_Create_Unary(&ref[634],&ref[354])
	move_ret ref[197]
	c_ret

;; TERM 634: std_prolog
c_code local build_ref_634
	ret_reg &ref[634]
	call_c   Dyam_Create_Atom("std_prolog")
	move_ret ref[634]
	c_ret

long local pool_fun48[4]=[131073,build_ref_197,pool_fun47,pool_fun44]

pl_code local fun48
	call_c   Dyam_Update_Choice(fun47)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[197])
	call_c   Dyam_Cut()
	pl_jump  fun44()

;; TERM 191: rec_prolog _B / _C
c_code local build_ref_191
	ret_reg &ref[191]
	call_c   build_ref_544()
	call_c   build_ref_354()
	call_c   Dyam_Create_Unary(&ref[544],&ref[354])
	move_ret ref[191]
	c_ret

long local pool_fun49[4]=[131073,build_ref_191,pool_fun48,pool_fun44]

long local pool_fun50[3]=[65537,build_ref_201,pool_fun49]

pl_code local fun50
	call_c   Dyam_Remove_Choice()
	pl_call  Object_1(&ref[201])
fun49:
	call_c   DYAM_evpred_functor(V(5),V(1),V(2))
	fail_ret
	call_c   Dyam_Choice(fun48)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[191])
	call_c   Dyam_Cut()
	pl_jump  fun44()


;; TERM 190: public((_B / _C))
c_code local build_ref_190
	ret_reg &ref[190]
	call_c   build_ref_628()
	call_c   build_ref_354()
	call_c   Dyam_Create_Unary(&ref[628],&ref[354])
	move_ret ref[190]
	c_ret

long local pool_fun51[5]=[131074,build_ref_188,build_ref_190,pool_fun50,pool_fun49]

pl_code local fun51
	call_c   Dyam_Pool(pool_fun51)
	call_c   Dyam_Unify_Item(&ref[188])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun50)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[190])
	call_c   Dyam_Cut()
	pl_jump  fun49()

;; TERM 189: '*GUARD*'(pgm_to_lpda(register_predicate((_B / _C)), ('*GUARD*'(_D) :> _E))) :> []
c_code local build_ref_189
	ret_reg &ref[189]
	call_c   build_ref_188()
	call_c   Dyam_Create_Binary(I(9),&ref[188],I(0))
	move_ret ref[189]
	c_ret

c_code local build_seed_37
	ret_reg &seed[37]
	call_c   build_ref_33()
	call_c   build_ref_204()
	call_c   Dyam_Seed_Start(&ref[33],&ref[204],I(0),fun0,1)
	call_c   build_ref_203()
	call_c   Dyam_Seed_Add_Comp(&ref[203],fun56,0)
	call_c   Dyam_Seed_End()
	move_ret seed[37]
	c_ret

;; TERM 203: '*GUARD*'(pgm_to_lpda(register_predicate((_B / _C)), ('*PROLOG-FIRST*'(_D) :> _E)))
c_code local build_ref_203
	ret_reg &ref[203]
	call_c   build_ref_34()
	call_c   build_ref_202()
	call_c   Dyam_Create_Unary(&ref[34],&ref[202])
	move_ret ref[203]
	c_ret

;; TERM 202: pgm_to_lpda(register_predicate((_B / _C)), ('*PROLOG-FIRST*'(_D) :> _E))
c_code local build_ref_202
	ret_reg &ref[202]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_581()
	call_c   build_ref_354()
	call_c   Dyam_Create_Unary(&ref[581],&ref[354])
	move_ret R(0)
	call_c   build_ref_424()
	call_c   Dyam_Create_Unary(&ref[424],V(3))
	move_ret R(1)
	call_c   Dyam_Create_Binary(I(9),R(1),V(4))
	move_ret R(1)
	call_c   build_ref_589()
	call_c   Dyam_Create_Binary(&ref[589],R(0),R(1))
	move_ret ref[202]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 424: '*PROLOG-FIRST*'
c_code local build_ref_424
	ret_reg &ref[424]
	call_c   Dyam_Create_Atom("*PROLOG-FIRST*")
	move_ret ref[424]
	c_ret

;; TERM 205: '$call'(_I)
c_code local build_ref_205
	ret_reg &ref[205]
	call_c   build_ref_635()
	call_c   Dyam_Create_Unary(&ref[635],V(8))
	move_ret ref[205]
	c_ret

;; TERM 635: '$call'
c_code local build_ref_635
	ret_reg &ref[635]
	call_c   Dyam_Create_Atom("$call")
	move_ret ref[635]
	c_ret

c_code local build_seed_72
	ret_reg &seed[72]
	call_c   build_ref_34()
	call_c   build_ref_207()
	call_c   Dyam_Seed_Start(&ref[34],&ref[207],I(0),fun1,1)
	call_c   build_ref_208()
	call_c   Dyam_Seed_Add_Comp(&ref[208],&ref[207],0)
	call_c   Dyam_Seed_End()
	move_ret seed[72]
	c_ret

;; TERM 208: '*GUARD*'(body_to_lpda(_G, _I, '*PROLOG-LAST*', _E, prolog)) :> '$$HOLE$$'
c_code local build_ref_208
	ret_reg &ref[208]
	call_c   build_ref_207()
	call_c   Dyam_Create_Binary(I(9),&ref[207],I(7))
	move_ret ref[208]
	c_ret

;; TERM 207: '*GUARD*'(body_to_lpda(_G, _I, '*PROLOG-LAST*', _E, prolog))
c_code local build_ref_207
	ret_reg &ref[207]
	call_c   build_ref_34()
	call_c   build_ref_206()
	call_c   Dyam_Create_Unary(&ref[34],&ref[206])
	move_ret ref[207]
	c_ret

;; TERM 206: body_to_lpda(_G, _I, '*PROLOG-LAST*', _E, prolog)
c_code local build_ref_206
	ret_reg &ref[206]
	call_c   build_ref_601()
	call_c   build_ref_426()
	call_c   build_ref_579()
	call_c   Dyam_Term_Start(&ref[601],5)
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(&ref[426])
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(&ref[579])
	call_c   Dyam_Term_End()
	move_ret ref[206]
	c_ret

long local pool_fun52[2]=[1,build_seed_72]

long local pool_fun53[3]=[65537,build_ref_196,pool_fun52]

pl_code local fun53
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(8),&ref[196])
	fail_ret
fun52:
	call_c   Dyam_Deallocate()
	pl_jump  fun15(&seed[72],1)


long local pool_fun54[4]=[131073,build_ref_205,pool_fun53,pool_fun52]

long local pool_fun55[3]=[65537,build_ref_201,pool_fun54]

pl_code local fun55
	call_c   Dyam_Remove_Choice()
	pl_call  Object_1(&ref[201])
fun54:
	call_c   DYAM_evpred_functor(V(5),V(1),V(2))
	fail_ret
	call_c   Dyam_Reg_Load(0,V(5))
	move     V(6), R(2)
	move     S(5), R(3)
	move     V(7), R(4)
	move     S(5), R(5)
	pl_call  pred_my_numbervars_3()
	call_c   Dyam_Unify(V(3),&ref[205])
	fail_ret
	call_c   Dyam_Choice(fun53)
	call_c   Dyam_Unify(V(8),V(5))
	fail_ret
	pl_jump  fun52()


long local pool_fun56[5]=[131074,build_ref_203,build_ref_190,pool_fun55,pool_fun54]

pl_code local fun56
	call_c   Dyam_Pool(pool_fun56)
	call_c   Dyam_Unify_Item(&ref[203])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun55)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[190])
	call_c   Dyam_Cut()
	pl_jump  fun54()

;; TERM 204: '*GUARD*'(pgm_to_lpda(register_predicate((_B / _C)), ('*PROLOG-FIRST*'(_D) :> _E))) :> []
c_code local build_ref_204
	ret_reg &ref[204]
	call_c   build_ref_203()
	call_c   Dyam_Create_Binary(I(9),&ref[203],I(0))
	move_ret ref[204]
	c_ret

c_code local build_seed_28
	ret_reg &seed[28]
	call_c   build_ref_33()
	call_c   build_ref_174()
	call_c   Dyam_Seed_Start(&ref[33],&ref[174],I(0),fun0,1)
	call_c   build_ref_173()
	call_c   Dyam_Seed_Add_Comp(&ref[173],fun61,0)
	call_c   Dyam_Seed_End()
	move_ret seed[28]
	c_ret

;; TERM 173: '*GUARD*'(body_to_lpda_handler(_B, (_C -> _D ; _E), _F, _G, _H))
c_code local build_ref_173
	ret_reg &ref[173]
	call_c   build_ref_34()
	call_c   build_ref_172()
	call_c   Dyam_Create_Unary(&ref[34],&ref[172])
	move_ret ref[173]
	c_ret

;; TERM 172: body_to_lpda_handler(_B, (_C -> _D ; _E), _F, _G, _H)
c_code local build_ref_172
	ret_reg &ref[172]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_609()
	call_c   Dyam_Create_Binary(&ref[609],V(2),V(3))
	move_ret R(0)
	call_c   Dyam_Create_Binary(I(5),R(0),V(4))
	move_ret R(0)
	call_c   build_ref_586()
	call_c   Dyam_Term_Start(&ref[586],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[172]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_68
	ret_reg &seed[68]
	call_c   build_ref_34()
	call_c   build_ref_176()
	call_c   Dyam_Seed_Start(&ref[34],&ref[176],I(0),fun1,1)
	call_c   build_ref_177()
	call_c   Dyam_Seed_Add_Comp(&ref[177],&ref[176],0)
	call_c   Dyam_Seed_End()
	move_ret seed[68]
	c_ret

;; TERM 177: '*GUARD*'(body_to_lpda(_B, ('*COMMIT*'(_C) , _D ; _E), _F, _G, _H)) :> '$$HOLE$$'
c_code local build_ref_177
	ret_reg &ref[177]
	call_c   build_ref_176()
	call_c   Dyam_Create_Binary(I(9),&ref[176],I(7))
	move_ret ref[177]
	c_ret

;; TERM 176: '*GUARD*'(body_to_lpda(_B, ('*COMMIT*'(_C) , _D ; _E), _F, _G, _H))
c_code local build_ref_176
	ret_reg &ref[176]
	call_c   build_ref_34()
	call_c   build_ref_175()
	call_c   Dyam_Create_Unary(&ref[34],&ref[175])
	move_ret ref[176]
	c_ret

;; TERM 175: body_to_lpda(_B, ('*COMMIT*'(_C) , _D ; _E), _F, _G, _H)
c_code local build_ref_175
	ret_reg &ref[175]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_606()
	call_c   Dyam_Create_Unary(&ref[606],V(2))
	move_ret R(0)
	call_c   Dyam_Create_Binary(I(4),R(0),V(3))
	move_ret R(0)
	call_c   Dyam_Create_Binary(I(5),R(0),V(4))
	move_ret R(0)
	call_c   build_ref_601()
	call_c   Dyam_Term_Start(&ref[601],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[175]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun61[3]=[2,build_ref_173,build_seed_68]

pl_code local fun61
	call_c   Dyam_Pool(pool_fun61)
	call_c   Dyam_Unify_Item(&ref[173])
	fail_ret
	pl_jump  fun15(&seed[68],1)

;; TERM 174: '*GUARD*'(body_to_lpda_handler(_B, (_C -> _D ; _E), _F, _G, _H)) :> []
c_code local build_ref_174
	ret_reg &ref[174]
	call_c   build_ref_173()
	call_c   Dyam_Create_Binary(I(9),&ref[173],I(0))
	move_ret ref[174]
	c_ret

c_code local build_seed_10
	ret_reg &seed[10]
	call_c   build_ref_33()
	call_c   build_ref_211()
	call_c   Dyam_Seed_Start(&ref[33],&ref[211],I(0),fun0,1)
	call_c   build_ref_210()
	call_c   Dyam_Seed_Add_Comp(&ref[210],fun60,0)
	call_c   Dyam_Seed_End()
	move_ret seed[10]
	c_ret

;; TERM 210: '*GUARD*'(destructure_unify(_B, _C, _D, _E))
c_code local build_ref_210
	ret_reg &ref[210]
	call_c   build_ref_34()
	call_c   build_ref_209()
	call_c   Dyam_Create_Unary(&ref[34],&ref[209])
	move_ret ref[210]
	c_ret

;; TERM 209: destructure_unify(_B, _C, _D, _E)
c_code local build_ref_209
	ret_reg &ref[209]
	call_c   build_ref_604()
	call_c   Dyam_Term_Start(&ref[604],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[209]
	c_ret

;; TERM 214: fail :> _D
c_code local build_ref_214
	ret_reg &ref[214]
	call_c   build_ref_213()
	call_c   Dyam_Create_Binary(I(9),&ref[213],V(3))
	move_ret ref[214]
	c_ret

;; TERM 212: destructure_unify / 2
c_code local build_ref_212
	ret_reg &ref[212]
	call_c   build_ref_627()
	call_c   build_ref_604()
	call_c   Dyam_Create_Binary(&ref[627],&ref[604],N(2))
	move_ret ref[212]
	c_ret

long local pool_fun59[3]=[2,build_ref_214,build_ref_212]

pl_code local fun59
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(4),&ref[214])
	fail_ret
fun58:
	move     &ref[212], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Deallocate(1)
	pl_jump  pred_abolish_1()


long local pool_fun60[4]=[65538,build_ref_210,build_ref_212,pool_fun59]

pl_code local fun60
	call_c   Dyam_Pool(pool_fun60)
	call_c   Dyam_Unify_Item(&ref[210])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun59)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(2))
	call_c   Dyam_Reg_Load(4,V(3))
	call_c   Dyam_Reg_Load(6,V(4))
	pl_call  pred_destructure_unify_aux_4()
	call_c   Dyam_Cut()
	pl_jump  fun58()

;; TERM 211: '*GUARD*'(destructure_unify(_B, _C, _D, _E)) :> []
c_code local build_ref_211
	ret_reg &ref[211]
	call_c   build_ref_210()
	call_c   Dyam_Create_Binary(I(9),&ref[210],I(0))
	move_ret ref[211]
	c_ret

c_code local build_seed_9
	ret_reg &seed[9]
	call_c   build_ref_33()
	call_c   build_ref_217()
	call_c   Dyam_Seed_Start(&ref[33],&ref[217],I(0),fun0,1)
	call_c   build_ref_216()
	call_c   Dyam_Seed_Add_Comp(&ref[216],fun62,0)
	call_c   Dyam_Seed_End()
	move_ret seed[9]
	c_ret

;; TERM 216: '*GUARD*'(destructure_unify_args([_B|_C], [_D|_E], _F, _G))
c_code local build_ref_216
	ret_reg &ref[216]
	call_c   build_ref_34()
	call_c   build_ref_215()
	call_c   Dyam_Create_Unary(&ref[34],&ref[215])
	move_ret ref[216]
	c_ret

;; TERM 215: destructure_unify_args([_B|_C], [_D|_E], _F, _G)
c_code local build_ref_215
	ret_reg &ref[215]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   Dyam_Create_List(V(1),V(2))
	move_ret R(0)
	call_c   Dyam_Create_List(V(3),V(4))
	move_ret R(1)
	call_c   build_ref_584()
	call_c   Dyam_Term_Start(&ref[584],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[215]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_73
	ret_reg &seed[73]
	call_c   build_ref_34()
	call_c   build_ref_219()
	call_c   Dyam_Seed_Start(&ref[34],&ref[219],I(0),fun1,1)
	call_c   build_ref_220()
	call_c   Dyam_Seed_Add_Comp(&ref[220],&ref[219],0)
	call_c   Dyam_Seed_End()
	move_ret seed[73]
	c_ret

;; TERM 220: '*GUARD*'(destructure_unify_args(_C, _E, _F, _H)) :> '$$HOLE$$'
c_code local build_ref_220
	ret_reg &ref[220]
	call_c   build_ref_219()
	call_c   Dyam_Create_Binary(I(9),&ref[219],I(7))
	move_ret ref[220]
	c_ret

;; TERM 219: '*GUARD*'(destructure_unify_args(_C, _E, _F, _H))
c_code local build_ref_219
	ret_reg &ref[219]
	call_c   build_ref_34()
	call_c   build_ref_218()
	call_c   Dyam_Create_Unary(&ref[34],&ref[218])
	move_ret ref[219]
	c_ret

;; TERM 218: destructure_unify_args(_C, _E, _F, _H)
c_code local build_ref_218
	ret_reg &ref[218]
	call_c   build_ref_584()
	call_c   Dyam_Term_Start(&ref[584],4)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[218]
	c_ret

long local pool_fun62[3]=[2,build_ref_216,build_seed_73]

pl_code local fun62
	call_c   Dyam_Pool(pool_fun62)
	call_c   Dyam_Unify_Item(&ref[216])
	fail_ret
	call_c   Dyam_Allocate(0)
	pl_call  fun15(&seed[73],1)
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(3))
	call_c   Dyam_Reg_Load(4,V(7))
	call_c   Dyam_Reg_Load(6,V(6))
	call_c   Dyam_Reg_Deallocate(4)
	pl_jump  pred_destructure_unify_aux_4()

;; TERM 217: '*GUARD*'(destructure_unify_args([_B|_C], [_D|_E], _F, _G)) :> []
c_code local build_ref_217
	ret_reg &ref[217]
	call_c   build_ref_216()
	call_c   Dyam_Create_Binary(I(9),&ref[216],I(0))
	move_ret ref[217]
	c_ret

c_code local build_seed_32
	ret_reg &seed[32]
	call_c   build_ref_33()
	call_c   build_ref_225()
	call_c   Dyam_Seed_Start(&ref[33],&ref[225],I(0),fun0,1)
	call_c   build_ref_224()
	call_c   Dyam_Seed_Add_Comp(&ref[224],fun69,0)
	call_c   Dyam_Seed_End()
	move_ret seed[32]
	c_ret

;; TERM 224: '*GUARD*'(body_to_lpda(_B, _C, _D, _E, _F))
c_code local build_ref_224
	ret_reg &ref[224]
	call_c   build_ref_34()
	call_c   build_ref_223()
	call_c   Dyam_Create_Unary(&ref[34],&ref[223])
	move_ret ref[224]
	c_ret

;; TERM 223: body_to_lpda(_B, _C, _D, _E, _F)
c_code local build_ref_223
	ret_reg &ref[223]
	call_c   build_ref_601()
	call_c   Dyam_Term_Start(&ref[601],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[223]
	c_ret

c_code local build_seed_76
	ret_reg &seed[76]
	call_c   build_ref_34()
	call_c   build_ref_233()
	call_c   Dyam_Seed_Start(&ref[34],&ref[233],I(0),fun1,1)
	call_c   build_ref_234()
	call_c   Dyam_Seed_Add_Comp(&ref[234],&ref[233],0)
	call_c   Dyam_Seed_End()
	move_ret seed[76]
	c_ret

;; TERM 234: '*GUARD*'(litteral_to_lpda(_B, _G, _D, _E, _F)) :> '$$HOLE$$'
c_code local build_ref_234
	ret_reg &ref[234]
	call_c   build_ref_233()
	call_c   Dyam_Create_Binary(I(9),&ref[233],I(7))
	move_ret ref[234]
	c_ret

;; TERM 233: '*GUARD*'(litteral_to_lpda(_B, _G, _D, _E, _F))
c_code local build_ref_233
	ret_reg &ref[233]
	call_c   build_ref_34()
	call_c   build_ref_232()
	call_c   Dyam_Create_Unary(&ref[34],&ref[232])
	move_ret ref[233]
	c_ret

;; TERM 232: litteral_to_lpda(_B, _G, _D, _E, _F)
c_code local build_ref_232
	ret_reg &ref[232]
	call_c   build_ref_636()
	call_c   Dyam_Term_Start(&ref[636],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[232]
	c_ret

;; TERM 636: litteral_to_lpda
c_code local build_ref_636
	ret_reg &ref[636]
	call_c   Dyam_Create_Atom("litteral_to_lpda")
	move_ret ref[636]
	c_ret

long local pool_fun66[2]=[1,build_seed_76]

pl_code local fun66
	call_c   Dyam_Remove_Choice()
	pl_call  fun15(&seed[76],1)
fun65:
	call_c   Dyam_Deallocate()
	pl_ret


c_code local build_seed_75
	ret_reg &seed[75]
	call_c   build_ref_34()
	call_c   build_ref_230()
	call_c   Dyam_Seed_Start(&ref[34],&ref[230],I(0),fun1,1)
	call_c   build_ref_231()
	call_c   Dyam_Seed_Add_Comp(&ref[231],&ref[230],0)
	call_c   Dyam_Seed_End()
	move_ret seed[75]
	c_ret

;; TERM 231: '*GUARD*'(body_to_lpda_handler(_B, _G, _D, _E, _F)) :> '$$HOLE$$'
c_code local build_ref_231
	ret_reg &ref[231]
	call_c   build_ref_230()
	call_c   Dyam_Create_Binary(I(9),&ref[230],I(7))
	move_ret ref[231]
	c_ret

;; TERM 230: '*GUARD*'(body_to_lpda_handler(_B, _G, _D, _E, _F))
c_code local build_ref_230
	ret_reg &ref[230]
	call_c   build_ref_34()
	call_c   build_ref_229()
	call_c   Dyam_Create_Unary(&ref[34],&ref[229])
	move_ret ref[230]
	c_ret

;; TERM 229: body_to_lpda_handler(_B, _G, _D, _E, _F)
c_code local build_ref_229
	ret_reg &ref[229]
	call_c   build_ref_586()
	call_c   Dyam_Term_Start(&ref[586],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[229]
	c_ret

long local pool_fun67[3]=[65537,build_seed_75,pool_fun66]

pl_code local fun68
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(6),V(2))
	fail_ret
fun67:
	call_c   Dyam_Choice(fun66)
	call_c   Dyam_Set_Cut()
	pl_call  fun15(&seed[75],1)
	call_c   Dyam_Cut()
	pl_jump  fun65()


c_code local build_seed_74
	ret_reg &seed[74]
	call_c   build_ref_34()
	call_c   build_ref_227()
	call_c   Dyam_Seed_Start(&ref[34],&ref[227],I(0),fun1,1)
	call_c   build_ref_228()
	call_c   Dyam_Seed_Add_Comp(&ref[228],&ref[227],0)
	call_c   Dyam_Seed_End()
	move_ret seed[74]
	c_ret

;; TERM 228: '*GUARD*'(toplevel(term_expand(_C, _G))) :> '$$HOLE$$'
c_code local build_ref_228
	ret_reg &ref[228]
	call_c   build_ref_227()
	call_c   Dyam_Create_Binary(I(9),&ref[227],I(7))
	move_ret ref[228]
	c_ret

;; TERM 227: '*GUARD*'(toplevel(term_expand(_C, _G)))
c_code local build_ref_227
	ret_reg &ref[227]
	call_c   build_ref_34()
	call_c   build_ref_226()
	call_c   Dyam_Create_Unary(&ref[34],&ref[226])
	move_ret ref[227]
	c_ret

;; TERM 226: toplevel(term_expand(_C, _G))
c_code local build_ref_226
	ret_reg &ref[226]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_638()
	call_c   Dyam_Create_Binary(&ref[638],V(2),V(6))
	move_ret R(0)
	call_c   build_ref_637()
	call_c   Dyam_Create_Unary(&ref[637],R(0))
	move_ret ref[226]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 637: toplevel
c_code local build_ref_637
	ret_reg &ref[637]
	call_c   Dyam_Create_Atom("toplevel")
	move_ret ref[637]
	c_ret

;; TERM 638: term_expand
c_code local build_ref_638
	ret_reg &ref[638]
	call_c   Dyam_Create_Atom("term_expand")
	move_ret ref[638]
	c_ret

long local pool_fun69[5]=[131074,build_ref_224,build_seed_74,pool_fun67,pool_fun67]

pl_code local fun69
	call_c   Dyam_Pool(pool_fun69)
	call_c   Dyam_Unify_Item(&ref[224])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun68)
	call_c   Dyam_Set_Cut()
	pl_call  fun15(&seed[74],1)
	call_c   Dyam_Reg_Load(0,V(6))
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(7), R(4)
	move     S(5), R(5)
	pl_call  pred_my_numbervars_3()
	call_c   Dyam_Cut()
	pl_jump  fun67()

;; TERM 225: '*GUARD*'(body_to_lpda(_B, _C, _D, _E, _F)) :> []
c_code local build_ref_225
	ret_reg &ref[225]
	call_c   build_ref_224()
	call_c   Dyam_Create_Binary(I(9),&ref[224],I(0))
	move_ret ref[225]
	c_ret

c_code local build_seed_31
	ret_reg &seed[31]
	call_c   build_ref_33()
	call_c   build_ref_243()
	call_c   Dyam_Seed_Start(&ref[33],&ref[243],I(0),fun0,1)
	call_c   build_ref_242()
	call_c   Dyam_Seed_Add_Comp(&ref[242],fun82,0)
	call_c   Dyam_Seed_End()
	move_ret seed[31]
	c_ret

;; TERM 242: '*GUARD*'(body_to_lpda_handler(_B, _O43906037, _D, _E, _F))
c_code local build_ref_242
	ret_reg &ref[242]
	call_c   build_ref_34()
	call_c   build_ref_241()
	call_c   Dyam_Create_Unary(&ref[34],&ref[241])
	move_ret ref[242]
	c_ret

;; TERM 241: body_to_lpda_handler(_B, _O43906037, _D, _E, _F)
c_code local build_ref_241
	ret_reg &ref[241]
	call_c   build_ref_586()
	call_c   build_ref_254()
	call_c   Dyam_Term_Start(&ref[586],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(&ref[254])
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[241]
	c_ret

;; TERM 254: _O43906037
c_code local build_ref_254
	ret_reg &ref[254]
	call_c   Dyam_Create_Unary(I(6),V(2))
	move_ret ref[254]
	c_ret

;; TERM 258: '$CLOSURE'('$fun'(81, 0, 1142861920), '$TUPPLE'(35085872005428))
c_code local build_ref_258
	ret_reg &ref[258]
	call_c   build_ref_257()
	call_c   Dyam_Closure_Aux(fun81,&ref[257])
	move_ret ref[258]
	c_ret

;; TERM 250: lpda_meta_call(_G, _H, _I)
c_code local build_ref_250
	ret_reg &ref[250]
	call_c   build_ref_575()
	call_c   Dyam_Term_Start(&ref[575],3)
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_End()
	move_ret ref[250]
	c_ret

c_code local build_seed_79
	ret_reg &seed[79]
	call_c   build_ref_34()
	call_c   build_ref_252()
	call_c   Dyam_Seed_Start(&ref[34],&ref[252],I(0),fun1,1)
	call_c   build_ref_253()
	call_c   Dyam_Seed_Add_Comp(&ref[253],&ref[252],0)
	call_c   Dyam_Seed_End()
	move_ret seed[79]
	c_ret

;; TERM 253: '*GUARD*'(litteral_to_lpda(_B, _I, _D, _E, _F)) :> '$$HOLE$$'
c_code local build_ref_253
	ret_reg &ref[253]
	call_c   build_ref_252()
	call_c   Dyam_Create_Binary(I(9),&ref[252],I(7))
	move_ret ref[253]
	c_ret

;; TERM 252: '*GUARD*'(litteral_to_lpda(_B, _I, _D, _E, _F))
c_code local build_ref_252
	ret_reg &ref[252]
	call_c   build_ref_34()
	call_c   build_ref_251()
	call_c   Dyam_Create_Unary(&ref[34],&ref[251])
	move_ret ref[252]
	c_ret

;; TERM 251: litteral_to_lpda(_B, _I, _D, _E, _F)
c_code local build_ref_251
	ret_reg &ref[251]
	call_c   build_ref_636()
	call_c   Dyam_Term_Start(&ref[636],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[251]
	c_ret

long local pool_fun79[3]=[2,build_ref_250,build_seed_79]

long local pool_fun80[3]=[65537,build_ref_254,pool_fun79]

pl_code local fun80
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(7),&ref[254])
	fail_ret
	call_c   Dyam_Unify(V(6),V(5))
	fail_ret
fun79:
	pl_call  Object_1(&ref[250])
	call_c   Dyam_Deallocate()
	pl_jump  fun15(&seed[79],1)


;; TERM 248: answer(_G)
c_code local build_ref_248
	ret_reg &ref[248]
	call_c   build_ref_602()
	call_c   Dyam_Create_Unary(&ref[602],V(6))
	move_ret ref[248]
	c_ret

;; TERM 249: '$answer'(_O43906037)
c_code local build_ref_249
	ret_reg &ref[249]
	call_c   build_ref_639()
	call_c   build_ref_254()
	call_c   Dyam_Create_Unary(&ref[639],&ref[254])
	move_ret ref[249]
	c_ret

;; TERM 639: '$answer'
c_code local build_ref_639
	ret_reg &ref[639]
	call_c   Dyam_Create_Atom("$answer")
	move_ret ref[639]
	c_ret

long local pool_fun81[5]=[131074,build_ref_248,build_ref_249,pool_fun80,pool_fun79]

pl_code local fun81
	call_c   Dyam_Pool(pool_fun81)
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun80)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(5),&ref[248])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(7),&ref[249])
	fail_ret
	pl_jump  fun79()

;; TERM 257: '$TUPPLE'(35085872005428)
c_code local build_ref_257
	ret_reg &ref[257]
	call_c   Dyam_Create_Simple_Tupple(0,260046848)
	move_ret ref[257]
	c_ret

long local pool_fun82[3]=[2,build_ref_242,build_ref_258]

pl_code local fun82
	call_c   Dyam_Pool(pool_fun82)
	call_c   Dyam_Unify_Item(&ref[242])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[258], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Deallocate(2)
fun78:
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(2)
	call_c   Dyam_Choice(fun77)
	pl_call  fun74(&seed[77],1)
	pl_fail


;; TERM 243: '*GUARD*'(body_to_lpda_handler(_B, _O43906037, _D, _E, _F)) :> []
c_code local build_ref_243
	ret_reg &ref[243]
	call_c   build_ref_242()
	call_c   Dyam_Create_Binary(I(9),&ref[242],I(0))
	move_ret ref[243]
	c_ret

c_code local build_seed_14
	ret_reg &seed[14]
	call_c   build_ref_25()
	call_c   build_ref_261()
	call_c   Dyam_Seed_Start(&ref[25],&ref[261],I(0),fun6,1)
	call_c   build_ref_262()
	call_c   Dyam_Seed_Add_Comp(&ref[262],fun85,0)
	call_c   Dyam_Seed_End()
	move_ret seed[14]
	c_ret

;; TERM 262: '*CITEM*'('call_decompose_args/3'((_B / _C)), _A)
c_code local build_ref_262
	ret_reg &ref[262]
	call_c   build_ref_29()
	call_c   build_ref_259()
	call_c   Dyam_Create_Binary(&ref[29],&ref[259],V(0))
	move_ret ref[262]
	c_ret

;; TERM 259: 'call_decompose_args/3'((_B / _C))
c_code local build_ref_259
	ret_reg &ref[259]
	call_c   build_ref_640()
	call_c   build_ref_354()
	call_c   Dyam_Create_Unary(&ref[640],&ref[354])
	move_ret ref[259]
	c_ret

;; TERM 640: 'call_decompose_args/3'
c_code local build_ref_640
	ret_reg &ref[640]
	call_c   Dyam_Create_Atom("call_decompose_args/3")
	move_ret ref[640]
	c_ret

;; TERM 267: [_G|_H]
c_code local build_ref_267
	ret_reg &ref[267]
	call_c   Dyam_Create_List(V(6),V(7))
	move_ret ref[267]
	c_ret

;; TERM 268: [_E|_H]
c_code local build_ref_268
	ret_reg &ref[268]
	call_c   Dyam_Create_List(V(4),V(7))
	move_ret ref[268]
	c_ret

c_code local build_seed_80
	ret_reg &seed[80]
	call_c   build_ref_0()
	call_c   build_ref_265()
	call_c   Dyam_Seed_Start(&ref[0],&ref[265],&ref[265],fun1,1)
	call_c   build_ref_266()
	call_c   Dyam_Seed_Add_Comp(&ref[266],&ref[265],0)
	call_c   Dyam_Seed_End()
	move_ret seed[80]
	c_ret

;; TERM 266: '*RITEM*'(_A, return([_D,_E], _F)) :> '$$HOLE$$'
c_code local build_ref_266
	ret_reg &ref[266]
	call_c   build_ref_265()
	call_c   Dyam_Create_Binary(I(9),&ref[265],I(7))
	move_ret ref[266]
	c_ret

;; TERM 265: '*RITEM*'(_A, return([_D,_E], _F))
c_code local build_ref_265
	ret_reg &ref[265]
	call_c   build_ref_0()
	call_c   build_ref_264()
	call_c   Dyam_Create_Binary(&ref[0],V(0),&ref[264])
	move_ret ref[265]
	c_ret

;; TERM 264: return([_D,_E], _F)
c_code local build_ref_264
	ret_reg &ref[264]
	call_c   build_ref_641()
	call_c   build_ref_263()
	call_c   Dyam_Create_Binary(&ref[641],&ref[263],V(5))
	move_ret ref[264]
	c_ret

;; TERM 263: [_D,_E]
c_code local build_ref_263
	ret_reg &ref[263]
	call_c   Dyam_Create_Tupple(3,4,I(0))
	move_ret ref[263]
	c_ret

;; TERM 641: return
c_code local build_ref_641
	ret_reg &ref[641]
	call_c   Dyam_Create_Atom("return")
	move_ret ref[641]
	c_ret

long local pool_fun83[2]=[1,build_seed_80]

long local pool_fun84[4]=[65538,build_ref_267,build_ref_268,pool_fun83]

pl_code local fun84
	call_c   Dyam_Remove_Choice()
	call_c   DYAM_evpred_univ(V(3),&ref[267])
	fail_ret
	call_c   Dyam_Unify(V(5),&ref[268])
	fail_ret
fun83:
	call_c   Dyam_Deallocate()
	pl_jump  fun8(&seed[80],2)


long local pool_fun85[5]=[131074,build_ref_262,build_ref_263,pool_fun84,pool_fun83]

pl_code local fun85
	call_c   Dyam_Pool(pool_fun85)
	call_c   Dyam_Unify_Item(&ref[262])
	fail_ret
	call_c   DYAM_evpred_functor(V(3),V(1),V(2))
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun84)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_gt(V(2),N(5))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(5),&ref[263])
	fail_ret
	pl_jump  fun83()

;; TERM 261: '*FIRST*'('call_decompose_args/3'((_B / _C))) :> []
c_code local build_ref_261
	ret_reg &ref[261]
	call_c   build_ref_260()
	call_c   Dyam_Create_Binary(I(9),&ref[260],I(0))
	move_ret ref[261]
	c_ret

;; TERM 260: '*FIRST*'('call_decompose_args/3'((_B / _C)))
c_code local build_ref_260
	ret_reg &ref[260]
	call_c   build_ref_25()
	call_c   build_ref_259()
	call_c   Dyam_Create_Unary(&ref[25],&ref[259])
	move_ret ref[260]
	c_ret

c_code local build_seed_15
	ret_reg &seed[15]
	call_c   build_ref_25()
	call_c   build_ref_271()
	call_c   Dyam_Seed_Start(&ref[25],&ref[271],I(0),fun6,1)
	call_c   build_ref_272()
	call_c   Dyam_Seed_Add_Comp(&ref[272],fun86,0)
	call_c   Dyam_Seed_End()
	move_ret seed[15]
	c_ret

;; TERM 272: '*CITEM*'('call_decompose_args/3'((prolog _B / _C)), _A)
c_code local build_ref_272
	ret_reg &ref[272]
	call_c   build_ref_29()
	call_c   build_ref_269()
	call_c   Dyam_Create_Binary(&ref[29],&ref[269],V(0))
	move_ret ref[272]
	c_ret

;; TERM 269: 'call_decompose_args/3'((prolog _B / _C))
c_code local build_ref_269
	ret_reg &ref[269]
	call_c   build_ref_640()
	call_c   build_ref_318()
	call_c   Dyam_Create_Unary(&ref[640],&ref[318])
	move_ret ref[269]
	c_ret

;; TERM 318: prolog _B / _C
c_code local build_ref_318
	ret_reg &ref[318]
	call_c   build_ref_579()
	call_c   build_ref_354()
	call_c   Dyam_Create_Unary(&ref[579],&ref[354])
	move_ret ref[318]
	c_ret

long local pool_fun86[5]=[131074,build_ref_272,build_ref_263,pool_fun84,pool_fun83]

pl_code local fun86
	call_c   Dyam_Pool(pool_fun86)
	call_c   Dyam_Unify_Item(&ref[272])
	fail_ret
	call_c   DYAM_evpred_functor(V(3),V(1),V(2))
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun84)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_gt(V(2),N(5))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(5),&ref[263])
	fail_ret
	pl_jump  fun83()

;; TERM 271: '*FIRST*'('call_decompose_args/3'((prolog _B / _C))) :> []
c_code local build_ref_271
	ret_reg &ref[271]
	call_c   build_ref_270()
	call_c   Dyam_Create_Binary(I(9),&ref[270],I(0))
	move_ret ref[271]
	c_ret

;; TERM 270: '*FIRST*'('call_decompose_args/3'((prolog _B / _C)))
c_code local build_ref_270
	ret_reg &ref[270]
	call_c   build_ref_25()
	call_c   build_ref_269()
	call_c   Dyam_Create_Unary(&ref[25],&ref[269])
	move_ret ref[270]
	c_ret

c_code local build_seed_8
	ret_reg &seed[8]
	call_c   build_ref_25()
	call_c   build_ref_275()
	call_c   Dyam_Seed_Start(&ref[25],&ref[275],I(0),fun6,1)
	call_c   build_ref_276()
	call_c   Dyam_Seed_Add_Comp(&ref[276],fun104,0)
	call_c   Dyam_Seed_End()
	move_ret seed[8]
	c_ret

;; TERM 276: '*CITEM*'('call_term_module_shift/5'(_B, (_C / _D)), _A)
c_code local build_ref_276
	ret_reg &ref[276]
	call_c   build_ref_29()
	call_c   build_ref_273()
	call_c   Dyam_Create_Binary(&ref[29],&ref[273],V(0))
	move_ret ref[276]
	c_ret

;; TERM 273: 'call_term_module_shift/5'(_B, (_C / _D))
c_code local build_ref_273
	ret_reg &ref[273]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_627()
	call_c   Dyam_Create_Binary(&ref[627],V(2),V(3))
	move_ret R(0)
	call_c   build_ref_642()
	call_c   Dyam_Create_Binary(&ref[642],V(1),R(0))
	move_ret ref[273]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 642: 'call_term_module_shift/5'
c_code local build_ref_642
	ret_reg &ref[642]
	call_c   Dyam_Create_Atom("call_term_module_shift/5")
	move_ret ref[642]
	c_ret

;; TERM 278: [_C|_O]
c_code local build_ref_278
	ret_reg &ref[278]
	call_c   Dyam_Create_List(V(2),V(14))
	move_ret ref[278]
	c_ret

;; TERM 279: [_E|_O]
c_code local build_ref_279
	ret_reg &ref[279]
	call_c   Dyam_Create_List(V(4),V(14))
	move_ret ref[279]
	c_ret

c_code local build_seed_81
	ret_reg &seed[81]
	call_c   build_ref_0()
	call_c   build_ref_281()
	call_c   Dyam_Seed_Start(&ref[0],&ref[281],&ref[281],fun1,1)
	call_c   build_ref_282()
	call_c   Dyam_Seed_Add_Comp(&ref[282],&ref[281],0)
	call_c   Dyam_Seed_End()
	move_ret seed[81]
	c_ret

;; TERM 282: '*RITEM*'(_A, return((_E / _D), _F, _G)) :> '$$HOLE$$'
c_code local build_ref_282
	ret_reg &ref[282]
	call_c   build_ref_281()
	call_c   Dyam_Create_Binary(I(9),&ref[281],I(7))
	move_ret ref[282]
	c_ret

;; TERM 281: '*RITEM*'(_A, return((_E / _D), _F, _G))
c_code local build_ref_281
	ret_reg &ref[281]
	call_c   build_ref_0()
	call_c   build_ref_280()
	call_c   Dyam_Create_Binary(&ref[0],V(0),&ref[280])
	move_ret ref[281]
	c_ret

;; TERM 280: return((_E / _D), _F, _G)
c_code local build_ref_280
	ret_reg &ref[280]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_627()
	call_c   Dyam_Create_Binary(&ref[627],V(4),V(3))
	move_ret R(0)
	call_c   build_ref_641()
	call_c   Dyam_Term_Start(&ref[641],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[280]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun87[4]=[3,build_ref_278,build_ref_279,build_seed_81]

pl_code local fun91
	call_c   Dyam_Remove_Choice()
fun90:
	call_c   Dyam_Unify(V(7),V(1))
	fail_ret
	call_c   Dyam_Unify(V(8),V(2))
	fail_ret
fun87:
	call_c   Dyam_Reg_Load(0,V(8))
	call_c   Dyam_Reg_Load(2,V(7))
	move     V(4), R(4)
	move     S(5), R(5)
	pl_call  pred_deep_module_shift_3()
	call_c   DYAM_evpred_length(V(14),V(3))
	fail_ret
	call_c   DYAM_evpred_univ(V(5),&ref[278])
	fail_ret
	call_c   DYAM_evpred_univ(V(6),&ref[279])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_jump  fun8(&seed[81],2)



pl_code local fun99
	call_c   Dyam_Remove_Choice()
fun98:
	call_c   Dyam_Choice(fun91)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(1),V(9))
	fail_ret
	call_c   Dyam_Cut()
	pl_fail


;; TERM 287: module_import((_K / _D), _J)
c_code local build_ref_287
	ret_reg &ref[287]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_627()
	call_c   Dyam_Create_Binary(&ref[627],V(10),V(3))
	move_ret R(0)
	call_c   build_ref_643()
	call_c   Dyam_Create_Binary(&ref[643],R(0),V(9))
	move_ret ref[287]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 643: module_import
c_code local build_ref_643
	ret_reg &ref[643]
	call_c   Dyam_Create_Atom("module_import")
	move_ret ref[643]
	c_ret

long local pool_fun100[3]=[65537,build_ref_287,pool_fun87]

pl_code local fun100
	call_c   Dyam_Update_Choice(fun99)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[287])
	call_c   Dyam_Cut()
	pl_fail

;; TERM 286: compiler_extension((_C / _D), _N)
c_code local build_ref_286
	ret_reg &ref[286]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_627()
	call_c   Dyam_Create_Binary(&ref[627],V(2),V(3))
	move_ret R(0)
	call_c   build_ref_644()
	call_c   Dyam_Create_Binary(&ref[644],R(0),V(13))
	move_ret ref[286]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 644: compiler_extension
c_code local build_ref_644
	ret_reg &ref[644]
	call_c   Dyam_Create_Atom("compiler_extension")
	move_ret ref[644]
	c_ret

long local pool_fun92[3]=[65537,build_ref_286,pool_fun87]

pl_code local fun93
	call_c   Dyam_Remove_Choice()
fun92:
	call_c   Dyam_Choice(fun91)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[286])
	call_c   Dyam_Cut()
	pl_fail


;; TERM 285: predicate((_C / _D), builtin)
c_code local build_ref_285
	ret_reg &ref[285]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_627()
	call_c   Dyam_Create_Binary(&ref[627],V(2),V(3))
	move_ret R(0)
	call_c   build_ref_645()
	call_c   build_ref_646()
	call_c   Dyam_Create_Binary(&ref[645],R(0),&ref[646])
	move_ret ref[285]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 646: builtin
c_code local build_ref_646
	ret_reg &ref[646]
	call_c   Dyam_Create_Atom("builtin")
	move_ret ref[646]
	c_ret

;; TERM 645: predicate
c_code local build_ref_645
	ret_reg &ref[645]
	call_c   Dyam_Create_Atom("predicate")
	move_ret ref[645]
	c_ret

long local pool_fun94[3]=[65537,build_ref_285,pool_fun92]

pl_code local fun95
	call_c   Dyam_Remove_Choice()
fun94:
	call_c   Dyam_Choice(fun93)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[285])
	call_c   Dyam_Cut()
	pl_fail


;; TERM 284: builtin((_C / _D), _M)
c_code local build_ref_284
	ret_reg &ref[284]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_627()
	call_c   Dyam_Create_Binary(&ref[627],V(2),V(3))
	move_ret R(0)
	call_c   build_ref_646()
	call_c   Dyam_Create_Binary(&ref[646],R(0),V(12))
	move_ret ref[284]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun96[3]=[65537,build_ref_284,pool_fun94]

pl_code local fun97
	call_c   Dyam_Remove_Choice()
fun96:
	call_c   Dyam_Choice(fun95)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[284])
	call_c   Dyam_Cut()
	pl_fail


;; TERM 283: foreign((_C / _D), _L)
c_code local build_ref_283
	ret_reg &ref[283]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_627()
	call_c   Dyam_Create_Binary(&ref[627],V(2),V(3))
	move_ret R(0)
	call_c   build_ref_630()
	call_c   Dyam_Create_Binary(&ref[630],R(0),V(11))
	move_ret ref[283]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun101[4]=[131073,build_ref_283,pool_fun100,pool_fun96]

pl_code local fun102
	call_c   Dyam_Remove_Choice()
fun101:
	call_c   DYAM_evpred_atom_to_module(V(2),V(9),V(10))
	fail_ret
	call_c   Dyam_Choice(fun100)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(9),I(0))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun97)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[283])
	call_c   Dyam_Cut()
	pl_fail


pl_code local fun103
	call_c   Dyam_Update_Choice(fun102)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(1),I(0))
	fail_ret
	call_c   Dyam_Cut()
	pl_fail

;; TERM 277: module_import((_C / _D), _H)
c_code local build_ref_277
	ret_reg &ref[277]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_627()
	call_c   Dyam_Create_Binary(&ref[627],V(2),V(3))
	move_ret R(0)
	call_c   build_ref_643()
	call_c   Dyam_Create_Binary(&ref[643],R(0),V(7))
	move_ret ref[277]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

pl_code local fun89
	call_c   Dyam_Remove_Choice()
fun88:
	call_c   Dyam_Unify(V(8),V(2))
	fail_ret
	pl_jump  fun87()


long local pool_fun104[5]=[131074,build_ref_276,build_ref_277,pool_fun101,pool_fun87]

pl_code local fun104
	call_c   Dyam_Pool(pool_fun104)
	call_c   Dyam_Unify_Item(&ref[276])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun103)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[277])
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun89)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(7),I(0))
	fail_ret
	call_c   Dyam_Cut()
	pl_fail

;; TERM 275: '*FIRST*'('call_term_module_shift/5'(_B, (_C / _D))) :> []
c_code local build_ref_275
	ret_reg &ref[275]
	call_c   build_ref_274()
	call_c   Dyam_Create_Binary(I(9),&ref[274],I(0))
	move_ret ref[275]
	c_ret

;; TERM 274: '*FIRST*'('call_term_module_shift/5'(_B, (_C / _D)))
c_code local build_ref_274
	ret_reg &ref[274]
	call_c   build_ref_25()
	call_c   build_ref_273()
	call_c   Dyam_Create_Unary(&ref[25],&ref[273])
	move_ret ref[274]
	c_ret

c_code local build_seed_24
	ret_reg &seed[24]
	call_c   build_ref_33()
	call_c   build_ref_290()
	call_c   Dyam_Seed_Start(&ref[33],&ref[290],I(0),fun0,1)
	call_c   build_ref_289()
	call_c   Dyam_Seed_Add_Comp(&ref[289],fun109,0)
	call_c   Dyam_Seed_End()
	move_ret seed[24]
	c_ret

;; TERM 289: '*GUARD*'(body_to_lpda_handler(_B, phrase(_C, _D, _E), _F, _G, _H))
c_code local build_ref_289
	ret_reg &ref[289]
	call_c   build_ref_34()
	call_c   build_ref_288()
	call_c   Dyam_Create_Unary(&ref[34],&ref[288])
	move_ret ref[289]
	c_ret

;; TERM 288: body_to_lpda_handler(_B, phrase(_C, _D, _E), _F, _G, _H)
c_code local build_ref_288
	ret_reg &ref[288]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_647()
	call_c   Dyam_Term_Start(&ref[647],3)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_586()
	call_c   Dyam_Term_Start(&ref[586],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[288]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 647: phrase
c_code local build_ref_647
	ret_reg &ref[647]
	call_c   Dyam_Create_Atom("phrase")
	move_ret ref[647]
	c_ret

;; TERM 300: '$CLOSURE'('$fun'(108, 0, 1143054248), '$TUPPLE'(35085875413156))
c_code local build_ref_300
	ret_reg &ref[300]
	call_c   build_ref_299()
	call_c   Dyam_Closure_Aux(fun108,&ref[299])
	move_ret ref[300]
	c_ret

pl_code local fun108
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(2))
	call_c   Dyam_Reg_Load(4,V(3))
	call_c   Dyam_Reg_Load(6,V(4))
	call_c   Dyam_Reg_Load(8,V(8))
	call_c   Dyam_Reg_Load(10,V(8))
	call_c   Dyam_Reg_Load(12,V(5))
	call_c   Dyam_Reg_Load(14,V(6))
	call_c   Dyam_Reg_Load(16,V(7))
	call_c   Dyam_Reg_Deallocate(9)
	pl_jump  pred_dcg_body_to_lpda_9()

;; TERM 299: '$TUPPLE'(35085875413156)
c_code local build_ref_299
	ret_reg &ref[299]
	call_c   Dyam_Create_Simple_Tupple(0,267386880)
	move_ret ref[299]
	c_ret

long local pool_fun109[3]=[2,build_ref_289,build_ref_300]

pl_code local fun109
	call_c   Dyam_Pool(pool_fun109)
	call_c   Dyam_Unify_Item(&ref[289])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[300], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	move     I(0), R(4)
	move     0, R(5)
	move     V(8), R(6)
	move     S(5), R(7)
	call_c   Dyam_Reg_Deallocate(4)
fun107:
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Reg_Bind(2,V(4))
	call_c   Dyam_Multi_Reg_Bind(4,2,2)
	call_c   Dyam_Choice(fun106)
	pl_call  fun74(&seed[82],1)
	pl_fail


;; TERM 290: '*GUARD*'(body_to_lpda_handler(_B, phrase(_C, _D, _E), _F, _G, _H)) :> []
c_code local build_ref_290
	ret_reg &ref[290]
	call_c   build_ref_289()
	call_c   Dyam_Create_Binary(I(9),&ref[289],I(0))
	move_ret ref[290]
	c_ret

c_code local build_seed_11
	ret_reg &seed[11]
	call_c   build_ref_25()
	call_c   build_ref_303()
	call_c   Dyam_Seed_Start(&ref[25],&ref[303],I(0),fun6,1)
	call_c   build_ref_304()
	call_c   Dyam_Seed_Add_Comp(&ref[304],fun117,0)
	call_c   Dyam_Seed_End()
	move_ret seed[11]
	c_ret

;; TERM 304: '*CITEM*'(wrapping_predicate((prolog _B / _C)), _A)
c_code local build_ref_304
	ret_reg &ref[304]
	call_c   build_ref_29()
	call_c   build_ref_301()
	call_c   Dyam_Create_Binary(&ref[29],&ref[301],V(0))
	move_ret ref[304]
	c_ret

;; TERM 301: wrapping_predicate((prolog _B / _C))
c_code local build_ref_301
	ret_reg &ref[301]
	call_c   build_ref_648()
	call_c   build_ref_318()
	call_c   Dyam_Create_Unary(&ref[648],&ref[318])
	move_ret ref[301]
	c_ret

;; TERM 648: wrapping_predicate
c_code local build_ref_648
	ret_reg &ref[648]
	call_c   Dyam_Create_Atom("wrapping_predicate")
	move_ret ref[648]
	c_ret

;; TERM 317: '$CLOSURE'('$fun'(116, 0, 1143111652), '$TUPPLE'(35085872005944))
c_code local build_ref_317
	ret_reg &ref[317]
	call_c   build_ref_316()
	call_c   Dyam_Closure_Aux(fun116,&ref[316])
	move_ret ref[317]
	c_ret

;; TERM 313: [_G,_D,_E]
c_code local build_ref_313
	ret_reg &ref[313]
	call_c   build_ref_263()
	call_c   Dyam_Create_List(V(6),&ref[263])
	move_ret ref[313]
	c_ret

;; TERM 314: '*WRAPPER*'((prolog _B / _C), [_G|_F], '*PROLOG-LAST*'(_D, _E))
c_code local build_ref_314
	ret_reg &ref[314]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   Dyam_Create_List(V(6),V(5))
	move_ret R(0)
	call_c   build_ref_426()
	call_c   Dyam_Create_Binary(&ref[426],V(3),V(4))
	move_ret R(1)
	call_c   build_ref_649()
	call_c   build_ref_318()
	call_c   Dyam_Term_Start(&ref[649],3)
	call_c   Dyam_Term_Arg(&ref[318])
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_End()
	move_ret ref[314]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 649: '*WRAPPER*'
c_code local build_ref_649
	ret_reg &ref[649]
	call_c   Dyam_Create_Atom("*WRAPPER*")
	move_ret ref[649]
	c_ret

long local pool_fun116[4]=[3,build_ref_313,build_ref_314,build_seed_50]

pl_code local fun116
	call_c   Dyam_Pool(pool_fun116)
	call_c   Dyam_Allocate(0)
	move     &ref[313], R(0)
	move     S(5), R(1)
	move     V(7), R(2)
	move     S(5), R(3)
	move     V(8), R(4)
	move     S(5), R(5)
	pl_call  pred_my_numbervars_3()
	call_c   DYAM_evpred_assert_1(&ref[314])
	call_c   Dyam_Deallocate()
	pl_jump  fun8(&seed[50],2)

;; TERM 316: '$TUPPLE'(35085872005944)
c_code local build_ref_316
	ret_reg &ref[316]
	call_c   Dyam_Create_Simple_Tupple(0,528482304)
	move_ret ref[316]
	c_ret

long local pool_fun117[5]=[4,build_ref_304,build_ref_317,build_ref_318,build_ref_263]

pl_code local fun117
	call_c   Dyam_Pool(pool_fun117)
	call_c   Dyam_Unify_Item(&ref[304])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[317], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	move     &ref[318], R(4)
	move     S(5), R(5)
	move     &ref[263], R(6)
	move     S(5), R(7)
	move     V(5), R(8)
	move     S(5), R(9)
	call_c   Dyam_Reg_Deallocate(5)
fun115:
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Reg_Bind(2,V(5))
	call_c   Dyam_Multi_Reg_Bind(4,2,3)
	call_c   Dyam_Choice(fun114)
	pl_call  fun74(&seed[84],1)
	pl_fail


;; TERM 303: '*FIRST*'(wrapping_predicate((prolog _B / _C))) :> []
c_code local build_ref_303
	ret_reg &ref[303]
	call_c   build_ref_302()
	call_c   Dyam_Create_Binary(I(9),&ref[302],I(0))
	move_ret ref[303]
	c_ret

;; TERM 302: '*FIRST*'(wrapping_predicate((prolog _B / _C)))
c_code local build_ref_302
	ret_reg &ref[302]
	call_c   build_ref_25()
	call_c   build_ref_301()
	call_c   Dyam_Create_Unary(&ref[25],&ref[301])
	move_ret ref[302]
	c_ret

c_code local build_seed_41
	ret_reg &seed[41]
	call_c   build_ref_33()
	call_c   build_ref_321()
	call_c   Dyam_Seed_Start(&ref[33],&ref[321],I(0),fun0,1)
	call_c   build_ref_320()
	call_c   Dyam_Seed_Add_Comp(&ref[320],fun118,0)
	call_c   Dyam_Seed_End()
	move_ret seed[41]
	c_ret

;; TERM 320: '*GUARD*'(build_cond_loader(_B, _C, _D, '$loader'(_E)))
c_code local build_ref_320
	ret_reg &ref[320]
	call_c   build_ref_34()
	call_c   build_ref_319()
	call_c   Dyam_Create_Unary(&ref[34],&ref[319])
	move_ret ref[320]
	c_ret

;; TERM 319: build_cond_loader(_B, _C, _D, '$loader'(_E))
c_code local build_ref_319
	ret_reg &ref[319]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_596()
	call_c   Dyam_Create_Unary(&ref[596],V(4))
	move_ret R(0)
	call_c   build_ref_597()
	call_c   Dyam_Term_Start(&ref[597],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_End()
	move_ret ref[319]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_86
	ret_reg &seed[86]
	call_c   build_ref_34()
	call_c   build_ref_324()
	call_c   Dyam_Seed_Start(&ref[34],&ref[324],I(0),fun1,1)
	call_c   build_ref_325()
	call_c   Dyam_Seed_Add_Comp(&ref[325],&ref[324],0)
	call_c   Dyam_Seed_End()
	move_ret seed[86]
	c_ret

;; TERM 325: '*GUARD*'(body_to_lpda(_B, _C, (_J :> fail), _H, rec_prolog)) :> '$$HOLE$$'
c_code local build_ref_325
	ret_reg &ref[325]
	call_c   build_ref_324()
	call_c   Dyam_Create_Binary(I(9),&ref[324],I(7))
	move_ret ref[325]
	c_ret

;; TERM 324: '*GUARD*'(body_to_lpda(_B, _C, (_J :> fail), _H, rec_prolog))
c_code local build_ref_324
	ret_reg &ref[324]
	call_c   build_ref_34()
	call_c   build_ref_323()
	call_c   Dyam_Create_Unary(&ref[34],&ref[323])
	move_ret ref[324]
	c_ret

;; TERM 323: body_to_lpda(_B, _C, (_J :> fail), _H, rec_prolog)
c_code local build_ref_323
	ret_reg &ref[323]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_213()
	call_c   Dyam_Create_Binary(I(9),V(9),&ref[213])
	move_ret R(0)
	call_c   build_ref_601()
	call_c   build_ref_544()
	call_c   Dyam_Term_Start(&ref[601],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(&ref[544])
	call_c   Dyam_Term_End()
	move_ret ref[323]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_87
	ret_reg &seed[87]
	call_c   build_ref_34()
	call_c   build_ref_327()
	call_c   Dyam_Seed_Start(&ref[34],&ref[327],I(0),fun1,1)
	call_c   build_ref_328()
	call_c   Dyam_Seed_Add_Comp(&ref[328],&ref[327],0)
	call_c   Dyam_Seed_End()
	move_ret seed[87]
	c_ret

;; TERM 328: '*GUARD*'(body_to_lpda(_B, true, _G, _I, rec_prolog)) :> '$$HOLE$$'
c_code local build_ref_328
	ret_reg &ref[328]
	call_c   build_ref_327()
	call_c   Dyam_Create_Binary(I(9),&ref[327],I(7))
	move_ret ref[328]
	c_ret

;; TERM 327: '*GUARD*'(body_to_lpda(_B, true, _G, _I, rec_prolog))
c_code local build_ref_327
	ret_reg &ref[327]
	call_c   build_ref_34()
	call_c   build_ref_326()
	call_c   Dyam_Create_Unary(&ref[34],&ref[326])
	move_ret ref[327]
	c_ret

;; TERM 326: body_to_lpda(_B, true, _G, _I, rec_prolog)
c_code local build_ref_326
	ret_reg &ref[326]
	call_c   build_ref_601()
	call_c   build_ref_84()
	call_c   build_ref_544()
	call_c   Dyam_Term_Start(&ref[601],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(&ref[84])
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(&ref[544])
	call_c   Dyam_Term_End()
	move_ret ref[326]
	c_ret

long local pool_fun118[5]=[4,build_ref_320,build_ref_322,build_seed_86,build_seed_87]

pl_code local fun118
	call_c   Dyam_Pool(pool_fun118)
	call_c   Dyam_Unify_Item(&ref[320])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(2))
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(5), R(4)
	move     S(5), R(5)
	pl_call  pred_my_numbervars_3()
	move     &ref[322], R(0)
	move     0, R(1)
	call_c   Dyam_Reg_Load(2,V(4))
	move     V(6), R(4)
	move     S(5), R(5)
	move     V(7), R(6)
	move     S(5), R(7)
	move     V(8), R(8)
	move     S(5), R(9)
	pl_call  pred_handle_choice_5()
	call_c   Dyam_Reg_Load(0,V(3))
	move     V(9), R(2)
	move     S(5), R(3)
	pl_call  pred_trans_unfold_2()
	pl_call  fun15(&seed[86],1)
	call_c   Dyam_Deallocate()
	pl_jump  fun15(&seed[87],1)

;; TERM 321: '*GUARD*'(build_cond_loader(_B, _C, _D, '$loader'(_E))) :> []
c_code local build_ref_321
	ret_reg &ref[321]
	call_c   build_ref_320()
	call_c   Dyam_Create_Binary(I(9),&ref[320],I(0))
	move_ret ref[321]
	c_ret

c_code local build_seed_12
	ret_reg &seed[12]
	call_c   build_ref_25()
	call_c   build_ref_342()
	call_c   Dyam_Seed_Start(&ref[25],&ref[342],I(0),fun6,1)
	call_c   build_ref_343()
	call_c   Dyam_Seed_Add_Comp(&ref[343],fun126,0)
	call_c   Dyam_Seed_End()
	move_ret seed[12]
	c_ret

;; TERM 343: '*CITEM*'(wrapping_predicate((lc _B / _C)), _A)
c_code local build_ref_343
	ret_reg &ref[343]
	call_c   build_ref_29()
	call_c   build_ref_340()
	call_c   Dyam_Create_Binary(&ref[29],&ref[340],V(0))
	move_ret ref[343]
	c_ret

;; TERM 340: wrapping_predicate((lc _B / _C))
c_code local build_ref_340
	ret_reg &ref[340]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_650()
	call_c   build_ref_354()
	call_c   Dyam_Create_Unary(&ref[650],&ref[354])
	move_ret R(0)
	call_c   build_ref_648()
	call_c   Dyam_Create_Unary(&ref[648],R(0))
	move_ret ref[340]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 650: lc
c_code local build_ref_650
	ret_reg &ref[650]
	call_c   Dyam_Create_Atom("lc")
	move_ret ref[650]
	c_ret

;; TERM 353: '$CLOSURE'('$fun'(125, 0, 1143212704), '$TUPPLE'(35085872005944))
c_code local build_ref_353
	ret_reg &ref[353]
	call_c   build_ref_316()
	call_c   Dyam_Closure_Aux(fun125,&ref[316])
	move_ret ref[353]
	c_ret

c_code local build_seed_88
	ret_reg &seed[88]
	call_c   build_ref_34()
	call_c   build_ref_345()
	call_c   Dyam_Seed_Start(&ref[34],&ref[345],I(0),fun1,1)
	call_c   build_ref_346()
	call_c   Dyam_Seed_Add_Comp(&ref[346],&ref[345],0)
	call_c   Dyam_Seed_End()
	move_ret seed[88]
	c_ret

;; TERM 346: '*GUARD*'(make_callret(_D, _G, _H)) :> '$$HOLE$$'
c_code local build_ref_346
	ret_reg &ref[346]
	call_c   build_ref_345()
	call_c   Dyam_Create_Binary(I(9),&ref[345],I(7))
	move_ret ref[346]
	c_ret

;; TERM 345: '*GUARD*'(make_callret(_D, _G, _H))
c_code local build_ref_345
	ret_reg &ref[345]
	call_c   build_ref_34()
	call_c   build_ref_344()
	call_c   Dyam_Create_Unary(&ref[34],&ref[344])
	move_ret ref[345]
	c_ret

;; TERM 344: make_callret(_D, _G, _H)
c_code local build_ref_344
	ret_reg &ref[344]
	call_c   build_ref_651()
	call_c   Dyam_Term_Start(&ref[651],3)
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[344]
	c_ret

;; TERM 651: make_callret
c_code local build_ref_651
	ret_reg &ref[651]
	call_c   Dyam_Create_Atom("make_callret")
	move_ret ref[651]
	c_ret

;; TERM 347: [_I,_D,_E,_J]
c_code local build_ref_347
	ret_reg &ref[347]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_505()
	call_c   Dyam_Create_Tupple(3,4,&ref[505])
	move_ret R(0)
	call_c   Dyam_Create_List(V(8),R(0))
	move_ret ref[347]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 505: [_J]
c_code local build_ref_505
	ret_reg &ref[505]
	call_c   Dyam_Create_List(V(9),I(0))
	move_ret ref[505]
	c_ret

c_code local build_seed_89
	ret_reg &seed[89]
	call_c   build_ref_34()
	call_c   build_ref_349()
	call_c   Dyam_Seed_Start(&ref[34],&ref[349],I(0),fun1,1)
	call_c   build_ref_350()
	call_c   Dyam_Seed_Add_Comp(&ref[350],&ref[349],0)
	call_c   Dyam_Seed_End()
	move_ret seed[89]
	c_ret

;; TERM 350: '*GUARD*'(body_to_lpda(_M, internal_lc(_G, _J), noop, _N, rec_prolog)) :> '$$HOLE$$'
c_code local build_ref_350
	ret_reg &ref[350]
	call_c   build_ref_349()
	call_c   Dyam_Create_Binary(I(9),&ref[349],I(7))
	move_ret ref[350]
	c_ret

;; TERM 349: '*GUARD*'(body_to_lpda(_M, internal_lc(_G, _J), noop, _N, rec_prolog))
c_code local build_ref_349
	ret_reg &ref[349]
	call_c   build_ref_34()
	call_c   build_ref_348()
	call_c   Dyam_Create_Unary(&ref[34],&ref[348])
	move_ret ref[349]
	c_ret

;; TERM 348: body_to_lpda(_M, internal_lc(_G, _J), noop, _N, rec_prolog)
c_code local build_ref_348
	ret_reg &ref[348]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_652()
	call_c   Dyam_Create_Binary(&ref[652],V(6),V(9))
	move_ret R(0)
	call_c   build_ref_601()
	call_c   build_ref_322()
	call_c   build_ref_544()
	call_c   Dyam_Term_Start(&ref[601],5)
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[322])
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(&ref[544])
	call_c   Dyam_Term_End()
	move_ret ref[348]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 652: internal_lc
c_code local build_ref_652
	ret_reg &ref[652]
	call_c   Dyam_Create_Atom("internal_lc")
	move_ret ref[652]
	c_ret

;; TERM 351: '*WRAPPER*'((lc _B / _C), [_I|_F], '*LCNEXT*'(_G, _H, '*PROLOG-LAST*', _E, (_G ^ _J ^ _N)))
c_code local build_ref_351
	ret_reg &ref[351]
	call_c   Dyam_Pseudo_Choice(3)
	call_c   build_ref_650()
	call_c   build_ref_354()
	call_c   Dyam_Create_Unary(&ref[650],&ref[354])
	move_ret R(0)
	call_c   Dyam_Create_List(V(8),V(5))
	move_ret R(1)
	call_c   build_ref_626()
	call_c   Dyam_Create_Binary(&ref[626],V(9),V(13))
	move_ret R(2)
	call_c   Dyam_Create_Binary(&ref[626],V(6),R(2))
	move_ret R(2)
	call_c   build_ref_653()
	call_c   build_ref_426()
	call_c   Dyam_Term_Start(&ref[653],5)
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(&ref[426])
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(R(2))
	call_c   Dyam_Term_End()
	move_ret R(2)
	call_c   build_ref_649()
	call_c   Dyam_Term_Start(&ref[649],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(R(2))
	call_c   Dyam_Term_End()
	move_ret ref[351]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 653: '*LCNEXT*'
c_code local build_ref_653
	ret_reg &ref[653]
	call_c   Dyam_Create_Atom("*LCNEXT*")
	move_ret ref[653]
	c_ret

long local pool_fun125[6]=[5,build_seed_88,build_ref_347,build_seed_89,build_ref_351,build_seed_50]

pl_code local fun125
	call_c   Dyam_Pool(pool_fun125)
	call_c   Dyam_Allocate(0)
	pl_call  fun15(&seed[88],1)
	move     &ref[347], R(0)
	move     S(5), R(1)
	move     V(10), R(2)
	move     S(5), R(3)
	move     V(11), R(4)
	move     S(5), R(5)
	pl_call  pred_my_numbervars_3()
	pl_call  fun15(&seed[89],1)
	call_c   DYAM_evpred_assert_1(&ref[351])
	call_c   Dyam_Deallocate()
	pl_jump  fun8(&seed[50],2)

long local pool_fun126[5]=[4,build_ref_343,build_ref_353,build_ref_354,build_ref_263]

pl_code local fun126
	call_c   Dyam_Pool(pool_fun126)
	call_c   Dyam_Unify_Item(&ref[343])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[353], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	move     &ref[354], R(4)
	move     S(5), R(5)
	move     &ref[263], R(6)
	move     S(5), R(7)
	move     V(5), R(8)
	move     S(5), R(9)
	call_c   Dyam_Reg_Deallocate(5)
	pl_jump  fun115()

;; TERM 342: '*FIRST*'(wrapping_predicate((lc _B / _C))) :> []
c_code local build_ref_342
	ret_reg &ref[342]
	call_c   build_ref_341()
	call_c   Dyam_Create_Binary(I(9),&ref[341],I(0))
	move_ret ref[342]
	c_ret

;; TERM 341: '*FIRST*'(wrapping_predicate((lc _B / _C)))
c_code local build_ref_341
	ret_reg &ref[341]
	call_c   build_ref_25()
	call_c   build_ref_340()
	call_c   Dyam_Create_Unary(&ref[25],&ref[340])
	move_ret ref[341]
	c_ret

c_code local build_seed_13
	ret_reg &seed[13]
	call_c   build_ref_25()
	call_c   build_ref_357()
	call_c   Dyam_Seed_Start(&ref[25],&ref[357],I(0),fun6,1)
	call_c   build_ref_358()
	call_c   Dyam_Seed_Add_Comp(&ref[358],fun132,0)
	call_c   Dyam_Seed_End()
	move_ret seed[13]
	c_ret

;; TERM 358: '*CITEM*'(wrapping_predicate((_B / _C)), _A)
c_code local build_ref_358
	ret_reg &ref[358]
	call_c   build_ref_29()
	call_c   build_ref_355()
	call_c   Dyam_Create_Binary(&ref[29],&ref[355],V(0))
	move_ret ref[358]
	c_ret

;; TERM 355: wrapping_predicate((_B / _C))
c_code local build_ref_355
	ret_reg &ref[355]
	call_c   build_ref_648()
	call_c   build_ref_354()
	call_c   Dyam_Create_Unary(&ref[648],&ref[354])
	move_ret ref[355]
	c_ret

;; TERM 368: '$CLOSURE'('$fun'(131, 0, 1143255628), '$TUPPLE'(35085872005944))
c_code local build_ref_368
	ret_reg &ref[368]
	call_c   build_ref_316()
	call_c   Dyam_Closure_Aux(fun131,&ref[316])
	move_ret ref[368]
	c_ret

;; TERM 359: [_I,_D,_E]
c_code local build_ref_359
	ret_reg &ref[359]
	call_c   build_ref_263()
	call_c   Dyam_Create_List(V(8),&ref[263])
	move_ret ref[359]
	c_ret

;; TERM 366: '*NEXT*'{nabla=> _E, call=> _G, ret=> _H, right=> '*PROLOG-LAST*'}
c_code local build_ref_366
	ret_reg &ref[366]
	call_c   build_ref_654()
	call_c   build_ref_426()
	call_c   Dyam_Term_Start(&ref[654],4)
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(&ref[426])
	call_c   Dyam_Term_End()
	move_ret ref[366]
	c_ret

;; TERM 654: '*NEXT*'!'$ft'
c_code local build_ref_654
	ret_reg &ref[654]
	call_c   build_ref_655()
	call_c   Dyam_Create_Atom_Module("$ft",&ref[655])
	move_ret ref[654]
	c_ret

;; TERM 655: '*NEXT*'
c_code local build_ref_655
	ret_reg &ref[655]
	call_c   Dyam_Create_Atom("*NEXT*")
	move_ret ref[655]
	c_ret

;; TERM 361: '*WRAPPER*'((_B / _C), [_I|_F], _L)
c_code local build_ref_361
	ret_reg &ref[361]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(8),V(5))
	move_ret R(0)
	call_c   build_ref_649()
	call_c   build_ref_354()
	call_c   Dyam_Term_Start(&ref[649],3)
	call_c   Dyam_Term_Arg(&ref[354])
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_End()
	move_ret ref[361]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun127[3]=[2,build_ref_361,build_seed_50]

long local pool_fun128[3]=[65537,build_ref_366,pool_fun127]

pl_code local fun128
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(11),&ref[366])
	fail_ret
fun127:
	call_c   DYAM_evpred_assert_1(&ref[361])
	call_c   Dyam_Deallocate()
	pl_jump  fun8(&seed[50],2)


;; TERM 364: nabla_subsume((_B / _C))
c_code local build_ref_364
	ret_reg &ref[364]
	call_c   build_ref_656()
	call_c   build_ref_354()
	call_c   Dyam_Create_Unary(&ref[656],&ref[354])
	move_ret ref[364]
	c_ret

;; TERM 656: nabla_subsume
c_code local build_ref_656
	ret_reg &ref[656]
	call_c   Dyam_Create_Atom("nabla_subsume")
	move_ret ref[656]
	c_ret

;; TERM 365: '*NEXTALT*'(_G, _H, '*PROLOG-LAST*', _E)
c_code local build_ref_365
	ret_reg &ref[365]
	call_c   build_ref_657()
	call_c   build_ref_426()
	call_c   Dyam_Term_Start(&ref[657],4)
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(&ref[426])
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[365]
	c_ret

;; TERM 657: '*NEXTALT*'
c_code local build_ref_657
	ret_reg &ref[657]
	call_c   Dyam_Create_Atom("*NEXTALT*")
	move_ret ref[657]
	c_ret

long local pool_fun129[5]=[131074,build_ref_364,build_ref_365,pool_fun128,pool_fun127]

pl_code local fun129
	call_c   Dyam_Update_Choice(fun128)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[364])
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(11),&ref[365])
	fail_ret
	pl_jump  fun127()

;; TERM 362: nabla_variance((_B / _C))
c_code local build_ref_362
	ret_reg &ref[362]
	call_c   build_ref_658()
	call_c   build_ref_354()
	call_c   Dyam_Create_Unary(&ref[658],&ref[354])
	move_ret ref[362]
	c_ret

;; TERM 658: nabla_variance
c_code local build_ref_658
	ret_reg &ref[658]
	call_c   Dyam_Create_Atom("nabla_variance")
	move_ret ref[658]
	c_ret

;; TERM 363: '*NEXTALT2*'(_G, _H, '*PROLOG-LAST*', _E)
c_code local build_ref_363
	ret_reg &ref[363]
	call_c   build_ref_659()
	call_c   build_ref_426()
	call_c   Dyam_Term_Start(&ref[659],4)
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(&ref[426])
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[363]
	c_ret

;; TERM 659: '*NEXTALT2*'
c_code local build_ref_659
	ret_reg &ref[659]
	call_c   Dyam_Create_Atom("*NEXTALT2*")
	move_ret ref[659]
	c_ret

long local pool_fun130[5]=[131074,build_ref_362,build_ref_363,pool_fun129,pool_fun127]

pl_code local fun130
	call_c   Dyam_Update_Choice(fun129)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[362])
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(11),&ref[363])
	fail_ret
	pl_jump  fun127()

;; TERM 360: '*LIGHTNEXTALT*'(_G, _H, '*PROLOG-LAST*', _E)
c_code local build_ref_360
	ret_reg &ref[360]
	call_c   build_ref_660()
	call_c   build_ref_426()
	call_c   Dyam_Term_Start(&ref[660],4)
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(&ref[426])
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[360]
	c_ret

;; TERM 660: '*LIGHTNEXTALT*'
c_code local build_ref_660
	ret_reg &ref[660]
	call_c   Dyam_Create_Atom("*LIGHTNEXTALT*")
	move_ret ref[660]
	c_ret

long local pool_fun131[7]=[131076,build_seed_88,build_ref_359,build_ref_198,build_ref_360,pool_fun130,pool_fun127]

pl_code local fun131
	call_c   Dyam_Pool(pool_fun131)
	call_c   Dyam_Allocate(0)
	pl_call  fun15(&seed[88],1)
	move     &ref[359], R(0)
	move     S(5), R(1)
	move     V(9), R(2)
	move     S(5), R(3)
	move     V(10), R(4)
	move     S(5), R(5)
	pl_call  pred_my_numbervars_3()
	call_c   Dyam_Choice(fun130)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[198])
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(11),&ref[360])
	fail_ret
	pl_jump  fun127()

long local pool_fun132[5]=[4,build_ref_358,build_ref_368,build_ref_354,build_ref_263]

pl_code local fun132
	call_c   Dyam_Pool(pool_fun132)
	call_c   Dyam_Unify_Item(&ref[358])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[368], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	move     &ref[354], R(4)
	move     S(5), R(5)
	move     &ref[263], R(6)
	move     S(5), R(7)
	move     V(5), R(8)
	move     S(5), R(9)
	call_c   Dyam_Reg_Deallocate(5)
	pl_jump  fun115()

;; TERM 357: '*FIRST*'(wrapping_predicate((_B / _C))) :> []
c_code local build_ref_357
	ret_reg &ref[357]
	call_c   build_ref_356()
	call_c   Dyam_Create_Binary(I(9),&ref[356],I(0))
	move_ret ref[357]
	c_ret

;; TERM 356: '*FIRST*'(wrapping_predicate((_B / _C)))
c_code local build_ref_356
	ret_reg &ref[356]
	call_c   build_ref_25()
	call_c   build_ref_355()
	call_c   Dyam_Create_Unary(&ref[25],&ref[355])
	move_ret ref[356]
	c_ret

c_code local build_seed_17
	ret_reg &seed[17]
	call_c   build_ref_33()
	call_c   build_ref_385()
	call_c   Dyam_Seed_Start(&ref[33],&ref[385],I(0),fun0,1)
	call_c   build_ref_384()
	call_c   Dyam_Seed_Add_Comp(&ref[384],fun149,0)
	call_c   Dyam_Seed_End()
	move_ret seed[17]
	c_ret

;; TERM 384: '*GUARD*'(body_to_lpda_handler(_B, _C, _D, _E, _F))
c_code local build_ref_384
	ret_reg &ref[384]
	call_c   build_ref_34()
	call_c   build_ref_383()
	call_c   Dyam_Create_Unary(&ref[34],&ref[383])
	move_ret ref[384]
	c_ret

;; TERM 383: body_to_lpda_handler(_B, _C, _D, _E, _F)
c_code local build_ref_383
	ret_reg &ref[383]
	call_c   build_ref_586()
	call_c   Dyam_Term_Start(&ref[586],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[383]
	c_ret

;; TERM 386: @*{goal=> _G, vars=> _H, from=> _I, to=> _J, collect_first=> _K, collect_last=> _L, collect_loop=> _M, collect_next=> _N, collect_pred=> _O}
c_code local build_ref_386
	ret_reg &ref[386]
	call_c   build_ref_616()
	call_c   Dyam_Term_Start(&ref[616],9)
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_End()
	move_ret ref[386]
	c_ret

;; TERM 387: kleene
c_code local build_ref_387
	ret_reg &ref[387]
	call_c   Dyam_Create_Atom("kleene")
	move_ret ref[387]
	c_ret

;; TERM 388: '_kleene~w'
c_code local build_ref_388
	ret_reg &ref[388]
	call_c   Dyam_Create_Atom("_kleene~w")
	move_ret ref[388]
	c_ret

;; TERM 389: [_P]
c_code local build_ref_389
	ret_reg &ref[389]
	call_c   Dyam_Create_List(V(15),I(0))
	move_ret ref[389]
	c_ret

;; TERM 390: kleene_conditions([_I,_J], [_R,_S], _T, _U, _V)
c_code local build_ref_390
	ret_reg &ref[390]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   Dyam_Create_Tupple(8,9,I(0))
	move_ret R(0)
	call_c   Dyam_Create_Tupple(17,18,I(0))
	move_ret R(1)
	call_c   build_ref_661()
	call_c   Dyam_Term_Start(&ref[661],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_Arg(V(21))
	call_c   Dyam_Term_End()
	move_ret ref[390]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 661: kleene_conditions
c_code local build_ref_661
	ret_reg &ref[661]
	call_c   Dyam_Create_Atom("kleene_conditions")
	move_ret ref[661]
	c_ret

;; TERM 391: [_R,_S,_T,_U,_V,@*{goal=> _G, vars=> _H, from=> _I, to=> _J, collect_first=> _K, collect_last=> _L, collect_loop=> _M, collect_next=> _N, collect_pred=> _O}]
c_code local build_ref_391
	ret_reg &ref[391]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_386()
	call_c   Dyam_Create_List(&ref[386],I(0))
	move_ret R(0)
	call_c   Dyam_Create_Tupple(17,21,R(0))
	move_ret ref[391]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 392: [_R|_M]
c_code local build_ref_392
	ret_reg &ref[392]
	call_c   Dyam_Create_List(V(17),V(12))
	move_ret ref[392]
	c_ret

;; TERM 393: [_S|_N]
c_code local build_ref_393
	ret_reg &ref[393]
	call_c   Dyam_Create_List(V(18),V(13))
	move_ret ref[393]
	c_ret

;; TERM 394: [_L,[_I,_J]]
c_code local build_ref_394
	ret_reg &ref[394]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Tupple(8,9,I(0))
	move_ret R(0)
	call_c   Dyam_Create_List(R(0),I(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(11),R(0))
	move_ret ref[394]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_91
	ret_reg &seed[91]
	call_c   build_ref_34()
	call_c   build_ref_396()
	call_c   Dyam_Seed_Start(&ref[34],&ref[396],I(0),fun1,1)
	call_c   build_ref_397()
	call_c   Dyam_Seed_Add_Comp(&ref[397],&ref[396],0)
	call_c   Dyam_Seed_End()
	move_ret seed[91]
	c_ret

;; TERM 397: '*GUARD*'(body_to_lpda_handler(_B, (_U , _L = _M), _D, _C1, _F)) :> '$$HOLE$$'
c_code local build_ref_397
	ret_reg &ref[397]
	call_c   build_ref_396()
	call_c   Dyam_Create_Binary(I(9),&ref[396],I(7))
	move_ret ref[397]
	c_ret

;; TERM 396: '*GUARD*'(body_to_lpda_handler(_B, (_U , _L = _M), _D, _C1, _F))
c_code local build_ref_396
	ret_reg &ref[396]
	call_c   build_ref_34()
	call_c   build_ref_395()
	call_c   Dyam_Create_Unary(&ref[34],&ref[395])
	move_ret ref[396]
	c_ret

;; TERM 395: body_to_lpda_handler(_B, (_U , _L = _M), _D, _C1, _F)
c_code local build_ref_395
	ret_reg &ref[395]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_603()
	call_c   Dyam_Create_Binary(&ref[603],V(11),V(12))
	move_ret R(0)
	call_c   Dyam_Create_Binary(I(4),V(20),R(0))
	move_ret R(0)
	call_c   build_ref_586()
	call_c   Dyam_Term_Start(&ref[586],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(28))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[395]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_92
	ret_reg &seed[92]
	call_c   build_ref_34()
	call_c   build_ref_399()
	call_c   Dyam_Seed_Start(&ref[34],&ref[399],I(0),fun1,1)
	call_c   build_ref_400()
	call_c   Dyam_Seed_Add_Comp(&ref[400],&ref[399],0)
	call_c   Dyam_Seed_End()
	move_ret seed[92]
	c_ret

;; TERM 400: '*GUARD*'(body_to_lpda_handler(_B, (_V , _G), '*KLEENE-LAST*'(_Q, _Y, _X, _Z, _B1, noop), _D1, _F)) :> '$$HOLE$$'
c_code local build_ref_400
	ret_reg &ref[400]
	call_c   build_ref_399()
	call_c   Dyam_Create_Binary(I(9),&ref[399],I(7))
	move_ret ref[400]
	c_ret

;; TERM 399: '*GUARD*'(body_to_lpda_handler(_B, (_V , _G), '*KLEENE-LAST*'(_Q, _Y, _X, _Z, _B1, noop), _D1, _F))
c_code local build_ref_399
	ret_reg &ref[399]
	call_c   build_ref_34()
	call_c   build_ref_398()
	call_c   Dyam_Create_Unary(&ref[34],&ref[398])
	move_ret ref[399]
	c_ret

;; TERM 398: body_to_lpda_handler(_B, (_V , _G), '*KLEENE-LAST*'(_Q, _Y, _X, _Z, _B1, noop), _D1, _F)
c_code local build_ref_398
	ret_reg &ref[398]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   Dyam_Create_Binary(I(4),V(21),V(6))
	move_ret R(0)
	call_c   build_ref_662()
	call_c   build_ref_322()
	call_c   Dyam_Term_Start(&ref[662],6)
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(24))
	call_c   Dyam_Term_Arg(V(23))
	call_c   Dyam_Term_Arg(V(25))
	call_c   Dyam_Term_Arg(V(27))
	call_c   Dyam_Term_Arg(&ref[322])
	call_c   Dyam_Term_End()
	move_ret R(1)
	call_c   build_ref_586()
	call_c   Dyam_Term_Start(&ref[586],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(29))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[398]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 662: '*KLEENE-LAST*'
c_code local build_ref_662
	ret_reg &ref[662]
	call_c   Dyam_Create_Atom("*KLEENE-LAST*")
	move_ret ref[662]
	c_ret

;; TERM 401: '*KLEENE*'(_Q, _Z, '*KLEENE-ALTERNATIVE*'(_C1, _D1))
c_code local build_ref_401
	ret_reg &ref[401]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_664()
	call_c   Dyam_Create_Binary(&ref[664],V(28),V(29))
	move_ret R(0)
	call_c   build_ref_663()
	call_c   Dyam_Term_Start(&ref[663],3)
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(25))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_End()
	move_ret ref[401]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 663: '*KLEENE*'
c_code local build_ref_663
	ret_reg &ref[663]
	call_c   Dyam_Create_Atom("*KLEENE*")
	move_ret ref[663]
	c_ret

;; TERM 664: '*KLEENE-ALTERNATIVE*'
c_code local build_ref_664
	ret_reg &ref[664]
	call_c   Dyam_Create_Atom("*KLEENE-ALTERNATIVE*")
	move_ret ref[664]
	c_ret

c_code local build_seed_93
	ret_reg &seed[93]
	call_c   build_ref_34()
	call_c   build_ref_403()
	call_c   Dyam_Seed_Start(&ref[34],&ref[403],I(0),fun1,1)
	call_c   build_ref_404()
	call_c   Dyam_Seed_Add_Comp(&ref[404],&ref[403],0)
	call_c   Dyam_Seed_End()
	move_ret seed[93]
	c_ret

;; TERM 404: '*GUARD*'(body_to_lpda_handler(_B, (_T , _K = _M), _E1, _E, _F)) :> '$$HOLE$$'
c_code local build_ref_404
	ret_reg &ref[404]
	call_c   build_ref_403()
	call_c   Dyam_Create_Binary(I(9),&ref[403],I(7))
	move_ret ref[404]
	c_ret

;; TERM 403: '*GUARD*'(body_to_lpda_handler(_B, (_T , _K = _M), _E1, _E, _F))
c_code local build_ref_403
	ret_reg &ref[403]
	call_c   build_ref_34()
	call_c   build_ref_402()
	call_c   Dyam_Create_Unary(&ref[34],&ref[402])
	move_ret ref[403]
	c_ret

;; TERM 402: body_to_lpda_handler(_B, (_T , _K = _M), _E1, _E, _F)
c_code local build_ref_402
	ret_reg &ref[402]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_603()
	call_c   Dyam_Create_Binary(&ref[603],V(10),V(12))
	move_ret R(0)
	call_c   Dyam_Create_Binary(I(4),V(19),R(0))
	move_ret R(0)
	call_c   build_ref_586()
	call_c   Dyam_Term_Start(&ref[586],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(30))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[402]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun149[15]=[14,build_ref_384,build_ref_386,build_ref_387,build_ref_388,build_ref_389,build_ref_390,build_ref_391,build_ref_392,build_ref_393,build_ref_394,build_seed_91,build_seed_92,build_ref_401,build_seed_93]

pl_code local fun149
	call_c   Dyam_Pool(pool_fun149)
	call_c   Dyam_Unify_Item(&ref[384])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(2))
	move     &ref[386], R(2)
	move     S(5), R(3)
	pl_call  pred_kleene_normalize_2()
	move     &ref[387], R(0)
	move     0, R(1)
	move     V(15), R(2)
	move     S(5), R(3)
	pl_call  pred_update_counter_2()
	move     &ref[388], R(0)
	move     0, R(1)
	move     &ref[389], R(2)
	move     S(5), R(3)
	move     V(16), R(4)
	move     S(5), R(5)
	pl_call  pred_name_builder_3()
	pl_call  Object_1(&ref[390])
	move     &ref[391], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(22), R(4)
	move     S(5), R(5)
	pl_call  pred_my_numbervars_3()
	call_c   Dyam_Unify(V(23),&ref[392])
	fail_ret
	call_c   Dyam_Unify(V(24),&ref[393])
	fail_ret
	call_c   Dyam_Reg_Load(0,V(7))
	move     V(25), R(2)
	move     S(5), R(3)
	pl_call  pred_tupple_2()
	call_c   Dyam_Reg_Load(0,V(3))
	move     V(26), R(2)
	move     S(5), R(3)
	pl_call  pred_tupple_2()
	move     &ref[394], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(26))
	move     V(27), R(4)
	move     S(5), R(5)
	pl_call  pred_extend_tupple_3()
	pl_call  fun15(&seed[91],1)
	pl_call  fun15(&seed[92],1)
	call_c   Dyam_Unify(V(30),&ref[401])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_jump  fun15(&seed[93],1)

;; TERM 385: '*GUARD*'(body_to_lpda_handler(_B, _C, _D, _E, _F)) :> []
c_code local build_ref_385
	ret_reg &ref[385]
	call_c   build_ref_384()
	call_c   Dyam_Create_Binary(I(9),&ref[384],I(0))
	move_ret ref[385]
	c_ret

c_code local build_seed_35
	ret_reg &seed[35]
	call_c   build_ref_33()
	call_c   build_ref_407()
	call_c   Dyam_Seed_Start(&ref[33],&ref[407],I(0),fun0,1)
	call_c   build_ref_406()
	call_c   Dyam_Seed_Add_Comp(&ref[406],fun168,0)
	call_c   Dyam_Seed_End()
	move_ret seed[35]
	c_ret

;; TERM 406: '*GUARD*'(pgm_to_lpda('$fact'(_B), _C))
c_code local build_ref_406
	ret_reg &ref[406]
	call_c   build_ref_34()
	call_c   build_ref_405()
	call_c   Dyam_Create_Unary(&ref[34],&ref[405])
	move_ret ref[406]
	c_ret

;; TERM 405: pgm_to_lpda('$fact'(_B), _C)
c_code local build_ref_405
	ret_reg &ref[405]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_665()
	call_c   Dyam_Create_Unary(&ref[665],V(1))
	move_ret R(0)
	call_c   build_ref_589()
	call_c   Dyam_Create_Binary(&ref[589],R(0),V(2))
	move_ret ref[405]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 665: '$fact'
c_code local build_ref_665
	ret_reg &ref[665]
	call_c   Dyam_Create_Atom("$fact")
	move_ret ref[665]
	c_ret

;; TERM 408: _F / _G
c_code local build_ref_408
	ret_reg &ref[408]
	call_c   build_ref_627()
	call_c   Dyam_Create_Binary(&ref[627],V(5),V(6))
	move_ret ref[408]
	c_ret

;; TERM 458: '$CLOSURE'('$fun'(158, 0, 1143528096), '$TUPPLE'(35085872007424))
c_code local build_ref_458
	ret_reg &ref[458]
	call_c   build_ref_421()
	call_c   Dyam_Closure_Aux(fun158,&ref[421])
	move_ret ref[458]
	c_ret

c_code local build_seed_96
	ret_reg &seed[96]
	call_c   build_ref_34()
	call_c   build_ref_438()
	call_c   Dyam_Seed_Start(&ref[34],&ref[438],I(0),fun1,1)
	call_c   build_ref_439()
	call_c   Dyam_Seed_Add_Comp(&ref[439],&ref[438],0)
	call_c   Dyam_Seed_End()
	move_ret seed[96]
	c_ret

;; TERM 439: '*GUARD*'(make_callret(_I, _L, _M)) :> '$$HOLE$$'
c_code local build_ref_439
	ret_reg &ref[439]
	call_c   build_ref_438()
	call_c   Dyam_Create_Binary(I(9),&ref[438],I(7))
	move_ret ref[439]
	c_ret

;; TERM 438: '*GUARD*'(make_callret(_I, _L, _M))
c_code local build_ref_438
	ret_reg &ref[438]
	call_c   build_ref_34()
	call_c   build_ref_437()
	call_c   Dyam_Create_Unary(&ref[34],&ref[437])
	move_ret ref[438]
	c_ret

;; TERM 437: make_callret(_I, _L, _M)
c_code local build_ref_437
	ret_reg &ref[437]
	call_c   build_ref_651()
	call_c   Dyam_Term_Start(&ref[651],3)
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_End()
	move_ret ref[437]
	c_ret

;; TERM 456: '*FIRST*'(_L) :> '*LAST*'(_M)
c_code local build_ref_456
	ret_reg &ref[456]
	call_c   build_ref_440()
	call_c   build_ref_455()
	call_c   Dyam_Create_Binary(I(9),&ref[440],&ref[455])
	move_ret ref[456]
	c_ret

;; TERM 455: '*LAST*'(_M)
c_code local build_ref_455
	ret_reg &ref[455]
	call_c   build_ref_666()
	call_c   Dyam_Create_Unary(&ref[666],V(12))
	move_ret ref[455]
	c_ret

;; TERM 666: '*LAST*'
c_code local build_ref_666
	ret_reg &ref[666]
	call_c   Dyam_Create_Atom("*LAST*")
	move_ret ref[666]
	c_ret

;; TERM 440: '*FIRST*'(_L)
c_code local build_ref_440
	ret_reg &ref[440]
	call_c   build_ref_25()
	call_c   Dyam_Create_Unary(&ref[25],V(11))
	move_ret ref[440]
	c_ret

long local pool_fun158[3]=[2,build_seed_96,build_ref_456]

pl_code local fun158
	call_c   Dyam_Pool(pool_fun158)
	call_c   Dyam_Allocate(0)
	pl_call  fun15(&seed[96],1)
	call_c   Dyam_Unify(V(2),&ref[456])
	fail_ret
	pl_jump  fun65()

;; TERM 421: '$TUPPLE'(35085872007424)
c_code local build_ref_421
	ret_reg &ref[421]
	call_c   Dyam_Create_Simple_Tupple(0,68157440)
	move_ret ref[421]
	c_ret

;; TERM 409: _H / _G
c_code local build_ref_409
	ret_reg &ref[409]
	call_c   build_ref_627()
	call_c   Dyam_Create_Binary(&ref[627],V(7),V(6))
	move_ret ref[409]
	c_ret

long local pool_fun159[3]=[2,build_ref_458,build_ref_409]

pl_code local fun159
	call_c   Dyam_Remove_Choice()
	move     &ref[458], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	move     &ref[409], R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Deallocate(3)
fun152:
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Reg_Bind(2,V(3))
	call_c   Dyam_Reg_Bind(4,V(2))
	call_c   Dyam_Choice(fun151)
	pl_call  fun74(&seed[94],1)
	pl_fail


;; TERM 454: '$answers'(_Q)
c_code local build_ref_454
	ret_reg &ref[454]
	call_c   build_ref_600()
	call_c   Dyam_Create_Unary(&ref[600],V(16))
	move_ret ref[454]
	c_ret

;; TERM 450: 'Builtin predicate not accepted as rule head: ~w'
c_code local build_ref_450
	ret_reg &ref[450]
	call_c   Dyam_Create_Atom("Builtin predicate not accepted as rule head: ~w")
	move_ret ref[450]
	c_ret

;; TERM 451: [_I]
c_code local build_ref_451
	ret_reg &ref[451]
	call_c   Dyam_Create_List(V(8),I(0))
	move_ret ref[451]
	c_ret

long local pool_fun160[5]=[65539,build_ref_454,build_ref_450,build_ref_451,pool_fun159]

pl_code local fun160
	call_c   Dyam_Update_Choice(fun159)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(8),&ref[454])
	fail_ret
	call_c   Dyam_Cut()
	move     &ref[450], R(0)
	move     0, R(1)
	move     &ref[451], R(2)
	move     S(5), R(3)
	pl_call  pred_error_2()
	pl_jump  fun65()

;; TERM 453: wait(_P)
c_code local build_ref_453
	ret_reg &ref[453]
	call_c   build_ref_587()
	call_c   Dyam_Create_Unary(&ref[587],V(15))
	move_ret ref[453]
	c_ret

long local pool_fun161[5]=[65539,build_ref_453,build_ref_450,build_ref_451,pool_fun160]

pl_code local fun161
	call_c   Dyam_Update_Choice(fun160)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(8),&ref[453])
	fail_ret
	call_c   Dyam_Cut()
	move     &ref[450], R(0)
	move     0, R(1)
	move     &ref[451], R(2)
	move     S(5), R(3)
	pl_call  pred_error_2()
	pl_jump  fun65()

;; TERM 452: builtin((_H / _G), _O)
c_code local build_ref_452
	ret_reg &ref[452]
	call_c   build_ref_646()
	call_c   build_ref_409()
	call_c   Dyam_Create_Binary(&ref[646],&ref[409],V(14))
	move_ret ref[452]
	c_ret

long local pool_fun162[5]=[65539,build_ref_452,build_ref_450,build_ref_451,pool_fun161]

pl_code local fun162
	call_c   Dyam_Update_Choice(fun161)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[452])
	call_c   Dyam_Cut()
	move     &ref[450], R(0)
	move     0, R(1)
	move     &ref[451], R(2)
	move     S(5), R(3)
	pl_call  pred_error_2()
	pl_jump  fun65()

;; TERM 449: foreign((_H / _G), _N)
c_code local build_ref_449
	ret_reg &ref[449]
	call_c   build_ref_630()
	call_c   build_ref_409()
	call_c   Dyam_Create_Binary(&ref[630],&ref[409],V(13))
	move_ret ref[449]
	c_ret

long local pool_fun163[5]=[65539,build_ref_449,build_ref_450,build_ref_451,pool_fun162]

pl_code local fun163
	call_c   Dyam_Update_Choice(fun162)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[449])
	call_c   Dyam_Cut()
	move     &ref[450], R(0)
	move     0, R(1)
	move     &ref[451], R(2)
	move     S(5), R(3)
	pl_call  pred_error_2()
	pl_jump  fun65()

;; TERM 445: extensional _H / _G
c_code local build_ref_445
	ret_reg &ref[445]
	call_c   build_ref_632()
	call_c   build_ref_409()
	call_c   Dyam_Create_Unary(&ref[632],&ref[409])
	move_ret ref[445]
	c_ret

;; TERM 448: '$CLOSURE'('$fun'(157, 0, 1143507040), '$TUPPLE'(35085872007424))
c_code local build_ref_448
	ret_reg &ref[448]
	call_c   build_ref_421()
	call_c   Dyam_Closure_Aux(fun157,&ref[421])
	move_ret ref[448]
	c_ret

;; TERM 446: '*DATABASE*'(_I)
c_code local build_ref_446
	ret_reg &ref[446]
	call_c   build_ref_18()
	call_c   Dyam_Create_Unary(&ref[18],V(8))
	move_ret ref[446]
	c_ret

long local pool_fun157[2]=[1,build_ref_446]

pl_code local fun157
	call_c   Dyam_Pool(pool_fun157)
	call_c   Dyam_Unify(V(2),&ref[446])
	fail_ret
	call_c   Dyam_Allocate(0)
	pl_jump  fun65()

long local pool_fun164[5]=[65539,build_ref_445,build_ref_448,build_ref_409,pool_fun163]

pl_code local fun164
	call_c   Dyam_Update_Choice(fun163)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[445])
	call_c   Dyam_Cut()
	move     &ref[448], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	move     &ref[409], R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  fun152()

;; TERM 436: light_tabular _H / _G
c_code local build_ref_436
	ret_reg &ref[436]
	call_c   build_ref_633()
	call_c   build_ref_409()
	call_c   Dyam_Create_Unary(&ref[633],&ref[409])
	move_ret ref[436]
	c_ret

;; TERM 444: '$CLOSURE'('$fun'(156, 0, 1143501040), '$TUPPLE'(35085872007424))
c_code local build_ref_444
	ret_reg &ref[444]
	call_c   build_ref_421()
	call_c   Dyam_Closure_Aux(fun156,&ref[421])
	move_ret ref[444]
	c_ret

;; TERM 442: '*FIRST*'(_L) :> '*LIGHTLAST*'(_M)
c_code local build_ref_442
	ret_reg &ref[442]
	call_c   build_ref_440()
	call_c   build_ref_441()
	call_c   Dyam_Create_Binary(I(9),&ref[440],&ref[441])
	move_ret ref[442]
	c_ret

;; TERM 441: '*LIGHTLAST*'(_M)
c_code local build_ref_441
	ret_reg &ref[441]
	call_c   build_ref_667()
	call_c   Dyam_Create_Unary(&ref[667],V(12))
	move_ret ref[441]
	c_ret

;; TERM 667: '*LIGHTLAST*'
c_code local build_ref_667
	ret_reg &ref[667]
	call_c   Dyam_Create_Atom("*LIGHTLAST*")
	move_ret ref[667]
	c_ret

long local pool_fun156[3]=[2,build_seed_96,build_ref_442]

pl_code local fun156
	call_c   Dyam_Pool(pool_fun156)
	call_c   Dyam_Allocate(0)
	pl_call  fun15(&seed[96],1)
	call_c   Dyam_Unify(V(2),&ref[442])
	fail_ret
	pl_jump  fun65()

long local pool_fun165[5]=[65539,build_ref_436,build_ref_444,build_ref_409,pool_fun164]

pl_code local fun165
	call_c   Dyam_Update_Choice(fun164)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[436])
	call_c   Dyam_Cut()
	move     &ref[444], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	move     &ref[409], R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  fun152()

;; TERM 430: std_prolog _H / _G
c_code local build_ref_430
	ret_reg &ref[430]
	call_c   build_ref_634()
	call_c   build_ref_409()
	call_c   Dyam_Create_Unary(&ref[634],&ref[409])
	move_ret ref[430]
	c_ret

;; TERM 435: '$CLOSURE'('$fun'(155, 0, 1143483872), '$TUPPLE'(35085872007460))
c_code local build_ref_435
	ret_reg &ref[435]
	call_c   build_ref_434()
	call_c   Dyam_Closure_Aux(fun155,&ref[434])
	move_ret ref[435]
	c_ret

;; TERM 431: [_J|_K]
c_code local build_ref_431
	ret_reg &ref[431]
	call_c   Dyam_Create_List(V(9),V(10))
	move_ret ref[431]
	c_ret

;; TERM 432: '*STD-PROLOG-FIRST*'((_H / _G), _K, deallocate_layer)
c_code local build_ref_432
	ret_reg &ref[432]
	call_c   build_ref_668()
	call_c   build_ref_409()
	call_c   build_ref_573()
	call_c   Dyam_Term_Start(&ref[668],3)
	call_c   Dyam_Term_Arg(&ref[409])
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(&ref[573])
	call_c   Dyam_Term_End()
	move_ret ref[432]
	c_ret

;; TERM 668: '*STD-PROLOG-FIRST*'
c_code local build_ref_668
	ret_reg &ref[668]
	call_c   Dyam_Create_Atom("*STD-PROLOG-FIRST*")
	move_ret ref[668]
	c_ret

long local pool_fun155[3]=[2,build_ref_431,build_ref_432]

pl_code local fun155
	call_c   Dyam_Pool(pool_fun155)
	call_c   DYAM_evpred_univ(V(8),&ref[431])
	fail_ret
	call_c   Dyam_Unify(V(2),&ref[432])
	fail_ret
	call_c   Dyam_Allocate(0)
	pl_jump  fun65()

;; TERM 434: '$TUPPLE'(35085872007460)
c_code local build_ref_434
	ret_reg &ref[434]
	call_c   Dyam_Create_Simple_Tupple(0,74448896)
	move_ret ref[434]
	c_ret

long local pool_fun166[5]=[65539,build_ref_430,build_ref_435,build_ref_409,pool_fun165]

pl_code local fun166
	call_c   Dyam_Update_Choice(fun165)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[430])
	call_c   Dyam_Cut()
	move     &ref[435], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	move     &ref[409], R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  fun152()

;; TERM 423: prolog _H / _G
c_code local build_ref_423
	ret_reg &ref[423]
	call_c   build_ref_579()
	call_c   build_ref_409()
	call_c   Dyam_Create_Unary(&ref[579],&ref[409])
	move_ret ref[423]
	c_ret

;; TERM 429: '$CLOSURE'('$fun'(154, 0, 1143478100), '$TUPPLE'(35085872007424))
c_code local build_ref_429
	ret_reg &ref[429]
	call_c   build_ref_421()
	call_c   Dyam_Closure_Aux(fun154,&ref[421])
	move_ret ref[429]
	c_ret

;; TERM 427: '*PROLOG-FIRST*'(_I) :> '*PROLOG-LAST*'
c_code local build_ref_427
	ret_reg &ref[427]
	call_c   build_ref_425()
	call_c   build_ref_426()
	call_c   Dyam_Create_Binary(I(9),&ref[425],&ref[426])
	move_ret ref[427]
	c_ret

;; TERM 425: '*PROLOG-FIRST*'(_I)
c_code local build_ref_425
	ret_reg &ref[425]
	call_c   build_ref_424()
	call_c   Dyam_Create_Unary(&ref[424],V(8))
	move_ret ref[425]
	c_ret

long local pool_fun154[2]=[1,build_ref_427]

pl_code local fun154
	call_c   Dyam_Pool(pool_fun154)
	call_c   Dyam_Unify(V(2),&ref[427])
	fail_ret
	call_c   Dyam_Allocate(0)
	pl_jump  fun65()

long local pool_fun167[5]=[65539,build_ref_423,build_ref_429,build_ref_409,pool_fun166]

pl_code local fun167
	call_c   Dyam_Update_Choice(fun166)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[423])
	call_c   Dyam_Cut()
	move     &ref[429], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	move     &ref[409], R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  fun152()

;; TERM 410: rec_prolog _H / _G
c_code local build_ref_410
	ret_reg &ref[410]
	call_c   build_ref_544()
	call_c   build_ref_409()
	call_c   Dyam_Create_Unary(&ref[544],&ref[409])
	move_ret ref[410]
	c_ret

;; TERM 422: '$CLOSURE'('$fun'(153, 0, 1143464296), '$TUPPLE'(35085872007424))
c_code local build_ref_422
	ret_reg &ref[422]
	call_c   build_ref_421()
	call_c   Dyam_Closure_Aux(fun153,&ref[421])
	move_ret ref[422]
	c_ret

;; TERM 419: '*GUARD*'(_I) :> noop
c_code local build_ref_419
	ret_reg &ref[419]
	call_c   build_ref_418()
	call_c   build_ref_322()
	call_c   Dyam_Create_Binary(I(9),&ref[418],&ref[322])
	move_ret ref[419]
	c_ret

;; TERM 418: '*GUARD*'(_I)
c_code local build_ref_418
	ret_reg &ref[418]
	call_c   build_ref_34()
	call_c   Dyam_Create_Unary(&ref[34],V(8))
	move_ret ref[418]
	c_ret

long local pool_fun153[2]=[1,build_ref_419]

pl_code local fun153
	call_c   Dyam_Pool(pool_fun153)
	call_c   Dyam_Unify(V(2),&ref[419])
	fail_ret
	call_c   Dyam_Allocate(0)
	pl_jump  fun65()

long local pool_fun168[7]=[65541,build_ref_406,build_ref_408,build_ref_410,build_ref_422,build_ref_409,pool_fun167]

pl_code local fun168
	call_c   Dyam_Pool(pool_fun168)
	call_c   Dyam_Unify_Item(&ref[406])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(1))
	move     V(3), R(2)
	move     S(5), R(3)
	move     V(4), R(4)
	move     S(5), R(5)
	pl_call  pred_my_numbervars_3()
	call_c   DYAM_evpred_functor(V(1),V(5),V(6))
	fail_ret
	move     &ref[408], R(0)
	move     S(5), R(1)
	move     &ref[409], R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Load(4,V(1))
	move     V(8), R(6)
	move     S(5), R(7)
	pl_call  pred_term_current_module_shift_4()
	call_c   Dyam_Choice(fun167)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[410])
	call_c   Dyam_Cut()
	move     &ref[422], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	move     &ref[409], R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  fun152()

;; TERM 407: '*GUARD*'(pgm_to_lpda('$fact'(_B), _C)) :> []
c_code local build_ref_407
	ret_reg &ref[407]
	call_c   build_ref_406()
	call_c   Dyam_Create_Binary(I(9),&ref[406],I(0))
	move_ret ref[407]
	c_ret

c_code local build_seed_33
	ret_reg &seed[33]
	call_c   build_ref_33()
	call_c   build_ref_459()
	call_c   Dyam_Seed_Start(&ref[33],&ref[459],I(0),fun0,1)
	call_c   build_ref_70()
	call_c   Dyam_Seed_Add_Comp(&ref[70],fun184,0)
	call_c   Dyam_Seed_End()
	move_ret seed[33]
	c_ret

;; TERM 461: _G / _H
c_code local build_ref_461
	ret_reg &ref[461]
	call_c   build_ref_627()
	call_c   Dyam_Create_Binary(&ref[627],V(6),V(7))
	move_ret ref[461]
	c_ret

;; TERM 462: _I / _H
c_code local build_ref_462
	ret_reg &ref[462]
	call_c   build_ref_627()
	call_c   Dyam_Create_Binary(&ref[627],V(8),V(7))
	move_ret ref[462]
	c_ret

;; TERM 513: '$CLOSURE'('$fun'(177, 0, 1143708296), '$TUPPLE'(35085872800704))
c_code local build_ref_513
	ret_reg &ref[513]
	call_c   build_ref_473()
	call_c   Dyam_Closure_Aux(fun177,&ref[473])
	move_ret ref[513]
	c_ret

c_code local build_seed_100
	ret_reg &seed[100]
	call_c   build_ref_34()
	call_c   build_ref_494()
	call_c   Dyam_Seed_Start(&ref[34],&ref[494],I(0),fun1,1)
	call_c   build_ref_495()
	call_c   Dyam_Seed_Add_Comp(&ref[495],&ref[494],0)
	call_c   Dyam_Seed_End()
	move_ret seed[100]
	c_ret

;; TERM 495: '*GUARD*'(make_callret(_J, _N, _O)) :> '$$HOLE$$'
c_code local build_ref_495
	ret_reg &ref[495]
	call_c   build_ref_494()
	call_c   Dyam_Create_Binary(I(9),&ref[494],I(7))
	move_ret ref[495]
	c_ret

;; TERM 494: '*GUARD*'(make_callret(_J, _N, _O))
c_code local build_ref_494
	ret_reg &ref[494]
	call_c   build_ref_34()
	call_c   build_ref_493()
	call_c   Dyam_Create_Unary(&ref[34],&ref[493])
	move_ret ref[494]
	c_ret

;; TERM 493: make_callret(_J, _N, _O)
c_code local build_ref_493
	ret_reg &ref[493]
	call_c   build_ref_651()
	call_c   Dyam_Term_Start(&ref[651],3)
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_End()
	move_ret ref[493]
	c_ret

;; TERM 497: '*FIRST*'(_N) :> _K
c_code local build_ref_497
	ret_reg &ref[497]
	call_c   build_ref_496()
	call_c   Dyam_Create_Binary(I(9),&ref[496],V(10))
	move_ret ref[497]
	c_ret

;; TERM 496: '*FIRST*'(_N)
c_code local build_ref_496
	ret_reg &ref[496]
	call_c   build_ref_25()
	call_c   Dyam_Create_Unary(&ref[25],V(13))
	move_ret ref[496]
	c_ret

c_code local build_seed_102
	ret_reg &seed[102]
	call_c   build_ref_34()
	call_c   build_ref_510()
	call_c   Dyam_Seed_Start(&ref[34],&ref[510],I(0),fun1,1)
	call_c   build_ref_511()
	call_c   Dyam_Seed_Add_Comp(&ref[511],&ref[510],0)
	call_c   Dyam_Seed_End()
	move_ret seed[102]
	c_ret

;; TERM 511: '*GUARD*'(body_to_lpda(_E, _C, '*LAST*'(_O), _K, dyalog)) :> '$$HOLE$$'
c_code local build_ref_511
	ret_reg &ref[511]
	call_c   build_ref_510()
	call_c   Dyam_Create_Binary(I(9),&ref[510],I(7))
	move_ret ref[511]
	c_ret

;; TERM 510: '*GUARD*'(body_to_lpda(_E, _C, '*LAST*'(_O), _K, dyalog))
c_code local build_ref_510
	ret_reg &ref[510]
	call_c   build_ref_34()
	call_c   build_ref_509()
	call_c   Dyam_Create_Unary(&ref[34],&ref[509])
	move_ret ref[510]
	c_ret

;; TERM 509: body_to_lpda(_E, _C, '*LAST*'(_O), _K, dyalog)
c_code local build_ref_509
	ret_reg &ref[509]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_666()
	call_c   Dyam_Create_Unary(&ref[666],V(14))
	move_ret R(0)
	call_c   build_ref_601()
	call_c   build_ref_576()
	call_c   Dyam_Term_Start(&ref[601],5)
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(&ref[576])
	call_c   Dyam_Term_End()
	move_ret ref[509]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun177[4]=[3,build_seed_100,build_ref_497,build_seed_102]

pl_code local fun177
	call_c   Dyam_Pool(pool_fun177)
	call_c   Dyam_Allocate(0)
	pl_call  fun15(&seed[100],1)
	call_c   Dyam_Unify(V(3),&ref[497])
	fail_ret
	pl_call  fun15(&seed[102],1)
	pl_jump  fun65()

;; TERM 473: '$TUPPLE'(35085872800704)
c_code local build_ref_473
	ret_reg &ref[473]
	call_c   Dyam_Create_Simple_Tupple(0,117964800)
	move_ret ref[473]
	c_ret

long local pool_fun178[3]=[2,build_ref_513,build_ref_462]

pl_code local fun178
	call_c   Dyam_Remove_Choice()
	move     &ref[513], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	move     &ref[462], R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  fun152()

;; TERM 508: '$answers'(_S)
c_code local build_ref_508
	ret_reg &ref[508]
	call_c   build_ref_600()
	call_c   Dyam_Create_Unary(&ref[600],V(18))
	move_ret ref[508]
	c_ret

;; TERM 504: 'Not a valid rule head: ~w'
c_code local build_ref_504
	ret_reg &ref[504]
	call_c   Dyam_Create_Atom("Not a valid rule head: ~w")
	move_ret ref[504]
	c_ret

long local pool_fun173[3]=[2,build_ref_504,build_ref_505]

long local pool_fun174[3]=[65537,build_ref_508,pool_fun173]

pl_code local fun174
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(9),&ref[508])
	fail_ret
fun173:
	call_c   Dyam_Cut()
	move     &ref[504], R(0)
	move     0, R(1)
	move     &ref[505], R(2)
	move     S(5), R(3)
	pl_call  pred_error_2()
	pl_jump  fun65()


;; TERM 507: wait(_R)
c_code local build_ref_507
	ret_reg &ref[507]
	call_c   build_ref_587()
	call_c   Dyam_Create_Unary(&ref[587],V(17))
	move_ret ref[507]
	c_ret

long local pool_fun175[4]=[131073,build_ref_507,pool_fun174,pool_fun173]

pl_code local fun175
	call_c   Dyam_Update_Choice(fun174)
	call_c   Dyam_Unify(V(9),&ref[507])
	fail_ret
	pl_jump  fun173()

;; TERM 506: builtin((_I / _H), _Q)
c_code local build_ref_506
	ret_reg &ref[506]
	call_c   build_ref_646()
	call_c   build_ref_462()
	call_c   Dyam_Create_Binary(&ref[646],&ref[462],V(16))
	move_ret ref[506]
	c_ret

long local pool_fun176[4]=[131073,build_ref_506,pool_fun175,pool_fun173]

pl_code local fun176
	call_c   Dyam_Update_Choice(fun175)
	pl_call  Object_1(&ref[506])
	pl_jump  fun173()

;; TERM 503: foreign((_I / _H), _P)
c_code local build_ref_503
	ret_reg &ref[503]
	call_c   build_ref_630()
	call_c   build_ref_462()
	call_c   Dyam_Create_Binary(&ref[630],&ref[462],V(15))
	move_ret ref[503]
	c_ret

long local pool_fun179[5]=[196609,build_ref_503,pool_fun178,pool_fun176,pool_fun173]

pl_code local fun179
	call_c   Dyam_Update_Choice(fun178)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Choice(fun176)
	pl_call  Object_1(&ref[503])
	pl_jump  fun173()

;; TERM 492: light_tabular _I / _H
c_code local build_ref_492
	ret_reg &ref[492]
	call_c   build_ref_633()
	call_c   build_ref_462()
	call_c   Dyam_Create_Unary(&ref[633],&ref[462])
	move_ret ref[492]
	c_ret

;; TERM 502: '$CLOSURE'('$fun'(172, 0, 1143671820), '$TUPPLE'(35085872800704))
c_code local build_ref_502
	ret_reg &ref[502]
	call_c   build_ref_473()
	call_c   Dyam_Closure_Aux(fun172,&ref[473])
	move_ret ref[502]
	c_ret

c_code local build_seed_101
	ret_reg &seed[101]
	call_c   build_ref_34()
	call_c   build_ref_499()
	call_c   Dyam_Seed_Start(&ref[34],&ref[499],I(0),fun1,1)
	call_c   build_ref_500()
	call_c   Dyam_Seed_Add_Comp(&ref[500],&ref[499],0)
	call_c   Dyam_Seed_End()
	move_ret seed[101]
	c_ret

;; TERM 500: '*GUARD*'(body_to_lpda(_E, _C, '*LIGHTLAST*'(_O), _K, rec_prolog)) :> '$$HOLE$$'
c_code local build_ref_500
	ret_reg &ref[500]
	call_c   build_ref_499()
	call_c   Dyam_Create_Binary(I(9),&ref[499],I(7))
	move_ret ref[500]
	c_ret

;; TERM 499: '*GUARD*'(body_to_lpda(_E, _C, '*LIGHTLAST*'(_O), _K, rec_prolog))
c_code local build_ref_499
	ret_reg &ref[499]
	call_c   build_ref_34()
	call_c   build_ref_498()
	call_c   Dyam_Create_Unary(&ref[34],&ref[498])
	move_ret ref[499]
	c_ret

;; TERM 498: body_to_lpda(_E, _C, '*LIGHTLAST*'(_O), _K, rec_prolog)
c_code local build_ref_498
	ret_reg &ref[498]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_667()
	call_c   Dyam_Create_Unary(&ref[667],V(14))
	move_ret R(0)
	call_c   build_ref_601()
	call_c   build_ref_544()
	call_c   Dyam_Term_Start(&ref[601],5)
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(&ref[544])
	call_c   Dyam_Term_End()
	move_ret ref[498]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun172[4]=[3,build_seed_100,build_ref_497,build_seed_101]

pl_code local fun172
	call_c   Dyam_Pool(pool_fun172)
	call_c   Dyam_Allocate(0)
	pl_call  fun15(&seed[100],1)
	call_c   Dyam_Unify(V(3),&ref[497])
	fail_ret
	pl_call  fun15(&seed[101],1)
	pl_jump  fun65()

long local pool_fun180[5]=[65539,build_ref_492,build_ref_502,build_ref_462,pool_fun179]

pl_code local fun180
	call_c   Dyam_Update_Choice(fun179)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[492])
	call_c   Dyam_Cut()
	move     &ref[502], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	move     &ref[462], R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  fun152()

;; TERM 483: std_prolog _I / _H
c_code local build_ref_483
	ret_reg &ref[483]
	call_c   build_ref_634()
	call_c   build_ref_462()
	call_c   Dyam_Create_Unary(&ref[634],&ref[462])
	move_ret ref[483]
	c_ret

;; TERM 491: '$CLOSURE'('$fun'(171, 0, 1143649628), '$TUPPLE'(35085872800736))
c_code local build_ref_491
	ret_reg &ref[491]
	call_c   build_ref_490()
	call_c   Dyam_Closure_Aux(fun171,&ref[490])
	move_ret ref[491]
	c_ret

;; TERM 484: [_L|_M]
c_code local build_ref_484
	ret_reg &ref[484]
	call_c   Dyam_Create_List(V(11),V(12))
	move_ret ref[484]
	c_ret

;; TERM 485: '*STD-PROLOG-FIRST*'((_I / _H), _M, _K)
c_code local build_ref_485
	ret_reg &ref[485]
	call_c   build_ref_668()
	call_c   build_ref_462()
	call_c   Dyam_Term_Start(&ref[668],3)
	call_c   Dyam_Term_Arg(&ref[462])
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_End()
	move_ret ref[485]
	c_ret

c_code local build_seed_99
	ret_reg &seed[99]
	call_c   build_ref_34()
	call_c   build_ref_487()
	call_c   Dyam_Seed_Start(&ref[34],&ref[487],I(0),fun1,1)
	call_c   build_ref_488()
	call_c   Dyam_Seed_Add_Comp(&ref[488],&ref[487],0)
	call_c   Dyam_Seed_End()
	move_ret seed[99]
	c_ret

;; TERM 488: '*GUARD*'(body_to_lpda(_E, _C, deallocate_layer, _K, rec_prolog)) :> '$$HOLE$$'
c_code local build_ref_488
	ret_reg &ref[488]
	call_c   build_ref_487()
	call_c   Dyam_Create_Binary(I(9),&ref[487],I(7))
	move_ret ref[488]
	c_ret

;; TERM 487: '*GUARD*'(body_to_lpda(_E, _C, deallocate_layer, _K, rec_prolog))
c_code local build_ref_487
	ret_reg &ref[487]
	call_c   build_ref_34()
	call_c   build_ref_486()
	call_c   Dyam_Create_Unary(&ref[34],&ref[486])
	move_ret ref[487]
	c_ret

;; TERM 486: body_to_lpda(_E, _C, deallocate_layer, _K, rec_prolog)
c_code local build_ref_486
	ret_reg &ref[486]
	call_c   build_ref_601()
	call_c   build_ref_573()
	call_c   build_ref_544()
	call_c   Dyam_Term_Start(&ref[601],5)
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(&ref[573])
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(&ref[544])
	call_c   Dyam_Term_End()
	move_ret ref[486]
	c_ret

long local pool_fun171[4]=[3,build_ref_484,build_ref_485,build_seed_99]

pl_code local fun171
	call_c   Dyam_Pool(pool_fun171)
	call_c   DYAM_evpred_univ(V(9),&ref[484])
	fail_ret
	call_c   Dyam_Unify(V(3),&ref[485])
	fail_ret
	call_c   Dyam_Allocate(0)
	pl_call  fun15(&seed[99],1)
	pl_jump  fun65()

;; TERM 490: '$TUPPLE'(35085872800736)
c_code local build_ref_490
	ret_reg &ref[490]
	call_c   Dyam_Create_Simple_Tupple(0,121110528)
	move_ret ref[490]
	c_ret

long local pool_fun181[5]=[65539,build_ref_483,build_ref_491,build_ref_462,pool_fun180]

pl_code local fun181
	call_c   Dyam_Update_Choice(fun180)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[483])
	call_c   Dyam_Cut()
	move     &ref[491], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	move     &ref[462], R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  fun152()

;; TERM 475: prolog _I / _H
c_code local build_ref_475
	ret_reg &ref[475]
	call_c   build_ref_579()
	call_c   build_ref_462()
	call_c   Dyam_Create_Unary(&ref[579],&ref[462])
	move_ret ref[475]
	c_ret

;; TERM 482: '$CLOSURE'('$fun'(170, 0, 1143636748), '$TUPPLE'(35085872800704))
c_code local build_ref_482
	ret_reg &ref[482]
	call_c   build_ref_473()
	call_c   Dyam_Closure_Aux(fun170,&ref[473])
	move_ret ref[482]
	c_ret

;; TERM 477: '*PROLOG-FIRST*'(_J) :> _K
c_code local build_ref_477
	ret_reg &ref[477]
	call_c   build_ref_476()
	call_c   Dyam_Create_Binary(I(9),&ref[476],V(10))
	move_ret ref[477]
	c_ret

;; TERM 476: '*PROLOG-FIRST*'(_J)
c_code local build_ref_476
	ret_reg &ref[476]
	call_c   build_ref_424()
	call_c   Dyam_Create_Unary(&ref[424],V(9))
	move_ret ref[476]
	c_ret

c_code local build_seed_98
	ret_reg &seed[98]
	call_c   build_ref_34()
	call_c   build_ref_479()
	call_c   Dyam_Seed_Start(&ref[34],&ref[479],I(0),fun1,1)
	call_c   build_ref_480()
	call_c   Dyam_Seed_Add_Comp(&ref[480],&ref[479],0)
	call_c   Dyam_Seed_End()
	move_ret seed[98]
	c_ret

;; TERM 480: '*GUARD*'(body_to_lpda(_E, _C, '*PROLOG-LAST*', _K, prolog)) :> '$$HOLE$$'
c_code local build_ref_480
	ret_reg &ref[480]
	call_c   build_ref_479()
	call_c   Dyam_Create_Binary(I(9),&ref[479],I(7))
	move_ret ref[480]
	c_ret

;; TERM 479: '*GUARD*'(body_to_lpda(_E, _C, '*PROLOG-LAST*', _K, prolog))
c_code local build_ref_479
	ret_reg &ref[479]
	call_c   build_ref_34()
	call_c   build_ref_478()
	call_c   Dyam_Create_Unary(&ref[34],&ref[478])
	move_ret ref[479]
	c_ret

;; TERM 478: body_to_lpda(_E, _C, '*PROLOG-LAST*', _K, prolog)
c_code local build_ref_478
	ret_reg &ref[478]
	call_c   build_ref_601()
	call_c   build_ref_426()
	call_c   build_ref_579()
	call_c   Dyam_Term_Start(&ref[601],5)
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(&ref[426])
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(&ref[579])
	call_c   Dyam_Term_End()
	move_ret ref[478]
	c_ret

long local pool_fun170[3]=[2,build_ref_477,build_seed_98]

pl_code local fun170
	call_c   Dyam_Pool(pool_fun170)
	call_c   Dyam_Unify(V(3),&ref[477])
	fail_ret
	call_c   Dyam_Allocate(0)
	pl_call  fun15(&seed[98],1)
	pl_jump  fun65()

long local pool_fun182[5]=[65539,build_ref_475,build_ref_482,build_ref_462,pool_fun181]

pl_code local fun182
	call_c   Dyam_Update_Choice(fun181)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[475])
	call_c   Dyam_Cut()
	move     &ref[482], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	move     &ref[462], R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  fun152()

;; TERM 466: rec_prolog _I / _H
c_code local build_ref_466
	ret_reg &ref[466]
	call_c   build_ref_544()
	call_c   build_ref_462()
	call_c   Dyam_Create_Unary(&ref[544],&ref[462])
	move_ret ref[466]
	c_ret

;; TERM 474: '$CLOSURE'('$fun'(169, 0, 1143619280), '$TUPPLE'(35085872800704))
c_code local build_ref_474
	ret_reg &ref[474]
	call_c   build_ref_473()
	call_c   Dyam_Closure_Aux(fun169,&ref[473])
	move_ret ref[474]
	c_ret

;; TERM 468: '*GUARD*'(_J) :> _K
c_code local build_ref_468
	ret_reg &ref[468]
	call_c   build_ref_467()
	call_c   Dyam_Create_Binary(I(9),&ref[467],V(10))
	move_ret ref[468]
	c_ret

;; TERM 467: '*GUARD*'(_J)
c_code local build_ref_467
	ret_reg &ref[467]
	call_c   build_ref_34()
	call_c   Dyam_Create_Unary(&ref[34],V(9))
	move_ret ref[467]
	c_ret

c_code local build_seed_97
	ret_reg &seed[97]
	call_c   build_ref_34()
	call_c   build_ref_470()
	call_c   Dyam_Seed_Start(&ref[34],&ref[470],I(0),fun1,1)
	call_c   build_ref_471()
	call_c   Dyam_Seed_Add_Comp(&ref[471],&ref[470],0)
	call_c   Dyam_Seed_End()
	move_ret seed[97]
	c_ret

;; TERM 471: '*GUARD*'(body_to_lpda(_E, _C, noop, _K, rec_prolog)) :> '$$HOLE$$'
c_code local build_ref_471
	ret_reg &ref[471]
	call_c   build_ref_470()
	call_c   Dyam_Create_Binary(I(9),&ref[470],I(7))
	move_ret ref[471]
	c_ret

;; TERM 470: '*GUARD*'(body_to_lpda(_E, _C, noop, _K, rec_prolog))
c_code local build_ref_470
	ret_reg &ref[470]
	call_c   build_ref_34()
	call_c   build_ref_469()
	call_c   Dyam_Create_Unary(&ref[34],&ref[469])
	move_ret ref[470]
	c_ret

;; TERM 469: body_to_lpda(_E, _C, noop, _K, rec_prolog)
c_code local build_ref_469
	ret_reg &ref[469]
	call_c   build_ref_601()
	call_c   build_ref_322()
	call_c   build_ref_544()
	call_c   Dyam_Term_Start(&ref[601],5)
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(&ref[322])
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(&ref[544])
	call_c   Dyam_Term_End()
	move_ret ref[469]
	c_ret

long local pool_fun169[3]=[2,build_ref_468,build_seed_97]

pl_code local fun169
	call_c   Dyam_Pool(pool_fun169)
	call_c   Dyam_Unify(V(3),&ref[468])
	fail_ret
	call_c   Dyam_Allocate(0)
	pl_call  fun15(&seed[97],1)
	pl_jump  fun65()

long local pool_fun183[5]=[65539,build_ref_466,build_ref_474,build_ref_462,pool_fun182]

pl_code local fun183
	call_c   Dyam_Update_Choice(fun182)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[466])
	call_c   Dyam_Cut()
	move     &ref[474], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	move     &ref[462], R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  fun152()

;; TERM 463: extensional _I / _H
c_code local build_ref_463
	ret_reg &ref[463]
	call_c   build_ref_632()
	call_c   build_ref_462()
	call_c   Dyam_Create_Unary(&ref[632],&ref[462])
	move_ret ref[463]
	c_ret

;; TERM 464: '~w declared as extensional but defined by clause ~w'
c_code local build_ref_464
	ret_reg &ref[464]
	call_c   Dyam_Create_Atom("~w declared as extensional but defined by clause ~w")
	move_ret ref[464]
	c_ret

;; TERM 465: [_I / _H,(_J :- _C)]
c_code local build_ref_465
	ret_reg &ref[465]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_592()
	call_c   Dyam_Create_Binary(&ref[592],V(9),V(2))
	move_ret R(0)
	call_c   Dyam_Create_List(R(0),I(0))
	move_ret R(0)
	call_c   build_ref_462()
	call_c   Dyam_Create_List(&ref[462],R(0))
	move_ret ref[465]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun184[9]=[65543,build_ref_70,build_ref_460,build_ref_461,build_ref_462,build_ref_463,build_ref_464,build_ref_465,pool_fun183]

pl_code local fun184
	call_c   Dyam_Pool(pool_fun184)
	call_c   Dyam_Unify_Item(&ref[70])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[460], R(0)
	move     S(5), R(1)
	move     V(4), R(2)
	move     S(5), R(3)
	move     V(5), R(4)
	move     S(5), R(5)
	pl_call  pred_my_numbervars_3()
	call_c   DYAM_evpred_functor(V(1),V(6),V(7))
	fail_ret
	move     &ref[461], R(0)
	move     S(5), R(1)
	move     &ref[462], R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Load(4,V(1))
	move     V(9), R(6)
	move     S(5), R(7)
	pl_call  pred_term_current_module_shift_4()
	call_c   Dyam_Choice(fun183)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[463])
	call_c   Dyam_Cut()
	move     &ref[464], R(0)
	move     0, R(1)
	move     &ref[465], R(2)
	move     S(5), R(3)
	pl_call  pred_error_2()
	pl_jump  fun65()

;; TERM 459: '*GUARD*'(clause_to_lpda((_B :- _C), _D)) :> []
c_code local build_ref_459
	ret_reg &ref[459]
	call_c   build_ref_70()
	call_c   Dyam_Create_Binary(I(9),&ref[70],I(0))
	move_ret ref[459]
	c_ret

c_code local build_seed_16
	ret_reg &seed[16]
	call_c   build_ref_33()
	call_c   build_ref_516()
	call_c   Dyam_Seed_Start(&ref[33],&ref[516],I(0),fun0,1)
	call_c   build_ref_515()
	call_c   Dyam_Seed_Add_Comp(&ref[515],fun210,0)
	call_c   Dyam_Seed_End()
	move_ret seed[16]
	c_ret

;; TERM 515: '*GUARD*'(litteral_to_lpda(_B, _C, _D, _E, _F))
c_code local build_ref_515
	ret_reg &ref[515]
	call_c   build_ref_34()
	call_c   build_ref_514()
	call_c   Dyam_Create_Unary(&ref[34],&ref[514])
	move_ret ref[515]
	c_ret

;; TERM 514: litteral_to_lpda(_B, _C, _D, _E, _F)
c_code local build_ref_514
	ret_reg &ref[514]
	call_c   build_ref_636()
	call_c   Dyam_Term_Start(&ref[636],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[514]
	c_ret

;; TERM 517: _I / _J
c_code local build_ref_517
	ret_reg &ref[517]
	call_c   build_ref_627()
	call_c   Dyam_Create_Binary(&ref[627],V(8),V(9))
	move_ret ref[517]
	c_ret

;; TERM 518: _L / _J
c_code local build_ref_518
	ret_reg &ref[518]
	call_c   build_ref_627()
	call_c   Dyam_Create_Binary(&ref[627],V(11),V(9))
	move_ret ref[518]
	c_ret

c_code local build_seed_103
	ret_reg &seed[103]
	call_c   build_ref_34()
	call_c   build_ref_530()
	call_c   Dyam_Seed_Start(&ref[34],&ref[530],I(0),fun1,1)
	call_c   build_ref_531()
	call_c   Dyam_Seed_Add_Comp(&ref[531],&ref[530],0)
	call_c   Dyam_Seed_End()
	move_ret seed[103]
	c_ret

;; TERM 531: '*GUARD*'(make_callret(_M, _P, _Q)) :> '$$HOLE$$'
c_code local build_ref_531
	ret_reg &ref[531]
	call_c   build_ref_530()
	call_c   Dyam_Create_Binary(I(9),&ref[530],I(7))
	move_ret ref[531]
	c_ret

;; TERM 530: '*GUARD*'(make_callret(_M, _P, _Q))
c_code local build_ref_530
	ret_reg &ref[530]
	call_c   build_ref_34()
	call_c   build_ref_529()
	call_c   Dyam_Create_Unary(&ref[34],&ref[529])
	move_ret ref[530]
	c_ret

;; TERM 529: make_callret(_M, _P, _Q)
c_code local build_ref_529
	ret_reg &ref[529]
	call_c   build_ref_651()
	call_c   Dyam_Term_Start(&ref[651],3)
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_End()
	move_ret ref[529]
	c_ret

;; TERM 570: '$CLOSURE'('$fun'(199, 0, 1143955296), '$TUPPLE'(35085872801472))
c_code local build_ref_570
	ret_reg &ref[570]
	call_c   build_ref_569()
	call_c   Dyam_Closure_Aux(fun199,&ref[569])
	move_ret ref[570]
	c_ret

;; TERM 567: '$CLOSURE'('$fun'(196, 0, 1143937468), '$TUPPLE'(35085872536560))
c_code local build_ref_567
	ret_reg &ref[567]
	call_c   build_ref_566()
	call_c   Dyam_Closure_Aux(fun196,&ref[566])
	move_ret ref[567]
	c_ret

;; TERM 564: '*WRAPPER-CALL*'(_V, _U, _D)
c_code local build_ref_564
	ret_reg &ref[564]
	call_c   build_ref_669()
	call_c   Dyam_Term_Start(&ref[669],3)
	call_c   Dyam_Term_Arg(V(21))
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[564]
	c_ret

;; TERM 669: '*WRAPPER-CALL*'
c_code local build_ref_669
	ret_reg &ref[669]
	call_c   Dyam_Create_Atom("*WRAPPER-CALL*")
	move_ret ref[669]
	c_ret

long local pool_fun196[2]=[1,build_ref_564]

pl_code local fun196
	call_c   Dyam_Pool(pool_fun196)
	call_c   Dyam_Unify(V(4),&ref[564])
	fail_ret
	call_c   Dyam_Allocate(0)
	pl_jump  fun65()

;; TERM 566: '$TUPPLE'(35085872536560)
c_code local build_ref_566
	ret_reg &ref[566]
	call_c   Dyam_Create_Simple_Tupple(0,50332032)
	move_ret ref[566]
	c_ret

long local pool_fun197[2]=[1,build_ref_567]

long local pool_fun198[3]=[65537,build_ref_518,pool_fun197]

pl_code local fun198
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(21),&ref[518])
	fail_ret
fun197:
	move     &ref[567], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Load(4,V(21))
	call_c   Dyam_Reg_Deallocate(3)
fun195:
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Reg_Bind(2,V(3))
	call_c   Dyam_Reg_Bind(4,V(2))
	call_c   Dyam_Choice(fun194)
	pl_call  fun74(&seed[106],1)
	pl_fail



;; TERM 556: lc _L / _J
c_code local build_ref_556
	ret_reg &ref[556]
	call_c   build_ref_650()
	call_c   build_ref_518()
	call_c   Dyam_Create_Unary(&ref[650],&ref[518])
	move_ret ref[556]
	c_ret

long local pool_fun199[5]=[131074,build_ref_518,build_ref_556,pool_fun198,pool_fun197]

pl_code local fun199
	call_c   Dyam_Pool(pool_fun199)
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun198)
	call_c   Dyam_Set_Cut()
	move     &ref[518], R(0)
	move     S(5), R(1)
	pl_call  pred_check_lc_1()
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(21),&ref[556])
	fail_ret
	call_c   Dyam_Reg_Load(0,V(21))
	move     V(22), R(2)
	move     S(5), R(3)
	pl_call  pred_update_counter_2()
	pl_jump  fun197()

;; TERM 569: '$TUPPLE'(35085872801472)
c_code local build_ref_569
	ret_reg &ref[569]
	call_c   Dyam_Create_Simple_Tupple(0,50987264)
	move_ret ref[569]
	c_ret

;; TERM 571: [_M,_G]
c_code local build_ref_571
	ret_reg &ref[571]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(6),I(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(12),R(0))
	move_ret ref[571]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun200[4]=[3,build_ref_570,build_ref_518,build_ref_571]

pl_code local fun200
	call_c   Dyam_Remove_Choice()
	move     &ref[570], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	move     &ref[518], R(4)
	move     S(5), R(5)
	move     &ref[571], R(6)
	move     S(5), R(7)
	move     V(20), R(8)
	move     S(5), R(9)
	call_c   Dyam_Reg_Deallocate(5)
	pl_jump  fun115()

long local pool_fun201[4]=[65538,build_seed_103,build_ref_518,pool_fun200]

pl_code local fun201
	call_c   Dyam_Remove_Choice()
	move     &ref[518], R(0)
	move     S(5), R(1)
	pl_call  pred_registered_predicate_1()
	pl_call  fun15(&seed[103],1)
	call_c   Dyam_Choice(fun200)
	call_c   Dyam_Set_Cut()
	move     &ref[518], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(6))
	call_c   Dyam_Reg_Load(4,V(15))
	call_c   Dyam_Reg_Load(6,V(16))
	call_c   Dyam_Reg_Load(8,V(3))
	call_c   Dyam_Reg_Load(10,V(4))
	pl_call  pred_try_lco_6()
	call_c   Dyam_Cut()
	pl_jump  fun65()

;; TERM 546: 'Not a recursive prolog predicate ~w'
c_code local build_ref_546
	ret_reg &ref[546]
	call_c   Dyam_Create_Atom("Not a recursive prolog predicate ~w")
	move_ret ref[546]
	c_ret

;; TERM 547: [_M]
c_code local build_ref_547
	ret_reg &ref[547]
	call_c   Dyam_Create_List(V(12),I(0))
	move_ret ref[547]
	c_ret

long local pool_fun191[3]=[2,build_ref_546,build_ref_547]

pl_code local fun192
	call_c   Dyam_Remove_Choice()
fun191:
	call_c   Dyam_Cut()
	move     &ref[546], R(0)
	move     0, R(1)
	move     &ref[547], R(2)
	move     S(5), R(3)
	pl_call  pred_error_2()
	pl_jump  fun65()


;; TERM 532: light_tabular _L / _J
c_code local build_ref_532
	ret_reg &ref[532]
	call_c   build_ref_633()
	call_c   build_ref_518()
	call_c   Dyam_Create_Unary(&ref[633],&ref[518])
	move_ret ref[532]
	c_ret

long local pool_fun202[5]=[131074,build_ref_544,build_ref_532,pool_fun201,pool_fun191]

pl_code local fun202
	call_c   Dyam_Update_Choice(fun201)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(5),&ref[544])
	fail_ret
	call_c   Dyam_Choice(fun192)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[532])
	call_c   Dyam_Cut()
	pl_fail

;; TERM 552: extensional _L / _J
c_code local build_ref_552
	ret_reg &ref[552]
	call_c   build_ref_632()
	call_c   build_ref_518()
	call_c   Dyam_Create_Unary(&ref[632],&ref[518])
	move_ret ref[552]
	c_ret

c_code local build_seed_105
	ret_reg &seed[105]
	call_c   build_ref_34()
	call_c   build_ref_554()
	call_c   Dyam_Seed_Start(&ref[34],&ref[554],I(0),fun1,1)
	call_c   build_ref_555()
	call_c   Dyam_Seed_Add_Comp(&ref[555],&ref[554],0)
	call_c   Dyam_Seed_End()
	move_ret seed[105]
	c_ret

;; TERM 555: '*GUARD*'(litteral_to_lpda(_B, recorded(_M), _D, _E, _F)) :> '$$HOLE$$'
c_code local build_ref_555
	ret_reg &ref[555]
	call_c   build_ref_554()
	call_c   Dyam_Create_Binary(I(9),&ref[554],I(7))
	move_ret ref[555]
	c_ret

;; TERM 554: '*GUARD*'(litteral_to_lpda(_B, recorded(_M), _D, _E, _F))
c_code local build_ref_554
	ret_reg &ref[554]
	call_c   build_ref_34()
	call_c   build_ref_553()
	call_c   Dyam_Create_Unary(&ref[34],&ref[553])
	move_ret ref[554]
	c_ret

;; TERM 553: litteral_to_lpda(_B, recorded(_M), _D, _E, _F)
c_code local build_ref_553
	ret_reg &ref[553]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_670()
	call_c   Dyam_Create_Unary(&ref[670],V(12))
	move_ret R(0)
	call_c   build_ref_636()
	call_c   Dyam_Term_Start(&ref[636],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[553]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 670: recorded
c_code local build_ref_670
	ret_reg &ref[670]
	call_c   Dyam_Create_Atom("recorded")
	move_ret ref[670]
	c_ret

long local pool_fun203[5]=[65539,build_ref_552,build_ref_518,build_seed_105,pool_fun202]

pl_code local fun203
	call_c   Dyam_Update_Choice(fun202)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[552])
	call_c   Dyam_Cut()
	move     &ref[518], R(0)
	move     S(5), R(1)
	pl_call  pred_registered_predicate_1()
	pl_call  fun15(&seed[105],1)
	pl_jump  fun65()

;; TERM 548: std_prolog _L / _J
c_code local build_ref_548
	ret_reg &ref[548]
	call_c   build_ref_634()
	call_c   build_ref_518()
	call_c   Dyam_Create_Unary(&ref[634],&ref[518])
	move_ret ref[548]
	c_ret

;; TERM 549: [_T|_U]
c_code local build_ref_549
	ret_reg &ref[549]
	call_c   Dyam_Create_List(V(19),V(20))
	move_ret ref[549]
	c_ret

;; TERM 551: '*STD-PROLOG-CALL*'((_L / _J), _U) :> _D
c_code local build_ref_551
	ret_reg &ref[551]
	call_c   build_ref_550()
	call_c   Dyam_Create_Binary(I(9),&ref[550],V(3))
	move_ret ref[551]
	c_ret

;; TERM 550: '*STD-PROLOG-CALL*'((_L / _J), _U)
c_code local build_ref_550
	ret_reg &ref[550]
	call_c   build_ref_671()
	call_c   build_ref_518()
	call_c   Dyam_Create_Binary(&ref[671],&ref[518],V(20))
	move_ret ref[550]
	c_ret

;; TERM 671: '*STD-PROLOG-CALL*'
c_code local build_ref_671
	ret_reg &ref[671]
	call_c   Dyam_Create_Atom("*STD-PROLOG-CALL*")
	move_ret ref[671]
	c_ret

long local pool_fun204[6]=[65540,build_ref_548,build_ref_518,build_ref_549,build_ref_551,pool_fun203]

pl_code local fun204
	call_c   Dyam_Update_Choice(fun203)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[548])
	call_c   Dyam_Cut()
	move     &ref[518], R(0)
	move     S(5), R(1)
	pl_call  pred_registered_predicate_1()
	call_c   DYAM_evpred_univ(V(12),&ref[549])
	fail_ret
	call_c   Dyam_Unify(V(4),&ref[551])
	fail_ret
	pl_jump  fun65()

;; TERM 543: prolog _L / _J
c_code local build_ref_543
	ret_reg &ref[543]
	call_c   build_ref_579()
	call_c   build_ref_518()
	call_c   Dyam_Create_Unary(&ref[579],&ref[518])
	move_ret ref[543]
	c_ret

;; TERM 545: '*PROLOG*'{call=> _M, right=> _D}
c_code local build_ref_545
	ret_reg &ref[545]
	call_c   build_ref_672()
	call_c   Dyam_Create_Binary(&ref[672],V(12),V(3))
	move_ret ref[545]
	c_ret

;; TERM 672: '*PROLOG*'!'$ft'
c_code local build_ref_672
	ret_reg &ref[672]
	call_c   build_ref_673()
	call_c   Dyam_Create_Atom_Module("$ft",&ref[673])
	move_ret ref[672]
	c_ret

;; TERM 673: '*PROLOG*'
c_code local build_ref_673
	ret_reg &ref[673]
	call_c   Dyam_Create_Atom("*PROLOG*")
	move_ret ref[673]
	c_ret

long local pool_fun187[2]=[1,build_ref_545]

long local pool_fun190[4]=[65538,build_ref_546,build_ref_547,pool_fun187]

pl_code local fun190
	call_c   Dyam_Remove_Choice()
	move     &ref[546], R(0)
	move     0, R(1)
	move     &ref[547], R(2)
	move     S(5), R(3)
	pl_call  pred_error_2()
fun187:
	call_c   Dyam_Unify(V(4),&ref[545])
	fail_ret
	pl_jump  fun65()


pl_code local fun189
	call_c   Dyam_Remove_Choice()
fun188:
	call_c   Dyam_Cut()
	pl_jump  fun187()


long local pool_fun205[7]=[196611,build_ref_543,build_ref_518,build_ref_544,pool_fun204,pool_fun190,pool_fun187]

pl_code local fun205
	call_c   Dyam_Update_Choice(fun204)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[543])
	call_c   Dyam_Cut()
	move     &ref[518], R(0)
	move     S(5), R(1)
	pl_call  pred_registered_predicate_1()
	call_c   Dyam_Choice(fun190)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Choice(fun189)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(5),&ref[544])
	fail_ret
	call_c   Dyam_Cut()
	pl_fail

;; TERM 540: rec_prolog _L / _J
c_code local build_ref_540
	ret_reg &ref[540]
	call_c   build_ref_544()
	call_c   build_ref_518()
	call_c   Dyam_Create_Unary(&ref[544],&ref[518])
	move_ret ref[540]
	c_ret

;; TERM 542: '*GUARD*'(_M) :> _D
c_code local build_ref_542
	ret_reg &ref[542]
	call_c   build_ref_541()
	call_c   Dyam_Create_Binary(I(9),&ref[541],V(3))
	move_ret ref[542]
	c_ret

;; TERM 541: '*GUARD*'(_M)
c_code local build_ref_541
	ret_reg &ref[541]
	call_c   build_ref_34()
	call_c   Dyam_Create_Unary(&ref[34],V(12))
	move_ret ref[541]
	c_ret

long local pool_fun206[5]=[65539,build_ref_540,build_ref_518,build_ref_542,pool_fun205]

pl_code local fun206
	call_c   Dyam_Update_Choice(fun205)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[540])
	call_c   Dyam_Cut()
	move     &ref[518], R(0)
	move     S(5), R(1)
	pl_call  pred_registered_predicate_1()
	call_c   Dyam_Unify(V(4),&ref[542])
	fail_ret
	pl_jump  fun65()

;; TERM 536: compiler_extension((_L / _J), _R)
c_code local build_ref_536
	ret_reg &ref[536]
	call_c   build_ref_644()
	call_c   build_ref_518()
	call_c   Dyam_Create_Binary(&ref[644],&ref[518],V(17))
	move_ret ref[536]
	c_ret

c_code local build_seed_104
	ret_reg &seed[104]
	call_c   build_ref_34()
	call_c   build_ref_538()
	call_c   Dyam_Seed_Start(&ref[34],&ref[538],I(0),fun1,1)
	call_c   build_ref_539()
	call_c   Dyam_Seed_Add_Comp(&ref[539],&ref[538],0)
	call_c   Dyam_Seed_End()
	move_ret seed[104]
	c_ret

;; TERM 539: '*GUARD*'(body_to_lpda(_B, _S, _D, _E, _F)) :> '$$HOLE$$'
c_code local build_ref_539
	ret_reg &ref[539]
	call_c   build_ref_538()
	call_c   Dyam_Create_Binary(I(9),&ref[538],I(7))
	move_ret ref[539]
	c_ret

;; TERM 538: '*GUARD*'(body_to_lpda(_B, _S, _D, _E, _F))
c_code local build_ref_538
	ret_reg &ref[538]
	call_c   build_ref_34()
	call_c   build_ref_537()
	call_c   Dyam_Create_Unary(&ref[34],&ref[537])
	move_ret ref[538]
	c_ret

;; TERM 537: body_to_lpda(_B, _S, _D, _E, _F)
c_code local build_ref_537
	ret_reg &ref[537]
	call_c   build_ref_601()
	call_c   Dyam_Term_Start(&ref[601],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[537]
	c_ret

long local pool_fun207[4]=[65538,build_ref_536,build_seed_104,pool_fun206]

pl_code local fun207
	call_c   Dyam_Update_Choice(fun206)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[536])
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(12))
	call_c   Dyam_Reg_Load(2,V(17))
	move     V(18), R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Load(6,V(1))
	pl_call  pred_try_litteral_expansion_4()
	pl_call  fun15(&seed[104],1)
	pl_jump  fun65()

;; TERM 528: answer(_O)
c_code local build_ref_528
	ret_reg &ref[528]
	call_c   build_ref_602()
	call_c   Dyam_Create_Unary(&ref[602],V(14))
	move_ret ref[528]
	c_ret

;; TERM 535: '*PSEUDONEXT*'(_P, _Q, _D, _G)
c_code local build_ref_535
	ret_reg &ref[535]
	call_c   build_ref_674()
	call_c   Dyam_Term_Start(&ref[674],4)
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[535]
	c_ret

;; TERM 674: '*PSEUDONEXT*'
c_code local build_ref_674
	ret_reg &ref[674]
	call_c   Dyam_Create_Atom("*PSEUDONEXT*")
	move_ret ref[674]
	c_ret

long local pool_fun186[2]=[1,build_ref_535]

pl_code local fun186
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(4),&ref[535])
	fail_ret
	pl_jump  fun65()

;; TERM 534: '*PSEUDOLIGHTNEXT*'(_P, _Q, (deallocate :> succeed), _G) :> _D
c_code local build_ref_534
	ret_reg &ref[534]
	call_c   build_ref_533()
	call_c   Dyam_Create_Binary(I(9),&ref[533],V(3))
	move_ret ref[534]
	c_ret

;; TERM 533: '*PSEUDOLIGHTNEXT*'(_P, _Q, (deallocate :> succeed), _G)
c_code local build_ref_533
	ret_reg &ref[533]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_676()
	call_c   build_ref_677()
	call_c   Dyam_Create_Binary(I(9),&ref[676],&ref[677])
	move_ret R(0)
	call_c   build_ref_675()
	call_c   Dyam_Term_Start(&ref[675],4)
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[533]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 675: '*PSEUDOLIGHTNEXT*'
c_code local build_ref_675
	ret_reg &ref[675]
	call_c   Dyam_Create_Atom("*PSEUDOLIGHTNEXT*")
	move_ret ref[675]
	c_ret

;; TERM 677: succeed
c_code local build_ref_677
	ret_reg &ref[677]
	call_c   Dyam_Create_Atom("succeed")
	move_ret ref[677]
	c_ret

;; TERM 676: deallocate
c_code local build_ref_676
	ret_reg &ref[676]
	call_c   Dyam_Create_Atom("deallocate")
	move_ret ref[676]
	c_ret

long local pool_fun208[8]=[131077,build_ref_528,build_ref_518,build_seed_103,build_ref_532,build_ref_534,pool_fun207,pool_fun186]

pl_code local fun208
	call_c   Dyam_Update_Choice(fun207)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(5),&ref[528])
	fail_ret
	call_c   Dyam_Cut()
	move     &ref[518], R(0)
	move     S(5), R(1)
	pl_call  pred_registered_predicate_1()
	pl_call  fun15(&seed[103],1)
	call_c   Dyam_Choice(fun186)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[532])
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(4),&ref[534])
	fail_ret
	pl_jump  fun65()

;; TERM 525: builtin((_L / _J), _N)
c_code local build_ref_525
	ret_reg &ref[525]
	call_c   build_ref_646()
	call_c   build_ref_518()
	call_c   Dyam_Create_Binary(&ref[646],&ref[518],V(13))
	move_ret ref[525]
	c_ret

;; TERM 527: builtin(_M, _N) :> _D
c_code local build_ref_527
	ret_reg &ref[527]
	call_c   build_ref_526()
	call_c   Dyam_Create_Binary(I(9),&ref[526],V(3))
	move_ret ref[527]
	c_ret

;; TERM 526: builtin(_M, _N)
c_code local build_ref_526
	ret_reg &ref[526]
	call_c   build_ref_646()
	call_c   Dyam_Create_Binary(&ref[646],V(12),V(13))
	move_ret ref[526]
	c_ret

long local pool_fun209[4]=[65538,build_ref_525,build_ref_527,pool_fun208]

pl_code local fun209
	call_c   Dyam_Update_Choice(fun208)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[525])
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(4),&ref[527])
	fail_ret
	pl_jump  fun65()

;; TERM 519: foreign((_L / _J), _N)
c_code local build_ref_519
	ret_reg &ref[519]
	call_c   build_ref_630()
	call_c   build_ref_518()
	call_c   Dyam_Create_Binary(&ref[630],&ref[518],V(13))
	move_ret ref[519]
	c_ret

;; TERM 524: foreign(_M, _N) :> _D
c_code local build_ref_524
	ret_reg &ref[524]
	call_c   build_ref_523()
	call_c   Dyam_Create_Binary(I(9),&ref[523],V(3))
	move_ret ref[524]
	c_ret

;; TERM 523: foreign(_M, _N)
c_code local build_ref_523
	ret_reg &ref[523]
	call_c   build_ref_630()
	call_c   Dyam_Create_Binary(&ref[630],V(12),V(13))
	move_ret ref[523]
	c_ret

long local pool_fun185[2]=[1,build_ref_524]

pl_code local fun185
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(4),&ref[524])
	fail_ret
	pl_jump  fun65()

;; TERM 520: foreign_void_return((_L / _J))
c_code local build_ref_520
	ret_reg &ref[520]
	call_c   build_ref_678()
	call_c   build_ref_518()
	call_c   Dyam_Create_Unary(&ref[678],&ref[518])
	move_ret ref[520]
	c_ret

;; TERM 678: foreign_void_return
c_code local build_ref_678
	ret_reg &ref[678]
	call_c   Dyam_Create_Atom("foreign_void_return")
	move_ret ref[678]
	c_ret

;; TERM 522: foreign_void_return(_M, _N) :> _D
c_code local build_ref_522
	ret_reg &ref[522]
	call_c   build_ref_521()
	call_c   Dyam_Create_Binary(I(9),&ref[521],V(3))
	move_ret ref[522]
	c_ret

;; TERM 521: foreign_void_return(_M, _N)
c_code local build_ref_521
	ret_reg &ref[521]
	call_c   build_ref_678()
	call_c   Dyam_Create_Binary(&ref[678],V(12),V(13))
	move_ret ref[521]
	c_ret

long local pool_fun210[9]=[131078,build_ref_515,build_ref_517,build_ref_518,build_ref_519,build_ref_520,build_ref_522,pool_fun209,pool_fun185]

pl_code local fun210
	call_c   Dyam_Pool(pool_fun210)
	call_c   Dyam_Unify_Item(&ref[515])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     V(6), R(0)
	move     S(5), R(1)
	move     V(7), R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Load(4,V(2))
	pl_call  pred_xtagop_3()
	call_c   DYAM_evpred_functor(V(7),V(8),V(9))
	fail_ret
	call_c   Dyam_Unify(V(10),&ref[517])
	fail_ret
	call_c   Dyam_Reg_Load(0,V(10))
	move     &ref[518], R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Load(4,V(7))
	move     V(12), R(6)
	move     S(5), R(7)
	pl_call  pred_term_current_module_shift_4()
	call_c   Dyam_Choice(fun209)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[519])
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun185)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[520])
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(4),&ref[522])
	fail_ret
	pl_jump  fun65()

;; TERM 516: '*GUARD*'(litteral_to_lpda(_B, _C, _D, _E, _F)) :> []
c_code local build_ref_516
	ret_reg &ref[516]
	call_c   build_ref_515()
	call_c   Dyam_Create_Binary(I(9),&ref[515],I(0))
	move_ret ref[516]
	c_ret

c_code local build_seed_106
	ret_reg &seed[106]
	call_c   build_ref_29()
	call_c   build_ref_558()
	call_c   Dyam_Seed_Start(&ref[29],&ref[558],&ref[558],fun1,1)
	call_c   build_ref_560()
	call_c   Dyam_Seed_Add_Comp(&ref[560],&ref[558],0)
	call_c   Dyam_Seed_End()
	move_ret seed[106]
	c_ret

;; TERM 560: '*FIRST*'(wrapping_predicate(_C)) :> '$$HOLE$$'
c_code local build_ref_560
	ret_reg &ref[560]
	call_c   build_ref_559()
	call_c   Dyam_Create_Binary(I(9),&ref[559],I(7))
	move_ret ref[560]
	c_ret

;; TERM 559: '*FIRST*'(wrapping_predicate(_C))
c_code local build_ref_559
	ret_reg &ref[559]
	call_c   build_ref_25()
	call_c   build_ref_557()
	call_c   Dyam_Create_Unary(&ref[25],&ref[557])
	move_ret ref[559]
	c_ret

;; TERM 557: wrapping_predicate(_C)
c_code local build_ref_557
	ret_reg &ref[557]
	call_c   build_ref_648()
	call_c   Dyam_Create_Unary(&ref[648],V(2))
	move_ret ref[557]
	c_ret

;; TERM 558: '*CITEM*'(wrapping_predicate(_C), wrapping_predicate(_C))
c_code local build_ref_558
	ret_reg &ref[558]
	call_c   build_ref_29()
	call_c   build_ref_557()
	call_c   Dyam_Create_Binary(&ref[29],&ref[557],&ref[557])
	move_ret ref[558]
	c_ret

c_code local build_seed_107
	ret_reg &seed[107]
	call_c   build_ref_53()
	call_c   build_ref_563()
	call_c   Dyam_Seed_Start(&ref[53],&ref[563],I(0),fun6,1)
	call_c   build_ref_561()
	call_c   Dyam_Seed_Add_Comp(&ref[561],fun193,0)
	call_c   Dyam_Seed_End()
	move_ret seed[107]
	c_ret

;; TERM 561: '*RITEM*'(wrapping_predicate(_C), voidret)
c_code local build_ref_561
	ret_reg &ref[561]
	call_c   build_ref_0()
	call_c   build_ref_557()
	call_c   build_ref_2()
	call_c   Dyam_Create_Binary(&ref[0],&ref[557],&ref[2])
	move_ret ref[561]
	c_ret

pl_code local fun193
	call_c   build_ref_561()
	call_c   Dyam_Unify_Item(&ref[561])
	fail_ret
	pl_jump  Follow_Cont(V(1))

;; TERM 563: '*RITEM*'(wrapping_predicate(_C), voidret) :> lpda(5, '$TUPPLE'(35085875413160))
c_code local build_ref_563
	ret_reg &ref[563]
	call_c   build_ref_561()
	call_c   build_ref_562()
	call_c   Dyam_Create_Binary(I(9),&ref[561],&ref[562])
	move_ret ref[563]
	c_ret

;; TERM 562: lpda(5, '$TUPPLE'(35085875413160))
c_code local build_ref_562
	ret_reg &ref[562]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,134217728)
	move_ret R(0)
	call_c   build_ref_585()
	call_c   Dyam_Create_Binary(&ref[585],N(5),R(0))
	move_ret ref[562]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_94
	ret_reg &seed[94]
	call_c   build_ref_29()
	call_c   build_ref_412()
	call_c   Dyam_Seed_Start(&ref[29],&ref[412],&ref[412],fun1,1)
	call_c   build_ref_414()
	call_c   Dyam_Seed_Add_Comp(&ref[414],&ref[412],0)
	call_c   Dyam_Seed_End()
	move_ret seed[94]
	c_ret

;; TERM 414: '*FIRST*'(register_predicate(_C)) :> '$$HOLE$$'
c_code local build_ref_414
	ret_reg &ref[414]
	call_c   build_ref_413()
	call_c   Dyam_Create_Binary(I(9),&ref[413],I(7))
	move_ret ref[414]
	c_ret

;; TERM 413: '*FIRST*'(register_predicate(_C))
c_code local build_ref_413
	ret_reg &ref[413]
	call_c   build_ref_25()
	call_c   build_ref_411()
	call_c   Dyam_Create_Unary(&ref[25],&ref[411])
	move_ret ref[413]
	c_ret

;; TERM 411: register_predicate(_C)
c_code local build_ref_411
	ret_reg &ref[411]
	call_c   build_ref_581()
	call_c   Dyam_Create_Unary(&ref[581],V(2))
	move_ret ref[411]
	c_ret

;; TERM 412: '*CITEM*'(register_predicate(_C), register_predicate(_C))
c_code local build_ref_412
	ret_reg &ref[412]
	call_c   build_ref_29()
	call_c   build_ref_411()
	call_c   Dyam_Create_Binary(&ref[29],&ref[411],&ref[411])
	move_ret ref[412]
	c_ret

c_code local build_seed_95
	ret_reg &seed[95]
	call_c   build_ref_53()
	call_c   build_ref_417()
	call_c   Dyam_Seed_Start(&ref[53],&ref[417],I(0),fun6,1)
	call_c   build_ref_415()
	call_c   Dyam_Seed_Add_Comp(&ref[415],fun150,0)
	call_c   Dyam_Seed_End()
	move_ret seed[95]
	c_ret

;; TERM 415: '*RITEM*'(register_predicate(_C), voidret)
c_code local build_ref_415
	ret_reg &ref[415]
	call_c   build_ref_0()
	call_c   build_ref_411()
	call_c   build_ref_2()
	call_c   Dyam_Create_Binary(&ref[0],&ref[411],&ref[2])
	move_ret ref[415]
	c_ret

pl_code local fun150
	call_c   build_ref_415()
	call_c   Dyam_Unify_Item(&ref[415])
	fail_ret
	pl_jump  Follow_Cont(V(1))

;; TERM 417: '*RITEM*'(register_predicate(_C), voidret) :> lpda(4, '$TUPPLE'(35085875413160))
c_code local build_ref_417
	ret_reg &ref[417]
	call_c   build_ref_415()
	call_c   build_ref_416()
	call_c   Dyam_Create_Binary(I(9),&ref[415],&ref[416])
	move_ret ref[417]
	c_ret

;; TERM 416: lpda(4, '$TUPPLE'(35085875413160))
c_code local build_ref_416
	ret_reg &ref[416]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,134217728)
	move_ret R(0)
	call_c   build_ref_585()
	call_c   Dyam_Create_Binary(&ref[585],N(4),R(0))
	move_ret ref[416]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_90
	ret_reg &seed[90]
	call_c   build_ref_34()
	call_c   build_ref_381()
	call_c   Dyam_Seed_Start(&ref[34],&ref[381],I(0),fun1,1)
	call_c   build_ref_382()
	call_c   Dyam_Seed_Add_Comp(&ref[382],&ref[381],0)
	call_c   Dyam_Seed_End()
	move_ret seed[90]
	c_ret

;; TERM 382: '*GUARD*'(destructure_unify_args(_L, _N, _D, _E)) :> '$$HOLE$$'
c_code local build_ref_382
	ret_reg &ref[382]
	call_c   build_ref_381()
	call_c   Dyam_Create_Binary(I(9),&ref[381],I(7))
	move_ret ref[382]
	c_ret

;; TERM 381: '*GUARD*'(destructure_unify_args(_L, _N, _D, _E))
c_code local build_ref_381
	ret_reg &ref[381]
	call_c   build_ref_34()
	call_c   build_ref_380()
	call_c   Dyam_Create_Unary(&ref[34],&ref[380])
	move_ret ref[381]
	c_ret

;; TERM 380: destructure_unify_args(_L, _N, _D, _E)
c_code local build_ref_380
	ret_reg &ref[380]
	call_c   build_ref_584()
	call_c   Dyam_Term_Start(&ref[584],4)
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[380]
	c_ret

c_code local build_seed_84
	ret_reg &seed[84]
	call_c   build_ref_29()
	call_c   build_ref_306()
	call_c   Dyam_Seed_Start(&ref[29],&ref[306],&ref[306],fun1,1)
	call_c   build_ref_308()
	call_c   Dyam_Seed_Add_Comp(&ref[308],&ref[306],0)
	call_c   Dyam_Seed_End()
	move_ret seed[84]
	c_ret

;; TERM 308: '*FIRST*'('call_decompose_args/3'(_C)) :> '$$HOLE$$'
c_code local build_ref_308
	ret_reg &ref[308]
	call_c   build_ref_307()
	call_c   Dyam_Create_Binary(I(9),&ref[307],I(7))
	move_ret ref[308]
	c_ret

;; TERM 307: '*FIRST*'('call_decompose_args/3'(_C))
c_code local build_ref_307
	ret_reg &ref[307]
	call_c   build_ref_25()
	call_c   build_ref_305()
	call_c   Dyam_Create_Unary(&ref[25],&ref[305])
	move_ret ref[307]
	c_ret

;; TERM 305: 'call_decompose_args/3'(_C)
c_code local build_ref_305
	ret_reg &ref[305]
	call_c   build_ref_640()
	call_c   Dyam_Create_Unary(&ref[640],V(2))
	move_ret ref[305]
	c_ret

;; TERM 306: '*CITEM*'('call_decompose_args/3'(_C), 'call_decompose_args/3'(_C))
c_code local build_ref_306
	ret_reg &ref[306]
	call_c   build_ref_29()
	call_c   build_ref_305()
	call_c   Dyam_Create_Binary(&ref[29],&ref[305],&ref[305])
	move_ret ref[306]
	c_ret

c_code local build_seed_85
	ret_reg &seed[85]
	call_c   build_ref_53()
	call_c   build_ref_312()
	call_c   Dyam_Seed_Start(&ref[53],&ref[312],I(0),fun6,1)
	call_c   build_ref_310()
	call_c   Dyam_Seed_Add_Comp(&ref[310],fun113,0)
	call_c   Dyam_Seed_End()
	move_ret seed[85]
	c_ret

;; TERM 310: '*RITEM*'('call_decompose_args/3'(_C), return(_D, _E))
c_code local build_ref_310
	ret_reg &ref[310]
	call_c   build_ref_0()
	call_c   build_ref_305()
	call_c   build_ref_309()
	call_c   Dyam_Create_Binary(&ref[0],&ref[305],&ref[309])
	move_ret ref[310]
	c_ret

;; TERM 309: return(_D, _E)
c_code local build_ref_309
	ret_reg &ref[309]
	call_c   build_ref_641()
	call_c   Dyam_Create_Binary(&ref[641],V(3),V(4))
	move_ret ref[309]
	c_ret

pl_code local fun113
	call_c   build_ref_310()
	call_c   Dyam_Unify_Item(&ref[310])
	fail_ret
	pl_jump  Follow_Cont(V(1))

;; TERM 312: '*RITEM*'('call_decompose_args/3'(_C), return(_D, _E)) :> lpda(3, '$TUPPLE'(35085875413160))
c_code local build_ref_312
	ret_reg &ref[312]
	call_c   build_ref_310()
	call_c   build_ref_311()
	call_c   Dyam_Create_Binary(I(9),&ref[310],&ref[311])
	move_ret ref[312]
	c_ret

;; TERM 311: lpda(3, '$TUPPLE'(35085875413160))
c_code local build_ref_311
	ret_reg &ref[311]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,134217728)
	move_ret R(0)
	call_c   build_ref_585()
	call_c   Dyam_Create_Binary(&ref[585],N(3),R(0))
	move_ret ref[311]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_82
	ret_reg &seed[82]
	call_c   build_ref_29()
	call_c   build_ref_292()
	call_c   Dyam_Seed_Start(&ref[29],&ref[292],&ref[292],fun1,1)
	call_c   build_ref_294()
	call_c   Dyam_Seed_Add_Comp(&ref[294],&ref[292],0)
	call_c   Dyam_Seed_End()
	move_ret seed[82]
	c_ret

;; TERM 294: '*FIRST*'(bmg_uniform_stacks(_C, _D)) :> '$$HOLE$$'
c_code local build_ref_294
	ret_reg &ref[294]
	call_c   build_ref_293()
	call_c   Dyam_Create_Binary(I(9),&ref[293],I(7))
	move_ret ref[294]
	c_ret

;; TERM 293: '*FIRST*'(bmg_uniform_stacks(_C, _D))
c_code local build_ref_293
	ret_reg &ref[293]
	call_c   build_ref_25()
	call_c   build_ref_291()
	call_c   Dyam_Create_Unary(&ref[25],&ref[291])
	move_ret ref[293]
	c_ret

;; TERM 291: bmg_uniform_stacks(_C, _D)
c_code local build_ref_291
	ret_reg &ref[291]
	call_c   build_ref_679()
	call_c   Dyam_Create_Binary(&ref[679],V(2),V(3))
	move_ret ref[291]
	c_ret

;; TERM 679: bmg_uniform_stacks
c_code local build_ref_679
	ret_reg &ref[679]
	call_c   Dyam_Create_Atom("bmg_uniform_stacks")
	move_ret ref[679]
	c_ret

;; TERM 292: '*CITEM*'(bmg_uniform_stacks(_C, _D), bmg_uniform_stacks(_C, _D))
c_code local build_ref_292
	ret_reg &ref[292]
	call_c   build_ref_29()
	call_c   build_ref_291()
	call_c   Dyam_Create_Binary(&ref[29],&ref[291],&ref[291])
	move_ret ref[292]
	c_ret

c_code local build_seed_83
	ret_reg &seed[83]
	call_c   build_ref_53()
	call_c   build_ref_297()
	call_c   Dyam_Seed_Start(&ref[53],&ref[297],I(0),fun6,1)
	call_c   build_ref_295()
	call_c   Dyam_Seed_Add_Comp(&ref[295],fun105,0)
	call_c   Dyam_Seed_End()
	move_ret seed[83]
	c_ret

;; TERM 295: '*RITEM*'(bmg_uniform_stacks(_C, _D), voidret)
c_code local build_ref_295
	ret_reg &ref[295]
	call_c   build_ref_0()
	call_c   build_ref_291()
	call_c   build_ref_2()
	call_c   Dyam_Create_Binary(&ref[0],&ref[291],&ref[2])
	move_ret ref[295]
	c_ret

pl_code local fun105
	call_c   build_ref_295()
	call_c   Dyam_Unify_Item(&ref[295])
	fail_ret
	pl_jump  Follow_Cont(V(1))

;; TERM 297: '*RITEM*'(bmg_uniform_stacks(_C, _D), voidret) :> lpda(2, '$TUPPLE'(35085875413160))
c_code local build_ref_297
	ret_reg &ref[297]
	call_c   build_ref_295()
	call_c   build_ref_296()
	call_c   Dyam_Create_Binary(I(9),&ref[295],&ref[296])
	move_ret ref[297]
	c_ret

;; TERM 296: lpda(2, '$TUPPLE'(35085875413160))
c_code local build_ref_296
	ret_reg &ref[296]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,134217728)
	move_ret R(0)
	call_c   build_ref_585()
	call_c   Dyam_Create_Binary(&ref[585],N(2),R(0))
	move_ret ref[296]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_77
	ret_reg &seed[77]
	call_c   build_ref_29()
	call_c   build_ref_244()
	call_c   Dyam_Seed_Start(&ref[29],&ref[244],&ref[244],fun1,1)
	call_c   build_ref_245()
	call_c   Dyam_Seed_Add_Comp(&ref[245],&ref[244],0)
	call_c   Dyam_Seed_End()
	move_ret seed[77]
	c_ret

;; TERM 245: '*FIRST*'(metacall_initialize) :> '$$HOLE$$'
c_code local build_ref_245
	ret_reg &ref[245]
	call_c   build_ref_38()
	call_c   Dyam_Create_Binary(I(9),&ref[38],I(7))
	move_ret ref[245]
	c_ret

;; TERM 244: '*CITEM*'(metacall_initialize, metacall_initialize)
c_code local build_ref_244
	ret_reg &ref[244]
	call_c   build_ref_29()
	call_c   build_ref_4()
	call_c   Dyam_Create_Binary(&ref[29],&ref[4],&ref[4])
	move_ret ref[244]
	c_ret

c_code local build_seed_78
	ret_reg &seed[78]
	call_c   build_ref_53()
	call_c   build_ref_247()
	call_c   Dyam_Seed_Start(&ref[53],&ref[247],I(0),fun6,1)
	call_c   build_ref_5()
	call_c   Dyam_Seed_Add_Comp(&ref[5],fun75,0)
	call_c   Dyam_Seed_End()
	move_ret seed[78]
	c_ret

;; TERM 5: '*RITEM*'(metacall_initialize, voidret)
c_code local build_ref_5
	ret_reg &ref[5]
	call_c   build_ref_0()
	call_c   build_ref_4()
	call_c   build_ref_2()
	call_c   Dyam_Create_Binary(&ref[0],&ref[4],&ref[2])
	move_ret ref[5]
	c_ret

pl_code local fun75
	call_c   build_ref_5()
	call_c   Dyam_Unify_Item(&ref[5])
	fail_ret
	pl_jump  Follow_Cont(V(1))

;; TERM 247: '*RITEM*'(metacall_initialize, voidret) :> lpda(1, '$TUPPLE'(35085875413160))
c_code local build_ref_247
	ret_reg &ref[247]
	call_c   build_ref_5()
	call_c   build_ref_246()
	call_c   Dyam_Create_Binary(I(9),&ref[5],&ref[246])
	move_ret ref[247]
	c_ret

;; TERM 246: lpda(1, '$TUPPLE'(35085875413160))
c_code local build_ref_246
	ret_reg &ref[246]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,134217728)
	move_ret R(0)
	call_c   build_ref_585()
	call_c   Dyam_Create_Binary(&ref[585],N(1),R(0))
	move_ret ref[246]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_53
	ret_reg &seed[53]
	call_c   build_ref_34()
	call_c   build_ref_81()
	call_c   Dyam_Seed_Start(&ref[34],&ref[81],I(0),fun1,1)
	call_c   build_ref_82()
	call_c   Dyam_Seed_Add_Comp(&ref[82],&ref[81],0)
	call_c   Dyam_Seed_End()
	move_ret seed[53]
	c_ret

;; TERM 82: '*GUARD*'(toplevel(_D)) :> '$$HOLE$$'
c_code local build_ref_82
	ret_reg &ref[82]
	call_c   build_ref_81()
	call_c   Dyam_Create_Binary(I(9),&ref[81],I(7))
	move_ret ref[82]
	c_ret

;; TERM 81: '*GUARD*'(toplevel(_D))
c_code local build_ref_81
	ret_reg &ref[81]
	call_c   build_ref_34()
	call_c   build_ref_80()
	call_c   Dyam_Create_Unary(&ref[34],&ref[80])
	move_ret ref[81]
	c_ret

;; TERM 80: toplevel(_D)
c_code local build_ref_80
	ret_reg &ref[80]
	call_c   build_ref_637()
	call_c   Dyam_Create_Unary(&ref[637],V(3))
	move_ret ref[80]
	c_ret

;; TERM 1: register_predicate(_A)
c_code local build_ref_1
	ret_reg &ref[1]
	call_c   build_ref_581()
	call_c   Dyam_Create_Unary(&ref[581],V(0))
	move_ret ref[1]
	c_ret

;; TERM 3: '*RITEM*'(register_predicate(_A), voidret)
c_code local build_ref_3
	ret_reg &ref[3]
	call_c   build_ref_0()
	call_c   build_ref_1()
	call_c   build_ref_2()
	call_c   Dyam_Create_Binary(&ref[0],&ref[1],&ref[2])
	move_ret ref[3]
	c_ret

;; TERM 9: term_module_shift(_A, _B, _C, _D, _E)
c_code local build_ref_9
	ret_reg &ref[9]
	call_c   build_ref_680()
	call_c   Dyam_Term_Start(&ref[680],5)
	call_c   Dyam_Term_Arg(V(0))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[9]
	c_ret

;; TERM 680: term_module_shift
c_code local build_ref_680
	ret_reg &ref[680]
	call_c   Dyam_Create_Atom("term_module_shift")
	move_ret ref[680]
	c_ret

;; TERM 8: '*RITEM*'('call_term_module_shift/5'(_A, _B), return(_C, _D, _E))
c_code local build_ref_8
	ret_reg &ref[8]
	call_c   build_ref_0()
	call_c   build_ref_6()
	call_c   build_ref_7()
	call_c   Dyam_Create_Binary(&ref[0],&ref[6],&ref[7])
	move_ret ref[8]
	c_ret

;; TERM 7: return(_C, _D, _E)
c_code local build_ref_7
	ret_reg &ref[7]
	call_c   build_ref_641()
	call_c   Dyam_Term_Start(&ref[641],3)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[7]
	c_ret

;; TERM 6: 'call_term_module_shift/5'(_A, _B)
c_code local build_ref_6
	ret_reg &ref[6]
	call_c   build_ref_642()
	call_c   Dyam_Create_Binary(&ref[642],V(0),V(1))
	move_ret ref[6]
	c_ret

;; TERM 10: wrapping_predicate(_A)
c_code local build_ref_10
	ret_reg &ref[10]
	call_c   build_ref_648()
	call_c   Dyam_Create_Unary(&ref[648],V(0))
	move_ret ref[10]
	c_ret

;; TERM 11: '*RITEM*'(wrapping_predicate(_A), voidret)
c_code local build_ref_11
	ret_reg &ref[11]
	call_c   build_ref_0()
	call_c   build_ref_10()
	call_c   build_ref_2()
	call_c   Dyam_Create_Binary(&ref[0],&ref[10],&ref[2])
	move_ret ref[11]
	c_ret

;; TERM 15: decompose_args(_A, _B, _C)
c_code local build_ref_15
	ret_reg &ref[15]
	call_c   build_ref_681()
	call_c   Dyam_Term_Start(&ref[681],3)
	call_c   Dyam_Term_Arg(V(0))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_End()
	move_ret ref[15]
	c_ret

;; TERM 681: decompose_args
c_code local build_ref_681
	ret_reg &ref[681]
	call_c   Dyam_Create_Atom("decompose_args")
	move_ret ref[681]
	c_ret

;; TERM 14: '*RITEM*'('call_decompose_args/3'(_A), return(_B, _C))
c_code local build_ref_14
	ret_reg &ref[14]
	call_c   build_ref_0()
	call_c   build_ref_12()
	call_c   build_ref_13()
	call_c   Dyam_Create_Binary(&ref[0],&ref[12],&ref[13])
	move_ret ref[14]
	c_ret

;; TERM 13: return(_B, _C)
c_code local build_ref_13
	ret_reg &ref[13]
	call_c   build_ref_641()
	call_c   Dyam_Create_Binary(&ref[641],V(1),V(2))
	move_ret ref[13]
	c_ret

;; TERM 12: 'call_decompose_args/3'(_A)
c_code local build_ref_12
	ret_reg &ref[12]
	call_c   build_ref_640()
	call_c   Dyam_Create_Unary(&ref[640],V(0))
	move_ret ref[12]
	c_ret

;; TERM 16: bmg_uniform_stacks(_A, _B)
c_code local build_ref_16
	ret_reg &ref[16]
	call_c   build_ref_679()
	call_c   Dyam_Create_Binary(&ref[679],V(0),V(1))
	move_ret ref[16]
	c_ret

;; TERM 17: '*RITEM*'(bmg_uniform_stacks(_A, _B), voidret)
c_code local build_ref_17
	ret_reg &ref[17]
	call_c   build_ref_0()
	call_c   build_ref_16()
	call_c   build_ref_2()
	call_c   Dyam_Create_Binary(&ref[0],&ref[16],&ref[2])
	move_ret ref[17]
	c_ret

;; TERM 369: destructure_unify(_B, _F)
c_code local build_ref_369
	ret_reg &ref[369]
	call_c   build_ref_604()
	call_c   Dyam_Create_Binary(&ref[604],V(1),V(5))
	move_ret ref[369]
	c_ret

;; TERM 370: destructure_unify(_C, _G)
c_code local build_ref_370
	ret_reg &ref[370]
	call_c   build_ref_604()
	call_c   Dyam_Create_Binary(&ref[604],V(2),V(6))
	move_ret ref[370]
	c_ret

;; TERM 373: unify(_B, _C) :> _D
c_code local build_ref_373
	ret_reg &ref[373]
	call_c   build_ref_372()
	call_c   Dyam_Create_Binary(I(9),&ref[372],V(3))
	move_ret ref[373]
	c_ret

;; TERM 372: unify(_B, _C)
c_code local build_ref_372
	ret_reg &ref[372]
	call_c   build_ref_682()
	call_c   Dyam_Create_Binary(&ref[682],V(1),V(2))
	move_ret ref[372]
	c_ret

;; TERM 682: unify
c_code local build_ref_682
	ret_reg &ref[682]
	call_c   Dyam_Create_Atom("unify")
	move_ret ref[682]
	c_ret

;; TERM 376: unify(_C, _B) :> _D
c_code local build_ref_376
	ret_reg &ref[376]
	call_c   build_ref_375()
	call_c   Dyam_Create_Binary(I(9),&ref[375],V(3))
	move_ret ref[376]
	c_ret

;; TERM 375: unify(_C, _B)
c_code local build_ref_375
	ret_reg &ref[375]
	call_c   build_ref_682()
	call_c   Dyam_Create_Binary(&ref[682],V(2),V(1))
	move_ret ref[375]
	c_ret

;; TERM 371: destructure_unify(_B, _C)
c_code local build_ref_371
	ret_reg &ref[371]
	call_c   build_ref_604()
	call_c   Dyam_Create_Binary(&ref[604],V(1),V(2))
	move_ret ref[371]
	c_ret

;; TERM 377: <
c_code local build_ref_377
	ret_reg &ref[377]
	call_c   Dyam_Create_Atom("<")
	move_ret ref[377]
	c_ret

;; TERM 379: [_M|_N]
c_code local build_ref_379
	ret_reg &ref[379]
	call_c   Dyam_Create_List(V(12),V(13))
	move_ret ref[379]
	c_ret

;; TERM 378: [_K|_L]
c_code local build_ref_378
	ret_reg &ref[378]
	call_c   Dyam_Create_List(V(10),V(11))
	move_ret ref[378]
	c_ret

;; TERM 374: destructure_unify(_C, _B)
c_code local build_ref_374
	ret_reg &ref[374]
	call_c   build_ref_604()
	call_c   Dyam_Create_Binary(&ref[604],V(2),V(1))
	move_ret ref[374]
	c_ret

;; TERM 335: terminal_cont(_B)
c_code local build_ref_335
	ret_reg &ref[335]
	call_c   build_ref_572()
	call_c   Dyam_Create_Unary(&ref[572],V(1))
	move_ret ref[335]
	c_ret

;; TERM 337: '*JUMP*'(_H, _G)
c_code local build_ref_337
	ret_reg &ref[337]
	call_c   build_ref_683()
	call_c   Dyam_Create_Binary(&ref[683],V(7),V(6))
	move_ret ref[337]
	c_ret

;; TERM 683: '*JUMP*'
c_code local build_ref_683
	ret_reg &ref[683]
	call_c   Dyam_Create_Atom("*JUMP*")
	move_ret ref[683]
	c_ret

;; TERM 336: '*ALTERNATIVE*'(_E, _F)
c_code local build_ref_336
	ret_reg &ref[336]
	call_c   build_ref_684()
	call_c   Dyam_Create_Binary(&ref[684],V(4),V(5))
	move_ret ref[336]
	c_ret

;; TERM 684: '*ALTERNATIVE*'
c_code local build_ref_684
	ret_reg &ref[684]
	call_c   Dyam_Create_Atom("*ALTERNATIVE*")
	move_ret ref[684]
	c_ret

;; TERM 339: '*JUMP*'(_I, _G)
c_code local build_ref_339
	ret_reg &ref[339]
	call_c   build_ref_683()
	call_c   Dyam_Create_Binary(&ref[683],V(8),V(6))
	move_ret ref[339]
	c_ret

;; TERM 338: '*JUMP*'(_B)
c_code local build_ref_338
	ret_reg &ref[338]
	call_c   build_ref_683()
	call_c   Dyam_Create_Unary(&ref[683],V(1))
	move_ret ref[338]
	c_ret

;; TERM 332: _E :> _F
c_code local build_ref_332
	ret_reg &ref[332]
	call_c   Dyam_Create_Binary(I(9),V(4),V(5))
	move_ret ref[332]
	c_ret

;; TERM 331: _D :> _F
c_code local build_ref_331
	ret_reg &ref[331]
	call_c   Dyam_Create_Binary(I(9),V(3),V(5))
	move_ret ref[331]
	c_ret

;; TERM 330: '*ALTERNATIVE*'(_D, _E) :> _F
c_code local build_ref_330
	ret_reg &ref[330]
	call_c   build_ref_329()
	call_c   Dyam_Create_Binary(I(9),&ref[329],V(5))
	move_ret ref[330]
	c_ret

;; TERM 329: '*ALTERNATIVE*'(_D, _E)
c_code local build_ref_329
	ret_reg &ref[329]
	call_c   build_ref_684()
	call_c   Dyam_Create_Binary(&ref[684],V(3),V(4))
	move_ret ref[329]
	c_ret

;; TERM 333: _G :> _H
c_code local build_ref_333
	ret_reg &ref[333]
	call_c   Dyam_Create_Binary(I(9),V(6),V(7))
	move_ret ref[333]
	c_ret

;; TERM 334: '*OBJECT*'(_B)
c_code local build_ref_334
	ret_reg &ref[334]
	call_c   build_ref_685()
	call_c   Dyam_Create_Unary(&ref[685],V(1))
	move_ret ref[334]
	c_ret

;; TERM 685: '*OBJECT*'
c_code local build_ref_685
	ret_reg &ref[685]
	call_c   Dyam_Create_Atom("*OBJECT*")
	move_ret ref[685]
	c_ret

;; TERM 238: '*LIGHTLASTCALL*'(_C)
c_code local build_ref_238
	ret_reg &ref[238]
	call_c   build_ref_686()
	call_c   Dyam_Create_Unary(&ref[686],V(2))
	move_ret ref[238]
	c_ret

;; TERM 686: '*LIGHTLASTCALL*'
c_code local build_ref_686
	ret_reg &ref[686]
	call_c   Dyam_Create_Atom("*LIGHTLASTCALL*")
	move_ret ref[686]
	c_ret

;; TERM 237: '*LIGHTLAST*'(_D)
c_code local build_ref_237
	ret_reg &ref[237]
	call_c   build_ref_667()
	call_c   Dyam_Create_Unary(&ref[667],V(3))
	move_ret ref[237]
	c_ret

;; TERM 236: light_tabular _B
c_code local build_ref_236
	ret_reg &ref[236]
	call_c   build_ref_633()
	call_c   Dyam_Create_Unary(&ref[633],V(1))
	move_ret ref[236]
	c_ret

;; TERM 240: '*LASTCALL*'(_C)
c_code local build_ref_240
	ret_reg &ref[240]
	call_c   build_ref_687()
	call_c   Dyam_Create_Unary(&ref[687],V(2))
	move_ret ref[240]
	c_ret

;; TERM 687: '*LASTCALL*'
c_code local build_ref_687
	ret_reg &ref[687]
	call_c   Dyam_Create_Atom("*LASTCALL*")
	move_ret ref[687]
	c_ret

;; TERM 239: '*LAST*'(_D)
c_code local build_ref_239
	ret_reg &ref[239]
	call_c   build_ref_666()
	call_c   Dyam_Create_Unary(&ref[666],V(3))
	move_ret ref[239]
	c_ret

;; TERM 235: lco _B
c_code local build_ref_235
	ret_reg &ref[235]
	call_c   build_ref_688()
	call_c   Dyam_Create_Unary(&ref[688],V(1))
	move_ret ref[235]
	c_ret

;; TERM 688: lco
c_code local build_ref_688
	ret_reg &ref[688]
	call_c   Dyam_Create_Atom("lco")
	move_ret ref[688]
	c_ret

;; TERM 221: _B :- _D
c_code local build_ref_221
	ret_reg &ref[221]
	call_c   build_ref_592()
	call_c   Dyam_Create_Binary(&ref[592],V(1),V(3))
	move_ret ref[221]
	c_ret

;; TERM 222: 'No compiler expansion for ~w'
c_code local build_ref_222
	ret_reg &ref[222]
	call_c   Dyam_Create_Atom("No compiler expansion for ~w")
	move_ret ref[222]
	c_ret

;; TERM 128: tagop(_B, _C, _D)
c_code local build_ref_128
	ret_reg &ref[128]
	call_c   build_ref_689()
	call_c   Dyam_Term_Start(&ref[689],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[128]
	c_ret

;; TERM 689: tagop
c_code local build_ref_689
	ret_reg &ref[689]
	call_c   Dyam_Create_Atom("tagop")
	move_ret ref[689]
	c_ret

;; TERM 129: '$tagop'(_B, _C)
c_code local build_ref_129
	ret_reg &ref[129]
	call_c   build_ref_690()
	call_c   Dyam_Create_Binary(&ref[690],V(1),V(2))
	move_ret ref[129]
	c_ret

;; TERM 690: '$tagop'
c_code local build_ref_690
	ret_reg &ref[690]
	call_c   Dyam_Create_Atom("$tagop")
	move_ret ref[690]
	c_ret

;; TERM 79: _C ^ _D
c_code local build_ref_79
	ret_reg &ref[79]
	call_c   build_ref_626()
	call_c   Dyam_Create_Binary(&ref[626],V(2),V(3))
	move_ret ref[79]
	c_ret

;; TERM 83: 'call to toplevel failed: ~w'
c_code local build_ref_83
	ret_reg &ref[83]
	call_c   Dyam_Create_Atom("call to toplevel failed: ~w")
	move_ret ref[83]
	c_ret

;; TERM 85: '$toplevel'(_B)
c_code local build_ref_85
	ret_reg &ref[85]
	call_c   build_ref_612()
	call_c   Dyam_Create_Unary(&ref[612],V(1))
	move_ret ref[85]
	c_ret

;; TERM 60: predicate(_B, _C)
c_code local build_ref_60
	ret_reg &ref[60]
	call_c   build_ref_645()
	call_c   Dyam_Create_Binary(&ref[645],V(1),V(2))
	move_ret ref[60]
	c_ret

;; TERM 62: [_B]
c_code local build_ref_62
	ret_reg &ref[62]
	call_c   Dyam_Create_List(V(1),I(0))
	move_ret ref[62]
	c_ret

;; TERM 61: 'call to undefined predicate ~w'
c_code local build_ref_61
	ret_reg &ref[61]
	call_c   Dyam_Create_Atom("call to undefined predicate ~w")
	move_ret ref[61]
	c_ret

pl_code local fun7
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Object(R(0))
	call_c   Dyam_Tabule()
	pl_ret

pl_code local fun23
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Object(R(0))
	call_c   Dyam_Tabule()
	pl_ret

pl_code local fun194
	call_c   Dyam_Remove_Choice()
	pl_call  fun76(&seed[107],2,V(3))
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun151
	call_c   Dyam_Remove_Choice()
	pl_call  fun76(&seed[95],2,V(3))
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun134
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Reg_Load(0,V(1))
	pl_call  pred_check_deref_var_1()
fun133:
	call_c   Dyam_Cut()
	call_c   DYAM_evpred_assert_1(&ref[371])
	call_c   Dyam_Unify(V(4),&ref[373])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret


pl_code local fun136
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Reg_Load(0,V(2))
	pl_call  pred_check_deref_var_1()
fun135:
	call_c   Dyam_Cut()
	call_c   DYAM_evpred_assert_1(&ref[374])
	call_c   Dyam_Unify(V(4),&ref[376])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret


pl_code local fun138
	call_c   Dyam_Remove_Choice()
	call_c   DYAM_evpred_atomic(V(2))
	fail_ret
fun137:
	call_c   Dyam_Cut()
	pl_fail


pl_code local fun140
	call_c   Dyam_Remove_Choice()
	call_c   DYAM_evpred_assert_1(&ref[374])
fun139:
	call_c   DYAM_evpred_univ(V(1),&ref[378])
	fail_ret
	call_c   DYAM_evpred_univ(V(2),&ref[379])
	fail_ret
	pl_call  fun15(&seed[90],1)
	call_c   Dyam_Deallocate()
	pl_ret


pl_code local fun141
	call_c   Dyam_Update_Choice(fun70)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_functor(V(1),V(8),V(9))
	fail_ret
	call_c   DYAM_evpred_functor(V(2),V(8),V(9))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun140)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_Compare_3(&ref[377],V(1),V(2))
	fail_ret
	call_c   Dyam_Cut()
	call_c   DYAM_evpred_assert_1(&ref[371])
	pl_jump  fun139()

pl_code local fun142
	call_c   Dyam_Update_Choice(fun141)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Choice(fun138)
	call_c   DYAM_evpred_atomic(V(1))
	fail_ret
	pl_jump  fun137()

pl_code local fun143
	call_c   Dyam_Update_Choice(fun142)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Choice(fun136)
	call_c   Dyam_Reg_Load(0,V(2))
	pl_call  pred_check_var_1()
	pl_jump  fun135()

pl_code local fun144
	call_c   Dyam_Update_Choice(fun143)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Choice(fun134)
	call_c   Dyam_Reg_Load(0,V(1))
	pl_call  pred_check_var_1()
	pl_jump  fun133()

pl_code local fun145
	call_c   Dyam_Update_Choice(fun144)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Reg_Load(0,V(1))
	pl_call  pred_check_tupple_1()
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(1))
	move     V(7), R(2)
	move     S(5), R(3)
	pl_call  pred_oset_tupple_2()
	call_c   Dyam_Reg_Load(0,V(2))
	call_c   Dyam_Reg_Load(2,V(7))
	call_c   Dyam_Reg_Load(4,V(3))
	call_c   Dyam_Reg_Load(6,V(4))
	call_c   Dyam_Reg_Deallocate(4)
	pl_jump  pred_destructure_unify_aux_4()

pl_code local fun146
	call_c   Dyam_Update_Choice(fun145)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[370])
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(6))
	call_c   Dyam_Reg_Load(4,V(3))
	call_c   Dyam_Reg_Load(6,V(4))
	call_c   Dyam_Reg_Deallocate(4)
	pl_jump  pred_destructure_unify_aux_4()

pl_code local fun147
	call_c   Dyam_Update_Choice(fun146)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[369])
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(5))
	call_c   Dyam_Reg_Load(2,V(2))
	call_c   Dyam_Reg_Load(4,V(3))
	call_c   Dyam_Reg_Load(6,V(4))
	call_c   Dyam_Reg_Deallocate(4)
	pl_jump  pred_destructure_unify_aux_4()

pl_code local fun122
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Reg_Load_Ptr(2,V(8))
	fail_ret
	call_c   Dyam_Reg_Load(4,&ref[338])
	call_c   DyALog_Mutable_Write(R(2),&R(4),0)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(8))
	fail_ret
	call_c   Dyam_Unify(V(3),&ref[339])
	fail_ret
	call_c   Dyam_Unify(V(2),&ref[336])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun123
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Reg_Load(0,V(1))
	move     V(6), R(2)
	move     S(5), R(3)
	pl_call  pred_tupple_2()
	call_c   Dyam_Choice(fun122)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[337])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(3),V(1))
	fail_ret
	call_c   Dyam_Unify(V(2),&ref[336])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun119
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(2),&ref[334])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun120
	call_c   Dyam_Update_Choice(fun119)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[329])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(3))
	move     V(6), R(2)
	move     S(5), R(3)
	pl_call  pred_trans_unfold_2()
	call_c   Dyam_Reg_Load(0,V(4))
	move     V(7), R(2)
	move     S(5), R(3)
	pl_call  pred_trans_unfold_2()
	call_c   Dyam_Unify(V(2),&ref[333])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun114
	call_c   Dyam_Remove_Choice()
	pl_call  fun76(&seed[85],2,V(5))
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun106
	call_c   Dyam_Remove_Choice()
	pl_call  fun76(&seed[83],2,V(4))
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun73
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Object(R(0))
	call_c   Dyam_Tabule()
	pl_jump  Schedule_Prolog()

pl_code local fun74
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Choice(fun73)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Subsume(R(0))
	fail_ret
	call_c   Dyam_Cut()
	pl_ret

pl_code local fun76
	call_c   Dyam_Backptr_Trace(R(1),R(2))
	call_c   Dyam_Light_Object(R(0))
	pl_jump  Schedule_Prolog()

pl_code local fun77
	call_c   Dyam_Remove_Choice()
	pl_call  fun76(&seed[78],2,V(2))
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun70
	call_c   Dyam_Remove_Choice()
	pl_fail

pl_code local fun71
	call_c   Dyam_Update_Choice(fun70)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(&ref[239],V(4))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(5),&ref[240])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun63
	call_c   Dyam_Remove_Choice()
	move     &ref[222], R(0)
	move     0, R(1)
	move     &ref[62], R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_error_2()

pl_code local fun28
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(1),I(0))
	fail_ret
	call_c   Dyam_Unify(V(3),V(2))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun29
	call_c   Dyam_Update_Choice(fun28)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(3),&ref[129])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun16
	call_c   Dyam_Remove_Choice()
	move     &ref[83], R(0)
	move     0, R(1)
	move     &ref[62], R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_error_2()

pl_code local fun18
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(3),V(1))
	fail_ret
	call_c   Dyam_Unify(V(2),&ref[84])
	fail_ret
	pl_jump  fun17()

pl_code local fun10
	call_c   Dyam_Remove_Choice()
	move     &ref[61], R(0)
	move     0, R(1)
	move     &ref[62], R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_warning_2()


;;------------------ INIT POOL ------------

c_code local build_init_pool
	call_c   build_seed_44()
	call_c   build_seed_45()
	call_c   build_seed_46()
	call_c   build_seed_47()
	call_c   build_seed_48()
	call_c   build_seed_49()
	call_c   build_seed_6()
	call_c   build_seed_43()
	call_c   build_seed_0()
	call_c   build_seed_7()
	call_c   build_seed_42()
	call_c   build_seed_4()
	call_c   build_seed_5()
	call_c   build_seed_1()
	call_c   build_seed_39()
	call_c   build_seed_34()
	call_c   build_seed_3()
	call_c   build_seed_40()
	call_c   build_seed_22()
	call_c   build_seed_20()
	call_c   build_seed_30()
	call_c   build_seed_2()
	call_c   build_seed_25()
	call_c   build_seed_21()
	call_c   build_seed_27()
	call_c   build_seed_23()
	call_c   build_seed_19()
	call_c   build_seed_38()
	call_c   build_seed_26()
	call_c   build_seed_18()
	call_c   build_seed_29()
	call_c   build_seed_36()
	call_c   build_seed_37()
	call_c   build_seed_28()
	call_c   build_seed_10()
	call_c   build_seed_9()
	call_c   build_seed_32()
	call_c   build_seed_31()
	call_c   build_seed_14()
	call_c   build_seed_15()
	call_c   build_seed_8()
	call_c   build_seed_24()
	call_c   build_seed_11()
	call_c   build_seed_41()
	call_c   build_seed_12()
	call_c   build_seed_13()
	call_c   build_seed_17()
	call_c   build_seed_35()
	call_c   build_seed_33()
	call_c   build_seed_16()
	call_c   build_seed_106()
	call_c   build_seed_107()
	call_c   build_seed_94()
	call_c   build_seed_95()
	call_c   build_seed_90()
	call_c   build_seed_84()
	call_c   build_seed_85()
	call_c   build_seed_82()
	call_c   build_seed_83()
	call_c   build_seed_77()
	call_c   build_seed_78()
	call_c   build_seed_53()
	call_c   build_ref_1()
	call_c   build_ref_3()
	call_c   build_ref_4()
	call_c   build_ref_5()
	call_c   build_ref_9()
	call_c   build_ref_8()
	call_c   build_ref_10()
	call_c   build_ref_11()
	call_c   build_ref_15()
	call_c   build_ref_14()
	call_c   build_ref_16()
	call_c   build_ref_17()
	call_c   build_ref_369()
	call_c   build_ref_370()
	call_c   build_ref_373()
	call_c   build_ref_376()
	call_c   build_ref_371()
	call_c   build_ref_377()
	call_c   build_ref_379()
	call_c   build_ref_378()
	call_c   build_ref_374()
	call_c   build_ref_335()
	call_c   build_ref_337()
	call_c   build_ref_336()
	call_c   build_ref_339()
	call_c   build_ref_338()
	call_c   build_ref_332()
	call_c   build_ref_331()
	call_c   build_ref_330()
	call_c   build_ref_333()
	call_c   build_ref_329()
	call_c   build_ref_334()
	call_c   build_ref_238()
	call_c   build_ref_237()
	call_c   build_ref_236()
	call_c   build_ref_240()
	call_c   build_ref_239()
	call_c   build_ref_235()
	call_c   build_ref_221()
	call_c   build_ref_222()
	call_c   build_ref_128()
	call_c   build_ref_129()
	call_c   build_ref_79()
	call_c   build_ref_83()
	call_c   build_ref_84()
	call_c   build_ref_85()
	call_c   build_ref_60()
	call_c   build_ref_62()
	call_c   build_ref_61()
	c_ret

long local ref[691]
long local seed[108]

long local _initialization

c_code global initialization_dyalog_lpda
	call_c   Dyam_Check_And_Update_Initialization(_initialization)
	fail_c_ret
	call_c   initialization_dyalog_format()
	call_c   build_init_pool()
	call_c   build_viewers()
	call_c   load()
	c_ret

