/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 1998, 2002, 2003, 2004, 2008, 2009, 2011, 2013 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  header.pl -- Header for all files of DyALog Compiler
 *
 * ----------------------------------------------------------------
 * Description
 * 
 * ----------------------------------------------------------------
 */

:-op(600,xfy,:>).
:-op(  700, xfx, [?=]). % for default value
		 
:-rec_prolog append/3.

:-std_prolog
	update_counter/2,
	reset_counter/2,
	value_counter/2,

	read_file/2,
	read_files/2,

	label_code/2,
	choice_code/3,
	jump_code/2,
	closure_code/3,
	
	name_builder/3
.	


:-std_prolog toplevel_loop/0.

:-std_prolog try_compiler_toplevel/2.

:-rec_prolog toplevel/1.

:-rec_prolog check_analyzer_mode/1.

:-prolog emit_at_pda/0, analyze_at_pda/1, analyze_at_clause/1.

:-prolog 	init_install/1.

:-std_prolog
	label_term/2,
	label_term_list/2,
	label_local_term/2
	.

:-rec_prolog seed_model/1.

:-light_tabular
	register_predicate/1,
	registered_predicate/1,
	wrapping_predicate/1
	.

:-extensional wrapper_function_name/2.

:-light_tabular decompose_args/3.
:-mode(decompose_args/3,+(+,-,-)).

:-rec_prolog
	make_callret/3,
	make_dcg_callret/7,

	pgm_to_lpda/2,
	body_to_lpda/5,
	clause_to_lpda/2,

	rcg_body_to_lpda/5,

	build_cond_loader/4,
	tag_normalize_and_compile/7,
	
	get_clause/1
	.

:-std_prolog trans_unfold/2.

:-rec_prolog scan_to_lpda/7.
:-rec_prolog non_terminal_to_lpda/9.
:-rec_prolog litteral_to_lpda/5.

:-prolog
	seed_install/3,
	unfold/5,
	build_closure/5,
	build_args_closure/6,

	unfold_std_trans/3,
	     
	app_model/1
		
	.

:-rec_prolog
	std_prolog_unif_args/5,
	std_prolog_unif_args_alt/6,
	std_prolog_load_args_alt/6,
	std_prolog_load_args/5.

:-std_prolog emit_function/1.
:-std_prolog emit_init_code/1.
:-std_prolog inlinable/1.

:-std_prolog handle_choice/5.

:-std_prolog registered_predicate/1.

:-light_tabular metacall_initialize.

:-features( '*WAIT*', [task,cont] ).
:-features( '*WAIT-CONT*', [rest,cont,tupple] ).

:-features(compiler_info,[name,version,author,email]).

:-features(seed,  [model,
		   id,
		   anchor,
		   subs_comp,
		   code,
		   go,
		   tabule,
		   schedule,
		   subsume,
		   model_ref,
		   subs_comp_ref,
		   c_type,
		   c_type_ref,
		   out_env,	% list of out_env
		   appinfo,	% to transport info to customize applications
		   tail_flag	% to build more compact tail names (with new_gen_tail)
		  ]).

:-features(application, [item,
			 trans,
			 item_comp, % item_comp(item_model)
			 code,      % code(trans_model)
			 item_id,
			 trans_id,
			 mode,
			 restrict,
			 out_env,
			 key        % key to get info about an application in an appinfo list
			]).

:-features('$fun_info',[code,refs,ins]).

:-extensional
	(   prolog)/1,
	(   rec_prolog)/1,
	(   std_prolog)/1,
	(   light_tabular)/1,
	(   extensional)/1,
	(   foreign)/2,
	(   builtin)/2,
	(   system_register)/2,
	(   lco)/1,
	( scanner)/4,
	( tagop )/3,
	'$fact'/1,
	(   ':-')/2,
	(   '?-')/1,
	compiler_extension/2,
	guide/1,
	foreign_void_return/1
	.

:-std_prolog xtagop/3.

:-std_prolog jump_value/2,jump_inc/1,jump_decr/1.

:-std_prolog record_without_doublon/1.
:-std_prolog holes/1.
:-std_prolog tupple/2,union/3,inter/3,delete/3,oset_tupple/2,extend_tupple/3,duplicate_vars/2.
:-std_prolog tupple_delete/3.
:-extensional tupple_delete_wrapper/3.
:-std_prolog warning/2,error/2,internal_error/2.
:-std_prolog gen_tail/2.
:-std_prolog new_gen_tail/4.
:-std_prolog default_module/2.
:-std_prolog reg_value/2.

:-std_prolog check_var/1, check_deref_var/1,check_tupple/1,zero_var/1,null_tupple/1,check_fun_tupple/1.
:-std_prolog var_in_tupple/2.

:-std_prolog add_ordered_table/3,ordered_table/2,length_ordered_table/2, reverse_ordered_table/1, reset_ordered_table/1, consume_ordered_table/2.

:-std_prolog
	ma_emit_label/1,
	ma_emit_jump/1,
	ma_emit_move/2,
	ma_emit_call_c/2,
	ma_emit_jump_ret/0,
	ma_emit_fail_ret/0,
	ma_emit_true_ret/0,
	ma_emit_fail_c_ret/0,
	ma_emit_true_c_ret/0,
	ma_emit_move_ret/1,
	ma_emit_ret/0,
	ma_emit_pl_code/2,
	ma_emit_code/2,
	ma_emit_ident/2,
	ma_emit_table/4,
	ma_emit_pl_call/2,
	ma_emit_pl_jump/2,
	ma_emit_pl_fail/0,
	ma_emit_pl_ret/0,
	ma_emit_jump_reg/2,
	ma_emit_ret_reg/1,
	ma_emit_cache_or_build/2
	.


:-std_prolog emit_globals/2, emit_globals_in_init_pool/1.

:-std_prolog register_init_pool_fun/1.

:-std_prolog abolish/1.
:-std_prolog foreign_arg/3.
:-std_prolog enumerate/3.

:-std_prolog my_numbervars/3.

:-xcompiler(( exists( G ) :-
	    ( item_term(Item,G) -> true ; fail),
	      recorded(Item)
	    )).

:-xcompiler(( X ?= V :- (X=V xor true))). %% Setting a default value

:-rec_prolog
	destructure_unify/4,
	destructure_unify_args/4.

%% About BMG

:-std_prolog dcg_null_step/5.
:-std_prolog bmg_null_step/7.

:-light_tabular
	bmg_args/6,
	bmg_get_stacks/1,
	bmg_uniform_stacks/2.

:-mode(bmg_args/6,+(-,-,+,+,-,-)).
:-mode(bmg_get_stacks/1,+(-)).

:-extensional
	(   bmg_stacks)/1,
	(   bmg_stack)/1,
	(   bmg_island)/2,
	(   bmg_island)/1,
	(   bmg_pushable)/2
	.

:-rec_prolog
	bmg_equations/4,
	bmg_inside_island/8,
	bmg_pop/4,
	bmg_pop_code/8
	.

:-rec_prolog bmg_get_stacks/2.

:-light_tabular bmg_head_stacks/6.
:-mode(bmg_head_stacks/6,+(+,-,-,-,-,-)).

%% About DCGs

:-rec_prolog body_to_lpda/5,dcg_body_to_lpda_handler/9.
:-std_prolog dcg_body_to_lpda/9.
	
%%:-lco body_to_lpda/5,dcg_body_to_lpda/9.

:-extensional compiler_info/3.
:-extensional
%% About TAGs
:-op(800,fx,[tree,auxtree,spinetree]).
:-op(700,xfx,[at]).
:-op(500,xfy,[and]).
:-op(399,fx,[*,+,**,=]).

:-features(tag_node,
	   [  id,
	      label,
	      children,    %% list of tag nodes
	      kind,        %% in {scan,subst,foot,std,escape,anchor}
	      adj,         %% in {yes,no,strict}
	      spine,       %% in {yes,no,left,right}
	      top,         %% Top args
	      bot,         %% bot args
	      token,       %% access to token for lex/anchor/coanchor nodes
	      adjleft,     %% in {yes,no}
	      adjright,    %% in {yes,no}
	      adjwrap      %% in {yes,no}
	   ]
	  ).

:-features(tag_tree,
	   [ family,
	     name,
	     tree
	   ]
	  ).

:-features(tag_anchor,
	   [ family,
	     coanchors,
	     equations
	   ]).

:-extensional (tree)/1,(auxtree)/1, (spinetree)/1.

%% For TAG modulation (see tag_maker.pl)
:-features(nt_modulation, [nt_pattern,left_pattern,right_pattern]).
:-features(tag_modulation, [subst_modulation,top_modulation,bot_modulation]).

%% About RCG

:-std_prolog rcg_functor/4.

%% About Left Corner

:-std_prolog check_lc/1.

%% About modules

:-std_prolog term_module_get/2, term_current_module_shift/4.

:-light_tabular term_module_shift/5.
:-mode(term_module_shift/5,+(+,+,-,-,-)).

:-std_prolog deep_module_shift/3.

%% About autoload

:-std_prolog autoloader_simplify/2.
:-extensional autoload/0.
:-rec_prolog try_install_loader/4.

:-xcompiler(( install_loader(Id,Loader^Autoload_Builder,Trans,AutoloadedTrans) :-
	    (	autoload,
		parse_mode(token),
		Autoload_Builder,
		Loader \== true ->
		build_cond_loader(Id,(Loader->true;fail),Trans,AutoloadedTrans)
	    ;
		AutoloadedTrans=Trans
	    ),
	      numbervars_delete(Id)
	    )).

%% About Kleene

:-extensional kleene_conditions/5.

:-std_prolog kleene_normalize/2.

:-features((@*),[goal,
		 vars,
		 from,
		 to,
		 collect_first,
		 collect_last,
		 collect_loop,
		 collect_next,
		 collect_pred
		]
	  ).

%% About Interleaving

%% About guards

:-features(guard,
	   [goal,
	    plus,
	    minus
	   ])
.


:-require 'format.pl'.







