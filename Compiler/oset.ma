;; Compiler: DyALog 1.14.0
;; File "oset.pl"


;;------------------ LOADING ------------

c_code local load
	call_c   Dyam_Loading_Set()
	call_c   Dyam_Loading_Reset()
	c_ret

pl_code global pred_oset_union_3
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(3)
	call_c   Dyam_Choice(fun11)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Reg_Load_Ptr(2,V(1))
	fail_ret
	call_c   Dyam_Reg_Load_Ptr(4,V(2))
	fail_ret
	call_c   oset_union(R(2),R(4))
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(4))
	fail_ret
	call_c   Dyam_Cut()
fun10:
	call_c   Dyam_Unify(V(4),V(3))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret


pl_code global pred_oset_inter_3
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(3)
	call_c   Dyam_Choice(fun11)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Reg_Load_Ptr(2,V(1))
	fail_ret
	call_c   Dyam_Reg_Load_Ptr(4,V(2))
	fail_ret
	call_c   oset_inter(R(2),R(4))
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(4))
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun10()

pl_code global pred_oset_delete_3
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(3)
	call_c   Dyam_Choice(fun11)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Reg_Load_Ptr(2,V(1))
	fail_ret
	call_c   Dyam_Reg_Load_Ptr(4,V(2))
	fail_ret
	call_c   oset_delete(R(2),R(4))
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(4))
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun10()

pl_code global pred_number_to_oset_2
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(2)
	call_c   Dyam_Choice(fun8)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Reg_Load_Number(2,V(1))
	fail_ret
	call_c   oset_insert(R(2),0)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(3))
	fail_ret
	call_c   Dyam_Cut()
fun7:
	call_c   Dyam_Unify(V(2),V(3))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret


pl_code global pred_oset_register_2
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(2)
	call_c   Dyam_Choice(fun5)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Reg_Load_Ptr(2,V(1))
	fail_ret
	call_c   oset_register(R(2))
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(3))
	fail_ret
	call_c   Dyam_Cut()
fun4:
	call_c   Dyam_Unify(V(3),V(2))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret


pl_code global pred_oset_insert_3
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(3)
	call_c   Dyam_Reg_Load_Number(2,V(1))
	fail_ret
	call_c   Dyam_Reg_Load_Ptr(4,V(2))
	fail_ret
	call_c   oset_insert(R(2),R(4))
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(3))
	fail_ret
	call_c   Dyam_Reg_Deallocate(3)
	pl_ret

pl_code global pred_oset_list_2
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(2)
	call_c   Dyam_Reg_Load_Ptr(2,V(1))
	fail_ret
	call_c   oset_list(R(2))
	move_ret R(0)
	call_c   Dyam_Reg_Unify(0,V(2))
	fail_ret
	call_c   Dyam_Reg_Deallocate(2)
	pl_ret

pl_code global pred_oset_list_alt_2
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(2)
	call_c   Dyam_Reg_Load_Ptr(2,V(1))
	fail_ret
	call_c   oset_list_alt(R(2))
	move_ret R(0)
	call_c   Dyam_Reg_Unify(0,V(2))
	fail_ret
	call_c   Dyam_Reg_Deallocate(2)
	pl_ret

pl_code global pred_oset_void_1
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Unify_Cst(0,N(0))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret


;;------------------ VIEWERS ------------

c_code local build_viewers
	c_ret

pl_code local fun11
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(4),N(0))
	fail_ret
	pl_jump  fun10()

pl_code local fun8
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(3),N(0))
	fail_ret
	pl_jump  fun7()

pl_code local fun5
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(3),N(0))
	fail_ret
	pl_jump  fun4()


;;------------------ INIT POOL ------------

c_code local build_init_pool
	c_ret

long local ref[0]
long local seed[0]

long local _initialization

c_code global initialization_dyalog_oset
	call_c   Dyam_Check_And_Update_Initialization(_initialization)
	fail_c_ret
	call_c   build_init_pool()
	call_c   build_viewers()
	call_c   load()
	c_ret

