/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 1998, 2002, 2003, 2004, 2005, 2007, 2008, 2010, 2011, 2012, 2016, 2018 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  lpda.pl -- From Programs to LPDAs
 *
 * ----------------------------------------------------------------
 * Description
 * 
 * ----------------------------------------------------------------
 */

:-include 'header.pl'.

get_clause(Clause) :-
	recorded( '$clause'(Clause) )
	.

get_clause(Clause) :-
	'$answers'( Clause::register_predicate(_) )
	.

build_cond_loader(Id,Cond,Trans,'$loader'(Loader)) :-
	my_numbervars(Cond,Id,_),
	handle_choice(noop,Loader,Last,ContA,ContB),
	trans_unfold(Trans,UTrans),
	body_to_lpda(Id,Cond,UTrans :> fail, ContA,rec_prolog),
	body_to_lpda(Id,true,Last,ContB,rec_prolog)
	.

%% :-std_prolog trans_unfold/2.
%% +pattern: trans_unfold(Code,UnfoldedCode)
%% +desc: used to unfold ALTERNATIVES around FIRST
%% +desc: the case may arise for instance for TIG right aux trees with disjunction

trans_unfold(Code,UnfoldedCode) :-
	( Code = '*ALTERNATIVE*'(Code1,Code2) :> Code3 ->
	    trans_unfold(Code1 :> Code3,UnfoldedCode1),
	    trans_unfold(Code2 :> Code3,UnfoldedCode2),
	    UnfoldedCode = UnfoldedCode1 :> UnfoldedCode2
	;   Code = '*ALTERNATIVE*'(Code1,Code2) ->
	    trans_unfold(Code1,UnfoldedCode1),
	    trans_unfold(Code2,UnfoldedCode2),
	    UnfoldedCode = UnfoldedCode1 :> UnfoldedCode2
	;   
	    UnfoldedCode = '*OBJECT*'(Code)
	)
	.

%%% Load on condition
pgm_to_lpda( '$loader'(Cond,Clause), Loader) :-
	pgm_to_lpda(Clause,Trans),
	build_cond_loader(Id,Cond,Trans,Loader)
	.

%%% Initial Item (if query)
pgm_to_lpda( '$query'(_,_), '*CITEM*'(start,start) ) :-
	record_without_doublon( main_module )
	.

%%% Compilation of the Query (if any)
pgm_to_lpda( '$query'(Query,Answer_Subst), '*FIRST*'(start) :> Trans ) :-
	my_numbervars(Query,Id,_),
	body_to_lpda(Id, Query, '*ANSWER*'{ answer=>Answer_Subst }, Trans, dyalog).

%%% Compilation of "Call" Clauses
register_predicate(_).

pgm_to_lpda( register_predicate(F/N), '*PROLOG-FIRST*'(Call) :> Cont ) :-
	( recorded( public(F/N) ) xor recorded( public('*default*'))),
	functor(T,F,N),
	my_numbervars(T,Id,_),
	Call = '$call'(Body),
	(   Body = T ; Body = '$answers'(T) ),
	body_to_lpda(Id,Body,'*PROLOG-LAST*',Cont,prolog)
	.

pgm_to_lpda( register_predicate(Pred::F/N), '*GUARD*'(Call) :> Cont ) :-
	(   recorded( public(Pred) ) xor recorded( public('*default*') ) ),
	functor(T,F,N),
	(   rec_prolog(Pred)
	xor std_prolog(Pred)
	xor light_tabular(Pred)
	xor extensional(Pred)
	xor foreign(Pred,_)
	),
	my_numbervars(T,Id,_),
	Call = '$pcall'(Body),
	(   Body = T ; Body = '$answers'(T) ),
	body_to_lpda(Id,Body,noop,Cont,rec_prolog)
	.


%%% Compilation of the Facts
pgm_to_lpda( '$fact'(T1), Trans ) :-
	my_numbervars(T1,Id,_),
	functor(T1,F1,N),	
	term_current_module_shift(F1/N,F/N,T1,T),
	( rec_prolog(F/N) ->
	    register_predicate(F/N),
	    Trans='*GUARD*'(T) :> noop
	;   prolog(F/N) ->
	    register_predicate(F/N),
	    Trans='*PROLOG-FIRST*'(T) :> '*PROLOG-LAST*'
	;   std_prolog(F/N) ->
	    register_predicate(F/N),
	    T =.. [_|Args],
	    Trans='*STD-PROLOG-FIRST*'(F/N,Args,deallocate_layer)
	;   light_tabular(F/N) ->
	    register_predicate(F/N),
	    make_callret(T,Call,Return),
	    Trans = '*FIRST*'(Call) :> '*LIGHTLAST*'(Return)
	;   extensional(F/N) ->
	    register_predicate(F/N),
	    Trans='*DATABASE*'(T)
	;   foreign(F/N,_) ->
	    error( 'Builtin predicate not accepted as rule head: ~w',[T])
	;   builtin(F/N,_) ->
	    error( 'Builtin predicate not accepted as rule head: ~w',[T])
	;   T = wait(_) ->
	    error( 'Builtin predicate not accepted as rule head: ~w',[T])
	;   T = '$answers'(_) ->
	    error( 'Builtin predicate not accepted as rule head: ~w',[T])
	;
	    register_predicate(F/N),
	    make_callret(T,Call,Return),
	    Trans = '*FIRST*'(Call) :> '*LAST*'(Return)
	).

%%% Compilation of the Clauses
pgm_to_lpda( Clause::(Head :- Body), Trans ) :-
	clause_to_lpda( Clause, Trans).

clause_to_lpda( Clause::(Head1 :- Body),Trans) :-
	my_numbervars( Clause, Id,_),
	functor( Head1,F1,N ),
	term_current_module_shift(F1/N,F/N,Head1,Head),
	( extensional( F/N ) ->
	    error( '~w declared as extensional but defined by clause ~w',
		   [F/N,(Head:-Body)])
	;   rec_prolog(F/N) ->
	    register_predicate(F/N),	
	    Trans = '*GUARD*'(Head) :> Cont,
	    body_to_lpda(Id,Body,noop,Cont, rec_prolog)
	;   prolog(F/N) ->
	    register_predicate(F/N),	
	    Trans = '*PROLOG-FIRST*'(Head) :> Cont,
	    body_to_lpda(Id,Body,'*PROLOG-LAST*',Cont,prolog)
	;   std_prolog(F/N) ->
	    register_predicate(F/N),
	    Head =.. [_|Args],
	    Trans = '*STD-PROLOG-FIRST*'(F/N,Args,Cont),
	    body_to_lpda(Id,Body,deallocate_layer,Cont,rec_prolog)
	;   light_tabular(F/N) ->
	    register_predicate(F/N),
	    make_callret(Head,Call,Return),
	    Trans = '*FIRST*'(Call) :> Cont,
	    body_to_lpda(Id,Body,'*LIGHTLAST*'(Return),Cont,rec_prolog)
	;   ( foreign(F/N,_)
	    ;	builtin(F/N,_)
	    ;	Head = wait(_)
	    ;	Head = '$answers'(_)
	    ) ->
	    error( 'Not a valid rule head: ~w',[Head])
	;   
	    register_predicate(F/N),	
	    make_callret(Head,Call,Return),
	    Trans = '*FIRST*'(Call) :> Cont,
	    body_to_lpda(Id,Body,'*LAST*'(Return),Cont,dyalog)
	)
	.

%%% Compilation of body
%%% Type:
%%%     * dyalog : when clause head predicate is of type dyalog
%%%     * prolog : when clause head predicate is of type prolog
%%%     * answer(Type) : used when encountering an '$answer' call in mode Type

body_to_lpda(Id, Body1 , Cont, Trans, Type) :-
	(   toplevel(term_expand(Body1,Body)),
	    my_numbervars(Body,Id,_)
	xor Body=Body1 ), % *HOOK*
	( body_to_lpda_handler(Id,Body,Cont,Trans,Type) ->
	    true
	;   
	    litteral_to_lpda(Id, Body, Cont, Trans,Type)
	)
	.

:-rec_prolog body_to_lpda_handler/5.

body_to_lpda_handler(Id,Body::'$VAR'(_),Cont,Trans,Type) :-
	metacall_initialize,
	( Type = answer(Type2) ->
	    Goal = '$answer'(Body)
	;   
	    Goal = Body,
	    Type2 = Type
	),
	lpda_meta_call(Type2,Goal,Call),
	litteral_to_lpda(Id,Call, Cont, Trans, Type)
	.

:-extensional lpda_meta_call/3.

lpda_meta_call(rec_prolog,Goal,call!pcall(Goal)).
lpda_meta_call(prolog,Goal,call!call(Goal)).
lpda_meta_call(dyalog,Goal,call!call(Goal)).

body_to_lpda_handler(Id,(A=B),Cont,Trans,Type) :-
	destructure_unify(A,B,Cont,Trans).

body_to_lpda_handler(Id,(A,B),Cont,Trans,Type) :-
	body_to_lpda(Id,B,Cont,ContB,Type),
	body_to_lpda(Id,A,ContB,Trans,Type)
	.

body_to_lpda_handler(Id,(A->B;C),Cont,Trans,Type) :-
	body_to_lpda(Id, (('*COMMIT*'(A),B) ; C), Cont, Trans, Type )
	.

body_to_lpda_handler(Id,(A->B),Cont,Trans,Type) :-
	body_to_lpda(Id, (A -> B ; fail), Cont, Trans, Type)
	.

body_to_lpda_handler(Id,(A;B),Cont,Trans,Type) :-
	\+ A = (_ -> _),
	handle_choice(Cont,Trans,Last,ContA,ContB),
	body_to_lpda(Id,A,Last,ContA,Type),
	body_to_lpda(Id,B,Last,ContB,Type)
	.

body_to_lpda_handler(Id,'*COMMIT*'(A),Cont,'*SETCUT*' :> ContA,Type) :-
	body_to_lpda(Id,A,'*CUT*' :> Cont,ContA,rec_prolog)
	.


body_to_lpda_handler(Id,phrase(A,Left,Right),Cont,Trans,Type) :-
	bmg_uniform_stacks([],BMG),
	dcg_body_to_lpda(Id,A,Left,Right,BMG,BMG,Cont,Trans,Type)
	.


body_to_lpda_handler(Id,tag_phrase(A,Left,Right),Cont,Trans,Type) :-
	tag_normalize_and_compile(Id,A,Left,Right,Cont,Trans,Type),
	true
	.

body_to_lpda_handler(Id,rcg_phrase(A),Cont,Trans,Type) :-
	rcg_body_to_lpda(Id,A,Cont,Trans,Type)
	.

body_to_lpda_handler(Id,fail,Cont,fail :> Cont,Type).

body_to_lpda_handler(Id,true,Cont,Cont,Type).

body_to_lpda_handler(Id,'$internal_tail'(Tail),Cont,('*INTERNAL_TAIL*'(Tail) :> Cont),Type).

body_to_lpda_handler(Id,Body::'$interface'( Template, Options ),Cont,(Body :> Cont),Type).

%% Should modify the syntax of wait(Task),Rest
%% into Task,wait,Rest
body_to_lpda_handler(Id,wait(Task),Cont,('*WAIT*'(Cont) :> ContTask),Type) :-
	body_to_lpda(Id,Task,fail,ContTask,Type)
	.

body_to_lpda_handler(Id,wait,Cont,'*WAIT*'(Cont) :> fail,Type).

body_to_lpda_handler(Id,'$answers'(A),Cont,Trans,Type) :-
	body_to_lpda(Id,A,Cont,Trans,answer(Type)).

body_to_lpda_handler(Id,Body::'$toplevel'(_),Cont,Trans,Type) :-
	try_compiler_toplevel(Body,A),
	body_to_lpda(Id,A,Cont,Trans,Type)
	.

try_compiler_toplevel('$toplevel'(A),X) :-
	( A = X^AA -> true ; AA=A,X=true),
	(   toplevel(AA) xor error('call to toplevel failed: ~w',[A]) )
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Kleene Handling

%% @case Kleene Star (A @*)
body_to_lpda_handler(Id,(AA @*),Cont,Trans,Type) :-
	(   AA = Vars ^ A xor AA = A),
	body_to_lpda_handler(Id,@*{goal => A, vars=>Vars},Cont,Trans,Type)
	.

%% @case Range Kleene Star @*{goal=>A}
body_to_lpda_handler(Id,
		     K,
		     Cont,Trans,Type
		    ) :-
%%	format('Handling Kleene ~w\n',[Body]),

	kleene_normalize(K,
			 KleeneStruct:: @*{ goal => A,
					    vars => Vars,
					    from => From,
					    to => To,
					    collect_first=>UserFirst,
					    collect_loop=>UserParam,
					    collect_next=>UserArg,
					    collect_last=>UserLast
					  }),
	
	update_counter(kleene, KleeneId),
	name_builder('_kleene~w',[KleeneId],KleeneLabel),
	kleene_conditions(KleeneCond::[From,To],[Counter,CounterInc],InitCond,ExitCond,LoopCond),
	my_numbervars([Counter,CounterInc,InitCond,ExitCond,LoopCond,KleeneStruct],Id,_),
	Args = [Counter|UserParam],
	Loop_Args = [CounterInc|UserArg],
	tupple(Vars,TA),
	tupple(Cont,TCont),
	extend_tupple([UserLast,KleeneCond],TCont,RTCont),
	body_to_lpda_handler(Id,
		     (	 ExitCond,UserLast=UserParam),
		     Cont,
		     ContExit,
		     Type
		    ),
	body_to_lpda_handler(Id,
		     (	 LoopCond, A ),
		     '*KLEENE-LAST*'(KleeneLabel,
				     Loop_Args,
				     Args,
				     TA,
				     RTCont,
				     noop
				    ),
		     ContLoop,
		     Type),
	Trans1 = '*KLEENE*'(KleeneLabel,
			    TA,
			    '*KLEENE-ALTERNATIVE*'(ContExit,ContLoop)
			    ),
	body_to_lpda_handler(Id,
		     (	 InitCond,UserFirst=UserParam),
		     Trans1,
		     Trans,
		     Type),
%%	format('Kleene ~K\n',[Trans]),
	true
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

:-extensional terminal_cont/1.

terminal_cont('*SA-PROLOG-LAST*').          % last call prolog mode for SA
terminal_cont('*PROLOG-LAST*').             % last call prolog mode
terminal_cont(deallocate_layer).            % last call std_prolog mode

handle_choice(Cont,Trans,Last,ContA,ContB) :-
	( terminal_cont(Cont) ->
	    Last=Cont,
	    Trans= '*ALTERNATIVE*'(ContA,ContB)
	;   
	    tupple(Cont,Tupple),
	    ( Cont = '*JUMP*'(_,Tupple) ->
	      Last = Cont,
	      Trans = '*ALTERNATIVE*'(ContA,ContB)
	    ;
	      mutable(M,'*JUMP*'(Cont)),
	      Last = '*JUMP*'(M,Tupple),
	      Trans = '*ALTERNATIVE*'(ContA,ContB)
	    )
	)
	.


%	term_current_module_shift(A1,A),

xtagop(Tag,A,XA) :-
	(   tagop(Tag,A,XA)
	xor XA= '$tagop'(Tag,A)
	xor Tag=[], XA=A
	)
	.

%% Add error case if Cont <> *FAIL* and A not a guard predicate
litteral_to_lpda(Id, AA, Cont, Trans, Type) :-
	xtagop(Tag,A1,AA),
	functor(A1,F1,N),
	Pred1=F1/N,
	term_current_module_shift(Pred1,Pred::F/N,A1,A),
	( foreign(Pred,Fun) ->
%%	  format('Test foreign pred1=~w pred=~w\n',[Pred1,Pred]),
	  ( foreign_void_return(Pred) ->
%%	    format('Found foreign void return ~w\n',[Pred]),
	    Trans = foreign_void_return(A,Fun) :> Cont
	  ;
	    Trans = foreign(A,Fun) :> Cont
	  )
	;   builtin(Pred,Fun) ->
	    Trans = builtin(A,Fun) :> Cont
	;   Type = answer(_) ->
	    registered_predicate(Pred),
	    make_callret(A,Call,Return),
	    ( light_tabular(Pred) ->
		Trans = '*PSEUDOLIGHTNEXT*'(Call,Return,deallocate :> succeed,Tag) :> Cont
	    ;	
		Trans = '*PSEUDONEXT*'(Call,Return,Cont,Tag)
	    )
	;   compiler_extension(Pred,M_L) ->
	    try_litteral_expansion(A,M_L,Expanded_A,Id),
	    body_to_lpda(Id,Expanded_A,Cont,Trans,Type)
	;   rec_prolog(Pred) ->
	    registered_predicate(Pred),
	    Trans = '*GUARD*'(A) :> Cont
	;   prolog(Pred) ->
	    registered_predicate(Pred),
	    (	Type \== rec_prolog xor error('Not a recursive prolog predicate ~w',[A]) ),
	Trans = '*PROLOG*'{call=>A,right=>Cont}
/*
         %% Do not seem interesting to wrap Prolog predicates
	 decompose_args(prolog(F/N),A,Args),
         wrapping_predicate(prolog(F/N)),
	 Trans = '*WRAPPER-CALL*'(prolog(F/N),Args,Cont)
*/
	;   std_prolog(Pred) ->
	    registered_predicate(Pred),
	    A =.. [_|Args],
	    Trans= '*STD-PROLOG-CALL*'(Pred,Args) :> Cont
	;   extensional(Pred) ->
	    registered_predicate(Pred),
	    litteral_to_lpda(Id, recorded(A), Cont, Trans, Type)
	;   Type == rec_prolog, \+ light_tabular(Pred) ->
	error( 'Not a recursive prolog predicate ~w', [A])
	;   
	    registered_predicate(Pred),
	    make_callret(A, Call, Return ),
	    ( try_lco(Pred,Tag,Call,Return,Cont,Trans) ->
		true
	    ;	
		decompose_args(Pred,[A,Tag],Args),
		( check_lc(Pred) ->	
		    Pred2 = lc(Pred),
		    update_counter(Pred2,_)
		;   
		    Pred2=Pred
		),
		wrapping_predicate( Pred2 ),
		Trans = '*WRAPPER-CALL*'(Pred2,Args,Cont)
	    )
	)
	.

decompose_args(prolog(F/N),[T,Tag],Args) :-
	functor(T,F,N),
	( N > 5 -> Args = [T,Tag] ; T =.. [_|Args1], Args = [Tag|Args1] )
	.

decompose_args(F/N,[T,Tag],Args) :-
	functor(T,F,N),
	( N > 5 -> Args = [T,Tag] ; T =.. [_|Args1], Args = [Tag|Args1] )
	.

:-std_prolog try_lco/6.

try_lco(Pred,[],Call,Return,Cont,Trans) :-
	lco(Pred),
	(   light_tabular(Pred),
	    '*LIGHTLAST*'(Return) == Cont ->
	    Trans = '*LIGHTLASTCALL*'(Call)
	;   '*LAST*'(Return) == Cont ->
	    Trans = '*LASTCALL*'(Call)
	;   
	    fail
	)
	.

wrapping_predicate(Pred:: (F/N)) :-
	decompose_args(Pred,[A,Tag],Args),
	make_callret(A,C,R),
	my_numbervars([Closure,A,Tag],_,_),
	( light_tabular(Pred) ->
	      Body = '*LIGHTNEXTALT*'(C,R,'*PROLOG-LAST*',Tag)
	; recorded(nabla_variance(Pred)) ->
	  Body = '*NEXTALT2*'(C,R,'*PROLOG-LAST*',Tag)
	; recorded(nabla_subsume(Pred)) ->
	  Body = '*NEXTALT*'(C,R,'*PROLOG-LAST*',Tag)
	;   
	Body = '*NEXT*'{ call=>C, ret=>R, right=>'*PROLOG-LAST*', nabla=>Tag }
	),
	record( '*WRAPPER*'(Pred,
			    [Closure|Args],
			    Body
			   ) )
	.

wrapping_predicate(Pred::lc(F/N)) :-
	decompose_args(F/N,[A,Tag],Args),
	make_callret(A,C,R),
	my_numbervars([Closure,A,Tag,LC],_,_),
	body_to_lpda(Id,'internal_lc'(C,LC),noop,LCInit,rec_prolog),
	record( '*WRAPPER*'(Pred,
			    [Closure|Args],
			    '*LCNEXT*'(C,R,'*PROLOG-LAST*',Tag,C^LC^LCInit)
			   ) )
	.


wrapping_predicate(Pred::prolog(F/N)) :-
	decompose_args(Pred,[A,Tag],Args),
	my_numbervars([Closure,A,Tag],_,_),
	record( '*WRAPPER*'(Pred,
			    [Closure|Args],
			    '*PROLOG-LAST*'(A,Tag)
			   )
	      ).

:-std_prolog try_litteral_expansion/4.

try_litteral_expansion(A,M_L,Body,Id) :-
	mutable_read(M_L,L),
	( domain( (A:-Body) , L ) ->
	    %% By hypothese, A is ground => Head=A similar to subsumes_chk
	    %%	    format('Found expansion ~w ~w:-~w\n',[A,Head,Body]),
	    my_numbervars(Body,Id,_)
	;   
	    error('No compiler expansion for ~w',[A])
	)
	.

registered_predicate(Spec) :-
	( recorded( predicate(Spec,_) )
	xor warning( 'call to undefined predicate ~w', [Spec]))
	.

%% Generate unification code for unifying A and B
%% A and B are numbervar-ized
destructure_unify(A,B,Cont,Trans) :-
	(destructure_unify_aux(A,B,Cont,Trans) xor Trans= (fail :> Cont)),
	abolish( destructure_unify/2 )
	.

:-std_prolog destructure_unify_aux/4.
:-rec_prolog destructure_unify_args/4.
	
destructure_unify_aux(A,B,Cont,Trans) :-
	( A==B ->
	    Trans = Cont
	;   recorded( destructure_unify(A,AA) ) ->
	    destructure_unify_aux(AA,B,Cont,Trans)
	;   recorded( destructure_unify(B,BB) ) ->
	    destructure_unify_aux(A,BB,Cont,Trans)
	;   check_tupple(A) ->
	    oset_tupple(A,TA),
	    destructure_unify_aux(B,TA,Cont,Trans)
	;   (check_var(A) ; check_deref_var(A)) ->
	    record( destructure_unify(A,B) ),
	    Trans = unify(A,B) :> Cont
	;   (check_var(B) ; check_deref_var(B)) ->
	    record( destructure_unify(B,A) ),
	    Trans = unify(B,A) :> Cont
	;   (atomic(A) ; atomic(B)) ->
	    fail
	;   functor(A,F,N),functor(B,F,N) ->
	    ( A @< B -> record( destructure_unify(A,B) )
	    ;	record( destructure_unify(B,A) )),
	    A =.. [_|A_Args], B =.. [_|B_Args],
	    destructure_unify_args(A_Args,B_Args,Cont,Trans)
	;   
	    fail
	).

destructure_unify_args([],[],Cont,Cont).
destructure_unify_args([A|LA],[B|LB],Cont,Trans) :-
	destructure_unify_args(LA,LB,Cont,Cont2),
	destructure_unify_aux(A,B,Cont2,Trans)
	.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Modules

term_module_shift(Module,Pred1::F1/N,F/N,T1,T) :-
	%%		format(';; try shift ~w ~w\n',[Module,Pred1]),
	( recorded( module_import(Pred1,ModuleX) ) ->
	    %% imported predicate: only shift if Module1 is not []
	    ModuleX \== [],
	    FX=F1
	;   Module \== [],         %% only try to shift if not in [] module
	    atom_module(F1,Module2,F2),
	    ( Module2 == [] ->
		\+ foreign(Pred1,_),   %% and is not some kind of builtin predicate
		\+ builtin(Pred1,_),
		\+ recorded( predicate(Pred1,builtin) ),
		\+ compiler_extension(Pred1,_),
		ModuleX=Module,
		FX = F1
	    ;	\+ recorded( module_import(F2/N,Module2)),
		%% functors with module are not shifted when imported
		Module \== Module2,
		%% functors within current module are not shifted
		ModuleX = Module,
		FX=F1
	    )
	),
	%% we deep shift
	deep_module_shift(FX,ModuleX,F),
	length(Args,N),
	T1 =.. [F1|Args],
	T =.. [F|Args],
	%% format(';; shifted ~w ~w ~w\n',[F/N,T1,T]),
	true
	.
	    
	
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Meta Call '$call'

metacall_initialize :-
	read_files('call.pl',require)
	.

