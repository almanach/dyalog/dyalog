/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2002, 2003, 2004, 2007, 2008, 2009, 2011 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  tagguide_compile.pl -- TAG guide
 *
 * ----------------------------------------------------------------
 * Description
 *  Compile a TIG guide for TAG, with non terminal stripped of arguments
 * ----------------------------------------------------------------
 */
:-include 'header.pl'.
:-require 'tag_compile.pl'.
:-require 'tig.pl'.
:-require 'tag_dyn.pl'.
:-require 'tag_maker.pl'.
:-require 'tag_normalize.pl'.

:-rec_prolog tagguide_compile_tree/6.

:-light_tabular tagguide_pred/2.
:-mode(tagguide_pred/2,+(+,-)).

tagguide_pred(A,SA) :-
	deep_module_shift(A,strip,SA),
	record_without_doublon( adjkind( SA/0, left ) ),
	record_without_doublon( adjkind( SA/0, right ) )
	.

:-light_tabular tagguide_name/3.
:-mode(tagguide_pred/3,+(+,+,-)).

tagguide_name(A,Mode,SA) :-
	( Mode == no ->
	    S1 = A
	;
	    deep_module_shift(A,Mode,S1)
	),
	deep_module_shift(S1,strip,SA)
	.

:-std_prolog tagguide_strip/3.

:-rec_prolog tagguide_strip_handler/3.

tagguide_strip(A,Mode,SA) :-
	( tagguide_strip_handler(A,Mode,SA) ->
%%	    format('STRIP ~w ~w\n\t=> ~w\n',[A,Mode,SA]),
	    true
	;   %% by default, nodes are removed
%%	    format('STRIP ~w ~w FAILED\n',[A,Mode]),
	    fail
	)
	.

tagguide_strip_handler([],_,[]).
tagguide_strip_handler([A|L],Mode,S) :-
	( tagguide_strip(A,Mode,SA) ->
	    ( tagguide_strip(L,Mode,SL) ->
		S = [SA|SL]
	    ;	
		S = [SA]
	    )
	;
	    tagguide_strip(L,Mode,S)
	)
	.

tagguide_strip_handler(N::tag_node{ id => Id,
				    label => A,
				    kind => Kind,
				    spine => Spine,
				    adj => Adj,
				    adjleft => AdjLeft,
				    adjright => AdjRight,
				    adjwrap => AdjWrap,
				    children => Children,
				    top => Top,
				    bot => Bot
				  },
		       Mode,
		       SN::tag_node{ id => Id,
				     label => SA,
				     kind => SKind,
				     spine => Spine,
				     adj => Adj,
				     adjleft => AdjLeft,
				     adjright => AdjRight,
				     adjwrap => AdjWrap,
				     children => SChildren,
				     top => STop,
				     bot => SBot
				   }
		      ) :-
	domain(Spine,[yes,Mode]),
	( domain(Kind,[anchor,coanchor]) ->
	    %% Anchor and coanchor nodes are not stripped
	    %% however adj is possible on these nodes
	    %% => we add a stripped parent node to allow adj (when needed)
	    ( (Adj = no ; \+ Top = Bot ) ->	
		SN = N
	    ;	SChildren = [ tag_node{ id => Id,
					label => A,
					kind => Kind,
					adj => no,
					spine => Spine,
					children => [],
					top => Top,
					bot => Bot
				      }
			    ],
		tagguide_pred(A,SA),
		STop = SA,
		SBot = SA,
		SKind = std
	    )
	;   Kind == scan ->
	    %% empty scan nodes are removed
	    %% A = [_|_],
	    SN=N
	;   Kind == escape  ->
	    %% escape nodes are removed
	    SN = N
%%	    fail
	;   tagguide_pred(A,SA),
	    STop = SA,
	    SBot = SA,
	    SKind = Kind,
	    tagguide_strip(Children,Mode,SChildren)
	)
	.

tagguide_strip_handler( (A;B),
			Mode,
			S ) :-
	tagguide_strip([A,B],Mode,L),
	( L = [SX] ->
	    S = guard{ goal => SX, plus => true, minus => true }
	;   
	    L = [SA,SB],
	    S = (SA;SB)
	)
	.

tagguide_strip_handler( (A ## B),
			Mode,
			S ) :-
	tagguide_strip([A,B],Mode,L),
	(   L = [S] xor L = [SA,SB], S = (SA ## SB) )
	.

tagguide_strip_handler( '$answers'(A),
			Mode,
			'$answers'(SA) ) :-
			tagguide_strip(A,Mode,SA)
			.

tagguide_strip_handler( guard{ goal => A, plus => Plus, minus => Minus },
			Mode,
			S ) :-
	%%	S = guard{ goal => SA, plus => Plus, minus => Minus }
	tagguide_strip(A,Mode,SA),
	( Minus == fail ->
	    S = SA
	;   Plus == fail ->
	    S = guard{ goal => SA, plus => fail, minus => true }
	;   
	    S = guard{ goal => SA, plus => true, minus => true }
	)
	.

tagguide_strip_handler( G::'$pos'(Pos), Mode, G ).

tagguide_strip_handler( @*{ goal => A,
			    from => From,
			    to => To
			  },
			Mode,
			@*{ goal => SA,
			    vars => [],
			    from => From,
			    to => To,
			    collect_first => [],
			    collect_loop => [],
			    collect_next => [],
			    collect_last => []
			  }
		      ) :-
	tagguide_strip(A,Mode,SA)
	.

pgm_to_lpda( T::tag_tree{ name=> Name1, family=> Family }, Trans ) :-
	guide(strip),
%%	format('norm ~w ->\n',[T]),
	normalize(T,L::tag_node{ spine => Spine} ),
	(   recorded( Info1::tag_anchor{ family => Family } )
	xor Info1 = tag_anchor{ family => Family }
	),
	Info = strip(Info1),
	( Spine == no ->
	    Mode = no
	;   recorded( tig(Name1,Mode) ) ->
	    true
	;   
	    domain(Mode,[left,right])
	),
	tagguide_name(Name1,Mode,Name),
%%	format('HERE mode=~w id=~w\n',[Mode,Name]),
	tagguide_strip(L,Mode,SL),
	( domain(Mode,[left,right]) ->
	    record_without_doublon( tig(Name,Mode) )
	;
	    true
	),
	record_without_doublon(guide(Name,Mode,Name1)),
	tag_numbervars(Info,Name,_),
%%	format('TAG STRIP mode=~w => ~w\n',[Mode,SL]),
	tagguide_compile_tree(Name,SL,Trans1,dyalog,Info,Mode),
	install_loader(Name,
		       Loader^tag_autoloader(Name,SL,Info,Loader,_,_),
		       Trans1,
		       Trans)
	.

%% ----------------------------------------
%% No longer used
%% but generic predicate to be moved somewhere else

:-std_prolog reset_numbervars/2.

reset_numbervars(Id,M) :-
	( recorded( numbervars(Id,MM) ) ->
	    mutable(MM,M)
	;
	    mutable(MM,M),
	    record( numbervars(Id,MM) )
	)
	.

%%--------------------------------------

tagguide_compile_tree(Id,Tree::tag_node{ label=> NT, spine => Spine, top => A, bot => B },Trans,Type,Info,Mode) :-
%%	format('Trying TAGGUIDE id=~w mode=~w info=~w\n',[Id,Mode,Info]),
	tree_info(Tree,Info),
	tag_numbervars(Left,Id,_),
	tag_numbervars(Right,Id,_),
	tag_numbervars(Tree,Id,_),
	functor(A,F,N),
	register_predicate(tag(F/N)),
	( Spine == yes ->
	    functor(Foot_Top,F,N),
	    ( Mode == left ->
		make_tig_callret(Mode,A,Foot_Top,Left,Foot_Right,Call,Return),
		XInfo = Info * left_tig(Foot_Top,Foot_Right,Return),
		Trans = '*SAFIRST*'(Call) :> Cont,
		Last = noop, %% Return is ensured by foot node
	      tupple([],Seen)
	    ;	Mode == right ->
		make_tig_callret(Mode,Foot_Top,A,Foot_Left,Right,Call,Return),
		XInfo = Info * right_tig(Foot_Top,Foot_Left,Call),
		Trans = Cont,
		Last = '*GUIDE-LAST*'(Return,Id),
	      tupple([],Seen)
	    ;	error('Not a TAGGUIDE mode in {left,right} for tree ~w',[Id])
	    ),
	    tag_compile_subtree_noadj(Id,Tree,Left,Right,Last,Cont,Type,XInfo,Seen)
	;
	    make_tag_subst_callret(A,Left,Right,Call,Return),
	    Trans = '*SAFIRST*'(Call) :> Cont,
	    Last = '*GUIDE-LAST*'(Return,Id),
	  tupple(Call,Seen),
	    tag_compile_subtree(Id,Tree,Left,Right,Last,Cont,Type,Info,Seen)
	),
%%	format('TAGGUIDE TRANS ~w ~w: ~w\n\n',[Id,Mode,Trans]),
	true
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Extension of tag_dyn.pl


unfold( '*GUIDE*'(C,Tree,Right), Env, T, C2, Out_Env) :-
	seed_install( seed{ model=>'*RITEM*'(C,Tree) :> Right(Env)(T),
			    tabule => light,
			    schedule => prolog,
			    out_env => [Out_Env] 
			  },
		      2,
		      C2 )
	.

unfold( '*GUIDE-LAST*'(Return,Tree),
	Env,
	T,
	C2 :> C1,
	Env) :-
	seed_install( seed{ model=>'*RITEM*'(Env,Return)}, 1, C2 ),
	seed_install( seed{ model=>'*RITEM*'(Env,Tree),
			    schedule => no
			  }, 1, C1 )
	.


