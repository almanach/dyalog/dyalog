/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 1999, 2002, 2003, 2004, 2005, 2008, 2011, 2021 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  dcg.pl -- Compilation of DCG into LPDA
 *
 * ----------------------------------------------------------------
 * Description
 *
 *     Extension of file lpda.pl to deal with Definite Clause Programs
 *     Actually implements an extension of DCGs called BMG for
 *     Bound Movement Grammars
 *
 * ----------------------------------------------------------------
 */
 
:-include 'header.pl'.

:-extensional parse_mode/1.

pgm_to_lpda( grammar_rule(dcg,Rule :: (THead --> Body)) , Trans ) :-
    my_numbervars( Rule , Id, _),
    my_numbervars( Left, Id, _),
    my_numbervars( Right, Id, _),

    %% The head may be wrapped with a trail:  Head,Trail --> Body
    ( THead = (BMG_Head,Trail) ->
	( Trail == [] ->
	    XTrail = Right
	;   
	    my_numbervars(XTrail,Id,_)
	)
    ;	BMG_Head = THead, XTrail=Right, Trail = []
    ),

    %% The head may be wrapped with a BMG push: Head Stack NT --> Body
    ( BMG_Head =.. [Stack, Head1, NT1],	bmg_stack(Stack) ->
	functor(NT1,NT1_F,NT_N),
	term_current_module_shift(dcg(NT1_F/NT_N),_,NT1,NT),
%%	format( 'BMG Push stack ~w\n', [BMG_Head]),
	true
    ;	Head1 = BMG_Head, Stack=[], NT=[]
    ),
    bmg_head_stacks(Stack,NT,BMG_Left,BMG_Right,BMG_Inside_Left,BMG_Inside_Right),
%%    format('BMG stacks\n\t~w\n\t~w\n\t~w\n\t~w\n',[BMG_Left,BMG_Right,BMG_Inside_Left,BMG_Inside_Right]),
    my_numbervars(BMG_Left,Id,_),
    my_numbervars(BMG_Right,Id,_),    
    functor( Head1, F1, N ),
    term_current_module_shift(dcg(F1/N),Pred,Head1,Head),
    ( rec_prolog( Pred::dcg(F/N) ) ->
	register_predicate(Pred),
	dcg_prolog_make(Pred,Head,Left,Right,BMG_Left,BMG_Right,Dcg_Head),
	Trans='*GUARD*'(Dcg_Head) :> Cont,
	dcg_trail_to_lpda(Id,Trail,Right,XTrail,noop,TrailCont,rec_prolog),
	dcg_body_to_lpda(Id,Body,Left,XTrail,BMG_Inside_Left,BMG_Inside_Right,TrailCont,Cont,rec_prolog)
    ;	prolog( Pred ) ->
	register_predicate(Pred),
	dcg_prolog_make(Pred,Head,Left,Right,BMG_Left,BMG_Right,Dcg_Head),
	Trans='*PROLOG-FIRST*'(Dcg_Head) :> Cont,
	dcg_trail_to_lpda(Id,Trail,Right,XTrail,'*PROLOG-LAST*',TrailCont,prolog),
	dcg_body_to_lpda(Id,Body,Left,XTrail,BMG_Inside_Left,BMG_Inside_Right,TrailCont,Cont,prolog)
    ; 
	register_predicate(Pred),
	make_dcg_callret(Head,Left,Right,BMG_Left,BMG_Right,Call,Return),
	First = '*FIRST*'(Call),
	( light_tabular(Pred) ->
	    ( check_lc(Pred) ->
		Last = '*LIGHTLCLAST*'(Return)
	    ;	
		Last = '*LIGHTLAST*'(Return)
	    ),
	    Type = rec_prolog
	;
	    Last = '*LAST*'(Return),
	    Type=dyalog
	),
	dcg_trail_to_lpda(Id,Trail,Right,XTrail,Last,TrailCont,Type),
	dcg_body_to_lpda(Id,Body,Left,XTrail,BMG_Inside_Left,BMG_Inside_Right,TrailCont,Cont,Type),
	dcg_try_lc_transform(Id,F/N,First,Cont,Call,Trans1),
	install_loader(Id,Loader^dcg_autoloader(Body,Loader),Trans1,Trans)
    )
    .

pgm_to_lpda( register_predicate(Pred::dcg(F/N)), '*PROLOG-FIRST*'(Call) :> Cont ) :-
	(   recorded( public(Pred) ) xor recorded( public(dcg('*default*')) ) ),
	functor(T,F,N),
	my_numbervars(T,Id,_),
	position_and_stacks(Id,L,BMG_L),
	position_and_stacks(Id,R,BMG_R),
	(   Body = T ; Body = '$answers'(T) ),
	Call = '$bmgcall'(Body,L,R,BMG_L,BMG_R),
	dcg_body_to_lpda(Id,Body,L,R,BMG_L,BMG_R,'*PROLOG-LAST*',Cont,prolog)
	.


:-std_prolog numbervars_delete/1.

:-rec_prolog dcg_try_lc_transform/6.

dcg_try_lc_transform(Id,F/N,FirstTrans,Cont,Call,Trans) :-
	(   check_lc(dcg(F/N)),
	    Cont = '*WRAPPER-CALL*'(lc_dcg(H/M),First_Args,First_Cont) ->
	    bmg_get_stacks(BMG_Left),
	    bmg_get_stacks(BMG_Right),
	    functor(First,H,M),
	    make_dcg_callret(First,Left,Right,BMG_Left,BMG_Right,C,R),
	    bmg_wrap_args(First,Left,Right,Tag,BMG_Left,BMG_Right,First_Args),
	    tupple(First_Args,T),
	    my_numbervars([LC_List,LC],Id,_),
	    body_to_lpda(Id,
			 (   'internal_reverse_lc'(Call,LC_List),domain(LC,LC_List)),
			 noop,
			 LCInit,
			 rec_prolog),
	    Trigger = 'internal_lc_trigger'(Id,Left,Right,R,C,Call),
	    body_to_lpda(Id,(recorded(Trigger) -> fail ; record(Trigger)),First_Cont,Cont2,rec_prolog),
	    Model = '*RITEM*'(C,R) :> '*LCFIRST*'(Call,Cont2,Call^LC^LCInit)(Call)(T),
	    Trans = '*OBJECT*'( seed{ model => Model,
				      schedule => no,
				      tabule => yes
				    }),
	    value_counter(lc_dcg(H/M),V),
	    VV is V-1,
	    reset_counter(lc_dcg(H/M),VV)
	;   
	    Trans = FirstTrans :> Cont,
	    (	check_lc(dcg(F/N)),
		recorded( compiler_analyzer(lc) ) ->
		copy_numbervars(Call,XCall),
		record_without_doublon( tmp_terminal(XCall) ),
		record_without_doublon( tmp_lc(XCall,XCall) )
	    ;	
		true
	    )
	)
	.

%%! @predicate dcg_body_to_lpda(Id,DcgBody,Left,Right,BMG_Left,BMG_Right,Cont,Trans,Type)
%%! @brief essentially a wrapper around dcg_body_to_lpda_handler()

%%! :-std_prolog dcg_body_to_lpda/9.

dcg_body_to_lpda(Id, Body1 , Left, Right, BMG_Left, BMG_Right, Cont, Trans, Type) :-
	(   toplevel(dcg_term_expand(Body1,Body)),
	    my_numbervars(Body,Id,_)
	xor Body1=Body), % *HOOK*
	( dcg_body_to_lpda_handler(Id,Body,Left,Right,BMG_Left,BMG_Right,Cont,Trans,Type) ->
	    true
	;   
	    bmg_non_terminal_to_lpda(Id, Body, Left, Right, BMG_Left, BMG_Right, Cont, Trans,Type)
	)
	.

%%! @predicate dcg_body_to_lpda_handler(Id,DcgBody,Left,Right,BMG_Left,BMG_Right,Cont,Trans,Type)
%%! @brief to express handlers for all cases of DCG constructions

%%! :-rec_prolog dcg_body_to_lpda_handler/9.

%% ------

%%! @defgroup dcg_body_to_lpda_handler
%%! test

/*@{
*/

%% -----

%%! @case MetaCall
dcg_body_to_lpda_handler(Id,Body::'$VAR'(_),Left,Right,BMG_Left,BMG_Right,Cont,Trans,Type) :-
	dcg_body_to_lpda_handler(Id,'$call'(Body),Left,Right,BMG_Left,BMG_Right,Cont,Trans,Type)
	.

dcg_body_to_lpda_handler(Id,'$call'(Body),Left,Right,BMG_Left,BMG_Right,Cont,Trans,Type) :-
	metacall_initialize,
	( Type = answer(Type2) ->
	    body_to_lpda(Id,call!bmgcall('$answers'(Body),Left,Right,BMG_Left,BMG_Right),Cont,Trans,Type2)
	;   
	    body_to_lpda(Id,call!bmgcall(Body,Left,Right,BMG_Left,BMG_Right),Cont,Trans,Type)
	)
	.

%%! @case Concatenation (A,B)
dcg_body_to_lpda_handler(Id,(A,B),Left,Right,BMG_Left,BMG_Right,Cont,Trans,Type) :-
	dcg_body_to_lpda(Id,B,Middle,Right,BMG_Middle,BMG_Right,Cont,ContB,Type),
	position_and_stacks(Id,Middle,BMG_Middle),
	dcg_body_to_lpda(Id,A,Left,Middle,BMG_Left,BMG_Middle,ContB,Trans,Type)
	.

%%! @case Rightward Reading (A +> B)
dcg_body_to_lpda_handler(Id,(A+>B),Left,Right,BMG_Left,BMG_Right,Cont,Trans,Type) :-
	dcg_body_to_lpda_handler(Id,(A,B),Left,Right,BMG_Left,BMG_Right,Cont,Trans,Type)
	.

%%! @case Leftward Reading (A <+ B)
dcg_body_to_lpda_handler(Id,( A <+ B ),Left,Right,BMG_Left,BMG_Right,Cont,Trans,Type) :-
	position_and_stacks(Id,Middle,BMG_Middle),
	dcg_body_to_lpda(Id,A,Left,Middle,BMG_Left,BMG_Middle,Cont,ContA,Type),    
	dcg_body_to_lpda(Id,B,Middle,Right,BMG_Middle,BMG_Right,ContA,Trans,Type)
	.

%%! @case Intersection (A & B)
dcg_body_to_lpda_handler(Id,(A & B),Left,Right,BMG_Left,BMG_Right,Cont,Trans,Type) :-
	position_and_stacks(Id,Left,BMG_Left),
	dcg_body_to_lpda(Id,B,Left,Right,BMG_Left,BMG_Right,Cont,ContB,Type),
	dcg_body_to_lpda(Id,A,Left,Right,BMG_Left,BMG_Right,ContB,Trans,Type)
	.

%%! @case Guard (A->B;C)
dcg_body_to_lpda_handler(Id,(A->B;C),Left,Right,BMG_Left,BMG_Right,Cont,Trans,Type ) :-
	dcg_body_to_lpda_handler(Id,(('*COMMIT*'(A),B);C),Left,Right,BMG_Left,BMG_Right,Cont,Trans,Type)
	.

%%! @case Special Guard (A->B)
dcg_body_to_lpda_handler(Id,(A->B),Left,Right,BMG_Left,BMG_Right,Cont,Trans,Type) :-
	dcg_body_to_lpda_handler(Id, (A -> B ; fail), Left, Right, BMG_Left, BMG_Right, Cont, Trans, Type)
	.

%% Guard commit 
dcg_body_to_lpda_handler(Id,'*COMMIT*'(A),Left,Right,BMG_Left,BMG_Right,Cont,'*SETCUT*' :> ContA,Type ) :-
	dcg_body_to_lpda(Id,A,Left,Right,BMG_Left,BMG_Right, '*CUT*' :> Cont, ContA, rec_prolog)
	.

%%! @case Disjunction (A;B)
dcg_body_to_lpda_handler(Id,(A;B),Left,Right,BMG_Left,BMG_Right,Cont,Trans,Type ) :-
	\+ A = (_ -> _),
	handle_choice(Cont,Trans,Last,ContA,ContB),
	position_and_stacks(Id,Left,BMG_Left),
	dcg_body_to_lpda(Id,A,Left,Right, BMG_Left, BMG_Right,Last,ContA,Type),
	dcg_body_to_lpda(Id,B,Left,Right, BMG_Left, BMG_Right,Last,ContB,Type)
	.

%%! @case Optional A @?
dcg_body_to_lpda_handler(Id,(A @?),Left,Right,BMG_Left,BMG_Right,Cont,Trans,Type) :-
	dcg_body_to_lpda_handler(Id,(true ; A),Left,Right,BMG_Left,BMG_Right,Cont,Trans,Type).

%%! @case Failure (fail)
dcg_body_to_lpda_handler(Id,fail,Left,Right,BMG_Left,BMG_Right,Cont,fail :> Cont,Type  ).

%%! @case Success (true)
%% same as empty scan
dcg_body_to_lpda_handler(Id,true,Left,Right,BMG_Left,BMG_Right,Cont,Trans,Type  ) :-	
	bmg_null_step(Id,Left,Right,BMG_Left,BMG_Right,Cont,Trans)
	.

%%! @case Escape to Prolog {Escape}
dcg_body_to_lpda_handler(Id,{ Call },Left,Right,BMG_Left,BMG_Right,Cont,Trans,Type  ) :-
	body_to_lpda(Id,Call,Cont,Call_Trans,Type),
	bmg_null_step(Id,Left,Right,BMG_Left,BMG_Right,Call_Trans,Trans)
	.

%%! @case Empty scanning []
dcg_body_to_lpda_handler(Id,[],Left,Right,BMG_Left,BMG_Right,Cont,Trans,Type  ) :-
	bmg_null_step(Id,Left,Right,BMG_Left,BMG_Right,Cont,Trans)
	.

%%! @case Scan [T1,..,TN]
dcg_body_to_lpda_handler(Id,Body::[_|_],Left,Right,BMG_Left,BMG_Right,Cont,Trans,Type  ) :-
	bmg_equations(BMG_Left,BMG_Right,Cont,BMG_Trans),
	scan_to_lpda(Id,Body,Left,Right,BMG_Trans,Trans,Type)
	.

%%! @case Scan '$noskip'([T1,..,TN])
%% sometimes, we wish to protect a scan from the skipping mechanism, for instance during autoloading
dcg_body_to_lpda_handler(Id,Body::'$noskip'([_|_]),Left,Right,BMG_Left,BMG_Right,Cont,Trans,Type  ) :-
	bmg_equations(BMG_Left,BMG_Right,Cont,BMG_Trans),
	scan_to_lpda(Id,Body,Left,Right,BMG_Trans,Trans,Type),
	true
	.

dcg_body_to_lpda_handler(Id,'$dcg_pos'(A),Left,Right,BMG_Left,BMG_Right,Cont,Trans,Type  ) :-
	dcg_body_to_lpda(Id,{Left=A},
			 Left,Right,BMG_Left,BMG_Right,Cont,Trans,Type)
	.

dcg_body_to_lpda_handler(Id,Body::wait(Task),Left,Right,BMG_Left,BMG_Right,Cont,Trans,Type  ) :-
	error( 'Not yet implemented in DCG body ~w',[Body])
	.

dcg_body_to_lpda_handler(Id,Body::'$answers'(A),Left,Right,BMG_Left,BMG_Right,Cont,Trans,Type  ) :-
	dcg_body_to_lpda(Id,A,Left,Right,BMG_Left,BMG_Right,Cont,Trans,answer(Type))
	.

dcg_body_to_lpda_handler(Id,Body::'$block'(A),Left,Right,BMG_Left,BMG_Right,Cont,Trans,Type  ) :-
	dcg_body_to_lpda(Id,A,Left,Right,BMG_Left,BMG_Right,Cont,Trans,Type)
	.

/*@}
*/

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Kleene Handling


%% @case Kleene Star (A @*)
dcg_body_to_lpda_handler(Id,(AA @*),Left,Right,BMG_Left,BMG_Right,Cont,Trans,Type) :-
	(   AA = Vars ^ A xor AA = A),
	dcg_body_to_lpda_handler(Id,@*{goal => A, vars=>Vars},
				 Left,Right,BMG_Left,BMG_Right,Cont,Trans,Type)
	.

%% @case Kleene Star strict (A @+)
dcg_body_to_lpda_handler(Id,(AA @+),Left,Right,BMG_Left,BMG_Right,Cont,Trans,Type) :-
	(   AA = Vars ^ A xor AA = A),
	dcg_body_to_lpda_handler(Id,@*{goal => A, vars=>Vars, from => 1},
				 Left,Right,BMG_Left,BMG_Right,Cont,Trans,Type)
	.

%% @case Range Kleene Star @*{goal=>A}
dcg_body_to_lpda_handler(Id,
			 K,
			 Left,Right,BMG_Left,BMG_Right,Cont,Trans,Type  ) :-
	kleene_normalize(K,
			 KleeneStruct:: @*{ goal => A,
					    vars => Vars,
					    from => From,
					    to => To,
					    collect_first=>UserFirst,
					    collect_loop=>UserParam,
					    collect_next=>UserArg,
					    collect_last=>UserLast
					  }),

	update_counter(kleene, KleeneId),
	name_builder('_kleene~w',[KleeneId],KleeneLabel),
	kleene_conditions(KleeneCond::[From,To],[Counter,CounterInc],InitCond,ExitCond,LoopCond),
	position_and_stacks(Id,Left,BMG_Left),
	position_and_stacks(Id,Middle,BMG_Middle),
	position_and_stacks(Id,Left2,BMG_Left2),
	my_numbervars([Counter,CounterInc,InitCond,ExitCond,LoopCond,KleeneStruct],Id,_),
	Args1 = [Counter,Left2|BMG_Left2],
	Loop_Args1 = [CounterInc,Middle|BMG_Middle],
	append(Args1,UserParam,Args),
	append(Loop_Args1,UserArg,Loop_Args),
	tupple(Vars,TA),
	tupple(Cont,TCont),
	extend_tupple([Right,BMG_Right,UserLast,KleeneCond],TCont,RTCont),

	/*
	oset_tupple(TA,XA),
	format('Start Kleene left=~w right=~w\n\t~K\n\tCont=~w\n\targs=~w param=~w vars=~w:~w\n',
	       [Left,Right,KleeneStruct,Cont,Args,Loop_Args,TA,XA]),
	*/
	
	dcg_body_to_lpda_handler(Id,{ ExitCond, UserLast=UserParam },
				 Left2,Right,
				 BMG_Left2,BMG_Right,
				 Cont,
				 ContExit,
				 Type),
	dcg_body_to_lpda_handler(Id,
				 (   { LoopCond }, A ),
				 Left2,Middle,BMG_Left2,BMG_Middle,
				 '*KLEENE-LAST*'(KleeneLabel,
						 Loop_Args,
						 Args,
						 TA,
						 RTCont,
						 noop
						),
				 ContLoop,
				 Type),

	Trans1 = '*KLEENE*'(KleeneLabel,
			    TA,
			    '*ALTERNATIVE*'(ContExit,ContLoop)
			    ),
	dcg_body_to_lpda_handler(Id,{InitCond,UserFirst=UserParam},
				 Left,Left2,BMG_Left,BMG_Left2,Trans1,Trans,Type),
%%	format('Kleene ~w\n',[Trans]),
	true
	.

%%! @predicate kleene_conditions(RangeCond,Counters,InitCond,ExitCond,LoopCond)
%%! @brief Build conditions associated to kleene star with range @b [Min,Max]

%%! @arg @b Max may be set to @b inf for unbounded kleene star
%%! :-extensional kleene_conditions/5

kleene_conditions([Min,Max],
		  [Counter,CounterInc],
		  %% Init
		  %% Set default bounds for min and max if needed
		  (   Counter = 0,            
		      (	  Min = 0 xor true),
		      (	  Max = inf xor true),
%%		      format('Enter min=~w max=~w counter=~w\n',[Min,Max,Counter]),
		      true
		  ),
		  %% Exit
		  (   Counter >= Min,
%%		      format('Exit counter=~w\n',[Counter]),
		      true
		  ),
		  %% Loop
		  (
		      %% format('Try loop counter=~w\n',[Counter]),
		      Max == inf -> 
		      (	Counter =< Min ->
			  CounterInc is Counter + 1
		      ;	  
			  Counter = CounterInc
		      )
		  ;   
		      Counter < Max, CounterInc is Counter + 1
		  )
		 ).

%%! @predicate kleene_normalize(KleeneStruct,NormalizedKleeneStruct)
%%! @brief Normalize Kleene Struct by setting some defaults

kleene_normalize(@*{goal=>A,
		    vars=>Vars,
		    from=>From,
		    to=>To,
		    collect_first=> Collect_First,
		    collect_last=> Collect_Last,
		    collect_loop=> Collect_Loop,
		    collect_next=> Collect_Next,
		    collect_pred=> Collect_Pred
		   },
		 @*{goal=>A,
		    vars=>N_Vars,
		    from=>From,	% default set at run time: 0
		    to=>To,     % default set at run time: inf
		    collect_first=> N_Collect_First,
		    collect_last=> N_Collect_Last,
		    collect_loop=> N_Collect_Loop,
		    collect_next=> N_Collect_Next,
		    collect_pred=> N_Collect_Pred
		   }
		 ) :-
	install_default(Vars,[],N_Vars),
	install_default(Collect_First,[],N_Collect_First),
	install_default(Collect_Loop,N_Collect_First,N_Collect_Loop),
	install_default(Collect_Last,N_Collect_First,N_Collect_Last),
	install_default(Collect_Next,N_Collect_Last,N_Collect_Next),
	install_default(Collect_Pred,true,N_Collect_Pred)
	.

%% *WARNING* The following predicate to set defaults is not sound
%% it does not recognize when Old is some shared variable
%% A way to identify anonymous variables is missing
%% To avoid this problem, please use '$protect'(Old)
:-xcompiler
	install_default(Old,Default,New) :-
		( var(Old) -> New = Default
		;   Old = '$VAR'(_) -> New = Default
		;   Old = '$protect'(New) -> true
		;   New = Old
		)
		.
		
:-light_tabular dcg_kleene_args/7.
:-mode(dcg_kleene_args/7,[+|-]).

dcg_kleene_args(In,Left,Right,BMG_In,BMG_Left,BMG_Right,[In,Left,Right|BMG_Args]) :-
	bmg_get_stacks(BMG_In),
	bmg_get_stacks(BMG_Left),
	bmg_get_stacks(BMG_Right),
	append(BMG_Left,BMG_Right,BMG_Args1),
	append(BMG_In,BMG_Args1,BMG_Args)
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Interleave operator

%%! @case Interleaving (A ## B)
dcg_body_to_lpda_handler(Id,Body::(A ## B),Left,Right,BMG_Left,BMG_Right,Cont,Trans,Type):-
	interleave_initialize,
	il_label([],Label),
	my_numbervars([IL_Left,IL_Right],Id,_),
	il_tupple(Body,Tupple),
	position_and_stacks(Id,Left,BMG_Left),
	dcg_il_loop_to_lpda(Id,Left,BMG_Left,IL_Right,Loop_Code,Type),
	dcg_il_body_to_lpda(Id,Body,
			    IL_Left,IL_Right,IL_Out,IL_Out,
			    noop,Loop_Code,
			    Trans1,Type,Label),
	dcg_il_start_to_lpda(Id,
			     Cont,
			     Right,BMG_Right,[],
			     [],IL_Left,
			     Trans1,
			     Trans2,
			     Type,Label),
	Trans = ( '*NOOP*'(Tupple) :> '*IL-START*'(Trans2) ),
%%	format('IL Trans: ~K\n',[Trans]),
	true
	.

%% defined in tag_compile.pl
:-std_prolog il_tupple/2.


%%! @predicate dcg_il_loop_to_lpda(Id,Pos,BMG_Pos,IL_Pos,Trans,Type)
%%! @brief Generate LPDA code to select an interleave alternative
:-std_prolog dcg_il_loop_to_lpda/6.

dcg_il_loop_to_lpda(Id,Pos,BMG_Pos,IL_Pos,'*IL-LOOP-WRAP*'(Trans),Type) :-
	my_numbervars([IL_Loop,Loop_Jump],Id,_),
	dcg_il_args(IL_Loop,Pos,BMG_Pos,IL_Args_Loop),
	update_counter(il_choice,Label),
	body_to_lpda(Id,
		     (	 %% format('Ready to choose ~w left=~w il=~w\n',[Label,Pos,IL_Pos]),
			 interleave_choose(Label,IL_Pos,IL_Loop,Loop_Jump)
			 %% format('Going to loop il_loop=~w\n',[IL_Loop])
		     ),
		     '*IL-LOOP*'(Loop_Jump,IL_Args_Loop),
		     Trans,
		     Type)
	.

%%! @predicate dcg_il_start_to_lpda(Id,Exit,Pos,BMG_Pos,IL_Pos,IL_Left,IL_Right,Cont,Trans,Type,Label)
%%! @brief Generate LPDA code to start a new interleave block with its exit alternative
:-std_prolog dcg_il_start_to_lpda/11.

dcg_il_start_to_lpda( Id,
		      Exit,
		      Pos,BMG_Pos,IL_Pos,
		      IL_Left,IL_Right,
		      IL_Cont,
		      '*IL-ALT*'(Exit_Jump,Exit,IL_Args_Exit,[]) :> Trans1,
		      Type,Label
		    ) :-
	my_numbervars(Exit_Jump,Id,_),
	dcg_il_args(IL_Pos,Pos,BMG_Pos,IL_Args_Exit),	
	body_to_lpda(Id,
		     interleave_start(Label,Exit_Jump,IL_Left,IL_Right),
		     IL_Cont,
		     Trans1,
		     Type)
	.

%%! @predicate dcg_il_register_to_lpda( Id,Alt,Pos,BMG_Pos,IL_Pos,IL_Left,IL_Right,Cont,Trans,Type,Label) 
%%! @brief Generate LPDA code to register an interleave alternative
:-std_prolog dcg_il_register_to_lpda/12.

dcg_il_register_to_lpda( Id,
			 Alt,
			 Pos,BMG_Pos,IL_Pos,
			 IL_Left,IL_Right,
			 IL_Cont,
			 VLoop,
			 '*IL-ALT*'(Alt_Jump,Alt,IL_Args_Alt,VLoop) :> Trans1,
			 Type,Label
		       ) :-
	my_numbervars(Alt_Jump,Id,_),
	dcg_il_args(IL_Pos,Pos,BMG_Pos,IL_Args_Alt),	
	body_to_lpda(Id,
		     interleave_register(Label,Alt_Jump,IL_Left,IL_Right),
		     IL_Cont,
		     Trans1,
		     Type)
	.

%%! @predicate interleave_initialize
%%! @brief Ensure loading of library file 'interleave.pl' tu support interleaving
:-light_tabular interleave_initialize/0.

interleave_initialize :-
	read_files('interleave.pl',require)
	.

%%! @predicate il_label(Module,Label)
%%! @brief Generate a fresh interleaving label
%%! As interleaving blocks may be nested, IL Labels are also nested using modules

:-std_prolog il_label/2.

il_label(Module,Label) :-
	update_counter(interleave, Id),
	name_builder('_interleave~w',[Id],Label1),
	atom_module(Label,Module,Label1)
	.

%%! @predicate dcg_il_args(IL_Table,Left,BMG_Left,IL_Args)
%%! @brief Collect arguments for DCG interleaving alternative
:-light_tabular dcg_il_args/4.

dcg_il_args(IL_Table,Left,BMG_Left,IL_Args::[IL_Table,Left|BMG_Left]) :-
%%	format('dcg_il_args left=~w bmg_left=~w args=~w\n',[Left,BMG_Left,IL_Args]),
	bmg_get_stacks(BMG_Left)
	.

%%! @predicate dcg_il_body_to_lpda(Id,A,IL_Left,IL_Right,IL_Out_Left,IL_Out_Right,Cont,Trans,Type,Label)
%%! @brief essentially a wrapper around dcg_il_body_to_lpda_handler/10
%%! Also wrap interleaving registration of an alternative build with dcg_body_to_lpda/9

%%! @param Id  -- Id of DCG production being compiled, used to number variables
%%! @param A -- The DCG element being compiled
%%! @param IL_Left -- The IL table before registering A
%%! @param IL_Right -- The IL table after registering A
%%! @param IL_Out_Left -- The IL table when starting alternative A
%%! @param IL_Out_Right -- The IL table before selecting next alternative Cont after A
%%! @param Cont -- Next alternative after A (possibly empty)
%%! @param Trans -- Resulting LPDA fragment
%%! @param Type -- Compilation mode
%%! @param Label -- Label of current interleaving block 

:-std_prolog dcg_il_body_to_lpda/11.

dcg_il_body_to_lpda(Id,Body1,
		    IL_Left,IL_Right,IL_Out_Left,IL_Out_Right,
		    Cont, IL_Cont,
		    Trans,Type,Label
		   ) :-
	(   toplevel(dcg_term_expand(Body1,Body)),
	    my_numbervars(Body,Id,_)
	xor Body1=Body), % *HOOK*
	( dcg_il_body_to_lpda_handler(Id,Body,
				      IL_Left,IL_Right,IL_Out_Left,IL_Out_Right,
				      Cont,IL_Cont,
				      Trans,Type,Label ) ->
	    true
	;
	    position_and_stacks(Id,Left,BMG_Left),
	    position_and_stacks(Id,Right,BMG_Right),
	    my_numbervars([IL_Out_Left,IL_Out_Right],Id,_),
	    dcg_il_loop_to_lpda(Id,Right,BMG_Right,IL_Out_Right,Loop_Code,Type),
	    ( Cont == noop ->
		Cont1 = Loop_Code,
		Right=_Right,
		BMG_Right=_BMG_Right,
		VLoop=[]
	    
	    ;	Cont = [VLoop^Loop_Code,[_Right|_BMG_Right],[Right|BMG_Right]]^KK ->
%%		format('IL Case ~w\n',[Cont]),
		Cont1 = KK
	    ;	
		Cont1 = Cont :> Loop_Code,
		Right=_Right,
		BMG_Right=_BMG_Right,
		VLoop=[]
	    ),
	    dcg_body_to_lpda(Id,Body, Left, _Right, BMG_Left, _BMG_Right, Cont1, Trans1,Type),
	    dcg_il_register_to_lpda( Id,Trans1,
				     Left,BMG_Left,IL_Out_Left,
				     IL_Left,IL_Right,IL_Cont,
				     [Loop_Code|VLoop],
				     Trans,
				     Type,Label
				   )
	),
%%	format('Trans body=~w cont=~w =>\n\t ~w\n\n',[Body1,Cont,Trans]),
	true
	.

%%! @brief to express handlers for all cases of DCG constructions whithin a interleaving block

:-rec_prolog dcg_il_body_to_lpda_handler/11.

%%! @case Empty Scan []
dcg_il_body_to_lpda_handler(Id,[],
			    IL_Left,IL_Right,_,_,
			    Cont,IL_Cont,
			    unify(IL_Left,IL_Right) :> Cont,Type,Label
			   ).

%%! @case Concatenation (A,B)
dcg_il_body_to_lpda_handler(Id,(A,B),
			    IL_Left,IL_Right,IL_Out_Left,IL_Out_Right,
			    Cont,IL_Cont,
			    Trans,Type,Label
			   ) :-
	my_numbervars([IL_LeftB,IL_RightB],Id,_),
	dcg_il_body_to_lpda(Id,B,
			    IL_LeftB,IL_RightB,IL_Out_Left,IL_Out_Right,
			    Cont,noop,
			    TransB,Type,Label),
	dcg_il_body_to_lpda(Id,A,
			    IL_Left,IL_Right,IL_LeftB,IL_RightB,
			    TransB,IL_Cont,
			    Trans,Type,Label)
	.

%%! @case Disjunction (A;B)
dcg_il_body_to_lpda_handler(Id,(A;B),
			    IL_Left,IL_Right,IL_Out_Left,IL_Out_Right,
			    Cont,IL_Cont,
			    Trans,Type,Label
			   ) :-
	\+ A = (_ -> _),
	handle_choice(IL_Cont,Trans,IL_Last,ContA,ContB),
	dcg_il_body_to_lpda(Id,A,
			    IL_Left,IL_Right,IL_Out_Left,IL_Out_Right,
			    Cont,IL_Last,
			    ContA,Type,Label),
	dcg_il_body_to_lpda(Id,B,
			    IL_Left,IL_Right,IL_Out_Left,IL_Out_Right,
			    Cont,IL_Last,
			    ContB,Type, Label)
	.

%%! @case Interleaving (A ## B)
dcg_il_body_to_lpda_handler(Id,(A ## B),
			    IL_Left,IL_Right,IL_Out_Left,IL_Out_Right,
			    Cont,
			    IL_Cont,
			    Trans,Type,Label
			   ):-
	( Cont == noop ->  
	    Label2 = Label,
	    IL_Left2 = IL_Left,
	    IL_Right2=IL_Right,
	    Trans = TransA
	;   
	    il_label(Label,Label2),
	    position_and_stacks(Id,Right,BMG_Right),
	    my_numbervars([IL_Left2,IL_Right2],Id,_),
	    dcg_il_loop_to_lpda(Id,Right,BMG_Right,IL_Out_Right,Loop_Code,Type),
	    dcg_il_start_to_lpda(Id,
				 Cont :> Loop_Code,
				 Right,BMG_Right,IL_Out_Left,
				 IL_Left,IL_Left2,
				 TransA,
				 Trans,
				 Type, Label2)
	),
	my_numbervars(IL_Middle,Id,_),
	dcg_il_body_to_lpda(Id,B,
			    IL_Middle,IL_Right,IL_OutB,IL_OutB,
			    noop,IL_Cont,
			    TransB,Type,Label2
			   ),
	dcg_il_body_to_lpda(Id,A,
			    IL_Left2,IL_Middle,IL_OutA,IL_OutA,
			    noop,TransB,
			    TransA,Type,Label2
			   )
	.

%% @case Kleene Star (A @*)
dcg_il_body_to_lpda_handler(Id,(AA @*),
			    IL_Left,IL_Right,
			    IL_Out_Left,IL_Out_Right,
			    Cont,IL_Cont,
			    Trans,Type,Label
			   ) :-
	(   AA = Vars ^ A xor AA = A),
	dcg_il_body_to_lpda_handler(Id,@*{goal => A, vars=>Vars},
				    IL_Left,IL_Right,
				    IL_Out_Left,IL_Out_Right,
				    Cont,IL_Cont,
				    Trans,Type,Label
				   )
	.

%% @case Range Kleene Star @*(A,[Min,Max],First^Arg^Param^Last)
dcg_il_body_to_lpda_handler(Id,
			    K,
			    IL_Left,IL_Right,
			    IL_Out_Left,IL_Out_Right,
			    Cont,IL_Cont,
			    Trans,Type,Label
			   ) :-
%%	format('IL KLEENE 2 ~w out_left=~w out_right=~w cont=~w\n',[AA,IL_Out_Left,IL_Out_Right,Cont]),
	kleene_normalize(K,
			 KleeneStruct:: @*{ goal => A,
					    vars => Vars,
					    from => From,
					    to => To,
					    collect_first=>UserFirst,
					    collect_loop=>UserParam,
					    collect_next=>UserArg,
					    collect_last=>UserLast
					  }),
	update_counter(kleene, KleeneId),
	name_builder('_kleene~w',[KleeneId],KleeneLabel),
	kleene_conditions(KleeneCond::[From,To],[Counter,CounterInc],InitCond,ExitCond,LoopCond),
	my_numbervars([Counter,CounterInc,InitCond,ExitCond,LoopCond,KleeneStruct],Id,_),
	my_numbervars([IL_Out_Right,IL_Out_Left,
		       IL_Left2,IL_Right2,
		       IL_Left3],Id,_),

	bmg_get_stacks(_BMG_Right),
	position_and_stacks(Id,Right,BMG_Right),
	Args1 = [Counter,IL_Left2,_Right|_BMG_Right],
	Loop_Args1 = [CounterInc,IL_Left3,Right|BMG_Right],
	append(Args1,UserParam,Args),
	append(Loop_Args1,UserArg,Loop_Args),
	tupple(Vars,TA),
	tupple(Cont,TCont),
	extend_tupple([IL_Out_Left,IL_Out_Right,IL_Right,UserLast,KleeneCond],TCont,RTCont),
	body_to_lpda(Id, (  %% format('exit il=~w counter=~w\n',[IL_Left2,Counter]),
			     ExitCond,
			     UserLast=UserParam,
			     IL_Left2=IL_Out_Left,
			     IL_Right2=IL_Out_Right			     
			 ),
		     Cont,
		     ContExit,
		     Type),

	VLoop=[IL_Left2,IL_Right2,IL_Left3,IL_Right,IL_Left,IL_Out_Right,IL_Out_Left],
	Kleene_Last= [VLoop^_Loop,
		      [Right|BMG_Right],
		      [_Right|_BMG_Right]]^ '*KLEENE-LAST*'(KleeneLabel,
							    Loop_Args,
							    Args,
							    TA,
							    RTCont,
							    _Loop
							   ),
	
	dcg_il_body_to_lpda(Id,
			    A,
			    IL_Left2,IL_Right2,
			    IL_Left3,IL_Right2,
			    Kleene_Last,
			    noop,
			    ContLoop2,
			    Type,
			    Label),
	body_to_lpda(Id,
		     (
			 %% format('loop il=~w\n',[IL_Left2]),
			 LoopCond
		     ),
		     ContLoop2,
		     ContLoop,
		     Type),


	Trans1 = '*KLEENE*'(KleeneLabel,
			    TA,
			    '*ALTERNATIVE*'(ContExit,ContLoop)
			    ),

	body_to_lpda(Id,
		     IL_Right=IL_Right2,
		     IL_Cont,
		     IL_Cont2,
		     Type),
	
	body_to_lpda(Id,
		     (%%	 format('enter il=~w\n',[IL_Left]),
			 InitCond,
			 UserFirst=UserParam,
			 IL_Left=IL_Left2
			 ),
		     Trans1 :> IL_Cont2,Trans,Type),
	
%%	format('\nIL Kleene ~K\n',[Trans]),
	true
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

dcg_null_step(Id,Left,Right,Cont,Trans) :-
	( Left = Right ->
	    Trans = Cont
	;   
	    Trans = unify(Left,Right) :> Cont
	)
	.

bmg_null_step(Id,Left,Right,BMG_Right,BMG_Left,Cont,Trans) :-
	( BMG_Left = BMG_Right ->
	    BMG_Trans = Cont
	;   
	    bmg_equations(BMG_Left,BMG_Right,Cont,BMG_Trans)
	),
	( Left = Right ->
	    Trans = BMG_Trans
	;   
	    Trans = unify(Left,Right) :> BMG_Trans
	)
	.


:-rec_prolog position_and_stacks/3.

position_and_stacks(Id,M,S) :-
	my_numbervars(M,Id,_),
	bmg_get_stacks(S,Id).

%% Scanning a terminal,
%% taking into account the two possible mode of scanning

scan_to_lpda(Id,Scan,Left,Right,Cont,Trans,Type) :-
	my_numbervars(Right,Id,_),
	parse_mode(Mode),
	( Mode == token ->   %% Scanning from a token database with 'C'/3
	  (Scan = '$noskip'(_Scan) ->
	   token_scan_to_lpda(Id,_Scan,Left,Right,Cont,Trans,Type,noksip)
	  ;
	   token_scan_to_lpda(Id,Scan,Left,Right,Cont,Trans,Type,skip)
	  )
	;   Mode == list ->  %% Scanning from a Prolog List
	    Trans = unify(Left,L) :> Cont,
	    (Scan = '$noskip'(_Scan) xor Scan = _Scan ),
	    append(_Scan,Right,L)
	;   
	    error( 'Not a valid parse mode ~w', Mode )
	)
	.



%% Skipping some tokens when allowed
:-rec_prolog generalized_scanner/6.

generalized_scanner(Id,Left,A,Right,Goal,SkipFlag) :-
	scanner(_Left,A,Right,Scan),
	( recorded( skipper(Left,_A,_Left,Skip) ),
	  SkipFlag = skip
	->
	  my_numbervars([_A,_Left],Id,_),
	  Goal = ((Skip;_Left=Left),Scan)
	;   
	  Left = _Left,
	  Goal = Scan
	)
	.

%% Scanning from a token database
:-rec_prolog token_scan_to_lpda/8.

token_scan_to_lpda(Id,[A],Left,Right,Cont,Trans,Type,SkipFlag) :-
	( A = (A1;A2) ->
	  token_scan_to_lpda(Id,A,Left,Right,Cont,Trans,Type,SkipFlag)
	;
	  generalized_scanner(Id,Left,A,Right,Scan,SkipFlag),
	  body_to_lpda(Id,Scan,Cont,Trans,Type)
	)
	.

token_scan_to_lpda(Id,(A1;A2),Left,Right,Cont,Trans,Type,SkipFlag) :-
	handle_choice(Cont,Trans,Last,ContA1,ContA2),
	token_scan_to_lpda(Id,A1,Left,Right,Last,ContA1,Type,SkipFlag),
	token_scan_to_lpda(Id,A2,Left,Right,Last,ContA2,Type,SkipFlag)
	.

token_scan_to_lpda(Id,[A|R],Left,Right,Cont,Trans,Type,SkipFlag) :-
	R = [_|_],
	my_numbervars(Middle,Id,_),
	token_scan_to_lpda(Id,R,Middle,Right,Cont,ContB,Type,SkipFlag),
	( A = (A1;A2) ->
	  handle_choice(ContB,Trans,Last,ContA1,ContA2),
	  token_scan_to_lpda(Id,A1,Left,Middle,Last,ContA1,Type,SkipFlag),
	  token_scan_to_lpda(Id,A2,Left,Middle,Last,ContA2,Type,SkipFlag)
	;
	  generalized_scanner(Id,Left,A,Middle,Scan,SkipFlag),
	  body_to_lpda(Id,Scan,ContB,Trans,Type)
	)
	.

%% Dealing with a non terminal, possibly wrapped with island constraints
%% It may also be possible to pop the non-terminal from some BMG stack

:-rec_prolog bmg_non_terminal_to_lpda/9.

bmg_non_terminal_to_lpda(Id, A, Left, Right, BMG_Left, BMG_Right, Cont, Trans,Type) :-
	bmg_inside_island(A,AA1,BMG_Left,BMG_Right, BMG_Inside_Left, BMG_Inside_Right, TmpCont, Cont2),
	functor(AA1,F1,N),
	term_current_module_shift(dcg(F1/N),_,AA1,AA),
	bmg_pop(AA,BMG_Left,BMG_Right,Pop),
	( Pop = [] ->
	    TmpCont=Cont,
	    non_terminal_to_lpda(Id,AA,Left,Right, BMG_Inside_Left, BMG_Inside_Right, Cont2, Trans,Type)
	;
%%	    format('BMG POP ~w -> ~w\n',[AA,Pop]),
%%	    TmpCont='*PROLOG-LAST*',
	    handle_choice(Cont,Trans,TmpCont,PopCode,NormalCode),
	    bmg_pop_code(Pop,AA,BMG_Left,BMG_Right,Left,Right,TmpCont,PopCode),
	    non_terminal_to_lpda(Id,AA,Left,Right, BMG_Inside_Left, BMG_Inside_Right, Cont2, NormalCode, prolog),
%%	    format('BMG POP CODE ~w ~w -> code=~w\n',[AA,Pop,Trans]),
	    true
	)
	.

%% Parsing an unwrapped non-terminal
	
non_terminal_to_lpda(Id, AA, Left, Right, BMG_Left, BMG_Right, Cont, Trans,Type) :-
	xtagop(Tag,A,AA),
	functor(A,F,N),
	registered_predicate(Pred::dcg(F/N)),
	( rec_prolog( Pred ) ->
	    dcg_prolog_make(Pred,A,Left,Right,BMG_Left,BMG_Right,XA),
	    Trans = '*GUARD*'(XA) :> Cont
	;   prolog( Pred ) ->
	    (	Type \== rec_prolog xor error('Not a recursive prolog dcg predicate ~w',[A]) ),
	    bmg_wrap_args(A,Left,Right,Tag,BMG_Left,BMG_Right,Args),
	    wrapping_predicate( dcg_prolog(F/N) ),
	    Trans = '*WRAPPER-CALL*'(dcg_prolog(F/N),Args,Cont)
	;   extensional( Pred ) ->
	    dcg_prolog_make(Pred,A,Left,Right,BMG_Left,BMG_Right,XA),
	    litteral_to_lpda(Id, recorded(XA), Cont, Trans, Type)
	;   Type == rec_prolog, \+ light_tabular(Pred) ->
	    error( 'Not a recursive prolog dcg predicate ~w', [A])
	;   Type = answer(_) ->
	    registered_predicate(Pred),
	    make_dcg_callret(A,Left,Right,BMG_Left,BMG_Right,Call,Return),
	    ( light_tabular(Pred) ->
		Trans = '*PSEUDOLIGHTNEXT*'(Call,Return,deallocate :> succeed,Tag) :> Cont
	    ;	
		Trans = '*PSEUDONEXT*'(Call,Return,Cont,Tag)
	    )
	;
	    make_dcg_callret(A, Left, Right, BMG_Left, BMG_Right, Call, Return ),
	    ( try_lco(Pred,Tag,Call,Return,Cont,Trans) ->
		true
	    ;
		bmg_wrap_args(A,Left,Right,Tag,BMG_Left,BMG_Right,Args),
		( check_lc(Pred) ->
		    Pred2 = lc_dcg(F/N),
		    update_counter(Pred2,_)
		;   
		    Pred2 = Pred
		),
		wrapping_predicate( Pred2 ),
		Trans = '*WRAPPER-CALL*'(Pred2,Args,Cont)
	    )
	)
	.

:-std_prolog try_lco/6.  %% see lpda.pl

:-extensional bmg_wrap_args/7.

bmg_wrap_args(A,Left,Right,Tag,[],[],[A,Left,Right,Tag]).
bmg_wrap_args(A,Left,Right,Tag,BMG_Left::[_|_],BMG_Right,
	      [A,Left,Right,Tag,BMG_Left,BMG_Right]).

wrapping_predicate( Pred::dcg(F/N) ) :-
	bmg_get_stacks(BMG_Left),
	bmg_get_stacks(BMG_Right),
	functor(A,F,N),
	make_dcg_callret(A,Left,Right,BMG_Left,BMG_Right,C,R),
	bmg_wrap_args(A,Left,Right,Tag,BMG_Left,BMG_Right,Args),
	my_numbervars([Closure,Left,Right,Tag,BMG_Left,BMG_Right,A],_,_),
	( light_tabular(Pred) ->
	    Body = '*LIGHTNEXTALT*'(C,R,'*PROLOG-LAST*',Tag)
	;   
	    Body = '*NEXT*'{ call=>C, ret=>R, right=>'*PROLOG-LAST*', nabla=>Tag }
	),
	record( '*WRAPPER*'( Pred,
			     [Closure|Args],
			     Body
			   ))
	.

wrapping_predicate( Pred::lc_dcg(F/N) ) :-
	bmg_get_stacks(BMG_Left),
	bmg_get_stacks(BMG_Right),
	functor(A,F,N),
	make_dcg_callret(A,Left,Right,BMG_Left,BMG_Right,C,R),
	bmg_wrap_args(A,Left,Right,Tag,BMG_Left,BMG_Right,Args),
	my_numbervars([Closure,Left,Right,Tag,BMG_Left,BMG_Right,A],Id,_),
	my_numbervars([LC,LC_List],Id,_),
	body_to_lpda(Id,
		     (	 'internal_terminal_lc'(C,LC_List),
			 domain(LC,LC_List)
		     ),
		     noop,TLCInit,rec_prolog),
	body_to_lpda(Id,
		     (	 'internal_lc'(C,LC_List),
			 domain(LC,LC_List)
		     ),
		     noop,LCInit,rec_prolog),
	%%	format('Wrapp dcg ~w -> ~w\n',[Pred,LCInit]),
	( light_tabular(dcg(F/N)) ->
	    Body = '*LIGHTLCNEXT*'(C,R,'*PROLOG-LAST*',Tag,C^LC^(TLCInit,LCInit))
	;   
	    Body = '*LCNEXT*'(C,R,'*PROLOG-LAST*',Tag,C^LC^(TLCInit,LCInit))
	),
	record( '*WRAPPER*'( Pred,
			     [Closure|Args],
			     %% '*NEXT*'{ call=>C, ret=>R, right=>'*PROLOG-LAST*', nabla=>Tag }
			     Body
			   ) )
	.

wrapping_predicate( Pred::dcg_prolog(F/N) ) :-
	bmg_get_stacks(BMG_Left),
	bmg_get_stacks(BMG_Right),
	functor(A,F,N),
	dcg_prolog_make(dcg(F/N),A,Left,Right,BMG_Left,BMG_Right,AA),
	bmg_wrap_args(A,Left,Right,Tag,BMG_Left,BMG_Right,Args),
	my_numbervars([Closure,Left,Right,Tag,BMG_Left,BMG_Right,A],_,_),
	record( '*WRAPPER*'( Pred,
			     [Closure|Args],
			     '*PROLOG-LAST*'(AA,Tag)
			   ) )
	.

%% Setting trail associated with a DCG rule head
%% depends on parsing mode (token or list)

:-rec_prolog dcg_trail_to_lpda/7.

dcg_trail_to_lpda(Id,Trail,Left,Right,Cont,Trans,Type) :-
	parse_mode(Mode),
	( Trail == [] ->
	    Trans = Cont
	;   Mode == token ->
	    my_numbervars(Label,Id,_),
	    token_trail_gen(Trail, Left,Right,TrailGen,Id),
	    body_to_lpda(Id, ( gensym(Label), Left = added(Label), TrailGen ), Cont, Trans, Type)
	;   Mode == list ->
	    Trans = unify(Left,L) :> Cont,
	    append(Trail,Right,L)
	;   
	    error( 'Not a valid parse mode ~w', Mode )
	)
	.

:-rec_prolog token_trail_gen/5.

token_trail_gen([A],Left,Right,record(Scan),Id) :-
	generalized_scanner(Id,Left,A,Right,Scan,skip)
	.

token_trail_gen([A|R::[_|_]],Left,Right,
		(   gensym(Middle),
		    record(Scan),
		    TrailGen ),
		Id ) :-
	my_numbervars(Middle,Id,_),
	generalized_scanner(Id,Left,A,added(Middle),Scan,skip),
	token_trail_gen(R,added(Middle),Right,TrailGen,Id)
	.

:-light_tabular dcg_prolog_make/7.
:-mode(dcg_prolog_make/7,+(+,-,-,-,-,-,-)).

dcg_prolog_make(dcg(F/N),T,L,R,BMG_L,BMG_R,Dcg_T) :-
	functor(T,F,N),
	T =.. [F|Args],
	bmg_args(L,R,BMG_L,BMG_R,Args,BMG_Args),
	Dcg_T =.. [F|BMG_Args],
	(   I = '*PROLOG-ITEM*'{top=>Dcg_T},
	    numbervars(I),
	    label_term(I,CI),
	    label_term(T(L,R),CT),
	    record( viewer(CI,CT) ),
	    fail
	;   
	    true
	)
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Modules

term_module_shift(Module,Pred1::dcg(F1/N),dcg(F/N),T1,T) :-
	( recorded( module_import(Pred1,Module1) ) ->
	    Module1 \== []
	;   Module \== [],
	    atom_module(F1,[],_),
	    Module1=Module
	),
	atom_module(F,Module1,F1),
	length(Args,N),
	T1 =.. [F1|Args],
	T =.. [F|Args]
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Autoloading


%%! @predicate autoloader_simplify(AutoLoadCode,Simplified_AutoLoadCode)
%%! @brief simplify toplevel autoload code

%%! :-std_prolog autoloader_simplify/2.

autoloader_simplify(Code,XCode) :-
	%% Assume inner subcode is already simplified by construction
	( Code = (true,XCode)
	xor Code = (XCode,true)
	xor ( Code = (true xor _), XCode = true)
	xor ( Code = (_ xor true), XCode = true)
	xor Code = XCode
	)
	.

try_install_loader(Id,Cond^Clause,Trans,TransWithLoader) :-
	mutable(Loader,true,false),
	every( ( erase_recorded( Clause ),
		   mutable_read(Loader,LoaderConds),
		   mutable(Loader,(LoaderConds,Cond)))),
	mutable_read(Loader,LoaderConds),
	( (LoaderConds == true xor parse_mode(list) xor \+ autoload ) ->
	    TransWithLoader = Trans
	;   
	    build_cond_loader(Id,(LoaderConds->true;fail),Trans,TransWithLoader)
	),
	numbervars_delete(Id)
	.


:-std_prolog dcg_autoloader/2.
%%! @predicate dcg_autoloader(Clause,Loader)
%%! @brief extract the autoload part of a DCG production

dcg_autoloader(T,Loader) :-
	(   T =.. [F,A,B],
	    domain(F,[(','),(+>),(<+),(&)])  ->
	    dcg_autoloader(A,Loader1),
	    dcg_autoloader(B,Loader2),
	    autoloader_simplify((Loader1,Loader2),Loader)
	;   T= (A->B) ->
	    dcg_autoloader(A,Loader1),
	    dcg_autoloader(B,Loader2),
	    autoloader_simplify((Loader1,Loader2),Loader)
	;   T= (A->B;C) ->
	    dcg_autoloader((A,B),Loader1),
	    dcg_autoloader(C,Loader2),
	    autoloader_simplify((Loader1 xor Loader2),Loader)
	;   T= (A;B) ->
	    dcg_autoloader(A,Loader1),
	    dcg_autoloader(B,Loader2),
	    autoloader_simplify((Loader1 xor Loader2),Loader)
	;   
	    Loader = true
	)
	.
