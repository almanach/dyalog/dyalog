/* -*- mode:prolog; -*- */

 /******************************************************************
 * $Id$
 * Copyright (C) 1999, 2003, 2004, 2007, 2008, 2010, 2011, 2013, 2016, 2018 by INRIA 
 * Authors : Djam� Seddah <djame@caramail.com>
 *           Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  tag_dyn.pl -- Compiling Item x Trans applications for TAGs
 *
 * ----------------------------------------------------------------
 * Description
 *
 *   This file is similar to seed.pl and application.pl for Logic Programs
 *   It implementats the compilation of 2SA (meta) transitions into
 *   DyALog seeds for the Dynamic Programming interpretation of 2SAs
 *
 *   Unfolding of the 2SA meta transitions (cf tag_compile.pl)
 *   interleaved with the installation of seeds (cf tag_seed.pl)
 *
 *   When installing a seed, templates for potential applications
 *   are called (see app_model in this file) that generate the application
 *   code and recursively call back the unfolding process
 *
 *   The application rules are based on paper "A tabular application for a class
 *   of 2-stack automata" <ftp://ftp.inria.fr/INRIA/Projects/Atoll/Eric.Clergerie/SD2SA.ps.gz>
 *
 *   WRT this paper,
 *        (a) the transitions are grouped into meta transitions using
 *  continuations rather than nabla predicates
 *        (b) application rule are splitted in small pieces using
 *        partial application to always combine a (pseudo) item with
 *        a (pseudo) transition
 * ----------------------------------------------------------------
 */

:-include 'header.pl'.

:-require 'tag_maker.pl'.

:-std_prolog tag_numbervars/3.

:-light_tabular tag_subsumption/1.
:-mode(tag_subsumption/1,+(-)).

tag_subsumption(Kind) :-
	( recorded(subsumption(tag(variance))) ->
	  Kind = variance
	;
	  Kind = yes
	).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  .I Generic Feature Types useful for 2SAs

:-features(sa_mini,[ms,as]). % ms x as components
:-features(sa_escape,
	   [ start,      % sa-mini component
	     end         % ms component
	   ]
	  ).

:-features(sa_env,
	   [ mspop,    % a sa_mini component
	     aspop,    % a MS component
	     holes,    % list of holes (given by a pair Call Foot_Call)
	     escape    % a sa_escape component
	   ]).



:-extensional null_escape/1.

null_escape(sa_escape{ start => [], end => [] } ).


:-std_prolog sa_env_normalize/2.

%% A defaul env ASpop may be normalized into a sa_env
sa_env_normalize(Env,NEnv::sa_env{ aspop=> ASpop,
				   mspop=> MSpop,
				   holes=> Holes,
				   escape=> Escape
				 }) :-
	( var(Env) ->
	    (	MSpop = [],
		Holes = [],
		null_escape(Escape) ->
		Env=ASpop
	    ;	
		Env=NEnv
	    )
	;   Env = sa_env{} ->
	    NEnv = Env
	;   null_escape(Escape),
	    MSpop=[],
	    ASpop=Env,
	    Holes=[]
	)
	.

:-std_prolog sa_env_extension/4.

%% Used to extend an sa env when entering a subtree and to reduce it when exiting
sa_env_extension(Env,
		 sa_env{ aspop=>ASpop,
			 mspop=> MSpop,
			 holes=> [Top,Top_Adj|Holes],
			 escape=>Escape
		       },
		 Top,
		 Top_Adj
		) :-
	sa_env_normalize(Env,sa_env{ aspop=> ASpop,
				     mspop=> MSpop,
				     holes => Holes,
				     escape=> Escape
				   })
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% .II Seeds

%% Call and Return items for substition nodes handled by CITEM and RITEM
%% (instead of specialized CALL-SUBST-ITEM and RET-SUBS-ITEM)

%% Call Item for Aux. Trees
:-features('*CAI*',
	   [ %% aspop,          % ms comp, assume equals current
	     current       % ms comp
	    ]).

seed_model(seed{ model=>'*CAI*'{},
		 c_type=>'*CAI*',
		 tabule=>yes,
		 subsume=>Kind,
		 schedule=>std}) :-
%	tag_subsumption(Kind),
	Kind = yes
	.

%% Return Item for Aux. Trees
:-features('*RAI*',
	   [ %escape,        % sa_escape comp
	     xstart,
	     xend,
	     aspop,         % ms comp proj ms
	     current        % ms comp
	    ]).

seed_model(seed{ model=>'*RAI*'{},
		 c_type=>'*RAI*',
		 tabule=>yes,
		 subsume=>Kind,
		 schedule=>std}) :-
%	tag_subsumption(Kind)
	Kind = yes
	.

%% No Call Item materialized for Foot node
%% Using Pseudo Item '*PI-4-1*' instead (should provide the same level of subsumption)

%% Return Items for Foot Nodes (RFI)
%% No complete items are built but 3 projections are tabulated instead

%% RFI for Rule 6
%% --------------
%% Projection of AB-[<D,d>E]C sur AB-[<*,d>*]C

:-features('*RFI*',
	   [ mspop,                    % ms comp
	     mspop_adj,                 % ms comp
	     current                  % ms comp
	   ]).

seed_model(seed{model=>'*RFI*'{},
		c_type=>'*RFI*',
		tabule=>yes,
		subsume=>Kind,
		schedule=>std}
	  ) :-
%	tag_subsumption(Kind)
	Kind = yes
	.

%% Meta transitions for substitution tree are handled by FIRST
%% (instead of using specialized SA-SUBST-FIRST metatransitions)

%% Meta transitions for aux. trees
seed_model( seed{ model=>'*SA-AUX-FIRST*'(Call) :> Right,
		  c_type=>'*SA-AUX-FIRST*',
		  anchor=> '*SA-AUX-FIRST*'(Call) :> [],
		  tabule=>yes,
		  subsume=>no,
		  tail_flag => flat,
		  schedule => no}) .

%% Pseudo Item CFI (ex PI-4-1)
%% ------------------
%% Projection of an item ABdCw on A**Cw
%% Used in application rules (4)
:-features('*CFI*',
	   [ aspop,     %% ms comp proj ms
	     current    %% ms comp proj ms
	   ]).

seed_model(seed{model=>'*CFI*'{},
		c_type=>'*CFI*',
		tabule=>yes,
		subsume=>Kind,
		schedule=>std
	       }) :-
%	tag_subsumption(Kind)
	Kind = yes
	.

%% Pseudo Trans CFI> (ex PT-4-2)
%% -------------------
seed_model( seed{ model=> (Head::'*CFI*'{}) :> Right,
		  c_type=>'*CFI>*',
		  tail_flag => flat,
		  tabule=>yes,
		  %% subsume => no,
		  subsume=>yes,
		  schedule=> first }).


%% Pseudo Trans RFI> (ex PT-6-23)
%% -------------------

seed_model( seed{ model=> (Head::'*RFI*'{}) :> Right,
		  c_type=>'*RFI>*',
		  tabule=>yes,
		%%  subsume=>no,
		  subsume => yes,
		  tail_flag => flat,
		  schedule => first
		}).

%% Pseudo Trans RAI> (ex PT-13-23)
%% -------------------
seed_model( seed{ model=> (Head::'*RAI*'{}) :> Right,
		  c_type=>'*RAI>*',
		  tabule=>yes,
		  subsume=>Kind,
		  tail_flag => flat,
		  schedule => first
		}) :-
	tag_subsumption(Kind)
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% .III Unfolding

%% Following is only used to help indexing of seeds !
unfold( Label : Trans, Env, T, C, Out_Env ) :-
	unfold(Trans,Env,T,C, Out_Env).

%% Substitution is handled by standard transitions and objects
%% For a subst. node A label a, we have the equivalence of
%%    2SA Call Item: <B><B>new[]<C,new>w <=> LPDA Call Item: *CITEM*(C,B)
%%          with B=C=call_a
%%    2SA Return Item: <B><B>new[]<C,new>e <=> LPDA Ret Item: *RITEM*(C,B)
%%          with B=call_a and C=return_a

unfold( '*SA-SUBST*'(C,R,Right,Node_Id), Env, T, C1 :> C2, Out_Env) :-
	seed_install( seed{ model=>'*SACITEM*'(C)
			  }, 1, C1 ),
	tag_subsumption(Kind),
	seed_install( seed{  %model=>'*RITEM*'(C,R) :> (IndexingKey^Right(Env)(T)),
			      model=>'*RITEM*'(C,R) :> Right(Env)(T),
			      out_env => [Out_Env],
			      subsume => Kind,
			      tail_flag => flat
			  },
		      [2,Node_Id],
		      C2 )
.

unfold( '*SA-SUBST-LIGHT*'(C,R,Right,Node_Id), Env, T, C1 :> C2, Out_Env) :-
	seed_install( seed{ model=>'*SACITEM*'(C),
			    schedule => prolog
			  }, 1, C1 ),
	tag_subsumption(Kind),
	seed_install( seed{ % model=>'*RITEM*'(C,R) :> (IndexingKey^Right(Env)(T)),
			    model=>'*RITEM*'(C,R) :> Right(Env)(T),
			    out_env => [Out_Env],
			    subsume => Kind,
			    tabule => light,
			    schedule => prolog,
			    tail_flag => flat
			  },
		      [2,Node_Id],
		      C2 ),
	choice_code(C2 :> deallocate_layer :> deallocate :> succeed, C1 :> fail, Code)
	.

unfold( '*SA-LAST*'(A), Env, _, Code, Env ) :-
    tag_subsumption(Kind),
    seed_install( seed{ model=>'*RITEM*'(Env,A), subsume => Kind }, 2, Code )
	.

    
    

seed_model( seed{ model=>'*SAFIRST*'(C) :> R,
                  c_type => '*SAFIRST*',
		  anchor=>'*SAFIRST*'(C) :> [],
		  tabule => yes, subsume => no, schedule => no } ).

seed_model( seed{ model=>'*SACITEM*'(_) ,
                  c_type => '*SACITEM*',
		  tabule => yes,
		  subsume => Kind,
		  schedule => Schedule
		}
	  ) :-
	Schedule ?= std,
%	tag_subsumption(_Kind),
%	Kind ?= _Kind
	Kind ?= yes
	.


app_model( application{ mode=> M,
			
			item=>   Item::'*SACITEM*'(Call::Pop),
			trans=>  Trans::('*SAFIRST*'(Call) :> Right),

			item_comp=>   Item,
    
			code=> ( item_unify(V) :> allocate :> Rest ),

			out_env => Out_Env
		      }
	 ) :-
	( M == trans ->
%%	    zero_var(Pop),
	    tupple(Call,T),
	    label_term( Item , V), 
	    unfold(Right :> deallocate :> succeed,
		   Pop,
		   T,
		   Rest,
		   Out_Env)
	;   M == item ->
	    holes(Trans),
	    Out_Env=[]
	;   
	    true
	)
	.



unfold( '*SA-PSEUDO-SUBST*'(C,R,Right,Node_Id), Env, T, C2, Out_Env) :-
	seed_install( seed{ model=>'*RITEM*'(C,R) :> Right(Env)(T),
			    out_env => [Out_Env],
			    tail_flag => flat
			  },
		      [2,Node_Id],
		      C2 )
	.



unfold( '*SA-SUBST-ALT*'(C,R,Right,Node_Id), Env, T, C1 :> C2, Out_Env) :-
%%	seed_install( seed{ model=>'*CITEM*'(C,C) }, 1, C1 ),
	seed_install( seed{ model=>'*SACITEM*'(C) }, 1, C1 ),
	seed_install( seed{ model=>'*RITEM*'(C,R) :> Right(Env)(T),
			    out_env => [Out_Env],
			    subsume => no,
			    tail_flag => flat
			  },
		      [2,Node_Id],
		      C2 )
	.



unfold( U::'*SA-CUTTER*'(Label,Left,EnterTupple,BelowCont,LastCont::'*SA-CUT-LAST*'(Label,Right,ExitTupple),RightCont),
	Env,
	T,
	%C2 :> forget_current_item :> C1 :> deallocate :> succeed,
	C1 :> C2,
	Out_Env) :-
    %    format(';; unfold cutter ~w\n',[U]),
    extend_tupple(Left,EnterTupple,EnterTupple2),
    oset_tupple(EnterTupple,EnterVars),
    oset_tupple(ExitTupple,ExitVars),
    C =.. [Label,Left|EnterVars],
    R =.. [return,Right|ExitVars],
    TmpEnv = C,
    %   format(';; SA-CUTTER C=~w R=~w Below=~w Env=~w TmpEnv=~w\n',[C,R,BelowCont,Env,TmpEnv]),
    tag_subsumption(Kind),
    seed_install( seed{ model => '*RITEM*'(C,R) :> RightCont(Env)(T),
			out_env => [Out_Env],
			subsume => Kind,
			tail_flag => flat
		      },
		  2,
		  C2),

%    format(';;\tDONE C2=~w\n',[C2]),
    seed_install( seed{ model => '*CITEM*'(C,C)}, 1, C1),
%    format('try init install\n',[]),
    XTrans = '*FIRST*'(C) :> BelowCont,
    trans_unfold(XTrans,XLoader),
    null_tupple(NullTupple),
    unfold(XLoader,[],NullTupple,XCode,_),
    record(load_init(XCode) ),
    add_viewer(  '*RITEM*'(C,R), cutter(Left,Right,Label) ),
%    format('done init install\n',[]),

	/*		
    seed_install( seed{ model => '*CONT*' :> BelowCont(TmpEnv)(EnterTupple2),
			out_env => [TmpOutEnv],
%			schedule => prolog,
%			schedule => first,
			subsume => yes,
			tabule => light,
			tail_flag => flat
		      },
		  1,
		  C1
		),
*/
    
%    unfold(BelowCont,TmpEnv,EnterTupple,C1,TmpOutEnv),
    %    format(';;\tDONE C1=~w\n',[C1]),
    true
	.

unfold( '*SA-CUT-LAST*'(Label,Right,ExitTupple),Env,T,Code,Env) :-
    oset_tupple(ExitTupple,ExitVars),
    R =.. [return,Right|ExitVars],
    %    format(';; SA CUT LAST C=~w R=~w\n',[Env,R]),
    tag_subsumption(Kind),
    seed_install( seed{ model => '*RITEM*'(Env,R), subsume => Kind}, 12, Code),
%    format(';; DONE SA CUT LAST\n',[]),
    true
	.

%% Metatransition SA-AUX-LAST: end of aux. tree
%% ------------------------------
%% Installation of a Return Item equivalent to BB+[D,E]Ce
%%   with C=<Return,Current_AS> [D,E]=Escape B=Pop
unfold('*SA-AUX-LAST*'(Return),
       Env::sa_env{  escape=>Escape::sa_escape{ start=>XStart,end=>XEnd},
		     mspop=>[],
		     holes=>[],
		     aspop=> ASPop},
       _,
       Code,
       Env):-
	seed_install(seed{model=>
			 '*RAI*'{ current=> Return,
%				  escape=> Escape,
				  xstart => XStart,
				  xend => XEnd,
				  aspop=> ASPop
				}
			 },
		     2,
		     Code
		    ).

%% Meta transition SA-ADJ-FIRST: adjonction node
%% ------------------------------
%% Install information about 4 different steps:
%%        * Adj Call & Return (call to & return from an auxiliary tree)
%%        * Foot Call & Return (call to & return from the subtree rooted at the adj. node)
unfold('*SA-ADJ-FIRST*'(Top_Call,Bot_Call,Cont),
       Env,
       Tupple,
       C1:>C2,
       Out_Env
      ) :-

	%% Adj. Call: Call Item equivalent to BB+C
	%% with B=Call  C=<Call,Acall>

	seed_install(seed{model=>
			 '*CAI*'{ current=> Top_Call
				  %% , aspop => Top_Call
				}
			 }, 1, C1),

	%% Foot Call: installation of pseudo transition CFI> (ex PT-4-2)
	%% equivalent to CFI :> Closure
	sa_env_extension(Env,XEnv,Bot_Call,Top_Call),
	seed_install(seed{model=>
			 '*CFI*'{ current => Bot_Call,
				  aspop => Top_Call } :>  Cont(XEnv)(Tupple),
			  out_env=>[Out_Env]
			 }, 2, C2)  %% 2
	.

%% Meta transition SA-ADJ-LAST: return of adj subtree
%% ------------------------------
%% Installation of Return Item equivalent to BC pop [DE]Ce
unfold('*SA-ADJ-LAST*'(Top_Return,Bot_Return,Cont,Node_Id),
       XEnv::sa_env{ holes=> [Bot_Call,Top_Call|_]  },
       Tupple,
       C1 :> C2,
       Out_Env
      ) :-

	%%format('ADJ LAST ~w\n\n',[XEnv]),
	
	%% FRET
	seed_install(seed{model=>
			 '*RFI*'{ current=> Bot_Return,
				  mspop=> Bot_Call,
				  mspop_adj => Top_Call
				}
			 }, 1, C1),
	
	%% ARET
	sa_env_extension(Env,XEnv,_,_),
	seed_install(seed{model=>
			 '*RAI*'{ current => Top_Return,
				  aspop => Top_Call,
				  xstart => Bot_Call,
				  xend => Bot_Return
				  /*
				  escape => sa_escape{ start=>Bot_Call,
						       end=>Bot_Return
						     }
                                  */
				} :> Cont(Env)(Tupple),
			  out_env=> [Out_Env]
			 }, [2,Node_Id], C2)
	.

%% Meta transition SA-FOOT: foot node of an aux tree
%% ------------------------------
%% Installation of (pseudo) Call Item CFI (ex PI-4-1)
%% and (pseudo) Return Trans RFI> (ex PT-6-3)
unfold('*SA-FOOT*'(Bot_Call,Bot_Return,Cont),
       Env,
       Tupple,
       C1:>C2,
       Out_Env
      ) :-

%%	format('SA FOOT ~w\n\n',[Env]),
	
	sa_env_normalize(Env, sa_env{ aspop=>ASpop, mspop=>MSpop, holes=>Holes }),
	
	%% Pseudo Call Item CFI (ex PI-4-1)
	seed_install(seed{model=>
			 '*CFI*'{ current=> Bot_Call,
				  aspop=>ASpop }
			 }, 1, C1),

%%	format('SA FOOT CFI ~w\n\n',[C1]),
	
	%% Pseudo Trans RFI> (ex PT-6-3)
	seed_install(seed{ model=>
			 '*RFI*'{ current=> Bot_Return,
				  mspop=> Bot_Call,
				  mspop_adj => ASpop
				}  :> Cont(sa_env{ mspop => MSpop,
						   aspop => ASpop,
						   holes => Holes,
						   escape => sa_escape{ start=>Bot_Call,
									end=>Bot_Return
								      }
						 })(Tupple),
			   out_env=> [Out_Env],
			   tail_flag => flat
			 }, 2, C2),

%%	format('SA FOOT RFI> ~w ~w\n\n',[Out_Env,C2]),

	true
	.

unfold('*SA-PROLOG-LAST*',
       Env,
       _,
       call('Follow_Cont',[L_Cont]),
       Env
      ) :-
	sa_env_normalize(Env,sa_env{ mspop=>Cont } ),
	label_term(Cont,L_Cont)
	.

unfold('*SA-PROLOG-LAST*'(Right),
       Env,
       T,
       unify(RCont,L_RClosure):>call('Follow_Cont',[L_Cont]),
       Out_Env
      ) :-
	sa_env_normalize(Env,sa_env{ mspop=>Cont(RCont,MSPop),
				     escape => Escape,
				     holes => Holes,
				     aspop => ASPop
				   } ),
	sa_env_normalize(Right_Env,
			 sa_env{ aspop => ASPop,
				 mspop => MSPop,
				 holes => Holes,
				 escape => Escape
			       }),
	build_closure(Right,
		      Right_Env,
		      T,
		      RClosure,
		      Out_Env),
	label_term(RClosure,L_RClosure),
	label_term(Cont,L_Cont)
	.

:-mode(sa_foot_wrapper_install/4,+(+,-,-,-)).

sa_foot_wrapper_install( Pred, Fun, V, W ) :-
	(   recorded( '*SA-FOOT-WRAPPER*'(Pred,Id,Args,Cont) )
	xor internal_error('no tabular wrapper for ~w',[Pred]) ),
	tupple(Args,Tupple),
	null_tupple(Void),
%%	format('SA wrapper install mode=~w ~w\n\t~w\n\t~w\n\tArgs1=~w Closure=~w\n',[Mode,Pred,Args,Cont,Args1,Closure]),
	null_escape(Escape),
	Args  =  [Closure|Args1],
	XArgs = [Closure|XArgs1],
	XArgs1= [ASPop|Args1],
	tag_numbervars(ASPop,Id,_),
	Env = sa_env{ aspop => ASPop,
		      mspop => Closure,
		      holes => [],
		      escape => Escape
		    },
	Out_Env = sa_env{ escape => Out_Escape },
%%	format('SA wrapper 2: Cont=~w\n\tEnv=~w XArgs=~w\n',[Cont,Env,XArgs]),
	unfold(Cont :> deallocate_layer :> deallocate :> succeed,
	       Env,
	       Tupple,
	       Code_Cont,
	       Out_Env),
%%	format('SA wrapper 2+: Out_Env=~w\n',[Out_Env]),
	tag_numbervars(Out_Escape,Id,_),
	tupple(Out_Escape,T),
	oset_tupple(T,Tup),
	duplicate_term([ASPop,Tup,Out_Escape|Args1],[CASPop,CTup,COut_Escape|CArgs1],[],_),
	V = [CASPop|CArgs1],
	W = COut_Escape,
%%	format('SA wrapper 3: Mode=~w V=~w W=~w\n',[Mode,V,W]),
	std_prolog_unif_args(XArgs,0,Code,Code_Cont,Void),
%%	format('SA wrapper 4: Code=~w\n',[Code_Cont]),
	label_code( allocate :> allocate_layer :> Code, Fun ),
	register_init_pool_fun( Fun ),
%%	format('SA wrapper register init pool code fun=~w\n',[Fun]),
	true
	.

unfold( Wrapper::'*SA-FOOT-WRAPPER-CALL*'(Pred,Args,Right),
	Env,
	T,
	Code,
	Last_Env
      ) :-	
%%	format('unfold ~w\n\tin env ~w\n\targs=~w\n',[Wrapper,Env,Args]),
	sa_env_normalize(Env,
			 In_Env::sa_env{ aspop=> ASPop,
					 mspop=> MSPop,
					 holes=> Holes
				       }
			),
%%	format('\tin env ~w\n',[In_Env]),
%%	format('Test ~w\n',
%%	       [sa_foot_wrapper_install(Pred,Fun,XArgs::[ASPop|Args],Escape)]),
	sa_foot_wrapper_install(Pred,Fun,XArgs::[ASPop|Args],Escape),
%%	format('SA-WRAPPER-CALL [0] xargs=~w out_env=~w\n',[XArgs,Out_Env]),
	extend_tupple(Args,T,T2),
	build_closure(Right,
		      Out_Env::sa_env{ aspop => ASPop,
				       mspop => MSPop,
				       holes => Holes,
				       escape => Escape
				     },
		      T2,
		      Closure,
		      Last_Env),
%%	format('SA-WRAPPER-CALL [0] 2\n',[]),
	length(XArgs,N),
	M is N+1,
	duplicate_vars([Env,Closure|XArgs],Useful),
	std_prolog_load_args_alt([Closure|XArgs],0,Code,call(Fun,[]) :> reg_reset(M) :> noop,T,Useful),
%%	format('\t=>~w\n\t~w\n',[Last_Env,Code]),
	true
	.


:-std_prolog duplicate_term/4.

duplicate_term(A,B,S1,S2) :-
	( A==B ->
	    S1=S2
	;   check_var(A) ->
	    (	domain(A:B,S1) ->
		S2=S1
	    ;	
		S2=[A:B|S1]
	    )
	;   A = [XA|YA],
	    B = [XB|YB] ->
	    duplicate_term(XA,XB,S1,S3),
	    duplicate_term(YA,YB,S3,S2)
	;   A =.. [F|ArgsA],
	    functor(A,F,N),
	    functor(B,F,N),
	    B =.. [F|ArgsB],
	    duplicate_term(ArgsA,ArgsB,S1,S2)
	)
	.

:-mode(sa_adj_wrapper_install/2,+(+,-)).

sa_adj_wrapper_install(Pred,Fun) :-
%%	format('WRAPPER CALLEE 1: ~w\n',[Pred]),
	( recorded( '*SA-ADJ-WRAPPER*'(Pred,XArgs::[BClosure,RClosure,BCont|Args],First,Last) )
	xor internal_error('no tabular wrapper for ~w',[Pred])),
	tupple(XArgs,Tupple),
	null_escape(Escape),
	null_tupple(Void),
%%	format('WRAPPER CALLEE 2: ~w xargs=~w first=~w last=~w\n',[Pred,XArgs,First,Last]),
	Env_First =  sa_env{ aspop => [],
			     mspop => BClosure,
			     holes => [],
			     escape => Escape
			   },
	Env_Last = sa_env{ aspop=>[],
			   mspop=>RClosure,
			   holes=>[X,Y],
			   escape=>Escape },
	( First = [FirstA|FirstB], Last = [LastA|LastB] ->
	    unfold(FirstA,
		   Env_First,
		   Tupple,
		   FirstA_Cont,
%%		   sa_env{ holes => [X,Y] }
		   _
		  ),
%%	    format('WRAPPER CALLEE 3: ~w [X,Y]=~w firstA=~w\n',[Pred,[X,Y],FirstA_Cont]),
	    unfold(FirstB,
		   Env_First,
		   Tupple,
		   FirstB_Cont,
		   sa_env{ holes => [X,Y] }
		  ),
%	    format('WRAPPER CALLEE 4: ~w [X,Y]=~w firstB=~w\n',[Pred,[X,Y],FirstB_Cont]),
	  LastA_Closure=RClosure, % we assume that LastA = SA-PROLOG-LAST
	  /*
	  build_closure(LastA,
			Env_Last,
			Tupple,
			LastA_Closure,
			_
		       ),
	  */
%%	    format('WRAPPER CALLEE 5: ~w lastA=~w\n',[Pred,LastA_Closure]),
	    build_closure(LastB,
			  Env_Last,
			  Tupple,
			  LastB_Closure,
			  sa_env{ holes => [] }
			 ),
%%	    format('WRAPPER CALLEE 6: ~w lastB=~w\n',[Pred,LastB_Closure]),
	    label_term(LastA_Closure,LastA_Label),
	    label_term(LastB_Closure,LastB_Label),
	    choice_code( unify(BCont,LastB_Label) :> FirstB_Cont :> fail,
			 unify(BCont,LastA_Label) :> FirstA_Cont :> fail,
			 Code_Cont ),
%%	    format('WRAPPER CALLEE 7: ~w choice=~w\n',[Pred,Code_Cont]),
	    true
	;   
	    unfold(First :> deallocate_layer :> deallocate :> succeed,
		   Env_First,
		   Tupple,
		   First_Cont,
		   sa_env{ holes => [X,Y] }
		  ),
	    build_closure(Last,
			  Env_Last,
			  Tupple,
			  Last_Closure,
			  sa_env{ holes => [] }
			 ),
	    label_term(Last_Closure,Last_Label),
	    Code_Cont = unify(BCont,Last_Label) :> First_Cont
	),
	std_prolog_unif_args(XArgs,0,Code,Code_Cont,Void),
	%%	format('SA wrapper 4: Code=~w\n',[Code_Cont]),
	label_code( allocate :> allocate_layer :> Code, Fun ),
	register_init_pool_fun( Fun ),
%	format('WRAPPER CALLEE 8: ~w fun=~x\n',[Pred,Fun]),
	true
	.

unfold( '*SA-ADJ-WRAPPER-CALL*'(Pred,Args,BCont,Below,RCont),
	Env,
	T,
	Code,
	Out_Env
      ) :-
	sa_env_normalize(Env,
			 In_Env::sa_env{ aspop=>  ASPop,
					 mspop=>  MSPop,
					 holes=>  Holes,
					 escape=> Escape
				       }
			 ),
%	format('WRAPPER CALL 1: ~w args=~w BCont=~w\n',[Pred,Args,BCont]),
	sa_adj_wrapper_install(Pred,Fun),
%	format('WRAPPER CALL 2: ~w callee=~x\n',[Pred,Fun]),
	extend_tupple(Args,T,T2),
	build_closure(Below,
		      _In_Env::sa_env{ aspop => ASPop,
				       mspop => BCont(RCont,MSPop),
				       holes => Holes,
				       escape => Escape
				     },
		      T2,
		      BClosure,
		      Out_Env
		     ),
	BClosure = '$CLOSURE'(_,TC),
	oset_tupple(TC,XTC),
	oset_tupple(T,XT),
%%	format('WRAPPER CALL 5: ~w args=~w BClosure=~w tupple=~w  t=~w in_env=~w out_env=>~w\n',[Pred,Args,BClosure,XTC,XT,_In_Env,Out_Env]),
	
	length(Args,N),
	M is N+3,
	duplicate_vars([Env,BClosure,RCont,BCont|Args],Useful),
	std_prolog_load_args_alt([BClosure,RCont,BCont|Args],0,Code,call(Fun,[]) :> reg_reset(M) :> noop,T,Useful)
	.
		     
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% .IV Application rules


%% Rule 10 specialized for the selection of an auxiliary tree
%% --------------------
%% Item  (1) :  <B><B> push [] <C,c>w
%% Trans (tau): <w,C,c> -> <w,*CONT*,c>
%% => <B><B> push [] <*CONT*,c>w
%% -------------------
%% Using only relevant info, we have
%% 
app_model( application{ mode =>M,
			item=> Item:: '*CAI*'{ current=> Call::ASpop
					     %% , aspop => ASpop
					     },
			trans=> Trans:: '*SA-AUX-FIRST*'(Call) :> Right,
			item_comp => Item,
			code => ( item_unify(V)
				:> allocate
				:> Rest
				),
			out_env => Out_Env
		      }
	 ):-
	( M == trans ->
	    %%format( 'CAI app ~w\n\n',[Trans]),
%%	    zero_var(ASpop),
	    label_term(Item,V),
	    tupple(Call,Tupple),
	    unfold( Right :> deallocate :> succeed,
		    ASpop,
		    Tupple,
		    Rest,
		    Out_Env)
	;   M == item ->
	    holes(Trans),
	    Out_Env=[]
	;
	    true
	)
	.

%% Rule FCALL
%% ----------------------
%% ----------------------
%% Rule used when starting from a foot to a subtree
%% 
app_model( application{ mode =>M,
			item => Item::'*CFI*'{},
			trans => Trans:: (Item :> Closure),
			item_comp => Item,
			code => Code,
			out_env => Out_Env
		      }
	 ) :-
	( M == trans ->
	    %%format( 'CFI app ~w\n\n',[Trans]),
	    unfold_std_trans(Trans,Code,Out_Env)
	;   M == item ->
	    holes(Trans),
	    Out_Env=[]
	;   
	    true
	)
	.

%% Rule FRET
%% ---------------
%% ---------------
%% Rule used when returning from a completed subtree to a foot
%% 
app_model( application{ mode =>M,
			item => Item :: '*RFI*'{},
			trans => Trans :: (Item :> Closure),
			item_comp => Item,
			code => Code,
			out_env => Out_Env
		      }
	 ) :-
	( M == trans ->
	    %%format( 'RFI app ~w\n\n',[Trans]),
	    unfold_std_trans(Trans,Code,Out_Env)
	;   M == item ->
	    holes(Trans)
	;   
	    true
	)
	.

%% Rule ARET
%% -------------
%% -------------
%% Rule used when returning from an completed aux tree to an adjonction node
%% 
app_model( application{ mode =>M,
			item => Item :: '*RAI*'{},
			trans => Trans :: ( Item :> Closure ),
			item_comp => Item,
			code => Code,
			out_env => Out_Env
		      }
	 ) :-
	( M == trans ->
	    %%format( 'RAI app ~w\n\n',[Trans]),
	    unfold_std_trans(Trans,Code,Out_Env)
	;   M == item ->
	    holes(Trans)
	;
	    true
	)
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Registering viewer for displaying forests

:-rec_prolog tag_viewer/6,tag_node_viewer/4.

tag_viewer(subst,T,Left,Right,Call,Return) :-
        I = '*RITEM*'(Call,Return),
	numbervars(I),
        label_term(I,CI),
        label_term(T(Left,Right),CT),
        record_without_doublon( viewer(CI,CT) )
	.

tag_viewer(top,Top,Top_Left,Top_Right,Top_Call,Top_Return) :-
	functor(Top,F,N),
	functor(Bot,F,N),
	make_tag_bot_callret(Bot,Bot_Left,Bot_Right,Bot_Call,Bot_Return),
        I = '*RAI*'{ current=>Top_Return,
		     aspop=>Top_Call,
%		     escape=>sa_escape{ start=>Bot_Call, end=>Bot_Return }
		     xstart => Bot_Call,
		     xend => Bot_Return
		   },
	numbervars(I),
        label_term(I,CI),
        label_term(Top(Top_Left,Top_Right)*Bot(Bot_Left,Bot_Right),CT),
        record_without_doublon( viewer(CI,CT) )
	.

:-std_prolog add_viewer/2.

add_viewer(Item,View) :-
	numbervars(Item),
	label_term(Item,Item_Ref),
	label_term(View,View_Ref),
	record_without_doublon( viewer(Item_Ref,View_Ref) )
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

:-rec_prolog reach_foot/2.

reach_foot([tag_node{ spine=>no }|Children],Foot) :-
	reach_foot( Children,Foot ).

reach_foot([tag_node{ spine=>yes, kind=>std, children=>Children }|_],Foot) :-
	reach_foot( Children,Foot ).

reach_foot([Foot::tag_node{ kind=>foot, top=>Top,bot=>Bot }|_],Foot).


