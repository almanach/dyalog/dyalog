/* -*- mode:prolog; -*-
 ************************************************************
 * $Id$
 * Copyright (C) 1998 Eric de la Clergerie
 * ------------------------------------------------------------
 *
 *   Maker --  Builder for Auxiliary Atoms (for LPDAs)
 *
 * ------------------------------------------------------------
 * Description
 *   Build call and return forms associated to various TAG operations
 *            - substitution (driven by a non terminal)
 *            - adjonction (drivent by a non terminal)
 *            - foot parsing (driven by a node)
 *
 *   Currently, the same modulation is used for substitutions and adjonctions
 *   given by declaration :-tag_mode(NT,Pat,LPat,RPat)
 *          (for instance :-tag_mode(gn,+,+,-)
 *
 *   Currently, no way to alter the default modulation on foot
 * ------------------------------------------------------------
 */

:-include 'header.pl'.
:-require 'maker.pl'.

/**********************************************************************
 * Call Return TAG Maker
 **********************************************************************/

:-rec_prolog tag_viewer/6,tag_node_viewer/4.

:-rec_prolog
	make_tag_node_callret/5,
	make_tag_callret/7,
	make_tag_subst_callret/5,
	make_tag_top_callret/5,
	make_tag_bot_callret/5
	.

%% Call/Ret for a node
make_tag_node_callret(TreeName,Node::tag_node{ id=>Node_Id, label=> NT, bot=>Bot, children=>Children },Call,Return,Node_Id) :-
	( number(Node_Id) ->
	    name_builder('~w:#~w',[TreeName,Node_Id],Info)
	;   
	    name_builder('~w:~w',[TreeName,Node_Id],Info)
	),
	(   recorded( 'tag_node_modulation'(NT/0,Pat) )
	xor recorded( 'tag_node_modulation'('*default*',Pat) )
	xor Pat = (+) ),
	tupple( Bot*Children, Tupple ),
	( Pat == (+) ->
	    Call =.. [Info,Tupple],
	    Return = 'footret'
	;   Pat == (-) ->
	    Call = 'footcall',
	    Return =.. [Info,Tupple]
	;   
	    Call =.. [Info,Tupple],
	    Return =.. [Info,Tupple]
	),
	( tag_node_viewer(Node,Info,Call,Return), fail ; true )
	.

%% Call/Ret for a Substition Non Terminal
make_tag_subst_callret(T,Left,Right,Call,Return) :-
	make_tag_callret(T,Left,Right,
			 Call,Return,
			 subst*(Filter::tag_modulation{ subst_modulation=> Modulation }),
			 Modulation)
	.

%% Call/Ret for a Top Non Terminal
make_tag_top_callret(T,Left,Right,Call,Return) :-
	make_tag_callret(T,Left,Right,
			 Call,Return,
			 top*(Filter::tag_modulation{ top_modulation=> Modulation }),
			 Modulation)
	.

%% Call/Ret for a Bot Non Terminal
make_tag_bot_callret(T,Left,Right,Call,Return) :-
	make_tag_callret(T,Left,Right,
			 Call,Return,
			 bot*(Filter::tag_modulation{ bot_modulation=> Modulation }),
			 Modulation)
	.

make_tag_callret(T,Left,Right,Call,Return,Filter,Modulation) :-
	functor(T,P,N),
	functor(TT,P,N),
	make_true_tag_callret(TT,XLeft,XRight,Call,Return,Filter,Modulation),
	T*Left*Right=TT*XLeft*XRight
	.

:-light_tabular make_true_tag_callret/7.

make_true_tag_callret(T,Left,Right,Call,Return,
                      Kind*Filter,
		      Modulation::nt_modulation{ nt_pattern => Pat,
						 left_pattern => LPat,
						 right_pattern => RPat
					       }) :-
	functor(T,P,N),
	( recorded( 'tag_modulation'(P/N,Filter) ) -> true
	;   recorded( 'tag_modulation'('*default*',Filter ) ) -> true 
	;   Pat=(+), LPat=(+), RPat=(-)
	),

	( Pat = [FPat|Args_Pat] ->
	    Pattern = [FPat,LPat,RPat|Args_Pat]
	;   compound(Pat) ->
	    Pat =.. [FPat|Args_Pat],
	    Pattern = [FPat,LPat,RPat|Args_Pat]
	;   
	    Pattern = [Pat,LPat,RPat|Pat]
	),
	T =.. [F|Args],
	TT =.. [F,Left,Right|Args],
	make_callret_with_pat(Pattern,TT,Call,Return),
	(   tag_viewer(Kind,T,Left,Right,Call,Return), fail ; true )
	.










