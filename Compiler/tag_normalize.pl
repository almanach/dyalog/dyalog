/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 1998, 2002, 2003, 2004, 2005, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2021 by INRIA 
 * Authors:
 *      Djame Seddah <Djame.Seddah@inria.fr>
 *      Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  tag_normalize.pl -- TAG normalizer
 *
 * ----------------------------------------------------------------
 * Description
 *  Normalize Tree Adjoining Grammars
 * ----------------------------------------------------------------
e */

:-include 'header.pl'.

:-rec_prolog
	normalize/5,
	normalize_args/5,
	feature_normalize/3
	.

:-extensional adjrestr/3.

:-finite_set(adj,[no,yes,strict,atmostone,one,sync]).
:-subset(possible_adj,adj[~ [no]]).
:-subset(required_adj,adj[strict,one]).
:-subset(possible_but_not_required_adj,adj[yes,atmostone,sync]).
:-subset(not_required_adj,adj[~ [strict,one]]).

normalize(Tree_with_Modifier,
	  Normalized_Tree,
	  Left,
	  Right,
	  Tree_Name
	 ):-
	Skel = tag_node{ id => Id,
			 label => Label,
			 children => Children,
			 kind => Kind,
			 adj => Adj::adj[],
			 adjleft => adj[],
			 adjright => adj[],
			 adjwrap => adj[],
			 spine => Spine,
			 top=>Top,
			 bot=>Bot
		       },
	( %% SubstNode with variable label
	    (	var(Tree_with_Modifier),  Label = '$call'(Tree_with_Modifier)
		xor	Tree_with_Modifier = '$VAR'(_),  Label = '$call'(Tree_with_Modifier)
		xor Tree_with_Modifier = '$call'(_), Label = Tree_with_Modifier
	    ) ->
	    Normalized_Tree = Skel,
	    (	Adj = no xor error( 'Not a valid TAG adjonction node ~w',[Tree_with_Modifier])),
	    Kind=subst,
	    Left=Right,
	    set_spine(Left,Spine),
	    misc_check(Normalized_Tree,Tree_with_Modifier,Tree_Name)
	%% ---------
	%% Meta Call
	;   Tree_with_Modifier =.. [apply,Label1|Rest] ->
	    Normalized_Tree = Skel,
	    Label = '$call'(Label1),
	    normalize_args(Rest,Children,Left,Right,Tree_Name),
	    ( Left \== Right -> Spine = yes ; set_spine(Left,Spine) ),
	    misc_check(Normalized_Tree,Tree_with_Modifier,Tree_Name)
	%% ------------
	%% Disjunction
	;   
	    Tree_with_Modifier = (T1;T2) ->
	    Normalized_Tree = (NT1;NT2),
	    (	normalize(T1,NT1,Left,Right,Tree_Name),
		normalize(T2,NT2,Left,Right,Tree_Name)
	    xor error('Incompatible TAG alternatives on ~w\n',[T])
	    )
	%% -----------
	%% guards
	;
	    Tree_with_Modifier = guard{ goal => T1, plus => Plus, minus => Minus } ->
	    (	Plus = true xor true),
	    (	Minus = fail xor true),
	    normalize(T1,NT1,Left,Right,Tree_Name),
	    ( fail, Plus = fail ->
	      normalize({Minus},Normalized_Tree,Left,Right,Tree_Name)
	    ; Plus = true, Minus = fail ->
	      Normalized_Tree = NT1
	    ;

	      guard_factorize(Plus,XPlus1),
	      guard_linearize(XPlus1,XPlus2),
	      guard_push_disjunctions(XPlus2,XPlus),
/*	      (Plus == XPlus
	      xor
	      format('\nTree ~w\n',[Tree_Name]),
	      format('Guard factorize plus=~w\n',[Plus]),
	      format('\t=>xplus=~w\n',[XPlus])
	      ),
*/
	      Normalized_Tree = guard{ goal => NT1, plus => XPlus, minus => Minus }
	    )
	%% ------------
	%% Optional
	;   Tree_with_Modifier = (T1 ?) ->
	  normalize(T1,NT1,Left,Right,Tree_Name),
	  Normalized_Tree = guard{ goal => NT1, plus => true, minus => true}
%%	    normalize(([];T1),Normalized_Tree,Left,Right,Tree_Name)
	%% ------------
	%% Sequence
	;   Tree_with_Modifier = (T1,T2) ->
%	    Normalized_Tree = [NT1,NT2],
	    normalize(T1,NT1,Left,Middle,Tree_Name),
	    normalize(T2,NT2,Middle,Right,Tree_Name),
	    %% we flatten sequences, when possible
	    ( NT2 = []  ->
		Normalized_Tree = NT1
	    ; NT2 = [_|_] ->
		(NT1 = [_|_] ->
		    append(NT1,NT2,Normalized_Tree)
		;
		    Normalized_Tree = [NT1|NT2]
		)
	    ; NT1 = [_|_] ->
		append(NT1,[NT2],Normalized_Tree)
	    ;	
		Normalized_Tree = [NT1,NT2]
	    )
	%% ------------
	%% Kleene
	;   Tree_with_Modifier = (T1 @*) ->
	    normalize(@*{ goal => T1},Normalized_Tree,Left,Right,Tree_Name)
	%% ------------
	%% Generalized Kleene
	;   Tree_with_Modifier = @*{} ->
%%	    format('Normalize Kleene ~w\n',[Tree_with_Modifier]),
	    kleene_normalize(Tree_with_Modifier,
			     @*{ goal => T1,
				 vars => K_Vars,
				 from => K_From,
				 to => K_To,
				 collect_first => KCF,
				 collect_last => KCL,
				 collect_loop => KCLoop,
				 collect_next => KCN,
				 collect_pred => KCP
			       }),
	    Normalized_Tree = @*{ goal => NT1,
				  vars => K_Vars,
				  from => K_From,
				  to => K_To,
				  collect_first => KCF,
				  collect_last => KCL,
				  collect_loop => KCLoop,
				  collect_next => KCN,
				  collect_pred => KCP
				},
	    normalize(T1,NT1,Left,Right,Tree_Name),
%%	    format('\t-->~w\n',[Normalized_Tree]),
	    (	Left == Right
	    xor error('Kleene star not allowed around TAG spine nodes: ~w\n',
		      [T1])
	    )
	%% -----------
	%% Interleave
	; Tree_with_Modifier = (T1 ## T2) ->
	    normalize(T1,NT1,Left,Middle,Tree_Name),
	    normalize(T2,NT2,Middle,Right,Tree_Name),
	    Normalized_Tree = (NT1 ## NT2)
	%% ------------
	%% Obligatory adjonction node
	;   Tree_with_Modifier = ++ Tree ->
	    Normalized_Tree = Skel,
	    (	Adj = strict xor error( 'Multiple incompatible marks on ~w',[Tree_with_Modifier])),
	    normalize(Tree,Normalized_Tree,Left,Right,Tree_Name)
	%% ------------
	%% Non obligatory adjonction node
	;   Tree_with_Modifier = +Tree ->
	    Normalized_Tree = Skel,
	    (	Adj = possible_adj[] xor error( 'Conflict on TAG modifiers ~w',[Tree_with_Modifier])),
	    normalize(Tree,Normalized_Tree,Left,Right,Tree_Name)
	%% ------------
	%% Non adjonction node
	;   Tree_with_Modifier = -Tree ->
	    Normalized_Tree = Skel,
	    (	Adj = no xor error( 'Multiple incompatible marks on ~w',[Tree_with_Modifier])),
	    normalize(Tree,Normalized_Tree,Left,Right,Tree_Name)
	%% ------------
	%% Anchor node (at most one per tree :: don't how to ensure it !)
	;   Tree_with_Modifier = (<> Label1) ->
	    Normalized_Tree = Skel,
	    ( normalize_label(Label1,Label) xor error( 'Not a valid TAG anchor ~w', [Tree_with_Modifier])),
	    ( \+ recorded( tag_anchor(Tree_Name,_) ) xor error( 'More than one anchor in tree ~w: ~w', [Tree_Name,Tree_with_Modifier])),
	    Kind=anchor,
	    Left = Right,
	    set_spine(Left,Spine),
	    misc_check(Normalized_Tree,Tree_with_Modifier,Tree_Name),
	    record( tag_anchor(Tree_Name,Id) )
	%% ------------
	%% CoAnchor node (almost like anchor node and a lex node)
	;   Tree_with_Modifier = (<=> Label1) ->
	    Normalized_Tree = Skel,
	    ( normalize_label(Label1,Label) xor error( 'Not a valid TAG coanchor ~w', [Tree_with_Modifier])),
	    Kind=coanchor,
	    Left = Right,
	    set_spine(Left,Spine),
	    misc_check(Normalized_Tree,Tree_with_Modifier,Tree_Name)
	%% -------------
	%% Features
	;   Tree_with_Modifier = (Features at Tree) ->
	    Normalized_Tree = Skel,
	    feature_normalize(Features,Normalized_Tree,Tree),
	    normalize(Tree,Normalized_Tree,Left,Right,Tree_Name)
	%% -------------
	%% Foot node or SubstFoot node
	;   (Tree_with_Modifier = * Label1, Kind = foot
	    ;	Tree_with_Modifier = (= Label1), Kind = substfoot ) ->
	    Normalized_Tree = Skel,
	    (	normalize_label(Label1,Label) xor error( 'Not a valid TAG foot ~w', [Tree_with_Modifier])),
	    %% We do not allow adjonction on a foot node
	    (	Adj = adj[no] xor error( 'Not a valid TAG adjonction node ~w (~w)',[Tree_with_Modifier,Adj])),
	    %% Check that the foot node is the first foot node in an aux tree
	    %% ( we no longer check that the label equals the root label
	    %%   to allow for spinetrees. Actually, there is no need for a label
	    %%   on foot nodes ! )
	    ( Left == nospine ->
		error( 'foot ~w in elementary tree',[Tree_with_Modifier])
	    ;	Left == right ->
		error( 'more than one foot ~w: ~w',[Tree_Name,Tree_with_Modifier])
	    ;	Left = left(_) -> %% no check on label
		Right = right
	    ;	
		error( 'Not a valid TAG foot ~w ~w ~w',[Tree_with_Modifier,Left,Right])
	    ),
	    Spine=yes,
	    misc_check(Normalized_Tree,Tree_with_Modifier,Tree_Name)
	%% --------------
	%%  Scan node
	;   ( Tree_with_Modifier= []
	    ; Tree_with_Modifier = [_|_]
	    ) ->
	    Normalized_Tree = Skel,
	    (	Adj = no xor error( 'Not a valid TAG adjonction node ~w',[Tree_with_Modifier])),
	    Kind = scan,
	    Label = Tree_with_Modifier,
	    Children = [],
	    set_spine(Left,Spine),
	    Left = Right,
	    (Id=[] xor true),
	    (Top = [], Bot = [] xor error('Features on scan node',[Tree_with_Modifier]))
	%% --------------
	%% skip predicates
	; Tree_with_Modifier = '$skip' ->
	  Normalized_Tree = Tree_with_Modifier,
	  Left = Right
	%% --------------
	%% Substitution node
	;   atomic(Tree_with_Modifier) ->
	    %% No adj allowed on substitution mode
	    Normalized_Tree = Skel,
	    (	Adj = no xor error( 'Not a valid TAG adjonction node ~w',[Tree_with_Modifier])),
	    Kind=subst,
	    normalize_label(Tree_with_Modifier,Label),
	    Left = Right,
	    set_spine(Left,Spine),
	    misc_check(Normalized_Tree,Tree_with_Modifier,Tree_Name)
	%% --------------
	%% Escape node (escape to Prolog)
	;   Tree_with_Modifier = {Label} ->
	    Normalized_Tree = Skel,
	    (	Adj = no xor  error( 'Not a valid TAG adjonction node ~w',[Tree_with_Modifier])),
	    Kind = escape,
	    Children=[],
	    set_spine(Left,Spine),
	    Left=Right,
	    Id=[],
	    (	Top = [], Bot = [] xor error('Features on escape  node',[Tree_with_Modifier]))
	%% --------------
	%% Answer traversal
	;   Tree_with_Modifier = '$answers'(T1) ->
	    Normalized_Tree = '$answers'(NT1),
	    normalize(T1,NT1,Left,Right,Tree_Name)
	%% --------------
	%% Protected node
	;   Tree_with_Modifier = '$protect'(T1) ->
	    Normalized_Tree = '$protect'(NT1),
	    normalize(T1,NT1,Left,Right,Tree_Name)
	%% --------------
	%% Position Predicate
	; Tree_with_Modifier = '$pos'(_) ->
	    Normalized_Tree = Tree_with_Modifier,
	    Left = Right
	%% --------------
	%% Compound node
	;   Kind = std,
	    Normalized_Tree = Skel,
	    Tree_with_Modifier =.. [Label|Rest],
	    normalize_args(Rest,Children,Left,Right,Tree_Name),	    
	    ( Left \== Right -> Spine = yes ; set_spine(Left,Spine) ),
	    misc_check(Normalized_Tree,Tree_with_Modifier,Tree_Name)
	),
	abolish(tag_anchor/2),
	abolish(tag_node_id/2)
	.


:-std_prolog normalize_label/2.

normalize_label(Label1,Label) :-
	( var(Label1) -> Label = '$call'(Label)
	;   atomic(Label1),
	    Label=Label1
	)
	.

:-std_prolog set_spine/2.

set_spine(Left,Spine) :-
	( Left = nospine ->
	    Spine=no
	;   Left = right ->
	    Spine=right
	;   
	    Spine=left
	)
	.

normalize_args([],[],Left,Left,_).
normalize_args([Head|Args],Out,Left,Right,Tree_Name):-
	normalize(Head,T,Left,Middle,Tree_Name),
	normalize_args(Args,Q,Middle,Right,Tree_Name),
	%% if T is a sequence, we flatten it in the argument list
	( T = [] ->
	    Out = Q
	; T = [_|_] ->
	    append(T,Q,Out)
	;   Out = [T|Q]
	)
	.

feature_normalize(Features,
		  Normalized_Tree::tag_node{ bot=> Bot,
					     top=>Top,
					     id=> Id,
					     token => Token,
					     adjleft => AdjLeft,
					     adjright => AdjRight,
					     adjwrap => AdjWrap
					   },
		  Tree) :-
	( Features = (F1 and F2) ->
	    feature_normalize(F1, Normalized_Tree, Tree),
	    feature_normalize(F2, Normalized_Tree, Tree)
	;   Features = (top = F1) ->
	    ( var(F1) -> Top=F1
	    ;	
		functor(F1,Functor,N),
		term_current_module_shift(tag(Functor/N),_,F1,F),
		(   F = Top xor error('top feature conflict ~w with ~w at ~w',[F,Top,Tree]))
	    )
	;   Features = (bot = F1) ->
	    ( var(F1) -> Bot =F1
	    ;	functor(F1,Functor,N),
		term_current_module_shift(tag(Functor/N),_,F1,F),
		(   F = Bot xor error('bot feature conflict ~w with ~w at ~w',[F,Top,Tree]))
	    )
	;   Features = (id=F) ->
	    (	F = Id xor error('node id conflict ~w with ~w at ~w',[F,Id,Tree]))
	;   Features = (token=F) ->
	    (	F = Token xor error('node token conflict ~w with ~w at ~w',[F,Token,Tree]) )
	;   Features = (adjleft=F) ->
	  ( AdjLeft = F xor error('adjleft conflict ~w with ~w at ~w',[F,AdjLeft,Tree]) ),
	  ( domain(F,adj[]) xor error('bad adjleft value ~w at ~w',[F,Tree]))
	;   Features = (adjright=F) ->
	  ( AdjRight = F xor error('adjright conflict ~w with ~w at ~w',[F,AdjLeft,Tree]) ),
	  ( domain(F,adj[]) xor error('bad adjright value ~w at ~w',[F,Tree]) )
	;   Features = (adjwrap=F) ->
	  ( AdjWrap = F xor error('adjwrap conflict ~w with ~w at ~w',[F,AdjWrap,Tree]) ),
	  ( domain(F,adj[]) xor error('bad adjwrap value ~w at ~w',[F,Tree]) )
	;   
	    error( 'not a valid feature ~w at ~w',[Features,Tree])
	)
	.

:-std_prolog feature_check/3.

feature_check(Label,Top,Bot) :-
	( Label = '$call'(_) ->
	    true
	;   recorded( tag_features(Label,T,B) ) ->
	    (	Top=T, Bot=B
	    xor error('features top ~w or bot ~w for ~w do not unify with defaults top ~w or bot ~w',[Top,Bot,Label,T,B]))
	;   
	    (	Label=Top,Label=Bot 
	    xor functor(Top,Label,N),functor(Bot,Label,N)
	    xor functor(Bot,Label,N),functor(Top,Label,N)
	    xor error('features top ~w or bot ~w not built on same non terminal ~w',[Top,Bot,Label])
	    )
	).

:-rec_prolog misc_check/3.

misc_check( Normalized_Tree :: tag_node{ id => Id,
					 label => Label,
					 children => Children,
					 adj => Adj,
					 spine => Spine,
					 top=>Top,
					 bot=>Bot,
					 kind => Kind,
					 token => Token,
					 adjleft => AdjLeft,
					 adjright => AdjRight,
					 adjwrap => AdjWrap
				       },
	    Tree_with_Modifier,
	    Tree_Name
	 ) :-
	feature_check(Label,Top,Bot),
	( var(Top) ->
	  AdjLeftRestr=yes,
	  AdjRightRestr=yes,
	  AdjWrapRestr=yes
	;
	  functor(Top,F,N),
	  ( adjrestr(F/N,left,AdjLeftRestr) xor AdjLeftRestr=yes),
	  ( adjrestr(F/N,right,AdjRightRestr) xor AdjRightRestr=yes),
	  ( adjrestr(F/N,wrap,AdjWrapRestr) xor AdjWrapRestr=yes)
	),
	( Adj==no ->
	  (	Top = Bot
	  xor error('Conflicting features top ~w and bot ~w on node ~w',[Top,Bot,Tree_with_Modifier])),
	  AdjLeft ?= no,
	  AdjRight ?= no,
	  AdjWrap ?= no
	; Adj == strict ->
	  AdjLeft ?= AdjLeftRestr,
	  AdjRight ?= AdjRightRestr,
	  AdjWrap ?= AdjWrapRestr,
	  true
	; \+ Top = Bot ->
	  Adj = strict,
	  AdjLeft ?= AdjLeftRestr,
	  AdjRight ?= AdjRightRestr,
	  AdjWrap ?= AdjWrapRestr
	;
	  Adj = yes,
	  AdjLeft ?= AdjLeftRestr,
	  AdjRight ?= AdjRightRestr,
	  AdjWrap ?= AdjWrapRestr
	),
	name_tag_node(Id,Tree_Name),
	Spine ?= no,
	Children ?=[],
	(   domain(Kind,[anchor,coanchor])
	xor Token = []
	xor warning('Token info ~w useless in tree for node ~w',[Token,Tree_Name,Tree_with_Modifier])
	)
	.

:-rec_prolog name_tag_node/2.

%% Local name of node in its tree
%% Should be unique in current tree
name_tag_node( Id, Tree_Name ) :-
	( nonvar( Id ) ->
	    true
	;
	    update_counter( tag_nodeid(Tree_Name), Id )
	),
	( recorded( tag_node_id(Tree_Name,Id) ) ->
	    error( 'duplicate local name ~w for tag node in tree ~w', [Id,Tree_Name] )
	;   
	    record( tag_node_id(Tree_Name,Id) )
	)
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Wrapping function to normalization

:-light_tabular normalize/2.
:-mode(normalize/2,+(+,-)).

normalize( tag_tree{ name=> Name,
		     family=>Family,
		     tree => T },
	   L ) :-
	%% See file note2.txt for naming conventions
	( var(Family) ->
	    (	recorded( tag_family( Family ) ) ->
		true
	    ;	var(Name) ->
		recorded( main_file(_,info(_,Family,_,_)) )
	    ;	Family=Name
	    )
	;   true
	),
	( var(Name) ->
	    update_counter( tag_trees, Id ),
	    number_chars( Id, Char_Id ),
	    atom_chars( LL, [0't,0'r,0'e,0'e|Char_Id] ),
	    default_module(LL,Name)
	;   true
	),
	( recorded( tag_tree(Name,_) ) ->
	    error( 'two TAG trees with same name ~w\n',[Name])
	;   record( tag_tree(Name,Family) )
	),
	normalize( T, L, Name ),
	%% Cleaning
	erase( tag_node_id(Name,_) )
	.

:-rec_prolog normalize/3.

normalize((tree T),L,Name):-
	normalize(T,L,nospine,nospine,Name)
%	,format('Normalized: ~w\n\t~w\n\n',[T,L])
	.

normalize((auxtree T),L,Name):-
	normalize(T,L::tag_node{ label=>Root, spine=>Spine },left(Root),Right,Name),
	(Spine == yes, Right == right xor error('missing foot node in ~w\n',[T]))
%%	,format('Normalized: ~w\n\t~w\n\n',[T,L])
	.

normalize((spinetree T),spinetree(L),Name):-
	normalize(T,L::tag_node{ label=>Root, spine=>Spine },left(Root),Right,Name),
	(Spine == yes, Right == right xor error('missing foot node in ~w\n',[T]))
	.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Modules

term_module_shift(Module,Pred1::tag(F1/N),tag(F/N),T1,T) :-
       	( recorded( module_import(Pred1,Module1) ) ->
	    Module1 \== []
	;   Module \== [],
	    atom_module(F1,[],_),
	    Module1=Module
	),
        atom_module(F,Module1,F1),
        length(Args,N),
        T1 =.. [F1|Args],
        T =.. [F|Args]
        .

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

:-rec_prolog guard_factorize/2.

guard_factorize(true,true).
guard_factorize(fail,fail).
guard_factorize(G::(X=Y),G).
guard_factorize(G::(\+ \+ X=Y),G).
guard_factorize(G::(X == Y),G).

guard_factorize((G1,G2),(XG1,XG2)) :-
	guard_factorize(G1,XG1),
	guard_factorize(G2,XG2)
	.

guard_factorize(G::(G1;G2),XG) :-
	guard_factors(G,[],Fs),
	%%	format('\tfactors ~w ~w\n',[G,Fs]),
	%	factor_sort(Fs,XFs),
	( 
	    %   factor_max(Fs,F,_)
	    domain(F:N,Fs),
	    N > 1,
	    \+ (( domain(_:_N,Fs), _N > N))
	 ->
		%	 format('\tsorted factors ~w\n',[XFs]),
		%% there is a possible factor
		%% we separe G into TG and FG
		%%    TG all share factor F1
		%%    FG do not share factor F1
		%	 format('\nUSE FACTOR ~w on ~w\n',[F,G]),
		guard_use_factor(G,F,TG,FG),
		%	 format('\tGOT TG=~w FG=~w\n',[TG,FG]),
		%%	 guard_factorize(TG,XTG),
		%%	 guard_factorize(FG,XFG),
		guard_and_combine(F,TG,K),
		guard_or_combine(K,FG,XG),
		%%	 guard_display(F,G,XG),
		true
	 ;
	 XG = G
	)
	.

:-std_prolog guard_display/3.

guard_display(F,G,YG) :-
	guard_simplify((F,G,YG),(XF,XG,XYG),[],_),
	format('\nuse factor ~w on ~w\n',[XF,XG]),
	format('\tgot ~w\n',[XYG]),
	true
	.

:-std_prolog guard_simplify/4.

guard_simplify(G,XG,L,XL) :-
	( G = (G1,G2) ->
	  guard_simplify(G1,XG1,L,M),
	  guard_simplify(G2,XG2,M,XL),
	  XG=(XG1,XG2)
	; G = (G1;G2) ->
	  guard_simplify(G1,XG1,L,M),
	  guard_simplify(G2,XG2,M,XL),
	  XG=(XG1;XG2)
	; G = true ->
	  XG = G,
	  L=XL
	; G = fail ->
	  XG = G,
	  L=XL
	; guard_lookup(G,L,XG) ->
	  L=XL
	;
	  gensym(XG),
	  XL = [G:XG|L]
	)
	.

:-std_prolog guard_lookup/3.

guard_lookup(G,[_G:_XG|XL],XG) :-
	( _G==G -> _XG=XG ; guard_lookup(G,XL,XG) )
	.

:-rec_prolog guard_factors/3.

guard_factors((G1;G2),Fs0,Fs2) :-
	guard_factors(G1,Fs0,Fs1),
	guard_factors(G2,Fs1,Fs2)
	.
guard_factors((G1,G2),Fs0,Fs2) :-
	%% not perfect if some factor occurs both in G1 and G2 !
	guard_factors(G1,Fs0,Fs1),
	guard_factors(G2,Fs1,Fs2)
	.
guard_factors(true,Fs,Fs).
guard_factors(fail,Fs,Fs).
guard_factors(E::(X=Y),Fs0,Fs1) :-
	guard_factors_aux(E,Fs0,Fs1).
guard_factors(E::(\+ \+ X=Y),Fs0,Fs1) :-
	guard_factors_aux(E,Fs0,Fs1).
guard_factors(E::(X==Y),Fs0,Fs1) :-
	guard_factors_aux(E,Fs0,Fs1).


:-std_prolog guard_factors_aux/3.

guard_factors_aux(E,L0,L1) :-
	( L0 = [] ->
	      L1 = [E:1]
	 ; \+ domain((E:_),L0) ->
	       L1 = [E:1|L0]
	; L0 = [XE:N|XL],
%%	  format('TEST e=~w with xe=~w\n',[E,XE]),
	  ( XE == E ->
	    M is N+1,
%%	    format('\thit n=~w\n',[M]),
	    L1 = [XE:M|XL]
	  ;
	    guard_factors_aux(E,XL,XL1),
	    L1 = [XE:N|XL1]
	  )
	)
	.


:-std_prolog factor_max/3.

factor_max(Fs,F,N) :-
    (Fs = [F:N] ->
	 N > 1
     ; Fs = [F1:N1|Fs1],
       ( factor_max(Fs1,F2,N2) ->
	     ( N1 > N2 ->
		   F=F1,
		   N=N2
	      ;
	      F = F2,
	      N = N2
	     )
	; N1 > 1,
	  F = F1,
	  N = N1
       )	      
    )
.

:-std_prolog factor_sort/2.

factor_sort(Fs1,Fs2) :-
	( Fs1 = [] -> Fs2 = []
	; Fs1 = [E:N|XFs1] ->
	  factor_sort(XFs1,XFs2),
	  ( N  = 1 ->
	    Fs2=XFs2
	  ;
	    factor_sort_add(E,N,XFs2,Fs2)
	  )
	)
	.

:-std_prolog factor_sort_add/4.

factor_sort_add(E,N,Fs1,Fs2) :-
	( Fs1 = [] ->
	  Fs2 = [E:N]
	; Fs1 = [E1:N1|XFs],
	  ( N1 > N ->
	    factor_sort_add(E,N,XFs,XFs2),
	    Fs2 = [E1:N1|XFs2]
	  ;
	    Fs2 = [E:N|Fs1]
	  )
	)
	.
	
:-std_prolog guard_use_factor/4.

%:-light_tabular guard_use_factor/4.
%:-mode(guard_use_factor/4,+(+,+,-,-)).

guard_use_factor(G,F,TG,FG) :-
	( G = (G1,G2) ->
	  guard_use_factor(G1,F,TG1,FG1),
	  guard_use_factor(G2,F,TG2,FG2),
	  ( TG1=fail,
	    TG2=fail ->
	    TG=fail,
	    guard_and_combine(FG1,FG2,FG)
	  ; 
	    guard_and_combine(TG1,TG2,K1),
	    guard_and_combine(TG1,FG2,K2),
	    guard_and_combine(FG1,TG2,K3),
	    guard_or_combine(K1,K2,V1),
	    guard_or_combine(K3,V1,_TG),
%	    (_TG == K3, TG=_TG xor _TG==K2, TG=_TG xor _TG == K1, TG=_TG xor guard_factorize(_TG,TG)),
	    (_TG = (_;_) -> guard_factorize(_TG,TG) ; _TG = TG ),
%	    TG=_TG,
	    guard_and_combine(FG1,FG2,FG)
	  )
	; G = (G1;G2) ->
	  guard_use_factor(G1,F,TG1,FG1),
	  guard_use_factor(G2,F,TG2,FG2),
	  guard_or_combine(TG1,TG2,_TG),
	  (_TG = (_;_) -> guard_factorize(_TG,TG) ; _TG = TG ),
%	  (_TG == TG1, TG=_TG xor _TG==TG2, TG=_TG xor guard_factorize(_TG,TG)),
%	  _TG = TG,
	  guard_or_combine(FG1,FG2,_FG),
%	  (fail, _FG = (_;_) -> guard_factorize(_FG,FG) ; _FG = FG )
%	  (_FG == FG1, FG=_FG xor _FG == FG2, FG=_FG xor guard_factorize(_FG,FG))
	  FG=_FG
	; G = true ->
	  TG=fail,
	  FG=true
	;  G = fail ->
	  TG=fail,
	  FG=fail
	; G == F ->
	  TG=true,
	  FG=fail
	;
	  TG = fail,
	  FG = G
	)
	.

:-std_prolog guard_and_combine/3.

guard_and_combine(G1,G2,G) :-
	( G1=true -> G=G2
	; G2=true -> G=G1
	; G1 = fail -> G=fail
	; G2 = fail -> G=fail
	; G = (G1,G2)
	)
	.

:-std_prolog guard_or_combine/3.

guard_or_combine(G1,G2,G) :-
	( G1=true -> G=true
	; G2=true -> G=true
	; G1 = fail -> G=G2
	; G2 = fail -> G=G1
	; G = (G1;G2)
	)
	.


:-std_prolog guard_linearize/2.

guard_linearize(G,XG) :-
	( G = ((G1,G2),G3) ->
	  guard_linearize(G1,XG1),
	  guard_linearize((G2,G3),XG23),
	  %	  guard_linearize((XG1,XG23),XG)
	  XG=(XG1,XG23)
	; G = ((G1;G2);G3) ->
	  guard_linearize(G1,XG1),
	  guard_linearize((G2;G3),XG23),
	  %%	  guard_linearize((XG1;XG23),XG)
	  XG=(XG1;XG23)
	; G = (G1;G2) ->
	  guard_linearize(G1,XG1),
	  guard_linearize(G2,XG2),
	  XG = (XG1;XG2)
	; G = (G1,G2) ->
	  guard_linearize(G1,XG1),
	  guard_linearize(G2,XG2),
	  XG = (XG1,XG2)
	;
	  XG=G
	)
	.

:-std_prolog guard_push_disjunctions/2.

guard_push_disjunctions(G,XG) :-
	( G = (G1::(_;_),E),domain(E,[(_=_),(\+ \+ _=_),(_ == _)]) ->
	  XG=(E,G1)
	; G = (G1,(E,G2)),domain(E,[(_=_),(\+ \+ _=_),(_ == _)]) ->
	  guard_push_disjunctions((G1,G2),XG12),
	  XG=(E,XG12)
	; G = (_G1,_G2) ->
	  guard_push_disjunctions(_G2,XG2),
	  ( _G2==XG2 ->
	    XG = G
	  ;
	    guard_push_disjunctions((_G1,XG2),XG)
	  )
	; G=XG
	)
	.


	  
