/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 1998, 2003, 2009 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  ma_emit.pl -- Emit Machine Instructions
 *
 * ----------------------------------------------------------------
 * Description
 * 
 * ----------------------------------------------------------------
 */

:-include 'header.pl'.

ma_emit_label(Id)    :-format('~x:\n',[Id]).
ma_emit_jump(A)      :-format('\tjump     ~x\n',[A]).
ma_emit_move(A,B)    :-format('\tmove     ~a, ~x\n',[A,B]).
ma_emit_call_c(F,L)  :-format('\tcall_c   ~x(~A)\n',[F,L]).
ma_emit_cache_or_build(A,F) :-
	format('\tcache_or_build   ~a,~x\n',[A,F]).
ma_emit_jump_ret     :-write('\tjump_ret\n').
ma_emit_fail_ret     :-write('\tfail_ret\n').
ma_emit_true_ret     :-write('\ttrue_ret\n').
ma_emit_fail_c_ret   :-write('\tfail_c_ret\n').
ma_emit_true_c_ret   :-write('\ttrue_c_ret\n').
ma_emit_move_ret(A)  :-format('\tmove_ret ~x\n',[A]).
ma_emit_ret          :-write('\tc_ret\n').
ma_emit_jump_reg(A,Id) :- format('\tjmp_reg ~a,~x\n',[A,Id]).
ma_emit_ret_reg(A) :- format('\tret_reg ~a\n',[A]).

ma_emit_pl_call(F,L) :- ( reg_value(F,_) ->
			    format('\tpl_call  ~a\n',[F])
			;   
			    format('\tpl_call  ~x(~A)\n',[F,L])
			).
ma_emit_pl_jump(F,L) :- ( reg_value(F,_) ->
			    format('\tpl_jump  ~a\n',[F])
			;   
			    format('\tpl_jump  ~x(~A)\n',[F,L])
			).
ma_emit_pl_fail      :-write('\tpl_fail\n').
ma_emit_pl_ret       :-write('\tpl_ret\n').

ma_emit_pl_code(Type,Id) :- format('pl_code ~w ~x\n',[Type,Id]).
ma_emit_code(Type,Id) :- format('c_code ~w ~x\n',[Type,Id]).
ma_emit_ident(Type,Id) :-format('long ~w ~x\n',[Type,Id]).
ma_emit_table(Type,Id,N,L) :-
	( L=[] ->
	    format('long ~w ~x[~w]\n',[Type,Id,N])
	;
	    format('long ~w ~x[~w]=[~X]\n',[Type,Id,N,L])
	)
	.











