;; Compiler: DyALog 1.14.0
;; File "term.pl"


;;------------------ LOADING ------------

c_code local load
	call_c   Dyam_Loading_Set()
	pl_call  fun3(&seed[14],0)
	pl_call  fun3(&seed[13],0)
	pl_call  fun3(&seed[12],0)
	pl_call  fun3(&seed[11],0)
	pl_call  fun3(&seed[10],0)
	pl_call  fun3(&seed[9],0)
	pl_call  fun3(&seed[8],0)
	pl_call  fun3(&seed[7],0)
	pl_call  fun3(&seed[6],0)
	pl_call  fun3(&seed[5],0)
	pl_call  fun3(&seed[4],0)
	pl_call  fun3(&seed[3],0)
	pl_call  fun3(&seed[2],0)
	pl_call  fun3(&seed[1],0)
	pl_call  fun3(&seed[0],0)
	call_c   Dyam_Loading_Reset()
	c_ret

pl_code global pred_label_local_term_2
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(2)
	call_c   Dyam_Choice(fun63)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Reg_Load(2,V(1))
	call_c   Compiler_Label_Elementary_Term(&R(2))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(2),V(1))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code global pred_local_register_4
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(2)
	call_c   Dyam_Reg_Unify(4,&ref[51])
	fail_ret
	call_c   Dyam_Reg_Bind(6,V(6))
	call_c   Dyam_Reg_Load_Ptr(2,V(3))
	fail_ret
	move     I(0), R(4)
	move     0, R(5)
	call_c   DyALog_Mutable_Write(R(2),&R(4),0)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(3))
	fail_ret
	call_c   Dyam_Reg_Load_Ptr(2,V(4))
	fail_ret
	move     I(0), R(4)
	move     0, R(5)
	call_c   DyALog_Mutable_Write(R(2),&R(4),0)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(4))
	fail_ret
	call_c   Dyam_Reg_Load_Ptr(2,V(7))
	fail_ret
	move     I(0), R(4)
	move     0, R(5)
	call_c   DyALog_Mutable_Write(R(2),&R(4),0)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(7))
	fail_ret
	call_c   Dyam_Reg_Load_Ptr(2,V(5))
	fail_ret
	move     I(0), R(4)
	move     0, R(5)
	call_c   DyALog_Mutable_Write(R(2),&R(4),0)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(5))
	fail_ret
	move     &ref[35], R(0)
	move     0, R(1)
	move     &ref[49], R(2)
	move     S(5), R(3)
	move     V(8), R(4)
	move     S(5), R(5)
	pl_call  pred_add_ordered_table_3()
	call_c   Dyam_Reg_Load_Ptr(2,V(7))
	fail_ret
	call_c   Dyam_Reg_Load(4,V(8))
	call_c   DyALog_Mutable_Write(R(2),&R(4),0)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(7))
	fail_ret
	call_c   Dyam_Choice(fun35)
	pl_call  Domain_2(&ref[50],V(6))
	call_c   Dyam_Reg_Load_Ptr(2,V(9))
	fail_ret
	call_c   Dyam_Reg_Load(4,V(8))
	call_c   DyALog_Mutable_Write(R(2),&R(4),0)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(9))
	fail_ret
	pl_fail

pl_code global pred_label_local_alt_tupple_2
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(2)
	call_c   Dyam_Choice(fun32)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),I(0))
	fail_ret
	call_c   Dyam_Cut()
	pl_call  Object_1(&ref[19])
	call_c   Dyam_Deallocate()
	pl_ret

pl_code global pred_label_term_2
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(2)
	call_c   Dyam_Choice(fun29)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Choice(fun25)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_Ground_1(V(1))
	fail_ret
	call_c   Dyam_Cut()
	pl_fail

pl_code global pred_local_register_again_1
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Unify(0,&ref[36])
	fail_ret
	call_c   Dyam_Reg_Load_Ptr(2,V(3))
	fail_ret
	move     I(0), R(4)
	move     0, R(5)
	call_c   DyALog_Mutable_Write(R(2),&R(4),0)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(3))
	fail_ret
	call_c   Dyam_Choice(fun22)
	pl_call  Domain_2(&ref[33],V(7))
	call_c   Dyam_Reg_Load_Ptr(2,V(10))
	fail_ret
	call_c   Dyam_Reg_Load(4,&ref[34])
	call_c   DyALog_Mutable_Read(R(2),&R(4))
	fail_ret
	call_c   Dyam_Reg_Load_Ptr(2,V(13))
	fail_ret
	move     I(0), R(4)
	move     0, R(5)
	call_c   DyALog_Mutable_Read(R(2),&R(4))
	fail_ret
	move     &ref[34], R(0)
	move     S(5), R(1)
	pl_call  pred_local_register_again_1()
	pl_fail

pl_code global pred_label_local_fun_tupple_2
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Unify(0,&ref[32])
	fail_ret
	call_c   Dyam_Reg_Bind(2,V(3))
	call_c   Dyam_Choice(fun17)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(2),N(0))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(3))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_label_local_term_2()

pl_code global pred_register_local_args_4
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(4)
	call_c   Dyam_Choice(fun15)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_lt(V(1),V(3))
	fail_ret
	call_c   Dyam_Cut()
	call_c   DYAM_evpred_is(V(5),&ref[28])
	fail_ret
	call_c   DYAM_evpred_arg(V(5),V(2),V(6))
	fail_ret
	call_c   Dyam_Unify(V(4),&ref[29])
	fail_ret
	call_c   Dyam_Reg_Load(0,V(6))
	call_c   Dyam_Reg_Load(2,V(7))
	pl_call  pred_label_local_term_2()
	call_c   Dyam_Reg_Load(0,V(5))
	call_c   Dyam_Reg_Load(2,V(2))
	call_c   Dyam_Reg_Load(4,V(3))
	call_c   Dyam_Reg_Load(6,V(8))
	call_c   Dyam_Reg_Deallocate(4)
	pl_jump  pred_register_local_args_4()

pl_code global pred_register_local_tupple_4
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(4)
	call_c   Dyam_Choice(fun13)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[24])
	fail_ret
	call_c   DYAM_evpred_is(V(5),&ref[25])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(6))
	call_c   Dyam_Reg_Load(2,V(5))
	call_c   Dyam_Reg_Load(4,V(3))
	call_c   Dyam_Reg_Load(6,V(4))
	call_c   Dyam_Reg_Deallocate(4)
	pl_jump  pred_register_local_tupple_4()

pl_code global pred_globalize_subterms_1
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Choice(fun9)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_functor(V(1),V(2),V(3))
	fail_ret
	call_c   DYAM_evpred_gt(V(3),N(0))
	fail_ret
	pl_call  Object_1(&ref[22])
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun11)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[23])
	call_c   Dyam_Cut()
fun10:
	call_c   Dyam_Choice(fun9)
	call_c   Dyam_Reg_Load(0,V(1))
	move     V(6), R(2)
	move     S(5), R(3)
	move     V(7), R(4)
	move     S(5), R(5)
	pl_call  pred_foreign_arg_3()
	call_c   Dyam_Reg_Load(0,V(7))
	move     V(8), R(2)
	move     S(5), R(3)
	pl_call  pred_label_term_2()
	pl_fail


pl_code global pred_label_local_tupple_2
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Unify(0,&ref[21])
	fail_ret
	call_c   Dyam_Reg_Bind(2,V(2))
	call_c   Dyam_Choice(fun7)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),N(0))
	fail_ret
	call_c   Dyam_Cut()
	pl_call  Object_1(&ref[19])
	call_c   Dyam_Deallocate()
	pl_ret

pl_code global pred_label_term_list_2
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(2)
	call_c   Dyam_Choice(fun4)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[17])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(2),&ref[18])
	fail_ret
	call_c   Dyam_Reg_Load(0,V(3))
	call_c   Dyam_Reg_Load(2,V(5))
	pl_call  pred_label_term_2()
	call_c   Dyam_Reg_Load(0,V(4))
	call_c   Dyam_Reg_Load(2,V(6))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_label_term_list_2()

pl_code global pred_register_local_deref_args_2
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(2)
	call_c   Dyam_Choice(fun4)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[17])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(2),&ref[18])
	fail_ret
	call_c   Dyam_Reg_Load(0,V(3))
	call_c   Dyam_Reg_Load(2,V(5))
	pl_call  pred_label_local_term_2()
	call_c   Dyam_Reg_Load(0,V(4))
	call_c   Dyam_Reg_Load(2,V(6))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_register_local_deref_args_2()

pl_code global pred_term_lock_1
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	pl_call  Object_1(&ref[16])
	call_c   Dyam_Reg_Load_Ptr(2,V(2))
	fail_ret
	call_c   Dyam_Reg_Load(4,V(1))
	call_c   DyALog_Mutable_Read(R(2),&R(4))
	fail_ret
	call_c   Dyam_Reg_Deallocate(3)
	pl_ret


;;------------------ VIEWERS ------------

c_code local build_viewers
	c_ret

c_code local build_seed_0
	ret_reg &seed[0]
	call_c   build_ref_0()
	call_c   build_ref_1()
	call_c   Dyam_Seed_Start(&ref[0],&ref[1],&ref[1],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[0]
	c_ret

pl_code local fun0
	pl_ret

;; TERM 1: globalize_pattern(sa_escape)
c_code local build_ref_1
	ret_reg &ref[1]
	call_c   build_ref_87()
	call_c   build_ref_88()
	call_c   Dyam_Create_Unary(&ref[87],&ref[88])
	move_ret ref[1]
	c_ret

;; TERM 88: sa_escape
c_code local build_ref_88
	ret_reg &ref[88]
	call_c   Dyam_Create_Atom("sa_escape")
	move_ret ref[88]
	c_ret

;; TERM 87: globalize_pattern
c_code local build_ref_87
	ret_reg &ref[87]
	call_c   Dyam_Create_Atom("globalize_pattern")
	move_ret ref[87]
	c_ret

;; TERM 0: '*DATABASE*'
c_code local build_ref_0
	ret_reg &ref[0]
	call_c   Dyam_Create_Atom("*DATABASE*")
	move_ret ref[0]
	c_ret

c_code local build_seed_1
	ret_reg &seed[1]
	call_c   build_ref_0()
	call_c   build_ref_2()
	call_c   Dyam_Seed_Start(&ref[0],&ref[2],&ref[2],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[1]
	c_ret

;; TERM 2: globalize_pattern(sa_env)
c_code local build_ref_2
	ret_reg &ref[2]
	call_c   build_ref_87()
	call_c   build_ref_89()
	call_c   Dyam_Create_Unary(&ref[87],&ref[89])
	move_ret ref[2]
	c_ret

;; TERM 89: sa_env
c_code local build_ref_89
	ret_reg &ref[89]
	call_c   Dyam_Create_Atom("sa_env")
	move_ret ref[89]
	c_ret

c_code local build_seed_2
	ret_reg &seed[2]
	call_c   build_ref_0()
	call_c   build_ref_3()
	call_c   Dyam_Seed_Start(&ref[0],&ref[3],&ref[3],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[2]
	c_ret

;; TERM 3: globalize_pattern(*)
c_code local build_ref_3
	ret_reg &ref[3]
	call_c   build_ref_87()
	call_c   build_ref_90()
	call_c   Dyam_Create_Unary(&ref[87],&ref[90])
	move_ret ref[3]
	c_ret

;; TERM 90: *
c_code local build_ref_90
	ret_reg &ref[90]
	call_c   Dyam_Create_Atom("*")
	move_ret ref[90]
	c_ret

c_code local build_seed_3
	ret_reg &seed[3]
	call_c   build_ref_0()
	call_c   build_ref_4()
	call_c   Dyam_Seed_Start(&ref[0],&ref[4],&ref[4],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[3]
	c_ret

;; TERM 4: globalize_pattern('$CLOSURE')
c_code local build_ref_4
	ret_reg &ref[4]
	call_c   build_ref_87()
	call_c   build_ref_79()
	call_c   Dyam_Create_Unary(&ref[87],&ref[79])
	move_ret ref[4]
	c_ret

;; TERM 79: '$CLOSURE'
c_code local build_ref_79
	ret_reg &ref[79]
	call_c   Dyam_Create_Atom("$CLOSURE")
	move_ret ref[79]
	c_ret

c_code local build_seed_4
	ret_reg &seed[4]
	call_c   build_ref_0()
	call_c   build_ref_5()
	call_c   Dyam_Seed_Start(&ref[0],&ref[5],&ref[5],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[4]
	c_ret

;; TERM 5: globalize_pattern('*RFI*')
c_code local build_ref_5
	ret_reg &ref[5]
	call_c   build_ref_87()
	call_c   build_ref_91()
	call_c   Dyam_Create_Unary(&ref[87],&ref[91])
	move_ret ref[5]
	c_ret

;; TERM 91: '*RFI*'
c_code local build_ref_91
	ret_reg &ref[91]
	call_c   Dyam_Create_Atom("*RFI*")
	move_ret ref[91]
	c_ret

c_code local build_seed_5
	ret_reg &seed[5]
	call_c   build_ref_0()
	call_c   build_ref_6()
	call_c   Dyam_Seed_Start(&ref[0],&ref[6],&ref[6],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[5]
	c_ret

;; TERM 6: globalize_pattern('*CFI*')
c_code local build_ref_6
	ret_reg &ref[6]
	call_c   build_ref_87()
	call_c   build_ref_92()
	call_c   Dyam_Create_Unary(&ref[87],&ref[92])
	move_ret ref[6]
	c_ret

;; TERM 92: '*CFI*'
c_code local build_ref_92
	ret_reg &ref[92]
	call_c   Dyam_Create_Atom("*CFI*")
	move_ret ref[92]
	c_ret

c_code local build_seed_6
	ret_reg &seed[6]
	call_c   build_ref_0()
	call_c   build_ref_7()
	call_c   Dyam_Seed_Start(&ref[0],&ref[7],&ref[7],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[6]
	c_ret

;; TERM 7: globalize_pattern('*RAI*')
c_code local build_ref_7
	ret_reg &ref[7]
	call_c   build_ref_87()
	call_c   build_ref_93()
	call_c   Dyam_Create_Unary(&ref[87],&ref[93])
	move_ret ref[7]
	c_ret

;; TERM 93: '*RAI*'
c_code local build_ref_93
	ret_reg &ref[93]
	call_c   Dyam_Create_Atom("*RAI*")
	move_ret ref[93]
	c_ret

c_code local build_seed_7
	ret_reg &seed[7]
	call_c   build_ref_0()
	call_c   build_ref_8()
	call_c   Dyam_Seed_Start(&ref[0],&ref[8],&ref[8],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[7]
	c_ret

;; TERM 8: globalize_pattern('*CAI*')
c_code local build_ref_8
	ret_reg &ref[8]
	call_c   build_ref_87()
	call_c   build_ref_94()
	call_c   Dyam_Create_Unary(&ref[87],&ref[94])
	move_ret ref[8]
	c_ret

;; TERM 94: '*CAI*'
c_code local build_ref_94
	ret_reg &ref[94]
	call_c   Dyam_Create_Atom("*CAI*")
	move_ret ref[94]
	c_ret

c_code local build_seed_8
	ret_reg &seed[8]
	call_c   build_ref_0()
	call_c   build_ref_9()
	call_c   Dyam_Seed_Start(&ref[0],&ref[9],&ref[9],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[8]
	c_ret

;; TERM 9: globalize_pattern('*GUARD*')
c_code local build_ref_9
	ret_reg &ref[9]
	call_c   build_ref_87()
	call_c   build_ref_95()
	call_c   Dyam_Create_Unary(&ref[87],&ref[95])
	move_ret ref[9]
	c_ret

;; TERM 95: '*GUARD*'
c_code local build_ref_95
	ret_reg &ref[95]
	call_c   Dyam_Create_Atom("*GUARD*")
	move_ret ref[95]
	c_ret

c_code local build_seed_9
	ret_reg &seed[9]
	call_c   build_ref_0()
	call_c   build_ref_10()
	call_c   Dyam_Seed_Start(&ref[0],&ref[10],&ref[10],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[9]
	c_ret

;; TERM 10: globalize_pattern('*PROLOG-FIRST*')
c_code local build_ref_10
	ret_reg &ref[10]
	call_c   build_ref_87()
	call_c   build_ref_96()
	call_c   Dyam_Create_Unary(&ref[87],&ref[96])
	move_ret ref[10]
	c_ret

;; TERM 96: '*PROLOG-FIRST*'
c_code local build_ref_96
	ret_reg &ref[96]
	call_c   Dyam_Create_Atom("*PROLOG-FIRST*")
	move_ret ref[96]
	c_ret

c_code local build_seed_10
	ret_reg &seed[10]
	call_c   build_ref_0()
	call_c   build_ref_11()
	call_c   Dyam_Seed_Start(&ref[0],&ref[11],&ref[11],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[10]
	c_ret

;; TERM 11: globalize_pattern('*PROLOG-ITEM*')
c_code local build_ref_11
	ret_reg &ref[11]
	call_c   build_ref_87()
	call_c   build_ref_97()
	call_c   Dyam_Create_Unary(&ref[87],&ref[97])
	move_ret ref[11]
	c_ret

;; TERM 97: '*PROLOG-ITEM*'
c_code local build_ref_97
	ret_reg &ref[97]
	call_c   Dyam_Create_Atom("*PROLOG-ITEM*")
	move_ret ref[97]
	c_ret

c_code local build_seed_11
	ret_reg &seed[11]
	call_c   build_ref_0()
	call_c   build_ref_12()
	call_c   Dyam_Seed_Start(&ref[0],&ref[12],&ref[12],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[11]
	c_ret

;; TERM 12: globalize_pattern('*FIRST*')
c_code local build_ref_12
	ret_reg &ref[12]
	call_c   build_ref_87()
	call_c   build_ref_98()
	call_c   Dyam_Create_Unary(&ref[87],&ref[98])
	move_ret ref[12]
	c_ret

;; TERM 98: '*FIRST*'
c_code local build_ref_98
	ret_reg &ref[98]
	call_c   Dyam_Create_Atom("*FIRST*")
	move_ret ref[98]
	c_ret

c_code local build_seed_12
	ret_reg &seed[12]
	call_c   build_ref_0()
	call_c   build_ref_13()
	call_c   Dyam_Seed_Start(&ref[0],&ref[13],&ref[13],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[12]
	c_ret

;; TERM 13: globalize_pattern('*RITEM*')
c_code local build_ref_13
	ret_reg &ref[13]
	call_c   build_ref_87()
	call_c   build_ref_99()
	call_c   Dyam_Create_Unary(&ref[87],&ref[99])
	move_ret ref[13]
	c_ret

;; TERM 99: '*RITEM*'
c_code local build_ref_99
	ret_reg &ref[99]
	call_c   Dyam_Create_Atom("*RITEM*")
	move_ret ref[99]
	c_ret

c_code local build_seed_13
	ret_reg &seed[13]
	call_c   build_ref_0()
	call_c   build_ref_14()
	call_c   Dyam_Seed_Start(&ref[0],&ref[14],&ref[14],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[13]
	c_ret

;; TERM 14: globalize_pattern('*CITEM*')
c_code local build_ref_14
	ret_reg &ref[14]
	call_c   build_ref_87()
	call_c   build_ref_100()
	call_c   Dyam_Create_Unary(&ref[87],&ref[100])
	move_ret ref[14]
	c_ret

;; TERM 100: '*CITEM*'
c_code local build_ref_100
	ret_reg &ref[100]
	call_c   Dyam_Create_Atom("*CITEM*")
	move_ret ref[100]
	c_ret

c_code local build_seed_14
	ret_reg &seed[14]
	call_c   build_ref_0()
	call_c   build_ref_15()
	call_c   Dyam_Seed_Start(&ref[0],&ref[15],&ref[15],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[14]
	c_ret

;; TERM 15: globalize_pattern(:>)
c_code local build_ref_15
	ret_reg &ref[15]
	call_c   build_ref_87()
	call_c   Dyam_Create_Unary(&ref[87],I(9))
	move_ret ref[15]
	c_ret

;; TERM 53: '$internal'(0)
c_code local build_ref_53
	ret_reg &ref[53]
	call_c   build_ref_101()
	call_c   Dyam_Create_Unary(&ref[101],N(0))
	move_ret ref[53]
	c_ret

;; TERM 101: '$internal'
c_code local build_ref_101
	ret_reg &ref[101]
	call_c   Dyam_Create_Atom("$internal")
	move_ret ref[101]
	c_ret

;; TERM 52: '$TUPPLE'(_D)
c_code local build_ref_52
	ret_reg &ref[52]
	call_c   Dyam_Create_Unary(I(10),V(3))
	move_ret ref[52]
	c_ret

;; TERM 54: lterm(_B, _C, _E, _F, _G)
c_code local build_ref_54
	ret_reg &ref[54]
	call_c   build_ref_35()
	call_c   Dyam_Term_Start(&ref[35],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[54]
	c_ret

;; TERM 35: lterm
c_code local build_ref_35
	ret_reg &ref[35]
	call_c   Dyam_Create_Atom("lterm")
	move_ret ref[35]
	c_ret

;; TERM 56: [_I,_J,_K,_L]
c_code local build_ref_56
	ret_reg &ref[56]
	call_c   Dyam_Create_Tupple(8,11,I(0))
	move_ret ref[56]
	c_ret

;; TERM 55: '$LOOP'
c_code local build_ref_55
	ret_reg &ref[55]
	call_c   Dyam_Create_Atom("$LOOP")
	move_ret ref[55]
	c_ret

;; TERM 59: _K43159023
c_code local build_ref_59
	ret_reg &ref[59]
	call_c   Dyam_Create_Unary(I(6),V(3))
	move_ret ref[59]
	c_ret

;; TERM 60: global_term(_B, _N)
c_code local build_ref_60
	ret_reg &ref[60]
	call_c   build_ref_102()
	call_c   Dyam_Create_Binary(&ref[102],V(1),V(13))
	move_ret ref[60]
	c_ret

;; TERM 102: global_term
c_code local build_ref_102
	ret_reg &ref[102]
	call_c   Dyam_Create_Atom("global_term")
	move_ret ref[102]
	c_ret

;; TERM 64: [_V]
c_code local build_ref_64
	ret_reg &ref[64]
	call_c   Dyam_Create_List(V(21),I(0))
	move_ret ref[64]
	c_ret

;; TERM 63: features(_P, _U)
c_code local build_ref_63
	ret_reg &ref[63]
	call_c   build_ref_103()
	call_c   Dyam_Create_Binary(&ref[103],V(15),V(20))
	move_ret ref[63]
	c_ret

;; TERM 103: features
c_code local build_ref_103
	ret_reg &ref[103]
	call_c   Dyam_Create_Atom("features")
	move_ret ref[103]
	c_ret

;; TERM 62: [_S|_T]
c_code local build_ref_62
	ret_reg &ref[62]
	call_c   Dyam_Create_List(V(18),V(19))
	move_ret ref[62]
	c_ret

;; TERM 61: '$smb'(_B)
c_code local build_ref_61
	ret_reg &ref[61]
	call_c   build_ref_104()
	call_c   Dyam_Create_Unary(&ref[104],V(1))
	move_ret ref[61]
	c_ret

;; TERM 104: '$smb'
c_code local build_ref_104
	ret_reg &ref[104]
	call_c   Dyam_Create_Atom("$smb")
	move_ret ref[104]
	c_ret

;; TERM 65: '$charlist'(_W)
c_code local build_ref_65
	ret_reg &ref[65]
	call_c   build_ref_105()
	call_c   Dyam_Create_Unary(&ref[105],V(22))
	move_ret ref[65]
	c_ret

;; TERM 105: '$charlist'
c_code local build_ref_105
	ret_reg &ref[105]
	call_c   Dyam_Create_Atom("$charlist")
	move_ret ref[105]
	c_ret

;; TERM 69: '$tupple'(_J1, _M1)
c_code local build_ref_69
	ret_reg &ref[69]
	call_c   build_ref_106()
	call_c   Dyam_Create_Binary(&ref[106],V(35),V(38))
	move_ret ref[69]
	c_ret

;; TERM 106: '$tupple'
c_code local build_ref_106
	ret_reg &ref[106]
	call_c   Dyam_Create_Atom("$tupple")
	move_ret ref[106]
	c_ret

;; TERM 68: _J1 + 1
c_code local build_ref_68
	ret_reg &ref[68]
	call_c   build_ref_107()
	call_c   Dyam_Create_Binary(&ref[107],V(35),N(1))
	move_ret ref[68]
	c_ret

;; TERM 107: +
c_code local build_ref_107
	ret_reg &ref[107]
	call_c   Dyam_Create_Atom("+")
	move_ret ref[107]
	c_ret

;; TERM 67: [_K43166383,_U43166382|_L1]
c_code local build_ref_67
	ret_reg &ref[67]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   Dyam_Create_Unary(I(6),V(35))
	move_ret R(0)
	call_c   Dyam_Create_Unary(I(6),V(36))
	move_ret R(1)
	call_c   Dyam_Create_List(R(1),V(37))
	move_ret R(1)
	call_c   Dyam_Create_List(R(0),R(1))
	move_ret ref[67]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 72: [_O1,_P1]
c_code local build_ref_72
	ret_reg &ref[72]
	call_c   Dyam_Create_Tupple(40,41,I(0))
	move_ret ref[72]
	c_ret

;; TERM 71: '$list'
c_code local build_ref_71
	ret_reg &ref[71]
	call_c   Dyam_Create_Atom("$list")
	move_ret ref[71]
	c_ret

;; TERM 70: [_N1|_L1]
c_code local build_ref_70
	ret_reg &ref[70]
	call_c   Dyam_Create_List(V(39),V(37))
	move_ret ref[70]
	c_ret

;; TERM 66: [_X|_Y]
c_code local build_ref_66
	ret_reg &ref[66]
	call_c   Dyam_Create_List(V(23),V(24))
	move_ret ref[66]
	c_ret

;; TERM 58: in_term_loop(_D)
c_code local build_ref_58
	ret_reg &ref[58]
	call_c   build_ref_108()
	call_c   Dyam_Create_Unary(&ref[108],V(3))
	move_ret ref[58]
	c_ret

;; TERM 108: in_term_loop
c_code local build_ref_108
	ret_reg &ref[108]
	call_c   Dyam_Create_Atom("in_term_loop")
	move_ret ref[108]
	c_ret

;; TERM 77: ['$LOOP',_Z,0]
c_code local build_ref_77
	ret_reg &ref[77]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(N(0),I(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(25),R(0))
	move_ret R(0)
	call_c   build_ref_55()
	call_c   Dyam_Create_List(&ref[55],R(0))
	move_ret ref[77]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 76: '$term'(3)
c_code local build_ref_76
	ret_reg &ref[76]
	call_c   build_ref_109()
	call_c   Dyam_Create_Unary(&ref[109],N(3))
	move_ret ref[76]
	c_ret

;; TERM 109: '$term'
c_code local build_ref_109
	ret_reg &ref[109]
	call_c   Dyam_Create_Atom("$term")
	move_ret ref[109]
	c_ret

;; TERM 73: '$VAR'(_K43159023, ['$LOOP',_Z])
c_code local build_ref_73
	ret_reg &ref[73]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(25),I(0))
	move_ret R(0)
	call_c   build_ref_55()
	call_c   Dyam_Create_List(&ref[55],R(0))
	move_ret R(0)
	call_c   build_ref_59()
	call_c   Dyam_Create_Binary(I(6),&ref[59],R(0))
	move_ret ref[73]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 75: [_A1,_K43159023|_B1]
c_code local build_ref_75
	ret_reg &ref[75]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_59()
	call_c   Dyam_Create_List(&ref[59],V(27))
	move_ret R(0)
	call_c   Dyam_Create_List(V(26),R(0))
	move_ret ref[75]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 74: [_A1|_B1]
c_code local build_ref_74
	ret_reg &ref[74]
	call_c   Dyam_Create_List(V(26),V(27))
	move_ret ref[74]
	c_ret

;; TERM 57: '$VAR'(_K43159023, _M)
c_code local build_ref_57
	ret_reg &ref[57]
	call_c   build_ref_59()
	call_c   Dyam_Create_Binary(I(6),&ref[59],V(12))
	move_ret ref[57]
	c_ret

;; TERM 83: '$CLOSURE'(_D1)
c_code local build_ref_83
	ret_reg &ref[83]
	call_c   build_ref_79()
	call_c   Dyam_Create_Unary(&ref[79],V(29))
	move_ret ref[83]
	c_ret

;; TERM 82: [_H1]
c_code local build_ref_82
	ret_reg &ref[82]
	call_c   Dyam_Create_List(V(33),I(0))
	move_ret ref[82]
	c_ret

;; TERM 81: '$fun'(_D, _F1, _G1)
c_code local build_ref_81
	ret_reg &ref[81]
	call_c   build_ref_110()
	call_c   Dyam_Term_Start(&ref[110],3)
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(31))
	call_c   Dyam_Term_Arg(V(32))
	call_c   Dyam_Term_End()
	move_ret ref[81]
	c_ret

;; TERM 110: '$fun'
c_code local build_ref_110
	ret_reg &ref[110]
	call_c   Dyam_Create_Atom("$fun")
	move_ret ref[110]
	c_ret

;; TERM 80: '$CLOSURE'(_D1, _E1)
c_code local build_ref_80
	ret_reg &ref[80]
	call_c   build_ref_79()
	call_c   Dyam_Create_Binary(&ref[79],V(29),V(30))
	move_ret ref[80]
	c_ret

;; TERM 85: '$unary'
c_code local build_ref_85
	ret_reg &ref[85]
	call_c   Dyam_Create_Atom("$unary")
	move_ret ref[85]
	c_ret

;; TERM 86: '$binary'
c_code local build_ref_86
	ret_reg &ref[86]
	call_c   Dyam_Create_Atom("$binary")
	move_ret ref[86]
	c_ret

;; TERM 78: '$term'(_C1)
c_code local build_ref_78
	ret_reg &ref[78]
	call_c   build_ref_109()
	call_c   Dyam_Create_Unary(&ref[109],V(28))
	move_ret ref[78]
	c_ret

;; TERM 84: [_D1|_B1]
c_code local build_ref_84
	ret_reg &ref[84]
	call_c   Dyam_Create_List(V(29),V(27))
	move_ret ref[84]
	c_ret

;; TERM 50: '$lterm'(_J, _K, _L)
c_code local build_ref_50
	ret_reg &ref[50]
	call_c   build_ref_111()
	call_c   Dyam_Term_Start(&ref[111],3)
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_End()
	move_ret ref[50]
	c_ret

;; TERM 111: '$lterm'
c_code local build_ref_111
	ret_reg &ref[111]
	call_c   Dyam_Create_Atom("$lterm")
	move_ret ref[111]
	c_ret

;; TERM 49: lterm(_B, '$lterm'(_D, _E, _F), _H, _C, _G)
c_code local build_ref_49
	ret_reg &ref[49]
	call_c   build_ref_35()
	call_c   build_ref_51()
	call_c   Dyam_Term_Start(&ref[35],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(&ref[51])
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[49]
	c_ret

;; TERM 51: '$lterm'(_D, _E, _F)
c_code local build_ref_51
	ret_reg &ref[51]
	call_c   build_ref_111()
	call_c   Dyam_Term_Start(&ref[111],3)
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[51]
	c_ret

;; TERM 44: lterm('$$TUPPLE'(_B), _C, _D, _E, _F)
c_code local build_ref_44
	ret_reg &ref[44]
	call_c   build_ref_35()
	call_c   build_ref_48()
	call_c   Dyam_Term_Start(&ref[35],5)
	call_c   Dyam_Term_Arg(&ref[48])
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[44]
	c_ret

;; TERM 48: '$$TUPPLE'(_B)
c_code local build_ref_48
	ret_reg &ref[48]
	call_c   build_ref_112()
	call_c   Dyam_Create_Unary(&ref[112],V(1))
	move_ret ref[48]
	c_ret

;; TERM 112: '$$TUPPLE'
c_code local build_ref_112
	ret_reg &ref[112]
	call_c   Dyam_Create_Atom("$$TUPPLE")
	move_ret ref[112]
	c_ret

;; TERM 47: [_K]
c_code local build_ref_47
	ret_reg &ref[47]
	call_c   Dyam_Create_List(V(10),I(0))
	move_ret ref[47]
	c_ret

;; TERM 46: '$alt_tupple'(_H, _I)
c_code local build_ref_46
	ret_reg &ref[46]
	call_c   build_ref_113()
	call_c   Dyam_Create_Binary(&ref[113],V(7),V(8))
	move_ret ref[46]
	c_ret

;; TERM 113: '$alt_tupple'
c_code local build_ref_113
	ret_reg &ref[113]
	call_c   Dyam_Create_Atom("$alt_tupple")
	move_ret ref[113]
	c_ret

;; TERM 45: [_H,_I|_J]
c_code local build_ref_45
	ret_reg &ref[45]
	call_c   Dyam_Create_Tupple(7,8,V(9))
	move_ret ref[45]
	c_ret

;; TERM 39: [_B]
c_code local build_ref_39
	ret_reg &ref[39]
	call_c   Dyam_Create_List(V(1),I(0))
	move_ret ref[39]
	c_ret

;; TERM 38: 'Term Labeling Var ~w'
c_code local build_ref_38
	ret_reg &ref[38]
	call_c   Dyam_Create_Atom("Term Labeling Var ~w")
	move_ret ref[38]
	c_ret

;; TERM 40: internal_atomic(_B, _C)
c_code local build_ref_40
	ret_reg &ref[40]
	call_c   build_ref_114()
	call_c   Dyam_Create_Binary(&ref[114],V(1),V(2))
	move_ret ref[40]
	c_ret

;; TERM 114: internal_atomic
c_code local build_ref_114
	ret_reg &ref[114]
	call_c   Dyam_Create_Atom("internal_atomic")
	move_ret ref[114]
	c_ret

;; TERM 41: global_term(_B, _C)
c_code local build_ref_41
	ret_reg &ref[41]
	call_c   build_ref_102()
	call_c   Dyam_Create_Binary(&ref[102],V(1),V(2))
	move_ret ref[41]
	c_ret

;; TERM 43: term
c_code local build_ref_43
	ret_reg &ref[43]
	call_c   Dyam_Create_Atom("term")
	move_ret ref[43]
	c_ret

;; TERM 42: '$term'(_D, _E)
c_code local build_ref_42
	ret_reg &ref[42]
	call_c   build_ref_109()
	call_c   Dyam_Create_Binary(&ref[109],V(3),V(4))
	move_ret ref[42]
	c_ret

;; TERM 34: lterm(_L, _M, _N, _O, _P)
c_code local build_ref_34
	ret_reg &ref[34]
	call_c   build_ref_35()
	call_c   Dyam_Term_Start(&ref[35],5)
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_End()
	move_ret ref[34]
	c_ret

;; TERM 33: '$lterm'(_I, _J, _K)
c_code local build_ref_33
	ret_reg &ref[33]
	call_c   build_ref_111()
	call_c   Dyam_Term_Start(&ref[111],3)
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_End()
	move_ret ref[33]
	c_ret

;; TERM 37: '$lterm'(_R, _S, _T)
c_code local build_ref_37
	ret_reg &ref[37]
	call_c   build_ref_111()
	call_c   Dyam_Term_Start(&ref[111],3)
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_End()
	move_ret ref[37]
	c_ret

;; TERM 36: lterm(_B, '$lterm'(_C, _D, _E), _F, _G, _H)
c_code local build_ref_36
	ret_reg &ref[36]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_111()
	call_c   Dyam_Term_Start(&ref[111],3)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_35()
	call_c   Dyam_Term_Start(&ref[35],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[36]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 31: '$$FUNTUPPLE'(_B, _F)
c_code local build_ref_31
	ret_reg &ref[31]
	call_c   build_ref_115()
	call_c   Dyam_Create_Binary(&ref[115],V(1),V(5))
	move_ret ref[31]
	c_ret

;; TERM 115: '$$FUNTUPPLE'
c_code local build_ref_115
	ret_reg &ref[115]
	call_c   Dyam_Create_Atom("$$FUNTUPPLE")
	move_ret ref[115]
	c_ret

;; TERM 30: '$fun_tupple'(_F)
c_code local build_ref_30
	ret_reg &ref[30]
	call_c   build_ref_116()
	call_c   Dyam_Create_Unary(&ref[116],V(5))
	move_ret ref[30]
	c_ret

;; TERM 116: '$fun_tupple'
c_code local build_ref_116
	ret_reg &ref[116]
	call_c   Dyam_Create_Atom("$fun_tupple")
	move_ret ref[116]
	c_ret

;; TERM 32: '$FUNTUPPLE'(_B, '$TUPPLE'(_C))
c_code local build_ref_32
	ret_reg &ref[32]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Unary(I(10),V(2))
	move_ret R(0)
	call_c   build_ref_117()
	call_c   Dyam_Create_Binary(&ref[117],V(1),R(0))
	move_ret ref[32]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 117: '$FUNTUPPLE'
c_code local build_ref_117
	ret_reg &ref[117]
	call_c   Dyam_Create_Atom("$FUNTUPPLE")
	move_ret ref[117]
	c_ret

;; TERM 29: [_H|_I]
c_code local build_ref_29
	ret_reg &ref[29]
	call_c   Dyam_Create_List(V(7),V(8))
	move_ret ref[29]
	c_ret

;; TERM 28: _B + 1
c_code local build_ref_28
	ret_reg &ref[28]
	call_c   build_ref_107()
	call_c   Dyam_Create_Binary(&ref[107],V(1),N(1))
	move_ret ref[28]
	c_ret

;; TERM 25: _C + 1
c_code local build_ref_25
	ret_reg &ref[25]
	call_c   build_ref_107()
	call_c   Dyam_Create_Binary(&ref[107],V(2),N(1))
	move_ret ref[25]
	c_ret

;; TERM 24: [_E43159022|_G]
c_code local build_ref_24
	ret_reg &ref[24]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Unary(I(6),V(5))
	move_ret R(0)
	call_c   Dyam_Create_List(R(0),V(6))
	move_ret ref[24]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 27: '$tupple'(_I, _C)
c_code local build_ref_27
	ret_reg &ref[27]
	call_c   build_ref_106()
	call_c   Dyam_Create_Binary(&ref[106],V(8),V(2))
	move_ret ref[27]
	c_ret

;; TERM 26: [_H]
c_code local build_ref_26
	ret_reg &ref[26]
	call_c   Dyam_Create_List(V(7),I(0))
	move_ret ref[26]
	c_ret

;; TERM 23: global_term(_C, _E)
c_code local build_ref_23
	ret_reg &ref[23]
	call_c   build_ref_102()
	call_c   Dyam_Create_Binary(&ref[102],V(2),V(4))
	move_ret ref[23]
	c_ret

;; TERM 22: globalize_pattern(_C)
c_code local build_ref_22
	ret_reg &ref[22]
	call_c   build_ref_87()
	call_c   Dyam_Create_Unary(&ref[87],V(2))
	move_ret ref[22]
	c_ret

;; TERM 19: internal_atomic([], _C)
c_code local build_ref_19
	ret_reg &ref[19]
	call_c   build_ref_114()
	call_c   Dyam_Create_Binary(&ref[114],I(0),V(2))
	move_ret ref[19]
	c_ret

;; TERM 20: '$xtupple'(_D)
c_code local build_ref_20
	ret_reg &ref[20]
	call_c   build_ref_118()
	call_c   Dyam_Create_Unary(&ref[118],V(3))
	move_ret ref[20]
	c_ret

;; TERM 118: '$xtupple'
c_code local build_ref_118
	ret_reg &ref[118]
	call_c   Dyam_Create_Atom("$xtupple")
	move_ret ref[118]
	c_ret

;; TERM 21: '$TUPPLE'(_B)
c_code local build_ref_21
	ret_reg &ref[21]
	call_c   Dyam_Create_Unary(I(10),V(1))
	move_ret ref[21]
	c_ret

;; TERM 18: [_F|_G]
c_code local build_ref_18
	ret_reg &ref[18]
	call_c   Dyam_Create_List(V(5),V(6))
	move_ret ref[18]
	c_ret

;; TERM 17: [_D|_E]
c_code local build_ref_17
	ret_reg &ref[17]
	call_c   Dyam_Create_List(V(3),V(4))
	move_ret ref[17]
	c_ret

;; TERM 16: term_handling(_C)
c_code local build_ref_16
	ret_reg &ref[16]
	call_c   build_ref_119()
	call_c   Dyam_Create_Unary(&ref[119],V(2))
	move_ret ref[16]
	c_ret

;; TERM 119: term_handling
c_code local build_ref_119
	ret_reg &ref[119]
	call_c   Dyam_Create_Atom("term_handling")
	move_ret ref[119]
	c_ret

pl_code local fun2
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Object(R(0))
	call_c   Dyam_Tabule()
	pl_ret

pl_code local fun3
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Choice(fun2)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Subsume(R(0))
	fail_ret
	call_c   Dyam_Cut()
	pl_ret

pl_code local fun38
	call_c   Dyam_Remove_Choice()
fun37:
	call_c   Dyam_Cut()
	call_c   Dyam_Deallocate()
	pl_ret


pl_code local fun40
	call_c   Dyam_Remove_Choice()
fun39:
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(2))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_label_term_2()


pl_code local fun42
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(19),I(0))
	fail_ret
fun41:
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(14))
	call_c   Dyam_Reg_Load(4,V(2))
	call_c   Dyam_Reg_Load(6,V(17))
	call_c   Dyam_Reg_Deallocate(4)
	pl_jump  pred_local_register_4()


pl_code local fun43
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Reg_Load(0,V(15))
	move     V(18), R(2)
	move     S(5), R(3)
	pl_call  pred_label_local_term_2()
	call_c   Dyam_Unify(V(17),&ref[62])
	fail_ret
	call_c   Dyam_Choice(fun42)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[63])
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(20))
	move     V(21), R(2)
	move     S(5), R(3)
	pl_call  pred_label_local_term_2()
	call_c   Dyam_Unify(V(19),&ref[64])
	fail_ret
	pl_jump  fun41()

pl_code local fun44
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(1),&ref[70])
	fail_ret
	call_c   Dyam_Unify(V(14),&ref[71])
	fail_ret
	call_c   Dyam_Unify(V(17),&ref[72])
	fail_ret
	call_c   Dyam_Reg_Load(0,V(39))
	call_c   Dyam_Reg_Load(2,V(40))
	pl_call  pred_label_local_term_2()
	call_c   Dyam_Reg_Load(0,V(37))
	call_c   Dyam_Reg_Load(2,V(41))
	pl_call  pred_label_local_term_2()
	pl_jump  fun41()

pl_code local fun46
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(14),&ref[78])
	fail_ret
fun45:
	call_c   Dyam_Reg_Load(0,V(34))
	call_c   Dyam_Reg_Load(2,V(29))
	pl_call  pred_label_local_term_2()
	move     N(0), R(0)
	move     0, R(1)
	call_c   Dyam_Reg_Load(2,V(1))
	call_c   Dyam_Reg_Load(4,V(28))
	call_c   Dyam_Reg_Load(6,V(27))
	pl_call  pred_register_local_args_4()
	pl_jump  fun41()


pl_code local fun47
	call_c   Dyam_Update_Choice(fun46)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(28),N(2))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(14),&ref[86])
	fail_ret
	pl_jump  fun45()

pl_code local fun48
	call_c   Dyam_Remove_Choice()
	call_c   DYAM_evpred_functor(V(1),V(34),V(28))
	fail_ret
	call_c   Dyam_Unify(V(17),&ref[84])
	fail_ret
	call_c   Dyam_Choice(fun47)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(28),N(1))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(14),&ref[85])
	fail_ret
	pl_jump  fun45()

pl_code local fun49
	call_c   Dyam_Update_Choice(fun48)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[80])
	fail_ret
	call_c   Dyam_Unify(V(29),&ref[81])
	fail_ret
	call_c   DYAM_evpred_number(V(3))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(30))
	move     V(33), R(2)
	move     S(5), R(3)
	pl_call  pred_label_local_term_2()
	call_c   Dyam_Unify(V(17),&ref[82])
	fail_ret
	call_c   Dyam_Unify(V(14),&ref[83])
	fail_ret
	pl_jump  fun41()

pl_code local fun50
	call_c   Dyam_Update_Choice(fun49)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[57])
	fail_ret
	call_c   DYAM_evpred_number(V(3))
	fail_ret
	call_c   Dyam_Cut()
	call_c   DYAM_evpred_length(V(12),V(28))
	fail_ret
	call_c   Dyam_Unify(V(19),&ref[74])
	fail_ret
	call_c   Dyam_Unify(V(17),&ref[75])
	fail_ret
	call_c   Dyam_Unify(V(14),&ref[78])
	fail_ret
	call_c   Dyam_Reg_Load(0,V(12))
	call_c   Dyam_Reg_Load(2,V(19))
	pl_call  pred_register_local_deref_args_2()
	pl_jump  fun41()

pl_code local fun51
	call_c   Dyam_Update_Choice(fun50)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[73])
	fail_ret
	call_c   DYAM_evpred_number(V(3))
	fail_ret
	call_c   Dyam_Cut()
	call_c   DYAM_evpred_assert_1(&ref[58])
	call_c   Dyam_Unify(V(19),&ref[74])
	fail_ret
	call_c   Dyam_Unify(V(17),&ref[75])
	fail_ret
	call_c   Dyam_Unify(V(14),&ref[76])
	fail_ret
	move     &ref[77], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(19))
	pl_call  pred_register_local_deref_args_2()
	call_c   DYAM_evpred_retract(&ref[58])
	fail_ret
	pl_jump  fun41()

pl_code local fun52
	call_c   Dyam_Update_Choice(fun51)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[66])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun44)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[67])
	fail_ret
	call_c   DYAM_evpred_is(V(36),&ref[68])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(14),&ref[69])
	fail_ret
	call_c   Dyam_Reg_Load(0,V(37))
	call_c   Dyam_Reg_Load(2,V(36))
	call_c   Dyam_Reg_Load(4,V(14))
	move     V(17), R(6)
	move     S(5), R(7)
	pl_call  pred_register_local_tupple_4()
	pl_jump  fun41()

pl_code local fun53
	call_c   Dyam_Update_Choice(fun52)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_atom_to_chars(V(22),V(1))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(14),&ref[65])
	fail_ret
	call_c   Dyam_Unify(V(17),I(0))
	fail_ret
	pl_jump  fun41()

pl_code local fun54
	call_c   Dyam_Update_Choice(fun53)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_atomic(V(1))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(14),&ref[61])
	fail_ret
	call_c   DYAM_evpred_atom_to_module(V(1),V(15),V(16))
	fail_ret
	call_c   Dyam_Choice(fun43)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(15),I(0))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(17),I(0))
	fail_ret
	pl_jump  fun41()

pl_code local fun55
	call_c   Dyam_Update_Choice(fun54)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_atomic(V(1))
	fail_ret
	call_c   Dyam_Choice(fun40)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[60])
	call_c   Dyam_Cut()
	pl_fail

pl_code local fun56
	call_c   Dyam_Update_Choice(fun55)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Reg_Load(0,V(1))
	pl_call  pred_check_fun_tupple_1()
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(2))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_label_local_fun_tupple_2()

pl_code local fun57
	call_c   Dyam_Update_Choice(fun56)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Reg_Load(0,V(1))
	pl_call  pred_check_tupple_1()
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(2))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_label_local_tupple_2()

pl_code local fun58
	call_c   Dyam_Update_Choice(fun57)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[57])
	fail_ret
	call_c   DYAM_evpred_number(V(3))
	fail_ret
	pl_call  Object_1(&ref[58])
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(2),&ref[59])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun59
	call_c   Dyam_Update_Choice(fun58)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_functor(V(1),&ref[55],N(3))
	fail_ret
	call_c   Dyam_Cut()
	call_c   DYAM_evpred_univ(V(1),&ref[56])
	fail_ret
	call_c   Dyam_Reg_Load(0,V(10))
	call_c   Dyam_Reg_Load(2,V(2))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_label_local_term_2()

pl_code local fun60
	call_c   Dyam_Update_Choice(fun59)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[54])
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load_Ptr(2,V(4))
	fail_ret
	move     V(7), R(4)
	move     S(5), R(5)
	call_c   DyALog_Mutable_Read(R(2),&R(4))
	fail_ret
	call_c   Dyam_Choice(fun9)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(7),I(0))
	fail_ret
	call_c   Dyam_Cut()
	move     &ref[54], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Deallocate(1)
	pl_jump  pred_local_register_again_1()

pl_code local fun61
	call_c   Dyam_Update_Choice(fun60)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[41])
	call_c   Dyam_Choice(fun38)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Reg_Load(0,V(2))
	pl_call  pred_term_lock_1()
	call_c   Dyam_Cut()
	pl_fail

pl_code local fun62
	call_c   Dyam_Update_Choice(fun61)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[52])
	fail_ret
	call_c   DYAM_sfol_identical(V(3),N(0))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(2),&ref[53])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun63
	call_c   Dyam_Update_Choice(fun62)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[40])
	call_c   Dyam_Cut()
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun35
	call_c   Dyam_Remove_Choice()
fun34:
	call_c   Dyam_Reg_Load_Ptr(2,V(3))
	fail_ret
	call_c   Dyam_Reg_Load(4,V(8))
	call_c   DyALog_Mutable_Write(R(2),&R(4),0)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(3))
	fail_ret
	call_c   Dyam_Reg_Load_Ptr(2,V(5))
	fail_ret
	call_c   Dyam_Reg_Load(4,&ref[49])
	call_c   DyALog_Mutable_Write(R(2),&R(4),0)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(5))
	fail_ret
	call_c   Dyam_Reg_Deallocate(4)
	pl_ret


pl_code local fun31
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(1),&ref[45])
	fail_ret
	call_c   Dyam_Reg_Load(0,V(9))
	move     V(10), R(2)
	move     S(5), R(3)
	pl_call  pred_label_local_alt_tupple_2()
	call_c   Dyam_Unify(V(4),&ref[46])
	fail_ret
	call_c   Dyam_Unify(V(5),&ref[47])
	fail_ret
	move     &ref[48], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(4))
	call_c   Dyam_Reg_Load(4,V(2))
	call_c   Dyam_Reg_Load(6,V(5))
	call_c   Dyam_Reg_Deallocate(4)
	pl_jump  pred_local_register_4()

pl_code local fun32
	call_c   Dyam_Update_Choice(fun31)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[44])
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load_Ptr(2,V(3))
	fail_ret
	move     V(6), R(4)
	move     S(5), R(5)
	call_c   DyALog_Mutable_Read(R(2),&R(4))
	fail_ret
	call_c   Dyam_Choice(fun9)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(6),I(0))
	fail_ret
	call_c   Dyam_Cut()
	move     &ref[44], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Deallocate(1)
	pl_jump  pred_local_register_again_1()

pl_code local fun25
	call_c   Dyam_Remove_Choice()
fun24:
	call_c   Dyam_Cut()
	move     &ref[38], R(0)
	move     0, R(1)
	move     &ref[39], R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_internal_error_2()


pl_code local fun26
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Reg_Load(0,V(1))
	pl_call  pred_globalize_subterms_1()
	call_c   Dyam_Unify(V(2),&ref[42])
	fail_ret
	call_c   Dyam_Reg_Load_Ptr(2,V(4))
	fail_ret
	call_c   Dyam_Reg_Load(4,V(1))
	call_c   DyALog_Mutable_Write(R(2),&R(4),0)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(4))
	fail_ret
	move     &ref[43], R(0)
	move     0, R(1)
	call_c   Dyam_Reg_Load(2,V(3))
	pl_call  pred_update_counter_2()
	call_c   DYAM_evpred_assert_1(&ref[41])
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun27
	call_c   Dyam_Update_Choice(fun26)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[41])
	call_c   Dyam_Cut()
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun28
	call_c   Dyam_Update_Choice(fun27)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[40])
	call_c   Dyam_Cut()
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun29
	call_c   Dyam_Update_Choice(fun28)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Reg_Load(2,V(1))
	call_c   Compiler_Label_Elementary_Term(&R(2))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(2),V(1))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun20
	call_c   Dyam_Remove_Choice()
fun19:
	call_c   Dyam_Reg_Load_Ptr(2,V(2))
	fail_ret
	call_c   Dyam_Reg_Load(4,V(16))
	call_c   DyALog_Mutable_Write(R(2),&R(4),0)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(2))
	fail_ret
	call_c   Dyam_Reg_Deallocate(4)
	pl_ret


pl_code local fun22
	call_c   Dyam_Remove_Choice()
fun21:
	move     &ref[35], R(0)
	move     0, R(1)
	move     &ref[36], R(2)
	move     S(5), R(3)
	move     V(16), R(4)
	move     S(5), R(5)
	pl_call  pred_add_ordered_table_3()
	call_c   Dyam_Reg_Load_Ptr(2,V(5))
	fail_ret
	call_c   Dyam_Reg_Load(4,V(16))
	call_c   DyALog_Mutable_Write(R(2),&R(4),0)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(5))
	fail_ret
	call_c   Dyam_Choice(fun20)
	pl_call  Domain_2(&ref[37],V(7))
	call_c   Dyam_Reg_Load_Ptr(2,V(17))
	fail_ret
	call_c   Dyam_Reg_Load(4,V(16))
	call_c   DyALog_Mutable_Write(R(2),&R(4),0)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(17))
	fail_ret
	pl_fail


pl_code local fun17
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Reg_Load(0,V(1))
	move     V(4), R(2)
	move     S(5), R(3)
	pl_call  pred_label_local_term_2()
	call_c   Dyam_Reg_Load(0,V(2))
	move     V(5), R(2)
	move     S(5), R(3)
	pl_call  pred_oset_list_alt_2()
	call_c   Dyam_Unify(V(6),V(4))
	fail_ret
	call_c   Dyam_Unify(V(7),&ref[30])
	fail_ret
	move     &ref[31], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(7))
	call_c   Dyam_Reg_Load(4,V(3))
	call_c   Dyam_Reg_Load(6,V(6))
	call_c   Dyam_Reg_Deallocate(4)
	pl_jump  pred_local_register_4()

pl_code local fun15
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(4),I(0))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun13
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Reg_Load(0,V(1))
	move     V(7), R(2)
	move     S(5), R(3)
	pl_call  pred_label_local_term_2()
	call_c   Dyam_Unify(V(4),&ref[26])
	fail_ret
	call_c   Dyam_Unify(V(3),&ref[27])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun11
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Reg_Load(0,V(2))
	move     V(5), R(2)
	move     S(5), R(3)
	pl_call  pred_label_term_2()
	pl_jump  fun10()

pl_code local fun9
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun7
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Reg_Load(0,V(1))
	move     V(3), R(2)
	move     S(5), R(3)
	pl_call  pred_oset_list_alt_2()
	call_c   Dyam_Unify(V(4),&ref[20])
	fail_ret
	move     &ref[21], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(4))
	call_c   Dyam_Reg_Load(4,V(2))
	move     I(0), R(6)
	move     0, R(7)
	call_c   Dyam_Reg_Deallocate(4)
	pl_jump  pred_local_register_4()

pl_code local fun4
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(2),I(0))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret


;;------------------ INIT POOL ------------

c_code local build_init_pool
	call_c   build_seed_0()
	call_c   build_seed_1()
	call_c   build_seed_2()
	call_c   build_seed_3()
	call_c   build_seed_4()
	call_c   build_seed_5()
	call_c   build_seed_6()
	call_c   build_seed_7()
	call_c   build_seed_8()
	call_c   build_seed_9()
	call_c   build_seed_10()
	call_c   build_seed_11()
	call_c   build_seed_12()
	call_c   build_seed_13()
	call_c   build_seed_14()
	call_c   build_ref_53()
	call_c   build_ref_52()
	call_c   build_ref_54()
	call_c   build_ref_56()
	call_c   build_ref_55()
	call_c   build_ref_59()
	call_c   build_ref_60()
	call_c   build_ref_64()
	call_c   build_ref_63()
	call_c   build_ref_62()
	call_c   build_ref_61()
	call_c   build_ref_65()
	call_c   build_ref_69()
	call_c   build_ref_68()
	call_c   build_ref_67()
	call_c   build_ref_72()
	call_c   build_ref_71()
	call_c   build_ref_70()
	call_c   build_ref_66()
	call_c   build_ref_58()
	call_c   build_ref_77()
	call_c   build_ref_76()
	call_c   build_ref_73()
	call_c   build_ref_75()
	call_c   build_ref_74()
	call_c   build_ref_57()
	call_c   build_ref_83()
	call_c   build_ref_82()
	call_c   build_ref_81()
	call_c   build_ref_80()
	call_c   build_ref_85()
	call_c   build_ref_86()
	call_c   build_ref_78()
	call_c   build_ref_84()
	call_c   build_ref_50()
	call_c   build_ref_49()
	call_c   build_ref_51()
	call_c   build_ref_44()
	call_c   build_ref_48()
	call_c   build_ref_47()
	call_c   build_ref_46()
	call_c   build_ref_45()
	call_c   build_ref_39()
	call_c   build_ref_38()
	call_c   build_ref_40()
	call_c   build_ref_41()
	call_c   build_ref_43()
	call_c   build_ref_42()
	call_c   build_ref_34()
	call_c   build_ref_33()
	call_c   build_ref_37()
	call_c   build_ref_35()
	call_c   build_ref_36()
	call_c   build_ref_31()
	call_c   build_ref_30()
	call_c   build_ref_32()
	call_c   build_ref_29()
	call_c   build_ref_28()
	call_c   build_ref_25()
	call_c   build_ref_24()
	call_c   build_ref_27()
	call_c   build_ref_26()
	call_c   build_ref_23()
	call_c   build_ref_22()
	call_c   build_ref_19()
	call_c   build_ref_20()
	call_c   build_ref_21()
	call_c   build_ref_18()
	call_c   build_ref_17()
	call_c   build_ref_16()
	c_ret

long local ref[120]
long local seed[15]

long local _initialization

c_code global initialization_dyalog_term
	call_c   Dyam_Check_And_Update_Initialization(_initialization)
	fail_c_ret
	call_c   initialization_dyalog_format()
	call_c   build_init_pool()
	call_c   build_viewers()
	call_c   load()
	c_ret

