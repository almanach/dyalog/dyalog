/* -*- mode:prolog; -*-
 ************************************************************
 * $Id$
 * Copyright (C) 1998 Eric de la Clergerie
 * ------------------------------------------------------------
 *
 *   Maker --  Builder for Auxiliary Atoms (for LPDAs)
 *
 * ------------------------------------------------------------
 * Description
 *
 * ------------------------------------------------------------
 */

:-include 'header.pl'.

/**********************************************************************
 * Call Return Maker
 **********************************************************************/

make_callret(T,Call,Return) :-
	functor(T,P,N),
	functor(TT,P,N),
	make_true_callret(TT,Call,Return),
	T=TT
	.

:-light_tabular make_true_callret/3.
:-mode(make_true_callret/3,+(+,-,-)).

make_true_callret(T,Call,Return) :-
	functor(T,P,N),
	( recorded( 'call_pattern'(P/N,Pattern) ) -> true ;
	    recorded( 'call_pattern'('*default*',Pattern ) ) ->  true ;
	    Pattern=(+)
	),
	make_callret_with_pat(Pattern,T,Call,Return),
%%	format('Callret ~w ~w -> ~w ~w\n',[Pattern,T,Call,Return]),
	(   %% side effect to register a viewver for predicate P/N
	    numbervars( T ),
	    I = '*RITEM*'(Call,Return),
	    label_term(I,CI),
	    label_term(T,CT),
	    record( viewer(CI,CT) ),
	    fail
	;   
	    true
	)
	.

:-rec_prolog
	make_callret_with_pat/4,
	make_callret_arg/4
	.
	
make_callret_with_pat(+,T,T,voidret).
make_callret_with_pat(-,T,voidcall,T).
make_callret_with_pat(*,T,T,T).

make_callret_with_pat(Pattern,T,Call,Return) :-
%%	format('make_callret_with_pat ~w for ~w\n',[Pattern,T]),
	( Pattern = [Pat|LPattern] -> true
	;   compound(Pattern) ->
	    Pattern =.. [Pat|LPattern]
	;
	    fail
	),
	T =.. [_|LT],
	functor(T,F,N),
	make_callret_functor(Pat,F,N,FCall,FRet),
	make_callret_arg(LPattern,LT,LCall,LRet),
	Return =.. [FRet|LRet],
	Call =.. [FCall|LCall]
	.

:-std_prolog make_callret_functor/5.

make_callret_functor(Mode,F,N,FC,FR) :-
	(Mode == (-) -> FC=call ; build_aux_name('call_~w/~w',F/N,FC)),
	(Mode == (+) -> FR=return ; build_aux_name('return_~w/~w',F/N,FR))
	.

:-std_prolog build_aux_name/3.

build_aux_name(Fmt,F/N,Name) :-
%%	format('Call AUX NAME ~w\n',[F/N]),
	atom_module(F,Module,FF),
	name_builder(Fmt,[FF,N],Name1),
	atom_module(Name,Module,Name1),
%%	format('AUX NAME ~w -> ~w\n',[F/N,Name]),
	true
	.

make_callret_arg(_,[],[],[]).
make_callret_arg([+|LPat],[A|L],[A|LCall],LRet) :-
    make_callret_arg(LPat,L,LCall,LRet).
make_callret_arg([*|LPat],[A|L],[A|LCall],[A|LRet]) :-
    make_callret_arg(LPat,L,LCall,LRet).
make_callret_arg([-|LPat],[A|L],LCall,[A|LRet]) :-
    make_callret_arg(LPat,L,LCall,LRet).

make_callret_arg(+,Args,Args,[]).
make_callret_arg(-,Args,[],Args).
make_callret_arg(*,Args,Args,Args).

/**********************************************************************
 * Call Return DCG Maker
 **********************************************************************/

make_dcg_callret(T,Left,Right,BMG_Left,BMG_Right,Call,Return) :-
    functor(T,P,N),
    functor(TT,P,N),
    make_true_dcg_callret(TT,XLeft,XRight,BMG_XLeft,BMG_XRight,Call,Return),
    T*Left*Right*BMG_Left*BMG_Right=TT*XLeft*XRight*BMG_XLeft*BMG_XRight
    .

:-light_tabular make_true_dcg_callret/7.
:-mode(make_true_dcg_callret/7,[+,+|-]).

make_true_dcg_callret(T,Left,Right,BMG_Left,BMG_Right,Call,Return) :-
	functor(T,P,N),
	( recorded( 'call_dcg_pattern'(P/N,Pat,LPat,RPat) ) -> true
	;   recorded( 'call_dcg_pattern'('*default*',Pat, LPat, RPat ) ) ->  true
	;   Pat=(+), LPat=(+), RPat=(-)
	),
	
	bmg_patterns(BMG_LPat,BMG_RPat),

%%	format('Pattern for ~w: ~w ~w ~w\n',[T,Pat,LPat,RPat]),
	
	( Pat = [FPat|Args_Pat] ->
	    bmg_args(LPat,RPat,BMG_LPat,BMG_RPat,Args_Pat,BMG_Pat),
	    Pattern = [FPat|BMG_Pat]
	;   compound(Pat) ->
	    Pat =.. [FPat|Args_Pat],
	    bmg_args(LPat,RPat,BMG_LPat,BMG_RPat,Args_Pat,BMG_Pat),
	    Pattern = [FPat|BMG_Pat]
	;   
	    bmg_args(LPat,RPat,BMG_LPat,BMG_RPat,Pat,BMG_Pat),
	    Pattern = [Pat|BMG_Pat]
	),
	T =.. [F|Args],
	bmg_args(Left,Right,BMG_Left,BMG_Right,Args,BMG_Args),
	TT =.. [F|BMG_Args],
	make_callret_with_pat(Pattern,TT,Call,Return),
	(   %% side effect to register a viewver for predicate P/N
	    numbervars( TT ),
	    I = '*RITEM*'(Call,Return),
	    label_term(I,CI),
	    label_term(T(Left,Right),CT),
	    record( viewer(CI,CT) ),
	    fail
	;   
	    true
	)
	.

bmg_args(Left,Right,BMG_Left,BMG_Right,Args,[Left,Right|BMG_Args]) :-
	bmg_get_stacks(BMG_Left),
	bmg_get_stacks(BMG_Right),
        append(BMG_Left,BMG_Right,BMG),
        append(BMG,Args,BMG_Args)
	.

bmg_uniform_stacks(Motif,S) :-
	bmg_stacks(Stacks),
	length(Stacks,N),
	make_list(S,N,Motif).

:-light_tabular bmg_patterns/2.
:-mode(bmg_patterns/2,+(-,-)).

bmg_patterns(L,R) :-
	bmg_uniform_stacks(+,L),
	bmg_uniform_stacks(-,R)
	.




