;; Compiler: DyALog 1.14.0
;; File "emit.pl"


;;------------------ LOADING ------------

c_code local load
	call_c   Dyam_Loading_Set()
	pl_call  fun63(&seed[6],0)
	pl_call  fun63(&seed[3],0)
	pl_call  fun63(&seed[11],0)
	pl_call  fun63(&seed[12],0)
	pl_call  fun63(&seed[7],0)
	pl_call  fun63(&seed[14],0)
	pl_call  fun63(&seed[9],0)
	pl_call  fun63(&seed[8],0)
	pl_call  fun63(&seed[10],0)
	pl_call  fun63(&seed[5],0)
	pl_call  fun63(&seed[4],0)
	pl_call  fun63(&seed[16],0)
	pl_call  fun63(&seed[13],0)
	pl_call  fun63(&seed[0],0)
	pl_call  fun63(&seed[2],0)
	pl_call  fun63(&seed[1],0)
	pl_call  fun63(&seed[15],0)
	call_c   Dyam_Loading_Reset()
	c_ret

pl_code global pred_emit_seed_1
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Unify(0,&ref[40])
	fail_ret
	call_c   Dyam_Reg_Load_Ptr(2,V(2))
	fail_ret
	call_c   Dyam_Reg_Load(4,&ref[226])
	call_c   DyALog_Mutable_Read(R(2),&R(4))
	fail_ret
	call_c   Dyam_Reg_Load_Ptr(2,V(2))
	fail_ret
	move     I(0), R(4)
	move     0, R(5)
	call_c   DyALog_Mutable_Write(R(2),&R(4),0)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(2))
	fail_ret
	call_c   DYAM_evpred_functor(V(6),V(18),V(19))
	fail_ret
	call_c   DYAM_evpred_univ(V(6),&ref[227])
	fail_ret
	move     &ref[23], R(0)
	move     0, R(1)
	move     &ref[228], R(2)
	move     S(5), R(3)
	pl_call  pred_ma_emit_code_2()
	move     &ref[40], R(0)
	move     S(5), R(1)
	pl_call  pred_ma_emit_ret_reg_1()
	move     &ref[229], R(0)
	move     S(5), R(1)
	move     &ref[40], R(2)
	move     S(5), R(3)
	pl_call  pred_emit_globals_2()
	move     &ref[230], R(0)
	move     0, R(1)
	move     &ref[229], R(2)
	move     S(5), R(3)
	pl_call  pred_ma_emit_call_c_2()
	call_c   Dyam_Choice(fun114)
	pl_call  Domain_2(&ref[231],V(21))
	move     &ref[232], R(0)
	move     S(5), R(1)
	move     &ref[40], R(2)
	move     S(5), R(3)
	pl_call  pred_emit_globals_2()
	move     &ref[233], R(0)
	move     0, R(1)
	move     &ref[232], R(2)
	move     S(5), R(3)
	pl_call  pred_ma_emit_call_c_2()
	pl_fail

pl_code global pred_emit_term_1
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Unify(0,&ref[44])
	fail_ret
	call_c   Dyam_Reg_Load_Ptr(2,V(2))
	fail_ret
	move     V(3), R(4)
	move     S(5), R(5)
	call_c   DyALog_Mutable_Read(R(2),&R(4))
	fail_ret
	call_c   Dyam_Reg_Load_Ptr(2,V(2))
	fail_ret
	move     I(0), R(4)
	move     0, R(5)
	call_c   DyALog_Mutable_Write(R(2),&R(4),0)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(2))
	fail_ret
	move     &ref[206], R(0)
	move     0, R(1)
	move     &ref[207], R(2)
	move     S(5), R(3)
	pl_call  pred_format_2()
	move     &ref[23], R(0)
	move     0, R(1)
	move     &ref[208], R(2)
	move     S(5), R(3)
	pl_call  pred_ma_emit_code_2()
	move     &ref[44], R(0)
	move     S(5), R(1)
	pl_call  pred_ma_emit_ret_reg_1()
	pl_call  Object_1(&ref[209])
	call_c   Dyam_Reg_Load_Ptr(2,V(4))
	fail_ret
	call_c   Dyam_Reg_Load(4,&ref[44])
	call_c   DyALog_Mutable_Write(R(2),&R(4),0)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(4))
	fail_ret
	call_c   Dyam_Reg_Load(0,V(3))
	move     &ref[210], R(2)
	move     S(5), R(3)
	pl_call  pred_label_local_term_2()
	call_c   Dyam_Reg_Load_Ptr(2,V(6))
	fail_ret
	call_c   Dyam_Reg_Load(4,&ref[44])
	call_c   DyALog_Mutable_Write(R(2),&R(4),0)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(6))
	fail_ret
	call_c   Dyam_Choice(fun112)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[211])
	call_c   Dyam_Cut()
fun111:
	call_c   Dyam_Reg_Load_Ptr(2,V(33))
	fail_ret
	call_c   Dyam_Reg_Load(4,&ref[212])
	call_c   DyALog_Mutable_Write(R(2),&R(4),0)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(33))
	fail_ret
	call_c   Dyam_Choice(fun110)
	move     &ref[213], R(0)
	move     0, R(1)
	move     V(8), R(2)
	move     S(5), R(3)
	pl_call  pred_ordered_table_2()
	call_c   Dyam_Unify(V(8),&ref[214])
	fail_ret
	call_c   Dyam_Reg_Load_Ptr(2,V(11))
	fail_ret
	move     V(14), R(4)
	move     S(5), R(5)
	call_c   DyALog_Mutable_Read(R(2),&R(4))
	fail_ret
	call_c   Dyam_Unify(V(10),&ref[215])
	fail_ret
	call_c   Dyam_Choice(fun109)
	call_c   Dyam_Unify(V(18),&ref[216])
	fail_ret
	pl_call  Domain_2(V(18),V(13))
	call_c   Dyam_Reg_Load(0,V(18))
	call_c   Dyam_Reg_Load(2,V(14))
	pl_call  pred_free_arg_2()
	pl_fail


pl_code global pred_emit_initializer_0
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	move     &ref[196], R(0)
	move     0, R(1)
	move     V(1), R(2)
	move     S(5), R(3)
	pl_call  pred_value_counter_2()
	move     &ref[23], R(0)
	move     0, R(1)
	move     &ref[197], R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Load(4,V(1))
	move     I(0), R(6)
	move     0, R(7)
	pl_call  pred_ma_emit_table_4()
	move     &ref[198], R(0)
	move     0, R(1)
	move     V(2), R(2)
	move     S(5), R(3)
	pl_call  pred_value_counter_2()
	move     &ref[23], R(0)
	move     0, R(1)
	move     &ref[199], R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Load(4,V(2))
	move     I(0), R(6)
	move     0, R(7)
	pl_call  pred_ma_emit_table_4()
	call_c   DYAM_Nl_0()
	fail_ret
	call_c   Dyam_Choice(fun100)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[60])
	call_c   Dyam_Cut()
	move     &ref[156], R(0)
	move     0, R(1)
	move     &ref[200], R(2)
	move     0, R(3)
	pl_call  pred_ma_emit_code_2()
	call_c   Dyam_Reg_Deallocate(0)
	pl_jump  pred_emit_initializer_aux_0()

pl_code global pred_free_arg_2
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Unify(0,&ref[109])
	fail_ret
	call_c   Dyam_Reg_Bind(2,V(4))
	call_c   Dyam_Reg_Load_Ptr(2,V(2))
	fail_ret
	move     V(5), R(4)
	move     S(5), R(5)
	call_c   DyALog_Mutable_Read(R(2),&R(4))
	fail_ret
	call_c   Dyam_Choice(fun98)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(5),&ref[192])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Deallocate()
	pl_ret

pl_code global pred_emit_propagated_directives_0
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Choice(fun10)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[60])
	call_c   Dyam_Cut()
	call_c   DYAM_Write_2(I(0),&ref[155])
	fail_ret
	call_c   DYAM_Nl_0()
	fail_ret
	move     &ref[156], R(0)
	move     0, R(1)
	move     &ref[157], R(2)
	move     0, R(3)
	pl_call  pred_ma_emit_code_2()
	move     &ref[24], R(0)
	move     0, R(1)
	move     I(0), R(2)
	move     0, R(3)
	pl_call  pred_ma_emit_call_c_2()
	call_c   Dyam_Choice(fun87)
	pl_call  Object_1(&ref[158])
	call_c   Dyam_Reg_Load(0,V(2))
	move     V(3), R(2)
	move     S(5), R(3)
	pl_call  pred_label_term_2()
	call_c   Dyam_Reg_Load(0,V(1))
	move     V(4), R(2)
	move     S(5), R(3)
	pl_call  pred_label_term_2()
	move     &ref[159], R(0)
	move     S(5), R(1)
	pl_call  pred_emit_globals_in_init_pool_1()
	move     &ref[160], R(0)
	move     0, R(1)
	move     &ref[159], R(2)
	move     S(5), R(3)
	pl_call  pred_ma_emit_call_c_2()
	pl_fail

pl_code global pred_first_register_1
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	pl_call  Object_1(&ref[139])
	call_c   Dyam_Reg_Load_Ptr(2,V(2))
	fail_ret
	call_c   Dyam_Reg_Load(4,&ref[140])
	call_c   DyALog_Mutable_Read(R(2),&R(4))
	fail_ret
	call_c   Dyam_Choice(fun79)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(4),&ref[141])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load_Ptr(2,V(2))
	fail_ret
	call_c   Dyam_Reg_Load(4,&ref[142])
	call_c   DyALog_Mutable_Write(R(2),&R(4),0)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(2))
	fail_ret
	call_c   Dyam_Reg_Deallocate(4)
	pl_ret

pl_code global pred_emit_xtupple_1
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Choice(fun71)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[115])
	fail_ret
	call_c   Dyam_Cut()
	move     &ref[116], R(0)
	move     0, R(1)
	move     &ref[117], R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_ma_emit_call_c_2()

pl_code global pred_emit_fun_tupple_2
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(2)
	call_c   Dyam_Choice(fun69)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),I(0))
	fail_ret
	call_c   Dyam_Cut()
	move     &ref[110], R(0)
	move     0, R(1)
	move     &ref[111], R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_ma_emit_call_c_2()

pl_code global pred_emit_lterm_post_2
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Unify(0,&ref[109])
	fail_ret
	call_c   Dyam_Reg_Bind(2,V(4))
	call_c   Dyam_Reg_Load_Ptr(2,V(2))
	fail_ret
	call_c   Dyam_Reg_Load(4,V(4))
	call_c   DyALog_Mutable_Read(R(2),&R(4))
	fail_ret
	call_c   Dyam_Reg_Load(0,V(4))
	pl_call  pred_ma_emit_move_ret_1()
	call_c   Dyam_Choice(fun67)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Choice(fun66)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[106])
	call_c   Dyam_Cut()
	pl_fail

pl_code global pred_emit_viewers_0
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   DYAM_Write_2(I(0),&ref[103])
	fail_ret
	call_c   DYAM_Nl_0()
	fail_ret
	move     &ref[23], R(0)
	move     0, R(1)
	move     &ref[61], R(2)
	move     0, R(3)
	pl_call  pred_ma_emit_code_2()
	call_c   Dyam_Choice(fun6)
	pl_call  Object_1(&ref[104])
	move     &ref[90], R(0)
	move     S(5), R(1)
	pl_call  pred_emit_globals_in_init_pool_1()
	move     &ref[105], R(0)
	move     0, R(1)
	move     &ref[90], R(2)
	move     S(5), R(3)
	pl_call  pred_ma_emit_call_c_2()
	pl_fail

pl_code global pred_emit_header_0
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   DYAM_Compiler_Info_1(&ref[95])
	fail_ret
	move     &ref[96], R(0)
	move     0, R(1)
	move     &ref[90], R(2)
	move     S(5), R(3)
	pl_call  pred_format_2()
	call_c   Dyam_Choice(fun61)
	pl_call  Object_1(&ref[97])
	call_c   Dyam_Choice(fun60)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(5),&ref[98])
	fail_ret
	call_c   Dyam_Cut()
	pl_fail

pl_code global pred_safe_c_normalize_2
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(2)
	move     &ref[94], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Load(4,V(1))
	call_c   Dyam_Reg_Load(6,V(2))
	call_c   Dyam_Reg_Deallocate(4)
fun52:
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Reg_Bind(2,V(4))
	call_c   Dyam_Multi_Reg_Bind(4,2,2)
	call_c   Dyam_Choice(fun51)
	pl_call  fun48(&seed[20],1)
	pl_fail


pl_code global pred_emit_loading_0
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   DYAM_Write_2(I(0),&ref[63])
	fail_ret
	call_c   DYAM_Nl_0()
	fail_ret
	move     &ref[23], R(0)
	move     0, R(1)
	move     &ref[62], R(2)
	move     0, R(3)
	pl_call  pred_ma_emit_code_2()
	move     &ref[64], R(0)
	move     0, R(1)
	move     I(0), R(2)
	move     0, R(3)
	pl_call  pred_ma_emit_call_c_2()
	call_c   Dyam_Choice(fun37)
	pl_call  Object_1(&ref[65])
	call_c   Dyam_Reg_Load(0,V(1))
	pl_call  pred_emit_init_code_1()
	pl_fail

pl_code global pred_emit_initializer_aux_0
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Choice(fun34)
	pl_call  Object_1(&ref[58])
	call_c   DYAM_Decompose_File_Name_4(V(1),V(2),V(3),V(4))
	fail_ret
	call_c   Dyam_Reg_Load(0,V(3))
	move     V(5), R(2)
	move     S(5), R(3)
	pl_call  pred_safe_c_normalize_2()
	move     &ref[59], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	pl_call  pred_ma_emit_call_c_2()
	pl_fail

pl_code global pred_clean_globals_1
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Choice(fun14)
	pl_call  Object_2(&ref[29],V(4))
	call_c   DYAM_evpred_retract(&ref[29])
	fail_ret
	pl_call  fun2(&seed[19],1)
	pl_fail

pl_code global pred_emit_named_functions_0
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Choice(fun10)
	pl_call  Object_1(&ref[6])
	pl_call  fun2(&seed[17],1)
	pl_fail

pl_code global pred_emit_init_pool_0
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Choice(fun8)
	pl_call  Object_1(&ref[11])
	pl_call  fun2(&seed[17],1)
	pl_fail

pl_code global pred_register_info_2
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(2)
	pl_call  Object_1(&ref[12])
	call_c   Dyam_Reg_Load_Ptr(2,V(3))
	fail_ret
	call_c   Dyam_Reg_Load(4,&ref[13])
	call_c   DyALog_Mutable_Read(R(2),&R(4))
	fail_ret
	call_c   Dyam_Reg_Deallocate(3)
	pl_ret


;;------------------ VIEWERS ------------

c_code local build_viewers
	call_c   Dyam_Load_Viewer(&ref[5],&ref[4])
	call_c   Dyam_Load_Viewer(&ref[3],&ref[1])
	c_ret

c_code local build_seed_15
	ret_reg &seed[15]
	call_c   build_ref_14()
	call_c   build_ref_17()
	call_c   Dyam_Seed_Start(&ref[14],&ref[17],I(0),fun19,1)
	call_c   build_ref_19()
	call_c   Dyam_Seed_Add_Comp(&ref[19],fun18,0)
	call_c   Dyam_Seed_End()
	move_ret seed[15]
	c_ret

;; TERM 19: '*CITEM*'(c_normalize(_B, _C), _A)
c_code local build_ref_19
	ret_reg &ref[19]
	call_c   build_ref_18()
	call_c   build_ref_15()
	call_c   Dyam_Create_Binary(&ref[18],&ref[15],V(0))
	move_ret ref[19]
	c_ret

;; TERM 15: c_normalize(_B, _C)
c_code local build_ref_15
	ret_reg &ref[15]
	call_c   build_ref_235()
	call_c   Dyam_Create_Binary(&ref[235],V(1),V(2))
	move_ret ref[15]
	c_ret

;; TERM 235: c_normalize
c_code local build_ref_235
	ret_reg &ref[235]
	call_c   Dyam_Create_Atom("c_normalize")
	move_ret ref[235]
	c_ret

;; TERM 18: '*CITEM*'
c_code local build_ref_18
	ret_reg &ref[18]
	call_c   Dyam_Create_Atom("*CITEM*")
	move_ret ref[18]
	c_ret

c_code local build_seed_18
	ret_reg &seed[18]
	call_c   build_ref_0()
	call_c   build_ref_20()
	call_c   Dyam_Seed_Start(&ref[0],&ref[20],&ref[20],fun0,1)
	call_c   build_ref_21()
	call_c   Dyam_Seed_Add_Comp(&ref[21],&ref[20],0)
	call_c   Dyam_Seed_End()
	move_ret seed[18]
	c_ret

pl_code local fun0
	pl_jump  Complete(0,0)

;; TERM 21: '*RITEM*'(_A, voidret) :> '$$HOLE$$'
c_code local build_ref_21
	ret_reg &ref[21]
	call_c   build_ref_20()
	call_c   Dyam_Create_Binary(I(9),&ref[20],I(7))
	move_ret ref[21]
	c_ret

;; TERM 20: '*RITEM*'(_A, voidret)
c_code local build_ref_20
	ret_reg &ref[20]
	call_c   build_ref_0()
	call_c   build_ref_2()
	call_c   Dyam_Create_Binary(&ref[0],V(0),&ref[2])
	move_ret ref[20]
	c_ret

;; TERM 2: voidret
c_code local build_ref_2
	ret_reg &ref[2]
	call_c   Dyam_Create_Atom("voidret")
	move_ret ref[2]
	c_ret

;; TERM 0: '*RITEM*'
c_code local build_ref_0
	ret_reg &ref[0]
	call_c   Dyam_Create_Atom("*RITEM*")
	move_ret ref[0]
	c_ret

pl_code local fun16
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Object(R(0))
	call_c   Dyam_Tabule()
	pl_ret

pl_code local fun17
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Choice(fun16)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Subsume(R(0))
	fail_ret
	call_c   Dyam_Cut()
	pl_ret

long local pool_fun18[3]=[2,build_ref_19,build_seed_18]

pl_code local fun18
	call_c   Dyam_Pool(pool_fun18)
	call_c   Dyam_Unify_Item(&ref[19])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load_String(2,V(1))
	fail_ret
	call_c   C_Normalize(R(2))
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_String(0,V(2))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_jump  fun17(&seed[18],2)

pl_code local fun19
	pl_jump  Apply(0,0)

;; TERM 17: '*FIRST*'(c_normalize(_B, _C)) :> []
c_code local build_ref_17
	ret_reg &ref[17]
	call_c   build_ref_16()
	call_c   Dyam_Create_Binary(I(9),&ref[16],I(0))
	move_ret ref[17]
	c_ret

;; TERM 16: '*FIRST*'(c_normalize(_B, _C))
c_code local build_ref_16
	ret_reg &ref[16]
	call_c   build_ref_14()
	call_c   build_ref_15()
	call_c   Dyam_Create_Unary(&ref[14],&ref[15])
	move_ret ref[16]
	c_ret

;; TERM 14: '*FIRST*'
c_code local build_ref_14
	ret_reg &ref[14]
	call_c   Dyam_Create_Atom("*FIRST*")
	move_ret ref[14]
	c_ret

c_code local build_seed_1
	ret_reg &seed[1]
	call_c   build_ref_36()
	call_c   build_ref_39()
	call_c   Dyam_Seed_Start(&ref[36],&ref[39],I(0),fun23,1)
	call_c   build_ref_38()
	call_c   Dyam_Seed_Add_Comp(&ref[38],fun22,0)
	call_c   Dyam_Seed_End()
	move_ret seed[1]
	c_ret

;; TERM 38: '*GUARD*'(data_emit('$seed'(_B, _C)))
c_code local build_ref_38
	ret_reg &ref[38]
	call_c   build_ref_7()
	call_c   build_ref_37()
	call_c   Dyam_Create_Unary(&ref[7],&ref[37])
	move_ret ref[38]
	c_ret

;; TERM 37: data_emit('$seed'(_B, _C))
c_code local build_ref_37
	ret_reg &ref[37]
	call_c   build_ref_236()
	call_c   build_ref_40()
	call_c   Dyam_Create_Unary(&ref[236],&ref[40])
	move_ret ref[37]
	c_ret

;; TERM 40: '$seed'(_B, _C)
c_code local build_ref_40
	ret_reg &ref[40]
	call_c   build_ref_237()
	call_c   Dyam_Create_Binary(&ref[237],V(1),V(2))
	move_ret ref[40]
	c_ret

;; TERM 237: '$seed'
c_code local build_ref_237
	ret_reg &ref[237]
	call_c   Dyam_Create_Atom("$seed")
	move_ret ref[237]
	c_ret

;; TERM 236: data_emit
c_code local build_ref_236
	ret_reg &ref[236]
	call_c   Dyam_Create_Atom("data_emit")
	move_ret ref[236]
	c_ret

;; TERM 7: '*GUARD*'
c_code local build_ref_7
	ret_reg &ref[7]
	call_c   Dyam_Create_Atom("*GUARD*")
	move_ret ref[7]
	c_ret

pl_code local fun21
	call_c   Dyam_Remove_Choice()
fun20:
	move     &ref[40], R(0)
	move     S(5), R(1)
	pl_call  pred_emit_seed_1()
	move     &ref[40], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Deallocate(1)
	pl_jump  pred_clean_globals_1()


long local pool_fun22[3]=[2,build_ref_38,build_ref_40]

pl_code local fun22
	call_c   Dyam_Pool(pool_fun22)
	call_c   Dyam_Unify_Item(&ref[38])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun21)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Reg_Load_Ptr(2,V(2))
	fail_ret
	move     I(0), R(4)
	move     0, R(5)
	call_c   DyALog_Mutable_Read(R(2),&R(4))
	fail_ret
	call_c   Dyam_Cut()
	pl_fail

pl_code local fun23
	pl_ret

;; TERM 39: '*GUARD*'(data_emit('$seed'(_B, _C))) :> []
c_code local build_ref_39
	ret_reg &ref[39]
	call_c   build_ref_38()
	call_c   Dyam_Create_Binary(I(9),&ref[38],I(0))
	move_ret ref[39]
	c_ret

;; TERM 36: '*GUARD_CLAUSE*'
c_code local build_ref_36
	ret_reg &ref[36]
	call_c   Dyam_Create_Atom("*GUARD_CLAUSE*")
	move_ret ref[36]
	c_ret

c_code local build_seed_2
	ret_reg &seed[2]
	call_c   build_ref_36()
	call_c   build_ref_43()
	call_c   Dyam_Seed_Start(&ref[36],&ref[43],I(0),fun23,1)
	call_c   build_ref_42()
	call_c   Dyam_Seed_Add_Comp(&ref[42],fun26,0)
	call_c   Dyam_Seed_End()
	move_ret seed[2]
	c_ret

;; TERM 42: '*GUARD*'(data_emit('$term'(_B, _C)))
c_code local build_ref_42
	ret_reg &ref[42]
	call_c   build_ref_7()
	call_c   build_ref_41()
	call_c   Dyam_Create_Unary(&ref[7],&ref[41])
	move_ret ref[42]
	c_ret

;; TERM 41: data_emit('$term'(_B, _C))
c_code local build_ref_41
	ret_reg &ref[41]
	call_c   build_ref_236()
	call_c   build_ref_44()
	call_c   Dyam_Create_Unary(&ref[236],&ref[44])
	move_ret ref[41]
	c_ret

;; TERM 44: '$term'(_B, _C)
c_code local build_ref_44
	ret_reg &ref[44]
	call_c   build_ref_238()
	call_c   Dyam_Create_Binary(&ref[238],V(1),V(2))
	move_ret ref[44]
	c_ret

;; TERM 238: '$term'
c_code local build_ref_238
	ret_reg &ref[238]
	call_c   Dyam_Create_Atom("$term")
	move_ret ref[238]
	c_ret

;; TERM 33: emitted_terms
c_code local build_ref_33
	ret_reg &ref[33]
	call_c   Dyam_Create_Atom("emitted_terms")
	move_ret ref[33]
	c_ret

long local pool_fun24[3]=[2,build_ref_33,build_ref_44]

pl_code local fun25
	call_c   Dyam_Remove_Choice()
fun24:
	move     &ref[33], R(0)
	move     0, R(1)
	move     V(3), R(2)
	move     S(5), R(3)
	pl_call  pred_update_counter_2()
	move     &ref[44], R(0)
	move     S(5), R(1)
	pl_call  pred_emit_term_1()
	move     &ref[44], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Deallocate(1)
	pl_jump  pred_clean_globals_1()


long local pool_fun26[3]=[65537,build_ref_42,pool_fun24]

pl_code local fun26
	call_c   Dyam_Pool(pool_fun26)
	call_c   Dyam_Unify_Item(&ref[42])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun25)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Reg_Load_Ptr(2,V(2))
	fail_ret
	move     I(0), R(4)
	move     0, R(5)
	call_c   DyALog_Mutable_Read(R(2),&R(4))
	fail_ret
	call_c   Dyam_Cut()
	pl_fail

;; TERM 43: '*GUARD*'(data_emit('$term'(_B, _C))) :> []
c_code local build_ref_43
	ret_reg &ref[43]
	call_c   build_ref_42()
	call_c   Dyam_Create_Binary(I(9),&ref[42],I(0))
	move_ret ref[43]
	c_ret

c_code local build_seed_0
	ret_reg &seed[0]
	call_c   build_ref_36()
	call_c   build_ref_51()
	call_c   Dyam_Seed_Start(&ref[36],&ref[51],I(0),fun23,1)
	call_c   build_ref_50()
	call_c   Dyam_Seed_Add_Comp(&ref[50],fun29,0)
	call_c   Dyam_Seed_End()
	move_ret seed[0]
	c_ret

;; TERM 50: '*GUARD*'(data_emit('$fun'(_B, _C, _D)))
c_code local build_ref_50
	ret_reg &ref[50]
	call_c   build_ref_7()
	call_c   build_ref_49()
	call_c   Dyam_Create_Unary(&ref[7],&ref[49])
	move_ret ref[50]
	c_ret

;; TERM 49: data_emit('$fun'(_B, _C, _D))
c_code local build_ref_49
	ret_reg &ref[49]
	call_c   build_ref_236()
	call_c   build_ref_52()
	call_c   Dyam_Create_Unary(&ref[236],&ref[52])
	move_ret ref[49]
	c_ret

;; TERM 52: '$fun'(_B, _C, _D)
c_code local build_ref_52
	ret_reg &ref[52]
	call_c   build_ref_239()
	call_c   Dyam_Term_Start(&ref[239],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[52]
	c_ret

;; TERM 239: '$fun'
c_code local build_ref_239
	ret_reg &ref[239]
	call_c   Dyam_Create_Atom("$fun")
	move_ret ref[239]
	c_ret

pl_code local fun28
	call_c   Dyam_Remove_Choice()
fun27:
	move     &ref[52], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Deallocate(1)
	pl_jump  pred_emit_function_1()


long local pool_fun29[3]=[2,build_ref_50,build_ref_52]

pl_code local fun29
	call_c   Dyam_Pool(pool_fun29)
	call_c   Dyam_Unify_Item(&ref[50])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun28)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Reg_Load_Ptr(2,V(3))
	fail_ret
	move     I(0), R(4)
	move     0, R(5)
	call_c   DyALog_Mutable_Read(R(2),&R(4))
	fail_ret
	call_c   Dyam_Cut()
	pl_fail

;; TERM 51: '*GUARD*'(data_emit('$fun'(_B, _C, _D))) :> []
c_code local build_ref_51
	ret_reg &ref[51]
	call_c   build_ref_50()
	call_c   Dyam_Create_Binary(I(9),&ref[50],I(0))
	move_ret ref[51]
	c_ret

c_code local build_seed_13
	ret_reg &seed[13]
	call_c   build_ref_36()
	call_c   build_ref_55()
	call_c   Dyam_Seed_Start(&ref[36],&ref[55],I(0),fun23,1)
	call_c   build_ref_54()
	call_c   Dyam_Seed_Add_Comp(&ref[54],fun30,0)
	call_c   Dyam_Seed_End()
	move_ret seed[13]
	c_ret

;; TERM 54: '*GUARD*'(emit_lterm('$charlist'(_B), [], _C, _D))
c_code local build_ref_54
	ret_reg &ref[54]
	call_c   build_ref_7()
	call_c   build_ref_53()
	call_c   Dyam_Create_Unary(&ref[7],&ref[53])
	move_ret ref[54]
	c_ret

;; TERM 53: emit_lterm('$charlist'(_B), [], _C, _D)
c_code local build_ref_53
	ret_reg &ref[53]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_241()
	call_c   Dyam_Create_Unary(&ref[241],V(1))
	move_ret R(0)
	call_c   build_ref_240()
	call_c   Dyam_Term_Start(&ref[240],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[53]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 240: emit_lterm
c_code local build_ref_240
	ret_reg &ref[240]
	call_c   Dyam_Create_Atom("emit_lterm")
	move_ret ref[240]
	c_ret

;; TERM 241: '$charlist'
c_code local build_ref_241
	ret_reg &ref[241]
	call_c   Dyam_Create_Atom("$charlist")
	move_ret ref[241]
	c_ret

;; TERM 56: 'Dyam_Create_Char_List'
c_code local build_ref_56
	ret_reg &ref[56]
	call_c   Dyam_Create_Atom("Dyam_Create_Char_List")
	move_ret ref[56]
	c_ret

;; TERM 57: ['$smb'(_B)]
c_code local build_ref_57
	ret_reg &ref[57]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_242()
	call_c   Dyam_Create_Unary(&ref[242],V(1))
	move_ret R(0)
	call_c   Dyam_Create_List(R(0),I(0))
	move_ret ref[57]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 242: '$smb'
c_code local build_ref_242
	ret_reg &ref[242]
	call_c   Dyam_Create_Atom("$smb")
	move_ret ref[242]
	c_ret

long local pool_fun30[4]=[3,build_ref_54,build_ref_56,build_ref_57]

pl_code local fun30
	call_c   Dyam_Pool(pool_fun30)
	call_c   Dyam_Unify_Item(&ref[54])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[56], R(0)
	move     0, R(1)
	move     &ref[57], R(2)
	move     S(5), R(3)
	pl_call  pred_ma_emit_call_c_2()
	call_c   Dyam_Reg_Load(0,V(2))
	move     V(4), R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_emit_lterm_post_2()

;; TERM 55: '*GUARD*'(emit_lterm('$charlist'(_B), [], _C, _D)) :> []
c_code local build_ref_55
	ret_reg &ref[55]
	call_c   build_ref_54()
	call_c   Dyam_Create_Binary(I(9),&ref[54],I(0))
	move_ret ref[55]
	c_ret

c_code local build_seed_16
	ret_reg &seed[16]
	call_c   build_ref_14()
	call_c   build_ref_68()
	call_c   Dyam_Seed_Start(&ref[14],&ref[68],I(0),fun19,1)
	call_c   build_ref_69()
	call_c   Dyam_Seed_Add_Comp(&ref[69],fun43,0)
	call_c   Dyam_Seed_End()
	move_ret seed[16]
	c_ret

;; TERM 69: '*CITEM*'(emit, _A)
c_code local build_ref_69
	ret_reg &ref[69]
	call_c   build_ref_18()
	call_c   build_ref_4()
	call_c   Dyam_Create_Binary(&ref[18],&ref[4],V(0))
	move_ret ref[69]
	c_ret

;; TERM 4: emit
c_code local build_ref_4
	ret_reg &ref[4]
	call_c   Dyam_Create_Atom("emit")
	move_ret ref[4]
	c_ret

;; TERM 72: term_handling(_F)
c_code local build_ref_72
	ret_reg &ref[72]
	call_c   build_ref_243()
	call_c   Dyam_Create_Unary(&ref[243],V(5))
	move_ret ref[72]
	c_ret

;; TERM 243: term_handling
c_code local build_ref_243
	ret_reg &ref[243]
	call_c   Dyam_Create_Atom("term_handling")
	move_ret ref[243]
	c_ret

c_code local build_seed_21
	ret_reg &seed[21]
	call_c   build_ref_0()
	call_c   build_ref_20()
	call_c   Dyam_Seed_Start(&ref[0],&ref[20],&ref[20],fun0,1)
	call_c   build_ref_21()
	call_c   Dyam_Seed_Add_Comp(&ref[21],&ref[20],0)
	call_c   Dyam_Seed_End()
	move_ret seed[21]
	c_ret

pl_code local fun39
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Object(R(0))
	call_c   Dyam_Tabule()
	call_c   Dyam_Schedule()
	pl_ret

pl_code local fun40
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Choice(fun39)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Subsume(R(0))
	fail_ret
	call_c   Dyam_Cut()
	pl_ret

long local pool_fun41[3]=[2,build_ref_72,build_seed_21]

pl_code local fun42
	call_c   Dyam_Remove_Choice()
fun41:
	call_c   Dyam_Reg_Load_Ptr(2,V(5))
	fail_ret
	move     N(0), R(4)
	move     0, R(5)
	call_c   DyALog_Mutable_Write(R(2),&R(4),0)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(5))
	fail_ret
	call_c   DYAM_evpred_assert_1(&ref[72])
	pl_call  pred_emit_header_0()
	pl_call  pred_emit_loading_0()
	pl_call  pred_emit_named_functions_0()
	pl_call  pred_emit_viewers_0()
	pl_call  pred_emit_propagated_directives_0()
	pl_call  pred_emit_init_pool_0()
	pl_call  pred_emit_initializer_0()
	call_c   DYAM_Set_Output_1(V(1))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_jump  fun40(&seed[21],2)


;; TERM 70: output_file(_C)
c_code local build_ref_70
	ret_reg &ref[70]
	call_c   build_ref_244()
	call_c   Dyam_Create_Unary(&ref[244],V(2))
	move_ret ref[70]
	c_ret

;; TERM 244: output_file
c_code local build_ref_244
	ret_reg &ref[244]
	call_c   Dyam_Create_Atom("output_file")
	move_ret ref[244]
	c_ret

;; TERM 71: write
c_code local build_ref_71
	ret_reg &ref[71]
	call_c   Dyam_Create_Atom("write")
	move_ret ref[71]
	c_ret

long local pool_fun43[6]=[131075,build_ref_69,build_ref_70,build_ref_71,pool_fun41,pool_fun41]

pl_code local fun43
	call_c   Dyam_Pool(pool_fun43)
	call_c   Dyam_Unify_Item(&ref[69])
	fail_ret
	call_c   DYAM_Current_Output_1(V(1))
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun42)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[70])
	call_c   Dyam_Cut()
	call_c   DYAM_Absolute_File_Name(V(2),V(3))
	fail_ret
	call_c   DYAM_Open_3(V(3),&ref[71],V(4))
	fail_ret
	call_c   DYAM_Set_Output_1(V(4))
	fail_ret
	pl_jump  fun41()

;; TERM 68: '*FIRST*'(emit) :> []
c_code local build_ref_68
	ret_reg &ref[68]
	call_c   build_ref_67()
	call_c   Dyam_Create_Binary(I(9),&ref[67],I(0))
	move_ret ref[68]
	c_ret

;; TERM 67: '*FIRST*'(emit)
c_code local build_ref_67
	ret_reg &ref[67]
	call_c   build_ref_14()
	call_c   build_ref_4()
	call_c   Dyam_Create_Unary(&ref[14],&ref[4])
	move_ret ref[67]
	c_ret

c_code local build_seed_4
	ret_reg &seed[4]
	call_c   build_ref_36()
	call_c   build_ref_75()
	call_c   Dyam_Seed_Start(&ref[36],&ref[75],I(0),fun23,1)
	call_c   build_ref_74()
	call_c   Dyam_Seed_Add_Comp(&ref[74],fun44,0)
	call_c   Dyam_Seed_End()
	move_ret seed[4]
	c_ret

;; TERM 74: '*GUARD*'(emit_lterm('$binary', _B, _C, _D))
c_code local build_ref_74
	ret_reg &ref[74]
	call_c   build_ref_7()
	call_c   build_ref_73()
	call_c   Dyam_Create_Unary(&ref[7],&ref[73])
	move_ret ref[74]
	c_ret

;; TERM 73: emit_lterm('$binary', _B, _C, _D)
c_code local build_ref_73
	ret_reg &ref[73]
	call_c   build_ref_240()
	call_c   build_ref_245()
	call_c   Dyam_Term_Start(&ref[240],4)
	call_c   Dyam_Term_Arg(&ref[245])
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[73]
	c_ret

;; TERM 245: '$binary'
c_code local build_ref_245
	ret_reg &ref[245]
	call_c   Dyam_Create_Atom("$binary")
	move_ret ref[245]
	c_ret

;; TERM 76: 'Dyam_Create_Binary'
c_code local build_ref_76
	ret_reg &ref[76]
	call_c   Dyam_Create_Atom("Dyam_Create_Binary")
	move_ret ref[76]
	c_ret

long local pool_fun44[3]=[2,build_ref_74,build_ref_76]

pl_code local fun44
	call_c   Dyam_Pool(pool_fun44)
	call_c   Dyam_Unify_Item(&ref[74])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(3))
	pl_call  pred_emit_globals_2()
	move     &ref[76], R(0)
	move     0, R(1)
	call_c   Dyam_Reg_Load(2,V(1))
	pl_call  pred_ma_emit_call_c_2()
	call_c   Dyam_Reg_Load(0,V(2))
	move     V(4), R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_emit_lterm_post_2()

;; TERM 75: '*GUARD*'(emit_lterm('$binary', _B, _C, _D)) :> []
c_code local build_ref_75
	ret_reg &ref[75]
	call_c   build_ref_74()
	call_c   Dyam_Create_Binary(I(9),&ref[74],I(0))
	move_ret ref[75]
	c_ret

c_code local build_seed_5
	ret_reg &seed[5]
	call_c   build_ref_36()
	call_c   build_ref_79()
	call_c   Dyam_Seed_Start(&ref[36],&ref[79],I(0),fun23,1)
	call_c   build_ref_78()
	call_c   Dyam_Seed_Add_Comp(&ref[78],fun45,0)
	call_c   Dyam_Seed_End()
	move_ret seed[5]
	c_ret

;; TERM 78: '*GUARD*'(emit_lterm('$unary', _B, _C, _D))
c_code local build_ref_78
	ret_reg &ref[78]
	call_c   build_ref_7()
	call_c   build_ref_77()
	call_c   Dyam_Create_Unary(&ref[7],&ref[77])
	move_ret ref[78]
	c_ret

;; TERM 77: emit_lterm('$unary', _B, _C, _D)
c_code local build_ref_77
	ret_reg &ref[77]
	call_c   build_ref_240()
	call_c   build_ref_246()
	call_c   Dyam_Term_Start(&ref[240],4)
	call_c   Dyam_Term_Arg(&ref[246])
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[77]
	c_ret

;; TERM 246: '$unary'
c_code local build_ref_246
	ret_reg &ref[246]
	call_c   Dyam_Create_Atom("$unary")
	move_ret ref[246]
	c_ret

;; TERM 80: 'Dyam_Create_Unary'
c_code local build_ref_80
	ret_reg &ref[80]
	call_c   Dyam_Create_Atom("Dyam_Create_Unary")
	move_ret ref[80]
	c_ret

long local pool_fun45[3]=[2,build_ref_78,build_ref_80]

pl_code local fun45
	call_c   Dyam_Pool(pool_fun45)
	call_c   Dyam_Unify_Item(&ref[78])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(3))
	pl_call  pred_emit_globals_2()
	move     &ref[80], R(0)
	move     0, R(1)
	call_c   Dyam_Reg_Load(2,V(1))
	pl_call  pred_ma_emit_call_c_2()
	call_c   Dyam_Reg_Load(0,V(2))
	move     V(4), R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_emit_lterm_post_2()

;; TERM 79: '*GUARD*'(emit_lterm('$unary', _B, _C, _D)) :> []
c_code local build_ref_79
	ret_reg &ref[79]
	call_c   build_ref_78()
	call_c   Dyam_Create_Binary(I(9),&ref[78],I(0))
	move_ret ref[79]
	c_ret

c_code local build_seed_10
	ret_reg &seed[10]
	call_c   build_ref_36()
	call_c   build_ref_83()
	call_c   Dyam_Seed_Start(&ref[36],&ref[83],I(0),fun23,1)
	call_c   build_ref_82()
	call_c   Dyam_Seed_Add_Comp(&ref[82],fun46,0)
	call_c   Dyam_Seed_End()
	move_ret seed[10]
	c_ret

;; TERM 82: '*GUARD*'(emit_lterm('$list', _B, _C, _D))
c_code local build_ref_82
	ret_reg &ref[82]
	call_c   build_ref_7()
	call_c   build_ref_81()
	call_c   Dyam_Create_Unary(&ref[7],&ref[81])
	move_ret ref[82]
	c_ret

;; TERM 81: emit_lterm('$list', _B, _C, _D)
c_code local build_ref_81
	ret_reg &ref[81]
	call_c   build_ref_240()
	call_c   build_ref_247()
	call_c   Dyam_Term_Start(&ref[240],4)
	call_c   Dyam_Term_Arg(&ref[247])
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[81]
	c_ret

;; TERM 247: '$list'
c_code local build_ref_247
	ret_reg &ref[247]
	call_c   Dyam_Create_Atom("$list")
	move_ret ref[247]
	c_ret

;; TERM 84: 'Dyam_Create_List'
c_code local build_ref_84
	ret_reg &ref[84]
	call_c   Dyam_Create_Atom("Dyam_Create_List")
	move_ret ref[84]
	c_ret

long local pool_fun46[3]=[2,build_ref_82,build_ref_84]

pl_code local fun46
	call_c   Dyam_Pool(pool_fun46)
	call_c   Dyam_Unify_Item(&ref[82])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(3))
	pl_call  pred_emit_globals_2()
	move     &ref[84], R(0)
	move     0, R(1)
	call_c   Dyam_Reg_Load(2,V(1))
	pl_call  pred_ma_emit_call_c_2()
	call_c   Dyam_Reg_Load(0,V(2))
	move     V(4), R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_emit_lterm_post_2()

;; TERM 83: '*GUARD*'(emit_lterm('$list', _B, _C, _D)) :> []
c_code local build_ref_83
	ret_reg &ref[83]
	call_c   build_ref_82()
	call_c   Dyam_Create_Binary(I(9),&ref[82],I(0))
	move_ret ref[83]
	c_ret

c_code local build_seed_8
	ret_reg &seed[8]
	call_c   build_ref_36()
	call_c   build_ref_122()
	call_c   Dyam_Seed_Start(&ref[36],&ref[122],I(0),fun23,1)
	call_c   build_ref_121()
	call_c   Dyam_Seed_Add_Comp(&ref[121],fun73,0)
	call_c   Dyam_Seed_End()
	move_ret seed[8]
	c_ret

;; TERM 121: '*GUARD*'(emit_lterm('$alt_tupple'(_B, _C), [_D], _E, _F))
c_code local build_ref_121
	ret_reg &ref[121]
	call_c   build_ref_7()
	call_c   build_ref_120()
	call_c   Dyam_Create_Unary(&ref[7],&ref[120])
	move_ret ref[121]
	c_ret

;; TERM 120: emit_lterm('$alt_tupple'(_B, _C), [_D], _E, _F)
c_code local build_ref_120
	ret_reg &ref[120]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_248()
	call_c   Dyam_Create_Binary(&ref[248],V(1),V(2))
	move_ret R(0)
	call_c   Dyam_Create_List(V(3),I(0))
	move_ret R(1)
	call_c   build_ref_240()
	call_c   Dyam_Term_Start(&ref[240],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[120]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 248: '$alt_tupple'
c_code local build_ref_248
	ret_reg &ref[248]
	call_c   Dyam_Create_Atom("$alt_tupple")
	move_ret ref[248]
	c_ret

;; TERM 123: 'Dyam_Create_Alt_Tupple'
c_code local build_ref_123
	ret_reg &ref[123]
	call_c   Dyam_Create_Atom("Dyam_Create_Alt_Tupple")
	move_ret ref[123]
	c_ret

;; TERM 124: [c(_B),c(_C),_D]
c_code local build_ref_124
	ret_reg &ref[124]
	call_c   Dyam_Pseudo_Choice(3)
	call_c   build_ref_249()
	call_c   Dyam_Create_Unary(&ref[249],V(1))
	move_ret R(0)
	call_c   Dyam_Create_Unary(&ref[249],V(2))
	move_ret R(1)
	call_c   Dyam_Create_List(V(3),I(0))
	move_ret R(2)
	call_c   Dyam_Create_List(R(1),R(2))
	move_ret R(2)
	call_c   Dyam_Create_List(R(0),R(2))
	move_ret ref[124]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 249: c
c_code local build_ref_249
	ret_reg &ref[249]
	call_c   Dyam_Create_Atom("c")
	move_ret ref[249]
	c_ret

long local pool_fun73[4]=[3,build_ref_121,build_ref_123,build_ref_124]

pl_code local fun73
	call_c   Dyam_Pool(pool_fun73)
	call_c   Dyam_Unify_Item(&ref[121])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(3))
	call_c   Dyam_Reg_Load(2,V(5))
	pl_call  pred_emit_globals_2()
	move     &ref[123], R(0)
	move     0, R(1)
	move     &ref[124], R(2)
	move     S(5), R(3)
	pl_call  pred_ma_emit_call_c_2()
	call_c   Dyam_Reg_Load(0,V(4))
	move     V(6), R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_emit_lterm_post_2()

;; TERM 122: '*GUARD*'(emit_lterm('$alt_tupple'(_B, _C), [_D], _E, _F)) :> []
c_code local build_ref_122
	ret_reg &ref[122]
	call_c   build_ref_121()
	call_c   Dyam_Create_Binary(I(9),&ref[121],I(0))
	move_ret ref[122]
	c_ret

c_code local build_seed_9
	ret_reg &seed[9]
	call_c   build_ref_36()
	call_c   build_ref_127()
	call_c   Dyam_Seed_Start(&ref[36],&ref[127],I(0),fun23,1)
	call_c   build_ref_126()
	call_c   Dyam_Seed_Add_Comp(&ref[126],fun74,0)
	call_c   Dyam_Seed_End()
	move_ret seed[9]
	c_ret

;; TERM 126: '*GUARD*'(emit_lterm('$tupple'(_B, _C), [_D], _E, _F))
c_code local build_ref_126
	ret_reg &ref[126]
	call_c   build_ref_7()
	call_c   build_ref_125()
	call_c   Dyam_Create_Unary(&ref[7],&ref[125])
	move_ret ref[126]
	c_ret

;; TERM 125: emit_lterm('$tupple'(_B, _C), [_D], _E, _F)
c_code local build_ref_125
	ret_reg &ref[125]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_250()
	call_c   Dyam_Create_Binary(&ref[250],V(1),V(2))
	move_ret R(0)
	call_c   Dyam_Create_List(V(3),I(0))
	move_ret R(1)
	call_c   build_ref_240()
	call_c   Dyam_Term_Start(&ref[240],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[125]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 250: '$tupple'
c_code local build_ref_250
	ret_reg &ref[250]
	call_c   Dyam_Create_Atom("$tupple")
	move_ret ref[250]
	c_ret

;; TERM 128: 'Dyam_Create_Tupple'
c_code local build_ref_128
	ret_reg &ref[128]
	call_c   Dyam_Create_Atom("Dyam_Create_Tupple")
	move_ret ref[128]
	c_ret

long local pool_fun74[4]=[3,build_ref_126,build_ref_128,build_ref_124]

pl_code local fun74
	call_c   Dyam_Pool(pool_fun74)
	call_c   Dyam_Unify_Item(&ref[126])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(3))
	call_c   Dyam_Reg_Load(2,V(5))
	pl_call  pred_emit_globals_2()
	move     &ref[128], R(0)
	move     0, R(1)
	move     &ref[124], R(2)
	move     S(5), R(3)
	pl_call  pred_ma_emit_call_c_2()
	call_c   Dyam_Reg_Load(0,V(4))
	move     V(6), R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_emit_lterm_post_2()

;; TERM 127: '*GUARD*'(emit_lterm('$tupple'(_B, _C), [_D], _E, _F)) :> []
c_code local build_ref_127
	ret_reg &ref[127]
	call_c   build_ref_126()
	call_c   Dyam_Create_Binary(I(9),&ref[126],I(0))
	move_ret ref[127]
	c_ret

c_code local build_seed_14
	ret_reg &seed[14]
	call_c   build_ref_36()
	call_c   build_ref_131()
	call_c   Dyam_Seed_Start(&ref[36],&ref[131],I(0),fun23,1)
	call_c   build_ref_130()
	call_c   Dyam_Seed_Add_Comp(&ref[130],fun75,0)
	call_c   Dyam_Seed_End()
	move_ret seed[14]
	c_ret

;; TERM 130: '*GUARD*'(emit_lterm('$CLOSURE'(_B), [_C], _D, _E))
c_code local build_ref_130
	ret_reg &ref[130]
	call_c   build_ref_7()
	call_c   build_ref_129()
	call_c   Dyam_Create_Unary(&ref[7],&ref[129])
	move_ret ref[130]
	c_ret

;; TERM 129: emit_lterm('$CLOSURE'(_B), [_C], _D, _E)
c_code local build_ref_129
	ret_reg &ref[129]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_91()
	call_c   Dyam_Create_Unary(&ref[91],V(1))
	move_ret R(0)
	call_c   build_ref_240()
	call_c   build_ref_111()
	call_c   Dyam_Term_Start(&ref[240],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[111])
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[129]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 111: [_C]
c_code local build_ref_111
	ret_reg &ref[111]
	call_c   Dyam_Create_List(V(2),I(0))
	move_ret ref[111]
	c_ret

;; TERM 91: '$CLOSURE'
c_code local build_ref_91
	ret_reg &ref[91]
	call_c   Dyam_Create_Atom("$CLOSURE")
	move_ret ref[91]
	c_ret

;; TERM 132: 'Dyam_Closure_Aux'
c_code local build_ref_132
	ret_reg &ref[132]
	call_c   Dyam_Create_Atom("Dyam_Closure_Aux")
	move_ret ref[132]
	c_ret

;; TERM 90: [_B,_C]
c_code local build_ref_90
	ret_reg &ref[90]
	call_c   Dyam_Create_Tupple(1,2,I(0))
	move_ret ref[90]
	c_ret

long local pool_fun75[4]=[3,build_ref_130,build_ref_132,build_ref_90]

pl_code local fun75
	call_c   Dyam_Pool(pool_fun75)
	call_c   Dyam_Unify_Item(&ref[130])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(2))
	call_c   Dyam_Reg_Load(2,V(4))
	pl_call  pred_emit_globals_2()
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(4))
	pl_call  pred_emit_globals_2()
	move     &ref[132], R(0)
	move     0, R(1)
	move     &ref[90], R(2)
	move     S(5), R(3)
	pl_call  pred_ma_emit_call_c_2()
	call_c   Dyam_Reg_Load(0,V(3))
	move     V(5), R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_emit_lterm_post_2()

;; TERM 131: '*GUARD*'(emit_lterm('$CLOSURE'(_B), [_C], _D, _E)) :> []
c_code local build_ref_131
	ret_reg &ref[131]
	call_c   build_ref_130()
	call_c   Dyam_Create_Binary(I(9),&ref[130],I(0))
	move_ret ref[131]
	c_ret

c_code local build_seed_7
	ret_reg &seed[7]
	call_c   build_ref_36()
	call_c   build_ref_135()
	call_c   Dyam_Seed_Start(&ref[36],&ref[135],I(0),fun23,1)
	call_c   build_ref_134()
	call_c   Dyam_Seed_Add_Comp(&ref[134],fun78,0)
	call_c   Dyam_Seed_End()
	move_ret seed[7]
	c_ret

;; TERM 134: '*GUARD*'(emit_lterm('$xtupple'([_B,_C|_D]), [], _E, _F))
c_code local build_ref_134
	ret_reg &ref[134]
	call_c   build_ref_7()
	call_c   build_ref_133()
	call_c   Dyam_Create_Unary(&ref[7],&ref[133])
	move_ret ref[134]
	c_ret

;; TERM 133: emit_lterm('$xtupple'([_B,_C|_D]), [], _E, _F)
c_code local build_ref_133
	ret_reg &ref[133]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Tupple(1,2,V(3))
	move_ret R(0)
	call_c   build_ref_251()
	call_c   Dyam_Create_Unary(&ref[251],R(0))
	move_ret R(0)
	call_c   build_ref_240()
	call_c   Dyam_Term_Start(&ref[240],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[133]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 251: '$xtupple'
c_code local build_ref_251
	ret_reg &ref[251]
	call_c   Dyam_Create_Atom("$xtupple")
	move_ret ref[251]
	c_ret

;; TERM 138: 'Dyam_Start_Tupple'
c_code local build_ref_138
	ret_reg &ref[138]
	call_c   Dyam_Create_Atom("Dyam_Start_Tupple")
	move_ret ref[138]
	c_ret

;; TERM 137: [c(_B),c(_C)]
c_code local build_ref_137
	ret_reg &ref[137]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_249()
	call_c   Dyam_Create_Unary(&ref[249],V(1))
	move_ret R(0)
	call_c   Dyam_Create_Unary(&ref[249],V(2))
	move_ret R(1)
	call_c   Dyam_Create_List(R(1),I(0))
	move_ret R(1)
	call_c   Dyam_Create_List(R(0),R(1))
	move_ret ref[137]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun77[3]=[2,build_ref_138,build_ref_137]

pl_code local fun77
	call_c   Dyam_Remove_Choice()
	move     &ref[138], R(0)
	move     0, R(1)
	move     &ref[137], R(2)
	move     S(5), R(3)
	pl_call  pred_ma_emit_call_c_2()
	call_c   Dyam_Reg_Load(0,V(3))
	pl_call  pred_emit_xtupple_1()
fun76:
	call_c   Dyam_Reg_Load(0,V(4))
	move     V(6), R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_emit_lterm_post_2()


;; TERM 136: 'Dyam_Create_Simple_Tupple'
c_code local build_ref_136
	ret_reg &ref[136]
	call_c   Dyam_Create_Atom("Dyam_Create_Simple_Tupple")
	move_ret ref[136]
	c_ret

long local pool_fun78[5]=[65539,build_ref_134,build_ref_136,build_ref_137,pool_fun77]

pl_code local fun78
	call_c   Dyam_Pool(pool_fun78)
	call_c   Dyam_Unify_Item(&ref[134])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun77)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(3),I(0))
	fail_ret
	call_c   Dyam_Cut()
	move     &ref[136], R(0)
	move     0, R(1)
	move     &ref[137], R(2)
	move     S(5), R(3)
	pl_call  pred_ma_emit_call_c_2()
	pl_jump  fun76()

;; TERM 135: '*GUARD*'(emit_lterm('$xtupple'([_B,_C|_D]), [], _E, _F)) :> []
c_code local build_ref_135
	ret_reg &ref[135]
	call_c   build_ref_134()
	call_c   Dyam_Create_Binary(I(9),&ref[134],I(0))
	move_ret ref[135]
	c_ret

c_code local build_seed_12
	ret_reg &seed[12]
	call_c   build_ref_36()
	call_c   build_ref_148()
	call_c   Dyam_Seed_Start(&ref[36],&ref[148],I(0),fun23,1)
	call_c   build_ref_147()
	call_c   Dyam_Seed_Add_Comp(&ref[147],fun83,0)
	call_c   Dyam_Seed_End()
	move_ret seed[12]
	c_ret

;; TERM 147: '*GUARD*'(emit_lterm('$smb'(_B), [], _C, _D))
c_code local build_ref_147
	ret_reg &ref[147]
	call_c   build_ref_7()
	call_c   build_ref_146()
	call_c   Dyam_Create_Unary(&ref[7],&ref[146])
	move_ret ref[147]
	c_ret

;; TERM 146: emit_lterm('$smb'(_B), [], _C, _D)
c_code local build_ref_146
	ret_reg &ref[146]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_242()
	call_c   Dyam_Create_Unary(&ref[242],V(1))
	move_ret R(0)
	call_c   build_ref_240()
	call_c   Dyam_Term_Start(&ref[240],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[146]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 149: 'Dyam_Create_Atom'
c_code local build_ref_149
	ret_reg &ref[149]
	call_c   Dyam_Create_Atom("Dyam_Create_Atom")
	move_ret ref[149]
	c_ret

pl_code local fun82
	call_c   Dyam_Remove_Choice()
fun81:
	call_c   Dyam_Deallocate()
	pl_ret


;; TERM 150: deref_functor((_B / _F))
c_code local build_ref_150
	ret_reg &ref[150]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_253()
	call_c   Dyam_Create_Binary(&ref[253],V(1),V(5))
	move_ret R(0)
	call_c   build_ref_252()
	call_c   Dyam_Create_Unary(&ref[252],R(0))
	move_ret ref[150]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 252: deref_functor
c_code local build_ref_252
	ret_reg &ref[252]
	call_c   Dyam_Create_Atom("deref_functor")
	move_ret ref[252]
	c_ret

;; TERM 253: /
c_code local build_ref_253
	ret_reg &ref[253]
	call_c   Dyam_Create_Atom("/")
	move_ret ref[253]
	c_ret

;; TERM 151: ';; deref functor ~w/~w\n'
c_code local build_ref_151
	ret_reg &ref[151]
	call_c   Dyam_Create_Atom(";; deref functor ~w/~w\n")
	move_ret ref[151]
	c_ret

;; TERM 152: [_B,_F]
c_code local build_ref_152
	ret_reg &ref[152]
	call_c   build_ref_100()
	call_c   Dyam_Create_List(V(1),&ref[100])
	move_ret ref[152]
	c_ret

;; TERM 100: [_F]
c_code local build_ref_100
	ret_reg &ref[100]
	call_c   Dyam_Create_List(V(5),I(0))
	move_ret ref[100]
	c_ret

;; TERM 153: 'Dyam_Set_Deref_Functor'
c_code local build_ref_153
	ret_reg &ref[153]
	call_c   Dyam_Create_Atom("Dyam_Set_Deref_Functor")
	move_ret ref[153]
	c_ret

;; TERM 154: [_E,_F]
c_code local build_ref_154
	ret_reg &ref[154]
	call_c   Dyam_Create_Tupple(4,5,I(0))
	move_ret ref[154]
	c_ret

long local pool_fun83[9]=[8,build_ref_147,build_ref_149,build_ref_57,build_ref_150,build_ref_151,build_ref_152,build_ref_153,build_ref_154]

pl_code local fun83
	call_c   Dyam_Pool(pool_fun83)
	call_c   Dyam_Unify_Item(&ref[147])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[149], R(0)
	move     0, R(1)
	move     &ref[57], R(2)
	move     S(5), R(3)
	pl_call  pred_ma_emit_call_c_2()
	call_c   Dyam_Reg_Load(0,V(2))
	move     V(4), R(2)
	move     S(5), R(3)
	pl_call  pred_emit_lterm_post_2()
	call_c   Dyam_Choice(fun82)
	pl_call  Object_1(&ref[150])
	move     &ref[151], R(0)
	move     0, R(1)
	move     &ref[152], R(2)
	move     S(5), R(3)
	pl_call  pred_format_2()
	move     &ref[153], R(0)
	move     0, R(1)
	move     &ref[154], R(2)
	move     S(5), R(3)
	pl_call  pred_ma_emit_call_c_2()
	pl_fail

;; TERM 148: '*GUARD*'(emit_lterm('$smb'(_B), [], _C, _D)) :> []
c_code local build_ref_148
	ret_reg &ref[148]
	call_c   build_ref_147()
	call_c   Dyam_Create_Binary(I(9),&ref[147],I(0))
	move_ret ref[148]
	c_ret

c_code local build_seed_11
	ret_reg &seed[11]
	call_c   build_ref_36()
	call_c   build_ref_169()
	call_c   Dyam_Seed_Start(&ref[36],&ref[169],I(0),fun23,1)
	call_c   build_ref_168()
	call_c   Dyam_Seed_Add_Comp(&ref[168],fun91,0)
	call_c   Dyam_Seed_End()
	move_ret seed[11]
	c_ret

;; TERM 168: '*GUARD*'(emit_lterm('$smb'(_B), [_C|_D], _E, _F))
c_code local build_ref_168
	ret_reg &ref[168]
	call_c   build_ref_7()
	call_c   build_ref_167()
	call_c   Dyam_Create_Unary(&ref[7],&ref[167])
	move_ret ref[168]
	c_ret

;; TERM 167: emit_lterm('$smb'(_B), [_C|_D], _E, _F)
c_code local build_ref_167
	ret_reg &ref[167]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_242()
	call_c   Dyam_Create_Unary(&ref[242],V(1))
	move_ret R(0)
	call_c   build_ref_240()
	call_c   build_ref_170()
	call_c   Dyam_Term_Start(&ref[240],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[170])
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[167]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 170: [_C|_D]
c_code local build_ref_170
	ret_reg &ref[170]
	call_c   Dyam_Create_List(V(2),V(3))
	move_ret ref[170]
	c_ret

;; TERM 171: 'Dyam_Create_Atom_Module'
c_code local build_ref_171
	ret_reg &ref[171]
	call_c   Dyam_Create_Atom("Dyam_Create_Atom_Module")
	move_ret ref[171]
	c_ret

;; TERM 172: ['$smb'(_B),_C]
c_code local build_ref_172
	ret_reg &ref[172]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_242()
	call_c   Dyam_Create_Unary(&ref[242],V(1))
	move_ret R(0)
	call_c   build_ref_111()
	call_c   Dyam_Create_List(R(0),&ref[111])
	move_ret ref[172]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 175: deref_functor((_B / _I))
c_code local build_ref_175
	ret_reg &ref[175]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_253()
	call_c   Dyam_Create_Binary(&ref[253],V(1),V(8))
	move_ret R(0)
	call_c   build_ref_252()
	call_c   Dyam_Create_Unary(&ref[252],R(0))
	move_ret ref[175]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 176: [_B,_I]
c_code local build_ref_176
	ret_reg &ref[176]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(8),I(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(1),R(0))
	move_ret ref[176]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 177: [_G,_I]
c_code local build_ref_177
	ret_reg &ref[177]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(8),I(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(6),R(0))
	move_ret ref[177]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun89[6]=[5,build_ref_175,build_ref_151,build_ref_176,build_ref_153,build_ref_177]

pl_code local fun90
	call_c   Dyam_Remove_Choice()
fun89:
	call_c   Dyam_Choice(fun82)
	pl_call  Object_1(&ref[175])
	move     &ref[151], R(0)
	move     0, R(1)
	move     &ref[176], R(2)
	move     S(5), R(3)
	pl_call  pred_format_2()
	move     &ref[153], R(0)
	move     0, R(1)
	move     &ref[177], R(2)
	move     S(5), R(3)
	pl_call  pred_ma_emit_call_c_2()
	pl_fail


;; TERM 173: [_H]
c_code local build_ref_173
	ret_reg &ref[173]
	call_c   Dyam_Create_List(V(7),I(0))
	move_ret ref[173]
	c_ret

;; TERM 163: 'DYAM_Feature_2'
c_code local build_ref_163
	ret_reg &ref[163]
	call_c   Dyam_Create_Atom("DYAM_Feature_2")
	move_ret ref[163]
	c_ret

;; TERM 174: [_C,_H]
c_code local build_ref_174
	ret_reg &ref[174]
	call_c   build_ref_173()
	call_c   Dyam_Create_List(V(2),&ref[173])
	move_ret ref[174]
	c_ret

long local pool_fun91[10]=[131079,build_ref_168,build_ref_170,build_ref_171,build_ref_172,build_ref_173,build_ref_163,build_ref_174,pool_fun89,pool_fun89]

pl_code local fun91
	call_c   Dyam_Pool(pool_fun91)
	call_c   Dyam_Unify_Item(&ref[168])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[170], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(5))
	pl_call  pred_emit_globals_2()
	move     &ref[171], R(0)
	move     0, R(1)
	move     &ref[172], R(2)
	move     S(5), R(3)
	pl_call  pred_ma_emit_call_c_2()
	call_c   Dyam_Reg_Load(0,V(4))
	move     V(6), R(2)
	move     S(5), R(3)
	pl_call  pred_emit_lterm_post_2()
	call_c   Dyam_Choice(fun90)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(3),&ref[173])
	fail_ret
	call_c   Dyam_Cut()
	move     &ref[163], R(0)
	move     0, R(1)
	move     &ref[174], R(2)
	move     S(5), R(3)
	pl_call  pred_ma_emit_call_c_2()
	pl_jump  fun89()

;; TERM 169: '*GUARD*'(emit_lterm('$smb'(_B), [_C|_D], _E, _F)) :> []
c_code local build_ref_169
	ret_reg &ref[169]
	call_c   build_ref_168()
	call_c   Dyam_Create_Binary(I(9),&ref[168],I(0))
	move_ret ref[169]
	c_ret

c_code local build_seed_3
	ret_reg &seed[3]
	call_c   build_ref_36()
	call_c   build_ref_180()
	call_c   Dyam_Seed_Start(&ref[36],&ref[180],I(0),fun23,1)
	call_c   build_ref_179()
	call_c   Dyam_Seed_Add_Comp(&ref[179],fun93,0)
	call_c   Dyam_Seed_End()
	move_ret seed[3]
	c_ret

;; TERM 179: '*GUARD*'(emit_lterm('$term'(_B), [_C|_D], _E, _F))
c_code local build_ref_179
	ret_reg &ref[179]
	call_c   build_ref_7()
	call_c   build_ref_178()
	call_c   Dyam_Create_Unary(&ref[7],&ref[178])
	move_ret ref[179]
	c_ret

;; TERM 178: emit_lterm('$term'(_B), [_C|_D], _E, _F)
c_code local build_ref_178
	ret_reg &ref[178]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_238()
	call_c   Dyam_Create_Unary(&ref[238],V(1))
	move_ret R(0)
	call_c   build_ref_240()
	call_c   build_ref_170()
	call_c   Dyam_Term_Start(&ref[240],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(&ref[170])
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[178]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 181: 'Dyam_Term_Start'
c_code local build_ref_181
	ret_reg &ref[181]
	call_c   Dyam_Create_Atom("Dyam_Term_Start")
	move_ret ref[181]
	c_ret

;; TERM 182: [_C,c(_B)]
c_code local build_ref_182
	ret_reg &ref[182]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_249()
	call_c   Dyam_Create_Unary(&ref[249],V(1))
	move_ret R(0)
	call_c   Dyam_Create_List(R(0),I(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(2),R(0))
	move_ret ref[182]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 185: 'Dyam_Term_End'
c_code local build_ref_185
	ret_reg &ref[185]
	call_c   Dyam_Create_Atom("Dyam_Term_End")
	move_ret ref[185]
	c_ret

long local pool_fun92[2]=[1,build_ref_185]

pl_code local fun92
	call_c   Dyam_Remove_Choice()
	move     &ref[185], R(0)
	move     0, R(1)
	move     I(0), R(2)
	move     0, R(3)
	pl_call  pred_ma_emit_call_c_2()
	call_c   Dyam_Reg_Load(0,V(4))
	move     V(7), R(2)
	move     S(5), R(3)
	pl_call  pred_emit_lterm_post_2()
	pl_jump  fun81()

;; TERM 183: 'Dyam_Term_Arg'
c_code local build_ref_183
	ret_reg &ref[183]
	call_c   Dyam_Create_Atom("Dyam_Term_Arg")
	move_ret ref[183]
	c_ret

;; TERM 184: [_G]
c_code local build_ref_184
	ret_reg &ref[184]
	call_c   Dyam_Create_List(V(6),I(0))
	move_ret ref[184]
	c_ret

long local pool_fun93[8]=[65542,build_ref_179,build_ref_170,build_ref_181,build_ref_182,build_ref_183,build_ref_184,pool_fun92]

pl_code local fun93
	call_c   Dyam_Pool(pool_fun93)
	call_c   Dyam_Unify_Item(&ref[179])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[170], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(5))
	pl_call  pred_emit_globals_2()
	move     &ref[181], R(0)
	move     0, R(1)
	move     &ref[182], R(2)
	move     S(5), R(3)
	pl_call  pred_ma_emit_call_c_2()
	call_c   Dyam_Choice(fun92)
	pl_call  Domain_2(V(6),V(3))
	move     &ref[183], R(0)
	move     0, R(1)
	move     &ref[184], R(2)
	move     S(5), R(3)
	pl_call  pred_ma_emit_call_c_2()
	pl_fail

;; TERM 180: '*GUARD*'(emit_lterm('$term'(_B), [_C|_D], _E, _F)) :> []
c_code local build_ref_180
	ret_reg &ref[180]
	call_c   build_ref_179()
	call_c   Dyam_Create_Binary(I(9),&ref[179],I(0))
	move_ret ref[180]
	c_ret

c_code local build_seed_6
	ret_reg &seed[6]
	call_c   build_ref_36()
	call_c   build_ref_188()
	call_c   Dyam_Seed_Start(&ref[36],&ref[188],I(0),fun23,1)
	call_c   build_ref_187()
	call_c   Dyam_Seed_Add_Comp(&ref[187],fun96,0)
	call_c   Dyam_Seed_End()
	move_ret seed[6]
	c_ret

;; TERM 187: '*GUARD*'(emit_lterm('$fun_tupple'([_B,_C|_D]), _E, _F, _G))
c_code local build_ref_187
	ret_reg &ref[187]
	call_c   build_ref_7()
	call_c   build_ref_186()
	call_c   Dyam_Create_Unary(&ref[7],&ref[186])
	move_ret ref[187]
	c_ret

;; TERM 186: emit_lterm('$fun_tupple'([_B,_C|_D]), _E, _F, _G)
c_code local build_ref_186
	ret_reg &ref[186]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Tupple(1,2,V(3))
	move_ret R(0)
	call_c   build_ref_254()
	call_c   Dyam_Create_Unary(&ref[254],R(0))
	move_ret R(0)
	call_c   build_ref_240()
	call_c   Dyam_Term_Start(&ref[240],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[186]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 254: '$fun_tupple'
c_code local build_ref_254
	ret_reg &ref[254]
	call_c   Dyam_Create_Atom("$fun_tupple")
	move_ret ref[254]
	c_ret

;; TERM 191: 'Dyam_Start_Fun_With_Tupple'
c_code local build_ref_191
	ret_reg &ref[191]
	call_c   Dyam_Create_Atom("Dyam_Start_Fun_With_Tupple")
	move_ret ref[191]
	c_ret

long local pool_fun95[3]=[2,build_ref_191,build_ref_137]

pl_code local fun95
	call_c   Dyam_Remove_Choice()
	move     &ref[191], R(0)
	move     0, R(1)
	move     &ref[137], R(2)
	move     S(5), R(3)
	pl_call  pred_ma_emit_call_c_2()
	call_c   Dyam_Reg_Load(0,V(3))
	call_c   Dyam_Reg_Load(2,V(4))
	pl_call  pred_emit_fun_tupple_2()
fun94:
	call_c   Dyam_Reg_Load(0,V(5))
	move     V(7), R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_emit_lterm_post_2()


;; TERM 189: 'Dyam_Create_Fun_With_Tupple'
c_code local build_ref_189
	ret_reg &ref[189]
	call_c   Dyam_Create_Atom("Dyam_Create_Fun_With_Tupple")
	move_ret ref[189]
	c_ret

;; TERM 190: [_E,c(_B),c(_C)]
c_code local build_ref_190
	ret_reg &ref[190]
	call_c   build_ref_137()
	call_c   Dyam_Create_List(V(4),&ref[137])
	move_ret ref[190]
	c_ret

long local pool_fun96[5]=[65539,build_ref_187,build_ref_189,build_ref_190,pool_fun95]

pl_code local fun96
	call_c   Dyam_Pool(pool_fun96)
	call_c   Dyam_Unify_Item(&ref[187])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(4))
	call_c   Dyam_Reg_Load(2,V(6))
	pl_call  pred_emit_globals_2()
	call_c   Dyam_Choice(fun95)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(3),I(0))
	fail_ret
	call_c   Dyam_Cut()
	move     &ref[189], R(0)
	move     0, R(1)
	move     &ref[190], R(2)
	move     S(5), R(3)
	pl_call  pred_ma_emit_call_c_2()
	pl_jump  fun94()

;; TERM 188: '*GUARD*'(emit_lterm('$fun_tupple'([_B,_C|_D]), _E, _F, _G)) :> []
c_code local build_ref_188
	ret_reg &ref[188]
	call_c   build_ref_187()
	call_c   Dyam_Create_Binary(I(9),&ref[187],I(0))
	move_ret ref[188]
	c_ret

c_code local build_seed_23
	ret_reg &seed[23]
	call_c   build_ref_7()
	call_c   build_ref_220()
	call_c   Dyam_Seed_Start(&ref[7],&ref[220],I(0),fun0,1)
	call_c   build_ref_221()
	call_c   Dyam_Seed_Add_Comp(&ref[221],&ref[220],0)
	call_c   Dyam_Seed_End()
	move_ret seed[23]
	c_ret

;; TERM 221: '*GUARD*'(emit_lterm(_B1, _N, _K, '$term'(_B, _C))) :> '$$HOLE$$'
c_code local build_ref_221
	ret_reg &ref[221]
	call_c   build_ref_220()
	call_c   Dyam_Create_Binary(I(9),&ref[220],I(7))
	move_ret ref[221]
	c_ret

;; TERM 220: '*GUARD*'(emit_lterm(_B1, _N, _K, '$term'(_B, _C)))
c_code local build_ref_220
	ret_reg &ref[220]
	call_c   build_ref_7()
	call_c   build_ref_219()
	call_c   Dyam_Create_Unary(&ref[7],&ref[219])
	move_ret ref[220]
	c_ret

;; TERM 219: emit_lterm(_B1, _N, _K, '$term'(_B, _C))
c_code local build_ref_219
	ret_reg &ref[219]
	call_c   build_ref_240()
	call_c   build_ref_44()
	call_c   Dyam_Term_Start(&ref[240],4)
	call_c   Dyam_Term_Arg(V(27))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(&ref[44])
	call_c   Dyam_Term_End()
	move_ret ref[219]
	c_ret

c_code local build_seed_20
	ret_reg &seed[20]
	call_c   build_ref_18()
	call_c   build_ref_46()
	call_c   Dyam_Seed_Start(&ref[18],&ref[46],&ref[46],fun0,1)
	call_c   build_ref_48()
	call_c   Dyam_Seed_Add_Comp(&ref[48],&ref[46],0)
	call_c   Dyam_Seed_End()
	move_ret seed[20]
	c_ret

;; TERM 48: '*FIRST*'(c_normalize(_C, _D)) :> '$$HOLE$$'
c_code local build_ref_48
	ret_reg &ref[48]
	call_c   build_ref_47()
	call_c   Dyam_Create_Binary(I(9),&ref[47],I(7))
	move_ret ref[48]
	c_ret

;; TERM 47: '*FIRST*'(c_normalize(_C, _D))
c_code local build_ref_47
	ret_reg &ref[47]
	call_c   build_ref_14()
	call_c   build_ref_45()
	call_c   Dyam_Create_Unary(&ref[14],&ref[45])
	move_ret ref[47]
	c_ret

;; TERM 45: c_normalize(_C, _D)
c_code local build_ref_45
	ret_reg &ref[45]
	call_c   build_ref_235()
	call_c   Dyam_Create_Binary(&ref[235],V(2),V(3))
	move_ret ref[45]
	c_ret

;; TERM 46: '*CITEM*'(c_normalize(_C, _D), c_normalize(_C, _D))
c_code local build_ref_46
	ret_reg &ref[46]
	call_c   build_ref_18()
	call_c   build_ref_45()
	call_c   Dyam_Create_Binary(&ref[18],&ref[45],&ref[45])
	move_ret ref[46]
	c_ret

c_code local build_seed_22
	ret_reg &seed[22]
	call_c   build_ref_85()
	call_c   build_ref_88()
	call_c   Dyam_Seed_Start(&ref[85],&ref[88],I(0),fun19,1)
	call_c   build_ref_86()
	call_c   Dyam_Seed_Add_Comp(&ref[86],fun49,0)
	call_c   Dyam_Seed_End()
	move_ret seed[22]
	c_ret

;; TERM 86: '*RITEM*'(c_normalize(_C, _D), voidret)
c_code local build_ref_86
	ret_reg &ref[86]
	call_c   build_ref_0()
	call_c   build_ref_45()
	call_c   build_ref_2()
	call_c   Dyam_Create_Binary(&ref[0],&ref[45],&ref[2])
	move_ret ref[86]
	c_ret

pl_code local fun49
	call_c   build_ref_86()
	call_c   Dyam_Unify_Item(&ref[86])
	fail_ret
	pl_jump  Follow_Cont(V(1))

;; TERM 88: '*RITEM*'(c_normalize(_C, _D), voidret) :> emit(0, '$TUPPLE'(35064645158528))
c_code local build_ref_88
	ret_reg &ref[88]
	call_c   build_ref_86()
	call_c   build_ref_87()
	call_c   Dyam_Create_Binary(I(9),&ref[86],&ref[87])
	move_ret ref[88]
	c_ret

;; TERM 87: emit(0, '$TUPPLE'(35064645158528))
c_code local build_ref_87
	ret_reg &ref[87]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,134217728)
	move_ret R(0)
	call_c   build_ref_4()
	call_c   Dyam_Create_Binary(&ref[4],N(0),R(0))
	move_ret ref[87]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 85: '*CURNEXT*'
c_code local build_ref_85
	ret_reg &ref[85]
	call_c   Dyam_Create_Atom("*CURNEXT*")
	move_ret ref[85]
	c_ret

c_code local build_seed_19
	ret_reg &seed[19]
	call_c   build_ref_7()
	call_c   build_ref_31()
	call_c   Dyam_Seed_Start(&ref[7],&ref[31],I(0),fun0,1)
	call_c   build_ref_32()
	call_c   Dyam_Seed_Add_Comp(&ref[32],&ref[31],0)
	call_c   Dyam_Seed_End()
	move_ret seed[19]
	c_ret

;; TERM 32: '*GUARD*'(data_emit(_C)) :> '$$HOLE$$'
c_code local build_ref_32
	ret_reg &ref[32]
	call_c   build_ref_31()
	call_c   Dyam_Create_Binary(I(9),&ref[31],I(7))
	move_ret ref[32]
	c_ret

;; TERM 31: '*GUARD*'(data_emit(_C))
c_code local build_ref_31
	ret_reg &ref[31]
	call_c   build_ref_7()
	call_c   build_ref_30()
	call_c   Dyam_Create_Unary(&ref[7],&ref[30])
	move_ret ref[31]
	c_ret

;; TERM 30: data_emit(_C)
c_code local build_ref_30
	ret_reg &ref[30]
	call_c   build_ref_236()
	call_c   Dyam_Create_Unary(&ref[236],V(2))
	move_ret ref[30]
	c_ret

c_code local build_seed_17
	ret_reg &seed[17]
	call_c   build_ref_7()
	call_c   build_ref_9()
	call_c   Dyam_Seed_Start(&ref[7],&ref[9],I(0),fun0,1)
	call_c   build_ref_10()
	call_c   Dyam_Seed_Add_Comp(&ref[10],&ref[9],0)
	call_c   Dyam_Seed_End()
	move_ret seed[17]
	c_ret

;; TERM 10: '*GUARD*'(data_emit(_B)) :> '$$HOLE$$'
c_code local build_ref_10
	ret_reg &ref[10]
	call_c   build_ref_9()
	call_c   Dyam_Create_Binary(I(9),&ref[9],I(7))
	move_ret ref[10]
	c_ret

;; TERM 9: '*GUARD*'(data_emit(_B))
c_code local build_ref_9
	ret_reg &ref[9]
	call_c   build_ref_7()
	call_c   build_ref_8()
	call_c   Dyam_Create_Unary(&ref[7],&ref[8])
	move_ret ref[9]
	c_ret

;; TERM 8: data_emit(_B)
c_code local build_ref_8
	ret_reg &ref[8]
	call_c   build_ref_236()
	call_c   Dyam_Create_Unary(&ref[236],V(1))
	move_ret ref[8]
	c_ret

;; TERM 1: c_normalize(_A, _B)
c_code local build_ref_1
	ret_reg &ref[1]
	call_c   build_ref_235()
	call_c   Dyam_Create_Binary(&ref[235],V(0),V(1))
	move_ret ref[1]
	c_ret

;; TERM 3: '*RITEM*'(c_normalize(_A, _B), voidret)
c_code local build_ref_3
	ret_reg &ref[3]
	call_c   build_ref_0()
	call_c   build_ref_1()
	call_c   build_ref_2()
	call_c   Dyam_Create_Binary(&ref[0],&ref[1],&ref[2])
	move_ret ref[3]
	c_ret

;; TERM 5: '*RITEM*'(emit, voidret)
c_code local build_ref_5
	ret_reg &ref[5]
	call_c   build_ref_0()
	call_c   build_ref_4()
	call_c   build_ref_2()
	call_c   Dyam_Create_Binary(&ref[0],&ref[4],&ref[2])
	move_ret ref[5]
	c_ret

;; TERM 232: [_W,_X,_Y]
c_code local build_ref_232
	ret_reg &ref[232]
	call_c   Dyam_Create_Tupple(22,24,I(0))
	move_ret ref[232]
	c_ret

;; TERM 233: 'Dyam_Seed_Add_Comp'
c_code local build_ref_233
	ret_reg &ref[233]
	call_c   Dyam_Create_Atom("Dyam_Seed_Add_Comp")
	move_ret ref[233]
	c_ret

;; TERM 231: component(_W, _X, _Y)
c_code local build_ref_231
	ret_reg &ref[231]
	call_c   build_ref_255()
	call_c   Dyam_Term_Start(&ref[255],3)
	call_c   Dyam_Term_Arg(V(22))
	call_c   Dyam_Term_Arg(V(23))
	call_c   Dyam_Term_Arg(V(24))
	call_c   Dyam_Term_End()
	move_ret ref[231]
	c_ret

;; TERM 255: component
c_code local build_ref_255
	ret_reg &ref[255]
	call_c   Dyam_Create_Atom("component")
	move_ret ref[255]
	c_ret

;; TERM 234: 'Dyam_Seed_End'
c_code local build_ref_234
	ret_reg &ref[234]
	call_c   Dyam_Create_Atom("Dyam_Seed_End")
	move_ret ref[234]
	c_ret

;; TERM 229: [_O,_L,_M,_H,c(_T)]
c_code local build_ref_229
	ret_reg &ref[229]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_249()
	call_c   Dyam_Create_Unary(&ref[249],V(19))
	move_ret R(0)
	call_c   Dyam_Create_List(R(0),I(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(7),R(0))
	move_ret R(0)
	call_c   Dyam_Create_Tupple(11,12,R(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(14),R(0))
	move_ret ref[229]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 230: 'Dyam_Seed_Start'
c_code local build_ref_230
	ret_reg &ref[230]
	call_c   Dyam_Create_Atom("Dyam_Seed_Start")
	move_ret ref[230]
	c_ret

;; TERM 228: build_seed_ + _B
c_code local build_ref_228
	ret_reg &ref[228]
	call_c   build_ref_256()
	call_c   build_ref_257()
	call_c   Dyam_Create_Binary(&ref[256],&ref[257],V(1))
	move_ret ref[228]
	c_ret

;; TERM 257: build_seed_
c_code local build_ref_257
	ret_reg &ref[257]
	call_c   Dyam_Create_Atom("build_seed_")
	move_ret ref[257]
	c_ret

;; TERM 256: +
c_code local build_ref_256
	ret_reg &ref[256]
	call_c   Dyam_Create_Atom("+")
	move_ret ref[256]
	c_ret

;; TERM 227: [_U|_V]
c_code local build_ref_227
	ret_reg &ref[227]
	call_c   Dyam_Create_List(V(20),V(21))
	move_ret ref[227]
	c_ret

;; TERM 226: seed{model=> _D, id=> '$seed'(_B, _C), anchor=> _E, subs_comp=> _F, code=> _G, go=> _H, tabule=> _I, schedule=> _J, subsume=> _K, model_ref=> _L, subs_comp_ref=> _M, c_type=> _N, c_type_ref=> _O, out_env=> _P, appinfo=> _Q, tail_flag=> _R}
c_code local build_ref_226
	ret_reg &ref[226]
	call_c   build_ref_258()
	call_c   build_ref_40()
	call_c   Dyam_Term_Start(&ref[258],16)
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(&ref[40])
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_End()
	move_ret ref[226]
	c_ret

;; TERM 258: seed!'$ft'
c_code local build_ref_258
	ret_reg &ref[258]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_274()
	call_c   Dyam_Create_List(&ref[274],I(0))
	move_ret R(0)
	call_c   build_ref_273()
	call_c   Dyam_Create_List(&ref[273],R(0))
	move_ret R(0)
	call_c   build_ref_272()
	call_c   Dyam_Create_List(&ref[272],R(0))
	move_ret R(0)
	call_c   build_ref_271()
	call_c   Dyam_Create_List(&ref[271],R(0))
	move_ret R(0)
	call_c   build_ref_270()
	call_c   Dyam_Create_List(&ref[270],R(0))
	move_ret R(0)
	call_c   build_ref_269()
	call_c   Dyam_Create_List(&ref[269],R(0))
	move_ret R(0)
	call_c   build_ref_268()
	call_c   Dyam_Create_List(&ref[268],R(0))
	move_ret R(0)
	call_c   build_ref_267()
	call_c   Dyam_Create_List(&ref[267],R(0))
	move_ret R(0)
	call_c   build_ref_266()
	call_c   Dyam_Create_List(&ref[266],R(0))
	move_ret R(0)
	call_c   build_ref_265()
	call_c   Dyam_Create_List(&ref[265],R(0))
	move_ret R(0)
	call_c   build_ref_264()
	call_c   Dyam_Create_List(&ref[264],R(0))
	move_ret R(0)
	call_c   build_ref_263()
	call_c   Dyam_Create_List(&ref[263],R(0))
	move_ret R(0)
	call_c   build_ref_262()
	call_c   Dyam_Create_List(&ref[262],R(0))
	move_ret R(0)
	call_c   build_ref_261()
	call_c   Dyam_Create_List(&ref[261],R(0))
	move_ret R(0)
	call_c   build_ref_260()
	call_c   Dyam_Create_List(&ref[260],R(0))
	move_ret R(0)
	call_c   build_ref_259()
	call_c   Dyam_Create_List(&ref[259],R(0))
	move_ret R(0)
	call_c   build_ref_199()
	call_c   Dyam_Create_Atom_Module("$ft",&ref[199])
	move_ret ref[258]
	call_c   DYAM_Feature_2(&ref[199],R(0))
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 199: seed
c_code local build_ref_199
	ret_reg &ref[199]
	call_c   Dyam_Create_Atom("seed")
	move_ret ref[199]
	c_ret

;; TERM 259: model
c_code local build_ref_259
	ret_reg &ref[259]
	call_c   Dyam_Create_Atom("model")
	move_ret ref[259]
	c_ret

;; TERM 260: id
c_code local build_ref_260
	ret_reg &ref[260]
	call_c   Dyam_Create_Atom("id")
	move_ret ref[260]
	c_ret

;; TERM 261: anchor
c_code local build_ref_261
	ret_reg &ref[261]
	call_c   Dyam_Create_Atom("anchor")
	move_ret ref[261]
	c_ret

;; TERM 262: subs_comp
c_code local build_ref_262
	ret_reg &ref[262]
	call_c   Dyam_Create_Atom("subs_comp")
	move_ret ref[262]
	c_ret

;; TERM 263: code
c_code local build_ref_263
	ret_reg &ref[263]
	call_c   Dyam_Create_Atom("code")
	move_ret ref[263]
	c_ret

;; TERM 264: go
c_code local build_ref_264
	ret_reg &ref[264]
	call_c   Dyam_Create_Atom("go")
	move_ret ref[264]
	c_ret

;; TERM 265: tabule
c_code local build_ref_265
	ret_reg &ref[265]
	call_c   Dyam_Create_Atom("tabule")
	move_ret ref[265]
	c_ret

;; TERM 266: schedule
c_code local build_ref_266
	ret_reg &ref[266]
	call_c   Dyam_Create_Atom("schedule")
	move_ret ref[266]
	c_ret

;; TERM 267: subsume
c_code local build_ref_267
	ret_reg &ref[267]
	call_c   Dyam_Create_Atom("subsume")
	move_ret ref[267]
	c_ret

;; TERM 268: model_ref
c_code local build_ref_268
	ret_reg &ref[268]
	call_c   Dyam_Create_Atom("model_ref")
	move_ret ref[268]
	c_ret

;; TERM 269: subs_comp_ref
c_code local build_ref_269
	ret_reg &ref[269]
	call_c   Dyam_Create_Atom("subs_comp_ref")
	move_ret ref[269]
	c_ret

;; TERM 270: c_type
c_code local build_ref_270
	ret_reg &ref[270]
	call_c   Dyam_Create_Atom("c_type")
	move_ret ref[270]
	c_ret

;; TERM 271: c_type_ref
c_code local build_ref_271
	ret_reg &ref[271]
	call_c   Dyam_Create_Atom("c_type_ref")
	move_ret ref[271]
	c_ret

;; TERM 272: out_env
c_code local build_ref_272
	ret_reg &ref[272]
	call_c   Dyam_Create_Atom("out_env")
	move_ret ref[272]
	c_ret

;; TERM 273: appinfo
c_code local build_ref_273
	ret_reg &ref[273]
	call_c   Dyam_Create_Atom("appinfo")
	move_ret ref[273]
	c_ret

;; TERM 274: tail_flag
c_code local build_ref_274
	ret_reg &ref[274]
	call_c   Dyam_Create_Atom("tail_flag")
	move_ret ref[274]
	c_ret

;; TERM 216: '$lterm'(_T, _U, _V)
c_code local build_ref_216
	ret_reg &ref[216]
	call_c   build_ref_275()
	call_c   Dyam_Term_Start(&ref[275],3)
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_Arg(V(21))
	call_c   Dyam_Term_End()
	move_ret ref[216]
	c_ret

;; TERM 275: '$lterm'
c_code local build_ref_275
	ret_reg &ref[275]
	call_c   Dyam_Create_Atom("$lterm")
	move_ret ref[275]
	c_ret

;; TERM 215: '$lterm'(_P, _Q, _R)
c_code local build_ref_215
	ret_reg &ref[215]
	call_c   build_ref_275()
	call_c   Dyam_Term_Start(&ref[275],3)
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_End()
	move_ret ref[215]
	c_ret

;; TERM 214: lterm(_J, _K, _L, _M, _N)
c_code local build_ref_214
	ret_reg &ref[214]
	call_c   build_ref_213()
	call_c   Dyam_Term_Start(&ref[213],5)
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_End()
	move_ret ref[214]
	c_ret

;; TERM 213: lterm
c_code local build_ref_213
	ret_reg &ref[213]
	call_c   Dyam_Create_Atom("lterm")
	move_ret ref[213]
	c_ret

;; TERM 218: '$lterm'(_C1, _D1, _E1)
c_code local build_ref_218
	ret_reg &ref[218]
	call_c   build_ref_275()
	call_c   Dyam_Term_Start(&ref[275],3)
	call_c   Dyam_Term_Arg(V(28))
	call_c   Dyam_Term_Arg(V(29))
	call_c   Dyam_Term_Arg(V(30))
	call_c   Dyam_Term_End()
	move_ret ref[218]
	c_ret

;; TERM 217: lterm(_A1, _K, _L, _B1, _N)
c_code local build_ref_217
	ret_reg &ref[217]
	call_c   build_ref_213()
	call_c   Dyam_Term_Start(&ref[213],5)
	call_c   Dyam_Term_Arg(V(26))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(27))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_End()
	move_ret ref[217]
	c_ret

;; TERM 222: 'Dyam_Remove_Pseudo_Choice'
c_code local build_ref_222
	ret_reg &ref[222]
	call_c   Dyam_Create_Atom("Dyam_Remove_Pseudo_Choice")
	move_ret ref[222]
	c_ret

;; TERM 225: [c(_Z)]
c_code local build_ref_225
	ret_reg &ref[225]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_249()
	call_c   Dyam_Create_Unary(&ref[249],V(25))
	move_ret R(0)
	call_c   Dyam_Create_List(R(0),I(0))
	move_ret ref[225]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 224: 'Dyam_Pseudo_Choice'
c_code local build_ref_224
	ret_reg &ref[224]
	call_c   Dyam_Create_Atom("Dyam_Pseudo_Choice")
	move_ret ref[224]
	c_ret

;; TERM 223: _X + 1
c_code local build_ref_223
	ret_reg &ref[223]
	call_c   build_ref_256()
	call_c   Dyam_Create_Binary(&ref[256],V(23),N(1))
	move_ret ref[223]
	c_ret

;; TERM 212: -1 : []
c_code local build_ref_212
	ret_reg &ref[212]
	call_c   build_ref_276()
	call_c   Dyam_Create_Binary(&ref[276],N(-1),I(0))
	move_ret ref[212]
	c_ret

;; TERM 276: :
c_code local build_ref_276
	ret_reg &ref[276]
	call_c   Dyam_Create_Atom(":")
	move_ret ref[276]
	c_ret

;; TERM 211: registers(_H1)
c_code local build_ref_211
	ret_reg &ref[211]
	call_c   build_ref_277()
	call_c   Dyam_Create_Unary(&ref[277],V(33))
	move_ret ref[211]
	c_ret

;; TERM 277: registers
c_code local build_ref_277
	ret_reg &ref[277]
	call_c   Dyam_Create_Atom("registers")
	move_ret ref[277]
	c_ret

;; TERM 210: '$lterm'(_F, _G, _H)
c_code local build_ref_210
	ret_reg &ref[210]
	call_c   build_ref_275()
	call_c   Dyam_Term_Start(&ref[275],3)
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[210]
	c_ret

;; TERM 209: term_handling(_E)
c_code local build_ref_209
	ret_reg &ref[209]
	call_c   build_ref_243()
	call_c   Dyam_Create_Unary(&ref[243],V(4))
	move_ret ref[209]
	c_ret

;; TERM 208: build_ref_ + _B
c_code local build_ref_208
	ret_reg &ref[208]
	call_c   build_ref_256()
	call_c   build_ref_278()
	call_c   Dyam_Create_Binary(&ref[256],&ref[278],V(1))
	move_ret ref[208]
	c_ret

;; TERM 278: build_ref_
c_code local build_ref_278
	ret_reg &ref[278]
	call_c   Dyam_Create_Atom("build_ref_")
	move_ret ref[278]
	c_ret

;; TERM 207: [_B,_D]
c_code local build_ref_207
	ret_reg &ref[207]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(3),I(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(1),R(0))
	move_ret ref[207]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 206: ';; TERM ~w: ~k\n'
c_code local build_ref_206
	ret_reg &ref[206]
	call_c   Dyam_Create_Atom(";; TERM ~w: ~k\n")
	move_ret ref[206]
	c_ret

;; TERM 200: main_initialization
c_code local build_ref_200
	ret_reg &ref[200]
	call_c   Dyam_Create_Atom("main_initialization")
	move_ret ref[200]
	c_ret

;; TERM 205: [c('_initialization')]
c_code local build_ref_205
	ret_reg &ref[205]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_249()
	call_c   build_ref_279()
	call_c   Dyam_Create_Unary(&ref[249],&ref[279])
	move_ret R(0)
	call_c   Dyam_Create_List(R(0),I(0))
	move_ret ref[205]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 279: '_initialization'
c_code local build_ref_279
	ret_reg &ref[279]
	call_c   Dyam_Create_Atom("_initialization")
	move_ret ref[279]
	c_ret

;; TERM 204: 'Dyam_Check_And_Update_Initialization'
c_code local build_ref_204
	ret_reg &ref[204]
	call_c   Dyam_Create_Atom("Dyam_Check_And_Update_Initialization")
	move_ret ref[204]
	c_ret

;; TERM 203: initialization_dyalog_ + _I
c_code local build_ref_203
	ret_reg &ref[203]
	call_c   build_ref_256()
	call_c   build_ref_280()
	call_c   Dyam_Create_Binary(&ref[256],&ref[280],V(8))
	move_ret ref[203]
	c_ret

;; TERM 280: initialization_dyalog_
c_code local build_ref_280
	ret_reg &ref[280]
	call_c   Dyam_Create_Atom("initialization_dyalog_")
	move_ret ref[280]
	c_ret

;; TERM 202: 'long local _initialization\n\n'
c_code local build_ref_202
	ret_reg &ref[202]
	call_c   Dyam_Create_Atom("long local _initialization\n\n")
	move_ret ref[202]
	c_ret

;; TERM 201: main_file(_D, info(_E, _F, _G, _H))
c_code local build_ref_201
	ret_reg &ref[201]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_282()
	call_c   Dyam_Term_Start(&ref[282],4)
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_281()
	call_c   Dyam_Create_Binary(&ref[281],V(3),R(0))
	move_ret ref[201]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 281: main_file
c_code local build_ref_281
	ret_reg &ref[281]
	call_c   Dyam_Create_Atom("main_file")
	move_ret ref[281]
	c_ret

;; TERM 282: info
c_code local build_ref_282
	ret_reg &ref[282]
	call_c   Dyam_Create_Atom("info")
	move_ret ref[282]
	c_ret

;; TERM 198: seeds
c_code local build_ref_198
	ret_reg &ref[198]
	call_c   Dyam_Create_Atom("seeds")
	move_ret ref[198]
	c_ret

;; TERM 197: ref
c_code local build_ref_197
	ret_reg &ref[197]
	call_c   Dyam_Create_Atom("ref")
	move_ret ref[197]
	c_ret

;; TERM 196: term
c_code local build_ref_196
	ret_reg &ref[196]
	call_c   Dyam_Create_Atom("term")
	move_ret ref[196]
	c_ret

;; TERM 192: '$term'(_G, _H)
c_code local build_ref_192
	ret_reg &ref[192]
	call_c   build_ref_238()
	call_c   Dyam_Create_Binary(&ref[238],V(6),V(7))
	move_ret ref[192]
	c_ret

;; TERM 195: _K : [_F|_L]
c_code local build_ref_195
	ret_reg &ref[195]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(5),V(11))
	move_ret R(0)
	call_c   build_ref_276()
	call_c   Dyam_Create_Binary(&ref[276],V(10),R(0))
	move_ret ref[195]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 194: _K : _L
c_code local build_ref_194
	ret_reg &ref[194]
	call_c   build_ref_276()
	call_c   Dyam_Create_Binary(&ref[276],V(10),V(11))
	move_ret ref[194]
	c_ret

;; TERM 193: registers(_J)
c_code local build_ref_193
	ret_reg &ref[193]
	call_c   build_ref_277()
	call_c   Dyam_Create_Unary(&ref[277],V(9))
	move_ret ref[193]
	c_ret

;; TERM 159: [_E,_D]
c_code local build_ref_159
	ret_reg &ref[159]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(3),I(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(4),R(0))
	move_ret ref[159]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 160: 'DYAM_Finite_Set_2'
c_code local build_ref_160
	ret_reg &ref[160]
	call_c   Dyam_Create_Atom("DYAM_Finite_Set_2")
	move_ret ref[160]
	c_ret

;; TERM 158: finite_set(_B, _C)
c_code local build_ref_158
	ret_reg &ref[158]
	call_c   build_ref_283()
	call_c   Dyam_Create_Binary(&ref[283],V(1),V(2))
	move_ret ref[158]
	c_ret

;; TERM 283: finite_set
c_code local build_ref_283
	ret_reg &ref[283]
	call_c   Dyam_Create_Atom("finite_set")
	move_ret ref[283]
	c_ret

;; TERM 162: [_H,_I]
c_code local build_ref_162
	ret_reg &ref[162]
	call_c   Dyam_Create_Tupple(7,8,I(0))
	move_ret ref[162]
	c_ret

;; TERM 161: features(_F, _G)
c_code local build_ref_161
	ret_reg &ref[161]
	call_c   build_ref_284()
	call_c   Dyam_Create_Binary(&ref[284],V(5),V(6))
	move_ret ref[161]
	c_ret

;; TERM 284: features
c_code local build_ref_284
	ret_reg &ref[284]
	call_c   Dyam_Create_Atom("features")
	move_ret ref[284]
	c_ret

;; TERM 166: [_K]
c_code local build_ref_166
	ret_reg &ref[166]
	call_c   Dyam_Create_List(V(10),I(0))
	move_ret ref[166]
	c_ret

;; TERM 165: 'DyALog_Register_Ender'
c_code local build_ref_165
	ret_reg &ref[165]
	call_c   Dyam_Create_Atom("DyALog_Register_Ender")
	move_ret ref[165]
	c_ret

;; TERM 164: dyalog_ender(_J, _K)
c_code local build_ref_164
	ret_reg &ref[164]
	call_c   build_ref_285()
	call_c   Dyam_Create_Binary(&ref[285],V(9),V(10))
	move_ret ref[164]
	c_ret

;; TERM 285: dyalog_ender
c_code local build_ref_285
	ret_reg &ref[285]
	call_c   Dyam_Create_Atom("dyalog_ender")
	move_ret ref[285]
	c_ret

;; TERM 157: main_directive_initialization
c_code local build_ref_157
	ret_reg &ref[157]
	call_c   Dyam_Create_Atom("main_directive_initialization")
	move_ret ref[157]
	c_ret

;; TERM 156: global
c_code local build_ref_156
	ret_reg &ref[156]
	call_c   Dyam_Create_Atom("global")
	move_ret ref[156]
	c_ret

;; TERM 155: '\n;;------------------ DIRECTIVES ------------\n'
c_code local build_ref_155
	ret_reg &ref[155]
	call_c   Dyam_Create_Atom("\n;;------------------ DIRECTIVES ------------\n")
	move_ret ref[155]
	c_ret

;; TERM 142: _D : _F
c_code local build_ref_142
	ret_reg &ref[142]
	call_c   build_ref_276()
	call_c   Dyam_Create_Binary(&ref[276],V(3),V(5))
	move_ret ref[142]
	c_ret

;; TERM 141: [_B|_F]
c_code local build_ref_141
	ret_reg &ref[141]
	call_c   Dyam_Create_List(V(1),V(5))
	move_ret ref[141]
	c_ret

;; TERM 145: _G : []
c_code local build_ref_145
	ret_reg &ref[145]
	call_c   build_ref_276()
	call_c   Dyam_Create_Binary(&ref[276],V(6),I(0))
	move_ret ref[145]
	c_ret

;; TERM 144: '$reg'(_G)
c_code local build_ref_144
	ret_reg &ref[144]
	call_c   build_ref_286()
	call_c   Dyam_Create_Unary(&ref[286],V(6))
	move_ret ref[144]
	c_ret

;; TERM 286: '$reg'
c_code local build_ref_286
	ret_reg &ref[286]
	call_c   Dyam_Create_Atom("$reg")
	move_ret ref[286]
	c_ret

;; TERM 143: _D + 1
c_code local build_ref_143
	ret_reg &ref[143]
	call_c   build_ref_256()
	call_c   Dyam_Create_Binary(&ref[256],V(3),N(1))
	move_ret ref[143]
	c_ret

;; TERM 140: _D : _E
c_code local build_ref_140
	ret_reg &ref[140]
	call_c   build_ref_276()
	call_c   Dyam_Create_Binary(&ref[276],V(3),V(4))
	move_ret ref[140]
	c_ret

;; TERM 139: registers(_C)
c_code local build_ref_139
	ret_reg &ref[139]
	call_c   build_ref_277()
	call_c   Dyam_Create_Unary(&ref[277],V(2))
	move_ret ref[139]
	c_ret

;; TERM 116: 'Dyam_Almost_End_Tupple'
c_code local build_ref_116
	ret_reg &ref[116]
	call_c   Dyam_Create_Atom("Dyam_Almost_End_Tupple")
	move_ret ref[116]
	c_ret

;; TERM 115: [_C,_D]
c_code local build_ref_115
	ret_reg &ref[115]
	call_c   Dyam_Create_Tupple(2,3,I(0))
	move_ret ref[115]
	c_ret

;; TERM 117: [c(_C),c(_D)]
c_code local build_ref_117
	ret_reg &ref[117]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_249()
	call_c   Dyam_Create_Unary(&ref[249],V(2))
	move_ret R(0)
	call_c   Dyam_Create_Unary(&ref[249],V(3))
	move_ret R(1)
	call_c   Dyam_Create_List(R(1),I(0))
	move_ret R(1)
	call_c   Dyam_Create_List(R(0),R(1))
	move_ret ref[117]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 119: 'Dyam_Write_Tupple'
c_code local build_ref_119
	ret_reg &ref[119]
	call_c   Dyam_Create_Atom("Dyam_Write_Tupple")
	move_ret ref[119]
	c_ret

;; TERM 118: [_C,_D|_E]
c_code local build_ref_118
	ret_reg &ref[118]
	call_c   Dyam_Create_Tupple(2,3,V(4))
	move_ret ref[118]
	c_ret

;; TERM 110: 'Dyam_End_Fun_With_Tupple'
c_code local build_ref_110
	ret_reg &ref[110]
	call_c   Dyam_Create_Atom("Dyam_End_Fun_With_Tupple")
	move_ret ref[110]
	c_ret

;; TERM 114: [c(_D),c(_E)]
c_code local build_ref_114
	ret_reg &ref[114]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_249()
	call_c   Dyam_Create_Unary(&ref[249],V(3))
	move_ret R(0)
	call_c   Dyam_Create_Unary(&ref[249],V(4))
	move_ret R(1)
	call_c   Dyam_Create_List(R(1),I(0))
	move_ret R(1)
	call_c   Dyam_Create_List(R(0),R(1))
	move_ret ref[114]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 113: 'Dyam_Write_Fun_With_Tupple'
c_code local build_ref_113
	ret_reg &ref[113]
	call_c   Dyam_Create_Atom("Dyam_Write_Fun_With_Tupple")
	move_ret ref[113]
	c_ret

;; TERM 112: [_D,_E|_F]
c_code local build_ref_112
	ret_reg &ref[112]
	call_c   Dyam_Create_Tupple(3,4,V(5))
	move_ret ref[112]
	c_ret

;; TERM 106: no_copyable(_E)
c_code local build_ref_106
	ret_reg &ref[106]
	call_c   build_ref_287()
	call_c   Dyam_Create_Unary(&ref[287],V(4))
	move_ret ref[106]
	c_ret

;; TERM 287: no_copyable
c_code local build_ref_287
	ret_reg &ref[287]
	call_c   Dyam_Create_Atom("no_copyable")
	move_ret ref[287]
	c_ret

;; TERM 108: [_E]
c_code local build_ref_108
	ret_reg &ref[108]
	call_c   Dyam_Create_List(V(4),I(0))
	move_ret ref[108]
	c_ret

;; TERM 107: 'Dyam_Set_Not_Copyable'
c_code local build_ref_107
	ret_reg &ref[107]
	call_c   Dyam_Create_Atom("Dyam_Set_Not_Copyable")
	move_ret ref[107]
	c_ret

;; TERM 109: '$lterm'(_B, _C, _D)
c_code local build_ref_109
	ret_reg &ref[109]
	call_c   build_ref_275()
	call_c   Dyam_Term_Start(&ref[275],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[109]
	c_ret

;; TERM 105: 'Dyam_Load_Viewer'
c_code local build_ref_105
	ret_reg &ref[105]
	call_c   Dyam_Create_Atom("Dyam_Load_Viewer")
	move_ret ref[105]
	c_ret

;; TERM 104: viewer(_B, _C)
c_code local build_ref_104
	ret_reg &ref[104]
	call_c   build_ref_288()
	call_c   Dyam_Create_Binary(&ref[288],V(1),V(2))
	move_ret ref[104]
	c_ret

;; TERM 288: viewer
c_code local build_ref_288
	ret_reg &ref[288]
	call_c   Dyam_Create_Atom("viewer")
	move_ret ref[288]
	c_ret

;; TERM 103: '\n;;------------------ VIEWERS ------------\n'
c_code local build_ref_103
	ret_reg &ref[103]
	call_c   Dyam_Create_Atom("\n;;------------------ VIEWERS ------------\n")
	move_ret ref[103]
	c_ret

;; TERM 98: -
c_code local build_ref_98
	ret_reg &ref[98]
	call_c   Dyam_Create_Atom("-")
	move_ret ref[98]
	c_ret

;; TERM 99: ';; File ~q\n'
c_code local build_ref_99
	ret_reg &ref[99]
	call_c   Dyam_Create_Atom(";; File ~q\n")
	move_ret ref[99]
	c_ret

;; TERM 97: main_file(_F, _G)
c_code local build_ref_97
	ret_reg &ref[97]
	call_c   build_ref_281()
	call_c   Dyam_Create_Binary(&ref[281],V(5),V(6))
	move_ret ref[97]
	c_ret

;; TERM 102: ';; File stdin\n'
c_code local build_ref_102
	ret_reg &ref[102]
	call_c   Dyam_Create_Atom(";; File stdin\n")
	move_ret ref[102]
	c_ret

;; TERM 101: main_file(-, _H)
c_code local build_ref_101
	ret_reg &ref[101]
	call_c   build_ref_281()
	call_c   build_ref_98()
	call_c   Dyam_Create_Binary(&ref[281],&ref[98],V(7))
	move_ret ref[101]
	c_ret

;; TERM 96: ';; Compiler: ~w ~w\n'
c_code local build_ref_96
	ret_reg &ref[96]
	call_c   Dyam_Create_Atom(";; Compiler: ~w ~w\n")
	move_ret ref[96]
	c_ret

;; TERM 95: compiler_info{name=> _B, version=> _C, author=> _D, email=> _E}
c_code local build_ref_95
	ret_reg &ref[95]
	call_c   build_ref_289()
	call_c   Dyam_Term_Start(&ref[289],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[95]
	c_ret

;; TERM 289: compiler_info!'$ft'
c_code local build_ref_289
	ret_reg &ref[289]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_294()
	call_c   Dyam_Create_List(&ref[294],I(0))
	move_ret R(0)
	call_c   build_ref_293()
	call_c   Dyam_Create_List(&ref[293],R(0))
	move_ret R(0)
	call_c   build_ref_292()
	call_c   Dyam_Create_List(&ref[292],R(0))
	move_ret R(0)
	call_c   build_ref_291()
	call_c   Dyam_Create_List(&ref[291],R(0))
	move_ret R(0)
	call_c   build_ref_290()
	call_c   Dyam_Create_Atom_Module("$ft",&ref[290])
	move_ret ref[289]
	call_c   DYAM_Feature_2(&ref[290],R(0))
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 290: compiler_info
c_code local build_ref_290
	ret_reg &ref[290]
	call_c   Dyam_Create_Atom("compiler_info")
	move_ret ref[290]
	c_ret

;; TERM 291: name
c_code local build_ref_291
	ret_reg &ref[291]
	call_c   Dyam_Create_Atom("name")
	move_ret ref[291]
	c_ret

;; TERM 292: version
c_code local build_ref_292
	ret_reg &ref[292]
	call_c   Dyam_Create_Atom("version")
	move_ret ref[292]
	c_ret

;; TERM 293: author
c_code local build_ref_293
	ret_reg &ref[293]
	call_c   Dyam_Create_Atom("author")
	move_ret ref[293]
	c_ret

;; TERM 294: email
c_code local build_ref_294
	ret_reg &ref[294]
	call_c   Dyam_Create_Atom("email")
	move_ret ref[294]
	c_ret

;; TERM 94: '$CLOSURE'('$fun'(54, 0, 1122593008), '$TUPPLE'(35064644907104))
c_code local build_ref_94
	ret_reg &ref[94]
	call_c   build_ref_93()
	call_c   Dyam_Closure_Aux(fun54,&ref[93])
	move_ret ref[94]
	c_ret

;; TERM 89: 'May have unexpected name clashes because of C normalization of ~w into ~w'
c_code local build_ref_89
	ret_reg &ref[89]
	call_c   Dyam_Create_Atom("May have unexpected name clashes because of C normalization of ~w into ~w")
	move_ret ref[89]
	c_ret

long local pool_fun53[3]=[2,build_ref_89,build_ref_90]

pl_code local fun53
	call_c   Dyam_Remove_Choice()
	move     &ref[89], R(0)
	move     0, R(1)
	move     &ref[90], R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_warning_2()

pl_code local fun54
	call_c   Dyam_Pool(pool_fun53)
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun53)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(1),V(2))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Deallocate()
	pl_ret

;; TERM 93: '$TUPPLE'(35064644907104)
c_code local build_ref_93
	ret_reg &ref[93]
	call_c   Dyam_Create_Simple_Tupple(0,201326592)
	move_ret ref[93]
	c_ret

;; TERM 65: load_init(_B)
c_code local build_ref_65
	ret_reg &ref[65]
	call_c   build_ref_295()
	call_c   Dyam_Create_Unary(&ref[295],V(1))
	move_ret ref[65]
	c_ret

;; TERM 295: load_init
c_code local build_ref_295
	ret_reg &ref[295]
	call_c   Dyam_Create_Atom("load_init")
	move_ret ref[295]
	c_ret

;; TERM 66: 'Dyam_Loading_Reset'
c_code local build_ref_66
	ret_reg &ref[66]
	call_c   Dyam_Create_Atom("Dyam_Loading_Reset")
	move_ret ref[66]
	c_ret

;; TERM 64: 'Dyam_Loading_Set'
c_code local build_ref_64
	ret_reg &ref[64]
	call_c   Dyam_Create_Atom("Dyam_Loading_Set")
	move_ret ref[64]
	c_ret

;; TERM 63: '\n;;------------------ LOADING ------------\n'
c_code local build_ref_63
	ret_reg &ref[63]
	call_c   Dyam_Create_Atom("\n;;------------------ LOADING ------------\n")
	move_ret ref[63]
	c_ret

;; TERM 59: initialization_dyalog_ + _F
c_code local build_ref_59
	ret_reg &ref[59]
	call_c   build_ref_256()
	call_c   build_ref_280()
	call_c   Dyam_Create_Binary(&ref[256],&ref[280],V(5))
	move_ret ref[59]
	c_ret

;; TERM 58: loaded(_B, require)
c_code local build_ref_58
	ret_reg &ref[58]
	call_c   build_ref_296()
	call_c   build_ref_297()
	call_c   Dyam_Create_Binary(&ref[296],V(1),&ref[297])
	move_ret ref[58]
	c_ret

;; TERM 297: require
c_code local build_ref_297
	ret_reg &ref[297]
	call_c   Dyam_Create_Atom("require")
	move_ret ref[297]
	c_ret

;; TERM 296: loaded
c_code local build_ref_296
	ret_reg &ref[296]
	call_c   Dyam_Create_Atom("loaded")
	move_ret ref[296]
	c_ret

;; TERM 60: main_module
c_code local build_ref_60
	ret_reg &ref[60]
	call_c   Dyam_Create_Atom("main_module")
	move_ret ref[60]
	c_ret

;; TERM 62: load
c_code local build_ref_62
	ret_reg &ref[62]
	call_c   Dyam_Create_Atom("load")
	move_ret ref[62]
	c_ret

;; TERM 61: build_viewers
c_code local build_ref_61
	ret_reg &ref[61]
	call_c   Dyam_Create_Atom("build_viewers")
	move_ret ref[61]
	c_ret

;; TERM 29: global(_B, _C)
c_code local build_ref_29
	ret_reg &ref[29]
	call_c   build_ref_156()
	call_c   Dyam_Create_Binary(&ref[156],V(1),V(2))
	move_ret ref[29]
	c_ret

;; TERM 35: lterm / 5
c_code local build_ref_35
	ret_reg &ref[35]
	call_c   build_ref_253()
	call_c   build_ref_213()
	call_c   Dyam_Create_Binary(&ref[253],&ref[213],N(5))
	move_ret ref[35]
	c_ret

;; TERM 34: ';; reset lterms\n'
c_code local build_ref_34
	ret_reg &ref[34]
	call_c   Dyam_Create_Atom(";; reset lterms\n")
	move_ret ref[34]
	c_ret

;; TERM 6: named_function(_B, _C, _D)
c_code local build_ref_6
	ret_reg &ref[6]
	call_c   build_ref_298()
	call_c   Dyam_Term_Start(&ref[298],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[6]
	c_ret

;; TERM 298: named_function
c_code local build_ref_298
	ret_reg &ref[298]
	call_c   Dyam_Create_Atom("named_function")
	move_ret ref[298]
	c_ret

;; TERM 26: build_seed_ + _C
c_code local build_ref_26
	ret_reg &ref[26]
	call_c   build_ref_256()
	call_c   build_ref_257()
	call_c   Dyam_Create_Binary(&ref[256],&ref[257],V(2))
	move_ret ref[26]
	c_ret

;; TERM 25: '$seed'(_C, _D)
c_code local build_ref_25
	ret_reg &ref[25]
	call_c   build_ref_237()
	call_c   Dyam_Create_Binary(&ref[237],V(2),V(3))
	move_ret ref[25]
	c_ret

;; TERM 28: build_ref_ + _C
c_code local build_ref_28
	ret_reg &ref[28]
	call_c   build_ref_256()
	call_c   build_ref_278()
	call_c   Dyam_Create_Binary(&ref[256],&ref[278],V(2))
	move_ret ref[28]
	c_ret

;; TERM 27: '$term'(_C, _E)
c_code local build_ref_27
	ret_reg &ref[27]
	call_c   build_ref_238()
	call_c   Dyam_Create_Binary(&ref[238],V(2),V(4))
	move_ret ref[27]
	c_ret

;; TERM 11: init_pool(_B)
c_code local build_ref_11
	ret_reg &ref[11]
	call_c   build_ref_299()
	call_c   Dyam_Create_Unary(&ref[299],V(1))
	move_ret ref[11]
	c_ret

;; TERM 299: init_pool
c_code local build_ref_299
	ret_reg &ref[299]
	call_c   Dyam_Create_Atom("init_pool")
	move_ret ref[299]
	c_ret

;; TERM 24: build_init_pool
c_code local build_ref_24
	ret_reg &ref[24]
	call_c   Dyam_Create_Atom("build_init_pool")
	move_ret ref[24]
	c_ret

;; TERM 23: local
c_code local build_ref_23
	ret_reg &ref[23]
	call_c   Dyam_Create_Atom("local")
	move_ret ref[23]
	c_ret

;; TERM 22: '\n;;------------------ INIT POOL ------------\n'
c_code local build_ref_22
	ret_reg &ref[22]
	call_c   Dyam_Create_Atom("\n;;------------------ INIT POOL ------------\n")
	move_ret ref[22]
	c_ret

;; TERM 13: _B : _C
c_code local build_ref_13
	ret_reg &ref[13]
	call_c   build_ref_276()
	call_c   Dyam_Create_Binary(&ref[276],V(1),V(2))
	move_ret ref[13]
	c_ret

;; TERM 12: registers(_D)
c_code local build_ref_12
	ret_reg &ref[12]
	call_c   build_ref_277()
	call_c   Dyam_Create_Unary(&ref[277],V(3))
	move_ret ref[12]
	c_ret

pl_code local fun63
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Object(R(0))
	call_c   Dyam_Tabule()
	pl_ret

pl_code local fun114
	call_c   Dyam_Remove_Choice()
	move     &ref[234], R(0)
	move     0, R(1)
	move     I(0), R(2)
	move     0, R(3)
	pl_call  pred_ma_emit_call_c_2()
	move     &ref[40], R(0)
	move     S(5), R(1)
	pl_call  pred_ma_emit_move_ret_1()
	pl_call  pred_ma_emit_ret_0()
	call_c   DYAM_Nl_0()
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun109
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Reg_Load_Ptr(2,V(16))
	fail_ret
	move     I(0), R(4)
	move     0, R(5)
	call_c   DyALog_Mutable_Read(R(2),&R(4))
	fail_ret
	move     V(22), R(0)
	move     S(5), R(1)
	pl_call  pred_first_register_1()
	call_c   Dyam_Reg_Load_Ptr(2,V(16))
	fail_ret
	call_c   Dyam_Reg_Load(4,V(22))
	call_c   DyALog_Mutable_Write(R(2),&R(4),0)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(16))
	fail_ret
	pl_fail

pl_code local fun103
	call_c   Dyam_Remove_Choice()
	move     &ref[222], R(0)
	move     0, R(1)
	move     I(0), R(2)
	move     0, R(3)
	pl_call  pred_ma_emit_call_c_2()
fun102:
	pl_call  pred_ma_emit_ret_0()
	call_c   DYAM_Nl_0()
	fail_ret
	move     &ref[213], R(0)
	move     0, R(1)
	call_c   Dyam_Reg_Deallocate(1)
	pl_jump  pred_reset_ordered_table_1()


pl_code local fun105
	call_c   Dyam_Remove_Choice()
fun104:
	call_c   Dyam_Choice(fun103)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(23),N(-1))
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun102()


pl_code local fun107
	call_c   Dyam_Remove_Choice()
	call_c   DYAM_evpred_is(V(25),&ref[223])
	fail_ret
	move     &ref[224], R(0)
	move     0, R(1)
	move     &ref[225], R(2)
	move     S(5), R(3)
	pl_call  pred_ma_emit_call_c_2()
fun106:
	call_c   Dyam_Choice(fun105)
	move     &ref[213], R(0)
	move     0, R(1)
	call_c   Dyam_Reg_Load(2,V(8))
	pl_call  pred_ordered_table_2()
	call_c   Dyam_Unify(V(8),&ref[217])
	fail_ret
	call_c   Dyam_Unify(V(10),&ref[218])
	fail_ret
	call_c   Dyam_Reg_Load_Ptr(2,V(28))
	fail_ret
	move     V(31), R(4)
	move     S(5), R(5)
	call_c   DyALog_Mutable_Read(R(2),&R(4))
	fail_ret
	call_c   Dyam_Reg_Load_Ptr(2,V(29))
	fail_ret
	move     V(32), R(4)
	move     S(5), R(5)
	call_c   DyALog_Mutable_Read(R(2),&R(4))
	fail_ret
	pl_call  fun2(&seed[23],1)
	call_c   Dyam_Reg_Load_Ptr(2,V(11))
	fail_ret
	move     I(0), R(4)
	move     0, R(5)
	call_c   DyALog_Mutable_Write(R(2),&R(4),0)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(11))
	fail_ret
	pl_fail


pl_code local fun110
	call_c   Dyam_Remove_Choice()
fun108:
	move     V(23), R(0)
	move     S(5), R(1)
	move     V(24), R(2)
	move     S(5), R(3)
	pl_call  pred_register_info_2()
	call_c   Dyam_Choice(fun107)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(23),N(-1))
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun106()


pl_code local fun112
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Reg_Load_Ptr(2,V(33))
	fail_ret
	move     I(0), R(4)
	move     0, R(5)
	call_c   DyALog_Mutable_Write(R(2),&R(4),0)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(33))
	fail_ret
	call_c   DYAM_evpred_assert_1(&ref[211])
	pl_jump  fun111()

pl_code local fun100
	call_c   Dyam_Update_Choice(fun10)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[201])
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(5))
	move     V(8), R(2)
	move     S(5), R(3)
	pl_call  pred_safe_c_normalize_2()
	move     &ref[202], R(0)
	move     0, R(1)
	move     I(0), R(2)
	move     0, R(3)
	pl_call  pred_format_2()
	move     &ref[156], R(0)
	move     0, R(1)
	move     &ref[203], R(2)
	move     S(5), R(3)
	pl_call  pred_ma_emit_code_2()
	move     &ref[204], R(0)
	move     0, R(1)
	move     &ref[205], R(2)
	move     S(5), R(3)
	pl_call  pred_ma_emit_call_c_2()
	pl_call  pred_ma_emit_fail_c_ret_0()
	call_c   Dyam_Reg_Deallocate(0)
	pl_jump  pred_emit_initializer_aux_0()

pl_code local fun97
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Reg_Load_Ptr(2,V(9))
	fail_ret
	call_c   Dyam_Reg_Load(4,&ref[195])
	call_c   DyALog_Mutable_Write(R(2),&R(4),0)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(9))
	fail_ret
	call_c   Dyam_Reg_Deallocate(4)
	pl_ret

pl_code local fun98
	call_c   Dyam_Update_Choice(fun10)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Reg_Load_Ptr(2,V(1))
	fail_ret
	move     V(8), R(4)
	move     S(5), R(5)
	call_c   DyALog_Mutable_Read(R(2),&R(4))
	fail_ret
	call_c   DYAM_evpred_ge(V(4),V(8))
	fail_ret
	call_c   Dyam_Cut()
	pl_call  Object_1(&ref[193])
	call_c   Dyam_Reg_Load_Ptr(2,V(9))
	fail_ret
	call_c   Dyam_Reg_Load(4,&ref[194])
	call_c   DyALog_Mutable_Read(R(2),&R(4))
	fail_ret
	call_c   Dyam_Choice(fun97)
	call_c   Dyam_Set_Cut()
	pl_call  Domain_2(V(5),V(11))
	call_c   Dyam_Cut()
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun85
	call_c   Dyam_Remove_Choice()
fun84:
	call_c   Dyam_Choice(fun6)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[164])
	call_c   Dyam_Cut()
	move     &ref[165], R(0)
	move     0, R(1)
	move     &ref[166], R(2)
	move     S(5), R(3)
	pl_call  pred_ma_emit_call_c_2()
fun3:
	pl_call  pred_ma_emit_ret_0()
	call_c   DYAM_Nl_0()
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret



pl_code local fun87
	call_c   Dyam_Remove_Choice()
fun86:
	call_c   Dyam_Choice(fun85)
	pl_call  Object_1(&ref[161])
	call_c   Dyam_Reg_Load(0,V(5))
	move     V(7), R(2)
	move     S(5), R(3)
	pl_call  pred_label_term_2()
	call_c   Dyam_Reg_Load(0,V(6))
	move     V(8), R(2)
	move     S(5), R(3)
	pl_call  pred_label_term_2()
	move     &ref[162], R(0)
	move     S(5), R(1)
	pl_call  pred_emit_globals_in_init_pool_1()
	move     &ref[163], R(0)
	move     0, R(1)
	move     &ref[162], R(2)
	move     S(5), R(3)
	pl_call  pred_ma_emit_call_c_2()
	pl_fail


pl_code local fun79
	call_c   Dyam_Remove_Choice()
	call_c   DYAM_evpred_is(V(6),&ref[143])
	fail_ret
	call_c   Dyam_Unify(V(1),&ref[144])
	fail_ret
	call_c   Dyam_Reg_Load_Ptr(2,V(2))
	fail_ret
	call_c   Dyam_Reg_Load(4,&ref[145])
	call_c   DyALog_Mutable_Write(R(2),&R(4),0)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(2))
	fail_ret
	call_c   Dyam_Reg_Deallocate(4)
	pl_ret

pl_code local fun71
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(1),&ref[118])
	fail_ret
	move     &ref[119], R(0)
	move     0, R(1)
	move     &ref[117], R(2)
	move     S(5), R(3)
	pl_call  pred_ma_emit_call_c_2()
	call_c   Dyam_Reg_Load(0,V(4))
	call_c   Dyam_Reg_Deallocate(1)
	pl_jump  pred_emit_xtupple_1()

pl_code local fun69
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(1),&ref[112])
	fail_ret
	move     &ref[113], R(0)
	move     0, R(1)
	move     &ref[114], R(2)
	move     S(5), R(3)
	pl_call  pred_ma_emit_call_c_2()
	call_c   Dyam_Reg_Load(0,V(5))
	call_c   Dyam_Reg_Load(2,V(2))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_emit_fun_tupple_2()

pl_code local fun66
	call_c   Dyam_Remove_Choice()
fun65:
	call_c   Dyam_Cut()
	call_c   Dyam_Deallocate()
	pl_ret


pl_code local fun67
	call_c   Dyam_Remove_Choice()
	move     &ref[107], R(0)
	move     0, R(1)
	move     &ref[108], R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_ma_emit_call_c_2()

pl_code local fun60
	call_c   Dyam_Remove_Choice()
fun59:
	move     &ref[99], R(0)
	move     0, R(1)
	move     &ref[100], R(2)
	move     S(5), R(3)
	pl_call  pred_format_2()
	pl_fail


pl_code local fun57
	call_c   Dyam_Remove_Choice()
fun56:
	call_c   DYAM_Nl_0()
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret


pl_code local fun61
	call_c   Dyam_Remove_Choice()
fun58:
	call_c   Dyam_Choice(fun57)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[101])
	call_c   Dyam_Cut()
	move     &ref[102], R(0)
	move     0, R(1)
	move     I(0), R(2)
	move     0, R(3)
	pl_call  pred_format_2()
	pl_jump  fun56()


pl_code local fun47
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Object(R(0))
	call_c   Dyam_Tabule()
	pl_jump  Schedule_Prolog()

pl_code local fun48
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Choice(fun47)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Subsume(R(0))
	fail_ret
	call_c   Dyam_Cut()
	pl_ret

pl_code local fun50
	call_c   Dyam_Backptr_Trace(R(1),R(2))
	call_c   Dyam_Light_Object(R(0))
	pl_jump  Schedule_Prolog()

pl_code local fun51
	call_c   Dyam_Remove_Choice()
	pl_call  fun50(&seed[22],2,V(4))
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun37
	call_c   Dyam_Remove_Choice()
fun36:
	move     &ref[66], R(0)
	move     0, R(1)
	move     I(0), R(2)
	move     0, R(3)
	pl_call  pred_ma_emit_call_c_2()
	pl_call  pred_ma_emit_ret_0()
	call_c   DYAM_Nl_0()
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret


pl_code local fun32
	call_c   Dyam_Remove_Choice()
	move     &ref[24], R(0)
	move     0, R(1)
	move     I(0), R(2)
	move     0, R(3)
	pl_call  pred_ma_emit_call_c_2()
fun31:
	move     &ref[61], R(0)
	move     0, R(1)
	move     I(0), R(2)
	move     0, R(3)
	pl_call  pred_ma_emit_call_c_2()
	move     &ref[62], R(0)
	move     0, R(1)
	move     I(0), R(2)
	move     0, R(3)
	pl_call  pred_ma_emit_call_c_2()
	pl_call  pred_ma_emit_ret_0()
	call_c   DYAM_Nl_0()
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret


pl_code local fun34
	call_c   Dyam_Remove_Choice()
fun33:
	call_c   Dyam_Choice(fun32)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[60])
	call_c   Dyam_Cut()
	pl_jump  fun31()


pl_code local fun12
	call_c   Dyam_Remove_Choice()
	move     &ref[34], R(0)
	move     0, R(1)
	move     I(0), R(2)
	move     0, R(3)
	pl_call  pred_format_2()
	move     &ref[33], R(0)
	move     0, R(1)
	move     N(0), R(2)
	move     0, R(3)
	pl_call  pred_reset_counter_2()
	move     &ref[35], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Deallocate(1)
	pl_jump  pred_abolish_1()

pl_code local fun14
	call_c   Dyam_Remove_Choice()
fun13:
	move     &ref[33], R(0)
	move     0, R(1)
	move     V(3), R(2)
	move     S(5), R(3)
	pl_call  pred_value_counter_2()
	call_c   Dyam_Choice(fun12)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_lt(V(3),N(3000))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Deallocate()
	pl_ret


pl_code local fun10
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun2
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Light_Object(R(0))
	pl_jump  Schedule_Prolog()

pl_code local fun4
	call_c   Dyam_Remove_Choice()
	pl_fail

pl_code local fun5
	call_c   Dyam_Update_Choice(fun4)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[27])
	fail_ret
	call_c   Dyam_Cut()
	move     &ref[28], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	pl_call  pred_ma_emit_call_c_2()
	pl_fail

pl_code local fun6
	call_c   Dyam_Remove_Choice()
	pl_jump  fun3()

pl_code local fun8
	call_c   Dyam_Remove_Choice()
fun7:
	call_c   DYAM_Write_2(I(0),&ref[22])
	fail_ret
	call_c   DYAM_Nl_0()
	fail_ret
	move     &ref[23], R(0)
	move     0, R(1)
	move     &ref[24], R(2)
	move     0, R(3)
	pl_call  pred_ma_emit_code_2()
	call_c   Dyam_Choice(fun6)
	pl_call  Object_2(&ref[11],V(5))
	call_c   DYAM_evpred_retract(&ref[11])
	fail_ret
	call_c   Dyam_Choice(fun5)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[25])
	fail_ret
	call_c   Dyam_Cut()
	move     &ref[26], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	pl_call  pred_ma_emit_call_c_2()
	pl_fail



;;------------------ INIT POOL ------------

c_code local build_init_pool
	call_c   build_seed_15()
	call_c   build_seed_1()
	call_c   build_seed_2()
	call_c   build_seed_0()
	call_c   build_seed_13()
	call_c   build_seed_16()
	call_c   build_seed_4()
	call_c   build_seed_5()
	call_c   build_seed_10()
	call_c   build_seed_8()
	call_c   build_seed_9()
	call_c   build_seed_14()
	call_c   build_seed_7()
	call_c   build_seed_12()
	call_c   build_seed_11()
	call_c   build_seed_3()
	call_c   build_seed_6()
	call_c   build_seed_23()
	call_c   build_seed_20()
	call_c   build_seed_22()
	call_c   build_seed_19()
	call_c   build_seed_17()
	call_c   build_ref_1()
	call_c   build_ref_3()
	call_c   build_ref_4()
	call_c   build_ref_5()
	call_c   build_ref_232()
	call_c   build_ref_233()
	call_c   build_ref_231()
	call_c   build_ref_40()
	call_c   build_ref_234()
	call_c   build_ref_229()
	call_c   build_ref_230()
	call_c   build_ref_228()
	call_c   build_ref_227()
	call_c   build_ref_226()
	call_c   build_ref_216()
	call_c   build_ref_215()
	call_c   build_ref_214()
	call_c   build_ref_218()
	call_c   build_ref_217()
	call_c   build_ref_213()
	call_c   build_ref_222()
	call_c   build_ref_225()
	call_c   build_ref_224()
	call_c   build_ref_223()
	call_c   build_ref_212()
	call_c   build_ref_211()
	call_c   build_ref_44()
	call_c   build_ref_210()
	call_c   build_ref_209()
	call_c   build_ref_208()
	call_c   build_ref_207()
	call_c   build_ref_206()
	call_c   build_ref_200()
	call_c   build_ref_205()
	call_c   build_ref_204()
	call_c   build_ref_203()
	call_c   build_ref_202()
	call_c   build_ref_201()
	call_c   build_ref_199()
	call_c   build_ref_198()
	call_c   build_ref_197()
	call_c   build_ref_196()
	call_c   build_ref_192()
	call_c   build_ref_195()
	call_c   build_ref_194()
	call_c   build_ref_193()
	call_c   build_ref_159()
	call_c   build_ref_160()
	call_c   build_ref_158()
	call_c   build_ref_162()
	call_c   build_ref_163()
	call_c   build_ref_161()
	call_c   build_ref_166()
	call_c   build_ref_165()
	call_c   build_ref_164()
	call_c   build_ref_157()
	call_c   build_ref_156()
	call_c   build_ref_155()
	call_c   build_ref_142()
	call_c   build_ref_141()
	call_c   build_ref_145()
	call_c   build_ref_144()
	call_c   build_ref_143()
	call_c   build_ref_140()
	call_c   build_ref_139()
	call_c   build_ref_116()
	call_c   build_ref_115()
	call_c   build_ref_117()
	call_c   build_ref_119()
	call_c   build_ref_118()
	call_c   build_ref_111()
	call_c   build_ref_110()
	call_c   build_ref_114()
	call_c   build_ref_113()
	call_c   build_ref_112()
	call_c   build_ref_106()
	call_c   build_ref_108()
	call_c   build_ref_107()
	call_c   build_ref_109()
	call_c   build_ref_105()
	call_c   build_ref_104()
	call_c   build_ref_103()
	call_c   build_ref_98()
	call_c   build_ref_100()
	call_c   build_ref_99()
	call_c   build_ref_97()
	call_c   build_ref_102()
	call_c   build_ref_101()
	call_c   build_ref_90()
	call_c   build_ref_96()
	call_c   build_ref_95()
	call_c   build_ref_94()
	call_c   build_ref_65()
	call_c   build_ref_66()
	call_c   build_ref_64()
	call_c   build_ref_63()
	call_c   build_ref_59()
	call_c   build_ref_58()
	call_c   build_ref_60()
	call_c   build_ref_62()
	call_c   build_ref_61()
	call_c   build_ref_29()
	call_c   build_ref_35()
	call_c   build_ref_34()
	call_c   build_ref_33()
	call_c   build_ref_6()
	call_c   build_ref_26()
	call_c   build_ref_25()
	call_c   build_ref_28()
	call_c   build_ref_27()
	call_c   build_ref_11()
	call_c   build_ref_24()
	call_c   build_ref_23()
	call_c   build_ref_22()
	call_c   build_ref_13()
	call_c   build_ref_12()
	c_ret

long local ref[300]
long local seed[24]

long local _initialization

c_code global initialization_dyalog_emit
	call_c   Dyam_Check_And_Update_Initialization(_initialization)
	fail_c_ret
	call_c   initialization_dyalog_format()
	call_c   build_init_pool()
	call_c   build_viewers()
	call_c   load()
	c_ret

