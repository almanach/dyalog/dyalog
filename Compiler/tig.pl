/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2003, 2004, 2007, 2008, 2009, 2010, 2011, 2013, 2014, 2016, 2018 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  tig.pl -- Tree Insertion Grammars
 *
 * ----------------------------------------------------------------
 * Description
 *   Implementation of Tree Insertion Grammar within DyALog
 *   maybe should be included in tag*.pl files
 * ----------------------------------------------------------------
 */

:-include 'header.pl'.
:-require 'reader.pl'.
:-require 'tag_normalize.pl'.
:-require 'tag_maker.pl'.
:-require 'tag_compile.pl'.
:-require 'tag_dyn.pl'.

:-rec_prolog tig_compile_subtree_left/10.
:-rec_prolog tig_compile_subtree_right/11.

analyze_at_clause( T::tag_tree{ tree => auxtree(_)  } ) :-
	recorded( compiler_analyzer(tag2tig) ),
	normalize(T,L)
	.

:-extensional tag_nt/2.

:-std_prolog tig/2.

tig(L::tag_node{label=>NT,top=>Top},Kind) :-
	functor(Top,_NT,N),
	record_without_doublon(tag_nt(NT,_NT/N)),
	potential_tig(L,Kind),
	\+ not_tig(L,Kind)
	.

:-light_tabular not_tig/2.

not_tig(L,Kind) :-
	(
%	 format('test kind=~w L=~w\n',[Kind,L]),
	 spine_node(L,N::tag_node{label=>NT,
				   top=>Top,
				   adjleft=>AdjLeft,
				   adjright=>AdjRight
				  }),
%	  format('test0 kind=~w L=~w N=~w\n',[Kind,L,N]),
	  (Kind == left, AdjLeft = possible_adj[] xor Kind == right, AdjRight == possible_adj[]),
%	  format('test1 kind=~w L=~w N=~w\n',[Kind,L,N]),
	  node_auxtree(NT,Top,T),
%	  format('test2 kind=~w L=~w N=~w T=~w\n',[Kind,L,N,T]),
	  (   \+ potential_tig(T,Kind) xor not_tig(T,Kind) ),
%	  format('=> not tig kind=~w L=~w\n',[Kind,L]),
	  true
	;
	  fail,
	  L = tag_node{label=>NT,
		       top=>Top,
		       adjleft=>AdjLeft,
		       adjright=>AdjRight
		      },
	  (Kind == left, AdjLeft = possible_adj[] xor Kind == right, AdjRight == possible_adj[]),
%	  format('here1 ~w ~w\n',[Kind,L]),
	  node_auxtree(NT,Top,T),
%	  format('here2 ~w ~w\n',[Kind,L]),
	  potential_tig(T,Kind),
%	  format('here3 ~w ~w\n',[Kind,L]),
	  not_tig(T,Kind),
%	  format('here4 ~w ~w\n',[Kind,L]),
	  true
	)
	.

:-light_tabular node_auxtree/3.

node_auxtree(NT,
	     Top,
	     LT::tag_node{ label=> NT,
			   top=>_Top,
			   spine=>yes
			 }
	    ) :-
	( \+ Top = _Top -> %% Need to do that to avoid binding _Top
	    fail
	;   
	    '$answers'(normalize(_,LT))
	    )
	.

:-std_prolog spine_node/2.

spine_node(N,M) :-
	%% Every adjoinable spine node but the root
	( N = (N1;N2) ->
	    (	spine_node(N1,M) ; spine_node(N2,M) )
	;   N = guard{ goal => N1 } ->
	    spine_node(N1,M)
	;
	    N=tag_node{children=>Children},
	    (	domain(K::tag_node{spine=>yes,adj=>Adj,adjwrap=>AdjWrap},Children)
	    ;	domain(guard{ goal => K },Children)
	    ),
	    (	M=K,
		Adj = possible_adj[],
		AdjWrap = possible_adj[]
	    ;	
		spine_node(K,M)
	    )
	)
	.

:-light_tabular potential_tig/2.

potential_tig(T,left) :- left_potential_tig(T).
potential_tig(T,right) :- right_potential_tig(T).

:-std_prolog left_potential_tig/1,right_potential_tig/1.

right_potential_tig(T) :-
	( T = (T1;T2) ->
	    right_potential_tig(T1),
	    right_potential_tig(T2)
	;   T = guard{ goal => T1 } ->
	    right_potential_tig(T1)
	;   T=[K|_] ->
	    right_potential_tig(K)
	;   T=tag_node{kind=>foot} ->
	    true
	;   T=tag_node{spine=>yes,children=>[K|_]},
	    right_potential_tig(K)
	)
	.

left_potential_tig(T) :-
	(   T=(T1;T2) ->
	    left_potential_tig(T1),
	    left_potential_tig(T2)
	;   T = guard{ goal => T1 } ->
	    left_potential_tig(T1)
	;   T=[_|_] ->
	    last_elt(T,K),
	    left_potential_tig(K)
	;   T=tag_node{kind=>foot} ->
	    true
	;   T=tag_node{spine=>yes,children=>Children},
	    last_elt(Children,K),
	    left_potential_tig(K)
	)
	.

:-std_prolog last_elt/2.

last_elt(L,A) :-
	(   L=[A]
	xor L=[_|LL],
	    last_elt(LL,A)
	)
	.

emit_at_pda :-
        recorded( compiler_analyzer(tag2tig) ),

        current_output(Old_Stream),
        ( recorded( output_file(File) ) ->
            absolute_file_name(File,AF),
            open(AF,write,Stream),
            set_output(Stream)
        ;
            true
        ),
	
	tag2tig_emit_header,
	
	%% Find TIG trees and emit :-tig(TreeName,TIGKind) directives
	every(('$answers'(normalize(T::tag_tree{name=>Name,tree=>auxtree(_)},
				    NT)),
	       ( tig(NT,Kind) ->
		   format(':-tig(~k,~w).\n',[Name,Kind])
	       ;   '$answers'(potential_tig(NT,Kind)) ->
		   format('%%:-tig(~w,~w).\n',[Name,Kind])
	       ;
		   format('%%:-tig(~w,wrap).\n',[Name])
	       )
	      )),

	nl,
	
	%% Foreach NonTerminal, list possible AdjKind
	every((tag_nt(NT,_NT/N),
	       functor(A,_NT,N),
	       ( atom_module(_NT,Feature,'$ft') ->
		   functor(Pred,_NT,N),
		   %% Optimized display for feature terms
		   '$interface'( 'DyALog_Assign_Display_Info'(Pred:term), [return(none)])
	       ;
		   Pred=_NT/N
	       ),
	       every((   '$answers'(normalize(_,Node)),
			 tig(Node::tag_node{label=>NT,top=>A},left) ->
			 format(':-adjkind(~k,left).\n',[Pred])
		     ;	 
			 true
		     )),
	       every(( '$answers'(normalize(_,Node)),
		       tig(Node,right) ->
		       format(':-adjkind(~k,right).\n',[Pred])
		     ;	 
		       true
		     )),
	       every((   '$answers'(normalize(_,Node)),
			 \+ tig(Node,left),
			 \+ tig(Node,right)
		     ->	 
			 format(':-adjkind(~k,wrap).\n',[Pred])
		     ;	 
			 true
		     )))),


	%% Setting default adj mode to no for other non terminal
	format( ':-adjkind(_,no).\n', [] ),
	
	nl,
        set_output(Old_Stream) 
	.


:-std_prolog tag2tig_emit_header.

tag2tig_emit_header :-
        date_time(Year,Month,Day,Hour,Min,Sec),
        compiler_info( compiler_info{ name=>Name, version=>Version }),
        format( '%% Compiler Analyzer tag2tig: ~w ~w\n', [Name,Version]),
        format( '%% Date    : ~w ~w ~w at ~w:~w:~w\n',[Month,Day,Year,Hour,Min,Sec]),
        every(  ( recorded( main_file(File,_) )
                , File \== '-'
                , absolute_file_name(File,F)
                , format('%% File ~q\n',[F])
                )
             ),
        ( recorded( main_file('-',_) ) -> format(';; File stdin\n',[]) ; true ),
        nl
        .
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% TIG analyzer main loop extension

%% tag2tig analyze run on pgm (no need for futher compiling)
check_analyzer_mode(tag2tig) :-
        record( stop_compilation_at(pgm) )
        .

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Reader extension

directive(D::tig(Spec,Mode),_,_,_) :-
	(   domain(Mode,[left,right])
	xor error('Not a valid TIG mode ~w',[D])),
	install( tig(Mode), Spec ).

installer(tig(Mode),TreeName/0, _) :-
	(   erase_recorded(tig(TreeName,OldMode)),
	    OldMode \== Mode ->
	    error( 'multiple TIG mode for tree ~w : ~w and ~w',[TreeName,Mode,OldMode])
	;   
	    true
	),
	record( tig(TreeName,Mode) )
	.

directive(D::adjkind(Spec,Mode),_,_,_) :-
	(   domain(Mode,[left,right,wrap,no])
	xor error('Not a valid TIG mode ~w',[D])),
	install( adjkind(Mode), Spec )
	.

installer(adjkind(Mode),Pred, _) :-
%%	format('Register adjkind ~w ~w\n',[Pred,Mode]),
	record_without_doublon( adjkind(Pred,Mode) )
	.


directive(D::adjrestr(Spec,Mode,Restr),_,_,_) :-
	(   domain(Mode,[left,right,wrap])
	xor error('Not a valid TIG mode ~w',[D])),
	( domain(Restr,[atmostone,one,strict])
	xor error('Nat a valid adj restriction ~w',[D])
	),
	install( adjrestr(Mode,Restr), Spec )
	.

installer(adjrestr(Mode,Restr),Pred, _) :-
%%	format('Register adjrestr ~w ~w\n',[Pred,Mode,Restr]),
	record_without_doublon( adjrestr(Pred,Mode,Restr) )
	.

%% This directive should be computable automatically
directive(D::no_empty_tig_trees,_,_,_) :-
	record_without_doublon( D )
	.

%% Deactivated: parsing slower with this option
%% May have to find something more clever !
directive(D::max_tig_adj(V),_,_,_) :-
%%	erase( max_tig_adj(_) ),
%%	record( D ),
	true
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Call/Ret Maker extension


:-rec_prolog alt_optimize_topbot_args/4.

alt_optimize_topbot_args([],[],L,L).
alt_optimize_topbot_args([T|Top],[B|Bot],In,Out) :-
	alt_optimize_topbot_args(Top,Bot,In,M),
	( T == B ->
	  Out = M
	;
	  Out = [B|M]
	).


:-std_prolog make_tig_callret/7.

make_tig_callret(Mode,Top,Bot,Left,Right,Call,Return) :-
	functor(Top,P,N),
	functor(Bot,P,N),
	functor(TTop,P,N),
	functor(TBot,P,N),
	make_true_tig_callret(Mode,TTop,TBot,XLeft,XRight,Call,Return),
	Top*Bot*Left*Right=TTop*TBot*XLeft*XRight
	.


:-light_tabular show_tag_features.

show_tag_features :-
	every(( recorded(tag_features(NT,Top,Bot)),
		format( 'TAG FEATURES nt=~w top=~w bot=~w\n',[NT,Top,Bot])
	      )).

:-light_tabular make_true_tig_callret/7.

make_true_tig_callret(Mode,Top,Bot,Left,Right,Call,Return) :-
%	show_tag_features,
	functor(Top,P,N),
	( %% recorded(tag_features_mode(_,Mode,Top,Bot)) xor
	recorded(tag_features(_,Top,Bot))
	xor true ),
%%	format('TIG CallRet top=~w bot=~w\n',[Top,Bot]),
	Top_Filter = tag_modulation{ top_modulation=> nt_modulation{ nt_pattern => Top_Pat,
								     left_pattern => Top_LPat,
								     right_pattern => Top_RPat
								   }},
	( recorded( 'tag_modulation'(P/N,Top_Filter) ) -> true
	;   recorded( 'tag_modulation'('*default*',Top_Filter ) ) -> true 
	;   Top_Pat=(+), Top_LPat=(+), Top_RPat=(-)
	),
	( Top_Pat = [Top_FPat|Top_Args_Pat] ->
	    Top_Pattern = [Top_FPat,Top_LPat,Top_RPat|Top_Args_Pat]
	;   compound(Top_Pat) ->
	    Top_Pat =.. [Top_FPat|Top_Args_Pat],
	    Top_Pattern = [Top_FPat,Top_LPat,Top_RPat|Top_Args_Pat]
	;   
	    Top_Pattern = [Top_Pat,Top_LPat,Top_RPat|Top_Pat]
	),
	Top =.. [F|Top_Args],
	deep_module_shift(F,Mode,FF),
	TTop =.. [FF,Left,Right|Top_Args],
	Bot =.. [F|Bot_Args],
	alt_optimize_topbot_args(Top_Args,Bot_Args,[],Bot_Args_Restr),
	make_callret_with_pat(Top_Pattern,TTop,Call,Return1),
	Return1 =.. [F1|Ret_Args1],
	append(Ret_Args1,Bot_Args_Restr,Ret_Args),
	Return =.. [F1|Ret_Args],
	every( tig_viewer(Mode,Top,Bot,Left,Right,Call,Return) )
	.

:-rec_prolog tig_viewer/7.

tig_viewer(left,Top,Bot,Left,Right,Call,Return) :-
	I = '*RITEM*'(Call,Return),
	numbervars(I),
	label_term(I,CI),
	label_term(Top(Left,Left)*Bot(Left,Right),CT),
	record_without_doublon( viewer(CI,CT) )
	.

tig_viewer(right,Top,Bot,Left,Right,Call,Return) :-
	I = '*RITEM*'(Call,Return),
	numbervars(I),
	label_term(I,CI),
	label_term(Top(Left,Right)*Bot(Right,Right),CT),
	record_without_doublon( viewer(CI,CT) )
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Compile extension

tag_compile_tree(Id,Tree,
		 Trans,
		 Type,
		 Info,
		 [Left,Right]
		) :-
	recorded( tig(Id,Mode) ),
%%	format('Tree Info ~w ~w\n',[Tree,Info]),
	tree_info(Tree,Info),
	tree_foot(Tree,Foot::tag_node{ top => Foot_Top }),
	(   Tree = tag_node{ label=> NT, top=> A, bot=>B,spine => yes }
	xor Tree = guard{ goal => tag_node{ label=> NT, top=> A, bot=>B,spine => yes }}
	),
	%%	format('\t--> ~w ~w\n',[Tree,Info]),
	functor(A,F,N),
	register_predicate(tag(F/N)),
	functor(Foot_Top,F,N),
	tag_numbervars(Left,Id,_),
	tag_numbervars(Right,Id,_),
	( Mode ==  left ->
	    tag_numbervars(Foot_Right,Id,_),
	    make_tig_callret(Mode,A,Foot_Top,Left,Foot_Right,Call,Return),
	    XInfo = Info * left_tig(Foot_Top,Foot_Right,Return),
	    Trans = '*SAFIRST*'(Call) :> Cont,
	    Last = noop,		%% Return is ensured by foot node
	  tag_numbervars(Call,Id,_),
	  tupple(Call,Seen)
	;   Mode == right -> %% right tig
	    tag_numbervars(Foot_Left,Id,_),
	    make_tig_callret(Mode,Foot_Top,A,Foot_Left,Right,Call,Return),
	    XInfo = Info * right_tig(Foot_Top,Foot_Left,Call),
	    Trans = Cont, %% Call is ensured by foot mode
	    Last1 = '*SA-LAST*'(Return),
	    ( verbose_tag(Id) ->
	      core_info(Id,Info,tag_anchor{ family => HyperTag }),
	      tag_numbervars(HyperTag,Id,_),
	      body_to_lpda(Id,verbose!struct(Id,HyperTag),Last1,Last,Type)
	    ;	
		Last = Last1
	    ),
	  tupple([],Seen)
	;
	    error('Not a TIG mode in {left,right} for tree ~w',[Id])
	),
	%% No adj on root of TIG aux trees, unless non unifiable top/bot args
	%% do not factorize tag_numbervars(Tree,Id,_)
	%% otherwise it blocks possible unification of A and B !
	( A=B ->
	    tag_numbervars(Tree,Id,_),
	    tag_compile_subtree_noadj(Id,Tree,Left,Right,Last,Cont2,Type,XInfo,Seen)
	;   
	    warning('Top/Bot mistmatch for ~w TIG aux tree ~w\n',[A,B,Mode,Id]),
	    tag_numbervars(Tree,Id,_),
	    tag_compile_subtree(Id,Tree,Left,Right,Last,Cont2,Type,XInfo,Seen)
	),
	( Mode == left, guide(strip) ->
	    tagguide_pred(NT,SNT),
	    tagguide_name(Id,Mode,SId),
	    make_tig_callret(Mode,SNT,SNT,Left,Right,SCall,_),
	    Cont3 = '*GUIDE*'(SCall,SId,Cont2)
	; Mode == left, recorded(debug) ->
	  body_to_lpda(Id,format('Selecting cat=~w left=~w tree ~w\n',[A,Left,Id]),Cont3,Cont2,Type)
	;   
	    Cont3 = Cont2
	),
	( Mode == left,
	  recorded( tagfilter(Id^Left^Right^NT^A^TagFilter) ),
	  recorded( lctag(Id,_,_,_) )
	  ->
	  tag_numbervars(TagFilter,Id,_),
	  body_to_lpda(Id, TagFilter, Cont3,Cont,Type)
	;
	  Cont=Cont3
	),
%%	format('TIG ~w ~w: ~w\n',[Id,Mode,Trans]),
	true
	.

tag_compile_node(Id,
		 T::tag_node{ label => NT, bot=> A, kind=>foot},
		 Left,
		 Right,
		 Cont,
		 Trans,
		 Type,
		 XInfo,
		 Seen
		) :-
	recorded( tig(Id,Mode) ),
	tag_numbervars(A,Id,_),
%%	make_tig_callret(Mode,A,A,Left,Right,Call,Return),
	( Mode == left ->
	  ( Cont == noop
	  xor warning('Foot in left auxtree=~w cont=~w\n',[Id,Cont])
	  ),
	  %% Cont may be non empty in case of guards or of escapes coming after the foot node
	  %% *** WARNING *** Not yet fixed correctly
	  XInfo = Info * left_tig(A,_Right,Return),
	    (	recorded( guide(Id,_,_) ) ->
%%		Last = Cont :> '*GUIDE-LAST*'(Return,Id)
		Last = '*GUIDE-LAST*'(Return,Id)
	    ;	
%%		Last = Cont :> '*LAST*'(Return)
		Last = '*SA-LAST*'(Return)
	    ),
	    ( Left = Right, Trans1 = Last
	    xor 	    Trans1 = unify(Left,Right) :> Last ),
	    (	Right = _Right, Trans2 = Trans1
	    xor 	    Trans2 = unify(Right,_Right) :> Trans1 ),
	    ( verbose_tag(Id) ->
	      core_info(Id,Info,tag_anchor{ family => HyperTag }),
	      tag_numbervars(HyperTag,Id,_),
	      body_to_lpda(Id,verbose!struct(Id,HyperTag),Trans2,Trans,Type)
	    ;	
		Trans = Trans2
	    )
	;
	    XInfo = Info * right_tig(A,_Left,Call),
	    (	Left = Right, Trans1 = Cont2
	    xor 	    Trans1 = unify(Left,Right) :> Cont3 ),
	    (	Left = _Left, Trans2 = Trans1
	    xor 	    Trans2 = unify(Left,_Left) :> Trans1 ),
%%	    Trans = '*SAFIRST*'(Call) :> unify(_Left,Left) :> unify(Left,Right) :> Cont2,
	    Trans = '*SAFIRST*'(Call) :> Trans2,
	    ( guide(strip), \+ recorded( guide(Id,_,_) ) ->
		tagguide_pred(NT,SNT),
		tagguide_name(Id,Mode,SId),
		tag_numbervars(_Right,Id,_),
		make_tig_callret(Mode,SNT,SNT,Left,_Right,SCall,_),
		Cont2 = '*GUIDE*'(SCall,SId,Cont)
	    ; recorded(debug) ->
	      body_to_lpda(Id,format('Selecting cat=~w left=~w tree ~w\n',[A,Left,Id]),Cont,Cont2,Type)
	    ;	
	      Cont2 = Cont
	    ),
	    ( recorded( tagfilter(Id^Left^Right^NT^A^TagFilter) ),
	      recorded( lctag(Id,_,_,_) )
	    ->
	      tag_numbervars(TagFilter,Id,_),
	      body_to_lpda(Id, TagFilter, Cont2,Cont3,Type)
	    ;
	      Cont2=Cont3
	    )
	)
	.

tig_compile_subtree_left(Id,
			 T::tag_node{ label => NT,
				      adj=>Adj,
				      spine=>Spine,
				      id=>A_Id,
				      adjleft => AdjLeft,
				      adjright => AdjRight,
				      bot => Bot
				      },
			 Left,
			 Right,
			 Cont,
			 Trans,
			 Type,
			 Info,
			 Top,
			 Seen
			) :-
	functor(Top,F,N),
	( Adj = strict ->
	  tag_numbervars(AdjDone,Id,_)
	;
	  AdjDone = (+)
	),
%%	format('Try Left TIG Adj=~w ~K\n\n',[Adj,T]),
	(   %fail,
	    recorded(adjkind(F/N,left)),
	    AdjLeft = possible_adj[],
	    \+ (Spine=yes,recorded( tig(Id,right) ))
	->
	    ( 
	      (\+ recorded(adjkind(F/N,right)) xor AdjRight = no),
	      \+ recorded(adjkind(F/N,wrap)) ->
	      tag_numbervars(Foot_Left,Id,_),
	      Left_Bot=Bot,
	      (apply_feature_mode_constraints(NT,left,Top,Left_Bot)
	      xor
	      format('can''t apply feature_mode constraints nt=~w mode=~w top=~w bot=~w nid=~w tid=~w\n',[NT,left,Top,Left_Bot,A_Id,Id]),
	      true),
	      tag_compile_subtree_noadj(Id,
					T,
					Foot_Left,
					Right,
					Cont,
					Cont2,
					Type,
					Info,
					Seen
				       ),
	      (Adj = strict ->
	       body_to_lpda(Id,(AdjDone == (+)),Cont2,Cont1,Type)
	      ;
	       Cont1 = Cont2
	      )
	    ;
	      tag_numbervars(Foot_Left,Id,_),
	      (apply_feature_mode_constraints(NT,left,Top,Left_Bot)
	      xor
	      format('can''t apply feature_mode constraints nt=~w mode=~w top=~w bot=~w nid=~w tid=~w\n',[NT,left,Top,Left_Bot,A_Id,Id]),
	      true),
	      ( Adj = strict ->
		body_to_lpda(Id,(AdjDone == (+)),Cont,Cont2,Type)
	      ;
	       Cont = Cont2
	      ),
	      tig_compile_subtree_right(Id,
					T,
					Foot_Left,
					Right,
					Cont2,
					Cont1,
					Type,
					Info,
					Left_Bot,
					Seen,
					AdjDone
				       )
	    ),
%	    functor(Left_Bot,F,N),
%	    ( recorded( tag_features(NT,Top,Left_Bot) ) xor true),
	    tag_numbervars(Top,Id,_),
	    ( Adj = strict, AdjLeft = atmostone, AdjRight = sync ->
	      % synchromization and strict adj => strict left adj
	      Adj2 = one
	    ; AdjLeft = adj[strict,atmostone,one] ->
	      Adj2 = AdjLeft
	    ; recorded(adjkind(F/N,Mode)), Mode \== left ->
	      Adj2 = yes
	    ;	
	      Adj2 = Adj
	    ),
	    ( %% true ->
	      recorded( no_tig_wrapper ) ->
	      tig_compile_adj_node(Id,
				   tig_adj(F/N,left,Adj2),
				   Left,
				   Top,
				   Foot_Left,
				   Left_Bot,
				   A_Id,
				   Cont1,
				   Trans,
				   Type,
				   Seen,
				   AdjDone
				  )
	    ;
	      wrapping_predicate(Pred::tig_adj(F/N,left,Adj2,NT)),
	      decompose_args(tig_adj(F/N,left),
			     [Left,Foot_Left,Top,Left_Bot,A_Id,AdjDone,Id],
			     Args),
	      Trans = '*WRAPPER-CALL-ALT*'(Pred,Args,Cont1)
	    )
	;
	    ( Adj = strict ->
	      body_to_lpda(Id,(AdjDone == (+)),Cont,Cont2,Type)
	    ;
	      Cont = Cont2
	    ),
	    tig_compile_subtree_right(Id,
				      T,
				      Left,
				      Right,
				      Cont2,
				      Trans,
				      Type,
				      Info,
				      Top,
				      Seen,
				      AdjDone
				     )
	),
%%	format('TIG left trans top=~w bot=~w ~w\n',[Top,Left_Bot,Trans]),
	true
	.

tig_compile_subtree_right(Id,
			  T::tag_node{ label => NT,
				       adj=>Adj,
				       bot => TrueBot,
				       top => TrueTop,
				       spine=>Spine,
				       adjright => AdjRight,
				       adjleft => AdjLeft,
				       id=>A_Id },
			  Left,
			  Right,
			  Cont,
			  Trans,
			  Type,
			  Info,
			  Top,
			  Seen,
			  AdjDone
			 ) :-
%%	format('Try Right TIG Adj ~K\n\ttop=~w\n\n',[T,Top]),
%%	format('Try Right TIG Adj ~w ~w\n',[A_Id,Id]),
	functor(Top,F,N),
	(   %fail,
	    recorded( adjkind(F/N,right) ),
	    AdjRight = possible_adj[],
	    \+ (Spine=yes,recorded( tig(Id,left)))
	->  
	    ( AdjRight = sync, Adj = strict, AdjLeft = adj[atmostone,one] ->
	      Adj2 = one
	    ; AdjRight = adj[strict,atmostone,one] ->
	      Adj2 = AdjRight
	    ; recorded(adjkind(F/N,Mode)), Mode \== right ->
	      Adj2 = yes
	    ;	
	      Adj2 = Adj
	    ),
	    ( recorded(adjkind(F/N,wrap)) ->
	      functor(Bot,F,N)
	    ;
	      Top=Bot
	    ),
	    (apply_feature_constraints(NT,Top,Bot) xor true),
	    tag_numbervars(Foot_Right,Id,_),
	    tag_numbervars(Top,Id,_),
	    ( %% true ->
	      recorded( no_tig_wrapper ) ->
	      %%	    format('HERE\n',[]),
	      tig_compile_adj_node(Id,
				   tig_adj(F/N,right,Adj2),
				   Foot_Right,
				   TrueBot,
				   Right,
				   Bot,
				   A_Id,
				   Cont,
				   Cont1,
				   Type,
				   Seen,
				   AdjDone
				  )
	      %%	    format('HERE2\n',[]),
	    ;
	      wrapping_predicate(Pred::tig_adj(F/N,right,Adj2,NT)),
	      ( decompose_args(tig_adj(F/N,right),
			       [Foot_Right,Right,TrueBot,_Bot,A_Id,AdjDone,Id],
			       Args)
	      xor format('*** PB DECOMPOSE adj_right ~w ~w nid=~w id=~w\n',[TrueBot,_Bot,A_Id,Id])
	      ),
	      feature_args_unif(Id,Bot,_Bot,'*WRAPPER-CALL-ALT*'(Pred,Args,Cont),Cont1,Type)
	    ),
	    tag_compile_subtree_potential_adj(Id,
					      T,
					      Left,
					      Foot_Right,
					      Cont1,
					      Trans,
					      Type,
					      Info,
					      Top,
					      Bot,
					      Seen,
					      AdjDone
					     ),
%%	    format('HERE3\n',[]),
	    true
	;
	    tag_compile_subtree_potential_adj(Id,T,Left,Right,Cont,Trans,Type,Info,Top,TrueBot,Seen,AdjDone)
	),
%%	format('TIG right trans top=~w bot=~w ~w\n',[TrueBot,_Bot,Trans]),
	true
	.

decompose_args( tig_adj(F/N,Mode), L::[Left,Right,Top,Bot,A_Id,AdjDone,CallerTree], Args ) :-
%%	format('Dec ~w ~w\n',[F/N,L]),
	functor(Top,F,N),
	functor(Bot,F,N),
	( (recorded(tag_features_mode(_,Mode,Top,Bot)) 
	   xor recorded(tag_features(_,Top,Bot))) ->
	  Top =.. [F|TopArgs],
	  Bot =.. [F|BotArgs],
	  optimize_topbot_args(TopArgs,BotArgs,[A_Id,AdjDone,CallerTree],TopBotArgs),
%%	  format('Optimize TopBot top=~w bot=~w topbot=~w\n',[TopArgs,BotArgs,TopBotArgs]),
	  Args = [Left,Right|TopBotArgs]
	; N > 15 ->
	  Args = L
	;
	  Top =.. [_|Top_Args],
	  Bot =.. [_|Bot_Args],
	  append(Top_Args,Bot_Args,X_Args),
	  Args = [Left,Right,A_Id,AdjDone,CallerTree|X_Args]
	)
	.


wrapping_predicate( Pred::tig_adj(F/N,Mode,Adj,NT) ) :-
%%      format('Wrap 1 pred=~w mode=~w\n',[F/N,Pred]),
%        decompose_args(tig_adj(F/N),[V1,Left,Right,Top,Bot,A_Id],Args1),
%	decompose_args(tig_adj(F/N),[V2,Middle,Right,Top_Middle,Bot,A_Id],Args2),
        decompose_args(tig_adj(F/N,Mode),[Left,Right,Top,Bot,A_Id,AdjDone,CallerTree],Args1),
	decompose_args(tig_adj(F/N,Mode),[Middle,Right,Top_Middle,Bot,A_Id,AdjDone,CallerTree],Args2),
	\+ Args1 = callret(_,_),
        tag_numbervars(Args::[Closure|Args1],Id,_),
	tag_numbervars(Args2,Id,_),
	( Adj = adj[atmostone,one,sync] ->
	  make_tig_callret(Mode,Top,Bot,Left,Right,Call,Return),
	  Inner_Call = '*PROLOG-LAST*'
	; Adj = yes ->
	  make_tig_callret(Mode,Top,Top_Middle,Left,Middle,Call,Return),
	  Inner_Call = '*WRAPPER-INNER-CALL*'(tig_adj(F/N,Mode,yes,NT),Args2,'*PROLOG-LAST*')
	;
	  make_tig_callret(Mode,Top,Top_Middle,Left,Middle,Call,Return),
	  wrapping_predicate(tig_adj(F/N,Mode,yes,NT)),
	  Inner_Call = '*WRAPPER-CALL-ALT*'(tig_adj(F/N,Mode,yes,NT),Args2,'*PROLOG-LAST*')
	),
	(  /* recorded( max_tig_adj(Max) ) ->
	  tag_numbervars([V1,V2],Id,_),
	  ContAdj1 = '*SA-SUBST-ALT*'(Call,
				 Return,
					%%% failtest('DYAM_Subsumes_Chk_2',[L_Params,L_Args]) :>
				 Inner_Call,
				 A_Id),
	  body_to_lpda(Id,shift_range(1,0,V1,V2),ContAdj1,ContAdj,Type)
	; */
	   recorded( no_empty_tig_trees) ->
	  %% If there are no tig trees covering empty spans,
	  %% then not really necessary to tabulate recursive calls for adjs
	  _ContAdj = '*SA-SUBST*'(Call,
				  Return,
				  %%% failtest('DYAM_Subsumes_Chk_2',[L_Params,L_Args]) :>
				  %% '*HIDE-ARGS*'([Right,Bot],Inner_Call),
				  Inner_Call,
				  A_Id)
	;
	  _ContAdj = '*CONT*'(
			     '*SA-SUBST*'(Call,
					  Return,
					%%% failtest('DYAM_Subsumes_Chk_2',[L_Params,L_Args]) :>
					  Inner_Call,
					  A_Id)
			    )
	),
	( Adj = sync ->
	  %% an adjoining has to be done before to be syncronized
	  body_to_lpda(Id,(AdjDone == +),_ContAdj,_ContAdj2,Type)
	;
	  body_to_lpda(Id,(AdjDone = +),_ContAdj,_ContAdj2,Type)
	),
	( recorded( tagfilter_cat(CallerTree^NT^Mode^Left^Right^Top^Filter) )
	->
	  tag_numbervars(Filter,Id,_),
%%	  format('Compiling ~w ~w ~w with ~w\n',[F,Mode,dyalog,Filter]),
	  body_to_lpda(Id,Filter,_ContAdj2,ContAdj,dyalog),
%%	  format('==> ~w\n',[ContAdj]),
	  true
	;
	  ContAdj = _ContAdj2
	),
	body_to_lpda(Id,(Top=Bot),'*PROLOG-LAST*' :> fail,ContNoAdj1,Type),
	body_to_lpda(Id,(Left=Right),ContNoAdj1,ContNoAdj,dyalog),
%%        ContNoAdj = unify(Left,Right) :> unify(Top,Bot) :> '*PROLOG-LAST*' :> fail,
        ( Adj = adj[strict,one] ->
	  Cont = ContAdj
        ;   
	  Cont = '*ALTERNATIVE-LAYER*'(ContNoAdj,ContAdj)
        ),
        record( Wrap::'*WRAPPER*'( Pred,Args,Cont) ),
%%	format('Wrap 4 ~w\n',[Wrap]),
        true
        .

decompose_args( callret_tig_adj(F/N,Mode), L::[Left,Right,Top,Bot,A_Id,AdjDone,CallerTree], Args ) :-
%%	format('Dec ~w ~w\n',[F/N,L]),
	functor(Top,F,N),
	functor(Bot,F,N),
	( (recorded(tag_features_mode(_,Mode,Top,Bot)) 
	   xor recorded(tag_features(_,Top,Bot))) ->
	  Top =.. [_|_CallArgs],
	  Bot =.. [_|_RetArgs],
	  Args = callret([Left,A_Id,AdjDone,CallerTree|_CallArgs],[Right,A_Id,AdjDone|_RetArgs])
	; N > 15 ->
	  Args = L
	;
	  Top =.. [_|_CallArgs],
	  Bot=.. [_|_RetArgs],
	  Args = callret([Left,A_Id,AdjDone,CallerTree|_CallArgs],[Right,A_Id,AdjDone|_RetArgs])
	)
	.

wrapping_predicate( Pred::tig_adj(F/N,Mode,Adj,NT) ) :-
%%      format('Wrap 1 pred=~w mode=~w\n',[F/N,Pred]),
%        decompose_args(tig_adj(F/N),[V1,Left,Right,Top,Bot,A_Id],Args1),
%	decompose_args(tig_adj(F/N),[V2,Middle,Right,Top_Middle,Bot,A_Id],Args2),
        decompose_args(tig_adj(F/N,Mode),[Left,Right,Top,Bot,A_Id,AdjDone,CallerTree],Args1),
	decompose_args(tig_adj(F/N,Mode),[Middle,Right,Top_Middle,Bot,A_Id,AdjDone,CallerTree],Args2),
	Args1 = callret(CallArgs1,RetArgs1),
	Args2 = callret(CallArgs2,RetArgs2),
        tag_numbervars(Args::[Closure|Args1],Id,_),
	tag_numbervars(Args2,Id,_),
	( Adj = adj[atmostone,one,sync] ->
	  make_tig_callret(Mode,Top,Bot,Left,Right,Call,Return),
	  Inner_Call = '*RETARGS*'(RetArgs1)
	; Adj = yes ->
	  make_tig_callret(Mode,Top,Top_Middle,Left,Middle,Call,Return),
	  Inner_Call = '*WRAPPER-INNER-CALL*'(tig_adj(F/N,Mode,yes,NT),Args2,'*PROLOG-LAST*')
	; %% Adj = strict
	  make_tig_callret(Mode,Top,Top_Middle,Left,Middle,Call,Return),
	  wrapping_predicate(tig_adj(F/N,Mode,yes,NT)),
	  Inner_Call = '*WRAPPER-CALL-ALT*'(tig_adj(F/N,Mode,yes,NT),Args2,'*PROLOG-LAST*')
	),
	( 
	  recorded( no_empty_tig_trees) ->
	  %% If there are no tig trees covering empty spans,
	  %% then not really necessary to tabulate recursive calls for adjs
	  _ContAdj = '*SA-SUBST*'(Call,
				  Return,
				  %%% failtest('DYAM_Subsumes_Chk_2',[L_Params,L_Args]) :>
				  %% '*HIDE-ARGS*'([Right,Bot],Inner_Call),
				  Inner_Call,
				  A_Id)
	;
	  _ContAdj = '*CONT*'(
			     '*SA-SUBST*'(Call,
					  Return,
					%%% failtest('DYAM_Subsumes_Chk_2',[L_Params,L_Args]) :>
					  Inner_Call,
					  A_Id)
			    )
	),
	( Adj = sync ->
	  %% an adjoining has to be done before to be syncronized
	  body_to_lpda(Id,(AdjDone == +),_ContAdj,_ContAdj2,Type)
	;
	  body_to_lpda(Id,(AdjDone = +),_ContAdj,_ContAdj2,Type)
	),
	( recorded( tagfilter_cat(CallerTree^NT^Mode^Left^Right^Top^Filter) )
	->
	  tag_numbervars(Filter,Id,_),
%%	  format('Compiling ~w ~w ~w with ~w\n',[F,Mode,dyalog,Filter]),
	  body_to_lpda(Id,Filter,_ContAdj2,ContAdj,dyalog),
%%	  format('==> ~w\n',[ContAdj]),
	  true
	;
	  ContAdj = _ContAdj2
	),
	body_to_lpda(Id,(Top=Bot),'*RETARGS*'(RetArgs1) :> fail,ContNoAdj1,Type),
	body_to_lpda(Id,(Left=Right),ContNoAdj1,ContNoAdj,dyalog),
%%        ContNoAdj = unify(Left,Right) :> unify(Top,Bot) :> '*PROLOG-LAST*' :> fail,
        ( Adj = adj[strict,one] ->
	  Cont = ContAdj
        ;   
	  Cont = '*ALTERNATIVE-LAYER*'(ContNoAdj,ContAdj)
        ),
        record( Wrap::'*WRAPPER*'( Pred,Args,Cont) ),
%%	format('Wrap 4 ~w\n',[Wrap]),
        true
        .


:-std_prolog tig_compile_adj_node/12.

tig_compile_adj_node(Id,
		     Pred::tig_adj(F/N,Mode,Adj),
		     Left,
		     Left_Arg,
		     Right,
		     Right_Arg,
		     A_Id,
		     Cont,
		     Trans,
		     Type,
		     Seen,
		     AdjDone
		    ) :-
%%	format('Trying TIG KLEENE pred=~w node=~w id=~w\n',[Pred,A_Id,Id]),
	functor(Left2_Arg,F,N),
	functor(Middle_Arg,F,N),
	( recorded( tag_features(_,Left2_Arg,Middle_Arg) ) xor true ),
	tag_numbervars([Left2,Left2_Arg,
			Middle,Middle_Arg,
			Left,Left_Arg,
			Right,Right_Arg],Id,_),
	make_tig_callret(Mode,Left2_Arg,Middle_Arg,Left2,Middle,Call,Return),

	( Adj = yes ->
	  From = 0
	;
	  From = 1
	),
	Vars = [A_Id],
	
	update_counter(kleene, KleeneId),
	name_builder('_kleene~w',[KleeneId],KleeneLabel),
	kleene_conditions(KleeneCond::[From,inf],[Counter,CounterInc],InitCond,ExitCond,LoopCond),
	tag_numbervars([Counter,CounterInc,
			InitCond,ExitCond,LoopCond,
			KleeneStruct],Id,_),
	Left2_Arg =.. [_|Left2_Args],
	Args = [Counter,Left2|Left2_Args],
	Middle_Arg =.. [_|Middle_Args],
	Loop_Args = [CounterInc,Middle|Middle_Args],
	Left_Arg =.. [_|Left_Args],
	UserParam = [Left|Left_Args],
	Right_Arg =.. [_|Right_Args],
	UserLast = [Right|Right_Args],
	tupple(Vars,TA),
	tupple(Cont,TCont),
	extend_tupple([Right,Right_Arg,KleeneCond],TCont,RTCont),
%%	format('Here1\n',[]),
	body_to_lpda(Id,
		     (	 ExitCond,
			 Right = Left2,
			 Right_Arg = Left2_Arg
		     ),
		     Cont,
		     ContExit,
		     Type
		    ),	
	Inner_Call = '*KLEENE-LAST*'(KleeneLabel,
				    Loop_Args,
				    Args,
				    TA,
				    RTCont,
				    noop
				   ),
	( 
	  recorded( no_empty_tig_trees) ->
	  _ContAdj = '*SA-SUBST*'(Call,
				  Return,
				  Inner_Call,
				  A_Id)
	;
	  _ContAdj = '*CONT*'(
			      '*SA-SUBST*'(Call,
					   Return,
					   Inner_Call,
					   A_Id)
			    )
	),
	body_to_lpda(Id,(AdjDone = +),_ContAdj,ContAdj,Type),
	%% should go from Left2 -> Middle
%%	format('Here2 loopcond=~w contadj=~w\n',[LoopCond,ContAdj]),
	body_to_lpda(Id,
		     LoopCond, 
		     ContAdj,
		     ContLoop,
		     Type),
	Trans1 = '*SIMPLE-KLEENE*'(KleeneLabel,
			    TA,
			    '*KLEENE-ALTERNATIVE*'(ContExit,ContLoop)
			   ),
%%	format('Here3\n',[]),
	body_to_lpda(Id,
		     (	 InitCond,
			 Left = Left2,
			 Left_Arg = Left2_Arg
		     ),
		     Trans1,
		     Trans,
		     Type),
%%	format('TIG KLEENE ~w ~w ~w\n\n~w\n',[KleeneLabel,A_Id,Id,Trans]),
	true
	.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Left Corner Analysis for TIGs (and TAGs)

:-finite_set(lctag_labels,[epsilon,true,no_epsilon]).

%% tag2tig analyze run on pgm (no need for futher compiling)
check_analyzer_mode(lctag) :-
        record( stop_compilation_at(pgm) )
        .

directive(D::lctag(TreeName,First,Root,Trees),_,_,_) :-
	record(D)
	.

directive(D::tagfilter(Name^Left^Right^Top^Filter),_,_,_) :-
	directive_updater( tagfilter(_N^_L^_R^_Top^_Filter), D )
	.

directive(D::tagfilter_cat(Name^Cat^Mode^Left^Right^Top^Filter),_,_,_) :-
%%	directive_updater( tagfilter_cat(Name^_C^_M^_L^_R^_Filter), D )
	record(D)
	.

directive(D::lctag_safe(NT),_,_,_) :-
	record(D)
	.

analyze_at_clause( T::tag_tree{} ) :-
	recorded( compiler_analyzer(lctag) ),
	normalize(T,L)
	.

emit_at_pda :-
        recorded( compiler_analyzer(lctag) ),

        current_output(Old_Stream),
        ( recorded( output_file(File) ) ->
            absolute_file_name(File,AF),
            open(AF,write,Stream),
            set_output(Stream)
        ;
            true
        ),
	
	lctag_emit_header,
%	format('%% Start lctag closure\n',[]),
	wait(( K::lctag_closure(_,_,_,_),
%	       format('%% got ~w\n',[K]),
	       true
	     )),
%	format('%% Done lctag closure\n',[]),
	wait((
	      ('$answers'(normalize(tag_tree{ name => T },
				   tag_node{ label => A}
				   ))
	       ;
	       '$answers'(normalize(tag_tree{ name => T },
				    guard{ goal => tag_node{ label => A}}
				   ))
	      ),
	      lctag_filter(T,Scans,Cats,PostScans,PostCats,Pos),
%	      format('%% Filtering lctag ~w ~w ~w\n',[T,Scans,Cats]),
	      /*
	      every(( domain([X|Trees],Scans),
		      format(':-lctag(~k,scan,~k,~k).\n',[T,X,Trees]))),
	      every(( domain([X|Trees],Cats),
		      format(':-lctag(~k,cat,~k,~k).\n',[T,X,Trees])))
	      */
	      every(( domain([X|_Trees],Scans),
		      ( domain(T,_Trees) -> Trees = [] ; Trees = _Trees),
		      format(':-lctag(~k,scan,~k,~k).\n',[T,X,Trees]))),
	      every(( domain([X|_Trees],Cats),
		      ( domain(T,_Trees) -> Trees = [] ; Trees = _Trees),
		      format(':-lctag(~k,cat,~k,~k).\n',[T,X,Trees]))),

	     every(( domain([X|_Trees],PostScans),
		     ( domain(T,_Trees) -> Trees = [] ; Trees = _Trees ),
		      format(':-lctag(~k,postscan,~k,~k).\n',[T,X,Trees]))),
	     every(( domain([X|_Trees],PostCats),
		     ( domain(T,_Trees) -> Trees = [] ; Trees = _Trees),
		      format(':-lctag(~k,postcat,~k,~k).\n',[T,X,Trees]))),
	     every(( domain([X|_Trees],Pos),
		     ( domain(T,_Trees) -> Trees = [] ; Trees = _Trees),
		      format(':-lctag(~k,pos,~k,~k).\n',[T,X,Trees])))
	      	      


	     )),
	
	nl,
        set_output(Old_Stream) 
	.

:-std_prolog lctag_emit_header.

lctag_emit_header :-
        date_time(Year,Month,Day,Hour,Min,Sec),
        compiler_info( compiler_info{ name=>Name, version=>Version }),
        format( '%% Compiler Analyzer lctag: ~w ~w\n', [Name,Version]),
        format( '%% Date    : ~w ~w ~w at ~w:~w:~w\n',[Month,Day,Year,Hour,Min,Sec]),
        every(  ( recorded( main_file(File,_) )
                , File \== '-'
                , absolute_file_name(File,F)
                , format('%% File ~q\n',[F])
                )
             ),
        ( recorded( main_file('-',_) ) -> format(';; File stdin\n',[]) ; true ),
        nl
        .

:-light_tabular apply_feature_constraints/3.
:-mode(apply_feature_constraints/3,+(+,-,-)).

apply_feature_constraints(A,Top,Bot) :-
	( recorded( tag_features(A,Top,Bot) )
	xor
	  Top=A,
	  Bot=A
	)
	.


:-light_tabular apply_feature_mode_constraints/4.
:-mode(apply_feature_mode_constraints/4,+(+,+,-,-)).

apply_feature_mode_constraints(A,Mode,Top,Bot) :-
	( recorded( tag_features_mode(A,Mode,Top,Bot) ) xor
	apply_feature_constraints(A,Top,Bot)
	)
	.

%%:-light_tabular lctag_filter/3.

lctag_filter(Tree,Scans,Cats,PostScans,PostCats,Pos) :-
	mutable(ScanM,[]),
	mutable(CatM,[]),
	mutable(PostScanM,[]),
	mutable(PostCatM,[]),
	mutable(PosM,[]),
	wait((
	      item_term(I,lctag_closure(X,Y,Tree,Trees)),
	      recorded(I),
	      mutable_read(CatM,_Cats),
	      mutable_read(ScanM,_Scan),
	      mutable_read(PostCatM,_PostCat),
	      mutable_read(PosM,_Pos),
	      ( Y = scan(YF) ->
		( var(YF) ->
		  record( lctag_true(Tree) )
		;
		  lctag_add_trees(YF,Trees,_Scan,_XScan),
%%		  format('%% lctag add trees lex=~w old=~w new=~w\n',[YF,_Scan,_XScan]),
		  mutable(ScanM,_XScan)
		)
	      ; Y=anchor(YF,_Bot) ->
		lctag_add_trees(YF:_Bot,Trees,_Cats,_XCats),
		mutable(CatM,_XCats)
	      ; Y= coanchor(YF,_Bot) ->
		lctag_add_trees(YF:_Bot,Trees,_Cats,_XCats),
		mutable(CatM,_XCats)
	      ; Y=lctag_labels[true,epsilon] ->
		record( lctag_true(Tree) )
	      ; (Y = postscan(YF); Y= (postscan(YF) + _)) ->
		mutable_read(PostScanM,_PostScan),
		lctag_add_trees(YF,Trees,_PostScan,_XPostScan),
		mutable(PostScanM,_XPostScan)
	      ; (Y = postcat(YF,_Bot) ; Y = (postcat(YF,_Bot) + _))->
		mutable_read(PostCatM,_PostCats),
		lctag_add_trees(YF:_Bot,Trees,_PostCats,_XPostCats),
		mutable(PostCatM,_XPostCats)
	      ; (Y = pos(YF) ; Y= (pos(YF) + _))->
%		format('%% pos tree=~w Y=~w\n',[Tree,Y]),
		mutable_read(PosM,_Pos),
		lctag_add_trees(YF,Trees,_Pos,_XPos),
		mutable(PosM,_XPos)
	      ;
		fail
	      ),
	      %%	       format('Handling tree ~k => scans=~w cats=~w for ~k\n',
	      %%		      [Y,_Scan,_Cats,Tree]),
	      true
	     )),
	( recorded( lctag_true(Tree) ) ->
	  %% the tree should be always be selected
	  fail
	;
	  mutable_read(ScanM,Scans),
	  mutable_read(CatM,Cats),
	  mutable_read(PosM,Pos),
	  mutable_read(PostScanM,PostScans),
	  mutable_read(PostCatM,PostCats),
%%	  format('FINAL Handling tree => scans=~w cats=~w for ~k\n',
%%		      [Scans,Cats,Tree]),
	  ( Scans = [],
	    Cats = [],
	    Pos = [],
	    PostScans = [],
	    PostCats = []
	  ->
	    fail
	  ;
	    true
	  )
	)
	.

:-std_prolog lctag_add_trees/4.

lctag_add_trees(K,Trees,L,XL) :-
%%	K =.. _XK,
%%	format('add tree try ~w vs ~w, XK=~w\n',[K,L,_XK]),
	( L = [] -> XL = [[K|Trees]]
	; L = [[_K|KL]|M],
	  subsumes_chk(_K,K),
%%	  subsumes_chk(K,_K),
	  true
	->
	  ( Trees = [] ->
	    XL = [[_K]|M]
	  ; KL= [] ->
	    XL = L
	  ; Trees = [T], domain(T,KL) ->
	    XL = L
% 	  ; KL=[_,_,_,_] ->
% 	    %% no need to test if too many possible starting trees
% 	    XL = [[K]|M]
	  ; Trees = [T] ->
	    XL = [[_K,T|KL]|M]
	  ; fail,
	    mutable(MKL,KL),
	    every(( domain(_T,Trees),
		    \+ domain(_T,KL),
		    mutable_read(MKL,_KL),
		    mutable(MKL,[_T|_KL])
		  )),
	    mutable_read(MKL,NewKL),
	    XL = [[_K|NewKL]|M]
	  )
%%	; L = [[K1|_]|M], K @< K1 ->
%%	  XL = [[K|Trees]|L]
	; fail,
	  L = [[_K|KL]|M],
	  subsumes_chk(K,_K),
%%	  subsumes_chk(K,_K),
	  true ->
%%	  format('add tree here K=~w subsumes _K=~w\n',[K,_K]),
	  %% ** WARNING** not complete
	  %% the trees of _K are lost (but we don't really use them)
	  ( Trees = [] ->
	    Trees2 = KL
	  ; KL = [] ->
	    Trees2 = Trees
	  ; Trees = [T], domain(T,KL) ->
	    Trees2 = KL
	  ; Trees = [T] ->
	    Trees2 = [T|KL]
	  ; mutable(MKL,KL),
	    every(( domain(_T,Trees),
		    \+ domain(_T,KL),
		    mutable_read(MKL,_KL),
		    mutable(MKL,[_T|_KL])
		  )),
	    mutable_read(MKL,Trees2)
	  ),
	  lctag_add_trees(K,Trees2,M,XL)
	; L = [U|M],
	  lctag_add_trees(K,Trees,M,XM),
	  XL=[U|XM]
	)
	.

:-rec_prolog lctag_simplify/4.

%% try to simplify the trees before computing the left corner relation
lctag_simplify(Tree,
	       N::tag_node{ id => Id,
			    top => Top,
			    bot => Bot,
			    label => L,
			    spine => Spine,
			    adj => Adj,
			    children => Children,
			    adjleft => AdjLeft,
			    adjright => AdjRight,
			    adjwrap => AdjWrap,
			    token => Token,
			    kind => Kind
			  },
	       State,
	       Out
	      ) :-
	( domain(Kind,[coanchor,scan,anchor]) ->
	  State = cut,
	  Out = N
	; Kind = foot ->
	  Out = N,
	  ( recorded( tig(Tree,Right) ) ->
	    State = keep
	  ; State = cut
	  )
	; domain(Kind,[escape,subst]) ->
	  State = keep,
	  Out = N
	; 	  
	  lctag_simplify(Tree,Children,State,OutChildren),
	  Out = tag_node{ id => Id,
			  top => Top,
			  bot => Bot,
			  label => L,
			  spine => Spine,
			  adj => Adj,
			  children => OutChildren,
			  adjleft => AdjLeft,
			  adjright => AdjRight,
			  adjwrap => AdjWrap,
			  token => Token,
			  kind => Kind
			}
	)
	.


lctag_simplify(Tree,
	       (N1 ## N2),
	       State,
	       (XN1 ## XN2)
	      ) :-
	lctag_simplify(Tree,N1,State1,XN1),
	lctag_simplify(Tree,N2,State2,XN2),
	( (State1 = cut, State2 =cut) -> State = cut ; State = keep )
	.

lctag_simplify(Tree,
	       (N1 ; N2),
	       State,
	       (XN1 ; XN2)
	      ) :-
	lctag_simplify(Tree,N1,State1,XN1),
	lctag_simplify(Tree,N2,State2,XN2),
	( (State1 = cut,State2 =cut) -> State = cut ; State = keep )
	.

lctag_simplify(Tree,
	       (N1 , N2),
	       State,
	       XN
	      ) :-
	lctag_simplify(Tree,N1,State1,XN1),
	( State1 = cut ->
	  XN = XN1,
	  State = cut
	; 
	  lctag_simplify(Tree,N2,State,XN2),
	  XN = (XN1,XN2)
	)
	.

lctag_simplify(Tree,
	       [N1|Rest],
	       State,
	       XN
	      ) :-
	lctag_simplify(Tree,N1,State1,XN1),
	( State1 = cut ->
	  XN = [XN1],
	  State = cut
	; 
	  lctag_simplify(Tree,Rest,State,XRest),
	  XN = [XN1|XRest]
	)
	.

lctag_simplify(Tree,[],keep,[]).
lctag_simplify(Tree,K:: '$pos'(_),keep,K).
lctag_simplify(Tree,K:: '$skip',keep,K).

lctag_simplify(Tree, @*{ goal => Goal },keep, @*{ goal => XGoal } ) :-
	lctag_simplify(Tree,Goal,State1,XGoal).

lctag_simplify(Tree,guard{ goal => A, plus => Plus, minus => Minus },State,guard{ goal => XA, plus => Plus, minus => Minus }) :-
	lctag_simplify(Tree,A,StateA,XA),
	( Minus = fail -> State = StateA ; State = keep )
	.

:-mode(lctag_closure/4,+(-,-,-,-)).

lctag_closure(X,First,Tree,Trees) :-
    ('$answers'(normalize(T::tag_tree{ name => Tree },
			     NT::tag_node{ top => Top,
					   bot => Bot,
					   label => L,
					   spine => Spine,
					   adj => Adj,
					   children => Children,
					   adjleft => AdjLeft,
					   adjright => AdjRight}
			 ))
     ;
     '$answers'(normalize(T, guard{ goal =>  NT})),
%%     format('%% auxtree with guard ~w\n',[Tree]),
     true
    ),
	apply_feature_constraints(L,Top,_Bot),
	( recorded( tig(Tree,left) ) ->
	  (Top=Bot, Adj = not_required_adj[] ->
	   XNT = Children
	  ;
	   AdjLeft = possible_adj[],
	   XNT = NT
	  ),
	  tree_foot(NT, tag_node{ bot => _Bot }),
	  apply_feature_mode_constraints(L,left,Top,_Bot),
	  X = adjleft(L,Top,_Bot)
	; recorded( tig(Tree,right) ) ->
	  ( Top=Bot, Adj = not_required_adj[] ->
	    XNT = Children
	  ;
	    AdjRight = possible_adj[],
	    XNT = NT
	  ),
	  tree_foot(NT, tag_node{ bot => _Bot }),
	  apply_feature_mode_constraints(L,right,Top,_Bot),
	  X = adjright(L,Top,_Bot)
	; Spine = yes ->
	  %% wrapping adj tree
	  tree_foot(NT, tag_node{ bot => _Bot }),
	  apply_feature_mode_constraints(L,wrap,Top,_Bot),
	  X = adjleft(L,Top,_Bot),
	  XNT = NT
	;
	  X = subst(L,Top),
	  XNT = NT
	),
	lctag_first(Tree,XNT,_First,Trees),
	( _First = true -> First = epsilon
%	;  _First = (_First1 + no_epsilon) -> First = _First
	; _First = (_First1 + lctag_labels[epsilon,true]) -> First = (_First1 + epsilon)
	;
	  First = _First
	),
%	format('%% lctag closure ~w first=~w trees=~w tree=~w\n',[X,First,Trees,Tree]),
	true
	.

:-mode(right_top_bot_constraints/3,+(+,-,-)).

right_top_bot_constraints(L,Top,Bot) :-
	lctag_closure(adjright(L,Top,Bot),_,_,_)
	.

:-std_prolog lctag_first_scan/2.

lctag_first_scan(Scan,First) :-
	( Scan = [] ->
	  First = epsilon
	; Scan = [A|_], var(A) ->
	  First = scan(A)
	; Scan = [(A1;A2)|_] ->
	  lctag_first_scan((A1;A2),First)
	; Scan = (A1;A2) ->
	  ( lctag_first_scan(A1,First)
	  ;  lctag_first_scan(A2,First)
	  )
	; Scan = [A|_],
	  First = scan(A)
	)
	.

%%:-light_tabular lctag_first/3.

%:-mode(lctag_first/4,+(+,+,+,-,-)).
:-prolog lctag_first/4.

lctag_first(Tree,
	    Node::tag_node{ kind => Kind,
			    label => L,
			    top => Top,
			    bot => Bot,
			    children =>  Children,
			    adj => Adj,
			    adjleft => AdjLeft,
			    adjright => AdjRight,
			    token => Token,
			    id => Id
			  },
	    First,
	    Trees
	   ) :-
	%%	format('%% lctag first tree=~w kind=~w label=~w\n',[Tree,Kind,L]),
	( Kind = scan ->
	  lctag_first_scan(L,First),
	  Trees=[],
%%	  format('lctag_first_scan ~w -> ~w\n',[L,First]),
	  true
	; Kind = coanchor ->
	  ( Adj = possible_adj[],
	    AdjLeft = possible_adj[],
	    apply_feature_mode_constraints(L,left,Top,_Bot),
	    ( recorded(lctag_safe(L)) xor Bot = _Bot),
	    lctag_closure(adjleft(L,Top,_Bot),First,TAdj,_Trees),
	    small_sorted_tree_add(TAdj,_Trees,Trees)
	  ;
	    Top=Bot,
	    ( var(Token) ->
	      First = coanchor(L,Bot)
	    ;
	      First = scan(Token)
	    ),
	    Trees=[]
	  )
	; Kind = anchor ->
	  (Adj = possible_adj[],
	   AdjLeft = possible_adj[],
	   apply_feature_mode_constraints(L,left,Top,_Bot),
	   ( recorded(lctag_safe(L)) xor Bot = _Bot),
	   lctag_closure(adjleft(L,Top,_Bot),First,TAdj,_Trees),
	   small_sorted_tree_add(TAdj,_Trees,Trees)
	  ;
	   Top = Bot,
	   ( var(Token) ->
	     First = anchor(L,Bot)
	   ;
	     First = scan(Token)
	   ),
	   Trees = []
	  )
	; Kind = subst ->
	  Top=Bot,
	  lctag_closure(subst(L,Top),First,TSubst,_Trees),
	  small_sorted_tree_add(TSubst,_Trees,Trees)
	; Kind = foot ->
	  Trees = [],
	  ( recorded( tig(Tree,right) ) ->
	    First = epsilon
	  ;
	    First = true	% block the exploration at the foot node (will not traverse after the foot node)
	  )
	; Kind = escape ->
	  ( L=noop(X) ->
	    noop_first(X,First)
	  ;
	    First = epsilon
	  ),
	  Trees = []
	; functor(Top,F,N),
	  ( Adj = possible_adj[],
	    AdjLeft =  possible_adj[],
	    apply_feature_mode_constraints(L,left,Top,_Bot),
	    ( recorded(lctag_safe(L)) xor \+ Top = Bot xor Bot = _Bot),
%%	    format('%% lctag feature constraint ~w L=~w Top=~w Bot=~w\n',[F,L,Top,Bot]),
	    ( %% for left and wrapping adjoining
	      mutable(M,false),
%	      lctag_closure(adjleft(L,Top,_Bot),_First,TAdj,_Trees),
%	      format('try L=~w top=~w _bot=~w\n',[L,Top,_Bot]),
	      lctag_closure(adjleft(L,Top,_Bot),_First,TAdj,_Trees),
%	      format('found for L=~w -> top=~w __bot=~w _first=~w\n',[L,Top,__Bot,_First]),
	      (  % _First \== true,
		_First \== epsilon,
		\+ (_First = (_ + lctag_labels[epsilon,true])),
%		\+ (_First = pos(_)),
%		\+ (_First = postscan(_)),
%		\+ (_First = postcat(_,_)),
		true
		->
		First = _First,
		%% format('%% adjleft ~w ~w ~w -> first=~w TAdj=~w\n',
		%% [L,Top,Bot,First,TAdj]),
		small_sorted_tree_add(TAdj,_Trees,Trees)
	      ;
		 mutable_read(M,false),
		 mutable(M,true),
		 lctag_first(Tree,Children,First,Trees)
	      )
%%	      Trees = []
	    ; %% for right adjoining,
	      recorded(adjkind(F/N,right)),
	      AdjRight = possible_adj[],
	      %% find constraints on Top and Bot
	      %% coming from potential future right adjoining
	      %% ** seems to be too costly
	      %% lctag_closure(adjright(L,Top,Bot),_,_,_),
	      right_top_bot_constraints(L,Top,Bot),
	      lctag_first(Tree,Children,_First,__Trees),
	      ( _First = epsilon ->
		%% when the children cover the empty string
		%% we continue with a right adjoing
		lctag_closure(adjright(L,Top,Bot),First,TAdj,_Trees),
		small_sorted_tree_add(TAdj,_Trees,Trees)
	      ; %% when the children cover a non-empty string
		%% we take its left corner
		First = _First,
		Trees = __Trees
	      )
	    )
	  ;
	    %% We search left-corner with no adjoining on Node
	    Adj = not_required_adj[],
	    Top = Bot,
	    lctag_first(Tree,Children,First,Trees),
	    true
	  ),
%%	  format('%% lctag first first=~w trees=~w nid=~w tree=~w\n',[First,Trees,Id,Tree]),
	  true
	),
%%	format('%% lctag first first=~w trees=~w tree=~w\n',[First,Trees,Tree]),
	true
	.

:-std_prolog noop_first/2.

noop_first(L,First) :-
	( L = scan(X) ->
	  First = L
	; L = pos(X) ->
	  First = pos(X)
	; L = postscan(X) ->
	  domain(_Token,X),
	  First = postscan(_Token)
	; L = postcat(X) ->
	  domain(_Cat,X),
	  ( recorded( tag_features(_Cat,_Top,_Bot) ) xor _Bot = _Cat ),
	  First = postcat(_Cat,_Bot)
	; L = (L1;L2) ->
	  ( noop_first(L1,First)
	  ; noop_first(L2,First)
	  )
	; domain(_Cat,L),
	  ( recorded( tag_features(_Cat,_Top,_Bot) ) xor _Bot = _Cat ),
	  First = coanchor(_Cat,_Bot)
	)
	.

lctag_first(Tree,
	    (N1 ## N2),
	    First,
	    Trees
	   ):-
	( lctag_first(Tree,N1,First,Trees)
	;
	  lctag_first(Tree,N2,First,Trees)
	)
	.

lctag_first(Tree,
	    (N1 ; N2),
	    First,
	    Trees
	   ) :-
	( lctag_first(Tree,N1,First,Trees)
	; lctag_first(Tree,N2,First,Trees)
	)
	.

lctag_first(Tree,
	    (N1,N2),
	    First,
	    Trees
	   ) :-
	lctag_first(Tree,N1,First1,Trees1),
	( First1 = epsilon ->
	  lctag_first(Tree,N2,First,Trees)
	; domain(First1,[pos(_),postcat(_,_),postscan(_), (_ + lctag_labels[epsilon,true])]) ->
	  ( First1 = (_First1 + lctag_labels[epsilon,true]) xor First1=_First1),
	  Trees = Trees1,
	  lctag_first(Tree,N2,First2,Trees2),
	  ( First2=lctag_labels[epsilon,true]->
	    First = _First1 + First2
	  ;
%	    First = _First1 + no_epsilon
	    First = First2
	  )
	;
	  First = First1,
	  Trees=Trees1
	)
	.

lctag_first(Tree,
	    [N1|Rest],
	    First,
	    Trees
	   ) :-
%	format('%% try reduce tree=~w N1=~w\n',[Tree,N1]),
	lctag_first(Tree,N1,First1,Trees1),
%	format('%% \t step1 tree=~w N1=~w first1=~w\n',[Tree,N1,First1]),
	( First1=lctag_labels[epsilon] ->
	  lctag_first(Tree,Rest,First,Trees)
	; domain(First1,[pos(_),postcat(_,_),postscan(_),(_ + lctag_labels[epsilon])]) ->
	  ( First1 = (_First1 + lctag_labels[epsilon]) xor First1=_First1),
	  Trees = Trees1,
	  lctag_first(Tree,Rest,First2,Trees2),
	  ( First2=lctag_labels[epsilon,true]->
	    First = _First1 + epsilon
	  ;
%	    First = _First1 + no_epsilon
	    First = First2
	  )
	;
	  First = First1,
	  Trees=Trees1
	),
%	format('%% => reduce tree=~w N1=~w first1=~w first2=~w first=~w\n',[Tree,N1,First1,First2,First]),
	true
	.

lctag_first(Tree,[],epsilon,[]).

lctag_first(Tree,'$pos'(_),epsilon,[]).

lctag_first(Tree,'$skip',epsilon,[]).

lctag_first(Tree,
	    @*{ goal => Goal },
	    First,
	    Trees
	   ) :-
	( First = epsilon,
	  Trees = []
	;
	  lctag_first(Tree,Goal,First,Trees)
	)
	.

lctag_first(Tree,
	    guard{ goal => A, plus => Plus, minus => Minus },
	    First,
	    Trees
	   ) :-
    ( lctag_first(Tree,A,First,Trees),
      %% the positive guard should succeed at least one, but we don't instantiate the variables
      (\+ \+ guard_eval(Plus) -> true ; fail)
     ;
     First = epsilon,
     Trees=[],
     %(guard_eval(Minus) xor fail)
     guard_eval(Minus)
    )
	.

:-extensional small_sorted_tree_add/3.
%%small_sorted_tree_add(T,_,[T]).
%%small_sorted_tree_add(T,_,[]).

small_sorted_tree_add(T,[],[T]).
small_sorted_tree_add(_,[T],[T]).

%:-std_prolog small_sorted_tree_add/3.
/*
small_sorted_tree_add(T,L,XL) :-
	( L = [_] -> XL = L
	; sorted_tree_add(T,L,XL)
	)
	.
*/
:-rec_prolog sorted_tree_add/3.

sorted_tree_add(T,[],[T]).
sorted_tree_add(T, L::[T1|M],XL) :-
	( T==T1 -> XL=L
	; T @< T1 -> XL=[T|L]
	; sorted_tree_add(T,M,XM),
	  XL=[T1|XM]
	)
	.

:-std_prolog guard_eval/1.

guard_eval(Guard) :-
	( Guard = true ->
	  true
	; Guard =  fail ->
	  fail
	;
	  Guard = (G1,G2) ->
	  guard_eval(G1),
	  guard_eval(G2)
	; Guard = (G1;G2) ->
	  ( guard_eval(G1)
	  ; guard_eval(G2)
	  )
	; Guard = (\+ \+ X=Y) ->
	  \+ \+ X=Y
	; Guard = (X=X)	
	)
	.
