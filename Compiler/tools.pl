/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 1998 - 2004, 2008, 2009, 2011 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  tools.pl -- Misc. Tools used by DyALog Compiler
 *
 * ----------------------------------------------------------------
 * Description
 * 
 * ----------------------------------------------------------------
 */

:-include 'header.pl'.
:-require 'oset.pl'.

append([],Y,Y).
append([A|X],Y,[A|Z]) :- append(X,Y,Z).

%:-mode(reverse/3,+(+,+,-)).
%:-lco reverse/3.

:-std_prolog reverse/3.

reverse(L,LL,Acc) :-
	( L=[A|B] -> reverse(B,[A|LL],Acc) ; Acc=LL)
	.

record_without_doublon( A ) :-
	(recorded( A ) xor record( A ))
	.

update_counter(Name,V) :- 
	( recorded( counter(Name,M) ) ->
	    mutable_inc(M,V)
	;   V=0,
	    mutable(M,1),
	    record( counter(Name,M) )
	)
	.

value_counter(Name,V) :-
	( recorded( counter(Name,M) ) ->
	    mutable_read(M,V)
	;   
	    V = 0
	)
	.

reset_counter(Name,V) :-
	( recorded( counter(Name,M) ) ->
	    mutable(M,V)
	;   
	    mutable(M,V),
	    record( counter(Name,M) )
	)
	.
	
length_ordered_table( Name, V ) :-
	( recorded( ordered_table(Name,MV,_,_) )->
	    mutable_read(MV,V)
	;   
	    V = 0
	).

add_ordered_table(Name,Data,V) :-
	( recorded( ordered_table(Name,MV,FirstHandle,LastHandle) ) ->
	    mutable_read(LastHandle,Last),
	    (mutable_read(Last,elem(_,_,Next)) xor Last = Next )
	;   
	    mutable(MV,0),
	    mutable(Last,[]),
	    mutable(Next,[]),
	    mutable(FirstHandle,Next),
	    mutable(LastHandle,Next),
	    record( ordered_table(Name,MV,FirstHandle,LastHandle) )
	),
	mutable_inc(MV,V),
	record( Data , Obj ),
	mutable(New,[]),
	mutable(Next,elem(Obj,Last,New)),
	mutable_read(FirstHandle,First),
%%	format('Add ordered ~w ~w: ~w/~w ~w ~w First=~w\n',[Name,Next,Obj,Data,Last,New,First]),
	mutable(LastHandle,Next)
	.


reset_ordered_table(Name) :-
	( recorded( ordered_table(Name,MV,FirstHandle,LastHandle) ) ->
	    mutable(MV,0),
	    mutable(Last,[]),
	    mutable(Next,[]),
	    mutable(FirstHandle,Next),
	    mutable(LastHandle,Next)
	;
	    true
	)
	.

ordered_table(Name,Data) :-
	( recorded( ordered_table(Name,_,FirstHandle,_) ) ->
	    mutable_read(FirstHandle,First),
	    follow_ordered_table(First,Data)
	;   
	    fail
	)
	.

consume_ordered_table(Name,Data) :-
	( recorded( ordered_table(Name,MV,FirstHandle,LastHandle) ) ->
	    mutable_read(FirstHandle,First),
	    %% Reset ordered_table
	    mutable(MV,0),
	    mutable(Last,[]),
	    mutable(Next,[]),
	    mutable(FirstHandle,Next),
	    mutable(LastHandle,Next),
	    %%
	    follow_ordered_table(First,Data)
	;   
	    fail
	)
	.

:-std_prolog follow_ordered_table/2.

follow_ordered_table(X,Data) :-
%	format('Follow ~w\n',[X]),
	mutable_read(X,elem(Obj,_,Next)),
	(
	    recorded( Data, Obj )
	;
	    follow_ordered_table(Next,Data)
	)
	.

reverse_ordered_table(Name) :-
	( recorded( ordered_table(Name,ML,MV) ) ->
	    mutable_read(ML,L),
	    reverse(L,[],RL)
	;
	    V=0,
	    mutable(MV,V),
	    RL=[]
	),
	mutable(ML,RL)
	.

holes(T) :- '$binder'(T,'$$HOLE$$').

error(Msg,Args) :-
	write(2,'dyalog: ' ),
	format(2,Msg,Args),
	nl(2),
	exit(1)
	.

internal_error(Msg,Args) :-
	write(2,'dyalog: internal error : '),
	format(2,Msg,Args),
	nl(2),
	exit(1)
	.

warning(Msg,Args) :-
	( recorded( warning ) ->
	    write(2,'dyalog: warning : '),
	    format(2,Msg,Args),
	    nl(2)
	;
	    true
	)
	.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

reg_value('&reg'(N),N).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Computing Tupple for numbervar-ized terms


check_var('$VAR'(N)) :- number(N).
check_deref_var('$VAR'('$VAR'(N),_)) :- number(N).

check_tupple('$TUPPLE'(N)) :- number(N).

check_fun_tupple('$FUNTUPPLE'(F,T)) :- atom(F), check_tupple(T).

zero_var('$VAR'(N)) :- N=0.
null_tupple('$TUPPLE'(N)) :- N=0.

%% should use a specialized C function for testing
/*
var_in_tupple('$VAR'(I),'$TUPPLE'(T)) :-
	number_to_oset(I,TI),
	oset_inter(TI,T,T2),
	\+ T2 = 0
	.
*/

var_in_tupple('$VAR'(I),'$TUPPLE'(T)) :-
	'$interface'( 'oset_member'(I:int,T:ptr),[]).

/*
tupple( T, '$TUPPLE'(R_Out) ) :-
	'$interface'( 'Numbervar_Tupple'(T:term), [return(Out:int)] ),
	oset_register(Out,R_Out)
	.
*/

tupple( T, '$TUPPLE'(R_Out) ) :-
	('$interface'( 'Numbervar_Tupple_And_Register'(T:term), [return(_R_Out:ptr)] ) xor _R_Out=0),
	_R_Out=R_Out
	.

:-std_prolog check_light_tupple/2.

check_light_tupple(T,S) :-
	( T='$TUPPLE'(S) xor error( 'Tupple expected in compact form: ~w',[T]) )
	.

union(T1,T2,T3) :-
	check_light_tupple(T1,S1),
	check_light_tupple(T2,S2),
	check_light_tupple(T3,S3),
	oset_union(S1,S2,S3).

inter(T1,T2,T3) :-
	check_light_tupple(T1,S1),
	check_light_tupple(T2,S2),
	check_light_tupple(T3,S3),
	oset_inter(S1,S2,S3).

delete(T1,T2,T3) :-
	check_light_tupple(T1,S1),
	check_light_tupple(T2,S2),
	check_light_tupple(T3,S3),
	oset_delete(S1,S2,S3).

oset_tupple(T,Tupple) :-
	check_light_tupple(T,Set),
	'$interface'( 'oset_numbervarlist'(Set:ptr), [return(Tupple:term)])
	.

/*
extend_tupple(Term,T1,T3) :-
	tupple(Term,T2),
	union(T2,T1,T3)
	.
*/

extend_tupple(Term,'$TUPPLE'(T1),'$TUPPLE'(T2)) :-
	('$interface'('Tupple_Extend'(Term:term,T1:ptr),[return(_T2:ptr)]) xor _T2=0),
	T2=_T2
	.

/*
tupple_delete(T1,Args,T2) :-
	tupple(Args,TArgs),
	delete(T1,TArgs,T2)
	.
*/

tupple_delete('$TUPPLE'(T1),Args,'$TUPPLE'(T2)) :-
	( '$interface'('Tupple_Delete'(Args:term,T1:ptr),[return(_T2:ptr)]) xor _T2=0),
	T2=_T2
	.

tupple_delete_wrapper('$TUPPLE'(Delete),A,'$TUPPLE'(Delete,A)).


duplicate_vars(List,Vars) :-
	null_tupple(Null),
	mutable(MSeen,Null),
	mutable(MDup,Null),
	every(( domain(X,List),
		tupple(X,TX),
		mutable_read(MSeen,Seen),
		inter(TX,Seen,Dup1),
		mutable_read(MDup,Dup),
		union(Dup1,Dup,Dup2),
		mutable(MDup,Dup2),
		union(Seen,TX,Seen2),
		mutable(MSeen,Seen2)
	      )),
	mutable_read(MDup,Vars)
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Associating new variables

my_numbervars(T,Id,M) :-
	( var(Id) ->
	    gensym(Id),
	    numbervars(T,1,M),
	    mutable(MM,M),
	    record( numbervars(Id,MM) )
	;   
	    recorded( numbervars(Id,MN) ),
	    mutable_read(MN,N),
	    numbervars(T,N,M),
	    mutable(MN,M)
	)
	.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Generate minimal information about the tail of a transition

%% Right: continuation
%% Env: Information to keep to go down in the stack
%% Tupple: List of variables seen so far
gen_tail(Right_Env(Tupple),Tail) :-
	Right_Env =.. [apply,Right|Env],
	%% format('Entering Gen Tail\n',[]),
	tupple(Right,T_Right),
	inter(Tupple,T_Right,T1),
	tupple(Env,T_Env),
	union(T_Env,T1,T_Trans),
	%% format('Gen Tail ~w ~w ~w\n\t -> ~w\n\n',[Right,Env,Tupple,T_Trans]),
	update_counter(nabla,N),
%	atom_number(N,Id),
%	default_module(N,Module_N),
%	atom_concat(F,N,NN),
	recorded( main_file(_,info(_,Module,_,_)) ),
	Tail =.. [Module,N,T_Trans]
%	recorded( main_file(_,info(_,F,_,_)) ),
%	Tail =.. [F,Id,T_Trans]
	.

new_gen_tail(FullRight,K,Tail,Tail_Flag) :-
	( FullRight = (IndexingKey ^ Right_Env(Tupple))
	xor
	FullRight = Right_Env(Tupple),
	  IndexingKey = []
	),
	Right_Env =.. [apply,Right|Env],
	%% format('Entering Gen Tail\n',[]),
	tupple(Right,T_Right),
	inter(Tupple,T_Right,T1),
	tupple(Env,T_Env),
	union(T_Env,T1,T2),
	tupple(K,TK),
	delete(T2,TK,T_Trans1),
	( Right = '*HIDE-ARGS*'(Args,_Right) ->
	  tupple(Args,TArgs),
	  delete(T_Trans1,TArgs,T_Trans)
	;
	  T_Trans = T_Trans1
	),
	%% format('Gen Tail ~w ~w ~w\n\t -> ~w\n\n',[Right,Env,Tupple,T_Trans]),
	update_counter(nabla,N),
	recorded( main_file(_,info(_,Module,_,_)) ),
	( %% fail,
	  IndexingKey = [],
	  Tail_Flag == flat ->
	  name_builder('~w_~w',[Module,N],Name),
%	  oset_tupple(T_Trans,XT_Trans),
%	  Tail =.. [Name|XT_Trans]
	  ( null_tupple(T_Trans) ->
	    Tail = Name
	  ;
	    Tail = '$FUNTUPPLE'(Name,T_Trans)
	  )
	; IndexingKey = [] ->
	  Tail =.. [Module,N,T_Trans]
	;
	  Tail =.. [Module,N,IndexingKey,T_Trans]
	),
	oset_tupple(T_Trans,XT_Trans),
%%	format('Gen Tail ~w ~w ~w\n\t -> ~w ~w\n\n',[Right,Env,Tupple,Tail,XT_Trans]),
	true
	.

default_module(Name,Module_Name) :-
	recorded( main_file(_,info(_,_,_,Module_Prefix)) ),
	name_builder('~w~w',[Module_Prefix,Name],Module_Name)
%	atom_concat(Module_Prefix,Name,Module_Name)
	.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

name_builder(Format,Args,Name) :-
	string_stream(_,S),
	format(S,Format,Args),
	flush_string_stream(S,Name),
	close(S)
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

abolish(F/N) :- '$interface'( 'Abolish'(F:term,N:int), [return(none)]).

foreign_arg(T,I,A) :- '$interface'( 'DyALog_Arg'(T: term, I: -int, A: term),
			      [ choice_size(2) ] ).

enumerate(A,B,C) :-
	'$interface'( 'DyALog_Count'(A:int,B:int, C: -int),
			  [ choice_size(0) ] )
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Module handling

term_module_get(T1,Module) :-
	(   functor(T1,F,_), atom_module(F,Module,_) xor Module = [] ).


term_current_module_shift(Pred1,Pred2,T1,T2) :-
	module_get(Module),
	(   term_module_shift(Module,Pred1,Pred2,T1,T2)
	xor T1 = T2, Pred1=Pred2
	),
	%%	format('current module shift ~w ~w -> ~w\n',[Module,T1,T2]),
	true
	.

%% F is of the form M1|M2!..!FF
%% Then ShiftF of the form Module!M1!M2!..!FF
%% However, if Module is a prefix of M1!M2!.., it is not added
deep_module_shift(F,Module,ShiftF) :-
	atom_module(F,Module1,F1),
	( Module1 == [] ->
	    atom_module(ShiftF,Module,F)
	;   Module == Module1 -> %% dont deep shift if same module
	    ShiftF = F
	;   
	    deep_module_shift(Module1,Module,ShiftModule1),
	    atom_module(ShiftF,ShiftModule1,F1)
	)
	.

%term_current_module_shift(Pred,Pred,T,T).


