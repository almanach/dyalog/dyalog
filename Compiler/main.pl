/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 1998, 2002, 2003, 2004, 2007, 2008, 2010, 2011 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  main.pl -- Main file for DyALog Compiler
 *
 * ----------------------------------------------------------------
 * Description
 * 
 * ----------------------------------------------------------------
 */

:-include 'header.pl'.

:-require 'config.pl'.
:-require 'tools.pl'.
:-require 'maker.pl'.
:-require 'reader.pl'.
:-require 'lpda.pl'.
:-require 'bmg.pl'.
:-require 'dcg.pl'.
:-require 'rcg.pl'.
:-require 'seed.pl'.
:-require 'application.pl'.
:-require 'term.pl'.
:-require 'code.pl'.
:-require 'code_emit.pl'.
:-require 'ma_emit.pl'.
:-require 'emit.pl'.
:-require 'stdprolog.pl'.
:-require 'foreign.pl'.
:-require 'lc.pl'.
:-require 'tig.pl'.
:-require 'toplevel.pl'.

%% For TAGs
:-require 'tag_compile.pl'.
:-require 'tag_dyn.pl'.
:-require 'tagguide_compile.pl'.
:-require 'tag_features.pl'.


?- wait(compile), ( exists( compile ) -> exit(0) ; error('Compilation failed', [] ) ).

compile :-
	compiler_init,
	wait( gen_application(_) ),
	argv( Argv ),
	phrase( parse_options, Argv, [] ),
	\+ recorded( stop_compilation_at(load) ),
	( recorded( toplevel_mode ) ->
	    toplevel_loop,
	    exit(0)
	;
	    true
	),
	( recorded( main_file(_,_) ) ->
	    ( recorded(stop_compilation_at(pgm)) ->
		wait(( get_clause(Clause),
		       analyze_at_clause(Clause))),
		emit_at_pda
	    ;	recorded(stop_compilation_at(pda)) ->
		format('Stop at PDA\n',[]),
		wait(( get_clause(Clause),
		       pgm_to_lpda(Clause,Trans),
		       analyze_at_pda(Trans)
		     )),
		emit_at_pda
	    ;	
		wait( (
			  (   get_clause(Clause),
			      pgm_to_lpda(Clause, Trans )
			  ;   %% New clauses may be built by pgm_to_lpda
			      get_extra_clause(Clause),
			      pgm_to_lpda(Clause, Trans)
			  ),
%%			  format( 'Trans ~w\n\n',[Trans]),
			  ( Trans = '*STD-PROLOG-FIRST*'(_,_,_) ->
			      stdprolog_install(Trans)
			  ;   
			      init_install(Trans)
			  )
		      )),
		%%	    code_analysis,
		\+ recorded(stop_compilation_at(emit)),
		emit,
		true
	    )
	;
	    true
	)
	.

:-prolog compiler_init/0,pgm_to_lpda_init/0.

compiler_init :-
	record( parse_mode(list) ),
	record( compiler_analyzer(none) ),
	mutable( MKind, dcg ),
	record( grammar_kind(MKind) ),
%%	record( extensional('C'/3) ),
	record( bmg_stacks([]) ),
	wait( read_files('dyalogrc',resource) ),
	%% TAG initializations
        op(800,fx,[tree,auxtree,spinetree]),
	op(710,xfx,[at]),
	op(705,xfy,[and]),
        op(399,fx,[*,**,=,++,<>,<=>])
	.

:-std_prolog main_file/1.

main_file(File) :-
	decompose_file_name(File,Dir,Base,Extension),
	atom_concat(Base,':',Module_Prefix),
	erase( main_file(_,_) ),
	record( main_file(File,info(Dir,Base,Extension,Module_Prefix)) )
	.

/* Magic Hack to build DyALog option usage
   may have to compile twice to work properly !
*/

:-xcompiler
	dyalog_option(Option,Help) :-
		'$toplevel'((record( '$usage_info'(Option,Help) ) ))
		.

:-rec_prolog get_extra_clause/1.

get_extra_clause(Clause) :-
	recorded( Clause::'$usage_info'(Option,Help) ),
%%	format(';; found option ~w\n',[Clause]),
	true.
pgm_to_lpda(Clause::'$usage_info'(Option,Help),Trans) :-
	pgm_to_lpda('$fact'(usage_info(Option,Help)),Trans).

/* end of magic hack */

:-rec_prolog (dcg parse_options/0, parse_option/0).

parse_options -->  parse_option, parse_options; [].
parse_option -->
	( ['-f',File] ->
	    { dyalog_option('-f <file>','load input file <file>'),
	      main_file(File),
	      read_file_alt(File) }
	;   ['-o',File] ->
	    { dyalog_option('-o <file>','output file <file>'),
	      record( output_file(File) )
	    }
	;   ['-parse'] ->
	    { dyalog_option('-parse','\tset parse mode'),
	      erase( parse_mode(_) ), record( parse_mode(token) ) }
	;   ['-verbose_tag'] ->
	    { dyalog_option('-verbose_tag','set verbose mode for TAGs'),
	      record_without_doublon( verbose_tag ) }
	;   ['-I',Path] ->
	    { dyalog_option('-I <path>','add path to file search path'),
	      add_load_path(Path) }
	;   ['-res',File] ->
	    { dyalog_option('-res <file>','load resource file'),
	      read_files(File,resource) }
	;   ['-use',File] ->
	    { dyalog_option('-use <file>','load module file'),
	      read_files(File,require) }
	;   ['-version'] ->
	    { dyalog_option('-version','display compiler version'),
	      emit_compiler_info }
	;   ['--version'] ->
	    { dyalog_option('-version','display compiler version'),
	      emit_compiler_info }
	;   ['-tfs',File] ->
	    { dyalog_option('-tfs <lib>','load TFS library'),
	      load_tfs(File) }
	;   ['-rcg'] ->
	    { dyalog_option('-rcg','\tset rcg mode'),
	      recorded( grammar_kind(MKind) ), mutable(MKind,rcg) }
	;   ['-warning'] ->
	    { dyalog_option('-warning','set warning mode'),
	      record( warning ) }
	;   ['-stop',Level] ->
	    { dyalog_option('-stop <level>','stop compilation at <level> in {load,pda,emit}'),
	      record( stop_compilation_at(Level) ) }
	;   ['-guide',Mode] ->
	    { dyalog_option('-guide <mode>','compile with guide in {tag}'),
	      record_without_doublon( guide(Mode) ) }
	;   ['-autoload'] ->
	    { dyalog_option('-autoload','set autoload mode'),
	      record_without_doublon( autoload ) }
	;   ['-analyze',Mode] ->
	    { dyalog_option('-analyze <mode>','replace compilation by program analyze for mode in {lc, tag_features, tag2tig, lctag}'),
	      check_analyzer_mode(Mode) ->
	      erase( compiler_analyzer(none) ),
	      record_without_doublon( compiler_analyzer(Mode) )
	    ;	warning('not a valid analyzer mode: ~w',[Mode])
	    }
	;   ['-toplevel'] ->
	    { dyalog_option('-toplevel','enter toplevel loop instead of compiling'),
	      record_without_doublon( 'toplevel_mode' ) }
	;   ['-usage'] ->
	    { dyalog_option('-usage','\tdisplay this help and exit'),
	      usage}
	;   [File] ->
	    { dyalog_option('<file>','\tload file'),
	      main_file(File), read_file_alt(File) }
	;   
	    { fail }
	)
	.

:-std_prolog usage/0.
:-extensional usage_info/2.

usage :-
	emit_compiler_info,
	format('options:\n',[]),
	every((   usage_info(Option,Help),
		  format('\t~w\t~w\n',[Option,Help]) )),
	format('\n----------------------------------------------\ndyalog generic options\n',[]),
	'$interface'(generic_usage,[return(none)]),
	exit(1)
	.

:-std_prolog read_file_alt/1.

read_file_alt(File) :-
	( recorded( toplevel_mode ) ->
	    read_files(File,toplevel)
	;
	    read_files(File,include)
	)
	.

:-std_prolog emit_compiler_info/0.

emit_compiler_info :-
	compiler_info( compiler_info{ name=>Name, version=>Version, author=>Author, email=>Email }),
	format( '~w compiler version ~w\n~w <~w>\n',[Name,Version,Author,Email])
	.


:-std_prolog load_tfs/1.

load_tfs(File) :-
	(   atom(File) xor error( 'Not a valid filename ~w', [File]) ),
%	(   find_file(File,AF) xor error( 'File not found ~w',[File] )),
	AF=File,
	( recorded( tfs_file(F) ) ->
	    (	F == AF xor error( 'Not possible to open several TFS files (~w ~w)',[AF,F]))
	;   
	    tfs_dlopen(AF),
	    record( tfs_file(AF) )
	)
	.



