/* 
 ******************************************************************
 * $Id: compiler_sup.c 1443 2014-12-16 14:33:44Z clerger $
 * Copyright (C) 2000, 2002, 2008, 2014 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  compiler_sup.c -- Compiler Support
 *
 * ----------------------------------------------------------------
 * Description
 * Some C function to speed-up the compiler
 * ----------------------------------------------------------------
 */

#include <string.h>
#include <ctype.h>
#include "libdyalog.h"
#include "builtins.h"

typedef void * oset_t;

static Bool
check_numbervar( sfol_t A )
{
    if (FOLCMPP(A->t) && FOLCMP_FUNCTOR(A->t) == FOLNUMBERVAR) {
        fol_t N=FOLCMP_REF(A->t,1);
        fkey_t Sk(N)=A->k;
        Deref(N);
        if (FOLINTP(N))
            Succeed;
        else
            Fail;
    } else
        Fail;
}

/*
static Bool
check_internal( sfol_t A ) 
{
    int i;
    if (!FOLSMBP(A->t))
        Fail;
    for(i=0; i<11; i++) {
        if (A->t==DFOLSMB(i)){
            A->t=folcmp_create_unary("$internal",DFOLINT(i));
            Succeed;
        }
    }
    Fail;
}
*/

Bool
Compiler_Label_Elementary_Term( sfol_t A )
{
    SFOL_Deref(A);
    return (FOL_NUMBERP(A->t) || FOLCHARP(A->t) || check_numbervar(A));
}

char *
C_Normalize( char * s) 
{
    char *t=(char *) GC_MALLOC_ATOMIC( 3*strlen(s)+1 );
    char *p;
    char *q;
    for(q=s,p=t;*q ;q++) {
            /* **warning**: isalnum is locale-dependent
               in the present case, we should ensure that alnum is used within
               the C locale
             */
        if (!isalnum(*q) || !isascii(*q) || *q == '_') {
            p += sprintf(p,"_%hhX",*q);
        } else {
            *p++ = *q;
        }
    }
    *p=0;
        // printf("cnormalize %s -> %s\n",s,t);
    return t;
}
