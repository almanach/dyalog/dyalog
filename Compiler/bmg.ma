;; Compiler: DyALog 1.14.0
;; File "bmg.pl"


;;------------------ LOADING ------------

c_code local load
	call_c   Dyam_Loading_Set()
	pl_call  fun14(&seed[1],0)
	pl_call  fun14(&seed[4],0)
	pl_call  fun14(&seed[5],0)
	pl_call  fun14(&seed[0],0)
	pl_call  fun14(&seed[2],0)
	pl_call  fun14(&seed[6],0)
	pl_call  fun14(&seed[7],0)
	pl_call  fun14(&seed[11],0)
	pl_call  fun14(&seed[3],0)
	pl_call  fun14(&seed[8],0)
	pl_call  fun14(&seed[10],0)
	pl_call  fun14(&seed[9],0)
	pl_call  fun14(&seed[14],0)
	pl_call  fun14(&seed[12],0)
	pl_call  fun14(&seed[15],0)
	pl_call  fun14(&seed[13],0)
	pl_call  fun14(&seed[16],0)
	call_c   Dyam_Loading_Reset()
	c_ret


;;------------------ VIEWERS ------------

c_code local build_viewers
	call_c   Dyam_Load_Viewer(&ref[7],&ref[8])
	call_c   Dyam_Load_Viewer(&ref[3],&ref[4])
	c_ret

;; TERM 4: bmg_head_stacks(_A, _B, _C, _D, _E, _F)
c_code local build_ref_4
	ret_reg &ref[4]
	call_c   build_ref_142()
	call_c   Dyam_Term_Start(&ref[142],6)
	call_c   Dyam_Term_Arg(V(0))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[4]
	c_ret

;; TERM 142: bmg_head_stacks
c_code local build_ref_142
	ret_reg &ref[142]
	call_c   Dyam_Create_Atom("bmg_head_stacks")
	move_ret ref[142]
	c_ret

;; TERM 3: '*RITEM*'('call_bmg_head_stacks/6'(_A), return(_B, _C, _D, _E, _F))
c_code local build_ref_3
	ret_reg &ref[3]
	call_c   build_ref_0()
	call_c   build_ref_1()
	call_c   build_ref_2()
	call_c   Dyam_Create_Binary(&ref[0],&ref[1],&ref[2])
	move_ret ref[3]
	c_ret

;; TERM 2: return(_B, _C, _D, _E, _F)
c_code local build_ref_2
	ret_reg &ref[2]
	call_c   build_ref_143()
	call_c   Dyam_Term_Start(&ref[143],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[2]
	c_ret

;; TERM 143: return
c_code local build_ref_143
	ret_reg &ref[143]
	call_c   Dyam_Create_Atom("return")
	move_ret ref[143]
	c_ret

;; TERM 1: 'call_bmg_head_stacks/6'(_A)
c_code local build_ref_1
	ret_reg &ref[1]
	call_c   build_ref_144()
	call_c   Dyam_Create_Unary(&ref[144],V(0))
	move_ret ref[1]
	c_ret

;; TERM 144: 'call_bmg_head_stacks/6'
c_code local build_ref_144
	ret_reg &ref[144]
	call_c   Dyam_Create_Atom("call_bmg_head_stacks/6")
	move_ret ref[144]
	c_ret

;; TERM 0: '*RITEM*'
c_code local build_ref_0
	ret_reg &ref[0]
	call_c   Dyam_Create_Atom("*RITEM*")
	move_ret ref[0]
	c_ret

;; TERM 8: bmg_get_stacks(_A)
c_code local build_ref_8
	ret_reg &ref[8]
	call_c   build_ref_145()
	call_c   Dyam_Create_Unary(&ref[145],V(0))
	move_ret ref[8]
	c_ret

;; TERM 145: bmg_get_stacks
c_code local build_ref_145
	ret_reg &ref[145]
	call_c   Dyam_Create_Atom("bmg_get_stacks")
	move_ret ref[145]
	c_ret

;; TERM 7: '*RITEM*'('call_bmg_get_stacks/1', return(_A))
c_code local build_ref_7
	ret_reg &ref[7]
	call_c   build_ref_0()
	call_c   build_ref_5()
	call_c   build_ref_6()
	call_c   Dyam_Create_Binary(&ref[0],&ref[5],&ref[6])
	move_ret ref[7]
	c_ret

;; TERM 6: return(_A)
c_code local build_ref_6
	ret_reg &ref[6]
	call_c   build_ref_143()
	call_c   Dyam_Create_Unary(&ref[143],V(0))
	move_ret ref[6]
	c_ret

;; TERM 5: 'call_bmg_get_stacks/1'
c_code local build_ref_5
	ret_reg &ref[5]
	call_c   Dyam_Create_Atom("call_bmg_get_stacks/1")
	move_ret ref[5]
	c_ret

c_code local build_seed_16
	ret_reg &seed[16]
	call_c   build_ref_9()
	call_c   build_ref_13()
	call_c   Dyam_Seed_Start(&ref[9],&ref[13],I(0),fun1,1)
	call_c   build_ref_12()
	call_c   Dyam_Seed_Add_Comp(&ref[12],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[16]
	c_ret

;; TERM 12: '*GUARD*'(bmg_equations([], [], _B, _B))
c_code local build_ref_12
	ret_reg &ref[12]
	call_c   build_ref_10()
	call_c   build_ref_11()
	call_c   Dyam_Create_Unary(&ref[10],&ref[11])
	move_ret ref[12]
	c_ret

;; TERM 11: bmg_equations([], [], _B, _B)
c_code local build_ref_11
	ret_reg &ref[11]
	call_c   build_ref_146()
	call_c   Dyam_Term_Start(&ref[146],4)
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_End()
	move_ret ref[11]
	c_ret

;; TERM 146: bmg_equations
c_code local build_ref_146
	ret_reg &ref[146]
	call_c   Dyam_Create_Atom("bmg_equations")
	move_ret ref[146]
	c_ret

;; TERM 10: '*GUARD*'
c_code local build_ref_10
	ret_reg &ref[10]
	call_c   Dyam_Create_Atom("*GUARD*")
	move_ret ref[10]
	c_ret

pl_code local fun0
	call_c   build_ref_12()
	call_c   Dyam_Unify_Item(&ref[12])
	fail_ret
	pl_ret

pl_code local fun1
	pl_ret

;; TERM 13: '*GUARD*'(bmg_equations([], [], _B, _B)) :> []
c_code local build_ref_13
	ret_reg &ref[13]
	call_c   build_ref_12()
	call_c   Dyam_Create_Binary(I(9),&ref[12],I(0))
	move_ret ref[13]
	c_ret

;; TERM 9: '*GUARD_CLAUSE*'
c_code local build_ref_9
	ret_reg &ref[9]
	call_c   Dyam_Create_Atom("*GUARD_CLAUSE*")
	move_ret ref[9]
	c_ret

c_code local build_seed_13
	ret_reg &seed[13]
	call_c   build_ref_9()
	call_c   build_ref_16()
	call_c   Dyam_Seed_Start(&ref[9],&ref[16],I(0),fun1,1)
	call_c   build_ref_15()
	call_c   Dyam_Seed_Add_Comp(&ref[15],fun2,0)
	call_c   Dyam_Seed_End()
	move_ret seed[13]
	c_ret

;; TERM 15: '*GUARD*'(bmg_pop_loop([], _B, [], [], []))
c_code local build_ref_15
	ret_reg &ref[15]
	call_c   build_ref_10()
	call_c   build_ref_14()
	call_c   Dyam_Create_Unary(&ref[10],&ref[14])
	move_ret ref[15]
	c_ret

;; TERM 14: bmg_pop_loop([], _B, [], [], [])
c_code local build_ref_14
	ret_reg &ref[14]
	call_c   build_ref_147()
	call_c   Dyam_Term_Start(&ref[147],5)
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_End()
	move_ret ref[14]
	c_ret

;; TERM 147: bmg_pop_loop
c_code local build_ref_147
	ret_reg &ref[147]
	call_c   Dyam_Create_Atom("bmg_pop_loop")
	move_ret ref[147]
	c_ret

pl_code local fun2
	call_c   build_ref_15()
	call_c   Dyam_Unify_Item(&ref[15])
	fail_ret
	pl_ret

;; TERM 16: '*GUARD*'(bmg_pop_loop([], _B, [], [], [])) :> []
c_code local build_ref_16
	ret_reg &ref[16]
	call_c   build_ref_15()
	call_c   Dyam_Create_Binary(I(9),&ref[15],I(0))
	move_ret ref[16]
	c_ret

c_code local build_seed_15
	ret_reg &seed[15]
	call_c   build_ref_9()
	call_c   build_ref_19()
	call_c   Dyam_Seed_Start(&ref[9],&ref[19],I(0),fun1,1)
	call_c   build_ref_18()
	call_c   Dyam_Seed_Add_Comp(&ref[18],fun3,0)
	call_c   Dyam_Seed_End()
	move_ret seed[15]
	c_ret

;; TERM 18: '*GUARD*'(bmg_head_stacks_loop([], _B, _C, [], [], [], []))
c_code local build_ref_18
	ret_reg &ref[18]
	call_c   build_ref_10()
	call_c   build_ref_17()
	call_c   Dyam_Create_Unary(&ref[10],&ref[17])
	move_ret ref[18]
	c_ret

;; TERM 17: bmg_head_stacks_loop([], _B, _C, [], [], [], [])
c_code local build_ref_17
	ret_reg &ref[17]
	call_c   build_ref_148()
	call_c   Dyam_Term_Start(&ref[148],7)
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_End()
	move_ret ref[17]
	c_ret

;; TERM 148: bmg_head_stacks_loop
c_code local build_ref_148
	ret_reg &ref[148]
	call_c   Dyam_Create_Atom("bmg_head_stacks_loop")
	move_ret ref[148]
	c_ret

pl_code local fun3
	call_c   build_ref_18()
	call_c   Dyam_Unify_Item(&ref[18])
	fail_ret
	pl_ret

;; TERM 19: '*GUARD*'(bmg_head_stacks_loop([], _B, _C, [], [], [], [])) :> []
c_code local build_ref_19
	ret_reg &ref[19]
	call_c   build_ref_18()
	call_c   Dyam_Create_Binary(I(9),&ref[18],I(0))
	move_ret ref[19]
	c_ret

c_code local build_seed_12
	ret_reg &seed[12]
	call_c   build_ref_9()
	call_c   build_ref_22()
	call_c   Dyam_Seed_Start(&ref[9],&ref[22],I(0),fun1,1)
	call_c   build_ref_21()
	call_c   Dyam_Seed_Add_Comp(&ref[21],fun4,0)
	call_c   Dyam_Seed_End()
	move_ret seed[12]
	c_ret

;; TERM 21: '*GUARD*'(bmg_pop_code_loop([], _B, _C, [], [], _D, _D))
c_code local build_ref_21
	ret_reg &ref[21]
	call_c   build_ref_10()
	call_c   build_ref_20()
	call_c   Dyam_Create_Unary(&ref[10],&ref[20])
	move_ret ref[21]
	c_ret

;; TERM 20: bmg_pop_code_loop([], _B, _C, [], [], _D, _D)
c_code local build_ref_20
	ret_reg &ref[20]
	call_c   build_ref_149()
	call_c   Dyam_Term_Start(&ref[149],7)
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[20]
	c_ret

;; TERM 149: bmg_pop_code_loop
c_code local build_ref_149
	ret_reg &ref[149]
	call_c   Dyam_Create_Atom("bmg_pop_code_loop")
	move_ret ref[149]
	c_ret

pl_code local fun4
	call_c   build_ref_21()
	call_c   Dyam_Unify_Item(&ref[21])
	fail_ret
	pl_ret

;; TERM 22: '*GUARD*'(bmg_pop_code_loop([], _B, _C, [], [], _D, _D)) :> []
c_code local build_ref_22
	ret_reg &ref[22]
	call_c   build_ref_21()
	call_c   Dyam_Create_Binary(I(9),&ref[21],I(0))
	move_ret ref[22]
	c_ret

c_code local build_seed_14
	ret_reg &seed[14]
	call_c   build_ref_9()
	call_c   build_ref_25()
	call_c   Dyam_Seed_Start(&ref[9],&ref[25],I(0),fun1,1)
	call_c   build_ref_24()
	call_c   Dyam_Seed_Add_Comp(&ref[24],fun5,0)
	call_c   Dyam_Seed_End()
	move_ret seed[14]
	c_ret

;; TERM 24: '*GUARD*'(bmg_inside_island_loop([], _B, [], [], [], [], _C, _C))
c_code local build_ref_24
	ret_reg &ref[24]
	call_c   build_ref_10()
	call_c   build_ref_23()
	call_c   Dyam_Create_Unary(&ref[10],&ref[23])
	move_ret ref[24]
	c_ret

;; TERM 23: bmg_inside_island_loop([], _B, [], [], [], [], _C, _C)
c_code local build_ref_23
	ret_reg &ref[23]
	call_c   build_ref_150()
	call_c   Dyam_Term_Start(&ref[150],8)
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_End()
	move_ret ref[23]
	c_ret

;; TERM 150: bmg_inside_island_loop
c_code local build_ref_150
	ret_reg &ref[150]
	call_c   Dyam_Create_Atom("bmg_inside_island_loop")
	move_ret ref[150]
	c_ret

pl_code local fun5
	call_c   build_ref_24()
	call_c   Dyam_Unify_Item(&ref[24])
	fail_ret
	pl_ret

;; TERM 25: '*GUARD*'(bmg_inside_island_loop([], _B, [], [], [], [], _C, _C)) :> []
c_code local build_ref_25
	ret_reg &ref[25]
	call_c   build_ref_24()
	call_c   Dyam_Create_Binary(I(9),&ref[24],I(0))
	move_ret ref[25]
	c_ret

c_code local build_seed_9
	ret_reg &seed[9]
	call_c   build_ref_26()
	call_c   build_ref_28()
	call_c   Dyam_Seed_Start(&ref[26],&ref[28],I(0),fun10,1)
	call_c   build_ref_30()
	call_c   Dyam_Seed_Add_Comp(&ref[30],fun13,0)
	call_c   Dyam_Seed_End()
	move_ret seed[9]
	c_ret

;; TERM 30: '*CITEM*'('call_bmg_get_stacks/1', _A)
c_code local build_ref_30
	ret_reg &ref[30]
	call_c   build_ref_29()
	call_c   build_ref_5()
	call_c   Dyam_Create_Binary(&ref[29],&ref[5],V(0))
	move_ret ref[30]
	c_ret

;; TERM 29: '*CITEM*'
c_code local build_ref_29
	ret_reg &ref[29]
	call_c   Dyam_Create_Atom("*CITEM*")
	move_ret ref[29]
	c_ret

;; TERM 31: bmg_stacks(_C)
c_code local build_ref_31
	ret_reg &ref[31]
	call_c   build_ref_151()
	call_c   Dyam_Create_Unary(&ref[151],V(2))
	move_ret ref[31]
	c_ret

;; TERM 151: bmg_stacks
c_code local build_ref_151
	ret_reg &ref[151]
	call_c   Dyam_Create_Atom("bmg_stacks")
	move_ret ref[151]
	c_ret

c_code local build_seed_17
	ret_reg &seed[17]
	call_c   build_ref_0()
	call_c   build_ref_33()
	call_c   Dyam_Seed_Start(&ref[0],&ref[33],&ref[33],fun6,1)
	call_c   build_ref_34()
	call_c   Dyam_Seed_Add_Comp(&ref[34],&ref[33],0)
	call_c   Dyam_Seed_End()
	move_ret seed[17]
	c_ret

pl_code local fun6
	pl_jump  Complete(0,0)

;; TERM 34: '*RITEM*'(_A, return(_B)) :> '$$HOLE$$'
c_code local build_ref_34
	ret_reg &ref[34]
	call_c   build_ref_33()
	call_c   Dyam_Create_Binary(I(9),&ref[33],I(7))
	move_ret ref[34]
	c_ret

;; TERM 33: '*RITEM*'(_A, return(_B))
c_code local build_ref_33
	ret_reg &ref[33]
	call_c   build_ref_0()
	call_c   build_ref_32()
	call_c   Dyam_Create_Binary(&ref[0],V(0),&ref[32])
	move_ret ref[33]
	c_ret

;; TERM 32: return(_B)
c_code local build_ref_32
	ret_reg &ref[32]
	call_c   build_ref_143()
	call_c   Dyam_Create_Unary(&ref[143],V(1))
	move_ret ref[32]
	c_ret

pl_code local fun11
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Object(R(0))
	call_c   Dyam_Tabule()
	pl_ret

pl_code local fun12
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Choice(fun11)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Subsume(R(0))
	fail_ret
	call_c   Dyam_Cut()
	pl_ret

long local pool_fun13[4]=[3,build_ref_30,build_ref_31,build_seed_17]

pl_code local fun13
	call_c   Dyam_Pool(pool_fun13)
	call_c   Dyam_Unify_Item(&ref[30])
	fail_ret
	call_c   Dyam_Allocate(0)
	pl_call  Object_1(&ref[31])
	call_c   DYAM_evpred_length(V(2),V(3))
	fail_ret
	call_c   DYAM_evpred_length(V(1),V(3))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_jump  fun12(&seed[17],2)

pl_code local fun10
	pl_jump  Apply(0,0)

;; TERM 28: '*FIRST*'('call_bmg_get_stacks/1') :> []
c_code local build_ref_28
	ret_reg &ref[28]
	call_c   build_ref_27()
	call_c   Dyam_Create_Binary(I(9),&ref[27],I(0))
	move_ret ref[28]
	c_ret

;; TERM 27: '*FIRST*'('call_bmg_get_stacks/1')
c_code local build_ref_27
	ret_reg &ref[27]
	call_c   build_ref_26()
	call_c   build_ref_5()
	call_c   Dyam_Create_Unary(&ref[26],&ref[5])
	move_ret ref[27]
	c_ret

;; TERM 26: '*FIRST*'
c_code local build_ref_26
	ret_reg &ref[26]
	call_c   Dyam_Create_Atom("*FIRST*")
	move_ret ref[26]
	c_ret

c_code local build_seed_10
	ret_reg &seed[10]
	call_c   build_ref_9()
	call_c   build_ref_37()
	call_c   Dyam_Seed_Start(&ref[9],&ref[37],I(0),fun1,1)
	call_c   build_ref_36()
	call_c   Dyam_Seed_Add_Comp(&ref[36],fun19,0)
	call_c   Dyam_Seed_End()
	move_ret seed[10]
	c_ret

;; TERM 36: '*GUARD*'(bmg_get_stacks(_B, _C))
c_code local build_ref_36
	ret_reg &ref[36]
	call_c   build_ref_10()
	call_c   build_ref_35()
	call_c   Dyam_Create_Unary(&ref[10],&ref[35])
	move_ret ref[36]
	c_ret

;; TERM 35: bmg_get_stacks(_B, _C)
c_code local build_ref_35
	ret_reg &ref[35]
	call_c   build_ref_145()
	call_c   Dyam_Create_Binary(&ref[145],V(1),V(2))
	move_ret ref[35]
	c_ret

;; TERM 56: '$CLOSURE'('$fun'(18, 0, 1080120136), '$TUPPLE'(35141738826220))
c_code local build_ref_56
	ret_reg &ref[56]
	call_c   build_ref_55()
	call_c   Dyam_Closure_Aux(fun18,&ref[55])
	move_ret ref[56]
	c_ret

pl_code local fun18
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(2))
	move     V(3), R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  pred_my_numbervars_3()

;; TERM 55: '$TUPPLE'(35141738826220)
c_code local build_ref_55
	ret_reg &ref[55]
	call_c   Dyam_Create_Simple_Tupple(0,201326592)
	move_ret ref[55]
	c_ret

long local pool_fun19[3]=[2,build_ref_36,build_ref_56]

pl_code local fun19
	call_c   Dyam_Pool(pool_fun19)
	call_c   Dyam_Unify_Item(&ref[36])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[56], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Load(4,V(1))
	call_c   Dyam_Reg_Deallocate(3)
fun17:
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Reg_Bind(2,V(3))
	call_c   Dyam_Reg_Bind(4,V(2))
	call_c   Dyam_Choice(fun16)
	pl_call  fun8(&seed[18],1)
	pl_fail


;; TERM 37: '*GUARD*'(bmg_get_stacks(_B, _C)) :> []
c_code local build_ref_37
	ret_reg &ref[37]
	call_c   build_ref_36()
	call_c   Dyam_Create_Binary(I(9),&ref[36],I(0))
	move_ret ref[37]
	c_ret

c_code local build_seed_8
	ret_reg &seed[8]
	call_c   build_ref_26()
	call_c   build_ref_47()
	call_c   Dyam_Seed_Start(&ref[26],&ref[47],I(0),fun10,1)
	call_c   build_ref_48()
	call_c   Dyam_Seed_Add_Comp(&ref[48],fun22,0)
	call_c   Dyam_Seed_End()
	move_ret seed[8]
	c_ret

;; TERM 48: '*CITEM*'('call_bmg_head_stacks/6'(_B), _A)
c_code local build_ref_48
	ret_reg &ref[48]
	call_c   build_ref_29()
	call_c   build_ref_45()
	call_c   Dyam_Create_Binary(&ref[29],&ref[45],V(0))
	move_ret ref[48]
	c_ret

;; TERM 45: 'call_bmg_head_stacks/6'(_B)
c_code local build_ref_45
	ret_reg &ref[45]
	call_c   build_ref_144()
	call_c   Dyam_Create_Unary(&ref[144],V(1))
	move_ret ref[45]
	c_ret

;; TERM 49: bmg_stacks(_H)
c_code local build_ref_49
	ret_reg &ref[49]
	call_c   build_ref_151()
	call_c   Dyam_Create_Unary(&ref[151],V(7))
	move_ret ref[49]
	c_ret

c_code local build_seed_20
	ret_reg &seed[20]
	call_c   build_ref_10()
	call_c   build_ref_51()
	call_c   Dyam_Seed_Start(&ref[10],&ref[51],I(0),fun6,1)
	call_c   build_ref_52()
	call_c   Dyam_Seed_Add_Comp(&ref[52],&ref[51],0)
	call_c   Dyam_Seed_End()
	move_ret seed[20]
	c_ret

;; TERM 52: '*GUARD*'(bmg_head_stacks_loop(_H, _B, _C, _D, _E, _F, _G)) :> '$$HOLE$$'
c_code local build_ref_52
	ret_reg &ref[52]
	call_c   build_ref_51()
	call_c   Dyam_Create_Binary(I(9),&ref[51],I(7))
	move_ret ref[52]
	c_ret

;; TERM 51: '*GUARD*'(bmg_head_stacks_loop(_H, _B, _C, _D, _E, _F, _G))
c_code local build_ref_51
	ret_reg &ref[51]
	call_c   build_ref_10()
	call_c   build_ref_50()
	call_c   Dyam_Create_Unary(&ref[10],&ref[50])
	move_ret ref[51]
	c_ret

;; TERM 50: bmg_head_stacks_loop(_H, _B, _C, _D, _E, _F, _G)
c_code local build_ref_50
	ret_reg &ref[50]
	call_c   build_ref_148()
	call_c   Dyam_Term_Start(&ref[148],7)
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[50]
	c_ret

pl_code local fun20
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Light_Object(R(0))
	pl_jump  Schedule_Prolog()

c_code local build_seed_22
	ret_reg &seed[22]
	call_c   build_ref_0()
	call_c   build_ref_65()
	call_c   Dyam_Seed_Start(&ref[0],&ref[65],&ref[65],fun6,1)
	call_c   build_ref_66()
	call_c   Dyam_Seed_Add_Comp(&ref[66],&ref[65],0)
	call_c   Dyam_Seed_End()
	move_ret seed[22]
	c_ret

;; TERM 66: '*RITEM*'(_A, return(_C, _D, _E, _F, _G)) :> '$$HOLE$$'
c_code local build_ref_66
	ret_reg &ref[66]
	call_c   build_ref_65()
	call_c   Dyam_Create_Binary(I(9),&ref[65],I(7))
	move_ret ref[66]
	c_ret

;; TERM 65: '*RITEM*'(_A, return(_C, _D, _E, _F, _G))
c_code local build_ref_65
	ret_reg &ref[65]
	call_c   build_ref_0()
	call_c   build_ref_64()
	call_c   Dyam_Create_Binary(&ref[0],V(0),&ref[64])
	move_ret ref[65]
	c_ret

;; TERM 64: return(_C, _D, _E, _F, _G)
c_code local build_ref_64
	ret_reg &ref[64]
	call_c   build_ref_143()
	call_c   Dyam_Term_Start(&ref[143],5)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[64]
	c_ret

long local pool_fun22[5]=[4,build_ref_48,build_ref_49,build_seed_20,build_seed_22]

pl_code local fun22
	call_c   Dyam_Pool(pool_fun22)
	call_c   Dyam_Unify_Item(&ref[48])
	fail_ret
	call_c   Dyam_Allocate(0)
	pl_call  Object_1(&ref[49])
	pl_call  fun20(&seed[20],1)
	call_c   Dyam_Deallocate()
	pl_jump  fun12(&seed[22],2)

;; TERM 47: '*FIRST*'('call_bmg_head_stacks/6'(_B)) :> []
c_code local build_ref_47
	ret_reg &ref[47]
	call_c   build_ref_46()
	call_c   Dyam_Create_Binary(I(9),&ref[46],I(0))
	move_ret ref[47]
	c_ret

;; TERM 46: '*FIRST*'('call_bmg_head_stacks/6'(_B))
c_code local build_ref_46
	ret_reg &ref[46]
	call_c   build_ref_26()
	call_c   build_ref_45()
	call_c   Dyam_Create_Unary(&ref[26],&ref[45])
	move_ret ref[46]
	c_ret

c_code local build_seed_3
	ret_reg &seed[3]
	call_c   build_ref_9()
	call_c   build_ref_59()
	call_c   Dyam_Seed_Start(&ref[9],&ref[59],I(0),fun1,1)
	call_c   build_ref_58()
	call_c   Dyam_Seed_Add_Comp(&ref[58],fun21,0)
	call_c   Dyam_Seed_End()
	move_ret seed[3]
	c_ret

;; TERM 58: '*GUARD*'(bmg_pop(_B, _C, _D, _E))
c_code local build_ref_58
	ret_reg &ref[58]
	call_c   build_ref_10()
	call_c   build_ref_57()
	call_c   Dyam_Create_Unary(&ref[10],&ref[57])
	move_ret ref[58]
	c_ret

;; TERM 57: bmg_pop(_B, _C, _D, _E)
c_code local build_ref_57
	ret_reg &ref[57]
	call_c   build_ref_152()
	call_c   Dyam_Term_Start(&ref[152],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[57]
	c_ret

;; TERM 152: bmg_pop
c_code local build_ref_152
	ret_reg &ref[152]
	call_c   Dyam_Create_Atom("bmg_pop")
	move_ret ref[152]
	c_ret

;; TERM 60: bmg_stacks(_F)
c_code local build_ref_60
	ret_reg &ref[60]
	call_c   build_ref_151()
	call_c   Dyam_Create_Unary(&ref[151],V(5))
	move_ret ref[60]
	c_ret

c_code local build_seed_21
	ret_reg &seed[21]
	call_c   build_ref_10()
	call_c   build_ref_62()
	call_c   Dyam_Seed_Start(&ref[10],&ref[62],I(0),fun6,1)
	call_c   build_ref_63()
	call_c   Dyam_Seed_Add_Comp(&ref[63],&ref[62],0)
	call_c   Dyam_Seed_End()
	move_ret seed[21]
	c_ret

;; TERM 63: '*GUARD*'(bmg_pop_loop(_F, (_G / _H), _C, _D, _E)) :> '$$HOLE$$'
c_code local build_ref_63
	ret_reg &ref[63]
	call_c   build_ref_62()
	call_c   Dyam_Create_Binary(I(9),&ref[62],I(7))
	move_ret ref[63]
	c_ret

;; TERM 62: '*GUARD*'(bmg_pop_loop(_F, (_G / _H), _C, _D, _E))
c_code local build_ref_62
	ret_reg &ref[62]
	call_c   build_ref_10()
	call_c   build_ref_61()
	call_c   Dyam_Create_Unary(&ref[10],&ref[61])
	move_ret ref[62]
	c_ret

;; TERM 61: bmg_pop_loop(_F, (_G / _H), _C, _D, _E)
c_code local build_ref_61
	ret_reg &ref[61]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_153()
	call_c   Dyam_Create_Binary(&ref[153],V(6),V(7))
	move_ret R(0)
	call_c   build_ref_147()
	call_c   Dyam_Term_Start(&ref[147],5)
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[61]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 153: /
c_code local build_ref_153
	ret_reg &ref[153]
	call_c   Dyam_Create_Atom("/")
	move_ret ref[153]
	c_ret

long local pool_fun21[4]=[3,build_ref_58,build_ref_60,build_seed_21]

pl_code local fun21
	call_c   Dyam_Pool(pool_fun21)
	call_c   Dyam_Unify_Item(&ref[58])
	fail_ret
	call_c   Dyam_Allocate(0)
	pl_call  Object_1(&ref[60])
	call_c   DYAM_evpred_functor(V(1),V(6),V(7))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_jump  fun20(&seed[21],1)

;; TERM 59: '*GUARD*'(bmg_pop(_B, _C, _D, _E)) :> []
c_code local build_ref_59
	ret_reg &ref[59]
	call_c   build_ref_58()
	call_c   Dyam_Create_Binary(I(9),&ref[58],I(0))
	move_ret ref[59]
	c_ret

c_code local build_seed_11
	ret_reg &seed[11]
	call_c   build_ref_9()
	call_c   build_ref_69()
	call_c   Dyam_Seed_Start(&ref[9],&ref[69],I(0),fun1,1)
	call_c   build_ref_68()
	call_c   Dyam_Seed_Add_Comp(&ref[68],fun25,0)
	call_c   Dyam_Seed_End()
	move_ret seed[11]
	c_ret

;; TERM 68: '*GUARD*'(bmg_equations([_B|_C], [_D|_E], _F, _G))
c_code local build_ref_68
	ret_reg &ref[68]
	call_c   build_ref_10()
	call_c   build_ref_67()
	call_c   Dyam_Create_Unary(&ref[10],&ref[67])
	move_ret ref[68]
	c_ret

;; TERM 67: bmg_equations([_B|_C], [_D|_E], _F, _G)
c_code local build_ref_67
	ret_reg &ref[67]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   Dyam_Create_List(V(1),V(2))
	move_ret R(0)
	call_c   Dyam_Create_List(V(3),V(4))
	move_ret R(1)
	call_c   build_ref_146()
	call_c   Dyam_Term_Start(&ref[146],4)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[67]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 74: unify(_B, _D) :> _H
c_code local build_ref_74
	ret_reg &ref[74]
	call_c   build_ref_73()
	call_c   Dyam_Create_Binary(I(9),&ref[73],V(7))
	move_ret ref[74]
	c_ret

;; TERM 73: unify(_B, _D)
c_code local build_ref_73
	ret_reg &ref[73]
	call_c   build_ref_154()
	call_c   Dyam_Create_Binary(&ref[154],V(1),V(3))
	move_ret ref[73]
	c_ret

;; TERM 154: unify
c_code local build_ref_154
	ret_reg &ref[154]
	call_c   Dyam_Create_Atom("unify")
	move_ret ref[154]
	c_ret

c_code local build_seed_23
	ret_reg &seed[23]
	call_c   build_ref_10()
	call_c   build_ref_71()
	call_c   Dyam_Seed_Start(&ref[10],&ref[71],I(0),fun6,1)
	call_c   build_ref_72()
	call_c   Dyam_Seed_Add_Comp(&ref[72],&ref[71],0)
	call_c   Dyam_Seed_End()
	move_ret seed[23]
	c_ret

;; TERM 72: '*GUARD*'(bmg_equations(_C, _E, _F, _H)) :> '$$HOLE$$'
c_code local build_ref_72
	ret_reg &ref[72]
	call_c   build_ref_71()
	call_c   Dyam_Create_Binary(I(9),&ref[71],I(7))
	move_ret ref[72]
	c_ret

;; TERM 71: '*GUARD*'(bmg_equations(_C, _E, _F, _H))
c_code local build_ref_71
	ret_reg &ref[71]
	call_c   build_ref_10()
	call_c   build_ref_70()
	call_c   Dyam_Create_Unary(&ref[10],&ref[70])
	move_ret ref[71]
	c_ret

;; TERM 70: bmg_equations(_C, _E, _F, _H)
c_code local build_ref_70
	ret_reg &ref[70]
	call_c   build_ref_146()
	call_c   Dyam_Term_Start(&ref[146],4)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[70]
	c_ret

long local pool_fun23[2]=[1,build_seed_23]

long local pool_fun24[3]=[65537,build_ref_74,pool_fun23]

pl_code local fun24
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(6),&ref[74])
	fail_ret
fun23:
	call_c   Dyam_Deallocate()
	pl_jump  fun20(&seed[23],1)


long local pool_fun25[4]=[131073,build_ref_68,pool_fun24,pool_fun23]

pl_code local fun25
	call_c   Dyam_Pool(pool_fun25)
	call_c   Dyam_Unify_Item(&ref[68])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun24)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(1),V(3))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(6),V(7))
	fail_ret
	pl_jump  fun23()

;; TERM 69: '*GUARD*'(bmg_equations([_B|_C], [_D|_E], _F, _G)) :> []
c_code local build_ref_69
	ret_reg &ref[69]
	call_c   build_ref_68()
	call_c   Dyam_Create_Binary(I(9),&ref[68],I(0))
	move_ret ref[69]
	c_ret

c_code local build_seed_7
	ret_reg &seed[7]
	call_c   build_ref_9()
	call_c   build_ref_77()
	call_c   Dyam_Seed_Start(&ref[9],&ref[77],I(0),fun1,1)
	call_c   build_ref_76()
	call_c   Dyam_Seed_Add_Comp(&ref[76],fun26,0)
	call_c   Dyam_Seed_End()
	move_ret seed[7]
	c_ret

;; TERM 76: '*GUARD*'(bmg_head_stacks_loop([_B|_C], _B, _D, [_E|_F], [[_D|_E]|_G], [[]|_H], [[]|_I]))
c_code local build_ref_76
	ret_reg &ref[76]
	call_c   build_ref_10()
	call_c   build_ref_75()
	call_c   Dyam_Create_Unary(&ref[10],&ref[75])
	move_ret ref[76]
	c_ret

;; TERM 75: bmg_head_stacks_loop([_B|_C], _B, _D, [_E|_F], [[_D|_E]|_G], [[]|_H], [[]|_I])
c_code local build_ref_75
	ret_reg &ref[75]
	call_c   Dyam_Pseudo_Choice(5)
	call_c   Dyam_Create_List(V(1),V(2))
	move_ret R(0)
	call_c   Dyam_Create_List(V(4),V(5))
	move_ret R(1)
	call_c   Dyam_Create_List(V(3),V(4))
	move_ret R(2)
	call_c   Dyam_Create_List(R(2),V(6))
	move_ret R(2)
	call_c   Dyam_Create_List(I(0),V(7))
	move_ret R(3)
	call_c   Dyam_Create_List(I(0),V(8))
	move_ret R(4)
	call_c   build_ref_148()
	call_c   Dyam_Term_Start(&ref[148],7)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(R(2))
	call_c   Dyam_Term_Arg(R(3))
	call_c   Dyam_Term_Arg(R(4))
	call_c   Dyam_Term_End()
	move_ret ref[75]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_24
	ret_reg &seed[24]
	call_c   build_ref_10()
	call_c   build_ref_79()
	call_c   Dyam_Seed_Start(&ref[10],&ref[79],I(0),fun6,1)
	call_c   build_ref_80()
	call_c   Dyam_Seed_Add_Comp(&ref[80],&ref[79],0)
	call_c   Dyam_Seed_End()
	move_ret seed[24]
	c_ret

;; TERM 80: '*GUARD*'(bmg_head_stacks_loop(_C, _B, _D, _F, _G, _H, _I)) :> '$$HOLE$$'
c_code local build_ref_80
	ret_reg &ref[80]
	call_c   build_ref_79()
	call_c   Dyam_Create_Binary(I(9),&ref[79],I(7))
	move_ret ref[80]
	c_ret

;; TERM 79: '*GUARD*'(bmg_head_stacks_loop(_C, _B, _D, _F, _G, _H, _I))
c_code local build_ref_79
	ret_reg &ref[79]
	call_c   build_ref_10()
	call_c   build_ref_78()
	call_c   Dyam_Create_Unary(&ref[10],&ref[78])
	move_ret ref[79]
	c_ret

;; TERM 78: bmg_head_stacks_loop(_C, _B, _D, _F, _G, _H, _I)
c_code local build_ref_78
	ret_reg &ref[78]
	call_c   build_ref_148()
	call_c   Dyam_Term_Start(&ref[148],7)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_End()
	move_ret ref[78]
	c_ret

long local pool_fun26[3]=[2,build_ref_76,build_seed_24]

pl_code local fun26
	call_c   Dyam_Pool(pool_fun26)
	call_c   Dyam_Unify_Item(&ref[76])
	fail_ret
	pl_jump  fun20(&seed[24],1)

;; TERM 77: '*GUARD*'(bmg_head_stacks_loop([_B|_C], _B, _D, [_E|_F], [[_D|_E]|_G], [[]|_H], [[]|_I])) :> []
c_code local build_ref_77
	ret_reg &ref[77]
	call_c   build_ref_76()
	call_c   Dyam_Create_Binary(I(9),&ref[76],I(0))
	move_ret ref[77]
	c_ret

c_code local build_seed_6
	ret_reg &seed[6]
	call_c   build_ref_9()
	call_c   build_ref_83()
	call_c   Dyam_Seed_Start(&ref[9],&ref[83],I(0),fun1,1)
	call_c   build_ref_82()
	call_c   Dyam_Seed_Add_Comp(&ref[82],fun29,0)
	call_c   Dyam_Seed_End()
	move_ret seed[6]
	c_ret

;; TERM 82: '*GUARD*'(bmg_head_stacks_loop([_B|_C], _D, _E, [_F|_G], [_H|_I], [_F|_J], [_H|_K]))
c_code local build_ref_82
	ret_reg &ref[82]
	call_c   build_ref_10()
	call_c   build_ref_81()
	call_c   Dyam_Create_Unary(&ref[10],&ref[81])
	move_ret ref[82]
	c_ret

;; TERM 81: bmg_head_stacks_loop([_B|_C], _D, _E, [_F|_G], [_H|_I], [_F|_J], [_H|_K])
c_code local build_ref_81
	ret_reg &ref[81]
	call_c   Dyam_Pseudo_Choice(5)
	call_c   Dyam_Create_List(V(1),V(2))
	move_ret R(0)
	call_c   Dyam_Create_List(V(5),V(6))
	move_ret R(1)
	call_c   Dyam_Create_List(V(7),V(8))
	move_ret R(2)
	call_c   Dyam_Create_List(V(5),V(9))
	move_ret R(3)
	call_c   Dyam_Create_List(V(7),V(10))
	move_ret R(4)
	call_c   build_ref_148()
	call_c   Dyam_Term_Start(&ref[148],7)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(R(2))
	call_c   Dyam_Term_Arg(R(3))
	call_c   Dyam_Term_Arg(R(4))
	call_c   Dyam_Term_End()
	move_ret ref[81]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_25
	ret_reg &seed[25]
	call_c   build_ref_10()
	call_c   build_ref_85()
	call_c   Dyam_Seed_Start(&ref[10],&ref[85],I(0),fun6,1)
	call_c   build_ref_86()
	call_c   Dyam_Seed_Add_Comp(&ref[86],&ref[85],0)
	call_c   Dyam_Seed_End()
	move_ret seed[25]
	c_ret

;; TERM 86: '*GUARD*'(bmg_head_stacks_loop(_C, _D, _E, _G, _I, _J, _K)) :> '$$HOLE$$'
c_code local build_ref_86
	ret_reg &ref[86]
	call_c   build_ref_85()
	call_c   Dyam_Create_Binary(I(9),&ref[85],I(7))
	move_ret ref[86]
	c_ret

;; TERM 85: '*GUARD*'(bmg_head_stacks_loop(_C, _D, _E, _G, _I, _J, _K))
c_code local build_ref_85
	ret_reg &ref[85]
	call_c   build_ref_10()
	call_c   build_ref_84()
	call_c   Dyam_Create_Unary(&ref[10],&ref[84])
	move_ret ref[85]
	c_ret

;; TERM 84: bmg_head_stacks_loop(_C, _D, _E, _G, _I, _J, _K)
c_code local build_ref_84
	ret_reg &ref[84]
	call_c   build_ref_148()
	call_c   Dyam_Term_Start(&ref[148],7)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_End()
	move_ret ref[84]
	c_ret

long local pool_fun27[2]=[1,build_seed_25]

pl_code local fun28
	call_c   Dyam_Remove_Choice()
fun27:
	call_c   Dyam_Deallocate()
	pl_jump  fun20(&seed[25],1)


long local pool_fun29[3]=[65537,build_ref_82,pool_fun27]

pl_code local fun29
	call_c   Dyam_Pool(pool_fun29)
	call_c   Dyam_Unify_Item(&ref[82])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun28)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(1),V(3))
	fail_ret
	call_c   Dyam_Cut()
	pl_fail

;; TERM 83: '*GUARD*'(bmg_head_stacks_loop([_B|_C], _D, _E, [_F|_G], [_H|_I], [_F|_J], [_H|_K])) :> []
c_code local build_ref_83
	ret_reg &ref[83]
	call_c   build_ref_82()
	call_c   Dyam_Create_Binary(I(9),&ref[82],I(0))
	move_ret ref[83]
	c_ret

c_code local build_seed_2
	ret_reg &seed[2]
	call_c   build_ref_9()
	call_c   build_ref_89()
	call_c   Dyam_Seed_Start(&ref[9],&ref[89],I(0),fun1,1)
	call_c   build_ref_88()
	call_c   Dyam_Seed_Add_Comp(&ref[88],fun34,0)
	call_c   Dyam_Seed_End()
	move_ret seed[2]
	c_ret

;; TERM 88: '*GUARD*'(bmg_pop_loop([_B|_C], _D, [_E|_F], [_G|_H], _I))
c_code local build_ref_88
	ret_reg &ref[88]
	call_c   build_ref_10()
	call_c   build_ref_87()
	call_c   Dyam_Create_Unary(&ref[10],&ref[87])
	move_ret ref[88]
	c_ret

;; TERM 87: bmg_pop_loop([_B|_C], _D, [_E|_F], [_G|_H], _I)
c_code local build_ref_87
	ret_reg &ref[87]
	call_c   Dyam_Pseudo_Choice(3)
	call_c   Dyam_Create_List(V(1),V(2))
	move_ret R(0)
	call_c   Dyam_Create_List(V(4),V(5))
	move_ret R(1)
	call_c   Dyam_Create_List(V(6),V(7))
	move_ret R(2)
	call_c   build_ref_147()
	call_c   Dyam_Term_Start(&ref[147],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(R(2))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_End()
	move_ret ref[87]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_26
	ret_reg &seed[26]
	call_c   build_ref_10()
	call_c   build_ref_93()
	call_c   Dyam_Seed_Start(&ref[10],&ref[93],I(0),fun6,1)
	call_c   build_ref_94()
	call_c   Dyam_Seed_Add_Comp(&ref[94],&ref[93],0)
	call_c   Dyam_Seed_End()
	move_ret seed[26]
	c_ret

;; TERM 94: '*GUARD*'(bmg_pop_loop(_C, _D, _F, _H, _J)) :> '$$HOLE$$'
c_code local build_ref_94
	ret_reg &ref[94]
	call_c   build_ref_93()
	call_c   Dyam_Create_Binary(I(9),&ref[93],I(7))
	move_ret ref[94]
	c_ret

;; TERM 93: '*GUARD*'(bmg_pop_loop(_C, _D, _F, _H, _J))
c_code local build_ref_93
	ret_reg &ref[93]
	call_c   build_ref_10()
	call_c   build_ref_92()
	call_c   Dyam_Create_Unary(&ref[10],&ref[92])
	move_ret ref[93]
	c_ret

;; TERM 92: bmg_pop_loop(_C, _D, _F, _H, _J)
c_code local build_ref_92
	ret_reg &ref[92]
	call_c   build_ref_147()
	call_c   Dyam_Term_Start(&ref[147],5)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret ref[92]
	c_ret

long local pool_fun30[2]=[1,build_seed_26]

pl_code local fun33
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(9),V(8))
	fail_ret
fun30:
	call_c   Dyam_Deallocate()
	pl_jump  fun20(&seed[26],1)


;; TERM 90: bmg_pushable(_D, _B)
c_code local build_ref_90
	ret_reg &ref[90]
	call_c   build_ref_155()
	call_c   Dyam_Create_Binary(&ref[155],V(3),V(1))
	move_ret ref[90]
	c_ret

;; TERM 155: bmg_pushable
c_code local build_ref_155
	ret_reg &ref[155]
	call_c   Dyam_Create_Atom("bmg_pushable")
	move_ret ref[155]
	c_ret

;; TERM 91: [_B|_J]
c_code local build_ref_91
	ret_reg &ref[91]
	call_c   Dyam_Create_List(V(1),V(9))
	move_ret ref[91]
	c_ret

long local pool_fun31[3]=[65537,build_ref_91,pool_fun30]

pl_code local fun32
	call_c   Dyam_Remove_Choice()
fun31:
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(8),&ref[91])
	fail_ret
	pl_jump  fun30()


long local pool_fun34[5]=[131074,build_ref_88,build_ref_90,pool_fun30,pool_fun31]

pl_code local fun34
	call_c   Dyam_Pool(pool_fun34)
	call_c   Dyam_Unify_Item(&ref[88])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun33)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[90])
	call_c   Dyam_Choice(fun32)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(4),V(6))
	fail_ret
	call_c   Dyam_Cut()
	pl_fail

;; TERM 89: '*GUARD*'(bmg_pop_loop([_B|_C], _D, [_E|_F], [_G|_H], _I)) :> []
c_code local build_ref_89
	ret_reg &ref[89]
	call_c   build_ref_88()
	call_c   Dyam_Create_Binary(I(9),&ref[88],I(0))
	move_ret ref[89]
	c_ret

c_code local build_seed_0
	ret_reg &seed[0]
	call_c   build_ref_9()
	call_c   build_ref_97()
	call_c   Dyam_Seed_Start(&ref[9],&ref[97],I(0),fun1,1)
	call_c   build_ref_96()
	call_c   Dyam_Seed_Add_Comp(&ref[96],fun37,0)
	call_c   Dyam_Seed_End()
	move_ret seed[0]
	c_ret

;; TERM 96: '*GUARD*'(bmg_pop_code_loop([_B|_C], _D, _E, [_F|_G], [_H|_I], _J, _K))
c_code local build_ref_96
	ret_reg &ref[96]
	call_c   build_ref_10()
	call_c   build_ref_95()
	call_c   Dyam_Create_Unary(&ref[10],&ref[95])
	move_ret ref[96]
	c_ret

;; TERM 95: bmg_pop_code_loop([_B|_C], _D, _E, [_F|_G], [_H|_I], _J, _K)
c_code local build_ref_95
	ret_reg &ref[95]
	call_c   Dyam_Pseudo_Choice(3)
	call_c   Dyam_Create_List(V(1),V(2))
	move_ret R(0)
	call_c   Dyam_Create_List(V(5),V(6))
	move_ret R(1)
	call_c   Dyam_Create_List(V(7),V(8))
	move_ret R(2)
	call_c   build_ref_149()
	call_c   Dyam_Term_Start(&ref[149],7)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(R(2))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_End()
	move_ret ref[95]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 104: unify(_F, _H) :> _L
c_code local build_ref_104
	ret_reg &ref[104]
	call_c   build_ref_103()
	call_c   Dyam_Create_Binary(I(9),&ref[103],V(11))
	move_ret ref[104]
	c_ret

;; TERM 103: unify(_F, _H)
c_code local build_ref_103
	ret_reg &ref[103]
	call_c   build_ref_154()
	call_c   Dyam_Create_Binary(&ref[154],V(5),V(7))
	move_ret ref[103]
	c_ret

c_code local build_seed_27
	ret_reg &seed[27]
	call_c   build_ref_10()
	call_c   build_ref_101()
	call_c   Dyam_Seed_Start(&ref[10],&ref[101],I(0),fun6,1)
	call_c   build_ref_102()
	call_c   Dyam_Seed_Add_Comp(&ref[102],&ref[101],0)
	call_c   Dyam_Seed_End()
	move_ret seed[27]
	c_ret

;; TERM 102: '*GUARD*'(bmg_pop_code_loop(_C, _D, _E, _G, _I, _L, _K)) :> '$$HOLE$$'
c_code local build_ref_102
	ret_reg &ref[102]
	call_c   build_ref_101()
	call_c   Dyam_Create_Binary(I(9),&ref[101],I(7))
	move_ret ref[102]
	c_ret

;; TERM 101: '*GUARD*'(bmg_pop_code_loop(_C, _D, _E, _G, _I, _L, _K))
c_code local build_ref_101
	ret_reg &ref[101]
	call_c   build_ref_10()
	call_c   build_ref_100()
	call_c   Dyam_Create_Unary(&ref[10],&ref[100])
	move_ret ref[101]
	c_ret

;; TERM 100: bmg_pop_code_loop(_C, _D, _E, _G, _I, _L, _K)
c_code local build_ref_100
	ret_reg &ref[100]
	call_c   build_ref_149()
	call_c   Dyam_Term_Start(&ref[149],7)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_End()
	move_ret ref[100]
	c_ret

long local pool_fun35[2]=[1,build_seed_27]

long local pool_fun36[3]=[65537,build_ref_104,pool_fun35]

pl_code local fun36
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(9),&ref[104])
	fail_ret
fun35:
	call_c   Dyam_Deallocate()
	pl_jump  fun20(&seed[27],1)


;; TERM 99: unify(_F, [_E|_H]) :> _L
c_code local build_ref_99
	ret_reg &ref[99]
	call_c   build_ref_98()
	call_c   Dyam_Create_Binary(I(9),&ref[98],V(11))
	move_ret ref[99]
	c_ret

;; TERM 98: unify(_F, [_E|_H])
c_code local build_ref_98
	ret_reg &ref[98]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(4),V(7))
	move_ret R(0)
	call_c   build_ref_154()
	call_c   Dyam_Create_Binary(&ref[154],V(5),R(0))
	move_ret ref[98]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun37[5]=[131074,build_ref_96,build_ref_99,pool_fun36,pool_fun35]

pl_code local fun37
	call_c   Dyam_Pool(pool_fun37)
	call_c   Dyam_Unify_Item(&ref[96])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun36)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(1),V(3))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(9),&ref[99])
	fail_ret
	pl_jump  fun35()

;; TERM 97: '*GUARD*'(bmg_pop_code_loop([_B|_C], _D, _E, [_F|_G], [_H|_I], _J, _K)) :> []
c_code local build_ref_97
	ret_reg &ref[97]
	call_c   build_ref_96()
	call_c   Dyam_Create_Binary(I(9),&ref[96],I(0))
	move_ret ref[97]
	c_ret

c_code local build_seed_5
	ret_reg &seed[5]
	call_c   build_ref_9()
	call_c   build_ref_107()
	call_c   Dyam_Seed_Start(&ref[9],&ref[107],I(0),fun1,1)
	call_c   build_ref_106()
	call_c   Dyam_Seed_Add_Comp(&ref[106],fun40,0)
	call_c   Dyam_Seed_End()
	move_ret seed[5]
	c_ret

;; TERM 106: '*GUARD*'(bmg_inside_island(_B, _C, _D, _E, _F, _G, _H, _I))
c_code local build_ref_106
	ret_reg &ref[106]
	call_c   build_ref_10()
	call_c   build_ref_105()
	call_c   Dyam_Create_Unary(&ref[10],&ref[105])
	move_ret ref[106]
	c_ret

;; TERM 105: bmg_inside_island(_B, _C, _D, _E, _F, _G, _H, _I)
c_code local build_ref_105
	ret_reg &ref[105]
	call_c   build_ref_156()
	call_c   Dyam_Term_Start(&ref[156],8)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_End()
	move_ret ref[105]
	c_ret

;; TERM 156: bmg_inside_island
c_code local build_ref_156
	ret_reg &ref[156]
	call_c   Dyam_Create_Atom("bmg_inside_island")
	move_ret ref[156]
	c_ret

pl_code local fun39
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(5),V(3))
	fail_ret
	call_c   Dyam_Unify(V(6),V(4))
	fail_ret
	call_c   Dyam_Unify(V(7),V(8))
	fail_ret
	call_c   Dyam_Unify(V(1),V(2))
	fail_ret
fun38:
	call_c   Dyam_Deallocate()
	pl_ret


;; TERM 108: [_J,_K]
c_code local build_ref_108
	ret_reg &ref[108]
	call_c   Dyam_Create_Tupple(9,10,I(0))
	move_ret ref[108]
	c_ret

;; TERM 109: bmg_island(_J)
c_code local build_ref_109
	ret_reg &ref[109]
	call_c   build_ref_157()
	call_c   Dyam_Create_Unary(&ref[157],V(9))
	move_ret ref[109]
	c_ret

;; TERM 157: bmg_island
c_code local build_ref_157
	ret_reg &ref[157]
	call_c   Dyam_Create_Atom("bmg_island")
	move_ret ref[157]
	c_ret

;; TERM 110: bmg_stacks(_L)
c_code local build_ref_110
	ret_reg &ref[110]
	call_c   build_ref_151()
	call_c   Dyam_Create_Unary(&ref[151],V(11))
	move_ret ref[110]
	c_ret

c_code local build_seed_28
	ret_reg &seed[28]
	call_c   build_ref_10()
	call_c   build_ref_112()
	call_c   Dyam_Seed_Start(&ref[10],&ref[112],I(0),fun6,1)
	call_c   build_ref_113()
	call_c   Dyam_Seed_Add_Comp(&ref[113],&ref[112],0)
	call_c   Dyam_Seed_End()
	move_ret seed[28]
	c_ret

;; TERM 113: '*GUARD*'(bmg_inside_island_loop(_L, _J, _D, _E, _M, _N, _H, _O)) :> '$$HOLE$$'
c_code local build_ref_113
	ret_reg &ref[113]
	call_c   build_ref_112()
	call_c   Dyam_Create_Binary(I(9),&ref[112],I(7))
	move_ret ref[113]
	c_ret

;; TERM 112: '*GUARD*'(bmg_inside_island_loop(_L, _J, _D, _E, _M, _N, _H, _O))
c_code local build_ref_112
	ret_reg &ref[112]
	call_c   build_ref_10()
	call_c   build_ref_111()
	call_c   Dyam_Create_Unary(&ref[10],&ref[111])
	move_ret ref[112]
	c_ret

;; TERM 111: bmg_inside_island_loop(_L, _J, _D, _E, _M, _N, _H, _O)
c_code local build_ref_111
	ret_reg &ref[111]
	call_c   build_ref_150()
	call_c   Dyam_Term_Start(&ref[150],8)
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_End()
	move_ret ref[111]
	c_ret

c_code local build_seed_29
	ret_reg &seed[29]
	call_c   build_ref_10()
	call_c   build_ref_115()
	call_c   Dyam_Seed_Start(&ref[10],&ref[115],I(0),fun6,1)
	call_c   build_ref_116()
	call_c   Dyam_Seed_Add_Comp(&ref[116],&ref[115],0)
	call_c   Dyam_Seed_End()
	move_ret seed[29]
	c_ret

;; TERM 116: '*GUARD*'(bmg_inside_island(_K, _C, _M, _N, _F, _G, _O, _I)) :> '$$HOLE$$'
c_code local build_ref_116
	ret_reg &ref[116]
	call_c   build_ref_115()
	call_c   Dyam_Create_Binary(I(9),&ref[115],I(7))
	move_ret ref[116]
	c_ret

;; TERM 115: '*GUARD*'(bmg_inside_island(_K, _C, _M, _N, _F, _G, _O, _I))
c_code local build_ref_115
	ret_reg &ref[115]
	call_c   build_ref_10()
	call_c   build_ref_114()
	call_c   Dyam_Create_Unary(&ref[10],&ref[114])
	move_ret ref[115]
	c_ret

;; TERM 114: bmg_inside_island(_K, _C, _M, _N, _F, _G, _O, _I)
c_code local build_ref_114
	ret_reg &ref[114]
	call_c   build_ref_156()
	call_c   Dyam_Term_Start(&ref[156],8)
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_End()
	move_ret ref[114]
	c_ret

long local pool_fun40[7]=[6,build_ref_106,build_ref_108,build_ref_109,build_ref_110,build_seed_28,build_seed_29]

pl_code local fun40
	call_c   Dyam_Pool(pool_fun40)
	call_c   Dyam_Unify_Item(&ref[106])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun39)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_univ(V(1),&ref[108])
	fail_ret
	pl_call  Object_1(&ref[109])
	call_c   Dyam_Cut()
	pl_call  Object_1(&ref[110])
	pl_call  fun20(&seed[28],1)
	pl_call  fun20(&seed[29],1)
	pl_jump  fun38()

;; TERM 107: '*GUARD*'(bmg_inside_island(_B, _C, _D, _E, _F, _G, _H, _I)) :> []
c_code local build_ref_107
	ret_reg &ref[107]
	call_c   build_ref_106()
	call_c   Dyam_Create_Binary(I(9),&ref[106],I(0))
	move_ret ref[107]
	c_ret

c_code local build_seed_4
	ret_reg &seed[4]
	call_c   build_ref_9()
	call_c   build_ref_119()
	call_c   Dyam_Seed_Start(&ref[9],&ref[119],I(0),fun1,1)
	call_c   build_ref_118()
	call_c   Dyam_Seed_Add_Comp(&ref[118],fun44,0)
	call_c   Dyam_Seed_End()
	move_ret seed[4]
	c_ret

;; TERM 118: '*GUARD*'(bmg_inside_island_loop([_B|_C], _D, [_E|_F], [_G|_H], [_I|_J], [_K|_L], _M, _N))
c_code local build_ref_118
	ret_reg &ref[118]
	call_c   build_ref_10()
	call_c   build_ref_117()
	call_c   Dyam_Create_Unary(&ref[10],&ref[117])
	move_ret ref[118]
	c_ret

;; TERM 117: bmg_inside_island_loop([_B|_C], _D, [_E|_F], [_G|_H], [_I|_J], [_K|_L], _M, _N)
c_code local build_ref_117
	ret_reg &ref[117]
	call_c   Dyam_Pseudo_Choice(5)
	call_c   Dyam_Create_List(V(1),V(2))
	move_ret R(0)
	call_c   Dyam_Create_List(V(4),V(5))
	move_ret R(1)
	call_c   Dyam_Create_List(V(6),V(7))
	move_ret R(2)
	call_c   Dyam_Create_List(V(8),V(9))
	move_ret R(3)
	call_c   Dyam_Create_List(V(10),V(11))
	move_ret R(4)
	call_c   build_ref_150()
	call_c   Dyam_Term_Start(&ref[150],8)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(R(2))
	call_c   Dyam_Term_Arg(R(3))
	call_c   Dyam_Term_Arg(R(4))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_End()
	move_ret ref[117]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_30
	ret_reg &seed[30]
	call_c   build_ref_10()
	call_c   build_ref_122()
	call_c   Dyam_Seed_Start(&ref[10],&ref[122],I(0),fun6,1)
	call_c   build_ref_123()
	call_c   Dyam_Seed_Add_Comp(&ref[123],&ref[122],0)
	call_c   Dyam_Seed_End()
	move_ret seed[30]
	c_ret

;; TERM 123: '*GUARD*'(bmg_inside_island_loop(_C, _D, _F, _H, _J, _L, _O, _N)) :> '$$HOLE$$'
c_code local build_ref_123
	ret_reg &ref[123]
	call_c   build_ref_122()
	call_c   Dyam_Create_Binary(I(9),&ref[122],I(7))
	move_ret ref[123]
	c_ret

;; TERM 122: '*GUARD*'(bmg_inside_island_loop(_C, _D, _F, _H, _J, _L, _O, _N))
c_code local build_ref_122
	ret_reg &ref[122]
	call_c   build_ref_10()
	call_c   build_ref_121()
	call_c   Dyam_Create_Unary(&ref[10],&ref[121])
	move_ret ref[122]
	c_ret

;; TERM 121: bmg_inside_island_loop(_C, _D, _F, _H, _J, _L, _O, _N)
c_code local build_ref_121
	ret_reg &ref[121]
	call_c   build_ref_150()
	call_c   Dyam_Term_Start(&ref[150],8)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_End()
	move_ret ref[121]
	c_ret

long local pool_fun41[2]=[1,build_seed_30]

pl_code local fun43
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(8),V(4))
	fail_ret
	call_c   Dyam_Unify(V(10),V(6))
	fail_ret
	call_c   Dyam_Unify(V(14),V(12))
	fail_ret
fun41:
	call_c   Dyam_Deallocate()
	pl_jump  fun20(&seed[30],1)


;; TERM 120: bmg_island(_D, _B)
c_code local build_ref_120
	ret_reg &ref[120]
	call_c   build_ref_157()
	call_c   Dyam_Create_Binary(&ref[157],V(3),V(1))
	move_ret ref[120]
	c_ret

;; TERM 125: unify(_E, _G) :> _M
c_code local build_ref_125
	ret_reg &ref[125]
	call_c   build_ref_124()
	call_c   Dyam_Create_Binary(I(9),&ref[124],V(12))
	move_ret ref[125]
	c_ret

;; TERM 124: unify(_E, _G)
c_code local build_ref_124
	ret_reg &ref[124]
	call_c   build_ref_154()
	call_c   Dyam_Create_Binary(&ref[154],V(4),V(6))
	move_ret ref[124]
	c_ret

long local pool_fun42[3]=[65537,build_ref_125,pool_fun41]

pl_code local fun42
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(14),&ref[125])
	fail_ret
	pl_jump  fun41()

long local pool_fun44[6]=[196610,build_ref_118,build_ref_120,pool_fun41,pool_fun42,pool_fun41]

pl_code local fun44
	call_c   Dyam_Pool(pool_fun44)
	call_c   Dyam_Unify_Item(&ref[118])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun43)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[120])
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(8),I(0))
	fail_ret
	call_c   Dyam_Unify(V(10),I(0))
	fail_ret
	call_c   Dyam_Choice(fun42)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(4),V(6))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(14),V(12))
	fail_ret
	pl_jump  fun41()

;; TERM 119: '*GUARD*'(bmg_inside_island_loop([_B|_C], _D, [_E|_F], [_G|_H], [_I|_J], [_K|_L], _M, _N)) :> []
c_code local build_ref_119
	ret_reg &ref[119]
	call_c   build_ref_118()
	call_c   Dyam_Create_Binary(I(9),&ref[118],I(0))
	move_ret ref[119]
	c_ret

c_code local build_seed_1
	ret_reg &seed[1]
	call_c   build_ref_9()
	call_c   build_ref_128()
	call_c   Dyam_Seed_Start(&ref[9],&ref[128],I(0),fun1,1)
	call_c   build_ref_127()
	call_c   Dyam_Seed_Add_Comp(&ref[127],fun46,0)
	call_c   Dyam_Seed_End()
	move_ret seed[1]
	c_ret

;; TERM 127: '*GUARD*'(bmg_pop_code([_B|_C], _D, _E, _F, _G, _H, _I, _J))
c_code local build_ref_127
	ret_reg &ref[127]
	call_c   build_ref_10()
	call_c   build_ref_126()
	call_c   Dyam_Create_Unary(&ref[10],&ref[126])
	move_ret ref[127]
	c_ret

;; TERM 126: bmg_pop_code([_B|_C], _D, _E, _F, _G, _H, _I, _J)
c_code local build_ref_126
	ret_reg &ref[126]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(1),V(2))
	move_ret R(0)
	call_c   build_ref_158()
	call_c   Dyam_Term_Start(&ref[158],8)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret ref[126]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 158: bmg_pop_code
c_code local build_ref_158
	ret_reg &ref[158]
	call_c   Dyam_Create_Atom("bmg_pop_code")
	move_ret ref[158]
	c_ret

;; TERM 129: bmg_stacks(_K)
c_code local build_ref_129
	ret_reg &ref[129]
	call_c   build_ref_151()
	call_c   Dyam_Create_Unary(&ref[151],V(10))
	move_ret ref[129]
	c_ret

;; TERM 135: unify(_G, _H) :> _N
c_code local build_ref_135
	ret_reg &ref[135]
	call_c   build_ref_133()
	call_c   Dyam_Create_Binary(I(9),&ref[133],V(13))
	move_ret ref[135]
	c_ret

;; TERM 133: unify(_G, _H)
c_code local build_ref_133
	ret_reg &ref[133]
	call_c   build_ref_154()
	call_c   Dyam_Create_Binary(&ref[154],V(6),V(7))
	move_ret ref[133]
	c_ret

c_code local build_seed_32
	ret_reg &seed[32]
	call_c   build_ref_10()
	call_c   build_ref_137()
	call_c   Dyam_Seed_Start(&ref[10],&ref[137],I(0),fun6,1)
	call_c   build_ref_138()
	call_c   Dyam_Seed_Add_Comp(&ref[138],&ref[137],0)
	call_c   Dyam_Seed_End()
	move_ret seed[32]
	c_ret

;; TERM 138: '*GUARD*'(bmg_pop_code_loop(_K, _B, _D, _E, _F, _N, _M)) :> '$$HOLE$$'
c_code local build_ref_138
	ret_reg &ref[138]
	call_c   build_ref_137()
	call_c   Dyam_Create_Binary(I(9),&ref[137],I(7))
	move_ret ref[138]
	c_ret

;; TERM 137: '*GUARD*'(bmg_pop_code_loop(_K, _B, _D, _E, _F, _N, _M))
c_code local build_ref_137
	ret_reg &ref[137]
	call_c   build_ref_10()
	call_c   build_ref_136()
	call_c   Dyam_Create_Unary(&ref[10],&ref[136])
	move_ret ref[137]
	c_ret

;; TERM 136: bmg_pop_code_loop(_K, _B, _D, _E, _F, _N, _M)
c_code local build_ref_136
	ret_reg &ref[136]
	call_c   build_ref_149()
	call_c   Dyam_Term_Start(&ref[149],7)
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_End()
	move_ret ref[136]
	c_ret

c_code local build_seed_33
	ret_reg &seed[33]
	call_c   build_ref_10()
	call_c   build_ref_140()
	call_c   Dyam_Seed_Start(&ref[10],&ref[140],I(0),fun6,1)
	call_c   build_ref_141()
	call_c   Dyam_Seed_Add_Comp(&ref[141],&ref[140],0)
	call_c   Dyam_Seed_End()
	move_ret seed[33]
	c_ret

;; TERM 141: '*GUARD*'(bmg_pop_code(_C, _D, _E, _F, _G, _H, _M, _O)) :> '$$HOLE$$'
c_code local build_ref_141
	ret_reg &ref[141]
	call_c   build_ref_140()
	call_c   Dyam_Create_Binary(I(9),&ref[140],I(7))
	move_ret ref[141]
	c_ret

;; TERM 140: '*GUARD*'(bmg_pop_code(_C, _D, _E, _F, _G, _H, _M, _O))
c_code local build_ref_140
	ret_reg &ref[140]
	call_c   build_ref_10()
	call_c   build_ref_139()
	call_c   Dyam_Create_Unary(&ref[10],&ref[139])
	move_ret ref[140]
	c_ret

;; TERM 139: bmg_pop_code(_C, _D, _E, _F, _G, _H, _M, _O)
c_code local build_ref_139
	ret_reg &ref[139]
	call_c   build_ref_158()
	call_c   Dyam_Term_Start(&ref[158],8)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_End()
	move_ret ref[139]
	c_ret

long local pool_fun45[4]=[3,build_ref_135,build_seed_32,build_seed_33]

pl_code local fun45
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Reg_Load(0,V(8))
	call_c   Dyam_Reg_Load(2,V(9))
	move     V(12), R(4)
	move     S(5), R(5)
	move     &ref[135], R(6)
	move     S(5), R(7)
	move     V(14), R(8)
	move     S(5), R(9)
	pl_call  pred_handle_choice_5()
	pl_call  fun20(&seed[32],1)
	pl_call  fun20(&seed[33],1)
	pl_jump  fun38()

c_code local build_seed_31
	ret_reg &seed[31]
	call_c   build_ref_10()
	call_c   build_ref_131()
	call_c   Dyam_Seed_Start(&ref[10],&ref[131],I(0),fun6,1)
	call_c   build_ref_132()
	call_c   Dyam_Seed_Add_Comp(&ref[132],&ref[131],0)
	call_c   Dyam_Seed_End()
	move_ret seed[31]
	c_ret

;; TERM 132: '*GUARD*'(bmg_pop_code_loop(_K, _B, _D, _E, _F, _L, _I)) :> '$$HOLE$$'
c_code local build_ref_132
	ret_reg &ref[132]
	call_c   build_ref_131()
	call_c   Dyam_Create_Binary(I(9),&ref[131],I(7))
	move_ret ref[132]
	c_ret

;; TERM 131: '*GUARD*'(bmg_pop_code_loop(_K, _B, _D, _E, _F, _L, _I))
c_code local build_ref_131
	ret_reg &ref[131]
	call_c   build_ref_10()
	call_c   build_ref_130()
	call_c   Dyam_Create_Unary(&ref[10],&ref[130])
	move_ret ref[131]
	c_ret

;; TERM 130: bmg_pop_code_loop(_K, _B, _D, _E, _F, _L, _I)
c_code local build_ref_130
	ret_reg &ref[130]
	call_c   build_ref_149()
	call_c   Dyam_Term_Start(&ref[149],7)
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_End()
	move_ret ref[130]
	c_ret

;; TERM 134: unify(_G, _H) :> _L
c_code local build_ref_134
	ret_reg &ref[134]
	call_c   build_ref_133()
	call_c   Dyam_Create_Binary(I(9),&ref[133],V(11))
	move_ret ref[134]
	c_ret

long local pool_fun46[6]=[65540,build_ref_127,build_ref_129,build_seed_31,build_ref_134,pool_fun45]

pl_code local fun46
	call_c   Dyam_Pool(pool_fun46)
	call_c   Dyam_Unify_Item(&ref[127])
	fail_ret
	call_c   Dyam_Allocate(0)
	pl_call  Object_1(&ref[129])
	call_c   Dyam_Choice(fun45)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(2),I(0))
	fail_ret
	call_c   Dyam_Cut()
	pl_call  fun20(&seed[31],1)
	call_c   Dyam_Unify(V(9),&ref[134])
	fail_ret
	pl_jump  fun38()

;; TERM 128: '*GUARD*'(bmg_pop_code([_B|_C], _D, _E, _F, _G, _H, _I, _J)) :> []
c_code local build_ref_128
	ret_reg &ref[128]
	call_c   build_ref_127()
	call_c   Dyam_Create_Binary(I(9),&ref[127],I(0))
	move_ret ref[128]
	c_ret

c_code local build_seed_18
	ret_reg &seed[18]
	call_c   build_ref_29()
	call_c   build_ref_38()
	call_c   Dyam_Seed_Start(&ref[29],&ref[38],&ref[38],fun6,1)
	call_c   build_ref_39()
	call_c   Dyam_Seed_Add_Comp(&ref[39],&ref[38],0)
	call_c   Dyam_Seed_End()
	move_ret seed[18]
	c_ret

;; TERM 39: '*FIRST*'('call_bmg_get_stacks/1') :> '$$HOLE$$'
c_code local build_ref_39
	ret_reg &ref[39]
	call_c   build_ref_27()
	call_c   Dyam_Create_Binary(I(9),&ref[27],I(7))
	move_ret ref[39]
	c_ret

;; TERM 38: '*CITEM*'('call_bmg_get_stacks/1', 'call_bmg_get_stacks/1')
c_code local build_ref_38
	ret_reg &ref[38]
	call_c   build_ref_29()
	call_c   build_ref_5()
	call_c   Dyam_Create_Binary(&ref[29],&ref[5],&ref[5])
	move_ret ref[38]
	c_ret

c_code local build_seed_19
	ret_reg &seed[19]
	call_c   build_ref_40()
	call_c   build_ref_44()
	call_c   Dyam_Seed_Start(&ref[40],&ref[44],I(0),fun10,1)
	call_c   build_ref_42()
	call_c   Dyam_Seed_Add_Comp(&ref[42],fun9,0)
	call_c   Dyam_Seed_End()
	move_ret seed[19]
	c_ret

;; TERM 42: '*RITEM*'('call_bmg_get_stacks/1', return(_C))
c_code local build_ref_42
	ret_reg &ref[42]
	call_c   build_ref_0()
	call_c   build_ref_5()
	call_c   build_ref_41()
	call_c   Dyam_Create_Binary(&ref[0],&ref[5],&ref[41])
	move_ret ref[42]
	c_ret

;; TERM 41: return(_C)
c_code local build_ref_41
	ret_reg &ref[41]
	call_c   build_ref_143()
	call_c   Dyam_Create_Unary(&ref[143],V(2))
	move_ret ref[41]
	c_ret

pl_code local fun9
	call_c   build_ref_42()
	call_c   Dyam_Unify_Item(&ref[42])
	fail_ret
	pl_jump  Follow_Cont(V(1))

;; TERM 44: '*RITEM*'('call_bmg_get_stacks/1', return(_C)) :> bmg(0, '$TUPPLE'(35141738826188))
c_code local build_ref_44
	ret_reg &ref[44]
	call_c   build_ref_42()
	call_c   build_ref_43()
	call_c   Dyam_Create_Binary(I(9),&ref[42],&ref[43])
	move_ret ref[44]
	c_ret

;; TERM 43: bmg(0, '$TUPPLE'(35141738826188))
c_code local build_ref_43
	ret_reg &ref[43]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,134217728)
	move_ret R(0)
	call_c   build_ref_159()
	call_c   Dyam_Create_Binary(&ref[159],N(0),R(0))
	move_ret ref[43]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 159: bmg
c_code local build_ref_159
	ret_reg &ref[159]
	call_c   Dyam_Create_Atom("bmg")
	move_ret ref[159]
	c_ret

;; TERM 40: '*CURNEXT*'
c_code local build_ref_40
	ret_reg &ref[40]
	call_c   Dyam_Create_Atom("*CURNEXT*")
	move_ret ref[40]
	c_ret

pl_code local fun14
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Object(R(0))
	call_c   Dyam_Tabule()
	pl_ret

pl_code local fun7
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Object(R(0))
	call_c   Dyam_Tabule()
	pl_jump  Schedule_Prolog()

pl_code local fun8
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Choice(fun7)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Subsume(R(0))
	fail_ret
	call_c   Dyam_Cut()
	pl_ret

pl_code local fun15
	call_c   Dyam_Backptr_Trace(R(1),R(2))
	call_c   Dyam_Light_Object(R(0))
	pl_jump  Schedule_Prolog()

pl_code local fun16
	call_c   Dyam_Remove_Choice()
	pl_call  fun15(&seed[19],2,V(3))
	call_c   Dyam_Deallocate()
	pl_ret


;;------------------ INIT POOL ------------

c_code local build_init_pool
	call_c   build_ref_4()
	call_c   build_ref_3()
	call_c   build_ref_8()
	call_c   build_ref_7()
	call_c   build_seed_16()
	call_c   build_seed_13()
	call_c   build_seed_15()
	call_c   build_seed_12()
	call_c   build_seed_14()
	call_c   build_seed_9()
	call_c   build_seed_10()
	call_c   build_seed_8()
	call_c   build_seed_3()
	call_c   build_seed_11()
	call_c   build_seed_7()
	call_c   build_seed_6()
	call_c   build_seed_2()
	call_c   build_seed_0()
	call_c   build_seed_5()
	call_c   build_seed_4()
	call_c   build_seed_1()
	call_c   build_seed_18()
	call_c   build_seed_19()
	c_ret

long local ref[160]
long local seed[34]

long local _initialization

c_code global initialization_dyalog_bmg
	call_c   Dyam_Check_And_Update_Initialization(_initialization)
	fail_c_ret
	call_c   initialization_dyalog_format()
	call_c   build_init_pool()
	call_c   build_viewers()
	call_c   load()
	c_ret

