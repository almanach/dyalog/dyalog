/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 1998, 2002, 2003, 2004, 2008, 2009, 2010, 2011, 2016 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  code_emit.pl -- Code Emitting
 *
 * ----------------------------------------------------------------
 * Description
 * 
 * ----------------------------------------------------------------
 */

:-include 'header.pl'.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Code Optimize

:-std_prolog code_optimize/3.
:-extensional stop_inst/4, allow_allocate_inversion/1.

code_optimize( K, KK, Length ) :-
%%	format(';; OPTIMIZE ~w\n',[K]),
        ( nonvar(K) xor internal_error( 'Code optimize', [] )),
	( stop_inst(K,KK,F,Length) ->
	    jump_inc(F)
	;   K = (A :> B) :> C ->
	    code_optimize( A :> (B :> C), KK,Length )
	;   K = noop :> A ->
	    code_optimize(A, KK,Length)
	;   K = allocate :> A ->
	    code_optimize(A,AA,LengthAA),
	    code_optimize_allocate(AA,KK,LengthAA,Length)
	;   K = call(F,Args) :> A ->
		code_optimize(A,AA,Length1),
%	        format('Optimize ~w ~w -> ~w\n',[F,A,AA]),
		( AA == deallocate :> succeed ->
		    KK = deallocate :> pl_jump(F,Args),
		    Length=2,
		    jump_inc(F)
		%% WARNING: should check if cond \+ reg_value(F,0) is really needed !
		%% ;   AA = reg_deallocate(N) :> succeed, (\+ reg_value(F,0)) ->
		;   AA = reg_deallocate(N) :> succeed ->
		    KK = reg_deallocate(N) :> pl_jump(F,Args),
		    Length=2,
		    jump_inc(F)
		;
		    AA == succeed ->
		    KK = pl_jump(F,Args),
		    Length=1,
		    jump_inc(F)
		;   
		    KK = call(F,Args) :> AA,
		    Length is Length1+1
		)
	;   K = remove_choice :> A ->
	    code_optimize(A,AA,Length1),
	    ( AA = choice(F) :>  B ->
		KK = update_choice(F) :> B,
		Length is Length1
	    ;
		KK = remove_choice :> AA,
		Length is Length1 + 1
	    
	    )
	;   K = A :> B, pl_inst(A,F,Args) ->
	    code_optimize( call(F,Args) :> B, KK,Length)
	;   K = A :> B ->
	    code_optimize(B,BB,Length1),
	    ( BB==fail, test_inst(A,F,Args) ->
		KK = notest(F,Args) :> fail,
		Length = 2
	    ;	
		KK = A :> BB,
		Length is Length1+1
	    )
	;   
	    KK = K,
	    Length=1
	),
%%	format( 'code_optimize\n\t~w\n\t~w\n\n',[K,KK]),
	true
	.


:-std_prolog code_optimize_allocate/4.

% K is already optimized, we are trying to move a preceding allocate
% as much as possible to try collapsing it with a deallocate
code_optimize_allocate(K,KK,LengthK,LengthKK) :-
	( K = (deallocate :> KK) ->
	    LengthKK is LengthK
	;   K==fail ->
	    KK=fail,
	    LengthKK is LengthK+1
	;   K = (A :> B), allow_allocate_inversion(A) ->
	    KK=A:>BB,
	    code_optimize_allocate(B,BB,LengthK,LengthKK)
	;   KK = (allocate :> K),
	    LengthKK is LengthK+1
	)
	.

allow_allocate_inversion(foreign(_,_)).
allow_allocate_inversion(foreign_void_return(_,_)).
allow_allocate_inversion(c_call(_Name,_Args,_Kind,_Reg)).
allow_allocate_inversion(item_unify(_)).
allow_allocate_inversion(unify(_,_)).
allow_allocate_inversion(unify(_,_,_,_)).
allow_allocate_inversion(subsume(_)).
allow_allocate_inversion(variance(_)).

stop_inst(succeed :> _, succeed,[],1).
stop_inst(pl_jump(F,Args) :> _, pl_jump(F,Args),F,1).
stop_inst(call(F,Args) :> succeed, pl_jump(F,Args),F,1).
stop_inst(call(F,Args) :> (deallocate :> succeed), deallocate :> pl_jump(F,Args),F,2).	 
stop_inst(return :> _, return,[],1).
stop_inst(fail :> _, fail,[],1).
stop_inst(reg_reset(N) :> deallocate_layer :> deallocate :> succeed, reg_deallocate(N) :> succeed,[],2 ).
stop_inst(reg_reset(N) :> noop :> deallocate :> succeed, reg_deallocate(N) :> succeed,[],2 ).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Code refs
%% Register refs to seeds, terms and functions in functions
%% => one extra traversal of code

:-std_prolog
	code_refs_add/3,
	code_refs/3,
	code_refs_aux/3,
	code_refs_add_ins/2
	.

%% K is already optimized
code_refs(K,LRefs,Ins) :-
	mutable(MRef,[],true),
	mutable(MIns,[],true),
	code_refs_aux(K,MRef,MIns),
	mutable_read(MRef,LRefs),
	mutable_read(MIns,Ins)
	.


code_refs_add(Ref,MRef,MIns) :-
	( domain(Ref,['$term'(_,_),'$seed'(_,_),'$fun'(_,_,_)]) ->
	    mutable_read(MRef,L),
	    (	domain(Ref,L) xor mutable(MRef,[Ref|L])),
	    ( Ref = '$fun'(_,_,Info) ->
		mutable_read(Info,'$fun_info'{refs=>LRefs,ins=>Ins}),
		code_refs_add_ins(LRefs,MIns),
		code_refs_add_ins(Ins,MIns)
	    ;
		true
	    )
	;   Ref = [_|_] ->
	    every((domain(A,Ref),code_refs_add(A,MRef,MIns)))
	;
	    true
	)
	.

code_refs_add_ins(L,MIns) :-
	every((   domain(Ref,L),
		  mutable_read(MIns,Ins),
		  (   domain(Ref,Ins) xor mutable(MIns,[Ref|Ins]))
	      ))
	.

code_refs_aux(K,MRef,MIns) :-
	( K = A :> KK ->
	    (	code_refs_aux(KK,MRef,MIns), fail ; true )
	;   
	    K = A
	),
	every((   foreign_arg(A,_,Arg),
		  code_refs_add(Arg,MRef,MIns)
	      ))
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Code emitting

:-extensional inst/3, test_inst/3, pl_inst/3.
:-rec_prolog special_inst/1.

:-std_prolog emit_code_aux/1.
:-std_prolog emit_code_elem/1.

:-std_prolog emit_pool/4.

emit_init_code(K) :-
	code_optimize(K,KK,_),
	code_refs(KK,LRefs,Ins),
	emit_globals_in_init_pool(LRefs),
	emit_code_aux(KK)
	.

emit_function(Id::'$fun'(_,Arity,Info)) :-
	mutable_read(Info,'$fun_info'{code=>Code,refs=>LRefs,ins=>Ins}),
	(   Arity == 0,
	    jump_value(Id,V),
	    V>0,
	    \+ recorded( named_function(Id,Name,global) ) ->
	%% Function to be inlined later => do not emit now
	%% but still need to emit its pool
	%% This function is derived
	%% The pool may not be built twice because emit_function
	    Inline = true
	;   
	    Inline = false
	),
%%	format(';; emit_function ~x ~w\n',[Id,Inline]),
	%% Emit pool
	( recorded( init_pool(Id) ) ->
%%	    format(';; ~x in init_pool\n',[Id]),
	    (	Inline == true
	    xor emit_globals_in_init_pool(LRefs))
	;   
	    emit_pool(LRefs,Id,Ins,Pool_Name)
	),
	(   Inline == true
	xor mutable(Info,[]),	% => emitted
				%	format(';; Emit pool ~x -> ~w\n',[Fun,Pool_Name]),
	    (	recorded( named_function(Id,Name,Type) ) xor Type = local ),
	    ma_emit_pl_code(Type,Id),
	    ( nonvar(Pool_Name), \+ erase_recorded( '$derived_fun'(Id) ) ->
	              ( Pool_Name='pool_'+_ ->
			  ma_emit_call_c('Dyam_Pool',['$label'(Pool_Name)])
		      ;	  
			  ma_emit_call_c(Pool_Name,[])
		      )
	    ;	
		true
	    ),
	    emit_code_aux(Code),
	    nl
	)
	.

:-std_prolog emit_inlined_function/1.

emit_inlined_function(Id::'$fun'(_,_,Info)):-
	mutable_read(Info,'$fun_info'{code=>Code,refs=>LRefs,ins=>Ins}),
	mutable(Info,[]),	% => emitted
	( recorded( init_pool(Id) ) ->
	    emit_globals_in_init_pool(LRefs)
	;   
	    true
	),
	emit_code_aux(Code),
	nl
	.

emit_code_aux( CC ) :-
	( CC = A :> B ->
	    emit_code_elem(A),
	    emit_code_aux(B)
	;   
	    emit_code_elem(CC)
	)
	.

emit_pool(LRefs,Pool,Ins,Pool_Name) :-
	%% recursively build included pools
	every(( domain(Ref,LRefs),
		(   \+ Ref = '$fun'(_,_,_)
		xor record_without_doublon('$derived_fun'(Ref))),
		data_emit(Ref)
	      )),
	( LRefs == [] ->
	    true
	;   LRefs = [DPool::'$fun'(_,_,_)] ->
	    ( recorded( '$pool'(DPool,Pool_Name)) ->
		record_without_doublon( '$pool'(Pool,Pool_Name) )
	    ;
		true
	    )
	;   LRefs = ['$term'(Id,_)] ->
	    Pool_Name = 'build_ref_'+Id,
	    record_without_doublon( '$pool'(Pool,Pool_Name) )
	;   LRefs = ['$seed'(Id,_)] ->
	    Pool_Name = 'build_seed' + Id,
	    record_without_doublon( '$pool'(Pool,Pool_Name) )
	;   %% Build pool Pool based on content of LRefs
	    decompose_refs(LRefs,L1,L2),
	    length(L1,N1),
	    length(L2,N2),
	    N is N1+N2+1,
	    (	N == 1		% Void pool
	    xor Pool_Name='pool_'+Pool,
		record_without_doublon( '$pool'(Pool,Pool_Name) ),
		append(L1,L2,L),
		M is N1 + (N2 << 16),
		ma_emit_table(local,Pool_Name,N,[M|L]),
		nl
	    )
	)
	.

:-rec_prolog decompose_refs/3.

decompose_refs([],[],[]).
decompose_refs(['$term'(Id,_)|Refs],['build_ref_' + Id|L1],L2) :- decompose_refs(Refs,L1,L2).
decompose_refs(['$seed'(Id,_)|Refs],['build_seed_' + Id|L1],L2) :- decompose_refs(Refs,L1,L2).
decompose_refs([Ref::'$fun'(Id,_,_)|Refs],L1,L2) :-
	( recorded('$pool'(Ref,Ref_Pool_Name)) ->
	    ( Ref_Pool_Name = 'pool_' + _ ->
		L1=L11,
		L2=[Ref_Pool_Name|L22]
	    ;
		L1=[Ref_Pool_Name|L11],
		L2=L22
	    )
	;
	    L1=L11,
	    L2=L22
	),
	decompose_refs(Refs,L11,L22).


:-rec_prolog data_emit/1.

emit_code_elem(CC) :-
	( CC == noop ->
	    true
	; CC = noop(_) ->
	    %% to be able to distinguish similar codes
	    true
	;   CC = notest(F,Args) ->
	    ma_emit_call_c(F,Args)
	;   CC = failtest(F,Args) ->
	    ma_emit_call_c(F,Args),
	    ma_emit_true_ret
	;   test_inst(CC, F, Args ) ->
	    ma_emit_call_c(F,Args),
	    ma_emit_fail_ret
	;   inst(CC, F, Args ) -> ma_emit_call_c(F,Args)
	%%	Next case handled in linearize !
	;   pl_inst(CC, F, Args ) -> ma_emit_pl_call(F,Args)
	%% Special Instructions
	;   special_inst(CC) ->
	    true
	;   
	    internal_error('Not a dyam instruction ~w',[CC])
	)
	.

emit_globals(A,Fun) :-
	(   A = '$term'(Id,_), \+ recorded( global(Fun,A) ) ->
	    ma_emit_call_c('build_ref_' + Id,[]),
%%	    ma_emit_cache_or_build(A,'build_ref_' + Id),
	    register_global(Fun,A)
	;   A = '$seed'(Id,_), \+ recorded( global(Fun,A) ) ->
	    ma_emit_call_c('build_seed_' + Id,[]),
	    register_global(Fun,A)
	;   A = '$fun'(_,_,_), \+ recorded( global(Fun,A) ) ->
	    register_global(Fun,A)
	;   A = [_|_] ->
	    every((domain(B,A),emit_globals(B,Fun)))
	;   
	    true
	)
	.

:-std_prolog register_global/2.

register_global(Fun,A) :-
	record( global(Fun,A) )
	.

/*
	( domain(A,['$term'(_,M),'$seed'(_,M),'$fun'(_,_,M)]) ->
	    mutable_read(M,[]) xor add_ordered_table( globals, global(Fun,A), _ )
	;   
	    true
	)
	.
*/

%:-std_prolog emit_globals_in_init_pool/1.

emit_globals_in_init_pool(A) :-
%%	format(';; Code Arg ~w Fun ~a\n',[A,Fun]),
	( domain(A,['$term'(_,_),'$seed'(_,_)]) ->
	    record_without_doublon( init_pool(A) )
	;   A = '$fun'(_,_,_) ->
	    register_init_pool_fun(A)
	;   A = [_|_] ->
	    every((domain(B,A),emit_globals_in_init_pool(B)))
	;
	    true
	)
	.

register_init_pool_fun(Id::'$fun'(_,_,Info)) :-
	(   recorded( init_pool(Id) )
	xor record( init_pool(Id)),
	    mutable_read(Info,'$fun_info'{code=>Code,refs=>LRefs,ins=>Ins}),
	    emit_globals_in_init_pool(LRefs),
	    every((domain(F::'$fun'(_,_,_),LRefs),
		   register_init_pool_fun(F)))
	)
	.
					  
special_inst(call(F,Args)) :- ma_emit_pl_call(F,Args).
special_inst(label(L)) :-  ma_emit_label(L).
special_inst(return) :- ma_emit_ret.
special_inst(succeed) :- ma_emit_pl_ret.
special_inst(fail) :- ma_emit_pl_fail.
special_inst(jump(F)) :-   ma_emit_jump(F).
special_inst(pl_jump(F,Args)) :-
	( reg_value(F,0) ->
	    ma_emit_pl_jump(F,Args)
	;   Args == [],
	    inlinable(F) ->
	    %% First occurence of pl_jump to inlinable function is inlined
	    ma_emit_label(F),
	    emit_inlined_function(F)
	;   
	    ma_emit_pl_jump(F,Args)
	)
	.

:-std_prolog inlinable/1.

inlinable(Id::'$fun'(_,0,CodeM)) :-
	\+ mutable_read(CodeM,[]),
	jump_value(Id,V),
	V>0,
	\+ recorded( named_function(Id,Name,global) )
	.

special_inst(c_call(F,Args,Return,Reg)) :-
	ma_emit_call_c(F,Args),
	(   Return == none xor ma_emit_fail_ret ),
	(   Reg == none	xor ma_emit_move_ret(Reg))
	.

test_inst(foreign(F,Args),'DYAM_'+F,Args).
	 
%% Operations

test_inst(( X == V ),'Dyam_Bind',[X,V]).
test_inst(item_unify( T ),'Dyam_Unify_Item',[T]).
test_inst(unify( A, B ),'Dyam_Unify',[A,B]).
test_inst(unify( A, KA, B, KB ),'sfol_unify',[A,KA,B,KB]).
	
%% DyALog cycle

pl_inst(direct( I, N ),'Apply',[c(I),c(N)]).
pl_inst(completion( I, N ),'Complete',[c(I),c(N)]).
test_inst(subsume( Seed_Id ),'Dyam_Subsume',[Seed_Id]).
test_inst(variance( Seed_Id ),'Dyam_Variance',[Seed_Id]).
	
%% DyALog objects

inst(backptr( Kind ),'Dyam_Backptr',[Kind]).
inst(backptr( Kind, Trace ),'Dyam_Backptr_Trace',[Kind,Trace]).
inst(object( Seed_Id ),'Dyam_Object',[Seed_Id]).
inst(light_object( Seed_Id ),'Dyam_Light_Object',[Seed_Id]).
inst(tabule,'Dyam_Tabule',[]).
inst(schedule,'Dyam_Schedule',[]).
inst(schedule_first,'Dyam_Now',[]).
inst(schedule_last,'Dyam_Schedule_Last',[]).
inst(answer,'Dyam_Answer',[]).
inst(indexingkey(Key,Subs_Comp_Id),'Dyam_Compute_IndexingKey',[Key,Subs_Comp_Id]).

%% DyALog Control

inst(choice(F),'Dyam_Choice',[F]).
inst(update_choice(F),'Dyam_Update_Choice',[F]).
inst(remove_choice,'Dyam_Remove_Choice',[]).
inst(cut,'Dyam_Cut',[]).
inst(setcut,'Dyam_Set_Cut',[]).

inst(allocate,'Dyam_Allocate',[c(0)]).
inst(deallocate,'Dyam_Deallocate',[]).
inst(deallocate_alt,'Dyam_Deallocate_Alt',[]).

inst(foreign_void_return(F,Args),'DYAM_'+F,Args).

%% Using registers with stdprolog predicates

inst( allocate_layer, 'Dyam_Reg_Allocate_Layer',[] ).

%%inst( deallocate_layer, 'Dyam_Reg_Deallocate_Layer',[] ).
special_inst( deallocate_layer ).

%%inst( reg_reset(_), 'Dyam_Reg_Reset',[] ).
special_inst( reg_reset(_) ).
	     
inst( reg_load(I,A), 'Dyam_Reg_Load', [c(I),A] ).

%%inst( reg_zero(I), 'Dyam_Reg_Load_Zero', [c(I)] ).
special_inst( reg_zero(I) ) :-
	ma_emit_move(c(0),'$reg'(I))
	.

%%inst( reg_load_cst(I,A), 'Dyam_Reg_Load_Cst', [c(I),A] ).
special_inst( reg_load_cst(I,A) ) :-
	emit_move(I,A,c(0)).

%%inst( reg_load_nil(I), 'Dyam_Reg_Load_Nil', [c(I)] ).
special_inst( reg_load_nil(I) ) :-
	label_term([],Nil),
	emit_move(I,Nil,c(0)).

%%inst( reg_load_var(I,A), 'Dyam_Reg_Load_Var', [c(I),A] ).
special_inst( reg_load_var(I,A) ) :-
	emit_move(I,A,'$system'(trans_k))
	.

:-std_prolog emit_move/3.

emit_move(I,A,Key) :-
	K is I+1,
	ma_emit_move(A,'$reg'(I)),
	ma_emit_move(Key,'$reg'(K) )
	.

test_inst( reg_unif(I,A), 'Dyam_Reg_Unify',[c(I),A] ).
test_inst( reg_unif_cst(I,A), 'Dyam_Reg_Unify_Cst',[c(I),A] ).
test_inst( reg_unif_nil(I), 'Dyam_Reg_Unify_Nil',[c(I)] ).
inst( reg_bind(I,A), 'Dyam_Reg_Bind',[c(I),A] ).
inst( reg_multi_bind(I,A,N), 'Dyam_Multi_Reg_Bind',[c(I),c(A),c(N)] ).
inst( reg_multi_bind_zero(A,N), 'Dyam_Multi_Reg_Bind_Zero',[c(A),c(N)] ).
inst( reg_multi_bind_zero_one(N), 'Dyam_Multi_Reg_Bind_ZeroOne',[c(N)] ).
inst(reg_deallocate(N),'Dyam_Reg_Deallocate',[c(N)]).
inst(reg_deallocate_alt(N),'Dyam_Reg_Deallocate_Alt',[c(N)]).

%% Using registers with '$interface'

test_inst( reg_load_number(I,A), 'Dyam_Reg_Load_Number',[c(I),A] ).
test_inst( reg_load_float(I,A), 'Dyam_Reg_Load_Float',[c(I),A] ).
test_inst( reg_load_ptr(I,A), 'Dyam_Reg_Load_Ptr',[c(I),A] ).
test_inst( reg_load_boolean(I,A), 'Dyam_Reg_Load_Boolean',[c(I),A] ).
test_inst( reg_load_char(I,A), 'Dyam_Reg_Load_Char',[c(I),A] ).
test_inst( reg_load_string(I,A), 'Dyam_Reg_Load_String',[c(I),A] ).
test_inst( reg_load_input(I,A), 'Dyam_Reg_Load_Input',[c(I),A] ).
test_inst( reg_load_output(I,A), 'Dyam_Reg_Load_Output',[c(I),A] ).

test_inst( reg_unif_c_number(I,A), 'Dyam_Reg_Unify_C_Number',[c(I),A] ).
test_inst( reg_unif_c_float(I,A), 'Dyam_Reg_Unify_C_Float',[c(I),A] ).
test_inst( reg_unif_c_ptr(I,A), 'Dyam_Reg_Unify_C_Ptr',[c(I),A] ).
test_inst( reg_unif_c_boolean(I,A), 'Dyam_Reg_Unify_C_Boolean',[c(I),A] ).
test_inst( reg_unif_c_char(I,A), 'Dyam_Reg_Unify_C_Char',[c(I),A] ).
test_inst( reg_unif_c_string(I,A), 'Dyam_Reg_Unify_C_String',[c(I),A] ).
test_inst( reg_unif_c_input(I,A), 'Dyam_Reg_Unify_C_Input',[c(I),A] ).
test_inst( reg_unif_c_output(I,A), 'Dyam_Reg_Unify_C_Output',[c(I),A] ).

test_inst( mem_load_number(I,A), 'Dyam_Mem_Load_Number',[c(I),A] ).
test_inst( mem_load_float(I,A), 'Dyam_Mem_Load_Float',[c(I),A] ).
test_inst( mem_load_ptr(I,A), 'Dyam_Mem_Load_Ptr',[c(I),A] ).
test_inst( mem_load_boolean(I,A), 'Dyam_Mem_Load_Boolean',[c(I),A] ).
test_inst( mem_load_char(I,A), 'Dyam_Mem_Load_Char',[c(I),A] ).
test_inst( mem_load_string(I,A), 'Dyam_Mem_Load_String',[c(I),A] ).
test_inst( mem_load_input(I,A), 'Dyam_Mem_Load_Input',[c(I),A] ).
test_inst( mem_load_output(I,A), 'Dyam_Mem_Load_Output',[c(I),A] ).
inst( mem_load(I,A), 'Dyam_Mem_Load', [c(I),A] ).
inst( mem_load_cst(I,A), 'Dyam_Mem_Load_Cst', [c(I),A] ).
inst( mem_load_nil(I), 'Dyam_Mem_Load_Nil', [c(I)] ).
inst( mem_load_var(I,A), 'Dyam_Mem_Load_Var', [c(I),A] ).

test_inst( reg_alt_unif(I,A), 'Dyam_Reg_Alt_Unify',[c(I),A] ).
test_inst( reg_alt_unif_cst(I,A), 'Dyam_Reg_Alt_Unify_Cst',[c(I),A] ).
test_inst( reg_alt_unif_nil(I), 'Dyam_Reg_Alt_Unify_Nil',[c(I)] ).
inst( reg_alt_bind(I,A), 'Dyam_Reg_Alt_Bind',[c(I),A] ).

inst( forget_current_item, 'Dyam_Forget_Current_Item',[]).

% test_inst( alt_unif(A,B), 'Dyam_Alt_Unify',[A,B] ).
% test_inst( alt_unif_cst(A,B), 'Dyam_Alt_Unify',[A,B] ).
% test_inst( alt_unif_nil(A), 'Dyam_Alt_Unify_Nil',[A] ).
% inst( alt_bind(A,B), 'Dyam_Alt_Bind',[A,B] ).

special_inst( closure(A,B,C) ) :-
	update_counter( closure_label, V ),
	format( '\tjmp_reg  ~a, Lclosure~w\n',[A,V] ),
	ma_emit_call_c( 'Dyam_Closure_Aux', [B,C] ),
	ma_emit_move_ret(A),
	format('Lclosure~w:\n',[V])
%	format('\tcall_c   ~x(~x~l)\n',['Dyam_Closure',A,[',~a',''],[B,C]])
	.





