;; Compiler: DyALog 1.14.0
;; File "main.pl"


;;------------------ LOADING ------------

c_code local load
	call_c   Dyam_Loading_Set()
	pl_call  fun53(&seed[0],0)
	pl_call  fun53(&seed[4],0)
	pl_call  fun53(&seed[6],0)
	pl_call  fun53(&seed[5],0)
	pl_call  fun53(&seed[1],0)
	pl_call  fun53(&seed[2],0)
	pl_call  fun53(&seed[3],0)
	pl_call  fun14(&seed[7],0)
	pl_call  fun4(&seed[25],0)
	pl_call  fun4(&seed[24],0)
	pl_call  fun4(&seed[23],0)
	pl_call  fun4(&seed[22],0)
	pl_call  fun4(&seed[21],0)
	pl_call  fun4(&seed[20],0)
	pl_call  fun4(&seed[19],0)
	pl_call  fun4(&seed[18],0)
	pl_call  fun4(&seed[18],0)
	pl_call  fun4(&seed[17],0)
	pl_call  fun4(&seed[16],0)
	pl_call  fun4(&seed[15],0)
	pl_call  fun4(&seed[14],0)
	pl_call  fun4(&seed[13],0)
	pl_call  fun4(&seed[12],0)
	pl_call  fun4(&seed[11],0)
	pl_call  fun4(&seed[10],0)
	pl_call  fun4(&seed[9],0)
	pl_call  fun4(&seed[8],0)
	call_c   Dyam_Loading_Reset()
	c_ret

pl_code global pred_usage_0
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	pl_call  pred_emit_compiler_info_0()
	move     &ref[75], R(0)
	move     0, R(1)
	move     I(0), R(2)
	move     0, R(3)
	pl_call  pred_format_2()
	call_c   Dyam_Choice(fun22)
	pl_call  Object_1(&ref[76])
	move     &ref[77], R(0)
	move     0, R(1)
	move     &ref[78], R(2)
	move     S(5), R(3)
	pl_call  pred_format_2()
	pl_fail

pl_code global pred_main_file_1
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   DYAM_Decompose_File_Name_4(V(1),V(2),V(3),V(4))
	fail_ret
	call_c   DYAM_Atom_Concat_3(V(3),&ref[72],V(5))
	fail_ret
	call_c   DYAM_evpred_retract(&ref[73])
	fail_ret
	call_c   DYAM_evpred_assert_1(&ref[74])
	call_c   Dyam_Deallocate()
	pl_ret

pl_code global pred_read_file_alt_1
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Choice(fun11)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[54])
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(1))
	move     &ref[55], R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_read_files_2()

pl_code global pred_emit_compiler_info_0
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   DYAM_Compiler_Info_1(&ref[51])
	fail_ret
	move     &ref[52], R(0)
	move     0, R(1)
	move     &ref[53], R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_format_2()

pl_code global pred_load_tfs_1
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Choice(fun8)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_atom(V(1))
	fail_ret
	call_c   Dyam_Cut()
fun7:
	call_c   Dyam_Unify(V(2),V(1))
	fail_ret
	call_c   Dyam_Choice(fun6)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[45])
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun5)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(3),V(2))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Deallocate()
	pl_ret



;;------------------ VIEWERS ------------

c_code local build_viewers
	call_c   Dyam_Load_Viewer(&ref[13],&ref[12])
	call_c   Dyam_Load_Viewer(&ref[11],&ref[10])
	call_c   Dyam_Load_Viewer(&ref[9],&ref[8])
	call_c   Dyam_Load_Viewer(&ref[7],&ref[5])
	call_c   Dyam_Load_Viewer(&ref[2],&ref[3])
	call_c   Dyam_Load_Viewer(&ref[0],&ref[1])
	c_ret


;;------------------ DIRECTIVES ------------

c_code global main_directive_initialization
	call_c   build_init_pool()
	call_c   DYAM_Finite_Set_2(&ref[222],&ref[221])
	call_c   DYAM_Finite_Set_2(&ref[224],&ref[223])
	call_c   DYAM_Feature_2(&ref[225],&ref[226])
	call_c   DYAM_Feature_2(&ref[227],&ref[228])
	call_c   DYAM_Feature_2(&ref[229],&ref[230])
	call_c   DYAM_Feature_2(&ref[231],&ref[232])
	call_c   DYAM_Feature_2(&ref[233],&ref[234])
	call_c   DYAM_Feature_2(&ref[235],&ref[236])
	call_c   DYAM_Feature_2(&ref[237],&ref[238])
	call_c   DYAM_Feature_2(&ref[239],&ref[240])
	call_c   DYAM_Feature_2(&ref[241],&ref[242])
	call_c   DYAM_Feature_2(&ref[243],&ref[244])
	call_c   DYAM_Feature_2(&ref[245],&ref[246])
	call_c   DYAM_Feature_2(&ref[247],&ref[248])
	call_c   DYAM_Feature_2(&ref[249],&ref[250])
	call_c   DYAM_Feature_2(&ref[251],&ref[252])
	call_c   DYAM_Feature_2(&ref[253],&ref[254])
	call_c   DYAM_Feature_2(&ref[255],&ref[256])
	call_c   DYAM_Feature_2(&ref[257],&ref[258])
	call_c   DYAM_Feature_2(&ref[259],&ref[260])
	call_c   DYAM_Feature_2(&ref[261],&ref[262])
	call_c   DYAM_Feature_2(&ref[263],&ref[264])
	call_c   DYAM_Feature_2(&ref[265],&ref[266])
	call_c   DYAM_Feature_2(&ref[267],&ref[268])
	c_ret

c_code local build_seed_8
	ret_reg &seed[8]
	call_c   build_ref_20()
	call_c   build_ref_21()
	call_c   Dyam_Seed_Start(&ref[20],&ref[21],&ref[21],fun1,0)
	call_c   Dyam_Seed_End()
	move_ret seed[8]
	c_ret

pl_code local fun1
	pl_ret

;; TERM 21: usage_info('<file>', '\tload file')
c_code local build_ref_21
	ret_reg &ref[21]
	call_c   build_ref_269()
	call_c   build_ref_270()
	call_c   build_ref_271()
	call_c   Dyam_Create_Binary(&ref[269],&ref[270],&ref[271])
	move_ret ref[21]
	c_ret

;; TERM 271: '\tload file'
c_code local build_ref_271
	ret_reg &ref[271]
	call_c   Dyam_Create_Atom("\tload file")
	move_ret ref[271]
	c_ret

;; TERM 270: '<file>'
c_code local build_ref_270
	ret_reg &ref[270]
	call_c   Dyam_Create_Atom("<file>")
	move_ret ref[270]
	c_ret

;; TERM 269: usage_info
c_code local build_ref_269
	ret_reg &ref[269]
	call_c   Dyam_Create_Atom("usage_info")
	move_ret ref[269]
	c_ret

;; TERM 20: '*DATABASE*'
c_code local build_ref_20
	ret_reg &ref[20]
	call_c   Dyam_Create_Atom("*DATABASE*")
	move_ret ref[20]
	c_ret

c_code local build_seed_9
	ret_reg &seed[9]
	call_c   build_ref_20()
	call_c   build_ref_22()
	call_c   Dyam_Seed_Start(&ref[20],&ref[22],&ref[22],fun1,0)
	call_c   Dyam_Seed_End()
	move_ret seed[9]
	c_ret

;; TERM 22: usage_info('-usage', '\tdisplay this help and exit')
c_code local build_ref_22
	ret_reg &ref[22]
	call_c   build_ref_269()
	call_c   build_ref_272()
	call_c   build_ref_273()
	call_c   Dyam_Create_Binary(&ref[269],&ref[272],&ref[273])
	move_ret ref[22]
	c_ret

;; TERM 273: '\tdisplay this help and exit'
c_code local build_ref_273
	ret_reg &ref[273]
	call_c   Dyam_Create_Atom("\tdisplay this help and exit")
	move_ret ref[273]
	c_ret

;; TERM 272: '-usage'
c_code local build_ref_272
	ret_reg &ref[272]
	call_c   Dyam_Create_Atom("-usage")
	move_ret ref[272]
	c_ret

c_code local build_seed_10
	ret_reg &seed[10]
	call_c   build_ref_20()
	call_c   build_ref_23()
	call_c   Dyam_Seed_Start(&ref[20],&ref[23],&ref[23],fun1,0)
	call_c   Dyam_Seed_End()
	move_ret seed[10]
	c_ret

;; TERM 23: usage_info('-toplevel', 'enter toplevel loop instead of compiling')
c_code local build_ref_23
	ret_reg &ref[23]
	call_c   build_ref_269()
	call_c   build_ref_274()
	call_c   build_ref_275()
	call_c   Dyam_Create_Binary(&ref[269],&ref[274],&ref[275])
	move_ret ref[23]
	c_ret

;; TERM 275: 'enter toplevel loop instead of compiling'
c_code local build_ref_275
	ret_reg &ref[275]
	call_c   Dyam_Create_Atom("enter toplevel loop instead of compiling")
	move_ret ref[275]
	c_ret

;; TERM 274: '-toplevel'
c_code local build_ref_274
	ret_reg &ref[274]
	call_c   Dyam_Create_Atom("-toplevel")
	move_ret ref[274]
	c_ret

c_code local build_seed_11
	ret_reg &seed[11]
	call_c   build_ref_20()
	call_c   build_ref_24()
	call_c   Dyam_Seed_Start(&ref[20],&ref[24],&ref[24],fun1,0)
	call_c   Dyam_Seed_End()
	move_ret seed[11]
	c_ret

;; TERM 24: usage_info('-analyze <mode>', 'replace compilation by program analyze for mode in {lc, tag_features, tag2tig, lctag}')
c_code local build_ref_24
	ret_reg &ref[24]
	call_c   build_ref_269()
	call_c   build_ref_276()
	call_c   build_ref_277()
	call_c   Dyam_Create_Binary(&ref[269],&ref[276],&ref[277])
	move_ret ref[24]
	c_ret

;; TERM 277: 'replace compilation by program analyze for mode in {lc, tag_features, tag2tig, lctag}'
c_code local build_ref_277
	ret_reg &ref[277]
	call_c   Dyam_Create_Atom("replace compilation by program analyze for mode in {lc, tag_features, tag2tig, lctag}")
	move_ret ref[277]
	c_ret

;; TERM 276: '-analyze <mode>'
c_code local build_ref_276
	ret_reg &ref[276]
	call_c   Dyam_Create_Atom("-analyze <mode>")
	move_ret ref[276]
	c_ret

c_code local build_seed_12
	ret_reg &seed[12]
	call_c   build_ref_20()
	call_c   build_ref_25()
	call_c   Dyam_Seed_Start(&ref[20],&ref[25],&ref[25],fun1,0)
	call_c   Dyam_Seed_End()
	move_ret seed[12]
	c_ret

;; TERM 25: usage_info('-autoload', 'set autoload mode')
c_code local build_ref_25
	ret_reg &ref[25]
	call_c   build_ref_269()
	call_c   build_ref_278()
	call_c   build_ref_279()
	call_c   Dyam_Create_Binary(&ref[269],&ref[278],&ref[279])
	move_ret ref[25]
	c_ret

;; TERM 279: 'set autoload mode'
c_code local build_ref_279
	ret_reg &ref[279]
	call_c   Dyam_Create_Atom("set autoload mode")
	move_ret ref[279]
	c_ret

;; TERM 278: '-autoload'
c_code local build_ref_278
	ret_reg &ref[278]
	call_c   Dyam_Create_Atom("-autoload")
	move_ret ref[278]
	c_ret

c_code local build_seed_13
	ret_reg &seed[13]
	call_c   build_ref_20()
	call_c   build_ref_26()
	call_c   Dyam_Seed_Start(&ref[20],&ref[26],&ref[26],fun1,0)
	call_c   Dyam_Seed_End()
	move_ret seed[13]
	c_ret

;; TERM 26: usage_info('-guide <mode>', 'compile with guide in {tag}')
c_code local build_ref_26
	ret_reg &ref[26]
	call_c   build_ref_269()
	call_c   build_ref_280()
	call_c   build_ref_281()
	call_c   Dyam_Create_Binary(&ref[269],&ref[280],&ref[281])
	move_ret ref[26]
	c_ret

;; TERM 281: 'compile with guide in {tag}'
c_code local build_ref_281
	ret_reg &ref[281]
	call_c   Dyam_Create_Atom("compile with guide in {tag}")
	move_ret ref[281]
	c_ret

;; TERM 280: '-guide <mode>'
c_code local build_ref_280
	ret_reg &ref[280]
	call_c   Dyam_Create_Atom("-guide <mode>")
	move_ret ref[280]
	c_ret

c_code local build_seed_14
	ret_reg &seed[14]
	call_c   build_ref_20()
	call_c   build_ref_27()
	call_c   Dyam_Seed_Start(&ref[20],&ref[27],&ref[27],fun1,0)
	call_c   Dyam_Seed_End()
	move_ret seed[14]
	c_ret

;; TERM 27: usage_info('-stop <level>', 'stop compilation at <level> in {load,pda,emit}')
c_code local build_ref_27
	ret_reg &ref[27]
	call_c   build_ref_269()
	call_c   build_ref_282()
	call_c   build_ref_283()
	call_c   Dyam_Create_Binary(&ref[269],&ref[282],&ref[283])
	move_ret ref[27]
	c_ret

;; TERM 283: 'stop compilation at <level> in {load,pda,emit}'
c_code local build_ref_283
	ret_reg &ref[283]
	call_c   Dyam_Create_Atom("stop compilation at <level> in {load,pda,emit}")
	move_ret ref[283]
	c_ret

;; TERM 282: '-stop <level>'
c_code local build_ref_282
	ret_reg &ref[282]
	call_c   Dyam_Create_Atom("-stop <level>")
	move_ret ref[282]
	c_ret

c_code local build_seed_15
	ret_reg &seed[15]
	call_c   build_ref_20()
	call_c   build_ref_28()
	call_c   Dyam_Seed_Start(&ref[20],&ref[28],&ref[28],fun1,0)
	call_c   Dyam_Seed_End()
	move_ret seed[15]
	c_ret

;; TERM 28: usage_info('-warning', 'set warning mode')
c_code local build_ref_28
	ret_reg &ref[28]
	call_c   build_ref_269()
	call_c   build_ref_284()
	call_c   build_ref_285()
	call_c   Dyam_Create_Binary(&ref[269],&ref[284],&ref[285])
	move_ret ref[28]
	c_ret

;; TERM 285: 'set warning mode'
c_code local build_ref_285
	ret_reg &ref[285]
	call_c   Dyam_Create_Atom("set warning mode")
	move_ret ref[285]
	c_ret

;; TERM 284: '-warning'
c_code local build_ref_284
	ret_reg &ref[284]
	call_c   Dyam_Create_Atom("-warning")
	move_ret ref[284]
	c_ret

c_code local build_seed_16
	ret_reg &seed[16]
	call_c   build_ref_20()
	call_c   build_ref_29()
	call_c   Dyam_Seed_Start(&ref[20],&ref[29],&ref[29],fun1,0)
	call_c   Dyam_Seed_End()
	move_ret seed[16]
	c_ret

;; TERM 29: usage_info('-rcg', '\tset rcg mode')
c_code local build_ref_29
	ret_reg &ref[29]
	call_c   build_ref_269()
	call_c   build_ref_286()
	call_c   build_ref_287()
	call_c   Dyam_Create_Binary(&ref[269],&ref[286],&ref[287])
	move_ret ref[29]
	c_ret

;; TERM 287: '\tset rcg mode'
c_code local build_ref_287
	ret_reg &ref[287]
	call_c   Dyam_Create_Atom("\tset rcg mode")
	move_ret ref[287]
	c_ret

;; TERM 286: '-rcg'
c_code local build_ref_286
	ret_reg &ref[286]
	call_c   Dyam_Create_Atom("-rcg")
	move_ret ref[286]
	c_ret

c_code local build_seed_17
	ret_reg &seed[17]
	call_c   build_ref_20()
	call_c   build_ref_30()
	call_c   Dyam_Seed_Start(&ref[20],&ref[30],&ref[30],fun1,0)
	call_c   Dyam_Seed_End()
	move_ret seed[17]
	c_ret

;; TERM 30: usage_info('-tfs <lib>', 'load TFS library')
c_code local build_ref_30
	ret_reg &ref[30]
	call_c   build_ref_269()
	call_c   build_ref_288()
	call_c   build_ref_289()
	call_c   Dyam_Create_Binary(&ref[269],&ref[288],&ref[289])
	move_ret ref[30]
	c_ret

;; TERM 289: 'load TFS library'
c_code local build_ref_289
	ret_reg &ref[289]
	call_c   Dyam_Create_Atom("load TFS library")
	move_ret ref[289]
	c_ret

;; TERM 288: '-tfs <lib>'
c_code local build_ref_288
	ret_reg &ref[288]
	call_c   Dyam_Create_Atom("-tfs <lib>")
	move_ret ref[288]
	c_ret

c_code local build_seed_18
	ret_reg &seed[18]
	call_c   build_ref_20()
	call_c   build_ref_31()
	call_c   Dyam_Seed_Start(&ref[20],&ref[31],&ref[31],fun1,0)
	call_c   Dyam_Seed_End()
	move_ret seed[18]
	c_ret

;; TERM 31: usage_info('-version', 'display compiler version')
c_code local build_ref_31
	ret_reg &ref[31]
	call_c   build_ref_269()
	call_c   build_ref_290()
	call_c   build_ref_291()
	call_c   Dyam_Create_Binary(&ref[269],&ref[290],&ref[291])
	move_ret ref[31]
	c_ret

;; TERM 291: 'display compiler version'
c_code local build_ref_291
	ret_reg &ref[291]
	call_c   Dyam_Create_Atom("display compiler version")
	move_ret ref[291]
	c_ret

;; TERM 290: '-version'
c_code local build_ref_290
	ret_reg &ref[290]
	call_c   Dyam_Create_Atom("-version")
	move_ret ref[290]
	c_ret

c_code local build_seed_19
	ret_reg &seed[19]
	call_c   build_ref_20()
	call_c   build_ref_32()
	call_c   Dyam_Seed_Start(&ref[20],&ref[32],&ref[32],fun1,0)
	call_c   Dyam_Seed_End()
	move_ret seed[19]
	c_ret

;; TERM 32: usage_info('-use <file>', 'load module file')
c_code local build_ref_32
	ret_reg &ref[32]
	call_c   build_ref_269()
	call_c   build_ref_292()
	call_c   build_ref_293()
	call_c   Dyam_Create_Binary(&ref[269],&ref[292],&ref[293])
	move_ret ref[32]
	c_ret

;; TERM 293: 'load module file'
c_code local build_ref_293
	ret_reg &ref[293]
	call_c   Dyam_Create_Atom("load module file")
	move_ret ref[293]
	c_ret

;; TERM 292: '-use <file>'
c_code local build_ref_292
	ret_reg &ref[292]
	call_c   Dyam_Create_Atom("-use <file>")
	move_ret ref[292]
	c_ret

c_code local build_seed_20
	ret_reg &seed[20]
	call_c   build_ref_20()
	call_c   build_ref_33()
	call_c   Dyam_Seed_Start(&ref[20],&ref[33],&ref[33],fun1,0)
	call_c   Dyam_Seed_End()
	move_ret seed[20]
	c_ret

;; TERM 33: usage_info('-res <file>', 'load resource file')
c_code local build_ref_33
	ret_reg &ref[33]
	call_c   build_ref_269()
	call_c   build_ref_294()
	call_c   build_ref_295()
	call_c   Dyam_Create_Binary(&ref[269],&ref[294],&ref[295])
	move_ret ref[33]
	c_ret

;; TERM 295: 'load resource file'
c_code local build_ref_295
	ret_reg &ref[295]
	call_c   Dyam_Create_Atom("load resource file")
	move_ret ref[295]
	c_ret

;; TERM 294: '-res <file>'
c_code local build_ref_294
	ret_reg &ref[294]
	call_c   Dyam_Create_Atom("-res <file>")
	move_ret ref[294]
	c_ret

c_code local build_seed_21
	ret_reg &seed[21]
	call_c   build_ref_20()
	call_c   build_ref_34()
	call_c   Dyam_Seed_Start(&ref[20],&ref[34],&ref[34],fun1,0)
	call_c   Dyam_Seed_End()
	move_ret seed[21]
	c_ret

;; TERM 34: usage_info('-I <path>', 'add path to file search path')
c_code local build_ref_34
	ret_reg &ref[34]
	call_c   build_ref_269()
	call_c   build_ref_296()
	call_c   build_ref_297()
	call_c   Dyam_Create_Binary(&ref[269],&ref[296],&ref[297])
	move_ret ref[34]
	c_ret

;; TERM 297: 'add path to file search path'
c_code local build_ref_297
	ret_reg &ref[297]
	call_c   Dyam_Create_Atom("add path to file search path")
	move_ret ref[297]
	c_ret

;; TERM 296: '-I <path>'
c_code local build_ref_296
	ret_reg &ref[296]
	call_c   Dyam_Create_Atom("-I <path>")
	move_ret ref[296]
	c_ret

c_code local build_seed_22
	ret_reg &seed[22]
	call_c   build_ref_20()
	call_c   build_ref_35()
	call_c   Dyam_Seed_Start(&ref[20],&ref[35],&ref[35],fun1,0)
	call_c   Dyam_Seed_End()
	move_ret seed[22]
	c_ret

;; TERM 35: usage_info('-verbose_tag', 'set verbose mode for TAGs')
c_code local build_ref_35
	ret_reg &ref[35]
	call_c   build_ref_269()
	call_c   build_ref_298()
	call_c   build_ref_299()
	call_c   Dyam_Create_Binary(&ref[269],&ref[298],&ref[299])
	move_ret ref[35]
	c_ret

;; TERM 299: 'set verbose mode for TAGs'
c_code local build_ref_299
	ret_reg &ref[299]
	call_c   Dyam_Create_Atom("set verbose mode for TAGs")
	move_ret ref[299]
	c_ret

;; TERM 298: '-verbose_tag'
c_code local build_ref_298
	ret_reg &ref[298]
	call_c   Dyam_Create_Atom("-verbose_tag")
	move_ret ref[298]
	c_ret

c_code local build_seed_23
	ret_reg &seed[23]
	call_c   build_ref_20()
	call_c   build_ref_36()
	call_c   Dyam_Seed_Start(&ref[20],&ref[36],&ref[36],fun1,0)
	call_c   Dyam_Seed_End()
	move_ret seed[23]
	c_ret

;; TERM 36: usage_info('-parse', '\tset parse mode')
c_code local build_ref_36
	ret_reg &ref[36]
	call_c   build_ref_269()
	call_c   build_ref_300()
	call_c   build_ref_301()
	call_c   Dyam_Create_Binary(&ref[269],&ref[300],&ref[301])
	move_ret ref[36]
	c_ret

;; TERM 301: '\tset parse mode'
c_code local build_ref_301
	ret_reg &ref[301]
	call_c   Dyam_Create_Atom("\tset parse mode")
	move_ret ref[301]
	c_ret

;; TERM 300: '-parse'
c_code local build_ref_300
	ret_reg &ref[300]
	call_c   Dyam_Create_Atom("-parse")
	move_ret ref[300]
	c_ret

c_code local build_seed_24
	ret_reg &seed[24]
	call_c   build_ref_20()
	call_c   build_ref_37()
	call_c   Dyam_Seed_Start(&ref[20],&ref[37],&ref[37],fun1,0)
	call_c   Dyam_Seed_End()
	move_ret seed[24]
	c_ret

;; TERM 37: usage_info('-o <file>', 'output file <file>')
c_code local build_ref_37
	ret_reg &ref[37]
	call_c   build_ref_269()
	call_c   build_ref_302()
	call_c   build_ref_303()
	call_c   Dyam_Create_Binary(&ref[269],&ref[302],&ref[303])
	move_ret ref[37]
	c_ret

;; TERM 303: 'output file <file>'
c_code local build_ref_303
	ret_reg &ref[303]
	call_c   Dyam_Create_Atom("output file <file>")
	move_ret ref[303]
	c_ret

;; TERM 302: '-o <file>'
c_code local build_ref_302
	ret_reg &ref[302]
	call_c   Dyam_Create_Atom("-o <file>")
	move_ret ref[302]
	c_ret

c_code local build_seed_25
	ret_reg &seed[25]
	call_c   build_ref_20()
	call_c   build_ref_38()
	call_c   Dyam_Seed_Start(&ref[20],&ref[38],&ref[38],fun1,0)
	call_c   Dyam_Seed_End()
	move_ret seed[25]
	c_ret

;; TERM 38: usage_info('-f <file>', 'load input file <file>')
c_code local build_ref_38
	ret_reg &ref[38]
	call_c   build_ref_269()
	call_c   build_ref_304()
	call_c   build_ref_305()
	call_c   Dyam_Create_Binary(&ref[269],&ref[304],&ref[305])
	move_ret ref[38]
	c_ret

;; TERM 305: 'load input file <file>'
c_code local build_ref_305
	ret_reg &ref[305]
	call_c   Dyam_Create_Atom("load input file <file>")
	move_ret ref[305]
	c_ret

;; TERM 304: '-f <file>'
c_code local build_ref_304
	ret_reg &ref[304]
	call_c   Dyam_Create_Atom("-f <file>")
	move_ret ref[304]
	c_ret

c_code local build_seed_7
	ret_reg &seed[7]
	call_c   build_ref_14()
	call_c   build_ref_16()
	call_c   Dyam_Seed_Start(&ref[14],&ref[16],&ref[16],fun0,1)
	call_c   build_ref_19()
	call_c   Dyam_Seed_Add_Comp(&ref[19],&ref[16],0)
	call_c   Dyam_Seed_End()
	move_ret seed[7]
	c_ret

pl_code local fun0
	pl_jump  Complete(0,0)

;; TERM 19: '*FIRST*'(start) :> '$$HOLE$$'
c_code local build_ref_19
	ret_reg &ref[19]
	call_c   build_ref_18()
	call_c   Dyam_Create_Binary(I(9),&ref[18],I(7))
	move_ret ref[19]
	c_ret

;; TERM 18: '*FIRST*'(start)
c_code local build_ref_18
	ret_reg &ref[18]
	call_c   build_ref_17()
	call_c   build_ref_15()
	call_c   Dyam_Create_Unary(&ref[17],&ref[15])
	move_ret ref[18]
	c_ret

;; TERM 15: start
c_code local build_ref_15
	ret_reg &ref[15]
	call_c   Dyam_Create_Atom("start")
	move_ret ref[15]
	c_ret

;; TERM 17: '*FIRST*'
c_code local build_ref_17
	ret_reg &ref[17]
	call_c   Dyam_Create_Atom("*FIRST*")
	move_ret ref[17]
	c_ret

;; TERM 16: '*CITEM*'(start, start)
c_code local build_ref_16
	ret_reg &ref[16]
	call_c   build_ref_14()
	call_c   build_ref_15()
	call_c   Dyam_Create_Binary(&ref[14],&ref[15],&ref[15])
	move_ret ref[16]
	c_ret

;; TERM 14: '*CITEM*'
c_code local build_ref_14
	ret_reg &ref[14]
	call_c   Dyam_Create_Atom("*CITEM*")
	move_ret ref[14]
	c_ret

c_code local build_seed_3
	ret_reg &seed[3]
	call_c   build_ref_39()
	call_c   build_ref_43()
	call_c   Dyam_Seed_Start(&ref[39],&ref[43],I(0),fun1,1)
	call_c   build_ref_42()
	call_c   Dyam_Seed_Add_Comp(&ref[42],fun2,0)
	call_c   Dyam_Seed_End()
	move_ret seed[3]
	c_ret

;; TERM 42: '*GUARD*'(get_extra_clause('$usage_info'(_B, _C)))
c_code local build_ref_42
	ret_reg &ref[42]
	call_c   build_ref_40()
	call_c   build_ref_41()
	call_c   Dyam_Create_Unary(&ref[40],&ref[41])
	move_ret ref[42]
	c_ret

;; TERM 41: get_extra_clause('$usage_info'(_B, _C))
c_code local build_ref_41
	ret_reg &ref[41]
	call_c   build_ref_306()
	call_c   build_ref_44()
	call_c   Dyam_Create_Unary(&ref[306],&ref[44])
	move_ret ref[41]
	c_ret

;; TERM 44: '$usage_info'(_B, _C)
c_code local build_ref_44
	ret_reg &ref[44]
	call_c   build_ref_307()
	call_c   Dyam_Create_Binary(&ref[307],V(1),V(2))
	move_ret ref[44]
	c_ret

;; TERM 307: '$usage_info'
c_code local build_ref_307
	ret_reg &ref[307]
	call_c   Dyam_Create_Atom("$usage_info")
	move_ret ref[307]
	c_ret

;; TERM 306: get_extra_clause
c_code local build_ref_306
	ret_reg &ref[306]
	call_c   Dyam_Create_Atom("get_extra_clause")
	move_ret ref[306]
	c_ret

;; TERM 40: '*GUARD*'
c_code local build_ref_40
	ret_reg &ref[40]
	call_c   Dyam_Create_Atom("*GUARD*")
	move_ret ref[40]
	c_ret

long local pool_fun2[3]=[2,build_ref_42,build_ref_44]

pl_code local fun2
	call_c   Dyam_Pool(pool_fun2)
	call_c   Dyam_Unify_Item(&ref[42])
	fail_ret
	pl_jump  Object_1(&ref[44])

;; TERM 43: '*GUARD*'(get_extra_clause('$usage_info'(_B, _C))) :> []
c_code local build_ref_43
	ret_reg &ref[43]
	call_c   build_ref_42()
	call_c   Dyam_Create_Binary(I(9),&ref[42],I(0))
	move_ret ref[43]
	c_ret

;; TERM 39: '*GUARD_CLAUSE*'
c_code local build_ref_39
	ret_reg &ref[39]
	call_c   Dyam_Create_Atom("*GUARD_CLAUSE*")
	move_ret ref[39]
	c_ret

c_code local build_seed_2
	ret_reg &seed[2]
	call_c   build_ref_39()
	call_c   build_ref_59()
	call_c   Dyam_Seed_Start(&ref[39],&ref[59],I(0),fun1,1)
	call_c   build_ref_58()
	call_c   Dyam_Seed_Add_Comp(&ref[58],fun20,0)
	call_c   Dyam_Seed_End()
	move_ret seed[2]
	c_ret

;; TERM 58: '*GUARD*'(pgm_to_lpda('$usage_info'(_B, _C), _D))
c_code local build_ref_58
	ret_reg &ref[58]
	call_c   build_ref_40()
	call_c   build_ref_57()
	call_c   Dyam_Create_Unary(&ref[40],&ref[57])
	move_ret ref[58]
	c_ret

;; TERM 57: pgm_to_lpda('$usage_info'(_B, _C), _D)
c_code local build_ref_57
	ret_reg &ref[57]
	call_c   build_ref_308()
	call_c   build_ref_44()
	call_c   Dyam_Create_Binary(&ref[308],&ref[44],V(3))
	move_ret ref[57]
	c_ret

;; TERM 308: pgm_to_lpda
c_code local build_ref_308
	ret_reg &ref[308]
	call_c   Dyam_Create_Atom("pgm_to_lpda")
	move_ret ref[308]
	c_ret

c_code local build_seed_26
	ret_reg &seed[26]
	call_c   build_ref_40()
	call_c   build_ref_61()
	call_c   Dyam_Seed_Start(&ref[40],&ref[61],I(0),fun0,1)
	call_c   build_ref_62()
	call_c   Dyam_Seed_Add_Comp(&ref[62],&ref[61],0)
	call_c   Dyam_Seed_End()
	move_ret seed[26]
	c_ret

;; TERM 62: '*GUARD*'(pgm_to_lpda('$fact'(usage_info(_B, _C)), _D)) :> '$$HOLE$$'
c_code local build_ref_62
	ret_reg &ref[62]
	call_c   build_ref_61()
	call_c   Dyam_Create_Binary(I(9),&ref[61],I(7))
	move_ret ref[62]
	c_ret

;; TERM 61: '*GUARD*'(pgm_to_lpda('$fact'(usage_info(_B, _C)), _D))
c_code local build_ref_61
	ret_reg &ref[61]
	call_c   build_ref_40()
	call_c   build_ref_60()
	call_c   Dyam_Create_Unary(&ref[40],&ref[60])
	move_ret ref[61]
	c_ret

;; TERM 60: pgm_to_lpda('$fact'(usage_info(_B, _C)), _D)
c_code local build_ref_60
	ret_reg &ref[60]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_309()
	call_c   build_ref_76()
	call_c   Dyam_Create_Unary(&ref[309],&ref[76])
	move_ret R(0)
	call_c   build_ref_308()
	call_c   Dyam_Create_Binary(&ref[308],R(0),V(3))
	move_ret ref[60]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 76: usage_info(_B, _C)
c_code local build_ref_76
	ret_reg &ref[76]
	call_c   build_ref_269()
	call_c   Dyam_Create_Binary(&ref[269],V(1),V(2))
	move_ret ref[76]
	c_ret

;; TERM 309: '$fact'
c_code local build_ref_309
	ret_reg &ref[309]
	call_c   Dyam_Create_Atom("$fact")
	move_ret ref[309]
	c_ret

pl_code local fun15
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Light_Object(R(0))
	pl_jump  Schedule_Prolog()

long local pool_fun20[3]=[2,build_ref_58,build_seed_26]

pl_code local fun20
	call_c   Dyam_Pool(pool_fun20)
	call_c   Dyam_Unify_Item(&ref[58])
	fail_ret
	pl_jump  fun15(&seed[26],1)

;; TERM 59: '*GUARD*'(pgm_to_lpda('$usage_info'(_B, _C), _D)) :> []
c_code local build_ref_59
	ret_reg &ref[59]
	call_c   build_ref_58()
	call_c   Dyam_Create_Binary(I(9),&ref[58],I(0))
	move_ret ref[59]
	c_ret

c_code local build_seed_1
	ret_reg &seed[1]
	call_c   build_ref_39()
	call_c   build_ref_65()
	call_c   Dyam_Seed_Start(&ref[39],&ref[65],I(0),fun1,1)
	call_c   build_ref_64()
	call_c   Dyam_Seed_Add_Comp(&ref[64],fun18,0)
	call_c   Dyam_Seed_End()
	move_ret seed[1]
	c_ret

;; TERM 64: '*GUARD*'(parse_options(_B, _C))
c_code local build_ref_64
	ret_reg &ref[64]
	call_c   build_ref_40()
	call_c   build_ref_63()
	call_c   Dyam_Create_Unary(&ref[40],&ref[63])
	move_ret ref[64]
	c_ret

;; TERM 63: parse_options(_B, _C)
c_code local build_ref_63
	ret_reg &ref[63]
	call_c   build_ref_310()
	call_c   Dyam_Create_Binary(&ref[310],V(1),V(2))
	move_ret ref[63]
	c_ret

;; TERM 310: parse_options
c_code local build_ref_310
	ret_reg &ref[310]
	call_c   Dyam_Create_Atom("parse_options")
	move_ret ref[310]
	c_ret

pl_code local fun17
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(1),V(2))
	fail_ret
fun16:
	call_c   Dyam_Deallocate()
	pl_ret


c_code local build_seed_27
	ret_reg &seed[27]
	call_c   build_ref_40()
	call_c   build_ref_67()
	call_c   Dyam_Seed_Start(&ref[40],&ref[67],I(0),fun0,1)
	call_c   build_ref_68()
	call_c   Dyam_Seed_Add_Comp(&ref[68],&ref[67],0)
	call_c   Dyam_Seed_End()
	move_ret seed[27]
	c_ret

;; TERM 68: '*GUARD*'(parse_option(_B, _D)) :> '$$HOLE$$'
c_code local build_ref_68
	ret_reg &ref[68]
	call_c   build_ref_67()
	call_c   Dyam_Create_Binary(I(9),&ref[67],I(7))
	move_ret ref[68]
	c_ret

;; TERM 67: '*GUARD*'(parse_option(_B, _D))
c_code local build_ref_67
	ret_reg &ref[67]
	call_c   build_ref_40()
	call_c   build_ref_66()
	call_c   Dyam_Create_Unary(&ref[40],&ref[66])
	move_ret ref[67]
	c_ret

;; TERM 66: parse_option(_B, _D)
c_code local build_ref_66
	ret_reg &ref[66]
	call_c   build_ref_311()
	call_c   Dyam_Create_Binary(&ref[311],V(1),V(3))
	move_ret ref[66]
	c_ret

;; TERM 311: parse_option
c_code local build_ref_311
	ret_reg &ref[311]
	call_c   Dyam_Create_Atom("parse_option")
	move_ret ref[311]
	c_ret

c_code local build_seed_28
	ret_reg &seed[28]
	call_c   build_ref_40()
	call_c   build_ref_70()
	call_c   Dyam_Seed_Start(&ref[40],&ref[70],I(0),fun0,1)
	call_c   build_ref_71()
	call_c   Dyam_Seed_Add_Comp(&ref[71],&ref[70],0)
	call_c   Dyam_Seed_End()
	move_ret seed[28]
	c_ret

;; TERM 71: '*GUARD*'(parse_options(_D, _C)) :> '$$HOLE$$'
c_code local build_ref_71
	ret_reg &ref[71]
	call_c   build_ref_70()
	call_c   Dyam_Create_Binary(I(9),&ref[70],I(7))
	move_ret ref[71]
	c_ret

;; TERM 70: '*GUARD*'(parse_options(_D, _C))
c_code local build_ref_70
	ret_reg &ref[70]
	call_c   build_ref_40()
	call_c   build_ref_69()
	call_c   Dyam_Create_Unary(&ref[40],&ref[69])
	move_ret ref[70]
	c_ret

;; TERM 69: parse_options(_D, _C)
c_code local build_ref_69
	ret_reg &ref[69]
	call_c   build_ref_310()
	call_c   Dyam_Create_Binary(&ref[310],V(3),V(2))
	move_ret ref[69]
	c_ret

long local pool_fun18[4]=[3,build_ref_64,build_seed_27,build_seed_28]

pl_code local fun18
	call_c   Dyam_Pool(pool_fun18)
	call_c   Dyam_Unify_Item(&ref[64])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun17)
	pl_call  fun15(&seed[27],1)
	pl_call  fun15(&seed[28],1)
	pl_jump  fun16()

;; TERM 65: '*GUARD*'(parse_options(_B, _C)) :> []
c_code local build_ref_65
	ret_reg &ref[65]
	call_c   build_ref_64()
	call_c   Dyam_Create_Binary(I(9),&ref[64],I(0))
	move_ret ref[65]
	c_ret

c_code local build_seed_5
	ret_reg &seed[5]
	call_c   build_ref_17()
	call_c   build_ref_81()
	call_c   Dyam_Seed_Start(&ref[17],&ref[81],I(0),fun30,1)
	call_c   build_ref_82()
	call_c   Dyam_Seed_Add_Comp(&ref[82],fun62,0)
	call_c   Dyam_Seed_End()
	move_ret seed[5]
	c_ret

;; TERM 82: '*CITEM*'(compile, _A)
c_code local build_ref_82
	ret_reg &ref[82]
	call_c   build_ref_14()
	call_c   build_ref_5()
	call_c   Dyam_Create_Binary(&ref[14],&ref[5],V(0))
	move_ret ref[82]
	c_ret

;; TERM 5: compile
c_code local build_ref_5
	ret_reg &ref[5]
	call_c   Dyam_Create_Atom("compile")
	move_ret ref[5]
	c_ret

c_code local build_seed_51
	ret_reg &seed[51]
	call_c   build_ref_94()
	call_c   build_ref_161()
	call_c   Dyam_Seed_Start(&ref[94],&ref[161],I(0),fun0,1)
	call_c   build_ref_164()
	call_c   Dyam_Seed_Add_Comp(&ref[164],&ref[161],0)
	call_c   Dyam_Seed_End()
	move_ret seed[51]
	c_ret

;; TERM 164: '*PROLOG-FIRST*'(compiler_init) :> '$$HOLE$$'
c_code local build_ref_164
	ret_reg &ref[164]
	call_c   build_ref_163()
	call_c   Dyam_Create_Binary(I(9),&ref[163],I(7))
	move_ret ref[164]
	c_ret

;; TERM 163: '*PROLOG-FIRST*'(compiler_init)
c_code local build_ref_163
	ret_reg &ref[163]
	call_c   build_ref_96()
	call_c   build_ref_162()
	call_c   Dyam_Create_Unary(&ref[96],&ref[162])
	move_ret ref[163]
	c_ret

;; TERM 162: compiler_init
c_code local build_ref_162
	ret_reg &ref[162]
	call_c   Dyam_Create_Atom("compiler_init")
	move_ret ref[162]
	c_ret

;; TERM 96: '*PROLOG-FIRST*'
c_code local build_ref_96
	ret_reg &ref[96]
	call_c   Dyam_Create_Atom("*PROLOG-FIRST*")
	move_ret ref[96]
	c_ret

;; TERM 161: '*PROLOG-ITEM*'{top=> compiler_init, cont=> '$CLOSURE'('$fun'(61, 0, 1160736620), '$TUPPLE'(35105722351416))}
c_code local build_ref_161
	ret_reg &ref[161]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_125()
	call_c   Dyam_Closure_Aux(fun61,&ref[125])
	move_ret R(0)
	call_c   build_ref_312()
	call_c   build_ref_162()
	call_c   Dyam_Create_Binary(&ref[312],&ref[162],R(0))
	move_ret ref[161]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_29
	ret_reg &seed[29]
	call_c   build_ref_84()
	call_c   build_ref_83()
	call_c   Dyam_Seed_Start(&ref[84],&ref[83],I(0),fun58,0)
	call_c   Dyam_Seed_End()
	move_ret seed[29]
	c_ret

c_code local build_seed_30
	ret_reg &seed[30]
	call_c   build_ref_40()
	call_c   build_ref_86()
	call_c   Dyam_Seed_Start(&ref[40],&ref[86],I(0),fun0,1)
	call_c   build_ref_87()
	call_c   Dyam_Seed_Add_Comp(&ref[87],&ref[86],0)
	call_c   Dyam_Seed_End()
	move_ret seed[30]
	c_ret

;; TERM 87: '*GUARD*'(parse_options(_C, [])) :> '$$HOLE$$'
c_code local build_ref_87
	ret_reg &ref[87]
	call_c   build_ref_86()
	call_c   Dyam_Create_Binary(I(9),&ref[86],I(7))
	move_ret ref[87]
	c_ret

;; TERM 86: '*GUARD*'(parse_options(_C, []))
c_code local build_ref_86
	ret_reg &ref[86]
	call_c   build_ref_40()
	call_c   build_ref_85()
	call_c   Dyam_Create_Unary(&ref[40],&ref[85])
	move_ret ref[86]
	c_ret

;; TERM 85: parse_options(_C, [])
c_code local build_ref_85
	ret_reg &ref[85]
	call_c   build_ref_310()
	call_c   Dyam_Create_Binary(&ref[310],V(2),I(0))
	move_ret ref[85]
	c_ret

c_code local build_seed_32
	ret_reg &seed[32]
	call_c   build_ref_4()
	call_c   build_ref_92()
	call_c   Dyam_Seed_Start(&ref[4],&ref[92],&ref[92],fun0,1)
	call_c   build_ref_93()
	call_c   Dyam_Seed_Add_Comp(&ref[93],&ref[92],0)
	call_c   Dyam_Seed_End()
	move_ret seed[32]
	c_ret

;; TERM 93: '*RITEM*'(_A, voidret) :> '$$HOLE$$'
c_code local build_ref_93
	ret_reg &ref[93]
	call_c   build_ref_92()
	call_c   Dyam_Create_Binary(I(9),&ref[92],I(7))
	move_ret ref[93]
	c_ret

;; TERM 92: '*RITEM*'(_A, voidret)
c_code local build_ref_92
	ret_reg &ref[92]
	call_c   build_ref_4()
	call_c   build_ref_6()
	call_c   Dyam_Create_Binary(&ref[4],V(0),&ref[6])
	move_ret ref[92]
	c_ret

;; TERM 6: voidret
c_code local build_ref_6
	ret_reg &ref[6]
	call_c   Dyam_Create_Atom("voidret")
	move_ret ref[6]
	c_ret

;; TERM 4: '*RITEM*'
c_code local build_ref_4
	ret_reg &ref[4]
	call_c   Dyam_Create_Atom("*RITEM*")
	move_ret ref[4]
	c_ret

pl_code local fun14
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Choice(fun13)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Subsume(R(0))
	fail_ret
	call_c   Dyam_Cut()
	pl_ret

long local pool_fun24[2]=[1,build_seed_32]

pl_code local fun43
	call_c   Dyam_Remove_Choice()
fun24:
	call_c   Dyam_Deallocate()
	pl_jump  fun14(&seed[32],2)


;; TERM 89: main_file(_D, _E)
c_code local build_ref_89
	ret_reg &ref[89]
	call_c   build_ref_313()
	call_c   Dyam_Create_Binary(&ref[313],V(3),V(4))
	move_ret ref[89]
	c_ret

;; TERM 313: main_file
c_code local build_ref_313
	ret_reg &ref[313]
	call_c   Dyam_Create_Atom("main_file")
	move_ret ref[313]
	c_ret

c_code local build_seed_38
	ret_reg &seed[38]
	call_c   build_ref_84()
	call_c   build_ref_91()
	call_c   Dyam_Seed_Start(&ref[84],&ref[91],I(0),fun35,0)
	call_c   Dyam_Seed_End()
	move_ret seed[38]
	c_ret

;; TERM 126: '$CLOSURE'('$fun'(25, 0, 1160417012), '$TUPPLE'(35105722351416))
c_code local build_ref_126
	ret_reg &ref[126]
	call_c   build_ref_125()
	call_c   Dyam_Closure_Aux(fun25,&ref[125])
	move_ret ref[126]
	c_ret

pl_code local fun25
	call_c   Dyam_Pool(pool_fun24)
	call_c   Dyam_Allocate(0)
	pl_jump  fun24()

;; TERM 125: '$TUPPLE'(35105722351416)
c_code local build_ref_125
	ret_reg &ref[125]
	call_c   Dyam_Create_Simple_Tupple(0,268435456)
	move_ret ref[125]
	c_ret

long local pool_fun33[2]=[1,build_ref_126]

pl_code local fun34
	call_c   Dyam_Remove_Choice()
fun33:
	move     &ref[126], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Deallocate(2)
fun32:
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(2)
	pl_call  fun14(&seed[39],1)
	pl_call  fun31(&seed[40],2,V(2))
	call_c   Dyam_Deallocate()
	pl_ret



;; TERM 116: stop_compilation_at(emit)
c_code local build_ref_116
	ret_reg &ref[116]
	call_c   build_ref_314()
	call_c   build_ref_8()
	call_c   Dyam_Create_Unary(&ref[314],&ref[8])
	move_ret ref[116]
	c_ret

;; TERM 8: emit
c_code local build_ref_8
	ret_reg &ref[8]
	call_c   Dyam_Create_Atom("emit")
	move_ret ref[8]
	c_ret

;; TERM 314: stop_compilation_at
c_code local build_ref_314
	ret_reg &ref[314]
	call_c   Dyam_Create_Atom("stop_compilation_at")
	move_ret ref[314]
	c_ret

long local pool_fun35[3]=[65537,build_ref_116,pool_fun33]

pl_code local fun35
	call_c   Dyam_Pool(pool_fun35)
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun34)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[116])
	call_c   Dyam_Cut()
	pl_fail

;; TERM 91: cont('$TUPPLE'(35105722351404))
c_code local build_ref_91
	ret_reg &ref[91]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,318767104)
	move_ret R(0)
	call_c   build_ref_315()
	call_c   Dyam_Create_Unary(&ref[315],R(0))
	move_ret ref[91]
	call_c   Dyam_Set_Not_Copyable(&ref[91])
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 315: cont
c_code local build_ref_315
	ret_reg &ref[315]
	call_c   Dyam_Create_Atom("cont")
	move_ret ref[315]
	c_ret

;; TERM 84: '*WAIT_CONT*'
c_code local build_ref_84
	ret_reg &ref[84]
	call_c   Dyam_Create_Atom("*WAIT_CONT*")
	move_ret ref[84]
	c_ret

pl_code local fun27
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Object(R(0))
	call_c   Dyam_Schedule_Last()
	pl_ret

c_code local build_seed_44
	ret_reg &seed[44]
	call_c   build_ref_40()
	call_c   build_ref_142()
	call_c   Dyam_Seed_Start(&ref[40],&ref[142],I(0),fun0,1)
	call_c   build_ref_143()
	call_c   Dyam_Seed_Add_Comp(&ref[143],&ref[142],0)
	call_c   Dyam_Seed_End()
	move_ret seed[44]
	c_ret

;; TERM 143: '*GUARD*'(get_extra_clause(_F)) :> '$$HOLE$$'
c_code local build_ref_143
	ret_reg &ref[143]
	call_c   build_ref_142()
	call_c   Dyam_Create_Binary(I(9),&ref[142],I(7))
	move_ret ref[143]
	c_ret

;; TERM 142: '*GUARD*'(get_extra_clause(_F))
c_code local build_ref_142
	ret_reg &ref[142]
	call_c   build_ref_40()
	call_c   build_ref_141()
	call_c   Dyam_Create_Unary(&ref[40],&ref[141])
	move_ret ref[142]
	c_ret

;; TERM 141: get_extra_clause(_F)
c_code local build_ref_141
	ret_reg &ref[141]
	call_c   build_ref_306()
	call_c   Dyam_Create_Unary(&ref[306],V(5))
	move_ret ref[141]
	c_ret

c_code local build_seed_36
	ret_reg &seed[36]
	call_c   build_ref_40()
	call_c   build_ref_110()
	call_c   Dyam_Seed_Start(&ref[40],&ref[110],I(0),fun0,1)
	call_c   build_ref_111()
	call_c   Dyam_Seed_Add_Comp(&ref[111],&ref[110],0)
	call_c   Dyam_Seed_End()
	move_ret seed[36]
	c_ret

;; TERM 111: '*GUARD*'(pgm_to_lpda(_F, _G)) :> '$$HOLE$$'
c_code local build_ref_111
	ret_reg &ref[111]
	call_c   build_ref_110()
	call_c   Dyam_Create_Binary(I(9),&ref[110],I(7))
	move_ret ref[111]
	c_ret

;; TERM 110: '*GUARD*'(pgm_to_lpda(_F, _G))
c_code local build_ref_110
	ret_reg &ref[110]
	call_c   build_ref_40()
	call_c   build_ref_109()
	call_c   Dyam_Create_Unary(&ref[40],&ref[109])
	move_ret ref[110]
	c_ret

;; TERM 109: pgm_to_lpda(_F, _G)
c_code local build_ref_109
	ret_reg &ref[109]
	call_c   build_ref_308()
	call_c   Dyam_Create_Binary(&ref[308],V(5),V(6))
	move_ret ref[109]
	c_ret

c_code local build_seed_43
	ret_reg &seed[43]
	call_c   build_ref_94()
	call_c   build_ref_137()
	call_c   Dyam_Seed_Start(&ref[94],&ref[137],I(0),fun0,1)
	call_c   build_ref_140()
	call_c   Dyam_Seed_Add_Comp(&ref[140],&ref[137],0)
	call_c   Dyam_Seed_End()
	move_ret seed[43]
	c_ret

;; TERM 140: '*PROLOG-FIRST*'(init_install(_G)) :> '$$HOLE$$'
c_code local build_ref_140
	ret_reg &ref[140]
	call_c   build_ref_139()
	call_c   Dyam_Create_Binary(I(9),&ref[139],I(7))
	move_ret ref[140]
	c_ret

;; TERM 139: '*PROLOG-FIRST*'(init_install(_G))
c_code local build_ref_139
	ret_reg &ref[139]
	call_c   build_ref_96()
	call_c   build_ref_138()
	call_c   Dyam_Create_Unary(&ref[96],&ref[138])
	move_ret ref[139]
	c_ret

;; TERM 138: init_install(_G)
c_code local build_ref_138
	ret_reg &ref[138]
	call_c   build_ref_316()
	call_c   Dyam_Create_Unary(&ref[316],V(6))
	move_ret ref[138]
	c_ret

;; TERM 316: init_install
c_code local build_ref_316
	ret_reg &ref[316]
	call_c   Dyam_Create_Atom("init_install")
	move_ret ref[316]
	c_ret

;; TERM 137: '*PROLOG-ITEM*'{top=> init_install(_G), cont=> '$CLOSURE'('$fun'(28, 0, 1160442768), '$TUPPLE'(35105722351416))}
c_code local build_ref_137
	ret_reg &ref[137]
	call_c   build_ref_312()
	call_c   build_ref_138()
	call_c   build_ref_136()
	call_c   Dyam_Create_Binary(&ref[312],&ref[138],&ref[136])
	move_ret ref[137]
	c_ret

;; TERM 136: '$CLOSURE'('$fun'(28, 0, 1160442768), '$TUPPLE'(35105722351416))
c_code local build_ref_136
	ret_reg &ref[136]
	call_c   build_ref_125()
	call_c   Dyam_Closure_Aux(fun28,&ref[125])
	move_ret ref[136]
	c_ret

pl_code local fun28
	pl_fail

;; TERM 312: '*PROLOG-ITEM*'!'$ft'
c_code local build_ref_312
	ret_reg &ref[312]
	call_c   build_ref_94()
	call_c   Dyam_Create_Atom_Module("$ft",&ref[94])
	move_ret ref[312]
	c_ret

;; TERM 94: '*PROLOG-ITEM*'
c_code local build_ref_94
	ret_reg &ref[94]
	call_c   Dyam_Create_Atom("*PROLOG-ITEM*")
	move_ret ref[94]
	c_ret

long local pool_fun38[2]=[1,build_seed_43]

pl_code local fun38
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Deallocate()
	pl_jump  fun15(&seed[43],12)

;; TERM 127: '*STD-PROLOG-FIRST*'(_H, _I, _J)
c_code local build_ref_127
	ret_reg &ref[127]
	call_c   build_ref_317()
	call_c   Dyam_Term_Start(&ref[317],3)
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret ref[127]
	c_ret

;; TERM 317: '*STD-PROLOG-FIRST*'
c_code local build_ref_317
	ret_reg &ref[317]
	call_c   Dyam_Create_Atom("*STD-PROLOG-FIRST*")
	move_ret ref[317]
	c_ret

long local pool_fun39[4]=[65538,build_ref_127,build_ref_136,pool_fun38]

long local pool_fun40[4]=[65538,build_seed_44,build_seed_36,pool_fun39]

pl_code local fun40
	call_c   Dyam_Remove_Choice()
	pl_call  fun15(&seed[44],1)
	pl_call  fun15(&seed[36],1)
fun39:
	call_c   Dyam_Choice(fun38)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(6),&ref[127])
	fail_ret
	call_c   Dyam_Cut()
	move     &ref[136], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Load(4,V(6))
	call_c   Dyam_Reg_Deallocate(3)
fun37:
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Reg_Bind(2,V(3))
	call_c   Dyam_Reg_Bind(4,V(2))
	pl_call  fun14(&seed[41],1)
	pl_call  fun31(&seed[42],2,V(3))
	call_c   Dyam_Deallocate()
	pl_ret



c_code local build_seed_34
	ret_reg &seed[34]
	call_c   build_ref_40()
	call_c   build_ref_101()
	call_c   Dyam_Seed_Start(&ref[40],&ref[101],I(0),fun0,1)
	call_c   build_ref_102()
	call_c   Dyam_Seed_Add_Comp(&ref[102],&ref[101],0)
	call_c   Dyam_Seed_End()
	move_ret seed[34]
	c_ret

;; TERM 102: '*GUARD*'(get_clause(_F)) :> '$$HOLE$$'
c_code local build_ref_102
	ret_reg &ref[102]
	call_c   build_ref_101()
	call_c   Dyam_Create_Binary(I(9),&ref[101],I(7))
	move_ret ref[102]
	c_ret

;; TERM 101: '*GUARD*'(get_clause(_F))
c_code local build_ref_101
	ret_reg &ref[101]
	call_c   build_ref_40()
	call_c   build_ref_100()
	call_c   Dyam_Create_Unary(&ref[40],&ref[100])
	move_ret ref[101]
	c_ret

;; TERM 100: get_clause(_F)
c_code local build_ref_100
	ret_reg &ref[100]
	call_c   build_ref_318()
	call_c   Dyam_Create_Unary(&ref[318],V(5))
	move_ret ref[100]
	c_ret

;; TERM 318: get_clause
c_code local build_ref_318
	ret_reg &ref[318]
	call_c   Dyam_Create_Atom("get_clause")
	move_ret ref[318]
	c_ret

long local pool_fun41[6]=[131075,build_seed_38,build_seed_34,build_seed_36,pool_fun40,pool_fun39]

pl_code local fun41
	call_c   Dyam_Remove_Choice()
	pl_call  fun27(&seed[38],12)
	call_c   Dyam_Choice(fun40)
	pl_call  fun15(&seed[34],1)
	pl_call  fun15(&seed[36],1)
	pl_jump  fun39()

;; TERM 107: stop_compilation_at(pda)
c_code local build_ref_107
	ret_reg &ref[107]
	call_c   build_ref_314()
	call_c   build_ref_319()
	call_c   Dyam_Create_Unary(&ref[314],&ref[319])
	move_ret ref[107]
	c_ret

;; TERM 319: pda
c_code local build_ref_319
	ret_reg &ref[319]
	call_c   Dyam_Create_Atom("pda")
	move_ret ref[319]
	c_ret

;; TERM 108: 'Stop at PDA\n'
c_code local build_ref_108
	ret_reg &ref[108]
	call_c   Dyam_Create_Atom("Stop at PDA\n")
	move_ret ref[108]
	c_ret

c_code local build_seed_31
	ret_reg &seed[31]
	call_c   build_ref_84()
	call_c   build_ref_91()
	call_c   Dyam_Seed_Start(&ref[84],&ref[91],I(0),fun26,0)
	call_c   Dyam_Seed_End()
	move_ret seed[31]
	c_ret

c_code local build_seed_33
	ret_reg &seed[33]
	call_c   build_ref_94()
	call_c   build_ref_95()
	call_c   Dyam_Seed_Start(&ref[94],&ref[95],I(0),fun0,1)
	call_c   build_ref_99()
	call_c   Dyam_Seed_Add_Comp(&ref[99],&ref[95],0)
	call_c   Dyam_Seed_End()
	move_ret seed[33]
	c_ret

;; TERM 99: '*PROLOG-FIRST*'(emit_at_pda) :> '$$HOLE$$'
c_code local build_ref_99
	ret_reg &ref[99]
	call_c   build_ref_98()
	call_c   Dyam_Create_Binary(I(9),&ref[98],I(7))
	move_ret ref[99]
	c_ret

;; TERM 98: '*PROLOG-FIRST*'(emit_at_pda)
c_code local build_ref_98
	ret_reg &ref[98]
	call_c   build_ref_96()
	call_c   build_ref_97()
	call_c   Dyam_Create_Unary(&ref[96],&ref[97])
	move_ret ref[98]
	c_ret

;; TERM 97: emit_at_pda
c_code local build_ref_97
	ret_reg &ref[97]
	call_c   Dyam_Create_Atom("emit_at_pda")
	move_ret ref[97]
	c_ret

;; TERM 95: '*PROLOG-ITEM*'{top=> emit_at_pda, cont=> '$CLOSURE'('$fun'(25, 0, 1160417012), '$TUPPLE'(35105722351416))}
c_code local build_ref_95
	ret_reg &ref[95]
	call_c   build_ref_312()
	call_c   build_ref_97()
	call_c   build_ref_126()
	call_c   Dyam_Create_Binary(&ref[312],&ref[97],&ref[126])
	move_ret ref[95]
	c_ret

long local pool_fun26[2]=[1,build_seed_33]

pl_code local fun26
	call_c   Dyam_Pool(pool_fun26)
	pl_jump  fun15(&seed[33],12)

c_code local build_seed_37
	ret_reg &seed[37]
	call_c   build_ref_94()
	call_c   build_ref_112()
	call_c   Dyam_Seed_Start(&ref[94],&ref[112],I(0),fun0,1)
	call_c   build_ref_115()
	call_c   Dyam_Seed_Add_Comp(&ref[115],&ref[112],0)
	call_c   Dyam_Seed_End()
	move_ret seed[37]
	c_ret

;; TERM 115: '*PROLOG-FIRST*'(analyze_at_pda(_G)) :> '$$HOLE$$'
c_code local build_ref_115
	ret_reg &ref[115]
	call_c   build_ref_114()
	call_c   Dyam_Create_Binary(I(9),&ref[114],I(7))
	move_ret ref[115]
	c_ret

;; TERM 114: '*PROLOG-FIRST*'(analyze_at_pda(_G))
c_code local build_ref_114
	ret_reg &ref[114]
	call_c   build_ref_96()
	call_c   build_ref_113()
	call_c   Dyam_Create_Unary(&ref[96],&ref[113])
	move_ret ref[114]
	c_ret

;; TERM 113: analyze_at_pda(_G)
c_code local build_ref_113
	ret_reg &ref[113]
	call_c   build_ref_320()
	call_c   Dyam_Create_Unary(&ref[320],V(6))
	move_ret ref[113]
	c_ret

;; TERM 320: analyze_at_pda
c_code local build_ref_320
	ret_reg &ref[320]
	call_c   Dyam_Create_Atom("analyze_at_pda")
	move_ret ref[320]
	c_ret

;; TERM 112: '*PROLOG-ITEM*'{top=> analyze_at_pda(_G), cont=> '$CLOSURE'('$fun'(28, 0, 1160442768), '$TUPPLE'(35105722351416))}
c_code local build_ref_112
	ret_reg &ref[112]
	call_c   build_ref_312()
	call_c   build_ref_113()
	call_c   build_ref_136()
	call_c   Dyam_Create_Binary(&ref[312],&ref[113],&ref[136])
	move_ret ref[112]
	c_ret

long local pool_fun42[8]=[65542,build_ref_107,build_ref_108,build_seed_31,build_seed_34,build_seed_36,build_seed_37,pool_fun41]

pl_code local fun42
	call_c   Dyam_Update_Choice(fun41)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[107])
	call_c   Dyam_Cut()
	move     &ref[108], R(0)
	move     0, R(1)
	move     I(0), R(2)
	move     0, R(3)
	pl_call  pred_format_2()
	pl_call  fun27(&seed[31],12)
	pl_call  fun15(&seed[34],1)
	pl_call  fun15(&seed[36],1)
	call_c   Dyam_Deallocate()
	pl_jump  fun15(&seed[37],12)

;; TERM 90: stop_compilation_at(pgm)
c_code local build_ref_90
	ret_reg &ref[90]
	call_c   build_ref_314()
	call_c   build_ref_321()
	call_c   Dyam_Create_Unary(&ref[314],&ref[321])
	move_ret ref[90]
	c_ret

;; TERM 321: pgm
c_code local build_ref_321
	ret_reg &ref[321]
	call_c   Dyam_Create_Atom("pgm")
	move_ret ref[321]
	c_ret

c_code local build_seed_35
	ret_reg &seed[35]
	call_c   build_ref_94()
	call_c   build_ref_103()
	call_c   Dyam_Seed_Start(&ref[94],&ref[103],I(0),fun0,1)
	call_c   build_ref_106()
	call_c   Dyam_Seed_Add_Comp(&ref[106],&ref[103],0)
	call_c   Dyam_Seed_End()
	move_ret seed[35]
	c_ret

;; TERM 106: '*PROLOG-FIRST*'(analyze_at_clause(_F)) :> '$$HOLE$$'
c_code local build_ref_106
	ret_reg &ref[106]
	call_c   build_ref_105()
	call_c   Dyam_Create_Binary(I(9),&ref[105],I(7))
	move_ret ref[106]
	c_ret

;; TERM 105: '*PROLOG-FIRST*'(analyze_at_clause(_F))
c_code local build_ref_105
	ret_reg &ref[105]
	call_c   build_ref_96()
	call_c   build_ref_104()
	call_c   Dyam_Create_Unary(&ref[96],&ref[104])
	move_ret ref[105]
	c_ret

;; TERM 104: analyze_at_clause(_F)
c_code local build_ref_104
	ret_reg &ref[104]
	call_c   build_ref_322()
	call_c   Dyam_Create_Unary(&ref[322],V(5))
	move_ret ref[104]
	c_ret

;; TERM 322: analyze_at_clause
c_code local build_ref_322
	ret_reg &ref[322]
	call_c   Dyam_Create_Atom("analyze_at_clause")
	move_ret ref[322]
	c_ret

;; TERM 103: '*PROLOG-ITEM*'{top=> analyze_at_clause(_F), cont=> '$CLOSURE'('$fun'(28, 0, 1160442768), '$TUPPLE'(35105722351416))}
c_code local build_ref_103
	ret_reg &ref[103]
	call_c   build_ref_312()
	call_c   build_ref_104()
	call_c   build_ref_136()
	call_c   Dyam_Create_Binary(&ref[312],&ref[104],&ref[136])
	move_ret ref[103]
	c_ret

long local pool_fun44[8]=[131077,build_ref_89,build_ref_90,build_seed_31,build_seed_34,build_seed_35,pool_fun24,pool_fun42]

pl_code local fun45
	call_c   Dyam_Remove_Choice()
fun44:
	call_c   Dyam_Choice(fun43)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[89])
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun42)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[90])
	call_c   Dyam_Cut()
	pl_call  fun27(&seed[31],12)
	pl_call  fun15(&seed[34],1)
	call_c   Dyam_Deallocate()
	pl_jump  fun15(&seed[35],12)


;; TERM 54: toplevel_mode
c_code local build_ref_54
	ret_reg &ref[54]
	call_c   Dyam_Create_Atom("toplevel_mode")
	move_ret ref[54]
	c_ret

long local pool_fun46[4]=[131073,build_ref_54,pool_fun44,pool_fun44]

pl_code local fun47
	call_c   Dyam_Remove_Choice()
fun46:
	call_c   Dyam_Choice(fun45)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[54])
	call_c   Dyam_Cut()
	pl_call  pred_toplevel_loop_0()
	call_c   DYAM_Exit_1(N(0))
	fail_ret
	pl_jump  fun44()


;; TERM 88: stop_compilation_at(load)
c_code local build_ref_88
	ret_reg &ref[88]
	call_c   build_ref_314()
	call_c   build_ref_323()
	call_c   Dyam_Create_Unary(&ref[314],&ref[323])
	move_ret ref[88]
	c_ret

;; TERM 323: load
c_code local build_ref_323
	ret_reg &ref[323]
	call_c   Dyam_Create_Atom("load")
	move_ret ref[323]
	c_ret

long local pool_fun58[4]=[65538,build_seed_30,build_ref_88,pool_fun46]

pl_code local fun58
	call_c   Dyam_Pool(pool_fun58)
	call_c   DYAM_Os_Argv_1(V(2))
	fail_ret
	call_c   Dyam_Allocate(0)
	pl_call  fun15(&seed[30],1)
	call_c   Dyam_Choice(fun47)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[88])
	call_c   Dyam_Cut()
	pl_fail

;; TERM 83: cont('$TUPPLE'(35105722351340))
c_code local build_ref_83
	ret_reg &ref[83]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,402128896)
	move_ret R(0)
	call_c   build_ref_315()
	call_c   Dyam_Create_Unary(&ref[315],R(0))
	move_ret ref[83]
	call_c   Dyam_Set_Not_Copyable(&ref[83])
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun61[3]=[2,build_seed_29,build_ref_136]

pl_code local fun61
	call_c   Dyam_Pool(pool_fun61)
	call_c   Dyam_Allocate(0)
	pl_call  fun27(&seed[29],12)
	move     &ref[136], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	move     0, R(4)
	call_c   Dyam_Reg_Deallocate(3)
fun60:
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Reg_Bind(2,V(3))
	call_c   Dyam_Reg_Bind(4,V(2))
	pl_call  fun14(&seed[49],1)
	pl_call  fun31(&seed[50],2,V(3))
	call_c   Dyam_Deallocate()
	pl_ret


long local pool_fun62[3]=[2,build_ref_82,build_seed_51]

pl_code local fun62
	call_c   Dyam_Pool(pool_fun62)
	call_c   Dyam_Unify_Item(&ref[82])
	fail_ret
	pl_jump  fun15(&seed[51],12)

pl_code local fun30
	pl_jump  Apply(0,0)

;; TERM 81: '*FIRST*'(compile) :> []
c_code local build_ref_81
	ret_reg &ref[81]
	call_c   build_ref_80()
	call_c   Dyam_Create_Binary(I(9),&ref[80],I(0))
	move_ret ref[81]
	c_ret

;; TERM 80: '*FIRST*'(compile)
c_code local build_ref_80
	ret_reg &ref[80]
	call_c   build_ref_17()
	call_c   build_ref_5()
	call_c   Dyam_Create_Unary(&ref[17],&ref[5])
	move_ret ref[80]
	c_ret

c_code local build_seed_6
	ret_reg &seed[6]
	call_c   build_ref_17()
	call_c   build_ref_144()
	call_c   Dyam_Seed_Start(&ref[17],&ref[144],I(0),fun30,1)
	call_c   build_ref_145()
	call_c   Dyam_Seed_Add_Comp(&ref[145],fun57,0)
	call_c   Dyam_Seed_End()
	move_ret seed[6]
	c_ret

;; TERM 145: '*CITEM*'(start, _A)
c_code local build_ref_145
	ret_reg &ref[145]
	call_c   build_ref_14()
	call_c   build_ref_15()
	call_c   Dyam_Create_Binary(&ref[14],&ref[15],V(0))
	move_ret ref[145]
	c_ret

c_code local build_seed_45
	ret_reg &seed[45]
	call_c   build_ref_84()
	call_c   build_ref_146()
	call_c   Dyam_Seed_Start(&ref[84],&ref[146],I(0),fun54,0)
	call_c   Dyam_Seed_End()
	move_ret seed[45]
	c_ret

;; TERM 149: 'Compilation failed'
c_code local build_ref_149
	ret_reg &ref[149]
	call_c   Dyam_Create_Atom("Compilation failed")
	move_ret ref[149]
	c_ret

c_code local build_seed_46
	ret_reg &seed[46]
	call_c   build_ref_147()
	call_c   build_ref_148()
	call_c   Dyam_Seed_Start(&ref[147],&ref[148],I(0),fun1,0)
	call_c   Dyam_Seed_End()
	move_ret seed[46]
	c_ret

;; TERM 148: '*ANSWER*'{answer=> []}
c_code local build_ref_148
	ret_reg &ref[148]
	call_c   build_ref_324()
	call_c   Dyam_Create_Unary(&ref[324],I(0))
	move_ret ref[148]
	c_ret

;; TERM 324: '*ANSWER*'!'$ft'
c_code local build_ref_324
	ret_reg &ref[324]
	call_c   build_ref_147()
	call_c   Dyam_Create_Atom_Module("$ft",&ref[147])
	move_ret ref[324]
	c_ret

;; TERM 147: '*ANSWER*'
c_code local build_ref_147
	ret_reg &ref[147]
	call_c   Dyam_Create_Atom("*ANSWER*")
	move_ret ref[147]
	c_ret

pl_code local fun48
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Object(R(0))
	call_c   Dyam_Tabule()
	call_c   Dyam_Answer()
	pl_ret

long local pool_fun49[2]=[1,build_seed_46]

long local pool_fun52[3]=[65537,build_ref_149,pool_fun49]

pl_code local fun52
	call_c   Dyam_Remove_Choice()
	move     &ref[149], R(0)
	move     0, R(1)
	move     I(0), R(2)
	move     0, R(3)
	pl_call  pred_error_2()
fun49:
	call_c   Dyam_Deallocate()
	pl_jump  fun48(&seed[46],2)


pl_code local fun51
	call_c   Dyam_Remove_Choice()
	pl_fail

long local pool_fun54[4]=[131073,build_ref_5,pool_fun52,pool_fun49]

pl_code local fun54
	call_c   Dyam_Pool(pool_fun54)
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun52)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Choice(fun51)
	call_c   Dyam_Set_Cut()
	pl_call  Callret_2(V(1),&ref[5])
	call_c   Dyam_Cut()
fun50:
	pl_call  Object_1(V(1))
	call_c   Dyam_Cut()
	call_c   DYAM_Exit_1(N(0))
	fail_ret
	pl_jump  fun49()


;; TERM 146: cont('$TUPPLE'(35105722351804))
c_code local build_ref_146
	ret_reg &ref[146]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,402653184)
	move_ret R(0)
	call_c   build_ref_315()
	call_c   Dyam_Create_Unary(&ref[315],R(0))
	move_ret ref[146]
	call_c   Dyam_Set_Not_Copyable(&ref[146])
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun57[4]=[3,build_ref_145,build_seed_45,build_ref_136]

pl_code local fun57
	call_c   Dyam_Pool(pool_fun57)
	call_c   Dyam_Unify_Item(&ref[145])
	fail_ret
	call_c   Dyam_Allocate(0)
	pl_call  fun27(&seed[45],12)
	move     &ref[136], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Deallocate(2)
fun56:
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(2)
	pl_call  fun14(&seed[47],1)
	pl_call  fun31(&seed[48],2,V(2))
	call_c   Dyam_Deallocate()
	pl_ret


;; TERM 144: '*FIRST*'(start) :> []
c_code local build_ref_144
	ret_reg &ref[144]
	call_c   build_ref_18()
	call_c   Dyam_Create_Binary(I(9),&ref[18],I(0))
	move_ret ref[144]
	c_ret

c_code local build_seed_4
	ret_reg &seed[4]
	call_c   build_ref_165()
	call_c   build_ref_166()
	call_c   Dyam_Seed_Start(&ref[165],&ref[166],I(0),fun1,1)
	call_c   build_ref_167()
	call_c   Dyam_Seed_Add_Comp(&ref[167],fun64,0)
	call_c   Dyam_Seed_End()
	move_ret seed[4]
	c_ret

;; TERM 167: '*PROLOG-ITEM*'{top=> compiler_init, cont=> _A}
c_code local build_ref_167
	ret_reg &ref[167]
	call_c   build_ref_312()
	call_c   build_ref_162()
	call_c   Dyam_Create_Binary(&ref[312],&ref[162],V(0))
	move_ret ref[167]
	c_ret

;; TERM 168: parse_mode(list)
c_code local build_ref_168
	ret_reg &ref[168]
	call_c   build_ref_325()
	call_c   build_ref_326()
	call_c   Dyam_Create_Unary(&ref[325],&ref[326])
	move_ret ref[168]
	c_ret

;; TERM 326: list
c_code local build_ref_326
	ret_reg &ref[326]
	call_c   Dyam_Create_Atom("list")
	move_ret ref[326]
	c_ret

;; TERM 325: parse_mode
c_code local build_ref_325
	ret_reg &ref[325]
	call_c   Dyam_Create_Atom("parse_mode")
	move_ret ref[325]
	c_ret

;; TERM 169: compiler_analyzer(none)
c_code local build_ref_169
	ret_reg &ref[169]
	call_c   build_ref_327()
	call_c   build_ref_328()
	call_c   Dyam_Create_Unary(&ref[327],&ref[328])
	move_ret ref[169]
	c_ret

;; TERM 328: none
c_code local build_ref_328
	ret_reg &ref[328]
	call_c   Dyam_Create_Atom("none")
	move_ret ref[328]
	c_ret

;; TERM 327: compiler_analyzer
c_code local build_ref_327
	ret_reg &ref[327]
	call_c   Dyam_Create_Atom("compiler_analyzer")
	move_ret ref[327]
	c_ret

;; TERM 170: dcg
c_code local build_ref_170
	ret_reg &ref[170]
	call_c   Dyam_Create_Atom("dcg")
	move_ret ref[170]
	c_ret

;; TERM 171: grammar_kind(_B)
c_code local build_ref_171
	ret_reg &ref[171]
	call_c   build_ref_329()
	call_c   Dyam_Create_Unary(&ref[329],V(1))
	move_ret ref[171]
	c_ret

;; TERM 329: grammar_kind
c_code local build_ref_329
	ret_reg &ref[329]
	call_c   Dyam_Create_Atom("grammar_kind")
	move_ret ref[329]
	c_ret

;; TERM 172: bmg_stacks([])
c_code local build_ref_172
	ret_reg &ref[172]
	call_c   build_ref_330()
	call_c   Dyam_Create_Unary(&ref[330],I(0))
	move_ret ref[172]
	c_ret

;; TERM 330: bmg_stacks
c_code local build_ref_330
	ret_reg &ref[330]
	call_c   Dyam_Create_Atom("bmg_stacks")
	move_ret ref[330]
	c_ret

c_code local build_seed_52
	ret_reg &seed[52]
	call_c   build_ref_84()
	call_c   build_ref_146()
	call_c   Dyam_Seed_Start(&ref[84],&ref[146],I(0),fun63,0)
	call_c   Dyam_Seed_End()
	move_ret seed[52]
	c_ret

;; TERM 174: [tree,auxtree,spinetree]
c_code local build_ref_174
	ret_reg &ref[174]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_333()
	call_c   Dyam_Create_List(&ref[333],I(0))
	move_ret R(0)
	call_c   build_ref_332()
	call_c   Dyam_Create_List(&ref[332],R(0))
	move_ret R(0)
	call_c   build_ref_331()
	call_c   Dyam_Create_List(&ref[331],R(0))
	move_ret ref[174]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 331: tree
c_code local build_ref_331
	ret_reg &ref[331]
	call_c   Dyam_Create_Atom("tree")
	move_ret ref[331]
	c_ret

;; TERM 332: auxtree
c_code local build_ref_332
	ret_reg &ref[332]
	call_c   Dyam_Create_Atom("auxtree")
	move_ret ref[332]
	c_ret

;; TERM 333: spinetree
c_code local build_ref_333
	ret_reg &ref[333]
	call_c   Dyam_Create_Atom("spinetree")
	move_ret ref[333]
	c_ret

;; TERM 176: [at]
c_code local build_ref_176
	ret_reg &ref[176]
	call_c   build_ref_334()
	call_c   Dyam_Create_List(&ref[334],I(0))
	move_ret ref[176]
	c_ret

;; TERM 334: at
c_code local build_ref_334
	ret_reg &ref[334]
	call_c   Dyam_Create_Atom("at")
	move_ret ref[334]
	c_ret

;; TERM 175: xfx
c_code local build_ref_175
	ret_reg &ref[175]
	call_c   Dyam_Create_Atom("xfx")
	move_ret ref[175]
	c_ret

;; TERM 178: [and]
c_code local build_ref_178
	ret_reg &ref[178]
	call_c   build_ref_335()
	call_c   Dyam_Create_List(&ref[335],I(0))
	move_ret ref[178]
	c_ret

;; TERM 335: and
c_code local build_ref_335
	ret_reg &ref[335]
	call_c   Dyam_Create_Atom("and")
	move_ret ref[335]
	c_ret

;; TERM 177: xfy
c_code local build_ref_177
	ret_reg &ref[177]
	call_c   Dyam_Create_Atom("xfy")
	move_ret ref[177]
	c_ret

;; TERM 179: [*,**,=,++,<>,<=>]
c_code local build_ref_179
	ret_reg &ref[179]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_341()
	call_c   Dyam_Create_List(&ref[341],I(0))
	move_ret R(0)
	call_c   build_ref_340()
	call_c   Dyam_Create_List(&ref[340],R(0))
	move_ret R(0)
	call_c   build_ref_339()
	call_c   Dyam_Create_List(&ref[339],R(0))
	move_ret R(0)
	call_c   build_ref_338()
	call_c   Dyam_Create_List(&ref[338],R(0))
	move_ret R(0)
	call_c   build_ref_337()
	call_c   Dyam_Create_List(&ref[337],R(0))
	move_ret R(0)
	call_c   build_ref_336()
	call_c   Dyam_Create_List(&ref[336],R(0))
	move_ret ref[179]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 336: *
c_code local build_ref_336
	ret_reg &ref[336]
	call_c   Dyam_Create_Atom("*")
	move_ret ref[336]
	c_ret

;; TERM 337: **
c_code local build_ref_337
	ret_reg &ref[337]
	call_c   Dyam_Create_Atom("**")
	move_ret ref[337]
	c_ret

;; TERM 338: =
c_code local build_ref_338
	ret_reg &ref[338]
	call_c   Dyam_Create_Atom("=")
	move_ret ref[338]
	c_ret

;; TERM 339: ++
c_code local build_ref_339
	ret_reg &ref[339]
	call_c   Dyam_Create_Atom("++")
	move_ret ref[339]
	c_ret

;; TERM 340: <>
c_code local build_ref_340
	ret_reg &ref[340]
	call_c   Dyam_Create_Atom("<>")
	move_ret ref[340]
	c_ret

;; TERM 341: <=>
c_code local build_ref_341
	ret_reg &ref[341]
	call_c   Dyam_Create_Atom("<=>")
	move_ret ref[341]
	c_ret

;; TERM 173: fx
c_code local build_ref_173
	ret_reg &ref[173]
	call_c   Dyam_Create_Atom("fx")
	move_ret ref[173]
	c_ret

long local pool_fun63[8]=[7,build_ref_174,build_ref_176,build_ref_175,build_ref_178,build_ref_177,build_ref_179,build_ref_173]

pl_code local fun63
	call_c   Dyam_Pool(pool_fun63)
	call_c   DYAM_Op_3(N(800),&ref[173],&ref[174])
	fail_ret
	call_c   DYAM_Op_3(N(710),&ref[175],&ref[176])
	fail_ret
	call_c   DYAM_Op_3(N(705),&ref[177],&ref[178])
	fail_ret
	call_c   DYAM_Op_3(N(399),&ref[173],&ref[179])
	fail_ret
	pl_jump  Follow_Cont(V(0))

;; TERM 180: dyalogrc
c_code local build_ref_180
	ret_reg &ref[180]
	call_c   Dyam_Create_Atom("dyalogrc")
	move_ret ref[180]
	c_ret

;; TERM 181: resource
c_code local build_ref_181
	ret_reg &ref[181]
	call_c   Dyam_Create_Atom("resource")
	move_ret ref[181]
	c_ret

long local pool_fun64[10]=[9,build_ref_167,build_ref_168,build_ref_169,build_ref_170,build_ref_171,build_ref_172,build_seed_52,build_ref_180,build_ref_181]

pl_code local fun64
	call_c   Dyam_Pool(pool_fun64)
	call_c   Dyam_Unify_Item(&ref[167])
	fail_ret
	call_c   DYAM_evpred_assert_1(&ref[168])
	call_c   DYAM_evpred_assert_1(&ref[169])
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load_Ptr(2,V(1))
	fail_ret
	move     &ref[170], R(4)
	move     0, R(5)
	call_c   DyALog_Mutable_Write(R(2),&R(4),0)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(1))
	fail_ret
	call_c   DYAM_evpred_assert_1(&ref[171])
	call_c   DYAM_evpred_assert_1(&ref[172])
	pl_call  fun27(&seed[52],12)
	move     &ref[180], R(0)
	move     0, R(1)
	move     &ref[181], R(2)
	move     0, R(3)
	pl_call  pred_read_files_2()
	pl_fail

;; TERM 166: '*PROLOG-FIRST*'(compiler_init) :> []
c_code local build_ref_166
	ret_reg &ref[166]
	call_c   build_ref_163()
	call_c   Dyam_Create_Binary(I(9),&ref[163],I(0))
	move_ret ref[166]
	c_ret

;; TERM 165: '*PROLOG_FIRST*'
c_code local build_ref_165
	ret_reg &ref[165]
	call_c   Dyam_Create_Atom("*PROLOG_FIRST*")
	move_ret ref[165]
	c_ret

c_code local build_seed_0
	ret_reg &seed[0]
	call_c   build_ref_39()
	call_c   build_ref_184()
	call_c   Dyam_Seed_Start(&ref[39],&ref[184],I(0),fun1,1)
	call_c   build_ref_183()
	call_c   Dyam_Seed_Add_Comp(&ref[183],fun85,0)
	call_c   Dyam_Seed_End()
	move_ret seed[0]
	c_ret

;; TERM 183: '*GUARD*'(parse_option(_H, _I))
c_code local build_ref_183
	ret_reg &ref[183]
	call_c   build_ref_40()
	call_c   build_ref_182()
	call_c   Dyam_Create_Unary(&ref[40],&ref[182])
	move_ret ref[183]
	c_ret

;; TERM 182: parse_option(_H, _I)
c_code local build_ref_182
	ret_reg &ref[182]
	call_c   build_ref_311()
	call_c   Dyam_Create_Binary(&ref[311],V(7),V(8))
	move_ret ref[182]
	c_ret

pl_code local fun66
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(7),V(8))
	pl_fail

;; TERM 220: [_B|_I]
c_code local build_ref_220
	ret_reg &ref[220]
	call_c   Dyam_Create_List(V(1),V(8))
	move_ret ref[220]
	c_ret

long local pool_fun67[2]=[1,build_ref_220]

pl_code local fun67
	call_c   Dyam_Update_Choice(fun66)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(7),&ref[220])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(1))
	pl_call  pred_main_file_1()
	call_c   Dyam_Reg_Load(0,V(1))
	pl_call  pred_read_file_alt_1()
	pl_jump  fun16()

;; TERM 219: ['-usage'|_I]
c_code local build_ref_219
	ret_reg &ref[219]
	call_c   build_ref_272()
	call_c   Dyam_Create_List(&ref[272],V(8))
	move_ret ref[219]
	c_ret

long local pool_fun68[3]=[65537,build_ref_219,pool_fun67]

pl_code local fun68
	call_c   Dyam_Update_Choice(fun67)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(7),&ref[219])
	fail_ret
	call_c   Dyam_Cut()
	pl_call  pred_usage_0()
	pl_jump  fun16()

;; TERM 218: ['-toplevel'|_I]
c_code local build_ref_218
	ret_reg &ref[218]
	call_c   build_ref_274()
	call_c   Dyam_Create_List(&ref[274],V(8))
	move_ret ref[218]
	c_ret

long local pool_fun69[4]=[65538,build_ref_218,build_ref_54,pool_fun68]

pl_code local fun69
	call_c   Dyam_Update_Choice(fun68)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(7),&ref[218])
	fail_ret
	call_c   Dyam_Cut()
	move     &ref[54], R(0)
	move     0, R(1)
	pl_call  pred_record_without_doublon_1()
	pl_jump  fun16()

;; TERM 211: ['-analyze',_G|_I]
c_code local build_ref_211
	ret_reg &ref[211]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(6),V(8))
	move_ret R(0)
	call_c   build_ref_342()
	call_c   Dyam_Create_List(&ref[342],R(0))
	move_ret ref[211]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 342: '-analyze'
c_code local build_ref_342
	ret_reg &ref[342]
	call_c   Dyam_Create_Atom("-analyze")
	move_ret ref[342]
	c_ret

;; TERM 216: 'not a valid analyzer mode: ~w'
c_code local build_ref_216
	ret_reg &ref[216]
	call_c   Dyam_Create_Atom("not a valid analyzer mode: ~w")
	move_ret ref[216]
	c_ret

;; TERM 217: [_G]
c_code local build_ref_217
	ret_reg &ref[217]
	call_c   Dyam_Create_List(V(6),I(0))
	move_ret ref[217]
	c_ret

long local pool_fun65[3]=[2,build_ref_216,build_ref_217]

pl_code local fun65
	call_c   Dyam_Remove_Choice()
	move     &ref[216], R(0)
	move     0, R(1)
	move     &ref[217], R(2)
	move     S(5), R(3)
	pl_call  pred_warning_2()
	pl_jump  fun16()

c_code local build_seed_53
	ret_reg &seed[53]
	call_c   build_ref_40()
	call_c   build_ref_213()
	call_c   Dyam_Seed_Start(&ref[40],&ref[213],I(0),fun0,1)
	call_c   build_ref_214()
	call_c   Dyam_Seed_Add_Comp(&ref[214],&ref[213],0)
	call_c   Dyam_Seed_End()
	move_ret seed[53]
	c_ret

;; TERM 214: '*GUARD*'(check_analyzer_mode(_G)) :> '$$HOLE$$'
c_code local build_ref_214
	ret_reg &ref[214]
	call_c   build_ref_213()
	call_c   Dyam_Create_Binary(I(9),&ref[213],I(7))
	move_ret ref[214]
	c_ret

;; TERM 213: '*GUARD*'(check_analyzer_mode(_G))
c_code local build_ref_213
	ret_reg &ref[213]
	call_c   build_ref_40()
	call_c   build_ref_212()
	call_c   Dyam_Create_Unary(&ref[40],&ref[212])
	move_ret ref[213]
	c_ret

;; TERM 212: check_analyzer_mode(_G)
c_code local build_ref_212
	ret_reg &ref[212]
	call_c   build_ref_343()
	call_c   Dyam_Create_Unary(&ref[343],V(6))
	move_ret ref[212]
	c_ret

;; TERM 343: check_analyzer_mode
c_code local build_ref_343
	ret_reg &ref[343]
	call_c   Dyam_Create_Atom("check_analyzer_mode")
	move_ret ref[343]
	c_ret

;; TERM 215: compiler_analyzer(_G)
c_code local build_ref_215
	ret_reg &ref[215]
	call_c   build_ref_327()
	call_c   Dyam_Create_Unary(&ref[327],V(6))
	move_ret ref[215]
	c_ret

long local pool_fun70[7]=[131076,build_ref_211,build_seed_53,build_ref_169,build_ref_215,pool_fun69,pool_fun65]

pl_code local fun70
	call_c   Dyam_Update_Choice(fun69)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(7),&ref[211])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun65)
	call_c   Dyam_Set_Cut()
	pl_call  fun15(&seed[53],1)
	call_c   Dyam_Cut()
	call_c   DYAM_evpred_retract(&ref[169])
	fail_ret
	move     &ref[215], R(0)
	move     S(5), R(1)
	pl_call  pred_record_without_doublon_1()
	pl_jump  fun16()

;; TERM 209: ['-autoload'|_I]
c_code local build_ref_209
	ret_reg &ref[209]
	call_c   build_ref_278()
	call_c   Dyam_Create_List(&ref[278],V(8))
	move_ret ref[209]
	c_ret

;; TERM 210: autoload
c_code local build_ref_210
	ret_reg &ref[210]
	call_c   Dyam_Create_Atom("autoload")
	move_ret ref[210]
	c_ret

long local pool_fun71[4]=[65538,build_ref_209,build_ref_210,pool_fun70]

pl_code local fun71
	call_c   Dyam_Update_Choice(fun70)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(7),&ref[209])
	fail_ret
	call_c   Dyam_Cut()
	move     &ref[210], R(0)
	move     0, R(1)
	pl_call  pred_record_without_doublon_1()
	pl_jump  fun16()

;; TERM 207: ['-guide',_G|_I]
c_code local build_ref_207
	ret_reg &ref[207]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(6),V(8))
	move_ret R(0)
	call_c   build_ref_344()
	call_c   Dyam_Create_List(&ref[344],R(0))
	move_ret ref[207]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 344: '-guide'
c_code local build_ref_344
	ret_reg &ref[344]
	call_c   Dyam_Create_Atom("-guide")
	move_ret ref[344]
	c_ret

;; TERM 208: guide(_G)
c_code local build_ref_208
	ret_reg &ref[208]
	call_c   build_ref_345()
	call_c   Dyam_Create_Unary(&ref[345],V(6))
	move_ret ref[208]
	c_ret

;; TERM 345: guide
c_code local build_ref_345
	ret_reg &ref[345]
	call_c   Dyam_Create_Atom("guide")
	move_ret ref[345]
	c_ret

long local pool_fun72[4]=[65538,build_ref_207,build_ref_208,pool_fun71]

pl_code local fun72
	call_c   Dyam_Update_Choice(fun71)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(7),&ref[207])
	fail_ret
	call_c   Dyam_Cut()
	move     &ref[208], R(0)
	move     S(5), R(1)
	pl_call  pred_record_without_doublon_1()
	pl_jump  fun16()

;; TERM 205: ['-stop',_F|_I]
c_code local build_ref_205
	ret_reg &ref[205]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(5),V(8))
	move_ret R(0)
	call_c   build_ref_346()
	call_c   Dyam_Create_List(&ref[346],R(0))
	move_ret ref[205]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 346: '-stop'
c_code local build_ref_346
	ret_reg &ref[346]
	call_c   Dyam_Create_Atom("-stop")
	move_ret ref[346]
	c_ret

;; TERM 206: stop_compilation_at(_F)
c_code local build_ref_206
	ret_reg &ref[206]
	call_c   build_ref_314()
	call_c   Dyam_Create_Unary(&ref[314],V(5))
	move_ret ref[206]
	c_ret

long local pool_fun73[4]=[65538,build_ref_205,build_ref_206,pool_fun72]

pl_code local fun73
	call_c   Dyam_Update_Choice(fun72)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(7),&ref[205])
	fail_ret
	call_c   Dyam_Cut()
	call_c   DYAM_evpred_assert_1(&ref[206])
	pl_jump  fun16()

;; TERM 203: ['-warning'|_I]
c_code local build_ref_203
	ret_reg &ref[203]
	call_c   build_ref_284()
	call_c   Dyam_Create_List(&ref[284],V(8))
	move_ret ref[203]
	c_ret

;; TERM 204: warning
c_code local build_ref_204
	ret_reg &ref[204]
	call_c   Dyam_Create_Atom("warning")
	move_ret ref[204]
	c_ret

long local pool_fun74[4]=[65538,build_ref_203,build_ref_204,pool_fun73]

pl_code local fun74
	call_c   Dyam_Update_Choice(fun73)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(7),&ref[203])
	fail_ret
	call_c   Dyam_Cut()
	call_c   DYAM_evpred_assert_1(&ref[204])
	pl_jump  fun16()

;; TERM 200: ['-rcg'|_I]
c_code local build_ref_200
	ret_reg &ref[200]
	call_c   build_ref_286()
	call_c   Dyam_Create_List(&ref[286],V(8))
	move_ret ref[200]
	c_ret

;; TERM 201: grammar_kind(_E)
c_code local build_ref_201
	ret_reg &ref[201]
	call_c   build_ref_329()
	call_c   Dyam_Create_Unary(&ref[329],V(4))
	move_ret ref[201]
	c_ret

;; TERM 202: rcg
c_code local build_ref_202
	ret_reg &ref[202]
	call_c   Dyam_Create_Atom("rcg")
	move_ret ref[202]
	c_ret

long local pool_fun75[5]=[65539,build_ref_200,build_ref_201,build_ref_202,pool_fun74]

pl_code local fun75
	call_c   Dyam_Update_Choice(fun74)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(7),&ref[200])
	fail_ret
	call_c   Dyam_Cut()
	pl_call  Object_1(&ref[201])
	call_c   Dyam_Reg_Load_Ptr(2,V(4))
	fail_ret
	move     &ref[202], R(4)
	move     0, R(5)
	call_c   DyALog_Mutable_Write(R(2),&R(4),0)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(4))
	fail_ret
	pl_jump  fun16()

;; TERM 199: ['-tfs',_B|_I]
c_code local build_ref_199
	ret_reg &ref[199]
	call_c   build_ref_347()
	call_c   build_ref_220()
	call_c   Dyam_Create_List(&ref[347],&ref[220])
	move_ret ref[199]
	c_ret

;; TERM 347: '-tfs'
c_code local build_ref_347
	ret_reg &ref[347]
	call_c   Dyam_Create_Atom("-tfs")
	move_ret ref[347]
	c_ret

long local pool_fun76[3]=[65537,build_ref_199,pool_fun75]

pl_code local fun76
	call_c   Dyam_Update_Choice(fun75)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(7),&ref[199])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(1))
	pl_call  pred_load_tfs_1()
	pl_jump  fun16()

;; TERM 198: ['--version'|_I]
c_code local build_ref_198
	ret_reg &ref[198]
	call_c   build_ref_348()
	call_c   Dyam_Create_List(&ref[348],V(8))
	move_ret ref[198]
	c_ret

;; TERM 348: '--version'
c_code local build_ref_348
	ret_reg &ref[348]
	call_c   Dyam_Create_Atom("--version")
	move_ret ref[348]
	c_ret

long local pool_fun77[3]=[65537,build_ref_198,pool_fun76]

pl_code local fun77
	call_c   Dyam_Update_Choice(fun76)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(7),&ref[198])
	fail_ret
	call_c   Dyam_Cut()
	pl_call  pred_emit_compiler_info_0()
	pl_jump  fun16()

;; TERM 197: ['-version'|_I]
c_code local build_ref_197
	ret_reg &ref[197]
	call_c   build_ref_290()
	call_c   Dyam_Create_List(&ref[290],V(8))
	move_ret ref[197]
	c_ret

long local pool_fun78[3]=[65537,build_ref_197,pool_fun77]

pl_code local fun78
	call_c   Dyam_Update_Choice(fun77)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(7),&ref[197])
	fail_ret
	call_c   Dyam_Cut()
	pl_call  pred_emit_compiler_info_0()
	pl_jump  fun16()

;; TERM 195: ['-use',_B|_I]
c_code local build_ref_195
	ret_reg &ref[195]
	call_c   build_ref_349()
	call_c   build_ref_220()
	call_c   Dyam_Create_List(&ref[349],&ref[220])
	move_ret ref[195]
	c_ret

;; TERM 349: '-use'
c_code local build_ref_349
	ret_reg &ref[349]
	call_c   Dyam_Create_Atom("-use")
	move_ret ref[349]
	c_ret

;; TERM 196: require
c_code local build_ref_196
	ret_reg &ref[196]
	call_c   Dyam_Create_Atom("require")
	move_ret ref[196]
	c_ret

long local pool_fun79[4]=[65538,build_ref_195,build_ref_196,pool_fun78]

pl_code local fun79
	call_c   Dyam_Update_Choice(fun78)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(7),&ref[195])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(1))
	move     &ref[196], R(2)
	move     0, R(3)
	pl_call  pred_read_files_2()
	pl_jump  fun16()

;; TERM 194: ['-res',_B|_I]
c_code local build_ref_194
	ret_reg &ref[194]
	call_c   build_ref_350()
	call_c   build_ref_220()
	call_c   Dyam_Create_List(&ref[350],&ref[220])
	move_ret ref[194]
	c_ret

;; TERM 350: '-res'
c_code local build_ref_350
	ret_reg &ref[350]
	call_c   Dyam_Create_Atom("-res")
	move_ret ref[350]
	c_ret

long local pool_fun80[4]=[65538,build_ref_194,build_ref_181,pool_fun79]

pl_code local fun80
	call_c   Dyam_Update_Choice(fun79)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(7),&ref[194])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(1))
	move     &ref[181], R(2)
	move     0, R(3)
	pl_call  pred_read_files_2()
	pl_jump  fun16()

;; TERM 193: ['-I',_D|_I]
c_code local build_ref_193
	ret_reg &ref[193]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(3),V(8))
	move_ret R(0)
	call_c   build_ref_351()
	call_c   Dyam_Create_List(&ref[351],R(0))
	move_ret ref[193]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 351: '-I'
c_code local build_ref_351
	ret_reg &ref[351]
	call_c   Dyam_Create_Atom("-I")
	move_ret ref[351]
	c_ret

long local pool_fun81[3]=[65537,build_ref_193,pool_fun80]

pl_code local fun81
	call_c   Dyam_Update_Choice(fun80)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(7),&ref[193])
	fail_ret
	call_c   Dyam_Cut()
	call_c   DYAM_Add_Load_Path_1(V(3))
	fail_ret
	pl_jump  fun16()

;; TERM 191: ['-verbose_tag'|_I]
c_code local build_ref_191
	ret_reg &ref[191]
	call_c   build_ref_298()
	call_c   Dyam_Create_List(&ref[298],V(8))
	move_ret ref[191]
	c_ret

;; TERM 192: verbose_tag
c_code local build_ref_192
	ret_reg &ref[192]
	call_c   Dyam_Create_Atom("verbose_tag")
	move_ret ref[192]
	c_ret

long local pool_fun82[4]=[65538,build_ref_191,build_ref_192,pool_fun81]

pl_code local fun82
	call_c   Dyam_Update_Choice(fun81)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(7),&ref[191])
	fail_ret
	call_c   Dyam_Cut()
	move     &ref[192], R(0)
	move     0, R(1)
	pl_call  pred_record_without_doublon_1()
	pl_jump  fun16()

;; TERM 188: ['-parse'|_I]
c_code local build_ref_188
	ret_reg &ref[188]
	call_c   build_ref_300()
	call_c   Dyam_Create_List(&ref[300],V(8))
	move_ret ref[188]
	c_ret

;; TERM 189: parse_mode(_C)
c_code local build_ref_189
	ret_reg &ref[189]
	call_c   build_ref_325()
	call_c   Dyam_Create_Unary(&ref[325],V(2))
	move_ret ref[189]
	c_ret

;; TERM 190: parse_mode(token)
c_code local build_ref_190
	ret_reg &ref[190]
	call_c   build_ref_325()
	call_c   build_ref_352()
	call_c   Dyam_Create_Unary(&ref[325],&ref[352])
	move_ret ref[190]
	c_ret

;; TERM 352: token
c_code local build_ref_352
	ret_reg &ref[352]
	call_c   Dyam_Create_Atom("token")
	move_ret ref[352]
	c_ret

long local pool_fun83[5]=[65539,build_ref_188,build_ref_189,build_ref_190,pool_fun82]

pl_code local fun83
	call_c   Dyam_Update_Choice(fun82)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(7),&ref[188])
	fail_ret
	call_c   Dyam_Cut()
	call_c   DYAM_evpred_retract(&ref[189])
	fail_ret
	call_c   DYAM_evpred_assert_1(&ref[190])
	pl_jump  fun16()

;; TERM 186: ['-o',_B|_I]
c_code local build_ref_186
	ret_reg &ref[186]
	call_c   build_ref_353()
	call_c   build_ref_220()
	call_c   Dyam_Create_List(&ref[353],&ref[220])
	move_ret ref[186]
	c_ret

;; TERM 353: '-o'
c_code local build_ref_353
	ret_reg &ref[353]
	call_c   Dyam_Create_Atom("-o")
	move_ret ref[353]
	c_ret

;; TERM 187: output_file(_B)
c_code local build_ref_187
	ret_reg &ref[187]
	call_c   build_ref_354()
	call_c   Dyam_Create_Unary(&ref[354],V(1))
	move_ret ref[187]
	c_ret

;; TERM 354: output_file
c_code local build_ref_354
	ret_reg &ref[354]
	call_c   Dyam_Create_Atom("output_file")
	move_ret ref[354]
	c_ret

long local pool_fun84[4]=[65538,build_ref_186,build_ref_187,pool_fun83]

pl_code local fun84
	call_c   Dyam_Update_Choice(fun83)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(7),&ref[186])
	fail_ret
	call_c   Dyam_Cut()
	call_c   DYAM_evpred_assert_1(&ref[187])
	pl_jump  fun16()

;; TERM 185: ['-f',_B|_I]
c_code local build_ref_185
	ret_reg &ref[185]
	call_c   build_ref_355()
	call_c   build_ref_220()
	call_c   Dyam_Create_List(&ref[355],&ref[220])
	move_ret ref[185]
	c_ret

;; TERM 355: '-f'
c_code local build_ref_355
	ret_reg &ref[355]
	call_c   Dyam_Create_Atom("-f")
	move_ret ref[355]
	c_ret

long local pool_fun85[4]=[65538,build_ref_183,build_ref_185,pool_fun84]

pl_code local fun85
	call_c   Dyam_Pool(pool_fun85)
	call_c   Dyam_Unify_Item(&ref[183])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun84)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(7),&ref[185])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(1))
	pl_call  pred_main_file_1()
	call_c   Dyam_Reg_Load(0,V(1))
	pl_call  pred_read_file_alt_1()
	pl_jump  fun16()

;; TERM 184: '*GUARD*'(parse_option(_H, _I)) :> []
c_code local build_ref_184
	ret_reg &ref[184]
	call_c   build_ref_183()
	call_c   Dyam_Create_Binary(I(9),&ref[183],I(0))
	move_ret ref[184]
	c_ret

c_code local build_seed_50
	ret_reg &seed[50]
	call_c   build_ref_120()
	call_c   build_ref_160()
	call_c   Dyam_Seed_Start(&ref[120],&ref[160],I(0),fun30,1)
	call_c   build_ref_158()
	call_c   Dyam_Seed_Add_Comp(&ref[158],fun59,0)
	call_c   Dyam_Seed_End()
	move_ret seed[50]
	c_ret

;; TERM 158: '*RITEM*'(gen_application(_C), voidret)
c_code local build_ref_158
	ret_reg &ref[158]
	call_c   build_ref_4()
	call_c   build_ref_154()
	call_c   build_ref_6()
	call_c   Dyam_Create_Binary(&ref[4],&ref[154],&ref[6])
	move_ret ref[158]
	c_ret

;; TERM 154: gen_application(_C)
c_code local build_ref_154
	ret_reg &ref[154]
	call_c   build_ref_356()
	call_c   Dyam_Create_Unary(&ref[356],V(2))
	move_ret ref[154]
	c_ret

;; TERM 356: gen_application
c_code local build_ref_356
	ret_reg &ref[356]
	call_c   Dyam_Create_Atom("gen_application")
	move_ret ref[356]
	c_ret

pl_code local fun59
	call_c   build_ref_158()
	call_c   Dyam_Unify_Item(&ref[158])
	fail_ret
	pl_jump  Follow_Cont(V(1))

;; TERM 160: '*RITEM*'(gen_application(_C), voidret) :> main(3, '$TUPPLE'(35105721554936))
c_code local build_ref_160
	ret_reg &ref[160]
	call_c   build_ref_158()
	call_c   build_ref_159()
	call_c   Dyam_Create_Binary(I(9),&ref[158],&ref[159])
	move_ret ref[160]
	c_ret

;; TERM 159: main(3, '$TUPPLE'(35105721554936))
c_code local build_ref_159
	ret_reg &ref[159]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,134217728)
	move_ret R(0)
	call_c   build_ref_357()
	call_c   Dyam_Create_Binary(&ref[357],N(3),R(0))
	move_ret ref[159]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 357: main
c_code local build_ref_357
	ret_reg &ref[357]
	call_c   Dyam_Create_Atom("main")
	move_ret ref[357]
	c_ret

;; TERM 120: '*CURNEXT*'
c_code local build_ref_120
	ret_reg &ref[120]
	call_c   Dyam_Create_Atom("*CURNEXT*")
	move_ret ref[120]
	c_ret

c_code local build_seed_49
	ret_reg &seed[49]
	call_c   build_ref_14()
	call_c   build_ref_155()
	call_c   Dyam_Seed_Start(&ref[14],&ref[155],&ref[155],fun0,1)
	call_c   build_ref_157()
	call_c   Dyam_Seed_Add_Comp(&ref[157],&ref[155],0)
	call_c   Dyam_Seed_End()
	move_ret seed[49]
	c_ret

;; TERM 157: '*FIRST*'(gen_application(_C)) :> '$$HOLE$$'
c_code local build_ref_157
	ret_reg &ref[157]
	call_c   build_ref_156()
	call_c   Dyam_Create_Binary(I(9),&ref[156],I(7))
	move_ret ref[157]
	c_ret

;; TERM 156: '*FIRST*'(gen_application(_C))
c_code local build_ref_156
	ret_reg &ref[156]
	call_c   build_ref_17()
	call_c   build_ref_154()
	call_c   Dyam_Create_Unary(&ref[17],&ref[154])
	move_ret ref[156]
	c_ret

;; TERM 155: '*CITEM*'(gen_application(_C), gen_application(_C))
c_code local build_ref_155
	ret_reg &ref[155]
	call_c   build_ref_14()
	call_c   build_ref_154()
	call_c   Dyam_Create_Binary(&ref[14],&ref[154],&ref[154])
	move_ret ref[155]
	c_ret

c_code local build_seed_48
	ret_reg &seed[48]
	call_c   build_ref_120()
	call_c   build_ref_153()
	call_c   Dyam_Seed_Start(&ref[120],&ref[153],I(0),fun30,1)
	call_c   build_ref_7()
	call_c   Dyam_Seed_Add_Comp(&ref[7],fun55,0)
	call_c   Dyam_Seed_End()
	move_ret seed[48]
	c_ret

;; TERM 7: '*RITEM*'(compile, voidret)
c_code local build_ref_7
	ret_reg &ref[7]
	call_c   build_ref_4()
	call_c   build_ref_5()
	call_c   build_ref_6()
	call_c   Dyam_Create_Binary(&ref[4],&ref[5],&ref[6])
	move_ret ref[7]
	c_ret

pl_code local fun55
	call_c   build_ref_7()
	call_c   Dyam_Unify_Item(&ref[7])
	fail_ret
	pl_jump  Follow_Cont(V(1))

;; TERM 153: '*RITEM*'(compile, voidret) :> main(2, '$TUPPLE'(35105721554936))
c_code local build_ref_153
	ret_reg &ref[153]
	call_c   build_ref_7()
	call_c   build_ref_152()
	call_c   Dyam_Create_Binary(I(9),&ref[7],&ref[152])
	move_ret ref[153]
	c_ret

;; TERM 152: main(2, '$TUPPLE'(35105721554936))
c_code local build_ref_152
	ret_reg &ref[152]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,134217728)
	move_ret R(0)
	call_c   build_ref_357()
	call_c   Dyam_Create_Binary(&ref[357],N(2),R(0))
	move_ret ref[152]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_47
	ret_reg &seed[47]
	call_c   build_ref_14()
	call_c   build_ref_150()
	call_c   Dyam_Seed_Start(&ref[14],&ref[150],&ref[150],fun0,1)
	call_c   build_ref_151()
	call_c   Dyam_Seed_Add_Comp(&ref[151],&ref[150],0)
	call_c   Dyam_Seed_End()
	move_ret seed[47]
	c_ret

;; TERM 151: '*FIRST*'(compile) :> '$$HOLE$$'
c_code local build_ref_151
	ret_reg &ref[151]
	call_c   build_ref_80()
	call_c   Dyam_Create_Binary(I(9),&ref[80],I(7))
	move_ret ref[151]
	c_ret

;; TERM 150: '*CITEM*'(compile, compile)
c_code local build_ref_150
	ret_reg &ref[150]
	call_c   build_ref_14()
	call_c   build_ref_5()
	call_c   Dyam_Create_Binary(&ref[14],&ref[5],&ref[5])
	move_ret ref[150]
	c_ret

c_code local build_seed_42
	ret_reg &seed[42]
	call_c   build_ref_120()
	call_c   build_ref_134()
	call_c   Dyam_Seed_Start(&ref[120],&ref[134],I(0),fun30,1)
	call_c   build_ref_132()
	call_c   Dyam_Seed_Add_Comp(&ref[132],fun36,0)
	call_c   Dyam_Seed_End()
	move_ret seed[42]
	c_ret

;; TERM 132: '*RITEM*'(stdprolog_install(_C), voidret)
c_code local build_ref_132
	ret_reg &ref[132]
	call_c   build_ref_4()
	call_c   build_ref_128()
	call_c   build_ref_6()
	call_c   Dyam_Create_Binary(&ref[4],&ref[128],&ref[6])
	move_ret ref[132]
	c_ret

;; TERM 128: stdprolog_install(_C)
c_code local build_ref_128
	ret_reg &ref[128]
	call_c   build_ref_358()
	call_c   Dyam_Create_Unary(&ref[358],V(2))
	move_ret ref[128]
	c_ret

;; TERM 358: stdprolog_install
c_code local build_ref_358
	ret_reg &ref[358]
	call_c   Dyam_Create_Atom("stdprolog_install")
	move_ret ref[358]
	c_ret

pl_code local fun36
	call_c   build_ref_132()
	call_c   Dyam_Unify_Item(&ref[132])
	fail_ret
	pl_jump  Follow_Cont(V(1))

;; TERM 134: '*RITEM*'(stdprolog_install(_C), voidret) :> main(1, '$TUPPLE'(35105721554936))
c_code local build_ref_134
	ret_reg &ref[134]
	call_c   build_ref_132()
	call_c   build_ref_133()
	call_c   Dyam_Create_Binary(I(9),&ref[132],&ref[133])
	move_ret ref[134]
	c_ret

;; TERM 133: main(1, '$TUPPLE'(35105721554936))
c_code local build_ref_133
	ret_reg &ref[133]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,134217728)
	move_ret R(0)
	call_c   build_ref_357()
	call_c   Dyam_Create_Binary(&ref[357],N(1),R(0))
	move_ret ref[133]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_41
	ret_reg &seed[41]
	call_c   build_ref_14()
	call_c   build_ref_129()
	call_c   Dyam_Seed_Start(&ref[14],&ref[129],&ref[129],fun0,1)
	call_c   build_ref_131()
	call_c   Dyam_Seed_Add_Comp(&ref[131],&ref[129],0)
	call_c   Dyam_Seed_End()
	move_ret seed[41]
	c_ret

;; TERM 131: '*FIRST*'(stdprolog_install(_C)) :> '$$HOLE$$'
c_code local build_ref_131
	ret_reg &ref[131]
	call_c   build_ref_130()
	call_c   Dyam_Create_Binary(I(9),&ref[130],I(7))
	move_ret ref[131]
	c_ret

;; TERM 130: '*FIRST*'(stdprolog_install(_C))
c_code local build_ref_130
	ret_reg &ref[130]
	call_c   build_ref_17()
	call_c   build_ref_128()
	call_c   Dyam_Create_Unary(&ref[17],&ref[128])
	move_ret ref[130]
	c_ret

;; TERM 129: '*CITEM*'(stdprolog_install(_C), stdprolog_install(_C))
c_code local build_ref_129
	ret_reg &ref[129]
	call_c   build_ref_14()
	call_c   build_ref_128()
	call_c   Dyam_Create_Binary(&ref[14],&ref[128],&ref[128])
	move_ret ref[129]
	c_ret

c_code local build_seed_40
	ret_reg &seed[40]
	call_c   build_ref_120()
	call_c   build_ref_122()
	call_c   Dyam_Seed_Start(&ref[120],&ref[122],I(0),fun30,1)
	call_c   build_ref_9()
	call_c   Dyam_Seed_Add_Comp(&ref[9],fun29,0)
	call_c   Dyam_Seed_End()
	move_ret seed[40]
	c_ret

;; TERM 9: '*RITEM*'(emit, voidret)
c_code local build_ref_9
	ret_reg &ref[9]
	call_c   build_ref_4()
	call_c   build_ref_8()
	call_c   build_ref_6()
	call_c   Dyam_Create_Binary(&ref[4],&ref[8],&ref[6])
	move_ret ref[9]
	c_ret

pl_code local fun29
	call_c   build_ref_9()
	call_c   Dyam_Unify_Item(&ref[9])
	fail_ret
	pl_jump  Follow_Cont(V(1))

;; TERM 122: '*RITEM*'(emit, voidret) :> main(0, '$TUPPLE'(35105721554936))
c_code local build_ref_122
	ret_reg &ref[122]
	call_c   build_ref_9()
	call_c   build_ref_121()
	call_c   Dyam_Create_Binary(I(9),&ref[9],&ref[121])
	move_ret ref[122]
	c_ret

;; TERM 121: main(0, '$TUPPLE'(35105721554936))
c_code local build_ref_121
	ret_reg &ref[121]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,134217728)
	move_ret R(0)
	call_c   build_ref_357()
	call_c   Dyam_Create_Binary(&ref[357],N(0),R(0))
	move_ret ref[121]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_39
	ret_reg &seed[39]
	call_c   build_ref_14()
	call_c   build_ref_117()
	call_c   Dyam_Seed_Start(&ref[14],&ref[117],&ref[117],fun0,1)
	call_c   build_ref_119()
	call_c   Dyam_Seed_Add_Comp(&ref[119],&ref[117],0)
	call_c   Dyam_Seed_End()
	move_ret seed[39]
	c_ret

;; TERM 119: '*FIRST*'(emit) :> '$$HOLE$$'
c_code local build_ref_119
	ret_reg &ref[119]
	call_c   build_ref_118()
	call_c   Dyam_Create_Binary(I(9),&ref[118],I(7))
	move_ret ref[119]
	c_ret

;; TERM 118: '*FIRST*'(emit)
c_code local build_ref_118
	ret_reg &ref[118]
	call_c   build_ref_17()
	call_c   build_ref_8()
	call_c   Dyam_Create_Unary(&ref[17],&ref[8])
	move_ret ref[118]
	c_ret

;; TERM 117: '*CITEM*'(emit, emit)
c_code local build_ref_117
	ret_reg &ref[117]
	call_c   build_ref_14()
	call_c   build_ref_8()
	call_c   Dyam_Create_Binary(&ref[14],&ref[8],&ref[8])
	move_ret ref[117]
	c_ret

;; TERM 268: [module,file,preds]
c_code local build_ref_268
	ret_reg &ref[268]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_361()
	call_c   Dyam_Create_List(&ref[361],I(0))
	move_ret R(0)
	call_c   build_ref_360()
	call_c   Dyam_Create_List(&ref[360],R(0))
	move_ret R(0)
	call_c   build_ref_359()
	call_c   Dyam_Create_List(&ref[359],R(0))
	move_ret ref[268]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 359: module
c_code local build_ref_359
	ret_reg &ref[359]
	call_c   Dyam_Create_Atom("module")
	move_ret ref[359]
	c_ret

;; TERM 360: file
c_code local build_ref_360
	ret_reg &ref[360]
	call_c   Dyam_Create_Atom("file")
	move_ret ref[360]
	c_ret

;; TERM 361: preds
c_code local build_ref_361
	ret_reg &ref[361]
	call_c   Dyam_Create_Atom("preds")
	move_ret ref[361]
	c_ret

;; TERM 267: import
c_code local build_ref_267
	ret_reg &ref[267]
	call_c   Dyam_Create_Atom("import")
	move_ret ref[267]
	c_ret

;; TERM 266: [return,name,code,choice_size,load]
c_code local build_ref_266
	ret_reg &ref[266]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_323()
	call_c   Dyam_Create_List(&ref[323],I(0))
	move_ret R(0)
	call_c   build_ref_365()
	call_c   Dyam_Create_List(&ref[365],R(0))
	move_ret R(0)
	call_c   build_ref_364()
	call_c   Dyam_Create_List(&ref[364],R(0))
	move_ret R(0)
	call_c   build_ref_363()
	call_c   Dyam_Create_List(&ref[363],R(0))
	move_ret R(0)
	call_c   build_ref_362()
	call_c   Dyam_Create_List(&ref[362],R(0))
	move_ret ref[266]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 362: return
c_code local build_ref_362
	ret_reg &ref[362]
	call_c   Dyam_Create_Atom("return")
	move_ret ref[362]
	c_ret

;; TERM 363: name
c_code local build_ref_363
	ret_reg &ref[363]
	call_c   Dyam_Create_Atom("name")
	move_ret ref[363]
	c_ret

;; TERM 364: code
c_code local build_ref_364
	ret_reg &ref[364]
	call_c   Dyam_Create_Atom("code")
	move_ret ref[364]
	c_ret

;; TERM 365: choice_size
c_code local build_ref_365
	ret_reg &ref[365]
	call_c   Dyam_Create_Atom("choice_size")
	move_ret ref[365]
	c_ret

;; TERM 265: interface
c_code local build_ref_265
	ret_reg &ref[265]
	call_c   Dyam_Create_Atom("interface")
	move_ret ref[265]
	c_ret

;; TERM 264: [ms,as]
c_code local build_ref_264
	ret_reg &ref[264]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_367()
	call_c   Dyam_Create_List(&ref[367],I(0))
	move_ret R(0)
	call_c   build_ref_366()
	call_c   Dyam_Create_List(&ref[366],R(0))
	move_ret ref[264]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 366: ms
c_code local build_ref_366
	ret_reg &ref[366]
	call_c   Dyam_Create_Atom("ms")
	move_ret ref[366]
	c_ret

;; TERM 367: as
c_code local build_ref_367
	ret_reg &ref[367]
	call_c   Dyam_Create_Atom("as")
	move_ret ref[367]
	c_ret

;; TERM 263: sa_mini
c_code local build_ref_263
	ret_reg &ref[263]
	call_c   Dyam_Create_Atom("sa_mini")
	move_ret ref[263]
	c_ret

;; TERM 262: [start,end]
c_code local build_ref_262
	ret_reg &ref[262]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_368()
	call_c   Dyam_Create_List(&ref[368],I(0))
	move_ret R(0)
	call_c   build_ref_15()
	call_c   Dyam_Create_List(&ref[15],R(0))
	move_ret ref[262]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 368: end
c_code local build_ref_368
	ret_reg &ref[368]
	call_c   Dyam_Create_Atom("end")
	move_ret ref[368]
	c_ret

;; TERM 261: sa_escape
c_code local build_ref_261
	ret_reg &ref[261]
	call_c   Dyam_Create_Atom("sa_escape")
	move_ret ref[261]
	c_ret

;; TERM 260: [mspop,aspop,holes,escape]
c_code local build_ref_260
	ret_reg &ref[260]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_372()
	call_c   Dyam_Create_List(&ref[372],I(0))
	move_ret R(0)
	call_c   build_ref_371()
	call_c   Dyam_Create_List(&ref[371],R(0))
	move_ret R(0)
	call_c   build_ref_370()
	call_c   Dyam_Create_List(&ref[370],R(0))
	move_ret R(0)
	call_c   build_ref_369()
	call_c   Dyam_Create_List(&ref[369],R(0))
	move_ret ref[260]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 369: mspop
c_code local build_ref_369
	ret_reg &ref[369]
	call_c   Dyam_Create_Atom("mspop")
	move_ret ref[369]
	c_ret

;; TERM 370: aspop
c_code local build_ref_370
	ret_reg &ref[370]
	call_c   Dyam_Create_Atom("aspop")
	move_ret ref[370]
	c_ret

;; TERM 371: holes
c_code local build_ref_371
	ret_reg &ref[371]
	call_c   Dyam_Create_Atom("holes")
	move_ret ref[371]
	c_ret

;; TERM 372: escape
c_code local build_ref_372
	ret_reg &ref[372]
	call_c   Dyam_Create_Atom("escape")
	move_ret ref[372]
	c_ret

;; TERM 259: sa_env
c_code local build_ref_259
	ret_reg &ref[259]
	call_c   Dyam_Create_Atom("sa_env")
	move_ret ref[259]
	c_ret

;; TERM 258: [current]
c_code local build_ref_258
	ret_reg &ref[258]
	call_c   build_ref_373()
	call_c   Dyam_Create_List(&ref[373],I(0))
	move_ret ref[258]
	c_ret

;; TERM 373: current
c_code local build_ref_373
	ret_reg &ref[373]
	call_c   Dyam_Create_Atom("current")
	move_ret ref[373]
	c_ret

;; TERM 257: '*CAI*'
c_code local build_ref_257
	ret_reg &ref[257]
	call_c   Dyam_Create_Atom("*CAI*")
	move_ret ref[257]
	c_ret

;; TERM 256: [xstart,xend,aspop,current]
c_code local build_ref_256
	ret_reg &ref[256]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_375()
	call_c   build_ref_252()
	call_c   Dyam_Create_List(&ref[375],&ref[252])
	move_ret R(0)
	call_c   build_ref_374()
	call_c   Dyam_Create_List(&ref[374],R(0))
	move_ret ref[256]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 374: xstart
c_code local build_ref_374
	ret_reg &ref[374]
	call_c   Dyam_Create_Atom("xstart")
	move_ret ref[374]
	c_ret

;; TERM 252: [aspop,current]
c_code local build_ref_252
	ret_reg &ref[252]
	call_c   build_ref_370()
	call_c   build_ref_258()
	call_c   Dyam_Create_List(&ref[370],&ref[258])
	move_ret ref[252]
	c_ret

;; TERM 375: xend
c_code local build_ref_375
	ret_reg &ref[375]
	call_c   Dyam_Create_Atom("xend")
	move_ret ref[375]
	c_ret

;; TERM 255: '*RAI*'
c_code local build_ref_255
	ret_reg &ref[255]
	call_c   Dyam_Create_Atom("*RAI*")
	move_ret ref[255]
	c_ret

;; TERM 254: [mspop,mspop_adj,current]
c_code local build_ref_254
	ret_reg &ref[254]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_376()
	call_c   build_ref_258()
	call_c   Dyam_Create_List(&ref[376],&ref[258])
	move_ret R(0)
	call_c   build_ref_369()
	call_c   Dyam_Create_List(&ref[369],R(0))
	move_ret ref[254]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 376: mspop_adj
c_code local build_ref_376
	ret_reg &ref[376]
	call_c   Dyam_Create_Atom("mspop_adj")
	move_ret ref[376]
	c_ret

;; TERM 253: '*RFI*'
c_code local build_ref_253
	ret_reg &ref[253]
	call_c   Dyam_Create_Atom("*RFI*")
	move_ret ref[253]
	c_ret

;; TERM 251: '*CFI*'
c_code local build_ref_251
	ret_reg &ref[251]
	call_c   Dyam_Create_Atom("*CFI*")
	move_ret ref[251]
	c_ret

;; TERM 250: [task,cont]
c_code local build_ref_250
	ret_reg &ref[250]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_315()
	call_c   Dyam_Create_List(&ref[315],I(0))
	move_ret R(0)
	call_c   build_ref_377()
	call_c   Dyam_Create_List(&ref[377],R(0))
	move_ret ref[250]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 377: task
c_code local build_ref_377
	ret_reg &ref[377]
	call_c   Dyam_Create_Atom("task")
	move_ret ref[377]
	c_ret

;; TERM 249: '*WAIT*'
c_code local build_ref_249
	ret_reg &ref[249]
	call_c   Dyam_Create_Atom("*WAIT*")
	move_ret ref[249]
	c_ret

;; TERM 248: [rest,cont,tupple]
c_code local build_ref_248
	ret_reg &ref[248]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_379()
	call_c   Dyam_Create_List(&ref[379],I(0))
	move_ret R(0)
	call_c   build_ref_315()
	call_c   Dyam_Create_List(&ref[315],R(0))
	move_ret R(0)
	call_c   build_ref_378()
	call_c   Dyam_Create_List(&ref[378],R(0))
	move_ret ref[248]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 378: rest
c_code local build_ref_378
	ret_reg &ref[378]
	call_c   Dyam_Create_Atom("rest")
	move_ret ref[378]
	c_ret

;; TERM 379: tupple
c_code local build_ref_379
	ret_reg &ref[379]
	call_c   Dyam_Create_Atom("tupple")
	move_ret ref[379]
	c_ret

;; TERM 247: '*WAIT-CONT*'
c_code local build_ref_247
	ret_reg &ref[247]
	call_c   Dyam_Create_Atom("*WAIT-CONT*")
	move_ret ref[247]
	c_ret

;; TERM 246: [name,version,author,email]
c_code local build_ref_246
	ret_reg &ref[246]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_382()
	call_c   Dyam_Create_List(&ref[382],I(0))
	move_ret R(0)
	call_c   build_ref_381()
	call_c   Dyam_Create_List(&ref[381],R(0))
	move_ret R(0)
	call_c   build_ref_380()
	call_c   Dyam_Create_List(&ref[380],R(0))
	move_ret R(0)
	call_c   build_ref_363()
	call_c   Dyam_Create_List(&ref[363],R(0))
	move_ret ref[246]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 380: version
c_code local build_ref_380
	ret_reg &ref[380]
	call_c   Dyam_Create_Atom("version")
	move_ret ref[380]
	c_ret

;; TERM 381: author
c_code local build_ref_381
	ret_reg &ref[381]
	call_c   Dyam_Create_Atom("author")
	move_ret ref[381]
	c_ret

;; TERM 382: email
c_code local build_ref_382
	ret_reg &ref[382]
	call_c   Dyam_Create_Atom("email")
	move_ret ref[382]
	c_ret

;; TERM 245: compiler_info
c_code local build_ref_245
	ret_reg &ref[245]
	call_c   Dyam_Create_Atom("compiler_info")
	move_ret ref[245]
	c_ret

;; TERM 244: [model,id,anchor,subs_comp,code,go,tabule,schedule,subsume,model_ref,subs_comp_ref,c_type,c_type_ref,out_env,appinfo,tail_flag]
c_code local build_ref_244
	ret_reg &ref[244]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_397()
	call_c   Dyam_Create_List(&ref[397],I(0))
	move_ret R(0)
	call_c   build_ref_396()
	call_c   Dyam_Create_List(&ref[396],R(0))
	move_ret R(0)
	call_c   build_ref_395()
	call_c   Dyam_Create_List(&ref[395],R(0))
	move_ret R(0)
	call_c   build_ref_394()
	call_c   Dyam_Create_List(&ref[394],R(0))
	move_ret R(0)
	call_c   build_ref_393()
	call_c   Dyam_Create_List(&ref[393],R(0))
	move_ret R(0)
	call_c   build_ref_392()
	call_c   Dyam_Create_List(&ref[392],R(0))
	move_ret R(0)
	call_c   build_ref_391()
	call_c   Dyam_Create_List(&ref[391],R(0))
	move_ret R(0)
	call_c   build_ref_390()
	call_c   Dyam_Create_List(&ref[390],R(0))
	move_ret R(0)
	call_c   build_ref_389()
	call_c   Dyam_Create_List(&ref[389],R(0))
	move_ret R(0)
	call_c   build_ref_388()
	call_c   Dyam_Create_List(&ref[388],R(0))
	move_ret R(0)
	call_c   build_ref_387()
	call_c   Dyam_Create_List(&ref[387],R(0))
	move_ret R(0)
	call_c   build_ref_364()
	call_c   Dyam_Create_List(&ref[364],R(0))
	move_ret R(0)
	call_c   build_ref_386()
	call_c   Dyam_Create_List(&ref[386],R(0))
	move_ret R(0)
	call_c   build_ref_385()
	call_c   Dyam_Create_List(&ref[385],R(0))
	move_ret R(0)
	call_c   build_ref_384()
	call_c   Dyam_Create_List(&ref[384],R(0))
	move_ret R(0)
	call_c   build_ref_383()
	call_c   Dyam_Create_List(&ref[383],R(0))
	move_ret ref[244]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 383: model
c_code local build_ref_383
	ret_reg &ref[383]
	call_c   Dyam_Create_Atom("model")
	move_ret ref[383]
	c_ret

;; TERM 384: id
c_code local build_ref_384
	ret_reg &ref[384]
	call_c   Dyam_Create_Atom("id")
	move_ret ref[384]
	c_ret

;; TERM 385: anchor
c_code local build_ref_385
	ret_reg &ref[385]
	call_c   Dyam_Create_Atom("anchor")
	move_ret ref[385]
	c_ret

;; TERM 386: subs_comp
c_code local build_ref_386
	ret_reg &ref[386]
	call_c   Dyam_Create_Atom("subs_comp")
	move_ret ref[386]
	c_ret

;; TERM 387: go
c_code local build_ref_387
	ret_reg &ref[387]
	call_c   Dyam_Create_Atom("go")
	move_ret ref[387]
	c_ret

;; TERM 388: tabule
c_code local build_ref_388
	ret_reg &ref[388]
	call_c   Dyam_Create_Atom("tabule")
	move_ret ref[388]
	c_ret

;; TERM 389: schedule
c_code local build_ref_389
	ret_reg &ref[389]
	call_c   Dyam_Create_Atom("schedule")
	move_ret ref[389]
	c_ret

;; TERM 390: subsume
c_code local build_ref_390
	ret_reg &ref[390]
	call_c   Dyam_Create_Atom("subsume")
	move_ret ref[390]
	c_ret

;; TERM 391: model_ref
c_code local build_ref_391
	ret_reg &ref[391]
	call_c   Dyam_Create_Atom("model_ref")
	move_ret ref[391]
	c_ret

;; TERM 392: subs_comp_ref
c_code local build_ref_392
	ret_reg &ref[392]
	call_c   Dyam_Create_Atom("subs_comp_ref")
	move_ret ref[392]
	c_ret

;; TERM 393: c_type
c_code local build_ref_393
	ret_reg &ref[393]
	call_c   Dyam_Create_Atom("c_type")
	move_ret ref[393]
	c_ret

;; TERM 394: c_type_ref
c_code local build_ref_394
	ret_reg &ref[394]
	call_c   Dyam_Create_Atom("c_type_ref")
	move_ret ref[394]
	c_ret

;; TERM 395: out_env
c_code local build_ref_395
	ret_reg &ref[395]
	call_c   Dyam_Create_Atom("out_env")
	move_ret ref[395]
	c_ret

;; TERM 396: appinfo
c_code local build_ref_396
	ret_reg &ref[396]
	call_c   Dyam_Create_Atom("appinfo")
	move_ret ref[396]
	c_ret

;; TERM 397: tail_flag
c_code local build_ref_397
	ret_reg &ref[397]
	call_c   Dyam_Create_Atom("tail_flag")
	move_ret ref[397]
	c_ret

;; TERM 243: seed
c_code local build_ref_243
	ret_reg &ref[243]
	call_c   Dyam_Create_Atom("seed")
	move_ret ref[243]
	c_ret

;; TERM 242: [item,trans,item_comp,code,item_id,trans_id,mode,restrict,out_env,key]
c_code local build_ref_242
	ret_reg &ref[242]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_405()
	call_c   Dyam_Create_List(&ref[405],I(0))
	move_ret R(0)
	call_c   build_ref_395()
	call_c   Dyam_Create_List(&ref[395],R(0))
	move_ret R(0)
	call_c   build_ref_404()
	call_c   Dyam_Create_List(&ref[404],R(0))
	move_ret R(0)
	call_c   build_ref_403()
	call_c   Dyam_Create_List(&ref[403],R(0))
	move_ret R(0)
	call_c   build_ref_402()
	call_c   Dyam_Create_List(&ref[402],R(0))
	move_ret R(0)
	call_c   build_ref_401()
	call_c   Dyam_Create_List(&ref[401],R(0))
	move_ret R(0)
	call_c   build_ref_364()
	call_c   Dyam_Create_List(&ref[364],R(0))
	move_ret R(0)
	call_c   build_ref_400()
	call_c   Dyam_Create_List(&ref[400],R(0))
	move_ret R(0)
	call_c   build_ref_399()
	call_c   Dyam_Create_List(&ref[399],R(0))
	move_ret R(0)
	call_c   build_ref_398()
	call_c   Dyam_Create_List(&ref[398],R(0))
	move_ret ref[242]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 398: item
c_code local build_ref_398
	ret_reg &ref[398]
	call_c   Dyam_Create_Atom("item")
	move_ret ref[398]
	c_ret

;; TERM 399: trans
c_code local build_ref_399
	ret_reg &ref[399]
	call_c   Dyam_Create_Atom("trans")
	move_ret ref[399]
	c_ret

;; TERM 400: item_comp
c_code local build_ref_400
	ret_reg &ref[400]
	call_c   Dyam_Create_Atom("item_comp")
	move_ret ref[400]
	c_ret

;; TERM 401: item_id
c_code local build_ref_401
	ret_reg &ref[401]
	call_c   Dyam_Create_Atom("item_id")
	move_ret ref[401]
	c_ret

;; TERM 402: trans_id
c_code local build_ref_402
	ret_reg &ref[402]
	call_c   Dyam_Create_Atom("trans_id")
	move_ret ref[402]
	c_ret

;; TERM 403: mode
c_code local build_ref_403
	ret_reg &ref[403]
	call_c   Dyam_Create_Atom("mode")
	move_ret ref[403]
	c_ret

;; TERM 404: restrict
c_code local build_ref_404
	ret_reg &ref[404]
	call_c   Dyam_Create_Atom("restrict")
	move_ret ref[404]
	c_ret

;; TERM 405: key
c_code local build_ref_405
	ret_reg &ref[405]
	call_c   Dyam_Create_Atom("key")
	move_ret ref[405]
	c_ret

;; TERM 241: application
c_code local build_ref_241
	ret_reg &ref[241]
	call_c   Dyam_Create_Atom("application")
	move_ret ref[241]
	c_ret

;; TERM 240: [code,refs,ins]
c_code local build_ref_240
	ret_reg &ref[240]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_407()
	call_c   Dyam_Create_List(&ref[407],I(0))
	move_ret R(0)
	call_c   build_ref_406()
	call_c   Dyam_Create_List(&ref[406],R(0))
	move_ret R(0)
	call_c   build_ref_364()
	call_c   Dyam_Create_List(&ref[364],R(0))
	move_ret ref[240]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 406: refs
c_code local build_ref_406
	ret_reg &ref[406]
	call_c   Dyam_Create_Atom("refs")
	move_ret ref[406]
	c_ret

;; TERM 407: ins
c_code local build_ref_407
	ret_reg &ref[407]
	call_c   Dyam_Create_Atom("ins")
	move_ret ref[407]
	c_ret

;; TERM 239: '$fun_info'
c_code local build_ref_239
	ret_reg &ref[239]
	call_c   Dyam_Create_Atom("$fun_info")
	move_ret ref[239]
	c_ret

;; TERM 238: [id,label,children,kind,adj,spine,top,bot,token,adjleft,adjright,adjwrap]
c_code local build_ref_238
	ret_reg &ref[238]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_416()
	call_c   Dyam_Create_List(&ref[416],I(0))
	move_ret R(0)
	call_c   build_ref_415()
	call_c   Dyam_Create_List(&ref[415],R(0))
	move_ret R(0)
	call_c   build_ref_414()
	call_c   Dyam_Create_List(&ref[414],R(0))
	move_ret R(0)
	call_c   build_ref_352()
	call_c   Dyam_Create_List(&ref[352],R(0))
	move_ret R(0)
	call_c   build_ref_413()
	call_c   Dyam_Create_List(&ref[413],R(0))
	move_ret R(0)
	call_c   build_ref_412()
	call_c   Dyam_Create_List(&ref[412],R(0))
	move_ret R(0)
	call_c   build_ref_411()
	call_c   Dyam_Create_List(&ref[411],R(0))
	move_ret R(0)
	call_c   build_ref_224()
	call_c   Dyam_Create_List(&ref[224],R(0))
	move_ret R(0)
	call_c   build_ref_410()
	call_c   Dyam_Create_List(&ref[410],R(0))
	move_ret R(0)
	call_c   build_ref_409()
	call_c   Dyam_Create_List(&ref[409],R(0))
	move_ret R(0)
	call_c   build_ref_408()
	call_c   Dyam_Create_List(&ref[408],R(0))
	move_ret R(0)
	call_c   build_ref_384()
	call_c   Dyam_Create_List(&ref[384],R(0))
	move_ret ref[238]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 408: label
c_code local build_ref_408
	ret_reg &ref[408]
	call_c   Dyam_Create_Atom("label")
	move_ret ref[408]
	c_ret

;; TERM 409: children
c_code local build_ref_409
	ret_reg &ref[409]
	call_c   Dyam_Create_Atom("children")
	move_ret ref[409]
	c_ret

;; TERM 410: kind
c_code local build_ref_410
	ret_reg &ref[410]
	call_c   Dyam_Create_Atom("kind")
	move_ret ref[410]
	c_ret

;; TERM 224: adj
c_code local build_ref_224
	ret_reg &ref[224]
	call_c   Dyam_Create_Atom("adj")
	move_ret ref[224]
	c_ret

;; TERM 411: spine
c_code local build_ref_411
	ret_reg &ref[411]
	call_c   Dyam_Create_Atom("spine")
	move_ret ref[411]
	c_ret

;; TERM 412: top
c_code local build_ref_412
	ret_reg &ref[412]
	call_c   Dyam_Create_Atom("top")
	move_ret ref[412]
	c_ret

;; TERM 413: bot
c_code local build_ref_413
	ret_reg &ref[413]
	call_c   Dyam_Create_Atom("bot")
	move_ret ref[413]
	c_ret

;; TERM 414: adjleft
c_code local build_ref_414
	ret_reg &ref[414]
	call_c   Dyam_Create_Atom("adjleft")
	move_ret ref[414]
	c_ret

;; TERM 415: adjright
c_code local build_ref_415
	ret_reg &ref[415]
	call_c   Dyam_Create_Atom("adjright")
	move_ret ref[415]
	c_ret

;; TERM 416: adjwrap
c_code local build_ref_416
	ret_reg &ref[416]
	call_c   Dyam_Create_Atom("adjwrap")
	move_ret ref[416]
	c_ret

;; TERM 237: tag_node
c_code local build_ref_237
	ret_reg &ref[237]
	call_c   Dyam_Create_Atom("tag_node")
	move_ret ref[237]
	c_ret

;; TERM 236: [family,name,tree]
c_code local build_ref_236
	ret_reg &ref[236]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_331()
	call_c   Dyam_Create_List(&ref[331],I(0))
	move_ret R(0)
	call_c   build_ref_363()
	call_c   Dyam_Create_List(&ref[363],R(0))
	move_ret R(0)
	call_c   build_ref_417()
	call_c   Dyam_Create_List(&ref[417],R(0))
	move_ret ref[236]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 417: family
c_code local build_ref_417
	ret_reg &ref[417]
	call_c   Dyam_Create_Atom("family")
	move_ret ref[417]
	c_ret

;; TERM 235: tag_tree
c_code local build_ref_235
	ret_reg &ref[235]
	call_c   Dyam_Create_Atom("tag_tree")
	move_ret ref[235]
	c_ret

;; TERM 234: [family,coanchors,equations]
c_code local build_ref_234
	ret_reg &ref[234]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_419()
	call_c   Dyam_Create_List(&ref[419],I(0))
	move_ret R(0)
	call_c   build_ref_418()
	call_c   Dyam_Create_List(&ref[418],R(0))
	move_ret R(0)
	call_c   build_ref_417()
	call_c   Dyam_Create_List(&ref[417],R(0))
	move_ret ref[234]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 418: coanchors
c_code local build_ref_418
	ret_reg &ref[418]
	call_c   Dyam_Create_Atom("coanchors")
	move_ret ref[418]
	c_ret

;; TERM 419: equations
c_code local build_ref_419
	ret_reg &ref[419]
	call_c   Dyam_Create_Atom("equations")
	move_ret ref[419]
	c_ret

;; TERM 233: tag_anchor
c_code local build_ref_233
	ret_reg &ref[233]
	call_c   Dyam_Create_Atom("tag_anchor")
	move_ret ref[233]
	c_ret

;; TERM 232: [nt_pattern,left_pattern,right_pattern]
c_code local build_ref_232
	ret_reg &ref[232]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_422()
	call_c   Dyam_Create_List(&ref[422],I(0))
	move_ret R(0)
	call_c   build_ref_421()
	call_c   Dyam_Create_List(&ref[421],R(0))
	move_ret R(0)
	call_c   build_ref_420()
	call_c   Dyam_Create_List(&ref[420],R(0))
	move_ret ref[232]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 420: nt_pattern
c_code local build_ref_420
	ret_reg &ref[420]
	call_c   Dyam_Create_Atom("nt_pattern")
	move_ret ref[420]
	c_ret

;; TERM 421: left_pattern
c_code local build_ref_421
	ret_reg &ref[421]
	call_c   Dyam_Create_Atom("left_pattern")
	move_ret ref[421]
	c_ret

;; TERM 422: right_pattern
c_code local build_ref_422
	ret_reg &ref[422]
	call_c   Dyam_Create_Atom("right_pattern")
	move_ret ref[422]
	c_ret

;; TERM 231: nt_modulation
c_code local build_ref_231
	ret_reg &ref[231]
	call_c   Dyam_Create_Atom("nt_modulation")
	move_ret ref[231]
	c_ret

;; TERM 230: [subst_modulation,top_modulation,bot_modulation]
c_code local build_ref_230
	ret_reg &ref[230]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_425()
	call_c   Dyam_Create_List(&ref[425],I(0))
	move_ret R(0)
	call_c   build_ref_424()
	call_c   Dyam_Create_List(&ref[424],R(0))
	move_ret R(0)
	call_c   build_ref_423()
	call_c   Dyam_Create_List(&ref[423],R(0))
	move_ret ref[230]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 423: subst_modulation
c_code local build_ref_423
	ret_reg &ref[423]
	call_c   Dyam_Create_Atom("subst_modulation")
	move_ret ref[423]
	c_ret

;; TERM 424: top_modulation
c_code local build_ref_424
	ret_reg &ref[424]
	call_c   Dyam_Create_Atom("top_modulation")
	move_ret ref[424]
	c_ret

;; TERM 425: bot_modulation
c_code local build_ref_425
	ret_reg &ref[425]
	call_c   Dyam_Create_Atom("bot_modulation")
	move_ret ref[425]
	c_ret

;; TERM 229: tag_modulation
c_code local build_ref_229
	ret_reg &ref[229]
	call_c   Dyam_Create_Atom("tag_modulation")
	move_ret ref[229]
	c_ret

;; TERM 228: [goal,vars,from,to,collect_first,collect_last,collect_loop,collect_next,collect_pred]
c_code local build_ref_228
	ret_reg &ref[228]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_434()
	call_c   Dyam_Create_List(&ref[434],I(0))
	move_ret R(0)
	call_c   build_ref_433()
	call_c   Dyam_Create_List(&ref[433],R(0))
	move_ret R(0)
	call_c   build_ref_432()
	call_c   Dyam_Create_List(&ref[432],R(0))
	move_ret R(0)
	call_c   build_ref_431()
	call_c   Dyam_Create_List(&ref[431],R(0))
	move_ret R(0)
	call_c   build_ref_430()
	call_c   Dyam_Create_List(&ref[430],R(0))
	move_ret R(0)
	call_c   build_ref_429()
	call_c   Dyam_Create_List(&ref[429],R(0))
	move_ret R(0)
	call_c   build_ref_428()
	call_c   Dyam_Create_List(&ref[428],R(0))
	move_ret R(0)
	call_c   build_ref_427()
	call_c   Dyam_Create_List(&ref[427],R(0))
	move_ret R(0)
	call_c   build_ref_426()
	call_c   Dyam_Create_List(&ref[426],R(0))
	move_ret ref[228]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 426: goal
c_code local build_ref_426
	ret_reg &ref[426]
	call_c   Dyam_Create_Atom("goal")
	move_ret ref[426]
	c_ret

;; TERM 427: vars
c_code local build_ref_427
	ret_reg &ref[427]
	call_c   Dyam_Create_Atom("vars")
	move_ret ref[427]
	c_ret

;; TERM 428: from
c_code local build_ref_428
	ret_reg &ref[428]
	call_c   Dyam_Create_Atom("from")
	move_ret ref[428]
	c_ret

;; TERM 429: to
c_code local build_ref_429
	ret_reg &ref[429]
	call_c   Dyam_Create_Atom("to")
	move_ret ref[429]
	c_ret

;; TERM 430: collect_first
c_code local build_ref_430
	ret_reg &ref[430]
	call_c   Dyam_Create_Atom("collect_first")
	move_ret ref[430]
	c_ret

;; TERM 431: collect_last
c_code local build_ref_431
	ret_reg &ref[431]
	call_c   Dyam_Create_Atom("collect_last")
	move_ret ref[431]
	c_ret

;; TERM 432: collect_loop
c_code local build_ref_432
	ret_reg &ref[432]
	call_c   Dyam_Create_Atom("collect_loop")
	move_ret ref[432]
	c_ret

;; TERM 433: collect_next
c_code local build_ref_433
	ret_reg &ref[433]
	call_c   Dyam_Create_Atom("collect_next")
	move_ret ref[433]
	c_ret

;; TERM 434: collect_pred
c_code local build_ref_434
	ret_reg &ref[434]
	call_c   Dyam_Create_Atom("collect_pred")
	move_ret ref[434]
	c_ret

;; TERM 227: @*
c_code local build_ref_227
	ret_reg &ref[227]
	call_c   Dyam_Create_Atom("@*")
	move_ret ref[227]
	c_ret

;; TERM 226: [goal,plus,minus]
c_code local build_ref_226
	ret_reg &ref[226]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_436()
	call_c   Dyam_Create_List(&ref[436],I(0))
	move_ret R(0)
	call_c   build_ref_435()
	call_c   Dyam_Create_List(&ref[435],R(0))
	move_ret R(0)
	call_c   build_ref_426()
	call_c   Dyam_Create_List(&ref[426],R(0))
	move_ret ref[226]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 435: plus
c_code local build_ref_435
	ret_reg &ref[435]
	call_c   Dyam_Create_Atom("plus")
	move_ret ref[435]
	c_ret

;; TERM 436: minus
c_code local build_ref_436
	ret_reg &ref[436]
	call_c   Dyam_Create_Atom("minus")
	move_ret ref[436]
	c_ret

;; TERM 225: guard
c_code local build_ref_225
	ret_reg &ref[225]
	call_c   Dyam_Create_Atom("guard")
	move_ret ref[225]
	c_ret

;; TERM 223: [no,yes,strict,atmostone,one,sync]
c_code local build_ref_223
	ret_reg &ref[223]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_442()
	call_c   Dyam_Create_List(&ref[442],I(0))
	move_ret R(0)
	call_c   build_ref_441()
	call_c   Dyam_Create_List(&ref[441],R(0))
	move_ret R(0)
	call_c   build_ref_440()
	call_c   Dyam_Create_List(&ref[440],R(0))
	move_ret R(0)
	call_c   build_ref_439()
	call_c   Dyam_Create_List(&ref[439],R(0))
	move_ret R(0)
	call_c   build_ref_438()
	call_c   Dyam_Create_List(&ref[438],R(0))
	move_ret R(0)
	call_c   build_ref_437()
	call_c   Dyam_Create_List(&ref[437],R(0))
	move_ret ref[223]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 437: no
c_code local build_ref_437
	ret_reg &ref[437]
	call_c   Dyam_Create_Atom("no")
	move_ret ref[437]
	c_ret

;; TERM 438: yes
c_code local build_ref_438
	ret_reg &ref[438]
	call_c   Dyam_Create_Atom("yes")
	move_ret ref[438]
	c_ret

;; TERM 439: strict
c_code local build_ref_439
	ret_reg &ref[439]
	call_c   Dyam_Create_Atom("strict")
	move_ret ref[439]
	c_ret

;; TERM 440: atmostone
c_code local build_ref_440
	ret_reg &ref[440]
	call_c   Dyam_Create_Atom("atmostone")
	move_ret ref[440]
	c_ret

;; TERM 441: one
c_code local build_ref_441
	ret_reg &ref[441]
	call_c   Dyam_Create_Atom("one")
	move_ret ref[441]
	c_ret

;; TERM 442: sync
c_code local build_ref_442
	ret_reg &ref[442]
	call_c   Dyam_Create_Atom("sync")
	move_ret ref[442]
	c_ret

;; TERM 221: [epsilon,true,no_epsilon]
c_code local build_ref_221
	ret_reg &ref[221]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_445()
	call_c   Dyam_Create_List(&ref[445],I(0))
	move_ret R(0)
	call_c   build_ref_444()
	call_c   Dyam_Create_List(&ref[444],R(0))
	move_ret R(0)
	call_c   build_ref_443()
	call_c   Dyam_Create_List(&ref[443],R(0))
	move_ret ref[221]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 443: epsilon
c_code local build_ref_443
	ret_reg &ref[443]
	call_c   Dyam_Create_Atom("epsilon")
	move_ret ref[443]
	c_ret

;; TERM 444: true
c_code local build_ref_444
	ret_reg &ref[444]
	call_c   Dyam_Create_Atom("true")
	move_ret ref[444]
	c_ret

;; TERM 445: no_epsilon
c_code local build_ref_445
	ret_reg &ref[445]
	call_c   Dyam_Create_Atom("no_epsilon")
	move_ret ref[445]
	c_ret

;; TERM 222: lctag_labels
c_code local build_ref_222
	ret_reg &ref[222]
	call_c   Dyam_Create_Atom("lctag_labels")
	move_ret ref[222]
	c_ret

;; TERM 1: parse_option(_A,_B)
c_code local build_ref_1
	ret_reg &ref[1]
	call_c   build_ref_311()
	call_c   Dyam_Term_Start(I(3),3)
	call_c   Dyam_Term_Arg(&ref[311])
	call_c   Dyam_Term_Arg(V(0))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_End()
	move_ret ref[1]
	c_ret

;; TERM 0: '*PROLOG-ITEM*'{top=> parse_option(_A, _B), cont=> _C}
c_code local build_ref_0
	ret_reg &ref[0]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_311()
	call_c   Dyam_Create_Binary(&ref[311],V(0),V(1))
	move_ret R(0)
	call_c   build_ref_312()
	call_c   Dyam_Create_Binary(&ref[312],R(0),V(2))
	move_ret ref[0]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 3: parse_options(_A,_B)
c_code local build_ref_3
	ret_reg &ref[3]
	call_c   build_ref_310()
	call_c   Dyam_Term_Start(I(3),3)
	call_c   Dyam_Term_Arg(&ref[310])
	call_c   Dyam_Term_Arg(V(0))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_End()
	move_ret ref[3]
	c_ret

;; TERM 2: '*PROLOG-ITEM*'{top=> parse_options(_A, _B), cont=> _C}
c_code local build_ref_2
	ret_reg &ref[2]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_310()
	call_c   Dyam_Create_Binary(&ref[310],V(0),V(1))
	move_ret R(0)
	call_c   build_ref_312()
	call_c   Dyam_Create_Binary(&ref[312],R(0),V(2))
	move_ret ref[2]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 10: stdprolog_install(_A)
c_code local build_ref_10
	ret_reg &ref[10]
	call_c   build_ref_358()
	call_c   Dyam_Create_Unary(&ref[358],V(0))
	move_ret ref[10]
	c_ret

;; TERM 11: '*RITEM*'(stdprolog_install(_A), voidret)
c_code local build_ref_11
	ret_reg &ref[11]
	call_c   build_ref_4()
	call_c   build_ref_10()
	call_c   build_ref_6()
	call_c   Dyam_Create_Binary(&ref[4],&ref[10],&ref[6])
	move_ret ref[11]
	c_ret

;; TERM 12: gen_application(_A)
c_code local build_ref_12
	ret_reg &ref[12]
	call_c   build_ref_356()
	call_c   Dyam_Create_Unary(&ref[356],V(0))
	move_ret ref[12]
	c_ret

;; TERM 13: '*RITEM*'(gen_application(_A), voidret)
c_code local build_ref_13
	ret_reg &ref[13]
	call_c   build_ref_4()
	call_c   build_ref_12()
	call_c   build_ref_6()
	call_c   Dyam_Create_Binary(&ref[4],&ref[12],&ref[6])
	move_ret ref[13]
	c_ret

;; TERM 78: [_B,_C]
c_code local build_ref_78
	ret_reg &ref[78]
	call_c   Dyam_Create_Tupple(1,2,I(0))
	move_ret ref[78]
	c_ret

;; TERM 77: '\t~w\t~w\n'
c_code local build_ref_77
	ret_reg &ref[77]
	call_c   Dyam_Create_Atom("\t~w\t~w\n")
	move_ret ref[77]
	c_ret

;; TERM 79: '\n----------------------------------------------\ndyalog generic options\n'
c_code local build_ref_79
	ret_reg &ref[79]
	call_c   Dyam_Create_Atom("\n----------------------------------------------\ndyalog generic options\n")
	move_ret ref[79]
	c_ret

;; TERM 75: 'options:\n'
c_code local build_ref_75
	ret_reg &ref[75]
	call_c   Dyam_Create_Atom("options:\n")
	move_ret ref[75]
	c_ret

;; TERM 74: main_file(_B, info(_C, _D, _E, _F))
c_code local build_ref_74
	ret_reg &ref[74]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_446()
	call_c   Dyam_Term_Start(&ref[446],4)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_313()
	call_c   Dyam_Create_Binary(&ref[313],V(1),R(0))
	move_ret ref[74]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 446: info
c_code local build_ref_446
	ret_reg &ref[446]
	call_c   Dyam_Create_Atom("info")
	move_ret ref[446]
	c_ret

;; TERM 73: main_file(_G, _H)
c_code local build_ref_73
	ret_reg &ref[73]
	call_c   build_ref_313()
	call_c   Dyam_Create_Binary(&ref[313],V(6),V(7))
	move_ret ref[73]
	c_ret

;; TERM 72: :
c_code local build_ref_72
	ret_reg &ref[72]
	call_c   Dyam_Create_Atom(":")
	move_ret ref[72]
	c_ret

;; TERM 55: toplevel
c_code local build_ref_55
	ret_reg &ref[55]
	call_c   Dyam_Create_Atom("toplevel")
	move_ret ref[55]
	c_ret

;; TERM 56: include
c_code local build_ref_56
	ret_reg &ref[56]
	call_c   Dyam_Create_Atom("include")
	move_ret ref[56]
	c_ret

;; TERM 53: [_B,_C,_D,_E]
c_code local build_ref_53
	ret_reg &ref[53]
	call_c   Dyam_Create_Tupple(1,4,I(0))
	move_ret ref[53]
	c_ret

;; TERM 52: '~w compiler version ~w\n~w <~w>\n'
c_code local build_ref_52
	ret_reg &ref[52]
	call_c   Dyam_Create_Atom("~w compiler version ~w\n~w <~w>\n")
	move_ret ref[52]
	c_ret

;; TERM 51: compiler_info{name=> _B, version=> _C, author=> _D, email=> _E}
c_code local build_ref_51
	ret_reg &ref[51]
	call_c   build_ref_447()
	call_c   Dyam_Term_Start(&ref[447],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[51]
	c_ret

;; TERM 447: compiler_info!'$ft'
c_code local build_ref_447
	ret_reg &ref[447]
	call_c   build_ref_245()
	call_c   build_ref_246()
	call_c   Dyam_Create_Atom_Module("$ft",&ref[245])
	move_ret ref[447]
	call_c   DYAM_Feature_2(&ref[245],&ref[246])
	c_ret

;; TERM 47: [_C,_D]
c_code local build_ref_47
	ret_reg &ref[47]
	call_c   Dyam_Create_Tupple(2,3,I(0))
	move_ret ref[47]
	c_ret

;; TERM 46: 'Not possible to open several TFS files (~w ~w)'
c_code local build_ref_46
	ret_reg &ref[46]
	call_c   Dyam_Create_Atom("Not possible to open several TFS files (~w ~w)")
	move_ret ref[46]
	c_ret

;; TERM 45: tfs_file(_D)
c_code local build_ref_45
	ret_reg &ref[45]
	call_c   build_ref_448()
	call_c   Dyam_Create_Unary(&ref[448],V(3))
	move_ret ref[45]
	c_ret

;; TERM 448: tfs_file
c_code local build_ref_448
	ret_reg &ref[448]
	call_c   Dyam_Create_Atom("tfs_file")
	move_ret ref[448]
	c_ret

;; TERM 48: tfs_file(_C)
c_code local build_ref_48
	ret_reg &ref[48]
	call_c   build_ref_448()
	call_c   Dyam_Create_Unary(&ref[448],V(2))
	move_ret ref[48]
	c_ret

;; TERM 50: [_B]
c_code local build_ref_50
	ret_reg &ref[50]
	call_c   Dyam_Create_List(V(1),I(0))
	move_ret ref[50]
	c_ret

;; TERM 49: 'Not a valid filename ~w'
c_code local build_ref_49
	ret_reg &ref[49]
	call_c   Dyam_Create_Atom("Not a valid filename ~w")
	move_ret ref[49]
	c_ret

pl_code local fun3
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Object(R(0))
	call_c   Dyam_Tabule()
	pl_ret

pl_code local fun4
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Choice(fun3)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Subsume(R(0))
	fail_ret
	call_c   Dyam_Cut()
	pl_ret

pl_code local fun53
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Object(R(0))
	call_c   Dyam_Tabule()
	pl_ret

pl_code local fun31
	call_c   Dyam_Backptr_Trace(R(1),R(2))
	call_c   Dyam_Object(R(0))
	call_c   Dyam_Tabule()
	call_c   Dyam_Now()
	pl_ret

pl_code local fun13
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Object(R(0))
	call_c   Dyam_Tabule()
	call_c   Dyam_Schedule()
	pl_ret

pl_code local fun22
	call_c   Dyam_Remove_Choice()
fun21:
	move     &ref[79], R(0)
	move     0, R(1)
	move     I(0), R(2)
	move     0, R(3)
	pl_call  pred_format_2()
	call_c   generic_usage()
	call_c   DYAM_Exit_1(N(1))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret


pl_code local fun11
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Reg_Load(0,V(1))
	move     &ref[56], R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_read_files_2()

pl_code local fun5
	call_c   Dyam_Remove_Choice()
	move     &ref[46], R(0)
	move     0, R(1)
	move     &ref[47], R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_error_2()

pl_code local fun6
	call_c   Dyam_Remove_Choice()
	call_c   DYAM_Tfs_Dlopen_1(V(2))
	fail_ret
	call_c   DYAM_evpred_assert_1(&ref[48])
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun8
	call_c   Dyam_Remove_Choice()
	move     &ref[49], R(0)
	move     0, R(1)
	move     &ref[50], R(2)
	move     S(5), R(3)
	pl_call  pred_error_2()
	pl_jump  fun7()


;;------------------ INIT POOL ------------

c_code local build_init_pool
	call_c   build_seed_8()
	call_c   build_seed_9()
	call_c   build_seed_10()
	call_c   build_seed_11()
	call_c   build_seed_12()
	call_c   build_seed_13()
	call_c   build_seed_14()
	call_c   build_seed_15()
	call_c   build_seed_16()
	call_c   build_seed_17()
	call_c   build_seed_18()
	call_c   build_seed_19()
	call_c   build_seed_20()
	call_c   build_seed_21()
	call_c   build_seed_22()
	call_c   build_seed_23()
	call_c   build_seed_24()
	call_c   build_seed_25()
	call_c   build_seed_7()
	call_c   build_seed_3()
	call_c   build_seed_2()
	call_c   build_seed_1()
	call_c   build_seed_5()
	call_c   build_seed_6()
	call_c   build_seed_4()
	call_c   build_seed_0()
	call_c   build_seed_50()
	call_c   build_seed_49()
	call_c   build_seed_48()
	call_c   build_seed_47()
	call_c   build_seed_42()
	call_c   build_seed_41()
	call_c   build_seed_40()
	call_c   build_seed_39()
	call_c   build_ref_268()
	call_c   build_ref_267()
	call_c   build_ref_266()
	call_c   build_ref_265()
	call_c   build_ref_264()
	call_c   build_ref_263()
	call_c   build_ref_262()
	call_c   build_ref_261()
	call_c   build_ref_260()
	call_c   build_ref_259()
	call_c   build_ref_258()
	call_c   build_ref_257()
	call_c   build_ref_256()
	call_c   build_ref_255()
	call_c   build_ref_254()
	call_c   build_ref_253()
	call_c   build_ref_252()
	call_c   build_ref_251()
	call_c   build_ref_250()
	call_c   build_ref_249()
	call_c   build_ref_248()
	call_c   build_ref_247()
	call_c   build_ref_246()
	call_c   build_ref_245()
	call_c   build_ref_244()
	call_c   build_ref_243()
	call_c   build_ref_242()
	call_c   build_ref_241()
	call_c   build_ref_240()
	call_c   build_ref_239()
	call_c   build_ref_238()
	call_c   build_ref_237()
	call_c   build_ref_236()
	call_c   build_ref_235()
	call_c   build_ref_234()
	call_c   build_ref_233()
	call_c   build_ref_232()
	call_c   build_ref_231()
	call_c   build_ref_230()
	call_c   build_ref_229()
	call_c   build_ref_228()
	call_c   build_ref_227()
	call_c   build_ref_226()
	call_c   build_ref_225()
	call_c   build_ref_223()
	call_c   build_ref_224()
	call_c   build_ref_221()
	call_c   build_ref_222()
	call_c   build_ref_1()
	call_c   build_ref_0()
	call_c   build_ref_3()
	call_c   build_ref_2()
	call_c   build_ref_5()
	call_c   build_ref_7()
	call_c   build_ref_8()
	call_c   build_ref_9()
	call_c   build_ref_10()
	call_c   build_ref_11()
	call_c   build_ref_12()
	call_c   build_ref_13()
	call_c   build_ref_78()
	call_c   build_ref_77()
	call_c   build_ref_76()
	call_c   build_ref_79()
	call_c   build_ref_75()
	call_c   build_ref_74()
	call_c   build_ref_73()
	call_c   build_ref_72()
	call_c   build_ref_55()
	call_c   build_ref_54()
	call_c   build_ref_56()
	call_c   build_ref_53()
	call_c   build_ref_52()
	call_c   build_ref_51()
	call_c   build_ref_47()
	call_c   build_ref_46()
	call_c   build_ref_45()
	call_c   build_ref_48()
	call_c   build_ref_50()
	call_c   build_ref_49()
	c_ret

long local ref[449]
long local seed[54]

c_code global main_initialization
	call_c   initialization_dyalog_tag_5Ffeatures()
	call_c   initialization_dyalog_tagguide_5Fcompile()
	call_c   initialization_dyalog_toplevel()
	call_c   initialization_dyalog_tag_5Fdyn()
	call_c   initialization_dyalog_tag_5Fcompile()
	call_c   initialization_dyalog_tag_5Fmaker()
	call_c   initialization_dyalog_tag_5Fnormalize()
	call_c   initialization_dyalog_tig()
	call_c   initialization_dyalog_lc()
	call_c   initialization_dyalog_foreign()
	call_c   initialization_dyalog_stdprolog()
	call_c   initialization_dyalog_emit()
	call_c   initialization_dyalog_ma_5Femit()
	call_c   initialization_dyalog_code_5Femit()
	call_c   initialization_dyalog_code()
	call_c   initialization_dyalog_term()
	call_c   initialization_dyalog_application()
	call_c   initialization_dyalog_seed()
	call_c   initialization_dyalog_rcg()
	call_c   initialization_dyalog_dcg()
	call_c   initialization_dyalog_bmg()
	call_c   initialization_dyalog_lpda()
	call_c   initialization_dyalog_reader()
	call_c   initialization_dyalog_maker()
	call_c   initialization_dyalog_oset()
	call_c   initialization_dyalog_tools()
	call_c   initialization_dyalog_config()
	call_c   initialization_dyalog_format()
	call_c   build_viewers()
	call_c   load()
	c_ret

