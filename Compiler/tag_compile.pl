/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 1998, 2002, 2003, 2004, 2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2016, 2019, 2020, 2021 by INRIA 
 * Authors:
 *      Djame Seddah <Djame.Seddah@inria.fr>
 *      Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  tag_compile.pl -- TAG compiler for DyALog Compiler
 *
 * ----------------------------------------------------------------
 * Description
 *  Compile normalized TAGs into SD-2SA Meta Transitions
 *  (transitions are grouped by pairs Call/Return and continuations
 *   used in place of nabla predicates)
 *
 *  Substitution trees and substitution nodes are handled with PDA transitions
 *  (FIRST, LAST and NEXT instead of SA-FIRST, SA-LAST, SA-NEXT)
 * ----------------------------------------------------------------
 */

:-include 'header.pl'.
:-require 'tag_normalize.pl'.
:-require 'tag_maker.pl'.
:-require 'reader.pl'.

:-rec_prolog tag_compile_tree/6, tag_compile_subtree/9.

:-rec_prolog
	tag_compile_subtree_noadj/9,
	tag_compile_subtree_adj/12,
	tag_compile_subtree_potential_adj/12.
:-extensional parse_mode/1.

:-light_tabular tagguide_pred/2.
:-mode(tagguide_pred/2,+(+,-)).

:-light_tabular tagguide_name/3.
:-mode(tagguide_name/3,+(+,+,-)).

:-extensional tag_corrections/0.

:-extensional adjrestr/3.

:-light_tabular verbose_tag/1.

verbose_tag(Id) :-
	recorded( verbose_tag ),
	\+ recorded( guide(Id,_,_) )
	.

:-light_tabular core_info/3.
:-mode(core_info/3,+(+,+,-)).

core_info(Name,Info::tag_anchor{},Info).
core_info(Name,(XInfo * _),Info) :- core_info(Name,XInfo,Info).
%%core_info(Info::strip(_),Info).
core_info(Name,strip(XInfo),Info) :- core_info(Name,XInfo,Info).

%% Imported from tig.pl
:-std_prolog make_tig_callret/7.

%pgm_to_lpda( register_predicate(Pred::tag(F/N)), '*PROLOG-FIRST*'(Call) :> Cont ) :-
pgm_to_lpda( register_predicate(Pred::tag(F)), '*PROLOG-FIRST*'(Call) :> Cont ) :-
	(   recorded( public(Pred) ) xor recorded( public(tag('*default*')) ) ),
	(   recorded( tag_features(F,Top,Bot) )
	xor %functor(Top,F,N),
	    %functor(Bot,F,N)
	    F=Top,F=Bot
	),
	Node = tag_node{ label => F,
			 top => Top,
			 bot => Bot,
			 adj => no,
			 kind => subst,
			 children => []
		       },
	tag_numbervars([Node,L,R,Info],Id,_),
	(   Body = Node,
	    Type=prolog
	;   
	    Body = '$answers'(Node),
	    Type=prolog
	;
	    Body = '$protect'(Node),
	    Type = protect(prolog)
	),
	Call = '$tagcall'(Body,L,R,Info),
	tupple(Call,Seen),
	tag_compile_subtree(Id,Body,L,R,'*PROLOG-LAST*',Cont,Type,Info,Seen)
	.
	
pgm_to_lpda( T::tag_tree{ name=> Name, family=> Family  }, Trans ) :-
%%	format('norm ~w ->\n',[T]),
	normalize(T,L),
%%	format('\t~w\n\t~w\n',[T,L]),
	(   recorded( Info::tag_anchor{ family => Family } )
	xor Info = tag_anchor{ family => Family }
	),
	tag_numbervars([Left,Right],Name,_),
	tag_compile_tree(Name,L,Trans1,dyalog,Info,[Left,Right]),
	tag_numbervars(Info,Name,_),
	install_loader(Name,
		       Loader^tag_autoloader_wrapper(Name,L,Info,Loader),
		       Trans1,
		       Trans),
%%	format('Autoload ~w ~w\n',[Name,Loader]),
%%	format('TAG Tree ~w =>\n\t ~w\n\n',[T,Trans1]),
	true
	.

tag_compile_tree(Id,Tree,Trans,Type,Info,[Left,Right]) :-
	%%	format('Tree Info ~w ~w\n',[Tree,Info]),
    (   Tree = Root::tag_node{ label=> NT, top=> A, spine => Spine },
	Guard = true
		    xor Tree = guard{ goal => Root, plus => Guard },
%	    format('%% try tree with guard ~w\n',[Id]),
	    true
	),
	%tree_info(Tree,Info),
	tree_info(Tree,Info),
	%%	format('\t--> ~w ~w\n',[Tree,Info]),
	tag_numbervars(Left,Id,_),
	tag_numbervars(Right,Id,_),
	functor(A,F,N),
%	register_predicate(tag(F/N)),
	register_predicate(tag(NT)),
	( Spine == yes  ->
	    \+ recorded( tig(Id,_) ), % handling of TIG tree delegated to tig.pl
	    make_tag_top_callret(A,Left,Right,Call,Return),
	    Trans = '*SA-AUX-FIRST*'(Call) :> Cont,
	    Last1 = '*SA-AUX-LAST*'(Return),
	    Mode=wrap
	;   
	  make_tag_subst_callret(A,Left,Right,Call,Return),
	  Trans = '*SAFIRST*'(Call) :> Cont,
	  ( light_tabular(tag(F/N)) ->
	    Last1 = '*LIGHTLAST*'(Return)
	  ;
	    Last1 = '*LAST*'(Return)
	  ),
	  Mode = subst
	),
	tag_numbervars(Tree,Id,_),
	tupple(Call,_Seen),
	( verbose_tag(Id) ->
	  core_info(Id,Info,tag_anchor{ family => HyperTag }),
	  tag_numbervars(HyperTag,Id,_),
	  body_to_lpda(Id,verbose!struct(Id,HyperTag),Last1,Last2,Type),
	  extend_tupple(HyperTag,_Seen,Seen)
	;   
	Last2 = Last1,
	Seen = _Seen
	),
	body_to_lpda(Id,Guard,Last2,Last3,Type),
	( fail,
	  tag_extract_anchor_guards(Tree,AnchorGuards) ->
	     % format('found anchor guard ~w => ~w\n',[Id,AnchorGuards]),
	      %	     body_to_lpda(Id,(format('check final guard ~w\n',[Id]),AnchorGuards),Last2,Last,Type),
	      % body_to_lpda(Id,format('check final guard ~w\n',[Id]),Last2,Last,Type),
	      body_to_lpda(Id,(Id=Id),Last3,Last,Type),
	      %	     record_without_doublon(tag_extracted_anchor_guard(Id)),
	     % Last = Last2,
	     true
	 ;
	 Last = Last3
	),
%	record_without_doublon(tag_compile_first_left(Id,Left,Info)),
	(Spine == yes,
	 Root = tag_node{ adj => yes, top => Top, bot => Bot } ->
	 %% avoid left and right adj on root node of wrappind aux trees
	 %% it is always possible to do the tig adj on the adjoined node
	 tag_numbervars(AdjDone,Id,_),
	 tag_compile_subtree_adj(Id,Root,Left,Right,Last,Cont2,Type,Info,Top,Bot,Seen,AdjDone)
	;
	 tag_compile_subtree(Id,Root,Left,Right,Last,Cont2,Type,Info,Seen)
	),
	( guide(strip) ->
	    tagguide_pred(NT,SNT),
	    ( Spine == yes ->
		%% we know that this is not a TIG tree
		tagguide_name(Id,left,Left_Id),
		tag_numbervars(Foot_Left,Id,_),
		make_tig_callret(left,SNT,SNT,Left,Foot_Left,Left_Call,_),
		%% We check left guide at start
		%% right guide used after foot
		Cont3 = '*GUIDE*'(Left_Call,Left_Id,Cont2)
	    ;	
		tagguide_name(Id,no,GId),
		make_tag_subst_callret(SNT,Left,Right,GCall,_),
		Cont3 = '*GUIDE*'(GCall,GId,Cont2)
	    )
	;   recorded(debug) ->
	  body_to_lpda(Id,format('Selecting cat=~w left=~w tree ~w\n',[A,Left,Id]),Cont2,Cont3,Type)
	;
	    Cont3 = Cont2
	),
	( recorded( tagfilter(Id^Left^Right^NT^A^TagFilter) ),
	  recorded( lctag(Id,_,_,_) )
	  ->
	  tag_numbervars(TagFilter,Id,_),
	  body_to_lpda(Id, TagFilter, Cont3,Cont,Type)
	;
	  Cont=Cont3
	),
%	(Tree = guard{} -> format('TAG Tree ~w =>\n\t ~w\n',[Tree,Trans]) ; true),
	true
	.

tag_compile_subtree(Id,T,Left,Right,Cont,Trans,Type,Info,Seen):-
%%	format('TAG Subtree ~w\n',[T]),
	(   tag_compile_subtree_handler(Id,T,Left,Right,Cont,Trans,Type,Info,Seen) ->
	    true
	%% Last children in a children list
	;   T = [A] ->
	    tag_compile_subtree(Id,A,Left,Right,Cont,Trans,Type,Info,Seen)
	%% Traversing a children list
	;   T = [A|B] ->
	    tag_numbervars(Middle,Id,_),
	    extend_tupple(A,Seen,Seen2),
	    tag_compile_subtree(Id,B,Middle,Right,Cont,ContB,Type,Info,Seen2),
	    tag_compile_subtree(Id,A,Left,Middle,ContB,Trans,Type,Info,Seen)
	;   tag_compile_node(Id,T,Left,Right,Cont,Trans,Type,Info,Seen)
	),
%%	format('===> TAG Subtree ~w Trans=~w\n',[T,Trans]),
	true
	.

%%! @predicate tag_compile_subtree_handler(Id,T,Left,Right,Cont,Type,Info)
%%! @brief to express handlers for all cases of TAG "nodes"

:-rec_prolog tag_compile_subtree_handler/9.

%%! @case Disjunction
tag_compile_subtree_handler(Id,(A;B),Left,Right,Cont,Trans,Type,Info,Seen) :-
	handle_choice(Cont,Trans,Last,ContA,ContB),
	tag_compile_subtree(Id,A,Left,Right,Last,ContA,Type,Info,Seen),
	tag_compile_subtree(Id,B,Left,Right,Last,ContB,Type,Info,Seen),
	true
	.

%%! @case Empty Scan node
tag_compile_subtree_handler(Id,
			    tag_node{ label => [], kind => scan },
			    Left,Right,Cont,unify(Left,Right) :> Cont,Type,Info,_).

%%! @case Scan node
tag_compile_subtree_handler(Id,
			    tag_node{ label => L::[_|_], kind => scan, id => Node },
			    Left,Right,Cont,Trans,Type,Info,_) :-
	( recorded( skipper(_,_,_,_) ) ->
	  tag_numbervars([_Left],Id,_)
	;
	  _Left = Left
	),
	( verbose_tag(Id)  ->
	    tag_numbervars([Cat,Lex],Id,_),
	    body_to_lpda(Id,verbose!lexical(L,_Left,Right,Cat,Lex),Cont,Cont2,Type)
	;   
	    Cont2=Cont
	),
%%	format('Try Lex compile ~w in ~w\n',[L,Id]),
	scan_to_lpda(Id,L,_Left,Right,Cont2,Trans1,Type),
%%	format('Done Lex compile ~w in ~w\n',[L,Id]),
	(   core_info(Id,Info,CInfo),
	    CInfo = tag_anchor{ family => Family, coanchors => VarCoanchors },
	    recorded( tag_coanchor(Family,Node,LemmaVar, VarCoanchors) ) ->
	    tag_numbervars(LemmaVar,Id,_),
	    body_to_lpda(Id,check_coanchor(LemmaVar,_Left,Right),Trans1,Trans2,Type)
	;   
	    Trans2=Trans1
	),
	tag_wrap_skipper(Id,Left,_Left,Right,true,Trans2,Trans,Type),
	true
	.

%%! @case Escape node
tag_compile_subtree_handler(Id,
			    tag_node{ label => A, kind => escape },
			    Left,Right,Cont,unify(Left,Right) :> Trans,Type,Info,_) :-
	( A = noop(_) ->
	  Cont = Trans
	;
	  body_to_lpda(Id,A,Cont,Trans,Type)
	)
	.

%%! @case Position node
tag_compile_subtree_handler(Id,
			    '$pos'(Pos),
			    Left,Right,Cont,
			    unify(Pos,Left) :> unify(Left,Right) :> Cont,
			    Type,Info,_
			   )
.

%%! @case Skip node
tag_compile_subtree_handler(Id,
			    '$skip',
			    Left,Right,Cont,Trans,
			    Type,Info,_
			   ) :-
	tag_wrap_skipper(Id,Left,Right,Right,true,Cont,Trans,Type)
	.

%%! @case Answer
tag_compile_subtree_handler(Id,'$answers'(A),Left,Right,Cont,Trans,Type,Info,Seen) :-
	tag_compile_subtree(Id,A,Left,Right,Cont,Trans,answer(Type),Info,Seen)
	.

%%! @case Protect
tag_compile_subtree_handler(Id,'$protect'(A),Left,Right,Cont,Trans,Type,Info,Seen) :-
	tag_compile_subtree(Id,A,Left,Right,Cont,Trans,protect(Type),Info,Seen)
	.


%%! @case MetaCall
tag_compile_subtree_handler(Id,
			    tag_node{ id=>NId,
				      label => '$call'(A),
				      kind => Kind,
				      top => Top,
				      bot => Bot,
				      adj => Adj,
				      children => Children,
				      spine => Spine,
				      token => Token,
				      adjleft => AdjLeft,
				      adjwrap => AdjWrap,
				      adjright =>  AdjRight
				    },
			    Left,Right,Cont,Trans,Type,Info,Seen) :-
	metacall_initialize,
	Node = tag_node{ id=>NId,
			 label => A,
			 kind => Kind,
			 top => Top,
			 bot => Bot,
			 adj => Adj,
			 children => Children,
			 spine => Spine,
			 token => Token,
			 adjleft => AdjLeft,
			 adjright =>  AdjRight,
			 adjwrap => AdjWrap
		       },
	( Type = answer(Type2) ->
	    body_to_lpda(Id,call!tagcall('$answers'(Node),Left,Right,Info),Cont,Trans,Type2)
	;  Type = protect(Type2) -> 
	    body_to_lpda(Id,call!tagcall('$protect'(Node),Left,Right,Info),Cont,Trans,Type2)
	;   
	    body_to_lpda(Id,call!tagcall(Node,Left,Right,Info),Cont,Trans,Type)
	)
	.

%%! @case Plus-component of a Guarded Node

tag_compile_subtree_handler(Id,
			    '$plusguard'(A,Plus),
			    Left,Right,Cont,Trans,Type,Info,Seen
			   ) :-
    ( A = tag_node{ kind => Kind,
		    label => ALabel,
		    top => ATop,
		    id => NId,
		    spine => Spine
		  }
	xor NId = [], Kind = not_a_node, Spine = no
	),
	(
	    Kind = anchor,
	    recorded(tag_extracted_anchor_guard(Id)) ->
		%% some anchor guard are handled at the root of the tree
		Trans1 =Cont,
		% ApproxPlus = true,
		guard_approx(Plus,ApproxPlus,0,Seen,Kind),
		true
	 ;
	 %% we try to avoid making early choices
	 %% due to disjunctions in positive guards
	 atom(NId),
	 domain(Kind,[anchor,coanchor,lexical,subst,std]),
	 %% for spine node, the guard is always tried before
	 Spine \== yes
	->
	    %% we compute an approximation of the guard to be tried before the node
	    (Kind == subst ->
		 make_tag_subst_callret(ATop,Left,Right,ACall,_),
		 extend_tupple(ACall,Seen,XSeen),
		 true
	     ;
	     XSeen = Seen
	    ),
	    guard_approx(Plus,ApproxPlus,0,XSeen,Kind),
	    %% and test the true guard after the node
	    guard_post_handler(Id,Plus,Cont,Trans1,Type)
	;
	  %% no prior approximation
	  Trans1 = Cont,
	  ApproxPlus = Plus
	),
	extend_tupple(ApproxPlus,Seen,Seen2),
	tag_compile_subtree(Id,A,Left,Right,Trans1,Trans2,Type,Info,Seen2),
	%% a node may have several names
	%% actually, selecting a name should be done at node level (not a guard level)
	%% but a mult-name node is normally associated with a guard
	( atom(NId) ->
	  Trans3 = Trans2
	;
	  body_to_lpda(Id,domain(NId,NId),Trans2,Trans3,Type)
	),
	%% compiling the prior approximation
	body_to_lpda(Id,ApproxPlus,Trans3,Trans4,Type),
	( guard_depth(ApproxPlus,0) ->
	  Trans = Trans4
	;
	  tupple(ApproxPlus,TApproxPlus),
	  Trans = '*NOOP*'(TApproxPlus) :> Trans4
	)
	.


%%! @case Guarded Node
tag_compile_subtree_handler(Id,
			    guard{ goal => A,
				   plus => Plus,
				   minus => Minus
				 },
			    Left, Right, Cont, Trans, Type, Info,Seen) :-
	( Plus == true, Minus = fail ->
	  %% actually an unguarded node !
	  tag_compile_subtree(Id,A,Left,Right,Cont,Trans,Type,Info,Seen)
	; Plus == fail, Minus == fail ->
	  %% this case should not arise !
	  body_to_lpda(Id,fail,Cont,Trans,Type)
	; Plus == fail ->
	  body_to_lpda(Id,Minus,unify(Left,Right) :> Cont,Trans,Type)
	; Plus = '$postguard'(PostPlus) ->
	  %% a "post" plus guard (set by interleave: a pre approx guard has been already tried)
	  guard_post_handler(Id,PostPlus,Cont,Trans1,Type),
	  tag_compile_subtree(Id,A,Left,Right,Trans1,Trans,Type,Info,Seen)
	; Minus == fail ->
	  %% no negative guard
	  tag_compile_subtree(Id,'$plusguard'(A,Plus),Left,Right,Cont,Trans,Type,Info,Seen)
	;
	  handle_choice(Cont,Trans,Last,ContA,ContB),
	  body_to_lpda(Id,Minus,unify(Left,Right) :> Last,_ContB,Type),
	  tupple(Minus,TMinus),
	  ContB = '*NOOP*'(TMinus) :> _ContB,
	  tag_compile_subtree(Id,'$plusguard'(A,Plus),Left,Right,Last,ContA,Type,Info,Seen),
	  true
	)
	.

:-light_tabular format_require.

format_require :-
	read_files('format.pl',require)
	.

:-std_prolog guard_post_handler/5.

guard_post_handler(Id,G,Cont,Trans,Type) :-
	( G = (G1,G2) ->
	  guard_post_handler(Id,G2,Cont,Trans1,Type),
	  guard_post_handler(Id,G1,Trans1,Trans,Type)
	; G = (G1;G2) ->
	  body_to_lpda(Id,G,Cont,_Trans,Type),
	  tupple(G,TG),
	  Trans = '*NOOP*'(TG) :> _Trans
	;
	  %% no constraint to check at post time
	  %% because checked at ante time
	  Cont=Trans
	).

:-std_prolog double_neg_handler/6.

double_neg_handler(Id,G,Cont,Trans,Type,Depth) :-
	%%	format('Negation ~w\n',[G]),
	( G = (G1,G2) ->
	  double_neg_handler(Id,G2,Cont,Trans1,Type,Depth),
	  double_neg_handler(Id,G1,Trans1,Trans,Type,Depth)
	; G = (G1;G2) ->
	  ( Depth < 0 ->
	    NewDepth is Depth+1,
	    handle_choice(Cont,Trans,Last,Cont1,Cont2),
	    double_neg_handler(Id,G1,Last,Cont1,Type,NewDepth),
	    double_neg_handler(Id,G2,Last,Cont2,Type,NewDepth)
	  ;
	    Cont=Trans
	  )
	; G = true ->
	  Cont=Trans
	; G=fail ->
	  Cont=Trans
	;
	  body_to_lpda(Id,\+ (\+ G),Cont,Trans,Type)
	)
	.

:-std_prolog guard_approx/5.

guard_approx(G,XG,Depth,Seen,Kind) :-
	( var(G) ->
	  format('warning: guard is a var ~w\n',[G]),
	  XG=G
	;
	  G = (G1,G2) ->
	  guard_approx(G2,XG2,Depth,Seen,Kind),
	  guard_approx(G1,XG1,Depth,Seen,Kind),
	  ( XG2=fail ->
	    XG = fail
	  ; XG2=true ->
	    XG=XG1
	  ; XG1=fail ->
	    XG=fail
	  ; XG1=true ->
	    XG=XG2
	  ; 
	    XG=(XG1,XG2)
	  )
	; G = (G1;G2) ->
	  ( Depth < 2 ->
	    NewDepth is Depth+1,
	    guard_approx(G1,XG1,NewDepth,Seen,Kind),
	    guard_approx(G2,XG2,NewDepth,Seen,Kind),
	    ( XG1 = true ->
	      XG = true
	    ; XG2 = true ->
	      XG = true
	    ; XG1 = fail ->
	      XG = XG2
	    ; XG2 = fail ->
	      XG = XG1
	    ; (Depth > 0 xor Kind == subst),
	      true
	      ->
	      XG = (XG1 ; XG2)
	    ; guard_length(XG1,L1),
	      guard_length(XG2,L2),
	      L1 < L2 ->
	      XG = ((\+ \+ XG1) xor (\+ \+ XG2))
%	      XG = (XG1 ; XG2)
	    ;
	      XG = ((\+ \+ XG2) xor (\+ \+ XG1))
%	      XG = (XG2 ; XG1)
	    )
	  ;
	    XG=true
	  )
	; G = true ->
	  XG=G
	; G = fail ->
	  XG=G
	; Depth == 0 ->
	  XG=G
	; (G=(V1=V2) xor G=(V1==V2)),
	  ( ( local_var_in_tupple(V1,Seen),
	      local_var_in_tupple(V2,Seen))
	  xor (local_var_in_tupple(V1,Seen),
	       \+ local_var(V2,_)
	      )
	  xor (local_var_in_tupple(V2,Seen),
	       \+ local_var(V1,_)
	      )
	  )
	->
	  XG = G
	;
	  XG = true
	)
	.


:-std_prolog autoload_guard_approx/2.

autoload_guard_approx(G,XG) :-
	( var(G) ->
	  format('warning: guard is a var ~w\n',[G]),
	  XG=G
	;
	  G = (G1,G2) ->
	  autoload_guard_approx(G2,XG2),
	  autoload_guard_approx(G1,XG1),
	  ( XG2=fail ->
	    XG = fail
	  ; XG2=true ->
	    XG=XG1
	  ; XG1=fail ->
	    XG=fail
	  ; XG1=true ->
	    XG=XG2
	  ; 
	    XG=(XG1,XG2)
	  )
	; G = (G1;G2) ->
	  XG = true
	;
	  XG = G
	)
	.


:-std_prolog guard_depth/2.

guard_depth(G,D) :-
	( G=(G1,G2) ->
	  guard_depth(G1,D1),
	  guard_depth(G2,D2),
	  D is max(D1,D2)
	; G=(G1;G2) ->
	  guard_depth(G1,D1),
	  guard_depth(G2,D2),
	  D is 1+max(D1,D2)
	; G = (G1 xor G2) ->
	  guard_depth(G1,D1),
	  guard_depth(G2,D2),
	  D is 1+max(D1,D2)
	; G = (\+ \+ G1) ->
	  guard_depth(G1,D)
	;  D=0
	)
	.

:-std_prolog guard_length/2.

guard_length(G,L) :-
	( G = (G1,G2) ->
	  guard_length(G2,L2),
	  L is L2+1
	; G = (G1;G2) ->
	  guard_length(G2,L2),
	  guard_length(G1,L1),
	  L is L2+L1+1
	;
	  L=1
	)
	.

:-extensional local_strict_var/2.

local_strict_var('$VAR'(I),I).

:-extensional local_var/2.

local_var('$VAR'(I),I).
local_var('$VAR'('$VAR'(I),_),I).

:-std_prolog local_var_in_tupple/2.

local_var_in_tupple(V,'$TUPPLE'(T)) :-
	local_var(V,I),
	'$interface'( 'oset_member'(I:int,T:ptr),[]).

%%! @case Kleene star
%% only on non spine nodes
tag_compile_subtree_handler(Id,
			    KleeneStruct:: @*{ goal => A,
					       vars => Vars,
					       from => From,
					       to => To,
					       collect_first=>UserFirst,
					       collect_loop=>UserParam,
					       collect_next=>UserArg,
					       collect_last=>UserLast
					     },			    
			    Left,Right,Cont,Trans,Type,Info,Seen) :-
%%	format('Compiling TAG Kleene\n',[A]),
	update_counter(kleene, KleeneId),
        name_builder('_kleene~w',[KleeneId],KleeneLabel),
	kleene_conditions(KleeneCond::[From,To],[Counter,CounterInc],InitCond,ExitCond,LoopCond),
	tag_numbervars([Left2,Middle,Right],Id,_),
	tag_numbervars([Counter,CounterInc,InitCond,ExitCond,LoopCond,KleeneStruct],Id,_),
	Args1 = [Counter,Left2],
	Loop_Args1 = [CounterInc,Middle],
	append(Args1,UserParam,Args),
	append(Loop_Args1,UserArg,Loop_Args),
	tupple(Vars,TA),
	tupple(Cont,TCont),
	extend_tupple([Right,UserLast,KleeneCond],TCont,RTCont),
	body_to_lpda( Id,
		      (	  Left2=Right, ExitCond, UserLast = UserParam),
		      Cont,
		      ContExit,
		      Type
		    ),
	tag_compile_subtree(Id,A,
			    Left2,Middle,
			    '*KLEENE-LAST*'(KleeneLabel,
					    Loop_Args,
					    Args,
					    TA,
					    RTCont,
					    noop
					   ),
			    ContLoop2,
			    Type,
			    Info,
			    Seen),
	body_to_lpda(Id,LoopCond,ContLoop2,ContLoop,Type),
	Trans1 = '*KLEENE*'(KleeneLabel,
			    TA,
			    '*KLEENE-ALTERNATIVE*'(ContExit,ContLoop)
			    ),
	body_to_lpda(Id,(InitCond,Left=Left2,UserFirst=UserParam),
		     Trans1,
		     Trans,
		     Type),
%%	format('--> Kleene Trans ~w\n',[Trans]),
	true
	.

:-rec_prolog tag_compile_node/9.

tag_compile_node(Id,
                 T::tag_node{ label => NT, id => NId },
		 Left,
		 Right,
		 Cont,
		 Trans,
		 Type,
		 Info,
		 Seen
		) :-
%	format('Compile node ~w ~w ~w\n',[Id,NId,NT]),
	(   %% Foot node
	    T = tag_node{ label => NT, bot => A, kind => foot } ->
	    \+ recorded( tig(Id,_) ), % special TIG case for foot
	    tag_numbervars(A,Id,_),
	    functor(A,F,N),
	    Pred = tag_foot(F/N),
	    wrapping_predicate(Pred),
	    decompose_args(Pred,[Left,Right,A],Args),
	    Trans = '*SA-FOOT-WRAPPER-CALL*'(Pred,Args,Cont2),
	    ( fail, guide(strip) ->
		tagguide_pred(NT,SNT),
		tagguide_name(Id,right,GId),
		tag_numbervars(_Right,Id,_),
		make_tig_callret(right,SNT,SNT,Right,_Right,GCall,_),
		Cont2 = '*GUIDE*'(GCall,GId,Cont)
	    ;
		Cont2 = Cont
	    )
	%% SubstFoot Node (same as subst but keeps spine)
	;   T = tag_node{ bot => A, kind => substfoot } ->
	    tag_numbervars(A,Id,_),
	    make_tag_bot_callret(A,Left,Right,Call,Return),
	    Trans = '*SA-SUBSTFOOT*'(Call,Return,Cont)
	%% Node without adjonction
	;   T = tag_node{ adj => no } ->
	    tag_compile_subtree_noadj(Id,T,Left,Right,Cont,Trans,Type,Info,Seen)
	%% Node with full adjonction
	%%	;   allowed_wrap_adj(Id,T) ->
	%%    tag_compile_subtree_adj(Id,T,Left,Right,Cont,Trans,Type,Info)
        %% Node with potential tig adj but no wrapped adj (try left first)
	; recorded(tag_tree_cutter(NT)),
	  T = tag_node{ top => Top, spine => no, children => [_|_] } ->
	      %% a tree cutter works as cutting the subtree to build a new tree
	      update_counter(cutter,CutterId),
	      name_builder('_cutter~w',[CutterId],CutterLabel),
	      tupple(T,CutTupple),
              inter(CutTupple,Seen,EnterTupple),
              tupple(Cont,ContTupple),
              inter(CutTupple,ContTupple,ExitTupple),
	      CutCont = '*SA-CUT-LAST*'(CutterLabel,Right,ExitTupple),
	      (recorded(tree_cut(T,_Id)) ->
		   format('found similar cut tree for ~w and ~w\n',[Id,_Id])
	       ;
	       record(tree_cut(T,Id))
	      ),
	      tig_compile_subtree_left(Id,T,Left,Right,CutCont,CutTrans,Type,Info,Top,Seen),
	      Trans = '*SA-CUTTER*'(CutterLabel,Left,EnterTupple,CutTrans,CutCont,Cont),
	      true
	;   T = tag_node{ top=>Top },
	    tig_compile_subtree_left(Id,T,Left,Right,Cont,Trans,Type,Info,Top,Seen),
	    true
	),
%	format('~w ~w ~w => ~w\n',[Id,NId,NT,Trans]),
	true
	.


:-rec_prolog tig_compile_subtree_left/10.

:-std_prolog allowed_wrap_adj/2.

allowed_wrap_adj(Id,T::tag_node{ spine => Spine, bot => A, adjwrap => AdjWrap }) :-
%%	format('Check allowed adj ~w ~w\n',[Id,T]),
	\+ ( Spine = yes, recorded(tig(Id,_))),
	functor(A,F,N),
	%% WARNING:
	%% As soon as some tig information is provided for some predicate
	%% we assume that we are using TIG
	%% => adj on F/N is possible if and only if adjkind(F/N,wrap) provided !
	(   recorded(adjkind(F/N,wrap)), AdjWrap = possible_adj[]
	xor
	(   \+ recorded(adjkind(F/N,_)),
	    \+ recorded(adjkind(_,no))
	)
	),
%%	format('--> Allowed adj\n',[]),
	true
	.

decompose_args( tag_foot(F/N), L::[Left,Right,T], Args ) :-
	functor(T,F,N),
	( N > 30 -> Args = L ; T =.. [_|ArgsT], Args = [Left,Right|ArgsT] )
	.

wrapping_predicate( Pred::tag_foot(F/N) ) :-
	decompose_args(Pred,[Left,Right,A],Args1),
	make_tag_bot_callret(A,Left,Right,Call,Return),
	tag_numbervars(Args::[Closure|Args1],Id,_),
	record( '*SA-FOOT-WRAPPER*'(Pred,
				    Id,
				    Args,
				    '*SA-FOOT*'(Call,Return,'*SA-PROLOG-LAST*')
				   )
	      )
	.

:-std_prolog tag_compile_info_anchor/2.

tag_compile_info_anchor( Info::tag_anchor{ family => Family,
					   equations => VarEq
					 },
			 T::tag_node{ id => Node, bot=>Bot, top=>Top }
		       ) :-
	(   recorded( tag_equation(Family,Node,Top,Bot, VarEq ) ) xor true )
	.

tag_compile_subtree_potential_adj(Id,
				  T::tag_node{ top => TrueTop,
					       bot => TrueBot,
					       label => L,
					       id => A_Id
					     },
				  Left,
				  Right,
				  Cont,
				  Trans,
				  Type,
				  Info,
				  Top,
				  Bot,
				  Seen,
				  AdjDone
				 ) :-
%	format('Try Potential Adj ~w ~w\n',[A_Id,Id]),		
	(   allowed_wrap_adj(Id,T) ->
%%	    format('Potential HERE1\n',[]),
	    tag_compile_subtree_adj(Id,T,Left,Right,Cont,Trans,Type,Info,Top,Bot,Seen,AdjDone)
	;
%%	    format('Potential HERE2\n',[]),
	    ( Top == TrueTop, Bot==TrueBot ->
	      tag_numbervars(Top,Id,_),
	      tag_numbervars(Bot,Id,_),
%%            Trans = unify(Top,Bot) :> Trans1
              destructure_unify(Top,Bot,Trans1,Trans)
	    ; Top=Bot ->
	      tag_numbervars(Top,Id,_),
	      Trans = Trans1
	    ;
	      tag_numbervars(Top,Id,_),
	      tag_numbervars(Bot,Id,_),
	      %%            Trans = unify(Top,Bot) :> Trans1
              destructure_unify(Top,Bot,Trans1,Trans)
	    ),
%%	    format('Potential HERE3\n',[]),
	    tag_compile_subtree_noadj(Id,T,Left,Right,Cont,Trans1,Type,Info,Seen)
	),
%	format('Trans ~w ~w pos=~w: ~w\n',[A_Id,Id,L,Trans]),
	true
	.

tag_compile_subtree_adj(Id,
			T::tag_node{ label => NT,
				     top => TrueTop,
				     bot=> TrueBot,
				     adj=>Adj,
				     id=>A_Id ,
				     spine=>Spine},
			Left,
			Right,
			Cont,
			Trans,
			Type,
			Info,
			Top,
			Bot,
			Seen,
			AdjDone
		       ) :-
	tag_numbervars(Foot_Left,Id,_),
	tag_numbervars(Foot_Right,Id,_),
	tag_numbervars(Top,Id,_),
	tag_numbervars(Bot,Id,_),
	tag_numbervars(BCont,Id,_),
	tag_numbervars(RCont,Id,_),
	%%
	functor(Top,F,N),
	(   Adj = required_adj[],
	    \+ ( recorded(adjkind(F/N,Mode)), Mode \== wrap )
	->  %% Mandatory adjonction and no concurrent tig adj
	    Pred = tag_adj_strict(F/N)
	;   %% Non mandatory adjonction or concurrent tig adj
	    Pred = tag_adj(F/N)
	),
	wrapping_predicate(Pred),
	tag_compile_subtree_noadj(Id,
				  T,
				  Foot_Left,
				  Foot_Right,
				  '*SA-PROLOG-LAST*'(Cont),
				  Below,
				  Type,
				  Info,
				  Seen
				 ),
%	( recorded( tag_features(NT,Top,Bot) ) xor true),
	apply_feature_mode_constraints(NT,wrap,Top,_Bot),
	decompose_args(tag_adj(F/N),
		       [Left,Right,Foot_Left,Foot_Right,Top,_Bot,A_Id,AdjDone,Id],
		       Args),
	feature_args_unif(Id,
			  Bot,
			  _Bot,
			  '*SA-ADJ-WRAPPER-CALL*'(Pred,Args,BCont,Below,RCont),
			  Trans,
			  Type
			 ),
	true
	.


%% defined in tig.pl
%% but should rather be defined here !
:-light_tabular apply_feature_constraints/3.
:-mode(apply_feature_constraints/3,+(+,-,-)).

:-light_tabular apply_feature_mode_constraints/4.
:-mode(apply_feature_mode_constraints/4,+(+,+,-,-)).

:-std_prolog feature_args_unif/6.

feature_args_unif(Id,Top,Bot,Cont,Trans,Type) :-
	( Top = Bot ->
	  Cont = Trans
	;
	  Top =.. [F|TopArgs],
	  Bot =.. [F|BotArgs],
	  feature_args_unif_aux(TopArgs,BotArgs),
	  body_to_lpda(Id,(Top=Bot),Cont,Trans,Type)
	)
	.

:-rec_prolog feature_args_unif_aux/2.

feature_args_unif_aux([],[]).
feature_args_unif_aux([T|Top],[B|Bot]) :-
	( T=B xor true),
	feature_args_unif_aux(Top,Bot)
	.

wrapping_predicate( Pred::tag_adj(F/N)) :-
	decompose_args( Pred,
			[Left,Right,Foot_Left,Foot_Right,Top,Bot,A_Id,AdjDone,CallerTree],
			Args1
		      ),
%	format('test tagfilter_cat top=~w bot=~w\n',[Top,Bot]),	     
	make_tag_top_callret(Top,Left,Right,Call,Return),
	make_tag_bot_callret(Bot,Foot_Left,Foot_Right,Foot_Call,Foot_Return),
	tag_numbervars(Args::[BClosure,RClosure,BCont|Args1],Id,_),
	body_to_lpda(Id,
		     (Call=Foot_Call,Return=Foot_Return),
%		     (Call=Foot_Call),
		     '*SA-PROLOG-LAST*',
		     NoAdjContFirst,
		     dyalog),
	NoAdjContLast = '*SA-PROLOG-LAST*',
	/*
	body_to_lpda(Id,
%		     (Call=Foot_Call,Return=Foot_Return),
		     (Return=Foot_Return),
		     '*SA-PROLOG-LAST*',
		     NoAdjContLast,
		     dyalog),
	*/
	_AdjCont1 = '*SA-ADJ-FIRST*'(Call,Foot_Call,'*SA-PROLOG-LAST*'),
	body_to_lpda(Id,(AdjDone = +),_AdjCont1,_AdjCont,dyalog),
	%% following to be called before numbervars
	( recorded(tag_features_mode(NT,wrap,Top,Bot))
	xor recorded(tag_features(NT,Top,Bot))
	xor recorded(tag_features(NT,Top,_))
	xor NT=F),
%	format('tagfilter_cat ~w\n',[NT]),	     
	( recorded( tagfilter_cat(CallerTree^NT^wrap^Left^Right^Top^Filter) )
	->
	  tag_numbervars(Filter,Id,_),
	  %%	  format('Compiling ~w ~w ~w with ~w\n',[F,Mode,dyalog,Filter]),
	  body_to_lpda(Id,Filter,_AdjCont,AdjCont,dyalog),
	  %%	  format('==> ~w\n',[ContAdj]),
	  true
	;
	  AdjCont = _AdjCont
	),
	record( '*SA-ADJ-WRAPPER*'( Pred,
				    Args,
				    [ NoAdjContFirst
				    | AdjCont],
				    [ NoAdjContLast 
				    |'*SA-ADJ-LAST*'(Return,Foot_Return,'*SA-PROLOG-LAST*',A_Id)]
				  ) )
	.


wrapping_predicate( Pred::tag_adj_strict(F/N)) :-
	decompose_args( tag_adj(F/N),
			[Left,Right,Foot_Left,Foot_Right,Top,Bot,A_Id,+,CallerTree],
			Args1
		      ),
	make_tag_top_callret(Top,Left,Right,Call,Return),
	make_tag_bot_callret(Bot,Foot_Left,Foot_Right,Foot_Call,Foot_Return),
	tag_numbervars(Args::[BClosure,RClosure,BCont|Args1],Id,_),
	_AdjCont = '*SA-ADJ-FIRST*'(Call,Foot_Call,'*SA-PROLOG-LAST*'),
	( recorded(tag_features(NT,Top,Bot)) xor NT=F),
	( recorded( tagfilter_cat(CallerTree^NT^wrap^Left^Right^Top^Filter) )
	->
	  tag_numbervars(Filter,Id,_),
	  %%	  format('Compiling ~w ~w ~w with ~w\n',[F,Mode,dyalog,Filter]),
	  body_to_lpda(Id,Filter,_AdjCont,AdjCont,dyalog),
	  %%	  format('==> ~w\n',[ContAdj]),
	  true
	;
	  AdjCont = _AdjCont
	),
	record( '*SA-ADJ-WRAPPER*'( Pred,
				    Args,
				    AdjCont,
				    '*SA-ADJ-LAST*'(Return,Foot_Return,'*SA-PROLOG-LAST*',A_Id)
				  )
	      )
	.


:-rec_prolog optimize_topbot_args/4.

optimize_topbot_args([],[],L,L).
optimize_topbot_args([T|Top],[B|Bot],In,Out) :-
	optimize_topbot_args(Top,Bot,In,M),
	( T == B ->
	  Out = [T|M]
	;
	  Out = [T,B|M]
	).

decompose_args(tag_adj(F/N),
	       L::[Left,Right,Foot_Left,Foot_Right,Top,Bot,A_Id,AdjDone,CallerTree],
	       Args
	      ) :-
	functor(Top,F,N),
	functor(Bot,F,N),
	( ( recorded(tag_features_mode(_,wrap,Top,Bot))
	  xor recorded(tag_features(_,Top,Bot))) ->
	  Top =.. [F|TopArgs],
	  Bot =.. [F|BotArgs],
	  optimize_topbot_args(TopArgs,BotArgs,[A_Id,AdjDone,CallerTree],TopBotArgs),
	  Args = [Left,Right,Foot_Left,Foot_Right|TopBotArgs]
	; N > 15 ->
	  Args = L 
	;
	  make_tag_top_callret(Top,Left,Right,Call,Return),
	  make_tag_bot_callret(Bot,Foot_Left,Foot_Right,Foot_Call,Foot_Return),
	  Call =.. [_|Call_Args],
	  Foot_Call =.. [_|Foot_Call_Args],
	  append(Call_Args,Foot_Call_Args,Adj_First_Args),
	  Return =.. [_|Return_Args],
	  Foot_Return =.. [_|Foot_Return_Args],
	  append([A_Id,AdjDone,CallerTree|Return_Args],Foot_Return_Args,Adj_Last_Args),
	  append(Adj_First_Args,Adj_Last_Args,Args)
	),
%%	format('decompose_adj_args ~w => ~w ~w\n',[F/N,Adj_First_Args,Adj_Last_Args]),
	true
	.

:-light_tabular tag_custom_subst/2.
:-mode(tag_custom_subst/2,+(+,-)).

tag_custom_subst(Type,Pred) :-
	(\+ Type = protect(_)),
	recorded( tag_custom_subst(Pred) ),
	true
	.


:-rec_prolog tag_wrap_skipper/8.
%%:-std_prolog tag_wrap_skipper/6.

tag_wrap_skipper(Id,Left,_Left,Right,G,Cont,Trans,Type) :-
	( recorded( skipper(_,_,_,_) ) ->
	    tag_numbervars([_Left],Id,_),
	    body_to_lpda(Id,G,Cont,Cont1,Type),
	    ( verbose_tag(Id) ->
		Trace = true
	    ;
		Trace = false
	    ),
	    wrapping_predicate(Pred::tag_skipper(Trace)),
	    decompose_args(tag_skipper,[Left,_Left], Args),
	    Trans = '*WRAPPER-CALL-ALT*'(Pred,Args,Cont1),
	    true
	;   
	    Left = _Left,
	    body_to_lpda(Id,G,Cont,Trans,Type)
	)
	.


%% Inspired of multiple tig adjoining in tig.pl
%% a direct coding with @* doesn't seem to work (need to investigate)
wrapping_predicate( Pred::tag_skipper(TraceFlag) ) :-
	decompose_args( tag_skipper, [Left,Right], Args1),
	decompose_args( tag_skipper, [Middle,Right], Args2),
	recorded( skipper(Left,_A,Middle,Skip) ),
	tag_numbervars(Args::[Closure|Args1],Id,_),
	tag_numbervars(Args2,Id,_),
	( TraceFlag = true ->
	    tag_numbervars(Lex,Id,_),
	    Trace = '$tagop'(skip,verbose!epsilon(_A,Left,Middle,Lex)),
	    SkipTrace = (Skip,Trace)
	;   
	    SkipTrace = Skip
	),
	tag_numbervars([SkipTrace],Id,_),
	Inner_Call = '*WRAPPER-INNER-CALL*'(tag_skipper(TraceFlag),Args2,'*PROLOG-LAST*'),
	body_to_lpda(Id,SkipTrace,Inner_Call,InnerCont,dyalog),
%%	ContSkip = '*CONT*'(InnerCont),
	ContSkip = InnerCont,
	ContNoSkip = unify(Left,Right) :> '*PROLOG-LAST*' :> fail,
	Cont = '*ALTERNATIVE-LAYER*'(ContNoSkip,ContSkip),
	%%		body_to_lpda(Id,Wrapper,'*PROLOG-LAST*' :> fail,Cont,dyalog),
	%%	Cont = unify(Left,Right) :> '*PROLOG-LAST*',
	record( Wrap::'*WRAPPER*'(Pred,Args,Cont) ),
	true
	.

decompose_args( tag_skipper, L, L ).

tag_compile_subtree_noadj(Id,
			  T::tag_node{ id => Node,
				       bot => A,
				       kind => Kind,
				       children=>Children,
				       label=>Label,
				       token=> Token
				     },
			  Left,
			  Right,
			  Cont,
			  Trans,
			  Type,
			  Info,
			  Seen
			 ):-
	tag_numbervars(A,Id,_),
	%% substitution node
	%% => node not on spine
	( Kind == subst ->
	    (	Type = answer(_) ->
		make_tag_subst_callret(A,Left,Right,Call,Return),
		Trans1='*SA-PSEUDO-SUBST*'(Call,Return,Cont,Node)
	    ;	tag_custom_subst(Type,Custom_Subst) ->
		K =.. [Custom_Subst,Label,A,Left,Right,Node],
		handle_choice(Cont,Trans1,Last,ContA,ContB),
		body_to_lpda(Id,K,Last,ContB,Type),
		tag_compile_subtree(Id,'$protect'(T),Left,Right,Last,ContA,Type,Info,Seen)
	    ; fail ->
		%% no wrapping
		tag_subst_compile(Id,T,Left,Right,Cont,Trans1,Type,Info,Seen)
	    ;	%% fail ->
	    functor(A,F,N),
		decompose_args(tag_subst(F/N), [Left,Right,Node,A,Id], Args ),
		wrapping_predicate(tag_subst(F/N,Label)),
		Trans1='*WRAPPER-CALL-ALT*'(tag_subst(F/N),Args,Cont),
%		format('handling subst a=~w left=~w right=~w node=~w args=~w cont=~w\n',[A,Left,Right,Node,Args,Cont]),
		true
	    ),
	    %%	    Trans1 = '*SA-SUBST*'(Call,Return, Cont,Node),
	    (	core_info(Id,Info,CInfo),
		CInfo = tag_anchor{ family => Family, coanchors => VarCoanchors },
		recorded( tag_coanchor(Family,Node,LemmaVar, VarCoanchors) ) ->
		tag_numbervars(LemmaVar,Id,_),
		body_to_lpda(Id,check_coanchor(LemmaVar,Left,Right),Trans1,Trans,Type)
	    ;	
		Trans=Trans1
	    )
	%% Coanchor node
	%% => node not on spine
	;   Kind == coanchor ->
	    %%	    tag_numbervars(Token,Id,_),
%%	    format( 'CoAnchor ~w ~w -> ~w\n',[Id,Token,Info]),
	    Coanchor = coanchor(Token,Label,_Left,Right,A),
	    ( verbose_tag(Id) ->
		tag_numbervars(Lex,Id,_),
		Trace = '$tagop'(Node,verbose!coanchor(Token,_Left,Right,A,Lex)),
		CoanchorTrace = ( Coanchor , Trace )
	    ;	
		CoanchorTrace = Coanchor
	    ),
	    tag_wrap_skipper(Id,Left,_Left,Right,
			     CoanchorTrace,
			     Cont,
			     Trans1,
			     Type ),
%%	    format('Here.2 Wrapper=~w\n\tCont2=~w\n\tType=~w\n\tInfo=~w\n',[Wrapper,Cont2,Type,Info]),
%%	    format('Here.3\n',[]),
	    (	core_info(Id,Info,CInfo),
		CInfo = tag_anchor{ family => Family, coanchors => VarCoanchors },
		recorded( tag_coanchor(Family,Node,LemmaVar, VarCoanchors) ) ->
		tag_numbervars(LemmaVar,Id,_),
		body_to_lpda(Id,check_coanchor(LemmaVar,_Left,Right),Trans1,Trans,Type)
	    ;	
		Trans=Trans1
	    )
	%% Anchor node
	%% => node not on spine
	;   Kind == anchor ->
	    %% tag_numbervars(Token,Id,_),
	    %%	    format( 'Anchor ~w ~w -> ~w\n',[Id,Token,Info]),
	  tag_numbervars(Lex,Id,_),
	  Anchor = anchor(Info1,Token,Label,_Left,Right,A,Lex),
	  ( tag_corrections ->
	    CorrectionAnchor = correction_anchor(_Left),
	    XAnchor = (CorrectionAnchor, Anchor)
	  ;
	    XAnchor = Anchor
	  ),
	  ( verbose_tag(Id) ->
	    Trace = '$tagop'(Node,verbose!anchor(Token,_Left,Right,Id,A,Lex,Info1)),
	    AnchorTrace = (XAnchor,Trace)
	  ;	
	    AnchorTrace = XAnchor
	  ),
	  core_info(Id,Info,Info1),
	  tag_numbervars(Info1,Id,_),
	  tag_wrap_skipper(Id,Left,_Left,Right,
			   AnchorTrace,
			   Cont,
			   Trans,
			   Type),
	    %%	    Info = tag_anchor{ family => Family },
	    true
	%% Inside node
	%% nothing to do for this node, except compiling its children
	;   Kind ==  std ->
	    tag_compile_subtree(Id,Children,Left,Right,Cont,Trans,Type,Info,Seen)
	;   
	    error('TAG to SA compilation error: ~w',[T])
	)	,
%%	format('\n---------------\nTAG NoAdj ~w\n~K\n----------------\n',[T,Trans]),
	true
	.

decompose_args( tag_subst(F/N), [Left,Right,Node,T,CallerTree], Args ) :-
	functor(T,F,N),
	( N > 30 -> Args = [Left,Right,Node,T,CallerTree] ; T =.. [_|ArgsT], Args = [Left,Right,Node,CallerTree|ArgsT] )
	.

decompose_args( callret_tag_subst(F/N), [Left,Right,Node,T,CallerTree], Args ) :-
	functor(T,F,N),
	make_tag_subst_callret(T,Left,Right,Call,Return),
	( N > 30 ->
	  Args = [Left,Right,Node,T,CallerTree]
	; Call =.. [_|_CallArgs],
	  Return =.. [_|RetArgs],
	  append(_CallArgs,[Node,CallerTree],CallArgs),
	  Args = callret(CallArgs,RetArgs)
	)
	.

wrapping_predicate( tag_subst(F/N,NT) ) :-
	Pred = tag_subst(F/N),
	decompose_args(Pred,[Left,Right,Node,A,CallerTree],Args1),
	make_tag_subst_callret(A,Left,Right,Call,Return),
	tag_numbervars(Args::[Closure|Args1],Id,_),
	( Args1 = callret(CallArgs,RetArgs) ->
	  Last = '*RETARGS*'(RetArgs)
	;
	  Last = '*PROLOG-LAST*'
	),
	( light_tabular(tag(F/N)) ->
	  _Cont = '*SA-SUBST-LIGHT*'(Call,Return,Last,Node)
	;
	_Cont = '*SA-SUBST*'(Call,Return,Last,Node)
	),
	( recorded( tagfilter_cat(CallerTree^NT^subst^Left^Right^A^Filter) )
	  %% recorded( tagfilter_cat(CallerTree^NT^subst^Left^(Node,A,Closure)^Filter) )
	->
	  tag_numbervars(Filter,Id,_),
	  %%	  format('Compiling ~w ~w ~w with ~w\n',[F,Mode,dyalog,Filter]),
	  body_to_lpda(Id,Filter,_Cont,Cont,dyalog),
	  %%	  format('==> ~w\n',[ContAdj]),
	  true
	;
	  Cont = _Cont
	),
	record( '*WRAPPER*'( Pred,
			     Args,
			     Cont
			   ) )
	.

:-std_prolog tag_subst_compile/9.

tag_subst_compile(Id,
		  T::tag_node{ id => Node,
			       bot => A,
			       kind => subst,
			       label=>Label
			     },
		  Left,
		  Right,
		  Cont,
		  Trans,
		  Type,
		  Info,
		  Seen
		 ) :-
	make_tag_subst_callret(A,Left,Right,Call,Return),
	functor(A,F,N),
	( light_tabular(tag(F/N)) ->
	  _Cont = '*SA-SUBST-LIGHT*'(Call,Return,Cont,Node)
	;
	  _Cont = '*SA-SUBST*'(Call,Return,Cont,Node)
	),
	( %% recorded( tagfilter_cat(NT^subst^Left^Right^Filter) )
	  recorded( tagfilter_cat(Id^Label^subst^Left^(Node,A,[])^A^Filter) )
	->
	  tag_numbervars(Filter,Id,_),
	  %%	  format('Compiling ~w ~w ~w with ~w\n',[F,Mode,dyalog,Filter]),
	  body_to_lpda(Id,Filter,_Cont,Trans,dyalog),
	  %%	  format('==> ~w\n',[ContAdj]),
	  true
	;
	  Trans = _Cont
	)
	.

:-std_prolog tag_numbervars/3.

tag_numbervars(T,Id,M) :-
        ( var(Id) ->
            gensym(Id),
	    numbervars(T,1,M),
	    mutable(MM,M),
            record( numbervars(Id,MM) )
        ;   recorded( numbervars(Id,MM) ) ->
	    mutable_read(MM,N),
            numbervars(T,N,M),
	    mutable(MM,M)
	;   numbervars(T,1,M),	% use name of tree as id to number variables
	    mutable(MM,M),
            record( numbervars(Id,MM) )
        )
        .

:-std_prolog numbervars_delete/1.

numbervars_delete(Id) :- erase(numbervars(Id,_)).

%% Imported from tagguide.pl
:-std_prolog tagguide_strip/3.

tag_normalize_and_compile(Id,Tree,Left,Right,Cont,Trans,Type) :-
	normalize(tag_tree{ tree=> (tree Tree) },T),
	tag_numbervars(T,Id,_),
	tupple([],Seen),
	tag_compile_subtree(Id,T,Left,Right,Cont,Trans1,Type,[],Seen),
	( guide(strip) ->
	    tagguide_strip(T,no,ST),
	    tag_compile_subtree(Id,ST,Left,Right,fail,ContGuide,Type,[],Seen),
	    Trans = '*WAIT*'(Trans1) :> ContGuide
	;   
	    Trans = Trans1
	),
	true
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Compilation of TAG lemma

pgm_to_lpda( T::tag_lemma(Lemma,Label,Anchors), Trans ) :-
	Clause = (normalized_tag_lemma(Lemma,Label,_Family,_VarCoanchors,_VarEquations) :- _Body ),
	( var(Anchors) ->
	    error( 'tag_lemma: unexpected variable as anchor ~w', [T] )
	;   Anchors = tag_anchor{} ->
	    tag_compile_lemma_anchor( Anchors, Clause )
	;   domain( Anchor, Anchors ),
	    tag_compile_lemma_anchor( Anchor, Clause )
	),
	%% format( 'LEMMA ~w\n\n', [Clause] ),
	clause_to_lpda( Clause, Trans )
	.

:-std_prolog tag_compile_lemma_anchor/2.

tag_compile_lemma_anchor( T::tag_anchor{ family => Family,
					 coanchors => Coanchors,
					 equations => Equations
				       },
			  Clause
			) :-

	( var(Family) ->
	    error( 'tag_lemma: unexpected variable as family ~w', [T] )
	;   atom(Family) ->
	    (	recorded( Info::tag_anchor{ family => Family,
					    coanchors => VarCoanchors,
					    equations => VarEquations
					  } )
	    ->	true
	    ;
		warning( 'tag_lemma: reference to missing anchor model ~w', [Family] ),
		VarCoanchors=[],
		VarEquations=[]
	    ),
	    Coanchors ?= [],
	    Equations ?= [],
	    Clause = (normalized_tag_lemma(_,_,Family,VarCoanchors,VarEquations) :- Body ),
	    tag_compile_lemma_coanchors( Coanchors, VarCoanchors, Family, Body),
	    tag_compile_lemma_equations( Equations, VarEquations, Family)
	;   domain(F,Family),
	    tag_compile_lemma_anchor( tag_anchor{ family => F,
						  coanchors => Coanchors,
						  equations => Equations
						},
				      Clause
				    )
	)
	.

:-rec_prolog tag_compile_lemma_coanchors/4.

tag_compile_lemma_coanchors( [], _, _, true ).
tag_compile_lemma_coanchors( [Node=Lemma|Coanchors],
			     VarCoanchors,
			     Family,
			     (	 check_coanchor_in_anchor(LemmaVar,LemmaList),
				 Body
			     )
			   ) :-
	( recorded( tag_coanchor(Family,Node,LemmaVar,VarCoanchors) ) ->
	    tag_compile_lemma_check(Lemma,LemmaList),
	    tag_compile_lemma_coanchors(Coanchors,VarCoanchors,Family,Body)
	;   
	    error( 'Node ~w not referenced as a coanchor for family ~w\n',[Node,Family] )
	)
	.

:-std_prolog tag_compile_lemma_check/2.

tag_compile_lemma_check(Lemma,LemmaList) :-
	( Lemma = (L1 ; L2) ->
	    LemmaList = [L1|LemmaList2],
	    %%	    tag_compile_lemma_check(L1,LemmaVar,Body1),
	    tag_compile_lemma_check(L2,LemmaList2)
	;   
	    LemmaList = [Lemma]
	)
	.

:-rec_prolog tag_compile_lemma_equations/3.

:-std_prolog tag_anchor_analyse_equation/3.  %% See in reader.pl

tag_compile_lemma_equations([],_,_).
tag_compile_lemma_equations([E|Equations],VarEquations,Family) :-
	( var(E) ->
	    error( 'tag_lemma: unexpected variables in equation', [] )
	;   E = (U at Node) ->
	    (	recorded( tag_equation(Family,Node,Top,Bot,VarEquations) ) ->
		tag_anchor_analyse_equation(U,Top,Bot),
		tag_compile_lemma_equations(Equations,VarEquations,Family)
	    ;	
		error( 'Node ~w not referenced to support equations for family ~w\n',[Node,Family] )
	    )
	;   
	    error( 'tag_lemma: bad format for equation ~w', [E] )
	)
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%

:-std_prolog tree_info/2.

%% Assume Info is a core info of the form tag_anchor{} or strip(tag_anchor{})
tree_info(T,Info) :-
	( var(T) -> true
	;   T=[] -> true
	;   T=[A|B] ->
	  tree_info(A,Info),
	  tree_info(B,Info)
	;   T = @*{ goal => A } ->
	  tree_info(A,Info)
	;   T = (A ## B) ->
	  tree_info(A,Info),
	  tree_info(B,Info)
	;   T = (A,B) ->
	  tree_info(A,Info),
	  tree_info(B,Info)
	;   T= (A;B) ->
	  tree_info(A,Info),
	  tree_info(B,Info)
	;   T = '$answers'(A) ->
	  tree_info(A,Info)
	;   T = guard{ goal => A } ->
	  tree_info(A,Info)
	;   T = tag_node{ kind=>Kind, children=>Children } ->
	  (	domain(Kind,[scan,escape])
	    xor ( Info = strip(_) ;
		    tag_compile_info_anchor(Info,T)),
		tree_info(Children,Info))
	;   true
	)
	.

:-std_prolog tree_foot/2.

%% Assume Info is a core info of the form tag_anchor{} or strip(tag_anchor{})
tree_foot(T,Foot) :-
	( var(T) -> fail
	;   T=[] -> fail
	;   T=[A|B] -> fail
	;   T = @*{ goal => A } ->
	    tree_foot(A,Foot)
	;   T = (A ## B) ->
	    (	tree_foot(A,Foot) xor    tree_foot(B,Foot) )
	;   T= (A;B) ->
	    (	tree_foot(A,Foot) xor    tree_foot(B,Foot)  )
	;   T= (A,B) ->
	    (	tree_foot(A,Foot) xor    tree_foot(B,Foot)  )
	;   T = '$answers'(A) ->
	    tree_foot(A,Foot)
	;   T= tag_node{ kind => foot } -> Foot = T
	;   T = tag_node{ kind=>Kind, children=>Children } ->
	    domain(T1,Children),
	    tree_foot(T1,Foot)
	;   T = guard{ goal => T1 } ->
	    tree_foot(T1,Foot)
	;   fail
	)
	.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

:-std_prolog tag_autoloader_wrapper/4.

tag_autoloader_wrapper(Name,L,Info,Loader) :-
    %%	format('test autoloader ~w\n',[Name]),
    (L=_NRoot::tag_node{ spine => Spine,
			 label => Cat} xor L = guard{ goal => _NRoot }),
    tag_autoloader( Name,
		    L,
		    Info,
		    Loader1,
		    $[0,1000], $[0,1000]
		  ),
	( Spine \== yes ->
	  Mode = subst
	; recorded(tig(Name,Mode)) ->
	  true
	; Mode = wrap
	),
	mutable(LCM,true),
	every(( recorded(D::lctag(Name,Kind,Val,Trees)),
		mutable_read(LCM,_LCS),
		_LC=( record(K::lctag(Kind,Val,Name,Trees)) ),
		tag_numbervars(_LC,Name,_),
		mutable(LCM,(_LC,_LCS))
	      )),
	mutable_read(LCM,LCS),
	LCS2 = ( record(loaded_tree(Name)),
		 record(lctag_map(Cat,Mode,Name)),
		 record(lctag_map_reverse(Name,Cat,Mode)),
		 LCS ),
	( \+ recorded(lctag(Name,_,_,_)) ->
	  LCS3= ( record(lctag_ok(Name)), LCS2 )
	;
	  LCS3 = LCS2
	),
	%% use negation to avoid some bindings
	%% occurring during filtering to be present in the loaded tree
	(LCS3 = true ->
	 Loader = (\+ (Loader1 -> fail ; true ))
	;
	 Loader = ((\+ ( Loader1 -> fail ; true )) -> LCS3 ; fail)
	),
%%	format('LCS loader ~w ~w\n',[Name,Loader]),
	true
	.


:-std_prolog tag_autoloader/6.
%% +pattern: tag_autoloader(Tree,Info,Loader)
%% +desc: extract the autoload part of a TAG tree

%% Assume Info is a core info of the form tag_anchor{}
tag_autoloader(Name,T,Info,Loader,Left,Right) :-
	( T=[A] ->
	  tag_autoloader(Name,A,Info,Loader,Left,Right)
	;   (T=[A|B] ; T=(A,B)) ->
	  tag_autoloader(Name,A,Info,Loader1,Left,Middle),
	  tag_autoloader(Name,B,Info,Loader2,Middle,Right),
	  autoloader_simplify((Loader1,Loader2),_Loader),
	  tag_autoloader_simplify(_Loader,Loader)
	;   T = (A ## B) ->
	  tag_autoloader(Name,A,Info,Loader1,Left1,Right),
	  tag_autoloader(Name,B,Info,Loader2,Left2,Right),
	  autoloader_simplify(( term_range(Min,Max,Left),
				term_range(Min,Max,Left1),
				term_range(Min,Max,Left2),
				Loader1,Loader2
			      ),Loader)

	;   T= (A;B) ->
	  tag_autoloader(Name,A,Info,Loader1,Left,Right),
	  tag_autoloader(Name,B,Info,Loader2,Left,Right),
	  autoloader_simplify((Loader1 ; Loader2),Loader)
	;   T = tag_node{ label=>L::[_|_],kind=>scan } ->
	  Loader = ( phrase('$noskip'(L),Left,Right1) , term_range(Right1,1000,Right) )
	;   T = tag_node{ kind => escape, label => noop(expect(scan,L)) } ->
	  Loader = ( phrase('$noskip'(L),Left,Right1) , term_range(Right1,1000,Right) )
	;   T = tag_node{ kind=>anchor, label=>Label, top => Top, token => Token } ->
	  Info = tag_anchor{ family => Family },
	  Loader = ( tag_family_load(Family,Label,Top,Token,Name,Left,Right1),
		     term_range(Right1,1000,Right)
		   )
	;   T = tag_node{ kind=>coanchor, label=>Label, top => Top, token => Token } ->
	  Loader = ( tag_check_coanchor(Label,Top,Token,Left,Right1,Name),
		     term_range(Right1,1000,Right)
		   )
	;  T = tag_node{ kind => foot, label => Label, top => Top, token => Token },
	  recorded(tag_terminal_cat(Label)) ->
	  (recorded(tig(Name,TigMode)) xor TigMode=wrap),
	  Loader = ( tag_check_foot(Label,Top,Left,Right1,Name,TigMode),
		     term_range(Right1,1000,Right)
		   )
	; T = tag_node{ kind => subst, label => Label, top => Top, token => Token },
	  recorded(tag_subst_cat(Label,Cats)) ->
	  %% a more complete scheme could be tried on the model of lctag
	  Loader = ( tag_check_subst(Label,Cats,Left,Right1,Name),
		     term_range(Right1,1000,Right)
		   )
	;  T = tag_node{ children => Children::[_|_],
			 label => Cat,
			 top => Top,
			 bot => Bot,
			 adj => Adj
		       } ->
	  ( Adj = required_adj[] ->
%%	    format('strict adj: cat=~w  top=~w bot=~w\n',[Cat,Top,Bot]),		   
	    tag_autoloader(Name,Children,Info,Loader1,Left1,Right1),
	    Loader = ( tag_autoload_adj(Cat,Top,Bot,Left,Right,Left1,Right1),
		       Loader1
		     )
	  ;
	    tag_autoloader(Name,Children,Info,Loader,Left,Right)
	  )
	;   T = guard{ goal => A, plus => Plus, minus => fail } ->
	  tag_autoloader(Name,A,Info,Loader1,Left,Right),
	  null_tupple(Seen),
	  guard_approx(Plus,XPlus,0,Seen,autoload),
%%	  autoload_guard_approx(Plus,XPlus),
	  Loader = (Loader1,XPlus),
	  true
	;   fail,
	  T = guard{ goal => A, plus => Plus, minus => Minus } ->
	  tag_autoloader(Name,A,Info,Loader1,Left,Right),
	  null_tupple(Seen),
	  guard_approx(Plus,XPlus,0,Seen,autoload),
	  %%autoload_guard_approx(Plus,XPlus),
	  autoload_guard_approx(Minus,XMinus),
	  Loader = ((Loader1,XPlus) ; (Left=Right),XMinus) ,
	  true
	;   
	  Loader = (Left=Right)
	)
	.

:-std_prolog tag_autoloader_simplify/2.

tag_autoloader_simplify(L,XL) :-
	( L = ((L1,L2),L3) ->
	  tag_autoloader_simplify((L1,(L2,L3)),XL)
	; L = (X=Y,Z=T),var(Y),Y==Z ->
	  XL=(X=T)
	; L = (X=Y,(Z=T,L1)),var(Y),Y==Z ->
	  tag_autoloader_simplify((X=T,L1),XL)
	; XL=L
	)
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Interleave operator

%% We essentialy implement interleaving for TAG by copy-paste of interleaving for DCGs
%% It would best in some future to use some object-oriented mechanism
%% to get a more compact notation

%%! @case Interleave node
tag_compile_subtree_handler(Id,
			    Body::(A ## B),
			    Left,Right,Cont,Trans,Type,Info,Seen) :-
	interleave_initialize,
	il_label([],Label),
	tag_numbervars([IL_Left,IL_Right,PrevLeft,_Right,LeftRank,_RightRank,PrevLeftRank,RightRank],Id,_),
	tupple(Body,BodyTupple),
	tupple(Cont,ContTupple),
	inter(ContTupple,BodyTupple,KeepTupple),
	union(Seen,KeepTupple,Seen2),
	%extend_tupple(Cont,Seen,Seen2),
	il_tupple(Body,Tupple),
	union(Seen2,Tupple,Seen3),
	tag_il_loop_to_lpda(Id,
			    Left,_Right,PrevLeft,
			    LeftRank,_RightRank,PrevLeftRank,
			    IL_Right,Loop_Code,Type,Info),
	tag_il_body_to_lpda(Id,Body,
			    IL_Left,IL_Right,IL_Out,IL_Out,
			    noop,Loop_Code,
			    Trans1,Type,Info,Label,
			    Seen3
			   ),
	tag_il_start_to_lpda(Id,
			     Cont,
			     Right,Right,PrevLeft,
			     RightRank,RightRank,PrevLeftRank,
			     [],
			     [],IL_Left,
			     Trans1,
			     Trans2,
			     Type,Info,Label),
	Trans = ( '*NOOP*'(Tupple) :> '*IL-START*'(Trans2) ),
%%	format('IL Trans: ~w\n',[Trans]),
	true
	.

:-std_prolog il_tupple/2.

il_tupple(Body,Tupple) :-
	il_all_tupples(Body,[],Tupples),

	null_tupple(Null),
	mutable(MSeen,Null),
	mutable(MDup,Null),
	every(( domain(TX,Tupples),
		mutable_read(MSeen,Seen),
		inter(TX,Seen,Dup1),
		mutable_read(MDup,Dup),
		union(Dup1,Dup,Dup2),
		mutable(MDup,Dup2),
		union(Seen,TX,Seen2),
		mutable(MSeen,Seen2)
	      )),
	mutable_read(MDup,Tupple)
	
%	il_combine_tupples(Tupples,Tupple)
	.

:-std_prolog il_all_tupples/3.

il_all_tupples(Body,T1,T2) :-
	( Body = (A ## B) ->
	  il_all_tupples(A,T1,T3),
	  il_all_tupples(B,T3,T2)
	; tupple(Body,T),
	  T2 = [T|T1]
	)
	.

:-rec_prolog
	il_combine_tupples/2,
	il_combine_tupples_aux/3.

il_combine_tupples([],Null) :-
	null_tupple(Null).
%%il_combine_tupples([T],[]).
il_combine_tupples([T1|L],XT) :-
	il_combine_tupples(L,XL),
	il_combine_tupples_aux(T1,L,XT1),
	union(XT1,XL,XT)
	.

il_combine_tupples_aux(T,[],Null) :- null_tupple(Null).
il_combine_tupples_aux(T,[T1|L],XT) :-
	il_combine_tupples_aux(T,L,XL),
	inter(T,T1,T2),
	union(T2,XL,XT).

%%! @predicate tag_il_loop_to_lpda(Id,Pos,IL_Pos,Trans,Type,Info)
%%! @brief Generate LPDA code to select an interleave alternative
:-std_prolog tag_il_loop_to_lpda/11.

tag_il_loop_to_lpda(Id,
		    Pos,NextPos,PrevPos,
		    PosRank,NextPosRank,PrevPosRank,
		    IL_Pos,'*IL-LOOP-WRAP*'(Trans),Type,Info) :-
	tag_numbervars([IL_Loop,Loop_Jump],Id,_),
	tag_il_args(IL_Loop,
		    Pos,NextPos,PrevPos,
		    PosRank,NextPosRank,PrevPosRank,
		    IL_Args_Loop),
	update_counter(il_choice,Label),
	body_to_lpda(Id,
		     (
			 ( Pos == PrevPos ->
			   Pos = NextPos,
			   interleave_choose_first(Label,IL_Pos,IL_Loop,Loop_Jump,PosRank,PrevPosRank)
			 ; 
			 interleave_choose_alt(Label,IL_Pos,IL_Loop,Loop_Jump,PosRank)
			 ),
			 %% format('interleave prev_rank=~w prev_pos=~w rank=~w pos=~w\n',[PrevPosRank,PrevPos,PosRank,Pos]),
			 true
		     ),
		     '*IL-LOOP*'(Loop_Jump,IL_Args_Loop),
		     Trans,
		     Type)
	.

%%! @predicate tag_il_start_to_lpda(Id,Exit,Pos,IL_Pos,IL_Left,IL_Right,Cont,Trans,Type,Info,Label)
%%! @brief Generate LPDA code to start a new interleave block with its exit alternative
:-std_prolog tag_il_start_to_lpda/16.

tag_il_start_to_lpda( Id,
		      Exit,
		      Pos,NextPos,PrevPos,
		      PosRank,NextPosRank,PrevPosRank,
		      IL_Pos,
		      IL_Left,IL_Right,
		      IL_Cont,
		      '*IL-ALT*'(Exit_Jump,Exit,IL_Args_Exit,[]) :> Trans1,
		      Type,Info,Label
		    ) :-
	tag_numbervars(Exit_Jump,Id,_),
	tag_il_args(IL_Pos,
		    Pos,NextPos,PrevPos,
		    PosRank,NextPosRank,PrevPosRank,
		    IL_Args_Exit),
	body_to_lpda(Id,
		     interleave_start(Label,Exit_Jump,IL_Left,IL_Right),
		     IL_Cont,
		     Trans1,
		     Type)
	.

%%! @predicate tag_il_register_to_lpda( Id,Alt,Pos,IL_Pos,IL_Left,IL_Right,Cont,Trans,Type,Info,Label) 
%%! @brief Generate LPDA code to register an interleave alternative
:-std_prolog tag_il_register_to_lpda/17.

tag_il_register_to_lpda( Id,
			 Alt,
			 Pos,NextPos,PrevPos,
			 PosRank,NextPosRank,PrevPosRank,
			 IL_Pos,
			 IL_Left,IL_Right,
			 IL_Cont,
			 VLoop,
			 '*IL-ALT*'(Alt_Jump,Alt,IL_Args_Alt,VLoop) :> Trans1,
			 Type,Info,Label
		       ) :-
	my_numbervars(Alt_Jump,Id,_),
	tag_il_args(IL_Pos,
		    Pos,NextPos,PrevPos,
		    PosRank,NextPosRank,PrevPosRank,
		    IL_Args_Alt),	
	body_to_lpda(Id,
		     interleave_register(Label,Alt_Jump,IL_Left,IL_Right),
		     IL_Cont,
		     Trans1,
		     Type)
	.

%% inherited from dcg.pl
:-light_tabular interleave_initialize/0.


%% inherited from dcg.pl
:-std_prolog il_label/2.


%%! @predicate tag_il_args(IL_Table,Left,IL_Args)
%%! @brief Collect arguments for TAG interleaving alternative
:-light_tabular tag_il_args/8.

%tag_il_args(IL_Table,Left,Right,PrevLeft,IL_Args::[IL_Table,Left,Right,PrevLeft]).

tag_il_args(IL_Table,
	    Left,Right,PrevLeft,
	    LeftRank,RightRank,PrevLeftRank,
	    IL_Args::[IL_Table,Left,Right,PrevLeft,LeftRank,RightRank,PrevLeftRank]).

:-light_tabular tag_il_exit_args/3.

tag_il_args(IL_Table,Left,IL_Args::[IL_Table,Left]).

%%! @predicate tag_il_body_to_lpda(Id,A,IL_Left,IL_Right,IL_Out_Left,IL_Out_Right,Cont,Trans,Type,Info,Label)
%%! @brief essentially a wrapper around tag_il_body_to_lpda_handler/13
%%! Also wrap interleaving registration of an alternative build with tag_body_to_lpda/9

%%! @param Id  -- Id of TAG tree being compiled, used to number variables
%%! @param A -- The TAG subtree being compiled
%%! @param IL_Left -- The IL table before registering A
%%! @param IL_Right -- The IL table after registering A
%%! @param IL_Out_Left -- The IL table when starting alternative A
%%! @param IL_Out_Right -- The IL table before selecting next alternative Cont after A
%%! @param Cont -- Next alternative after A (possibly empty)
%%! @param Trans -- Resulting LPDA fragment
%%! @param Type -- Compilation mode
%%! @param Info -- Info of the tag tree 
%%! @param Label -- Label of current interleaving block
%%! @param Seen -- Variables seen before entering the interleaving block

:-std_prolog tag_il_body_to_lpda/13.

tag_il_body_to_lpda(Id,Body1,
		    IL_Left,IL_Right,IL_Out_Left,IL_Out_Right,
		    Cont, IL_Cont,
		    Trans,Type,Info,Label,Seen
		   ) :-
	(   toplevel(tag_term_expand(Body1,Body)),
	    my_numbervars(Body,Id,_)
	xor Body1=Body), % *HOOK*
	( tag_il_body_to_lpda_handler(Id,Body,
				      IL_Left,IL_Right,IL_Out_Left,IL_Out_Right,
				      Cont,IL_Cont,
				      Trans,Type,Info,Label,Seen ) ->
	    true
	;
	tag_numbervars([Left,Right,Left0,Right0,LeftRank,RightRank,LeftRank0,RightRank0,
			IL_Out_Left,IL_Out_Right],Id,_),
	tag_il_loop_to_lpda(Id,
			    Right,Right0,Left,
			    RightRank,RightRank0,LeftRank,
			    IL_Out_Right,Loop_Code,Type,Info),
	    ( Cont == noop ->
		Cont1 = Loop_Code,
		Right=_Right,
		VLoop=[]
	    
	    ;	Cont = [VLoop^Loop_Code,_Right,Right]^KK ->
%%		format('IL Case ~w\n',[Cont]),
		Cont1 = KK
	    ;	
		Cont1 = Cont :> Loop_Code,
		Right=_Right,
		VLoop=[]
	    ),
	    tag_compile_subtree(Id,Body, Left, _Right,Cont1, _Trans1,Type,Info,Seen),
	    %body_to_lpda(Id, (Left == Left0 -> Left = Right ; true), _Trans1,Trans1,Type),
	    Trans1 = _Trans1,
	    tag_il_register_to_lpda( Id,Trans1,
				     Left,Right,Left0,
				     LeftRank,RightRank,LeftRank0,
				     IL_Out_Left,
				     IL_Left,IL_Right,IL_Cont,
				     [Loop_Code|VLoop],
				     Trans,
				     Type,Info,Label
				   )
	)
	.

%%! @brief to express handlers for all cases of TAG constructions whithin a interleaving block

:-rec_prolog tag_il_body_to_lpda_handler/13.


%%! @case Empty Scan []
tag_il_body_to_lpda_handler(Id,
			    % [],
			    tag_node{ label => [], kind => scan },
			    IL_Left,IL_Right,_,_,
			    Cont,IL_Cont,
			    Trans:: (unify(IL_Left,IL_Right) :> IL_Cont),
			    Type,Info,Label,_
			   ) :-
%%	format('IL Void Scan ~w\n',[Trans]),
	true
	.


%%! @case Concatenation (A,B)
tag_il_body_to_lpda_handler(Id,(A,B),
			    IL_Left,IL_Right,IL_Out_Left,IL_Out_Right,
			    Cont,IL_Cont,
			    Trans,Type,Info,Label,Seen
			   ) :-
	tag_numbervars([IL_LeftB,IL_RightB],Id,_),
	extend_tupple(A,Seen,Seen2),
	tag_il_body_to_lpda(Id,B,
			    IL_LeftB,IL_RightB,IL_Out_Left,IL_Out_Right,
			    Cont,noop,
			    TransB,Type,Info,Label,Seen2),
	tag_il_body_to_lpda(Id,A,
			    IL_Left,IL_Right,IL_LeftB,IL_RightB,
			    TransB,IL_Cont,
			    Trans,Type,Info,Label,Seen)
	.

%%! @case Disjunction (A;B)
tag_il_body_to_lpda_handler(Id,(A;B),
			    IL_Left,IL_Right,IL_Out_Left,IL_Out_Right,
			    Cont,IL_Cont,
			    Trans,Type,Info,Label,Seen
			   ) :-
	\+ A = (_ -> _),
	handle_choice(IL_Cont,Trans,IL_Last,ContA,ContB),
	tag_il_body_to_lpda(Id,A,
			    IL_Left,IL_Right,IL_Out_Left,IL_Out_Right,
			    Cont,IL_Last,
			    ContA,Type,Info,Label,Seen),
	tag_il_body_to_lpda(Id,B,
			    IL_Left,IL_Right,IL_Out_Left,IL_Out_Right,
			    Cont,IL_Last,
			    ContB,Type,Info,Label,Seen)
	.


%%! @case Guarded node
%% the guards are tried at registration time, but after registration of A
%% the idea is to avoid some interleaving when the gaurds fail
tag_il_body_to_lpda_handler(Id,
			    guard{ goal => A,
				   plus => Plus,
				   minus => Minus
				 },
			    IL_Left,IL_Right,IL_Out_Left,IL_Out_Right,
			    Cont,IL_Cont,
			    Trans,Type,Info,Label,Seen
			   ) :-
    fail,
	\+ var(Minus),
	\+ (Minus == fail),
	handle_choice(IL_Cont,Trans,IL_Last,ContA,ContB),
	ApproxPlus = Plus,
	tupple(ApproxPlus,TPlus),
	duplicate_vars([TPlus,A],DupA),
	union(TPlus,Seen,Seen2),
	body_to_lpda(Id,(\+ (\+ ApproxPlus)),IL_Last,IL_LastA,Type),
	tag_il_body_to_lpda(Id,
%			    A,
			    guard{ goal => A, plus => '$postguard'(Plus), minus => fail },
			    IL_Left,IL_Right,IL_Out_Left,IL_Out_Right,
			    Cont,IL_LastA,
			    _ContA,Type,Info,Label,Seen2),
	%% variables in A and Plus not present before entering the interleaving block
	%% and not present in some other interleaving component may be lost
	%% when we dissociate the handling of A and Plus
	%% => we use the NOOP operator to push the variable of Plus
	ContA = '*NOOP*'(DupA) :> _ContA,
	/*
	tag_il_body_to_lpda(Id,
			    guard{ goal => A, plus => Plus, minus => fail },
			    IL_Left,IL_Right,IL_Out_Left,IL_Out_Right,
			    Cont,IL_Last,
			    ContA,Type,Info,Label,Seen),
	*/
	body_to_lpda(Id,Minus,unify(IL_Left,IL_Right) :> IL_Last,ContB,Type),
%%	format('IL Guard ~w\n',[Trans]),
	true
	.


%%! @case Interleaving (A ## B)
tag_il_body_to_lpda_handler(Id,(A ## B),
			    IL_Left,IL_Right,IL_Out_Left,IL_Out_Right,
			    Cont,
			    IL_Cont,
			    Trans,Type,Info,Label,Seen
			   ):-
	( Cont == noop ->  
	    Label2 = Label,
	    IL_Left2 = IL_Left,
	    IL_Right2=IL_Right,
	    Trans = TransA
	;   
	    il_label(Label,Label2),
	    tag_numbervars([Right,IL_Left2,IL_Right2,Left,NextRight,
			   LeftRank,RightRank,NextRightRank],Id,_),
	    tag_il_loop_to_lpda(Id,
				Right,NextRight,Left,
				RightRank,NextRightRank,LeftRank,
				IL_Out_Right,Loop_Code,Type,Info),
	    tag_il_start_to_lpda(Id,
				 Cont :> Loop_Code,
				 Right,NextRight,Left,
				 RightRank,NextRightRank,LeftRank,
				 IL_Out_Left,
				 IL_Left,IL_Left2,
				 TransA,
				 Trans,
				 Type, Info, Label2)
	),
	tag_numbervars(IL_Middle,Id,_),
	tag_il_body_to_lpda(Id,B,
			    IL_Middle,IL_Right,IL_OutB,IL_OutB,
			    noop,IL_Cont,
			    TransB,Type,Info,Label2,Seen
			   ),
	tag_il_body_to_lpda(Id,A,
			    IL_Left2,IL_Middle,IL_OutA,IL_OutA,
			    noop,TransB,
			    TransA,Type,Info,Label2,Seen
			   )
	.

%% @case Kleene Star (A @*)
tag_il_body_to_lpda_handler(Id,(AA @*),
			    IL_Left,IL_Right,
			    IL_Out_Left,IL_Out_Right,
			    Cont,IL_Cont,
			    Trans,Type,Info,Label,Seen
			   ) :-
	(   AA = Vars ^ A xor AA = A),
	tag_il_body_to_lpda_handler(Id,@*{goal => A, vars=>Vars},
				    IL_Left,IL_Right,
				    IL_Out_Left,IL_Out_Right,
				    Cont,IL_Cont,
				    Trans,Type,Info,Label,Seen
				   )
	.

%% @case Range Kleene Star @*(A,[Min,Max],First^Arg^Param^Last)
tag_il_body_to_lpda_handler(Id,
			    K,
			    IL_Left,IL_Right,
			    IL_Out_Left,IL_Out_Right,
			    Cont,IL_Cont,
			    Trans,Type,Info,Label,Seen
			   ) :-
%%	format('IL KLEENE 2 ~w out_left=~w out_right=~w cont=~w\n',[AA,IL_Out_Left,IL_Out_Right,Cont]),
	kleene_normalize(K,
			 KleeneStruct:: @*{ goal => A,
					    vars => Vars,
					    from => From,
					    to => To,
					    collect_first=>UserFirst,
					    collect_loop=>UserParam,
					    collect_next=>UserArg,
					    collect_last=>UserLast
					  }),
	update_counter(kleene, KleeneId),
	name_builder('_kleene~w',[KleeneId],KleeneLabel),
	kleene_conditions(KleeneCond::[From,To],[Counter,CounterInc],InitCond,ExitCond,LoopCond),
	tag_numbervars([Counter,CounterInc,InitCond,ExitCond,LoopCond,KleeneStruct],Id,_),
	tag_numbervars([IL_Out_Right,IL_Out_Left,
			IL_Left2,IL_Right2,
			IL_Left3],Id,_),

	tag_numbervars(Right,Id,_),
	Args1 = [Counter,IL_Left2,_Right],
	Loop_Args1 = [CounterInc,IL_Left3,Right],
	append(Args1,UserParam,Args),
	append(Loop_Args1,UserArg,Loop_Args),
	tupple(Vars,TA),
	tupple(Cont,TCont),
	extend_tupple([IL_Out_Left,IL_Out_Right,IL_Right,UserLast,KleeneCond],TCont,RTCont),
	body_to_lpda(Id, (  %% format('exit il=~w counter=~w\n',[IL_Left2,Counter]),
			     ExitCond,
			     UserLast=UserParam,
			     IL_Left2=IL_Out_Left,
			     IL_Right2=IL_Out_Right			     
			 ),
		     Cont,
		     ContExit,
		     Type),

	VLoop=[IL_Left2,IL_Right2,IL_Left3,IL_Right,IL_Left,IL_Out_Right,IL_Out_Left],
	Kleene_Last= [VLoop^_Loop,
		      Right,
		      _Right]^ '*KLEENE-LAST*'(KleeneLabel,
					       Loop_Args,
					       Args,
					       TA,
					       RTCont,
					       _Loop
					      ),
	
	tag_il_body_to_lpda(Id,
			    A,
			    IL_Left2,IL_Right2,
			    IL_Left3,IL_Right2,
			    Kleene_Last,
			    noop,
			    ContLoop2,
			    Type,
			    Info,
			    Label,
			    Seen),
	body_to_lpda(Id,
		     (
			 %% format('loop il=~w\n',[IL_Left2]),
			 LoopCond
		     ),
		     ContLoop2,
		     ContLoop,
		     Type),


	Trans1 = '*KLEENE*'(KleeneLabel,
			    TA,
			    '*ALTERNATIVE*'(ContExit,ContLoop)
			    ),

	body_to_lpda(Id,
		     IL_Right=IL_Right2,
		     IL_Cont,
		     IL_Cont2,
		     Type),
	
	body_to_lpda(Id,
		     (%%	 format('enter il=~w\n',[IL_Left]),
			 InitCond,
			 UserFirst=UserParam,
			 IL_Left=IL_Left2
			 ),
		     Trans1 :> IL_Cont2,Trans,Type),
	
%%	format('\nIL Kleene ~K\n',[Trans]),
	true
	.

:-std_prolog tag_extract_anchor_guards/2.
%% +pattern: tag_extract_anchor_guards(Tree,Guard)
%% +desc: extract the positive guard of the anchor node when present

tag_extract_anchor_guards(T,AnchorGuard) :-
    ((T=[A|B] ; T=(A,B) ; T = (A ## B) ; T = (A;B)) ->
	 (tag_extract_anchor_guards(A,AnchorGuard) xor tag_extract_anchor_guards(B,AnchorGuard))
     ; T = tag_node{ children => Children::[_|_] } ->
	   tag_extract_anchor_guards(Children,AnchorGuard)
     ; T = guard{ goal => A, plus => Plus } ->
	   (A = tag_node{ kind => anchor } ->
		AnchorGuard = Plus
	    ; tag_extract_anchor_guards(A,AnchorGuard)
	   )
     ;
     fail
    )
.


		


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Reader

directive(D::tag_custom_subst(Pred),_,_,_) :-
	directive_updater( tag_custom_subst(_),
			   tag_custom_subst(Pred) )
	.

directive(D::tag_terminal_cat(Cat),_,_,_) :-
	record_without_doublon(D)
	.

directive(D::tag_subst_cat(Cat,InCat),_,_,_) :-
	record_without_doublon(D)
	.

directive(D::tag_tree_cutter(Cat),_,_,_) :-
%%    format('record directive ~w\n',[D]),
    record_without_doublon(D)
	.
