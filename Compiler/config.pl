/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 1998, 2002, 2004, 2005, 2008, 2013, 2015, 2016 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  config.pl -- Configuration file for DyALog Compiler
 *
 * ----------------------------------------------------------------
 * Description
 * 
 * ----------------------------------------------------------------
 */

:-include 'header.pl'.

compiler_info('dyalog','1999','1.0').

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Internal Symbols

:-extensional internal_atomic/2.

internal_atomic([],'$internal'(0)).
internal_atomic((.),'$internal'(1)).
internal_atomic(({}),'$internal'(2)).
internal_atomic(apply,'$internal'(3)).
internal_atomic((','),'$internal'(4)).
internal_atomic((';'),'$internal'(5)).
internal_atomic('$VAR','$internal'(6)).
internal_atomic('$$HOLE$$','$internal'(7)).
internal_atomic('$SET$','$internal'(8)).
internal_atomic(':>','$internal'(9)).
internal_atomic('$TUPPLE','$internal'(10)).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Foreign Predicates

%% =/2 should be inlined
%% foreign(  (=)/2       , 'Unify_2'     ).

%% Single Valued Builtins

foreign(compiler_info/1,'Compiler_Info_1').

foreign((==)/2,sfol_identical).
foreign(compare/3,'Compare_3').
foreign((<)/2,evpred_lt).
foreign((>)/2,evpred_gt).
foreign((=<)/2,evpred_le).
foreign((>=)/2,evpred_ge).
foreign((=:=)/2,evpred_number_eq).
foreign((=\=)/2,evpred_number_neq).
foreign(is/2,evpred_is).
foreign(number/1,evpred_number).
foreign(float/1,evpred_float).
foreign(atomic/1,evpred_atomic).
foreign(var/1,evpred_variable).
foreign(compound/1,evpred_compound).
foreign(char/1,evpred_char).
foreign(integer/1,evpred_integer).
foreign(atom/1,evpred_atom).
foreign(simple/1,evpred_simple).
foreign(nonvar/1,evpred_notvar).
foreign(ground/1,'Ground_1').
foreign(length/2,evpred_length).
foreign(functor/3,evpred_functor).
foreign(arg/3,evpred_arg).
foreign((=..)/2,evpred_univ).
foreign(make_list/3,'Make_List').
foreign(make_intlist/3,'Make_Interval').
foreign(name/2,'Name_To_Chars').
foreign(atom_chars/2,evpred_atom_to_chars).
foreign(number_chars/2,evpred_number_to_chars).
foreign(atom_number/2,'Atom_To_Number').
foreign(atom_concat/3,'Atom_Concat_3').
foreign(char_code/2,'Char_Code_2').
foreign(copy_term/2,evpred_copy_term).
foreign(term_subsumer/3,'Builtin_Term_Subsumer').
foreign(subsumes_chk/2,'Subsumes_Chk_2').
foreign(numbervars/3,'Numbervars_3').
foreign(copy_numbervars/2,'Copy_Numbervars_2').
foreign('$binder'/2,'Binder_2').
foreign(argv/1,'Os_Argv_1').
foreign(add_load_path/1,'Add_Load_Path_1').
foreign(find_file/2,'Find_File_2').
foreign(absolute_file_name/2,'Absolute_File_Name').
foreign(decompose_file_name/4,'Decompose_File_Name_4').
foreign(access/2,'Os_Access_2').
foreign(cd/1,'Os_Cd_1').
foreign(getwd/1,'Os_Getwd_1').
foreign(getenv/2,'Os_Getenv_2').
foreign(mktmp/2,'Os_Mktemp_2').
foreign(system/2,'Os_System_2').
foreign(shell/2,'Os_Shell_2').
foreign(popen/3,'Os_Popen_3').
foreign(unlink/1,'Os_Unlink_1').
foreign(date_time/6,'Date_Time_6').
foreign(exit/1,'Exit_1').
foreign(current_output/1,'Current_Output_1').
foreign(current_input/1,'Current_Input_1').
foreign(set_input/1,'Set_Input_1').
foreign(set_output/1,'Set_Output_1').
foreign(open/3,'Open_3').
foreign(from_alias_to_stream/2,'From_Alias_To_Stream').
foreign(add_stream_alias/2,'Add_Stream_Alias_2').
foreign(close/1,'Close_1').
foreign(flush_output/1,'Flush_Output_1').
foreign(flush_output/0,'Flush_Output_0').
foreign(stream_prop_file_name/2,'Stream_Prop_File_Name_2').
foreign(stream_prop_mode/2,'Stream_Prop_Mode_2').
foreign(stream_prop_input/1,'Stream_Prop_Input_1').
foreign(stream_prop_output/1,'Stream_Prop_Output_1').
foreign(stream_prop_end_of_stream/2,'Stream_Prop_End_Of_Stream_2').
foreign(stream_prop_eof_action/2,'Stream_Prop_Eof_Action_2').
foreign(stream_prop_reposition/2,'Stream_Prop_Reposition_2').
foreign(stream_prop_type_2/2,'Stream_Prop_Type_2').
foreign(at_end_of_stream/1,'At_End_Of_Stream_1').
foreign(at_end_of_stream/0,'At_End_Of_Stream_0').
foreign(current_alias/2,'Current_Alias_2').
foreign(test_alias_not_assigned/1,'Test_Alias_Not_Assigned_1').
foreign(set_stream_eof_action/2,'Set_Stream_Eof_Action_2').
foreign(get_char/2,'Get_Char_2').
foreign(get_char/1,'Get_Char_1').
%foreign(put_char/2,'Put_Char_2').
%foreign(put_char/1,'Put_Char_1').
foreign(write/2,'Write_2').
foreign(write_c_atom/2,'Write_C_Atom_2').
foreign(display/2,'Display_2').
foreign(writeq/2,'Writeq_2').
foreign(writei/2,'Writei_2').
foreign(write_canonical/2,'Write_Canonical_2').
foreign(nl/1,'Nl_1').
foreign(nl/0,'Nl_0').
foreign(error/1,'Error_1').
foreign(read_term/3,'Read_Term_3').
%foreign(gensym/1,'Gensym_1').
foreign((@=)/2,'Derefterm_Bind_2').
foreign((hilog)/1,'Hilog_1').
foreign(feature/2,'Feature_2').
foreign(feature_intro/2,'Intro_2').
foreign(finite_set/2,'Finite_Set_2').
foreign(subset/2,'Subset_2').
foreign(op/3,'Op_3').
foreign((deref_term)/1,'DerefTermSet_1').
foreign(record/2,'evpred_assert').
foreign(record/1,'evpred_assert_1').
foreign_void_return(record/1).
foreign(erase/1,'evpred_retract').

foreign(skel_id/2,'Skel_Id_2').

foreign(tfs_dlopen/1,'Tfs_Dlopen_1').

foreign(atom_module/3,evpred_atom_to_module).

foreign(module_set/1,'Module_Set_1').
foreign(module_get/1,'Module_Get_1').

foreign(term_range/3,'Term_Range_3').
foreign(shift_range/4,'Shift_Range_4').

foreign(is_fset/1,evpred_fset).
foreign(is_loop/1,evpred_loop).
foreign(is_range/1,evpred_range).

%% Multi Valued Builtins
builtin(domain/2,'Domain_2').
builtin(recorded/2,'Object_2').
builtin(recorded/1,'Object_1').
builtin(item_term/2,'Callret_2').
builtin(forest/6,'Forest_6').
builtin(model!query/3,'Mdl_Query').

builtin(local_table!recorded/2,'Local_Object').

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

:-extensional predicate/2.

predicate('C'/3,builtin).

scanner(Left,Token,Right,'C'(Left,Token,Right)).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% System registers

system_register(top,0).
system_register(layer,1).
system_register(trail,2).
system_register(trans,4).
system_register(trans_k,5).
system_register(item,6).
system_register(item_k,7).
system_register(item_comp,8).
system_register(module,9).
system_register(backptr,10).
system_register(object,11).
system_register(heap,12).
system_register(heap_stop,13).
system_register(strate_level,14).

system_register(cp,15).
system_register(e,16).
system_register(b,17).
system_register(bc,18).
system_register(p,19).

system_register(ip,20).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

