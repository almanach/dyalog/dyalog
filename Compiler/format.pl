/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 1998, 2003 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  format.pl -- Formatted Printer
 *
 * ----------------------------------------------------------------
 * Description
 * 
 * ----------------------------------------------------------------
 */

:-std_prolog format/2,format/3,display_arg/2,display_id/2.
:-std_prolog warning/2,error/2,internal_error/2.
	 
format( Format, Args ) :-
	format([],Format,Args).

format(Stream,Format,Args) :-
        (   '$interface'( 'DyALog_Format'(Stream:term,
                                          Format:ptr,
                                          0'~:char,
                                         X: -char),
                          [return(Rest:ptr)] )
        ->  
            ( X == 0'w   ->  Args=[A|R],write(Stream,A)
            ;   X == 0'a ->  Args = [A|R], display_arg(Stream,A)
            ;   X == 0'x ->  Args = [A|R], display_id(Stream,A)
            ;	X == 0'A ->  Args = [A|R],
	                ( A=[] -> true
			;   A=[A1|A2],
			    display_arg(Stream,A1),
			    every(( domain(_A,A2),
				    put_char(Stream,0',),
				  display_arg(Stream,_A)))
			)
            ;	X == 0'X ->  Args = [A|R],
	               ( A=[] -> true
			;   A=[A1|A2],
			    display_id(Stream,A1),
			    every(( domain(_A,A2),
				    put_char(Stream,0',),
				  display_id(Stream,_A)))
			)
            ;   X == 0'q ->  Args=[A|R],write_c_atom(Stream,A)
            ;   X == 0'k ->  Args=[A|R],writeq(Stream,A)
	    ;	X == 0'K ->  Args=[A|R],writei(Stream,A)
            ;   internal_error('format: not a valid format option ~~~w',[X])
            ),
            format(Stream,Rest,R)
        ;   
            true
        )
        .

/*
format(Stream,Format,Args) :-
	(   '$interface'( 'DyALog_Format'(Stream:term,
					  Format:ptr,
					  0'~:char,
					 X: -char),
			  [return(Rest:ptr)] )
	->
	    ( format_escape(X,Stream,Args,R) ->
		format(Stream,Rest,R)
	    ;	
		internal_error('format: not a valid format option ~~~w or bad args',[X,Args])
	    )
	;   
	    true
	)
	.

:-rec_prolog format_escape/4.

format_escape(0'w,Stream,[A|R],R) :-write(Stream,A).
format_escape(0'q,Stream,[A|R],R) :-write_c_atom(Stream,A).
format_escape(0'k,Stream,[A|R],R) :-writeq(Stream,A).
format_escape(0'a,Stream,[A|R],R) :-display_arg(Stream,A).
format_escape(0'x,Stream,[A|R],R) :-display_id(Stream,A).
format_escape(0'l,Stream,[[_,_],[]|R],R).
format_escape(0'l,Stream,[[Format,Sep],[A1|A2]|R],R) :-
	format(Stream,Format,[A1]),
	every((   domain(A,A2),
		  write(Stream,Sep),
		  format(Stream,Format,[A]) ))
	.
format_escape(0'A,Stream,[[]|R],R).
format_escape(0'A,Stream,[[A1|A2]|R],R) :-
	display_arg(Stream,A1),
	every((   domain(A,A2),
		  put_char(Stream,0',),
		  display_arg(Stream,A) ))
	.
*/

display_id(S,A) :-
	( display_id_aux(A,S) ->
	    true
	;   display_arg_base(A,I,Base) ->
	    format(S,'~w[~w]',[Base,I])
	;   (atom(A) ; number(A) ; float(A)) ->
	    write(S,A)
	;   error('format: not a valid ~~a label ~w\n',[A])
	)
	.

:-rec_prolog display_id_aux/2.

display_id_aux(A::'$fun'(I,_,_),S) :-
	( recorded( named_function(A,Name,_) ) ->
	    write(S,Name)
	;   
	    format(S,'fun~w',[I])
	)
	.
display_id_aux('$label'(L),S) :-write(S,L).
display_id_aux('$reg'(I),S) :- format(S,'R(~w)',[I]).
display_id_aux('$freg'(I),S) :- format(S,'T(~w)',[I]).
display_id_aux(A::'$system'(Name),S) :-
	(   recorded(system_register(Name,I)) xor internal_error('Not a valid system register ~w',[A])),
	format('S(~w)',[I])
	.
display_id_aux(U + V,S) :- write(S,U), display_id(S,V).

display_arg( S, A ) :-
	( display_arg_aux(A,S) -> true
	;   display_arg_base(A,I,Base) ->
	    format(S,'&~w[~w]',[Base,I])
	;   display_arg_base_alt(A,I,Base) ->
	    format('~w(~w)',[Base,I])
	;   number(A) -> format(S,'N(~w)',[A])
	;   float(A) -> format(S,'F(~w)',[A])
	;   char(A)   -> N is A, format(S,'C(~w)',[N])
	;   atom(A) -> write(S,'&'), write(S,A) %write_c_atom(S,A)
	;   error('format: not a valid ~~a argument ~w\n',[A])
	)
	.

:-rec_prolog display_arg_aux/2.

display_arg_aux(A::'$fun'(I,_,_),S) :-
	( recorded( named_function(A,Name,_) ) ->
	    write(S,Name)
	;   
	    format(S,'fun~w',[I])
	)
	.
display_arg_aux('$label'(L),S) :- display_id(S,L).
display_arg_aux('&reg'(I),S) :- format(S,'&R(~w)',[I]).
display_arg_aux('&freg'(I),S) :- format(S,'&T(~w)',[I]).
display_arg_aux('$mem'(I),S) :- format(S,'foreign_bkt_buffer[~w]',[I]).
display_arg_aux(noop,S) :- write( S, '0').
display_arg_aux(A::'$system'(Name),S) :-
	(   recorded(system_register(Name,I))
	xor internal_error('Not a valid system register ~w',[A])),
	format('S(~w)',[I])
	.
display_arg_aux(A::'$tupple'(_,_),S) :- write(S,A).
display_arg_aux('$lterm'(_,Reg,_),S) :-	mutable_read(Reg,T),display_arg(S,T).
display_arg_aux(component(A1,A2),S) :- format(S,'comp ~a ~a',[A1,A2]).
display_arg_aux('$smb'(A),S) :- write_c_atom(S,A).
display_arg_aux(c(I),S) :- write(S,I).
display_arg_aux('$arg'(I),S) :- format(S,'R(~w)',[I]).
display_arg_aux(U + V,S) :- display_arg(S,U), display_id(S,V).

:-extensional display_arg_base/3.

display_arg_base( '$term'(I,_),     I, 'ref'     ).
display_arg_base( '$seed'(I,_),     I, 'seed'    ).
display_arg_base( '&mem'(I),        I, 'foreign_bkt_buffer' ).
		 
:-extensional display_arg_base_alt/3.

display_arg_base_alt( '$VAR'(I),      I, 'V'     ).
display_arg_base_alt( '$internal'(I), I, 'I'  ).
display_arg_base_alt( '$reg'(I),      I, 'R'     ).
display_arg_base_alt( '$freg'(I),     I, 'T'     ).


