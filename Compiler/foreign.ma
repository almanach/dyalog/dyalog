;; Compiler: DyALog 1.14.0
;; File "foreign.pl"


;;------------------ LOADING ------------

c_code local load
	call_c   Dyam_Loading_Set()
	pl_call  fun17(&seed[39],0)
	pl_call  fun17(&seed[42],0)
	pl_call  fun17(&seed[41],0)
	pl_call  fun17(&seed[28],0)
	pl_call  fun17(&seed[27],0)
	pl_call  fun17(&seed[22],0)
	pl_call  fun17(&seed[26],0)
	pl_call  fun17(&seed[21],0)
	pl_call  fun17(&seed[20],0)
	pl_call  fun17(&seed[23],0)
	pl_call  fun17(&seed[25],0)
	pl_call  fun17(&seed[29],0)
	pl_call  fun17(&seed[24],0)
	pl_call  fun17(&seed[32],0)
	pl_call  fun17(&seed[18],0)
	pl_call  fun17(&seed[35],0)
	pl_call  fun17(&seed[16],0)
	pl_call  fun17(&seed[38],0)
	pl_call  fun17(&seed[37],0)
	pl_call  fun17(&seed[36],0)
	pl_call  fun17(&seed[34],0)
	pl_call  fun17(&seed[33],0)
	pl_call  fun17(&seed[19],0)
	pl_call  fun17(&seed[17],0)
	pl_call  fun17(&seed[15],0)
	pl_call  fun17(&seed[14],0)
	pl_call  fun17(&seed[31],0)
	pl_call  fun17(&seed[30],0)
	pl_call  fun17(&seed[40],0)
	pl_call  fun17(&seed[43],0)
	pl_call  fun17(&seed[13],0)
	pl_call  fun17(&seed[12],0)
	pl_call  fun17(&seed[8],0)
	pl_call  fun17(&seed[6],0)
	pl_call  fun17(&seed[5],0)
	pl_call  fun17(&seed[4],0)
	pl_call  fun17(&seed[3],0)
	pl_call  fun17(&seed[2],0)
	pl_call  fun17(&seed[1],0)
	pl_call  fun17(&seed[0],0)
	pl_call  fun17(&seed[9],0)
	pl_call  fun17(&seed[7],0)
	pl_call  fun17(&seed[10],0)
	pl_call  fun17(&seed[11],0)
	call_c   Dyam_Loading_Reset()
	c_ret

pl_code global pred_check_mode_type_3
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(3)
	call_c   Dyam_Choice(fun18)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_univ(V(1),&ref[46])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(2))
	pl_call  pred_check_foreign_mode_1()
	call_c   Dyam_Reg_Load(0,V(3))
	call_c   Dyam_Reg_Deallocate(1)
	pl_jump  pred_check_foreign_type_1()

pl_code global pred_check_foreign_type_1
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	pl_call  Domain_2(V(1),&ref[36])
	call_c   Dyam_Deallocate()
	pl_ret

pl_code global pred_check_foreign_mode_1
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	pl_call  Domain_2(V(1),&ref[0])
	call_c   Dyam_Deallocate()
	pl_ret


;;------------------ VIEWERS ------------

c_code local build_viewers
	c_ret

c_code local build_seed_11
	ret_reg &seed[11]
	call_c   build_ref_1()
	call_c   build_ref_5()
	call_c   Dyam_Seed_Start(&ref[1],&ref[5],I(0),fun2,1)
	call_c   build_ref_4()
	call_c   Dyam_Seed_Add_Comp(&ref[4],fun1,0)
	call_c   Dyam_Seed_End()
	move_ret seed[11]
	c_ret

;; TERM 4: '*GUARD*'(foreign_parse_options([], _B))
c_code local build_ref_4
	ret_reg &ref[4]
	call_c   build_ref_2()
	call_c   build_ref_3()
	call_c   Dyam_Create_Unary(&ref[2],&ref[3])
	move_ret ref[4]
	c_ret

;; TERM 3: foreign_parse_options([], _B)
c_code local build_ref_3
	ret_reg &ref[3]
	call_c   build_ref_281()
	call_c   Dyam_Create_Binary(&ref[281],I(0),V(1))
	move_ret ref[3]
	c_ret

;; TERM 281: foreign_parse_options
c_code local build_ref_281
	ret_reg &ref[281]
	call_c   Dyam_Create_Atom("foreign_parse_options")
	move_ret ref[281]
	c_ret

;; TERM 2: '*GUARD*'
c_code local build_ref_2
	ret_reg &ref[2]
	call_c   Dyam_Create_Atom("*GUARD*")
	move_ret ref[2]
	c_ret

pl_code local fun1
	call_c   build_ref_4()
	call_c   Dyam_Unify_Item(&ref[4])
	fail_ret
	pl_ret

pl_code local fun2
	pl_ret

;; TERM 5: '*GUARD*'(foreign_parse_options([], _B)) :> []
c_code local build_ref_5
	ret_reg &ref[5]
	call_c   build_ref_4()
	call_c   Dyam_Create_Binary(I(9),&ref[4],I(0))
	move_ret ref[5]
	c_ret

;; TERM 1: '*GUARD_CLAUSE*'
c_code local build_ref_1
	ret_reg &ref[1]
	call_c   Dyam_Create_Atom("*GUARD_CLAUSE*")
	move_ret ref[1]
	c_ret

c_code local build_seed_10
	ret_reg &seed[10]
	call_c   build_ref_1()
	call_c   build_ref_8()
	call_c   Dyam_Seed_Start(&ref[1],&ref[8],I(0),fun2,1)
	call_c   build_ref_7()
	call_c   Dyam_Seed_Add_Comp(&ref[7],fun3,0)
	call_c   Dyam_Seed_End()
	move_ret seed[10]
	c_ret

;; TERM 7: '*GUARD*'(foreign_parse_init_load([], _B, noop))
c_code local build_ref_7
	ret_reg &ref[7]
	call_c   build_ref_2()
	call_c   build_ref_6()
	call_c   Dyam_Create_Unary(&ref[2],&ref[6])
	move_ret ref[7]
	c_ret

;; TERM 6: foreign_parse_init_load([], _B, noop)
c_code local build_ref_6
	ret_reg &ref[6]
	call_c   build_ref_282()
	call_c   build_ref_153()
	call_c   Dyam_Term_Start(&ref[282],3)
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(&ref[153])
	call_c   Dyam_Term_End()
	move_ret ref[6]
	c_ret

;; TERM 153: noop
c_code local build_ref_153
	ret_reg &ref[153]
	call_c   Dyam_Create_Atom("noop")
	move_ret ref[153]
	c_ret

;; TERM 282: foreign_parse_init_load
c_code local build_ref_282
	ret_reg &ref[282]
	call_c   Dyam_Create_Atom("foreign_parse_init_load")
	move_ret ref[282]
	c_ret

pl_code local fun3
	call_c   build_ref_7()
	call_c   Dyam_Unify_Item(&ref[7])
	fail_ret
	pl_ret

;; TERM 8: '*GUARD*'(foreign_parse_init_load([], _B, noop)) :> []
c_code local build_ref_8
	ret_reg &ref[8]
	call_c   build_ref_7()
	call_c   Dyam_Create_Binary(I(9),&ref[7],I(0))
	move_ret ref[8]
	c_ret

c_code local build_seed_7
	ret_reg &seed[7]
	call_c   build_ref_1()
	call_c   build_ref_11()
	call_c   Dyam_Seed_Start(&ref[1],&ref[11],I(0),fun2,1)
	call_c   build_ref_10()
	call_c   Dyam_Seed_Add_Comp(&ref[10],fun4,0)
	call_c   Dyam_Seed_End()
	move_ret seed[7]
	c_ret

;; TERM 10: '*GUARD*'(foreign_unify_arg(+, _B, _C, _D, noop))
c_code local build_ref_10
	ret_reg &ref[10]
	call_c   build_ref_2()
	call_c   build_ref_9()
	call_c   Dyam_Create_Unary(&ref[2],&ref[9])
	move_ret ref[10]
	c_ret

;; TERM 9: foreign_unify_arg(+, _B, _C, _D, noop)
c_code local build_ref_9
	ret_reg &ref[9]
	call_c   build_ref_283()
	call_c   build_ref_47()
	call_c   build_ref_153()
	call_c   Dyam_Term_Start(&ref[283],5)
	call_c   Dyam_Term_Arg(&ref[47])
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(&ref[153])
	call_c   Dyam_Term_End()
	move_ret ref[9]
	c_ret

;; TERM 47: +
c_code local build_ref_47
	ret_reg &ref[47]
	call_c   Dyam_Create_Atom("+")
	move_ret ref[47]
	c_ret

;; TERM 283: foreign_unify_arg
c_code local build_ref_283
	ret_reg &ref[283]
	call_c   Dyam_Create_Atom("foreign_unify_arg")
	move_ret ref[283]
	c_ret

pl_code local fun4
	call_c   build_ref_10()
	call_c   Dyam_Unify_Item(&ref[10])
	fail_ret
	pl_ret

;; TERM 11: '*GUARD*'(foreign_unify_arg(+, _B, _C, _D, noop)) :> []
c_code local build_ref_11
	ret_reg &ref[11]
	call_c   build_ref_10()
	call_c   Dyam_Create_Binary(I(9),&ref[10],I(0))
	move_ret ref[11]
	c_ret

c_code local build_seed_9
	ret_reg &seed[9]
	call_c   build_ref_1()
	call_c   build_ref_14()
	call_c   Dyam_Seed_Start(&ref[1],&ref[14],I(0),fun2,1)
	call_c   build_ref_13()
	call_c   Dyam_Seed_Add_Comp(&ref[13],fun5,0)
	call_c   Dyam_Seed_End()
	move_ret seed[9]
	c_ret

;; TERM 13: '*GUARD*'(foreign_parse_template_args([], _B, _C, noop, [], noop))
c_code local build_ref_13
	ret_reg &ref[13]
	call_c   build_ref_2()
	call_c   build_ref_12()
	call_c   Dyam_Create_Unary(&ref[2],&ref[12])
	move_ret ref[13]
	c_ret

;; TERM 12: foreign_parse_template_args([], _B, _C, noop, [], noop)
c_code local build_ref_12
	ret_reg &ref[12]
	call_c   build_ref_284()
	call_c   build_ref_153()
	call_c   Dyam_Term_Start(&ref[284],6)
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(&ref[153])
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(&ref[153])
	call_c   Dyam_Term_End()
	move_ret ref[12]
	c_ret

;; TERM 284: foreign_parse_template_args
c_code local build_ref_284
	ret_reg &ref[284]
	call_c   Dyam_Create_Atom("foreign_parse_template_args")
	move_ret ref[284]
	c_ret

pl_code local fun5
	call_c   build_ref_13()
	call_c   Dyam_Unify_Item(&ref[13])
	fail_ret
	pl_ret

;; TERM 14: '*GUARD*'(foreign_parse_template_args([], _B, _C, noop, [], noop)) :> []
c_code local build_ref_14
	ret_reg &ref[14]
	call_c   build_ref_13()
	call_c   Dyam_Create_Binary(I(9),&ref[13],I(0))
	move_ret ref[14]
	c_ret

c_code local build_seed_0
	ret_reg &seed[0]
	call_c   build_ref_1()
	call_c   build_ref_17()
	call_c   Dyam_Seed_Start(&ref[1],&ref[17],I(0),fun2,1)
	call_c   build_ref_16()
	call_c   Dyam_Seed_Add_Comp(&ref[16],fun6,0)
	call_c   Dyam_Seed_End()
	move_ret seed[0]
	c_ret

;; TERM 16: '*GUARD*'(foreign_unify_arg(-, output, _B, _C, reg_unif_c_output(_C, _B)))
c_code local build_ref_16
	ret_reg &ref[16]
	call_c   build_ref_2()
	call_c   build_ref_15()
	call_c   Dyam_Create_Unary(&ref[2],&ref[15])
	move_ret ref[16]
	c_ret

;; TERM 15: foreign_unify_arg(-, output, _B, _C, reg_unif_c_output(_C, _B))
c_code local build_ref_15
	ret_reg &ref[15]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_286()
	call_c   Dyam_Create_Binary(&ref[286],V(2),V(1))
	move_ret R(0)
	call_c   build_ref_283()
	call_c   build_ref_267()
	call_c   build_ref_285()
	call_c   Dyam_Term_Start(&ref[283],5)
	call_c   Dyam_Term_Arg(&ref[267])
	call_c   Dyam_Term_Arg(&ref[285])
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_End()
	move_ret ref[15]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 285: output
c_code local build_ref_285
	ret_reg &ref[285]
	call_c   Dyam_Create_Atom("output")
	move_ret ref[285]
	c_ret

;; TERM 267: -
c_code local build_ref_267
	ret_reg &ref[267]
	call_c   Dyam_Create_Atom("-")
	move_ret ref[267]
	c_ret

;; TERM 286: reg_unif_c_output
c_code local build_ref_286
	ret_reg &ref[286]
	call_c   Dyam_Create_Atom("reg_unif_c_output")
	move_ret ref[286]
	c_ret

pl_code local fun6
	call_c   build_ref_16()
	call_c   Dyam_Unify_Item(&ref[16])
	fail_ret
	pl_ret

;; TERM 17: '*GUARD*'(foreign_unify_arg(-, output, _B, _C, reg_unif_c_output(_C, _B))) :> []
c_code local build_ref_17
	ret_reg &ref[17]
	call_c   build_ref_16()
	call_c   Dyam_Create_Binary(I(9),&ref[16],I(0))
	move_ret ref[17]
	c_ret

c_code local build_seed_1
	ret_reg &seed[1]
	call_c   build_ref_1()
	call_c   build_ref_20()
	call_c   Dyam_Seed_Start(&ref[1],&ref[20],I(0),fun2,1)
	call_c   build_ref_19()
	call_c   Dyam_Seed_Add_Comp(&ref[19],fun7,0)
	call_c   Dyam_Seed_End()
	move_ret seed[1]
	c_ret

;; TERM 19: '*GUARD*'(foreign_unify_arg(-, input, _B, _C, reg_unif_c_input(_C, _B)))
c_code local build_ref_19
	ret_reg &ref[19]
	call_c   build_ref_2()
	call_c   build_ref_18()
	call_c   Dyam_Create_Unary(&ref[2],&ref[18])
	move_ret ref[19]
	c_ret

;; TERM 18: foreign_unify_arg(-, input, _B, _C, reg_unif_c_input(_C, _B))
c_code local build_ref_18
	ret_reg &ref[18]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_288()
	call_c   Dyam_Create_Binary(&ref[288],V(2),V(1))
	move_ret R(0)
	call_c   build_ref_283()
	call_c   build_ref_267()
	call_c   build_ref_287()
	call_c   Dyam_Term_Start(&ref[283],5)
	call_c   Dyam_Term_Arg(&ref[267])
	call_c   Dyam_Term_Arg(&ref[287])
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_End()
	move_ret ref[18]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 287: input
c_code local build_ref_287
	ret_reg &ref[287]
	call_c   Dyam_Create_Atom("input")
	move_ret ref[287]
	c_ret

;; TERM 288: reg_unif_c_input
c_code local build_ref_288
	ret_reg &ref[288]
	call_c   Dyam_Create_Atom("reg_unif_c_input")
	move_ret ref[288]
	c_ret

pl_code local fun7
	call_c   build_ref_19()
	call_c   Dyam_Unify_Item(&ref[19])
	fail_ret
	pl_ret

;; TERM 20: '*GUARD*'(foreign_unify_arg(-, input, _B, _C, reg_unif_c_input(_C, _B))) :> []
c_code local build_ref_20
	ret_reg &ref[20]
	call_c   build_ref_19()
	call_c   Dyam_Create_Binary(I(9),&ref[19],I(0))
	move_ret ref[20]
	c_ret

c_code local build_seed_2
	ret_reg &seed[2]
	call_c   build_ref_1()
	call_c   build_ref_23()
	call_c   Dyam_Seed_Start(&ref[1],&ref[23],I(0),fun2,1)
	call_c   build_ref_22()
	call_c   Dyam_Seed_Add_Comp(&ref[22],fun8,0)
	call_c   Dyam_Seed_End()
	move_ret seed[2]
	c_ret

;; TERM 22: '*GUARD*'(foreign_unify_arg(-, string, _B, _C, reg_unif_c_string(_C, _B)))
c_code local build_ref_22
	ret_reg &ref[22]
	call_c   build_ref_2()
	call_c   build_ref_21()
	call_c   Dyam_Create_Unary(&ref[2],&ref[21])
	move_ret ref[22]
	c_ret

;; TERM 21: foreign_unify_arg(-, string, _B, _C, reg_unif_c_string(_C, _B))
c_code local build_ref_21
	ret_reg &ref[21]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_290()
	call_c   Dyam_Create_Binary(&ref[290],V(2),V(1))
	move_ret R(0)
	call_c   build_ref_283()
	call_c   build_ref_267()
	call_c   build_ref_289()
	call_c   Dyam_Term_Start(&ref[283],5)
	call_c   Dyam_Term_Arg(&ref[267])
	call_c   Dyam_Term_Arg(&ref[289])
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_End()
	move_ret ref[21]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 289: string
c_code local build_ref_289
	ret_reg &ref[289]
	call_c   Dyam_Create_Atom("string")
	move_ret ref[289]
	c_ret

;; TERM 290: reg_unif_c_string
c_code local build_ref_290
	ret_reg &ref[290]
	call_c   Dyam_Create_Atom("reg_unif_c_string")
	move_ret ref[290]
	c_ret

pl_code local fun8
	call_c   build_ref_22()
	call_c   Dyam_Unify_Item(&ref[22])
	fail_ret
	pl_ret

;; TERM 23: '*GUARD*'(foreign_unify_arg(-, string, _B, _C, reg_unif_c_string(_C, _B))) :> []
c_code local build_ref_23
	ret_reg &ref[23]
	call_c   build_ref_22()
	call_c   Dyam_Create_Binary(I(9),&ref[22],I(0))
	move_ret ref[23]
	c_ret

c_code local build_seed_3
	ret_reg &seed[3]
	call_c   build_ref_1()
	call_c   build_ref_26()
	call_c   Dyam_Seed_Start(&ref[1],&ref[26],I(0),fun2,1)
	call_c   build_ref_25()
	call_c   Dyam_Seed_Add_Comp(&ref[25],fun9,0)
	call_c   Dyam_Seed_End()
	move_ret seed[3]
	c_ret

;; TERM 25: '*GUARD*'(foreign_unify_arg(-, char, _B, _C, reg_unif_c_char(_C, _B)))
c_code local build_ref_25
	ret_reg &ref[25]
	call_c   build_ref_2()
	call_c   build_ref_24()
	call_c   Dyam_Create_Unary(&ref[2],&ref[24])
	move_ret ref[25]
	c_ret

;; TERM 24: foreign_unify_arg(-, char, _B, _C, reg_unif_c_char(_C, _B))
c_code local build_ref_24
	ret_reg &ref[24]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_292()
	call_c   Dyam_Create_Binary(&ref[292],V(2),V(1))
	move_ret R(0)
	call_c   build_ref_283()
	call_c   build_ref_267()
	call_c   build_ref_291()
	call_c   Dyam_Term_Start(&ref[283],5)
	call_c   Dyam_Term_Arg(&ref[267])
	call_c   Dyam_Term_Arg(&ref[291])
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_End()
	move_ret ref[24]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 291: char
c_code local build_ref_291
	ret_reg &ref[291]
	call_c   Dyam_Create_Atom("char")
	move_ret ref[291]
	c_ret

;; TERM 292: reg_unif_c_char
c_code local build_ref_292
	ret_reg &ref[292]
	call_c   Dyam_Create_Atom("reg_unif_c_char")
	move_ret ref[292]
	c_ret

pl_code local fun9
	call_c   build_ref_25()
	call_c   Dyam_Unify_Item(&ref[25])
	fail_ret
	pl_ret

;; TERM 26: '*GUARD*'(foreign_unify_arg(-, char, _B, _C, reg_unif_c_char(_C, _B))) :> []
c_code local build_ref_26
	ret_reg &ref[26]
	call_c   build_ref_25()
	call_c   Dyam_Create_Binary(I(9),&ref[25],I(0))
	move_ret ref[26]
	c_ret

c_code local build_seed_4
	ret_reg &seed[4]
	call_c   build_ref_1()
	call_c   build_ref_29()
	call_c   Dyam_Seed_Start(&ref[1],&ref[29],I(0),fun2,1)
	call_c   build_ref_28()
	call_c   Dyam_Seed_Add_Comp(&ref[28],fun10,0)
	call_c   Dyam_Seed_End()
	move_ret seed[4]
	c_ret

;; TERM 28: '*GUARD*'(foreign_unify_arg(-, ptr, _B, _C, reg_unif_c_ptr(_C, _B)))
c_code local build_ref_28
	ret_reg &ref[28]
	call_c   build_ref_2()
	call_c   build_ref_27()
	call_c   Dyam_Create_Unary(&ref[2],&ref[27])
	move_ret ref[28]
	c_ret

;; TERM 27: foreign_unify_arg(-, ptr, _B, _C, reg_unif_c_ptr(_C, _B))
c_code local build_ref_27
	ret_reg &ref[27]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_294()
	call_c   Dyam_Create_Binary(&ref[294],V(2),V(1))
	move_ret R(0)
	call_c   build_ref_283()
	call_c   build_ref_267()
	call_c   build_ref_293()
	call_c   Dyam_Term_Start(&ref[283],5)
	call_c   Dyam_Term_Arg(&ref[267])
	call_c   Dyam_Term_Arg(&ref[293])
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_End()
	move_ret ref[27]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 293: ptr
c_code local build_ref_293
	ret_reg &ref[293]
	call_c   Dyam_Create_Atom("ptr")
	move_ret ref[293]
	c_ret

;; TERM 294: reg_unif_c_ptr
c_code local build_ref_294
	ret_reg &ref[294]
	call_c   Dyam_Create_Atom("reg_unif_c_ptr")
	move_ret ref[294]
	c_ret

pl_code local fun10
	call_c   build_ref_28()
	call_c   Dyam_Unify_Item(&ref[28])
	fail_ret
	pl_ret

;; TERM 29: '*GUARD*'(foreign_unify_arg(-, ptr, _B, _C, reg_unif_c_ptr(_C, _B))) :> []
c_code local build_ref_29
	ret_reg &ref[29]
	call_c   build_ref_28()
	call_c   Dyam_Create_Binary(I(9),&ref[28],I(0))
	move_ret ref[29]
	c_ret

c_code local build_seed_5
	ret_reg &seed[5]
	call_c   build_ref_1()
	call_c   build_ref_32()
	call_c   Dyam_Seed_Start(&ref[1],&ref[32],I(0),fun2,1)
	call_c   build_ref_31()
	call_c   Dyam_Seed_Add_Comp(&ref[31],fun11,0)
	call_c   Dyam_Seed_End()
	move_ret seed[5]
	c_ret

;; TERM 31: '*GUARD*'(foreign_unify_arg(-, float, _B, _C, reg_unif_c_float(_C, _B)))
c_code local build_ref_31
	ret_reg &ref[31]
	call_c   build_ref_2()
	call_c   build_ref_30()
	call_c   Dyam_Create_Unary(&ref[2],&ref[30])
	move_ret ref[31]
	c_ret

;; TERM 30: foreign_unify_arg(-, float, _B, _C, reg_unif_c_float(_C, _B))
c_code local build_ref_30
	ret_reg &ref[30]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_295()
	call_c   Dyam_Create_Binary(&ref[295],V(2),V(1))
	move_ret R(0)
	call_c   build_ref_283()
	call_c   build_ref_267()
	call_c   build_ref_229()
	call_c   Dyam_Term_Start(&ref[283],5)
	call_c   Dyam_Term_Arg(&ref[267])
	call_c   Dyam_Term_Arg(&ref[229])
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_End()
	move_ret ref[30]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 229: float
c_code local build_ref_229
	ret_reg &ref[229]
	call_c   Dyam_Create_Atom("float")
	move_ret ref[229]
	c_ret

;; TERM 295: reg_unif_c_float
c_code local build_ref_295
	ret_reg &ref[295]
	call_c   Dyam_Create_Atom("reg_unif_c_float")
	move_ret ref[295]
	c_ret

pl_code local fun11
	call_c   build_ref_31()
	call_c   Dyam_Unify_Item(&ref[31])
	fail_ret
	pl_ret

;; TERM 32: '*GUARD*'(foreign_unify_arg(-, float, _B, _C, reg_unif_c_float(_C, _B))) :> []
c_code local build_ref_32
	ret_reg &ref[32]
	call_c   build_ref_31()
	call_c   Dyam_Create_Binary(I(9),&ref[31],I(0))
	move_ret ref[32]
	c_ret

c_code local build_seed_6
	ret_reg &seed[6]
	call_c   build_ref_1()
	call_c   build_ref_35()
	call_c   Dyam_Seed_Start(&ref[1],&ref[35],I(0),fun2,1)
	call_c   build_ref_34()
	call_c   Dyam_Seed_Add_Comp(&ref[34],fun12,0)
	call_c   Dyam_Seed_End()
	move_ret seed[6]
	c_ret

;; TERM 34: '*GUARD*'(foreign_unify_arg(-, int, _B, _C, reg_unif_c_number(_C, _B)))
c_code local build_ref_34
	ret_reg &ref[34]
	call_c   build_ref_2()
	call_c   build_ref_33()
	call_c   Dyam_Create_Unary(&ref[2],&ref[33])
	move_ret ref[34]
	c_ret

;; TERM 33: foreign_unify_arg(-, int, _B, _C, reg_unif_c_number(_C, _B))
c_code local build_ref_33
	ret_reg &ref[33]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_297()
	call_c   Dyam_Create_Binary(&ref[297],V(2),V(1))
	move_ret R(0)
	call_c   build_ref_283()
	call_c   build_ref_267()
	call_c   build_ref_296()
	call_c   Dyam_Term_Start(&ref[283],5)
	call_c   Dyam_Term_Arg(&ref[267])
	call_c   Dyam_Term_Arg(&ref[296])
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_End()
	move_ret ref[33]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 296: int
c_code local build_ref_296
	ret_reg &ref[296]
	call_c   Dyam_Create_Atom("int")
	move_ret ref[296]
	c_ret

;; TERM 297: reg_unif_c_number
c_code local build_ref_297
	ret_reg &ref[297]
	call_c   Dyam_Create_Atom("reg_unif_c_number")
	move_ret ref[297]
	c_ret

pl_code local fun12
	call_c   build_ref_34()
	call_c   Dyam_Unify_Item(&ref[34])
	fail_ret
	pl_ret

;; TERM 35: '*GUARD*'(foreign_unify_arg(-, int, _B, _C, reg_unif_c_number(_C, _B))) :> []
c_code local build_ref_35
	ret_reg &ref[35]
	call_c   build_ref_34()
	call_c   Dyam_Create_Binary(I(9),&ref[34],I(0))
	move_ret ref[35]
	c_ret

c_code local build_seed_8
	ret_reg &seed[8]
	call_c   build_ref_1()
	call_c   build_ref_39()
	call_c   Dyam_Seed_Start(&ref[1],&ref[39],I(0),fun2,1)
	call_c   build_ref_38()
	call_c   Dyam_Seed_Add_Comp(&ref[38],fun14,0)
	call_c   Dyam_Seed_End()
	move_ret seed[8]
	c_ret

;; TERM 38: '*GUARD*'(foreign_load_arg(-, term, _B, _C, noop, _D, _E))
c_code local build_ref_38
	ret_reg &ref[38]
	call_c   build_ref_2()
	call_c   build_ref_37()
	call_c   Dyam_Create_Unary(&ref[2],&ref[37])
	move_ret ref[38]
	c_ret

;; TERM 37: foreign_load_arg(-, term, _B, _C, noop, _D, _E)
c_code local build_ref_37
	ret_reg &ref[37]
	call_c   build_ref_298()
	call_c   build_ref_267()
	call_c   build_ref_276()
	call_c   build_ref_153()
	call_c   Dyam_Term_Start(&ref[298],7)
	call_c   Dyam_Term_Arg(&ref[267])
	call_c   Dyam_Term_Arg(&ref[276])
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(&ref[153])
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[37]
	c_ret

;; TERM 276: term
c_code local build_ref_276
	ret_reg &ref[276]
	call_c   Dyam_Create_Atom("term")
	move_ret ref[276]
	c_ret

;; TERM 298: foreign_load_arg
c_code local build_ref_298
	ret_reg &ref[298]
	call_c   Dyam_Create_Atom("foreign_load_arg")
	move_ret ref[298]
	c_ret

pl_code local fun14
	call_c   build_ref_38()
	call_c   Dyam_Unify_Item(&ref[38])
	fail_ret
	pl_ret

;; TERM 39: '*GUARD*'(foreign_load_arg(-, term, _B, _C, noop, _D, _E)) :> []
c_code local build_ref_39
	ret_reg &ref[39]
	call_c   build_ref_38()
	call_c   Dyam_Create_Binary(I(9),&ref[38],I(0))
	move_ret ref[39]
	c_ret

c_code local build_seed_12
	ret_reg &seed[12]
	call_c   build_ref_1()
	call_c   build_ref_42()
	call_c   Dyam_Seed_Start(&ref[1],&ref[42],I(0),fun2,1)
	call_c   build_ref_41()
	call_c   Dyam_Seed_Add_Comp(&ref[41],fun15,0)
	call_c   Dyam_Seed_End()
	move_ret seed[12]
	c_ret

;; TERM 41: '*GUARD*'(foreign_unify_arg(-, term, _B, _C, reg_unif(_C, _D)))
c_code local build_ref_41
	ret_reg &ref[41]
	call_c   build_ref_2()
	call_c   build_ref_40()
	call_c   Dyam_Create_Unary(&ref[2],&ref[40])
	move_ret ref[41]
	c_ret

;; TERM 40: foreign_unify_arg(-, term, _B, _C, reg_unif(_C, _D))
c_code local build_ref_40
	ret_reg &ref[40]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_299()
	call_c   Dyam_Create_Binary(&ref[299],V(2),V(3))
	move_ret R(0)
	call_c   build_ref_283()
	call_c   build_ref_267()
	call_c   build_ref_276()
	call_c   Dyam_Term_Start(&ref[283],5)
	call_c   Dyam_Term_Arg(&ref[267])
	call_c   Dyam_Term_Arg(&ref[276])
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_End()
	move_ret ref[40]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 299: reg_unif
c_code local build_ref_299
	ret_reg &ref[299]
	call_c   Dyam_Create_Atom("reg_unif")
	move_ret ref[299]
	c_ret

pl_code local fun15
	call_c   build_ref_41()
	call_c   Dyam_Unify_Item(&ref[41])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(3))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_label_term_2()

;; TERM 42: '*GUARD*'(foreign_unify_arg(-, term, _B, _C, reg_unif(_C, _D))) :> []
c_code local build_ref_42
	ret_reg &ref[42]
	call_c   build_ref_41()
	call_c   Dyam_Create_Binary(I(9),&ref[41],I(0))
	move_ret ref[42]
	c_ret

c_code local build_seed_13
	ret_reg &seed[13]
	call_c   build_ref_1()
	call_c   build_ref_45()
	call_c   Dyam_Seed_Start(&ref[1],&ref[45],I(0),fun2,1)
	call_c   build_ref_44()
	call_c   Dyam_Seed_Add_Comp(&ref[44],fun16,0)
	call_c   Dyam_Seed_End()
	move_ret seed[13]
	c_ret

;; TERM 44: '*GUARD*'(foreign_unify_arg(-, bool, _B, _C, reg_unif_c_boolean(_C, _D)))
c_code local build_ref_44
	ret_reg &ref[44]
	call_c   build_ref_2()
	call_c   build_ref_43()
	call_c   Dyam_Create_Unary(&ref[2],&ref[43])
	move_ret ref[44]
	c_ret

;; TERM 43: foreign_unify_arg(-, bool, _B, _C, reg_unif_c_boolean(_C, _D))
c_code local build_ref_43
	ret_reg &ref[43]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_300()
	call_c   Dyam_Create_Binary(&ref[300],V(2),V(3))
	move_ret R(0)
	call_c   build_ref_283()
	call_c   build_ref_267()
	call_c   build_ref_57()
	call_c   Dyam_Term_Start(&ref[283],5)
	call_c   Dyam_Term_Arg(&ref[267])
	call_c   Dyam_Term_Arg(&ref[57])
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_End()
	move_ret ref[43]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 57: bool
c_code local build_ref_57
	ret_reg &ref[57]
	call_c   Dyam_Create_Atom("bool")
	move_ret ref[57]
	c_ret

;; TERM 300: reg_unif_c_boolean
c_code local build_ref_300
	ret_reg &ref[300]
	call_c   Dyam_Create_Atom("reg_unif_c_boolean")
	move_ret ref[300]
	c_ret

pl_code local fun16
	call_c   build_ref_44()
	call_c   Dyam_Unify_Item(&ref[44])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(3))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_label_term_2()

;; TERM 45: '*GUARD*'(foreign_unify_arg(-, bool, _B, _C, reg_unif_c_boolean(_C, _D))) :> []
c_code local build_ref_45
	ret_reg &ref[45]
	call_c   build_ref_44()
	call_c   Dyam_Create_Binary(I(9),&ref[44],I(0))
	move_ret ref[45]
	c_ret

c_code local build_seed_43
	ret_reg &seed[43]
	call_c   build_ref_48()
	call_c   build_ref_52()
	call_c   Dyam_Seed_Start(&ref[48],&ref[52],I(0),fun2,1)
	call_c   build_ref_53()
	call_c   Dyam_Seed_Add_Comp(&ref[53],fun28,0)
	call_c   Dyam_Seed_End()
	move_ret seed[43]
	c_ret

;; TERM 53: '*PROLOG-ITEM*'{top=> unfold('$interface'(_B, _C), _D, _E, _F, _D), cont=> _A}
c_code local build_ref_53
	ret_reg &ref[53]
	call_c   build_ref_301()
	call_c   build_ref_50()
	call_c   Dyam_Create_Binary(&ref[301],&ref[50],V(0))
	move_ret ref[53]
	c_ret

;; TERM 50: unfold('$interface'(_B, _C), _D, _E, _F, _D)
c_code local build_ref_50
	ret_reg &ref[50]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_303()
	call_c   Dyam_Create_Binary(&ref[303],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_302()
	call_c   Dyam_Term_Start(&ref[302],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[50]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 302: unfold
c_code local build_ref_302
	ret_reg &ref[302]
	call_c   Dyam_Create_Atom("unfold")
	move_ret ref[302]
	c_ret

;; TERM 303: '$interface'
c_code local build_ref_303
	ret_reg &ref[303]
	call_c   Dyam_Create_Atom("$interface")
	move_ret ref[303]
	c_ret

;; TERM 301: '*PROLOG-ITEM*'!'$ft'
c_code local build_ref_301
	ret_reg &ref[301]
	call_c   build_ref_304()
	call_c   Dyam_Create_Atom_Module("$ft",&ref[304])
	move_ret ref[301]
	c_ret

;; TERM 304: '*PROLOG-ITEM*'
c_code local build_ref_304
	ret_reg &ref[304]
	call_c   Dyam_Create_Atom("*PROLOG-ITEM*")
	move_ret ref[304]
	c_ret

c_code local build_seed_44
	ret_reg &seed[44]
	call_c   build_ref_2()
	call_c   build_ref_55()
	call_c   Dyam_Seed_Start(&ref[2],&ref[55],I(0),fun20,1)
	call_c   build_ref_56()
	call_c   Dyam_Seed_Add_Comp(&ref[56],&ref[55],0)
	call_c   Dyam_Seed_End()
	move_ret seed[44]
	c_ret

pl_code local fun20
	pl_jump  Complete(0,0)

;; TERM 56: '*GUARD*'(foreign_parse_options(_C, interface{return=> _G, name=> _H, code=> _F, choice_size=> _I, load=> _J})) :> '$$HOLE$$'
c_code local build_ref_56
	ret_reg &ref[56]
	call_c   build_ref_55()
	call_c   Dyam_Create_Binary(I(9),&ref[55],I(7))
	move_ret ref[56]
	c_ret

;; TERM 55: '*GUARD*'(foreign_parse_options(_C, interface{return=> _G, name=> _H, code=> _F, choice_size=> _I, load=> _J}))
c_code local build_ref_55
	ret_reg &ref[55]
	call_c   build_ref_2()
	call_c   build_ref_54()
	call_c   Dyam_Create_Unary(&ref[2],&ref[54])
	move_ret ref[55]
	c_ret

;; TERM 54: foreign_parse_options(_C, interface{return=> _G, name=> _H, code=> _F, choice_size=> _I, load=> _J})
c_code local build_ref_54
	ret_reg &ref[54]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_305()
	call_c   Dyam_Term_Start(&ref[305],5)
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_281()
	call_c   Dyam_Create_Binary(&ref[281],V(2),R(0))
	move_ret ref[54]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 305: interface!'$ft'
c_code local build_ref_305
	ret_reg &ref[305]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_311()
	call_c   Dyam_Create_List(&ref[311],I(0))
	move_ret R(0)
	call_c   build_ref_310()
	call_c   Dyam_Create_List(&ref[310],R(0))
	move_ret R(0)
	call_c   build_ref_309()
	call_c   Dyam_Create_List(&ref[309],R(0))
	move_ret R(0)
	call_c   build_ref_308()
	call_c   Dyam_Create_List(&ref[308],R(0))
	move_ret R(0)
	call_c   build_ref_307()
	call_c   Dyam_Create_List(&ref[307],R(0))
	move_ret R(0)
	call_c   build_ref_306()
	call_c   Dyam_Create_Atom_Module("$ft",&ref[306])
	move_ret ref[305]
	call_c   DYAM_Feature_2(&ref[306],R(0))
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 306: interface
c_code local build_ref_306
	ret_reg &ref[306]
	call_c   Dyam_Create_Atom("interface")
	move_ret ref[306]
	c_ret

;; TERM 307: return
c_code local build_ref_307
	ret_reg &ref[307]
	call_c   Dyam_Create_Atom("return")
	move_ret ref[307]
	c_ret

;; TERM 308: name
c_code local build_ref_308
	ret_reg &ref[308]
	call_c   Dyam_Create_Atom("name")
	move_ret ref[308]
	c_ret

;; TERM 309: code
c_code local build_ref_309
	ret_reg &ref[309]
	call_c   Dyam_Create_Atom("code")
	move_ret ref[309]
	c_ret

;; TERM 310: choice_size
c_code local build_ref_310
	ret_reg &ref[310]
	call_c   Dyam_Create_Atom("choice_size")
	move_ret ref[310]
	c_ret

;; TERM 311: load
c_code local build_ref_311
	ret_reg &ref[311]
	call_c   Dyam_Create_Atom("load")
	move_ret ref[311]
	c_ret

pl_code local fun21
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Light_Object(R(0))
	pl_jump  Schedule_Prolog()

c_code local build_seed_45
	ret_reg &seed[45]
	call_c   build_ref_2()
	call_c   build_ref_60()
	call_c   Dyam_Seed_Start(&ref[2],&ref[60],I(0),fun20,1)
	call_c   build_ref_61()
	call_c   Dyam_Seed_Add_Comp(&ref[61],&ref[60],0)
	call_c   Dyam_Seed_End()
	move_ret seed[45]
	c_ret

;; TERM 61: '*GUARD*'(foreign_parse_template(_B, interface{return=> _G, name=> _H, code=> _F, choice_size=> _I, load=> _J}, _E)) :> '$$HOLE$$'
c_code local build_ref_61
	ret_reg &ref[61]
	call_c   build_ref_60()
	call_c   Dyam_Create_Binary(I(9),&ref[60],I(7))
	move_ret ref[61]
	c_ret

;; TERM 60: '*GUARD*'(foreign_parse_template(_B, interface{return=> _G, name=> _H, code=> _F, choice_size=> _I, load=> _J}, _E))
c_code local build_ref_60
	ret_reg &ref[60]
	call_c   build_ref_2()
	call_c   build_ref_59()
	call_c   Dyam_Create_Unary(&ref[2],&ref[59])
	move_ret ref[60]
	c_ret

;; TERM 59: foreign_parse_template(_B, interface{return=> _G, name=> _H, code=> _F, choice_size=> _I, load=> _J}, _E)
c_code local build_ref_59
	ret_reg &ref[59]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_305()
	call_c   Dyam_Term_Start(&ref[305],5)
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_312()
	call_c   Dyam_Term_Start(&ref[312],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[59]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 312: foreign_parse_template
c_code local build_ref_312
	ret_reg &ref[312]
	call_c   Dyam_Create_Atom("foreign_parse_template")
	move_ret ref[312]
	c_ret

long local pool_fun22[2]=[1,build_seed_45]

pl_code local fun23
	call_c   Dyam_Remove_Choice()
fun22:
	pl_call  fun21(&seed[45],1)
	call_c   Dyam_Deallocate()
	pl_jump  Follow_Cont(V(0))


long local pool_fun24[3]=[131072,pool_fun22,pool_fun22]

pl_code local fun25
	call_c   Dyam_Remove_Choice()
fun24:
	call_c   Dyam_Choice(fun23)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(9),I(0))
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun22()


;; TERM 58: none
c_code local build_ref_58
	ret_reg &ref[58]
	call_c   Dyam_Create_Atom("none")
	move_ret ref[58]
	c_ret

long local pool_fun26[4]=[131073,build_ref_58,pool_fun24,pool_fun24]

pl_code local fun27
	call_c   Dyam_Remove_Choice()
fun26:
	call_c   Dyam_Choice(fun25)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(8),&ref[58])
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun24()


long local pool_fun28[6]=[131075,build_ref_53,build_seed_44,build_ref_57,pool_fun26,pool_fun26]

pl_code local fun28
	call_c   Dyam_Pool(pool_fun28)
	call_c   Dyam_Unify_Item(&ref[53])
	fail_ret
	call_c   Dyam_Allocate(0)
	pl_call  fun21(&seed[44],1)
	call_c   Dyam_Choice(fun27)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(6),&ref[57])
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun26()

;; TERM 52: '*PROLOG-FIRST*'(unfold('$interface'(_B, _C), _D, _E, _F, _D)) :> []
c_code local build_ref_52
	ret_reg &ref[52]
	call_c   build_ref_51()
	call_c   Dyam_Create_Binary(I(9),&ref[51],I(0))
	move_ret ref[52]
	c_ret

;; TERM 51: '*PROLOG-FIRST*'(unfold('$interface'(_B, _C), _D, _E, _F, _D))
c_code local build_ref_51
	ret_reg &ref[51]
	call_c   build_ref_49()
	call_c   build_ref_50()
	call_c   Dyam_Create_Unary(&ref[49],&ref[50])
	move_ret ref[51]
	c_ret

;; TERM 49: '*PROLOG-FIRST*'
c_code local build_ref_49
	ret_reg &ref[49]
	call_c   Dyam_Create_Atom("*PROLOG-FIRST*")
	move_ret ref[49]
	c_ret

;; TERM 48: '*PROLOG_FIRST*'
c_code local build_ref_48
	ret_reg &ref[48]
	call_c   Dyam_Create_Atom("*PROLOG_FIRST*")
	move_ret ref[48]
	c_ret

c_code local build_seed_40
	ret_reg &seed[40]
	call_c   build_ref_1()
	call_c   build_ref_64()
	call_c   Dyam_Seed_Start(&ref[1],&ref[64],I(0),fun2,1)
	call_c   build_ref_63()
	call_c   Dyam_Seed_Add_Comp(&ref[63],fun31,0)
	call_c   Dyam_Seed_End()
	move_ret seed[40]
	c_ret

;; TERM 63: '*GUARD*'(foreign_parse_init_load([_B|_C], _D, (_E :> _F)))
c_code local build_ref_63
	ret_reg &ref[63]
	call_c   build_ref_2()
	call_c   build_ref_62()
	call_c   Dyam_Create_Unary(&ref[2],&ref[62])
	move_ret ref[63]
	c_ret

;; TERM 62: foreign_parse_init_load([_B|_C], _D, (_E :> _F))
c_code local build_ref_62
	ret_reg &ref[62]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   Dyam_Create_List(V(1),V(2))
	move_ret R(0)
	call_c   Dyam_Create_Binary(I(9),V(4),V(5))
	move_ret R(1)
	call_c   build_ref_282()
	call_c   Dyam_Term_Start(&ref[282],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_End()
	move_ret ref[62]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 75: 'foreign load ~w'
c_code local build_ref_75
	ret_reg &ref[75]
	call_c   Dyam_Create_Atom("foreign load ~w")
	move_ret ref[75]
	c_ret

;; TERM 76: [_B]
c_code local build_ref_76
	ret_reg &ref[76]
	call_c   Dyam_Create_List(V(1),I(0))
	move_ret ref[76]
	c_ret

;; TERM 67: 2 * _D
c_code local build_ref_67
	ret_reg &ref[67]
	call_c   build_ref_66()
	call_c   Dyam_Create_Binary(&ref[66],N(2),V(3))
	move_ret ref[67]
	c_ret

;; TERM 66: *
c_code local build_ref_66
	ret_reg &ref[66]
	call_c   Dyam_Create_Atom("*")
	move_ret ref[66]
	c_ret

c_code local build_seed_46
	ret_reg &seed[46]
	call_c   build_ref_2()
	call_c   build_ref_69()
	call_c   Dyam_Seed_Start(&ref[2],&ref[69],I(0),fun20,1)
	call_c   build_ref_70()
	call_c   Dyam_Seed_Add_Comp(&ref[70],&ref[69],0)
	call_c   Dyam_Seed_End()
	move_ret seed[46]
	c_ret

;; TERM 70: '*GUARD*'(foreign_load_init_arg(_H, _G, _I, _E, _J)) :> '$$HOLE$$'
c_code local build_ref_70
	ret_reg &ref[70]
	call_c   build_ref_69()
	call_c   Dyam_Create_Binary(I(9),&ref[69],I(7))
	move_ret ref[70]
	c_ret

;; TERM 69: '*GUARD*'(foreign_load_init_arg(_H, _G, _I, _E, _J))
c_code local build_ref_69
	ret_reg &ref[69]
	call_c   build_ref_2()
	call_c   build_ref_68()
	call_c   Dyam_Create_Unary(&ref[2],&ref[68])
	move_ret ref[69]
	c_ret

;; TERM 68: foreign_load_init_arg(_H, _G, _I, _E, _J)
c_code local build_ref_68
	ret_reg &ref[68]
	call_c   build_ref_313()
	call_c   Dyam_Term_Start(&ref[313],5)
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret ref[68]
	c_ret

;; TERM 313: foreign_load_init_arg
c_code local build_ref_313
	ret_reg &ref[313]
	call_c   Dyam_Create_Atom("foreign_load_init_arg")
	move_ret ref[313]
	c_ret

;; TERM 71: _D + _J
c_code local build_ref_71
	ret_reg &ref[71]
	call_c   build_ref_47()
	call_c   Dyam_Create_Binary(&ref[47],V(3),V(9))
	move_ret ref[71]
	c_ret

c_code local build_seed_47
	ret_reg &seed[47]
	call_c   build_ref_2()
	call_c   build_ref_73()
	call_c   Dyam_Seed_Start(&ref[2],&ref[73],I(0),fun20,1)
	call_c   build_ref_74()
	call_c   Dyam_Seed_Add_Comp(&ref[74],&ref[73],0)
	call_c   Dyam_Seed_End()
	move_ret seed[47]
	c_ret

;; TERM 74: '*GUARD*'(foreign_parse_init_load(_C, _K, _F)) :> '$$HOLE$$'
c_code local build_ref_74
	ret_reg &ref[74]
	call_c   build_ref_73()
	call_c   Dyam_Create_Binary(I(9),&ref[73],I(7))
	move_ret ref[74]
	c_ret

;; TERM 73: '*GUARD*'(foreign_parse_init_load(_C, _K, _F))
c_code local build_ref_73
	ret_reg &ref[73]
	call_c   build_ref_2()
	call_c   build_ref_72()
	call_c   Dyam_Create_Unary(&ref[2],&ref[72])
	move_ret ref[73]
	c_ret

;; TERM 72: foreign_parse_init_load(_C, _K, _F)
c_code local build_ref_72
	ret_reg &ref[72]
	call_c   build_ref_282()
	call_c   Dyam_Term_Start(&ref[282],3)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[72]
	c_ret

long local pool_fun29[5]=[4,build_ref_67,build_seed_46,build_ref_71,build_seed_47]

long local pool_fun30[4]=[65538,build_ref_75,build_ref_76,pool_fun29]

pl_code local fun30
	call_c   Dyam_Remove_Choice()
	move     &ref[75], R(0)
	move     0, R(1)
	move     &ref[76], R(2)
	move     S(5), R(3)
	pl_call  pred_error_2()
fun29:
	call_c   DYAM_evpred_is(V(8),&ref[67])
	fail_ret
	pl_call  fun21(&seed[46],1)
	call_c   DYAM_evpred_is(V(10),&ref[71])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_jump  fun21(&seed[47],1)


;; TERM 65: _G : _H
c_code local build_ref_65
	ret_reg &ref[65]
	call_c   build_ref_314()
	call_c   Dyam_Create_Binary(&ref[314],V(6),V(7))
	move_ret ref[65]
	c_ret

;; TERM 314: :
c_code local build_ref_314
	ret_reg &ref[314]
	call_c   Dyam_Create_Atom(":")
	move_ret ref[314]
	c_ret

long local pool_fun31[5]=[131074,build_ref_63,build_ref_65,pool_fun30,pool_fun29]

pl_code local fun31
	call_c   Dyam_Pool(pool_fun31)
	call_c   Dyam_Unify_Item(&ref[63])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun30)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[65])
	fail_ret
	call_c   Dyam_Reg_Load(0,V(7))
	pl_call  pred_check_foreign_type_1()
	call_c   Dyam_Cut()
	pl_jump  fun29()

;; TERM 64: '*GUARD*'(foreign_parse_init_load([_B|_C], _D, (_E :> _F))) :> []
c_code local build_ref_64
	ret_reg &ref[64]
	call_c   build_ref_63()
	call_c   Dyam_Create_Binary(I(9),&ref[63],I(0))
	move_ret ref[64]
	c_ret

c_code local build_seed_30
	ret_reg &seed[30]
	call_c   build_ref_1()
	call_c   build_ref_79()
	call_c   Dyam_Seed_Start(&ref[1],&ref[79],I(0),fun2,1)
	call_c   build_ref_78()
	call_c   Dyam_Seed_Add_Comp(&ref[78],fun37,0)
	call_c   Dyam_Seed_End()
	move_ret seed[30]
	c_ret

;; TERM 78: '*GUARD*'(foreign_load_init_arg(output, _B, _C, _D, 1))
c_code local build_ref_78
	ret_reg &ref[78]
	call_c   build_ref_2()
	call_c   build_ref_77()
	call_c   Dyam_Create_Unary(&ref[2],&ref[77])
	move_ret ref[78]
	c_ret

;; TERM 77: foreign_load_init_arg(output, _B, _C, _D, 1)
c_code local build_ref_77
	ret_reg &ref[77]
	call_c   build_ref_313()
	call_c   build_ref_285()
	call_c   Dyam_Term_Start(&ref[313],5)
	call_c   Dyam_Term_Arg(&ref[285])
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(N(1))
	call_c   Dyam_Term_End()
	move_ret ref[77]
	c_ret

;; TERM 81: '~w output expected'
c_code local build_ref_81
	ret_reg &ref[81]
	call_c   Dyam_Create_Atom("~w output expected")
	move_ret ref[81]
	c_ret

long local pool_fun36[3]=[2,build_ref_81,build_ref_76]

pl_code local fun36
	call_c   Dyam_Remove_Choice()
	move     &ref[81], R(0)
	move     0, R(1)
	move     &ref[76], R(2)
	move     S(5), R(3)
	pl_call  pred_error_2()
fun32:
	call_c   Dyam_Deallocate()
	pl_ret


;; TERM 80: mem_load_output(_C, _E)
c_code local build_ref_80
	ret_reg &ref[80]
	call_c   build_ref_315()
	call_c   Dyam_Create_Binary(&ref[315],V(2),V(4))
	move_ret ref[80]
	c_ret

;; TERM 315: mem_load_output
c_code local build_ref_315
	ret_reg &ref[315]
	call_c   Dyam_Create_Atom("mem_load_output")
	move_ret ref[315]
	c_ret

long local pool_fun33[2]=[1,build_ref_80]

pl_code local fun34
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Reg_Load(0,V(1))
	pl_call  pred_check_var_1()
fun33:
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(1))
	move     V(4), R(2)
	move     S(5), R(3)
	pl_call  pred_label_term_2()
	call_c   Dyam_Unify(V(3),&ref[80])
	fail_ret
	pl_jump  fun32()


long local pool_fun35[3]=[131072,pool_fun33,pool_fun33]

pl_code local fun35
	call_c   Dyam_Update_Choice(fun34)
	call_c   DYAM_evpred_number(V(1))
	fail_ret
	pl_jump  fun33()

long local pool_fun37[5]=[196609,build_ref_78,pool_fun36,pool_fun35,pool_fun33]

pl_code local fun37
	call_c   Dyam_Pool(pool_fun37)
	call_c   Dyam_Unify_Item(&ref[78])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun36)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Choice(fun35)
	call_c   DYAM_evpred_atom(V(1))
	fail_ret
	pl_jump  fun33()

;; TERM 79: '*GUARD*'(foreign_load_init_arg(output, _B, _C, _D, 1)) :> []
c_code local build_ref_79
	ret_reg &ref[79]
	call_c   build_ref_78()
	call_c   Dyam_Create_Binary(I(9),&ref[78],I(0))
	move_ret ref[79]
	c_ret

c_code local build_seed_31
	ret_reg &seed[31]
	call_c   build_ref_1()
	call_c   build_ref_84()
	call_c   Dyam_Seed_Start(&ref[1],&ref[84],I(0),fun2,1)
	call_c   build_ref_83()
	call_c   Dyam_Seed_Add_Comp(&ref[83],fun42,0)
	call_c   Dyam_Seed_End()
	move_ret seed[31]
	c_ret

;; TERM 83: '*GUARD*'(foreign_load_init_arg(input, _B, _C, _D, 1))
c_code local build_ref_83
	ret_reg &ref[83]
	call_c   build_ref_2()
	call_c   build_ref_82()
	call_c   Dyam_Create_Unary(&ref[2],&ref[82])
	move_ret ref[83]
	c_ret

;; TERM 82: foreign_load_init_arg(input, _B, _C, _D, 1)
c_code local build_ref_82
	ret_reg &ref[82]
	call_c   build_ref_313()
	call_c   build_ref_287()
	call_c   Dyam_Term_Start(&ref[313],5)
	call_c   Dyam_Term_Arg(&ref[287])
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(N(1))
	call_c   Dyam_Term_End()
	move_ret ref[82]
	c_ret

;; TERM 86: '~w input expected'
c_code local build_ref_86
	ret_reg &ref[86]
	call_c   Dyam_Create_Atom("~w input expected")
	move_ret ref[86]
	c_ret

long local pool_fun41[3]=[2,build_ref_86,build_ref_76]

pl_code local fun41
	call_c   Dyam_Remove_Choice()
	move     &ref[86], R(0)
	move     0, R(1)
	move     &ref[76], R(2)
	move     S(5), R(3)
	pl_call  pred_error_2()
	pl_jump  fun32()

;; TERM 85: mem_load_input(_C, _E)
c_code local build_ref_85
	ret_reg &ref[85]
	call_c   build_ref_316()
	call_c   Dyam_Create_Binary(&ref[316],V(2),V(4))
	move_ret ref[85]
	c_ret

;; TERM 316: mem_load_input
c_code local build_ref_316
	ret_reg &ref[316]
	call_c   Dyam_Create_Atom("mem_load_input")
	move_ret ref[316]
	c_ret

long local pool_fun38[2]=[1,build_ref_85]

pl_code local fun39
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Reg_Load(0,V(1))
	pl_call  pred_check_var_1()
fun38:
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(1))
	move     V(4), R(2)
	move     S(5), R(3)
	pl_call  pred_label_term_2()
	call_c   Dyam_Unify(V(3),&ref[85])
	fail_ret
	pl_jump  fun32()


long local pool_fun40[3]=[131072,pool_fun38,pool_fun38]

pl_code local fun40
	call_c   Dyam_Update_Choice(fun39)
	call_c   DYAM_evpred_number(V(1))
	fail_ret
	pl_jump  fun38()

long local pool_fun42[5]=[196609,build_ref_83,pool_fun41,pool_fun40,pool_fun38]

pl_code local fun42
	call_c   Dyam_Pool(pool_fun42)
	call_c   Dyam_Unify_Item(&ref[83])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun41)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Choice(fun40)
	call_c   DYAM_evpred_atom(V(1))
	fail_ret
	pl_jump  fun38()

;; TERM 84: '*GUARD*'(foreign_load_init_arg(input, _B, _C, _D, 1)) :> []
c_code local build_ref_84
	ret_reg &ref[84]
	call_c   build_ref_83()
	call_c   Dyam_Create_Binary(I(9),&ref[83],I(0))
	move_ret ref[84]
	c_ret

c_code local build_seed_14
	ret_reg &seed[14]
	call_c   build_ref_1()
	call_c   build_ref_89()
	call_c   Dyam_Seed_Start(&ref[1],&ref[89],I(0),fun2,1)
	call_c   build_ref_88()
	call_c   Dyam_Seed_Add_Comp(&ref[88],fun45,0)
	call_c   Dyam_Seed_End()
	move_ret seed[14]
	c_ret

;; TERM 88: '*GUARD*'(foreign_load_arg(-, string, _B, _C, noop, _D, _E))
c_code local build_ref_88
	ret_reg &ref[88]
	call_c   build_ref_2()
	call_c   build_ref_87()
	call_c   Dyam_Create_Unary(&ref[2],&ref[87])
	move_ret ref[88]
	c_ret

;; TERM 87: foreign_load_arg(-, string, _B, _C, noop, _D, _E)
c_code local build_ref_87
	ret_reg &ref[87]
	call_c   build_ref_298()
	call_c   build_ref_267()
	call_c   build_ref_289()
	call_c   build_ref_153()
	call_c   Dyam_Term_Start(&ref[298],7)
	call_c   Dyam_Term_Arg(&ref[267])
	call_c   Dyam_Term_Arg(&ref[289])
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(&ref[153])
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[87]
	c_ret

;; TERM 90: '~w string expected'
c_code local build_ref_90
	ret_reg &ref[90]
	call_c   Dyam_Create_Atom("~w string expected")
	move_ret ref[90]
	c_ret

long local pool_fun43[3]=[2,build_ref_90,build_ref_76]

pl_code local fun43
	call_c   Dyam_Remove_Choice()
	move     &ref[90], R(0)
	move     0, R(1)
	move     &ref[76], R(2)
	move     S(5), R(3)
	pl_call  pred_error_2()
	pl_jump  fun32()

long local pool_fun44[2]=[65536,pool_fun43]

pl_code local fun44
	call_c   Dyam_Update_Choice(fun43)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Reg_Load(0,V(1))
	pl_call  pred_check_var_1()
	call_c   Dyam_Cut()
	pl_jump  fun32()

long local pool_fun45[3]=[65537,build_ref_88,pool_fun44]

pl_code local fun45
	call_c   Dyam_Pool(pool_fun45)
	call_c   Dyam_Unify_Item(&ref[88])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun44)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_atom(V(1))
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun32()

;; TERM 89: '*GUARD*'(foreign_load_arg(-, string, _B, _C, noop, _D, _E)) :> []
c_code local build_ref_89
	ret_reg &ref[89]
	call_c   build_ref_88()
	call_c   Dyam_Create_Binary(I(9),&ref[88],I(0))
	move_ret ref[89]
	c_ret

c_code local build_seed_15
	ret_reg &seed[15]
	call_c   build_ref_1()
	call_c   build_ref_93()
	call_c   Dyam_Seed_Start(&ref[1],&ref[93],I(0),fun2,1)
	call_c   build_ref_92()
	call_c   Dyam_Seed_Add_Comp(&ref[92],fun48,0)
	call_c   Dyam_Seed_End()
	move_ret seed[15]
	c_ret

;; TERM 92: '*GUARD*'(foreign_load_arg(-, char, _B, _C, noop, _D, _E))
c_code local build_ref_92
	ret_reg &ref[92]
	call_c   build_ref_2()
	call_c   build_ref_91()
	call_c   Dyam_Create_Unary(&ref[2],&ref[91])
	move_ret ref[92]
	c_ret

;; TERM 91: foreign_load_arg(-, char, _B, _C, noop, _D, _E)
c_code local build_ref_91
	ret_reg &ref[91]
	call_c   build_ref_298()
	call_c   build_ref_267()
	call_c   build_ref_291()
	call_c   build_ref_153()
	call_c   Dyam_Term_Start(&ref[298],7)
	call_c   Dyam_Term_Arg(&ref[267])
	call_c   Dyam_Term_Arg(&ref[291])
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(&ref[153])
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[91]
	c_ret

;; TERM 94: '~w char expected'
c_code local build_ref_94
	ret_reg &ref[94]
	call_c   Dyam_Create_Atom("~w char expected")
	move_ret ref[94]
	c_ret

long local pool_fun46[3]=[2,build_ref_94,build_ref_76]

pl_code local fun46
	call_c   Dyam_Remove_Choice()
	move     &ref[94], R(0)
	move     0, R(1)
	move     &ref[76], R(2)
	move     S(5), R(3)
	pl_call  pred_error_2()
	pl_jump  fun32()

long local pool_fun47[2]=[65536,pool_fun46]

pl_code local fun47
	call_c   Dyam_Update_Choice(fun46)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Reg_Load(0,V(1))
	pl_call  pred_check_var_1()
	call_c   Dyam_Cut()
	pl_jump  fun32()

long local pool_fun48[3]=[65537,build_ref_92,pool_fun47]

pl_code local fun48
	call_c   Dyam_Pool(pool_fun48)
	call_c   Dyam_Unify_Item(&ref[92])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun47)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_char(V(1))
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun32()

;; TERM 93: '*GUARD*'(foreign_load_arg(-, char, _B, _C, noop, _D, _E)) :> []
c_code local build_ref_93
	ret_reg &ref[93]
	call_c   build_ref_92()
	call_c   Dyam_Create_Binary(I(9),&ref[92],I(0))
	move_ret ref[93]
	c_ret

c_code local build_seed_17
	ret_reg &seed[17]
	call_c   build_ref_1()
	call_c   build_ref_97()
	call_c   Dyam_Seed_Start(&ref[1],&ref[97],I(0),fun2,1)
	call_c   build_ref_96()
	call_c   Dyam_Seed_Add_Comp(&ref[96],fun51,0)
	call_c   Dyam_Seed_End()
	move_ret seed[17]
	c_ret

;; TERM 96: '*GUARD*'(foreign_load_arg(-, ptr, _B, _C, noop, _D, _E))
c_code local build_ref_96
	ret_reg &ref[96]
	call_c   build_ref_2()
	call_c   build_ref_95()
	call_c   Dyam_Create_Unary(&ref[2],&ref[95])
	move_ret ref[96]
	c_ret

;; TERM 95: foreign_load_arg(-, ptr, _B, _C, noop, _D, _E)
c_code local build_ref_95
	ret_reg &ref[95]
	call_c   build_ref_298()
	call_c   build_ref_267()
	call_c   build_ref_293()
	call_c   build_ref_153()
	call_c   Dyam_Term_Start(&ref[298],7)
	call_c   Dyam_Term_Arg(&ref[267])
	call_c   Dyam_Term_Arg(&ref[293])
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(&ref[153])
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[95]
	c_ret

;; TERM 98: '~w address or var expected'
c_code local build_ref_98
	ret_reg &ref[98]
	call_c   Dyam_Create_Atom("~w address or var expected")
	move_ret ref[98]
	c_ret

long local pool_fun49[3]=[2,build_ref_98,build_ref_76]

pl_code local fun49
	call_c   Dyam_Remove_Choice()
	move     &ref[98], R(0)
	move     0, R(1)
	move     &ref[76], R(2)
	move     S(5), R(3)
	pl_call  pred_error_2()
	pl_jump  fun32()

long local pool_fun50[2]=[65536,pool_fun49]

pl_code local fun50
	call_c   Dyam_Update_Choice(fun49)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Reg_Load(0,V(1))
	pl_call  pred_check_var_1()
	call_c   Dyam_Cut()
	pl_jump  fun32()

long local pool_fun51[3]=[65537,build_ref_96,pool_fun50]

pl_code local fun51
	call_c   Dyam_Pool(pool_fun51)
	call_c   Dyam_Unify_Item(&ref[96])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun50)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_number(V(1))
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun32()

;; TERM 97: '*GUARD*'(foreign_load_arg(-, ptr, _B, _C, noop, _D, _E)) :> []
c_code local build_ref_97
	ret_reg &ref[97]
	call_c   build_ref_96()
	call_c   Dyam_Create_Binary(I(9),&ref[96],I(0))
	move_ret ref[97]
	c_ret

c_code local build_seed_19
	ret_reg &seed[19]
	call_c   build_ref_1()
	call_c   build_ref_101()
	call_c   Dyam_Seed_Start(&ref[1],&ref[101],I(0),fun2,1)
	call_c   build_ref_100()
	call_c   Dyam_Seed_Add_Comp(&ref[100],fun54,0)
	call_c   Dyam_Seed_End()
	move_ret seed[19]
	c_ret

;; TERM 100: '*GUARD*'(foreign_load_arg(-, int, _B, _C, noop, _D, _E))
c_code local build_ref_100
	ret_reg &ref[100]
	call_c   build_ref_2()
	call_c   build_ref_99()
	call_c   Dyam_Create_Unary(&ref[2],&ref[99])
	move_ret ref[100]
	c_ret

;; TERM 99: foreign_load_arg(-, int, _B, _C, noop, _D, _E)
c_code local build_ref_99
	ret_reg &ref[99]
	call_c   build_ref_298()
	call_c   build_ref_267()
	call_c   build_ref_296()
	call_c   build_ref_153()
	call_c   Dyam_Term_Start(&ref[298],7)
	call_c   Dyam_Term_Arg(&ref[267])
	call_c   Dyam_Term_Arg(&ref[296])
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(&ref[153])
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[99]
	c_ret

;; TERM 102: '~w integer or var expected'
c_code local build_ref_102
	ret_reg &ref[102]
	call_c   Dyam_Create_Atom("~w integer or var expected")
	move_ret ref[102]
	c_ret

long local pool_fun52[3]=[2,build_ref_102,build_ref_76]

pl_code local fun52
	call_c   Dyam_Remove_Choice()
	move     &ref[102], R(0)
	move     0, R(1)
	move     &ref[76], R(2)
	move     S(5), R(3)
	pl_call  pred_error_2()
	pl_jump  fun32()

long local pool_fun53[2]=[65536,pool_fun52]

pl_code local fun53
	call_c   Dyam_Update_Choice(fun52)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Reg_Load(0,V(1))
	pl_call  pred_check_var_1()
	call_c   Dyam_Cut()
	pl_jump  fun32()

long local pool_fun54[3]=[65537,build_ref_100,pool_fun53]

pl_code local fun54
	call_c   Dyam_Pool(pool_fun54)
	call_c   Dyam_Unify_Item(&ref[100])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun53)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_number(V(1))
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun32()

;; TERM 101: '*GUARD*'(foreign_load_arg(-, int, _B, _C, noop, _D, _E)) :> []
c_code local build_ref_101
	ret_reg &ref[101]
	call_c   build_ref_100()
	call_c   Dyam_Create_Binary(I(9),&ref[100],I(0))
	move_ret ref[101]
	c_ret

c_code local build_seed_33
	ret_reg &seed[33]
	call_c   build_ref_1()
	call_c   build_ref_105()
	call_c   Dyam_Seed_Start(&ref[1],&ref[105],I(0),fun2,1)
	call_c   build_ref_104()
	call_c   Dyam_Seed_Add_Comp(&ref[104],fun57,0)
	call_c   Dyam_Seed_End()
	move_ret seed[33]
	c_ret

;; TERM 104: '*GUARD*'(foreign_load_init_arg(string, _B, _C, _D, 1))
c_code local build_ref_104
	ret_reg &ref[104]
	call_c   build_ref_2()
	call_c   build_ref_103()
	call_c   Dyam_Create_Unary(&ref[2],&ref[103])
	move_ret ref[104]
	c_ret

;; TERM 103: foreign_load_init_arg(string, _B, _C, _D, 1)
c_code local build_ref_103
	ret_reg &ref[103]
	call_c   build_ref_313()
	call_c   build_ref_289()
	call_c   Dyam_Term_Start(&ref[313],5)
	call_c   Dyam_Term_Arg(&ref[289])
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(N(1))
	call_c   Dyam_Term_End()
	move_ret ref[103]
	c_ret

;; TERM 106: mem_load_string(_C, _E)
c_code local build_ref_106
	ret_reg &ref[106]
	call_c   build_ref_317()
	call_c   Dyam_Create_Binary(&ref[317],V(2),V(4))
	move_ret ref[106]
	c_ret

;; TERM 317: mem_load_string
c_code local build_ref_317
	ret_reg &ref[317]
	call_c   Dyam_Create_Atom("mem_load_string")
	move_ret ref[317]
	c_ret

long local pool_fun55[2]=[1,build_ref_106]

pl_code local fun56
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Reg_Load(0,V(1))
	pl_call  pred_check_var_1()
fun55:
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(3),&ref[106])
	fail_ret
	pl_jump  fun32()


long local pool_fun57[5]=[196609,build_ref_104,pool_fun43,pool_fun55,pool_fun55]

pl_code local fun57
	call_c   Dyam_Pool(pool_fun57)
	call_c   Dyam_Unify_Item(&ref[104])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(1))
	move     V(4), R(2)
	move     S(5), R(3)
	pl_call  pred_label_term_2()
	call_c   Dyam_Choice(fun43)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Choice(fun56)
	call_c   DYAM_evpred_atom(V(1))
	fail_ret
	pl_jump  fun55()

;; TERM 105: '*GUARD*'(foreign_load_init_arg(string, _B, _C, _D, 1)) :> []
c_code local build_ref_105
	ret_reg &ref[105]
	call_c   build_ref_104()
	call_c   Dyam_Create_Binary(I(9),&ref[104],I(0))
	move_ret ref[105]
	c_ret

c_code local build_seed_34
	ret_reg &seed[34]
	call_c   build_ref_1()
	call_c   build_ref_109()
	call_c   Dyam_Seed_Start(&ref[1],&ref[109],I(0),fun2,1)
	call_c   build_ref_108()
	call_c   Dyam_Seed_Add_Comp(&ref[108],fun60,0)
	call_c   Dyam_Seed_End()
	move_ret seed[34]
	c_ret

;; TERM 108: '*GUARD*'(foreign_load_init_arg(char, _B, _C, _D, 1))
c_code local build_ref_108
	ret_reg &ref[108]
	call_c   build_ref_2()
	call_c   build_ref_107()
	call_c   Dyam_Create_Unary(&ref[2],&ref[107])
	move_ret ref[108]
	c_ret

;; TERM 107: foreign_load_init_arg(char, _B, _C, _D, 1)
c_code local build_ref_107
	ret_reg &ref[107]
	call_c   build_ref_313()
	call_c   build_ref_291()
	call_c   Dyam_Term_Start(&ref[313],5)
	call_c   Dyam_Term_Arg(&ref[291])
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(N(1))
	call_c   Dyam_Term_End()
	move_ret ref[107]
	c_ret

;; TERM 110: mem_load_char(_C, _E)
c_code local build_ref_110
	ret_reg &ref[110]
	call_c   build_ref_318()
	call_c   Dyam_Create_Binary(&ref[318],V(2),V(4))
	move_ret ref[110]
	c_ret

;; TERM 318: mem_load_char
c_code local build_ref_318
	ret_reg &ref[318]
	call_c   Dyam_Create_Atom("mem_load_char")
	move_ret ref[318]
	c_ret

long local pool_fun58[2]=[1,build_ref_110]

pl_code local fun59
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Reg_Load(0,V(1))
	pl_call  pred_check_var_1()
fun58:
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(3),&ref[110])
	fail_ret
	pl_jump  fun32()


long local pool_fun60[5]=[196609,build_ref_108,pool_fun46,pool_fun58,pool_fun58]

pl_code local fun60
	call_c   Dyam_Pool(pool_fun60)
	call_c   Dyam_Unify_Item(&ref[108])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(1))
	move     V(4), R(2)
	move     S(5), R(3)
	pl_call  pred_label_term_2()
	call_c   Dyam_Choice(fun46)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Choice(fun59)
	call_c   DYAM_evpred_char(V(1))
	fail_ret
	pl_jump  fun58()

;; TERM 109: '*GUARD*'(foreign_load_init_arg(char, _B, _C, _D, 1)) :> []
c_code local build_ref_109
	ret_reg &ref[109]
	call_c   build_ref_108()
	call_c   Dyam_Create_Binary(I(9),&ref[108],I(0))
	move_ret ref[109]
	c_ret

c_code local build_seed_36
	ret_reg &seed[36]
	call_c   build_ref_1()
	call_c   build_ref_113()
	call_c   Dyam_Seed_Start(&ref[1],&ref[113],I(0),fun2,1)
	call_c   build_ref_112()
	call_c   Dyam_Seed_Add_Comp(&ref[112],fun64,0)
	call_c   Dyam_Seed_End()
	move_ret seed[36]
	c_ret

;; TERM 112: '*GUARD*'(foreign_load_init_arg(ptr, _B, _C, _D, 1))
c_code local build_ref_112
	ret_reg &ref[112]
	call_c   build_ref_2()
	call_c   build_ref_111()
	call_c   Dyam_Create_Unary(&ref[2],&ref[111])
	move_ret ref[112]
	c_ret

;; TERM 111: foreign_load_init_arg(ptr, _B, _C, _D, 1)
c_code local build_ref_111
	ret_reg &ref[111]
	call_c   build_ref_313()
	call_c   build_ref_293()
	call_c   Dyam_Term_Start(&ref[313],5)
	call_c   Dyam_Term_Arg(&ref[293])
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(N(1))
	call_c   Dyam_Term_End()
	move_ret ref[111]
	c_ret

;; TERM 115: '~w ptr expected'
c_code local build_ref_115
	ret_reg &ref[115]
	call_c   Dyam_Create_Atom("~w ptr expected")
	move_ret ref[115]
	c_ret

long local pool_fun63[3]=[2,build_ref_115,build_ref_76]

pl_code local fun63
	call_c   Dyam_Remove_Choice()
	move     &ref[115], R(0)
	move     0, R(1)
	move     &ref[76], R(2)
	move     S(5), R(3)
	pl_call  pred_error_2()
	pl_jump  fun32()

;; TERM 114: mem_load_ptr(_C, _E)
c_code local build_ref_114
	ret_reg &ref[114]
	call_c   build_ref_319()
	call_c   Dyam_Create_Binary(&ref[319],V(2),V(4))
	move_ret ref[114]
	c_ret

;; TERM 319: mem_load_ptr
c_code local build_ref_319
	ret_reg &ref[319]
	call_c   Dyam_Create_Atom("mem_load_ptr")
	move_ret ref[319]
	c_ret

long local pool_fun61[2]=[1,build_ref_114]

pl_code local fun62
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Reg_Load(0,V(1))
	pl_call  pred_check_var_1()
fun61:
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(3),&ref[114])
	fail_ret
	pl_jump  fun32()


long local pool_fun64[5]=[196609,build_ref_112,pool_fun63,pool_fun61,pool_fun61]

pl_code local fun64
	call_c   Dyam_Pool(pool_fun64)
	call_c   Dyam_Unify_Item(&ref[112])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(1))
	move     V(4), R(2)
	move     S(5), R(3)
	pl_call  pred_label_term_2()
	call_c   Dyam_Choice(fun63)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Choice(fun62)
	call_c   DYAM_evpred_number(V(1))
	fail_ret
	pl_jump  fun61()

;; TERM 113: '*GUARD*'(foreign_load_init_arg(ptr, _B, _C, _D, 1)) :> []
c_code local build_ref_113
	ret_reg &ref[113]
	call_c   build_ref_112()
	call_c   Dyam_Create_Binary(I(9),&ref[112],I(0))
	move_ret ref[113]
	c_ret

c_code local build_seed_37
	ret_reg &seed[37]
	call_c   build_ref_1()
	call_c   build_ref_118()
	call_c   Dyam_Seed_Start(&ref[1],&ref[118],I(0),fun2,1)
	call_c   build_ref_117()
	call_c   Dyam_Seed_Add_Comp(&ref[117],fun68,0)
	call_c   Dyam_Seed_End()
	move_ret seed[37]
	c_ret

;; TERM 117: '*GUARD*'(foreign_load_init_arg(float, _B, _C, _D, 1))
c_code local build_ref_117
	ret_reg &ref[117]
	call_c   build_ref_2()
	call_c   build_ref_116()
	call_c   Dyam_Create_Unary(&ref[2],&ref[116])
	move_ret ref[117]
	c_ret

;; TERM 116: foreign_load_init_arg(float, _B, _C, _D, 1)
c_code local build_ref_116
	ret_reg &ref[116]
	call_c   build_ref_313()
	call_c   build_ref_229()
	call_c   Dyam_Term_Start(&ref[313],5)
	call_c   Dyam_Term_Arg(&ref[229])
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(N(1))
	call_c   Dyam_Term_End()
	move_ret ref[116]
	c_ret

;; TERM 120: '~w float expected'
c_code local build_ref_120
	ret_reg &ref[120]
	call_c   Dyam_Create_Atom("~w float expected")
	move_ret ref[120]
	c_ret

long local pool_fun67[3]=[2,build_ref_120,build_ref_76]

pl_code local fun67
	call_c   Dyam_Remove_Choice()
	move     &ref[120], R(0)
	move     0, R(1)
	move     &ref[76], R(2)
	move     S(5), R(3)
	pl_call  pred_error_2()
	pl_jump  fun32()

;; TERM 119: mem_load_float(_C, _E)
c_code local build_ref_119
	ret_reg &ref[119]
	call_c   build_ref_320()
	call_c   Dyam_Create_Binary(&ref[320],V(2),V(4))
	move_ret ref[119]
	c_ret

;; TERM 320: mem_load_float
c_code local build_ref_320
	ret_reg &ref[320]
	call_c   Dyam_Create_Atom("mem_load_float")
	move_ret ref[320]
	c_ret

long local pool_fun65[2]=[1,build_ref_119]

pl_code local fun66
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Reg_Load(0,V(1))
	pl_call  pred_check_var_1()
fun65:
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(3),&ref[119])
	fail_ret
	pl_jump  fun32()


long local pool_fun68[5]=[196609,build_ref_117,pool_fun67,pool_fun65,pool_fun65]

pl_code local fun68
	call_c   Dyam_Pool(pool_fun68)
	call_c   Dyam_Unify_Item(&ref[117])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(1))
	move     V(4), R(2)
	move     S(5), R(3)
	pl_call  pred_label_term_2()
	call_c   Dyam_Choice(fun67)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Choice(fun66)
	call_c   DYAM_evpred_float(V(1))
	fail_ret
	pl_jump  fun65()

;; TERM 118: '*GUARD*'(foreign_load_init_arg(float, _B, _C, _D, 1)) :> []
c_code local build_ref_118
	ret_reg &ref[118]
	call_c   build_ref_117()
	call_c   Dyam_Create_Binary(I(9),&ref[117],I(0))
	move_ret ref[118]
	c_ret

c_code local build_seed_38
	ret_reg &seed[38]
	call_c   build_ref_1()
	call_c   build_ref_123()
	call_c   Dyam_Seed_Start(&ref[1],&ref[123],I(0),fun2,1)
	call_c   build_ref_122()
	call_c   Dyam_Seed_Add_Comp(&ref[122],fun72,0)
	call_c   Dyam_Seed_End()
	move_ret seed[38]
	c_ret

;; TERM 122: '*GUARD*'(foreign_load_init_arg(int, _B, _C, _D, 1))
c_code local build_ref_122
	ret_reg &ref[122]
	call_c   build_ref_2()
	call_c   build_ref_121()
	call_c   Dyam_Create_Unary(&ref[2],&ref[121])
	move_ret ref[122]
	c_ret

;; TERM 121: foreign_load_init_arg(int, _B, _C, _D, 1)
c_code local build_ref_121
	ret_reg &ref[121]
	call_c   build_ref_313()
	call_c   build_ref_296()
	call_c   Dyam_Term_Start(&ref[313],5)
	call_c   Dyam_Term_Arg(&ref[296])
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(N(1))
	call_c   Dyam_Term_End()
	move_ret ref[121]
	c_ret

;; TERM 125: '~w integer expected'
c_code local build_ref_125
	ret_reg &ref[125]
	call_c   Dyam_Create_Atom("~w integer expected")
	move_ret ref[125]
	c_ret

long local pool_fun71[3]=[2,build_ref_125,build_ref_76]

pl_code local fun71
	call_c   Dyam_Remove_Choice()
	move     &ref[125], R(0)
	move     0, R(1)
	move     &ref[76], R(2)
	move     S(5), R(3)
	pl_call  pred_error_2()
	pl_jump  fun32()

;; TERM 124: mem_load_number(_C, _E)
c_code local build_ref_124
	ret_reg &ref[124]
	call_c   build_ref_321()
	call_c   Dyam_Create_Binary(&ref[321],V(2),V(4))
	move_ret ref[124]
	c_ret

;; TERM 321: mem_load_number
c_code local build_ref_321
	ret_reg &ref[321]
	call_c   Dyam_Create_Atom("mem_load_number")
	move_ret ref[321]
	c_ret

long local pool_fun69[2]=[1,build_ref_124]

pl_code local fun70
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Reg_Load(0,V(1))
	pl_call  pred_check_var_1()
fun69:
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(3),&ref[124])
	fail_ret
	pl_jump  fun32()


long local pool_fun72[5]=[196609,build_ref_122,pool_fun71,pool_fun69,pool_fun69]

pl_code local fun72
	call_c   Dyam_Pool(pool_fun72)
	call_c   Dyam_Unify_Item(&ref[122])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(1))
	move     V(4), R(2)
	move     S(5), R(3)
	pl_call  pred_label_term_2()
	call_c   Dyam_Choice(fun71)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Choice(fun70)
	call_c   DYAM_evpred_number(V(1))
	fail_ret
	pl_jump  fun69()

;; TERM 123: '*GUARD*'(foreign_load_init_arg(int, _B, _C, _D, 1)) :> []
c_code local build_ref_123
	ret_reg &ref[123]
	call_c   build_ref_122()
	call_c   Dyam_Create_Binary(I(9),&ref[122],I(0))
	move_ret ref[123]
	c_ret

c_code local build_seed_16
	ret_reg &seed[16]
	call_c   build_ref_1()
	call_c   build_ref_128()
	call_c   Dyam_Seed_Start(&ref[1],&ref[128],I(0),fun2,1)
	call_c   build_ref_127()
	call_c   Dyam_Seed_Add_Comp(&ref[127],fun75,0)
	call_c   Dyam_Seed_End()
	move_ret seed[16]
	c_ret

;; TERM 127: '*GUARD*'(foreign_load_arg(-, bool, _B, _C, noop, _D, _E))
c_code local build_ref_127
	ret_reg &ref[127]
	call_c   build_ref_2()
	call_c   build_ref_126()
	call_c   Dyam_Create_Unary(&ref[2],&ref[126])
	move_ret ref[127]
	c_ret

;; TERM 126: foreign_load_arg(-, bool, _B, _C, noop, _D, _E)
c_code local build_ref_126
	ret_reg &ref[126]
	call_c   build_ref_298()
	call_c   build_ref_267()
	call_c   build_ref_57()
	call_c   build_ref_153()
	call_c   Dyam_Term_Start(&ref[298],7)
	call_c   Dyam_Term_Arg(&ref[267])
	call_c   Dyam_Term_Arg(&ref[57])
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(&ref[153])
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[126]
	c_ret

;; TERM 130: '~w boolean or var expected'
c_code local build_ref_130
	ret_reg &ref[130]
	call_c   Dyam_Create_Atom("~w boolean or var expected")
	move_ret ref[130]
	c_ret

long local pool_fun73[3]=[2,build_ref_130,build_ref_76]

pl_code local fun73
	call_c   Dyam_Remove_Choice()
	move     &ref[130], R(0)
	move     0, R(1)
	move     &ref[76], R(2)
	move     S(5), R(3)
	pl_call  pred_error_2()
	pl_jump  fun32()

long local pool_fun74[2]=[65536,pool_fun73]

pl_code local fun74
	call_c   Dyam_Update_Choice(fun73)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Reg_Load(0,V(1))
	pl_call  pred_check_var_1()
	call_c   Dyam_Cut()
	pl_jump  fun32()

;; TERM 129: [true,false]
c_code local build_ref_129
	ret_reg &ref[129]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_181()
	call_c   Dyam_Create_List(&ref[181],I(0))
	move_ret R(0)
	call_c   build_ref_179()
	call_c   Dyam_Create_List(&ref[179],R(0))
	move_ret ref[129]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 179: true
c_code local build_ref_179
	ret_reg &ref[179]
	call_c   Dyam_Create_Atom("true")
	move_ret ref[179]
	c_ret

;; TERM 181: false
c_code local build_ref_181
	ret_reg &ref[181]
	call_c   Dyam_Create_Atom("false")
	move_ret ref[181]
	c_ret

long local pool_fun75[4]=[65538,build_ref_127,build_ref_129,pool_fun74]

pl_code local fun75
	call_c   Dyam_Pool(pool_fun75)
	call_c   Dyam_Unify_Item(&ref[127])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun74)
	call_c   Dyam_Set_Cut()
	pl_call  Domain_2(V(1),&ref[129])
	call_c   Dyam_Cut()
	pl_jump  fun32()

;; TERM 128: '*GUARD*'(foreign_load_arg(-, bool, _B, _C, noop, _D, _E)) :> []
c_code local build_ref_128
	ret_reg &ref[128]
	call_c   build_ref_127()
	call_c   Dyam_Create_Binary(I(9),&ref[127],I(0))
	move_ret ref[128]
	c_ret

c_code local build_seed_35
	ret_reg &seed[35]
	call_c   build_ref_1()
	call_c   build_ref_133()
	call_c   Dyam_Seed_Start(&ref[1],&ref[133],I(0),fun2,1)
	call_c   build_ref_132()
	call_c   Dyam_Seed_Add_Comp(&ref[132],fun79,0)
	call_c   Dyam_Seed_End()
	move_ret seed[35]
	c_ret

;; TERM 132: '*GUARD*'(foreign_load_init_arg(bool, _B, _C, _D, 1))
c_code local build_ref_132
	ret_reg &ref[132]
	call_c   build_ref_2()
	call_c   build_ref_131()
	call_c   Dyam_Create_Unary(&ref[2],&ref[131])
	move_ret ref[132]
	c_ret

;; TERM 131: foreign_load_init_arg(bool, _B, _C, _D, 1)
c_code local build_ref_131
	ret_reg &ref[131]
	call_c   build_ref_313()
	call_c   build_ref_57()
	call_c   Dyam_Term_Start(&ref[313],5)
	call_c   Dyam_Term_Arg(&ref[57])
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(N(1))
	call_c   Dyam_Term_End()
	move_ret ref[131]
	c_ret

;; TERM 135: '~w boolean expected'
c_code local build_ref_135
	ret_reg &ref[135]
	call_c   Dyam_Create_Atom("~w boolean expected")
	move_ret ref[135]
	c_ret

long local pool_fun78[3]=[2,build_ref_135,build_ref_76]

pl_code local fun78
	call_c   Dyam_Remove_Choice()
	move     &ref[135], R(0)
	move     0, R(1)
	move     &ref[76], R(2)
	move     S(5), R(3)
	pl_call  pred_error_2()
	pl_jump  fun32()

;; TERM 134: mem_load_boolean(_C, _E)
c_code local build_ref_134
	ret_reg &ref[134]
	call_c   build_ref_322()
	call_c   Dyam_Create_Binary(&ref[322],V(2),V(4))
	move_ret ref[134]
	c_ret

;; TERM 322: mem_load_boolean
c_code local build_ref_322
	ret_reg &ref[322]
	call_c   Dyam_Create_Atom("mem_load_boolean")
	move_ret ref[322]
	c_ret

long local pool_fun76[2]=[1,build_ref_134]

pl_code local fun77
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Reg_Load(0,V(1))
	pl_call  pred_check_var_1()
fun76:
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(3),&ref[134])
	fail_ret
	pl_jump  fun32()


long local pool_fun79[6]=[196610,build_ref_132,build_ref_129,pool_fun78,pool_fun76,pool_fun76]

pl_code local fun79
	call_c   Dyam_Pool(pool_fun79)
	call_c   Dyam_Unify_Item(&ref[132])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(1))
	move     V(4), R(2)
	move     S(5), R(3)
	pl_call  pred_label_term_2()
	call_c   Dyam_Choice(fun78)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Choice(fun77)
	pl_call  Domain_2(V(1),&ref[129])
	pl_jump  fun76()

;; TERM 133: '*GUARD*'(foreign_load_init_arg(bool, _B, _C, _D, 1)) :> []
c_code local build_ref_133
	ret_reg &ref[133]
	call_c   build_ref_132()
	call_c   Dyam_Create_Binary(I(9),&ref[132],I(0))
	move_ret ref[133]
	c_ret

c_code local build_seed_18
	ret_reg &seed[18]
	call_c   build_ref_1()
	call_c   build_ref_138()
	call_c   Dyam_Seed_Start(&ref[1],&ref[138],I(0),fun2,1)
	call_c   build_ref_137()
	call_c   Dyam_Seed_Add_Comp(&ref[137],fun83,0)
	call_c   Dyam_Seed_End()
	move_ret seed[18]
	c_ret

;; TERM 137: '*GUARD*'(foreign_load_arg(-, float, _B, _C, noop, _D, _E))
c_code local build_ref_137
	ret_reg &ref[137]
	call_c   build_ref_2()
	call_c   build_ref_136()
	call_c   Dyam_Create_Unary(&ref[2],&ref[136])
	move_ret ref[137]
	c_ret

;; TERM 136: foreign_load_arg(-, float, _B, _C, noop, _D, _E)
c_code local build_ref_136
	ret_reg &ref[136]
	call_c   build_ref_298()
	call_c   build_ref_267()
	call_c   build_ref_229()
	call_c   build_ref_153()
	call_c   Dyam_Term_Start(&ref[298],7)
	call_c   Dyam_Term_Arg(&ref[267])
	call_c   Dyam_Term_Arg(&ref[229])
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(&ref[153])
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[136]
	c_ret

;; TERM 139: '~w float or var expected'
c_code local build_ref_139
	ret_reg &ref[139]
	call_c   Dyam_Create_Atom("~w float or var expected")
	move_ret ref[139]
	c_ret

long local pool_fun80[3]=[2,build_ref_139,build_ref_76]

pl_code local fun80
	call_c   Dyam_Remove_Choice()
	move     &ref[139], R(0)
	move     0, R(1)
	move     &ref[76], R(2)
	move     S(5), R(3)
	pl_call  pred_error_2()
	pl_jump  fun32()

long local pool_fun81[2]=[65536,pool_fun80]

pl_code local fun81
	call_c   Dyam_Update_Choice(fun80)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Reg_Load(0,V(1))
	pl_call  pred_check_var_1()
	call_c   Dyam_Cut()
	pl_jump  fun32()

long local pool_fun82[2]=[65536,pool_fun81]

pl_code local fun82
	call_c   Dyam_Update_Choice(fun81)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_number(V(1))
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun32()

long local pool_fun83[3]=[65537,build_ref_137,pool_fun82]

pl_code local fun83
	call_c   Dyam_Pool(pool_fun83)
	call_c   Dyam_Unify_Item(&ref[137])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun82)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_float(V(1))
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun32()

;; TERM 138: '*GUARD*'(foreign_load_arg(-, float, _B, _C, noop, _D, _E)) :> []
c_code local build_ref_138
	ret_reg &ref[138]
	call_c   build_ref_137()
	call_c   Dyam_Create_Binary(I(9),&ref[137],I(0))
	move_ret ref[138]
	c_ret

c_code local build_seed_32
	ret_reg &seed[32]
	call_c   build_ref_1()
	call_c   build_ref_142()
	call_c   Dyam_Seed_Start(&ref[1],&ref[142],I(0),fun2,1)
	call_c   build_ref_141()
	call_c   Dyam_Seed_Add_Comp(&ref[141],fun86,0)
	call_c   Dyam_Seed_End()
	move_ret seed[32]
	c_ret

;; TERM 141: '*GUARD*'(foreign_load_init_arg(term, _B, _C, _D, 2))
c_code local build_ref_141
	ret_reg &ref[141]
	call_c   build_ref_2()
	call_c   build_ref_140()
	call_c   Dyam_Create_Unary(&ref[2],&ref[140])
	move_ret ref[141]
	c_ret

;; TERM 140: foreign_load_init_arg(term, _B, _C, _D, 2)
c_code local build_ref_140
	ret_reg &ref[140]
	call_c   build_ref_313()
	call_c   build_ref_276()
	call_c   Dyam_Term_Start(&ref[313],5)
	call_c   Dyam_Term_Arg(&ref[276])
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(N(2))
	call_c   Dyam_Term_End()
	move_ret ref[140]
	c_ret

;; TERM 145: mem_load(_C, _E)
c_code local build_ref_145
	ret_reg &ref[145]
	call_c   build_ref_323()
	call_c   Dyam_Create_Binary(&ref[323],V(2),V(4))
	move_ret ref[145]
	c_ret

;; TERM 323: mem_load
c_code local build_ref_323
	ret_reg &ref[323]
	call_c   Dyam_Create_Atom("mem_load")
	move_ret ref[323]
	c_ret

long local pool_fun84[2]=[1,build_ref_145]

pl_code local fun84
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(3),&ref[145])
	fail_ret
	pl_jump  fun32()

;; TERM 144: mem_load_cst(_C, _E)
c_code local build_ref_144
	ret_reg &ref[144]
	call_c   build_ref_324()
	call_c   Dyam_Create_Binary(&ref[324],V(2),V(4))
	move_ret ref[144]
	c_ret

;; TERM 324: mem_load_cst
c_code local build_ref_324
	ret_reg &ref[324]
	call_c   Dyam_Create_Atom("mem_load_cst")
	move_ret ref[324]
	c_ret

long local pool_fun85[3]=[65537,build_ref_144,pool_fun84]

pl_code local fun85
	call_c   Dyam_Update_Choice(fun84)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_atomic(V(1))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(3),&ref[144])
	fail_ret
	pl_jump  fun32()

;; TERM 143: mem_load_nil(_C)
c_code local build_ref_143
	ret_reg &ref[143]
	call_c   build_ref_325()
	call_c   Dyam_Create_Unary(&ref[325],V(2))
	move_ret ref[143]
	c_ret

;; TERM 325: mem_load_nil
c_code local build_ref_325
	ret_reg &ref[325]
	call_c   Dyam_Create_Atom("mem_load_nil")
	move_ret ref[325]
	c_ret

long local pool_fun86[4]=[65538,build_ref_141,build_ref_143,pool_fun85]

pl_code local fun86
	call_c   Dyam_Pool(pool_fun86)
	call_c   Dyam_Unify_Item(&ref[141])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(1))
	move     V(4), R(2)
	move     S(5), R(3)
	pl_call  pred_label_term_2()
	call_c   Dyam_Choice(fun85)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(1),I(0))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(3),&ref[143])
	fail_ret
	pl_jump  fun32()

;; TERM 142: '*GUARD*'(foreign_load_init_arg(term, _B, _C, _D, 2)) :> []
c_code local build_ref_142
	ret_reg &ref[142]
	call_c   build_ref_141()
	call_c   Dyam_Create_Binary(I(9),&ref[141],I(0))
	move_ret ref[142]
	c_ret

c_code local build_seed_24
	ret_reg &seed[24]
	call_c   build_ref_1()
	call_c   build_ref_148()
	call_c   Dyam_Seed_Start(&ref[1],&ref[148],I(0),fun2,1)
	call_c   build_ref_147()
	call_c   Dyam_Seed_Add_Comp(&ref[147],fun88,0)
	call_c   Dyam_Seed_End()
	move_ret seed[24]
	c_ret

;; TERM 147: '*GUARD*'(foreign_load_arg(+, string, _B, _C, _D, _E, _F))
c_code local build_ref_147
	ret_reg &ref[147]
	call_c   build_ref_2()
	call_c   build_ref_146()
	call_c   Dyam_Create_Unary(&ref[2],&ref[146])
	move_ret ref[147]
	c_ret

;; TERM 146: foreign_load_arg(+, string, _B, _C, _D, _E, _F)
c_code local build_ref_146
	ret_reg &ref[146]
	call_c   build_ref_298()
	call_c   build_ref_47()
	call_c   build_ref_289()
	call_c   Dyam_Term_Start(&ref[298],7)
	call_c   Dyam_Term_Arg(&ref[47])
	call_c   Dyam_Term_Arg(&ref[289])
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[146]
	c_ret

;; TERM 149: reg_load_string(_C, _G)
c_code local build_ref_149
	ret_reg &ref[149]
	call_c   build_ref_326()
	call_c   Dyam_Create_Binary(&ref[326],V(2),V(6))
	move_ret ref[149]
	c_ret

;; TERM 326: reg_load_string
c_code local build_ref_326
	ret_reg &ref[326]
	call_c   Dyam_Create_Atom("reg_load_string")
	move_ret ref[326]
	c_ret

long local pool_fun87[3]=[65537,build_ref_149,pool_fun43]

pl_code local fun87
	call_c   Dyam_Update_Choice(fun43)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Reg_Load(0,V(1))
	pl_call  pred_check_var_1()
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(3),&ref[149])
	fail_ret
	pl_jump  fun32()

long local pool_fun88[4]=[65538,build_ref_147,build_ref_149,pool_fun87]

pl_code local fun88
	call_c   Dyam_Pool(pool_fun88)
	call_c   Dyam_Unify_Item(&ref[147])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(1))
	move     V(6), R(2)
	move     S(5), R(3)
	pl_call  pred_label_term_2()
	call_c   Dyam_Choice(fun87)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_atom(V(1))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(3),&ref[149])
	fail_ret
	pl_jump  fun32()

;; TERM 148: '*GUARD*'(foreign_load_arg(+, string, _B, _C, _D, _E, _F)) :> []
c_code local build_ref_148
	ret_reg &ref[148]
	call_c   build_ref_147()
	call_c   Dyam_Create_Binary(I(9),&ref[147],I(0))
	move_ret ref[148]
	c_ret

c_code local build_seed_29
	ret_reg &seed[29]
	call_c   build_ref_1()
	call_c   build_ref_152()
	call_c   Dyam_Seed_Start(&ref[1],&ref[152],I(0),fun2,1)
	call_c   build_ref_151()
	call_c   Dyam_Seed_Add_Comp(&ref[151],fun90,0)
	call_c   Dyam_Seed_End()
	move_ret seed[29]
	c_ret

;; TERM 151: '*GUARD*'(foreign_load_arg(+, int, _B, _C, _D, _E, _F))
c_code local build_ref_151
	ret_reg &ref[151]
	call_c   build_ref_2()
	call_c   build_ref_150()
	call_c   Dyam_Create_Unary(&ref[2],&ref[150])
	move_ret ref[151]
	c_ret

;; TERM 150: foreign_load_arg(+, int, _B, _C, _D, _E, _F)
c_code local build_ref_150
	ret_reg &ref[150]
	call_c   build_ref_298()
	call_c   build_ref_47()
	call_c   build_ref_296()
	call_c   Dyam_Term_Start(&ref[298],7)
	call_c   Dyam_Term_Arg(&ref[47])
	call_c   Dyam_Term_Arg(&ref[296])
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[150]
	c_ret

;; TERM 155: reg_load_number(_C, _G)
c_code local build_ref_155
	ret_reg &ref[155]
	call_c   build_ref_327()
	call_c   Dyam_Create_Binary(&ref[327],V(2),V(6))
	move_ret ref[155]
	c_ret

;; TERM 327: reg_load_number
c_code local build_ref_327
	ret_reg &ref[327]
	call_c   Dyam_Create_Atom("reg_load_number")
	move_ret ref[327]
	c_ret

long local pool_fun89[3]=[65537,build_ref_155,pool_fun71]

pl_code local fun89
	call_c   Dyam_Update_Choice(fun71)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Reg_Load(0,V(1))
	pl_call  pred_check_var_1()
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(1))
	move     V(6), R(2)
	move     S(5), R(3)
	pl_call  pred_label_term_2()
	call_c   Dyam_Unify(V(3),&ref[155])
	fail_ret
	pl_jump  fun32()

;; TERM 154: c(_B)
c_code local build_ref_154
	ret_reg &ref[154]
	call_c   build_ref_328()
	call_c   Dyam_Create_Unary(&ref[328],V(1))
	move_ret ref[154]
	c_ret

;; TERM 328: c
c_code local build_ref_328
	ret_reg &ref[328]
	call_c   Dyam_Create_Atom("c")
	move_ret ref[328]
	c_ret

long local pool_fun90[5]=[65539,build_ref_151,build_ref_153,build_ref_154,pool_fun89]

pl_code local fun90
	call_c   Dyam_Pool(pool_fun90)
	call_c   Dyam_Unify_Item(&ref[151])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun89)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_number(V(1))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(3),&ref[153])
	fail_ret
	call_c   Dyam_Unify(V(4),&ref[154])
	fail_ret
	pl_jump  fun32()

;; TERM 152: '*GUARD*'(foreign_load_arg(+, int, _B, _C, _D, _E, _F)) :> []
c_code local build_ref_152
	ret_reg &ref[152]
	call_c   build_ref_151()
	call_c   Dyam_Create_Binary(I(9),&ref[151],I(0))
	move_ret ref[152]
	c_ret

c_code local build_seed_25
	ret_reg &seed[25]
	call_c   build_ref_1()
	call_c   build_ref_158()
	call_c   Dyam_Seed_Start(&ref[1],&ref[158],I(0),fun2,1)
	call_c   build_ref_157()
	call_c   Dyam_Seed_Add_Comp(&ref[157],fun92,0)
	call_c   Dyam_Seed_End()
	move_ret seed[25]
	c_ret

;; TERM 157: '*GUARD*'(foreign_load_arg(+, char, _B, _C, _D, _E, _F))
c_code local build_ref_157
	ret_reg &ref[157]
	call_c   build_ref_2()
	call_c   build_ref_156()
	call_c   Dyam_Create_Unary(&ref[2],&ref[156])
	move_ret ref[157]
	c_ret

;; TERM 156: foreign_load_arg(+, char, _B, _C, _D, _E, _F)
c_code local build_ref_156
	ret_reg &ref[156]
	call_c   build_ref_298()
	call_c   build_ref_47()
	call_c   build_ref_291()
	call_c   Dyam_Term_Start(&ref[298],7)
	call_c   Dyam_Term_Arg(&ref[47])
	call_c   Dyam_Term_Arg(&ref[291])
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[156]
	c_ret

;; TERM 160: reg_load_char(_C, _H)
c_code local build_ref_160
	ret_reg &ref[160]
	call_c   build_ref_329()
	call_c   Dyam_Create_Binary(&ref[329],V(2),V(7))
	move_ret ref[160]
	c_ret

;; TERM 329: reg_load_char
c_code local build_ref_329
	ret_reg &ref[329]
	call_c   Dyam_Create_Atom("reg_load_char")
	move_ret ref[329]
	c_ret

long local pool_fun91[3]=[65537,build_ref_160,pool_fun46]

pl_code local fun91
	call_c   Dyam_Update_Choice(fun46)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Reg_Load(0,V(1))
	pl_call  pred_check_var_1()
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(1))
	move     V(7), R(2)
	move     S(5), R(3)
	pl_call  pred_label_term_2()
	call_c   Dyam_Unify(V(3),&ref[160])
	fail_ret
	pl_jump  fun32()

;; TERM 159: c(_G)
c_code local build_ref_159
	ret_reg &ref[159]
	call_c   build_ref_328()
	call_c   Dyam_Create_Unary(&ref[328],V(6))
	move_ret ref[159]
	c_ret

long local pool_fun92[5]=[65539,build_ref_157,build_ref_153,build_ref_159,pool_fun91]

pl_code local fun92
	call_c   Dyam_Pool(pool_fun92)
	call_c   Dyam_Unify_Item(&ref[157])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun91)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_char(V(1))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(3),&ref[153])
	fail_ret
	call_c   DYAM_Char_Code_2(V(1),V(6))
	fail_ret
	call_c   Dyam_Unify(V(4),&ref[159])
	fail_ret
	pl_jump  fun32()

;; TERM 158: '*GUARD*'(foreign_load_arg(+, char, _B, _C, _D, _E, _F)) :> []
c_code local build_ref_158
	ret_reg &ref[158]
	call_c   build_ref_157()
	call_c   Dyam_Create_Binary(I(9),&ref[157],I(0))
	move_ret ref[158]
	c_ret

c_code local build_seed_23
	ret_reg &seed[23]
	call_c   build_ref_1()
	call_c   build_ref_163()
	call_c   Dyam_Seed_Start(&ref[1],&ref[163],I(0),fun2,1)
	call_c   build_ref_162()
	call_c   Dyam_Seed_Add_Comp(&ref[162],fun96,0)
	call_c   Dyam_Seed_End()
	move_ret seed[23]
	c_ret

;; TERM 162: '*GUARD*'(foreign_load_arg(+, term_no_deref, _B, _C, _D, _E, _F))
c_code local build_ref_162
	ret_reg &ref[162]
	call_c   build_ref_2()
	call_c   build_ref_161()
	call_c   Dyam_Create_Unary(&ref[2],&ref[161])
	move_ret ref[162]
	c_ret

;; TERM 161: foreign_load_arg(+, term_no_deref, _B, _C, _D, _E, _F)
c_code local build_ref_161
	ret_reg &ref[161]
	call_c   build_ref_298()
	call_c   build_ref_47()
	call_c   build_ref_277()
	call_c   Dyam_Term_Start(&ref[298],7)
	call_c   Dyam_Term_Arg(&ref[47])
	call_c   Dyam_Term_Arg(&ref[277])
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[161]
	c_ret

;; TERM 277: term_no_deref
c_code local build_ref_277
	ret_reg &ref[277]
	call_c   Dyam_Create_Atom("term_no_deref")
	move_ret ref[277]
	c_ret

;; TERM 167: reg_load(_C, _G)
c_code local build_ref_167
	ret_reg &ref[167]
	call_c   build_ref_330()
	call_c   Dyam_Create_Binary(&ref[330],V(2),V(6))
	move_ret ref[167]
	c_ret

;; TERM 330: reg_load
c_code local build_ref_330
	ret_reg &ref[330]
	call_c   Dyam_Create_Atom("reg_load")
	move_ret ref[330]
	c_ret

long local pool_fun93[2]=[1,build_ref_167]

pl_code local fun93
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(3),&ref[167])
	fail_ret
	pl_jump  fun32()

;; TERM 166: reg_load_var(_C, _G)
c_code local build_ref_166
	ret_reg &ref[166]
	call_c   build_ref_331()
	call_c   Dyam_Create_Binary(&ref[331],V(2),V(6))
	move_ret ref[166]
	c_ret

;; TERM 331: reg_load_var
c_code local build_ref_331
	ret_reg &ref[331]
	call_c   Dyam_Create_Atom("reg_load_var")
	move_ret ref[331]
	c_ret

long local pool_fun94[3]=[65537,build_ref_166,pool_fun93]

pl_code local fun94
	call_c   Dyam_Update_Choice(fun93)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Reg_Load(0,V(1))
	pl_call  pred_check_var_1()
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(3),&ref[166])
	fail_ret
	pl_jump  fun32()

;; TERM 165: reg_load_cst(_C, _G)
c_code local build_ref_165
	ret_reg &ref[165]
	call_c   build_ref_332()
	call_c   Dyam_Create_Binary(&ref[332],V(2),V(6))
	move_ret ref[165]
	c_ret

;; TERM 332: reg_load_cst
c_code local build_ref_332
	ret_reg &ref[332]
	call_c   Dyam_Create_Atom("reg_load_cst")
	move_ret ref[332]
	c_ret

long local pool_fun95[3]=[65537,build_ref_165,pool_fun94]

pl_code local fun95
	call_c   Dyam_Update_Choice(fun94)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_atomic(V(1))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(3),&ref[165])
	fail_ret
	pl_jump  fun32()

;; TERM 164: reg_load_nil(_C)
c_code local build_ref_164
	ret_reg &ref[164]
	call_c   build_ref_333()
	call_c   Dyam_Create_Unary(&ref[333],V(2))
	move_ret ref[164]
	c_ret

;; TERM 333: reg_load_nil
c_code local build_ref_333
	ret_reg &ref[333]
	call_c   Dyam_Create_Atom("reg_load_nil")
	move_ret ref[333]
	c_ret

long local pool_fun96[4]=[65538,build_ref_162,build_ref_164,pool_fun95]

pl_code local fun96
	call_c   Dyam_Pool(pool_fun96)
	call_c   Dyam_Unify_Item(&ref[162])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(1))
	move     V(6), R(2)
	move     S(5), R(3)
	pl_call  pred_label_term_2()
	call_c   Dyam_Choice(fun95)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(1),I(0))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(3),&ref[164])
	fail_ret
	pl_jump  fun32()

;; TERM 163: '*GUARD*'(foreign_load_arg(+, term_no_deref, _B, _C, _D, _E, _F)) :> []
c_code local build_ref_163
	ret_reg &ref[163]
	call_c   build_ref_162()
	call_c   Dyam_Create_Binary(I(9),&ref[162],I(0))
	move_ret ref[163]
	c_ret

c_code local build_seed_20
	ret_reg &seed[20]
	call_c   build_ref_1()
	call_c   build_ref_170()
	call_c   Dyam_Seed_Start(&ref[1],&ref[170],I(0),fun2,1)
	call_c   build_ref_169()
	call_c   Dyam_Seed_Add_Comp(&ref[169],fun99,0)
	call_c   Dyam_Seed_End()
	move_ret seed[20]
	c_ret

;; TERM 169: '*GUARD*'(foreign_load_arg(+, output, _B, _C, _D, _E, _F))
c_code local build_ref_169
	ret_reg &ref[169]
	call_c   build_ref_2()
	call_c   build_ref_168()
	call_c   Dyam_Create_Unary(&ref[2],&ref[168])
	move_ret ref[169]
	c_ret

;; TERM 168: foreign_load_arg(+, output, _B, _C, _D, _E, _F)
c_code local build_ref_168
	ret_reg &ref[168]
	call_c   build_ref_298()
	call_c   build_ref_47()
	call_c   build_ref_285()
	call_c   Dyam_Term_Start(&ref[298],7)
	call_c   Dyam_Term_Arg(&ref[47])
	call_c   Dyam_Term_Arg(&ref[285])
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[168]
	c_ret

;; TERM 171: reg_load_output(_C, _G)
c_code local build_ref_171
	ret_reg &ref[171]
	call_c   build_ref_334()
	call_c   Dyam_Create_Binary(&ref[334],V(2),V(6))
	move_ret ref[171]
	c_ret

;; TERM 334: reg_load_output
c_code local build_ref_334
	ret_reg &ref[334]
	call_c   Dyam_Create_Atom("reg_load_output")
	move_ret ref[334]
	c_ret

long local pool_fun97[3]=[65537,build_ref_171,pool_fun43]

pl_code local fun97
	call_c   Dyam_Update_Choice(fun43)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Reg_Load(0,V(1))
	pl_call  pred_check_var_1()
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(3),&ref[171])
	fail_ret
	pl_jump  fun32()

long local pool_fun98[3]=[65537,build_ref_171,pool_fun97]

pl_code local fun98
	call_c   Dyam_Update_Choice(fun97)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_number(V(1))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(3),&ref[171])
	fail_ret
	pl_jump  fun32()

long local pool_fun99[4]=[65538,build_ref_169,build_ref_171,pool_fun98]

pl_code local fun99
	call_c   Dyam_Pool(pool_fun99)
	call_c   Dyam_Unify_Item(&ref[169])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(1))
	move     V(6), R(2)
	move     S(5), R(3)
	pl_call  pred_label_term_2()
	call_c   Dyam_Choice(fun98)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_atom(V(1))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(3),&ref[171])
	fail_ret
	pl_jump  fun32()

;; TERM 170: '*GUARD*'(foreign_load_arg(+, output, _B, _C, _D, _E, _F)) :> []
c_code local build_ref_170
	ret_reg &ref[170]
	call_c   build_ref_169()
	call_c   Dyam_Create_Binary(I(9),&ref[169],I(0))
	move_ret ref[170]
	c_ret

c_code local build_seed_21
	ret_reg &seed[21]
	call_c   build_ref_1()
	call_c   build_ref_174()
	call_c   Dyam_Seed_Start(&ref[1],&ref[174],I(0),fun2,1)
	call_c   build_ref_173()
	call_c   Dyam_Seed_Add_Comp(&ref[173],fun102,0)
	call_c   Dyam_Seed_End()
	move_ret seed[21]
	c_ret

;; TERM 173: '*GUARD*'(foreign_load_arg(+, input, _B, _C, _D, _E, _F))
c_code local build_ref_173
	ret_reg &ref[173]
	call_c   build_ref_2()
	call_c   build_ref_172()
	call_c   Dyam_Create_Unary(&ref[2],&ref[172])
	move_ret ref[173]
	c_ret

;; TERM 172: foreign_load_arg(+, input, _B, _C, _D, _E, _F)
c_code local build_ref_172
	ret_reg &ref[172]
	call_c   build_ref_298()
	call_c   build_ref_47()
	call_c   build_ref_287()
	call_c   Dyam_Term_Start(&ref[298],7)
	call_c   Dyam_Term_Arg(&ref[47])
	call_c   Dyam_Term_Arg(&ref[287])
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[172]
	c_ret

;; TERM 175: reg_load_input(_C, _G)
c_code local build_ref_175
	ret_reg &ref[175]
	call_c   build_ref_335()
	call_c   Dyam_Create_Binary(&ref[335],V(2),V(6))
	move_ret ref[175]
	c_ret

;; TERM 335: reg_load_input
c_code local build_ref_335
	ret_reg &ref[335]
	call_c   Dyam_Create_Atom("reg_load_input")
	move_ret ref[335]
	c_ret

long local pool_fun100[3]=[65537,build_ref_175,pool_fun43]

pl_code local fun100
	call_c   Dyam_Update_Choice(fun43)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Reg_Load(0,V(1))
	pl_call  pred_check_var_1()
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(3),&ref[175])
	fail_ret
	pl_jump  fun32()

long local pool_fun101[3]=[65537,build_ref_175,pool_fun100]

pl_code local fun101
	call_c   Dyam_Update_Choice(fun100)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_number(V(1))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(3),&ref[175])
	fail_ret
	pl_jump  fun32()

long local pool_fun102[4]=[65538,build_ref_173,build_ref_175,pool_fun101]

pl_code local fun102
	call_c   Dyam_Pool(pool_fun102)
	call_c   Dyam_Unify_Item(&ref[173])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(1))
	move     V(6), R(2)
	move     S(5), R(3)
	pl_call  pred_label_term_2()
	call_c   Dyam_Choice(fun101)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_atom(V(1))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(3),&ref[175])
	fail_ret
	pl_jump  fun32()

;; TERM 174: '*GUARD*'(foreign_load_arg(+, input, _B, _C, _D, _E, _F)) :> []
c_code local build_ref_174
	ret_reg &ref[174]
	call_c   build_ref_173()
	call_c   Dyam_Create_Binary(I(9),&ref[173],I(0))
	move_ret ref[174]
	c_ret

c_code local build_seed_26
	ret_reg &seed[26]
	call_c   build_ref_1()
	call_c   build_ref_178()
	call_c   Dyam_Seed_Start(&ref[1],&ref[178],I(0),fun2,1)
	call_c   build_ref_177()
	call_c   Dyam_Seed_Add_Comp(&ref[177],fun105,0)
	call_c   Dyam_Seed_End()
	move_ret seed[26]
	c_ret

;; TERM 177: '*GUARD*'(foreign_load_arg(+, bool, _B, _C, _D, _E, _F))
c_code local build_ref_177
	ret_reg &ref[177]
	call_c   build_ref_2()
	call_c   build_ref_176()
	call_c   Dyam_Create_Unary(&ref[2],&ref[176])
	move_ret ref[177]
	c_ret

;; TERM 176: foreign_load_arg(+, bool, _B, _C, _D, _E, _F)
c_code local build_ref_176
	ret_reg &ref[176]
	call_c   build_ref_298()
	call_c   build_ref_47()
	call_c   build_ref_57()
	call_c   Dyam_Term_Start(&ref[298],7)
	call_c   Dyam_Term_Arg(&ref[47])
	call_c   Dyam_Term_Arg(&ref[57])
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[176]
	c_ret

;; TERM 183: reg_load_boolean(_C, _G)
c_code local build_ref_183
	ret_reg &ref[183]
	call_c   build_ref_336()
	call_c   Dyam_Create_Binary(&ref[336],V(2),V(6))
	move_ret ref[183]
	c_ret

;; TERM 336: reg_load_boolean
c_code local build_ref_336
	ret_reg &ref[336]
	call_c   Dyam_Create_Atom("reg_load_boolean")
	move_ret ref[336]
	c_ret

long local pool_fun103[3]=[65537,build_ref_183,pool_fun78]

pl_code local fun103
	call_c   Dyam_Update_Choice(fun78)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Reg_Load(0,V(1))
	pl_call  pred_check_var_1()
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(1))
	move     V(6), R(2)
	move     S(5), R(3)
	pl_call  pred_label_term_2()
	call_c   Dyam_Unify(V(3),&ref[183])
	fail_ret
	pl_jump  fun32()

;; TERM 182: c(0)
c_code local build_ref_182
	ret_reg &ref[182]
	call_c   build_ref_328()
	call_c   Dyam_Create_Unary(&ref[328],N(0))
	move_ret ref[182]
	c_ret

long local pool_fun104[5]=[65539,build_ref_181,build_ref_153,build_ref_182,pool_fun103]

pl_code local fun104
	call_c   Dyam_Update_Choice(fun103)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(1),&ref[181])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(3),&ref[153])
	fail_ret
	call_c   Dyam_Unify(V(4),&ref[182])
	fail_ret
	pl_jump  fun32()

;; TERM 180: c(1)
c_code local build_ref_180
	ret_reg &ref[180]
	call_c   build_ref_328()
	call_c   Dyam_Create_Unary(&ref[328],N(1))
	move_ret ref[180]
	c_ret

long local pool_fun105[6]=[65540,build_ref_177,build_ref_179,build_ref_153,build_ref_180,pool_fun104]

pl_code local fun105
	call_c   Dyam_Pool(pool_fun105)
	call_c   Dyam_Unify_Item(&ref[177])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun104)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(1),&ref[179])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(3),&ref[153])
	fail_ret
	call_c   Dyam_Unify(V(4),&ref[180])
	fail_ret
	pl_jump  fun32()

;; TERM 178: '*GUARD*'(foreign_load_arg(+, bool, _B, _C, _D, _E, _F)) :> []
c_code local build_ref_178
	ret_reg &ref[178]
	call_c   build_ref_177()
	call_c   Dyam_Create_Binary(I(9),&ref[177],I(0))
	move_ret ref[178]
	c_ret

c_code local build_seed_22
	ret_reg &seed[22]
	call_c   build_ref_1()
	call_c   build_ref_186()
	call_c   Dyam_Seed_Start(&ref[1],&ref[186],I(0),fun2,1)
	call_c   build_ref_185()
	call_c   Dyam_Seed_Add_Comp(&ref[185],fun110,0)
	call_c   Dyam_Seed_End()
	move_ret seed[22]
	c_ret

;; TERM 185: '*GUARD*'(foreign_load_arg(+, term, _B, _C, _D, _E, _F))
c_code local build_ref_185
	ret_reg &ref[185]
	call_c   build_ref_2()
	call_c   build_ref_184()
	call_c   Dyam_Create_Unary(&ref[2],&ref[184])
	move_ret ref[185]
	c_ret

;; TERM 184: foreign_load_arg(+, term, _B, _C, _D, _E, _F)
c_code local build_ref_184
	ret_reg &ref[184]
	call_c   build_ref_298()
	call_c   build_ref_47()
	call_c   build_ref_276()
	call_c   Dyam_Term_Start(&ref[298],7)
	call_c   Dyam_Term_Arg(&ref[47])
	call_c   Dyam_Term_Arg(&ref[276])
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[184]
	c_ret

long local pool_fun106[2]=[1,build_ref_166]

pl_code local fun107
	call_c   Dyam_Remove_Choice()
fun106:
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(3),&ref[166])
	fail_ret
	pl_jump  fun32()


long local pool_fun108[3]=[131072,pool_fun93,pool_fun106]

pl_code local fun108
	call_c   Dyam_Update_Choice(fun93)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Reg_Load(0,V(1))
	pl_call  pred_check_var_1()
	call_c   Dyam_Choice(fun107)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(5))
	pl_call  pred_var_in_tupple_2()
	call_c   Dyam_Cut()
	pl_fail

long local pool_fun109[3]=[65537,build_ref_165,pool_fun108]

pl_code local fun109
	call_c   Dyam_Update_Choice(fun108)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_atomic(V(1))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(3),&ref[165])
	fail_ret
	pl_jump  fun32()

long local pool_fun110[4]=[65538,build_ref_185,build_ref_164,pool_fun109]

pl_code local fun110
	call_c   Dyam_Pool(pool_fun110)
	call_c   Dyam_Unify_Item(&ref[185])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(1))
	move     V(6), R(2)
	move     S(5), R(3)
	pl_call  pred_label_term_2()
	call_c   Dyam_Choice(fun109)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(1),I(0))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(3),&ref[164])
	fail_ret
	pl_jump  fun32()

;; TERM 186: '*GUARD*'(foreign_load_arg(+, term, _B, _C, _D, _E, _F)) :> []
c_code local build_ref_186
	ret_reg &ref[186]
	call_c   build_ref_185()
	call_c   Dyam_Create_Binary(I(9),&ref[185],I(0))
	move_ret ref[186]
	c_ret

c_code local build_seed_27
	ret_reg &seed[27]
	call_c   build_ref_1()
	call_c   build_ref_189()
	call_c   Dyam_Seed_Start(&ref[1],&ref[189],I(0),fun2,1)
	call_c   build_ref_188()
	call_c   Dyam_Seed_Add_Comp(&ref[188],fun114,0)
	call_c   Dyam_Seed_End()
	move_ret seed[27]
	c_ret

;; TERM 188: '*GUARD*'(foreign_load_arg(+, ptr, _B, _C, _D, _E, _F))
c_code local build_ref_188
	ret_reg &ref[188]
	call_c   build_ref_2()
	call_c   build_ref_187()
	call_c   Dyam_Create_Unary(&ref[2],&ref[187])
	move_ret ref[188]
	c_ret

;; TERM 187: foreign_load_arg(+, ptr, _B, _C, _D, _E, _F)
c_code local build_ref_187
	ret_reg &ref[187]
	call_c   build_ref_298()
	call_c   build_ref_47()
	call_c   build_ref_293()
	call_c   Dyam_Term_Start(&ref[298],7)
	call_c   Dyam_Term_Arg(&ref[47])
	call_c   Dyam_Term_Arg(&ref[293])
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[187]
	c_ret

;; TERM 192: '~w address expected'
c_code local build_ref_192
	ret_reg &ref[192]
	call_c   Dyam_Create_Atom("~w address expected")
	move_ret ref[192]
	c_ret

long local pool_fun111[3]=[2,build_ref_192,build_ref_76]

pl_code local fun111
	call_c   Dyam_Remove_Choice()
	move     &ref[192], R(0)
	move     0, R(1)
	move     &ref[76], R(2)
	move     S(5), R(3)
	pl_call  pred_error_2()
	pl_jump  fun32()

;; TERM 191: reg_load_ptr(_C, _J)
c_code local build_ref_191
	ret_reg &ref[191]
	call_c   build_ref_337()
	call_c   Dyam_Create_Binary(&ref[337],V(2),V(9))
	move_ret ref[191]
	c_ret

;; TERM 337: reg_load_ptr
c_code local build_ref_337
	ret_reg &ref[337]
	call_c   Dyam_Create_Atom("reg_load_ptr")
	move_ret ref[337]
	c_ret

long local pool_fun112[3]=[65537,build_ref_191,pool_fun111]

pl_code local fun112
	call_c   Dyam_Update_Choice(fun111)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Reg_Load(0,V(1))
	pl_call  pred_check_var_1()
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(1))
	move     V(9), R(2)
	move     S(5), R(3)
	pl_call  pred_label_term_2()
	call_c   Dyam_Unify(V(3),&ref[191])
	fail_ret
	pl_jump  fun32()

;; TERM 190: '$fun'(_G, _H, _I)
c_code local build_ref_190
	ret_reg &ref[190]
	call_c   build_ref_338()
	call_c   Dyam_Term_Start(&ref[338],3)
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_End()
	move_ret ref[190]
	c_ret

;; TERM 338: '$fun'
c_code local build_ref_338
	ret_reg &ref[338]
	call_c   Dyam_Create_Atom("$fun")
	move_ret ref[338]
	c_ret

long local pool_fun113[4]=[65538,build_ref_190,build_ref_153,pool_fun112]

pl_code local fun113
	call_c   Dyam_Update_Choice(fun112)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[190])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(3),&ref[153])
	fail_ret
	call_c   Dyam_Unify(V(4),V(1))
	fail_ret
	pl_jump  fun32()

long local pool_fun114[5]=[65539,build_ref_188,build_ref_153,build_ref_154,pool_fun113]

pl_code local fun114
	call_c   Dyam_Pool(pool_fun114)
	call_c   Dyam_Unify_Item(&ref[188])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun113)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_number(V(1))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(3),&ref[153])
	fail_ret
	call_c   Dyam_Unify(V(4),&ref[154])
	fail_ret
	pl_jump  fun32()

;; TERM 189: '*GUARD*'(foreign_load_arg(+, ptr, _B, _C, _D, _E, _F)) :> []
c_code local build_ref_189
	ret_reg &ref[189]
	call_c   build_ref_188()
	call_c   Dyam_Create_Binary(I(9),&ref[188],I(0))
	move_ret ref[189]
	c_ret

c_code local build_seed_28
	ret_reg &seed[28]
	call_c   build_ref_1()
	call_c   build_ref_195()
	call_c   Dyam_Seed_Start(&ref[1],&ref[195],I(0),fun2,1)
	call_c   build_ref_194()
	call_c   Dyam_Seed_Add_Comp(&ref[194],fun117,0)
	call_c   Dyam_Seed_End()
	move_ret seed[28]
	c_ret

;; TERM 194: '*GUARD*'(foreign_load_arg(+, float, _B, _C, _D, _E, _F))
c_code local build_ref_194
	ret_reg &ref[194]
	call_c   build_ref_2()
	call_c   build_ref_193()
	call_c   Dyam_Create_Unary(&ref[2],&ref[193])
	move_ret ref[194]
	c_ret

;; TERM 193: foreign_load_arg(+, float, _B, _C, _D, _E, _F)
c_code local build_ref_193
	ret_reg &ref[193]
	call_c   build_ref_298()
	call_c   build_ref_47()
	call_c   build_ref_229()
	call_c   Dyam_Term_Start(&ref[298],7)
	call_c   Dyam_Term_Arg(&ref[47])
	call_c   Dyam_Term_Arg(&ref[229])
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[193]
	c_ret

;; TERM 197: reg_load_float(_C, _H)
c_code local build_ref_197
	ret_reg &ref[197]
	call_c   build_ref_339()
	call_c   Dyam_Create_Binary(&ref[339],V(2),V(7))
	move_ret ref[197]
	c_ret

;; TERM 339: reg_load_float
c_code local build_ref_339
	ret_reg &ref[339]
	call_c   Dyam_Create_Atom("reg_load_float")
	move_ret ref[339]
	c_ret

long local pool_fun115[3]=[65537,build_ref_197,pool_fun67]

pl_code local fun115
	call_c   Dyam_Update_Choice(fun67)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Reg_Load(0,V(1))
	pl_call  pred_check_var_1()
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(1))
	move     V(7), R(2)
	move     S(5), R(3)
	pl_call  pred_label_term_2()
	call_c   Dyam_Unify(V(3),&ref[197])
	fail_ret
	pl_jump  fun32()

;; TERM 196: 1.00000 * _B
c_code local build_ref_196
	ret_reg &ref[196]
	call_c   build_ref_66()
	call_c   Dyam_Create_Binary(&ref[66],F(1.00000),V(1))
	move_ret ref[196]
	c_ret

long local pool_fun116[5]=[65539,build_ref_153,build_ref_196,build_ref_159,pool_fun115]

pl_code local fun116
	call_c   Dyam_Update_Choice(fun115)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_number(V(1))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(3),&ref[153])
	fail_ret
	call_c   DYAM_evpred_is(V(6),&ref[196])
	fail_ret
	call_c   Dyam_Unify(V(4),&ref[159])
	fail_ret
	pl_jump  fun32()

long local pool_fun117[5]=[65539,build_ref_194,build_ref_153,build_ref_154,pool_fun116]

pl_code local fun117
	call_c   Dyam_Pool(pool_fun117)
	call_c   Dyam_Unify_Item(&ref[194])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun116)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_float(V(1))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(3),&ref[153])
	fail_ret
	call_c   Dyam_Unify(V(4),&ref[154])
	fail_ret
	pl_jump  fun32()

;; TERM 195: '*GUARD*'(foreign_load_arg(+, float, _B, _C, _D, _E, _F)) :> []
c_code local build_ref_195
	ret_reg &ref[195]
	call_c   build_ref_194()
	call_c   Dyam_Create_Binary(I(9),&ref[194],I(0))
	move_ret ref[195]
	c_ret

c_code local build_seed_41
	ret_reg &seed[41]
	call_c   build_ref_1()
	call_c   build_ref_200()
	call_c   Dyam_Seed_Start(&ref[1],&ref[200],I(0),fun2,1)
	call_c   build_ref_199()
	call_c   Dyam_Seed_Add_Comp(&ref[199],fun130,0)
	call_c   Dyam_Seed_End()
	move_ret seed[41]
	c_ret

;; TERM 199: '*GUARD*'(foreign_parse_template(_B, interface{return=> _C, name=> _D, code=> _E, choice_size=> _F, load=> _G}, _H))
c_code local build_ref_199
	ret_reg &ref[199]
	call_c   build_ref_2()
	call_c   build_ref_198()
	call_c   Dyam_Create_Unary(&ref[2],&ref[198])
	move_ret ref[199]
	c_ret

;; TERM 198: foreign_parse_template(_B, interface{return=> _C, name=> _D, code=> _E, choice_size=> _F, load=> _G}, _H)
c_code local build_ref_198
	ret_reg &ref[198]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_305()
	call_c   Dyam_Term_Start(&ref[305],5)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_312()
	call_c   Dyam_Term_Start(&ref[312],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[198]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 243: '~w is not a valid C name'
c_code local build_ref_243
	ret_reg &ref[243]
	call_c   Dyam_Create_Atom("~w is not a valid C name")
	move_ret ref[243]
	c_ret

;; TERM 244: [_D]
c_code local build_ref_244
	ret_reg &ref[244]
	call_c   Dyam_Create_List(V(3),I(0))
	move_ret ref[244]
	c_ret

;; TERM 237: c_call(_D, _M, bool, none)
c_code local build_ref_237
	ret_reg &ref[237]
	call_c   build_ref_340()
	call_c   build_ref_57()
	call_c   build_ref_58()
	call_c   Dyam_Term_Start(&ref[340],4)
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(&ref[57])
	call_c   Dyam_Term_Arg(&ref[58])
	call_c   Dyam_Term_End()
	move_ret ref[237]
	c_ret

;; TERM 340: c_call
c_code local build_ref_340
	ret_reg &ref[340]
	call_c   Dyam_Create_Atom("c_call")
	move_ret ref[340]
	c_ret

;; TERM 212: choice_label
c_code local build_ref_212
	ret_reg &ref[212]
	call_c   Dyam_Create_Atom("choice_label")
	move_ret ref[212]
	c_ret

;; TERM 213: 'L~w~w_choice_~w'
c_code local build_ref_213
	ret_reg &ref[213]
	call_c   Dyam_Create_Atom("L~w~w_choice_~w")
	move_ret ref[213]
	c_ret

;; TERM 214: [_D,_J,_V]
c_code local build_ref_214
	ret_reg &ref[214]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(21),I(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(9),R(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(3),R(0))
	move_ret ref[214]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 215: '$label'(_W)
c_code local build_ref_215
	ret_reg &ref[215]
	call_c   build_ref_341()
	call_c   Dyam_Create_Unary(&ref[341],V(22))
	move_ret ref[215]
	c_ret

;; TERM 341: '$label'
c_code local build_ref_341
	ret_reg &ref[341]
	call_c   Dyam_Create_Atom("$label")
	move_ret ref[341]
	c_ret

c_code local build_seed_49
	ret_reg &seed[49]
	call_c   build_ref_2()
	call_c   build_ref_217()
	call_c   Dyam_Seed_Start(&ref[2],&ref[217],I(0),fun20,1)
	call_c   build_ref_218()
	call_c   Dyam_Seed_Add_Comp(&ref[218],&ref[217],0)
	call_c   Dyam_Seed_End()
	move_ret seed[49]
	c_ret

;; TERM 218: '*GUARD*'(foreign_parse_init_load(_G, 0, _Y)) :> '$$HOLE$$'
c_code local build_ref_218
	ret_reg &ref[218]
	call_c   build_ref_217()
	call_c   Dyam_Create_Binary(I(9),&ref[217],I(7))
	move_ret ref[218]
	c_ret

;; TERM 217: '*GUARD*'(foreign_parse_init_load(_G, 0, _Y))
c_code local build_ref_217
	ret_reg &ref[217]
	call_c   build_ref_2()
	call_c   build_ref_216()
	call_c   Dyam_Create_Unary(&ref[2],&ref[216])
	move_ret ref[217]
	c_ret

;; TERM 216: foreign_parse_init_load(_G, 0, _Y)
c_code local build_ref_216
	ret_reg &ref[216]
	call_c   build_ref_282()
	call_c   Dyam_Term_Start(&ref[282],3)
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(N(0))
	call_c   Dyam_Term_Arg(V(24))
	call_c   Dyam_Term_End()
	move_ret ref[216]
	c_ret

;; TERM 224: c_call('Dyam_Foreign_Create_Choice', [_X,c(_J),c(_F)], none, none) :> _Y :> label(_X) :> c_call('Dyam_Foreign_Update_Choice', [_X,c(_J),c(_F)], none, none)
c_code local build_ref_224
	ret_reg &ref[224]
	call_c   build_ref_219()
	call_c   build_ref_223()
	call_c   Dyam_Create_Binary(I(9),&ref[219],&ref[223])
	move_ret ref[224]
	c_ret

;; TERM 223: _Y :> label(_X) :> c_call('Dyam_Foreign_Update_Choice', [_X,c(_J),c(_F)], none, none)
c_code local build_ref_223
	ret_reg &ref[223]
	call_c   build_ref_222()
	call_c   Dyam_Create_Binary(I(9),V(24),&ref[222])
	move_ret ref[223]
	c_ret

;; TERM 222: label(_X) :> c_call('Dyam_Foreign_Update_Choice', [_X,c(_J),c(_F)], none, none)
c_code local build_ref_222
	ret_reg &ref[222]
	call_c   build_ref_220()
	call_c   build_ref_221()
	call_c   Dyam_Create_Binary(I(9),&ref[220],&ref[221])
	move_ret ref[222]
	c_ret

;; TERM 221: c_call('Dyam_Foreign_Update_Choice', [_X,c(_J),c(_F)], none, none)
c_code local build_ref_221
	ret_reg &ref[221]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_328()
	call_c   Dyam_Create_Unary(&ref[328],V(9))
	move_ret R(0)
	call_c   Dyam_Create_Unary(&ref[328],V(5))
	move_ret R(1)
	call_c   Dyam_Create_List(R(1),I(0))
	move_ret R(1)
	call_c   Dyam_Create_List(R(0),R(1))
	move_ret R(1)
	call_c   Dyam_Create_List(V(23),R(1))
	move_ret R(1)
	call_c   build_ref_340()
	call_c   build_ref_342()
	call_c   build_ref_58()
	call_c   Dyam_Term_Start(&ref[340],4)
	call_c   Dyam_Term_Arg(&ref[342])
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(&ref[58])
	call_c   Dyam_Term_Arg(&ref[58])
	call_c   Dyam_Term_End()
	move_ret ref[221]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 342: 'Dyam_Foreign_Update_Choice'
c_code local build_ref_342
	ret_reg &ref[342]
	call_c   Dyam_Create_Atom("Dyam_Foreign_Update_Choice")
	move_ret ref[342]
	c_ret

;; TERM 220: label(_X)
c_code local build_ref_220
	ret_reg &ref[220]
	call_c   build_ref_343()
	call_c   Dyam_Create_Unary(&ref[343],V(23))
	move_ret ref[220]
	c_ret

;; TERM 343: label
c_code local build_ref_343
	ret_reg &ref[343]
	call_c   Dyam_Create_Atom("label")
	move_ret ref[343]
	c_ret

;; TERM 219: c_call('Dyam_Foreign_Create_Choice', [_X,c(_J),c(_F)], none, none)
c_code local build_ref_219
	ret_reg &ref[219]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_328()
	call_c   Dyam_Create_Unary(&ref[328],V(9))
	move_ret R(0)
	call_c   Dyam_Create_Unary(&ref[328],V(5))
	move_ret R(1)
	call_c   Dyam_Create_List(R(1),I(0))
	move_ret R(1)
	call_c   Dyam_Create_List(R(0),R(1))
	move_ret R(1)
	call_c   Dyam_Create_List(V(23),R(1))
	move_ret R(1)
	call_c   build_ref_340()
	call_c   build_ref_344()
	call_c   build_ref_58()
	call_c   Dyam_Term_Start(&ref[340],4)
	call_c   Dyam_Term_Arg(&ref[344])
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(&ref[58])
	call_c   Dyam_Term_Arg(&ref[58])
	call_c   Dyam_Term_End()
	move_ret ref[219]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 344: 'Dyam_Foreign_Create_Choice'
c_code local build_ref_344
	ret_reg &ref[344]
	call_c   Dyam_Create_Atom("Dyam_Foreign_Create_Choice")
	move_ret ref[344]
	c_ret

;; TERM 211: _L :> _U :> _O :> _N :> _P :> reg_reset(_K)
c_code local build_ref_211
	ret_reg &ref[211]
	call_c   build_ref_210()
	call_c   Dyam_Create_Binary(I(9),V(11),&ref[210])
	move_ret ref[211]
	c_ret

;; TERM 210: _U :> _O :> _N :> _P :> reg_reset(_K)
c_code local build_ref_210
	ret_reg &ref[210]
	call_c   build_ref_209()
	call_c   Dyam_Create_Binary(I(9),V(20),&ref[209])
	move_ret ref[210]
	c_ret

;; TERM 209: _O :> _N :> _P :> reg_reset(_K)
c_code local build_ref_209
	ret_reg &ref[209]
	call_c   build_ref_208()
	call_c   Dyam_Create_Binary(I(9),V(14),&ref[208])
	move_ret ref[209]
	c_ret

;; TERM 208: _N :> _P :> reg_reset(_K)
c_code local build_ref_208
	ret_reg &ref[208]
	call_c   build_ref_207()
	call_c   Dyam_Create_Binary(I(9),V(13),&ref[207])
	move_ret ref[208]
	c_ret

;; TERM 207: _P :> reg_reset(_K)
c_code local build_ref_207
	ret_reg &ref[207]
	call_c   build_ref_206()
	call_c   Dyam_Create_Binary(I(9),V(15),&ref[206])
	move_ret ref[207]
	c_ret

;; TERM 206: reg_reset(_K)
c_code local build_ref_206
	ret_reg &ref[206]
	call_c   build_ref_345()
	call_c   Dyam_Create_Unary(&ref[345],V(10))
	move_ret ref[206]
	c_ret

;; TERM 345: reg_reset
c_code local build_ref_345
	ret_reg &ref[345]
	call_c   Dyam_Create_Atom("reg_reset")
	move_ret ref[345]
	c_ret

long local pool_fun119[8]=[7,build_ref_212,build_ref_213,build_ref_214,build_ref_215,build_seed_49,build_ref_224,build_ref_211]

pl_code local fun119
	call_c   Dyam_Remove_Choice()
	move     &ref[212], R(0)
	move     0, R(1)
	move     V(21), R(2)
	move     S(5), R(3)
	pl_call  pred_update_counter_2()
	move     &ref[213], R(0)
	move     0, R(1)
	move     &ref[214], R(2)
	move     S(5), R(3)
	move     V(22), R(4)
	move     S(5), R(5)
	pl_call  pred_name_builder_3()
	call_c   Dyam_Unify(V(23),&ref[215])
	fail_ret
	pl_call  fun21(&seed[49],1)
	call_c   Dyam_Unify(V(20),&ref[224])
	fail_ret
fun118:
	call_c   Dyam_Unify(V(4),&ref[211])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret


long local pool_fun120[5]=[65539,build_ref_58,build_ref_153,build_ref_211,pool_fun119]

long local pool_fun123[4]=[65538,build_ref_237,build_ref_153,pool_fun120]

pl_code local fun123
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(14),&ref[237])
	fail_ret
	call_c   Dyam_Unify(V(15),&ref[153])
	fail_ret
fun120:
	call_c   Dyam_Choice(fun119)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(5),&ref[58])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(20),&ref[153])
	fail_ret
	pl_jump  fun118()


;; TERM 234: call
c_code local build_ref_234
	ret_reg &ref[234]
	call_c   Dyam_Create_Atom("call")
	move_ret ref[234]
	c_ret

;; TERM 235: c_call(_D, _M, bool, '$reg'(0))
c_code local build_ref_235
	ret_reg &ref[235]
	call_c   build_ref_340()
	call_c   build_ref_57()
	call_c   build_ref_232()
	call_c   Dyam_Term_Start(&ref[340],4)
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(&ref[57])
	call_c   Dyam_Term_Arg(&ref[232])
	call_c   Dyam_Term_End()
	move_ret ref[235]
	c_ret

;; TERM 232: '$reg'(0)
c_code local build_ref_232
	ret_reg &ref[232]
	call_c   build_ref_346()
	call_c   Dyam_Create_Unary(&ref[346],N(0))
	move_ret ref[232]
	c_ret

;; TERM 346: '$reg'
c_code local build_ref_346
	ret_reg &ref[346]
	call_c   Dyam_Create_Atom("$reg")
	move_ret ref[346]
	c_ret

;; TERM 236: call('$reg'(0), [])
c_code local build_ref_236
	ret_reg &ref[236]
	call_c   build_ref_234()
	call_c   build_ref_232()
	call_c   Dyam_Create_Binary(&ref[234],&ref[232],I(0))
	move_ret ref[236]
	c_ret

long local pool_fun124[6]=[131075,build_ref_234,build_ref_235,build_ref_236,pool_fun123,pool_fun120]

pl_code local fun124
	call_c   Dyam_Update_Choice(fun123)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(2),&ref[234])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(14),&ref[235])
	fail_ret
	call_c   Dyam_Unify(V(15),&ref[236])
	fail_ret
	pl_jump  fun120()

;; TERM 233: c_call(_D, _M, none, none)
c_code local build_ref_233
	ret_reg &ref[233]
	call_c   build_ref_340()
	call_c   build_ref_58()
	call_c   Dyam_Term_Start(&ref[340],4)
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(&ref[58])
	call_c   Dyam_Term_Arg(&ref[58])
	call_c   Dyam_Term_End()
	move_ret ref[233]
	c_ret

long local pool_fun125[6]=[131075,build_ref_58,build_ref_233,build_ref_153,pool_fun124,pool_fun120]

pl_code local fun125
	call_c   Dyam_Update_Choice(fun124)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(2),&ref[58])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(14),&ref[233])
	fail_ret
	call_c   Dyam_Unify(V(15),&ref[153])
	fail_ret
	pl_jump  fun120()

;; TERM 225: _Q : _R
c_code local build_ref_225
	ret_reg &ref[225]
	call_c   build_ref_314()
	call_c   Dyam_Create_Binary(&ref[314],V(16),V(17))
	move_ret ref[225]
	c_ret

c_code local build_seed_50
	ret_reg &seed[50]
	call_c   build_ref_2()
	call_c   build_ref_227()
	call_c   Dyam_Seed_Start(&ref[2],&ref[227],I(0),fun20,1)
	call_c   build_ref_228()
	call_c   Dyam_Seed_Add_Comp(&ref[228],&ref[227],0)
	call_c   Dyam_Seed_End()
	move_ret seed[50]
	c_ret

;; TERM 228: '*GUARD*'(foreign_unify_arg(-, _R, _Q, 0, _S)) :> '$$HOLE$$'
c_code local build_ref_228
	ret_reg &ref[228]
	call_c   build_ref_227()
	call_c   Dyam_Create_Binary(I(9),&ref[227],I(7))
	move_ret ref[228]
	c_ret

;; TERM 227: '*GUARD*'(foreign_unify_arg(-, _R, _Q, 0, _S))
c_code local build_ref_227
	ret_reg &ref[227]
	call_c   build_ref_2()
	call_c   build_ref_226()
	call_c   Dyam_Create_Unary(&ref[2],&ref[226])
	move_ret ref[227]
	c_ret

;; TERM 226: foreign_unify_arg(-, _R, _Q, 0, _S)
c_code local build_ref_226
	ret_reg &ref[226]
	call_c   build_ref_283()
	call_c   build_ref_267()
	call_c   Dyam_Term_Start(&ref[283],5)
	call_c   Dyam_Term_Arg(&ref[267])
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(N(0))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_End()
	move_ret ref[226]
	c_ret

;; TERM 231: c_call(_D, _M, none, _T)
c_code local build_ref_231
	ret_reg &ref[231]
	call_c   build_ref_340()
	call_c   build_ref_58()
	call_c   Dyam_Term_Start(&ref[340],4)
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(&ref[58])
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_End()
	move_ret ref[231]
	c_ret

long local pool_fun121[3]=[65537,build_ref_231,pool_fun120]

long local pool_fun122[3]=[65537,build_ref_232,pool_fun121]

pl_code local fun122
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(19),&ref[232])
	fail_ret
fun121:
	call_c   Dyam_Unify(V(14),&ref[231])
	fail_ret
	call_c   Dyam_Unify(V(15),V(18))
	fail_ret
	pl_jump  fun120()


;; TERM 230: '$freg'(0)
c_code local build_ref_230
	ret_reg &ref[230]
	call_c   build_ref_347()
	call_c   Dyam_Create_Unary(&ref[347],N(0))
	move_ret ref[230]
	c_ret

;; TERM 347: '$freg'
c_code local build_ref_347
	ret_reg &ref[347]
	call_c   Dyam_Create_Atom("$freg")
	move_ret ref[347]
	c_ret

long local pool_fun126[8]=[196612,build_ref_225,build_seed_50,build_ref_229,build_ref_230,pool_fun125,pool_fun122,pool_fun121]

pl_code local fun126
	call_c   Dyam_Update_Choice(fun125)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(2),&ref[225])
	fail_ret
	call_c   Dyam_Cut()
	pl_call  fun21(&seed[50],1)
	call_c   Dyam_Choice(fun122)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(17),&ref[229])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(19),&ref[230])
	fail_ret
	pl_jump  fun121()

;; TERM 201: '$$repeat'
c_code local build_ref_201
	ret_reg &ref[201]
	call_c   Dyam_Create_Atom("$$repeat")
	move_ret ref[201]
	c_ret

long local pool_fun127[5]=[131074,build_ref_201,build_ref_153,pool_fun126,pool_fun120]

long local pool_fun128[4]=[65538,build_ref_243,build_ref_244,pool_fun127]

pl_code local fun128
	call_c   Dyam_Remove_Choice()
	move     &ref[243], R(0)
	move     0, R(1)
	move     &ref[244], R(2)
	move     S(5), R(3)
	pl_call  pred_error_2()
fun127:
	call_c   Dyam_Choice(fun126)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(1),&ref[201])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(14),&ref[153])
	fail_ret
	call_c   Dyam_Unify(V(15),&ref[153])
	fail_ret
	pl_jump  fun120()


;; TERM 238: [_D|_I]
c_code local build_ref_238
	ret_reg &ref[238]
	call_c   Dyam_Create_List(V(3),V(8))
	move_ret ref[238]
	c_ret

;; TERM 239: _J + 1
c_code local build_ref_239
	ret_reg &ref[239]
	call_c   build_ref_47()
	call_c   Dyam_Create_Binary(&ref[47],V(9),N(1))
	move_ret ref[239]
	c_ret

c_code local build_seed_51
	ret_reg &seed[51]
	call_c   build_ref_2()
	call_c   build_ref_241()
	call_c   Dyam_Seed_Start(&ref[2],&ref[241],I(0),fun20,1)
	call_c   build_ref_242()
	call_c   Dyam_Seed_Add_Comp(&ref[242],&ref[241],0)
	call_c   Dyam_Seed_End()
	move_ret seed[51]
	c_ret

;; TERM 242: '*GUARD*'(foreign_parse_template_args(_I, 1, _H, _L, _M, _N)) :> '$$HOLE$$'
c_code local build_ref_242
	ret_reg &ref[242]
	call_c   build_ref_241()
	call_c   Dyam_Create_Binary(I(9),&ref[241],I(7))
	move_ret ref[242]
	c_ret

;; TERM 241: '*GUARD*'(foreign_parse_template_args(_I, 1, _H, _L, _M, _N))
c_code local build_ref_241
	ret_reg &ref[241]
	call_c   build_ref_2()
	call_c   build_ref_240()
	call_c   Dyam_Create_Unary(&ref[2],&ref[240])
	move_ret ref[241]
	c_ret

;; TERM 240: foreign_parse_template_args(_I, 1, _H, _L, _M, _N)
c_code local build_ref_240
	ret_reg &ref[240]
	call_c   build_ref_284()
	call_c   Dyam_Term_Start(&ref[284],6)
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(N(1))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_End()
	move_ret ref[240]
	c_ret

long local pool_fun129[6]=[131075,build_ref_238,build_ref_239,build_seed_51,pool_fun128,pool_fun127]

pl_code local fun129
	call_c   Dyam_Update_Choice(fun128)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_univ(V(1),&ref[238])
	fail_ret
	call_c   DYAM_evpred_atom(V(3))
	fail_ret
	call_c   Dyam_Cut()
	call_c   DYAM_evpred_functor(V(1),V(3),V(9))
	fail_ret
	call_c   DYAM_evpred_is(V(10),&ref[239])
	fail_ret
	pl_call  fun21(&seed[51],1)
	pl_jump  fun127()

;; TERM 202: repeat
c_code local build_ref_202
	ret_reg &ref[202]
	call_c   Dyam_Create_Atom("repeat")
	move_ret ref[202]
	c_ret

c_code local build_seed_48
	ret_reg &seed[48]
	call_c   build_ref_2()
	call_c   build_ref_204()
	call_c   Dyam_Seed_Start(&ref[2],&ref[204],I(0),fun20,1)
	call_c   build_ref_205()
	call_c   Dyam_Seed_Add_Comp(&ref[205],&ref[204],0)
	call_c   Dyam_Seed_End()
	move_ret seed[48]
	c_ret

;; TERM 205: '*GUARD*'(foreign_parse_template_args([], 1, _H, _L, _M, _N)) :> '$$HOLE$$'
c_code local build_ref_205
	ret_reg &ref[205]
	call_c   build_ref_204()
	call_c   Dyam_Create_Binary(I(9),&ref[204],I(7))
	move_ret ref[205]
	c_ret

;; TERM 204: '*GUARD*'(foreign_parse_template_args([], 1, _H, _L, _M, _N))
c_code local build_ref_204
	ret_reg &ref[204]
	call_c   build_ref_2()
	call_c   build_ref_203()
	call_c   Dyam_Create_Unary(&ref[2],&ref[203])
	move_ret ref[204]
	c_ret

;; TERM 203: foreign_parse_template_args([], 1, _H, _L, _M, _N)
c_code local build_ref_203
	ret_reg &ref[203]
	call_c   build_ref_284()
	call_c   Dyam_Term_Start(&ref[284],6)
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(N(1))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_End()
	move_ret ref[203]
	c_ret

long local pool_fun130[7]=[131076,build_ref_199,build_ref_201,build_ref_202,build_seed_48,pool_fun129,pool_fun127]

pl_code local fun130
	call_c   Dyam_Pool(pool_fun130)
	call_c   Dyam_Unify_Item(&ref[199])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun129)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(1),&ref[201])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(3),&ref[202])
	fail_ret
	call_c   Dyam_Unify(V(8),I(0))
	fail_ret
	call_c   Dyam_Unify(V(9),N(0))
	fail_ret
	call_c   Dyam_Unify(V(10),N(1))
	fail_ret
	pl_call  fun21(&seed[48],1)
	pl_jump  fun127()

;; TERM 200: '*GUARD*'(foreign_parse_template(_B, interface{return=> _C, name=> _D, code=> _E, choice_size=> _F, load=> _G}, _H)) :> []
c_code local build_ref_200
	ret_reg &ref[200]
	call_c   build_ref_199()
	call_c   Dyam_Create_Binary(I(9),&ref[199],I(0))
	move_ret ref[200]
	c_ret

c_code local build_seed_42
	ret_reg &seed[42]
	call_c   build_ref_1()
	call_c   build_ref_247()
	call_c   Dyam_Seed_Start(&ref[1],&ref[247],I(0),fun2,1)
	call_c   build_ref_246()
	call_c   Dyam_Seed_Add_Comp(&ref[246],fun139,0)
	call_c   Dyam_Seed_End()
	move_ret seed[42]
	c_ret

;; TERM 246: '*GUARD*'(foreign_parse_options([_B|_C], interface{return=> _D, name=> _E, code=> _F, choice_size=> _G, load=> _H}))
c_code local build_ref_246
	ret_reg &ref[246]
	call_c   build_ref_2()
	call_c   build_ref_245()
	call_c   Dyam_Create_Unary(&ref[2],&ref[245])
	move_ret ref[246]
	c_ret

;; TERM 245: foreign_parse_options([_B|_C], interface{return=> _D, name=> _E, code=> _F, choice_size=> _G, load=> _H})
c_code local build_ref_245
	ret_reg &ref[245]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   Dyam_Create_List(V(1),V(2))
	move_ret R(0)
	call_c   build_ref_305()
	call_c   Dyam_Term_Start(&ref[305],5)
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret R(1)
	call_c   build_ref_281()
	call_c   Dyam_Create_Binary(&ref[281],R(0),R(1))
	move_ret ref[245]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 259: 'unexpected option ~w'
c_code local build_ref_259
	ret_reg &ref[259]
	call_c   Dyam_Create_Atom("unexpected option ~w")
	move_ret ref[259]
	c_ret

c_code local build_seed_52
	ret_reg &seed[52]
	call_c   build_ref_2()
	call_c   build_ref_251()
	call_c   Dyam_Seed_Start(&ref[2],&ref[251],I(0),fun20,1)
	call_c   build_ref_252()
	call_c   Dyam_Seed_Add_Comp(&ref[252],&ref[251],0)
	call_c   Dyam_Seed_End()
	move_ret seed[52]
	c_ret

;; TERM 252: '*GUARD*'(foreign_parse_options(_C, interface{return=> _D, name=> _E, code=> _F, choice_size=> _G, load=> _H})) :> '$$HOLE$$'
c_code local build_ref_252
	ret_reg &ref[252]
	call_c   build_ref_251()
	call_c   Dyam_Create_Binary(I(9),&ref[251],I(7))
	move_ret ref[252]
	c_ret

;; TERM 251: '*GUARD*'(foreign_parse_options(_C, interface{return=> _D, name=> _E, code=> _F, choice_size=> _G, load=> _H}))
c_code local build_ref_251
	ret_reg &ref[251]
	call_c   build_ref_2()
	call_c   build_ref_250()
	call_c   Dyam_Create_Unary(&ref[2],&ref[250])
	move_ret ref[251]
	c_ret

;; TERM 250: foreign_parse_options(_C, interface{return=> _D, name=> _E, code=> _F, choice_size=> _G, load=> _H})
c_code local build_ref_250
	ret_reg &ref[250]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_305()
	call_c   Dyam_Term_Start(&ref[305],5)
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_281()
	call_c   Dyam_Create_Binary(&ref[281],V(2),R(0))
	move_ret ref[250]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun131[2]=[1,build_seed_52]

long local pool_fun133[4]=[65538,build_ref_259,build_ref_76,pool_fun131]

pl_code local fun133
	call_c   Dyam_Remove_Choice()
	move     &ref[259], R(0)
	move     0, R(1)
	move     &ref[76], R(2)
	move     S(5), R(3)
	pl_call  pred_error_2()
fun131:
	call_c   Dyam_Deallocate()
	pl_jump  fun21(&seed[52],1)


;; TERM 257: choice_size(_G)
c_code local build_ref_257
	ret_reg &ref[257]
	call_c   build_ref_310()
	call_c   Dyam_Create_Unary(&ref[310],V(6))
	move_ret ref[257]
	c_ret

;; TERM 258: 'positive number expected in ~w'
c_code local build_ref_258
	ret_reg &ref[258]
	call_c   Dyam_Create_Atom("positive number expected in ~w")
	move_ret ref[258]
	c_ret

long local pool_fun132[4]=[65538,build_ref_258,build_ref_76,pool_fun131]

pl_code local fun132
	call_c   Dyam_Remove_Choice()
	move     &ref[258], R(0)
	move     0, R(1)
	move     &ref[76], R(2)
	move     S(5), R(3)
	pl_call  pred_error_2()
	pl_jump  fun131()

long local pool_fun134[5]=[196609,build_ref_257,pool_fun133,pool_fun132,pool_fun131]

pl_code local fun134
	call_c   Dyam_Update_Choice(fun133)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[257])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun132)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_ge(V(6),N(0))
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun131()

;; TERM 256: load(_H)
c_code local build_ref_256
	ret_reg &ref[256]
	call_c   build_ref_311()
	call_c   Dyam_Create_Unary(&ref[311],V(7))
	move_ret ref[256]
	c_ret

long local pool_fun135[4]=[131073,build_ref_256,pool_fun134,pool_fun131]

pl_code local fun135
	call_c   Dyam_Update_Choice(fun134)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[256])
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun131()

;; TERM 255: return(none)
c_code local build_ref_255
	ret_reg &ref[255]
	call_c   build_ref_307()
	call_c   build_ref_58()
	call_c   Dyam_Create_Unary(&ref[307],&ref[58])
	move_ret ref[255]
	c_ret

long local pool_fun136[5]=[131074,build_ref_255,build_ref_58,pool_fun135,pool_fun131]

pl_code local fun136
	call_c   Dyam_Update_Choice(fun135)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[255])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(3),&ref[58])
	fail_ret
	pl_jump  fun131()

;; TERM 254: return(call)
c_code local build_ref_254
	ret_reg &ref[254]
	call_c   build_ref_307()
	call_c   build_ref_234()
	call_c   Dyam_Create_Unary(&ref[307],&ref[234])
	move_ret ref[254]
	c_ret

long local pool_fun137[5]=[131074,build_ref_254,build_ref_234,pool_fun136,pool_fun131]

pl_code local fun137
	call_c   Dyam_Update_Choice(fun136)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[254])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(3),&ref[234])
	fail_ret
	pl_jump  fun131()

;; TERM 253: return(bool)
c_code local build_ref_253
	ret_reg &ref[253]
	call_c   build_ref_307()
	call_c   build_ref_57()
	call_c   Dyam_Create_Unary(&ref[307],&ref[57])
	move_ret ref[253]
	c_ret

long local pool_fun138[5]=[131074,build_ref_253,build_ref_57,pool_fun137,pool_fun131]

pl_code local fun138
	call_c   Dyam_Update_Choice(fun137)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[253])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(3),&ref[57])
	fail_ret
	pl_jump  fun131()

;; TERM 248: return((_I : _J))
c_code local build_ref_248
	ret_reg &ref[248]
	call_c   build_ref_307()
	call_c   build_ref_249()
	call_c   Dyam_Create_Unary(&ref[307],&ref[249])
	move_ret ref[248]
	c_ret

;; TERM 249: _I : _J
c_code local build_ref_249
	ret_reg &ref[249]
	call_c   build_ref_314()
	call_c   Dyam_Create_Binary(&ref[314],V(8),V(9))
	move_ret ref[249]
	c_ret

long local pool_fun139[6]=[131075,build_ref_246,build_ref_248,build_ref_249,pool_fun138,pool_fun131]

pl_code local fun139
	call_c   Dyam_Pool(pool_fun139)
	call_c   Dyam_Unify_Item(&ref[246])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun138)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[248])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(9))
	pl_call  pred_check_foreign_type_1()
	call_c   Dyam_Unify(V(3),&ref[249])
	fail_ret
	pl_jump  fun131()

;; TERM 247: '*GUARD*'(foreign_parse_options([_B|_C], interface{return=> _D, name=> _E, code=> _F, choice_size=> _G, load=> _H})) :> []
c_code local build_ref_247
	ret_reg &ref[247]
	call_c   build_ref_246()
	call_c   Dyam_Create_Binary(I(9),&ref[246],I(0))
	move_ret ref[247]
	c_ret

c_code local build_seed_39
	ret_reg &seed[39]
	call_c   build_ref_1()
	call_c   build_ref_262()
	call_c   Dyam_Seed_Start(&ref[1],&ref[262],I(0),fun2,1)
	call_c   build_ref_261()
	call_c   Dyam_Seed_Add_Comp(&ref[261],fun148,0)
	call_c   Dyam_Seed_End()
	move_ret seed[39]
	c_ret

;; TERM 261: '*GUARD*'(foreign_parse_template_args([_B|_C], _D, _E, (_F :> _G), [_H|_I], (_J :> _K)))
c_code local build_ref_261
	ret_reg &ref[261]
	call_c   build_ref_2()
	call_c   build_ref_260()
	call_c   Dyam_Create_Unary(&ref[2],&ref[260])
	move_ret ref[261]
	c_ret

;; TERM 260: foreign_parse_template_args([_B|_C], _D, _E, (_F :> _G), [_H|_I], (_J :> _K))
c_code local build_ref_260
	ret_reg &ref[260]
	call_c   Dyam_Pseudo_Choice(4)
	call_c   Dyam_Create_List(V(1),V(2))
	move_ret R(0)
	call_c   Dyam_Create_Binary(I(9),V(5),V(6))
	move_ret R(1)
	call_c   Dyam_Create_List(V(7),V(8))
	move_ret R(2)
	call_c   Dyam_Create_Binary(I(9),V(9),V(10))
	move_ret R(3)
	call_c   build_ref_284()
	call_c   Dyam_Term_Start(&ref[284],6)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(R(2))
	call_c   Dyam_Term_Arg(R(3))
	call_c   Dyam_Term_End()
	move_ret ref[260]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 280: '~w is not a valid (exp : mode type)'
c_code local build_ref_280
	ret_reg &ref[280]
	call_c   Dyam_Create_Atom("~w is not a valid (exp : mode type)")
	move_ret ref[280]
	c_ret

long local pool_fun147[3]=[2,build_ref_280,build_ref_76]

pl_code local fun147
	call_c   Dyam_Remove_Choice()
	move     &ref[280], R(0)
	move     0, R(1)
	move     &ref[76], R(2)
	move     S(5), R(3)
	pl_call  pred_error_2()
	pl_jump  fun32()

;; TERM 263: _L : _M
c_code local build_ref_263
	ret_reg &ref[263]
	call_c   build_ref_314()
	call_c   Dyam_Create_Binary(&ref[314],V(11),V(12))
	move_ret ref[263]
	c_ret

c_code local build_seed_53
	ret_reg &seed[53]
	call_c   build_ref_2()
	call_c   build_ref_265()
	call_c   Dyam_Seed_Start(&ref[2],&ref[265],I(0),fun20,1)
	call_c   build_ref_266()
	call_c   Dyam_Seed_Add_Comp(&ref[266],&ref[265],0)
	call_c   Dyam_Seed_End()
	move_ret seed[53]
	c_ret

;; TERM 266: '*GUARD*'(foreign_load_arg(_N, _O, _L, _P, _F, _H, _E)) :> '$$HOLE$$'
c_code local build_ref_266
	ret_reg &ref[266]
	call_c   build_ref_265()
	call_c   Dyam_Create_Binary(I(9),&ref[265],I(7))
	move_ret ref[266]
	c_ret

;; TERM 265: '*GUARD*'(foreign_load_arg(_N, _O, _L, _P, _F, _H, _E))
c_code local build_ref_265
	ret_reg &ref[265]
	call_c   build_ref_2()
	call_c   build_ref_264()
	call_c   Dyam_Create_Unary(&ref[2],&ref[264])
	move_ret ref[265]
	c_ret

;; TERM 264: foreign_load_arg(_N, _O, _L, _P, _F, _H, _E)
c_code local build_ref_264
	ret_reg &ref[264]
	call_c   build_ref_298()
	call_c   Dyam_Term_Start(&ref[298],7)
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[264]
	c_ret

c_code local build_seed_54
	ret_reg &seed[54]
	call_c   build_ref_2()
	call_c   build_ref_270()
	call_c   Dyam_Seed_Start(&ref[2],&ref[270],I(0),fun20,1)
	call_c   build_ref_271()
	call_c   Dyam_Seed_Add_Comp(&ref[271],&ref[270],0)
	call_c   Dyam_Seed_End()
	move_ret seed[54]
	c_ret

;; TERM 271: '*GUARD*'(foreign_unify_arg(_N, _O, _L, _P, _J)) :> '$$HOLE$$'
c_code local build_ref_271
	ret_reg &ref[271]
	call_c   build_ref_270()
	call_c   Dyam_Create_Binary(I(9),&ref[270],I(7))
	move_ret ref[271]
	c_ret

;; TERM 270: '*GUARD*'(foreign_unify_arg(_N, _O, _L, _P, _J))
c_code local build_ref_270
	ret_reg &ref[270]
	call_c   build_ref_2()
	call_c   build_ref_269()
	call_c   Dyam_Create_Unary(&ref[2],&ref[269])
	move_ret ref[270]
	c_ret

;; TERM 269: foreign_unify_arg(_N, _O, _L, _P, _J)
c_code local build_ref_269
	ret_reg &ref[269]
	call_c   build_ref_283()
	call_c   Dyam_Term_Start(&ref[283],5)
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret ref[269]
	c_ret

;; TERM 272: _D + 1
c_code local build_ref_272
	ret_reg &ref[272]
	call_c   build_ref_47()
	call_c   Dyam_Create_Binary(&ref[47],V(3),N(1))
	move_ret ref[272]
	c_ret

c_code local build_seed_55
	ret_reg &seed[55]
	call_c   build_ref_2()
	call_c   build_ref_274()
	call_c   Dyam_Seed_Start(&ref[2],&ref[274],I(0),fun20,1)
	call_c   build_ref_275()
	call_c   Dyam_Seed_Add_Comp(&ref[275],&ref[274],0)
	call_c   Dyam_Seed_End()
	move_ret seed[55]
	c_ret

;; TERM 275: '*GUARD*'(foreign_parse_template_args(_C, _R, _Q, _G, _I, _K)) :> '$$HOLE$$'
c_code local build_ref_275
	ret_reg &ref[275]
	call_c   build_ref_274()
	call_c   Dyam_Create_Binary(I(9),&ref[274],I(7))
	move_ret ref[275]
	c_ret

;; TERM 274: '*GUARD*'(foreign_parse_template_args(_C, _R, _Q, _G, _I, _K))
c_code local build_ref_274
	ret_reg &ref[274]
	call_c   build_ref_2()
	call_c   build_ref_273()
	call_c   Dyam_Create_Unary(&ref[2],&ref[273])
	move_ret ref[274]
	c_ret

;; TERM 273: foreign_parse_template_args(_C, _R, _Q, _G, _I, _K)
c_code local build_ref_273
	ret_reg &ref[273]
	call_c   build_ref_284()
	call_c   Dyam_Term_Start(&ref[284],6)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_End()
	move_ret ref[273]
	c_ret

long local pool_fun140[4]=[3,build_seed_54,build_ref_272,build_seed_55]

pl_code local fun144
	call_c   Dyam_Remove_Choice()
fun140:
	pl_call  fun21(&seed[54],1)
	call_c   Dyam_Reg_Load(0,V(11))
	call_c   Dyam_Reg_Load(2,V(4))
	move     V(16), R(4)
	move     S(5), R(5)
	pl_call  pred_extend_tupple_3()
	call_c   DYAM_evpred_is(V(17),&ref[272])
	fail_ret
	pl_call  fun21(&seed[55],1)
	pl_jump  fun32()


;; TERM 279: '$reg'(_P)
c_code local build_ref_279
	ret_reg &ref[279]
	call_c   build_ref_346()
	call_c   Dyam_Create_Unary(&ref[346],V(15))
	move_ret ref[279]
	c_ret

long local pool_fun145[4]=[131073,build_ref_279,pool_fun140,pool_fun140]

pl_code local fun145
	call_c   Dyam_Update_Choice(fun144)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(7),&ref[279])
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun140()

;; TERM 278: '$freg'(_P)
c_code local build_ref_278
	ret_reg &ref[278]
	call_c   build_ref_347()
	call_c   Dyam_Create_Unary(&ref[347],V(15))
	move_ret ref[278]
	c_ret

long local pool_fun146[6]=[196610,build_ref_229,build_ref_278,pool_fun145,pool_fun140,pool_fun140]

pl_code local fun146
	call_c   Dyam_Update_Choice(fun145)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(14),&ref[229])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun144)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(7),&ref[278])
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun140()

;; TERM 268: '&reg'(_P)
c_code local build_ref_268
	ret_reg &ref[268]
	call_c   build_ref_348()
	call_c   Dyam_Create_Unary(&ref[348],V(15))
	move_ret ref[268]
	c_ret

;; TERM 348: '&reg'
c_code local build_ref_348
	ret_reg &ref[348]
	call_c   Dyam_Create_Atom("&reg")
	move_ret ref[348]
	c_ret

long local pool_fun141[3]=[65537,build_ref_268,pool_fun140]

long local pool_fun142[3]=[65537,build_ref_277,pool_fun141]

pl_code local fun142
	call_c   Dyam_Remove_Choice()
	call_c   DYAM_sfol_identical(V(14),&ref[277])
	fail_ret
fun141:
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(7),&ref[268])
	fail_ret
	pl_jump  fun140()


long local pool_fun143[4]=[131073,build_ref_276,pool_fun142,pool_fun141]

pl_code local fun143
	call_c   Dyam_Update_Choice(fun142)
	call_c   DYAM_sfol_identical(V(14),&ref[276])
	fail_ret
	pl_jump  fun141()

long local pool_fun148[10]=[262149,build_ref_261,build_ref_263,build_ref_67,build_seed_53,build_ref_267,pool_fun147,pool_fun146,pool_fun143,pool_fun141]

pl_code local fun148
	call_c   Dyam_Pool(pool_fun148)
	call_c   Dyam_Unify_Item(&ref[261])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun147)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[263])
	fail_ret
	call_c   Dyam_Reg_Load(0,V(12))
	move     V(13), R(2)
	move     S(5), R(3)
	move     V(14), R(4)
	move     S(5), R(5)
	pl_call  pred_check_mode_type_3()
	call_c   Dyam_Cut()
	call_c   DYAM_evpred_is(V(15),&ref[67])
	fail_ret
	pl_call  fun21(&seed[53],1)
	call_c   Dyam_Choice(fun146)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Choice(fun143)
	call_c   DYAM_sfol_identical(V(13),&ref[267])
	fail_ret
	pl_jump  fun141()

;; TERM 262: '*GUARD*'(foreign_parse_template_args([_B|_C], _D, _E, (_F :> _G), [_H|_I], (_J :> _K))) :> []
c_code local build_ref_262
	ret_reg &ref[262]
	call_c   build_ref_261()
	call_c   Dyam_Create_Binary(I(9),&ref[261],I(0))
	move_ret ref[262]
	c_ret

;; TERM 46: [_C,_D]
c_code local build_ref_46
	ret_reg &ref[46]
	call_c   Dyam_Create_Tupple(2,3,I(0))
	move_ret ref[46]
	c_ret

;; TERM 36: [int,float,char,bool,string,ptr,term,input,output,term_no_deref]
c_code local build_ref_36
	ret_reg &ref[36]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_277()
	call_c   Dyam_Create_List(&ref[277],I(0))
	move_ret R(0)
	call_c   build_ref_285()
	call_c   Dyam_Create_List(&ref[285],R(0))
	move_ret R(0)
	call_c   build_ref_287()
	call_c   Dyam_Create_List(&ref[287],R(0))
	move_ret R(0)
	call_c   build_ref_276()
	call_c   Dyam_Create_List(&ref[276],R(0))
	move_ret R(0)
	call_c   build_ref_293()
	call_c   Dyam_Create_List(&ref[293],R(0))
	move_ret R(0)
	call_c   build_ref_289()
	call_c   Dyam_Create_List(&ref[289],R(0))
	move_ret R(0)
	call_c   build_ref_57()
	call_c   Dyam_Create_List(&ref[57],R(0))
	move_ret R(0)
	call_c   build_ref_291()
	call_c   Dyam_Create_List(&ref[291],R(0))
	move_ret R(0)
	call_c   build_ref_229()
	call_c   Dyam_Create_List(&ref[229],R(0))
	move_ret R(0)
	call_c   build_ref_296()
	call_c   Dyam_Create_List(&ref[296],R(0))
	move_ret ref[36]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 0: [+,-]
c_code local build_ref_0
	ret_reg &ref[0]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_267()
	call_c   Dyam_Create_List(&ref[267],I(0))
	move_ret R(0)
	call_c   build_ref_47()
	call_c   Dyam_Create_List(&ref[47],R(0))
	move_ret ref[0]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

pl_code local fun17
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Object(R(0))
	call_c   Dyam_Tabule()
	pl_ret

pl_code local fun18
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(1),V(3))
	fail_ret
	call_c   Dyam_Reg_Load(0,V(3))
	pl_call  pred_check_foreign_type_1()
	call_c   Dyam_Unify(V(2),&ref[47])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret


;;------------------ INIT POOL ------------

c_code local build_init_pool
	call_c   build_seed_11()
	call_c   build_seed_10()
	call_c   build_seed_7()
	call_c   build_seed_9()
	call_c   build_seed_0()
	call_c   build_seed_1()
	call_c   build_seed_2()
	call_c   build_seed_3()
	call_c   build_seed_4()
	call_c   build_seed_5()
	call_c   build_seed_6()
	call_c   build_seed_8()
	call_c   build_seed_12()
	call_c   build_seed_13()
	call_c   build_seed_43()
	call_c   build_seed_40()
	call_c   build_seed_30()
	call_c   build_seed_31()
	call_c   build_seed_14()
	call_c   build_seed_15()
	call_c   build_seed_17()
	call_c   build_seed_19()
	call_c   build_seed_33()
	call_c   build_seed_34()
	call_c   build_seed_36()
	call_c   build_seed_37()
	call_c   build_seed_38()
	call_c   build_seed_16()
	call_c   build_seed_35()
	call_c   build_seed_18()
	call_c   build_seed_32()
	call_c   build_seed_24()
	call_c   build_seed_29()
	call_c   build_seed_25()
	call_c   build_seed_23()
	call_c   build_seed_20()
	call_c   build_seed_21()
	call_c   build_seed_26()
	call_c   build_seed_22()
	call_c   build_seed_27()
	call_c   build_seed_28()
	call_c   build_seed_41()
	call_c   build_seed_42()
	call_c   build_seed_39()
	call_c   build_ref_46()
	call_c   build_ref_47()
	call_c   build_ref_36()
	call_c   build_ref_0()
	c_ret

long local ref[349]
long local seed[56]

long local _initialization

c_code global initialization_dyalog_foreign
	call_c   Dyam_Check_And_Update_Initialization(_initialization)
	fail_c_ret
	call_c   initialization_dyalog_reader()
	call_c   initialization_dyalog_oset()
	call_c   initialization_dyalog_stdprolog()
	call_c   initialization_dyalog_format()
	call_c   build_init_pool()
	call_c   build_viewers()
	call_c   load()
	c_ret

