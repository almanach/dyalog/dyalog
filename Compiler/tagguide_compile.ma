;; Compiler: DyALog 1.14.0
;; File "tagguide_compile.pl"


;;------------------ LOADING ------------

c_code local load
	call_c   Dyam_Loading_Set()
	pl_call  fun8(&seed[4],0)
	pl_call  fun8(&seed[11],0)
	pl_call  fun8(&seed[2],0)
	pl_call  fun8(&seed[12],0)
	pl_call  fun8(&seed[5],0)
	pl_call  fun8(&seed[7],0)
	pl_call  fun8(&seed[3],0)
	pl_call  fun8(&seed[10],0)
	pl_call  fun8(&seed[6],0)
	pl_call  fun8(&seed[9],0)
	pl_call  fun8(&seed[13],0)
	pl_call  fun8(&seed[14],0)
	pl_call  fun8(&seed[8],0)
	pl_call  fun8(&seed[0],0)
	pl_call  fun8(&seed[1],0)
	call_c   Dyam_Loading_Reset()
	c_ret

pl_code global pred_reset_numbervars_2
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(2)
	call_c   Dyam_Choice(fun16)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[48])
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load_Ptr(2,V(3))
	fail_ret
	call_c   Dyam_Reg_Load(4,V(2))
	call_c   DyALog_Mutable_Write(R(2),&R(4),0)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(3))
	fail_ret
	call_c   Dyam_Reg_Deallocate(4)
	pl_ret

pl_code global pred_tagguide_strip_3
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(3)
	call_c   Dyam_Choice(fun6)
	call_c   Dyam_Set_Cut()
	pl_call  fun5(&seed[15],1)
	call_c   Dyam_Cut()
	call_c   Dyam_Deallocate()
	pl_ret


;;------------------ VIEWERS ------------

c_code local build_viewers
	call_c   Dyam_Load_Viewer(&ref[13],&ref[14])
	call_c   Dyam_Load_Viewer(&ref[10],&ref[11])
	call_c   Dyam_Load_Viewer(&ref[6],&ref[7])
	call_c   Dyam_Load_Viewer(&ref[3],&ref[1])
	c_ret

;; TERM 1: register_predicate(_A)
c_code local build_ref_1
	ret_reg &ref[1]
	call_c   build_ref_203()
	call_c   Dyam_Create_Unary(&ref[203],V(0))
	move_ret ref[1]
	c_ret

;; TERM 203: register_predicate
c_code local build_ref_203
	ret_reg &ref[203]
	call_c   Dyam_Create_Atom("register_predicate")
	move_ret ref[203]
	c_ret

;; TERM 3: '*RITEM*'(register_predicate(_A), voidret)
c_code local build_ref_3
	ret_reg &ref[3]
	call_c   build_ref_0()
	call_c   build_ref_1()
	call_c   build_ref_2()
	call_c   Dyam_Create_Binary(&ref[0],&ref[1],&ref[2])
	move_ret ref[3]
	c_ret

;; TERM 2: voidret
c_code local build_ref_2
	ret_reg &ref[2]
	call_c   Dyam_Create_Atom("voidret")
	move_ret ref[2]
	c_ret

;; TERM 0: '*RITEM*'
c_code local build_ref_0
	ret_reg &ref[0]
	call_c   Dyam_Create_Atom("*RITEM*")
	move_ret ref[0]
	c_ret

;; TERM 7: tagguide_name(_A, _B, _C)
c_code local build_ref_7
	ret_reg &ref[7]
	call_c   build_ref_204()
	call_c   Dyam_Term_Start(&ref[204],3)
	call_c   Dyam_Term_Arg(V(0))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_End()
	move_ret ref[7]
	c_ret

;; TERM 204: tagguide_name
c_code local build_ref_204
	ret_reg &ref[204]
	call_c   Dyam_Create_Atom("tagguide_name")
	move_ret ref[204]
	c_ret

;; TERM 6: '*RITEM*'('call_tagguide_name/3'(_A, _B), return(_C))
c_code local build_ref_6
	ret_reg &ref[6]
	call_c   build_ref_0()
	call_c   build_ref_4()
	call_c   build_ref_5()
	call_c   Dyam_Create_Binary(&ref[0],&ref[4],&ref[5])
	move_ret ref[6]
	c_ret

;; TERM 5: return(_C)
c_code local build_ref_5
	ret_reg &ref[5]
	call_c   build_ref_205()
	call_c   Dyam_Create_Unary(&ref[205],V(2))
	move_ret ref[5]
	c_ret

;; TERM 205: return
c_code local build_ref_205
	ret_reg &ref[205]
	call_c   Dyam_Create_Atom("return")
	move_ret ref[205]
	c_ret

;; TERM 4: 'call_tagguide_name/3'(_A, _B)
c_code local build_ref_4
	ret_reg &ref[4]
	call_c   build_ref_206()
	call_c   Dyam_Create_Binary(&ref[206],V(0),V(1))
	move_ret ref[4]
	c_ret

;; TERM 206: 'call_tagguide_name/3'
c_code local build_ref_206
	ret_reg &ref[206]
	call_c   Dyam_Create_Atom("call_tagguide_name/3")
	move_ret ref[206]
	c_ret

;; TERM 11: normalize(_A, _B)
c_code local build_ref_11
	ret_reg &ref[11]
	call_c   build_ref_207()
	call_c   Dyam_Create_Binary(&ref[207],V(0),V(1))
	move_ret ref[11]
	c_ret

;; TERM 207: normalize
c_code local build_ref_207
	ret_reg &ref[207]
	call_c   Dyam_Create_Atom("normalize")
	move_ret ref[207]
	c_ret

;; TERM 10: '*RITEM*'('call_normalize/2'(_A), return(_B))
c_code local build_ref_10
	ret_reg &ref[10]
	call_c   build_ref_0()
	call_c   build_ref_8()
	call_c   build_ref_9()
	call_c   Dyam_Create_Binary(&ref[0],&ref[8],&ref[9])
	move_ret ref[10]
	c_ret

;; TERM 9: return(_B)
c_code local build_ref_9
	ret_reg &ref[9]
	call_c   build_ref_205()
	call_c   Dyam_Create_Unary(&ref[205],V(1))
	move_ret ref[9]
	c_ret

;; TERM 8: 'call_normalize/2'(_A)
c_code local build_ref_8
	ret_reg &ref[8]
	call_c   build_ref_208()
	call_c   Dyam_Create_Unary(&ref[208],V(0))
	move_ret ref[8]
	c_ret

;; TERM 208: 'call_normalize/2'
c_code local build_ref_208
	ret_reg &ref[208]
	call_c   Dyam_Create_Atom("call_normalize/2")
	move_ret ref[208]
	c_ret

;; TERM 14: tagguide_pred(_A, _B)
c_code local build_ref_14
	ret_reg &ref[14]
	call_c   build_ref_209()
	call_c   Dyam_Create_Binary(&ref[209],V(0),V(1))
	move_ret ref[14]
	c_ret

;; TERM 209: tagguide_pred
c_code local build_ref_209
	ret_reg &ref[209]
	call_c   Dyam_Create_Atom("tagguide_pred")
	move_ret ref[209]
	c_ret

;; TERM 13: '*RITEM*'('call_tagguide_pred/2'(_A), return(_B))
c_code local build_ref_13
	ret_reg &ref[13]
	call_c   build_ref_0()
	call_c   build_ref_12()
	call_c   build_ref_9()
	call_c   Dyam_Create_Binary(&ref[0],&ref[12],&ref[9])
	move_ret ref[13]
	c_ret

;; TERM 12: 'call_tagguide_pred/2'(_A)
c_code local build_ref_12
	ret_reg &ref[12]
	call_c   build_ref_210()
	call_c   Dyam_Create_Unary(&ref[210],V(0))
	move_ret ref[12]
	c_ret

;; TERM 210: 'call_tagguide_pred/2'
c_code local build_ref_210
	ret_reg &ref[210]
	call_c   Dyam_Create_Atom("call_tagguide_pred/2")
	move_ret ref[210]
	c_ret

;; TERM 48: numbervars(_B, _D)
c_code local build_ref_48
	ret_reg &ref[48]
	call_c   build_ref_211()
	call_c   Dyam_Create_Binary(&ref[211],V(1),V(3))
	move_ret ref[48]
	c_ret

;; TERM 211: numbervars
c_code local build_ref_211
	ret_reg &ref[211]
	call_c   Dyam_Create_Atom("numbervars")
	move_ret ref[211]
	c_ret

c_code local build_seed_1
	ret_reg &seed[1]
	call_c   build_ref_15()
	call_c   build_ref_19()
	call_c   Dyam_Seed_Start(&ref[15],&ref[19],I(0),fun1,1)
	call_c   build_ref_18()
	call_c   Dyam_Seed_Add_Comp(&ref[18],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[1]
	c_ret

;; TERM 18: '*GUARD*'(tagguide_strip_handler([], _B, []))
c_code local build_ref_18
	ret_reg &ref[18]
	call_c   build_ref_16()
	call_c   build_ref_17()
	call_c   Dyam_Create_Unary(&ref[16],&ref[17])
	move_ret ref[18]
	c_ret

;; TERM 17: tagguide_strip_handler([], _B, [])
c_code local build_ref_17
	ret_reg &ref[17]
	call_c   build_ref_212()
	call_c   Dyam_Term_Start(&ref[212],3)
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_End()
	move_ret ref[17]
	c_ret

;; TERM 212: tagguide_strip_handler
c_code local build_ref_212
	ret_reg &ref[212]
	call_c   Dyam_Create_Atom("tagguide_strip_handler")
	move_ret ref[212]
	c_ret

;; TERM 16: '*GUARD*'
c_code local build_ref_16
	ret_reg &ref[16]
	call_c   Dyam_Create_Atom("*GUARD*")
	move_ret ref[16]
	c_ret

pl_code local fun0
	call_c   build_ref_18()
	call_c   Dyam_Unify_Item(&ref[18])
	fail_ret
	pl_ret

pl_code local fun1
	pl_ret

;; TERM 19: '*GUARD*'(tagguide_strip_handler([], _B, [])) :> []
c_code local build_ref_19
	ret_reg &ref[19]
	call_c   build_ref_18()
	call_c   Dyam_Create_Binary(I(9),&ref[18],I(0))
	move_ret ref[19]
	c_ret

;; TERM 15: '*GUARD_CLAUSE*'
c_code local build_ref_15
	ret_reg &ref[15]
	call_c   Dyam_Create_Atom("*GUARD_CLAUSE*")
	move_ret ref[15]
	c_ret

c_code local build_seed_0
	ret_reg &seed[0]
	call_c   build_ref_15()
	call_c   build_ref_22()
	call_c   Dyam_Seed_Start(&ref[15],&ref[22],I(0),fun1,1)
	call_c   build_ref_21()
	call_c   Dyam_Seed_Add_Comp(&ref[21],fun2,0)
	call_c   Dyam_Seed_End()
	move_ret seed[0]
	c_ret

;; TERM 21: '*GUARD*'(tagguide_strip_handler('$pos'(_B), _C, '$pos'(_B)))
c_code local build_ref_21
	ret_reg &ref[21]
	call_c   build_ref_16()
	call_c   build_ref_20()
	call_c   Dyam_Create_Unary(&ref[16],&ref[20])
	move_ret ref[21]
	c_ret

;; TERM 20: tagguide_strip_handler('$pos'(_B), _C, '$pos'(_B))
c_code local build_ref_20
	ret_reg &ref[20]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_213()
	call_c   Dyam_Create_Unary(&ref[213],V(1))
	move_ret R(0)
	call_c   build_ref_212()
	call_c   Dyam_Term_Start(&ref[212],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_End()
	move_ret ref[20]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 213: '$pos'
c_code local build_ref_213
	ret_reg &ref[213]
	call_c   Dyam_Create_Atom("$pos")
	move_ret ref[213]
	c_ret

pl_code local fun2
	call_c   build_ref_21()
	call_c   Dyam_Unify_Item(&ref[21])
	fail_ret
	pl_ret

;; TERM 22: '*GUARD*'(tagguide_strip_handler('$pos'(_B), _C, '$pos'(_B))) :> []
c_code local build_ref_22
	ret_reg &ref[22]
	call_c   build_ref_21()
	call_c   Dyam_Create_Binary(I(9),&ref[21],I(0))
	move_ret ref[22]
	c_ret

c_code local build_seed_8
	ret_reg &seed[8]
	call_c   build_ref_15()
	call_c   build_ref_28()
	call_c   Dyam_Seed_Start(&ref[15],&ref[28],I(0),fun1,1)
	call_c   build_ref_27()
	call_c   Dyam_Seed_Add_Comp(&ref[27],fun4,0)
	call_c   Dyam_Seed_End()
	move_ret seed[8]
	c_ret

;; TERM 27: '*GUARD*'(tagguide_strip_handler('$answers'(_B), _C, '$answers'(_D)))
c_code local build_ref_27
	ret_reg &ref[27]
	call_c   build_ref_16()
	call_c   build_ref_26()
	call_c   Dyam_Create_Unary(&ref[16],&ref[26])
	move_ret ref[27]
	c_ret

;; TERM 26: tagguide_strip_handler('$answers'(_B), _C, '$answers'(_D))
c_code local build_ref_26
	ret_reg &ref[26]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_214()
	call_c   Dyam_Create_Unary(&ref[214],V(1))
	move_ret R(0)
	call_c   Dyam_Create_Unary(&ref[214],V(3))
	move_ret R(1)
	call_c   build_ref_212()
	call_c   Dyam_Term_Start(&ref[212],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_End()
	move_ret ref[26]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 214: '$answers'
c_code local build_ref_214
	ret_reg &ref[214]
	call_c   Dyam_Create_Atom("$answers")
	move_ret ref[214]
	c_ret

pl_code local fun4
	call_c   build_ref_27()
	call_c   Dyam_Unify_Item(&ref[27])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(2))
	call_c   Dyam_Reg_Load(4,V(3))
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  pred_tagguide_strip_3()

;; TERM 28: '*GUARD*'(tagguide_strip_handler('$answers'(_B), _C, '$answers'(_D))) :> []
c_code local build_ref_28
	ret_reg &ref[28]
	call_c   build_ref_27()
	call_c   Dyam_Create_Binary(I(9),&ref[27],I(0))
	move_ret ref[28]
	c_ret

c_code local build_seed_14
	ret_reg &seed[14]
	call_c   build_ref_29()
	call_c   build_ref_32()
	call_c   Dyam_Seed_Start(&ref[29],&ref[32],I(0),fun12,1)
	call_c   build_ref_34()
	call_c   Dyam_Seed_Add_Comp(&ref[34],fun11,0)
	call_c   Dyam_Seed_End()
	move_ret seed[14]
	c_ret

;; TERM 34: '*CITEM*'('call_tagguide_pred/2'(_B), _A)
c_code local build_ref_34
	ret_reg &ref[34]
	call_c   build_ref_33()
	call_c   build_ref_30()
	call_c   Dyam_Create_Binary(&ref[33],&ref[30],V(0))
	move_ret ref[34]
	c_ret

;; TERM 30: 'call_tagguide_pred/2'(_B)
c_code local build_ref_30
	ret_reg &ref[30]
	call_c   build_ref_210()
	call_c   Dyam_Create_Unary(&ref[210],V(1))
	move_ret ref[30]
	c_ret

;; TERM 33: '*CITEM*'
c_code local build_ref_33
	ret_reg &ref[33]
	call_c   Dyam_Create_Atom("*CITEM*")
	move_ret ref[33]
	c_ret

;; TERM 35: strip
c_code local build_ref_35
	ret_reg &ref[35]
	call_c   Dyam_Create_Atom("strip")
	move_ret ref[35]
	c_ret

;; TERM 36: adjkind((_C / 0), left)
c_code local build_ref_36
	ret_reg &ref[36]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_216()
	call_c   Dyam_Create_Binary(&ref[216],V(2),N(0))
	move_ret R(0)
	call_c   build_ref_215()
	call_c   build_ref_177()
	call_c   Dyam_Create_Binary(&ref[215],R(0),&ref[177])
	move_ret ref[36]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 177: left
c_code local build_ref_177
	ret_reg &ref[177]
	call_c   Dyam_Create_Atom("left")
	move_ret ref[177]
	c_ret

;; TERM 215: adjkind
c_code local build_ref_215
	ret_reg &ref[215]
	call_c   Dyam_Create_Atom("adjkind")
	move_ret ref[215]
	c_ret

;; TERM 216: /
c_code local build_ref_216
	ret_reg &ref[216]
	call_c   Dyam_Create_Atom("/")
	move_ret ref[216]
	c_ret

;; TERM 37: adjkind((_C / 0), right)
c_code local build_ref_37
	ret_reg &ref[37]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_216()
	call_c   Dyam_Create_Binary(&ref[216],V(2),N(0))
	move_ret R(0)
	call_c   build_ref_215()
	call_c   build_ref_187()
	call_c   Dyam_Create_Binary(&ref[215],R(0),&ref[187])
	move_ret ref[37]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 187: right
c_code local build_ref_187
	ret_reg &ref[187]
	call_c   Dyam_Create_Atom("right")
	move_ret ref[187]
	c_ret

c_code local build_seed_16
	ret_reg &seed[16]
	call_c   build_ref_0()
	call_c   build_ref_38()
	call_c   Dyam_Seed_Start(&ref[0],&ref[38],&ref[38],fun3,1)
	call_c   build_ref_39()
	call_c   Dyam_Seed_Add_Comp(&ref[39],&ref[38],0)
	call_c   Dyam_Seed_End()
	move_ret seed[16]
	c_ret

pl_code local fun3
	pl_jump  Complete(0,0)

;; TERM 39: '*RITEM*'(_A, return(_C)) :> '$$HOLE$$'
c_code local build_ref_39
	ret_reg &ref[39]
	call_c   build_ref_38()
	call_c   Dyam_Create_Binary(I(9),&ref[38],I(7))
	move_ret ref[39]
	c_ret

;; TERM 38: '*RITEM*'(_A, return(_C))
c_code local build_ref_38
	ret_reg &ref[38]
	call_c   build_ref_0()
	call_c   build_ref_5()
	call_c   Dyam_Create_Binary(&ref[0],V(0),&ref[5])
	move_ret ref[38]
	c_ret

pl_code local fun9
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Object(R(0))
	call_c   Dyam_Tabule()
	pl_ret

pl_code local fun10
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Choice(fun9)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Subsume(R(0))
	fail_ret
	call_c   Dyam_Cut()
	pl_ret

long local pool_fun11[6]=[5,build_ref_34,build_ref_35,build_ref_36,build_ref_37,build_seed_16]

pl_code local fun11
	call_c   Dyam_Pool(pool_fun11)
	call_c   Dyam_Unify_Item(&ref[34])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(1))
	move     &ref[35], R(2)
	move     0, R(3)
	move     V(2), R(4)
	move     S(5), R(5)
	pl_call  pred_deep_module_shift_3()
	move     &ref[36], R(0)
	move     S(5), R(1)
	pl_call  pred_record_without_doublon_1()
	move     &ref[37], R(0)
	move     S(5), R(1)
	pl_call  pred_record_without_doublon_1()
	call_c   Dyam_Deallocate()
	pl_jump  fun10(&seed[16],2)

pl_code local fun12
	pl_jump  Apply(0,0)

;; TERM 32: '*FIRST*'('call_tagguide_pred/2'(_B)) :> []
c_code local build_ref_32
	ret_reg &ref[32]
	call_c   build_ref_31()
	call_c   Dyam_Create_Binary(I(9),&ref[31],I(0))
	move_ret ref[32]
	c_ret

;; TERM 31: '*FIRST*'('call_tagguide_pred/2'(_B))
c_code local build_ref_31
	ret_reg &ref[31]
	call_c   build_ref_29()
	call_c   build_ref_30()
	call_c   Dyam_Create_Unary(&ref[29],&ref[30])
	move_ret ref[31]
	c_ret

;; TERM 29: '*FIRST*'
c_code local build_ref_29
	ret_reg &ref[29]
	call_c   Dyam_Create_Atom("*FIRST*")
	move_ret ref[29]
	c_ret

c_code local build_seed_13
	ret_reg &seed[13]
	call_c   build_ref_29()
	call_c   build_ref_42()
	call_c   Dyam_Seed_Start(&ref[29],&ref[42],I(0),fun12,1)
	call_c   build_ref_43()
	call_c   Dyam_Seed_Add_Comp(&ref[43],fun15,0)
	call_c   Dyam_Seed_End()
	move_ret seed[13]
	c_ret

;; TERM 43: '*CITEM*'('call_tagguide_name/3'(_B, _C), _A)
c_code local build_ref_43
	ret_reg &ref[43]
	call_c   build_ref_33()
	call_c   build_ref_40()
	call_c   Dyam_Create_Binary(&ref[33],&ref[40],V(0))
	move_ret ref[43]
	c_ret

;; TERM 40: 'call_tagguide_name/3'(_B, _C)
c_code local build_ref_40
	ret_reg &ref[40]
	call_c   build_ref_206()
	call_c   Dyam_Create_Binary(&ref[206],V(1),V(2))
	move_ret ref[40]
	c_ret

c_code local build_seed_17
	ret_reg &seed[17]
	call_c   build_ref_0()
	call_c   build_ref_46()
	call_c   Dyam_Seed_Start(&ref[0],&ref[46],&ref[46],fun3,1)
	call_c   build_ref_47()
	call_c   Dyam_Seed_Add_Comp(&ref[47],&ref[46],0)
	call_c   Dyam_Seed_End()
	move_ret seed[17]
	c_ret

;; TERM 47: '*RITEM*'(_A, return(_D)) :> '$$HOLE$$'
c_code local build_ref_47
	ret_reg &ref[47]
	call_c   build_ref_46()
	call_c   Dyam_Create_Binary(I(9),&ref[46],I(7))
	move_ret ref[47]
	c_ret

;; TERM 46: '*RITEM*'(_A, return(_D))
c_code local build_ref_46
	ret_reg &ref[46]
	call_c   build_ref_0()
	call_c   build_ref_45()
	call_c   Dyam_Create_Binary(&ref[0],V(0),&ref[45])
	move_ret ref[46]
	c_ret

;; TERM 45: return(_D)
c_code local build_ref_45
	ret_reg &ref[45]
	call_c   build_ref_205()
	call_c   Dyam_Create_Unary(&ref[205],V(3))
	move_ret ref[45]
	c_ret

long local pool_fun13[3]=[2,build_ref_35,build_seed_17]

pl_code local fun14
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(2))
	move     V(4), R(4)
	move     S(5), R(5)
	pl_call  pred_deep_module_shift_3()
fun13:
	call_c   Dyam_Reg_Load(0,V(4))
	move     &ref[35], R(2)
	move     0, R(3)
	move     V(3), R(4)
	move     S(5), R(5)
	pl_call  pred_deep_module_shift_3()
	call_c   Dyam_Deallocate()
	pl_jump  fun10(&seed[17],2)


;; TERM 44: no
c_code local build_ref_44
	ret_reg &ref[44]
	call_c   Dyam_Create_Atom("no")
	move_ret ref[44]
	c_ret

long local pool_fun15[5]=[131074,build_ref_43,build_ref_44,pool_fun13,pool_fun13]

pl_code local fun15
	call_c   Dyam_Pool(pool_fun15)
	call_c   Dyam_Unify_Item(&ref[43])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun14)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(2),&ref[44])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(4),V(1))
	fail_ret
	pl_jump  fun13()

;; TERM 42: '*FIRST*'('call_tagguide_name/3'(_B, _C)) :> []
c_code local build_ref_42
	ret_reg &ref[42]
	call_c   build_ref_41()
	call_c   Dyam_Create_Binary(I(9),&ref[41],I(0))
	move_ret ref[42]
	c_ret

;; TERM 41: '*FIRST*'('call_tagguide_name/3'(_B, _C))
c_code local build_ref_41
	ret_reg &ref[41]
	call_c   build_ref_29()
	call_c   build_ref_40()
	call_c   Dyam_Create_Unary(&ref[29],&ref[40])
	move_ret ref[41]
	c_ret

c_code local build_seed_9
	ret_reg &seed[9]
	call_c   build_ref_15()
	call_c   build_ref_51()
	call_c   Dyam_Seed_Start(&ref[15],&ref[51],I(0),fun1,1)
	call_c   build_ref_50()
	call_c   Dyam_Seed_Add_Comp(&ref[50],fun20,0)
	call_c   Dyam_Seed_End()
	move_ret seed[9]
	c_ret

;; TERM 50: '*GUARD*'(tagguide_strip_handler((_B ## _C), _D, _E))
c_code local build_ref_50
	ret_reg &ref[50]
	call_c   build_ref_16()
	call_c   build_ref_49()
	call_c   Dyam_Create_Unary(&ref[16],&ref[49])
	move_ret ref[50]
	c_ret

;; TERM 49: tagguide_strip_handler((_B ## _C), _D, _E)
c_code local build_ref_49
	ret_reg &ref[49]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_217()
	call_c   Dyam_Create_Binary(&ref[217],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_212()
	call_c   Dyam_Term_Start(&ref[212],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[49]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 217: ##
c_code local build_ref_217
	ret_reg &ref[217]
	call_c   Dyam_Create_Atom("##")
	move_ret ref[217]
	c_ret

;; TERM 52: [_B,_C]
c_code local build_ref_52
	ret_reg &ref[52]
	call_c   Dyam_Create_Tupple(1,2,I(0))
	move_ret ref[52]
	c_ret

;; TERM 54: [_G,_H]
c_code local build_ref_54
	ret_reg &ref[54]
	call_c   Dyam_Create_Tupple(6,7,I(0))
	move_ret ref[54]
	c_ret

;; TERM 55: _G ## _H
c_code local build_ref_55
	ret_reg &ref[55]
	call_c   build_ref_217()
	call_c   Dyam_Create_Binary(&ref[217],V(6),V(7))
	move_ret ref[55]
	c_ret

long local pool_fun19[3]=[2,build_ref_54,build_ref_55]

pl_code local fun19
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(5),&ref[54])
	fail_ret
	call_c   Dyam_Unify(V(4),&ref[55])
	fail_ret
fun18:
	call_c   Dyam_Deallocate()
	pl_ret


;; TERM 53: [_E]
c_code local build_ref_53
	ret_reg &ref[53]
	call_c   Dyam_Create_List(V(4),I(0))
	move_ret ref[53]
	c_ret

long local pool_fun20[5]=[65539,build_ref_50,build_ref_52,build_ref_53,pool_fun19]

pl_code local fun20
	call_c   Dyam_Pool(pool_fun20)
	call_c   Dyam_Unify_Item(&ref[50])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[52], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(3))
	move     V(5), R(4)
	move     S(5), R(5)
	pl_call  pred_tagguide_strip_3()
	call_c   Dyam_Choice(fun19)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(5),&ref[53])
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun18()

;; TERM 51: '*GUARD*'(tagguide_strip_handler((_B ## _C), _D, _E)) :> []
c_code local build_ref_51
	ret_reg &ref[51]
	call_c   build_ref_50()
	call_c   Dyam_Create_Binary(I(9),&ref[50],I(0))
	move_ret ref[51]
	c_ret

c_code local build_seed_6
	ret_reg &seed[6]
	call_c   build_ref_15()
	call_c   build_ref_69()
	call_c   Dyam_Seed_Start(&ref[15],&ref[69],I(0),fun1,1)
	call_c   build_ref_68()
	call_c   Dyam_Seed_Add_Comp(&ref[68],fun21,0)
	call_c   Dyam_Seed_End()
	move_ret seed[6]
	c_ret

;; TERM 68: '*GUARD*'(tagguide_strip_handler(@*{goal=> _B, vars=> _C, from=> _D, to=> _E, collect_first=> _F, collect_last=> _G, collect_loop=> _H, collect_next=> _I, collect_pred=> _J}, _K, @*{goal=> _L, vars=> [], from=> _D, to=> _E, collect_first=> [], collect_last=> [], collect_loop=> [], collect_next=> [], collect_pred=> _M}))
c_code local build_ref_68
	ret_reg &ref[68]
	call_c   build_ref_16()
	call_c   build_ref_67()
	call_c   Dyam_Create_Unary(&ref[16],&ref[67])
	move_ret ref[68]
	c_ret

;; TERM 67: tagguide_strip_handler(@*{goal=> _B, vars=> _C, from=> _D, to=> _E, collect_first=> _F, collect_last=> _G, collect_loop=> _H, collect_next=> _I, collect_pred=> _J}, _K, @*{goal=> _L, vars=> [], from=> _D, to=> _E, collect_first=> [], collect_last=> [], collect_loop=> [], collect_next=> [], collect_pred=> _M})
c_code local build_ref_67
	ret_reg &ref[67]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_218()
	call_c   Dyam_Term_Start(&ref[218],9)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   Dyam_Term_Start(&ref[218],9)
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_End()
	move_ret R(1)
	call_c   build_ref_212()
	call_c   Dyam_Term_Start(&ref[212],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_End()
	move_ret ref[67]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 218: @*!'$ft'
c_code local build_ref_218
	ret_reg &ref[218]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_228()
	call_c   Dyam_Create_List(&ref[228],I(0))
	move_ret R(0)
	call_c   build_ref_227()
	call_c   Dyam_Create_List(&ref[227],R(0))
	move_ret R(0)
	call_c   build_ref_226()
	call_c   Dyam_Create_List(&ref[226],R(0))
	move_ret R(0)
	call_c   build_ref_225()
	call_c   Dyam_Create_List(&ref[225],R(0))
	move_ret R(0)
	call_c   build_ref_224()
	call_c   Dyam_Create_List(&ref[224],R(0))
	move_ret R(0)
	call_c   build_ref_223()
	call_c   Dyam_Create_List(&ref[223],R(0))
	move_ret R(0)
	call_c   build_ref_222()
	call_c   Dyam_Create_List(&ref[222],R(0))
	move_ret R(0)
	call_c   build_ref_221()
	call_c   Dyam_Create_List(&ref[221],R(0))
	move_ret R(0)
	call_c   build_ref_220()
	call_c   Dyam_Create_List(&ref[220],R(0))
	move_ret R(0)
	call_c   build_ref_219()
	call_c   Dyam_Create_Atom_Module("$ft",&ref[219])
	move_ret ref[218]
	call_c   DYAM_Feature_2(&ref[219],R(0))
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 219: @*
c_code local build_ref_219
	ret_reg &ref[219]
	call_c   Dyam_Create_Atom("@*")
	move_ret ref[219]
	c_ret

;; TERM 220: goal
c_code local build_ref_220
	ret_reg &ref[220]
	call_c   Dyam_Create_Atom("goal")
	move_ret ref[220]
	c_ret

;; TERM 221: vars
c_code local build_ref_221
	ret_reg &ref[221]
	call_c   Dyam_Create_Atom("vars")
	move_ret ref[221]
	c_ret

;; TERM 222: from
c_code local build_ref_222
	ret_reg &ref[222]
	call_c   Dyam_Create_Atom("from")
	move_ret ref[222]
	c_ret

;; TERM 223: to
c_code local build_ref_223
	ret_reg &ref[223]
	call_c   Dyam_Create_Atom("to")
	move_ret ref[223]
	c_ret

;; TERM 224: collect_first
c_code local build_ref_224
	ret_reg &ref[224]
	call_c   Dyam_Create_Atom("collect_first")
	move_ret ref[224]
	c_ret

;; TERM 225: collect_last
c_code local build_ref_225
	ret_reg &ref[225]
	call_c   Dyam_Create_Atom("collect_last")
	move_ret ref[225]
	c_ret

;; TERM 226: collect_loop
c_code local build_ref_226
	ret_reg &ref[226]
	call_c   Dyam_Create_Atom("collect_loop")
	move_ret ref[226]
	c_ret

;; TERM 227: collect_next
c_code local build_ref_227
	ret_reg &ref[227]
	call_c   Dyam_Create_Atom("collect_next")
	move_ret ref[227]
	c_ret

;; TERM 228: collect_pred
c_code local build_ref_228
	ret_reg &ref[228]
	call_c   Dyam_Create_Atom("collect_pred")
	move_ret ref[228]
	c_ret

pl_code local fun21
	call_c   build_ref_68()
	call_c   Dyam_Unify_Item(&ref[68])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(10))
	call_c   Dyam_Reg_Load(4,V(11))
	call_c   Dyam_Reg_Deallocate(3)
	pl_jump  pred_tagguide_strip_3()

;; TERM 69: '*GUARD*'(tagguide_strip_handler(@*{goal=> _B, vars=> _C, from=> _D, to=> _E, collect_first=> _F, collect_last=> _G, collect_loop=> _H, collect_next=> _I, collect_pred=> _J}, _K, @*{goal=> _L, vars=> [], from=> _D, to=> _E, collect_first=> [], collect_last=> [], collect_loop=> [], collect_next=> [], collect_pred=> _M})) :> []
c_code local build_ref_69
	ret_reg &ref[69]
	call_c   build_ref_68()
	call_c   Dyam_Create_Binary(I(9),&ref[68],I(0))
	move_ret ref[69]
	c_ret

c_code local build_seed_10
	ret_reg &seed[10]
	call_c   build_ref_15()
	call_c   build_ref_72()
	call_c   Dyam_Seed_Start(&ref[15],&ref[72],I(0),fun1,1)
	call_c   build_ref_71()
	call_c   Dyam_Seed_Add_Comp(&ref[71],fun23,0)
	call_c   Dyam_Seed_End()
	move_ret seed[10]
	c_ret

;; TERM 71: '*GUARD*'(tagguide_strip_handler((_B ; _C), _D, _E))
c_code local build_ref_71
	ret_reg &ref[71]
	call_c   build_ref_16()
	call_c   build_ref_70()
	call_c   Dyam_Create_Unary(&ref[16],&ref[70])
	move_ret ref[71]
	c_ret

;; TERM 70: tagguide_strip_handler((_B ; _C), _D, _E)
c_code local build_ref_70
	ret_reg &ref[70]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Binary(I(5),V(1),V(2))
	move_ret R(0)
	call_c   build_ref_212()
	call_c   Dyam_Term_Start(&ref[212],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[70]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 75: [_H,_I]
c_code local build_ref_75
	ret_reg &ref[75]
	call_c   Dyam_Create_Tupple(7,8,I(0))
	move_ret ref[75]
	c_ret

;; TERM 76: _H ; _I
c_code local build_ref_76
	ret_reg &ref[76]
	call_c   Dyam_Create_Binary(I(5),V(7),V(8))
	move_ret ref[76]
	c_ret

long local pool_fun22[3]=[2,build_ref_75,build_ref_76]

pl_code local fun22
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(5),&ref[75])
	fail_ret
	call_c   Dyam_Unify(V(4),&ref[76])
	fail_ret
	pl_jump  fun18()

;; TERM 73: [_G]
c_code local build_ref_73
	ret_reg &ref[73]
	call_c   Dyam_Create_List(V(6),I(0))
	move_ret ref[73]
	c_ret

;; TERM 74: guard{goal=> _G, plus=> true, minus=> true}
c_code local build_ref_74
	ret_reg &ref[74]
	call_c   build_ref_229()
	call_c   build_ref_113()
	call_c   Dyam_Term_Start(&ref[229],3)
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(&ref[113])
	call_c   Dyam_Term_Arg(&ref[113])
	call_c   Dyam_Term_End()
	move_ret ref[74]
	c_ret

;; TERM 113: true
c_code local build_ref_113
	ret_reg &ref[113]
	call_c   Dyam_Create_Atom("true")
	move_ret ref[113]
	c_ret

;; TERM 229: guard!'$ft'
c_code local build_ref_229
	ret_reg &ref[229]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_232()
	call_c   Dyam_Create_List(&ref[232],I(0))
	move_ret R(0)
	call_c   build_ref_231()
	call_c   Dyam_Create_List(&ref[231],R(0))
	move_ret R(0)
	call_c   build_ref_220()
	call_c   Dyam_Create_List(&ref[220],R(0))
	move_ret R(0)
	call_c   build_ref_230()
	call_c   Dyam_Create_Atom_Module("$ft",&ref[230])
	move_ret ref[229]
	call_c   DYAM_Feature_2(&ref[230],R(0))
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 230: guard
c_code local build_ref_230
	ret_reg &ref[230]
	call_c   Dyam_Create_Atom("guard")
	move_ret ref[230]
	c_ret

;; TERM 231: plus
c_code local build_ref_231
	ret_reg &ref[231]
	call_c   Dyam_Create_Atom("plus")
	move_ret ref[231]
	c_ret

;; TERM 232: minus
c_code local build_ref_232
	ret_reg &ref[232]
	call_c   Dyam_Create_Atom("minus")
	move_ret ref[232]
	c_ret

long local pool_fun23[6]=[65540,build_ref_71,build_ref_52,build_ref_73,build_ref_74,pool_fun22]

pl_code local fun23
	call_c   Dyam_Pool(pool_fun23)
	call_c   Dyam_Unify_Item(&ref[71])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[52], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(3))
	move     V(5), R(4)
	move     S(5), R(5)
	pl_call  pred_tagguide_strip_3()
	call_c   Dyam_Choice(fun22)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(5),&ref[73])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(4),&ref[74])
	fail_ret
	pl_jump  fun18()

;; TERM 72: '*GUARD*'(tagguide_strip_handler((_B ; _C), _D, _E)) :> []
c_code local build_ref_72
	ret_reg &ref[72]
	call_c   build_ref_71()
	call_c   Dyam_Create_Binary(I(9),&ref[71],I(0))
	move_ret ref[72]
	c_ret

c_code local build_seed_3
	ret_reg &seed[3]
	call_c   build_ref_56()
	call_c   build_ref_60()
	call_c   Dyam_Seed_Start(&ref[56],&ref[60],I(0),fun1,1)
	call_c   build_ref_61()
	call_c   Dyam_Seed_Add_Comp(&ref[61],fun55,0)
	call_c   Dyam_Seed_End()
	move_ret seed[3]
	c_ret

;; TERM 61: '*PROLOG-ITEM*'{top=> unfold('*GUIDE*'(_B, _C, _D), _E, _F, _G, _H), cont=> _A}
c_code local build_ref_61
	ret_reg &ref[61]
	call_c   build_ref_233()
	call_c   build_ref_58()
	call_c   Dyam_Create_Binary(&ref[233],&ref[58],V(0))
	move_ret ref[61]
	c_ret

;; TERM 58: unfold('*GUIDE*'(_B, _C, _D), _E, _F, _G, _H)
c_code local build_ref_58
	ret_reg &ref[58]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_235()
	call_c   Dyam_Term_Start(&ref[235],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_234()
	call_c   Dyam_Term_Start(&ref[234],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[58]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 234: unfold
c_code local build_ref_234
	ret_reg &ref[234]
	call_c   Dyam_Create_Atom("unfold")
	move_ret ref[234]
	c_ret

;; TERM 235: '*GUIDE*'
c_code local build_ref_235
	ret_reg &ref[235]
	call_c   Dyam_Create_Atom("*GUIDE*")
	move_ret ref[235]
	c_ret

;; TERM 233: '*PROLOG-ITEM*'!'$ft'
c_code local build_ref_233
	ret_reg &ref[233]
	call_c   build_ref_62()
	call_c   Dyam_Create_Atom_Module("$ft",&ref[62])
	move_ret ref[233]
	c_ret

;; TERM 62: '*PROLOG-ITEM*'
c_code local build_ref_62
	ret_reg &ref[62]
	call_c   Dyam_Create_Atom("*PROLOG-ITEM*")
	move_ret ref[62]
	c_ret

c_code local build_seed_18
	ret_reg &seed[18]
	call_c   build_ref_62()
	call_c   build_ref_63()
	call_c   Dyam_Seed_Start(&ref[62],&ref[63],I(0),fun3,1)
	call_c   build_ref_66()
	call_c   Dyam_Seed_Add_Comp(&ref[66],&ref[63],0)
	call_c   Dyam_Seed_End()
	move_ret seed[18]
	c_ret

;; TERM 66: '*PROLOG-FIRST*'(seed_install(seed{model=> ('*RITEM*'(_B, _C) :> _D(_E)(_F)), id=> _I, anchor=> _J, subs_comp=> _K, code=> _L, go=> _M, tabule=> light, schedule=> prolog, subsume=> _N, model_ref=> _O, subs_comp_ref=> _P, c_type=> _Q, c_type_ref=> _R, out_env=> [_H], appinfo=> _S, tail_flag=> _T}, 2, _G)) :> '$$HOLE$$'
c_code local build_ref_66
	ret_reg &ref[66]
	call_c   build_ref_65()
	call_c   Dyam_Create_Binary(I(9),&ref[65],I(7))
	move_ret ref[66]
	c_ret

;; TERM 65: '*PROLOG-FIRST*'(seed_install(seed{model=> ('*RITEM*'(_B, _C) :> _D(_E)(_F)), id=> _I, anchor=> _J, subs_comp=> _K, code=> _L, go=> _M, tabule=> light, schedule=> prolog, subsume=> _N, model_ref=> _O, subs_comp_ref=> _P, c_type=> _Q, c_type_ref=> _R, out_env=> [_H], appinfo=> _S, tail_flag=> _T}, 2, _G))
c_code local build_ref_65
	ret_reg &ref[65]
	call_c   build_ref_57()
	call_c   build_ref_64()
	call_c   Dyam_Create_Unary(&ref[57],&ref[64])
	move_ret ref[65]
	c_ret

;; TERM 64: seed_install(seed{model=> ('*RITEM*'(_B, _C) :> _D(_E)(_F)), id=> _I, anchor=> _J, subs_comp=> _K, code=> _L, go=> _M, tabule=> light, schedule=> prolog, subsume=> _N, model_ref=> _O, subs_comp_ref=> _P, c_type=> _Q, c_type_ref=> _R, out_env=> [_H], appinfo=> _S, tail_flag=> _T}, 2, _G)
c_code local build_ref_64
	ret_reg &ref[64]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_0()
	call_c   Dyam_Create_Binary(&ref[0],V(1),V(2))
	move_ret R(0)
	call_c   Dyam_Create_Binary(I(3),V(3),V(4))
	move_ret R(1)
	call_c   Dyam_Create_Binary(I(3),R(1),V(5))
	move_ret R(1)
	call_c   Dyam_Create_Binary(I(9),R(0),R(1))
	move_ret R(1)
	call_c   Dyam_Create_List(V(7),I(0))
	move_ret R(0)
	call_c   build_ref_237()
	call_c   build_ref_238()
	call_c   build_ref_239()
	call_c   Dyam_Term_Start(&ref[237],16)
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(&ref[238])
	call_c   Dyam_Term_Arg(&ref[239])
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_236()
	call_c   Dyam_Term_Start(&ref[236],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(N(2))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[64]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 236: seed_install
c_code local build_ref_236
	ret_reg &ref[236]
	call_c   Dyam_Create_Atom("seed_install")
	move_ret ref[236]
	c_ret

;; TERM 239: prolog
c_code local build_ref_239
	ret_reg &ref[239]
	call_c   Dyam_Create_Atom("prolog")
	move_ret ref[239]
	c_ret

;; TERM 238: light
c_code local build_ref_238
	ret_reg &ref[238]
	call_c   Dyam_Create_Atom("light")
	move_ret ref[238]
	c_ret

;; TERM 237: seed!'$ft'
c_code local build_ref_237
	ret_reg &ref[237]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_256()
	call_c   Dyam_Create_List(&ref[256],I(0))
	move_ret R(0)
	call_c   build_ref_255()
	call_c   Dyam_Create_List(&ref[255],R(0))
	move_ret R(0)
	call_c   build_ref_254()
	call_c   Dyam_Create_List(&ref[254],R(0))
	move_ret R(0)
	call_c   build_ref_253()
	call_c   Dyam_Create_List(&ref[253],R(0))
	move_ret R(0)
	call_c   build_ref_252()
	call_c   Dyam_Create_List(&ref[252],R(0))
	move_ret R(0)
	call_c   build_ref_251()
	call_c   Dyam_Create_List(&ref[251],R(0))
	move_ret R(0)
	call_c   build_ref_250()
	call_c   Dyam_Create_List(&ref[250],R(0))
	move_ret R(0)
	call_c   build_ref_249()
	call_c   Dyam_Create_List(&ref[249],R(0))
	move_ret R(0)
	call_c   build_ref_248()
	call_c   Dyam_Create_List(&ref[248],R(0))
	move_ret R(0)
	call_c   build_ref_247()
	call_c   Dyam_Create_List(&ref[247],R(0))
	move_ret R(0)
	call_c   build_ref_246()
	call_c   Dyam_Create_List(&ref[246],R(0))
	move_ret R(0)
	call_c   build_ref_245()
	call_c   Dyam_Create_List(&ref[245],R(0))
	move_ret R(0)
	call_c   build_ref_244()
	call_c   Dyam_Create_List(&ref[244],R(0))
	move_ret R(0)
	call_c   build_ref_243()
	call_c   Dyam_Create_List(&ref[243],R(0))
	move_ret R(0)
	call_c   build_ref_242()
	call_c   Dyam_Create_List(&ref[242],R(0))
	move_ret R(0)
	call_c   build_ref_241()
	call_c   Dyam_Create_List(&ref[241],R(0))
	move_ret R(0)
	call_c   build_ref_240()
	call_c   Dyam_Create_Atom_Module("$ft",&ref[240])
	move_ret ref[237]
	call_c   DYAM_Feature_2(&ref[240],R(0))
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 240: seed
c_code local build_ref_240
	ret_reg &ref[240]
	call_c   Dyam_Create_Atom("seed")
	move_ret ref[240]
	c_ret

;; TERM 241: model
c_code local build_ref_241
	ret_reg &ref[241]
	call_c   Dyam_Create_Atom("model")
	move_ret ref[241]
	c_ret

;; TERM 242: id
c_code local build_ref_242
	ret_reg &ref[242]
	call_c   Dyam_Create_Atom("id")
	move_ret ref[242]
	c_ret

;; TERM 243: anchor
c_code local build_ref_243
	ret_reg &ref[243]
	call_c   Dyam_Create_Atom("anchor")
	move_ret ref[243]
	c_ret

;; TERM 244: subs_comp
c_code local build_ref_244
	ret_reg &ref[244]
	call_c   Dyam_Create_Atom("subs_comp")
	move_ret ref[244]
	c_ret

;; TERM 245: code
c_code local build_ref_245
	ret_reg &ref[245]
	call_c   Dyam_Create_Atom("code")
	move_ret ref[245]
	c_ret

;; TERM 246: go
c_code local build_ref_246
	ret_reg &ref[246]
	call_c   Dyam_Create_Atom("go")
	move_ret ref[246]
	c_ret

;; TERM 247: tabule
c_code local build_ref_247
	ret_reg &ref[247]
	call_c   Dyam_Create_Atom("tabule")
	move_ret ref[247]
	c_ret

;; TERM 248: schedule
c_code local build_ref_248
	ret_reg &ref[248]
	call_c   Dyam_Create_Atom("schedule")
	move_ret ref[248]
	c_ret

;; TERM 249: subsume
c_code local build_ref_249
	ret_reg &ref[249]
	call_c   Dyam_Create_Atom("subsume")
	move_ret ref[249]
	c_ret

;; TERM 250: model_ref
c_code local build_ref_250
	ret_reg &ref[250]
	call_c   Dyam_Create_Atom("model_ref")
	move_ret ref[250]
	c_ret

;; TERM 251: subs_comp_ref
c_code local build_ref_251
	ret_reg &ref[251]
	call_c   Dyam_Create_Atom("subs_comp_ref")
	move_ret ref[251]
	c_ret

;; TERM 252: c_type
c_code local build_ref_252
	ret_reg &ref[252]
	call_c   Dyam_Create_Atom("c_type")
	move_ret ref[252]
	c_ret

;; TERM 253: c_type_ref
c_code local build_ref_253
	ret_reg &ref[253]
	call_c   Dyam_Create_Atom("c_type_ref")
	move_ret ref[253]
	c_ret

;; TERM 254: out_env
c_code local build_ref_254
	ret_reg &ref[254]
	call_c   Dyam_Create_Atom("out_env")
	move_ret ref[254]
	c_ret

;; TERM 255: appinfo
c_code local build_ref_255
	ret_reg &ref[255]
	call_c   Dyam_Create_Atom("appinfo")
	move_ret ref[255]
	c_ret

;; TERM 256: tail_flag
c_code local build_ref_256
	ret_reg &ref[256]
	call_c   Dyam_Create_Atom("tail_flag")
	move_ret ref[256]
	c_ret

;; TERM 57: '*PROLOG-FIRST*'
c_code local build_ref_57
	ret_reg &ref[57]
	call_c   Dyam_Create_Atom("*PROLOG-FIRST*")
	move_ret ref[57]
	c_ret

;; TERM 63: '*PROLOG-ITEM*'{top=> seed_install(seed{model=> ('*RITEM*'(_B, _C) :> _D(_E)(_F)), id=> _I, anchor=> _J, subs_comp=> _K, code=> _L, go=> _M, tabule=> light, schedule=> prolog, subsume=> _N, model_ref=> _O, subs_comp_ref=> _P, c_type=> _Q, c_type_ref=> _R, out_env=> [_H], appinfo=> _S, tail_flag=> _T}, 2, _G), cont=> _A}
c_code local build_ref_63
	ret_reg &ref[63]
	call_c   build_ref_233()
	call_c   build_ref_64()
	call_c   Dyam_Create_Binary(&ref[233],&ref[64],V(0))
	move_ret ref[63]
	c_ret

pl_code local fun5
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Light_Object(R(0))
	pl_jump  Schedule_Prolog()

long local pool_fun55[3]=[2,build_ref_61,build_seed_18]

pl_code local fun55
	call_c   Dyam_Pool(pool_fun55)
	call_c   Dyam_Unify_Item(&ref[61])
	fail_ret
	pl_jump  fun5(&seed[18],12)

;; TERM 60: '*PROLOG-FIRST*'(unfold('*GUIDE*'(_B, _C, _D), _E, _F, _G, _H)) :> []
c_code local build_ref_60
	ret_reg &ref[60]
	call_c   build_ref_59()
	call_c   Dyam_Create_Binary(I(9),&ref[59],I(0))
	move_ret ref[60]
	c_ret

;; TERM 59: '*PROLOG-FIRST*'(unfold('*GUIDE*'(_B, _C, _D), _E, _F, _G, _H))
c_code local build_ref_59
	ret_reg &ref[59]
	call_c   build_ref_57()
	call_c   build_ref_58()
	call_c   Dyam_Create_Unary(&ref[57],&ref[58])
	move_ret ref[59]
	c_ret

;; TERM 56: '*PROLOG_FIRST*'
c_code local build_ref_56
	ret_reg &ref[56]
	call_c   Dyam_Create_Atom("*PROLOG_FIRST*")
	move_ret ref[56]
	c_ret

c_code local build_seed_7
	ret_reg &seed[7]
	call_c   build_ref_15()
	call_c   build_ref_79()
	call_c   Dyam_Seed_Start(&ref[15],&ref[79],I(0),fun1,1)
	call_c   build_ref_78()
	call_c   Dyam_Seed_Add_Comp(&ref[78],fun26,0)
	call_c   Dyam_Seed_End()
	move_ret seed[7]
	c_ret

;; TERM 78: '*GUARD*'(tagguide_strip_handler(guard{goal=> _B, plus=> _C, minus=> _D}, _E, _F))
c_code local build_ref_78
	ret_reg &ref[78]
	call_c   build_ref_16()
	call_c   build_ref_77()
	call_c   Dyam_Create_Unary(&ref[16],&ref[77])
	move_ret ref[78]
	c_ret

;; TERM 77: tagguide_strip_handler(guard{goal=> _B, plus=> _C, minus=> _D}, _E, _F)
c_code local build_ref_77
	ret_reg &ref[77]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_229()
	call_c   Dyam_Term_Start(&ref[229],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_212()
	call_c   Dyam_Term_Start(&ref[212],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[77]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun24[2]=[1,build_ref_74]

pl_code local fun24
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(5),&ref[74])
	fail_ret
	pl_jump  fun18()

;; TERM 80: fail
c_code local build_ref_80
	ret_reg &ref[80]
	call_c   Dyam_Create_Atom("fail")
	move_ret ref[80]
	c_ret

;; TERM 81: guard{goal=> _G, plus=> fail, minus=> true}
c_code local build_ref_81
	ret_reg &ref[81]
	call_c   build_ref_229()
	call_c   build_ref_80()
	call_c   build_ref_113()
	call_c   Dyam_Term_Start(&ref[229],3)
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(&ref[80])
	call_c   Dyam_Term_Arg(&ref[113])
	call_c   Dyam_Term_End()
	move_ret ref[81]
	c_ret

long local pool_fun25[4]=[65538,build_ref_80,build_ref_81,pool_fun24]

pl_code local fun25
	call_c   Dyam_Update_Choice(fun24)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(2),&ref[80])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(5),&ref[81])
	fail_ret
	pl_jump  fun18()

long local pool_fun26[4]=[65538,build_ref_78,build_ref_80,pool_fun25]

pl_code local fun26
	call_c   Dyam_Pool(pool_fun26)
	call_c   Dyam_Unify_Item(&ref[78])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(4))
	move     V(6), R(4)
	move     S(5), R(5)
	pl_call  pred_tagguide_strip_3()
	call_c   Dyam_Choice(fun25)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(3),&ref[80])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(5),V(6))
	fail_ret
	pl_jump  fun18()

;; TERM 79: '*GUARD*'(tagguide_strip_handler(guard{goal=> _B, plus=> _C, minus=> _D}, _E, _F)) :> []
c_code local build_ref_79
	ret_reg &ref[79]
	call_c   build_ref_78()
	call_c   Dyam_Create_Binary(I(9),&ref[78],I(0))
	move_ret ref[79]
	c_ret

c_code local build_seed_5
	ret_reg &seed[5]
	call_c   build_ref_15()
	call_c   build_ref_84()
	call_c   Dyam_Seed_Start(&ref[15],&ref[84],I(0),fun1,1)
	call_c   build_ref_83()
	call_c   Dyam_Seed_Add_Comp(&ref[83],fun49,0)
	call_c   Dyam_Seed_End()
	move_ret seed[5]
	c_ret

;; TERM 83: '*GUARD*'(pgm_to_lpda(tag_tree{family=> _B, name=> _C, tree=> _D}, _E))
c_code local build_ref_83
	ret_reg &ref[83]
	call_c   build_ref_16()
	call_c   build_ref_82()
	call_c   Dyam_Create_Unary(&ref[16],&ref[82])
	move_ret ref[83]
	c_ret

;; TERM 82: pgm_to_lpda(tag_tree{family=> _B, name=> _C, tree=> _D}, _E)
c_code local build_ref_82
	ret_reg &ref[82]
	call_c   build_ref_257()
	call_c   build_ref_125()
	call_c   Dyam_Create_Binary(&ref[257],&ref[125],V(4))
	move_ret ref[82]
	c_ret

;; TERM 125: tag_tree{family=> _B, name=> _C, tree=> _D}
c_code local build_ref_125
	ret_reg &ref[125]
	call_c   build_ref_258()
	call_c   Dyam_Term_Start(&ref[258],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[125]
	c_ret

;; TERM 258: tag_tree!'$ft'
c_code local build_ref_258
	ret_reg &ref[258]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_262()
	call_c   Dyam_Create_List(&ref[262],I(0))
	move_ret R(0)
	call_c   build_ref_261()
	call_c   Dyam_Create_List(&ref[261],R(0))
	move_ret R(0)
	call_c   build_ref_260()
	call_c   Dyam_Create_List(&ref[260],R(0))
	move_ret R(0)
	call_c   build_ref_259()
	call_c   Dyam_Create_Atom_Module("$ft",&ref[259])
	move_ret ref[258]
	call_c   DYAM_Feature_2(&ref[259],R(0))
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 259: tag_tree
c_code local build_ref_259
	ret_reg &ref[259]
	call_c   Dyam_Create_Atom("tag_tree")
	move_ret ref[259]
	c_ret

;; TERM 260: family
c_code local build_ref_260
	ret_reg &ref[260]
	call_c   Dyam_Create_Atom("family")
	move_ret ref[260]
	c_ret

;; TERM 261: name
c_code local build_ref_261
	ret_reg &ref[261]
	call_c   Dyam_Create_Atom("name")
	move_ret ref[261]
	c_ret

;; TERM 262: tree
c_code local build_ref_262
	ret_reg &ref[262]
	call_c   Dyam_Create_Atom("tree")
	move_ret ref[262]
	c_ret

;; TERM 257: pgm_to_lpda
c_code local build_ref_257
	ret_reg &ref[257]
	call_c   Dyam_Create_Atom("pgm_to_lpda")
	move_ret ref[257]
	c_ret

;; TERM 85: guide(strip)
c_code local build_ref_85
	ret_reg &ref[85]
	call_c   build_ref_263()
	call_c   build_ref_35()
	call_c   Dyam_Create_Unary(&ref[263],&ref[35])
	move_ret ref[85]
	c_ret

;; TERM 263: guide
c_code local build_ref_263
	ret_reg &ref[263]
	call_c   Dyam_Create_Atom("guide")
	move_ret ref[263]
	c_ret

;; TERM 124: '$CLOSURE'('$fun'(48, 0, 1119312640), '$TUPPLE'(35047639355136))
c_code local build_ref_124
	ret_reg &ref[124]
	call_c   build_ref_123()
	call_c   Dyam_Closure_Aux(fun48,&ref[123])
	move_ret ref[124]
	c_ret

;; TERM 95: strip(tag_anchor{family=> _B, coanchors=> _R, equations=> _S})
c_code local build_ref_95
	ret_reg &ref[95]
	call_c   build_ref_35()
	call_c   build_ref_94()
	call_c   Dyam_Create_Unary(&ref[35],&ref[94])
	move_ret ref[95]
	c_ret

;; TERM 94: tag_anchor{family=> _B, coanchors=> _R, equations=> _S}
c_code local build_ref_94
	ret_reg &ref[94]
	call_c   build_ref_264()
	call_c   Dyam_Term_Start(&ref[264],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_End()
	move_ret ref[94]
	c_ret

;; TERM 264: tag_anchor!'$ft'
c_code local build_ref_264
	ret_reg &ref[264]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_267()
	call_c   Dyam_Create_List(&ref[267],I(0))
	move_ret R(0)
	call_c   build_ref_266()
	call_c   Dyam_Create_List(&ref[266],R(0))
	move_ret R(0)
	call_c   build_ref_260()
	call_c   Dyam_Create_List(&ref[260],R(0))
	move_ret R(0)
	call_c   build_ref_265()
	call_c   Dyam_Create_Atom_Module("$ft",&ref[265])
	move_ret ref[264]
	call_c   DYAM_Feature_2(&ref[265],R(0))
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 265: tag_anchor
c_code local build_ref_265
	ret_reg &ref[265]
	call_c   Dyam_Create_Atom("tag_anchor")
	move_ret ref[265]
	c_ret

;; TERM 266: coanchors
c_code local build_ref_266
	ret_reg &ref[266]
	call_c   Dyam_Create_Atom("coanchors")
	move_ret ref[266]
	c_ret

;; TERM 267: equations
c_code local build_ref_267
	ret_reg &ref[267]
	call_c   Dyam_Create_Atom("equations")
	move_ret ref[267]
	c_ret

;; TERM 105: [left,right]
c_code local build_ref_105
	ret_reg &ref[105]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_187()
	call_c   Dyam_Create_List(&ref[187],I(0))
	move_ret R(0)
	call_c   build_ref_177()
	call_c   Dyam_Create_List(&ref[177],R(0))
	move_ret ref[105]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 120: '$CLOSURE'('$fun'(42, 0, 1119281004), '$TUPPLE'(35047639354708))
c_code local build_ref_120
	ret_reg &ref[120]
	call_c   build_ref_119()
	call_c   Dyam_Closure_Aux(fun42,&ref[119])
	move_ret ref[120]
	c_ret

;; TERM 104: tag_node{id=> _F, label=> _G, children=> _H, kind=> _I, adj=> _J, spine=> _K, top=> _L, bot=> _M, token=> _N, adjleft=> _O, adjright=> _P, adjwrap=> _Q}
c_code local build_ref_104
	ret_reg &ref[104]
	call_c   build_ref_268()
	call_c   Dyam_Term_Start(&ref[268],12)
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_End()
	move_ret ref[104]
	c_ret

;; TERM 268: tag_node!'$ft'
c_code local build_ref_268
	ret_reg &ref[268]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_280()
	call_c   Dyam_Create_List(&ref[280],I(0))
	move_ret R(0)
	call_c   build_ref_279()
	call_c   Dyam_Create_List(&ref[279],R(0))
	move_ret R(0)
	call_c   build_ref_278()
	call_c   Dyam_Create_List(&ref[278],R(0))
	move_ret R(0)
	call_c   build_ref_277()
	call_c   Dyam_Create_List(&ref[277],R(0))
	move_ret R(0)
	call_c   build_ref_276()
	call_c   Dyam_Create_List(&ref[276],R(0))
	move_ret R(0)
	call_c   build_ref_275()
	call_c   Dyam_Create_List(&ref[275],R(0))
	move_ret R(0)
	call_c   build_ref_274()
	call_c   Dyam_Create_List(&ref[274],R(0))
	move_ret R(0)
	call_c   build_ref_273()
	call_c   Dyam_Create_List(&ref[273],R(0))
	move_ret R(0)
	call_c   build_ref_272()
	call_c   Dyam_Create_List(&ref[272],R(0))
	move_ret R(0)
	call_c   build_ref_271()
	call_c   Dyam_Create_List(&ref[271],R(0))
	move_ret R(0)
	call_c   build_ref_270()
	call_c   Dyam_Create_List(&ref[270],R(0))
	move_ret R(0)
	call_c   build_ref_242()
	call_c   Dyam_Create_List(&ref[242],R(0))
	move_ret R(0)
	call_c   build_ref_269()
	call_c   Dyam_Create_Atom_Module("$ft",&ref[269])
	move_ret ref[268]
	call_c   DYAM_Feature_2(&ref[269],R(0))
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 269: tag_node
c_code local build_ref_269
	ret_reg &ref[269]
	call_c   Dyam_Create_Atom("tag_node")
	move_ret ref[269]
	c_ret

;; TERM 270: label
c_code local build_ref_270
	ret_reg &ref[270]
	call_c   Dyam_Create_Atom("label")
	move_ret ref[270]
	c_ret

;; TERM 271: children
c_code local build_ref_271
	ret_reg &ref[271]
	call_c   Dyam_Create_Atom("children")
	move_ret ref[271]
	c_ret

;; TERM 272: kind
c_code local build_ref_272
	ret_reg &ref[272]
	call_c   Dyam_Create_Atom("kind")
	move_ret ref[272]
	c_ret

;; TERM 273: adj
c_code local build_ref_273
	ret_reg &ref[273]
	call_c   Dyam_Create_Atom("adj")
	move_ret ref[273]
	c_ret

;; TERM 274: spine
c_code local build_ref_274
	ret_reg &ref[274]
	call_c   Dyam_Create_Atom("spine")
	move_ret ref[274]
	c_ret

;; TERM 275: top
c_code local build_ref_275
	ret_reg &ref[275]
	call_c   Dyam_Create_Atom("top")
	move_ret ref[275]
	c_ret

;; TERM 276: bot
c_code local build_ref_276
	ret_reg &ref[276]
	call_c   Dyam_Create_Atom("bot")
	move_ret ref[276]
	c_ret

;; TERM 277: token
c_code local build_ref_277
	ret_reg &ref[277]
	call_c   Dyam_Create_Atom("token")
	move_ret ref[277]
	c_ret

;; TERM 278: adjleft
c_code local build_ref_278
	ret_reg &ref[278]
	call_c   Dyam_Create_Atom("adjleft")
	move_ret ref[278]
	c_ret

;; TERM 279: adjright
c_code local build_ref_279
	ret_reg &ref[279]
	call_c   Dyam_Create_Atom("adjright")
	move_ret ref[279]
	c_ret

;; TERM 280: adjwrap
c_code local build_ref_280
	ret_reg &ref[280]
	call_c   Dyam_Create_Atom("adjwrap")
	move_ret ref[280]
	c_ret

;; TERM 107: guide(_X, _W, _C)
c_code local build_ref_107
	ret_reg &ref[107]
	call_c   build_ref_263()
	call_c   Dyam_Term_Start(&ref[263],3)
	call_c   Dyam_Term_Arg(V(23))
	call_c   Dyam_Term_Arg(V(22))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_End()
	move_ret ref[107]
	c_ret

c_code local build_seed_23
	ret_reg &seed[23]
	call_c   build_ref_16()
	call_c   build_ref_109()
	call_c   Dyam_Seed_Start(&ref[16],&ref[109],I(0),fun3,1)
	call_c   build_ref_110()
	call_c   Dyam_Seed_Add_Comp(&ref[110],&ref[109],0)
	call_c   Dyam_Seed_End()
	move_ret seed[23]
	c_ret

;; TERM 110: '*GUARD*'(tagguide_compile_tree(_X, _Y, _A1, dyalog, _V, _W)) :> '$$HOLE$$'
c_code local build_ref_110
	ret_reg &ref[110]
	call_c   build_ref_109()
	call_c   Dyam_Create_Binary(I(9),&ref[109],I(7))
	move_ret ref[110]
	c_ret

;; TERM 109: '*GUARD*'(tagguide_compile_tree(_X, _Y, _A1, dyalog, _V, _W))
c_code local build_ref_109
	ret_reg &ref[109]
	call_c   build_ref_16()
	call_c   build_ref_108()
	call_c   Dyam_Create_Unary(&ref[16],&ref[108])
	move_ret ref[109]
	c_ret

;; TERM 108: tagguide_compile_tree(_X, _Y, _A1, dyalog, _V, _W)
c_code local build_ref_108
	ret_reg &ref[108]
	call_c   build_ref_281()
	call_c   build_ref_282()
	call_c   Dyam_Term_Start(&ref[281],6)
	call_c   Dyam_Term_Arg(V(23))
	call_c   Dyam_Term_Arg(V(24))
	call_c   Dyam_Term_Arg(V(26))
	call_c   Dyam_Term_Arg(&ref[282])
	call_c   Dyam_Term_Arg(V(21))
	call_c   Dyam_Term_Arg(V(22))
	call_c   Dyam_Term_End()
	move_ret ref[108]
	c_ret

;; TERM 282: dyalog
c_code local build_ref_282
	ret_reg &ref[282]
	call_c   Dyam_Create_Atom("dyalog")
	move_ret ref[282]
	c_ret

;; TERM 281: tagguide_compile_tree
c_code local build_ref_281
	ret_reg &ref[281]
	call_c   Dyam_Create_Atom("tagguide_compile_tree")
	move_ret ref[281]
	c_ret

pl_code local fun39
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(4),V(26))
	fail_ret
fun36:
	call_c   Dyam_Reg_Load(0,V(23))
	call_c   Dyam_Reg_Deallocate(1)
	pl_jump  pred_numbervars_delete_1()


;; TERM 111: autoload
c_code local build_ref_111
	ret_reg &ref[111]
	call_c   Dyam_Create_Atom("autoload")
	move_ret ref[111]
	c_ret

;; TERM 112: parse_mode(token)
c_code local build_ref_112
	ret_reg &ref[112]
	call_c   build_ref_283()
	call_c   build_ref_277()
	call_c   Dyam_Create_Unary(&ref[283],&ref[277])
	move_ret ref[112]
	c_ret

;; TERM 283: parse_mode
c_code local build_ref_283
	ret_reg &ref[283]
	call_c   Dyam_Create_Atom("parse_mode")
	move_ret ref[283]
	c_ret

c_code local build_seed_24
	ret_reg &seed[24]
	call_c   build_ref_16()
	call_c   build_ref_115()
	call_c   Dyam_Seed_Start(&ref[16],&ref[115],I(0),fun3,1)
	call_c   build_ref_116()
	call_c   Dyam_Seed_Add_Comp(&ref[116],&ref[115],0)
	call_c   Dyam_Seed_End()
	move_ret seed[24]
	c_ret

;; TERM 116: '*GUARD*'(build_cond_loader(_X, (_B1 -> true ; fail), _A1, _E)) :> '$$HOLE$$'
c_code local build_ref_116
	ret_reg &ref[116]
	call_c   build_ref_115()
	call_c   Dyam_Create_Binary(I(9),&ref[115],I(7))
	move_ret ref[116]
	c_ret

;; TERM 115: '*GUARD*'(build_cond_loader(_X, (_B1 -> true ; fail), _A1, _E))
c_code local build_ref_115
	ret_reg &ref[115]
	call_c   build_ref_16()
	call_c   build_ref_114()
	call_c   Dyam_Create_Unary(&ref[16],&ref[114])
	move_ret ref[115]
	c_ret

;; TERM 114: build_cond_loader(_X, (_B1 -> true ; fail), _A1, _E)
c_code local build_ref_114
	ret_reg &ref[114]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_285()
	call_c   build_ref_113()
	call_c   Dyam_Create_Binary(&ref[285],V(27),&ref[113])
	move_ret R(0)
	call_c   build_ref_80()
	call_c   Dyam_Create_Binary(I(5),R(0),&ref[80])
	move_ret R(0)
	call_c   build_ref_284()
	call_c   Dyam_Term_Start(&ref[284],4)
	call_c   Dyam_Term_Arg(V(23))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(26))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[114]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 284: build_cond_loader
c_code local build_ref_284
	ret_reg &ref[284]
	call_c   Dyam_Create_Atom("build_cond_loader")
	move_ret ref[284]
	c_ret

;; TERM 285: ->
c_code local build_ref_285
	ret_reg &ref[285]
	call_c   Dyam_Create_Atom("->")
	move_ret ref[285]
	c_ret

long local pool_fun37[2]=[1,build_seed_24]

pl_code local fun38
	call_c   Dyam_Remove_Choice()
fun37:
	call_c   Dyam_Cut()
	pl_call  fun5(&seed[24],1)
	pl_jump  fun36()


long local pool_fun40[7]=[65541,build_ref_107,build_seed_23,build_ref_111,build_ref_112,build_ref_113,pool_fun37]

pl_code local fun41
	call_c   Dyam_Remove_Choice()
fun40:
	move     &ref[107], R(0)
	move     S(5), R(1)
	pl_call  pred_record_without_doublon_1()
	call_c   Dyam_Reg_Load(0,V(21))
	call_c   Dyam_Reg_Load(2,V(23))
	move     V(25), R(4)
	move     S(5), R(5)
	pl_call  pred_tag_numbervars_3()
	pl_call  fun5(&seed[23],1)
	call_c   Dyam_Choice(fun39)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[111])
	pl_call  Object_1(&ref[112])
	call_c   Dyam_Reg_Load(0,V(23))
	call_c   Dyam_Reg_Load(2,V(24))
	call_c   Dyam_Reg_Load(4,V(21))
	move     V(27), R(6)
	move     S(5), R(7)
	move     V(28), R(8)
	move     S(5), R(9)
	move     V(29), R(10)
	move     S(5), R(11)
	pl_call  pred_tag_autoloader_6()
	call_c   Dyam_Choice(fun38)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(27),&ref[113])
	fail_ret
	call_c   Dyam_Cut()
	pl_fail


;; TERM 106: tig(_X, _W)
c_code local build_ref_106
	ret_reg &ref[106]
	call_c   build_ref_286()
	call_c   Dyam_Create_Binary(&ref[286],V(23),V(22))
	move_ret ref[106]
	c_ret

;; TERM 286: tig
c_code local build_ref_286
	ret_reg &ref[286]
	call_c   Dyam_Create_Atom("tig")
	move_ret ref[286]
	c_ret

long local pool_fun42[6]=[131075,build_ref_104,build_ref_105,build_ref_106,pool_fun40,pool_fun40]

pl_code local fun42
	call_c   Dyam_Pool(pool_fun42)
	call_c   Dyam_Allocate(0)
	move     &ref[104], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(22))
	move     V(24), R(4)
	move     S(5), R(5)
	pl_call  pred_tagguide_strip_3()
	call_c   Dyam_Choice(fun41)
	call_c   Dyam_Set_Cut()
	pl_call  Domain_2(V(22),&ref[105])
	call_c   Dyam_Cut()
	move     &ref[106], R(0)
	move     S(5), R(1)
	pl_call  pred_record_without_doublon_1()
	pl_jump  fun40()

;; TERM 119: '$TUPPLE'(35047639354708)
c_code local build_ref_119
	ret_reg &ref[119]
	call_c   Dyam_Create_Simple_Tupple(0,100659424)
	move_ret ref[119]
	c_ret

long local pool_fun43[2]=[1,build_ref_120]

long local pool_fun44[3]=[65537,build_ref_105,pool_fun43]

pl_code local fun44
	call_c   Dyam_Remove_Choice()
	pl_call  Domain_2(V(22),&ref[105])
fun43:
	move     &ref[120], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Load(4,V(2))
	call_c   Dyam_Reg_Load(6,V(22))
	move     V(23), R(8)
	move     S(5), R(9)
	call_c   Dyam_Reg_Deallocate(5)
fun35:
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Reg_Bind(2,V(5))
	call_c   Dyam_Multi_Reg_Bind(4,2,3)
	call_c   Dyam_Choice(fun34)
	pl_call  fun28(&seed[21],1)
	pl_fail



;; TERM 121: tig(_C, _W)
c_code local build_ref_121
	ret_reg &ref[121]
	call_c   build_ref_286()
	call_c   Dyam_Create_Binary(&ref[286],V(2),V(22))
	move_ret ref[121]
	c_ret

long local pool_fun45[4]=[131073,build_ref_121,pool_fun44,pool_fun43]

pl_code local fun45
	call_c   Dyam_Update_Choice(fun44)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[121])
	call_c   Dyam_Cut()
	pl_jump  fun43()

long local pool_fun46[5]=[131074,build_ref_95,build_ref_44,pool_fun45,pool_fun43]

pl_code local fun47
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(17),V(19))
	fail_ret
	call_c   Dyam_Unify(V(18),V(20))
	fail_ret
fun46:
	call_c   Dyam_Unify(V(21),&ref[95])
	fail_ret
	call_c   Dyam_Choice(fun45)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(10),&ref[44])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(22),&ref[44])
	fail_ret
	pl_jump  fun43()


long local pool_fun48[4]=[131073,build_ref_94,pool_fun46,pool_fun46]

pl_code local fun48
	call_c   Dyam_Pool(pool_fun48)
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun47)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[94])
	call_c   Dyam_Cut()
	pl_jump  fun46()

;; TERM 123: '$TUPPLE'(35047639355136)
c_code local build_ref_123
	ret_reg &ref[123]
	call_c   Dyam_Create_Simple_Tupple(0,234876928)
	move_ret ref[123]
	c_ret

long local pool_fun49[6]=[5,build_ref_83,build_ref_85,build_ref_124,build_ref_125,build_ref_104]

pl_code local fun49
	call_c   Dyam_Pool(pool_fun49)
	call_c   Dyam_Unify_Item(&ref[83])
	fail_ret
	call_c   Dyam_Allocate(0)
	pl_call  Object_1(&ref[85])
	move     &ref[124], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	move     &ref[125], R(4)
	move     S(5), R(5)
	move     &ref[104], R(6)
	move     S(5), R(7)
	call_c   Dyam_Reg_Deallocate(4)
fun32:
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Reg_Bind(2,V(4))
	call_c   Dyam_Multi_Reg_Bind(4,2,2)
	call_c   Dyam_Choice(fun31)
	pl_call  fun28(&seed[19],1)
	pl_fail


;; TERM 84: '*GUARD*'(pgm_to_lpda(tag_tree{family=> _B, name=> _C, tree=> _D}, _E)) :> []
c_code local build_ref_84
	ret_reg &ref[84]
	call_c   build_ref_83()
	call_c   Dyam_Create_Binary(I(9),&ref[83],I(0))
	move_ret ref[84]
	c_ret

c_code local build_seed_12
	ret_reg &seed[12]
	call_c   build_ref_15()
	call_c   build_ref_128()
	call_c   Dyam_Seed_Start(&ref[15],&ref[128],I(0),fun1,1)
	call_c   build_ref_127()
	call_c   Dyam_Seed_Add_Comp(&ref[127],fun52,0)
	call_c   Dyam_Seed_End()
	move_ret seed[12]
	c_ret

;; TERM 127: '*GUARD*'(tagguide_strip_handler([_B|_C], _D, _E))
c_code local build_ref_127
	ret_reg &ref[127]
	call_c   build_ref_16()
	call_c   build_ref_126()
	call_c   Dyam_Create_Unary(&ref[16],&ref[126])
	move_ret ref[127]
	c_ret

;; TERM 126: tagguide_strip_handler([_B|_C], _D, _E)
c_code local build_ref_126
	ret_reg &ref[126]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(1),V(2))
	move_ret R(0)
	call_c   build_ref_212()
	call_c   Dyam_Term_Start(&ref[212],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[126]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

pl_code local fun51
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Reg_Load(0,V(2))
	call_c   Dyam_Reg_Load(2,V(3))
	call_c   Dyam_Reg_Load(4,V(4))
	pl_call  pred_tagguide_strip_3()
	pl_jump  fun18()

;; TERM 130: [_F]
c_code local build_ref_130
	ret_reg &ref[130]
	call_c   Dyam_Create_List(V(5),I(0))
	move_ret ref[130]
	c_ret

long local pool_fun50[2]=[1,build_ref_130]

pl_code local fun50
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(4),&ref[130])
	fail_ret
	pl_jump  fun18()

;; TERM 129: [_F|_G]
c_code local build_ref_129
	ret_reg &ref[129]
	call_c   Dyam_Create_List(V(5),V(6))
	move_ret ref[129]
	c_ret

long local pool_fun52[4]=[65538,build_ref_127,build_ref_129,pool_fun50]

pl_code local fun52
	call_c   Dyam_Pool(pool_fun52)
	call_c   Dyam_Unify_Item(&ref[127])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun51)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(3))
	move     V(5), R(4)
	move     S(5), R(5)
	pl_call  pred_tagguide_strip_3()
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun50)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Reg_Load(0,V(2))
	call_c   Dyam_Reg_Load(2,V(3))
	move     V(6), R(4)
	move     S(5), R(5)
	pl_call  pred_tagguide_strip_3()
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(4),&ref[129])
	fail_ret
	pl_jump  fun18()

;; TERM 128: '*GUARD*'(tagguide_strip_handler([_B|_C], _D, _E)) :> []
c_code local build_ref_128
	ret_reg &ref[128]
	call_c   build_ref_127()
	call_c   Dyam_Create_Binary(I(9),&ref[127],I(0))
	move_ret ref[128]
	c_ret

c_code local build_seed_2
	ret_reg &seed[2]
	call_c   build_ref_56()
	call_c   build_ref_133()
	call_c   Dyam_Seed_Start(&ref[56],&ref[133],I(0),fun1,1)
	call_c   build_ref_134()
	call_c   Dyam_Seed_Add_Comp(&ref[134],fun54,0)
	call_c   Dyam_Seed_End()
	move_ret seed[2]
	c_ret

;; TERM 134: '*PROLOG-ITEM*'{top=> unfold('*GUIDE-LAST*'(_B, _C), _D, _E, (_F :> _G), _D), cont=> _A}
c_code local build_ref_134
	ret_reg &ref[134]
	call_c   build_ref_233()
	call_c   build_ref_131()
	call_c   Dyam_Create_Binary(&ref[233],&ref[131],V(0))
	move_ret ref[134]
	c_ret

;; TERM 131: unfold('*GUIDE-LAST*'(_B, _C), _D, _E, (_F :> _G), _D)
c_code local build_ref_131
	ret_reg &ref[131]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_287()
	call_c   Dyam_Create_Binary(&ref[287],V(1),V(2))
	move_ret R(0)
	call_c   Dyam_Create_Binary(I(9),V(5),V(6))
	move_ret R(1)
	call_c   build_ref_234()
	call_c   Dyam_Term_Start(&ref[234],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[131]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 287: '*GUIDE-LAST*'
c_code local build_ref_287
	ret_reg &ref[287]
	call_c   Dyam_Create_Atom("*GUIDE-LAST*")
	move_ret ref[287]
	c_ret

c_code local build_seed_26
	ret_reg &seed[26]
	call_c   build_ref_62()
	call_c   build_ref_139()
	call_c   Dyam_Seed_Start(&ref[62],&ref[139],I(0),fun3,1)
	call_c   build_ref_142()
	call_c   Dyam_Seed_Add_Comp(&ref[142],&ref[139],0)
	call_c   Dyam_Seed_End()
	move_ret seed[26]
	c_ret

;; TERM 142: '*PROLOG-FIRST*'(seed_install(seed{model=> '*RITEM*'(_D, _B), id=> _H, anchor=> _I, subs_comp=> _J, code=> _K, go=> _L, tabule=> _M, schedule=> _N, subsume=> _O, model_ref=> _P, subs_comp_ref=> _Q, c_type=> _R, c_type_ref=> _S, out_env=> _T, appinfo=> _U, tail_flag=> _V}, 1, _F)) :> '$$HOLE$$'
c_code local build_ref_142
	ret_reg &ref[142]
	call_c   build_ref_141()
	call_c   Dyam_Create_Binary(I(9),&ref[141],I(7))
	move_ret ref[142]
	c_ret

;; TERM 141: '*PROLOG-FIRST*'(seed_install(seed{model=> '*RITEM*'(_D, _B), id=> _H, anchor=> _I, subs_comp=> _J, code=> _K, go=> _L, tabule=> _M, schedule=> _N, subsume=> _O, model_ref=> _P, subs_comp_ref=> _Q, c_type=> _R, c_type_ref=> _S, out_env=> _T, appinfo=> _U, tail_flag=> _V}, 1, _F))
c_code local build_ref_141
	ret_reg &ref[141]
	call_c   build_ref_57()
	call_c   build_ref_140()
	call_c   Dyam_Create_Unary(&ref[57],&ref[140])
	move_ret ref[141]
	c_ret

;; TERM 140: seed_install(seed{model=> '*RITEM*'(_D, _B), id=> _H, anchor=> _I, subs_comp=> _J, code=> _K, go=> _L, tabule=> _M, schedule=> _N, subsume=> _O, model_ref=> _P, subs_comp_ref=> _Q, c_type=> _R, c_type_ref=> _S, out_env=> _T, appinfo=> _U, tail_flag=> _V}, 1, _F)
c_code local build_ref_140
	ret_reg &ref[140]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_0()
	call_c   Dyam_Create_Binary(&ref[0],V(3),V(1))
	move_ret R(0)
	call_c   build_ref_237()
	call_c   Dyam_Term_Start(&ref[237],16)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_Arg(V(21))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_236()
	call_c   Dyam_Term_Start(&ref[236],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(N(1))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[140]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 139: '*PROLOG-ITEM*'{top=> seed_install(seed{model=> '*RITEM*'(_D, _B), id=> _H, anchor=> _I, subs_comp=> _J, code=> _K, go=> _L, tabule=> _M, schedule=> _N, subsume=> _O, model_ref=> _P, subs_comp_ref=> _Q, c_type=> _R, c_type_ref=> _S, out_env=> _T, appinfo=> _U, tail_flag=> _V}, 1, _F), cont=> '$CLOSURE'('$fun'(53, 0, 1119362888), '$TUPPLE'(35047639355364))}
c_code local build_ref_139
	ret_reg &ref[139]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,373293056)
	move_ret R(0)
	call_c   Dyam_Closure_Aux(fun53,R(0))
	move_ret R(0)
	call_c   build_ref_233()
	call_c   build_ref_140()
	call_c   Dyam_Create_Binary(&ref[233],&ref[140],R(0))
	move_ret ref[139]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_25
	ret_reg &seed[25]
	call_c   build_ref_62()
	call_c   build_ref_135()
	call_c   Dyam_Seed_Start(&ref[62],&ref[135],I(0),fun3,1)
	call_c   build_ref_138()
	call_c   Dyam_Seed_Add_Comp(&ref[138],&ref[135],0)
	call_c   Dyam_Seed_End()
	move_ret seed[25]
	c_ret

;; TERM 138: '*PROLOG-FIRST*'(seed_install(seed{model=> '*RITEM*'(_D, _C), id=> _W, anchor=> _X, subs_comp=> _Y, code=> _Z, go=> _A1, tabule=> _B1, schedule=> no, subsume=> _C1, model_ref=> _D1, subs_comp_ref=> _E1, c_type=> _F1, c_type_ref=> _G1, out_env=> _H1, appinfo=> _I1, tail_flag=> _J1}, 1, _G)) :> '$$HOLE$$'
c_code local build_ref_138
	ret_reg &ref[138]
	call_c   build_ref_137()
	call_c   Dyam_Create_Binary(I(9),&ref[137],I(7))
	move_ret ref[138]
	c_ret

;; TERM 137: '*PROLOG-FIRST*'(seed_install(seed{model=> '*RITEM*'(_D, _C), id=> _W, anchor=> _X, subs_comp=> _Y, code=> _Z, go=> _A1, tabule=> _B1, schedule=> no, subsume=> _C1, model_ref=> _D1, subs_comp_ref=> _E1, c_type=> _F1, c_type_ref=> _G1, out_env=> _H1, appinfo=> _I1, tail_flag=> _J1}, 1, _G))
c_code local build_ref_137
	ret_reg &ref[137]
	call_c   build_ref_57()
	call_c   build_ref_136()
	call_c   Dyam_Create_Unary(&ref[57],&ref[136])
	move_ret ref[137]
	c_ret

;; TERM 136: seed_install(seed{model=> '*RITEM*'(_D, _C), id=> _W, anchor=> _X, subs_comp=> _Y, code=> _Z, go=> _A1, tabule=> _B1, schedule=> no, subsume=> _C1, model_ref=> _D1, subs_comp_ref=> _E1, c_type=> _F1, c_type_ref=> _G1, out_env=> _H1, appinfo=> _I1, tail_flag=> _J1}, 1, _G)
c_code local build_ref_136
	ret_reg &ref[136]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_0()
	call_c   Dyam_Create_Binary(&ref[0],V(3),V(2))
	move_ret R(0)
	call_c   build_ref_237()
	call_c   build_ref_44()
	call_c   Dyam_Term_Start(&ref[237],16)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(22))
	call_c   Dyam_Term_Arg(V(23))
	call_c   Dyam_Term_Arg(V(24))
	call_c   Dyam_Term_Arg(V(25))
	call_c   Dyam_Term_Arg(V(26))
	call_c   Dyam_Term_Arg(V(27))
	call_c   Dyam_Term_Arg(&ref[44])
	call_c   Dyam_Term_Arg(V(28))
	call_c   Dyam_Term_Arg(V(29))
	call_c   Dyam_Term_Arg(V(30))
	call_c   Dyam_Term_Arg(V(31))
	call_c   Dyam_Term_Arg(V(32))
	call_c   Dyam_Term_Arg(V(33))
	call_c   Dyam_Term_Arg(V(34))
	call_c   Dyam_Term_Arg(V(35))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_236()
	call_c   Dyam_Term_Start(&ref[236],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(N(1))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[136]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 135: '*PROLOG-ITEM*'{top=> seed_install(seed{model=> '*RITEM*'(_D, _C), id=> _W, anchor=> _X, subs_comp=> _Y, code=> _Z, go=> _A1, tabule=> _B1, schedule=> no, subsume=> _C1, model_ref=> _D1, subs_comp_ref=> _E1, c_type=> _F1, c_type_ref=> _G1, out_env=> _H1, appinfo=> _I1, tail_flag=> _J1}, 1, _G), cont=> _A}
c_code local build_ref_135
	ret_reg &ref[135]
	call_c   build_ref_233()
	call_c   build_ref_136()
	call_c   Dyam_Create_Binary(&ref[233],&ref[136],V(0))
	move_ret ref[135]
	c_ret

long local pool_fun53[2]=[1,build_seed_25]

pl_code local fun53
	call_c   Dyam_Pool(pool_fun53)
	pl_jump  fun5(&seed[25],12)

long local pool_fun54[3]=[2,build_ref_134,build_seed_26]

pl_code local fun54
	call_c   Dyam_Pool(pool_fun54)
	call_c   Dyam_Unify_Item(&ref[134])
	fail_ret
	pl_jump  fun5(&seed[26],12)

;; TERM 133: '*PROLOG-FIRST*'(unfold('*GUIDE-LAST*'(_B, _C), _D, _E, (_F :> _G), _D)) :> []
c_code local build_ref_133
	ret_reg &ref[133]
	call_c   build_ref_132()
	call_c   Dyam_Create_Binary(I(9),&ref[132],I(0))
	move_ret ref[133]
	c_ret

;; TERM 132: '*PROLOG-FIRST*'(unfold('*GUIDE-LAST*'(_B, _C), _D, _E, (_F :> _G), _D))
c_code local build_ref_132
	ret_reg &ref[132]
	call_c   build_ref_57()
	call_c   build_ref_131()
	call_c   Dyam_Create_Unary(&ref[57],&ref[131])
	move_ret ref[132]
	c_ret

c_code local build_seed_11
	ret_reg &seed[11]
	call_c   build_ref_15()
	call_c   build_ref_145()
	call_c   Dyam_Seed_Start(&ref[15],&ref[145],I(0),fun1,1)
	call_c   build_ref_144()
	call_c   Dyam_Seed_Add_Comp(&ref[144],fun68,0)
	call_c   Dyam_Seed_End()
	move_ret seed[11]
	c_ret

;; TERM 144: '*GUARD*'(tagguide_strip_handler(tag_node{id=> _B, label=> _C, children=> _D, kind=> _E, adj=> _F, spine=> _G, top=> _H, bot=> _I, token=> _J, adjleft=> _K, adjright=> _L, adjwrap=> _M}, _N, tag_node{id=> _B, label=> _O, children=> _P, kind=> _Q, adj=> _F, spine=> _G, top=> _R, bot=> _S, token=> _T, adjleft=> _K, adjright=> _L, adjwrap=> _M}))
c_code local build_ref_144
	ret_reg &ref[144]
	call_c   build_ref_16()
	call_c   build_ref_143()
	call_c   Dyam_Create_Unary(&ref[16],&ref[143])
	move_ret ref[144]
	c_ret

;; TERM 143: tagguide_strip_handler(tag_node{id=> _B, label=> _C, children=> _D, kind=> _E, adj=> _F, spine=> _G, top=> _H, bot=> _I, token=> _J, adjleft=> _K, adjright=> _L, adjwrap=> _M}, _N, tag_node{id=> _B, label=> _O, children=> _P, kind=> _Q, adj=> _F, spine=> _G, top=> _R, bot=> _S, token=> _T, adjleft=> _K, adjright=> _L, adjwrap=> _M})
c_code local build_ref_143
	ret_reg &ref[143]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_268()
	call_c   Dyam_Term_Start(&ref[268],12)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   Dyam_Term_Start(&ref[268],12)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_End()
	move_ret R(1)
	call_c   build_ref_212()
	call_c   Dyam_Term_Start(&ref[212],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_End()
	move_ret ref[143]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 146: [yes,_N]
c_code local build_ref_146
	ret_reg &ref[146]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(13),I(0))
	move_ret R(0)
	call_c   build_ref_176()
	call_c   Dyam_Create_List(&ref[176],R(0))
	move_ret ref[146]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 176: yes
c_code local build_ref_176
	ret_reg &ref[176]
	call_c   Dyam_Create_Atom("yes")
	move_ret ref[176]
	c_ret

;; TERM 164: '$CLOSURE'('$fun'(64, 0, 1119434908), '$TUPPLE'(35047639093316))
c_code local build_ref_164
	ret_reg &ref[164]
	call_c   build_ref_163()
	call_c   Dyam_Closure_Aux(fun64,&ref[163])
	move_ret ref[164]
	c_ret

pl_code local fun64
	call_c   Dyam_Unify(V(17),V(14))
	fail_ret
	call_c   Dyam_Unify(V(18),V(14))
	fail_ret
	call_c   Dyam_Unify(V(16),V(4))
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(3))
	call_c   Dyam_Reg_Load(2,V(13))
	call_c   Dyam_Reg_Load(4,V(15))
	pl_call  pred_tagguide_strip_3()
	pl_jump  fun18()

;; TERM 163: '$TUPPLE'(35047639093316)
c_code local build_ref_163
	ret_reg &ref[163]
	call_c   Dyam_Create_Simple_Tupple(0,50396160)
	move_ret ref[163]
	c_ret

long local pool_fun65[2]=[1,build_ref_164]

pl_code local fun65
	call_c   Dyam_Remove_Choice()
	move     &ref[164], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Load(4,V(2))
	call_c   Dyam_Reg_Load(6,V(14))
	call_c   Dyam_Reg_Deallocate(4)
fun61:
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Reg_Bind(2,V(4))
	call_c   Dyam_Multi_Reg_Bind(4,2,2)
	call_c   Dyam_Choice(fun60)
	pl_call  fun28(&seed[27],1)
	pl_fail


;; TERM 161: escape
c_code local build_ref_161
	ret_reg &ref[161]
	call_c   Dyam_Create_Atom("escape")
	move_ret ref[161]
	c_ret

long local pool_fun66[3]=[65537,build_ref_161,pool_fun65]

pl_code local fun66
	call_c   Dyam_Update_Choice(fun65)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(4),&ref[161])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(14),V(2))
	fail_ret
	call_c   Dyam_Unify(V(15),V(3))
	fail_ret
	call_c   Dyam_Unify(V(16),V(4))
	fail_ret
	call_c   Dyam_Unify(V(17),V(7))
	fail_ret
	call_c   Dyam_Unify(V(18),V(8))
	fail_ret
	call_c   Dyam_Unify(V(19),V(9))
	fail_ret
	pl_jump  fun18()

;; TERM 160: scan
c_code local build_ref_160
	ret_reg &ref[160]
	call_c   Dyam_Create_Atom("scan")
	move_ret ref[160]
	c_ret

long local pool_fun67[3]=[65537,build_ref_160,pool_fun66]

pl_code local fun67
	call_c   Dyam_Update_Choice(fun66)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(4),&ref[160])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(14),V(2))
	fail_ret
	call_c   Dyam_Unify(V(15),V(3))
	fail_ret
	call_c   Dyam_Unify(V(16),V(4))
	fail_ret
	call_c   Dyam_Unify(V(17),V(7))
	fail_ret
	call_c   Dyam_Unify(V(18),V(8))
	fail_ret
	call_c   Dyam_Unify(V(19),V(9))
	fail_ret
	pl_jump  fun18()

;; TERM 147: [anchor,coanchor]
c_code local build_ref_147
	ret_reg &ref[147]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_288()
	call_c   Dyam_Create_List(&ref[288],I(0))
	move_ret R(0)
	call_c   build_ref_243()
	call_c   Dyam_Create_List(&ref[243],R(0))
	move_ret ref[147]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 288: coanchor
c_code local build_ref_288
	ret_reg &ref[288]
	call_c   Dyam_Create_Atom("coanchor")
	move_ret ref[288]
	c_ret

;; TERM 148: [tag_node{id=> _B, label=> _C, children=> [], kind=> _E, adj=> no, spine=> _G, top=> _H, bot=> _I, token=> _U, adjleft=> _V, adjright=> _W, adjwrap=> _X}]
c_code local build_ref_148
	ret_reg &ref[148]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_268()
	call_c   build_ref_44()
	call_c   Dyam_Term_Start(&ref[268],12)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(&ref[44])
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_Arg(V(21))
	call_c   Dyam_Term_Arg(V(22))
	call_c   Dyam_Term_Arg(V(23))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   Dyam_Create_List(R(0),I(0))
	move_ret ref[148]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 159: '$CLOSURE'('$fun'(62, 0, 1119416620), '$TUPPLE'(35047639355608))
c_code local build_ref_159
	ret_reg &ref[159]
	call_c   build_ref_158()
	call_c   Dyam_Closure_Aux(fun62,&ref[158])
	move_ret ref[159]
	c_ret

;; TERM 156: std
c_code local build_ref_156
	ret_reg &ref[156]
	call_c   Dyam_Create_Atom("std")
	move_ret ref[156]
	c_ret

long local pool_fun62[2]=[1,build_ref_156]

pl_code local fun62
	call_c   Dyam_Pool(pool_fun62)
	call_c   Dyam_Unify(V(17),V(14))
	fail_ret
	call_c   Dyam_Unify(V(18),V(14))
	fail_ret
	call_c   Dyam_Unify(V(16),&ref[156])
	fail_ret
	call_c   Dyam_Allocate(0)
	pl_jump  fun18()

;; TERM 158: '$TUPPLE'(35047639355608)
c_code local build_ref_158
	ret_reg &ref[158]
	call_c   Dyam_Create_Simple_Tupple(0,23552)
	move_ret ref[158]
	c_ret

long local pool_fun63[3]=[2,build_ref_148,build_ref_159]

pl_code local fun63
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(15),&ref[148])
	fail_ret
	move     &ref[159], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	call_c   Dyam_Reg_Load(4,V(2))
	call_c   Dyam_Reg_Load(6,V(14))
	call_c   Dyam_Reg_Deallocate(4)
	pl_jump  fun61()

pl_code local fun57
	call_c   Dyam_Remove_Choice()
fun56:
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(14),V(2))
	fail_ret
	call_c   Dyam_Unify(V(15),V(3))
	fail_ret
	call_c   Dyam_Unify(V(16),V(4))
	fail_ret
	call_c   Dyam_Unify(V(17),V(7))
	fail_ret
	call_c   Dyam_Unify(V(18),V(8))
	fail_ret
	call_c   Dyam_Unify(V(19),V(9))
	fail_ret
	pl_jump  fun18()


pl_code local fun58
	call_c   Dyam_Update_Choice(fun57)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(7),V(8))
	fail_ret
	call_c   Dyam_Cut()
	pl_fail

long local pool_fun68[7]=[131076,build_ref_144,build_ref_146,build_ref_147,build_ref_44,pool_fun67,pool_fun63]

pl_code local fun68
	call_c   Dyam_Pool(pool_fun68)
	call_c   Dyam_Unify_Item(&ref[144])
	fail_ret
	call_c   Dyam_Allocate(0)
	pl_call  Domain_2(V(6),&ref[146])
	call_c   Dyam_Choice(fun67)
	call_c   Dyam_Set_Cut()
	pl_call  Domain_2(V(4),&ref[147])
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun63)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Choice(fun58)
	call_c   Dyam_Unify(V(5),&ref[44])
	fail_ret
	pl_jump  fun56()

;; TERM 145: '*GUARD*'(tagguide_strip_handler(tag_node{id=> _B, label=> _C, children=> _D, kind=> _E, adj=> _F, spine=> _G, top=> _H, bot=> _I, token=> _J, adjleft=> _K, adjright=> _L, adjwrap=> _M}, _N, tag_node{id=> _B, label=> _O, children=> _P, kind=> _Q, adj=> _F, spine=> _G, top=> _R, bot=> _S, token=> _T, adjleft=> _K, adjright=> _L, adjwrap=> _M})) :> []
c_code local build_ref_145
	ret_reg &ref[145]
	call_c   build_ref_144()
	call_c   Dyam_Create_Binary(I(9),&ref[144],I(0))
	move_ret ref[145]
	c_ret

c_code local build_seed_4
	ret_reg &seed[4]
	call_c   build_ref_15()
	call_c   build_ref_167()
	call_c   Dyam_Seed_Start(&ref[15],&ref[167],I(0),fun1,1)
	call_c   build_ref_166()
	call_c   Dyam_Seed_Add_Comp(&ref[166],fun77,0)
	call_c   Dyam_Seed_End()
	move_ret seed[4]
	c_ret

;; TERM 166: '*GUARD*'(tagguide_compile_tree(_B, tag_node{id=> _C, label=> _D, children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}, _O, _P, _Q, _R))
c_code local build_ref_166
	ret_reg &ref[166]
	call_c   build_ref_16()
	call_c   build_ref_165()
	call_c   Dyam_Create_Unary(&ref[16],&ref[165])
	move_ret ref[166]
	c_ret

;; TERM 165: tagguide_compile_tree(_B, tag_node{id=> _C, label=> _D, children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}, _O, _P, _Q, _R)
c_code local build_ref_165
	ret_reg &ref[165]
	call_c   build_ref_281()
	call_c   build_ref_168()
	call_c   Dyam_Term_Start(&ref[281],6)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(&ref[168])
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_End()
	move_ret ref[165]
	c_ret

;; TERM 168: tag_node{id=> _C, label=> _D, children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}
c_code local build_ref_168
	ret_reg &ref[168]
	call_c   build_ref_268()
	call_c   Dyam_Term_Start(&ref[268],12)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_End()
	move_ret ref[168]
	c_ret

;; TERM 201: '$CLOSURE'('$fun'(76, 0, 1119554932), '$TUPPLE'(35047639093824))
c_code local build_ref_201
	ret_reg &ref[201]
	call_c   build_ref_200()
	call_c   Dyam_Closure_Aux(fun76,&ref[200])
	move_ret ref[201]
	c_ret

c_code local build_seed_32
	ret_reg &seed[32]
	call_c   build_ref_16()
	call_c   build_ref_194()
	call_c   Dyam_Seed_Start(&ref[16],&ref[194],I(0),fun3,1)
	call_c   build_ref_195()
	call_c   Dyam_Seed_Add_Comp(&ref[195],&ref[194],0)
	call_c   Dyam_Seed_End()
	move_ret seed[32]
	c_ret

;; TERM 195: '*GUARD*'(make_tag_subst_callret(_I, _S, _U, _B1, _C1)) :> '$$HOLE$$'
c_code local build_ref_195
	ret_reg &ref[195]
	call_c   build_ref_194()
	call_c   Dyam_Create_Binary(I(9),&ref[194],I(7))
	move_ret ref[195]
	c_ret

;; TERM 194: '*GUARD*'(make_tag_subst_callret(_I, _S, _U, _B1, _C1))
c_code local build_ref_194
	ret_reg &ref[194]
	call_c   build_ref_16()
	call_c   build_ref_193()
	call_c   Dyam_Create_Unary(&ref[16],&ref[193])
	move_ret ref[194]
	c_ret

;; TERM 193: make_tag_subst_callret(_I, _S, _U, _B1, _C1)
c_code local build_ref_193
	ret_reg &ref[193]
	call_c   build_ref_289()
	call_c   Dyam_Term_Start(&ref[289],5)
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_Arg(V(27))
	call_c   Dyam_Term_Arg(V(28))
	call_c   Dyam_Term_End()
	move_ret ref[193]
	c_ret

;; TERM 289: make_tag_subst_callret
c_code local build_ref_289
	ret_reg &ref[289]
	call_c   Dyam_Create_Atom("make_tag_subst_callret")
	move_ret ref[289]
	c_ret

;; TERM 182: '*SAFIRST*'(_B1) :> _E1
c_code local build_ref_182
	ret_reg &ref[182]
	call_c   build_ref_181()
	call_c   Dyam_Create_Binary(I(9),&ref[181],V(30))
	move_ret ref[182]
	c_ret

;; TERM 181: '*SAFIRST*'(_B1)
c_code local build_ref_181
	ret_reg &ref[181]
	call_c   build_ref_290()
	call_c   Dyam_Create_Unary(&ref[290],V(27))
	move_ret ref[181]
	c_ret

;; TERM 290: '*SAFIRST*'
c_code local build_ref_290
	ret_reg &ref[290]
	call_c   Dyam_Create_Atom("*SAFIRST*")
	move_ret ref[290]
	c_ret

;; TERM 190: '*GUIDE-LAST*'(_C1, _B)
c_code local build_ref_190
	ret_reg &ref[190]
	call_c   build_ref_287()
	call_c   Dyam_Create_Binary(&ref[287],V(28),V(1))
	move_ret ref[190]
	c_ret

c_code local build_seed_33
	ret_reg &seed[33]
	call_c   build_ref_16()
	call_c   build_ref_197()
	call_c   Dyam_Seed_Start(&ref[16],&ref[197],I(0),fun3,1)
	call_c   build_ref_198()
	call_c   Dyam_Seed_Add_Comp(&ref[198],&ref[197],0)
	call_c   Dyam_Seed_End()
	move_ret seed[33]
	c_ret

;; TERM 198: '*GUARD*'(tag_compile_subtree(_B, tag_node{id=> _C, label=> _D, children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}, _S, _U, _F1, _E1, _P, _Q, _G1)) :> '$$HOLE$$'
c_code local build_ref_198
	ret_reg &ref[198]
	call_c   build_ref_197()
	call_c   Dyam_Create_Binary(I(9),&ref[197],I(7))
	move_ret ref[198]
	c_ret

;; TERM 197: '*GUARD*'(tag_compile_subtree(_B, tag_node{id=> _C, label=> _D, children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}, _S, _U, _F1, _E1, _P, _Q, _G1))
c_code local build_ref_197
	ret_reg &ref[197]
	call_c   build_ref_16()
	call_c   build_ref_196()
	call_c   Dyam_Create_Unary(&ref[16],&ref[196])
	move_ret ref[197]
	c_ret

;; TERM 196: tag_compile_subtree(_B, tag_node{id=> _C, label=> _D, children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}, _S, _U, _F1, _E1, _P, _Q, _G1)
c_code local build_ref_196
	ret_reg &ref[196]
	call_c   build_ref_291()
	call_c   build_ref_168()
	call_c   Dyam_Term_Start(&ref[291],9)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(&ref[168])
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_Arg(V(31))
	call_c   Dyam_Term_Arg(V(30))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(32))
	call_c   Dyam_Term_End()
	move_ret ref[196]
	c_ret

;; TERM 291: tag_compile_subtree
c_code local build_ref_291
	ret_reg &ref[291]
	call_c   Dyam_Create_Atom("tag_compile_subtree")
	move_ret ref[291]
	c_ret

long local pool_fun75[5]=[4,build_seed_32,build_ref_182,build_ref_190,build_seed_33]

pl_code local fun75
	call_c   Dyam_Remove_Choice()
	pl_call  fun5(&seed[32],1)
	call_c   Dyam_Unify(V(14),&ref[182])
	fail_ret
	call_c   Dyam_Unify(V(31),&ref[190])
	fail_ret
	call_c   Dyam_Reg_Load(0,V(27))
	move     V(32), R(2)
	move     S(5), R(3)
	pl_call  pred_tupple_2()
	pl_call  fun5(&seed[33],1)
	pl_jump  fun18()

;; TERM 191: 'Not a TAGGUIDE mode in {left,right} for tree ~w'
c_code local build_ref_191
	ret_reg &ref[191]
	call_c   Dyam_Create_Atom("Not a TAGGUIDE mode in {left,right} for tree ~w")
	move_ret ref[191]
	c_ret

;; TERM 192: [_B]
c_code local build_ref_192
	ret_reg &ref[192]
	call_c   Dyam_Create_List(V(1),I(0))
	move_ret ref[192]
	c_ret

c_code local build_seed_31
	ret_reg &seed[31]
	call_c   build_ref_16()
	call_c   build_ref_185()
	call_c   Dyam_Seed_Start(&ref[16],&ref[185],I(0),fun3,1)
	call_c   build_ref_186()
	call_c   Dyam_Seed_Add_Comp(&ref[186],&ref[185],0)
	call_c   Dyam_Seed_End()
	move_ret seed[31]
	c_ret

;; TERM 186: '*GUARD*'(tag_compile_subtree_noadj(_B, tag_node{id=> _C, label=> _D, children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}, _S, _U, _F1, _E1, _P, _D1, _G1)) :> '$$HOLE$$'
c_code local build_ref_186
	ret_reg &ref[186]
	call_c   build_ref_185()
	call_c   Dyam_Create_Binary(I(9),&ref[185],I(7))
	move_ret ref[186]
	c_ret

;; TERM 185: '*GUARD*'(tag_compile_subtree_noadj(_B, tag_node{id=> _C, label=> _D, children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}, _S, _U, _F1, _E1, _P, _D1, _G1))
c_code local build_ref_185
	ret_reg &ref[185]
	call_c   build_ref_16()
	call_c   build_ref_184()
	call_c   Dyam_Create_Unary(&ref[16],&ref[184])
	move_ret ref[185]
	c_ret

;; TERM 184: tag_compile_subtree_noadj(_B, tag_node{id=> _C, label=> _D, children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}, _S, _U, _F1, _E1, _P, _D1, _G1)
c_code local build_ref_184
	ret_reg &ref[184]
	call_c   build_ref_292()
	call_c   build_ref_168()
	call_c   Dyam_Term_Start(&ref[292],9)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(&ref[168])
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_Arg(V(31))
	call_c   Dyam_Term_Arg(V(30))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(29))
	call_c   Dyam_Term_Arg(V(32))
	call_c   Dyam_Term_End()
	move_ret ref[184]
	c_ret

;; TERM 292: tag_compile_subtree_noadj
c_code local build_ref_292
	ret_reg &ref[292]
	call_c   Dyam_Create_Atom("tag_compile_subtree_noadj")
	move_ret ref[292]
	c_ret

long local pool_fun72[2]=[1,build_seed_31]

long local pool_fun73[4]=[65538,build_ref_191,build_ref_192,pool_fun72]

pl_code local fun73
	call_c   Dyam_Remove_Choice()
	move     &ref[191], R(0)
	move     0, R(1)
	move     &ref[192], R(2)
	move     S(5), R(3)
	pl_call  pred_error_2()
fun72:
	pl_call  fun5(&seed[31],1)
	pl_jump  fun18()


;; TERM 189: _Q * right_tig(_Z, _H1, _B1)
c_code local build_ref_189
	ret_reg &ref[189]
	call_c   build_ref_178()
	call_c   build_ref_188()
	call_c   Dyam_Create_Binary(&ref[178],V(16),&ref[188])
	move_ret ref[189]
	c_ret

;; TERM 188: right_tig(_Z, _H1, _B1)
c_code local build_ref_188
	ret_reg &ref[188]
	call_c   build_ref_293()
	call_c   Dyam_Term_Start(&ref[293],3)
	call_c   Dyam_Term_Arg(V(25))
	call_c   Dyam_Term_Arg(V(33))
	call_c   Dyam_Term_Arg(V(27))
	call_c   Dyam_Term_End()
	move_ret ref[188]
	c_ret

;; TERM 293: right_tig
c_code local build_ref_293
	ret_reg &ref[293]
	call_c   Dyam_Create_Atom("right_tig")
	move_ret ref[293]
	c_ret

;; TERM 178: *
c_code local build_ref_178
	ret_reg &ref[178]
	call_c   Dyam_Create_Atom("*")
	move_ret ref[178]
	c_ret

long local pool_fun74[6]=[131075,build_ref_187,build_ref_189,build_ref_190,pool_fun73,pool_fun72]

pl_code local fun74
	call_c   Dyam_Update_Choice(fun73)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(17),&ref[187])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(17))
	call_c   Dyam_Reg_Load(2,V(25))
	call_c   Dyam_Reg_Load(4,V(8))
	move     V(33), R(6)
	move     S(5), R(7)
	call_c   Dyam_Reg_Load(8,V(20))
	move     V(27), R(10)
	move     S(5), R(11)
	move     V(28), R(12)
	move     S(5), R(13)
	pl_call  pred_make_tig_callret_7()
	call_c   Dyam_Unify(V(29),&ref[189])
	fail_ret
	call_c   Dyam_Unify(V(14),V(30))
	fail_ret
	call_c   Dyam_Unify(V(31),&ref[190])
	fail_ret
	move     I(0), R(0)
	move     0, R(1)
	move     V(32), R(2)
	move     S(5), R(3)
	pl_call  pred_tupple_2()
	pl_jump  fun72()

;; TERM 180: _Q * left_tig(_Z, _A1, _C1)
c_code local build_ref_180
	ret_reg &ref[180]
	call_c   build_ref_178()
	call_c   build_ref_179()
	call_c   Dyam_Create_Binary(&ref[178],V(16),&ref[179])
	move_ret ref[180]
	c_ret

;; TERM 179: left_tig(_Z, _A1, _C1)
c_code local build_ref_179
	ret_reg &ref[179]
	call_c   build_ref_294()
	call_c   Dyam_Term_Start(&ref[294],3)
	call_c   Dyam_Term_Arg(V(25))
	call_c   Dyam_Term_Arg(V(26))
	call_c   Dyam_Term_Arg(V(28))
	call_c   Dyam_Term_End()
	move_ret ref[179]
	c_ret

;; TERM 294: left_tig
c_code local build_ref_294
	ret_reg &ref[294]
	call_c   Dyam_Create_Atom("left_tig")
	move_ret ref[294]
	c_ret

;; TERM 183: noop
c_code local build_ref_183
	ret_reg &ref[183]
	call_c   Dyam_Create_Atom("noop")
	move_ret ref[183]
	c_ret

long local pool_fun76[9]=[196613,build_ref_176,build_ref_177,build_ref_180,build_ref_182,build_ref_183,pool_fun75,pool_fun74,pool_fun72]

pl_code local fun76
	call_c   Dyam_Pool(pool_fun76)
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun75)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(7),&ref[176])
	fail_ret
	call_c   Dyam_Cut()
	call_c   DYAM_evpred_functor(V(25),V(23),V(24))
	fail_ret
	call_c   Dyam_Choice(fun74)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(17),&ref[177])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(17))
	call_c   Dyam_Reg_Load(2,V(8))
	call_c   Dyam_Reg_Load(4,V(25))
	call_c   Dyam_Reg_Load(6,V(18))
	move     V(26), R(8)
	move     S(5), R(9)
	move     V(27), R(10)
	move     S(5), R(11)
	move     V(28), R(12)
	move     S(5), R(13)
	pl_call  pred_make_tig_callret_7()
	call_c   Dyam_Unify(V(29),&ref[180])
	fail_ret
	call_c   Dyam_Unify(V(14),&ref[182])
	fail_ret
	call_c   Dyam_Unify(V(31),&ref[183])
	fail_ret
	move     I(0), R(0)
	move     0, R(1)
	move     V(32), R(2)
	move     S(5), R(3)
	pl_call  pred_tupple_2()
	pl_jump  fun72()

;; TERM 200: '$TUPPLE'(35047639093824)
c_code local build_ref_200
	ret_reg &ref[200]
	call_c   Dyam_Create_Simple_Tupple(0,268434736)
	move_ret ref[200]
	c_ret

;; TERM 202: tag((_X / _Y))
c_code local build_ref_202
	ret_reg &ref[202]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_216()
	call_c   Dyam_Create_Binary(&ref[216],V(23),V(24))
	move_ret R(0)
	call_c   build_ref_295()
	call_c   Dyam_Create_Unary(&ref[295],R(0))
	move_ret ref[202]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 295: tag
c_code local build_ref_295
	ret_reg &ref[295]
	call_c   Dyam_Create_Atom("tag")
	move_ret ref[295]
	c_ret

long local pool_fun77[5]=[4,build_ref_166,build_ref_168,build_ref_201,build_ref_202]

pl_code local fun77
	call_c   Dyam_Pool(pool_fun77)
	call_c   Dyam_Unify_Item(&ref[166])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     &ref[168], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(16))
	pl_call  pred_tree_info_2()
	move     V(18), R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(19), R(4)
	move     S(5), R(5)
	pl_call  pred_tag_numbervars_3()
	move     V(20), R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(21), R(4)
	move     S(5), R(5)
	pl_call  pred_tag_numbervars_3()
	move     &ref[168], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(1))
	move     V(22), R(4)
	move     S(5), R(5)
	pl_call  pred_tag_numbervars_3()
	call_c   DYAM_evpred_functor(V(8),V(23),V(24))
	fail_ret
	move     &ref[201], R(0)
	move     S(5), R(1)
	move     I(0), R(2)
	move     0, R(3)
	move     &ref[202], R(4)
	move     S(5), R(5)
	call_c   Dyam_Reg_Deallocate(3)
fun71:
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Reg_Bind(0,V(1))
	call_c   Dyam_Reg_Bind(2,V(3))
	call_c   Dyam_Reg_Bind(4,V(2))
	call_c   Dyam_Choice(fun70)
	pl_call  fun28(&seed[29],1)
	pl_fail


;; TERM 167: '*GUARD*'(tagguide_compile_tree(_B, tag_node{id=> _C, label=> _D, children=> _E, kind=> _F, adj=> _G, spine=> _H, top=> _I, bot=> _J, token=> _K, adjleft=> _L, adjright=> _M, adjwrap=> _N}, _O, _P, _Q, _R)) :> []
c_code local build_ref_167
	ret_reg &ref[167]
	call_c   build_ref_166()
	call_c   Dyam_Create_Binary(I(9),&ref[166],I(0))
	move_ret ref[167]
	c_ret

c_code local build_seed_29
	ret_reg &seed[29]
	call_c   build_ref_33()
	call_c   build_ref_170()
	call_c   Dyam_Seed_Start(&ref[33],&ref[170],&ref[170],fun3,1)
	call_c   build_ref_172()
	call_c   Dyam_Seed_Add_Comp(&ref[172],&ref[170],0)
	call_c   Dyam_Seed_End()
	move_ret seed[29]
	c_ret

;; TERM 172: '*FIRST*'(register_predicate(_C)) :> '$$HOLE$$'
c_code local build_ref_172
	ret_reg &ref[172]
	call_c   build_ref_171()
	call_c   Dyam_Create_Binary(I(9),&ref[171],I(7))
	move_ret ref[172]
	c_ret

;; TERM 171: '*FIRST*'(register_predicate(_C))
c_code local build_ref_171
	ret_reg &ref[171]
	call_c   build_ref_29()
	call_c   build_ref_169()
	call_c   Dyam_Create_Unary(&ref[29],&ref[169])
	move_ret ref[171]
	c_ret

;; TERM 169: register_predicate(_C)
c_code local build_ref_169
	ret_reg &ref[169]
	call_c   build_ref_203()
	call_c   Dyam_Create_Unary(&ref[203],V(2))
	move_ret ref[169]
	c_ret

;; TERM 170: '*CITEM*'(register_predicate(_C), register_predicate(_C))
c_code local build_ref_170
	ret_reg &ref[170]
	call_c   build_ref_33()
	call_c   build_ref_169()
	call_c   Dyam_Create_Binary(&ref[33],&ref[169],&ref[169])
	move_ret ref[170]
	c_ret

c_code local build_seed_30
	ret_reg &seed[30]
	call_c   build_ref_90()
	call_c   build_ref_175()
	call_c   Dyam_Seed_Start(&ref[90],&ref[175],I(0),fun12,1)
	call_c   build_ref_173()
	call_c   Dyam_Seed_Add_Comp(&ref[173],fun69,0)
	call_c   Dyam_Seed_End()
	move_ret seed[30]
	c_ret

;; TERM 173: '*RITEM*'(register_predicate(_C), voidret)
c_code local build_ref_173
	ret_reg &ref[173]
	call_c   build_ref_0()
	call_c   build_ref_169()
	call_c   build_ref_2()
	call_c   Dyam_Create_Binary(&ref[0],&ref[169],&ref[2])
	move_ret ref[173]
	c_ret

pl_code local fun69
	call_c   build_ref_173()
	call_c   Dyam_Unify_Item(&ref[173])
	fail_ret
	pl_jump  Follow_Cont(V(1))

;; TERM 175: '*RITEM*'(register_predicate(_C), voidret) :> tagguide_compile(3, '$TUPPLE'(35047639353724))
c_code local build_ref_175
	ret_reg &ref[175]
	call_c   build_ref_173()
	call_c   build_ref_174()
	call_c   Dyam_Create_Binary(I(9),&ref[173],&ref[174])
	move_ret ref[175]
	c_ret

;; TERM 174: tagguide_compile(3, '$TUPPLE'(35047639353724))
c_code local build_ref_174
	ret_reg &ref[174]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,134217728)
	move_ret R(0)
	call_c   build_ref_296()
	call_c   Dyam_Create_Binary(&ref[296],N(3),R(0))
	move_ret ref[174]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 296: tagguide_compile
c_code local build_ref_296
	ret_reg &ref[296]
	call_c   Dyam_Create_Atom("tagguide_compile")
	move_ret ref[296]
	c_ret

;; TERM 90: '*CURNEXT*'
c_code local build_ref_90
	ret_reg &ref[90]
	call_c   Dyam_Create_Atom("*CURNEXT*")
	move_ret ref[90]
	c_ret

c_code local build_seed_27
	ret_reg &seed[27]
	call_c   build_ref_33()
	call_c   build_ref_150()
	call_c   Dyam_Seed_Start(&ref[33],&ref[150],&ref[150],fun3,1)
	call_c   build_ref_152()
	call_c   Dyam_Seed_Add_Comp(&ref[152],&ref[150],0)
	call_c   Dyam_Seed_End()
	move_ret seed[27]
	c_ret

;; TERM 152: '*FIRST*'('call_tagguide_pred/2'(_C)) :> '$$HOLE$$'
c_code local build_ref_152
	ret_reg &ref[152]
	call_c   build_ref_151()
	call_c   Dyam_Create_Binary(I(9),&ref[151],I(7))
	move_ret ref[152]
	c_ret

;; TERM 151: '*FIRST*'('call_tagguide_pred/2'(_C))
c_code local build_ref_151
	ret_reg &ref[151]
	call_c   build_ref_29()
	call_c   build_ref_149()
	call_c   Dyam_Create_Unary(&ref[29],&ref[149])
	move_ret ref[151]
	c_ret

;; TERM 149: 'call_tagguide_pred/2'(_C)
c_code local build_ref_149
	ret_reg &ref[149]
	call_c   build_ref_210()
	call_c   Dyam_Create_Unary(&ref[210],V(2))
	move_ret ref[149]
	c_ret

;; TERM 150: '*CITEM*'('call_tagguide_pred/2'(_C), 'call_tagguide_pred/2'(_C))
c_code local build_ref_150
	ret_reg &ref[150]
	call_c   build_ref_33()
	call_c   build_ref_149()
	call_c   Dyam_Create_Binary(&ref[33],&ref[149],&ref[149])
	move_ret ref[150]
	c_ret

c_code local build_seed_28
	ret_reg &seed[28]
	call_c   build_ref_90()
	call_c   build_ref_155()
	call_c   Dyam_Seed_Start(&ref[90],&ref[155],I(0),fun12,1)
	call_c   build_ref_153()
	call_c   Dyam_Seed_Add_Comp(&ref[153],fun59,0)
	call_c   Dyam_Seed_End()
	move_ret seed[28]
	c_ret

;; TERM 153: '*RITEM*'('call_tagguide_pred/2'(_C), return(_D))
c_code local build_ref_153
	ret_reg &ref[153]
	call_c   build_ref_0()
	call_c   build_ref_149()
	call_c   build_ref_45()
	call_c   Dyam_Create_Binary(&ref[0],&ref[149],&ref[45])
	move_ret ref[153]
	c_ret

pl_code local fun59
	call_c   build_ref_153()
	call_c   Dyam_Unify_Item(&ref[153])
	fail_ret
	pl_jump  Follow_Cont(V(1))

;; TERM 155: '*RITEM*'('call_tagguide_pred/2'(_C), return(_D)) :> tagguide_compile(2, '$TUPPLE'(35047639353724))
c_code local build_ref_155
	ret_reg &ref[155]
	call_c   build_ref_153()
	call_c   build_ref_154()
	call_c   Dyam_Create_Binary(I(9),&ref[153],&ref[154])
	move_ret ref[155]
	c_ret

;; TERM 154: tagguide_compile(2, '$TUPPLE'(35047639353724))
c_code local build_ref_154
	ret_reg &ref[154]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,134217728)
	move_ret R(0)
	call_c   build_ref_296()
	call_c   Dyam_Create_Binary(&ref[296],N(2),R(0))
	move_ret ref[154]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_21
	ret_reg &seed[21]
	call_c   build_ref_33()
	call_c   build_ref_97()
	call_c   Dyam_Seed_Start(&ref[33],&ref[97],&ref[97],fun3,1)
	call_c   build_ref_99()
	call_c   Dyam_Seed_Add_Comp(&ref[99],&ref[97],0)
	call_c   Dyam_Seed_End()
	move_ret seed[21]
	c_ret

;; TERM 99: '*FIRST*'('call_tagguide_name/3'(_C, _D)) :> '$$HOLE$$'
c_code local build_ref_99
	ret_reg &ref[99]
	call_c   build_ref_98()
	call_c   Dyam_Create_Binary(I(9),&ref[98],I(7))
	move_ret ref[99]
	c_ret

;; TERM 98: '*FIRST*'('call_tagguide_name/3'(_C, _D))
c_code local build_ref_98
	ret_reg &ref[98]
	call_c   build_ref_29()
	call_c   build_ref_96()
	call_c   Dyam_Create_Unary(&ref[29],&ref[96])
	move_ret ref[98]
	c_ret

;; TERM 96: 'call_tagguide_name/3'(_C, _D)
c_code local build_ref_96
	ret_reg &ref[96]
	call_c   build_ref_206()
	call_c   Dyam_Create_Binary(&ref[206],V(2),V(3))
	move_ret ref[96]
	c_ret

;; TERM 97: '*CITEM*'('call_tagguide_name/3'(_C, _D), 'call_tagguide_name/3'(_C, _D))
c_code local build_ref_97
	ret_reg &ref[97]
	call_c   build_ref_33()
	call_c   build_ref_96()
	call_c   Dyam_Create_Binary(&ref[33],&ref[96],&ref[96])
	move_ret ref[97]
	c_ret

c_code local build_seed_22
	ret_reg &seed[22]
	call_c   build_ref_90()
	call_c   build_ref_103()
	call_c   Dyam_Seed_Start(&ref[90],&ref[103],I(0),fun12,1)
	call_c   build_ref_101()
	call_c   Dyam_Seed_Add_Comp(&ref[101],fun33,0)
	call_c   Dyam_Seed_End()
	move_ret seed[22]
	c_ret

;; TERM 101: '*RITEM*'('call_tagguide_name/3'(_C, _D), return(_E))
c_code local build_ref_101
	ret_reg &ref[101]
	call_c   build_ref_0()
	call_c   build_ref_96()
	call_c   build_ref_100()
	call_c   Dyam_Create_Binary(&ref[0],&ref[96],&ref[100])
	move_ret ref[101]
	c_ret

;; TERM 100: return(_E)
c_code local build_ref_100
	ret_reg &ref[100]
	call_c   build_ref_205()
	call_c   Dyam_Create_Unary(&ref[205],V(4))
	move_ret ref[100]
	c_ret

pl_code local fun33
	call_c   build_ref_101()
	call_c   Dyam_Unify_Item(&ref[101])
	fail_ret
	pl_jump  Follow_Cont(V(1))

;; TERM 103: '*RITEM*'('call_tagguide_name/3'(_C, _D), return(_E)) :> tagguide_compile(1, '$TUPPLE'(35047639353724))
c_code local build_ref_103
	ret_reg &ref[103]
	call_c   build_ref_101()
	call_c   build_ref_102()
	call_c   Dyam_Create_Binary(I(9),&ref[101],&ref[102])
	move_ret ref[103]
	c_ret

;; TERM 102: tagguide_compile(1, '$TUPPLE'(35047639353724))
c_code local build_ref_102
	ret_reg &ref[102]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,134217728)
	move_ret R(0)
	call_c   build_ref_296()
	call_c   Dyam_Create_Binary(&ref[296],N(1),R(0))
	move_ret ref[102]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_19
	ret_reg &seed[19]
	call_c   build_ref_33()
	call_c   build_ref_87()
	call_c   Dyam_Seed_Start(&ref[33],&ref[87],&ref[87],fun3,1)
	call_c   build_ref_89()
	call_c   Dyam_Seed_Add_Comp(&ref[89],&ref[87],0)
	call_c   Dyam_Seed_End()
	move_ret seed[19]
	c_ret

;; TERM 89: '*FIRST*'('call_normalize/2'(_C)) :> '$$HOLE$$'
c_code local build_ref_89
	ret_reg &ref[89]
	call_c   build_ref_88()
	call_c   Dyam_Create_Binary(I(9),&ref[88],I(7))
	move_ret ref[89]
	c_ret

;; TERM 88: '*FIRST*'('call_normalize/2'(_C))
c_code local build_ref_88
	ret_reg &ref[88]
	call_c   build_ref_29()
	call_c   build_ref_86()
	call_c   Dyam_Create_Unary(&ref[29],&ref[86])
	move_ret ref[88]
	c_ret

;; TERM 86: 'call_normalize/2'(_C)
c_code local build_ref_86
	ret_reg &ref[86]
	call_c   build_ref_208()
	call_c   Dyam_Create_Unary(&ref[208],V(2))
	move_ret ref[86]
	c_ret

;; TERM 87: '*CITEM*'('call_normalize/2'(_C), 'call_normalize/2'(_C))
c_code local build_ref_87
	ret_reg &ref[87]
	call_c   build_ref_33()
	call_c   build_ref_86()
	call_c   Dyam_Create_Binary(&ref[33],&ref[86],&ref[86])
	move_ret ref[87]
	c_ret

c_code local build_seed_20
	ret_reg &seed[20]
	call_c   build_ref_90()
	call_c   build_ref_93()
	call_c   Dyam_Seed_Start(&ref[90],&ref[93],I(0),fun12,1)
	call_c   build_ref_91()
	call_c   Dyam_Seed_Add_Comp(&ref[91],fun29,0)
	call_c   Dyam_Seed_End()
	move_ret seed[20]
	c_ret

;; TERM 91: '*RITEM*'('call_normalize/2'(_C), return(_D))
c_code local build_ref_91
	ret_reg &ref[91]
	call_c   build_ref_0()
	call_c   build_ref_86()
	call_c   build_ref_45()
	call_c   Dyam_Create_Binary(&ref[0],&ref[86],&ref[45])
	move_ret ref[91]
	c_ret

pl_code local fun29
	call_c   build_ref_91()
	call_c   Dyam_Unify_Item(&ref[91])
	fail_ret
	pl_jump  Follow_Cont(V(1))

;; TERM 93: '*RITEM*'('call_normalize/2'(_C), return(_D)) :> tagguide_compile(0, '$TUPPLE'(35047639353724))
c_code local build_ref_93
	ret_reg &ref[93]
	call_c   build_ref_91()
	call_c   build_ref_92()
	call_c   Dyam_Create_Binary(I(9),&ref[91],&ref[92])
	move_ret ref[93]
	c_ret

;; TERM 92: tagguide_compile(0, '$TUPPLE'(35047639353724))
c_code local build_ref_92
	ret_reg &ref[92]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,134217728)
	move_ret R(0)
	call_c   build_ref_296()
	call_c   Dyam_Create_Binary(&ref[296],N(0),R(0))
	move_ret ref[92]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_15
	ret_reg &seed[15]
	call_c   build_ref_16()
	call_c   build_ref_24()
	call_c   Dyam_Seed_Start(&ref[16],&ref[24],I(0),fun3,1)
	call_c   build_ref_25()
	call_c   Dyam_Seed_Add_Comp(&ref[25],&ref[24],0)
	call_c   Dyam_Seed_End()
	move_ret seed[15]
	c_ret

;; TERM 25: '*GUARD*'(tagguide_strip_handler(_B, _C, _D)) :> '$$HOLE$$'
c_code local build_ref_25
	ret_reg &ref[25]
	call_c   build_ref_24()
	call_c   Dyam_Create_Binary(I(9),&ref[24],I(7))
	move_ret ref[25]
	c_ret

;; TERM 24: '*GUARD*'(tagguide_strip_handler(_B, _C, _D))
c_code local build_ref_24
	ret_reg &ref[24]
	call_c   build_ref_16()
	call_c   build_ref_23()
	call_c   Dyam_Create_Unary(&ref[16],&ref[23])
	move_ret ref[24]
	c_ret

;; TERM 23: tagguide_strip_handler(_B, _C, _D)
c_code local build_ref_23
	ret_reg &ref[23]
	call_c   build_ref_212()
	call_c   Dyam_Term_Start(&ref[212],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[23]
	c_ret

pl_code local fun8
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Object(R(0))
	call_c   Dyam_Tabule()
	pl_ret

pl_code local fun70
	call_c   Dyam_Remove_Choice()
	pl_call  fun30(&seed[30],2,V(3))
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun60
	call_c   Dyam_Remove_Choice()
	pl_call  fun30(&seed[28],2,V(4))
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun34
	call_c   Dyam_Remove_Choice()
	pl_call  fun30(&seed[22],2,V(5))
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun27
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Object(R(0))
	call_c   Dyam_Tabule()
	pl_jump  Schedule_Prolog()

pl_code local fun28
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Choice(fun27)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Subsume(R(0))
	fail_ret
	call_c   Dyam_Cut()
	pl_ret

pl_code local fun30
	call_c   Dyam_Backptr_Trace(R(1),R(2))
	call_c   Dyam_Light_Object(R(0))
	pl_jump  Schedule_Prolog()

pl_code local fun31
	call_c   Dyam_Remove_Choice()
	pl_call  fun30(&seed[20],2,V(4))
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun16
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Reg_Load_Ptr(2,V(3))
	fail_ret
	call_c   Dyam_Reg_Load(4,V(2))
	call_c   DyALog_Mutable_Write(R(2),&R(4),0)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(3))
	fail_ret
	call_c   DYAM_evpred_assert_1(&ref[48])
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun6
	call_c   Dyam_Remove_Choice()
	pl_fail


;;------------------ INIT POOL ------------

c_code local build_init_pool
	call_c   build_ref_1()
	call_c   build_ref_3()
	call_c   build_ref_7()
	call_c   build_ref_6()
	call_c   build_ref_11()
	call_c   build_ref_10()
	call_c   build_ref_14()
	call_c   build_ref_13()
	call_c   build_ref_48()
	call_c   build_seed_1()
	call_c   build_seed_0()
	call_c   build_seed_8()
	call_c   build_seed_14()
	call_c   build_seed_13()
	call_c   build_seed_9()
	call_c   build_seed_6()
	call_c   build_seed_10()
	call_c   build_seed_3()
	call_c   build_seed_7()
	call_c   build_seed_5()
	call_c   build_seed_12()
	call_c   build_seed_2()
	call_c   build_seed_11()
	call_c   build_seed_4()
	call_c   build_seed_29()
	call_c   build_seed_30()
	call_c   build_seed_27()
	call_c   build_seed_28()
	call_c   build_seed_21()
	call_c   build_seed_22()
	call_c   build_seed_19()
	call_c   build_seed_20()
	call_c   build_seed_15()
	c_ret

long local ref[297]
long local seed[34]

long local _initialization

c_code global initialization_dyalog_tagguide_5Fcompile
	call_c   Dyam_Check_And_Update_Initialization(_initialization)
	fail_c_ret
	call_c   initialization_dyalog_tag_5Fdyn()
	call_c   initialization_dyalog_tig()
	call_c   initialization_dyalog_reader()
	call_c   initialization_dyalog_maker()
	call_c   initialization_dyalog_tag_5Fmaker()
	call_c   initialization_dyalog_tag_5Fnormalize()
	call_c   initialization_dyalog_tag_5Fcompile()
	call_c   initialization_dyalog_format()
	call_c   build_init_pool()
	call_c   build_viewers()
	call_c   load()
	c_ret

