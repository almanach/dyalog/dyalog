/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2000, 2003, 2004, 2008, 2011 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  stdprolog.pl -- Handling Std Prolog predicates
 *
 * ----------------------------------------------------------------
 * Description
 *   Std Prolog predicates are what are closer from predicates in
 *   a standard Prolog system
 *
 *   They are a subclass of rec_prolog predicates and are introduced
 *   with directive :-std_prolog
 *
 *   No more than one clause per StdProlog predicate
 *
 * ----------------------------------------------------------------
 */

:-include 'header.pl'.

:-require 'oset.pl'.

stdprolog_install( '*STD-PROLOG-FIRST*'(F/N,Args,Cont) ) :-
	tupple(Args,Tupple),
	null_tupple(Void),
	duplicate_vars(Args,DupArgs),
	tupple(Cont,T_Cont),
	inter(T_Cont,Tupple,U1),
	union(U1,DupArgs,U2),
	unfold(Cont :> deallocate :> succeed,[],Tupple,Code_Cont,_),
%%	format( 'stdprolog ~w\n',[Code_Cont]),
	std_prolog_unif_args_alt(Args,0,Code,Code_Cont,Void,U2),
	%% noop(F/N) used to dstinguish functions with same code
	%% It may happen (rarely)
	label_code( noop(F/N) :> allocate :> allocate_layer :> Code, FunId),
	std_prolog_function(F,N,Fun),
	%%	format( '%% StdProlog ~w ~w ~w\n\n',[Fun,Id,Code] ),
	record( named_function(FunId,Fun,global) ),
	register_init_pool_fun(FunId),
	( recorded( dyalog_ender(F/N) ) ->
%	  '$interface'('DyALog_Register_Ender'(FunId:ptr),[return(none)]),
	  record( dyalog_ender(F/N,FunId) ),
	  true
	;
	  true
	)
	.

unfold( deallocate_layer, Env, _, deallocate_layer,Env ).

unfold( '*STD-PROLOG-CALL*'(F/N,Args), Env, Tupple, Code, Env ) :-
	std_prolog_function(F,N,Fun),
	std_prolog_load_args(Args,0,Code,call(Fun,[]) :> reg_reset(N),Tupple)
	.

std_prolog_unif_args([],_,Cont,Cont,_).
std_prolog_unif_args([A|X],N, XCode,Cont,Tupple) :-
	M is N+1,
	R is N*2,
	label_term(A,Label_A),
	( A==[] ->
	    Inst = reg_unif_nil(R)
	;   atomic(A) ->
	    Inst = reg_unif_cst(R,Label_A)
	;   (check_var(A), \+ var_in_tupple(A,Tupple)) ->
	    Inst = reg_bind(R,Label_A)
	;   
	    Inst = reg_unif(R,Label_A)
	),
%	tupple(A,T_A),
	extend_tupple(A,Tupple,New_Tupple),
	std_prolog_unif_args(X,M,Code,Cont,New_Tupple),
	( Code = reg_bind(S,Label_B) :> _Code,
	  Inst = reg_bind(R,Label_A),
	  S is R+2,
	  Label_B = '$VAR'(B_Index),
	  Label_A = '$VAR'(A_Index),
	  B_Index is A_Index+1 ->
	  ( R == 0 ->
	    ( A_Index == 1 ->
	      XCode = reg_multi_bind_zero_one(2) :> _Code
	    ;
	      XCode = reg_multi_bind_zero(A_Index,2) :> _Code
	    )
	  ;
	    XCode = reg_multi_bind(R,A_Index,2) :> _Code
	  )
	; Code = reg_multi_bind(S,B_Index,Multi) :> _Code,
	  Inst = reg_bind(R,Label_A),
	  S is R+2,
	  Label_A = '$VAR'(A_Index),
	  B_Index is A_Index+1 ->
	  NewMulti is Multi+1,
	  ( R == 0 ->
	    ( A_Index == 1 ->
	      XCode = reg_multi_bind_zero_one(NewMulti) :> _Code
	    ;
	      XCode = reg_multi_bind_zero(A_Index,NewMulti) :> _Code
	    )
	  ;
	    XCode = reg_multi_bind(R,A_Index,NewMulti) :> _Code
	  )
	;
	  XCode = Inst :> Code
	)
	.


std_prolog_unif_args_alt([],_,Cont,Cont,_,Useful).
std_prolog_unif_args_alt([A|X],N, XCode,Cont,Tupple,Useful) :-
	M is N+1,
	R is N*2,
	label_term(A,Label_A),
	( A==[] ->
	    Inst = reg_unif_nil(R)
	;   atomic(A) ->
	    Inst = reg_unif_cst(R,Label_A)
	; ( check_var(A), \+ var_in_tupple(A,Useful) ) ->
%%	  format('discard ~w as useless\n',[A]),
	    Inst = noop
	;   (check_var(A), \+ var_in_tupple(A,Tupple)) ->
	    Inst = reg_bind(R,Label_A)
	;   
	    Inst = reg_unif(R,Label_A)
	),
%	tupple(A,T_A),
	extend_tupple(A,Tupple,New_Tupple),
	std_prolog_unif_args_alt(X,M,Code,Cont,New_Tupple,Useful),
	( Code = reg_bind(S,Label_B) :> _Code,
	  Inst = reg_bind(R,Label_A),
	  S is R+2,
	  Label_B = '$VAR'(B_Index),
	  Label_A = '$VAR'(A_Index),
	  B_Index is A_Index+1 ->
	  ( R == 0 ->
	    ( A_Index == 1 ->
	      XCode = reg_multi_bind_zero_one(2) :> _Code
	    ;
	      XCode = reg_multi_bind_zero(A_Index,2) :> _Code
	    )
	  ;
	    XCode = reg_multi_bind(R,A_Index,2) :> _Code
	  )
	; Code = reg_multi_bind(S,B_Index,Multi) :> _Code,
	  Inst = reg_bind(R,Label_A),
	  S is R+2,
	  Label_A = '$VAR'(A_Index),
	  B_Index is A_Index+1 ->
	  NewMulti is Multi+1,
	  ( R == 0 ->
	    ( A_Index == 1 ->
	      XCode = reg_multi_bind_zero_one(NewMulti) :> _Code
	    ;
	      XCode = reg_multi_bind_zero(A_Index,NewMulti) :> _Code
	    )
	  ;
	    XCode = reg_multi_bind(R,A_Index,NewMulti) :> _Code
	  )
	;
	  XCode = Inst :> Code
	)
	.


std_prolog_load_args([],_,Cont,Cont,_).
std_prolog_load_args([A|X],N, Inst :> Code,Cont,Tupple) :-
%%	oset_tupple(Tupple,TT),
%%	format( 'Load args ~w ~w\n',[A,TT]),
	M is N+1,
	R is N*2,
	label_term(A,Label_A),
	( A==[] ->
	    Inst = reg_load_nil(R)
	;   atomic(A) ->
	    Inst=reg_load_cst(R,Label_A)
	;   var_in_tupple(A,Tupple) ->
	    %% Potentially bound variable -> Need to deref it
	  Inst=reg_load(R,Label_A)
	;   
	    %% Compound term or free variable -> no need to deref
	    Inst = reg_load_var(R,Label_A)
	),
%%	format( '\t~w\n',[Inst]),
	std_prolog_load_args(X,M,Code,Cont,Tupple)
	.


std_prolog_load_args_alt([],_,Cont,Cont,_,_).
std_prolog_load_args_alt([A|X],N, Inst :> Code,Cont,Tupple,Useful) :-
%%	oset_tupple(Tupple,TT),
%%	format( 'Load args ~w ~w\n',[A,TT]),
	M is N+1,
	R is N*2,
	label_term(A,Label_A),
	( A==[] ->
	    Inst = reg_load_nil(R)
	;   atomic(A) ->
	    Inst=reg_load_cst(R,Label_A)
	;   var_in_tupple(A,Tupple) ->
	  %% Potentially bound variable -> Need to deref it
	  Inst=reg_load(R,Label_A)
	; %%fail,
	  check_var(A),
	  \+ var_in_tupple(A,Useful) ->
	  %% Free variable not used after the call
	  %% we load a zero value
%	  format('useless var ~w\n',[A]),
	  Inst = reg_zero(R)
	;   
	    %% Compound term or free variable -> no need to deref
	    Inst = reg_load_var(R,Label_A)
	),
%%	format( '\t~w\n',[Inst]),
	std_prolog_load_args_alt(X,M,Code,Cont,Tupple,Useful)
	.


:-light_tabular std_prolog_function/3.

std_prolog_function(F,N,Fun) :-
	atom_module(F,Module,FF),
	c_module_name(Module,C_Module),
	(   FF == '$ft' ->
	    %% F is a feature functor
	    name_builder('pred_~w__ft_~w',[C_Module,N],Fun)
	;   
	    name_builder('pred_~w~w_~w',[C_Module,FF,N],Fun)
	)
	.

:-light_tabular c_module_name/2.

c_module_name(Module,C_Module) :-
	( Module == [] ->
	    C_Module=''
	;   atom_module(Module,Module2,Name) ->
	    c_module_name(Module2,C_Module2),
	    name_builder('~w~w__',[C_Module2,Name],C_Module)
	)
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Wrappers

wrapper_install( Pred, FunName ) :-
	(   recorded( '*WRAPPER*'(Pred,Args::[Closure|_Args],Cont) )
	xor internal_error('no tabular wrapper for ~w',[Pred]) ),
	\+ _Args = callret(_,_),
	tupple(Args,Tupple),
	null_tupple(Void),
	unfold(Cont :> deallocate_layer :> deallocate :> succeed,
	       Closure, Tupple, Code_Cont, _),
	std_prolog_unif_args(Args,0,Code1,Code_Cont,Void),
	Code = allocate :> allocate_layer :> Code1,
	label_code( Code , Fun ),
	register_init_pool_fun( Fun ),
	( wrapper_function_name(Pred,FunName) ->
	    record( named_function(Fun,FunName,local) )
	;
	    Fun = FunName
	),
%%	format( 'wrapper install ~w => ~w\n',[Fun,FunName]),
	true
	.


wrapper_install( Pred, FunName ) :-
	(   recorded( '*WRAPPER*'(Pred,[Closure|_Args],Cont) )
	xor internal_error('no tabular wrapper for ~w',[Pred]) ),
	_Args = callret(_CallArgs,_RetArgs),
	tupple(CallArgs::[Closure|_CallArgs],Tupple),
	null_tupple(Void),
	unfold(Cont :> deallocate_layer :> deallocate :> succeed,
	       Closure, Tupple, Code_Cont, _),
	std_prolog_unif_args(CallArgs,0,Code1,Code_Cont,Void),
	Code = allocate :> allocate_layer :> Code1,
	label_code( Code , Fun ),
	register_init_pool_fun( Fun ),
	( wrapper_function_name(Pred,FunName) ->
	    record( named_function(Fun,FunName,local) )
	;
	    Fun = FunName
	),
%%	format( 'wrapper install ~w => ~w\n',[Fun,FunName]),
	true
	.

unfold('*RETARGS*'(RetArgs),Env,T,Code,Env) :-
	label_term(Env,L_Env),
	length(RetArgs,N),
	M is N+1,
	std_prolog_load_args(RetArgs,1,Code,call('Follow_Cont',[L_Env]) :> reg_reset(M),T)
	.

unfold( load_args(Args,Cont), Env, T, Code, Out_Env ) :-
	extend_tupple(Env,T,XT),
	extend_tupple(Args,T,YT),
	unfold(Cont,Env,YT, CodeCont, Out_Env),
	std_prolog_load_args(Args,0,Code,CodeCont,XT)
	.

unfold( Wrapper::'*WRAPPER-CALL*'(Pred,Args,Right), Env, T, Code, Out_Env ) :-
%%	 format('unfold ~w\n\tin env ~w\n',[Wrapper,Env]),
	\+ Args = callret(_,_),
	wrapper_install(Pred,Fun),
	extend_tupple(Args,T,T2),
%	tupple(Args,TArgs),
%%	T2=T,			% unsafe trick: to be checked
	( Right == '*PROLOG-LAST*' ->
	  Closure = Env,
	  Out_Env = Env
	% ; Right =.. ['$contargs',TrueRight|ContArgs],
	%   extend_tupple(ContArgs,T2,T3),
	%   build_closure(TrueRight,Env,T3,TrueClosure,Out_Env),
	%   Closure =.. ['$contargs',TrueClosure|ContArgs]
	;
	  build_closure(Right,Env,T2,Closure,Out_Env)
	),
	duplicate_vars([Env,Closure|Args],Useful),
	length(Args,N),
	M is N+1,
	std_prolog_load_args_alt([Closure|Args],0,Code,call(Fun,[]) :> reg_reset(M) :> noop,T,Useful),

%	oset_tupple(T2,XT2),
%	format('\t (WRAPPER CALL ~w) =>out_env=~w\n\ttupple=~w\n\tcode=~w\n',[Wrapper,Out_Env,XT2,Code]),

	true
	.


unfold( Wrapper::'*WRAPPER-CALL-ALT*'(Pred,Args,Right), Env, T, Code, Out_Env ) :-
%%	 format('unfold ~w\n\tin env ~w\n',[Wrapper,Env]),
	\+ Args = callret(_,_),
	wrapper_install(Pred,Fun),
	extend_tupple(Args,T,T2),
%%	T2=T,			% unsafe trick: to be checked
	( Right == '*PROLOG-LAST*' ->
	  Closure = Env,
	  Out_Env = Env,
	  null_tupple(CT)
	
	% ; Right =.. ['$contargs',TrueRight|ContArgs],
	%   extend_tupple(ContArgs,T2,T3),
	%   build_closure(TrueRight,Env,T3,TrueClosure,Out_Env),
	%   Closure =.. ['$contargs',TrueClosure|ContArgs]
	
	;
	  build_closure(Right,Env,T2,Closure,Out_Env),
	  Closure = '$CLOSURE'(_,CT)
	),
	duplicate_vars([Env,Closure|Args],Useful),
	length(Args,N),
	M is N+1,
	std_prolog_load_args_alt([Closure|Args],0,Code,call(Fun,[]) :> reg_reset(M) :> noop,T,Useful),
%	std_prolog_load_args([Closure|Args],0,Code,call(Fun,[]) :> reg_reset(M) :> noop,T),
	
	oset_tupple(T2,XT2),
	oset_tupple(CT,XCT),
	label_term(Closure,Closure_L),
%%	format('\t (WRAPPER CALL ~w)\n\tenv=~w\n\t =>out_env=~w\n\ttupple=~w\n\tclosure=~w : ~w\n\tcode=~w\n\tT2=~w\n\targs=~w\n\n',[Wrapper,Env,Out_Env,XCT,Closure,Closure_L,Code,XT2,Args]),
	true
	.

unfold( Wrapper::'*WRAPPER-CALL-ALT*'(Pred,callret(CallArgs,RetArgs),Right), Env, T, Code, Out_Env ) :-
%%	 format('unfold ~w\n\tin env ~w\n',[Wrapper,Env]),
	wrapper_install(Pred,Fun),
	extend_tupple(CallArgs,T,T2),
%%	T2=T,			% unsafe trick: to be checked
	( Right == '*PROLOG-LAST*' ->
	  Closure = Env,
	  Out_Env = Env,
	  null_tupple(CT)
	;
	  build_args_closure(Right,callret(CallArgs,RetArgs),Env,T2,Closure,Out_Env),
	  Closure = '$CLOSURE'(_,CT)
	),
	duplicate_vars([Env,Closure|CallArgs],Useful),
	length(CallArgs,N),
	M is N+1,
	std_prolog_load_args_alt([Closure|CallArgs],0,Code,call(Fun,[]) :> reg_reset(M) :> noop,T,Useful),

%	oset_tupple(T2,XT2),
%	oset_tupple(CT,XCT),
	label_term(Closure,Closure_L),
%	format('\t (WRAPPER CALL ~w)\n\tenv=~w\n\t =>out_env=~w\n\ttupple=~w\n\tclosure=~w : ~w\n\tcode=~w\n\n',[Wrapper,Env,Out_Env,XCT,Closure,Closure_L,Code]),

	true
	.

unfold( Wrapper::'*WRAPPER-INNER-CALL*'(Pred,Args,Right), Env, T, Code, Out_Env ) :-
%%	 format('unfold ~w\n\tin env ~w\n',[Wrapper,Env]),
	\+ Args = callret(_,_),
	(   wrapper_function_name(Pred,FunName)
	xor update_counter(wrapper, WrapperId),
	    name_builder('_looping_wrapper~w',[WrapperId],FunName),
	    record( wrapper_function_name(Pred,FunName) )
	),
	extend_tupple(Args,T,T2),
	( Right == '*PROLOG-LAST*' ->
	    Closure = Env,
	    Out_Env=Env
	;
	    build_closure(Right,Env,T2,Closure,Out_Env)
	),
	duplicate_vars([Env,Closure|Args],Useful),
	length(Args,N),
	M is N+1,
%%	unfold('*CONT*'('*KLEENE-CALL*'(FunName,[Right|Args])),Out_Env,T2,Call_Code,_),
	std_prolog_load_args_alt([Closure|Args],0,Code,call(FunName,[]) :> reg_reset(M) :> noop,T,Useful),
%%	std_prolog_load_args([Closure|Args],0,Code,Call_Code :> reg_reset(M) :> noop,T),
%%	 format('\t=>~w\n\t~w\n',[Out_Env,Code]),
	true
	.

unfold( Wrapper::'*WRAPPER-INNER-CALL*'(Pred,callret(CallArgs,RetArgs),Right), Env, T, Code, Out_Env ) :-
%%	 format('unfold ~w\n\tin env ~w\n',[Wrapper,Env]),
	(   wrapper_function_name(Pred,FunName)
	xor update_counter(wrapper, WrapperId),
	    name_builder('_looping_wrapper~w',[WrapperId],FunName),
	    record( wrapper_function_name(Pred,FunName) )
	),
	extend_tupple(CallArgs,T,T2),
	( Right == '*PROLOG-LAST*' ->
	    Closure = Env,
	    Out_Env=Env
	;
	  build_closure(Right,Env,T2,Closure,Out_Env)
	),
	duplicate_vars([Env,Closure|CallArgs],Useful),
	length(RetArgs,N),
	M is N+1,
%%	unfold('*CONT*'('*KLEENE-CALL*'(FunName,[Right|Args])),Out_Env,T2,Call_Code,_),
	std_prolog_load_args_alt([Closure|CallArgs],0,Code,call(FunName,[]) :> reg_reset(M) :> noop,T,Useful),
%%	std_prolog_load_args([Closure|Args],0,Code,Call_Code :> reg_reset(M) :> noop,T),
%%	 format('\t=>~w\n\t~w\n',[Out_Env,Code]),
	true
	.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% register a closing function for DyALog

:-require 'reader.pl'.

directive(dyalog_ender(Pred),_,_,_) :-
	erase( dyalog_ender(_) ),
	record( dyalog_ender(Pred/0) )
	.



