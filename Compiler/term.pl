/* -*- mode:prolog; -*-
 ************************************************************
 * $Id$
 * Copyright (C) 1998, 2002, 2011 Eric de la Clergerie
 * ------------------------------------------------------------
 *
 *   Term -- Term Registering and Labeling
 *
 * ------------------------------------------------------------
 * Description
 *
 * ------------------------------------------------------------
 */

:-include 'header.pl'.

:-std_prolog register_local_args/4, register_local_deref_args/2.
%:-std_prolog register_local_list/2.
:-std_prolog register_local_tupple/4.
:-std_prolog label_local_term/2.

:-extensional internal_atomic/2.

:-xcompiler
	label_elem_term(T) :-
		'$interface'( 'Compiler_Label_Elementary_Term'(T:term), [] )
		.

label_term_list(L,LL) :-
	( L=[A|X] ->
	    LL=[LA|LX],
	    label_term(A,LA),
	    label_term_list(X,LX)
	;   LL=[]
	).

label_term(T,L) :-
%%	format('Label Term ~w\n',[T]),
	( (\+ ground(T)) -> internal_error( 'Term Labeling Var ~w', [T] )
	;   label_elem_term(T) -> L=T
%%	;   (T='$arg'(N), number(N)) -> L=T
	;   internal_atomic(T,L) -> true
	;   recorded( global_term(T,L) ) ->
%%	    format('Recorded ~w: ~w\n',[Id,T]),
	    true
	;
	    globalize_subterms(T),
	    L='$term'(Id,MT),
	    mutable(MT,T),
	    update_counter(term,Id),
	    record(global_term(T,L))
	)
%	, format('        -> ~w\n',[L])
	.

:-std_prolog globalize_subterms/1.

globalize_subterms(T) :-
	(   functor(T,F,N),
	    N>0,
	    globalize_pattern(F) ->
	    (	recorded( global_term(F,_) )
	    xor label_term(F,F_Id)),
	    every((   foreign_arg(T,_,Arg),
		      label_term(Arg,_)
		  ))
	;   
	    true
	)
	.

:-extensional globalize_pattern/1.

globalize_pattern(:>).
globalize_pattern('*CITEM*').
globalize_pattern('*RITEM*').
globalize_pattern('*FIRST*').
globalize_pattern('*PROLOG-ITEM*').
globalize_pattern('*PROLOG-FIRST*').
globalize_pattern('*GUARD*').
globalize_pattern('*CAI*').
globalize_pattern('*RAI*').
globalize_pattern('*CFI*').
globalize_pattern('*RFI*').
globalize_pattern('$CLOSURE').
globalize_pattern(*).
globalize_pattern(sa_env).
globalize_pattern(sa_escape).

:-std_prolog term_lock/1.

term_lock(L) :-
	recorded( term_handling(M) ),
	mutable_read(M,L)
	.

label_local_term(T, L ) :-
%	format('Term ~w\n',[T]),
	( label_elem_term(T) -> L=T
%	;   (T='$arg'(N), number(N)) -> L=T
	;   internal_atomic(T,L) -> true
	;   T='$TUPPLE'(N),N==0 -> L='$internal'(0)
	;   recorded( global_term(T,L) ), \+ term_lock(L) -> true
	;   recorded( O::lterm(T,L,LevelM,_,_) ) ->
	    mutable_read(LevelM,Level),
%	    format(';; Recorded ~w: Level=~w\n',[T,Level]),
	    ( Level == [] ->
		local_register_again(O)
	    ;
		true
	    )
	;   functor(T,'$LOOP',3) ->
	    T =.. [_,_,TT,_],
	    %%	    format('\tcrossing loop ~w\n',[TT]),
	    label_local_term(TT,L)
	;   (T='$VAR'('$VAR'(N),Args), number(N), recorded( in_term_loop(N) ) ) ->
	    %% nested variable in a loop
	    %%	    format('\tin loop labeling ~w\n',[N]),
	    L='$VAR'(N)
	;   check_tupple(T) -> label_local_tupple(T,L)
	;   check_fun_tupple(T) -> label_local_fun_tupple(T,L)
	;   atomic(T), \+ recorded( global_term(T,_) ) -> label_term(T,L) % globalize symbols !
	;   
	    ( atomic(T) ->
		B= '$smb'(T),
		atom_module(T,Module,_),
		( Module == [] ->
		    Live=[]
		;
		    label_local_term(Module,Module_Ref),
		    Live=[Module_Ref|Live1],
		    ( recorded(features(Module,Features)) ->
			label_local_term(Features,Features_Ref),
			Live1=[Features_Ref]
		    ;	
			Live1 = []
		    )
		)
	    ;	atom_chars(S,T) ->
		B = '$charlist'(S),
		Live=[]
	    ;	T = [_|_] ->
		register_local_list(T,B,Live)
	    ;	( T='$VAR'('$VAR'(N),['$LOOP',Arg]), number(N) ) -> % For loop terms
		record( in_term_loop(N) ),
		%% format('Loop labeling ~w: ~w\n',[N,Arg]),
		Live1=[FB|BB],
		Live=[FB,'$VAR'(N)|BB],
		B= '$term'(3),
		register_local_deref_args(['$LOOP',Arg,0],Live1),
		erase( in_term_loop(N) )
	    ;	( T='$VAR'('$VAR'(N),Args), number(N) ) -> % For deref terms
		%% WARNING: bug when using T='$VAR'(V,Args), check_var(T)
		%% Do not know why !
		length(Args,Arity),
		Live1=[FB|BB],
		Live=[FB,'$VAR'(N)|BB],
		B= '$term'(Arity),
		register_local_deref_args(Args,Live1)
	    ;	( T='$CLOSURE'( F, Env ), F='$fun'(N,_,_), number(N) ) ->
		%% Closure
		label_local_term(Env,L_Env),
		Live = [L_Env],
		B='$CLOSURE'(F)
	    ;	functor(T,Functor,Arity),
		Live=[F|BB],
		( Arity == 1 ->
		    B = '$unary'
		;   Arity == 2 ->
		    B = '$binary'
		;
		    B = '$term'(Arity)
		),
		label_local_term(Functor,F),
		register_local_args(0,T,Arity,BB)
	    ),
	    local_register(T,B,L,Live),
	    true
	)
	.

register_local_deref_args(LX,LY) :-
        (   LX=[A|X] ->
            LY=[L|Y],
            label_local_term(A,L),
            register_local_deref_args(X,Y)
        ;
            LY=[]
        )
        .

register_local_args(I,T,Arity,LY) :-
	(   I < Arity ->
	    J is I+1,
	    arg(J,T,A),
	    LY=[L|Y],
	    label_local_term(A,L),
	    register_local_args(J,T,Arity,Y)
	;   
	    LY=[]
	)
	.


:-xcompiler			% Inlining
register_local_list(A,Inst,Live) :-
	( A = ['$VAR'(N),'$VAR'(M)|Y], M is N+1 ->
	    Inst = '$tupple'(N,_),
	    register_local_tupple(Y,M,Inst,Live)
	;   A = [X|Y],
	    Inst = '$list',
	    Live=[LX,LY],
	    label_local_term(X,LX),
	    label_local_term(Y,LY)
	)
	.


register_local_tupple(A,N,Inst,Live) :-
	( A=['$VAR'(M)|Y], M is N+1 ->
	    register_local_tupple(Y,M,Inst,Live)
	;   
	    label_local_term(A,LA),
	    Live=[LA],
	    Inst = '$tupple'(_,N)
	)
	.

:-std_prolog label_local_fun_tupple/2.

label_local_fun_tupple('$FUNTUPPLE'(F,'$TUPPLE'(S)),L) :-
%	format('labelling fun tupple ~w ~w\n',[F,S]),
	( S=0 -> label_local_term(F,L)
	; label_local_term(F,L1),
%	  format('here l1=~w\n',[L1]),
	  oset_list_alt(S,List),
%	  label_local_alt_tupple(List,L2),
%	  format('here l2=~w\n',[L2]),
	  Live=L1,
	  Inst='$fun_tupple'(List),
	  local_register('$$FUNTUPPLE'(F,List),Inst,L,Live)
	),
%	format('=>got ~w\n',[L]),
	true
	.

:-std_prolog label_local_tupple/2.

:-std_prolog oset_list/2. %% From oset.pl
:-std_prolog oset_list_alt/2. %% From oset.pl

label_local_tupple(T::'$TUPPLE'(S),L) :-
	( S=0 -> internal_atomic([],L)
	;   oset_list_alt(S,List),
	  % label_local_alt_tupple(List,L)
	  Inst = '$xtupple'(List),
	  local_register(T,Inst,L,[])
	)
	.

:-std_prolog label_local_alt_tupple/2.

label_local_alt_tupple(List,Label) :-
	( List = [] ->
	    internal_atomic([],Label)
	;   recorded( O::lterm('$$TUPPLE'(List),Label,LevelM,Inst,Live) ) ->
	    mutable_read(LevelM,Level),
	    ( Level == [] ->
		local_register_again(O)
	    ;
		true
	    )
	;   List=[Base,Mask|List1],
	    label_local_alt_tupple(List1,Label1),
	    Inst = '$alt_tupple'(Base,Mask),
	    Live = [Label1],
	    local_register('$$TUPPLE'(List),Inst,Label,Live)
	)
	.

:-std_prolog local_register/4.

local_register(T,Inst,O::'$lterm'(Level,Reg,AM),Live) :-
	mutable(Level,[]),
	mutable(Reg,[]),
	mutable(IdM,[]),
	mutable(AM,[]),
	add_ordered_table(lterm,A::lterm(T,O,IdM,Inst,Live),Id),
	mutable(IdM,Id),
	mark_live(Live,Id),
	mutable(Level,Id),
	mutable(AM,A)
	.

:-std_prolog local_register_again/1.

local_register_again( O::lterm(T,'$lterm'(Level,Reg,OM),IdM,_,Live) ) :-
	mutable(Reg,[]),
	(   domain(Arg::'$lterm'(_,_,OOM),Live),
	    mutable_read(OOM,OO::lterm(TT,_,XM,_,_)),
	    mutable_read(XM,[]),
	    local_register_again(OO),
	    fail
	;
	    true
	),
	add_ordered_table(lterm,O,Id),
	mutable(IdM,Id),
	mark_live(Live,Id),
	mutable(Level,Id)
	.



%%:-std_prolog mark_live/2.

:-xcompiler			% Inlining
mark_live(L,Id) :-
	(   domain('$lterm'(UL,_,_),L),
	    mutable(UL,Id),
	    fail
	;
	    true
	)
	.







