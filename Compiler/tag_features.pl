/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2003, 2004, 2007, 2008, 2009, 2011, 2016 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  tag_features.pl -- Optimizing TAG Non-terminal features
 *
 * ----------------------------------------------------------------
 * Description
 *   Abstract interpretation to optimize top and bottom features on
 *   TAG non-terminals
 *
 *   A tag feature constraints C=(T,B) for a non terminal N
 *   should satisfy C^2=C
 *   In practice, T=N(X1,..,Xn) B=N(Y1,..,Yn)
 *   where
 *          Xi and Yj are variables
 *          Xi=Xj => i=j
 *          Yi=Yj => i=j
 *          Xi=Yj => i=j
 *
 *  We try to find the most instanciated feature constraints
 *  such that
 *      no aux tree, possibly with internal adjoinings using
 *      the set of constraints, has Top(Root) and Bot(Foot)
 *      not satisfying the attached constraint
 *
 *  To compute the set of contraints
 *       - we compute an approximation of the aux trees as skels
 *       providing the existing relationship between variables
 *       and indicating potential places of adjoining
 *       - we start with the most possible instanciated constraints
 *       - we check if there is a violation when applying on skels
 *       - if not, we stop
 *       - Otherwise, we generalize the constraints
 *
 *  This algorithm should work for standard trees, but may not work
 *  when some variable relationships may not be captured in skels
 *  In particular, if we have relationships stating that
 *  Top(N) doesn't unify with Bot(N)
 *
 *  Initial most general constraints may be provided through
 *  directives :-tag_features
 *  It is an error if the resulting most instantiated constraints
 *  violates the initial most general constraints.
 *
 * ----------------------------------------------------------------
 */

:-include 'header.pl'.
:-require 'reader.pl'.
:-require 'tag_normalize.pl'.

:-extensional tag_nt/2.
:-extensional tag_features/3.

analyze_at_clause( T::tag_tree{ tree => auxtree(_)  } ) :-
	recorded( compiler_analyzer(tag_features) ),
	normalize(T,L)
	.

emit_at_pda :-
        recorded( compiler_analyzer(tag_features) ),

        current_output(Old_Stream),
        ( recorded( output_file(File) ) ->
            absolute_file_name(File,AF),
            open(AF,write,Stream),
            set_output(Stream)
        ;
            true
        ),
	
	tag_features_emit_header,

	%% install most specifics feature constraints
	every(( recorded(tag_features(NT,Top,Bot)),
		Top = Bot,
		functor(Top,_NT,N),
		record_without_doublon(tag_nt(NT,_NT/N)),
		record( tag_features_tmp(NT,Top,Bot) )
	      )),

	%% build propagation skels within aux trees
	every(('$answers'(normalize(T::tag_tree{name=>Name,tree=>auxtree(_)},
				    NT)),
	       tag_features_skel(Name,NT,Skel,N,Root,Foot,Mode),
	       format('%% Skel ~w: N=~w Root=~w Foot=~w Skel=~w\n',
	       	      [Name,N,Root,Foot,Skel]),
	       true
	      )),

        %% relax feature constraints until reaching a fixpoint
	tag_features_fixpoint,

	every(( tag_features_tmp(N,Top,Bot),
		'$interface'( 'DyALog_Assign_Display_Info'([Top,Bot]:term),
			      [return(none)]),
		( recorded( tag_features(N,_Top,_Bot) ) ->
		  tag_features_subsumes(_Top,_Bot,Top,Bot)
		;
		  true
		),
		format(':-tag_features(~k,~k,~k).\n',[N,Top,Bot])
	      )),

	format('\n\n%% Feature tag mode\n\n',[]),
	every(( tag_features_mode_tmp(N,Mode,Top,Bot),
		tag_features_tmp(N,_Top,_Bot),
		tag_features_subsumes(_Top,_Bot,Top,Bot),
		'$interface'( 'DyALog_Assign_Display_Info'([Top,Bot]:term),
			      [return(none)]),
		format(':-tag_features_mode(~k,~k,~k,~k).\n',[N,Mode,Top,Bot])
	      )),
	
	nl,
        set_output(Old_Stream) 
	.

:-std_prolog tag_features_emit_header.

tag_features_emit_header :-
        date_time(Year,Month,Day,Hour,Min,Sec),
        compiler_info( compiler_info{ name=>Name, version=>Version }),
        format( '%% Compiler Analyzer tag_features: ~w ~w\n', [Name,Version]),
        format( '%% Date    : ~w ~w ~w at ~w:~w:~w\n',[Month,Day,Year,Hour,Min,Sec]),
        every(  ( recorded( main_file(File,_) )
                , File \== '-'
                , absolute_file_name(File,F)
                , format('%% File ~q\n',[F])
                )
             ),
        ( recorded( main_file('-',_) ) -> format(';; File stdin\n',[]) ; true ),
        nl
        .
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% TAG_feature analyzer main loop extension

%% tag_features analyze run on pgm (no need for futher compiling)
check_analyzer_mode(tag_features) :-
        record( stop_compilation_at(pgm) )
        .

:-light_tabular tag_features_skel/7.
:-mode(tag_features_skel/7,+(+,+,-,-,-,-,-)).

%% T is a normalized aux tree
tag_features_skel(Name,T,Skel,NT,Root,Foot,Mode) :-
	( T = Node::tag_node{ label=> NT, top => Root }
	; T = guard{ goal => Node }
	),
	( recorded( tig(Name,Mode) ) xor Mode = wrap ),
	functor(Root,_NT,N),
	record_without_doublon(tag_nt(NT,_NT/N)),
	( tag_features_tmp(NT,_,_)
	xor functor(Root,_NT,N),
	  functor(_Top,_NT,N),
	  record( tag_features_tmp(NT,_Top,_Top))
	),
	( tag_features_mode_tmp(NT,Mode,_,_)
	xor functor(Root,_NT,N),
	  functor(_Top,_NT,N),
	  record( tag_features_mode_tmp(NT,Mode,_Top,_Top))
	),
	tag_features_skel_node(Node,[],Skel,Foot),
	( var(Foot) ->
	  error('Foot node not reached in ~w\n\ttree=~w',[Name,T])
	;
	  true
	)
	.

:-std_prolog tag_features_skel_node/4.

tag_features_skel_node(T,In,Out,Foot) :-
	(T = guard{ goal => T1::tag_node{ spine => Spine }, minus => Minus } ->
	  ( (Spine = yes xor Minus == fail) ->
	    tag_features_skel_node(T1,In,Out,Foot)
	  ;
	    In=Out
	  )
	; T = guard{ goal => T1, minus => fail } ->
	 %% rare case where T1 is not a node
	 tag_features_skel_node(T1,In,Out,Foot)
	;  T=[] ->
	  In=Out
	;  T=[K|L] ->
	  tag_features_skel_node(K,In,Middle,Foot),
	  tag_features_skel_node(L,Middle,Out,Foot)
	;  T=(K,L) ->
	  tag_features_skel_node(K,In,Middle,Foot),
	  tag_features_skel_node(L,Middle,Out,Foot)
	; T=(K##L) ->
	  tag_features_skel_node(K,In,Middle,Foot),
	  tag_features_skel_node(L,Middle,Out,Foot)
	; T=tag_node{ top => Top,
		      bot => Bot,
		      label => NT,
		      kind => Kind,
		      children => Children,
		      adj => Adj,
		      adjleft => AdjLeft,
		      adjright => AdjRight,
		      adjwrap => AdjWrap
		    },
	  \+ domain(Kind,[scan,escape]) ->
	  (Kind = foot -> Foot = Bot ; true ),
	  (Adj = no ->
	   tag_features_skel_node(Children,In,Out,Foot)
	  ;
	   ( tag_features_tmp(NT,_,_)
	   xor functor(Top,_NT,N),
	     functor(_Top,_NT,N),
	     record( tag_features_tmp(NT,_Top,_Top))
	   ),
	   ( recorded( adjkind(NT,left) ),
	     AdjLeft = possible_but_not_required_adj[] ->
	     ( tag_features_mode_tmp(NT,left,_,_)
	     xor functor(Top,_NT,N),
	       functor(_Top,_NT,N),
	       record( tag_features_mode_tmp(NT,left,_Top,_Top))
	     ),
	     functor(Bot,_NT,N),
	     functor(BotLeft,_NT,N),
	     In1 = [[left,NT,Top,BotLeft]|In]
	   ;
	     In1 = In,
	     BotLeft = Top
	   ),
	   ( ( recorded( adjkind(NT,wrap) )
	     xor (\+ recorded( adjkind(NT,left) ),
		  \+ recorded( adjkind(NT,right))
		 ))
	    %% AdjWrap = possible_but_not_required_adj[]
	    ->
	     ( tag_features_mode_tmp(NT,wrap,_,_)
	     xor functor(Top,_NT,N),
	       functor(_Top,_NT,N),
	       record( tag_features_mode_tmp(NT,wrap,_Top,_Top))
	     ),
	     functor(Bot,_NT,N),
	     functor(BotWrap,_NT,N),
	     In2 = [[wrap,NT,BotLeft,BotWrap]|In1]
	   ;
	     In2 = In1,
	     BotLeft=BotWrap
	   ),
	   ( recorded( adjkind(NT,right) ),
	     AdjRight = possible_but_not_required_adj[] ->
	     ( tag_features_mode_tmp(NT,right,_,_)
	     xor functor(Top,_NT,N),
	       functor(_Top,_NT,N),
	       record( tag_features_mode_tmp(NT,right,_Top,_Top))
	     ),
	     In3 = [[right,NT,BotWrap,Bot]|In2]
	   ;
	     BotWrap=Bot,
	     In3 = In2
	   ),
	   tag_features_skel_node(Children,[[NT,Top,Bot]|In3],Out,Foot)
	  )
	; % Other cases: alternatives, kleene, ...
	  % they should not cover the foot node
	  In=Out
	)
	.


:-std_prolog tag_features_fixpoint/0.

%% Fixpoint until convergence of tag_features constraints on each non-terminal

:-extensional tag_features_new_round.

tag_features_fixpoint :-
	format('%% New round\n',[]),
	every(( '$answers'( tag_features_skel(Name,_,Skel,NT,Root,Foot,Mode) ),
		tag_features_apply(NT,Skel,Root,Foot),
		%% format('%%\t\tapply name=~w ~w ~w ~w\n',[Name,NT,Root,Foot]),
		tag_features_aggregate(NT,Root,Foot),
		tag_features_mode_aggregate(NT,Mode,Root,Foot),
		%% format('%%\t\taggregate ~w ~w ~w\n',[NT,Root,Foot]),
		true
	      )),
	%%	format('%%\t updating\n',[]),
	every(( tag_nt(NT,_),
		tag_features_current(NT,T2,B2),
		erase( tag_features_current(NT,_,_) ),
		tag_features_tmp(NT,T1,B1),
		( tag_features_subsumes(T2,B2,T1,B1) ->
		  %% format('%% Need for new round for ~w: ~w ~w\n',[NT,T2,B2]),
		  %% Need to generalize current constraints
		  erase( tag_features_tmp(NT,_,_) ),
		  record( tag_features_tmp(NT,T2,B2) ),
		  record_without_doublon( tag_features_new_round )
		;
		  true
		)
	      )),

	every(( tag_nt(NT,_),
		tag_features_mode_current(NT,Mode,T2,B2),
		erase( tag_features_mode_current(NT,Mode,_,_) ),
		tag_features_mode_tmp(NT,Mode,T1,B1),
		( tag_features_subsumes(T2,B2,T1,B1) ->
		  %% format('%% Need for new round for ~w ~w: ~w ~w\n',[NT,Mode,T2,B2]),
		  %% Need to generalize current constraints
		  erase( tag_features_mode_tmp(NT,Mode,_,_) ),
		  record( tag_features_mode_tmp(NT,Mode,T2,B2) ),
		  record_without_doublon( tag_features_new_round )
		;
		  true
		)
	      )),


	( \+ tag_features_new_round
	xor
	erase( tag_features_new_round ),
	  tag_features_fixpoint
	)
	.

:-std_prolog tag_features_apply/4,
	tag_features_apply_aux/1.

:-extensional tag_features_tmp/3.
:-extensional tag_features_current/3.

:-extensional tag_features_mode_tmp/4.
:-extensional tag_features_mode_current/4.

tag_features_apply(NT,Skel,Root,Foot) :-
	tag_features_apply_aux(Skel)
	.

tag_features_apply_aux(Skel) :-
	( Skel = [[NT,Top,Bot]|Skel2] ->
	  %% we try to use the current most instantiated constraints
	  %% if not possible, => we have to generalize for next round
	  ( tag_features_tmp(NT,Top,Bot)
	  xor tag_features_aggregate(NT,Top,Bot)
	  ),
	  tag_features_apply_aux(Skel2)
	; Skel = [[Mode,NT,Top,Bot]|Skel2] ->
	  ( tag_features_mode_tmp(NT,Mode,Top,Bot)
	  xor tag_features_mode_aggregate(NT,Mode,Top,Bot)
	  ),
	  tag_features_apply_aux(Skel2)
	;
	  true
	)
	.

:-std_prolog tag_features_aggregate/3.

tag_features_aggregate(NT,Top,Bot) :-
	( tag_features_current(NT,T1,B1)
	xor
	T1=B1,
	  T1=Top
	),
%%	format('%% Combine ~w ~w ~w ~w\n',[T1,B1,Top,Bot]),
	tag_features_combine(T1,B1,Top,Bot,T3,B3),
/*
        ( tag_features_tmp(NT,T3,B3)
	xor tag_features(NT,T3,B3)
	xor true
	),
*/
%%	format( 'new current ~w top=~w bot=~w\n',[NT,T3,B3]),
	
	erase( tag_features_current(NT,_,_) ),
	record( tag_features_current(NT,T3,B3) )
	.

:-std_prolog tag_features_mode_aggregate/4.

tag_features_mode_aggregate(NT,Mode,Top,Bot) :-
	( tag_features_mode_current(NT,Mode,T1,B1)
	xor
	T1=B1,
	  T1=Top
	),
%%	format('%% Combine ~w ~w ~w ~w\n',[T1,B1,Top,Bot]),
	tag_features_combine(T1,B1,Top,Bot,T3,B3),
/*
        ( tag_features_tmp(NT,T3,B3)
	xor tag_features(NT,T3,B3)
	xor true
	),
*/
	erase( tag_features_mode_current(NT,Mode,_,_) ),
	record( tag_features_mode_current(NT,Mode,T3,B3) )
	.



:-std_prolog
	tag_features_combine/6
	.

:-rec_prolog
	tag_features_combine_aux/6
	.

tag_features_combine(Top1,Bot1,
		     Top2,Bot2,
		     Top3,Bot3
		    ) :-
	Top1 =.. [NT|TA1],
	Bot1 =.. [NT|BA1],
	Top2 =.. [NT|TA2],
	Bot2 =.. [NT|BA2],
	tag_features_combine_aux(TA1,BA1,TA2,BA2,TA3,BA3),
	Top3 =.. [NT|TA3],
	Bot3 =.. [NT|BA3]
	.

tag_features_combine_aux([],[],[],[],[],[]).
tag_features_combine_aux([T1|TA1],[B1|BA1],
			 [T2|TA2],[B2|BA2],
			 [T3|TA3],[B3|BA3]
			) :-
	( T1 == B1,
	  T2 == B2 ->
	  T3 = B3
	;
	  true
	),
	tag_features_combine_aux(TA1,BA1,TA2,BA2,TA3,BA3)
	.

:-std_prolog tag_features_subsumes/4.

tag_features_subsumes(T1,B1,T2,B2) :-
	%%	format('%% Test feat subsumes: ~w ~w ~w ~w\n',[T1,B1,T2,B2]),
	%% WARNING: subsumes_chk only works correctly if
	%% [T1,B1] and [T2,B2] are without common variables
	\+ subsumes_chk([T2,B2],[T1,B1])
	.

