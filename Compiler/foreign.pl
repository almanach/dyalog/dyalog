/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 1999 - 2003, 2011 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  foreign.pl -- Foreign Interface
 *
 * ----------------------------------------------------------------
 * Description
 *
 *    Interface to C functions from DyALog
 *
 *    The C interface syntax
           <foreign> :=    '$interface'( <template>, [ <option>* )
          <option>   :=     return(<return>)
                      |     choice_size(N)             % none or N >= 0
                      |     load([<exp>:<type>*])
          <return>   :=     none
                      |     call
                      |     bool
                      |     <exp>:<type>
          <template> :=     <pred>( <arg>, ..., <arg> )
           <arg>     :=     <exp>:<mod><type>
                      |     <exp>:<type>                % = default +
                      |     <exp>                       % = default +term
           <mod>     :=     +                           % input argument
                      |     -                           % output argument
           <type>    :=     int
                      |     char
                      |     string
                      |     bool
                      |     term
                      |     float
                      |     ptr
* ----------------------------------------------------------------
*/

:-include 'header.pl'.

:-require 'stdprolog.pl'.

:-features(interface,[return,name,code,choice_size,load]).

unfold( '$interface'(Template,Options), Env, Tupple, Code, Env ) :-
%%	format('Unfolding Interface ~w\n',[Exp] ),
	foreign_parse_options(Options,
			      I::interface{ return => Return,
					    code => Code,
					    choice_size => Choice,
					    load => Load
					  }),
	Return ?= bool,
	Choice ?= none,
	Load ?= [],
	foreign_parse_template(Template,I,Tupple)
	.

:-rec_prolog foreign_parse_options/2.

foreign_parse_options([],_).
foreign_parse_options([ Option | Options ],
		      I::interface{ return => Return,
				    choice_size => Choice,
				    load => Load
				  }
		     ) :-
	( Option = return(X:Type) ->
	    check_foreign_type(Type),
	    Return = (X:Type)
	;   Option = return(bool) ->
	    Return = bool
	;   Option = return(call) ->
	    Return = call
	;   Option = return(none) ->
	    Return = none
	;   Option = load(Load) -> true
	;   Option = choice_size(Choice) ->
	    ( Choice >= 0 xor error( 'positive number expected in ~w',[Option] ))
	;
	    error( 'unexpected option ~w', [Option] )
	),
	foreign_parse_options(Options,I)
	.

:-rec_prolog foreign_parse_template/3.

foreign_parse_template( Template, I::interface{ return=>Return,
						name => Name,
						code => Code,
						choice_size => Choice,
						load => Init_Load
					      }, Tupple ) :-
	( Template == '$$repeat' ->
	    Name=repeat,
	    Args=[],
	    N=0,
	    M=1,
	    foreign_parse_template_args( [], 1, Tupple, Before_Call, Call_Args, After_Call )
	;   Template =.. [Name|Args], atom(Name) ->
	    functor(Template,Name,N),
	    M is N+1,
	    foreign_parse_template_args( Args, 1, Tupple, Before_Call, Call_Args, After_Call )
	;   error( '~w is not a valid C name', [Name] )
	),
	( Template == '$$repeat' ->
	    Call=noop,
	    End=noop
	;   Return = (X:T) ->
	    foreign_unify_arg(-,T,X,0,AC0),
	    (T == float ->
		RetReg = '$freg'(0)
	    ;	
		RetReg = '$reg'(0)
	    ),
	    Call = c_call(Name,Call_Args,none,RetReg),
	    End = AC0
	;   Return == none ->
	    Call = c_call(Name,Call_Args,none,none),
	    End = noop
	
	;   Return == call ->
	    Call = c_call(Name,Call_Args,bool,'$reg'(0)),
	    End = call('$reg'(0),[])
	;
	    Call = c_call(Name,Call_Args,bool,none),
	    End = noop
	),
	( Choice == none ->
	    Choice_Code = noop
	;
	    update_counter(choice_label,C_I),
	    name_builder('L~w~w_choice_~w',[Name,N,C_I],Choice_Label),
	    Label = '$label'( Choice_Label ),
	    foreign_parse_init_load(Init_Load,0,Init_Load_Code),
	    Choice_Code = ( c_call('Dyam_Foreign_Create_Choice',[Label,c(N),c(Choice)],none,none)
			  :> Init_Load_Code
			  :> label( Label )
			  :> c_call('Dyam_Foreign_Update_Choice',[Label,c(N),c(Choice)],none,none)
			  )
	),
	Code = Before_Call :> Choice_Code :> Call :> After_Call :> End :> reg_reset(M),
%%	format('\tCode -> ~w\n',[Code] ),
	true
	.

:-rec_prolog foreign_parse_init_load/3.

foreign_parse_init_load( [],_,noop).
foreign_parse_init_load( [A|Rest], I, XCode :> Code ) :-
	(   A = X:Type,
	    check_foreign_type(Type)
	xor error('foreign load ~w',[A] ) ),
	R is 2 * I,
	foreign_load_init_arg(Type,X,R,XCode,L),
	J is I+L,
	foreign_parse_init_load(Rest,J,Code)
	.

:-rec_prolog foreign_parse_template_args/6.

foreign_parse_template_args( [], _, _, noop, [], noop ).
foreign_parse_template_args( [A|Args], I, Tupple,
			     BC :> Before_Call,
			     [CA|Call_Args],
			     AC :> After_Call ) :-
	(   A = (V:Mode_Type),
	    check_mode_type(Mode_Type,Mode,Type)
	->
	    R is 2 * I,
	    foreign_load_arg(Mode,Type,V,R,BC,CA,Tupple),
	    ( (Mode == (-) ; Type == term ; Type == term_no_deref) ->
		CA = '&reg'(R)
	    ;	( Type == float ->
		    CA ?= '$freg'(R)
		;   
		    CA ?= '$reg'(R)
		)
	    ),
	    foreign_unify_arg(Mode,Type,V,R,AC),
%%	    format('\t A->~w BC->~w CA->~w AC->~w\n',[A,BC,CA,AC]),
	    extend_tupple(V,Tupple,New_Tupple),
	    J is I+1,
	    foreign_parse_template_args(Args,J,New_Tupple,
					Before_Call,Call_Args,After_Call)
	;   error( '~w is not a valid (exp : mode type)',[A])
	)
	.

:-std_prolog check_mode_type/3, check_foreign_type/1, check_foreign_mode/1.

check_mode_type( Mode_Type, Mode, Type ) :-
	(   Mode_Type =.. [Mode,Type] ->
	    check_foreign_mode(Mode),
	    check_foreign_type(Type)
	;   Mode_Type = Type,
	    check_foreign_type(Type),
	    Mode = (+)
	)
	.

check_foreign_mode(Mode) :- domain(Mode,[+,-]).
check_foreign_type(Type) :- domain(Type,[int,float,char,bool,string,ptr,term,input,output,term_no_deref]).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Loading initial args

:-rec_prolog foreign_load_init_arg/5.

foreign_load_init_arg(int,A,I,Inst,1) :-
	label_term(A,Label_A),
	( (number(A) ; check_var(A)) -> Inst = mem_load_number(I,Label_A)
	;   error( '~w integer expected',[A] )
	)
	.

foreign_load_init_arg(float,A,I,Inst,1) :-
	label_term(A,Label_A),
	( (float(A) ; check_var(A)) -> Inst = mem_load_float(I,Label_A)
	;   error( '~w float expected',[A] )
	)
	.

foreign_load_init_arg(ptr,A,I,Inst,1) :-
	label_term(A,Label_A),
	( (number(A) ; check_var(A)) -> Inst = mem_load_ptr(I,Label_A)
	;   error( '~w ptr expected',[A] )
	)
	.

foreign_load_init_arg(bool,A,I,Inst,1) :-
	label_term(A,Label_A),
	( (domain(A,[true,false]) ; check_var(A)) -> Inst = mem_load_boolean(I,Label_A)
	;   error( '~w boolean expected',[A] )
	)
	.

foreign_load_init_arg(char,A,I,Inst,1) :-
	label_term(A,Label_A),
	( (char(A) ; check_var(A)) -> Inst = mem_load_char(I,Label_A)
	;   error( '~w char expected',[A] )
	)
	.

foreign_load_init_arg(string,A,I,Inst,1) :-
	label_term(A,Label_A),
	( (atom(A) ; check_var(A)) -> Inst = mem_load_string(I,Label_A)
	;   error( '~w string expected',[A] )
	)
	.

foreign_load_init_arg(term, A, I, Inst,2) :-
	label_term(A,Label_A),
	( A==[] ->
            Inst = mem_load_nil(I)
        ;   atomic(A) ->
            Inst=mem_load_cst(I,Label_A)
        ;   
            Inst=mem_load(I,Label_A)
        )
	.

foreign_load_init_arg(input,A,I,Inst,1) :-
	( (atom(A) ; number(A) ; check_var(A)) ->
	    label_term(A,Label_A),
	    Inst = mem_load_input(I,Label_A)
	;   error( '~w input expected',[A] )
	)
	.

foreign_load_init_arg(output,A,I,Inst,1) :-
	( (atom(A) ; number(A) ; check_var(A)) ->
	    label_term(A,Label_A),
	    Inst = mem_load_output(I,Label_A)
	;   error( '~w output expected',[A] )
	)
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Loading Arguments

:-rec_prolog foreign_load_arg/7.

%% Input argument + Type

foreign_load_arg(+,int, A, I, Inst, CA, _ ) :-
	( number(A) -> Inst = noop, CA = c(A)
	;   check_var(A) ->
	    label_term(A,Label_A),
	    Inst = reg_load_number(I,Label_A)
	;   error( '~w integer expected',[A] )
	)
	.

foreign_load_arg(+,float, A, I, Inst, CA, _ ) :-
	( float(A) -> Inst = noop, CA = c(A)
	;   number(A) -> Inst = noop, B is 1.0 * A, CA = c(B)
	;   check_var(A) ->
	    label_term(A,Label_A),
	    Inst = reg_load_float(I,Label_A)
	;   error( '~w float expected',[A] )
	)
	.

foreign_load_arg(+,ptr, A, I, Inst, CA, _ ) :-
	( number(A) ->
	    Inst = noop, CA = c(A)
	;   A = '$fun'(_,_,_) ->
	    Inst = noop, CA = A
	;   
	    check_var(A) ->
	    label_term(A,Label_A),
	    Inst = reg_load_ptr(I,Label_A)
	;   error( '~w address expected',[A] )
	)
	.

foreign_load_arg(+,bool, A, I, Inst, CA, _ ) :-	
	( A == true -> Inst = noop, CA = c(1)
	; A == false -> Inst = noop, CA= c(0)
	;   check_var(A) ->
	    label_term(A,Label_A),
	    Inst = reg_load_boolean(I,Label_A)
	;   error( '~w boolean expected',[A] )
	)
	.

foreign_load_arg(+,char, A, I, Inst, CA, _ ) :-
	( char(A) -> Inst = noop, char_code(A,AA), CA=c(AA)
	;   check_var(A) ->
	    label_term(A,Label_A),
	    Inst = reg_load_char(I,Label_A)
	;   error( '~w char expected',[A] )
	)
	.

foreign_load_arg(+,string, A, I, Inst, CA, _ ) :-
	label_term(A,Label_A),
	( atom(A) -> Inst = reg_load_string(I,Label_A)
	;   check_var(A) -> Inst = reg_load_string(I,Label_A)
	;   error( '~w string expected',[A] )
	)
	.

foreign_load_arg(+,term_no_deref, A, I, Inst, _ , Tupple ) :-
	label_term(A,Label_A),
	( A==[] ->
	  Inst = reg_load_nil(I)
        ;   atomic(A) ->
	  Inst=reg_load_cst(I,Label_A)
        ;   check_var(A) ->
	  Inst = reg_load_var(I,Label_A)
        ;   
	  Inst=reg_load(I,Label_A)
        )
	.

foreign_load_arg(+,term, A, I, Inst, _ , Tupple ) :-
	label_term(A,Label_A),
	( A==[] ->
            Inst = reg_load_nil(I)
        ;   atomic(A) ->
            Inst=reg_load_cst(I,Label_A)
        ;   (check_var(A), \+ var_in_tupple(A,Tupple)) ->
	    Inst = reg_load_var(I,Label_A)
        ;   
            Inst=reg_load(I,Label_A)
        )
	.


foreign_load_arg(+,input, A, I, Inst, CA, _ ) :-
	label_term(A,Label_A),
	( atom(A) -> Inst = reg_load_input(I,Label_A)
	;   number(A) -> Inst = reg_load_input(I,Label_A)
	;   check_var(A) -> Inst = reg_load_input(I,Label_A)
	;   error( '~w string expected',[A] )
	)
	.

foreign_load_arg(+,output, A, I, Inst, CA, _ ) :-
	label_term(A,Label_A),
	( atom(A) -> Inst = reg_load_output(I,Label_A)
	;   number(A) -> Inst = reg_load_output(I,Label_A)
	;   check_var(A) -> Inst = reg_load_output(I,Label_A)
	;   error( '~w string expected',[A] )
	)
	.

%% Output argument: - Type
	
foreign_load_arg(-,int, A, I, noop, _, _ ) :-
	(   number(A) xor check_var(A) xor error( '~w integer or var expected',[A] ))
	.

foreign_load_arg(-,float, A, I, noop, _, _ ) :-
	(   float(A) xor number(A) xor check_var(A) xor error( '~w float or var expected',[A] ))
	.

foreign_load_arg(-, ptr, A, I, noop, _, _ ) :-
	(   number(A) xor check_var(A) xor error( '~w address or var expected',[A] ))
	.

foreign_load_arg(-,bool, A, I, noop, _, _ ) :-
	(   domain(A,[true,false]) xor check_var(A) xor error( '~w boolean or var expected',[A] ))
	.

foreign_load_arg(-,char, A, I, noop, _, _ ) :-
	(   char(A)  xor check_var(A) xor  error( '~w char expected',[A] ))
	.

foreign_load_arg(-,string, A, I, noop, _, _ ) :-
	(   atom(A) xor check_var(A) xor error( '~w string expected',[A] ))
	.

foreign_load_arg(-,term, A, I, noop, _, _ ).

%% Input/Output argument: ? Type

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% 

:-rec_prolog foreign_unify_arg/5.

foreign_unify_arg(+,_, A, I, noop ).
foreign_unify_arg(-,int, A, I, reg_unif_c_number(I,A) ).
foreign_unify_arg(-,float, A, I, reg_unif_c_float(I,A) ).
foreign_unify_arg(-,ptr, A, I, reg_unif_c_ptr(I,A) ).
foreign_unify_arg(-,bool, A, I, reg_unif_c_boolean(I,Label_A) ) :- label_term(A,Label_A).
foreign_unify_arg(-,char, A, I, reg_unif_c_char(I,A) ).
foreign_unify_arg(-,string, A, I, reg_unif_c_string(I,A) ).
foreign_unify_arg(-,term,A,I, reg_unif(I,Label_A) ) :- label_term(A,Label_A).
foreign_unify_arg(-,input, A, I, reg_unif_c_input(I,A) ).
foreign_unify_arg(-,output, A, I, reg_unif_c_output(I,A) ).









