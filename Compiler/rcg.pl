/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2001, 2002, 2003, 2004 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  rcg.pl -- compilation of RCG
 *
 * ----------------------------------------------------------------
 * Description
 *   Extension of the compiler to handle Range Concatenation Grammars
 *   RCG have been introduced by Pierre Boullier
 *   We extend them with logic argument, using the Hilog notation
 *
 *   A xRCG production has the form
 *          Prod ::= A_0 --> A_1, .. , A_n.
 *          A    ::= T (S_1,..,S_n)
 *                 |  { Prolog_Body }
 *                 |  & BuiltinEscape
 *          T    ::= prolog term
 *          S    ::= K_1 @ .. @ K_n
 *          K    ::= [Token]
 *                |  Var
 * ----------------------------------------------------------------
 */

:-include 'header.pl'.

:-extensional parse_mode/1.

pgm_to_lpda( grammar_rule(rcg,Rule :: (Head --> Body) ), Trans ) :-
	rcg_expand(Id,Head,Key,T,Args,Code),
	( rec_prolog( Key ) ->
	    rcg_prolog_make(Key,T,Args,XHead),
	    Trans='*GUARD*'(XHead) :> Cont,
	    Type = rec_prolog,
	    Last = noop
	;   prolog( Key ) ->
	    rcg_prolog_make(Key,T,Args,XHead),
	    Trans='*PROLOG-FIRST*'(XHead) :> Cont,
	    Type = prolog,
	    Last = '*PROLOG-LAST*'
	;   
	    rcg_make_callret(Key,T,Args,Call,Return),
	    Trans = '*FIRST*'(Call) :> Cont,
	    ( light_tabular(Key) ->
		Type=rec_prolog,
		Last = '*LIGHTLAST*'(Return)
	    ;	
		Type = dyalog,
		Last = '*LAST*'(Return)
	    )
	),
	rcg_body_to_lpda(Id,Body,Last,Cont1,Type),
	body_to_lpda(Id,Code,Cont1,Cont,Type),
	erase( rcg_assign(Id,_,_,_) ),
%%	format('RCG Trans ~w\n',[Trans]),
	true
	.

rcg_body_to_lpda(Id,Body,Cont,Trans,Type) :-
	( Body = '$VAR'(_) ->
	    error( 'Not yet implemented ~w',[Body])
	;   Body = (A , B) -> %% Intersection
	    rcg_body_to_lpda(Id,B,Cont,ContB,Type),
	    rcg_body_to_lpda(Id,A,ContB,Trans,Type)
	;   Body = (A->B;C) ->
	    rcg_body_to_lpda(Id,(('*COMMIT*'(A),B);C),Cont,Trans,Type)
	;   Body = (A->B) ->
	    rcg_body_to_lpda(Id, (A -> B ; fail), Cont, Trans, Type)
	;   Body = (A;B) ->
	    handle_choice(Cont,Trans,Last,ContA,ContB),
	    rcg_body_to_lpda(Id,A,Last,ContA,Type),
	    rcg_body_to_lpda(Id,B,Last,ContB,Type)
	;   Body = '*COMMIT*'(A) ->
	    rcg_body_to_lpda(Id,A,'*CUT*' :> Cont, ContA, rec_prolog),
	    Trans = '*SETCUT*' :> ContA
	;   Body = fail ->
	    Trans = fail :> Cont
	;   Body = true ->	    
	    Trans = Cont
	;   Body = { Call } ->
	    my_numbervars(Call,Id,_),
	    body_to_lpda(Id,Call,Cont,Trans,Type)
	;   ( Body = wait(Task) ; Body = '$answers'(A) ) ->	
	    error( 'Not yet implemented in RCG body ~w',[Body])
	;   rcg_non_terminal_to_lpda(Id, Body, Cont, Trans,Type)
	)
	.

:-rec_prolog rcg_non_terminal_to_lpda/5.
	
rcg_non_terminal_to_lpda(Id, AA, Cont, Trans,Type) :-
	xtagop(Tag,A,AA),
	rcg_expand(Id,A,Key,T,Args,Code),
	( rec_prolog( Key ) ->
	    rcg_prolog_make(Key,T,Args,XA),
	    Cont1 = '*GUARD*'(XA) :> Cont
	;   prolog(Key) ->
	    (	Type \== rec_prolog
	    xor error('Not a recursive prolog rcg predicate ~w',[A]) ),
	    rcg_wrap_args(Key,T,Tag,Args,Wrapped_Args),
	    wrapping_predicate( rcg_prolog(Key) ),
	    Cont1 = '*WRAPPER-CALL*'(rcg_prolog(Key),Wrapped_Args,Cont)
	;   extensional( Key ) ->
	    rcg_prolog_make(Key,T,Args,XA),
	    litteral_to_lpda(Id,recorded(XA), Cont,Cont1,Type)
	;   Type == rec_prolog, \+ light_tabular(Key) ->
	    error( 'Not a recursive prolog rcg predicate ~w', [A])
	;   
	    rcg_make_callret(Key,T,Args,Call,Return),
	    ( lco( Key ), '*LAST*'(Return) == Cont ->
		Cont1 = '*LASTCALL*'(Call)
	    ;
		rcg_wrap_args(Key,T,Tag,Args,Wrapped_Args),
		wrapping_predicate(Key),
		Cont1 = '*WRAPPER-CALL*'( Key, Wrapped_Args, Cont )
	    )
	),
	body_to_lpda(Id,Code,Cont1,Trans,dyalog)
	.


wrapping_predicate( Key::rcg(F/N,M) ) :-
	rcg_functor(A,Key,T,RCG_Args),
	rcg_args_expand(Id,RCG_Args,Args,_,true),
	rcg_make_callret(Key,T,Args,Call,Return),
	rcg_wrap_args(Key,T,Tag,Args,Wrapped_Args),
	my_numbervars([Closure,T,Tag,Args],Id,_),
	( light_tabular(Key) ->
            Body = '*LIGHTNEXTALT*'(Call,Return,'*PROLOG-LAST*',Tag)
        ;   
            Body = '*NEXT*'{ call=>Call, ret=>Return, right=>'*PROLOG-LAST*', nabla=>Tag }
        ),
	record( '*WRAPPER*'( Key,
			     [Closure|Wrapped_Args],
			     Body
			   ))
	.

wrapping_predicate( Pred::rcg_prolog(Key::rcg(F/N,M)) ) :-
	rcg_functor(A,Key,T,RCG_Args),
	rcg_args_expand(Id,RCG_Args,Args,_,true),
	rcg_prolog_make(Key,T,Args,X),
	rcg_wrap_args(Key,T,Tag,Args,Wrapped_Args),
	my_numbervars([Closure,T,Tag,Args],Id,_),
	record( '*WRAPPER*'( Pred,
			     [Closure|Wrapped_Args],
			     '*PROLOG-LAST*'(X,Tag)
			   ))
	.

:-light_tabular rcg_wrap_args/5.
:-mode(rcg_wrap_args/5,+(+,-,-,-,-)).

rcg_wrap_args(rcg(F/N,M),T,Tag,Args,Wrapped_Args) :-
	functor(T,F,N),
	T =.. [_|TArgs],
	MM is 2*M,
	length(Args,MM),
	append(TArgs,[Tag|Args],Wrapped_Args)
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% RCG predicates

rcg_functor(T,rcg(F/N,M),A,RCG_Args) :-
	( var(T) ->
	    atom(F),
	    number(N),
	    number(M),
	    functor(A,F,N),
	    length(RCG_Args,M),
	    T =.. [apply,A|RCG_Args]
	;
	    T =.. [apply,A|RCG_Args],
	    length(RCG_Args,M),
	    functor(A,F,N)
	)
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% RCG Expanders

:-rec_prolog rcg_expand/6.

rcg_expand(Id,A,Key,T,XArgs,XCode):-
	(   rcg_functor(A,Key1::rcg(F1/N,M),T1,Args)
	xor error('rcg litteral expected ~w\n',[T1])),
	term_current_module_shift(Key1,Key::rcg(F/N,M),T1,T),
	register_predicate(Key),
	rcg_args_expand(Id,Args,XArgs,XCode,true),
	my_numbervars(T,Id,_),
	my_numbervars(XArgs,Id,_),
	my_numbervars(XCode,Id,_)
	.

:-rec_prolog rcg_args_expand/5.

rcg_args_expand(Id,[],[],C,C).
rcg_args_expand(Id,[Str|Args],[L,R|XArgs],C_Left,C_Right) :-
	rcg_str_expand(Id,Str,L,R,C_Left,C_Middle),
	rcg_args_expand(Id,Args,XArgs,C_Middle,C_Right)
	.

:-extensional parse_mode/1.
:-std_prolog rcg_str_expand/6.

rcg_str_expand(Id,Str,Left,Right,C_Left,C_Right) :-
	( Str = Left:Right ->	% non numbervar-ized Range variable
	    C_Left = C_Right
	;   check_var(Str) ->
	 % we try not to numbervar Range variables before expansion
	 % but the case may happen because of Prolog escaped parts
	 % => special mechanism to handle it
	    ( var(Id) -> gensym(Id) ; true),
	    ( recorded( rcg_assign(Id,Str,_Left,_Right) ) ->
		C_Left = ( Left=_Left, Right = _Right, C_Right)
	    ;	
	    	my_numbervars(Left,Id,_),
		my_numbervars(Right,Id,_),
		record( rcg_assign(Id,Str,Left,Right) ),
		C_Left=(Str=(Left:Right),C_Right)
	    )
	;   Str = [] ->		% Empty range
	    C_Left = (Left = Right, C_Right)
	;   Str = [_|_] ->	% Terminals
	    parse_mode(Mode),
	    rcg_terminals_expand(Str,Mode,Left,Right,C_Left,C_Right)
	;   Str = (Str1 @ Str2) -> % Range Concatenation
	    C_Left = (Middle1=Middle2,C_Aux),
	    rcg_str_expand(Id,Str1,Left,Middle1,C_Aux,C_Middle),
	    rcg_str_expand(Id,Str2,Middle2,Right,C_Middle,C_Right)
	;
	    error('not a valid rcg string ~w',[Str])
	)
	.

:-std_prolog rcg_terminals_expand/6.

rcg_terminals_expand(Str,Mode,Left,Right,C_Left,C_Right) :-
	Str = [Token|Rest],
	C_Left = (Scan,C_Middle),
	( Mode == token ->
	    scanner(Left,Token,Middle,Scan)
	;   
	    Scan = ( Left = [Token|Middle] )
	),
	( Rest = [] ->
	    C_Middle = C_Right,
	    Middle = Right
	;   
	    rcg_terminals_expand(Rest,Mode,Middle,Right,C_Middle,C_Right)
	)
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% RCG makers

:-require 'maker.pl'.

:-rec_prolog rcg_prolog_make/4.

rcg_prolog_make(rcg(F/N,M),T,Args,Res) :-
	T =.. [F|TArgs],
	append(TArgs,Args,XArgs),
	Res =.. [F|XArgs]
	.

:-light_tabular rcg_make_callret/5.
:-mode(rcg_make_callret/5,+(+,-,-,-,-)).

rcg_make_callret(Key::rcg(F/N,M),T,Args,Call,Return) :-
	( recorded( call_pattern(Key,Pat) ) ->
	    true
	;   recorded( call_pattern(rcg('*default*',_),Pat)) ->
	    true
	;   
	    Pat =(+)
	),
	functor(T,F,N),
	MM is M * 2,
	length(Args,MM),
	make_list(RCG_Pat,MM,+),
	( Pat = [FPat|Args_Pat] ->
	    true
	; compound(Pat) ->
	    Pat =.. [FPat|Args_Pat]
	;   
	    FPat = Pat,
	    Args_Pat = Pat
	),
	append(RCG_Pat,Args_Pat,X_Pat),
	Pattern = [FPat|X_Pat],
	rcg_prolog_make(Key,T,Args,X),
	make_callret_with_pat(Pattern,X,Call,Return),
	(   rcg_functor(A,Key,T,RCG_Args),
	    rcg_args_expand(_,RCG_Args,Args,_,true),
	    numbervars(A),
	    I = '*RITEM*'(Call,Return),
	    label_term(I,CI),
	    label_term(A,CA),
	    record( viewer(CI,CA) ),
	    fail
	;   
	    true
	)
	.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Modules

term_module_shift(Module,Pred1::rcg(F1/N,M),rcg(F/N,M),T1,T) :-
	( recorded( module_import(Pred1,Module1) ) ->
	    Module1 \== []
	;   Module \== [],
	    atom_module(F1,[],_),
	    Module1=Module
	),
	atom_module(F,Module1,F1),
	length(Args,N),
	T1 =.. [F1|Args],
	T =.. [F|Args]
	.

