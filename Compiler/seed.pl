/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 1998, 2003, 2007, 2008, 2010, 2011, 2018 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  seed.pl -- From LPDAs to Seeds
 *
 * ----------------------------------------------------------------
 * Description
 * 
 * ----------------------------------------------------------------
 */

:-include 'header.pl'.

:-prolog component/4,install_app/6,seed_code/5.
:-rec_prolog anchor/4,subsumption/5.

seed_install( Seed::seed{ id=>SeedId::'$seed'(Id,_),
			  model=>T, tabule=> Tab,
			  schedule=> Sch, subsume => Subs,
			  subs_comp_ref => S_Id,
			  subs_comp => SC,
			  tail_flag => Tail_Flag,
			  anchor => Anchor
			},
	      Backptr_Info,
	      Call
	    ) :-
	%% format('Start Seed ~w\n',[Seed]),
	( T = (_:>_) ->
	    update_counter( seeds, Id ),
	    PP=Id
	;
	    PP=[]
	),
	seed_model( Seed ),
	seed( PP, Seed ),
	%%	format('-->1 Seed ~w\n',[Seed]),
	Obj = '$arg'(0),
	Bptr = '$arg'(1),
	( Backptr_Info=[Backptr,Trace] ->
	    label_term(Trace,Trace_Ref),
	    Trace_Reg = '$arg'(2),
	    Call = call(LCode,[SeedId,c(Backptr),Trace_Ref]),
	    Backptr_Call = backptr( Bptr, Trace_Reg),
	    LCode = '$fun'(_,3,_),
	    Install_Args=[Obj,Bptr,Trace_Reg]
	;
	    Backptr_Info=Backptr,
	    Call = call(LCode,[SeedId,c(Backptr)]),
	    Backptr_Call = backptr( Bptr ),
	    LCode = '$fun'(_,2,_),
	    Install_Args=[Obj,Bptr]
	),
	%% format('--> Seed ~w\n',[Seed]),
	test_schedule(Sch,T,C3),
	( Tab == light -> C1 = light_object(Obj) :> C2 ; C1 = object(Obj) :> C2 ),
	( Tab == yes   -> C2 = tabule :> C3 ; C2 = C3 ),
	( Subs == yes  ->
	  choice_code(C1, setcut :> subsume( Obj ) :> cut :> succeed,_Code)
	; Subs == variance ->
	  choice_code(C1, setcut :> variance( Obj ) :> cut :> succeed,_Code)
	;   _Code = C1
	),
%%	format('Seed model=~w subs_comp=~w\n',[T,SC]),
	( T = (_ :> (IndexingKey ^ _)),
	  SC = (_ :> Tail ) ->
	  label_term(Tail,Tail_Id),
	  Code = indexingkey(IndexingKey,Tail_Id) :> _Code
	;
	  Code = _Code
	),
	%% format('Seed ~w\n',[Seed]),
	label_code( Backptr_Call :> Code, LCode )
	.

%% Need to break otherwise boot compiler fails to compile this file
:-std_prolog test_schedule/3.

test_schedule(Sch,T,C3) :-
%%	format('PRE Schedule ~w ~w\n',[T,Sch]),
	( var(Sch) -> error('variable in test schedule ~w ~w',[Sch,T])
	;    Sch == std   -> C3 = schedule :> succeed
	;   Sch == last  -> C3 = schedule_last :> succeed
	;   Sch == first -> C3 = schedule_first :> succeed
	;   Sch == prolog -> C3 = call('Schedule_Prolog',[]) :> succeed
	;   T='*ANSWER*'{} -> C3 = answer :> succeed
	;   C3 = succeed
	)
%%	,format('Schedule ~w ~w ~w\n',[T,Sch,C3])
	.

%%:-light_tabular seed/2.

seed( 	PP, Seed :: seed{ id=>'$seed'(Id,SeedM),
			  model=>T,
			  anchor=> Anchor,
			  subs_comp=>SC,
			  code => Code,
			  go=> Go,
			  model_ref => Anchor_Ref,
			  subs_comp_ref => SC_Ref,
			  tabule => Tab,
			  subsume => Sub,
			  c_type => Type,
			  c_type_ref => Type_Ref,
			  out_env => Out_Env_List,
			  appinfo=> AppInfo,
			  tail_flag => Tail_Flag
			}
    ) :-
	mutable(SeedM,[]),
%%	seed_model( Seed ),
	label_term(Type,Type_Ref),
	%% format('Seed  In ~w\n',[Seed]),
	(PP==[] ->
	    update_counter( seeds, Id )
	;
	    Id=PP
	),
	mutable(Emitted,no),
%%	format('Seed  2 id=~w ~w\n',[Id,Seed]),
	anchor(Anchor,T,Anchor_Ref,Tail_Flag),
	%%format('Seed  3 ~w\n',[Seed]),
	subsumption(SC,Sub,Anchor,Anchor_Ref,SC_Ref),

	AppInfo ?= [],
	seed_code(T,AppInfo,Code,Go, Out_Env_List),
	%% format('Seed  4 ~w\n',[Seed]),
	numbervars( Seed, 1, _),
%%	format('Done seed id=~w\n',[Id]),
	mutable(SeedM,Seed)
	
	.

anchor(Anchor,Model,Ref,Tail_Flag) :-
	( var(Anchor) ->
	  ( Model = (K :> Right) ->
%%		format( 'Call GENTAIL ~w\n', [Model]),
		new_gen_tail(Right,K,Tail,Tail_Flag),   %% when possible, we use a tail
					%% that resumes Right
		Anchor = K :> Tail
	    ;	
		Anchor = Model
	  )
	;   
	    true
	),
	%%format('Label Anchor ~w\n',[Anchor]),
	label_term(Anchor,Ref)
	%% format('\t~w\n',[Ref])
	.

subsumption(SC,Sub,Model,Model_Ref,Ref) :-	
	( nonvar(SC) ->
	    label_term(SC,Ref)
	;   domain(Sub,[yes,variance]) ->
	    SC=Model,
	    Ref=Model_Ref
	;
	    SC = [],
	    label_term(SC,Ref)
	)
	.

seed_code(T,AppInfo,Code,Go,Out_Env_List) :-
	value_counter(T,N),
	functor(Code,code,N),
%%	format('Seed  Code ~w\n',[T]),
	install_app(N,T,AppInfo,Code,Go_Code,Out_Env_List),
%%	format('Seed  Code 2 ~w\n',[T]),
	label_code(Go_Code,Go)
	.
	
install_app(0,Model,_,_,Code,Out_Env_List) :-
	(   recorded( special_app( Model ) ) ->
	    special_app_model( application{ mode=>trans,
					    trans=>Model,
					    trans_id=>0,
					    code=>CC,
					    out_env => Out_Env
					  } ),
	    Code = allocate :> CC :> deallocate :> succeed,
	    Out_Env_List = [Out_Env_List]
	;   Code = succeed,
	    Out_Env_List=[]
	)
	.

install_app(I,Model, AppInfo,Code, Go, Out_Env_List ) :- I > 0, J is I-1,
%%	format('call comp ~w ~w\n',[J,Model]),
	component(Model,J,Head,AppInfo),
	(   Head = completion(J,Ref,Id,C,RelCode,InitCode),
	    Go1 = completion(Id,J),
	    Out_Env_List = Out_Env_List2
	;   Head = direct(J,Ref,Id,C,Out_Env,RelCode,InitCode),
	    Go1 = direct(Id,J),
	    Out_Env_List = [Out_Env|Out_Env_List2]
	;   Head = direct(Ref,C,Out_Env,RelCode,InitCode), %% when no need for TRANS to find ITEM
	    Go1 = noop,
	    Out_Env_List = [Out_Env|Out_Env_List2]
	),
	arg(I,Code,component(Ref,C,InitCode)),
	( RelCode = noop ->
	    Go1x=Go1
	;
	    null_tupple(Null_Tupple),
	    Go1x = allocate :> RelCode :> deallocate :> Go1
	),
	( I == 1 ->
	    Go = Go1x :> succeed,
	    Out_Env_List2=[]
	;   
	    install_app(J,Model,AppInfo,Code,Go2,Out_Env_List2),
	    choice_code(Go2, Go1x :> succeed,Go)
	)
	.

:-std_prolog handle_appinfo/4.

handle_appinfo(KeyInfoInit,AppInfo,RelCode,InitCode) :-
	( KeyInfoInit = [] ->
				% no key attached to the application
	    RelCode=noop,
	    InitCode=c(0)
	;   KeyInfoInit=key(Key,Info,InitCode),
	    domain(key(Key,Info,RelCode),AppInfo) ->
				% a key for the application
				% with associated info in AppInfo
	    true
	;   KeyInfoInit=key(_,[],[]),	% a key for the application
				% but no associated info in AppInfo
	    RelCode=noop,
	    InitCode=c(0)
	)
	.

:-extensional empty_app_key/1.

empty_app_key([]).
empty_app_key(key(_,[],[])).

component(Trans,T_Id, Comp,AppInfo) :-
%	format('call component trans (~w) ~w\n',[T_Id,Trans]),
 	exists( 
		gen_application(application{ key => KeyInfoInit,
					     trans=>Trans, item=>Item,
					     trans_id=>T_Id, item_id=>I_Id } )
		),
	handle_appinfo(KeyInfoInit,AppInfo,RelCode,InitCode),
%	format('component trans (~w) ~w x ~w\n',[T_Id,Trans,Item]),
	app_model( application{ key=>KeyInfoInit,
				mode=>trans,
				trans=>Trans, item=>Item,
				trans_id=>T_Id, item_id=>I_Id,
				code => Code,
				restrict => Restrict,
				out_env => Out_Env
			      }
		 ),
%%	format('component trans ~w\n\tout_env=~w\n\tcode=~w\n\n',
%%	       [Trans,Out_Env,Code]),
	( Restrict == yes ->
	    Comp=direct(Item_Ref,Code_Id,Out_Env,RelCode,InitCode)
	;
	    Comp=direct(T_Id,Item_Ref,I_Id,Code_Id,Out_Env,RelCode,InitCode)
	),
	label_code( Code, Code_Id ),
	label_term( Item, Item_Ref ).

component(Item,I_Id, completion(I_Id,Trans_Ref,T_Id,Comp_Id,RelCode,InitCode),AppInfo) :-
	exists(
		   gen_application(application{ key=>KeyInfoInit,
						trans=>Trans, item=>Item,
						trans_id=>T_Id, item_id=>I_Id })
		  ),
	handle_appinfo(KeyInfoInit,AppInfo,RelCode,InitCode),
	app_model( application{ key=>KeyInfoInit,
				mode=>item,
				trans=>Trans, item=>Item,
				trans_id=>T_Id, item_id=>I_Id,
				item_comp => Comp
			      }
		 ),
	label_term(Comp,Comp_Id),
	label_term(Trans,Trans_Ref).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Application models

gen_application( Application :: application{ mode=> gen, item=>I, trans=>T,
					     item_id=>I_Id, trans_id=>T_Id }
		 ) :-
	app_model( Application ),
	update_counter( I, I_Id ),
	update_counter( T, T_Id ).

gen_application( Application :: application{ mode=> gen, item=>none, trans=>T,
					     item_id=>none, trans_id=>0 }
	       ) :-
	special_app_model( Application ),
	record( special_app( T ) )
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Configuration of DyALog objects
%%
%% Anchor Mechanism
%%  To reduce the number of terms to be build in seeds, it is possible
%%  in some cases to replace to use a simplied field 'anchor' in place
%%  of field 'model'
%%
%%  Rules to follow are:
%%        - anchor must have the same set of variables that term (for collapsing).
%%        - anchor should allow ensure the same quality of retrieval that model
%%        for applications (if no application use some kind of seed, use Tupple for anchor)

seed_model( seed{ model=>'*CITEM*'(_,_) ,
                  c_type => '*CITEM*',
		  tabule => yes,
		  subsume => yes,
		  schedule => Schedule
		}
	  ) :-
	Schedule ?= std
	.

seed_model( seed{ model=>'*RITEM*'(_,_) ,
                  c_type => '*RITEM*',
		  tabule => yes,
		  subsume => Subsume,
		  schedule => Schedule
		} ) :-
    Schedule ?= std,
    Subsume ?= yes
	.

seed_model( seed{ model=> '*RITEM*'(C,R) :> Right,
		  c_type => '*CURNEXT*',
		  tabule => Tabule,
		  subsume => Subsume,
		  schedule => Schedule
		} ) :-
	Subsume ?= no,
	Tabule ?= yes,
	Schedule ?= first
	.

%% PROLOG-ITEM used as it when combining to PROLOG-FIRST
%% => no need to search a light anchor
seed_model( seed{ model=>'*PROLOG-ITEM*'{},
                  c_type => '*PROLOG-ITEM*',
		  tabule => light , subsume => no, schedule => prolog } ).

seed_model( seed{ model=>'*FIRST*'(C) :> R,
                  c_type => '*FIRST*',
		  anchor=>'*FIRST*'(C) :> [],
		  tabule => yes, subsume => no, schedule => no } ).

seed_model( seed{ model=>'*LCFIRST*'(C) :> R,
                  c_type => '*LCFIRST*',
		  tabule => Tabule,
		  subsume => no,
		  schedule => Schedule
		} ) :-
	Tabule ?= yes,
	Schedule ?= prolog
	.

seed_model( seed{ model=>'*PROLOG-FIRST*'(C) :> R,
                  c_type => '*PROLOG_FIRST*',
		  anchor=>'*PROLOG-FIRST*'(C) :> [],
		  tabule => yes, subsume => no, schedule => no } ).

seed_model( seed{ model=>'*GUARD*'(C) :> R,
		  anchor=>'*GUARD*'(C) :> [],
		  c_type => '*GUARD_CLAUSE*',
		  tabule => yes, subsume => no, schedule => no } ).

seed_model( seed{ model=>'*GUARD*'(C),
		  c_type => '*GUARD*',
		  tabule => light, subsume => no, schedule=>prolog}).

seed_model( seed{ model=> '*ANSWER*'{},
                  c_type => '*ANSWER*',
		  tabule => yes,  subsume => no, schedule => no } ).

seed_model( seed{ model=> '*DATABASE*'(A),
		  anchor=> A,
                  c_type => '*DATABASE*', 
		  tabule => yes,  subsume => yes, schedule => no } ).

seed_model( seed{ model=> T::'*WAIT-CONT*'{},
                  c_type => '*WAIT_CONT*',
		  anchor=> Anchor::cont(Tupple),
		  model_ref=>Ref,
		  tabule => no, subsume => no, schedule => last } ) :-
	tupple(T,Tupple),
	label_term(Anchor,Ref),
	record_without_doublon(no_copyable(Ref))
	.



seed_model( seed{ model=>T::'*CONT*':> Right,
		  c_type => '*CONT*',
		  tabule => Tabule, subsume => Subsume, schedule => Schedule
		}
	  ) :-
	Tabule ?= yes,
	Subsume ?= yes,
	Schedule ?= first
	.
	
