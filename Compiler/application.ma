;; Compiler: DyALog 1.14.0
;; File "application.pl"


;;------------------ LOADING ------------

c_code local load
	call_c   Dyam_Loading_Set()
	pl_call  fun18(&seed[31],0)
	pl_call  fun18(&seed[20],0)
	pl_call  fun18(&seed[18],0)
	pl_call  fun18(&seed[66],0)
	pl_call  fun18(&seed[19],0)
	pl_call  fun18(&seed[21],0)
	pl_call  fun18(&seed[32],0)
	pl_call  fun18(&seed[70],0)
	pl_call  fun18(&seed[30],0)
	pl_call  fun18(&seed[17],0)
	pl_call  fun18(&seed[49],0)
	pl_call  fun18(&seed[27],0)
	pl_call  fun18(&seed[50],0)
	pl_call  fun18(&seed[16],0)
	pl_call  fun18(&seed[35],0)
	pl_call  fun18(&seed[53],0)
	pl_call  fun18(&seed[52],0)
	pl_call  fun18(&seed[51],0)
	pl_call  fun18(&seed[42],0)
	pl_call  fun18(&seed[44],0)
	pl_call  fun18(&seed[15],0)
	pl_call  fun18(&seed[14],0)
	pl_call  fun18(&seed[12],0)
	pl_call  fun18(&seed[68],0)
	pl_call  fun18(&seed[69],0)
	pl_call  fun18(&seed[45],0)
	pl_call  fun18(&seed[13],0)
	pl_call  fun18(&seed[40],0)
	pl_call  fun18(&seed[65],0)
	pl_call  fun18(&seed[71],0)
	pl_call  fun18(&seed[62],0)
	pl_call  fun18(&seed[41],0)
	pl_call  fun18(&seed[43],0)
	pl_call  fun18(&seed[22],0)
	pl_call  fun18(&seed[63],0)
	pl_call  fun18(&seed[26],0)
	pl_call  fun18(&seed[36],0)
	pl_call  fun18(&seed[39],0)
	pl_call  fun18(&seed[25],0)
	pl_call  fun18(&seed[56],0)
	pl_call  fun18(&seed[55],0)
	pl_call  fun18(&seed[48],0)
	pl_call  fun18(&seed[47],0)
	pl_call  fun18(&seed[46],0)
	pl_call  fun18(&seed[61],0)
	pl_call  fun18(&seed[54],0)
	pl_call  fun18(&seed[34],0)
	pl_call  fun18(&seed[64],0)
	pl_call  fun18(&seed[67],0)
	pl_call  fun18(&seed[29],0)
	pl_call  fun18(&seed[60],0)
	pl_call  fun18(&seed[59],0)
	pl_call  fun18(&seed[58],0)
	pl_call  fun18(&seed[57],0)
	pl_call  fun18(&seed[33],0)
	pl_call  fun18(&seed[37],0)
	pl_call  fun18(&seed[23],0)
	pl_call  fun18(&seed[28],0)
	pl_call  fun18(&seed[38],0)
	pl_call  fun18(&seed[24],0)
	pl_call  fun18(&seed[5],0)
	pl_call  fun18(&seed[1],0)
	pl_call  fun18(&seed[2],0)
	pl_call  fun18(&seed[6],0)
	pl_call  fun18(&seed[0],0)
	pl_call  fun18(&seed[11],0)
	pl_call  fun18(&seed[10],0)
	pl_call  fun18(&seed[9],0)
	pl_call  fun18(&seed[8],0)
	pl_call  fun18(&seed[7],0)
	pl_call  fun18(&seed[4],0)
	pl_call  fun18(&seed[3],0)
	call_c   Dyam_Loading_Reset()
	c_ret

pl_code global pred_check_env_renaming_2
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(2)
	call_c   Dyam_Choice(fun87)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(1),V(2))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Deallocate()
	pl_ret

pl_code global pred_check_tag_3
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Allocate_Layer()
	call_c   Dyam_Multi_Reg_Bind_ZeroOne(3)
	call_c   Dyam_Choice(fun13)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(1),I(0))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(3),V(2))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret


;;------------------ VIEWERS ------------

c_code local build_viewers
	call_c   Dyam_Load_Viewer(&ref[3],&ref[1])
	c_ret

c_code local build_seed_3
	ret_reg &seed[3]
	call_c   build_ref_4()
	call_c   build_ref_8()
	call_c   Dyam_Seed_Start(&ref[4],&ref[8],I(0),fun1,1)
	call_c   build_ref_9()
	call_c   Dyam_Seed_Add_Comp(&ref[9],fun0,0)
	call_c   Dyam_Seed_End()
	move_ret seed[3]
	c_ret

;; TERM 9: '*PROLOG-ITEM*'{top=> unfold('*SETCUT*', _B, _C, setcut, _B), cont=> _A}
c_code local build_ref_9
	ret_reg &ref[9]
	call_c   build_ref_724()
	call_c   build_ref_6()
	call_c   Dyam_Create_Binary(&ref[724],&ref[6],V(0))
	move_ret ref[9]
	c_ret

;; TERM 6: unfold('*SETCUT*', _B, _C, setcut, _B)
c_code local build_ref_6
	ret_reg &ref[6]
	call_c   build_ref_725()
	call_c   build_ref_726()
	call_c   build_ref_727()
	call_c   Dyam_Term_Start(&ref[725],5)
	call_c   Dyam_Term_Arg(&ref[726])
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(&ref[727])
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_End()
	move_ret ref[6]
	c_ret

;; TERM 727: setcut
c_code local build_ref_727
	ret_reg &ref[727]
	call_c   Dyam_Create_Atom("setcut")
	move_ret ref[727]
	c_ret

;; TERM 726: '*SETCUT*'
c_code local build_ref_726
	ret_reg &ref[726]
	call_c   Dyam_Create_Atom("*SETCUT*")
	move_ret ref[726]
	c_ret

;; TERM 725: unfold
c_code local build_ref_725
	ret_reg &ref[725]
	call_c   Dyam_Create_Atom("unfold")
	move_ret ref[725]
	c_ret

;; TERM 724: '*PROLOG-ITEM*'!'$ft'
c_code local build_ref_724
	ret_reg &ref[724]
	call_c   build_ref_59()
	call_c   Dyam_Create_Atom_Module("$ft",&ref[59])
	move_ret ref[724]
	c_ret

;; TERM 59: '*PROLOG-ITEM*'
c_code local build_ref_59
	ret_reg &ref[59]
	call_c   Dyam_Create_Atom("*PROLOG-ITEM*")
	move_ret ref[59]
	c_ret

pl_code local fun0
	call_c   build_ref_9()
	call_c   Dyam_Unify_Item(&ref[9])
	fail_ret
	pl_jump  Follow_Cont(V(0))

pl_code local fun1
	pl_ret

;; TERM 8: '*PROLOG-FIRST*'(unfold('*SETCUT*', _B, _C, setcut, _B)) :> []
c_code local build_ref_8
	ret_reg &ref[8]
	call_c   build_ref_7()
	call_c   Dyam_Create_Binary(I(9),&ref[7],I(0))
	move_ret ref[8]
	c_ret

;; TERM 7: '*PROLOG-FIRST*'(unfold('*SETCUT*', _B, _C, setcut, _B))
c_code local build_ref_7
	ret_reg &ref[7]
	call_c   build_ref_5()
	call_c   build_ref_6()
	call_c   Dyam_Create_Unary(&ref[5],&ref[6])
	move_ret ref[7]
	c_ret

;; TERM 5: '*PROLOG-FIRST*'
c_code local build_ref_5
	ret_reg &ref[5]
	call_c   Dyam_Create_Atom("*PROLOG-FIRST*")
	move_ret ref[5]
	c_ret

;; TERM 4: '*PROLOG_FIRST*'
c_code local build_ref_4
	ret_reg &ref[4]
	call_c   Dyam_Create_Atom("*PROLOG_FIRST*")
	move_ret ref[4]
	c_ret

c_code local build_seed_4
	ret_reg &seed[4]
	call_c   build_ref_4()
	call_c   build_ref_12()
	call_c   Dyam_Seed_Start(&ref[4],&ref[12],I(0),fun1,1)
	call_c   build_ref_13()
	call_c   Dyam_Seed_Add_Comp(&ref[13],fun2,0)
	call_c   Dyam_Seed_End()
	move_ret seed[4]
	c_ret

;; TERM 13: '*PROLOG-ITEM*'{top=> unfold('*CUT*', _B, _C, cut, _B), cont=> _A}
c_code local build_ref_13
	ret_reg &ref[13]
	call_c   build_ref_724()
	call_c   build_ref_10()
	call_c   Dyam_Create_Binary(&ref[724],&ref[10],V(0))
	move_ret ref[13]
	c_ret

;; TERM 10: unfold('*CUT*', _B, _C, cut, _B)
c_code local build_ref_10
	ret_reg &ref[10]
	call_c   build_ref_725()
	call_c   build_ref_728()
	call_c   build_ref_729()
	call_c   Dyam_Term_Start(&ref[725],5)
	call_c   Dyam_Term_Arg(&ref[728])
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(&ref[729])
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_End()
	move_ret ref[10]
	c_ret

;; TERM 729: cut
c_code local build_ref_729
	ret_reg &ref[729]
	call_c   Dyam_Create_Atom("cut")
	move_ret ref[729]
	c_ret

;; TERM 728: '*CUT*'
c_code local build_ref_728
	ret_reg &ref[728]
	call_c   Dyam_Create_Atom("*CUT*")
	move_ret ref[728]
	c_ret

pl_code local fun2
	call_c   build_ref_13()
	call_c   Dyam_Unify_Item(&ref[13])
	fail_ret
	pl_jump  Follow_Cont(V(0))

;; TERM 12: '*PROLOG-FIRST*'(unfold('*CUT*', _B, _C, cut, _B)) :> []
c_code local build_ref_12
	ret_reg &ref[12]
	call_c   build_ref_11()
	call_c   Dyam_Create_Binary(I(9),&ref[11],I(0))
	move_ret ref[12]
	c_ret

;; TERM 11: '*PROLOG-FIRST*'(unfold('*CUT*', _B, _C, cut, _B))
c_code local build_ref_11
	ret_reg &ref[11]
	call_c   build_ref_5()
	call_c   build_ref_10()
	call_c   Dyam_Create_Unary(&ref[5],&ref[10])
	move_ret ref[11]
	c_ret

c_code local build_seed_7
	ret_reg &seed[7]
	call_c   build_ref_4()
	call_c   build_ref_16()
	call_c   Dyam_Seed_Start(&ref[4],&ref[16],I(0),fun1,1)
	call_c   build_ref_17()
	call_c   Dyam_Seed_Add_Comp(&ref[17],fun3,0)
	call_c   Dyam_Seed_End()
	move_ret seed[7]
	c_ret

;; TERM 17: '*PROLOG-ITEM*'{top=> unfold(allocate, _B, _C, allocate, _B), cont=> _A}
c_code local build_ref_17
	ret_reg &ref[17]
	call_c   build_ref_724()
	call_c   build_ref_14()
	call_c   Dyam_Create_Binary(&ref[724],&ref[14],V(0))
	move_ret ref[17]
	c_ret

;; TERM 14: unfold(allocate, _B, _C, allocate, _B)
c_code local build_ref_14
	ret_reg &ref[14]
	call_c   build_ref_725()
	call_c   build_ref_299()
	call_c   Dyam_Term_Start(&ref[725],5)
	call_c   Dyam_Term_Arg(&ref[299])
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(&ref[299])
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_End()
	move_ret ref[14]
	c_ret

;; TERM 299: allocate
c_code local build_ref_299
	ret_reg &ref[299]
	call_c   Dyam_Create_Atom("allocate")
	move_ret ref[299]
	c_ret

pl_code local fun3
	call_c   build_ref_17()
	call_c   Dyam_Unify_Item(&ref[17])
	fail_ret
	pl_jump  Follow_Cont(V(0))

;; TERM 16: '*PROLOG-FIRST*'(unfold(allocate, _B, _C, allocate, _B)) :> []
c_code local build_ref_16
	ret_reg &ref[16]
	call_c   build_ref_15()
	call_c   Dyam_Create_Binary(I(9),&ref[15],I(0))
	move_ret ref[16]
	c_ret

;; TERM 15: '*PROLOG-FIRST*'(unfold(allocate, _B, _C, allocate, _B))
c_code local build_ref_15
	ret_reg &ref[15]
	call_c   build_ref_5()
	call_c   build_ref_14()
	call_c   Dyam_Create_Unary(&ref[5],&ref[14])
	move_ret ref[15]
	c_ret

c_code local build_seed_8
	ret_reg &seed[8]
	call_c   build_ref_4()
	call_c   build_ref_20()
	call_c   Dyam_Seed_Start(&ref[4],&ref[20],I(0),fun1,1)
	call_c   build_ref_21()
	call_c   Dyam_Seed_Add_Comp(&ref[21],fun4,0)
	call_c   Dyam_Seed_End()
	move_ret seed[8]
	c_ret

;; TERM 21: '*PROLOG-ITEM*'{top=> unfold(deallocate_alt, _B, _C, deallocate_alt, _B), cont=> _A}
c_code local build_ref_21
	ret_reg &ref[21]
	call_c   build_ref_724()
	call_c   build_ref_18()
	call_c   Dyam_Create_Binary(&ref[724],&ref[18],V(0))
	move_ret ref[21]
	c_ret

;; TERM 18: unfold(deallocate_alt, _B, _C, deallocate_alt, _B)
c_code local build_ref_18
	ret_reg &ref[18]
	call_c   build_ref_725()
	call_c   build_ref_730()
	call_c   Dyam_Term_Start(&ref[725],5)
	call_c   Dyam_Term_Arg(&ref[730])
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(&ref[730])
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_End()
	move_ret ref[18]
	c_ret

;; TERM 730: deallocate_alt
c_code local build_ref_730
	ret_reg &ref[730]
	call_c   Dyam_Create_Atom("deallocate_alt")
	move_ret ref[730]
	c_ret

pl_code local fun4
	call_c   build_ref_21()
	call_c   Dyam_Unify_Item(&ref[21])
	fail_ret
	pl_jump  Follow_Cont(V(0))

;; TERM 20: '*PROLOG-FIRST*'(unfold(deallocate_alt, _B, _C, deallocate_alt, _B)) :> []
c_code local build_ref_20
	ret_reg &ref[20]
	call_c   build_ref_19()
	call_c   Dyam_Create_Binary(I(9),&ref[19],I(0))
	move_ret ref[20]
	c_ret

;; TERM 19: '*PROLOG-FIRST*'(unfold(deallocate_alt, _B, _C, deallocate_alt, _B))
c_code local build_ref_19
	ret_reg &ref[19]
	call_c   build_ref_5()
	call_c   build_ref_18()
	call_c   Dyam_Create_Unary(&ref[5],&ref[18])
	move_ret ref[19]
	c_ret

c_code local build_seed_9
	ret_reg &seed[9]
	call_c   build_ref_4()
	call_c   build_ref_24()
	call_c   Dyam_Seed_Start(&ref[4],&ref[24],I(0),fun1,1)
	call_c   build_ref_25()
	call_c   Dyam_Seed_Add_Comp(&ref[25],fun5,0)
	call_c   Dyam_Seed_End()
	move_ret seed[9]
	c_ret

;; TERM 25: '*PROLOG-ITEM*'{top=> unfold(deallocate, _B, _C, deallocate, _B), cont=> _A}
c_code local build_ref_25
	ret_reg &ref[25]
	call_c   build_ref_724()
	call_c   build_ref_22()
	call_c   Dyam_Create_Binary(&ref[724],&ref[22],V(0))
	move_ret ref[25]
	c_ret

;; TERM 22: unfold(deallocate, _B, _C, deallocate, _B)
c_code local build_ref_22
	ret_reg &ref[22]
	call_c   build_ref_725()
	call_c   build_ref_300()
	call_c   Dyam_Term_Start(&ref[725],5)
	call_c   Dyam_Term_Arg(&ref[300])
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(&ref[300])
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_End()
	move_ret ref[22]
	c_ret

;; TERM 300: deallocate
c_code local build_ref_300
	ret_reg &ref[300]
	call_c   Dyam_Create_Atom("deallocate")
	move_ret ref[300]
	c_ret

pl_code local fun5
	call_c   build_ref_25()
	call_c   Dyam_Unify_Item(&ref[25])
	fail_ret
	pl_jump  Follow_Cont(V(0))

;; TERM 24: '*PROLOG-FIRST*'(unfold(deallocate, _B, _C, deallocate, _B)) :> []
c_code local build_ref_24
	ret_reg &ref[24]
	call_c   build_ref_23()
	call_c   Dyam_Create_Binary(I(9),&ref[23],I(0))
	move_ret ref[24]
	c_ret

;; TERM 23: '*PROLOG-FIRST*'(unfold(deallocate, _B, _C, deallocate, _B))
c_code local build_ref_23
	ret_reg &ref[23]
	call_c   build_ref_5()
	call_c   build_ref_22()
	call_c   Dyam_Create_Unary(&ref[5],&ref[22])
	move_ret ref[23]
	c_ret

c_code local build_seed_10
	ret_reg &seed[10]
	call_c   build_ref_4()
	call_c   build_ref_28()
	call_c   Dyam_Seed_Start(&ref[4],&ref[28],I(0),fun1,1)
	call_c   build_ref_29()
	call_c   Dyam_Seed_Add_Comp(&ref[29],fun6,0)
	call_c   Dyam_Seed_End()
	move_ret seed[10]
	c_ret

;; TERM 29: '*PROLOG-ITEM*'{top=> unfold(succeed, _B, _C, succeed, _B), cont=> _A}
c_code local build_ref_29
	ret_reg &ref[29]
	call_c   build_ref_724()
	call_c   build_ref_26()
	call_c   Dyam_Create_Binary(&ref[724],&ref[26],V(0))
	move_ret ref[29]
	c_ret

;; TERM 26: unfold(succeed, _B, _C, succeed, _B)
c_code local build_ref_26
	ret_reg &ref[26]
	call_c   build_ref_725()
	call_c   build_ref_301()
	call_c   Dyam_Term_Start(&ref[725],5)
	call_c   Dyam_Term_Arg(&ref[301])
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(&ref[301])
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_End()
	move_ret ref[26]
	c_ret

;; TERM 301: succeed
c_code local build_ref_301
	ret_reg &ref[301]
	call_c   Dyam_Create_Atom("succeed")
	move_ret ref[301]
	c_ret

pl_code local fun6
	call_c   build_ref_29()
	call_c   Dyam_Unify_Item(&ref[29])
	fail_ret
	pl_jump  Follow_Cont(V(0))

;; TERM 28: '*PROLOG-FIRST*'(unfold(succeed, _B, _C, succeed, _B)) :> []
c_code local build_ref_28
	ret_reg &ref[28]
	call_c   build_ref_27()
	call_c   Dyam_Create_Binary(I(9),&ref[27],I(0))
	move_ret ref[28]
	c_ret

;; TERM 27: '*PROLOG-FIRST*'(unfold(succeed, _B, _C, succeed, _B))
c_code local build_ref_27
	ret_reg &ref[27]
	call_c   build_ref_5()
	call_c   build_ref_26()
	call_c   Dyam_Create_Unary(&ref[5],&ref[26])
	move_ret ref[27]
	c_ret

c_code local build_seed_11
	ret_reg &seed[11]
	call_c   build_ref_4()
	call_c   build_ref_32()
	call_c   Dyam_Seed_Start(&ref[4],&ref[32],I(0),fun1,1)
	call_c   build_ref_33()
	call_c   Dyam_Seed_Add_Comp(&ref[33],fun7,0)
	call_c   Dyam_Seed_End()
	move_ret seed[11]
	c_ret

;; TERM 33: '*PROLOG-ITEM*'{top=> unfold(noop, _B, _C, noop, _B), cont=> _A}
c_code local build_ref_33
	ret_reg &ref[33]
	call_c   build_ref_724()
	call_c   build_ref_30()
	call_c   Dyam_Create_Binary(&ref[724],&ref[30],V(0))
	move_ret ref[33]
	c_ret

;; TERM 30: unfold(noop, _B, _C, noop, _B)
c_code local build_ref_30
	ret_reg &ref[30]
	call_c   build_ref_725()
	call_c   build_ref_680()
	call_c   Dyam_Term_Start(&ref[725],5)
	call_c   Dyam_Term_Arg(&ref[680])
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(&ref[680])
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_End()
	move_ret ref[30]
	c_ret

;; TERM 680: noop
c_code local build_ref_680
	ret_reg &ref[680]
	call_c   Dyam_Create_Atom("noop")
	move_ret ref[680]
	c_ret

pl_code local fun7
	call_c   build_ref_33()
	call_c   Dyam_Unify_Item(&ref[33])
	fail_ret
	pl_jump  Follow_Cont(V(0))

;; TERM 32: '*PROLOG-FIRST*'(unfold(noop, _B, _C, noop, _B)) :> []
c_code local build_ref_32
	ret_reg &ref[32]
	call_c   build_ref_31()
	call_c   Dyam_Create_Binary(I(9),&ref[31],I(0))
	move_ret ref[32]
	c_ret

;; TERM 31: '*PROLOG-FIRST*'(unfold(noop, _B, _C, noop, _B))
c_code local build_ref_31
	ret_reg &ref[31]
	call_c   build_ref_5()
	call_c   build_ref_30()
	call_c   Dyam_Create_Unary(&ref[5],&ref[30])
	move_ret ref[31]
	c_ret

c_code local build_seed_0
	ret_reg &seed[0]
	call_c   build_ref_4()
	call_c   build_ref_36()
	call_c   Dyam_Seed_Start(&ref[4],&ref[36],I(0),fun1,1)
	call_c   build_ref_37()
	call_c   Dyam_Seed_Add_Comp(&ref[37],fun8,0)
	call_c   Dyam_Seed_End()
	move_ret seed[0]
	c_ret

;; TERM 37: '*PROLOG-ITEM*'{top=> unfold('*NOOP*'(_B), _C, _D, noop, _C), cont=> _A}
c_code local build_ref_37
	ret_reg &ref[37]
	call_c   build_ref_724()
	call_c   build_ref_34()
	call_c   Dyam_Create_Binary(&ref[724],&ref[34],V(0))
	move_ret ref[37]
	c_ret

;; TERM 34: unfold('*NOOP*'(_B), _C, _D, noop, _C)
c_code local build_ref_34
	ret_reg &ref[34]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_731()
	call_c   Dyam_Create_Unary(&ref[731],V(1))
	move_ret R(0)
	call_c   build_ref_725()
	call_c   build_ref_680()
	call_c   Dyam_Term_Start(&ref[725],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(&ref[680])
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_End()
	move_ret ref[34]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 731: '*NOOP*'
c_code local build_ref_731
	ret_reg &ref[731]
	call_c   Dyam_Create_Atom("*NOOP*")
	move_ret ref[731]
	c_ret

pl_code local fun8
	call_c   build_ref_37()
	call_c   Dyam_Unify_Item(&ref[37])
	fail_ret
	pl_jump  Follow_Cont(V(0))

;; TERM 36: '*PROLOG-FIRST*'(unfold('*NOOP*'(_B), _C, _D, noop, _C)) :> []
c_code local build_ref_36
	ret_reg &ref[36]
	call_c   build_ref_35()
	call_c   Dyam_Create_Binary(I(9),&ref[35],I(0))
	move_ret ref[36]
	c_ret

;; TERM 35: '*PROLOG-FIRST*'(unfold('*NOOP*'(_B), _C, _D, noop, _C))
c_code local build_ref_35
	ret_reg &ref[35]
	call_c   build_ref_5()
	call_c   build_ref_34()
	call_c   Dyam_Create_Unary(&ref[5],&ref[34])
	move_ret ref[35]
	c_ret

c_code local build_seed_6
	ret_reg &seed[6]
	call_c   build_ref_4()
	call_c   build_ref_40()
	call_c   Dyam_Seed_Start(&ref[4],&ref[40],I(0),fun1,1)
	call_c   build_ref_41()
	call_c   Dyam_Seed_Add_Comp(&ref[41],fun9,0)
	call_c   Dyam_Seed_End()
	move_ret seed[6]
	c_ret

;; TERM 41: '*PROLOG-ITEM*'{top=> unfold(fail, _B, _C, (fail :> _C), _B), cont=> _A}
c_code local build_ref_41
	ret_reg &ref[41]
	call_c   build_ref_724()
	call_c   build_ref_38()
	call_c   Dyam_Create_Binary(&ref[724],&ref[38],V(0))
	move_ret ref[41]
	c_ret

;; TERM 38: unfold(fail, _B, _C, (fail :> _C), _B)
c_code local build_ref_38
	ret_reg &ref[38]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_424()
	call_c   Dyam_Create_Binary(I(9),&ref[424],V(2))
	move_ret R(0)
	call_c   build_ref_725()
	call_c   Dyam_Term_Start(&ref[725],5)
	call_c   Dyam_Term_Arg(&ref[424])
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_End()
	move_ret ref[38]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 424: fail
c_code local build_ref_424
	ret_reg &ref[424]
	call_c   Dyam_Create_Atom("fail")
	move_ret ref[424]
	c_ret

pl_code local fun9
	call_c   build_ref_41()
	call_c   Dyam_Unify_Item(&ref[41])
	fail_ret
	pl_jump  Follow_Cont(V(0))

;; TERM 40: '*PROLOG-FIRST*'(unfold(fail, _B, _C, (fail :> _C), _B)) :> []
c_code local build_ref_40
	ret_reg &ref[40]
	call_c   build_ref_39()
	call_c   Dyam_Create_Binary(I(9),&ref[39],I(0))
	move_ret ref[40]
	c_ret

;; TERM 39: '*PROLOG-FIRST*'(unfold(fail, _B, _C, (fail :> _C), _B))
c_code local build_ref_39
	ret_reg &ref[39]
	call_c   build_ref_5()
	call_c   build_ref_38()
	call_c   Dyam_Create_Unary(&ref[5],&ref[38])
	move_ret ref[39]
	c_ret

c_code local build_seed_2
	ret_reg &seed[2]
	call_c   build_ref_4()
	call_c   build_ref_44()
	call_c   Dyam_Seed_Start(&ref[4],&ref[44],I(0),fun1,1)
	call_c   build_ref_45()
	call_c   Dyam_Seed_Add_Comp(&ref[45],fun10,0)
	call_c   Dyam_Seed_End()
	move_ret seed[2]
	c_ret

;; TERM 45: '*PROLOG-ITEM*'{top=> unfold('*SAFE-EXIT*', _B, _C, (reg_reset(0) :> deallocate :> succeed), _B), cont=> _A}
c_code local build_ref_45
	ret_reg &ref[45]
	call_c   build_ref_724()
	call_c   build_ref_42()
	call_c   Dyam_Create_Binary(&ref[724],&ref[42],V(0))
	move_ret ref[45]
	c_ret

;; TERM 42: unfold('*SAFE-EXIT*', _B, _C, (reg_reset(0) :> deallocate :> succeed), _B)
c_code local build_ref_42
	ret_reg &ref[42]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_733()
	call_c   Dyam_Create_Unary(&ref[733],N(0))
	move_ret R(0)
	call_c   build_ref_302()
	call_c   Dyam_Create_Binary(I(9),R(0),&ref[302])
	move_ret R(0)
	call_c   build_ref_725()
	call_c   build_ref_732()
	call_c   Dyam_Term_Start(&ref[725],5)
	call_c   Dyam_Term_Arg(&ref[732])
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_End()
	move_ret ref[42]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 732: '*SAFE-EXIT*'
c_code local build_ref_732
	ret_reg &ref[732]
	call_c   Dyam_Create_Atom("*SAFE-EXIT*")
	move_ret ref[732]
	c_ret

;; TERM 302: deallocate :> succeed
c_code local build_ref_302
	ret_reg &ref[302]
	call_c   build_ref_300()
	call_c   build_ref_301()
	call_c   Dyam_Create_Binary(I(9),&ref[300],&ref[301])
	move_ret ref[302]
	c_ret

;; TERM 733: reg_reset
c_code local build_ref_733
	ret_reg &ref[733]
	call_c   Dyam_Create_Atom("reg_reset")
	move_ret ref[733]
	c_ret

pl_code local fun10
	call_c   build_ref_45()
	call_c   Dyam_Unify_Item(&ref[45])
	fail_ret
	pl_jump  Follow_Cont(V(0))

;; TERM 44: '*PROLOG-FIRST*'(unfold('*SAFE-EXIT*', _B, _C, (reg_reset(0) :> deallocate :> succeed), _B)) :> []
c_code local build_ref_44
	ret_reg &ref[44]
	call_c   build_ref_43()
	call_c   Dyam_Create_Binary(I(9),&ref[43],I(0))
	move_ret ref[44]
	c_ret

;; TERM 43: '*PROLOG-FIRST*'(unfold('*SAFE-EXIT*', _B, _C, (reg_reset(0) :> deallocate :> succeed), _B))
c_code local build_ref_43
	ret_reg &ref[43]
	call_c   build_ref_5()
	call_c   build_ref_42()
	call_c   Dyam_Create_Unary(&ref[5],&ref[42])
	move_ret ref[43]
	c_ret

c_code local build_seed_1
	ret_reg &seed[1]
	call_c   build_ref_4()
	call_c   build_ref_48()
	call_c   Dyam_Seed_Start(&ref[4],&ref[48],I(0),fun1,1)
	call_c   build_ref_49()
	call_c   Dyam_Seed_Add_Comp(&ref[49],fun11,0)
	call_c   Dyam_Seed_End()
	move_ret seed[1]
	c_ret

;; TERM 49: '*PROLOG-ITEM*'{top=> unfold('*KLEENE-CALL*'(_B, _C), _D, _E, call(_B, []), _D), cont=> _A}
c_code local build_ref_49
	ret_reg &ref[49]
	call_c   build_ref_724()
	call_c   build_ref_46()
	call_c   Dyam_Create_Binary(&ref[724],&ref[46],V(0))
	move_ret ref[49]
	c_ret

;; TERM 46: unfold('*KLEENE-CALL*'(_B, _C), _D, _E, call(_B, []), _D)
c_code local build_ref_46
	ret_reg &ref[46]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_734()
	call_c   Dyam_Create_Binary(&ref[734],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_725()
	call_c   build_ref_617()
	call_c   Dyam_Term_Start(&ref[725],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(&ref[617])
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[46]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 617: call(_B, [])
c_code local build_ref_617
	ret_reg &ref[617]
	call_c   build_ref_735()
	call_c   Dyam_Create_Binary(&ref[735],V(1),I(0))
	move_ret ref[617]
	c_ret

;; TERM 735: call
c_code local build_ref_735
	ret_reg &ref[735]
	call_c   Dyam_Create_Atom("call")
	move_ret ref[735]
	c_ret

;; TERM 734: '*KLEENE-CALL*'
c_code local build_ref_734
	ret_reg &ref[734]
	call_c   Dyam_Create_Atom("*KLEENE-CALL*")
	move_ret ref[734]
	c_ret

pl_code local fun11
	call_c   build_ref_49()
	call_c   Dyam_Unify_Item(&ref[49])
	fail_ret
	pl_jump  Follow_Cont(V(0))

;; TERM 48: '*PROLOG-FIRST*'(unfold('*KLEENE-CALL*'(_B, _C), _D, _E, call(_B, []), _D)) :> []
c_code local build_ref_48
	ret_reg &ref[48]
	call_c   build_ref_47()
	call_c   Dyam_Create_Binary(I(9),&ref[47],I(0))
	move_ret ref[48]
	c_ret

;; TERM 47: '*PROLOG-FIRST*'(unfold('*KLEENE-CALL*'(_B, _C), _D, _E, call(_B, []), _D))
c_code local build_ref_47
	ret_reg &ref[47]
	call_c   build_ref_5()
	call_c   build_ref_46()
	call_c   Dyam_Create_Unary(&ref[5],&ref[46])
	move_ret ref[47]
	c_ret

c_code local build_seed_5
	ret_reg &seed[5]
	call_c   build_ref_4()
	call_c   build_ref_52()
	call_c   Dyam_Seed_Start(&ref[4],&ref[52],I(0),fun1,1)
	call_c   build_ref_53()
	call_c   Dyam_Seed_Add_Comp(&ref[53],fun12,0)
	call_c   Dyam_Seed_End()
	move_ret seed[5]
	c_ret

;; TERM 53: '*PROLOG-ITEM*'{top=> unfold(failtest(_B, _C), _D, _E, failtest(_B, _C), _D), cont=> _A}
c_code local build_ref_53
	ret_reg &ref[53]
	call_c   build_ref_724()
	call_c   build_ref_50()
	call_c   Dyam_Create_Binary(&ref[724],&ref[50],V(0))
	move_ret ref[53]
	c_ret

;; TERM 50: unfold(failtest(_B, _C), _D, _E, failtest(_B, _C), _D)
c_code local build_ref_50
	ret_reg &ref[50]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_736()
	call_c   Dyam_Create_Binary(&ref[736],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_725()
	call_c   Dyam_Term_Start(&ref[725],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[50]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 736: failtest
c_code local build_ref_736
	ret_reg &ref[736]
	call_c   Dyam_Create_Atom("failtest")
	move_ret ref[736]
	c_ret

pl_code local fun12
	call_c   build_ref_53()
	call_c   Dyam_Unify_Item(&ref[53])
	fail_ret
	pl_jump  Follow_Cont(V(0))

;; TERM 52: '*PROLOG-FIRST*'(unfold(failtest(_B, _C), _D, _E, failtest(_B, _C), _D)) :> []
c_code local build_ref_52
	ret_reg &ref[52]
	call_c   build_ref_51()
	call_c   Dyam_Create_Binary(I(9),&ref[51],I(0))
	move_ret ref[52]
	c_ret

;; TERM 51: '*PROLOG-FIRST*'(unfold(failtest(_B, _C), _D, _E, failtest(_B, _C), _D))
c_code local build_ref_51
	ret_reg &ref[51]
	call_c   build_ref_5()
	call_c   build_ref_50()
	call_c   Dyam_Create_Unary(&ref[5],&ref[50])
	move_ret ref[51]
	c_ret

c_code local build_seed_24
	ret_reg &seed[24]
	call_c   build_ref_4()
	call_c   build_ref_57()
	call_c   Dyam_Seed_Start(&ref[4],&ref[57],I(0),fun1,1)
	call_c   build_ref_58()
	call_c   Dyam_Seed_Add_Comp(&ref[58],fun20,0)
	call_c   Dyam_Seed_End()
	move_ret seed[24]
	c_ret

;; TERM 58: '*PROLOG-ITEM*'{top=> unfold('*IL-LOOP-WRAP*'(_B), _C, _D, _E, _F), cont=> _A}
c_code local build_ref_58
	ret_reg &ref[58]
	call_c   build_ref_724()
	call_c   build_ref_55()
	call_c   Dyam_Create_Binary(&ref[724],&ref[55],V(0))
	move_ret ref[58]
	c_ret

;; TERM 55: unfold('*IL-LOOP-WRAP*'(_B), _C, _D, _E, _F)
c_code local build_ref_55
	ret_reg &ref[55]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_737()
	call_c   Dyam_Create_Unary(&ref[737],V(1))
	move_ret R(0)
	call_c   build_ref_725()
	call_c   Dyam_Term_Start(&ref[725],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[55]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 737: '*IL-LOOP-WRAP*'
c_code local build_ref_737
	ret_reg &ref[737]
	call_c   Dyam_Create_Atom("*IL-LOOP-WRAP*")
	move_ret ref[737]
	c_ret

c_code local build_seed_72
	ret_reg &seed[72]
	call_c   build_ref_59()
	call_c   build_ref_60()
	call_c   Dyam_Seed_Start(&ref[59],&ref[60],I(0),fun15,1)
	call_c   build_ref_63()
	call_c   Dyam_Seed_Add_Comp(&ref[63],&ref[60],0)
	call_c   Dyam_Seed_End()
	move_ret seed[72]
	c_ret

pl_code local fun15
	pl_jump  Complete(0,0)

;; TERM 63: '*PROLOG-FIRST*'(unfold(_B, _C, _D, _E, _F)) :> '$$HOLE$$'
c_code local build_ref_63
	ret_reg &ref[63]
	call_c   build_ref_62()
	call_c   Dyam_Create_Binary(I(9),&ref[62],I(7))
	move_ret ref[63]
	c_ret

;; TERM 62: '*PROLOG-FIRST*'(unfold(_B, _C, _D, _E, _F))
c_code local build_ref_62
	ret_reg &ref[62]
	call_c   build_ref_5()
	call_c   build_ref_61()
	call_c   Dyam_Create_Unary(&ref[5],&ref[61])
	move_ret ref[62]
	c_ret

;; TERM 61: unfold(_B, _C, _D, _E, _F)
c_code local build_ref_61
	ret_reg &ref[61]
	call_c   build_ref_725()
	call_c   Dyam_Term_Start(&ref[725],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[61]
	c_ret

;; TERM 60: '*PROLOG-ITEM*'{top=> unfold(_B, _C, _D, _E, _F), cont=> _A}
c_code local build_ref_60
	ret_reg &ref[60]
	call_c   build_ref_724()
	call_c   build_ref_61()
	call_c   Dyam_Create_Binary(&ref[724],&ref[61],V(0))
	move_ret ref[60]
	c_ret

pl_code local fun19
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Light_Object(R(0))
	pl_jump  Schedule_Prolog()

long local pool_fun20[3]=[2,build_ref_58,build_seed_72]

pl_code local fun20
	call_c   Dyam_Pool(pool_fun20)
	call_c   Dyam_Unify_Item(&ref[58])
	fail_ret
	pl_jump  fun19(&seed[72],12)

;; TERM 57: '*PROLOG-FIRST*'(unfold('*IL-LOOP-WRAP*'(_B), _C, _D, _E, _F)) :> []
c_code local build_ref_57
	ret_reg &ref[57]
	call_c   build_ref_56()
	call_c   Dyam_Create_Binary(I(9),&ref[56],I(0))
	move_ret ref[57]
	c_ret

;; TERM 56: '*PROLOG-FIRST*'(unfold('*IL-LOOP-WRAP*'(_B), _C, _D, _E, _F))
c_code local build_ref_56
	ret_reg &ref[56]
	call_c   build_ref_5()
	call_c   build_ref_55()
	call_c   Dyam_Create_Unary(&ref[5],&ref[55])
	move_ret ref[56]
	c_ret

c_code local build_seed_38
	ret_reg &seed[38]
	call_c   build_ref_4()
	call_c   build_ref_66()
	call_c   Dyam_Seed_Start(&ref[4],&ref[66],I(0),fun1,1)
	call_c   build_ref_67()
	call_c   Dyam_Seed_Add_Comp(&ref[67],fun16,0)
	call_c   Dyam_Seed_End()
	move_ret seed[38]
	c_ret

;; TERM 67: '*PROLOG-ITEM*'{top=> unfold('*PROLOG-LAST*', _B, _C, call('Follow_Cont', [_D]), _B), cont=> _A}
c_code local build_ref_67
	ret_reg &ref[67]
	call_c   build_ref_724()
	call_c   build_ref_64()
	call_c   Dyam_Create_Binary(&ref[724],&ref[64],V(0))
	move_ret ref[67]
	c_ret

;; TERM 64: unfold('*PROLOG-LAST*', _B, _C, call('Follow_Cont', [_D]), _B)
c_code local build_ref_64
	ret_reg &ref[64]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(3),I(0))
	move_ret R(0)
	call_c   build_ref_735()
	call_c   build_ref_738()
	call_c   Dyam_Create_Binary(&ref[735],&ref[738],R(0))
	move_ret R(0)
	call_c   build_ref_725()
	call_c   build_ref_514()
	call_c   Dyam_Term_Start(&ref[725],5)
	call_c   Dyam_Term_Arg(&ref[514])
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_End()
	move_ret ref[64]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 514: '*PROLOG-LAST*'
c_code local build_ref_514
	ret_reg &ref[514]
	call_c   Dyam_Create_Atom("*PROLOG-LAST*")
	move_ret ref[514]
	c_ret

;; TERM 738: 'Follow_Cont'
c_code local build_ref_738
	ret_reg &ref[738]
	call_c   Dyam_Create_Atom("Follow_Cont")
	move_ret ref[738]
	c_ret

pl_code local fun16
	call_c   build_ref_67()
	call_c   Dyam_Unify_Item(&ref[67])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(3))
	pl_call  pred_label_term_2()
	call_c   Dyam_Deallocate()
	pl_jump  Follow_Cont(V(0))

;; TERM 66: '*PROLOG-FIRST*'(unfold('*PROLOG-LAST*', _B, _C, call('Follow_Cont', [_D]), _B)) :> []
c_code local build_ref_66
	ret_reg &ref[66]
	call_c   build_ref_65()
	call_c   Dyam_Create_Binary(I(9),&ref[65],I(0))
	move_ret ref[66]
	c_ret

;; TERM 65: '*PROLOG-FIRST*'(unfold('*PROLOG-LAST*', _B, _C, call('Follow_Cont', [_D]), _B))
c_code local build_ref_65
	ret_reg &ref[65]
	call_c   build_ref_5()
	call_c   build_ref_64()
	call_c   Dyam_Create_Unary(&ref[5],&ref[64])
	move_ret ref[65]
	c_ret

c_code local build_seed_28
	ret_reg &seed[28]
	call_c   build_ref_4()
	call_c   build_ref_78()
	call_c   Dyam_Seed_Start(&ref[4],&ref[78],I(0),fun1,1)
	call_c   build_ref_79()
	call_c   Dyam_Seed_Add_Comp(&ref[79],fun21,0)
	call_c   Dyam_Seed_End()
	move_ret seed[28]
	c_ret

;; TERM 79: '*PROLOG-ITEM*'{top=> unfold('$TUPPLE'(_B, _C), _D, _E, _F, _G), cont=> _A}
c_code local build_ref_79
	ret_reg &ref[79]
	call_c   build_ref_724()
	call_c   build_ref_76()
	call_c   Dyam_Create_Binary(&ref[724],&ref[76],V(0))
	move_ret ref[79]
	c_ret

;; TERM 76: unfold('$TUPPLE'(_B, _C), _D, _E, _F, _G)
c_code local build_ref_76
	ret_reg &ref[76]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Binary(I(10),V(1),V(2))
	move_ret R(0)
	call_c   build_ref_725()
	call_c   Dyam_Term_Start(&ref[725],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[76]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_73
	ret_reg &seed[73]
	call_c   build_ref_59()
	call_c   build_ref_72()
	call_c   Dyam_Seed_Start(&ref[59],&ref[72],I(0),fun15,1)
	call_c   build_ref_75()
	call_c   Dyam_Seed_Add_Comp(&ref[75],&ref[72],0)
	call_c   Dyam_Seed_End()
	move_ret seed[73]
	c_ret

;; TERM 75: '*PROLOG-FIRST*'(unfold(_C, _D, _E, _F, _G)) :> '$$HOLE$$'
c_code local build_ref_75
	ret_reg &ref[75]
	call_c   build_ref_74()
	call_c   Dyam_Create_Binary(I(9),&ref[74],I(7))
	move_ret ref[75]
	c_ret

;; TERM 74: '*PROLOG-FIRST*'(unfold(_C, _D, _E, _F, _G))
c_code local build_ref_74
	ret_reg &ref[74]
	call_c   build_ref_5()
	call_c   build_ref_73()
	call_c   Dyam_Create_Unary(&ref[5],&ref[73])
	move_ret ref[74]
	c_ret

;; TERM 73: unfold(_C, _D, _E, _F, _G)
c_code local build_ref_73
	ret_reg &ref[73]
	call_c   build_ref_725()
	call_c   Dyam_Term_Start(&ref[725],5)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[73]
	c_ret

;; TERM 72: '*PROLOG-ITEM*'{top=> unfold(_C, _D, _E, _F, _G), cont=> _A}
c_code local build_ref_72
	ret_reg &ref[72]
	call_c   build_ref_724()
	call_c   build_ref_73()
	call_c   Dyam_Create_Binary(&ref[724],&ref[73],V(0))
	move_ret ref[72]
	c_ret

long local pool_fun21[3]=[2,build_ref_79,build_seed_73]

pl_code local fun21
	call_c   Dyam_Pool(pool_fun21)
	call_c   Dyam_Unify_Item(&ref[79])
	fail_ret
	pl_jump  fun19(&seed[73],12)

;; TERM 78: '*PROLOG-FIRST*'(unfold('$TUPPLE'(_B, _C), _D, _E, _F, _G)) :> []
c_code local build_ref_78
	ret_reg &ref[78]
	call_c   build_ref_77()
	call_c   Dyam_Create_Binary(I(9),&ref[77],I(0))
	move_ret ref[78]
	c_ret

;; TERM 77: '*PROLOG-FIRST*'(unfold('$TUPPLE'(_B, _C), _D, _E, _F, _G))
c_code local build_ref_77
	ret_reg &ref[77]
	call_c   build_ref_5()
	call_c   build_ref_76()
	call_c   Dyam_Create_Unary(&ref[5],&ref[76])
	move_ret ref[77]
	c_ret

c_code local build_seed_23
	ret_reg &seed[23]
	call_c   build_ref_4()
	call_c   build_ref_70()
	call_c   Dyam_Seed_Start(&ref[4],&ref[70],I(0),fun1,1)
	call_c   build_ref_71()
	call_c   Dyam_Seed_Add_Comp(&ref[71],fun22,0)
	call_c   Dyam_Seed_End()
	move_ret seed[23]
	c_ret

;; TERM 71: '*PROLOG-ITEM*'{top=> unfold('*HIDE-ARGS*'(_B, _C), _D, _E, _F, _G), cont=> _A}
c_code local build_ref_71
	ret_reg &ref[71]
	call_c   build_ref_724()
	call_c   build_ref_68()
	call_c   Dyam_Create_Binary(&ref[724],&ref[68],V(0))
	move_ret ref[71]
	c_ret

;; TERM 68: unfold('*HIDE-ARGS*'(_B, _C), _D, _E, _F, _G)
c_code local build_ref_68
	ret_reg &ref[68]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_739()
	call_c   Dyam_Create_Binary(&ref[739],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_725()
	call_c   Dyam_Term_Start(&ref[725],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[68]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 739: '*HIDE-ARGS*'
c_code local build_ref_739
	ret_reg &ref[739]
	call_c   Dyam_Create_Atom("*HIDE-ARGS*")
	move_ret ref[739]
	c_ret

long local pool_fun22[3]=[2,build_ref_71,build_seed_73]

pl_code local fun22
	call_c   Dyam_Pool(pool_fun22)
	call_c   Dyam_Unify_Item(&ref[71])
	fail_ret
	pl_jump  fun19(&seed[73],12)

;; TERM 70: '*PROLOG-FIRST*'(unfold('*HIDE-ARGS*'(_B, _C), _D, _E, _F, _G)) :> []
c_code local build_ref_70
	ret_reg &ref[70]
	call_c   build_ref_69()
	call_c   Dyam_Create_Binary(I(9),&ref[69],I(0))
	move_ret ref[70]
	c_ret

;; TERM 69: '*PROLOG-FIRST*'(unfold('*HIDE-ARGS*'(_B, _C), _D, _E, _F, _G))
c_code local build_ref_69
	ret_reg &ref[69]
	call_c   build_ref_5()
	call_c   build_ref_68()
	call_c   Dyam_Create_Unary(&ref[5],&ref[68])
	move_ret ref[69]
	c_ret

c_code local build_seed_37
	ret_reg &seed[37]
	call_c   build_ref_4()
	call_c   build_ref_82()
	call_c   Dyam_Seed_Start(&ref[4],&ref[82],I(0),fun1,1)
	call_c   build_ref_83()
	call_c   Dyam_Seed_Add_Comp(&ref[83],fun17,0)
	call_c   Dyam_Seed_End()
	move_ret seed[37]
	c_ret

;; TERM 83: '*PROLOG-ITEM*'{top=> unfold('*PROLOG-LAST*'(_B), _C, _D, call('Follow_Cont', [_E]), _C), cont=> _A}
c_code local build_ref_83
	ret_reg &ref[83]
	call_c   build_ref_724()
	call_c   build_ref_80()
	call_c   Dyam_Create_Binary(&ref[724],&ref[80],V(0))
	move_ret ref[83]
	c_ret

;; TERM 80: unfold('*PROLOG-LAST*'(_B), _C, _D, call('Follow_Cont', [_E]), _C)
c_code local build_ref_80
	ret_reg &ref[80]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_514()
	call_c   Dyam_Create_Unary(&ref[514],V(1))
	move_ret R(0)
	call_c   Dyam_Create_List(V(4),I(0))
	move_ret R(1)
	call_c   build_ref_735()
	call_c   build_ref_738()
	call_c   Dyam_Create_Binary(&ref[735],&ref[738],R(1))
	move_ret R(1)
	call_c   build_ref_725()
	call_c   Dyam_Term_Start(&ref[725],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_End()
	move_ret ref[80]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

pl_code local fun17
	call_c   build_ref_83()
	call_c   Dyam_Unify_Item(&ref[83])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(4))
	pl_call  pred_label_term_2()
	call_c   Dyam_Deallocate()
	pl_jump  Follow_Cont(V(0))

;; TERM 82: '*PROLOG-FIRST*'(unfold('*PROLOG-LAST*'(_B), _C, _D, call('Follow_Cont', [_E]), _C)) :> []
c_code local build_ref_82
	ret_reg &ref[82]
	call_c   build_ref_81()
	call_c   Dyam_Create_Binary(I(9),&ref[81],I(0))
	move_ret ref[82]
	c_ret

;; TERM 81: '*PROLOG-FIRST*'(unfold('*PROLOG-LAST*'(_B), _C, _D, call('Follow_Cont', [_E]), _C))
c_code local build_ref_81
	ret_reg &ref[81]
	call_c   build_ref_5()
	call_c   build_ref_80()
	call_c   Dyam_Create_Unary(&ref[5],&ref[80])
	move_ret ref[81]
	c_ret

c_code local build_seed_33
	ret_reg &seed[33]
	call_c   build_ref_4()
	call_c   build_ref_86()
	call_c   Dyam_Seed_Start(&ref[4],&ref[86],I(0),fun1,1)
	call_c   build_ref_87()
	call_c   Dyam_Seed_Add_Comp(&ref[87],fun23,0)
	call_c   Dyam_Seed_End()
	move_ret seed[33]
	c_ret

;; TERM 87: '*PROLOG-ITEM*'{top=> unfold('*INTERNAL_TAIL*'(_B), _C, _D, unify(_E, _F), _C), cont=> _A}
c_code local build_ref_87
	ret_reg &ref[87]
	call_c   build_ref_724()
	call_c   build_ref_84()
	call_c   Dyam_Create_Binary(&ref[724],&ref[84],V(0))
	move_ret ref[87]
	c_ret

;; TERM 84: unfold('*INTERNAL_TAIL*'(_B), _C, _D, unify(_E, _F), _C)
c_code local build_ref_84
	ret_reg &ref[84]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_740()
	call_c   Dyam_Create_Unary(&ref[740],V(1))
	move_ret R(0)
	call_c   build_ref_741()
	call_c   Dyam_Create_Binary(&ref[741],V(4),V(5))
	move_ret R(1)
	call_c   build_ref_725()
	call_c   Dyam_Term_Start(&ref[725],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_End()
	move_ret ref[84]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 741: unify
c_code local build_ref_741
	ret_reg &ref[741]
	call_c   Dyam_Create_Atom("unify")
	move_ret ref[741]
	c_ret

;; TERM 740: '*INTERNAL_TAIL*'
c_code local build_ref_740
	ret_reg &ref[740]
	call_c   Dyam_Create_Atom("*INTERNAL_TAIL*")
	move_ret ref[740]
	c_ret

pl_code local fun23
	call_c   build_ref_87()
	call_c   Dyam_Unify_Item(&ref[87])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(4))
	pl_call  pred_label_term_2()
	call_c   Dyam_Reg_Load(0,V(2))
	call_c   Dyam_Reg_Load(2,V(5))
	pl_call  pred_label_term_2()
	call_c   Dyam_Deallocate()
	pl_jump  Follow_Cont(V(0))

;; TERM 86: '*PROLOG-FIRST*'(unfold('*INTERNAL_TAIL*'(_B), _C, _D, unify(_E, _F), _C)) :> []
c_code local build_ref_86
	ret_reg &ref[86]
	call_c   build_ref_85()
	call_c   Dyam_Create_Binary(I(9),&ref[85],I(0))
	move_ret ref[86]
	c_ret

;; TERM 85: '*PROLOG-FIRST*'(unfold('*INTERNAL_TAIL*'(_B), _C, _D, unify(_E, _F), _C))
c_code local build_ref_85
	ret_reg &ref[85]
	call_c   build_ref_5()
	call_c   build_ref_84()
	call_c   Dyam_Create_Unary(&ref[5],&ref[84])
	move_ret ref[85]
	c_ret

c_code local build_seed_57
	ret_reg &seed[57]
	call_c   build_ref_4()
	call_c   build_ref_90()
	call_c   Dyam_Seed_Start(&ref[4],&ref[90],I(0),fun1,1)
	call_c   build_ref_91()
	call_c   Dyam_Seed_Add_Comp(&ref[91],fun24,0)
	call_c   Dyam_Seed_End()
	move_ret seed[57]
	c_ret

;; TERM 91: '*PROLOG-ITEM*'{top=> unfold(builtin(_B, _C), _D, _E, call(_C, _F), _D), cont=> _A}
c_code local build_ref_91
	ret_reg &ref[91]
	call_c   build_ref_724()
	call_c   build_ref_88()
	call_c   Dyam_Create_Binary(&ref[724],&ref[88],V(0))
	move_ret ref[91]
	c_ret

;; TERM 88: unfold(builtin(_B, _C), _D, _E, call(_C, _F), _D)
c_code local build_ref_88
	ret_reg &ref[88]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_742()
	call_c   Dyam_Create_Binary(&ref[742],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_735()
	call_c   Dyam_Create_Binary(&ref[735],V(2),V(5))
	move_ret R(1)
	call_c   build_ref_725()
	call_c   Dyam_Term_Start(&ref[725],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[88]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 742: builtin
c_code local build_ref_742
	ret_reg &ref[742]
	call_c   Dyam_Create_Atom("builtin")
	move_ret ref[742]
	c_ret

;; TERM 92: [_G|_H]
c_code local build_ref_92
	ret_reg &ref[92]
	call_c   Dyam_Create_List(V(6),V(7))
	move_ret ref[92]
	c_ret

long local pool_fun24[3]=[2,build_ref_91,build_ref_92]

pl_code local fun24
	call_c   Dyam_Pool(pool_fun24)
	call_c   Dyam_Unify_Item(&ref[91])
	fail_ret
	call_c   DYAM_evpred_univ(V(1),&ref[92])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(7))
	call_c   Dyam_Reg_Load(2,V(5))
	pl_call  pred_label_term_list_2()
	call_c   Dyam_Deallocate()
	pl_jump  Follow_Cont(V(0))

;; TERM 90: '*PROLOG-FIRST*'(unfold(builtin(_B, _C), _D, _E, call(_C, _F), _D)) :> []
c_code local build_ref_90
	ret_reg &ref[90]
	call_c   build_ref_89()
	call_c   Dyam_Create_Binary(I(9),&ref[89],I(0))
	move_ret ref[90]
	c_ret

;; TERM 89: '*PROLOG-FIRST*'(unfold(builtin(_B, _C), _D, _E, call(_C, _F), _D))
c_code local build_ref_89
	ret_reg &ref[89]
	call_c   build_ref_5()
	call_c   build_ref_88()
	call_c   Dyam_Create_Unary(&ref[5],&ref[88])
	move_ret ref[89]
	c_ret

c_code local build_seed_58
	ret_reg &seed[58]
	call_c   build_ref_4()
	call_c   build_ref_95()
	call_c   Dyam_Seed_Start(&ref[4],&ref[95],I(0),fun1,1)
	call_c   build_ref_96()
	call_c   Dyam_Seed_Add_Comp(&ref[96],fun25,0)
	call_c   Dyam_Seed_End()
	move_ret seed[58]
	c_ret

;; TERM 96: '*PROLOG-ITEM*'{top=> unfold(foreign_void_return(_B, _C), _D, _E, foreign_void_return(_C, _F), _D), cont=> _A}
c_code local build_ref_96
	ret_reg &ref[96]
	call_c   build_ref_724()
	call_c   build_ref_93()
	call_c   Dyam_Create_Binary(&ref[724],&ref[93],V(0))
	move_ret ref[96]
	c_ret

;; TERM 93: unfold(foreign_void_return(_B, _C), _D, _E, foreign_void_return(_C, _F), _D)
c_code local build_ref_93
	ret_reg &ref[93]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_743()
	call_c   Dyam_Create_Binary(&ref[743],V(1),V(2))
	move_ret R(0)
	call_c   Dyam_Create_Binary(&ref[743],V(2),V(5))
	move_ret R(1)
	call_c   build_ref_725()
	call_c   Dyam_Term_Start(&ref[725],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[93]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 743: foreign_void_return
c_code local build_ref_743
	ret_reg &ref[743]
	call_c   Dyam_Create_Atom("foreign_void_return")
	move_ret ref[743]
	c_ret

long local pool_fun25[3]=[2,build_ref_96,build_ref_92]

pl_code local fun25
	call_c   Dyam_Pool(pool_fun25)
	call_c   Dyam_Unify_Item(&ref[96])
	fail_ret
	call_c   DYAM_evpred_univ(V(1),&ref[92])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(7))
	call_c   Dyam_Reg_Load(2,V(5))
	pl_call  pred_label_term_list_2()
	call_c   Dyam_Deallocate()
	pl_jump  Follow_Cont(V(0))

;; TERM 95: '*PROLOG-FIRST*'(unfold(foreign_void_return(_B, _C), _D, _E, foreign_void_return(_C, _F), _D)) :> []
c_code local build_ref_95
	ret_reg &ref[95]
	call_c   build_ref_94()
	call_c   Dyam_Create_Binary(I(9),&ref[94],I(0))
	move_ret ref[95]
	c_ret

;; TERM 94: '*PROLOG-FIRST*'(unfold(foreign_void_return(_B, _C), _D, _E, foreign_void_return(_C, _F), _D))
c_code local build_ref_94
	ret_reg &ref[94]
	call_c   build_ref_5()
	call_c   build_ref_93()
	call_c   Dyam_Create_Unary(&ref[5],&ref[93])
	move_ret ref[94]
	c_ret

c_code local build_seed_59
	ret_reg &seed[59]
	call_c   build_ref_4()
	call_c   build_ref_99()
	call_c   Dyam_Seed_Start(&ref[4],&ref[99],I(0),fun1,1)
	call_c   build_ref_100()
	call_c   Dyam_Seed_Add_Comp(&ref[100],fun26,0)
	call_c   Dyam_Seed_End()
	move_ret seed[59]
	c_ret

;; TERM 100: '*PROLOG-ITEM*'{top=> unfold(foreign(_B, _C), _D, _E, foreign(_C, _F), _D), cont=> _A}
c_code local build_ref_100
	ret_reg &ref[100]
	call_c   build_ref_724()
	call_c   build_ref_97()
	call_c   Dyam_Create_Binary(&ref[724],&ref[97],V(0))
	move_ret ref[100]
	c_ret

;; TERM 97: unfold(foreign(_B, _C), _D, _E, foreign(_C, _F), _D)
c_code local build_ref_97
	ret_reg &ref[97]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_744()
	call_c   Dyam_Create_Binary(&ref[744],V(1),V(2))
	move_ret R(0)
	call_c   Dyam_Create_Binary(&ref[744],V(2),V(5))
	move_ret R(1)
	call_c   build_ref_725()
	call_c   Dyam_Term_Start(&ref[725],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[97]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 744: foreign
c_code local build_ref_744
	ret_reg &ref[744]
	call_c   Dyam_Create_Atom("foreign")
	move_ret ref[744]
	c_ret

long local pool_fun26[3]=[2,build_ref_100,build_ref_92]

pl_code local fun26
	call_c   Dyam_Pool(pool_fun26)
	call_c   Dyam_Unify_Item(&ref[100])
	fail_ret
	call_c   DYAM_evpred_univ(V(1),&ref[92])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(7))
	call_c   Dyam_Reg_Load(2,V(5))
	pl_call  pred_label_term_list_2()
	call_c   Dyam_Deallocate()
	pl_jump  Follow_Cont(V(0))

;; TERM 99: '*PROLOG-FIRST*'(unfold(foreign(_B, _C), _D, _E, foreign(_C, _F), _D)) :> []
c_code local build_ref_99
	ret_reg &ref[99]
	call_c   build_ref_98()
	call_c   Dyam_Create_Binary(I(9),&ref[98],I(0))
	move_ret ref[99]
	c_ret

;; TERM 98: '*PROLOG-FIRST*'(unfold(foreign(_B, _C), _D, _E, foreign(_C, _F), _D))
c_code local build_ref_98
	ret_reg &ref[98]
	call_c   build_ref_5()
	call_c   build_ref_97()
	call_c   Dyam_Create_Unary(&ref[5],&ref[97])
	move_ret ref[98]
	c_ret

c_code local build_seed_60
	ret_reg &seed[60]
	call_c   build_ref_4()
	call_c   build_ref_103()
	call_c   Dyam_Seed_Start(&ref[4],&ref[103],I(0),fun1,1)
	call_c   build_ref_104()
	call_c   Dyam_Seed_Add_Comp(&ref[104],fun27,0)
	call_c   Dyam_Seed_End()
	move_ret seed[60]
	c_ret

;; TERM 104: '*PROLOG-ITEM*'{top=> unfold(unify(_B, _C), _D, _E, unify(_F, _G), _D), cont=> _A}
c_code local build_ref_104
	ret_reg &ref[104]
	call_c   build_ref_724()
	call_c   build_ref_101()
	call_c   Dyam_Create_Binary(&ref[724],&ref[101],V(0))
	move_ret ref[104]
	c_ret

;; TERM 101: unfold(unify(_B, _C), _D, _E, unify(_F, _G), _D)
c_code local build_ref_101
	ret_reg &ref[101]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_741()
	call_c   Dyam_Create_Binary(&ref[741],V(1),V(2))
	move_ret R(0)
	call_c   Dyam_Create_Binary(&ref[741],V(5),V(6))
	move_ret R(1)
	call_c   build_ref_725()
	call_c   Dyam_Term_Start(&ref[725],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[101]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

pl_code local fun27
	call_c   build_ref_104()
	call_c   Dyam_Unify_Item(&ref[104])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(5))
	pl_call  pred_label_term_2()
	call_c   Dyam_Reg_Load(0,V(2))
	call_c   Dyam_Reg_Load(2,V(6))
	pl_call  pred_label_term_2()
	call_c   Dyam_Deallocate()
	pl_jump  Follow_Cont(V(0))

;; TERM 103: '*PROLOG-FIRST*'(unfold(unify(_B, _C), _D, _E, unify(_F, _G), _D)) :> []
c_code local build_ref_103
	ret_reg &ref[103]
	call_c   build_ref_102()
	call_c   Dyam_Create_Binary(I(9),&ref[102],I(0))
	move_ret ref[103]
	c_ret

;; TERM 102: '*PROLOG-FIRST*'(unfold(unify(_B, _C), _D, _E, unify(_F, _G), _D))
c_code local build_ref_102
	ret_reg &ref[102]
	call_c   build_ref_5()
	call_c   build_ref_101()
	call_c   Dyam_Create_Unary(&ref[5],&ref[101])
	move_ret ref[102]
	c_ret

c_code local build_seed_29
	ret_reg &seed[29]
	call_c   build_ref_4()
	call_c   build_ref_171()
	call_c   Dyam_Seed_Start(&ref[4],&ref[171],I(0),fun1,1)
	call_c   build_ref_172()
	call_c   Dyam_Seed_Add_Comp(&ref[172],fun31,0)
	call_c   Dyam_Seed_End()
	move_ret seed[29]
	c_ret

;; TERM 172: '*PROLOG-ITEM*'{top=> unfold('*IL-START*'(_B), _C, _D, (_E :> _F), _G), cont=> _A}
c_code local build_ref_172
	ret_reg &ref[172]
	call_c   build_ref_724()
	call_c   build_ref_169()
	call_c   Dyam_Create_Binary(&ref[724],&ref[169],V(0))
	move_ret ref[172]
	c_ret

;; TERM 169: unfold('*IL-START*'(_B), _C, _D, (_E :> _F), _G)
c_code local build_ref_169
	ret_reg &ref[169]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_745()
	call_c   Dyam_Create_Unary(&ref[745],V(1))
	move_ret R(0)
	call_c   Dyam_Create_Binary(I(9),V(4),V(5))
	move_ret R(1)
	call_c   build_ref_725()
	call_c   Dyam_Term_Start(&ref[725],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[169]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 745: '*IL-START*'
c_code local build_ref_745
	ret_reg &ref[745]
	call_c   Dyam_Create_Atom("*IL-START*")
	move_ret ref[745]
	c_ret

;; TERM 182: 'Bad interleave transition: ~w\n'
c_code local build_ref_182
	ret_reg &ref[182]
	call_c   Dyam_Create_Atom("Bad interleave transition: ~w\n")
	move_ret ref[182]
	c_ret

;; TERM 183: ['*IL-START*'(_B)]
c_code local build_ref_183
	ret_reg &ref[183]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_745()
	call_c   Dyam_Create_Unary(&ref[745],V(1))
	move_ret R(0)
	call_c   Dyam_Create_List(R(0),I(0))
	move_ret ref[183]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_83
	ret_reg &seed[83]
	call_c   build_ref_59()
	call_c   build_ref_178()
	call_c   Dyam_Seed_Start(&ref[59],&ref[178],I(0),fun15,1)
	call_c   build_ref_181()
	call_c   Dyam_Seed_Add_Comp(&ref[181],&ref[178],0)
	call_c   Dyam_Seed_End()
	move_ret seed[83]
	c_ret

;; TERM 181: '*PROLOG-FIRST*'(unfold(_I, _C, _D, _F, _J)) :> '$$HOLE$$'
c_code local build_ref_181
	ret_reg &ref[181]
	call_c   build_ref_180()
	call_c   Dyam_Create_Binary(I(9),&ref[180],I(7))
	move_ret ref[181]
	c_ret

;; TERM 180: '*PROLOG-FIRST*'(unfold(_I, _C, _D, _F, _J))
c_code local build_ref_180
	ret_reg &ref[180]
	call_c   build_ref_5()
	call_c   build_ref_179()
	call_c   Dyam_Create_Unary(&ref[5],&ref[179])
	move_ret ref[180]
	c_ret

;; TERM 179: unfold(_I, _C, _D, _F, _J)
c_code local build_ref_179
	ret_reg &ref[179]
	call_c   build_ref_725()
	call_c   Dyam_Term_Start(&ref[725],5)
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret ref[179]
	c_ret

;; TERM 178: '*PROLOG-ITEM*'{top=> unfold(_I, _C, _D, _F, _J), cont=> '$CLOSURE'('$fun'(28, 0, 1177358372), '$TUPPLE'(35125262024820))}
c_code local build_ref_178
	ret_reg &ref[178]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,326631424)
	move_ret R(0)
	call_c   Dyam_Closure_Aux(fun28,R(0))
	move_ret R(0)
	call_c   build_ref_724()
	call_c   build_ref_179()
	call_c   Dyam_Create_Binary(&ref[724],&ref[179],R(0))
	move_ret ref[178]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_82
	ret_reg &seed[82]
	call_c   build_ref_59()
	call_c   build_ref_174()
	call_c   Dyam_Seed_Start(&ref[59],&ref[174],I(0),fun15,1)
	call_c   build_ref_177()
	call_c   Dyam_Seed_Add_Comp(&ref[177],&ref[174],0)
	call_c   Dyam_Seed_End()
	move_ret seed[82]
	c_ret

;; TERM 177: '*PROLOG-FIRST*'(unfold(_H, _J, _K, _E, _G)) :> '$$HOLE$$'
c_code local build_ref_177
	ret_reg &ref[177]
	call_c   build_ref_176()
	call_c   Dyam_Create_Binary(I(9),&ref[176],I(7))
	move_ret ref[177]
	c_ret

;; TERM 176: '*PROLOG-FIRST*'(unfold(_H, _J, _K, _E, _G))
c_code local build_ref_176
	ret_reg &ref[176]
	call_c   build_ref_5()
	call_c   build_ref_175()
	call_c   Dyam_Create_Unary(&ref[5],&ref[175])
	move_ret ref[176]
	c_ret

;; TERM 175: unfold(_H, _J, _K, _E, _G)
c_code local build_ref_175
	ret_reg &ref[175]
	call_c   build_ref_725()
	call_c   Dyam_Term_Start(&ref[725],5)
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[175]
	c_ret

;; TERM 174: '*PROLOG-ITEM*'{top=> unfold(_H, _J, _K, _E, _G), cont=> _A}
c_code local build_ref_174
	ret_reg &ref[174]
	call_c   build_ref_724()
	call_c   build_ref_175()
	call_c   Dyam_Create_Binary(&ref[724],&ref[175],V(0))
	move_ret ref[174]
	c_ret

long local pool_fun28[2]=[1,build_seed_82]

pl_code local fun28
	call_c   Dyam_Pool(pool_fun28)
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(8))
	call_c   Dyam_Reg_Load(2,V(3))
	move     V(10), R(4)
	move     S(5), R(5)
	pl_call  pred_extend_tupple_3()
	call_c   Dyam_Deallocate()
	pl_jump  fun19(&seed[82],12)

long local pool_fun29[2]=[1,build_seed_83]

long local pool_fun30[4]=[65538,build_ref_182,build_ref_183,pool_fun29]

pl_code local fun30
	call_c   Dyam_Remove_Choice()
	move     &ref[182], R(0)
	move     0, R(1)
	move     &ref[183], R(2)
	move     S(5), R(3)
	pl_call  pred_internal_error_2()
fun29:
	call_c   Dyam_Deallocate()
	pl_jump  fun19(&seed[83],12)


;; TERM 173: _H :> _I
c_code local build_ref_173
	ret_reg &ref[173]
	call_c   Dyam_Create_Binary(I(9),V(7),V(8))
	move_ret ref[173]
	c_ret

long local pool_fun31[5]=[131074,build_ref_172,build_ref_173,pool_fun30,pool_fun29]

pl_code local fun31
	call_c   Dyam_Pool(pool_fun31)
	call_c   Dyam_Unify_Item(&ref[172])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun30)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[173])
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun29()

;; TERM 171: '*PROLOG-FIRST*'(unfold('*IL-START*'(_B), _C, _D, (_E :> _F), _G)) :> []
c_code local build_ref_171
	ret_reg &ref[171]
	call_c   build_ref_170()
	call_c   Dyam_Create_Binary(I(9),&ref[170],I(0))
	move_ret ref[171]
	c_ret

;; TERM 170: '*PROLOG-FIRST*'(unfold('*IL-START*'(_B), _C, _D, (_E :> _F), _G))
c_code local build_ref_170
	ret_reg &ref[170]
	call_c   build_ref_5()
	call_c   build_ref_169()
	call_c   Dyam_Create_Unary(&ref[5],&ref[169])
	move_ret ref[170]
	c_ret

c_code local build_seed_67
	ret_reg &seed[67]
	call_c   build_ref_4()
	call_c   build_ref_163()
	call_c   Dyam_Seed_Start(&ref[4],&ref[163],I(0),fun1,1)
	call_c   build_ref_164()
	call_c   Dyam_Seed_Add_Comp(&ref[164],fun53,0)
	call_c   Dyam_Seed_End()
	move_ret seed[67]
	c_ret

;; TERM 164: '*PROLOG-ITEM*'{top=> unfold('*OBJECT*'(_B, _C), _D, _E, _F, _D), cont=> _A}
c_code local build_ref_164
	ret_reg &ref[164]
	call_c   build_ref_724()
	call_c   build_ref_161()
	call_c   Dyam_Create_Binary(&ref[724],&ref[161],V(0))
	move_ret ref[164]
	c_ret

;; TERM 161: unfold('*OBJECT*'(_B, _C), _D, _E, _F, _D)
c_code local build_ref_161
	ret_reg &ref[161]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_746()
	call_c   Dyam_Create_Binary(&ref[746],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_725()
	call_c   Dyam_Term_Start(&ref[725],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[161]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 746: '*OBJECT*'
c_code local build_ref_746
	ret_reg &ref[746]
	call_c   Dyam_Create_Atom("*OBJECT*")
	move_ret ref[746]
	c_ret

c_code local build_seed_81
	ret_reg &seed[81]
	call_c   build_ref_59()
	call_c   build_ref_165()
	call_c   Dyam_Seed_Start(&ref[59],&ref[165],I(0),fun15,1)
	call_c   build_ref_168()
	call_c   Dyam_Seed_Add_Comp(&ref[168],&ref[165],0)
	call_c   Dyam_Seed_End()
	move_ret seed[81]
	c_ret

;; TERM 168: '*PROLOG-FIRST*'(seed_install(seed{model=> _B, id=> _G, anchor=> _H, subs_comp=> _I, code=> _J, go=> _K, tabule=> _L, schedule=> _M, subsume=> _N, model_ref=> _O, subs_comp_ref=> _P, c_type=> _Q, c_type_ref=> _R, out_env=> _S, appinfo=> _C, tail_flag=> _T}, 0, _F)) :> '$$HOLE$$'
c_code local build_ref_168
	ret_reg &ref[168]
	call_c   build_ref_167()
	call_c   Dyam_Create_Binary(I(9),&ref[167],I(7))
	move_ret ref[168]
	c_ret

;; TERM 167: '*PROLOG-FIRST*'(seed_install(seed{model=> _B, id=> _G, anchor=> _H, subs_comp=> _I, code=> _J, go=> _K, tabule=> _L, schedule=> _M, subsume=> _N, model_ref=> _O, subs_comp_ref=> _P, c_type=> _Q, c_type_ref=> _R, out_env=> _S, appinfo=> _C, tail_flag=> _T}, 0, _F))
c_code local build_ref_167
	ret_reg &ref[167]
	call_c   build_ref_5()
	call_c   build_ref_166()
	call_c   Dyam_Create_Unary(&ref[5],&ref[166])
	move_ret ref[167]
	c_ret

;; TERM 166: seed_install(seed{model=> _B, id=> _G, anchor=> _H, subs_comp=> _I, code=> _J, go=> _K, tabule=> _L, schedule=> _M, subsume=> _N, model_ref=> _O, subs_comp_ref=> _P, c_type=> _Q, c_type_ref=> _R, out_env=> _S, appinfo=> _C, tail_flag=> _T}, 0, _F)
c_code local build_ref_166
	ret_reg &ref[166]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_748()
	call_c   Dyam_Term_Start(&ref[748],16)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_747()
	call_c   Dyam_Term_Start(&ref[747],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(N(0))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[166]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 747: seed_install
c_code local build_ref_747
	ret_reg &ref[747]
	call_c   Dyam_Create_Atom("seed_install")
	move_ret ref[747]
	c_ret

;; TERM 748: seed!'$ft'
c_code local build_ref_748
	ret_reg &ref[748]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_765()
	call_c   Dyam_Create_List(&ref[765],I(0))
	move_ret R(0)
	call_c   build_ref_764()
	call_c   Dyam_Create_List(&ref[764],R(0))
	move_ret R(0)
	call_c   build_ref_763()
	call_c   Dyam_Create_List(&ref[763],R(0))
	move_ret R(0)
	call_c   build_ref_762()
	call_c   Dyam_Create_List(&ref[762],R(0))
	move_ret R(0)
	call_c   build_ref_761()
	call_c   Dyam_Create_List(&ref[761],R(0))
	move_ret R(0)
	call_c   build_ref_760()
	call_c   Dyam_Create_List(&ref[760],R(0))
	move_ret R(0)
	call_c   build_ref_759()
	call_c   Dyam_Create_List(&ref[759],R(0))
	move_ret R(0)
	call_c   build_ref_758()
	call_c   Dyam_Create_List(&ref[758],R(0))
	move_ret R(0)
	call_c   build_ref_757()
	call_c   Dyam_Create_List(&ref[757],R(0))
	move_ret R(0)
	call_c   build_ref_756()
	call_c   Dyam_Create_List(&ref[756],R(0))
	move_ret R(0)
	call_c   build_ref_755()
	call_c   Dyam_Create_List(&ref[755],R(0))
	move_ret R(0)
	call_c   build_ref_754()
	call_c   Dyam_Create_List(&ref[754],R(0))
	move_ret R(0)
	call_c   build_ref_753()
	call_c   Dyam_Create_List(&ref[753],R(0))
	move_ret R(0)
	call_c   build_ref_752()
	call_c   Dyam_Create_List(&ref[752],R(0))
	move_ret R(0)
	call_c   build_ref_751()
	call_c   Dyam_Create_List(&ref[751],R(0))
	move_ret R(0)
	call_c   build_ref_750()
	call_c   Dyam_Create_List(&ref[750],R(0))
	move_ret R(0)
	call_c   build_ref_749()
	call_c   Dyam_Create_Atom_Module("$ft",&ref[749])
	move_ret ref[748]
	call_c   DYAM_Feature_2(&ref[749],R(0))
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 749: seed
c_code local build_ref_749
	ret_reg &ref[749]
	call_c   Dyam_Create_Atom("seed")
	move_ret ref[749]
	c_ret

;; TERM 750: model
c_code local build_ref_750
	ret_reg &ref[750]
	call_c   Dyam_Create_Atom("model")
	move_ret ref[750]
	c_ret

;; TERM 751: id
c_code local build_ref_751
	ret_reg &ref[751]
	call_c   Dyam_Create_Atom("id")
	move_ret ref[751]
	c_ret

;; TERM 752: anchor
c_code local build_ref_752
	ret_reg &ref[752]
	call_c   Dyam_Create_Atom("anchor")
	move_ret ref[752]
	c_ret

;; TERM 753: subs_comp
c_code local build_ref_753
	ret_reg &ref[753]
	call_c   Dyam_Create_Atom("subs_comp")
	move_ret ref[753]
	c_ret

;; TERM 754: code
c_code local build_ref_754
	ret_reg &ref[754]
	call_c   Dyam_Create_Atom("code")
	move_ret ref[754]
	c_ret

;; TERM 755: go
c_code local build_ref_755
	ret_reg &ref[755]
	call_c   Dyam_Create_Atom("go")
	move_ret ref[755]
	c_ret

;; TERM 756: tabule
c_code local build_ref_756
	ret_reg &ref[756]
	call_c   Dyam_Create_Atom("tabule")
	move_ret ref[756]
	c_ret

;; TERM 757: schedule
c_code local build_ref_757
	ret_reg &ref[757]
	call_c   Dyam_Create_Atom("schedule")
	move_ret ref[757]
	c_ret

;; TERM 758: subsume
c_code local build_ref_758
	ret_reg &ref[758]
	call_c   Dyam_Create_Atom("subsume")
	move_ret ref[758]
	c_ret

;; TERM 759: model_ref
c_code local build_ref_759
	ret_reg &ref[759]
	call_c   Dyam_Create_Atom("model_ref")
	move_ret ref[759]
	c_ret

;; TERM 760: subs_comp_ref
c_code local build_ref_760
	ret_reg &ref[760]
	call_c   Dyam_Create_Atom("subs_comp_ref")
	move_ret ref[760]
	c_ret

;; TERM 761: c_type
c_code local build_ref_761
	ret_reg &ref[761]
	call_c   Dyam_Create_Atom("c_type")
	move_ret ref[761]
	c_ret

;; TERM 762: c_type_ref
c_code local build_ref_762
	ret_reg &ref[762]
	call_c   Dyam_Create_Atom("c_type_ref")
	move_ret ref[762]
	c_ret

;; TERM 763: out_env
c_code local build_ref_763
	ret_reg &ref[763]
	call_c   Dyam_Create_Atom("out_env")
	move_ret ref[763]
	c_ret

;; TERM 764: appinfo
c_code local build_ref_764
	ret_reg &ref[764]
	call_c   Dyam_Create_Atom("appinfo")
	move_ret ref[764]
	c_ret

;; TERM 765: tail_flag
c_code local build_ref_765
	ret_reg &ref[765]
	call_c   Dyam_Create_Atom("tail_flag")
	move_ret ref[765]
	c_ret

;; TERM 165: '*PROLOG-ITEM*'{top=> seed_install(seed{model=> _B, id=> _G, anchor=> _H, subs_comp=> _I, code=> _J, go=> _K, tabule=> _L, schedule=> _M, subsume=> _N, model_ref=> _O, subs_comp_ref=> _P, c_type=> _Q, c_type_ref=> _R, out_env=> _S, appinfo=> _C, tail_flag=> _T}, 0, _F), cont=> _A}
c_code local build_ref_165
	ret_reg &ref[165]
	call_c   build_ref_724()
	call_c   build_ref_166()
	call_c   Dyam_Create_Binary(&ref[724],&ref[166],V(0))
	move_ret ref[165]
	c_ret

long local pool_fun53[3]=[2,build_ref_164,build_seed_81]

pl_code local fun53
	call_c   Dyam_Pool(pool_fun53)
	call_c   Dyam_Unify_Item(&ref[164])
	fail_ret
	pl_jump  fun19(&seed[81],12)

;; TERM 163: '*PROLOG-FIRST*'(unfold('*OBJECT*'(_B, _C), _D, _E, _F, _D)) :> []
c_code local build_ref_163
	ret_reg &ref[163]
	call_c   build_ref_162()
	call_c   Dyam_Create_Binary(I(9),&ref[162],I(0))
	move_ret ref[163]
	c_ret

;; TERM 162: '*PROLOG-FIRST*'(unfold('*OBJECT*'(_B, _C), _D, _E, _F, _D))
c_code local build_ref_162
	ret_reg &ref[162]
	call_c   build_ref_5()
	call_c   build_ref_161()
	call_c   Dyam_Create_Unary(&ref[5],&ref[161])
	move_ret ref[162]
	c_ret

c_code local build_seed_64
	ret_reg &seed[64]
	call_c   build_ref_4()
	call_c   build_ref_194()
	call_c   Dyam_Seed_Start(&ref[4],&ref[194],I(0),fun1,1)
	call_c   build_ref_195()
	call_c   Dyam_Seed_Add_Comp(&ref[195],fun34,0)
	call_c   Dyam_Seed_End()
	move_ret seed[64]
	c_ret

;; TERM 195: '*PROLOG-ITEM*'{top=> unfold('*KLEENE-ALTERNATIVE*'(_B, _C), _D, _E, _F, _G), cont=> _A}
c_code local build_ref_195
	ret_reg &ref[195]
	call_c   build_ref_724()
	call_c   build_ref_192()
	call_c   Dyam_Create_Binary(&ref[724],&ref[192],V(0))
	move_ret ref[195]
	c_ret

;; TERM 192: unfold('*KLEENE-ALTERNATIVE*'(_B, _C), _D, _E, _F, _G)
c_code local build_ref_192
	ret_reg &ref[192]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_766()
	call_c   Dyam_Create_Binary(&ref[766],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_725()
	call_c   Dyam_Term_Start(&ref[725],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[192]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 766: '*KLEENE-ALTERNATIVE*'
c_code local build_ref_766
	ret_reg &ref[766]
	call_c   Dyam_Create_Atom("*KLEENE-ALTERNATIVE*")
	move_ret ref[766]
	c_ret

c_code local build_seed_86
	ret_reg &seed[86]
	call_c   build_ref_59()
	call_c   build_ref_200()
	call_c   Dyam_Seed_Start(&ref[59],&ref[200],I(0),fun15,1)
	call_c   build_ref_203()
	call_c   Dyam_Seed_Add_Comp(&ref[203],&ref[200],0)
	call_c   Dyam_Seed_End()
	move_ret seed[86]
	c_ret

;; TERM 203: '*PROLOG-FIRST*'(unfold(_B, _D, _E, _H, _G)) :> '$$HOLE$$'
c_code local build_ref_203
	ret_reg &ref[203]
	call_c   build_ref_202()
	call_c   Dyam_Create_Binary(I(9),&ref[202],I(7))
	move_ret ref[203]
	c_ret

;; TERM 202: '*PROLOG-FIRST*'(unfold(_B, _D, _E, _H, _G))
c_code local build_ref_202
	ret_reg &ref[202]
	call_c   build_ref_5()
	call_c   build_ref_201()
	call_c   Dyam_Create_Unary(&ref[5],&ref[201])
	move_ret ref[202]
	c_ret

;; TERM 201: unfold(_B, _D, _E, _H, _G)
c_code local build_ref_201
	ret_reg &ref[201]
	call_c   build_ref_725()
	call_c   Dyam_Term_Start(&ref[725],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[201]
	c_ret

;; TERM 200: '*PROLOG-ITEM*'{top=> unfold(_B, _D, _E, _H, _G), cont=> '$CLOSURE'('$fun'(33, 0, 1177420388), '$TUPPLE'(35125252331408))}
c_code local build_ref_200
	ret_reg &ref[200]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,396361728)
	move_ret R(0)
	call_c   Dyam_Closure_Aux(fun33,R(0))
	move_ret R(0)
	call_c   build_ref_724()
	call_c   build_ref_201()
	call_c   Dyam_Create_Binary(&ref[724],&ref[201],R(0))
	move_ret ref[200]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_85
	ret_reg &seed[85]
	call_c   build_ref_59()
	call_c   build_ref_196()
	call_c   Dyam_Seed_Start(&ref[59],&ref[196],I(0),fun15,1)
	call_c   build_ref_199()
	call_c   Dyam_Seed_Add_Comp(&ref[199],&ref[196],0)
	call_c   Dyam_Seed_End()
	move_ret seed[85]
	c_ret

;; TERM 199: '*PROLOG-FIRST*'(unfold((_C :> deallocate :> succeed), _D, _E, _I, _J)) :> '$$HOLE$$'
c_code local build_ref_199
	ret_reg &ref[199]
	call_c   build_ref_198()
	call_c   Dyam_Create_Binary(I(9),&ref[198],I(7))
	move_ret ref[199]
	c_ret

;; TERM 198: '*PROLOG-FIRST*'(unfold((_C :> deallocate :> succeed), _D, _E, _I, _J))
c_code local build_ref_198
	ret_reg &ref[198]
	call_c   build_ref_5()
	call_c   build_ref_197()
	call_c   Dyam_Create_Unary(&ref[5],&ref[197])
	move_ret ref[198]
	c_ret

;; TERM 197: unfold((_C :> deallocate :> succeed), _D, _E, _I, _J)
c_code local build_ref_197
	ret_reg &ref[197]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_302()
	call_c   Dyam_Create_Binary(I(9),V(2),&ref[302])
	move_ret R(0)
	call_c   build_ref_725()
	call_c   Dyam_Term_Start(&ref[725],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret ref[197]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 196: '*PROLOG-ITEM*'{top=> unfold((_C :> deallocate :> succeed), _D, _E, _I, _J), cont=> '$CLOSURE'('$fun'(32, 0, 1177410244), '$TUPPLE'(35125252331372))}
c_code local build_ref_196
	ret_reg &ref[196]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,279969792)
	move_ret R(0)
	call_c   Dyam_Closure_Aux(fun32,R(0))
	move_ret R(0)
	call_c   build_ref_724()
	call_c   build_ref_197()
	call_c   Dyam_Create_Binary(&ref[724],&ref[197],R(0))
	move_ret ref[196]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

pl_code local fun32
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(8))
	call_c   Dyam_Reg_Load(2,V(7))
	call_c   Dyam_Reg_Load(4,V(5))
	pl_call  pred_choice_code_3()
	call_c   Dyam_Deallocate()
	pl_jump  Follow_Cont(V(0))

long local pool_fun33[2]=[1,build_seed_85]

pl_code local fun33
	call_c   Dyam_Pool(pool_fun33)
	pl_jump  fun19(&seed[85],12)

long local pool_fun34[3]=[2,build_ref_195,build_seed_86]

pl_code local fun34
	call_c   Dyam_Pool(pool_fun34)
	call_c   Dyam_Unify_Item(&ref[195])
	fail_ret
	pl_jump  fun19(&seed[86],12)

;; TERM 194: '*PROLOG-FIRST*'(unfold('*KLEENE-ALTERNATIVE*'(_B, _C), _D, _E, _F, _G)) :> []
c_code local build_ref_194
	ret_reg &ref[194]
	call_c   build_ref_193()
	call_c   Dyam_Create_Binary(I(9),&ref[193],I(0))
	move_ret ref[194]
	c_ret

;; TERM 193: '*PROLOG-FIRST*'(unfold('*KLEENE-ALTERNATIVE*'(_B, _C), _D, _E, _F, _G))
c_code local build_ref_193
	ret_reg &ref[193]
	call_c   build_ref_5()
	call_c   build_ref_192()
	call_c   Dyam_Create_Unary(&ref[5],&ref[192])
	move_ret ref[193]
	c_ret

c_code local build_seed_34
	ret_reg &seed[34]
	call_c   build_ref_4()
	call_c   build_ref_107()
	call_c   Dyam_Seed_Start(&ref[4],&ref[107],I(0),fun1,1)
	call_c   build_ref_108()
	call_c   Dyam_Seed_Add_Comp(&ref[108],fun58,0)
	call_c   Dyam_Seed_End()
	move_ret seed[34]
	c_ret

;; TERM 108: '*PROLOG-ITEM*'{top=> unfold('*GUARD*'(_B), _C, _D, _E, _C), cont=> _A}
c_code local build_ref_108
	ret_reg &ref[108]
	call_c   build_ref_724()
	call_c   build_ref_105()
	call_c   Dyam_Create_Binary(&ref[724],&ref[105],V(0))
	move_ret ref[108]
	c_ret

;; TERM 105: unfold('*GUARD*'(_B), _C, _D, _E, _C)
c_code local build_ref_105
	ret_reg &ref[105]
	call_c   build_ref_725()
	call_c   build_ref_504()
	call_c   Dyam_Term_Start(&ref[725],5)
	call_c   Dyam_Term_Arg(&ref[504])
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_End()
	move_ret ref[105]
	c_ret

;; TERM 504: '*GUARD*'(_B)
c_code local build_ref_504
	ret_reg &ref[504]
	call_c   build_ref_222()
	call_c   Dyam_Create_Unary(&ref[222],V(1))
	move_ret ref[504]
	c_ret

;; TERM 222: '*GUARD*'
c_code local build_ref_222
	ret_reg &ref[222]
	call_c   Dyam_Create_Atom("*GUARD*")
	move_ret ref[222]
	c_ret

c_code local build_seed_74
	ret_reg &seed[74]
	call_c   build_ref_59()
	call_c   build_ref_109()
	call_c   Dyam_Seed_Start(&ref[59],&ref[109],I(0),fun15,1)
	call_c   build_ref_112()
	call_c   Dyam_Seed_Add_Comp(&ref[112],&ref[109],0)
	call_c   Dyam_Seed_End()
	move_ret seed[74]
	c_ret

;; TERM 112: '*PROLOG-FIRST*'(seed_install(seed{model=> '*GUARD*'(_B), id=> _F, anchor=> _G, subs_comp=> _H, code=> _I, go=> _J, tabule=> _K, schedule=> _L, subsume=> _M, model_ref=> _N, subs_comp_ref=> _O, c_type=> _P, c_type_ref=> _Q, out_env=> _R, appinfo=> _S, tail_flag=> _T}, 1, _E)) :> '$$HOLE$$'
c_code local build_ref_112
	ret_reg &ref[112]
	call_c   build_ref_111()
	call_c   Dyam_Create_Binary(I(9),&ref[111],I(7))
	move_ret ref[112]
	c_ret

;; TERM 111: '*PROLOG-FIRST*'(seed_install(seed{model=> '*GUARD*'(_B), id=> _F, anchor=> _G, subs_comp=> _H, code=> _I, go=> _J, tabule=> _K, schedule=> _L, subsume=> _M, model_ref=> _N, subs_comp_ref=> _O, c_type=> _P, c_type_ref=> _Q, out_env=> _R, appinfo=> _S, tail_flag=> _T}, 1, _E))
c_code local build_ref_111
	ret_reg &ref[111]
	call_c   build_ref_5()
	call_c   build_ref_110()
	call_c   Dyam_Create_Unary(&ref[5],&ref[110])
	move_ret ref[111]
	c_ret

;; TERM 110: seed_install(seed{model=> '*GUARD*'(_B), id=> _F, anchor=> _G, subs_comp=> _H, code=> _I, go=> _J, tabule=> _K, schedule=> _L, subsume=> _M, model_ref=> _N, subs_comp_ref=> _O, c_type=> _P, c_type_ref=> _Q, out_env=> _R, appinfo=> _S, tail_flag=> _T}, 1, _E)
c_code local build_ref_110
	ret_reg &ref[110]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_748()
	call_c   build_ref_504()
	call_c   Dyam_Term_Start(&ref[748],16)
	call_c   Dyam_Term_Arg(&ref[504])
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_747()
	call_c   Dyam_Term_Start(&ref[747],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(N(1))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[110]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 109: '*PROLOG-ITEM*'{top=> seed_install(seed{model=> '*GUARD*'(_B), id=> _F, anchor=> _G, subs_comp=> _H, code=> _I, go=> _J, tabule=> _K, schedule=> _L, subsume=> _M, model_ref=> _N, subs_comp_ref=> _O, c_type=> _P, c_type_ref=> _Q, out_env=> _R, appinfo=> _S, tail_flag=> _T}, 1, _E), cont=> _A}
c_code local build_ref_109
	ret_reg &ref[109]
	call_c   build_ref_724()
	call_c   build_ref_110()
	call_c   Dyam_Create_Binary(&ref[724],&ref[110],V(0))
	move_ret ref[109]
	c_ret

long local pool_fun58[3]=[2,build_ref_108,build_seed_74]

pl_code local fun58
	call_c   Dyam_Pool(pool_fun58)
	call_c   Dyam_Unify_Item(&ref[108])
	fail_ret
	pl_jump  fun19(&seed[74],12)

;; TERM 107: '*PROLOG-FIRST*'(unfold('*GUARD*'(_B), _C, _D, _E, _C)) :> []
c_code local build_ref_107
	ret_reg &ref[107]
	call_c   build_ref_106()
	call_c   Dyam_Create_Binary(I(9),&ref[106],I(0))
	move_ret ref[107]
	c_ret

;; TERM 106: '*PROLOG-FIRST*'(unfold('*GUARD*'(_B), _C, _D, _E, _C))
c_code local build_ref_106
	ret_reg &ref[106]
	call_c   build_ref_5()
	call_c   build_ref_105()
	call_c   Dyam_Create_Unary(&ref[5],&ref[105])
	move_ret ref[106]
	c_ret

c_code local build_seed_54
	ret_reg &seed[54]
	call_c   build_ref_4()
	call_c   build_ref_115()
	call_c   Dyam_Seed_Start(&ref[4],&ref[115],I(0),fun1,1)
	call_c   build_ref_116()
	call_c   Dyam_Seed_Add_Comp(&ref[116],fun59,0)
	call_c   Dyam_Seed_End()
	move_ret seed[54]
	c_ret

;; TERM 116: '*PROLOG-ITEM*'{top=> unfold('*ANSWER*'{answer=> _B}, _C, _D, _E, _C), cont=> _A}
c_code local build_ref_116
	ret_reg &ref[116]
	call_c   build_ref_724()
	call_c   build_ref_113()
	call_c   Dyam_Create_Binary(&ref[724],&ref[113],V(0))
	move_ret ref[116]
	c_ret

;; TERM 113: unfold('*ANSWER*'{answer=> _B}, _C, _D, _E, _C)
c_code local build_ref_113
	ret_reg &ref[113]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_767()
	call_c   Dyam_Create_Unary(&ref[767],V(1))
	move_ret R(0)
	call_c   build_ref_725()
	call_c   Dyam_Term_Start(&ref[725],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_End()
	move_ret ref[113]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 767: '*ANSWER*'!'$ft'
c_code local build_ref_767
	ret_reg &ref[767]
	call_c   build_ref_768()
	call_c   Dyam_Create_Atom_Module("$ft",&ref[768])
	move_ret ref[767]
	c_ret

;; TERM 768: '*ANSWER*'
c_code local build_ref_768
	ret_reg &ref[768]
	call_c   Dyam_Create_Atom("*ANSWER*")
	move_ret ref[768]
	c_ret

c_code local build_seed_75
	ret_reg &seed[75]
	call_c   build_ref_59()
	call_c   build_ref_117()
	call_c   Dyam_Seed_Start(&ref[59],&ref[117],I(0),fun15,1)
	call_c   build_ref_120()
	call_c   Dyam_Seed_Add_Comp(&ref[120],&ref[117],0)
	call_c   Dyam_Seed_End()
	move_ret seed[75]
	c_ret

;; TERM 120: '*PROLOG-FIRST*'(seed_install(seed{model=> '*ANSWER*'{answer=> _B}, id=> _F, anchor=> _G, subs_comp=> _H, code=> _I, go=> _J, tabule=> _K, schedule=> _L, subsume=> _M, model_ref=> _N, subs_comp_ref=> _O, c_type=> _P, c_type_ref=> _Q, out_env=> _R, appinfo=> _S, tail_flag=> _T}, 2, _E)) :> '$$HOLE$$'
c_code local build_ref_120
	ret_reg &ref[120]
	call_c   build_ref_119()
	call_c   Dyam_Create_Binary(I(9),&ref[119],I(7))
	move_ret ref[120]
	c_ret

;; TERM 119: '*PROLOG-FIRST*'(seed_install(seed{model=> '*ANSWER*'{answer=> _B}, id=> _F, anchor=> _G, subs_comp=> _H, code=> _I, go=> _J, tabule=> _K, schedule=> _L, subsume=> _M, model_ref=> _N, subs_comp_ref=> _O, c_type=> _P, c_type_ref=> _Q, out_env=> _R, appinfo=> _S, tail_flag=> _T}, 2, _E))
c_code local build_ref_119
	ret_reg &ref[119]
	call_c   build_ref_5()
	call_c   build_ref_118()
	call_c   Dyam_Create_Unary(&ref[5],&ref[118])
	move_ret ref[119]
	c_ret

;; TERM 118: seed_install(seed{model=> '*ANSWER*'{answer=> _B}, id=> _F, anchor=> _G, subs_comp=> _H, code=> _I, go=> _J, tabule=> _K, schedule=> _L, subsume=> _M, model_ref=> _N, subs_comp_ref=> _O, c_type=> _P, c_type_ref=> _Q, out_env=> _R, appinfo=> _S, tail_flag=> _T}, 2, _E)
c_code local build_ref_118
	ret_reg &ref[118]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_767()
	call_c   Dyam_Create_Unary(&ref[767],V(1))
	move_ret R(0)
	call_c   build_ref_748()
	call_c   Dyam_Term_Start(&ref[748],16)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_747()
	call_c   Dyam_Term_Start(&ref[747],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(N(2))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[118]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 117: '*PROLOG-ITEM*'{top=> seed_install(seed{model=> '*ANSWER*'{answer=> _B}, id=> _F, anchor=> _G, subs_comp=> _H, code=> _I, go=> _J, tabule=> _K, schedule=> _L, subsume=> _M, model_ref=> _N, subs_comp_ref=> _O, c_type=> _P, c_type_ref=> _Q, out_env=> _R, appinfo=> _S, tail_flag=> _T}, 2, _E), cont=> _A}
c_code local build_ref_117
	ret_reg &ref[117]
	call_c   build_ref_724()
	call_c   build_ref_118()
	call_c   Dyam_Create_Binary(&ref[724],&ref[118],V(0))
	move_ret ref[117]
	c_ret

long local pool_fun59[3]=[2,build_ref_116,build_seed_75]

pl_code local fun59
	call_c   Dyam_Pool(pool_fun59)
	call_c   Dyam_Unify_Item(&ref[116])
	fail_ret
	pl_jump  fun19(&seed[75],12)

;; TERM 115: '*PROLOG-FIRST*'(unfold('*ANSWER*'{answer=> _B}, _C, _D, _E, _C)) :> []
c_code local build_ref_115
	ret_reg &ref[115]
	call_c   build_ref_114()
	call_c   Dyam_Create_Binary(I(9),&ref[114],I(0))
	move_ret ref[115]
	c_ret

;; TERM 114: '*PROLOG-FIRST*'(unfold('*ANSWER*'{answer=> _B}, _C, _D, _E, _C))
c_code local build_ref_114
	ret_reg &ref[114]
	call_c   build_ref_5()
	call_c   build_ref_113()
	call_c   Dyam_Create_Unary(&ref[5],&ref[113])
	move_ret ref[114]
	c_ret

c_code local build_seed_61
	ret_reg &seed[61]
	call_c   build_ref_4()
	call_c   build_ref_206()
	call_c   Dyam_Seed_Start(&ref[4],&ref[206],I(0),fun1,1)
	call_c   build_ref_207()
	call_c   Dyam_Seed_Add_Comp(&ref[207],fun36,0)
	call_c   Dyam_Seed_End()
	move_ret seed[61]
	c_ret

;; TERM 207: '*PROLOG-ITEM*'{top=> unfold((_B :> _C), _D, _E, (_F :> _G), _H), cont=> _A}
c_code local build_ref_207
	ret_reg &ref[207]
	call_c   build_ref_724()
	call_c   build_ref_204()
	call_c   Dyam_Create_Binary(&ref[724],&ref[204],V(0))
	move_ret ref[207]
	c_ret

;; TERM 204: unfold((_B :> _C), _D, _E, (_F :> _G), _H)
c_code local build_ref_204
	ret_reg &ref[204]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   Dyam_Create_Binary(I(9),V(1),V(2))
	move_ret R(0)
	call_c   Dyam_Create_Binary(I(9),V(5),V(6))
	move_ret R(1)
	call_c   build_ref_725()
	call_c   Dyam_Term_Start(&ref[725],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[204]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_88
	ret_reg &seed[88]
	call_c   build_ref_59()
	call_c   build_ref_212()
	call_c   Dyam_Seed_Start(&ref[59],&ref[212],I(0),fun15,1)
	call_c   build_ref_215()
	call_c   Dyam_Seed_Add_Comp(&ref[215],&ref[212],0)
	call_c   Dyam_Seed_End()
	move_ret seed[88]
	c_ret

;; TERM 215: '*PROLOG-FIRST*'(unfold(_B, _D, _E, _F, _I)) :> '$$HOLE$$'
c_code local build_ref_215
	ret_reg &ref[215]
	call_c   build_ref_214()
	call_c   Dyam_Create_Binary(I(9),&ref[214],I(7))
	move_ret ref[215]
	c_ret

;; TERM 214: '*PROLOG-FIRST*'(unfold(_B, _D, _E, _F, _I))
c_code local build_ref_214
	ret_reg &ref[214]
	call_c   build_ref_5()
	call_c   build_ref_213()
	call_c   Dyam_Create_Unary(&ref[5],&ref[213])
	move_ret ref[214]
	c_ret

;; TERM 213: unfold(_B, _D, _E, _F, _I)
c_code local build_ref_213
	ret_reg &ref[213]
	call_c   build_ref_725()
	call_c   Dyam_Term_Start(&ref[725],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_End()
	move_ret ref[213]
	c_ret

;; TERM 212: '*PROLOG-ITEM*'{top=> unfold(_B, _D, _E, _F, _I), cont=> '$CLOSURE'('$fun'(35, 0, 1177442012), '$TUPPLE'(35125253112768))}
c_code local build_ref_212
	ret_reg &ref[212]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,493879296)
	move_ret R(0)
	call_c   Dyam_Closure_Aux(fun35,R(0))
	move_ret R(0)
	call_c   build_ref_724()
	call_c   build_ref_213()
	call_c   Dyam_Create_Binary(&ref[724],&ref[213],R(0))
	move_ret ref[212]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_87
	ret_reg &seed[87]
	call_c   build_ref_59()
	call_c   build_ref_208()
	call_c   Dyam_Seed_Start(&ref[59],&ref[208],I(0),fun15,1)
	call_c   build_ref_211()
	call_c   Dyam_Seed_Add_Comp(&ref[211],&ref[208],0)
	call_c   Dyam_Seed_End()
	move_ret seed[87]
	c_ret

;; TERM 211: '*PROLOG-FIRST*'(unfold(_C, _I, _J, _G, _H)) :> '$$HOLE$$'
c_code local build_ref_211
	ret_reg &ref[211]
	call_c   build_ref_210()
	call_c   Dyam_Create_Binary(I(9),&ref[210],I(7))
	move_ret ref[211]
	c_ret

;; TERM 210: '*PROLOG-FIRST*'(unfold(_C, _I, _J, _G, _H))
c_code local build_ref_210
	ret_reg &ref[210]
	call_c   build_ref_5()
	call_c   build_ref_209()
	call_c   Dyam_Create_Unary(&ref[5],&ref[209])
	move_ret ref[210]
	c_ret

;; TERM 209: unfold(_C, _I, _J, _G, _H)
c_code local build_ref_209
	ret_reg &ref[209]
	call_c   build_ref_725()
	call_c   Dyam_Term_Start(&ref[725],5)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[209]
	c_ret

;; TERM 208: '*PROLOG-ITEM*'{top=> unfold(_C, _I, _J, _G, _H), cont=> _A}
c_code local build_ref_208
	ret_reg &ref[208]
	call_c   build_ref_724()
	call_c   build_ref_209()
	call_c   Dyam_Create_Binary(&ref[724],&ref[209],V(0))
	move_ret ref[208]
	c_ret

long local pool_fun35[2]=[1,build_seed_87]

pl_code local fun35
	call_c   Dyam_Pool(pool_fun35)
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(4))
	move     V(9), R(4)
	move     S(5), R(5)
	pl_call  pred_extend_tupple_3()
	call_c   Dyam_Deallocate()
	pl_jump  fun19(&seed[87],12)

long local pool_fun36[3]=[2,build_ref_207,build_seed_88]

pl_code local fun36
	call_c   Dyam_Pool(pool_fun36)
	call_c   Dyam_Unify_Item(&ref[207])
	fail_ret
	pl_jump  fun19(&seed[88],12)

;; TERM 206: '*PROLOG-FIRST*'(unfold((_B :> _C), _D, _E, (_F :> _G), _H)) :> []
c_code local build_ref_206
	ret_reg &ref[206]
	call_c   build_ref_205()
	call_c   Dyam_Create_Binary(I(9),&ref[205],I(0))
	move_ret ref[206]
	c_ret

;; TERM 205: '*PROLOG-FIRST*'(unfold((_B :> _C), _D, _E, (_F :> _G), _H))
c_code local build_ref_205
	ret_reg &ref[205]
	call_c   build_ref_5()
	call_c   build_ref_204()
	call_c   Dyam_Create_Unary(&ref[5],&ref[204])
	move_ret ref[205]
	c_ret

c_code local build_seed_46
	ret_reg &seed[46]
	call_c   build_ref_4()
	call_c   build_ref_123()
	call_c   Dyam_Seed_Start(&ref[4],&ref[123],I(0),fun1,1)
	call_c   build_ref_124()
	call_c   Dyam_Seed_Add_Comp(&ref[124],fun68,0)
	call_c   Dyam_Seed_End()
	move_ret seed[46]
	c_ret

;; TERM 124: '*PROLOG-ITEM*'{top=> unfold('*LIGHTLASTCALL*'(_B), _C, _D, _E, _C), cont=> _A}
c_code local build_ref_124
	ret_reg &ref[124]
	call_c   build_ref_724()
	call_c   build_ref_121()
	call_c   Dyam_Create_Binary(&ref[724],&ref[121],V(0))
	move_ret ref[124]
	c_ret

;; TERM 121: unfold('*LIGHTLASTCALL*'(_B), _C, _D, _E, _C)
c_code local build_ref_121
	ret_reg &ref[121]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_769()
	call_c   Dyam_Create_Unary(&ref[769],V(1))
	move_ret R(0)
	call_c   build_ref_725()
	call_c   Dyam_Term_Start(&ref[725],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_End()
	move_ret ref[121]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 769: '*LIGHTLASTCALL*'
c_code local build_ref_769
	ret_reg &ref[769]
	call_c   Dyam_Create_Atom("*LIGHTLASTCALL*")
	move_ret ref[769]
	c_ret

c_code local build_seed_76
	ret_reg &seed[76]
	call_c   build_ref_59()
	call_c   build_ref_125()
	call_c   Dyam_Seed_Start(&ref[59],&ref[125],I(0),fun15,1)
	call_c   build_ref_128()
	call_c   Dyam_Seed_Add_Comp(&ref[128],&ref[125],0)
	call_c   Dyam_Seed_End()
	move_ret seed[76]
	c_ret

;; TERM 128: '*PROLOG-FIRST*'(seed_install(seed{model=> '*CITEM*'(_B, _C), id=> _F, anchor=> _G, subs_comp=> _H, code=> _I, go=> _J, tabule=> _K, schedule=> prolog, subsume=> _L, model_ref=> _M, subs_comp_ref=> _N, c_type=> _O, c_type_ref=> _P, out_env=> _Q, appinfo=> _R, tail_flag=> _S}, 2, _E)) :> '$$HOLE$$'
c_code local build_ref_128
	ret_reg &ref[128]
	call_c   build_ref_127()
	call_c   Dyam_Create_Binary(I(9),&ref[127],I(7))
	move_ret ref[128]
	c_ret

;; TERM 127: '*PROLOG-FIRST*'(seed_install(seed{model=> '*CITEM*'(_B, _C), id=> _F, anchor=> _G, subs_comp=> _H, code=> _I, go=> _J, tabule=> _K, schedule=> prolog, subsume=> _L, model_ref=> _M, subs_comp_ref=> _N, c_type=> _O, c_type_ref=> _P, out_env=> _Q, appinfo=> _R, tail_flag=> _S}, 2, _E))
c_code local build_ref_127
	ret_reg &ref[127]
	call_c   build_ref_5()
	call_c   build_ref_126()
	call_c   Dyam_Create_Unary(&ref[5],&ref[126])
	move_ret ref[127]
	c_ret

;; TERM 126: seed_install(seed{model=> '*CITEM*'(_B, _C), id=> _F, anchor=> _G, subs_comp=> _H, code=> _I, go=> _J, tabule=> _K, schedule=> prolog, subsume=> _L, model_ref=> _M, subs_comp_ref=> _N, c_type=> _O, c_type_ref=> _P, out_env=> _Q, appinfo=> _R, tail_flag=> _S}, 2, _E)
c_code local build_ref_126
	ret_reg &ref[126]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_748()
	call_c   build_ref_633()
	call_c   build_ref_770()
	call_c   Dyam_Term_Start(&ref[748],16)
	call_c   Dyam_Term_Arg(&ref[633])
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(&ref[770])
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_747()
	call_c   Dyam_Term_Start(&ref[747],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(N(2))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[126]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 770: prolog
c_code local build_ref_770
	ret_reg &ref[770]
	call_c   Dyam_Create_Atom("prolog")
	move_ret ref[770]
	c_ret

;; TERM 633: '*CITEM*'(_B, _C)
c_code local build_ref_633
	ret_reg &ref[633]
	call_c   build_ref_329()
	call_c   Dyam_Create_Binary(&ref[329],V(1),V(2))
	move_ret ref[633]
	c_ret

;; TERM 329: '*CITEM*'
c_code local build_ref_329
	ret_reg &ref[329]
	call_c   Dyam_Create_Atom("*CITEM*")
	move_ret ref[329]
	c_ret

;; TERM 125: '*PROLOG-ITEM*'{top=> seed_install(seed{model=> '*CITEM*'(_B, _C), id=> _F, anchor=> _G, subs_comp=> _H, code=> _I, go=> _J, tabule=> _K, schedule=> prolog, subsume=> _L, model_ref=> _M, subs_comp_ref=> _N, c_type=> _O, c_type_ref=> _P, out_env=> _Q, appinfo=> _R, tail_flag=> _S}, 2, _E), cont=> _A}
c_code local build_ref_125
	ret_reg &ref[125]
	call_c   build_ref_724()
	call_c   build_ref_126()
	call_c   Dyam_Create_Binary(&ref[724],&ref[126],V(0))
	move_ret ref[125]
	c_ret

long local pool_fun68[3]=[2,build_ref_124,build_seed_76]

pl_code local fun68
	call_c   Dyam_Pool(pool_fun68)
	call_c   Dyam_Unify_Item(&ref[124])
	fail_ret
	pl_jump  fun19(&seed[76],12)

;; TERM 123: '*PROLOG-FIRST*'(unfold('*LIGHTLASTCALL*'(_B), _C, _D, _E, _C)) :> []
c_code local build_ref_123
	ret_reg &ref[123]
	call_c   build_ref_122()
	call_c   Dyam_Create_Binary(I(9),&ref[122],I(0))
	move_ret ref[123]
	c_ret

;; TERM 122: '*PROLOG-FIRST*'(unfold('*LIGHTLASTCALL*'(_B), _C, _D, _E, _C))
c_code local build_ref_122
	ret_reg &ref[122]
	call_c   build_ref_5()
	call_c   build_ref_121()
	call_c   Dyam_Create_Unary(&ref[5],&ref[121])
	move_ret ref[122]
	c_ret

c_code local build_seed_47
	ret_reg &seed[47]
	call_c   build_ref_4()
	call_c   build_ref_131()
	call_c   Dyam_Seed_Start(&ref[4],&ref[131],I(0),fun1,1)
	call_c   build_ref_132()
	call_c   Dyam_Seed_Add_Comp(&ref[132],fun69,0)
	call_c   Dyam_Seed_End()
	move_ret seed[47]
	c_ret

;; TERM 132: '*PROLOG-ITEM*'{top=> unfold('*LIGHTLCLAST*'(_B), _C, _D, _E, _C), cont=> _A}
c_code local build_ref_132
	ret_reg &ref[132]
	call_c   build_ref_724()
	call_c   build_ref_129()
	call_c   Dyam_Create_Binary(&ref[724],&ref[129],V(0))
	move_ret ref[132]
	c_ret

;; TERM 129: unfold('*LIGHTLCLAST*'(_B), _C, _D, _E, _C)
c_code local build_ref_129
	ret_reg &ref[129]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_771()
	call_c   Dyam_Create_Unary(&ref[771],V(1))
	move_ret R(0)
	call_c   build_ref_725()
	call_c   Dyam_Term_Start(&ref[725],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_End()
	move_ret ref[129]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 771: '*LIGHTLCLAST*'
c_code local build_ref_771
	ret_reg &ref[771]
	call_c   Dyam_Create_Atom("*LIGHTLCLAST*")
	move_ret ref[771]
	c_ret

c_code local build_seed_77
	ret_reg &seed[77]
	call_c   build_ref_59()
	call_c   build_ref_133()
	call_c   Dyam_Seed_Start(&ref[59],&ref[133],I(0),fun15,1)
	call_c   build_ref_136()
	call_c   Dyam_Seed_Add_Comp(&ref[136],&ref[133],0)
	call_c   Dyam_Seed_End()
	move_ret seed[77]
	c_ret

;; TERM 136: '*PROLOG-FIRST*'(seed_install(seed{model=> '*RITEM*'(_C, _B), id=> _F, anchor=> _G, subs_comp=> _H, code=> _I, go=> _J, tabule=> _K, schedule=> prolog, subsume=> _L, model_ref=> _M, subs_comp_ref=> _N, c_type=> _O, c_type_ref=> _P, out_env=> _Q, appinfo=> _R, tail_flag=> _S}, 2, _E)) :> '$$HOLE$$'
c_code local build_ref_136
	ret_reg &ref[136]
	call_c   build_ref_135()
	call_c   Dyam_Create_Binary(I(9),&ref[135],I(7))
	move_ret ref[136]
	c_ret

;; TERM 135: '*PROLOG-FIRST*'(seed_install(seed{model=> '*RITEM*'(_C, _B), id=> _F, anchor=> _G, subs_comp=> _H, code=> _I, go=> _J, tabule=> _K, schedule=> prolog, subsume=> _L, model_ref=> _M, subs_comp_ref=> _N, c_type=> _O, c_type_ref=> _P, out_env=> _Q, appinfo=> _R, tail_flag=> _S}, 2, _E))
c_code local build_ref_135
	ret_reg &ref[135]
	call_c   build_ref_5()
	call_c   build_ref_134()
	call_c   Dyam_Create_Unary(&ref[5],&ref[134])
	move_ret ref[135]
	c_ret

;; TERM 134: seed_install(seed{model=> '*RITEM*'(_C, _B), id=> _F, anchor=> _G, subs_comp=> _H, code=> _I, go=> _J, tabule=> _K, schedule=> prolog, subsume=> _L, model_ref=> _M, subs_comp_ref=> _N, c_type=> _O, c_type_ref=> _P, out_env=> _Q, appinfo=> _R, tail_flag=> _S}, 2, _E)
c_code local build_ref_134
	ret_reg &ref[134]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_0()
	call_c   Dyam_Create_Binary(&ref[0],V(2),V(1))
	move_ret R(0)
	call_c   build_ref_748()
	call_c   build_ref_770()
	call_c   Dyam_Term_Start(&ref[748],16)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(&ref[770])
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_747()
	call_c   Dyam_Term_Start(&ref[747],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(N(2))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[134]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 0: '*RITEM*'
c_code local build_ref_0
	ret_reg &ref[0]
	call_c   Dyam_Create_Atom("*RITEM*")
	move_ret ref[0]
	c_ret

;; TERM 133: '*PROLOG-ITEM*'{top=> seed_install(seed{model=> '*RITEM*'(_C, _B), id=> _F, anchor=> _G, subs_comp=> _H, code=> _I, go=> _J, tabule=> _K, schedule=> prolog, subsume=> _L, model_ref=> _M, subs_comp_ref=> _N, c_type=> _O, c_type_ref=> _P, out_env=> _Q, appinfo=> _R, tail_flag=> _S}, 2, _E), cont=> _A}
c_code local build_ref_133
	ret_reg &ref[133]
	call_c   build_ref_724()
	call_c   build_ref_134()
	call_c   Dyam_Create_Binary(&ref[724],&ref[134],V(0))
	move_ret ref[133]
	c_ret

long local pool_fun69[3]=[2,build_ref_132,build_seed_77]

pl_code local fun69
	call_c   Dyam_Pool(pool_fun69)
	call_c   Dyam_Unify_Item(&ref[132])
	fail_ret
	pl_jump  fun19(&seed[77],12)

;; TERM 131: '*PROLOG-FIRST*'(unfold('*LIGHTLCLAST*'(_B), _C, _D, _E, _C)) :> []
c_code local build_ref_131
	ret_reg &ref[131]
	call_c   build_ref_130()
	call_c   Dyam_Create_Binary(I(9),&ref[130],I(0))
	move_ret ref[131]
	c_ret

;; TERM 130: '*PROLOG-FIRST*'(unfold('*LIGHTLCLAST*'(_B), _C, _D, _E, _C))
c_code local build_ref_130
	ret_reg &ref[130]
	call_c   build_ref_5()
	call_c   build_ref_129()
	call_c   Dyam_Create_Unary(&ref[5],&ref[129])
	move_ret ref[130]
	c_ret

c_code local build_seed_48
	ret_reg &seed[48]
	call_c   build_ref_4()
	call_c   build_ref_139()
	call_c   Dyam_Seed_Start(&ref[4],&ref[139],I(0),fun1,1)
	call_c   build_ref_140()
	call_c   Dyam_Seed_Add_Comp(&ref[140],fun70,0)
	call_c   Dyam_Seed_End()
	move_ret seed[48]
	c_ret

;; TERM 140: '*PROLOG-ITEM*'{top=> unfold('*LIGHTLAST*'(_B), _C, _D, _E, _C), cont=> _A}
c_code local build_ref_140
	ret_reg &ref[140]
	call_c   build_ref_724()
	call_c   build_ref_137()
	call_c   Dyam_Create_Binary(&ref[724],&ref[137],V(0))
	move_ret ref[140]
	c_ret

;; TERM 137: unfold('*LIGHTLAST*'(_B), _C, _D, _E, _C)
c_code local build_ref_137
	ret_reg &ref[137]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_772()
	call_c   Dyam_Create_Unary(&ref[772],V(1))
	move_ret R(0)
	call_c   build_ref_725()
	call_c   Dyam_Term_Start(&ref[725],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_End()
	move_ret ref[137]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 772: '*LIGHTLAST*'
c_code local build_ref_772
	ret_reg &ref[772]
	call_c   Dyam_Create_Atom("*LIGHTLAST*")
	move_ret ref[772]
	c_ret

c_code local build_seed_78
	ret_reg &seed[78]
	call_c   build_ref_59()
	call_c   build_ref_141()
	call_c   Dyam_Seed_Start(&ref[59],&ref[141],I(0),fun15,1)
	call_c   build_ref_144()
	call_c   Dyam_Seed_Add_Comp(&ref[144],&ref[141],0)
	call_c   Dyam_Seed_End()
	move_ret seed[78]
	c_ret

;; TERM 144: '*PROLOG-FIRST*'(seed_install(seed{model=> '*RITEM*'(_C, _B), id=> _F, anchor=> _G, subs_comp=> _H, code=> _I, go=> _J, tabule=> _K, schedule=> no, subsume=> _L, model_ref=> _M, subs_comp_ref=> _N, c_type=> _O, c_type_ref=> _P, out_env=> _Q, appinfo=> _R, tail_flag=> _S}, 2, _E)) :> '$$HOLE$$'
c_code local build_ref_144
	ret_reg &ref[144]
	call_c   build_ref_143()
	call_c   Dyam_Create_Binary(I(9),&ref[143],I(7))
	move_ret ref[144]
	c_ret

;; TERM 143: '*PROLOG-FIRST*'(seed_install(seed{model=> '*RITEM*'(_C, _B), id=> _F, anchor=> _G, subs_comp=> _H, code=> _I, go=> _J, tabule=> _K, schedule=> no, subsume=> _L, model_ref=> _M, subs_comp_ref=> _N, c_type=> _O, c_type_ref=> _P, out_env=> _Q, appinfo=> _R, tail_flag=> _S}, 2, _E))
c_code local build_ref_143
	ret_reg &ref[143]
	call_c   build_ref_5()
	call_c   build_ref_142()
	call_c   Dyam_Create_Unary(&ref[5],&ref[142])
	move_ret ref[143]
	c_ret

;; TERM 142: seed_install(seed{model=> '*RITEM*'(_C, _B), id=> _F, anchor=> _G, subs_comp=> _H, code=> _I, go=> _J, tabule=> _K, schedule=> no, subsume=> _L, model_ref=> _M, subs_comp_ref=> _N, c_type=> _O, c_type_ref=> _P, out_env=> _Q, appinfo=> _R, tail_flag=> _S}, 2, _E)
c_code local build_ref_142
	ret_reg &ref[142]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_0()
	call_c   Dyam_Create_Binary(&ref[0],V(2),V(1))
	move_ret R(0)
	call_c   build_ref_748()
	call_c   build_ref_773()
	call_c   Dyam_Term_Start(&ref[748],16)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(&ref[773])
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_747()
	call_c   Dyam_Term_Start(&ref[747],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(N(2))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[142]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 773: no
c_code local build_ref_773
	ret_reg &ref[773]
	call_c   Dyam_Create_Atom("no")
	move_ret ref[773]
	c_ret

;; TERM 141: '*PROLOG-ITEM*'{top=> seed_install(seed{model=> '*RITEM*'(_C, _B), id=> _F, anchor=> _G, subs_comp=> _H, code=> _I, go=> _J, tabule=> _K, schedule=> no, subsume=> _L, model_ref=> _M, subs_comp_ref=> _N, c_type=> _O, c_type_ref=> _P, out_env=> _Q, appinfo=> _R, tail_flag=> _S}, 2, _E), cont=> _A}
c_code local build_ref_141
	ret_reg &ref[141]
	call_c   build_ref_724()
	call_c   build_ref_142()
	call_c   Dyam_Create_Binary(&ref[724],&ref[142],V(0))
	move_ret ref[141]
	c_ret

long local pool_fun70[3]=[2,build_ref_140,build_seed_78]

pl_code local fun70
	call_c   Dyam_Pool(pool_fun70)
	call_c   Dyam_Unify_Item(&ref[140])
	fail_ret
	pl_jump  fun19(&seed[78],12)

;; TERM 139: '*PROLOG-FIRST*'(unfold('*LIGHTLAST*'(_B), _C, _D, _E, _C)) :> []
c_code local build_ref_139
	ret_reg &ref[139]
	call_c   build_ref_138()
	call_c   Dyam_Create_Binary(I(9),&ref[138],I(0))
	move_ret ref[139]
	c_ret

;; TERM 138: '*PROLOG-FIRST*'(unfold('*LIGHTLAST*'(_B), _C, _D, _E, _C))
c_code local build_ref_138
	ret_reg &ref[138]
	call_c   build_ref_5()
	call_c   build_ref_137()
	call_c   Dyam_Create_Unary(&ref[5],&ref[137])
	move_ret ref[138]
	c_ret

c_code local build_seed_55
	ret_reg &seed[55]
	call_c   build_ref_4()
	call_c   build_ref_147()
	call_c   Dyam_Seed_Start(&ref[4],&ref[147],I(0),fun1,1)
	call_c   build_ref_148()
	call_c   Dyam_Seed_Add_Comp(&ref[148],fun71,0)
	call_c   Dyam_Seed_End()
	move_ret seed[55]
	c_ret

;; TERM 148: '*PROLOG-ITEM*'{top=> unfold('*LASTCALL*'(_B), _C, _D, _E, _C), cont=> _A}
c_code local build_ref_148
	ret_reg &ref[148]
	call_c   build_ref_724()
	call_c   build_ref_145()
	call_c   Dyam_Create_Binary(&ref[724],&ref[145],V(0))
	move_ret ref[148]
	c_ret

;; TERM 145: unfold('*LASTCALL*'(_B), _C, _D, _E, _C)
c_code local build_ref_145
	ret_reg &ref[145]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_774()
	call_c   Dyam_Create_Unary(&ref[774],V(1))
	move_ret R(0)
	call_c   build_ref_725()
	call_c   Dyam_Term_Start(&ref[725],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_End()
	move_ret ref[145]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 774: '*LASTCALL*'
c_code local build_ref_774
	ret_reg &ref[774]
	call_c   Dyam_Create_Atom("*LASTCALL*")
	move_ret ref[774]
	c_ret

c_code local build_seed_79
	ret_reg &seed[79]
	call_c   build_ref_59()
	call_c   build_ref_149()
	call_c   Dyam_Seed_Start(&ref[59],&ref[149],I(0),fun15,1)
	call_c   build_ref_152()
	call_c   Dyam_Seed_Add_Comp(&ref[152],&ref[149],0)
	call_c   Dyam_Seed_End()
	move_ret seed[79]
	c_ret

;; TERM 152: '*PROLOG-FIRST*'(seed_install(seed{model=> '*CITEM*'(_B, _C), id=> _F, anchor=> _G, subs_comp=> _H, code=> _I, go=> _J, tabule=> _K, schedule=> _L, subsume=> _M, model_ref=> _N, subs_comp_ref=> _O, c_type=> _P, c_type_ref=> _Q, out_env=> _R, appinfo=> _S, tail_flag=> _T}, 2, _E)) :> '$$HOLE$$'
c_code local build_ref_152
	ret_reg &ref[152]
	call_c   build_ref_151()
	call_c   Dyam_Create_Binary(I(9),&ref[151],I(7))
	move_ret ref[152]
	c_ret

;; TERM 151: '*PROLOG-FIRST*'(seed_install(seed{model=> '*CITEM*'(_B, _C), id=> _F, anchor=> _G, subs_comp=> _H, code=> _I, go=> _J, tabule=> _K, schedule=> _L, subsume=> _M, model_ref=> _N, subs_comp_ref=> _O, c_type=> _P, c_type_ref=> _Q, out_env=> _R, appinfo=> _S, tail_flag=> _T}, 2, _E))
c_code local build_ref_151
	ret_reg &ref[151]
	call_c   build_ref_5()
	call_c   build_ref_150()
	call_c   Dyam_Create_Unary(&ref[5],&ref[150])
	move_ret ref[151]
	c_ret

;; TERM 150: seed_install(seed{model=> '*CITEM*'(_B, _C), id=> _F, anchor=> _G, subs_comp=> _H, code=> _I, go=> _J, tabule=> _K, schedule=> _L, subsume=> _M, model_ref=> _N, subs_comp_ref=> _O, c_type=> _P, c_type_ref=> _Q, out_env=> _R, appinfo=> _S, tail_flag=> _T}, 2, _E)
c_code local build_ref_150
	ret_reg &ref[150]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_748()
	call_c   build_ref_633()
	call_c   Dyam_Term_Start(&ref[748],16)
	call_c   Dyam_Term_Arg(&ref[633])
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_747()
	call_c   Dyam_Term_Start(&ref[747],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(N(2))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[150]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 149: '*PROLOG-ITEM*'{top=> seed_install(seed{model=> '*CITEM*'(_B, _C), id=> _F, anchor=> _G, subs_comp=> _H, code=> _I, go=> _J, tabule=> _K, schedule=> _L, subsume=> _M, model_ref=> _N, subs_comp_ref=> _O, c_type=> _P, c_type_ref=> _Q, out_env=> _R, appinfo=> _S, tail_flag=> _T}, 2, _E), cont=> _A}
c_code local build_ref_149
	ret_reg &ref[149]
	call_c   build_ref_724()
	call_c   build_ref_150()
	call_c   Dyam_Create_Binary(&ref[724],&ref[150],V(0))
	move_ret ref[149]
	c_ret

long local pool_fun71[3]=[2,build_ref_148,build_seed_79]

pl_code local fun71
	call_c   Dyam_Pool(pool_fun71)
	call_c   Dyam_Unify_Item(&ref[148])
	fail_ret
	pl_jump  fun19(&seed[79],12)

;; TERM 147: '*PROLOG-FIRST*'(unfold('*LASTCALL*'(_B), _C, _D, _E, _C)) :> []
c_code local build_ref_147
	ret_reg &ref[147]
	call_c   build_ref_146()
	call_c   Dyam_Create_Binary(I(9),&ref[146],I(0))
	move_ret ref[147]
	c_ret

;; TERM 146: '*PROLOG-FIRST*'(unfold('*LASTCALL*'(_B), _C, _D, _E, _C))
c_code local build_ref_146
	ret_reg &ref[146]
	call_c   build_ref_5()
	call_c   build_ref_145()
	call_c   Dyam_Create_Unary(&ref[5],&ref[145])
	move_ret ref[146]
	c_ret

c_code local build_seed_56
	ret_reg &seed[56]
	call_c   build_ref_4()
	call_c   build_ref_155()
	call_c   Dyam_Seed_Start(&ref[4],&ref[155],I(0),fun1,1)
	call_c   build_ref_156()
	call_c   Dyam_Seed_Add_Comp(&ref[156],fun72,0)
	call_c   Dyam_Seed_End()
	move_ret seed[56]
	c_ret

;; TERM 156: '*PROLOG-ITEM*'{top=> unfold('*LAST*'(_B), _C, _D, _E, _C), cont=> _A}
c_code local build_ref_156
	ret_reg &ref[156]
	call_c   build_ref_724()
	call_c   build_ref_153()
	call_c   Dyam_Create_Binary(&ref[724],&ref[153],V(0))
	move_ret ref[156]
	c_ret

;; TERM 153: unfold('*LAST*'(_B), _C, _D, _E, _C)
c_code local build_ref_153
	ret_reg &ref[153]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_775()
	call_c   Dyam_Create_Unary(&ref[775],V(1))
	move_ret R(0)
	call_c   build_ref_725()
	call_c   Dyam_Term_Start(&ref[725],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_End()
	move_ret ref[153]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 775: '*LAST*'
c_code local build_ref_775
	ret_reg &ref[775]
	call_c   Dyam_Create_Atom("*LAST*")
	move_ret ref[775]
	c_ret

c_code local build_seed_80
	ret_reg &seed[80]
	call_c   build_ref_59()
	call_c   build_ref_157()
	call_c   Dyam_Seed_Start(&ref[59],&ref[157],I(0),fun15,1)
	call_c   build_ref_160()
	call_c   Dyam_Seed_Add_Comp(&ref[160],&ref[157],0)
	call_c   Dyam_Seed_End()
	move_ret seed[80]
	c_ret

;; TERM 160: '*PROLOG-FIRST*'(seed_install(seed{model=> '*RITEM*'(_C, _B), id=> _F, anchor=> _G, subs_comp=> _H, code=> _I, go=> _J, tabule=> _K, schedule=> _L, subsume=> _M, model_ref=> _N, subs_comp_ref=> _O, c_type=> _P, c_type_ref=> _Q, out_env=> _R, appinfo=> _S, tail_flag=> _T}, 2, _E)) :> '$$HOLE$$'
c_code local build_ref_160
	ret_reg &ref[160]
	call_c   build_ref_159()
	call_c   Dyam_Create_Binary(I(9),&ref[159],I(7))
	move_ret ref[160]
	c_ret

;; TERM 159: '*PROLOG-FIRST*'(seed_install(seed{model=> '*RITEM*'(_C, _B), id=> _F, anchor=> _G, subs_comp=> _H, code=> _I, go=> _J, tabule=> _K, schedule=> _L, subsume=> _M, model_ref=> _N, subs_comp_ref=> _O, c_type=> _P, c_type_ref=> _Q, out_env=> _R, appinfo=> _S, tail_flag=> _T}, 2, _E))
c_code local build_ref_159
	ret_reg &ref[159]
	call_c   build_ref_5()
	call_c   build_ref_158()
	call_c   Dyam_Create_Unary(&ref[5],&ref[158])
	move_ret ref[159]
	c_ret

;; TERM 158: seed_install(seed{model=> '*RITEM*'(_C, _B), id=> _F, anchor=> _G, subs_comp=> _H, code=> _I, go=> _J, tabule=> _K, schedule=> _L, subsume=> _M, model_ref=> _N, subs_comp_ref=> _O, c_type=> _P, c_type_ref=> _Q, out_env=> _R, appinfo=> _S, tail_flag=> _T}, 2, _E)
c_code local build_ref_158
	ret_reg &ref[158]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_0()
	call_c   Dyam_Create_Binary(&ref[0],V(2),V(1))
	move_ret R(0)
	call_c   build_ref_748()
	call_c   Dyam_Term_Start(&ref[748],16)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_747()
	call_c   Dyam_Term_Start(&ref[747],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(N(2))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[158]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 157: '*PROLOG-ITEM*'{top=> seed_install(seed{model=> '*RITEM*'(_C, _B), id=> _F, anchor=> _G, subs_comp=> _H, code=> _I, go=> _J, tabule=> _K, schedule=> _L, subsume=> _M, model_ref=> _N, subs_comp_ref=> _O, c_type=> _P, c_type_ref=> _Q, out_env=> _R, appinfo=> _S, tail_flag=> _T}, 2, _E), cont=> _A}
c_code local build_ref_157
	ret_reg &ref[157]
	call_c   build_ref_724()
	call_c   build_ref_158()
	call_c   Dyam_Create_Binary(&ref[724],&ref[158],V(0))
	move_ret ref[157]
	c_ret

long local pool_fun72[3]=[2,build_ref_156,build_seed_80]

pl_code local fun72
	call_c   Dyam_Pool(pool_fun72)
	call_c   Dyam_Unify_Item(&ref[156])
	fail_ret
	pl_jump  fun19(&seed[80],12)

;; TERM 155: '*PROLOG-FIRST*'(unfold('*LAST*'(_B), _C, _D, _E, _C)) :> []
c_code local build_ref_155
	ret_reg &ref[155]
	call_c   build_ref_154()
	call_c   Dyam_Create_Binary(I(9),&ref[154],I(0))
	move_ret ref[155]
	c_ret

;; TERM 154: '*PROLOG-FIRST*'(unfold('*LAST*'(_B), _C, _D, _E, _C))
c_code local build_ref_154
	ret_reg &ref[154]
	call_c   build_ref_5()
	call_c   build_ref_153()
	call_c   Dyam_Create_Unary(&ref[5],&ref[153])
	move_ret ref[154]
	c_ret

c_code local build_seed_25
	ret_reg &seed[25]
	call_c   build_ref_4()
	call_c   build_ref_218()
	call_c   Dyam_Seed_Start(&ref[4],&ref[218],I(0),fun1,1)
	call_c   build_ref_219()
	call_c   Dyam_Seed_Add_Comp(&ref[219],fun37,0)
	call_c   Dyam_Seed_End()
	move_ret seed[25]
	c_ret

;; TERM 219: '*PROLOG-ITEM*'{top=> unfold('*IL-LOOP*'(_B, [_C|_D]), _E, _F, _G, _E), cont=> _A}
c_code local build_ref_219
	ret_reg &ref[219]
	call_c   build_ref_724()
	call_c   build_ref_216()
	call_c   Dyam_Create_Binary(&ref[724],&ref[216],V(0))
	move_ret ref[219]
	c_ret

;; TERM 216: unfold('*IL-LOOP*'(_B, [_C|_D]), _E, _F, _G, _E)
c_code local build_ref_216
	ret_reg &ref[216]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_776()
	call_c   build_ref_220()
	call_c   Dyam_Create_Binary(&ref[776],V(1),&ref[220])
	move_ret R(0)
	call_c   build_ref_725()
	call_c   Dyam_Term_Start(&ref[725],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[216]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 220: [_C|_D]
c_code local build_ref_220
	ret_reg &ref[220]
	call_c   Dyam_Create_List(V(2),V(3))
	move_ret ref[220]
	c_ret

;; TERM 776: '*IL-LOOP*'
c_code local build_ref_776
	ret_reg &ref[776]
	call_c   Dyam_Create_Atom("*IL-LOOP*")
	move_ret ref[776]
	c_ret

;; TERM 221: _H + 1
c_code local build_ref_221
	ret_reg &ref[221]
	call_c   build_ref_777()
	call_c   Dyam_Create_Binary(&ref[777],V(7),N(1))
	move_ret ref[221]
	c_ret

;; TERM 777: +
c_code local build_ref_777
	ret_reg &ref[777]
	call_c   Dyam_Create_Atom("+")
	move_ret ref[777]
	c_ret

c_code local build_seed_89
	ret_reg &seed[89]
	call_c   build_ref_222()
	call_c   build_ref_224()
	call_c   Dyam_Seed_Start(&ref[222],&ref[224],I(0),fun15,1)
	call_c   build_ref_225()
	call_c   Dyam_Seed_Add_Comp(&ref[225],&ref[224],0)
	call_c   Dyam_Seed_End()
	move_ret seed[89]
	c_ret

;; TERM 225: '*GUARD*'(std_prolog_load_args([_C|_D], 1, _G, (call('Follow_Cont', [_B]) :> reg_reset(_I)), _F)) :> '$$HOLE$$'
c_code local build_ref_225
	ret_reg &ref[225]
	call_c   build_ref_224()
	call_c   Dyam_Create_Binary(I(9),&ref[224],I(7))
	move_ret ref[225]
	c_ret

;; TERM 224: '*GUARD*'(std_prolog_load_args([_C|_D], 1, _G, (call('Follow_Cont', [_B]) :> reg_reset(_I)), _F))
c_code local build_ref_224
	ret_reg &ref[224]
	call_c   build_ref_222()
	call_c   build_ref_223()
	call_c   Dyam_Create_Unary(&ref[222],&ref[223])
	move_ret ref[224]
	c_ret

;; TERM 223: std_prolog_load_args([_C|_D], 1, _G, (call('Follow_Cont', [_B]) :> reg_reset(_I)), _F)
c_code local build_ref_223
	ret_reg &ref[223]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_735()
	call_c   build_ref_738()
	call_c   build_ref_267()
	call_c   Dyam_Create_Binary(&ref[735],&ref[738],&ref[267])
	move_ret R(0)
	call_c   build_ref_733()
	call_c   Dyam_Create_Unary(&ref[733],V(8))
	move_ret R(1)
	call_c   Dyam_Create_Binary(I(9),R(0),R(1))
	move_ret R(1)
	call_c   build_ref_778()
	call_c   build_ref_220()
	call_c   Dyam_Term_Start(&ref[778],5)
	call_c   Dyam_Term_Arg(&ref[220])
	call_c   Dyam_Term_Arg(N(1))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[223]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 778: std_prolog_load_args
c_code local build_ref_778
	ret_reg &ref[778]
	call_c   Dyam_Create_Atom("std_prolog_load_args")
	move_ret ref[778]
	c_ret

;; TERM 267: [_B]
c_code local build_ref_267
	ret_reg &ref[267]
	call_c   Dyam_Create_List(V(1),I(0))
	move_ret ref[267]
	c_ret

long local pool_fun37[5]=[4,build_ref_219,build_ref_220,build_ref_221,build_seed_89]

pl_code local fun37
	call_c   Dyam_Pool(pool_fun37)
	call_c   Dyam_Unify_Item(&ref[219])
	fail_ret
	call_c   DYAM_evpred_length(&ref[220],V(7))
	fail_ret
	call_c   DYAM_evpred_is(V(8),&ref[221])
	fail_ret
	call_c   Dyam_Allocate(0)
	pl_call  fun19(&seed[89],1)
	call_c   Dyam_Deallocate()
	pl_jump  Follow_Cont(V(0))

;; TERM 218: '*PROLOG-FIRST*'(unfold('*IL-LOOP*'(_B, [_C|_D]), _E, _F, _G, _E)) :> []
c_code local build_ref_218
	ret_reg &ref[218]
	call_c   build_ref_217()
	call_c   Dyam_Create_Binary(I(9),&ref[217],I(0))
	move_ret ref[218]
	c_ret

;; TERM 217: '*PROLOG-FIRST*'(unfold('*IL-LOOP*'(_B, [_C|_D]), _E, _F, _G, _E))
c_code local build_ref_217
	ret_reg &ref[217]
	call_c   build_ref_5()
	call_c   build_ref_216()
	call_c   Dyam_Create_Unary(&ref[5],&ref[216])
	move_ret ref[217]
	c_ret

c_code local build_seed_39
	ret_reg &seed[39]
	call_c   build_ref_4()
	call_c   build_ref_186()
	call_c   Dyam_Seed_Start(&ref[4],&ref[186],I(0),fun1,1)
	call_c   build_ref_187()
	call_c   Dyam_Seed_Add_Comp(&ref[187],fun75,0)
	call_c   Dyam_Seed_End()
	move_ret seed[39]
	c_ret

;; TERM 187: '*PROLOG-ITEM*'{top=> unfold('*WAIT*'(_B), _C, _D, _E, _C), cont=> _A}
c_code local build_ref_187
	ret_reg &ref[187]
	call_c   build_ref_724()
	call_c   build_ref_184()
	call_c   Dyam_Create_Binary(&ref[724],&ref[184],V(0))
	move_ret ref[187]
	c_ret

;; TERM 184: unfold('*WAIT*'(_B), _C, _D, _E, _C)
c_code local build_ref_184
	ret_reg &ref[184]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_779()
	call_c   Dyam_Create_Unary(&ref[779],V(1))
	move_ret R(0)
	call_c   build_ref_725()
	call_c   Dyam_Term_Start(&ref[725],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_End()
	move_ret ref[184]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 779: '*WAIT*'
c_code local build_ref_779
	ret_reg &ref[779]
	call_c   Dyam_Create_Atom("*WAIT*")
	move_ret ref[779]
	c_ret

c_code local build_seed_84
	ret_reg &seed[84]
	call_c   build_ref_59()
	call_c   build_ref_188()
	call_c   Dyam_Seed_Start(&ref[59],&ref[188],I(0),fun15,1)
	call_c   build_ref_191()
	call_c   Dyam_Seed_Add_Comp(&ref[191],&ref[188],0)
	call_c   Dyam_Seed_End()
	move_ret seed[84]
	c_ret

;; TERM 191: '*PROLOG-FIRST*'(seed_install(seed{model=> '*WAIT-CONT*'{rest=> _B, cont=> _C, tupple=> _D}, id=> _F, anchor=> _G, subs_comp=> _H, code=> _I, go=> _J, tabule=> _K, schedule=> _L, subsume=> _M, model_ref=> _N, subs_comp_ref=> _O, c_type=> _P, c_type_ref=> _Q, out_env=> _R, appinfo=> _S, tail_flag=> _T}, 12, _E)) :> '$$HOLE$$'
c_code local build_ref_191
	ret_reg &ref[191]
	call_c   build_ref_190()
	call_c   Dyam_Create_Binary(I(9),&ref[190],I(7))
	move_ret ref[191]
	c_ret

;; TERM 190: '*PROLOG-FIRST*'(seed_install(seed{model=> '*WAIT-CONT*'{rest=> _B, cont=> _C, tupple=> _D}, id=> _F, anchor=> _G, subs_comp=> _H, code=> _I, go=> _J, tabule=> _K, schedule=> _L, subsume=> _M, model_ref=> _N, subs_comp_ref=> _O, c_type=> _P, c_type_ref=> _Q, out_env=> _R, appinfo=> _S, tail_flag=> _T}, 12, _E))
c_code local build_ref_190
	ret_reg &ref[190]
	call_c   build_ref_5()
	call_c   build_ref_189()
	call_c   Dyam_Create_Unary(&ref[5],&ref[189])
	move_ret ref[190]
	c_ret

;; TERM 189: seed_install(seed{model=> '*WAIT-CONT*'{rest=> _B, cont=> _C, tupple=> _D}, id=> _F, anchor=> _G, subs_comp=> _H, code=> _I, go=> _J, tabule=> _K, schedule=> _L, subsume=> _M, model_ref=> _N, subs_comp_ref=> _O, c_type=> _P, c_type_ref=> _Q, out_env=> _R, appinfo=> _S, tail_flag=> _T}, 12, _E)
c_code local build_ref_189
	ret_reg &ref[189]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_780()
	call_c   Dyam_Term_Start(&ref[780],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_748()
	call_c   Dyam_Term_Start(&ref[748],16)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_747()
	call_c   Dyam_Term_Start(&ref[747],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(N(12))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[189]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 780: '*WAIT-CONT*'!'$ft'
c_code local build_ref_780
	ret_reg &ref[780]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_784()
	call_c   Dyam_Create_List(&ref[784],I(0))
	move_ret R(0)
	call_c   build_ref_783()
	call_c   Dyam_Create_List(&ref[783],R(0))
	move_ret R(0)
	call_c   build_ref_782()
	call_c   Dyam_Create_List(&ref[782],R(0))
	move_ret R(0)
	call_c   build_ref_781()
	call_c   Dyam_Create_Atom_Module("$ft",&ref[781])
	move_ret ref[780]
	call_c   DYAM_Feature_2(&ref[781],R(0))
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 781: '*WAIT-CONT*'
c_code local build_ref_781
	ret_reg &ref[781]
	call_c   Dyam_Create_Atom("*WAIT-CONT*")
	move_ret ref[781]
	c_ret

;; TERM 782: rest
c_code local build_ref_782
	ret_reg &ref[782]
	call_c   Dyam_Create_Atom("rest")
	move_ret ref[782]
	c_ret

;; TERM 783: cont
c_code local build_ref_783
	ret_reg &ref[783]
	call_c   Dyam_Create_Atom("cont")
	move_ret ref[783]
	c_ret

;; TERM 784: tupple
c_code local build_ref_784
	ret_reg &ref[784]
	call_c   Dyam_Create_Atom("tupple")
	move_ret ref[784]
	c_ret

;; TERM 188: '*PROLOG-ITEM*'{top=> seed_install(seed{model=> '*WAIT-CONT*'{rest=> _B, cont=> _C, tupple=> _D}, id=> _F, anchor=> _G, subs_comp=> _H, code=> _I, go=> _J, tabule=> _K, schedule=> _L, subsume=> _M, model_ref=> _N, subs_comp_ref=> _O, c_type=> _P, c_type_ref=> _Q, out_env=> _R, appinfo=> _S, tail_flag=> _T}, 12, _E), cont=> _A}
c_code local build_ref_188
	ret_reg &ref[188]
	call_c   build_ref_724()
	call_c   build_ref_189()
	call_c   Dyam_Create_Binary(&ref[724],&ref[189],V(0))
	move_ret ref[188]
	c_ret

long local pool_fun75[3]=[2,build_ref_187,build_seed_84]

pl_code local fun75
	call_c   Dyam_Pool(pool_fun75)
	call_c   Dyam_Unify_Item(&ref[187])
	fail_ret
	pl_jump  fun19(&seed[84],12)

;; TERM 186: '*PROLOG-FIRST*'(unfold('*WAIT*'(_B), _C, _D, _E, _C)) :> []
c_code local build_ref_186
	ret_reg &ref[186]
	call_c   build_ref_185()
	call_c   Dyam_Create_Binary(I(9),&ref[185],I(0))
	move_ret ref[186]
	c_ret

;; TERM 185: '*PROLOG-FIRST*'(unfold('*WAIT*'(_B), _C, _D, _E, _C))
c_code local build_ref_185
	ret_reg &ref[185]
	call_c   build_ref_5()
	call_c   build_ref_184()
	call_c   Dyam_Create_Unary(&ref[5],&ref[184])
	move_ret ref[185]
	c_ret

c_code local build_seed_36
	ret_reg &seed[36]
	call_c   build_ref_4()
	call_c   build_ref_236()
	call_c   Dyam_Seed_Start(&ref[4],&ref[236],I(0),fun1,1)
	call_c   build_ref_237()
	call_c   Dyam_Seed_Add_Comp(&ref[237],fun74,0)
	call_c   Dyam_Seed_End()
	move_ret seed[36]
	c_ret

;; TERM 237: '*PROLOG-ITEM*'{top=> unfold('*PROLOG-LAST*'(_B, _C), _D, _E, _F, _D), cont=> _A}
c_code local build_ref_237
	ret_reg &ref[237]
	call_c   build_ref_724()
	call_c   build_ref_234()
	call_c   Dyam_Create_Binary(&ref[724],&ref[234],V(0))
	move_ret ref[237]
	c_ret

;; TERM 234: unfold('*PROLOG-LAST*'(_B, _C), _D, _E, _F, _D)
c_code local build_ref_234
	ret_reg &ref[234]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_514()
	call_c   Dyam_Create_Binary(&ref[514],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_725()
	call_c   Dyam_Term_Start(&ref[725],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret ref[234]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_91
	ret_reg &seed[91]
	call_c   build_ref_59()
	call_c   build_ref_238()
	call_c   Dyam_Seed_Start(&ref[59],&ref[238],I(0),fun15,1)
	call_c   build_ref_241()
	call_c   Dyam_Seed_Add_Comp(&ref[241],&ref[238],0)
	call_c   Dyam_Seed_End()
	move_ret seed[91]
	c_ret

;; TERM 241: '*PROLOG-FIRST*'(seed_install(seed{model=> '*PROLOG-ITEM*'{top=> _B, cont=> _D}, id=> _H, anchor=> _I, subs_comp=> _J, code=> _K, go=> _L, tabule=> _M, schedule=> _N, subsume=> _O, model_ref=> _P, subs_comp_ref=> _Q, c_type=> _R, c_type_ref=> _S, out_env=> _T, appinfo=> _U, tail_flag=> _V}, _G, _F)) :> '$$HOLE$$'
c_code local build_ref_241
	ret_reg &ref[241]
	call_c   build_ref_240()
	call_c   Dyam_Create_Binary(I(9),&ref[240],I(7))
	move_ret ref[241]
	c_ret

;; TERM 240: '*PROLOG-FIRST*'(seed_install(seed{model=> '*PROLOG-ITEM*'{top=> _B, cont=> _D}, id=> _H, anchor=> _I, subs_comp=> _J, code=> _K, go=> _L, tabule=> _M, schedule=> _N, subsume=> _O, model_ref=> _P, subs_comp_ref=> _Q, c_type=> _R, c_type_ref=> _S, out_env=> _T, appinfo=> _U, tail_flag=> _V}, _G, _F))
c_code local build_ref_240
	ret_reg &ref[240]
	call_c   build_ref_5()
	call_c   build_ref_239()
	call_c   Dyam_Create_Unary(&ref[5],&ref[239])
	move_ret ref[240]
	c_ret

;; TERM 239: seed_install(seed{model=> '*PROLOG-ITEM*'{top=> _B, cont=> _D}, id=> _H, anchor=> _I, subs_comp=> _J, code=> _K, go=> _L, tabule=> _M, schedule=> _N, subsume=> _O, model_ref=> _P, subs_comp_ref=> _Q, c_type=> _R, c_type_ref=> _S, out_env=> _T, appinfo=> _U, tail_flag=> _V}, _G, _F)
c_code local build_ref_239
	ret_reg &ref[239]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_724()
	call_c   Dyam_Create_Binary(&ref[724],V(1),V(3))
	move_ret R(0)
	call_c   build_ref_748()
	call_c   Dyam_Term_Start(&ref[748],16)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_Arg(V(21))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_747()
	call_c   Dyam_Term_Start(&ref[747],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[239]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 238: '*PROLOG-ITEM*'{top=> seed_install(seed{model=> '*PROLOG-ITEM*'{top=> _B, cont=> _D}, id=> _H, anchor=> _I, subs_comp=> _J, code=> _K, go=> _L, tabule=> _M, schedule=> _N, subsume=> _O, model_ref=> _P, subs_comp_ref=> _Q, c_type=> _R, c_type_ref=> _S, out_env=> _T, appinfo=> _U, tail_flag=> _V}, _G, _F), cont=> _A}
c_code local build_ref_238
	ret_reg &ref[238]
	call_c   build_ref_724()
	call_c   build_ref_239()
	call_c   Dyam_Create_Binary(&ref[724],&ref[239],V(0))
	move_ret ref[238]
	c_ret

long local pool_fun74[3]=[2,build_ref_237,build_seed_91]

pl_code local fun74
	call_c   Dyam_Pool(pool_fun74)
	call_c   Dyam_Unify_Item(&ref[237])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(2))
	move     N(4), R(2)
	move     0, R(3)
	move     V(6), R(4)
	move     S(5), R(5)
	pl_call  pred_check_tag_3()
	call_c   Dyam_Deallocate()
	pl_jump  fun19(&seed[91],12)

;; TERM 236: '*PROLOG-FIRST*'(unfold('*PROLOG-LAST*'(_B, _C), _D, _E, _F, _D)) :> []
c_code local build_ref_236
	ret_reg &ref[236]
	call_c   build_ref_235()
	call_c   Dyam_Create_Binary(I(9),&ref[235],I(0))
	move_ret ref[236]
	c_ret

;; TERM 235: '*PROLOG-FIRST*'(unfold('*PROLOG-LAST*'(_B, _C), _D, _E, _F, _D))
c_code local build_ref_235
	ret_reg &ref[235]
	call_c   build_ref_5()
	call_c   build_ref_234()
	call_c   Dyam_Create_Unary(&ref[5],&ref[234])
	move_ret ref[235]
	c_ret

c_code local build_seed_26
	ret_reg &seed[26]
	call_c   build_ref_4()
	call_c   build_ref_244()
	call_c   Dyam_Seed_Start(&ref[4],&ref[244],I(0),fun1,1)
	call_c   build_ref_245()
	call_c   Dyam_Seed_Add_Comp(&ref[245],fun39,0)
	call_c   Dyam_Seed_End()
	move_ret seed[26]
	c_ret

;; TERM 245: '*PROLOG-ITEM*'{top=> unfold('*IL-THREAD*'(_B, _C), _D, _E, _F, _G), cont=> _A}
c_code local build_ref_245
	ret_reg &ref[245]
	call_c   build_ref_724()
	call_c   build_ref_242()
	call_c   Dyam_Create_Binary(&ref[724],&ref[242],V(0))
	move_ret ref[245]
	c_ret

;; TERM 242: unfold('*IL-THREAD*'(_B, _C), _D, _E, _F, _G)
c_code local build_ref_242
	ret_reg &ref[242]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_785()
	call_c   Dyam_Create_Binary(&ref[785],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_725()
	call_c   Dyam_Term_Start(&ref[725],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[242]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 785: '*IL-THREAD*'
c_code local build_ref_785
	ret_reg &ref[785]
	call_c   Dyam_Create_Atom("*IL-THREAD*")
	move_ret ref[785]
	c_ret

c_code local build_seed_93
	ret_reg &seed[93]
	call_c   build_ref_59()
	call_c   build_ref_249()
	call_c   Dyam_Seed_Start(&ref[59],&ref[249],I(0),fun15,1)
	call_c   build_ref_252()
	call_c   Dyam_Seed_Add_Comp(&ref[252],&ref[249],0)
	call_c   Dyam_Seed_End()
	move_ret seed[93]
	c_ret

;; TERM 252: '*PROLOG-FIRST*'(unfold((allocate :> _B :> deallocate :> succeed), _D, _H, _I, _G)) :> '$$HOLE$$'
c_code local build_ref_252
	ret_reg &ref[252]
	call_c   build_ref_251()
	call_c   Dyam_Create_Binary(I(9),&ref[251],I(7))
	move_ret ref[252]
	c_ret

;; TERM 251: '*PROLOG-FIRST*'(unfold((allocate :> _B :> deallocate :> succeed), _D, _H, _I, _G))
c_code local build_ref_251
	ret_reg &ref[251]
	call_c   build_ref_5()
	call_c   build_ref_250()
	call_c   Dyam_Create_Unary(&ref[5],&ref[250])
	move_ret ref[251]
	c_ret

;; TERM 250: unfold((allocate :> _B :> deallocate :> succeed), _D, _H, _I, _G)
c_code local build_ref_250
	ret_reg &ref[250]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_302()
	call_c   Dyam_Create_Binary(I(9),V(1),&ref[302])
	move_ret R(0)
	call_c   build_ref_299()
	call_c   Dyam_Create_Binary(I(9),&ref[299],R(0))
	move_ret R(0)
	call_c   build_ref_725()
	call_c   Dyam_Term_Start(&ref[725],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[250]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 249: '*PROLOG-ITEM*'{top=> unfold((allocate :> _B :> deallocate :> succeed), _D, _H, _I, _G), cont=> '$CLOSURE'('$fun'(38, 0, 1177519052), '$TUPPLE'(35125262024908))}
c_code local build_ref_249
	ret_reg &ref[249]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,361758720)
	move_ret R(0)
	call_c   Dyam_Closure_Aux(fun38,R(0))
	move_ret R(0)
	call_c   build_ref_724()
	call_c   build_ref_250()
	call_c   Dyam_Create_Binary(&ref[724],&ref[250],R(0))
	move_ret ref[249]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_92
	ret_reg &seed[92]
	call_c   build_ref_222()
	call_c   build_ref_247()
	call_c   Dyam_Seed_Start(&ref[222],&ref[247],I(0),fun15,1)
	call_c   build_ref_248()
	call_c   Dyam_Seed_Add_Comp(&ref[248],&ref[247],0)
	call_c   Dyam_Seed_End()
	move_ret seed[92]
	c_ret

;; TERM 248: '*GUARD*'(std_prolog_unif_args(_C, 1, _F, _I, _E)) :> '$$HOLE$$'
c_code local build_ref_248
	ret_reg &ref[248]
	call_c   build_ref_247()
	call_c   Dyam_Create_Binary(I(9),&ref[247],I(7))
	move_ret ref[248]
	c_ret

;; TERM 247: '*GUARD*'(std_prolog_unif_args(_C, 1, _F, _I, _E))
c_code local build_ref_247
	ret_reg &ref[247]
	call_c   build_ref_222()
	call_c   build_ref_246()
	call_c   Dyam_Create_Unary(&ref[222],&ref[246])
	move_ret ref[247]
	c_ret

;; TERM 246: std_prolog_unif_args(_C, 1, _F, _I, _E)
c_code local build_ref_246
	ret_reg &ref[246]
	call_c   build_ref_786()
	call_c   Dyam_Term_Start(&ref[786],5)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(N(1))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[246]
	c_ret

;; TERM 786: std_prolog_unif_args
c_code local build_ref_786
	ret_reg &ref[786]
	call_c   Dyam_Create_Atom("std_prolog_unif_args")
	move_ret ref[786]
	c_ret

long local pool_fun38[2]=[1,build_seed_92]

pl_code local fun38
	call_c   Dyam_Pool(pool_fun38)
	call_c   Dyam_Allocate(0)
	pl_call  fun19(&seed[92],1)
	call_c   Dyam_Reg_Load(0,V(4))
	move     V(9), R(2)
	move     S(5), R(3)
	pl_call  pred_oset_tupple_2()
	call_c   Dyam_Deallocate()
	pl_jump  Follow_Cont(V(0))

long local pool_fun39[3]=[2,build_ref_245,build_seed_93]

pl_code local fun39
	call_c   Dyam_Pool(pool_fun39)
	call_c   Dyam_Unify_Item(&ref[245])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(2))
	call_c   Dyam_Reg_Load(2,V(4))
	move     V(7), R(4)
	move     S(5), R(5)
	pl_call  pred_extend_tupple_3()
	call_c   Dyam_Deallocate()
	pl_jump  fun19(&seed[93],12)

;; TERM 244: '*PROLOG-FIRST*'(unfold('*IL-THREAD*'(_B, _C), _D, _E, _F, _G)) :> []
c_code local build_ref_244
	ret_reg &ref[244]
	call_c   build_ref_243()
	call_c   Dyam_Create_Binary(I(9),&ref[243],I(0))
	move_ret ref[244]
	c_ret

;; TERM 243: '*PROLOG-FIRST*'(unfold('*IL-THREAD*'(_B, _C), _D, _E, _F, _G))
c_code local build_ref_243
	ret_reg &ref[243]
	call_c   build_ref_5()
	call_c   build_ref_242()
	call_c   Dyam_Create_Unary(&ref[5],&ref[242])
	move_ret ref[243]
	c_ret

c_code local build_seed_63
	ret_reg &seed[63]
	call_c   build_ref_4()
	call_c   build_ref_228()
	call_c   Dyam_Seed_Start(&ref[4],&ref[228],I(0),fun1,1)
	call_c   build_ref_229()
	call_c   Dyam_Seed_Add_Comp(&ref[229],fun93,0)
	call_c   Dyam_Seed_End()
	move_ret seed[63]
	c_ret

;; TERM 229: '*PROLOG-ITEM*'{top=> unfold('*CONT*'(_B), _C, _D, _E, _C), cont=> _A}
c_code local build_ref_229
	ret_reg &ref[229]
	call_c   build_ref_724()
	call_c   build_ref_226()
	call_c   Dyam_Create_Binary(&ref[724],&ref[226],V(0))
	move_ret ref[229]
	c_ret

;; TERM 226: unfold('*CONT*'(_B), _C, _D, _E, _C)
c_code local build_ref_226
	ret_reg &ref[226]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_787()
	call_c   Dyam_Create_Unary(&ref[787],V(1))
	move_ret R(0)
	call_c   build_ref_725()
	call_c   Dyam_Term_Start(&ref[725],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_End()
	move_ret ref[226]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 787: '*CONT*'
c_code local build_ref_787
	ret_reg &ref[787]
	call_c   Dyam_Create_Atom("*CONT*")
	move_ret ref[787]
	c_ret

c_code local build_seed_90
	ret_reg &seed[90]
	call_c   build_ref_59()
	call_c   build_ref_230()
	call_c   Dyam_Seed_Start(&ref[59],&ref[230],I(0),fun15,1)
	call_c   build_ref_233()
	call_c   Dyam_Seed_Add_Comp(&ref[233],&ref[230],0)
	call_c   Dyam_Seed_End()
	move_ret seed[90]
	c_ret

;; TERM 233: '*PROLOG-FIRST*'(seed_install(seed{model=> ('*CONT*' :> _B(_C)(_D)), id=> _G, anchor=> _H, subs_comp=> _I, code=> _J, go=> _K, tabule=> _L, schedule=> prolog, subsume=> _M, model_ref=> _N, subs_comp_ref=> _O, c_type=> _P, c_type_ref=> _Q, out_env=> [_R], appinfo=> _S, tail_flag=> _T}, 12, _E)) :> '$$HOLE$$'
c_code local build_ref_233
	ret_reg &ref[233]
	call_c   build_ref_232()
	call_c   Dyam_Create_Binary(I(9),&ref[232],I(7))
	move_ret ref[233]
	c_ret

;; TERM 232: '*PROLOG-FIRST*'(seed_install(seed{model=> ('*CONT*' :> _B(_C)(_D)), id=> _G, anchor=> _H, subs_comp=> _I, code=> _J, go=> _K, tabule=> _L, schedule=> prolog, subsume=> _M, model_ref=> _N, subs_comp_ref=> _O, c_type=> _P, c_type_ref=> _Q, out_env=> [_R], appinfo=> _S, tail_flag=> _T}, 12, _E))
c_code local build_ref_232
	ret_reg &ref[232]
	call_c   build_ref_5()
	call_c   build_ref_231()
	call_c   Dyam_Create_Unary(&ref[5],&ref[231])
	move_ret ref[232]
	c_ret

;; TERM 231: seed_install(seed{model=> ('*CONT*' :> _B(_C)(_D)), id=> _G, anchor=> _H, subs_comp=> _I, code=> _J, go=> _K, tabule=> _L, schedule=> prolog, subsume=> _M, model_ref=> _N, subs_comp_ref=> _O, c_type=> _P, c_type_ref=> _Q, out_env=> [_R], appinfo=> _S, tail_flag=> _T}, 12, _E)
c_code local build_ref_231
	ret_reg &ref[231]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   Dyam_Create_Binary(I(3),V(1),V(2))
	move_ret R(0)
	call_c   Dyam_Create_Binary(I(3),R(0),V(3))
	move_ret R(0)
	call_c   build_ref_787()
	call_c   Dyam_Create_Binary(I(9),&ref[787],R(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(17),I(0))
	move_ret R(1)
	call_c   build_ref_748()
	call_c   build_ref_770()
	call_c   Dyam_Term_Start(&ref[748],16)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(&ref[770])
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_End()
	move_ret R(1)
	call_c   build_ref_747()
	call_c   Dyam_Term_Start(&ref[747],3)
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(N(12))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[231]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 230: '*PROLOG-ITEM*'{top=> seed_install(seed{model=> ('*CONT*' :> _B(_C)(_D)), id=> _G, anchor=> _H, subs_comp=> _I, code=> _J, go=> _K, tabule=> _L, schedule=> prolog, subsume=> _M, model_ref=> _N, subs_comp_ref=> _O, c_type=> _P, c_type_ref=> _Q, out_env=> [_R], appinfo=> _S, tail_flag=> _T}, 12, _E), cont=> _A}
c_code local build_ref_230
	ret_reg &ref[230]
	call_c   build_ref_724()
	call_c   build_ref_231()
	call_c   Dyam_Create_Binary(&ref[724],&ref[231],V(0))
	move_ret ref[230]
	c_ret

long local pool_fun93[3]=[2,build_ref_229,build_seed_90]

pl_code local fun93
	call_c   Dyam_Pool(pool_fun93)
	call_c   Dyam_Unify_Item(&ref[229])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(3))
	move     V(5), R(2)
	move     S(5), R(3)
	pl_call  pred_oset_tupple_2()
	call_c   Dyam_Deallocate()
	pl_jump  fun19(&seed[90],12)

;; TERM 228: '*PROLOG-FIRST*'(unfold('*CONT*'(_B), _C, _D, _E, _C)) :> []
c_code local build_ref_228
	ret_reg &ref[228]
	call_c   build_ref_227()
	call_c   Dyam_Create_Binary(I(9),&ref[227],I(0))
	move_ret ref[228]
	c_ret

;; TERM 227: '*PROLOG-FIRST*'(unfold('*CONT*'(_B), _C, _D, _E, _C))
c_code local build_ref_227
	ret_reg &ref[227]
	call_c   build_ref_5()
	call_c   build_ref_226()
	call_c   Dyam_Create_Unary(&ref[5],&ref[226])
	move_ret ref[227]
	c_ret

c_code local build_seed_22
	ret_reg &seed[22]
	call_c   build_ref_4()
	call_c   build_ref_255()
	call_c   Dyam_Seed_Start(&ref[4],&ref[255],I(0),fun1,1)
	call_c   build_ref_256()
	call_c   Dyam_Seed_Add_Comp(&ref[256],fun45,0)
	call_c   Dyam_Seed_End()
	move_ret seed[22]
	c_ret

;; TERM 256: '*PROLOG-ITEM*'{top=> unfold_std_trans(_B, (item_unify(_C) :> allocate :> _D), _E), cont=> _A}
c_code local build_ref_256
	ret_reg &ref[256]
	call_c   build_ref_724()
	call_c   build_ref_253()
	call_c   Dyam_Create_Binary(&ref[724],&ref[253],V(0))
	move_ret ref[256]
	c_ret

;; TERM 253: unfold_std_trans(_B, (item_unify(_C) :> allocate :> _D), _E)
c_code local build_ref_253
	ret_reg &ref[253]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_789()
	call_c   Dyam_Create_Unary(&ref[789],V(2))
	move_ret R(0)
	call_c   build_ref_299()
	call_c   Dyam_Create_Binary(I(9),&ref[299],V(3))
	move_ret R(1)
	call_c   Dyam_Create_Binary(I(9),R(0),R(1))
	move_ret R(1)
	call_c   build_ref_788()
	call_c   Dyam_Term_Start(&ref[788],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[253]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 788: unfold_std_trans
c_code local build_ref_788
	ret_reg &ref[788]
	call_c   Dyam_Create_Atom("unfold_std_trans")
	move_ret ref[788]
	c_ret

;; TERM 789: item_unify
c_code local build_ref_789
	ret_reg &ref[789]
	call_c   Dyam_Create_Atom("item_unify")
	move_ret ref[789]
	c_ret

;; TERM 266: 'Bad transition format ~w'
c_code local build_ref_266
	ret_reg &ref[266]
	call_c   Dyam_Create_Atom("Bad transition format ~w")
	move_ret ref[266]
	c_ret

c_code local build_seed_94
	ret_reg &seed[94]
	call_c   build_ref_59()
	call_c   build_ref_260()
	call_c   Dyam_Seed_Start(&ref[59],&ref[260],I(0),fun15,1)
	call_c   build_ref_263()
	call_c   Dyam_Seed_Add_Comp(&ref[263],&ref[260],0)
	call_c   Dyam_Seed_End()
	move_ret seed[94]
	c_ret

;; TERM 263: '*PROLOG-FIRST*'(unfold((_L :> deallocate :> succeed), _I, _M, _D, _E)) :> '$$HOLE$$'
c_code local build_ref_263
	ret_reg &ref[263]
	call_c   build_ref_262()
	call_c   Dyam_Create_Binary(I(9),&ref[262],I(7))
	move_ret ref[263]
	c_ret

;; TERM 262: '*PROLOG-FIRST*'(unfold((_L :> deallocate :> succeed), _I, _M, _D, _E))
c_code local build_ref_262
	ret_reg &ref[262]
	call_c   build_ref_5()
	call_c   build_ref_261()
	call_c   Dyam_Create_Unary(&ref[5],&ref[261])
	move_ret ref[262]
	c_ret

;; TERM 261: unfold((_L :> deallocate :> succeed), _I, _M, _D, _E)
c_code local build_ref_261
	ret_reg &ref[261]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_302()
	call_c   Dyam_Create_Binary(I(9),V(11),&ref[302])
	move_ret R(0)
	call_c   build_ref_725()
	call_c   Dyam_Term_Start(&ref[725],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[261]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 260: '*PROLOG-ITEM*'{top=> unfold((_L :> deallocate :> succeed), _I, _M, _D, _E), cont=> _A}
c_code local build_ref_260
	ret_reg &ref[260]
	call_c   build_ref_724()
	call_c   build_ref_261()
	call_c   Dyam_Create_Binary(&ref[724],&ref[261],V(0))
	move_ret ref[260]
	c_ret

long local pool_fun40[2]=[1,build_seed_94]

pl_code local fun41
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(11),V(7))
	fail_ret
fun40:
	call_c   Dyam_Reg_Load(0,V(5))
	call_c   Dyam_Reg_Load(2,V(9))
	move     V(12), R(4)
	move     S(5), R(5)
	pl_call  pred_extend_tupple_3()
	call_c   Dyam_Reg_Load(0,V(5))
	call_c   Dyam_Reg_Load(2,V(2))
	pl_call  pred_label_term_2()
	call_c   Dyam_Deallocate()
	pl_jump  fun19(&seed[94],12)


;; TERM 259: '*HIDE-ARGS*'(_K, _L)
c_code local build_ref_259
	ret_reg &ref[259]
	call_c   build_ref_739()
	call_c   Dyam_Create_Binary(&ref[739],V(10),V(11))
	move_ret ref[259]
	c_ret

long local pool_fun42[4]=[131073,build_ref_259,pool_fun40,pool_fun40]

long local pool_fun43[4]=[65538,build_ref_266,build_ref_267,pool_fun42]

pl_code local fun43
	call_c   Dyam_Remove_Choice()
	move     &ref[266], R(0)
	move     0, R(1)
	move     &ref[267], R(2)
	move     S(5), R(3)
	pl_call  pred_internal_error_2()
fun42:
	call_c   Dyam_Choice(fun41)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(7),&ref[259])
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun40()


;; TERM 265: _F :> _H(_I)(_J)
c_code local build_ref_265
	ret_reg &ref[265]
	call_c   build_ref_264()
	call_c   Dyam_Create_Binary(I(9),V(5),&ref[264])
	move_ret ref[265]
	c_ret

;; TERM 264: _H(_I)(_J)
c_code local build_ref_264
	ret_reg &ref[264]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Binary(I(3),V(7),V(8))
	move_ret R(0)
	call_c   Dyam_Create_Binary(I(3),R(0),V(9))
	move_ret ref[264]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun44[4]=[131073,build_ref_265,pool_fun43,pool_fun42]

pl_code local fun44
	call_c   Dyam_Update_Choice(fun43)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[265])
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun42()

;; TERM 258: _F :> _G ^ _H(_I)(_J)
c_code local build_ref_258
	ret_reg &ref[258]
	call_c   build_ref_257()
	call_c   Dyam_Create_Binary(I(9),V(5),&ref[257])
	move_ret ref[258]
	c_ret

;; TERM 257: _G ^ _H(_I)(_J)
c_code local build_ref_257
	ret_reg &ref[257]
	call_c   build_ref_790()
	call_c   build_ref_264()
	call_c   Dyam_Create_Binary(&ref[790],V(6),&ref[264])
	move_ret ref[257]
	c_ret

;; TERM 790: ^
c_code local build_ref_790
	ret_reg &ref[790]
	call_c   Dyam_Create_Atom("^")
	move_ret ref[790]
	c_ret

long local pool_fun45[5]=[131074,build_ref_256,build_ref_258,pool_fun44,pool_fun42]

pl_code local fun45
	call_c   Dyam_Pool(pool_fun45)
	call_c   Dyam_Unify_Item(&ref[256])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun44)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[258])
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun42()

;; TERM 255: '*PROLOG-FIRST*'(unfold_std_trans(_B, (item_unify(_C) :> allocate :> _D), _E)) :> []
c_code local build_ref_255
	ret_reg &ref[255]
	call_c   build_ref_254()
	call_c   Dyam_Create_Binary(I(9),&ref[254],I(0))
	move_ret ref[255]
	c_ret

;; TERM 254: '*PROLOG-FIRST*'(unfold_std_trans(_B, (item_unify(_C) :> allocate :> _D), _E))
c_code local build_ref_254
	ret_reg &ref[254]
	call_c   build_ref_5()
	call_c   build_ref_253()
	call_c   Dyam_Create_Unary(&ref[5],&ref[253])
	move_ret ref[254]
	c_ret

c_code local build_seed_43
	ret_reg &seed[43]
	call_c   build_ref_4()
	call_c   build_ref_278()
	call_c   Dyam_Seed_Start(&ref[4],&ref[278],I(0),fun1,1)
	call_c   build_ref_279()
	call_c   Dyam_Seed_Add_Comp(&ref[279],fun102,0)
	call_c   Dyam_Seed_End()
	move_ret seed[43]
	c_ret

;; TERM 279: '*PROLOG-ITEM*'{top=> unfold('*LIGHTLCFIRST*'(_B, _C, _D), _E, _F, _G, _H), cont=> _A}
c_code local build_ref_279
	ret_reg &ref[279]
	call_c   build_ref_724()
	call_c   build_ref_276()
	call_c   Dyam_Create_Binary(&ref[724],&ref[276],V(0))
	move_ret ref[279]
	c_ret

;; TERM 276: unfold('*LIGHTLCFIRST*'(_B, _C, _D), _E, _F, _G, _H)
c_code local build_ref_276
	ret_reg &ref[276]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_791()
	call_c   Dyam_Term_Start(&ref[791],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_725()
	call_c   Dyam_Term_Start(&ref[725],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[276]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 791: '*LIGHTLCFIRST*'
c_code local build_ref_791
	ret_reg &ref[791]
	call_c   Dyam_Create_Atom("*LIGHTLCFIRST*")
	move_ret ref[791]
	c_ret

;; TERM 410: _B ^ _J ^ _K
c_code local build_ref_410
	ret_reg &ref[410]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_790()
	call_c   Dyam_Create_Binary(&ref[790],V(9),V(10))
	move_ret R(0)
	call_c   Dyam_Create_Binary(&ref[790],V(1),R(0))
	move_ret ref[410]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_112
	ret_reg &seed[112]
	call_c   build_ref_59()
	call_c   build_ref_412()
	call_c   Dyam_Seed_Start(&ref[59],&ref[412],I(0),fun15,1)
	call_c   build_ref_415()
	call_c   Dyam_Seed_Add_Comp(&ref[415],&ref[412],0)
	call_c   Dyam_Seed_End()
	move_ret seed[112]
	c_ret

;; TERM 415: '*PROLOG-FIRST*'(unfold(_K, [], _F, _L, _M)) :> '$$HOLE$$'
c_code local build_ref_415
	ret_reg &ref[415]
	call_c   build_ref_414()
	call_c   Dyam_Create_Binary(I(9),&ref[414],I(7))
	move_ret ref[415]
	c_ret

;; TERM 414: '*PROLOG-FIRST*'(unfold(_K, [], _F, _L, _M))
c_code local build_ref_414
	ret_reg &ref[414]
	call_c   build_ref_5()
	call_c   build_ref_413()
	call_c   Dyam_Create_Unary(&ref[5],&ref[413])
	move_ret ref[414]
	c_ret

;; TERM 413: unfold(_K, [], _F, _L, _M)
c_code local build_ref_413
	ret_reg &ref[413]
	call_c   build_ref_725()
	call_c   Dyam_Term_Start(&ref[725],5)
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_End()
	move_ret ref[413]
	c_ret

;; TERM 412: '*PROLOG-ITEM*'{top=> unfold(_K, [], _F, _L, _M), cont=> '$CLOSURE'('$fun'(100, 0, 1178015620), '$TUPPLE'(35125252331928))}
c_code local build_ref_412
	ret_reg &ref[412]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,501874688)
	move_ret R(0)
	call_c   Dyam_Closure_Aux(fun100,R(0))
	move_ret R(0)
	call_c   build_ref_724()
	call_c   build_ref_413()
	call_c   Dyam_Create_Binary(&ref[724],&ref[413],R(0))
	move_ret ref[412]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 411: [key(lcselect, [_B,_J], _L)]
c_code local build_ref_411
	ret_reg &ref[411]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(9),I(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(1),R(0))
	move_ret R(0)
	call_c   build_ref_792()
	call_c   build_ref_793()
	call_c   Dyam_Term_Start(&ref[792],3)
	call_c   Dyam_Term_Arg(&ref[793])
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   Dyam_Create_List(R(0),I(0))
	move_ret ref[411]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 793: lcselect
c_code local build_ref_793
	ret_reg &ref[793]
	call_c   Dyam_Create_Atom("lcselect")
	move_ret ref[793]
	c_ret

;; TERM 792: key
c_code local build_ref_792
	ret_reg &ref[792]
	call_c   Dyam_Create_Atom("key")
	move_ret ref[792]
	c_ret

c_code local build_seed_96
	ret_reg &seed[96]
	call_c   build_ref_59()
	call_c   build_ref_280()
	call_c   Dyam_Seed_Start(&ref[59],&ref[280],I(0),fun15,1)
	call_c   build_ref_283()
	call_c   Dyam_Seed_Add_Comp(&ref[283],&ref[280],0)
	call_c   Dyam_Seed_End()
	move_ret seed[96]
	c_ret

;; TERM 283: '*PROLOG-FIRST*'(seed_install(seed{model=> ('*LCFIRST*'(_B) :> _C(_E)(_F)), id=> _N, anchor=> _O, subs_comp=> _P, code=> _Q, go=> _R, tabule=> light, schedule=> prolog, subsume=> _S, model_ref=> _T, subs_comp_ref=> _U, c_type=> _V, c_type_ref=> _W, out_env=> [_H], appinfo=> _I, tail_flag=> _X}, 2, _G)) :> '$$HOLE$$'
c_code local build_ref_283
	ret_reg &ref[283]
	call_c   build_ref_282()
	call_c   Dyam_Create_Binary(I(9),&ref[282],I(7))
	move_ret ref[283]
	c_ret

;; TERM 282: '*PROLOG-FIRST*'(seed_install(seed{model=> ('*LCFIRST*'(_B) :> _C(_E)(_F)), id=> _N, anchor=> _O, subs_comp=> _P, code=> _Q, go=> _R, tabule=> light, schedule=> prolog, subsume=> _S, model_ref=> _T, subs_comp_ref=> _U, c_type=> _V, c_type_ref=> _W, out_env=> [_H], appinfo=> _I, tail_flag=> _X}, 2, _G))
c_code local build_ref_282
	ret_reg &ref[282]
	call_c   build_ref_5()
	call_c   build_ref_281()
	call_c   Dyam_Create_Unary(&ref[5],&ref[281])
	move_ret ref[282]
	c_ret

;; TERM 281: seed_install(seed{model=> ('*LCFIRST*'(_B) :> _C(_E)(_F)), id=> _N, anchor=> _O, subs_comp=> _P, code=> _Q, go=> _R, tabule=> light, schedule=> prolog, subsume=> _S, model_ref=> _T, subs_comp_ref=> _U, c_type=> _V, c_type_ref=> _W, out_env=> [_H], appinfo=> _I, tail_flag=> _X}, 2, _G)
c_code local build_ref_281
	ret_reg &ref[281]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_794()
	call_c   Dyam_Create_Unary(&ref[794],V(1))
	move_ret R(0)
	call_c   Dyam_Create_Binary(I(3),V(2),V(4))
	move_ret R(1)
	call_c   Dyam_Create_Binary(I(3),R(1),V(5))
	move_ret R(1)
	call_c   Dyam_Create_Binary(I(9),R(0),R(1))
	move_ret R(1)
	call_c   Dyam_Create_List(V(7),I(0))
	move_ret R(0)
	call_c   build_ref_748()
	call_c   build_ref_795()
	call_c   build_ref_770()
	call_c   Dyam_Term_Start(&ref[748],16)
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(&ref[795])
	call_c   Dyam_Term_Arg(&ref[770])
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_Arg(V(21))
	call_c   Dyam_Term_Arg(V(22))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(23))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_747()
	call_c   Dyam_Term_Start(&ref[747],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(N(2))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[281]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 795: light
c_code local build_ref_795
	ret_reg &ref[795]
	call_c   Dyam_Create_Atom("light")
	move_ret ref[795]
	c_ret

;; TERM 794: '*LCFIRST*'
c_code local build_ref_794
	ret_reg &ref[794]
	call_c   Dyam_Create_Atom("*LCFIRST*")
	move_ret ref[794]
	c_ret

;; TERM 280: '*PROLOG-ITEM*'{top=> seed_install(seed{model=> ('*LCFIRST*'(_B) :> _C(_E)(_F)), id=> _N, anchor=> _O, subs_comp=> _P, code=> _Q, go=> _R, tabule=> light, schedule=> prolog, subsume=> _S, model_ref=> _T, subs_comp_ref=> _U, c_type=> _V, c_type_ref=> _W, out_env=> [_H], appinfo=> _I, tail_flag=> _X}, 2, _G), cont=> _A}
c_code local build_ref_280
	ret_reg &ref[280]
	call_c   build_ref_724()
	call_c   build_ref_281()
	call_c   Dyam_Create_Binary(&ref[724],&ref[281],V(0))
	move_ret ref[280]
	c_ret

long local pool_fun99[2]=[1,build_seed_96]

long local pool_fun100[3]=[65537,build_ref_411,pool_fun99]

pl_code local fun100
	call_c   Dyam_Pool(pool_fun100)
	call_c   Dyam_Unify(V(8),&ref[411])
	fail_ret
	call_c   Dyam_Allocate(0)
fun99:
	call_c   Dyam_Deallocate()
	pl_jump  fun19(&seed[96],12)


long local pool_fun101[3]=[2,build_ref_410,build_seed_112]

pl_code local fun101
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(3),&ref[410])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_jump  fun19(&seed[112],12)

long local pool_fun102[4]=[131073,build_ref_279,pool_fun101,pool_fun99]

pl_code local fun102
	call_c   Dyam_Pool(pool_fun102)
	call_c   Dyam_Unify_Item(&ref[279])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun101)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(3),I(0))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(8),I(0))
	fail_ret
	pl_jump  fun99()

;; TERM 278: '*PROLOG-FIRST*'(unfold('*LIGHTLCFIRST*'(_B, _C, _D), _E, _F, _G, _H)) :> []
c_code local build_ref_278
	ret_reg &ref[278]
	call_c   build_ref_277()
	call_c   Dyam_Create_Binary(I(9),&ref[277],I(0))
	move_ret ref[278]
	c_ret

;; TERM 277: '*PROLOG-FIRST*'(unfold('*LIGHTLCFIRST*'(_B, _C, _D), _E, _F, _G, _H))
c_code local build_ref_277
	ret_reg &ref[277]
	call_c   build_ref_5()
	call_c   build_ref_276()
	call_c   Dyam_Create_Unary(&ref[5],&ref[276])
	move_ret ref[277]
	c_ret

c_code local build_seed_41
	ret_reg &seed[41]
	call_c   build_ref_4()
	call_c   build_ref_270()
	call_c   Dyam_Seed_Start(&ref[4],&ref[270],I(0),fun1,1)
	call_c   build_ref_271()
	call_c   Dyam_Seed_Add_Comp(&ref[271],fun106,0)
	call_c   Dyam_Seed_End()
	move_ret seed[41]
	c_ret

;; TERM 271: '*PROLOG-ITEM*'{top=> unfold('*LCFIRST*'(_B, _C, _D), _E, _F, _G, _H), cont=> _A}
c_code local build_ref_271
	ret_reg &ref[271]
	call_c   build_ref_724()
	call_c   build_ref_268()
	call_c   Dyam_Create_Binary(&ref[724],&ref[268],V(0))
	move_ret ref[271]
	c_ret

;; TERM 268: unfold('*LCFIRST*'(_B, _C, _D), _E, _F, _G, _H)
c_code local build_ref_268
	ret_reg &ref[268]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_794()
	call_c   Dyam_Term_Start(&ref[794],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_725()
	call_c   Dyam_Term_Start(&ref[725],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[268]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_113
	ret_reg &seed[113]
	call_c   build_ref_59()
	call_c   build_ref_416()
	call_c   Dyam_Seed_Start(&ref[59],&ref[416],I(0),fun15,1)
	call_c   build_ref_415()
	call_c   Dyam_Seed_Add_Comp(&ref[415],&ref[416],0)
	call_c   Dyam_Seed_End()
	move_ret seed[113]
	c_ret

;; TERM 416: '*PROLOG-ITEM*'{top=> unfold(_K, [], _F, _L, _M), cont=> '$CLOSURE'('$fun'(104, 0, 1178029356), '$TUPPLE'(35125252331928))}
c_code local build_ref_416
	ret_reg &ref[416]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,501874688)
	move_ret R(0)
	call_c   Dyam_Closure_Aux(fun104,R(0))
	move_ret R(0)
	call_c   build_ref_724()
	call_c   build_ref_413()
	call_c   Dyam_Create_Binary(&ref[724],&ref[413],R(0))
	move_ret ref[416]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_95
	ret_reg &seed[95]
	call_c   build_ref_59()
	call_c   build_ref_272()
	call_c   Dyam_Seed_Start(&ref[59],&ref[272],I(0),fun15,1)
	call_c   build_ref_275()
	call_c   Dyam_Seed_Add_Comp(&ref[275],&ref[272],0)
	call_c   Dyam_Seed_End()
	move_ret seed[95]
	c_ret

;; TERM 275: '*PROLOG-FIRST*'(seed_install(seed{model=> ('*LCFIRST*'(_B) :> _C(_E)(_F)), id=> _N, anchor=> _O, subs_comp=> _P, code=> _Q, go=> _R, tabule=> _S, schedule=> _T, subsume=> _U, model_ref=> _V, subs_comp_ref=> _W, c_type=> _X, c_type_ref=> _Y, out_env=> [_H], appinfo=> _I, tail_flag=> _Z}, 2, _G)) :> '$$HOLE$$'
c_code local build_ref_275
	ret_reg &ref[275]
	call_c   build_ref_274()
	call_c   Dyam_Create_Binary(I(9),&ref[274],I(7))
	move_ret ref[275]
	c_ret

;; TERM 274: '*PROLOG-FIRST*'(seed_install(seed{model=> ('*LCFIRST*'(_B) :> _C(_E)(_F)), id=> _N, anchor=> _O, subs_comp=> _P, code=> _Q, go=> _R, tabule=> _S, schedule=> _T, subsume=> _U, model_ref=> _V, subs_comp_ref=> _W, c_type=> _X, c_type_ref=> _Y, out_env=> [_H], appinfo=> _I, tail_flag=> _Z}, 2, _G))
c_code local build_ref_274
	ret_reg &ref[274]
	call_c   build_ref_5()
	call_c   build_ref_273()
	call_c   Dyam_Create_Unary(&ref[5],&ref[273])
	move_ret ref[274]
	c_ret

;; TERM 273: seed_install(seed{model=> ('*LCFIRST*'(_B) :> _C(_E)(_F)), id=> _N, anchor=> _O, subs_comp=> _P, code=> _Q, go=> _R, tabule=> _S, schedule=> _T, subsume=> _U, model_ref=> _V, subs_comp_ref=> _W, c_type=> _X, c_type_ref=> _Y, out_env=> [_H], appinfo=> _I, tail_flag=> _Z}, 2, _G)
c_code local build_ref_273
	ret_reg &ref[273]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_794()
	call_c   Dyam_Create_Unary(&ref[794],V(1))
	move_ret R(0)
	call_c   Dyam_Create_Binary(I(3),V(2),V(4))
	move_ret R(1)
	call_c   Dyam_Create_Binary(I(3),R(1),V(5))
	move_ret R(1)
	call_c   Dyam_Create_Binary(I(9),R(0),R(1))
	move_ret R(1)
	call_c   Dyam_Create_List(V(7),I(0))
	move_ret R(0)
	call_c   build_ref_748()
	call_c   Dyam_Term_Start(&ref[748],16)
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_Arg(V(21))
	call_c   Dyam_Term_Arg(V(22))
	call_c   Dyam_Term_Arg(V(23))
	call_c   Dyam_Term_Arg(V(24))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(25))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_747()
	call_c   Dyam_Term_Start(&ref[747],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(N(2))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[273]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 272: '*PROLOG-ITEM*'{top=> seed_install(seed{model=> ('*LCFIRST*'(_B) :> _C(_E)(_F)), id=> _N, anchor=> _O, subs_comp=> _P, code=> _Q, go=> _R, tabule=> _S, schedule=> _T, subsume=> _U, model_ref=> _V, subs_comp_ref=> _W, c_type=> _X, c_type_ref=> _Y, out_env=> [_H], appinfo=> _I, tail_flag=> _Z}, 2, _G), cont=> _A}
c_code local build_ref_272
	ret_reg &ref[272]
	call_c   build_ref_724()
	call_c   build_ref_273()
	call_c   Dyam_Create_Binary(&ref[724],&ref[273],V(0))
	move_ret ref[272]
	c_ret

long local pool_fun103[2]=[1,build_seed_95]

long local pool_fun104[3]=[65537,build_ref_411,pool_fun103]

pl_code local fun104
	call_c   Dyam_Pool(pool_fun104)
	call_c   Dyam_Unify(V(8),&ref[411])
	fail_ret
	call_c   Dyam_Allocate(0)
fun103:
	call_c   Dyam_Deallocate()
	pl_jump  fun19(&seed[95],12)


long local pool_fun105[3]=[2,build_ref_410,build_seed_113]

pl_code local fun105
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(3),&ref[410])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_jump  fun19(&seed[113],12)

long local pool_fun106[4]=[131073,build_ref_271,pool_fun105,pool_fun103]

pl_code local fun106
	call_c   Dyam_Pool(pool_fun106)
	call_c   Dyam_Unify_Item(&ref[271])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun105)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(3),I(0))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(8),I(0))
	fail_ret
	pl_jump  fun103()

;; TERM 270: '*PROLOG-FIRST*'(unfold('*LCFIRST*'(_B, _C, _D), _E, _F, _G, _H)) :> []
c_code local build_ref_270
	ret_reg &ref[270]
	call_c   build_ref_269()
	call_c   Dyam_Create_Binary(I(9),&ref[269],I(0))
	move_ret ref[270]
	c_ret

;; TERM 269: '*PROLOG-FIRST*'(unfold('*LCFIRST*'(_B, _C, _D), _E, _F, _G, _H))
c_code local build_ref_269
	ret_reg &ref[269]
	call_c   build_ref_5()
	call_c   build_ref_268()
	call_c   Dyam_Create_Unary(&ref[5],&ref[268])
	move_ret ref[269]
	c_ret

c_code local build_seed_62
	ret_reg &seed[62]
	call_c   build_ref_4()
	call_c   build_ref_286()
	call_c   Dyam_Seed_Start(&ref[4],&ref[286],I(0),fun1,1)
	call_c   build_ref_287()
	call_c   Dyam_Seed_Add_Comp(&ref[287],fun50,0)
	call_c   Dyam_Seed_End()
	move_ret seed[62]
	c_ret

;; TERM 287: '*PROLOG-ITEM*'{top=> unfold('*ALTERNATIVE-LAYER*'(_B, _C), _D, _E, _F, _G), cont=> _A}
c_code local build_ref_287
	ret_reg &ref[287]
	call_c   build_ref_724()
	call_c   build_ref_284()
	call_c   Dyam_Create_Binary(&ref[724],&ref[284],V(0))
	move_ret ref[287]
	c_ret

;; TERM 284: unfold('*ALTERNATIVE-LAYER*'(_B, _C), _D, _E, _F, _G)
c_code local build_ref_284
	ret_reg &ref[284]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_796()
	call_c   Dyam_Create_Binary(&ref[796],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_725()
	call_c   Dyam_Term_Start(&ref[725],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[284]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 796: '*ALTERNATIVE-LAYER*'
c_code local build_ref_796
	ret_reg &ref[796]
	call_c   Dyam_Create_Atom("*ALTERNATIVE-LAYER*")
	move_ret ref[796]
	c_ret

c_code local build_seed_98
	ret_reg &seed[98]
	call_c   build_ref_59()
	call_c   build_ref_294()
	call_c   Dyam_Seed_Start(&ref[59],&ref[294],I(0),fun15,1)
	call_c   build_ref_203()
	call_c   Dyam_Seed_Add_Comp(&ref[203],&ref[294],0)
	call_c   Dyam_Seed_End()
	move_ret seed[98]
	c_ret

;; TERM 294: '*PROLOG-ITEM*'{top=> unfold(_B, _D, _E, _H, _G), cont=> '$CLOSURE'('$fun'(49, 0, 1177613588), '$TUPPLE'(35125262287180))}
c_code local build_ref_294
	ret_reg &ref[294]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,534773760)
	move_ret R(0)
	call_c   Dyam_Closure_Aux(fun49,R(0))
	move_ret R(0)
	call_c   build_ref_724()
	call_c   build_ref_201()
	call_c   Dyam_Create_Binary(&ref[724],&ref[201],R(0))
	move_ret ref[294]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_97
	ret_reg &seed[97]
	call_c   build_ref_59()
	call_c   build_ref_290()
	call_c   Dyam_Seed_Start(&ref[59],&ref[290],I(0),fun15,1)
	call_c   build_ref_293()
	call_c   Dyam_Seed_Add_Comp(&ref[293],&ref[290],0)
	call_c   Dyam_Seed_End()
	move_ret seed[97]
	c_ret

;; TERM 293: '*PROLOG-FIRST*'(unfold((_C :> deallocate_layer :> deallocate :> succeed), _D, _E, _I, _J)) :> '$$HOLE$$'
c_code local build_ref_293
	ret_reg &ref[293]
	call_c   build_ref_292()
	call_c   Dyam_Create_Binary(I(9),&ref[292],I(7))
	move_ret ref[293]
	c_ret

;; TERM 292: '*PROLOG-FIRST*'(unfold((_C :> deallocate_layer :> deallocate :> succeed), _D, _E, _I, _J))
c_code local build_ref_292
	ret_reg &ref[292]
	call_c   build_ref_5()
	call_c   build_ref_291()
	call_c   Dyam_Create_Unary(&ref[5],&ref[291])
	move_ret ref[292]
	c_ret

;; TERM 291: unfold((_C :> deallocate_layer :> deallocate :> succeed), _D, _E, _I, _J)
c_code local build_ref_291
	ret_reg &ref[291]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_422()
	call_c   Dyam_Create_Binary(I(9),V(2),&ref[422])
	move_ret R(0)
	call_c   build_ref_725()
	call_c   Dyam_Term_Start(&ref[725],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret ref[291]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 422: deallocate_layer :> deallocate :> succeed
c_code local build_ref_422
	ret_reg &ref[422]
	call_c   build_ref_421()
	call_c   build_ref_302()
	call_c   Dyam_Create_Binary(I(9),&ref[421],&ref[302])
	move_ret ref[422]
	c_ret

;; TERM 421: deallocate_layer
c_code local build_ref_421
	ret_reg &ref[421]
	call_c   Dyam_Create_Atom("deallocate_layer")
	move_ret ref[421]
	c_ret

;; TERM 290: '*PROLOG-ITEM*'{top=> unfold((_C :> deallocate_layer :> deallocate :> succeed), _D, _E, _I, _J), cont=> '$CLOSURE'('$fun'(48, 0, 1177604480), '$TUPPLE'(35125262287160))}
c_code local build_ref_290
	ret_reg &ref[290]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,486014976)
	move_ret R(0)
	call_c   Dyam_Closure_Aux(fun48,R(0))
	move_ret R(0)
	call_c   build_ref_724()
	call_c   build_ref_291()
	call_c   Dyam_Create_Binary(&ref[724],&ref[291],R(0))
	move_ret ref[290]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 288: 'Unfolding alternatives do not return same env: ~w and ~w\n\tOut_Env1=~w\n\tOut_Env2=~w'
c_code local build_ref_288
	ret_reg &ref[288]
	call_c   Dyam_Create_Atom("Unfolding alternatives do not return same env: ~w and ~w\n\tOut_Env1=~w\n\tOut_Env2=~w")
	move_ret ref[288]
	c_ret

;; TERM 289: [_B,_C,_G,_J]
c_code local build_ref_289
	ret_reg &ref[289]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(9),I(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(6),R(0))
	move_ret R(0)
	call_c   Dyam_Create_Tupple(1,2,R(0))
	move_ret ref[289]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun47[3]=[2,build_ref_288,build_ref_289]

pl_code local fun47
	call_c   Dyam_Remove_Choice()
	move     &ref[288], R(0)
	move     0, R(1)
	move     &ref[289], R(2)
	move     S(5), R(3)
	pl_call  pred_internal_error_2()
fun46:
	call_c   Dyam_Reg_Load(0,V(8))
	call_c   Dyam_Reg_Load(2,V(7))
	call_c   Dyam_Reg_Load(4,V(5))
	pl_call  pred_choice_code_3()
	call_c   Dyam_Deallocate()
	pl_jump  Follow_Cont(V(0))


long local pool_fun48[2]=[65536,pool_fun47]

pl_code local fun48
	call_c   Dyam_Pool(pool_fun48)
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun47)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(6),V(9))
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun46()

long local pool_fun49[2]=[1,build_seed_97]

pl_code local fun49
	call_c   Dyam_Pool(pool_fun49)
	pl_jump  fun19(&seed[97],12)

long local pool_fun50[3]=[2,build_ref_287,build_seed_98]

pl_code local fun50
	call_c   Dyam_Pool(pool_fun50)
	call_c   Dyam_Unify_Item(&ref[287])
	fail_ret
	pl_jump  fun19(&seed[98],12)

;; TERM 286: '*PROLOG-FIRST*'(unfold('*ALTERNATIVE-LAYER*'(_B, _C), _D, _E, _F, _G)) :> []
c_code local build_ref_286
	ret_reg &ref[286]
	call_c   build_ref_285()
	call_c   Dyam_Create_Binary(I(9),&ref[285],I(0))
	move_ret ref[286]
	c_ret

;; TERM 285: '*PROLOG-FIRST*'(unfold('*ALTERNATIVE-LAYER*'(_B, _C), _D, _E, _F, _G))
c_code local build_ref_285
	ret_reg &ref[285]
	call_c   build_ref_5()
	call_c   build_ref_284()
	call_c   Dyam_Create_Unary(&ref[5],&ref[284])
	move_ret ref[285]
	c_ret

c_code local build_seed_71
	ret_reg &seed[71]
	call_c   build_ref_4()
	call_c   build_ref_297()
	call_c   Dyam_Seed_Start(&ref[4],&ref[297],I(0),fun1,1)
	call_c   build_ref_298()
	call_c   Dyam_Seed_Add_Comp(&ref[298],fun52,0)
	call_c   Dyam_Seed_End()
	move_ret seed[71]
	c_ret

;; TERM 298: '*PROLOG-ITEM*'{top=> build_closure(_B, _C, _D, '$CLOSURE'(_E, _F), _G), cont=> _A}
c_code local build_ref_298
	ret_reg &ref[298]
	call_c   build_ref_724()
	call_c   build_ref_295()
	call_c   Dyam_Create_Binary(&ref[724],&ref[295],V(0))
	move_ret ref[298]
	c_ret

;; TERM 295: build_closure(_B, _C, _D, '$CLOSURE'(_E, _F), _G)
c_code local build_ref_295
	ret_reg &ref[295]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_544()
	call_c   Dyam_Create_Binary(&ref[544],V(4),V(5))
	move_ret R(0)
	call_c   build_ref_797()
	call_c   Dyam_Term_Start(&ref[797],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[295]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 797: build_closure
c_code local build_ref_797
	ret_reg &ref[797]
	call_c   Dyam_Create_Atom("build_closure")
	move_ret ref[797]
	c_ret

;; TERM 544: '$CLOSURE'
c_code local build_ref_544
	ret_reg &ref[544]
	call_c   Dyam_Create_Atom("$CLOSURE")
	move_ret ref[544]
	c_ret

c_code local build_seed_99
	ret_reg &seed[99]
	call_c   build_ref_59()
	call_c   build_ref_305()
	call_c   Dyam_Seed_Start(&ref[59],&ref[305],I(0),fun15,1)
	call_c   build_ref_308()
	call_c   Dyam_Seed_Add_Comp(&ref[308],&ref[305],0)
	call_c   Dyam_Seed_End()
	move_ret seed[99]
	c_ret

;; TERM 308: '*PROLOG-FIRST*'(unfold(_B, _C, _D, _H, _G)) :> '$$HOLE$$'
c_code local build_ref_308
	ret_reg &ref[308]
	call_c   build_ref_307()
	call_c   Dyam_Create_Binary(I(9),&ref[307],I(7))
	move_ret ref[308]
	c_ret

;; TERM 307: '*PROLOG-FIRST*'(unfold(_B, _C, _D, _H, _G))
c_code local build_ref_307
	ret_reg &ref[307]
	call_c   build_ref_5()
	call_c   build_ref_306()
	call_c   Dyam_Create_Unary(&ref[5],&ref[306])
	move_ret ref[307]
	c_ret

;; TERM 306: unfold(_B, _C, _D, _H, _G)
c_code local build_ref_306
	ret_reg &ref[306]
	call_c   build_ref_725()
	call_c   Dyam_Term_Start(&ref[725],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[306]
	c_ret

;; TERM 305: '*PROLOG-ITEM*'{top=> unfold(_B, _C, _D, _H, _G), cont=> '$CLOSURE'('$fun'(51, 0, 1177636080), '$TUPPLE'(35125252331480))}
c_code local build_ref_305
	ret_reg &ref[305]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,530579456)
	move_ret R(0)
	call_c   Dyam_Closure_Aux(fun51,R(0))
	move_ret R(0)
	call_c   build_ref_724()
	call_c   build_ref_306()
	call_c   Dyam_Create_Binary(&ref[724],&ref[306],R(0))
	move_ret ref[305]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 304: allocate :> _H :> deallocate :> succeed
c_code local build_ref_304
	ret_reg &ref[304]
	call_c   build_ref_299()
	call_c   build_ref_303()
	call_c   Dyam_Create_Binary(I(9),&ref[299],&ref[303])
	move_ret ref[304]
	c_ret

;; TERM 303: _H :> deallocate :> succeed
c_code local build_ref_303
	ret_reg &ref[303]
	call_c   build_ref_302()
	call_c   Dyam_Create_Binary(I(9),V(7),&ref[302])
	move_ret ref[303]
	c_ret

pl_code local fun51
	call_c   build_ref_304()
	call_c   Dyam_Allocate(0)
	move     &ref[304], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(4))
	pl_call  pred_label_code_2()
	call_c   Dyam_Reg_Load(0,V(1))
	move     V(8), R(2)
	move     S(5), R(3)
	pl_call  pred_tupple_2()
	call_c   Dyam_Reg_Load(0,V(3))
	call_c   Dyam_Reg_Load(2,V(8))
	move     V(9), R(4)
	move     S(5), R(5)
	pl_call  pred_inter_3()
	call_c   Dyam_Reg_Load(0,V(2))
	call_c   Dyam_Reg_Load(2,V(9))
	call_c   Dyam_Reg_Load(4,V(5))
	pl_call  pred_extend_tupple_3()
	call_c   Dyam_Deallocate()
	pl_jump  Follow_Cont(V(0))

long local pool_fun52[3]=[2,build_ref_298,build_seed_99]

pl_code local fun52
	call_c   Dyam_Pool(pool_fun52)
	call_c   Dyam_Unify_Item(&ref[298])
	fail_ret
	pl_jump  fun19(&seed[99],12)

;; TERM 297: '*PROLOG-FIRST*'(build_closure(_B, _C, _D, '$CLOSURE'(_E, _F), _G)) :> []
c_code local build_ref_297
	ret_reg &ref[297]
	call_c   build_ref_296()
	call_c   Dyam_Create_Binary(I(9),&ref[296],I(0))
	move_ret ref[297]
	c_ret

;; TERM 296: '*PROLOG-FIRST*'(build_closure(_B, _C, _D, '$CLOSURE'(_E, _F), _G))
c_code local build_ref_296
	ret_reg &ref[296]
	call_c   build_ref_5()
	call_c   build_ref_295()
	call_c   Dyam_Create_Unary(&ref[5],&ref[295])
	move_ret ref[296]
	c_ret

c_code local build_seed_65
	ret_reg &seed[65]
	call_c   build_ref_4()
	call_c   build_ref_311()
	call_c   Dyam_Seed_Start(&ref[4],&ref[311],I(0),fun1,1)
	call_c   build_ref_312()
	call_c   Dyam_Seed_Add_Comp(&ref[312],fun57,0)
	call_c   Dyam_Seed_End()
	move_ret seed[65]
	c_ret

;; TERM 312: '*PROLOG-ITEM*'{top=> unfold('*ALTERNATIVE*'(_B, _C), _D, _E, _F, _G), cont=> _A}
c_code local build_ref_312
	ret_reg &ref[312]
	call_c   build_ref_724()
	call_c   build_ref_309()
	call_c   Dyam_Create_Binary(&ref[724],&ref[309],V(0))
	move_ret ref[312]
	c_ret

;; TERM 309: unfold('*ALTERNATIVE*'(_B, _C), _D, _E, _F, _G)
c_code local build_ref_309
	ret_reg &ref[309]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_798()
	call_c   Dyam_Create_Binary(&ref[798],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_725()
	call_c   Dyam_Term_Start(&ref[725],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[309]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 798: '*ALTERNATIVE*'
c_code local build_ref_798
	ret_reg &ref[798]
	call_c   Dyam_Create_Atom("*ALTERNATIVE*")
	move_ret ref[798]
	c_ret

c_code local build_seed_101
	ret_reg &seed[101]
	call_c   build_ref_59()
	call_c   build_ref_316()
	call_c   Dyam_Seed_Start(&ref[59],&ref[316],I(0),fun15,1)
	call_c   build_ref_203()
	call_c   Dyam_Seed_Add_Comp(&ref[203],&ref[316],0)
	call_c   Dyam_Seed_End()
	move_ret seed[101]
	c_ret

;; TERM 316: '*PROLOG-ITEM*'{top=> unfold(_B, _D, _E, _H, _G), cont=> '$CLOSURE'('$fun'(56, 0, 1177656332), '$TUPPLE'(35125262287180))}
c_code local build_ref_316
	ret_reg &ref[316]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,534773760)
	move_ret R(0)
	call_c   Dyam_Closure_Aux(fun56,R(0))
	move_ret R(0)
	call_c   build_ref_724()
	call_c   build_ref_201()
	call_c   Dyam_Create_Binary(&ref[724],&ref[201],R(0))
	move_ret ref[316]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_100
	ret_reg &seed[100]
	call_c   build_ref_59()
	call_c   build_ref_315()
	call_c   Dyam_Seed_Start(&ref[59],&ref[315],I(0),fun15,1)
	call_c   build_ref_199()
	call_c   Dyam_Seed_Add_Comp(&ref[199],&ref[315],0)
	call_c   Dyam_Seed_End()
	move_ret seed[100]
	c_ret

;; TERM 315: '*PROLOG-ITEM*'{top=> unfold((_C :> deallocate :> succeed), _D, _E, _I, _J), cont=> '$CLOSURE'('$fun'(55, 0, 1177657016), '$TUPPLE'(35125253112884))}
c_code local build_ref_315
	ret_reg &ref[315]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,519569408)
	move_ret R(0)
	call_c   Dyam_Closure_Aux(fun55,R(0))
	move_ret R(0)
	call_c   build_ref_724()
	call_c   build_ref_197()
	call_c   Dyam_Create_Binary(&ref[724],&ref[197],R(0))
	move_ret ref[315]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 313: 'Unfolding alternatives do not return same env: ~w and ~w\n\tOut_Env1=~w\n\tOut_Env2=~w\n\tEnv=~w'
c_code local build_ref_313
	ret_reg &ref[313]
	call_c   Dyam_Create_Atom("Unfolding alternatives do not return same env: ~w and ~w\n\tOut_Env1=~w\n\tOut_Env2=~w\n\tEnv=~w")
	move_ret ref[313]
	c_ret

;; TERM 314: [_B,_C,_G,_J,_D]
c_code local build_ref_314
	ret_reg &ref[314]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(3),I(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(9),R(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(6),R(0))
	move_ret R(0)
	call_c   Dyam_Create_Tupple(1,2,R(0))
	move_ret ref[314]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun54[3]=[2,build_ref_313,build_ref_314]

pl_code local fun54
	call_c   Dyam_Remove_Choice()
	move     &ref[313], R(0)
	move     0, R(1)
	move     &ref[314], R(2)
	move     S(5), R(3)
	pl_call  pred_internal_error_2()
	pl_jump  fun46()

long local pool_fun55[2]=[65536,pool_fun54]

pl_code local fun55
	call_c   Dyam_Pool(pool_fun55)
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun54)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(6),V(9))
	fail_ret
	call_c   Dyam_Cut()
	pl_jump  fun46()

long local pool_fun56[2]=[1,build_seed_100]

pl_code local fun56
	call_c   Dyam_Pool(pool_fun56)
	pl_jump  fun19(&seed[100],12)

long local pool_fun57[3]=[2,build_ref_312,build_seed_101]

pl_code local fun57
	call_c   Dyam_Pool(pool_fun57)
	call_c   Dyam_Unify_Item(&ref[312])
	fail_ret
	pl_jump  fun19(&seed[101],12)

;; TERM 311: '*PROLOG-FIRST*'(unfold('*ALTERNATIVE*'(_B, _C), _D, _E, _F, _G)) :> []
c_code local build_ref_311
	ret_reg &ref[311]
	call_c   build_ref_310()
	call_c   Dyam_Create_Binary(I(9),&ref[310],I(0))
	move_ret ref[311]
	c_ret

;; TERM 310: '*PROLOG-FIRST*'(unfold('*ALTERNATIVE*'(_B, _C), _D, _E, _F, _G))
c_code local build_ref_310
	ret_reg &ref[310]
	call_c   build_ref_5()
	call_c   build_ref_309()
	call_c   Dyam_Create_Unary(&ref[5],&ref[309])
	move_ret ref[310]
	c_ret

c_code local build_seed_40
	ret_reg &seed[40]
	call_c   build_ref_4()
	call_c   build_ref_319()
	call_c   Dyam_Seed_Start(&ref[4],&ref[319],I(0),fun1,1)
	call_c   build_ref_320()
	call_c   Dyam_Seed_Add_Comp(&ref[320],fun108,0)
	call_c   Dyam_Seed_End()
	move_ret seed[40]
	c_ret

;; TERM 320: '*PROLOG-ITEM*'{top=> unfold('*PSEUDONEXT*'(_B, _C, _D, _E), _F, _G, _H, _I), cont=> _A}
c_code local build_ref_320
	ret_reg &ref[320]
	call_c   build_ref_724()
	call_c   build_ref_317()
	call_c   Dyam_Create_Binary(&ref[724],&ref[317],V(0))
	move_ret ref[320]
	c_ret

;; TERM 317: unfold('*PSEUDONEXT*'(_B, _C, _D, _E), _F, _G, _H, _I)
c_code local build_ref_317
	ret_reg &ref[317]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_799()
	call_c   Dyam_Term_Start(&ref[799],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_725()
	call_c   Dyam_Term_Start(&ref[725],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_End()
	move_ret ref[317]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 799: '*PSEUDONEXT*'
c_code local build_ref_799
	ret_reg &ref[799]
	call_c   Dyam_Create_Atom("*PSEUDONEXT*")
	move_ret ref[799]
	c_ret

c_code local build_seed_102
	ret_reg &seed[102]
	call_c   build_ref_59()
	call_c   build_ref_321()
	call_c   Dyam_Seed_Start(&ref[59],&ref[321],I(0),fun15,1)
	call_c   build_ref_324()
	call_c   Dyam_Seed_Add_Comp(&ref[324],&ref[321],0)
	call_c   Dyam_Seed_End()
	move_ret seed[102]
	c_ret

;; TERM 324: '*PROLOG-FIRST*'(seed_install(seed{model=> ('*RITEM*'(_B, _C) :> _D(_F)(_G)), id=> _K, anchor=> _L, subs_comp=> _M, code=> _N, go=> _O, tabule=> _P, schedule=> _Q, subsume=> _R, model_ref=> _S, subs_comp_ref=> _T, c_type=> _U, c_type_ref=> _V, out_env=> [_I], appinfo=> _W, tail_flag=> _X}, _J, _H)) :> '$$HOLE$$'
c_code local build_ref_324
	ret_reg &ref[324]
	call_c   build_ref_323()
	call_c   Dyam_Create_Binary(I(9),&ref[323],I(7))
	move_ret ref[324]
	c_ret

;; TERM 323: '*PROLOG-FIRST*'(seed_install(seed{model=> ('*RITEM*'(_B, _C) :> _D(_F)(_G)), id=> _K, anchor=> _L, subs_comp=> _M, code=> _N, go=> _O, tabule=> _P, schedule=> _Q, subsume=> _R, model_ref=> _S, subs_comp_ref=> _T, c_type=> _U, c_type_ref=> _V, out_env=> [_I], appinfo=> _W, tail_flag=> _X}, _J, _H))
c_code local build_ref_323
	ret_reg &ref[323]
	call_c   build_ref_5()
	call_c   build_ref_322()
	call_c   Dyam_Create_Unary(&ref[5],&ref[322])
	move_ret ref[323]
	c_ret

;; TERM 322: seed_install(seed{model=> ('*RITEM*'(_B, _C) :> _D(_F)(_G)), id=> _K, anchor=> _L, subs_comp=> _M, code=> _N, go=> _O, tabule=> _P, schedule=> _Q, subsume=> _R, model_ref=> _S, subs_comp_ref=> _T, c_type=> _U, c_type_ref=> _V, out_env=> [_I], appinfo=> _W, tail_flag=> _X}, _J, _H)
c_code local build_ref_322
	ret_reg &ref[322]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   Dyam_Create_Binary(I(3),V(3),V(5))
	move_ret R(0)
	call_c   Dyam_Create_Binary(I(3),R(0),V(6))
	move_ret R(0)
	call_c   build_ref_408()
	call_c   Dyam_Create_Binary(I(9),&ref[408],R(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(8),I(0))
	move_ret R(1)
	call_c   build_ref_748()
	call_c   Dyam_Term_Start(&ref[748],16)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_Arg(V(21))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(22))
	call_c   Dyam_Term_Arg(V(23))
	call_c   Dyam_Term_End()
	move_ret R(1)
	call_c   build_ref_747()
	call_c   Dyam_Term_Start(&ref[747],3)
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[322]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 408: '*RITEM*'(_B, _C)
c_code local build_ref_408
	ret_reg &ref[408]
	call_c   build_ref_0()
	call_c   Dyam_Create_Binary(&ref[0],V(1),V(2))
	move_ret ref[408]
	c_ret

;; TERM 321: '*PROLOG-ITEM*'{top=> seed_install(seed{model=> ('*RITEM*'(_B, _C) :> _D(_F)(_G)), id=> _K, anchor=> _L, subs_comp=> _M, code=> _N, go=> _O, tabule=> _P, schedule=> _Q, subsume=> _R, model_ref=> _S, subs_comp_ref=> _T, c_type=> _U, c_type_ref=> _V, out_env=> [_I], appinfo=> _W, tail_flag=> _X}, _J, _H), cont=> _A}
c_code local build_ref_321
	ret_reg &ref[321]
	call_c   build_ref_724()
	call_c   build_ref_322()
	call_c   Dyam_Create_Binary(&ref[724],&ref[322],V(0))
	move_ret ref[321]
	c_ret

long local pool_fun108[3]=[2,build_ref_320,build_seed_102]

pl_code local fun108
	call_c   Dyam_Pool(pool_fun108)
	call_c   Dyam_Unify_Item(&ref[320])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(4))
	move     N(2), R(2)
	move     0, R(3)
	move     V(9), R(4)
	move     S(5), R(5)
	pl_call  pred_check_tag_3()
	call_c   Dyam_Deallocate()
	pl_jump  fun19(&seed[102],12)

;; TERM 319: '*PROLOG-FIRST*'(unfold('*PSEUDONEXT*'(_B, _C, _D, _E), _F, _G, _H, _I)) :> []
c_code local build_ref_319
	ret_reg &ref[319]
	call_c   build_ref_318()
	call_c   Dyam_Create_Binary(I(9),&ref[318],I(0))
	move_ret ref[319]
	c_ret

;; TERM 318: '*PROLOG-FIRST*'(unfold('*PSEUDONEXT*'(_B, _C, _D, _E), _F, _G, _H, _I))
c_code local build_ref_318
	ret_reg &ref[318]
	call_c   build_ref_5()
	call_c   build_ref_317()
	call_c   Dyam_Create_Unary(&ref[5],&ref[317])
	move_ret ref[318]
	c_ret

c_code local build_seed_13
	ret_reg &seed[13]
	call_c   build_ref_325()
	call_c   build_ref_328()
	call_c   Dyam_Seed_Start(&ref[325],&ref[328],I(0),fun67,1)
	call_c   build_ref_330()
	call_c   Dyam_Seed_Add_Comp(&ref[330],fun66,0)
	call_c   Dyam_Seed_End()
	move_ret seed[13]
	c_ret

;; TERM 330: '*CITEM*'(special_app_model(application{item=> none, trans=> '*WAIT-CONT*'{rest=> _B, cont=> _C, tupple=> _D}, item_comp=> _E, code=> _F, item_id=> _G, trans_id=> 0, mode=> _H, restrict=> _I, out_env=> _J, key=> _K}), _A)
c_code local build_ref_330
	ret_reg &ref[330]
	call_c   build_ref_329()
	call_c   build_ref_326()
	call_c   Dyam_Create_Binary(&ref[329],&ref[326],V(0))
	move_ret ref[330]
	c_ret

;; TERM 326: special_app_model(application{item=> none, trans=> '*WAIT-CONT*'{rest=> _B, cont=> _C, tupple=> _D}, item_comp=> _E, code=> _F, item_id=> _G, trans_id=> 0, mode=> _H, restrict=> _I, out_env=> _J, key=> _K})
c_code local build_ref_326
	ret_reg &ref[326]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_780()
	call_c   Dyam_Term_Start(&ref[780],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_801()
	call_c   build_ref_802()
	call_c   Dyam_Term_Start(&ref[801],10)
	call_c   Dyam_Term_Arg(&ref[802])
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(N(0))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_800()
	call_c   Dyam_Create_Unary(&ref[800],R(0))
	move_ret ref[326]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 800: special_app_model
c_code local build_ref_800
	ret_reg &ref[800]
	call_c   Dyam_Create_Atom("special_app_model")
	move_ret ref[800]
	c_ret

;; TERM 802: none
c_code local build_ref_802
	ret_reg &ref[802]
	call_c   Dyam_Create_Atom("none")
	move_ret ref[802]
	c_ret

;; TERM 801: application!'$ft'
c_code local build_ref_801
	ret_reg &ref[801]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_792()
	call_c   Dyam_Create_List(&ref[792],I(0))
	move_ret R(0)
	call_c   build_ref_763()
	call_c   Dyam_Create_List(&ref[763],R(0))
	move_ret R(0)
	call_c   build_ref_808()
	call_c   Dyam_Create_List(&ref[808],R(0))
	move_ret R(0)
	call_c   build_ref_807()
	call_c   Dyam_Create_List(&ref[807],R(0))
	move_ret R(0)
	call_c   build_ref_806()
	call_c   Dyam_Create_List(&ref[806],R(0))
	move_ret R(0)
	call_c   build_ref_805()
	call_c   Dyam_Create_List(&ref[805],R(0))
	move_ret R(0)
	call_c   build_ref_754()
	call_c   Dyam_Create_List(&ref[754],R(0))
	move_ret R(0)
	call_c   build_ref_804()
	call_c   Dyam_Create_List(&ref[804],R(0))
	move_ret R(0)
	call_c   build_ref_331()
	call_c   Dyam_Create_List(&ref[331],R(0))
	move_ret R(0)
	call_c   build_ref_338()
	call_c   Dyam_Create_List(&ref[338],R(0))
	move_ret R(0)
	call_c   build_ref_803()
	call_c   Dyam_Create_Atom_Module("$ft",&ref[803])
	move_ret ref[801]
	call_c   DYAM_Feature_2(&ref[803],R(0))
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 803: application
c_code local build_ref_803
	ret_reg &ref[803]
	call_c   Dyam_Create_Atom("application")
	move_ret ref[803]
	c_ret

;; TERM 338: item
c_code local build_ref_338
	ret_reg &ref[338]
	call_c   Dyam_Create_Atom("item")
	move_ret ref[338]
	c_ret

;; TERM 331: trans
c_code local build_ref_331
	ret_reg &ref[331]
	call_c   Dyam_Create_Atom("trans")
	move_ret ref[331]
	c_ret

;; TERM 804: item_comp
c_code local build_ref_804
	ret_reg &ref[804]
	call_c   Dyam_Create_Atom("item_comp")
	move_ret ref[804]
	c_ret

;; TERM 805: item_id
c_code local build_ref_805
	ret_reg &ref[805]
	call_c   Dyam_Create_Atom("item_id")
	move_ret ref[805]
	c_ret

;; TERM 806: trans_id
c_code local build_ref_806
	ret_reg &ref[806]
	call_c   Dyam_Create_Atom("trans_id")
	move_ret ref[806]
	c_ret

;; TERM 807: mode
c_code local build_ref_807
	ret_reg &ref[807]
	call_c   Dyam_Create_Atom("mode")
	move_ret ref[807]
	c_ret

;; TERM 808: restrict
c_code local build_ref_808
	ret_reg &ref[808]
	call_c   Dyam_Create_Atom("restrict")
	move_ret ref[808]
	c_ret

c_code local build_seed_103
	ret_reg &seed[103]
	call_c   build_ref_0()
	call_c   build_ref_332()
	call_c   Dyam_Seed_Start(&ref[0],&ref[332],&ref[332],fun15,1)
	call_c   build_ref_333()
	call_c   Dyam_Seed_Add_Comp(&ref[333],&ref[332],0)
	call_c   Dyam_Seed_End()
	move_ret seed[103]
	c_ret

;; TERM 333: '*RITEM*'(_A, voidret) :> '$$HOLE$$'
c_code local build_ref_333
	ret_reg &ref[333]
	call_c   build_ref_332()
	call_c   Dyam_Create_Binary(I(9),&ref[332],I(7))
	move_ret ref[333]
	c_ret

;; TERM 332: '*RITEM*'(_A, voidret)
c_code local build_ref_332
	ret_reg &ref[332]
	call_c   build_ref_0()
	call_c   build_ref_2()
	call_c   Dyam_Create_Binary(&ref[0],V(0),&ref[2])
	move_ret ref[332]
	c_ret

;; TERM 2: voidret
c_code local build_ref_2
	ret_reg &ref[2]
	call_c   Dyam_Create_Atom("voidret")
	move_ret ref[2]
	c_ret

pl_code local fun60
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Object(R(0))
	call_c   Dyam_Tabule()
	call_c   Dyam_Schedule()
	pl_ret

pl_code local fun61
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Choice(fun60)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Subsume(R(0))
	fail_ret
	call_c   Dyam_Cut()
	pl_ret

long local pool_fun62[2]=[1,build_seed_103]

pl_code local fun64
	call_c   Dyam_Remove_Choice()
fun62:
	call_c   Dyam_Deallocate()
	pl_jump  fun61(&seed[103],2)


long local pool_fun65[3]=[65537,build_ref_338,pool_fun62]

pl_code local fun65
	call_c   Dyam_Update_Choice(fun64)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(7),&ref[338])
	fail_ret
	call_c   Dyam_Cut()
	pl_fail

c_code local build_seed_104
	ret_reg &seed[104]
	call_c   build_ref_59()
	call_c   build_ref_334()
	call_c   Dyam_Seed_Start(&ref[59],&ref[334],I(0),fun15,1)
	call_c   build_ref_337()
	call_c   Dyam_Seed_Add_Comp(&ref[337],&ref[334],0)
	call_c   Dyam_Seed_End()
	move_ret seed[104]
	c_ret

;; TERM 337: '*PROLOG-FIRST*'(unfold(_B, _C, _D, _F, _J)) :> '$$HOLE$$'
c_code local build_ref_337
	ret_reg &ref[337]
	call_c   build_ref_336()
	call_c   Dyam_Create_Binary(I(9),&ref[336],I(7))
	move_ret ref[337]
	c_ret

;; TERM 336: '*PROLOG-FIRST*'(unfold(_B, _C, _D, _F, _J))
c_code local build_ref_336
	ret_reg &ref[336]
	call_c   build_ref_5()
	call_c   build_ref_335()
	call_c   Dyam_Create_Unary(&ref[5],&ref[335])
	move_ret ref[336]
	c_ret

;; TERM 335: unfold(_B, _C, _D, _F, _J)
c_code local build_ref_335
	ret_reg &ref[335]
	call_c   build_ref_725()
	call_c   Dyam_Term_Start(&ref[725],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret ref[335]
	c_ret

;; TERM 334: '*PROLOG-ITEM*'{top=> unfold(_B, _C, _D, _F, _J), cont=> '$CLOSURE'('$fun'(63, 0, 1177713252), '$TUPPLE'(35125252598484))}
c_code local build_ref_334
	ret_reg &ref[334]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,268435456)
	move_ret R(0)
	call_c   Dyam_Closure_Aux(fun63,R(0))
	move_ret R(0)
	call_c   build_ref_724()
	call_c   build_ref_335()
	call_c   Dyam_Create_Binary(&ref[724],&ref[335],R(0))
	move_ret ref[334]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

pl_code local fun63
	call_c   Dyam_Pool(pool_fun62)
	call_c   Dyam_Allocate(0)
	pl_jump  fun62()

long local pool_fun66[5]=[65539,build_ref_330,build_ref_331,build_seed_104,pool_fun65]

pl_code local fun66
	call_c   Dyam_Pool(pool_fun66)
	call_c   Dyam_Unify_Item(&ref[330])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun65)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(7),&ref[331])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Deallocate()
	pl_jump  fun19(&seed[104],12)

pl_code local fun67
	pl_jump  Apply(0,0)

;; TERM 328: '*FIRST*'(special_app_model(application{item=> none, trans=> '*WAIT-CONT*'{rest=> _B, cont=> _C, tupple=> _D}, item_comp=> _E, code=> _F, item_id=> _G, trans_id=> 0, mode=> _H, restrict=> _I, out_env=> _J, key=> _K})) :> []
c_code local build_ref_328
	ret_reg &ref[328]
	call_c   build_ref_327()
	call_c   Dyam_Create_Binary(I(9),&ref[327],I(0))
	move_ret ref[328]
	c_ret

;; TERM 327: '*FIRST*'(special_app_model(application{item=> none, trans=> '*WAIT-CONT*'{rest=> _B, cont=> _C, tupple=> _D}, item_comp=> _E, code=> _F, item_id=> _G, trans_id=> 0, mode=> _H, restrict=> _I, out_env=> _J, key=> _K}))
c_code local build_ref_327
	ret_reg &ref[327]
	call_c   build_ref_325()
	call_c   build_ref_326()
	call_c   Dyam_Create_Unary(&ref[325],&ref[326])
	move_ret ref[327]
	c_ret

;; TERM 325: '*FIRST*'
c_code local build_ref_325
	ret_reg &ref[325]
	call_c   Dyam_Create_Atom("*FIRST*")
	move_ret ref[325]
	c_ret

c_code local build_seed_45
	ret_reg &seed[45]
	call_c   build_ref_4()
	call_c   build_ref_341()
	call_c   Dyam_Seed_Start(&ref[4],&ref[341],I(0),fun1,1)
	call_c   build_ref_342()
	call_c   Dyam_Seed_Add_Comp(&ref[342],fun115,0)
	call_c   Dyam_Seed_End()
	move_ret seed[45]
	c_ret

;; TERM 342: '*PROLOG-ITEM*'{top=> unfold('*PSEUDOLIGHTNEXT*'(_B, _C, _D, _E), _F, _G, _H, _I), cont=> _A}
c_code local build_ref_342
	ret_reg &ref[342]
	call_c   build_ref_724()
	call_c   build_ref_339()
	call_c   Dyam_Create_Binary(&ref[724],&ref[339],V(0))
	move_ret ref[342]
	c_ret

;; TERM 339: unfold('*PSEUDOLIGHTNEXT*'(_B, _C, _D, _E), _F, _G, _H, _I)
c_code local build_ref_339
	ret_reg &ref[339]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_809()
	call_c   Dyam_Term_Start(&ref[809],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_725()
	call_c   Dyam_Term_Start(&ref[725],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_End()
	move_ret ref[339]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 809: '*PSEUDOLIGHTNEXT*'
c_code local build_ref_809
	ret_reg &ref[809]
	call_c   Dyam_Create_Atom("*PSEUDOLIGHTNEXT*")
	move_ret ref[809]
	c_ret

c_code local build_seed_105
	ret_reg &seed[105]
	call_c   build_ref_59()
	call_c   build_ref_343()
	call_c   Dyam_Seed_Start(&ref[59],&ref[343],I(0),fun15,1)
	call_c   build_ref_346()
	call_c   Dyam_Seed_Add_Comp(&ref[346],&ref[343],0)
	call_c   Dyam_Seed_End()
	move_ret seed[105]
	c_ret

;; TERM 346: '*PROLOG-FIRST*'(seed_install(seed{model=> ('*RITEM*'(_B, _C) :> _D(_F)(_G)), id=> _K, anchor=> _L, subs_comp=> _M, code=> _N, go=> _O, tabule=> light, schedule=> prolog, subsume=> _P, model_ref=> _Q, subs_comp_ref=> _R, c_type=> _S, c_type_ref=> _T, out_env=> [_I], appinfo=> _U, tail_flag=> _V}, _J, _W)) :> '$$HOLE$$'
c_code local build_ref_346
	ret_reg &ref[346]
	call_c   build_ref_345()
	call_c   Dyam_Create_Binary(I(9),&ref[345],I(7))
	move_ret ref[346]
	c_ret

;; TERM 345: '*PROLOG-FIRST*'(seed_install(seed{model=> ('*RITEM*'(_B, _C) :> _D(_F)(_G)), id=> _K, anchor=> _L, subs_comp=> _M, code=> _N, go=> _O, tabule=> light, schedule=> prolog, subsume=> _P, model_ref=> _Q, subs_comp_ref=> _R, c_type=> _S, c_type_ref=> _T, out_env=> [_I], appinfo=> _U, tail_flag=> _V}, _J, _W))
c_code local build_ref_345
	ret_reg &ref[345]
	call_c   build_ref_5()
	call_c   build_ref_344()
	call_c   Dyam_Create_Unary(&ref[5],&ref[344])
	move_ret ref[345]
	c_ret

;; TERM 344: seed_install(seed{model=> ('*RITEM*'(_B, _C) :> _D(_F)(_G)), id=> _K, anchor=> _L, subs_comp=> _M, code=> _N, go=> _O, tabule=> light, schedule=> prolog, subsume=> _P, model_ref=> _Q, subs_comp_ref=> _R, c_type=> _S, c_type_ref=> _T, out_env=> [_I], appinfo=> _U, tail_flag=> _V}, _J, _W)
c_code local build_ref_344
	ret_reg &ref[344]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   Dyam_Create_Binary(I(3),V(3),V(5))
	move_ret R(0)
	call_c   Dyam_Create_Binary(I(3),R(0),V(6))
	move_ret R(0)
	call_c   build_ref_408()
	call_c   Dyam_Create_Binary(I(9),&ref[408],R(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(8),I(0))
	move_ret R(1)
	call_c   build_ref_748()
	call_c   build_ref_795()
	call_c   build_ref_770()
	call_c   Dyam_Term_Start(&ref[748],16)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(&ref[795])
	call_c   Dyam_Term_Arg(&ref[770])
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_Arg(V(21))
	call_c   Dyam_Term_End()
	move_ret R(1)
	call_c   build_ref_747()
	call_c   Dyam_Term_Start(&ref[747],3)
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(22))
	call_c   Dyam_Term_End()
	move_ret ref[344]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 343: '*PROLOG-ITEM*'{top=> seed_install(seed{model=> ('*RITEM*'(_B, _C) :> _D(_F)(_G)), id=> _K, anchor=> _L, subs_comp=> _M, code=> _N, go=> _O, tabule=> light, schedule=> prolog, subsume=> _P, model_ref=> _Q, subs_comp_ref=> _R, c_type=> _S, c_type_ref=> _T, out_env=> [_I], appinfo=> _U, tail_flag=> _V}, _J, _W), cont=> '$CLOSURE'('$fun'(73, 0, 1177825000), '$TUPPLE'(35125252598628))}
c_code local build_ref_343
	ret_reg &ref[343]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,270532672)
	move_ret R(0)
	call_c   Dyam_Closure_Aux(fun73,R(0))
	move_ret R(0)
	call_c   build_ref_724()
	call_c   build_ref_344()
	call_c   Dyam_Create_Binary(&ref[724],&ref[344],R(0))
	move_ret ref[343]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

pl_code local fun73
	call_c   Dyam_Unify(V(7),V(22))
	fail_ret
	pl_jump  Follow_Cont(V(0))

long local pool_fun115[3]=[2,build_ref_342,build_seed_105]

pl_code local fun115
	call_c   Dyam_Pool(pool_fun115)
	call_c   Dyam_Unify_Item(&ref[342])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(4))
	move     N(2), R(2)
	move     0, R(3)
	move     V(9), R(4)
	move     S(5), R(5)
	pl_call  pred_check_tag_3()
	call_c   Dyam_Deallocate()
	pl_jump  fun19(&seed[105],12)

;; TERM 341: '*PROLOG-FIRST*'(unfold('*PSEUDOLIGHTNEXT*'(_B, _C, _D, _E), _F, _G, _H, _I)) :> []
c_code local build_ref_341
	ret_reg &ref[341]
	call_c   build_ref_340()
	call_c   Dyam_Create_Binary(I(9),&ref[340],I(0))
	move_ret ref[341]
	c_ret

;; TERM 340: '*PROLOG-FIRST*'(unfold('*PSEUDOLIGHTNEXT*'(_B, _C, _D, _E), _F, _G, _H, _I))
c_code local build_ref_340
	ret_reg &ref[340]
	call_c   build_ref_5()
	call_c   build_ref_339()
	call_c   Dyam_Create_Unary(&ref[5],&ref[339])
	move_ret ref[340]
	c_ret

c_code local build_seed_69
	ret_reg &seed[69]
	call_c   build_ref_4()
	call_c   build_ref_349()
	call_c   Dyam_Seed_Start(&ref[4],&ref[349],I(0),fun1,1)
	call_c   build_ref_350()
	call_c   Dyam_Seed_Add_Comp(&ref[350],fun84,0)
	call_c   Dyam_Seed_End()
	move_ret seed[69]
	c_ret

;; TERM 350: '*PROLOG-ITEM*'{top=> init_install(_B), cont=> _A}
c_code local build_ref_350
	ret_reg &ref[350]
	call_c   build_ref_724()
	call_c   build_ref_347()
	call_c   Dyam_Create_Binary(&ref[724],&ref[347],V(0))
	move_ret ref[350]
	c_ret

;; TERM 347: init_install(_B)
c_code local build_ref_347
	ret_reg &ref[347]
	call_c   build_ref_810()
	call_c   Dyam_Create_Unary(&ref[810],V(1))
	move_ret ref[347]
	c_ret

;; TERM 810: init_install
c_code local build_ref_810
	ret_reg &ref[810]
	call_c   Dyam_Create_Atom("init_install")
	move_ret ref[810]
	c_ret

c_code local build_seed_107
	ret_reg &seed[107]
	call_c   build_ref_59()
	call_c   build_ref_361()
	call_c   Dyam_Seed_Start(&ref[59],&ref[361],I(0),fun15,1)
	call_c   build_ref_364()
	call_c   Dyam_Seed_Add_Comp(&ref[364],&ref[361],0)
	call_c   Dyam_Seed_End()
	move_ret seed[107]
	c_ret

;; TERM 364: '*PROLOG-FIRST*'(unfold(_D, [], _C, _H, _L)) :> '$$HOLE$$'
c_code local build_ref_364
	ret_reg &ref[364]
	call_c   build_ref_363()
	call_c   Dyam_Create_Binary(I(9),&ref[363],I(7))
	move_ret ref[364]
	c_ret

;; TERM 363: '*PROLOG-FIRST*'(unfold(_D, [], _C, _H, _L))
c_code local build_ref_363
	ret_reg &ref[363]
	call_c   build_ref_5()
	call_c   build_ref_362()
	call_c   Dyam_Create_Unary(&ref[5],&ref[362])
	move_ret ref[363]
	c_ret

;; TERM 362: unfold(_D, [], _C, _H, _L)
c_code local build_ref_362
	ret_reg &ref[362]
	call_c   build_ref_725()
	call_c   Dyam_Term_Start(&ref[725],5)
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_End()
	move_ret ref[362]
	c_ret

;; TERM 361: '*PROLOG-ITEM*'{top=> unfold(_D, [], _C, _H, _L), cont=> '$CLOSURE'('$fun'(78, 0, 1177864984), '$TUPPLE'(35125252598756))}
c_code local build_ref_361
	ret_reg &ref[361]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,270532608)
	move_ret R(0)
	call_c   Dyam_Closure_Aux(fun78,R(0))
	move_ret R(0)
	call_c   build_ref_724()
	call_c   build_ref_362()
	call_c   Dyam_Create_Binary(&ref[724],&ref[362],R(0))
	move_ret ref[361]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 355: load_init(_H)
c_code local build_ref_355
	ret_reg &ref[355]
	call_c   build_ref_811()
	call_c   Dyam_Create_Unary(&ref[811],V(7))
	move_ret ref[355]
	c_ret

;; TERM 811: load_init
c_code local build_ref_811
	ret_reg &ref[811]
	call_c   Dyam_Create_Atom("load_init")
	move_ret ref[811]
	c_ret

pl_code local fun78
	call_c   build_ref_355()
	call_c   Dyam_Allocate(0)
fun76:
	call_c   DYAM_evpred_assert_1(&ref[355])
	call_c   Dyam_Deallocate()
	pl_jump  Follow_Cont(V(0))


long local pool_fun79[2]=[1,build_seed_107]

pl_code local fun82
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Reg_Load(0,V(1))
	move     V(3), R(2)
	move     S(5), R(3)
	pl_call  pred_trans_unfold_2()
fun79:
	call_c   Dyam_Deallocate()
	pl_jump  fun19(&seed[107],12)


;; TERM 365: '*OBJECT*'(_J, _K)
c_code local build_ref_365
	ret_reg &ref[365]
	call_c   build_ref_746()
	call_c   Dyam_Create_Binary(&ref[746],V(9),V(10))
	move_ret ref[365]
	c_ret

long local pool_fun81[3]=[65537,build_ref_365,pool_fun79]

pl_code local fun81
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(1),&ref[365])
	fail_ret
fun80:
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(3),V(1))
	fail_ret
	pl_jump  fun79()


;; TERM 360: '*OBJECT*'(_I)
c_code local build_ref_360
	ret_reg &ref[360]
	call_c   build_ref_746()
	call_c   Dyam_Create_Unary(&ref[746],V(8))
	move_ret ref[360]
	c_ret

long local pool_fun83[5]=[196609,build_ref_360,pool_fun79,pool_fun81,pool_fun79]

pl_code local fun83
	call_c   Dyam_Update_Choice(fun82)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Choice(fun81)
	call_c   Dyam_Unify(V(1),&ref[360])
	fail_ret
	pl_jump  fun80()

;; TERM 351: '$loader'(_D)
c_code local build_ref_351
	ret_reg &ref[351]
	call_c   build_ref_812()
	call_c   Dyam_Create_Unary(&ref[812],V(3))
	move_ret ref[351]
	c_ret

;; TERM 812: '$loader'
c_code local build_ref_812
	ret_reg &ref[812]
	call_c   Dyam_Create_Atom("$loader")
	move_ret ref[812]
	c_ret

c_code local build_seed_106
	ret_reg &seed[106]
	call_c   build_ref_59()
	call_c   build_ref_356()
	call_c   Dyam_Seed_Start(&ref[59],&ref[356],I(0),fun15,1)
	call_c   build_ref_359()
	call_c   Dyam_Seed_Add_Comp(&ref[359],&ref[356],0)
	call_c   Dyam_Seed_End()
	move_ret seed[106]
	c_ret

;; TERM 359: '*PROLOG-FIRST*'(unfold(_D, [], _C, _E, _F)) :> '$$HOLE$$'
c_code local build_ref_359
	ret_reg &ref[359]
	call_c   build_ref_358()
	call_c   Dyam_Create_Binary(I(9),&ref[358],I(7))
	move_ret ref[359]
	c_ret

;; TERM 358: '*PROLOG-FIRST*'(unfold(_D, [], _C, _E, _F))
c_code local build_ref_358
	ret_reg &ref[358]
	call_c   build_ref_5()
	call_c   build_ref_357()
	call_c   Dyam_Create_Unary(&ref[5],&ref[357])
	move_ret ref[358]
	c_ret

;; TERM 357: unfold(_D, [], _C, _E, _F)
c_code local build_ref_357
	ret_reg &ref[357]
	call_c   build_ref_725()
	call_c   Dyam_Term_Start(&ref[725],5)
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[357]
	c_ret

;; TERM 356: '*PROLOG-ITEM*'{top=> unfold(_D, [], _C, _E, _F), cont=> '$CLOSURE'('$fun'(77, 0, 1177847856), '$TUPPLE'(35125262549028))}
c_code local build_ref_356
	ret_reg &ref[356]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,285212672)
	move_ret R(0)
	call_c   Dyam_Closure_Aux(fun77,R(0))
	move_ret R(0)
	call_c   build_ref_724()
	call_c   build_ref_357()
	call_c   Dyam_Create_Binary(&ref[724],&ref[357],R(0))
	move_ret ref[356]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 353: allocate :> _E :> deallocate :> succeed
c_code local build_ref_353
	ret_reg &ref[353]
	call_c   build_ref_299()
	call_c   build_ref_352()
	call_c   Dyam_Create_Binary(I(9),&ref[299],&ref[352])
	move_ret ref[353]
	c_ret

;; TERM 352: _E :> deallocate :> succeed
c_code local build_ref_352
	ret_reg &ref[352]
	call_c   build_ref_302()
	call_c   Dyam_Create_Binary(I(9),V(4),&ref[302])
	move_ret ref[352]
	c_ret

;; TERM 354: call(_G, [])
c_code local build_ref_354
	ret_reg &ref[354]
	call_c   build_ref_735()
	call_c   Dyam_Create_Binary(&ref[735],V(6),I(0))
	move_ret ref[354]
	c_ret

long local pool_fun77[4]=[3,build_ref_353,build_ref_354,build_ref_355]

pl_code local fun77
	call_c   Dyam_Pool(pool_fun77)
	call_c   Dyam_Allocate(0)
	move     &ref[353], R(0)
	move     S(5), R(1)
	move     V(6), R(2)
	move     S(5), R(3)
	pl_call  pred_label_code_2()
	call_c   Dyam_Unify(V(7),&ref[354])
	fail_ret
	pl_jump  fun76()

long local pool_fun84[5]=[65539,build_ref_350,build_ref_351,build_seed_106,pool_fun83]

pl_code local fun84
	call_c   Dyam_Pool(pool_fun84)
	call_c   Dyam_Unify_Item(&ref[350])
	fail_ret
	call_c   Dyam_Allocate(0)
	move     V(2), R(0)
	move     S(5), R(1)
	pl_call  pred_null_tupple_1()
	call_c   Dyam_Choice(fun83)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[351])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Deallocate()
	pl_jump  fun19(&seed[106],12)

;; TERM 349: '*PROLOG-FIRST*'(init_install(_B)) :> []
c_code local build_ref_349
	ret_reg &ref[349]
	call_c   build_ref_348()
	call_c   Dyam_Create_Binary(I(9),&ref[348],I(0))
	move_ret ref[349]
	c_ret

;; TERM 348: '*PROLOG-FIRST*'(init_install(_B))
c_code local build_ref_348
	ret_reg &ref[348]
	call_c   build_ref_5()
	call_c   build_ref_347()
	call_c   Dyam_Create_Unary(&ref[5],&ref[347])
	move_ret ref[348]
	c_ret

c_code local build_seed_68
	ret_reg &seed[68]
	call_c   build_ref_4()
	call_c   build_ref_374()
	call_c   Dyam_Seed_Start(&ref[4],&ref[374],I(0),fun1,1)
	call_c   build_ref_375()
	call_c   Dyam_Seed_Add_Comp(&ref[375],fun91,0)
	call_c   Dyam_Seed_End()
	move_ret seed[68]
	c_ret

;; TERM 375: '*PROLOG-ITEM*'{top=> unfold('*OBJECT*'(_B), _C, _D, _E, _C), cont=> _A}
c_code local build_ref_375
	ret_reg &ref[375]
	call_c   build_ref_724()
	call_c   build_ref_372()
	call_c   Dyam_Create_Binary(&ref[724],&ref[372],V(0))
	move_ret ref[375]
	c_ret

;; TERM 372: unfold('*OBJECT*'(_B), _C, _D, _E, _C)
c_code local build_ref_372
	ret_reg &ref[372]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_746()
	call_c   Dyam_Create_Unary(&ref[746],V(1))
	move_ret R(0)
	call_c   build_ref_725()
	call_c   Dyam_Term_Start(&ref[725],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_End()
	move_ret ref[372]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 381: seed{model=> _B, id=> _W, anchor=> _X, subs_comp=> _Y, code=> _Z, go=> _A1, tabule=> _B1, schedule=> _C1, subsume=> _D1, model_ref=> _E1, subs_comp_ref=> _F1, c_type=> _G1, c_type_ref=> _H1, out_env=> _I1, appinfo=> _J1, tail_flag=> _K1}
c_code local build_ref_381
	ret_reg &ref[381]
	call_c   build_ref_748()
	call_c   Dyam_Term_Start(&ref[748],16)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(22))
	call_c   Dyam_Term_Arg(V(23))
	call_c   Dyam_Term_Arg(V(24))
	call_c   Dyam_Term_Arg(V(25))
	call_c   Dyam_Term_Arg(V(26))
	call_c   Dyam_Term_Arg(V(27))
	call_c   Dyam_Term_Arg(V(28))
	call_c   Dyam_Term_Arg(V(29))
	call_c   Dyam_Term_Arg(V(30))
	call_c   Dyam_Term_Arg(V(31))
	call_c   Dyam_Term_Arg(V(32))
	call_c   Dyam_Term_Arg(V(33))
	call_c   Dyam_Term_Arg(V(34))
	call_c   Dyam_Term_Arg(V(35))
	call_c   Dyam_Term_Arg(V(36))
	call_c   Dyam_Term_End()
	move_ret ref[381]
	c_ret

c_code local build_seed_108
	ret_reg &seed[108]
	call_c   build_ref_59()
	call_c   build_ref_377()
	call_c   Dyam_Seed_Start(&ref[59],&ref[377],I(0),fun15,1)
	call_c   build_ref_380()
	call_c   Dyam_Seed_Add_Comp(&ref[380],&ref[377],0)
	call_c   Dyam_Seed_End()
	move_ret seed[108]
	c_ret

;; TERM 380: '*PROLOG-FIRST*'(seed_install(_V, 0, _E)) :> '$$HOLE$$'
c_code local build_ref_380
	ret_reg &ref[380]
	call_c   build_ref_379()
	call_c   Dyam_Create_Binary(I(9),&ref[379],I(7))
	move_ret ref[380]
	c_ret

;; TERM 379: '*PROLOG-FIRST*'(seed_install(_V, 0, _E))
c_code local build_ref_379
	ret_reg &ref[379]
	call_c   build_ref_5()
	call_c   build_ref_378()
	call_c   Dyam_Create_Unary(&ref[5],&ref[378])
	move_ret ref[379]
	c_ret

;; TERM 378: seed_install(_V, 0, _E)
c_code local build_ref_378
	ret_reg &ref[378]
	call_c   build_ref_747()
	call_c   Dyam_Term_Start(&ref[747],3)
	call_c   Dyam_Term_Arg(V(21))
	call_c   Dyam_Term_Arg(N(0))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret ref[378]
	c_ret

;; TERM 377: '*PROLOG-ITEM*'{top=> seed_install(_V, 0, _E), cont=> _A}
c_code local build_ref_377
	ret_reg &ref[377]
	call_c   build_ref_724()
	call_c   build_ref_378()
	call_c   Dyam_Create_Binary(&ref[724],&ref[378],V(0))
	move_ret ref[377]
	c_ret

long local pool_fun89[2]=[1,build_seed_108]

long local pool_fun90[3]=[65537,build_ref_381,pool_fun89]

pl_code local fun90
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(21),&ref[381])
	fail_ret
fun89:
	call_c   Dyam_Deallocate()
	pl_jump  fun19(&seed[108],12)


;; TERM 376: seed{model=> _F, id=> _G, anchor=> _H, subs_comp=> _I, code=> _J, go=> _K, tabule=> _L, schedule=> _M, subsume=> _N, model_ref=> _O, subs_comp_ref=> _P, c_type=> _Q, c_type_ref=> _R, out_env=> _S, appinfo=> _T, tail_flag=> _U}
c_code local build_ref_376
	ret_reg &ref[376]
	call_c   build_ref_748()
	call_c   Dyam_Term_Start(&ref[748],16)
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_End()
	move_ret ref[376]
	c_ret

long local pool_fun91[5]=[131074,build_ref_375,build_ref_376,pool_fun90,pool_fun89]

pl_code local fun91
	call_c   Dyam_Pool(pool_fun91)
	call_c   Dyam_Unify_Item(&ref[375])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun90)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[376])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(21),V(1))
	fail_ret
	pl_jump  fun89()

;; TERM 374: '*PROLOG-FIRST*'(unfold('*OBJECT*'(_B), _C, _D, _E, _C)) :> []
c_code local build_ref_374
	ret_reg &ref[374]
	call_c   build_ref_373()
	call_c   Dyam_Create_Binary(I(9),&ref[373],I(0))
	move_ret ref[374]
	c_ret

;; TERM 373: '*PROLOG-FIRST*'(unfold('*OBJECT*'(_B), _C, _D, _E, _C))
c_code local build_ref_373
	ret_reg &ref[373]
	call_c   build_ref_5()
	call_c   build_ref_372()
	call_c   Dyam_Create_Unary(&ref[5],&ref[372])
	move_ret ref[373]
	c_ret

c_code local build_seed_12
	ret_reg &seed[12]
	call_c   build_ref_325()
	call_c   build_ref_384()
	call_c   Dyam_Seed_Start(&ref[325],&ref[384],I(0),fun67,1)
	call_c   build_ref_385()
	call_c   Dyam_Seed_Add_Comp(&ref[385],fun92,0)
	call_c   Dyam_Seed_End()
	move_ret seed[12]
	c_ret

;; TERM 385: '*CITEM*'(special_app_model(application{item=> none, trans=> ('*CONT*' :> _B(_C)(_D)), item_comp=> _E, code=> (allocate :> _F), item_id=> _G, trans_id=> 0, mode=> _H, restrict=> _I, out_env=> _J, key=> _K}), _A)
c_code local build_ref_385
	ret_reg &ref[385]
	call_c   build_ref_329()
	call_c   build_ref_382()
	call_c   Dyam_Create_Binary(&ref[329],&ref[382],V(0))
	move_ret ref[385]
	c_ret

;; TERM 382: special_app_model(application{item=> none, trans=> ('*CONT*' :> _B(_C)(_D)), item_comp=> _E, code=> (allocate :> _F), item_id=> _G, trans_id=> 0, mode=> _H, restrict=> _I, out_env=> _J, key=> _K})
c_code local build_ref_382
	ret_reg &ref[382]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   Dyam_Create_Binary(I(3),V(1),V(2))
	move_ret R(0)
	call_c   Dyam_Create_Binary(I(3),R(0),V(3))
	move_ret R(0)
	call_c   build_ref_787()
	call_c   Dyam_Create_Binary(I(9),&ref[787],R(0))
	move_ret R(0)
	call_c   build_ref_299()
	call_c   Dyam_Create_Binary(I(9),&ref[299],V(5))
	move_ret R(1)
	call_c   build_ref_801()
	call_c   build_ref_802()
	call_c   Dyam_Term_Start(&ref[801],10)
	call_c   Dyam_Term_Arg(&ref[802])
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(N(0))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_End()
	move_ret R(1)
	call_c   build_ref_800()
	call_c   Dyam_Create_Unary(&ref[800],R(1))
	move_ret ref[382]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_109
	ret_reg &seed[109]
	call_c   build_ref_59()
	call_c   build_ref_386()
	call_c   Dyam_Seed_Start(&ref[59],&ref[386],I(0),fun15,1)
	call_c   build_ref_389()
	call_c   Dyam_Seed_Add_Comp(&ref[389],&ref[386],0)
	call_c   Dyam_Seed_End()
	move_ret seed[109]
	c_ret

;; TERM 389: '*PROLOG-FIRST*'(unfold((_B :> deallocate :> succeed), _C, _D, _F, _J)) :> '$$HOLE$$'
c_code local build_ref_389
	ret_reg &ref[389]
	call_c   build_ref_388()
	call_c   Dyam_Create_Binary(I(9),&ref[388],I(7))
	move_ret ref[389]
	c_ret

;; TERM 388: '*PROLOG-FIRST*'(unfold((_B :> deallocate :> succeed), _C, _D, _F, _J))
c_code local build_ref_388
	ret_reg &ref[388]
	call_c   build_ref_5()
	call_c   build_ref_387()
	call_c   Dyam_Create_Unary(&ref[5],&ref[387])
	move_ret ref[388]
	c_ret

;; TERM 387: unfold((_B :> deallocate :> succeed), _C, _D, _F, _J)
c_code local build_ref_387
	ret_reg &ref[387]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_302()
	call_c   Dyam_Create_Binary(I(9),V(1),&ref[302])
	move_ret R(0)
	call_c   build_ref_725()
	call_c   Dyam_Term_Start(&ref[725],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret ref[387]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 386: '*PROLOG-ITEM*'{top=> unfold((_B :> deallocate :> succeed), _C, _D, _F, _J), cont=> '$CLOSURE'('$fun'(63, 0, 1177713252), '$TUPPLE'(35125252598484))}
c_code local build_ref_386
	ret_reg &ref[386]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,268435456)
	move_ret R(0)
	call_c   Dyam_Closure_Aux(fun63,R(0))
	move_ret R(0)
	call_c   build_ref_724()
	call_c   build_ref_387()
	call_c   Dyam_Create_Binary(&ref[724],&ref[387],R(0))
	move_ret ref[386]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun92[5]=[65539,build_ref_385,build_ref_331,build_seed_109,pool_fun65]

pl_code local fun92
	call_c   Dyam_Pool(pool_fun92)
	call_c   Dyam_Unify_Item(&ref[385])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun65)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(7),&ref[331])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Deallocate()
	pl_jump  fun19(&seed[109],12)

;; TERM 384: '*FIRST*'(special_app_model(application{item=> none, trans=> ('*CONT*' :> _B(_C)(_D)), item_comp=> _E, code=> (allocate :> _F), item_id=> _G, trans_id=> 0, mode=> _H, restrict=> _I, out_env=> _J, key=> _K})) :> []
c_code local build_ref_384
	ret_reg &ref[384]
	call_c   build_ref_383()
	call_c   Dyam_Create_Binary(I(9),&ref[383],I(0))
	move_ret ref[384]
	c_ret

;; TERM 383: '*FIRST*'(special_app_model(application{item=> none, trans=> ('*CONT*' :> _B(_C)(_D)), item_comp=> _E, code=> (allocate :> _F), item_id=> _G, trans_id=> 0, mode=> _H, restrict=> _I, out_env=> _J, key=> _K}))
c_code local build_ref_383
	ret_reg &ref[383]
	call_c   build_ref_325()
	call_c   build_ref_382()
	call_c   Dyam_Create_Unary(&ref[325],&ref[382])
	move_ret ref[383]
	c_ret

c_code local build_seed_14
	ret_reg &seed[14]
	call_c   build_ref_4()
	call_c   build_ref_392()
	call_c   Dyam_Seed_Start(&ref[4],&ref[392],I(0),fun1,1)
	call_c   build_ref_393()
	call_c   Dyam_Seed_Add_Comp(&ref[393],fun96,0)
	call_c   Dyam_Seed_End()
	move_ret seed[14]
	c_ret

;; TERM 393: '*PROLOG-ITEM*'{top=> app_model(application{item=> '*LIGHTRITEM*'(_B, _C), trans=> ('*LIGHTRITEM*'(_B, _C) :> _D), item_comp=> '*LIGHTRITEM*'(_B, _C), code=> _E, item_id=> _F, trans_id=> _G, mode=> _H, restrict=> _I, out_env=> _J, key=> _K}), cont=> _A}
c_code local build_ref_393
	ret_reg &ref[393]
	call_c   build_ref_724()
	call_c   build_ref_390()
	call_c   Dyam_Create_Binary(&ref[724],&ref[390],V(0))
	move_ret ref[393]
	c_ret

;; TERM 390: app_model(application{item=> '*LIGHTRITEM*'(_B, _C), trans=> ('*LIGHTRITEM*'(_B, _C) :> _D), item_comp=> '*LIGHTRITEM*'(_B, _C), code=> _E, item_id=> _F, trans_id=> _G, mode=> _H, restrict=> _I, out_env=> _J, key=> _K})
c_code local build_ref_390
	ret_reg &ref[390]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_801()
	call_c   build_ref_398()
	call_c   build_ref_399()
	call_c   Dyam_Term_Start(&ref[801],10)
	call_c   Dyam_Term_Arg(&ref[398])
	call_c   Dyam_Term_Arg(&ref[399])
	call_c   Dyam_Term_Arg(&ref[398])
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_813()
	call_c   Dyam_Create_Unary(&ref[813],R(0))
	move_ret ref[390]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 813: app_model
c_code local build_ref_813
	ret_reg &ref[813]
	call_c   Dyam_Create_Atom("app_model")
	move_ret ref[813]
	c_ret

;; TERM 399: '*LIGHTRITEM*'(_B, _C) :> _D
c_code local build_ref_399
	ret_reg &ref[399]
	call_c   build_ref_398()
	call_c   Dyam_Create_Binary(I(9),&ref[398],V(3))
	move_ret ref[399]
	c_ret

;; TERM 398: '*LIGHTRITEM*'(_B, _C)
c_code local build_ref_398
	ret_reg &ref[398]
	call_c   build_ref_814()
	call_c   Dyam_Create_Binary(&ref[814],V(1),V(2))
	move_ret ref[398]
	c_ret

;; TERM 814: '*LIGHTRITEM*'
c_code local build_ref_814
	ret_reg &ref[814]
	call_c   Dyam_Create_Atom("*LIGHTRITEM*")
	move_ret ref[814]
	c_ret

pl_code local fun94
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Deallocate()
	pl_jump  Follow_Cont(V(0))

long local pool_fun95[3]=[2,build_ref_338,build_ref_399]

pl_code local fun95
	call_c   Dyam_Update_Choice(fun94)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(7),&ref[338])
	fail_ret
	call_c   Dyam_Cut()
	move     &ref[399], R(0)
	move     S(5), R(1)
	pl_call  pred_holes_1()
	call_c   Dyam_Unify(V(9),I(0))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_jump  Follow_Cont(V(0))

c_code local build_seed_110
	ret_reg &seed[110]
	call_c   build_ref_59()
	call_c   build_ref_394()
	call_c   Dyam_Seed_Start(&ref[59],&ref[394],I(0),fun15,1)
	call_c   build_ref_397()
	call_c   Dyam_Seed_Add_Comp(&ref[397],&ref[394],0)
	call_c   Dyam_Seed_End()
	move_ret seed[110]
	c_ret

;; TERM 397: '*PROLOG-FIRST*'(unfold_std_trans(('*LIGHTRITEM*'(_B, _C) :> _D), _E, _J)) :> '$$HOLE$$'
c_code local build_ref_397
	ret_reg &ref[397]
	call_c   build_ref_396()
	call_c   Dyam_Create_Binary(I(9),&ref[396],I(7))
	move_ret ref[397]
	c_ret

;; TERM 396: '*PROLOG-FIRST*'(unfold_std_trans(('*LIGHTRITEM*'(_B, _C) :> _D), _E, _J))
c_code local build_ref_396
	ret_reg &ref[396]
	call_c   build_ref_5()
	call_c   build_ref_395()
	call_c   Dyam_Create_Unary(&ref[5],&ref[395])
	move_ret ref[396]
	c_ret

;; TERM 395: unfold_std_trans(('*LIGHTRITEM*'(_B, _C) :> _D), _E, _J)
c_code local build_ref_395
	ret_reg &ref[395]
	call_c   build_ref_788()
	call_c   build_ref_399()
	call_c   Dyam_Term_Start(&ref[788],3)
	call_c   Dyam_Term_Arg(&ref[399])
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret ref[395]
	c_ret

;; TERM 394: '*PROLOG-ITEM*'{top=> unfold_std_trans(('*LIGHTRITEM*'(_B, _C) :> _D), _E, _J), cont=> _A}
c_code local build_ref_394
	ret_reg &ref[394]
	call_c   build_ref_724()
	call_c   build_ref_395()
	call_c   Dyam_Create_Binary(&ref[724],&ref[395],V(0))
	move_ret ref[394]
	c_ret

long local pool_fun96[5]=[65539,build_ref_393,build_ref_331,build_seed_110,pool_fun95]

pl_code local fun96
	call_c   Dyam_Pool(pool_fun96)
	call_c   Dyam_Unify_Item(&ref[393])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun95)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(7),&ref[331])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Deallocate()
	pl_jump  fun19(&seed[110],12)

;; TERM 392: '*PROLOG-FIRST*'(app_model(application{item=> '*LIGHTRITEM*'(_B, _C), trans=> ('*LIGHTRITEM*'(_B, _C) :> _D), item_comp=> '*LIGHTRITEM*'(_B, _C), code=> _E, item_id=> _F, trans_id=> _G, mode=> _H, restrict=> _I, out_env=> _J, key=> _K})) :> []
c_code local build_ref_392
	ret_reg &ref[392]
	call_c   build_ref_391()
	call_c   Dyam_Create_Binary(I(9),&ref[391],I(0))
	move_ret ref[392]
	c_ret

;; TERM 391: '*PROLOG-FIRST*'(app_model(application{item=> '*LIGHTRITEM*'(_B, _C), trans=> ('*LIGHTRITEM*'(_B, _C) :> _D), item_comp=> '*LIGHTRITEM*'(_B, _C), code=> _E, item_id=> _F, trans_id=> _G, mode=> _H, restrict=> _I, out_env=> _J, key=> _K}))
c_code local build_ref_391
	ret_reg &ref[391]
	call_c   build_ref_5()
	call_c   build_ref_390()
	call_c   Dyam_Create_Unary(&ref[5],&ref[390])
	move_ret ref[391]
	c_ret

c_code local build_seed_15
	ret_reg &seed[15]
	call_c   build_ref_4()
	call_c   build_ref_402()
	call_c   Dyam_Seed_Start(&ref[4],&ref[402],I(0),fun1,1)
	call_c   build_ref_403()
	call_c   Dyam_Seed_Add_Comp(&ref[403],fun98,0)
	call_c   Dyam_Seed_End()
	move_ret seed[15]
	c_ret

;; TERM 403: '*PROLOG-ITEM*'{top=> app_model(application{item=> '*RITEM*'(_B, _C), trans=> ('*RITEM*'(_B, _C) :> _D), item_comp=> '*RITEM*'(_B, _C), code=> _E, item_id=> _F, trans_id=> _G, mode=> _H, restrict=> _I, out_env=> _J, key=> _K}), cont=> _A}
c_code local build_ref_403
	ret_reg &ref[403]
	call_c   build_ref_724()
	call_c   build_ref_400()
	call_c   Dyam_Create_Binary(&ref[724],&ref[400],V(0))
	move_ret ref[403]
	c_ret

;; TERM 400: app_model(application{item=> '*RITEM*'(_B, _C), trans=> ('*RITEM*'(_B, _C) :> _D), item_comp=> '*RITEM*'(_B, _C), code=> _E, item_id=> _F, trans_id=> _G, mode=> _H, restrict=> _I, out_env=> _J, key=> _K})
c_code local build_ref_400
	ret_reg &ref[400]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_801()
	call_c   build_ref_408()
	call_c   build_ref_409()
	call_c   Dyam_Term_Start(&ref[801],10)
	call_c   Dyam_Term_Arg(&ref[408])
	call_c   Dyam_Term_Arg(&ref[409])
	call_c   Dyam_Term_Arg(&ref[408])
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_813()
	call_c   Dyam_Create_Unary(&ref[813],R(0))
	move_ret ref[400]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 409: '*RITEM*'(_B, _C) :> _D
c_code local build_ref_409
	ret_reg &ref[409]
	call_c   build_ref_408()
	call_c   Dyam_Create_Binary(I(9),&ref[408],V(3))
	move_ret ref[409]
	c_ret

long local pool_fun97[3]=[2,build_ref_338,build_ref_409]

pl_code local fun97
	call_c   Dyam_Update_Choice(fun94)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(7),&ref[338])
	fail_ret
	call_c   Dyam_Cut()
	move     &ref[409], R(0)
	move     S(5), R(1)
	pl_call  pred_holes_1()
	call_c   Dyam_Unify(V(9),I(0))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_jump  Follow_Cont(V(0))

c_code local build_seed_111
	ret_reg &seed[111]
	call_c   build_ref_59()
	call_c   build_ref_404()
	call_c   Dyam_Seed_Start(&ref[59],&ref[404],I(0),fun15,1)
	call_c   build_ref_407()
	call_c   Dyam_Seed_Add_Comp(&ref[407],&ref[404],0)
	call_c   Dyam_Seed_End()
	move_ret seed[111]
	c_ret

;; TERM 407: '*PROLOG-FIRST*'(unfold_std_trans(('*RITEM*'(_B, _C) :> _D), _E, _J)) :> '$$HOLE$$'
c_code local build_ref_407
	ret_reg &ref[407]
	call_c   build_ref_406()
	call_c   Dyam_Create_Binary(I(9),&ref[406],I(7))
	move_ret ref[407]
	c_ret

;; TERM 406: '*PROLOG-FIRST*'(unfold_std_trans(('*RITEM*'(_B, _C) :> _D), _E, _J))
c_code local build_ref_406
	ret_reg &ref[406]
	call_c   build_ref_5()
	call_c   build_ref_405()
	call_c   Dyam_Create_Unary(&ref[5],&ref[405])
	move_ret ref[406]
	c_ret

;; TERM 405: unfold_std_trans(('*RITEM*'(_B, _C) :> _D), _E, _J)
c_code local build_ref_405
	ret_reg &ref[405]
	call_c   build_ref_788()
	call_c   build_ref_409()
	call_c   Dyam_Term_Start(&ref[788],3)
	call_c   Dyam_Term_Arg(&ref[409])
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret ref[405]
	c_ret

;; TERM 404: '*PROLOG-ITEM*'{top=> unfold_std_trans(('*RITEM*'(_B, _C) :> _D), _E, _J), cont=> _A}
c_code local build_ref_404
	ret_reg &ref[404]
	call_c   build_ref_724()
	call_c   build_ref_405()
	call_c   Dyam_Create_Binary(&ref[724],&ref[405],V(0))
	move_ret ref[404]
	c_ret

long local pool_fun98[5]=[65539,build_ref_403,build_ref_331,build_seed_111,pool_fun97]

pl_code local fun98
	call_c   Dyam_Pool(pool_fun98)
	call_c   Dyam_Unify_Item(&ref[403])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun97)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(7),&ref[331])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Deallocate()
	pl_jump  fun19(&seed[111],12)

;; TERM 402: '*PROLOG-FIRST*'(app_model(application{item=> '*RITEM*'(_B, _C), trans=> ('*RITEM*'(_B, _C) :> _D), item_comp=> '*RITEM*'(_B, _C), code=> _E, item_id=> _F, trans_id=> _G, mode=> _H, restrict=> _I, out_env=> _J, key=> _K})) :> []
c_code local build_ref_402
	ret_reg &ref[402]
	call_c   build_ref_401()
	call_c   Dyam_Create_Binary(I(9),&ref[401],I(0))
	move_ret ref[402]
	c_ret

;; TERM 401: '*PROLOG-FIRST*'(app_model(application{item=> '*RITEM*'(_B, _C), trans=> ('*RITEM*'(_B, _C) :> _D), item_comp=> '*RITEM*'(_B, _C), code=> _E, item_id=> _F, trans_id=> _G, mode=> _H, restrict=> _I, out_env=> _J, key=> _K}))
c_code local build_ref_401
	ret_reg &ref[401]
	call_c   build_ref_5()
	call_c   build_ref_400()
	call_c   Dyam_Create_Unary(&ref[5],&ref[400])
	move_ret ref[401]
	c_ret

c_code local build_seed_44
	ret_reg &seed[44]
	call_c   build_ref_4()
	call_c   build_ref_419()
	call_c   Dyam_Seed_Start(&ref[4],&ref[419],I(0),fun1,1)
	call_c   build_ref_420()
	call_c   Dyam_Seed_Add_Comp(&ref[420],fun121,0)
	call_c   Dyam_Seed_End()
	move_ret seed[44]
	c_ret

;; TERM 420: '*PROLOG-ITEM*'{top=> unfold('*LIGHTLCNEXT*'(_B, _C, _D, _E, _F), _G, _H, _I, _J), cont=> _A}
c_code local build_ref_420
	ret_reg &ref[420]
	call_c   build_ref_724()
	call_c   build_ref_417()
	call_c   Dyam_Create_Binary(&ref[724],&ref[417],V(0))
	move_ret ref[420]
	c_ret

;; TERM 417: unfold('*LIGHTLCNEXT*'(_B, _C, _D, _E, _F), _G, _H, _I, _J)
c_code local build_ref_417
	ret_reg &ref[417]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_815()
	call_c   Dyam_Term_Start(&ref[815],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_725()
	call_c   Dyam_Term_Start(&ref[725],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret ref[417]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 815: '*LIGHTLCNEXT*'
c_code local build_ref_815
	ret_reg &ref[815]
	call_c   Dyam_Create_Atom("*LIGHTLCNEXT*")
	move_ret ref[815]
	c_ret

;; TERM 456: _B ^ _L ^ (_M , _N)
c_code local build_ref_456
	ret_reg &ref[456]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Binary(I(4),V(12),V(13))
	move_ret R(0)
	call_c   build_ref_790()
	call_c   Dyam_Create_Binary(&ref[790],V(11),R(0))
	move_ret R(0)
	call_c   Dyam_Create_Binary(&ref[790],V(1),R(0))
	move_ret ref[456]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_121
	ret_reg &seed[121]
	call_c   build_ref_59()
	call_c   build_ref_462()
	call_c   Dyam_Seed_Start(&ref[59],&ref[462],I(0),fun15,1)
	call_c   build_ref_465()
	call_c   Dyam_Seed_Add_Comp(&ref[465],&ref[462],0)
	call_c   Dyam_Seed_End()
	move_ret seed[121]
	c_ret

;; TERM 465: '*PROLOG-FIRST*'(unfold(_N, [], _H, _O, _P)) :> '$$HOLE$$'
c_code local build_ref_465
	ret_reg &ref[465]
	call_c   build_ref_464()
	call_c   Dyam_Create_Binary(I(9),&ref[464],I(7))
	move_ret ref[465]
	c_ret

;; TERM 464: '*PROLOG-FIRST*'(unfold(_N, [], _H, _O, _P))
c_code local build_ref_464
	ret_reg &ref[464]
	call_c   build_ref_5()
	call_c   build_ref_463()
	call_c   Dyam_Create_Unary(&ref[5],&ref[463])
	move_ret ref[464]
	c_ret

;; TERM 463: unfold(_N, [], _H, _O, _P)
c_code local build_ref_463
	ret_reg &ref[463]
	call_c   build_ref_725()
	call_c   Dyam_Term_Start(&ref[725],5)
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_End()
	move_ret ref[463]
	c_ret

;; TERM 462: '*PROLOG-ITEM*'{top=> unfold(_N, [], _H, _O, _P), cont=> '$CLOSURE'('$fun'(119, 0, 1178141272), '$TUPPLE'(35125253113224))}
c_code local build_ref_462
	ret_reg &ref[462]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,528171008)
	move_ret R(0)
	call_c   Dyam_Closure_Aux(fun119,R(0))
	move_ret R(0)
	call_c   build_ref_724()
	call_c   build_ref_463()
	call_c   Dyam_Create_Binary(&ref[724],&ref[463],R(0))
	move_ret ref[462]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_120
	ret_reg &seed[120]
	call_c   build_ref_59()
	call_c   build_ref_458()
	call_c   Dyam_Seed_Start(&ref[59],&ref[458],I(0),fun15,1)
	call_c   build_ref_461()
	call_c   Dyam_Seed_Add_Comp(&ref[461],&ref[458],0)
	call_c   Dyam_Seed_End()
	move_ret seed[120]
	c_ret

;; TERM 461: '*PROLOG-FIRST*'(unfold(_M, [], _H, _Q, _R)) :> '$$HOLE$$'
c_code local build_ref_461
	ret_reg &ref[461]
	call_c   build_ref_460()
	call_c   Dyam_Create_Binary(I(9),&ref[460],I(7))
	move_ret ref[461]
	c_ret

;; TERM 460: '*PROLOG-FIRST*'(unfold(_M, [], _H, _Q, _R))
c_code local build_ref_460
	ret_reg &ref[460]
	call_c   build_ref_5()
	call_c   build_ref_459()
	call_c   Dyam_Create_Unary(&ref[5],&ref[459])
	move_ret ref[460]
	c_ret

;; TERM 459: unfold(_M, [], _H, _Q, _R)
c_code local build_ref_459
	ret_reg &ref[459]
	call_c   build_ref_725()
	call_c   Dyam_Term_Start(&ref[725],5)
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_End()
	move_ret ref[459]
	c_ret

;; TERM 458: '*PROLOG-ITEM*'{top=> unfold(_M, [], _H, _Q, _R), cont=> '$CLOSURE'('$fun'(118, 0, 1178126360), '$TUPPLE'(35125253113184))}
c_code local build_ref_458
	ret_reg &ref[458]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,528109568)
	move_ret R(0)
	call_c   Dyam_Closure_Aux(fun118,R(0))
	move_ret R(0)
	call_c   build_ref_724()
	call_c   build_ref_459()
	call_c   Dyam_Create_Binary(&ref[724],&ref[459],R(0))
	move_ret ref[458]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 457: [key(select, [_B,_L], _Q),key(lcselect, [_B,_L], _O)]
c_code local build_ref_457
	ret_reg &ref[457]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   Dyam_Create_List(V(11),I(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(1),R(0))
	move_ret R(0)
	call_c   build_ref_792()
	call_c   build_ref_816()
	call_c   Dyam_Term_Start(&ref[792],3)
	call_c   Dyam_Term_Arg(&ref[816])
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_End()
	move_ret R(1)
	call_c   build_ref_793()
	call_c   Dyam_Term_Start(&ref[792],3)
	call_c   Dyam_Term_Arg(&ref[793])
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   Dyam_Create_List(R(0),I(0))
	move_ret R(0)
	call_c   Dyam_Create_List(R(1),R(0))
	move_ret ref[457]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 816: select
c_code local build_ref_816
	ret_reg &ref[816]
	call_c   Dyam_Create_Atom("select")
	move_ret ref[816]
	c_ret

c_code local build_seed_119
	ret_reg &seed[119]
	call_c   build_ref_59()
	call_c   build_ref_452()
	call_c   Dyam_Seed_Start(&ref[59],&ref[452],I(0),fun15,1)
	call_c   build_ref_455()
	call_c   Dyam_Seed_Add_Comp(&ref[455],&ref[452],0)
	call_c   Dyam_Seed_End()
	move_ret seed[119]
	c_ret

;; TERM 455: '*PROLOG-FIRST*'(seed_install(seed{model=> '*CITEM*'(_B, _B), id=> _S, anchor=> _T, subs_comp=> _U, code=> _V, go=> _W, tabule=> _X, schedule=> prolog, subsume=> _Y, model_ref=> _Z, subs_comp_ref=> _A1, c_type=> _B1, c_type_ref=> _C1, out_env=> _D1, appinfo=> _K, tail_flag=> _E1}, 1, _F1)) :> '$$HOLE$$'
c_code local build_ref_455
	ret_reg &ref[455]
	call_c   build_ref_454()
	call_c   Dyam_Create_Binary(I(9),&ref[454],I(7))
	move_ret ref[455]
	c_ret

;; TERM 454: '*PROLOG-FIRST*'(seed_install(seed{model=> '*CITEM*'(_B, _B), id=> _S, anchor=> _T, subs_comp=> _U, code=> _V, go=> _W, tabule=> _X, schedule=> prolog, subsume=> _Y, model_ref=> _Z, subs_comp_ref=> _A1, c_type=> _B1, c_type_ref=> _C1, out_env=> _D1, appinfo=> _K, tail_flag=> _E1}, 1, _F1))
c_code local build_ref_454
	ret_reg &ref[454]
	call_c   build_ref_5()
	call_c   build_ref_453()
	call_c   Dyam_Create_Unary(&ref[5],&ref[453])
	move_ret ref[454]
	c_ret

;; TERM 453: seed_install(seed{model=> '*CITEM*'(_B, _B), id=> _S, anchor=> _T, subs_comp=> _U, code=> _V, go=> _W, tabule=> _X, schedule=> prolog, subsume=> _Y, model_ref=> _Z, subs_comp_ref=> _A1, c_type=> _B1, c_type_ref=> _C1, out_env=> _D1, appinfo=> _K, tail_flag=> _E1}, 1, _F1)
c_code local build_ref_453
	ret_reg &ref[453]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_329()
	call_c   Dyam_Create_Binary(&ref[329],V(1),V(1))
	move_ret R(0)
	call_c   build_ref_748()
	call_c   build_ref_770()
	call_c   Dyam_Term_Start(&ref[748],16)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_Arg(V(21))
	call_c   Dyam_Term_Arg(V(22))
	call_c   Dyam_Term_Arg(V(23))
	call_c   Dyam_Term_Arg(&ref[770])
	call_c   Dyam_Term_Arg(V(24))
	call_c   Dyam_Term_Arg(V(25))
	call_c   Dyam_Term_Arg(V(26))
	call_c   Dyam_Term_Arg(V(27))
	call_c   Dyam_Term_Arg(V(28))
	call_c   Dyam_Term_Arg(V(29))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(30))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_747()
	call_c   Dyam_Term_Start(&ref[747],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(N(1))
	call_c   Dyam_Term_Arg(V(31))
	call_c   Dyam_Term_End()
	move_ret ref[453]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 452: '*PROLOG-ITEM*'{top=> seed_install(seed{model=> '*CITEM*'(_B, _B), id=> _S, anchor=> _T, subs_comp=> _U, code=> _V, go=> _W, tabule=> _X, schedule=> prolog, subsume=> _Y, model_ref=> _Z, subs_comp_ref=> _A1, c_type=> _B1, c_type_ref=> _C1, out_env=> _D1, appinfo=> _K, tail_flag=> _E1}, 1, _F1), cont=> '$CLOSURE'('$fun'(116, 0, 1178122804), '$TUPPLE'(35125252332480))}
c_code local build_ref_452
	ret_reg &ref[452]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Start_Tupple(0,527958016)
	call_c   Dyam_Almost_End_Tupple(29,67108864)
	move_ret R(0)
	call_c   Dyam_Closure_Aux(fun116,R(0))
	move_ret R(0)
	call_c   build_ref_724()
	call_c   build_ref_453()
	call_c   Dyam_Create_Binary(&ref[724],&ref[453],R(0))
	move_ret ref[452]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_114
	ret_reg &seed[114]
	call_c   build_ref_59()
	call_c   build_ref_426()
	call_c   Dyam_Seed_Start(&ref[59],&ref[426],I(0),fun15,1)
	call_c   build_ref_429()
	call_c   Dyam_Seed_Add_Comp(&ref[429],&ref[426],0)
	call_c   Dyam_Seed_End()
	move_ret seed[114]
	c_ret

;; TERM 429: '*PROLOG-FIRST*'(seed_install(seed{model=> ('*RITEM*'(_B, _C) :> _D(_G)(_H)), id=> _H1, anchor=> _I1, subs_comp=> _J1, code=> _K1, go=> _L1, tabule=> light, schedule=> prolog, subsume=> _M1, model_ref=> _N1, subs_comp_ref=> _O1, c_type=> _P1, c_type_ref=> _Q1, out_env=> [_J], appinfo=> _R1, tail_flag=> _S1}, _G1, _T1)) :> '$$HOLE$$'
c_code local build_ref_429
	ret_reg &ref[429]
	call_c   build_ref_428()
	call_c   Dyam_Create_Binary(I(9),&ref[428],I(7))
	move_ret ref[429]
	c_ret

;; TERM 428: '*PROLOG-FIRST*'(seed_install(seed{model=> ('*RITEM*'(_B, _C) :> _D(_G)(_H)), id=> _H1, anchor=> _I1, subs_comp=> _J1, code=> _K1, go=> _L1, tabule=> light, schedule=> prolog, subsume=> _M1, model_ref=> _N1, subs_comp_ref=> _O1, c_type=> _P1, c_type_ref=> _Q1, out_env=> [_J], appinfo=> _R1, tail_flag=> _S1}, _G1, _T1))
c_code local build_ref_428
	ret_reg &ref[428]
	call_c   build_ref_5()
	call_c   build_ref_427()
	call_c   Dyam_Create_Unary(&ref[5],&ref[427])
	move_ret ref[428]
	c_ret

;; TERM 427: seed_install(seed{model=> ('*RITEM*'(_B, _C) :> _D(_G)(_H)), id=> _H1, anchor=> _I1, subs_comp=> _J1, code=> _K1, go=> _L1, tabule=> light, schedule=> prolog, subsume=> _M1, model_ref=> _N1, subs_comp_ref=> _O1, c_type=> _P1, c_type_ref=> _Q1, out_env=> [_J], appinfo=> _R1, tail_flag=> _S1}, _G1, _T1)
c_code local build_ref_427
	ret_reg &ref[427]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   Dyam_Create_Binary(I(3),V(3),V(6))
	move_ret R(0)
	call_c   Dyam_Create_Binary(I(3),R(0),V(7))
	move_ret R(0)
	call_c   build_ref_408()
	call_c   Dyam_Create_Binary(I(9),&ref[408],R(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(9),I(0))
	move_ret R(1)
	call_c   build_ref_748()
	call_c   build_ref_795()
	call_c   build_ref_770()
	call_c   Dyam_Term_Start(&ref[748],16)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(33))
	call_c   Dyam_Term_Arg(V(34))
	call_c   Dyam_Term_Arg(V(35))
	call_c   Dyam_Term_Arg(V(36))
	call_c   Dyam_Term_Arg(V(37))
	call_c   Dyam_Term_Arg(&ref[795])
	call_c   Dyam_Term_Arg(&ref[770])
	call_c   Dyam_Term_Arg(V(38))
	call_c   Dyam_Term_Arg(V(39))
	call_c   Dyam_Term_Arg(V(40))
	call_c   Dyam_Term_Arg(V(41))
	call_c   Dyam_Term_Arg(V(42))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(43))
	call_c   Dyam_Term_Arg(V(44))
	call_c   Dyam_Term_End()
	move_ret R(1)
	call_c   build_ref_747()
	call_c   Dyam_Term_Start(&ref[747],3)
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(32))
	call_c   Dyam_Term_Arg(V(45))
	call_c   Dyam_Term_End()
	move_ret ref[427]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 426: '*PROLOG-ITEM*'{top=> seed_install(seed{model=> ('*RITEM*'(_B, _C) :> _D(_G)(_H)), id=> _H1, anchor=> _I1, subs_comp=> _J1, code=> _K1, go=> _L1, tabule=> light, schedule=> prolog, subsume=> _M1, model_ref=> _N1, subs_comp_ref=> _O1, c_type=> _P1, c_type_ref=> _Q1, out_env=> [_J], appinfo=> _R1, tail_flag=> _S1}, _G1, _T1), cont=> '$CLOSURE'('$fun'(107, 0, 1178055436), '$TUPPLE'(35125252332136))}
c_code local build_ref_426
	ret_reg &ref[426]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Start_Tupple(0,269484032)
	call_c   Dyam_Almost_End_Tupple(29,67112960)
	move_ret R(0)
	call_c   Dyam_Closure_Aux(fun107,R(0))
	move_ret R(0)
	call_c   build_ref_724()
	call_c   build_ref_427()
	call_c   Dyam_Create_Binary(&ref[724],&ref[427],R(0))
	move_ret ref[426]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 423: _T1 :> deallocate_layer :> deallocate :> succeed
c_code local build_ref_423
	ret_reg &ref[423]
	call_c   build_ref_422()
	call_c   Dyam_Create_Binary(I(9),V(45),&ref[422])
	move_ret ref[423]
	c_ret

;; TERM 425: _F1 :> fail
c_code local build_ref_425
	ret_reg &ref[425]
	call_c   build_ref_424()
	call_c   Dyam_Create_Binary(I(9),V(31),&ref[424])
	move_ret ref[425]
	c_ret

long local pool_fun107[3]=[2,build_ref_423,build_ref_425]

pl_code local fun107
	call_c   Dyam_Pool(pool_fun107)
	call_c   Dyam_Allocate(0)
	move     &ref[423], R(0)
	move     S(5), R(1)
	move     &ref[425], R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Load(4,V(8))
	pl_call  pred_choice_code_3()
	call_c   Dyam_Deallocate()
	pl_jump  Follow_Cont(V(0))

long local pool_fun116[2]=[1,build_seed_114]

pl_code local fun116
	call_c   Dyam_Pool(pool_fun116)
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(4))
	move     N(2), R(2)
	move     0, R(3)
	move     V(32), R(4)
	move     S(5), R(5)
	pl_call  pred_check_tag_3()
	call_c   Dyam_Deallocate()
	pl_jump  fun19(&seed[114],12)

long local pool_fun117[2]=[1,build_seed_119]

long local pool_fun118[3]=[65537,build_ref_457,pool_fun117]

pl_code local fun118
	call_c   Dyam_Pool(pool_fun118)
	call_c   Dyam_Unify(V(10),&ref[457])
	fail_ret
	call_c   Dyam_Allocate(0)
fun117:
	call_c   Dyam_Deallocate()
	pl_jump  fun19(&seed[119],12)


long local pool_fun119[2]=[1,build_seed_120]

pl_code local fun119
	call_c   Dyam_Pool(pool_fun119)
	pl_jump  fun19(&seed[120],12)

long local pool_fun120[3]=[2,build_ref_456,build_seed_121]

pl_code local fun120
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(5),&ref[456])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_jump  fun19(&seed[121],12)

long local pool_fun121[4]=[131073,build_ref_420,pool_fun120,pool_fun117]

pl_code local fun121
	call_c   Dyam_Pool(pool_fun121)
	call_c   Dyam_Unify_Item(&ref[420])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun120)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(5),I(0))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(10),I(0))
	fail_ret
	pl_jump  fun117()

;; TERM 419: '*PROLOG-FIRST*'(unfold('*LIGHTLCNEXT*'(_B, _C, _D, _E, _F), _G, _H, _I, _J)) :> []
c_code local build_ref_419
	ret_reg &ref[419]
	call_c   build_ref_418()
	call_c   Dyam_Create_Binary(I(9),&ref[418],I(0))
	move_ret ref[419]
	c_ret

;; TERM 418: '*PROLOG-FIRST*'(unfold('*LIGHTLCNEXT*'(_B, _C, _D, _E, _F), _G, _H, _I, _J))
c_code local build_ref_418
	ret_reg &ref[418]
	call_c   build_ref_5()
	call_c   build_ref_417()
	call_c   Dyam_Create_Unary(&ref[5],&ref[417])
	move_ret ref[418]
	c_ret

c_code local build_seed_42
	ret_reg &seed[42]
	call_c   build_ref_4()
	call_c   build_ref_432()
	call_c   Dyam_Seed_Start(&ref[4],&ref[432],I(0),fun1,1)
	call_c   build_ref_433()
	call_c   Dyam_Seed_Add_Comp(&ref[433],fun114,0)
	call_c   Dyam_Seed_End()
	move_ret seed[42]
	c_ret

;; TERM 433: '*PROLOG-ITEM*'{top=> unfold('*LCNEXT*'(_B, _C, _D, _E, _F), _G, _H, (_I :> _J), _K), cont=> _A}
c_code local build_ref_433
	ret_reg &ref[433]
	call_c   build_ref_724()
	call_c   build_ref_430()
	call_c   Dyam_Create_Binary(&ref[724],&ref[430],V(0))
	move_ret ref[433]
	c_ret

;; TERM 430: unfold('*LCNEXT*'(_B, _C, _D, _E, _F), _G, _H, (_I :> _J), _K)
c_code local build_ref_430
	ret_reg &ref[430]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_817()
	call_c   Dyam_Term_Start(&ref[817],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   Dyam_Create_Binary(I(9),V(8),V(9))
	move_ret R(1)
	call_c   build_ref_725()
	call_c   Dyam_Term_Start(&ref[725],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_End()
	move_ret ref[430]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 817: '*LCNEXT*'
c_code local build_ref_817
	ret_reg &ref[817]
	call_c   Dyam_Create_Atom("*LCNEXT*")
	move_ret ref[817]
	c_ret

;; TERM 442: _B ^ _M ^ (_N , _O)
c_code local build_ref_442
	ret_reg &ref[442]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Binary(I(4),V(13),V(14))
	move_ret R(0)
	call_c   build_ref_790()
	call_c   Dyam_Create_Binary(&ref[790],V(12),R(0))
	move_ret R(0)
	call_c   Dyam_Create_Binary(&ref[790],V(1),R(0))
	move_ret ref[442]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_118
	ret_reg &seed[118]
	call_c   build_ref_59()
	call_c   build_ref_448()
	call_c   Dyam_Seed_Start(&ref[59],&ref[448],I(0),fun15,1)
	call_c   build_ref_451()
	call_c   Dyam_Seed_Add_Comp(&ref[451],&ref[448],0)
	call_c   Dyam_Seed_End()
	move_ret seed[118]
	c_ret

;; TERM 451: '*PROLOG-FIRST*'(unfold(_O, [], _H, _P, _Q)) :> '$$HOLE$$'
c_code local build_ref_451
	ret_reg &ref[451]
	call_c   build_ref_450()
	call_c   Dyam_Create_Binary(I(9),&ref[450],I(7))
	move_ret ref[451]
	c_ret

;; TERM 450: '*PROLOG-FIRST*'(unfold(_O, [], _H, _P, _Q))
c_code local build_ref_450
	ret_reg &ref[450]
	call_c   build_ref_5()
	call_c   build_ref_449()
	call_c   Dyam_Create_Unary(&ref[5],&ref[449])
	move_ret ref[450]
	c_ret

;; TERM 449: unfold(_O, [], _H, _P, _Q)
c_code local build_ref_449
	ret_reg &ref[449]
	call_c   build_ref_725()
	call_c   Dyam_Term_Start(&ref[725],5)
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_End()
	move_ret ref[449]
	c_ret

;; TERM 448: '*PROLOG-ITEM*'{top=> unfold(_O, [], _H, _P, _Q), cont=> '$CLOSURE'('$fun'(112, 0, 1178100952), '$TUPPLE'(35125252332384))}
c_code local build_ref_448
	ret_reg &ref[448]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,528326656)
	move_ret R(0)
	call_c   Dyam_Closure_Aux(fun112,R(0))
	move_ret R(0)
	call_c   build_ref_724()
	call_c   build_ref_449()
	call_c   Dyam_Create_Binary(&ref[724],&ref[449],R(0))
	move_ret ref[448]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_117
	ret_reg &seed[117]
	call_c   build_ref_59()
	call_c   build_ref_444()
	call_c   Dyam_Seed_Start(&ref[59],&ref[444],I(0),fun15,1)
	call_c   build_ref_447()
	call_c   Dyam_Seed_Add_Comp(&ref[447],&ref[444],0)
	call_c   Dyam_Seed_End()
	move_ret seed[117]
	c_ret

;; TERM 447: '*PROLOG-FIRST*'(unfold(_N, [], _H, _R, _S)) :> '$$HOLE$$'
c_code local build_ref_447
	ret_reg &ref[447]
	call_c   build_ref_446()
	call_c   Dyam_Create_Binary(I(9),&ref[446],I(7))
	move_ret ref[447]
	c_ret

;; TERM 446: '*PROLOG-FIRST*'(unfold(_N, [], _H, _R, _S))
c_code local build_ref_446
	ret_reg &ref[446]
	call_c   build_ref_5()
	call_c   build_ref_445()
	call_c   Dyam_Create_Unary(&ref[5],&ref[445])
	move_ret ref[446]
	c_ret

;; TERM 445: unfold(_N, [], _H, _R, _S)
c_code local build_ref_445
	ret_reg &ref[445]
	call_c   build_ref_725()
	call_c   Dyam_Term_Start(&ref[725],5)
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_End()
	move_ret ref[445]
	c_ret

;; TERM 444: '*PROLOG-ITEM*'{top=> unfold(_N, [], _H, _R, _S), cont=> '$CLOSURE'('$fun'(111, 0, 1178096276), '$TUPPLE'(35125252332344))}
c_code local build_ref_444
	ret_reg &ref[444]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,528295936)
	move_ret R(0)
	call_c   Dyam_Closure_Aux(fun111,R(0))
	move_ret R(0)
	call_c   build_ref_724()
	call_c   build_ref_445()
	call_c   Dyam_Create_Binary(&ref[724],&ref[445],R(0))
	move_ret ref[444]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 443: [key(select, [_B,_M], _R),key(lcselect, [_B,_M], _P)]
c_code local build_ref_443
	ret_reg &ref[443]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   Dyam_Create_List(V(12),I(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(1),R(0))
	move_ret R(0)
	call_c   build_ref_792()
	call_c   build_ref_816()
	call_c   Dyam_Term_Start(&ref[792],3)
	call_c   Dyam_Term_Arg(&ref[816])
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_End()
	move_ret R(1)
	call_c   build_ref_793()
	call_c   Dyam_Term_Start(&ref[792],3)
	call_c   Dyam_Term_Arg(&ref[793])
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   Dyam_Create_List(R(0),I(0))
	move_ret R(0)
	call_c   Dyam_Create_List(R(1),R(0))
	move_ret ref[443]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_116
	ret_reg &seed[116]
	call_c   build_ref_59()
	call_c   build_ref_438()
	call_c   Dyam_Seed_Start(&ref[59],&ref[438],I(0),fun15,1)
	call_c   build_ref_441()
	call_c   Dyam_Seed_Add_Comp(&ref[441],&ref[438],0)
	call_c   Dyam_Seed_End()
	move_ret seed[116]
	c_ret

;; TERM 441: '*PROLOG-FIRST*'(seed_install(seed{model=> '*CITEM*'(_B, _B), id=> _T, anchor=> _U, subs_comp=> _V, code=> _W, go=> _X, tabule=> _Y, schedule=> _Z, subsume=> _A1, model_ref=> _B1, subs_comp_ref=> _C1, c_type=> _D1, c_type_ref=> _E1, out_env=> _F1, appinfo=> _L, tail_flag=> _G1}, 1, _I)) :> '$$HOLE$$'
c_code local build_ref_441
	ret_reg &ref[441]
	call_c   build_ref_440()
	call_c   Dyam_Create_Binary(I(9),&ref[440],I(7))
	move_ret ref[441]
	c_ret

;; TERM 440: '*PROLOG-FIRST*'(seed_install(seed{model=> '*CITEM*'(_B, _B), id=> _T, anchor=> _U, subs_comp=> _V, code=> _W, go=> _X, tabule=> _Y, schedule=> _Z, subsume=> _A1, model_ref=> _B1, subs_comp_ref=> _C1, c_type=> _D1, c_type_ref=> _E1, out_env=> _F1, appinfo=> _L, tail_flag=> _G1}, 1, _I))
c_code local build_ref_440
	ret_reg &ref[440]
	call_c   build_ref_5()
	call_c   build_ref_439()
	call_c   Dyam_Create_Unary(&ref[5],&ref[439])
	move_ret ref[440]
	c_ret

;; TERM 439: seed_install(seed{model=> '*CITEM*'(_B, _B), id=> _T, anchor=> _U, subs_comp=> _V, code=> _W, go=> _X, tabule=> _Y, schedule=> _Z, subsume=> _A1, model_ref=> _B1, subs_comp_ref=> _C1, c_type=> _D1, c_type_ref=> _E1, out_env=> _F1, appinfo=> _L, tail_flag=> _G1}, 1, _I)
c_code local build_ref_439
	ret_reg &ref[439]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_329()
	call_c   Dyam_Create_Binary(&ref[329],V(1),V(1))
	move_ret R(0)
	call_c   build_ref_748()
	call_c   Dyam_Term_Start(&ref[748],16)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_Arg(V(21))
	call_c   Dyam_Term_Arg(V(22))
	call_c   Dyam_Term_Arg(V(23))
	call_c   Dyam_Term_Arg(V(24))
	call_c   Dyam_Term_Arg(V(25))
	call_c   Dyam_Term_Arg(V(26))
	call_c   Dyam_Term_Arg(V(27))
	call_c   Dyam_Term_Arg(V(28))
	call_c   Dyam_Term_Arg(V(29))
	call_c   Dyam_Term_Arg(V(30))
	call_c   Dyam_Term_Arg(V(31))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(32))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_747()
	call_c   Dyam_Term_Start(&ref[747],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(N(1))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_End()
	move_ret ref[439]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 438: '*PROLOG-ITEM*'{top=> seed_install(seed{model=> '*CITEM*'(_B, _B), id=> _T, anchor=> _U, subs_comp=> _V, code=> _W, go=> _X, tabule=> _Y, schedule=> _Z, subsume=> _A1, model_ref=> _B1, subs_comp_ref=> _C1, c_type=> _D1, c_type_ref=> _E1, out_env=> _F1, appinfo=> _L, tail_flag=> _G1}, 1, _I), cont=> '$CLOSURE'('$fun'(109, 0, 1178081772), '$TUPPLE'(35125252332256))}
c_code local build_ref_438
	ret_reg &ref[438]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,527171584)
	move_ret R(0)
	call_c   Dyam_Closure_Aux(fun109,R(0))
	move_ret R(0)
	call_c   build_ref_724()
	call_c   build_ref_439()
	call_c   Dyam_Create_Binary(&ref[724],&ref[439],R(0))
	move_ret ref[438]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_115
	ret_reg &seed[115]
	call_c   build_ref_59()
	call_c   build_ref_434()
	call_c   Dyam_Seed_Start(&ref[59],&ref[434],I(0),fun15,1)
	call_c   build_ref_437()
	call_c   Dyam_Seed_Add_Comp(&ref[437],&ref[434],0)
	call_c   Dyam_Seed_End()
	move_ret seed[115]
	c_ret

;; TERM 437: '*PROLOG-FIRST*'(seed_install(seed{model=> ('*RITEM*'(_B, _C) :> _D(_G)(_H)), id=> _I1, anchor=> _J1, subs_comp=> _K1, code=> _L1, go=> _M1, tabule=> _N1, schedule=> _O1, subsume=> _P1, model_ref=> _Q1, subs_comp_ref=> _R1, c_type=> _S1, c_type_ref=> _T1, out_env=> [_K], appinfo=> _U1, tail_flag=> _V1}, _H1, _J)) :> '$$HOLE$$'
c_code local build_ref_437
	ret_reg &ref[437]
	call_c   build_ref_436()
	call_c   Dyam_Create_Binary(I(9),&ref[436],I(7))
	move_ret ref[437]
	c_ret

;; TERM 436: '*PROLOG-FIRST*'(seed_install(seed{model=> ('*RITEM*'(_B, _C) :> _D(_G)(_H)), id=> _I1, anchor=> _J1, subs_comp=> _K1, code=> _L1, go=> _M1, tabule=> _N1, schedule=> _O1, subsume=> _P1, model_ref=> _Q1, subs_comp_ref=> _R1, c_type=> _S1, c_type_ref=> _T1, out_env=> [_K], appinfo=> _U1, tail_flag=> _V1}, _H1, _J))
c_code local build_ref_436
	ret_reg &ref[436]
	call_c   build_ref_5()
	call_c   build_ref_435()
	call_c   Dyam_Create_Unary(&ref[5],&ref[435])
	move_ret ref[436]
	c_ret

;; TERM 435: seed_install(seed{model=> ('*RITEM*'(_B, _C) :> _D(_G)(_H)), id=> _I1, anchor=> _J1, subs_comp=> _K1, code=> _L1, go=> _M1, tabule=> _N1, schedule=> _O1, subsume=> _P1, model_ref=> _Q1, subs_comp_ref=> _R1, c_type=> _S1, c_type_ref=> _T1, out_env=> [_K], appinfo=> _U1, tail_flag=> _V1}, _H1, _J)
c_code local build_ref_435
	ret_reg &ref[435]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   Dyam_Create_Binary(I(3),V(3),V(6))
	move_ret R(0)
	call_c   Dyam_Create_Binary(I(3),R(0),V(7))
	move_ret R(0)
	call_c   build_ref_408()
	call_c   Dyam_Create_Binary(I(9),&ref[408],R(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(10),I(0))
	move_ret R(1)
	call_c   build_ref_748()
	call_c   Dyam_Term_Start(&ref[748],16)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(34))
	call_c   Dyam_Term_Arg(V(35))
	call_c   Dyam_Term_Arg(V(36))
	call_c   Dyam_Term_Arg(V(37))
	call_c   Dyam_Term_Arg(V(38))
	call_c   Dyam_Term_Arg(V(39))
	call_c   Dyam_Term_Arg(V(40))
	call_c   Dyam_Term_Arg(V(41))
	call_c   Dyam_Term_Arg(V(42))
	call_c   Dyam_Term_Arg(V(43))
	call_c   Dyam_Term_Arg(V(44))
	call_c   Dyam_Term_Arg(V(45))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(46))
	call_c   Dyam_Term_Arg(V(47))
	call_c   Dyam_Term_End()
	move_ret R(1)
	call_c   build_ref_747()
	call_c   Dyam_Term_Start(&ref[747],3)
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(33))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret ref[435]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 434: '*PROLOG-ITEM*'{top=> seed_install(seed{model=> ('*RITEM*'(_B, _C) :> _D(_G)(_H)), id=> _I1, anchor=> _J1, subs_comp=> _K1, code=> _L1, go=> _M1, tabule=> _N1, schedule=> _O1, subsume=> _P1, model_ref=> _Q1, subs_comp_ref=> _R1, c_type=> _S1, c_type_ref=> _T1, out_env=> [_K], appinfo=> _U1, tail_flag=> _V1}, _H1, _J), cont=> _A}
c_code local build_ref_434
	ret_reg &ref[434]
	call_c   build_ref_724()
	call_c   build_ref_435()
	call_c   Dyam_Create_Binary(&ref[724],&ref[435],V(0))
	move_ret ref[434]
	c_ret

long local pool_fun109[2]=[1,build_seed_115]

pl_code local fun109
	call_c   Dyam_Pool(pool_fun109)
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(4))
	move     N(2), R(2)
	move     0, R(3)
	move     V(33), R(4)
	move     S(5), R(5)
	pl_call  pred_check_tag_3()
	call_c   Dyam_Deallocate()
	pl_jump  fun19(&seed[115],12)

long local pool_fun110[2]=[1,build_seed_116]

long local pool_fun111[3]=[65537,build_ref_443,pool_fun110]

pl_code local fun111
	call_c   Dyam_Pool(pool_fun111)
	call_c   Dyam_Unify(V(11),&ref[443])
	fail_ret
	call_c   Dyam_Allocate(0)
fun110:
	call_c   Dyam_Deallocate()
	pl_jump  fun19(&seed[116],12)


long local pool_fun112[2]=[1,build_seed_117]

pl_code local fun112
	call_c   Dyam_Pool(pool_fun112)
	pl_jump  fun19(&seed[117],12)

long local pool_fun113[3]=[2,build_ref_442,build_seed_118]

pl_code local fun113
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(5),&ref[442])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_jump  fun19(&seed[118],12)

long local pool_fun114[4]=[131073,build_ref_433,pool_fun113,pool_fun110]

pl_code local fun114
	call_c   Dyam_Pool(pool_fun114)
	call_c   Dyam_Unify_Item(&ref[433])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun113)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(5),I(0))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(11),I(0))
	fail_ret
	pl_jump  fun110()

;; TERM 432: '*PROLOG-FIRST*'(unfold('*LCNEXT*'(_B, _C, _D, _E, _F), _G, _H, (_I :> _J), _K)) :> []
c_code local build_ref_432
	ret_reg &ref[432]
	call_c   build_ref_431()
	call_c   Dyam_Create_Binary(I(9),&ref[431],I(0))
	move_ret ref[432]
	c_ret

;; TERM 431: '*PROLOG-FIRST*'(unfold('*LCNEXT*'(_B, _C, _D, _E, _F), _G, _H, (_I :> _J), _K))
c_code local build_ref_431
	ret_reg &ref[431]
	call_c   build_ref_5()
	call_c   build_ref_430()
	call_c   Dyam_Create_Unary(&ref[5],&ref[430])
	move_ret ref[431]
	c_ret

c_code local build_seed_51
	ret_reg &seed[51]
	call_c   build_ref_4()
	call_c   build_ref_468()
	call_c   Dyam_Seed_Start(&ref[4],&ref[468],I(0),fun1,1)
	call_c   build_ref_469()
	call_c   Dyam_Seed_Add_Comp(&ref[469],fun123,0)
	call_c   Dyam_Seed_End()
	move_ret seed[51]
	c_ret

;; TERM 469: '*PROLOG-ITEM*'{top=> unfold('*NEXTALT2*'(_B, _C, _D, _E), _F, _G, (_H :> _I), _J), cont=> _A}
c_code local build_ref_469
	ret_reg &ref[469]
	call_c   build_ref_724()
	call_c   build_ref_466()
	call_c   Dyam_Create_Binary(&ref[724],&ref[466],V(0))
	move_ret ref[469]
	c_ret

;; TERM 466: unfold('*NEXTALT2*'(_B, _C, _D, _E), _F, _G, (_H :> _I), _J)
c_code local build_ref_466
	ret_reg &ref[466]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_818()
	call_c   Dyam_Term_Start(&ref[818],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_725()
	call_c   build_ref_173()
	call_c   Dyam_Term_Start(&ref[725],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(&ref[173])
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret ref[466]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 818: '*NEXTALT2*'
c_code local build_ref_818
	ret_reg &ref[818]
	call_c   Dyam_Create_Atom("*NEXTALT2*")
	move_ret ref[818]
	c_ret

c_code local build_seed_123
	ret_reg &seed[123]
	call_c   build_ref_59()
	call_c   build_ref_474()
	call_c   Dyam_Seed_Start(&ref[59],&ref[474],I(0),fun15,1)
	call_c   build_ref_477()
	call_c   Dyam_Seed_Add_Comp(&ref[477],&ref[474],0)
	call_c   Dyam_Seed_End()
	move_ret seed[123]
	c_ret

;; TERM 477: '*PROLOG-FIRST*'(seed_install(seed{model=> '*CITEM*'(_B, _B), id=> _K, anchor=> _L, subs_comp=> _M, code=> _N, go=> _O, tabule=> _P, schedule=> _Q, subsume=> _R, model_ref=> _S, subs_comp_ref=> _T, c_type=> _U, c_type_ref=> _V, out_env=> _W, appinfo=> _X, tail_flag=> _Y}, 1, _H)) :> '$$HOLE$$'
c_code local build_ref_477
	ret_reg &ref[477]
	call_c   build_ref_476()
	call_c   Dyam_Create_Binary(I(9),&ref[476],I(7))
	move_ret ref[477]
	c_ret

;; TERM 476: '*PROLOG-FIRST*'(seed_install(seed{model=> '*CITEM*'(_B, _B), id=> _K, anchor=> _L, subs_comp=> _M, code=> _N, go=> _O, tabule=> _P, schedule=> _Q, subsume=> _R, model_ref=> _S, subs_comp_ref=> _T, c_type=> _U, c_type_ref=> _V, out_env=> _W, appinfo=> _X, tail_flag=> _Y}, 1, _H))
c_code local build_ref_476
	ret_reg &ref[476]
	call_c   build_ref_5()
	call_c   build_ref_475()
	call_c   Dyam_Create_Unary(&ref[5],&ref[475])
	move_ret ref[476]
	c_ret

;; TERM 475: seed_install(seed{model=> '*CITEM*'(_B, _B), id=> _K, anchor=> _L, subs_comp=> _M, code=> _N, go=> _O, tabule=> _P, schedule=> _Q, subsume=> _R, model_ref=> _S, subs_comp_ref=> _T, c_type=> _U, c_type_ref=> _V, out_env=> _W, appinfo=> _X, tail_flag=> _Y}, 1, _H)
c_code local build_ref_475
	ret_reg &ref[475]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_329()
	call_c   Dyam_Create_Binary(&ref[329],V(1),V(1))
	move_ret R(0)
	call_c   build_ref_748()
	call_c   Dyam_Term_Start(&ref[748],16)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_Arg(V(21))
	call_c   Dyam_Term_Arg(V(22))
	call_c   Dyam_Term_Arg(V(23))
	call_c   Dyam_Term_Arg(V(24))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_747()
	call_c   Dyam_Term_Start(&ref[747],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(N(1))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[475]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 474: '*PROLOG-ITEM*'{top=> seed_install(seed{model=> '*CITEM*'(_B, _B), id=> _K, anchor=> _L, subs_comp=> _M, code=> _N, go=> _O, tabule=> _P, schedule=> _Q, subsume=> _R, model_ref=> _S, subs_comp_ref=> _T, c_type=> _U, c_type_ref=> _V, out_env=> _W, appinfo=> _X, tail_flag=> _Y}, 1, _H), cont=> '$CLOSURE'('$fun'(122, 0, 1178168776), '$TUPPLE'(35125253113324))}
c_code local build_ref_474
	ret_reg &ref[474]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,534249472)
	move_ret R(0)
	call_c   Dyam_Closure_Aux(fun122,R(0))
	move_ret R(0)
	call_c   build_ref_724()
	call_c   build_ref_475()
	call_c   Dyam_Create_Binary(&ref[724],&ref[475],R(0))
	move_ret ref[474]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_122
	ret_reg &seed[122]
	call_c   build_ref_59()
	call_c   build_ref_470()
	call_c   Dyam_Seed_Start(&ref[59],&ref[470],I(0),fun15,1)
	call_c   build_ref_473()
	call_c   Dyam_Seed_Add_Comp(&ref[473],&ref[470],0)
	call_c   Dyam_Seed_End()
	move_ret seed[122]
	c_ret

;; TERM 473: '*PROLOG-FIRST*'(seed_install(seed{model=> ('*RITEM*'(_B, _C) :> _D(_F)(_G)), id=> _A1, anchor=> _B1, subs_comp=> _C1, code=> _D1, go=> _E1, tabule=> _F1, schedule=> _G1, subsume=> variance, model_ref=> _H1, subs_comp_ref=> _I1, c_type=> _J1, c_type_ref=> _K1, out_env=> [_J], appinfo=> _L1, tail_flag=> _M1}, _Z, _I)) :> '$$HOLE$$'
c_code local build_ref_473
	ret_reg &ref[473]
	call_c   build_ref_472()
	call_c   Dyam_Create_Binary(I(9),&ref[472],I(7))
	move_ret ref[473]
	c_ret

;; TERM 472: '*PROLOG-FIRST*'(seed_install(seed{model=> ('*RITEM*'(_B, _C) :> _D(_F)(_G)), id=> _A1, anchor=> _B1, subs_comp=> _C1, code=> _D1, go=> _E1, tabule=> _F1, schedule=> _G1, subsume=> variance, model_ref=> _H1, subs_comp_ref=> _I1, c_type=> _J1, c_type_ref=> _K1, out_env=> [_J], appinfo=> _L1, tail_flag=> _M1}, _Z, _I))
c_code local build_ref_472
	ret_reg &ref[472]
	call_c   build_ref_5()
	call_c   build_ref_471()
	call_c   Dyam_Create_Unary(&ref[5],&ref[471])
	move_ret ref[472]
	c_ret

;; TERM 471: seed_install(seed{model=> ('*RITEM*'(_B, _C) :> _D(_F)(_G)), id=> _A1, anchor=> _B1, subs_comp=> _C1, code=> _D1, go=> _E1, tabule=> _F1, schedule=> _G1, subsume=> variance, model_ref=> _H1, subs_comp_ref=> _I1, c_type=> _J1, c_type_ref=> _K1, out_env=> [_J], appinfo=> _L1, tail_flag=> _M1}, _Z, _I)
c_code local build_ref_471
	ret_reg &ref[471]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   Dyam_Create_Binary(I(3),V(3),V(5))
	move_ret R(0)
	call_c   Dyam_Create_Binary(I(3),R(0),V(6))
	move_ret R(0)
	call_c   build_ref_408()
	call_c   Dyam_Create_Binary(I(9),&ref[408],R(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(9),I(0))
	move_ret R(1)
	call_c   build_ref_748()
	call_c   build_ref_819()
	call_c   Dyam_Term_Start(&ref[748],16)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(26))
	call_c   Dyam_Term_Arg(V(27))
	call_c   Dyam_Term_Arg(V(28))
	call_c   Dyam_Term_Arg(V(29))
	call_c   Dyam_Term_Arg(V(30))
	call_c   Dyam_Term_Arg(V(31))
	call_c   Dyam_Term_Arg(V(32))
	call_c   Dyam_Term_Arg(&ref[819])
	call_c   Dyam_Term_Arg(V(33))
	call_c   Dyam_Term_Arg(V(34))
	call_c   Dyam_Term_Arg(V(35))
	call_c   Dyam_Term_Arg(V(36))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(37))
	call_c   Dyam_Term_Arg(V(38))
	call_c   Dyam_Term_End()
	move_ret R(1)
	call_c   build_ref_747()
	call_c   Dyam_Term_Start(&ref[747],3)
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(25))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_End()
	move_ret ref[471]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 819: variance
c_code local build_ref_819
	ret_reg &ref[819]
	call_c   Dyam_Create_Atom("variance")
	move_ret ref[819]
	c_ret

;; TERM 470: '*PROLOG-ITEM*'{top=> seed_install(seed{model=> ('*RITEM*'(_B, _C) :> _D(_F)(_G)), id=> _A1, anchor=> _B1, subs_comp=> _C1, code=> _D1, go=> _E1, tabule=> _F1, schedule=> _G1, subsume=> variance, model_ref=> _H1, subs_comp_ref=> _I1, c_type=> _J1, c_type_ref=> _K1, out_env=> [_J], appinfo=> _L1, tail_flag=> _M1}, _Z, _I), cont=> _A}
c_code local build_ref_470
	ret_reg &ref[470]
	call_c   build_ref_724()
	call_c   build_ref_471()
	call_c   Dyam_Create_Binary(&ref[724],&ref[471],V(0))
	move_ret ref[470]
	c_ret

long local pool_fun122[2]=[1,build_seed_122]

pl_code local fun122
	call_c   Dyam_Pool(pool_fun122)
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(4))
	move     N(2), R(2)
	move     0, R(3)
	move     V(25), R(4)
	move     S(5), R(5)
	pl_call  pred_check_tag_3()
	call_c   Dyam_Deallocate()
	pl_jump  fun19(&seed[122],12)

long local pool_fun123[3]=[2,build_ref_469,build_seed_123]

pl_code local fun123
	call_c   Dyam_Pool(pool_fun123)
	call_c   Dyam_Unify_Item(&ref[469])
	fail_ret
	pl_jump  fun19(&seed[123],12)

;; TERM 468: '*PROLOG-FIRST*'(unfold('*NEXTALT2*'(_B, _C, _D, _E), _F, _G, (_H :> _I), _J)) :> []
c_code local build_ref_468
	ret_reg &ref[468]
	call_c   build_ref_467()
	call_c   Dyam_Create_Binary(I(9),&ref[467],I(0))
	move_ret ref[468]
	c_ret

;; TERM 467: '*PROLOG-FIRST*'(unfold('*NEXTALT2*'(_B, _C, _D, _E), _F, _G, (_H :> _I), _J))
c_code local build_ref_467
	ret_reg &ref[467]
	call_c   build_ref_5()
	call_c   build_ref_466()
	call_c   Dyam_Create_Unary(&ref[5],&ref[466])
	move_ret ref[467]
	c_ret

c_code local build_seed_52
	ret_reg &seed[52]
	call_c   build_ref_4()
	call_c   build_ref_480()
	call_c   Dyam_Seed_Start(&ref[4],&ref[480],I(0),fun1,1)
	call_c   build_ref_481()
	call_c   Dyam_Seed_Add_Comp(&ref[481],fun125,0)
	call_c   Dyam_Seed_End()
	move_ret seed[52]
	c_ret

;; TERM 481: '*PROLOG-ITEM*'{top=> unfold('*NEXTALT*'(_B, _C, _D, _E), _F, _G, (_H :> _I), _J), cont=> _A}
c_code local build_ref_481
	ret_reg &ref[481]
	call_c   build_ref_724()
	call_c   build_ref_478()
	call_c   Dyam_Create_Binary(&ref[724],&ref[478],V(0))
	move_ret ref[481]
	c_ret

;; TERM 478: unfold('*NEXTALT*'(_B, _C, _D, _E), _F, _G, (_H :> _I), _J)
c_code local build_ref_478
	ret_reg &ref[478]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_820()
	call_c   Dyam_Term_Start(&ref[820],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_725()
	call_c   build_ref_173()
	call_c   Dyam_Term_Start(&ref[725],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(&ref[173])
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret ref[478]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 820: '*NEXTALT*'
c_code local build_ref_820
	ret_reg &ref[820]
	call_c   Dyam_Create_Atom("*NEXTALT*")
	move_ret ref[820]
	c_ret

c_code local build_seed_125
	ret_reg &seed[125]
	call_c   build_ref_59()
	call_c   build_ref_486()
	call_c   Dyam_Seed_Start(&ref[59],&ref[486],I(0),fun15,1)
	call_c   build_ref_477()
	call_c   Dyam_Seed_Add_Comp(&ref[477],&ref[486],0)
	call_c   Dyam_Seed_End()
	move_ret seed[125]
	c_ret

;; TERM 486: '*PROLOG-ITEM*'{top=> seed_install(seed{model=> '*CITEM*'(_B, _B), id=> _K, anchor=> _L, subs_comp=> _M, code=> _N, go=> _O, tabule=> _P, schedule=> _Q, subsume=> _R, model_ref=> _S, subs_comp_ref=> _T, c_type=> _U, c_type_ref=> _V, out_env=> _W, appinfo=> _X, tail_flag=> _Y}, 1, _H), cont=> '$CLOSURE'('$fun'(124, 0, 1178191820), '$TUPPLE'(35125253113324))}
c_code local build_ref_486
	ret_reg &ref[486]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,534249472)
	move_ret R(0)
	call_c   Dyam_Closure_Aux(fun124,R(0))
	move_ret R(0)
	call_c   build_ref_724()
	call_c   build_ref_475()
	call_c   Dyam_Create_Binary(&ref[724],&ref[475],R(0))
	move_ret ref[486]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_124
	ret_reg &seed[124]
	call_c   build_ref_59()
	call_c   build_ref_482()
	call_c   Dyam_Seed_Start(&ref[59],&ref[482],I(0),fun15,1)
	call_c   build_ref_485()
	call_c   Dyam_Seed_Add_Comp(&ref[485],&ref[482],0)
	call_c   Dyam_Seed_End()
	move_ret seed[124]
	c_ret

;; TERM 485: '*PROLOG-FIRST*'(seed_install(seed{model=> ('*RITEM*'(_B, _C) :> _D(_F)(_G)), id=> _A1, anchor=> _B1, subs_comp=> _C1, code=> _D1, go=> _E1, tabule=> _F1, schedule=> _G1, subsume=> yes, model_ref=> _H1, subs_comp_ref=> _I1, c_type=> _J1, c_type_ref=> _K1, out_env=> [_J], appinfo=> _L1, tail_flag=> _M1}, _Z, _I)) :> '$$HOLE$$'
c_code local build_ref_485
	ret_reg &ref[485]
	call_c   build_ref_484()
	call_c   Dyam_Create_Binary(I(9),&ref[484],I(7))
	move_ret ref[485]
	c_ret

;; TERM 484: '*PROLOG-FIRST*'(seed_install(seed{model=> ('*RITEM*'(_B, _C) :> _D(_F)(_G)), id=> _A1, anchor=> _B1, subs_comp=> _C1, code=> _D1, go=> _E1, tabule=> _F1, schedule=> _G1, subsume=> yes, model_ref=> _H1, subs_comp_ref=> _I1, c_type=> _J1, c_type_ref=> _K1, out_env=> [_J], appinfo=> _L1, tail_flag=> _M1}, _Z, _I))
c_code local build_ref_484
	ret_reg &ref[484]
	call_c   build_ref_5()
	call_c   build_ref_483()
	call_c   Dyam_Create_Unary(&ref[5],&ref[483])
	move_ret ref[484]
	c_ret

;; TERM 483: seed_install(seed{model=> ('*RITEM*'(_B, _C) :> _D(_F)(_G)), id=> _A1, anchor=> _B1, subs_comp=> _C1, code=> _D1, go=> _E1, tabule=> _F1, schedule=> _G1, subsume=> yes, model_ref=> _H1, subs_comp_ref=> _I1, c_type=> _J1, c_type_ref=> _K1, out_env=> [_J], appinfo=> _L1, tail_flag=> _M1}, _Z, _I)
c_code local build_ref_483
	ret_reg &ref[483]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   Dyam_Create_Binary(I(3),V(3),V(5))
	move_ret R(0)
	call_c   Dyam_Create_Binary(I(3),R(0),V(6))
	move_ret R(0)
	call_c   build_ref_408()
	call_c   Dyam_Create_Binary(I(9),&ref[408],R(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(9),I(0))
	move_ret R(1)
	call_c   build_ref_748()
	call_c   build_ref_503()
	call_c   Dyam_Term_Start(&ref[748],16)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(26))
	call_c   Dyam_Term_Arg(V(27))
	call_c   Dyam_Term_Arg(V(28))
	call_c   Dyam_Term_Arg(V(29))
	call_c   Dyam_Term_Arg(V(30))
	call_c   Dyam_Term_Arg(V(31))
	call_c   Dyam_Term_Arg(V(32))
	call_c   Dyam_Term_Arg(&ref[503])
	call_c   Dyam_Term_Arg(V(33))
	call_c   Dyam_Term_Arg(V(34))
	call_c   Dyam_Term_Arg(V(35))
	call_c   Dyam_Term_Arg(V(36))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(37))
	call_c   Dyam_Term_Arg(V(38))
	call_c   Dyam_Term_End()
	move_ret R(1)
	call_c   build_ref_747()
	call_c   Dyam_Term_Start(&ref[747],3)
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(25))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_End()
	move_ret ref[483]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 503: yes
c_code local build_ref_503
	ret_reg &ref[503]
	call_c   Dyam_Create_Atom("yes")
	move_ret ref[503]
	c_ret

;; TERM 482: '*PROLOG-ITEM*'{top=> seed_install(seed{model=> ('*RITEM*'(_B, _C) :> _D(_F)(_G)), id=> _A1, anchor=> _B1, subs_comp=> _C1, code=> _D1, go=> _E1, tabule=> _F1, schedule=> _G1, subsume=> yes, model_ref=> _H1, subs_comp_ref=> _I1, c_type=> _J1, c_type_ref=> _K1, out_env=> [_J], appinfo=> _L1, tail_flag=> _M1}, _Z, _I), cont=> _A}
c_code local build_ref_482
	ret_reg &ref[482]
	call_c   build_ref_724()
	call_c   build_ref_483()
	call_c   Dyam_Create_Binary(&ref[724],&ref[483],V(0))
	move_ret ref[482]
	c_ret

long local pool_fun124[2]=[1,build_seed_124]

pl_code local fun124
	call_c   Dyam_Pool(pool_fun124)
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(4))
	move     N(2), R(2)
	move     0, R(3)
	move     V(25), R(4)
	move     S(5), R(5)
	pl_call  pred_check_tag_3()
	call_c   Dyam_Deallocate()
	pl_jump  fun19(&seed[124],12)

long local pool_fun125[3]=[2,build_ref_481,build_seed_125]

pl_code local fun125
	call_c   Dyam_Pool(pool_fun125)
	call_c   Dyam_Unify_Item(&ref[481])
	fail_ret
	pl_jump  fun19(&seed[125],12)

;; TERM 480: '*PROLOG-FIRST*'(unfold('*NEXTALT*'(_B, _C, _D, _E), _F, _G, (_H :> _I), _J)) :> []
c_code local build_ref_480
	ret_reg &ref[480]
	call_c   build_ref_479()
	call_c   Dyam_Create_Binary(I(9),&ref[479],I(0))
	move_ret ref[480]
	c_ret

;; TERM 479: '*PROLOG-FIRST*'(unfold('*NEXTALT*'(_B, _C, _D, _E), _F, _G, (_H :> _I), _J))
c_code local build_ref_479
	ret_reg &ref[479]
	call_c   build_ref_5()
	call_c   build_ref_478()
	call_c   Dyam_Create_Unary(&ref[5],&ref[478])
	move_ret ref[479]
	c_ret

c_code local build_seed_53
	ret_reg &seed[53]
	call_c   build_ref_4()
	call_c   build_ref_489()
	call_c   Dyam_Seed_Start(&ref[4],&ref[489],I(0),fun1,1)
	call_c   build_ref_490()
	call_c   Dyam_Seed_Add_Comp(&ref[490],fun127,0)
	call_c   Dyam_Seed_End()
	move_ret seed[53]
	c_ret

;; TERM 490: '*PROLOG-ITEM*'{top=> unfold('*NEXT*'{nabla=> _B, call=> _C, ret=> _D, right=> _E}, _F, _G, (_H :> _I), _J), cont=> _A}
c_code local build_ref_490
	ret_reg &ref[490]
	call_c   build_ref_724()
	call_c   build_ref_487()
	call_c   Dyam_Create_Binary(&ref[724],&ref[487],V(0))
	move_ret ref[490]
	c_ret

;; TERM 487: unfold('*NEXT*'{nabla=> _B, call=> _C, ret=> _D, right=> _E}, _F, _G, (_H :> _I), _J)
c_code local build_ref_487
	ret_reg &ref[487]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_821()
	call_c   Dyam_Term_Start(&ref[821],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_725()
	call_c   build_ref_173()
	call_c   Dyam_Term_Start(&ref[725],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(&ref[173])
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret ref[487]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 821: '*NEXT*'!'$ft'
c_code local build_ref_821
	ret_reg &ref[821]
	call_c   build_ref_822()
	call_c   Dyam_Create_Atom_Module("$ft",&ref[822])
	move_ret ref[821]
	c_ret

;; TERM 822: '*NEXT*'
c_code local build_ref_822
	ret_reg &ref[822]
	call_c   Dyam_Create_Atom("*NEXT*")
	move_ret ref[822]
	c_ret

c_code local build_seed_127
	ret_reg &seed[127]
	call_c   build_ref_59()
	call_c   build_ref_495()
	call_c   Dyam_Seed_Start(&ref[59],&ref[495],I(0),fun15,1)
	call_c   build_ref_498()
	call_c   Dyam_Seed_Add_Comp(&ref[498],&ref[495],0)
	call_c   Dyam_Seed_End()
	move_ret seed[127]
	c_ret

;; TERM 498: '*PROLOG-FIRST*'(seed_install(seed{model=> '*CITEM*'(_C, _C), id=> _K, anchor=> _L, subs_comp=> _M, code=> _N, go=> _O, tabule=> _P, schedule=> _Q, subsume=> _R, model_ref=> _S, subs_comp_ref=> _T, c_type=> _U, c_type_ref=> _V, out_env=> _W, appinfo=> _X, tail_flag=> _Y}, 1, _H)) :> '$$HOLE$$'
c_code local build_ref_498
	ret_reg &ref[498]
	call_c   build_ref_497()
	call_c   Dyam_Create_Binary(I(9),&ref[497],I(7))
	move_ret ref[498]
	c_ret

;; TERM 497: '*PROLOG-FIRST*'(seed_install(seed{model=> '*CITEM*'(_C, _C), id=> _K, anchor=> _L, subs_comp=> _M, code=> _N, go=> _O, tabule=> _P, schedule=> _Q, subsume=> _R, model_ref=> _S, subs_comp_ref=> _T, c_type=> _U, c_type_ref=> _V, out_env=> _W, appinfo=> _X, tail_flag=> _Y}, 1, _H))
c_code local build_ref_497
	ret_reg &ref[497]
	call_c   build_ref_5()
	call_c   build_ref_496()
	call_c   Dyam_Create_Unary(&ref[5],&ref[496])
	move_ret ref[497]
	c_ret

;; TERM 496: seed_install(seed{model=> '*CITEM*'(_C, _C), id=> _K, anchor=> _L, subs_comp=> _M, code=> _N, go=> _O, tabule=> _P, schedule=> _Q, subsume=> _R, model_ref=> _S, subs_comp_ref=> _T, c_type=> _U, c_type_ref=> _V, out_env=> _W, appinfo=> _X, tail_flag=> _Y}, 1, _H)
c_code local build_ref_496
	ret_reg &ref[496]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_329()
	call_c   Dyam_Create_Binary(&ref[329],V(2),V(2))
	move_ret R(0)
	call_c   build_ref_748()
	call_c   Dyam_Term_Start(&ref[748],16)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_Arg(V(21))
	call_c   Dyam_Term_Arg(V(22))
	call_c   Dyam_Term_Arg(V(23))
	call_c   Dyam_Term_Arg(V(24))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_747()
	call_c   Dyam_Term_Start(&ref[747],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(N(1))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[496]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 495: '*PROLOG-ITEM*'{top=> seed_install(seed{model=> '*CITEM*'(_C, _C), id=> _K, anchor=> _L, subs_comp=> _M, code=> _N, go=> _O, tabule=> _P, schedule=> _Q, subsume=> _R, model_ref=> _S, subs_comp_ref=> _T, c_type=> _U, c_type_ref=> _V, out_env=> _W, appinfo=> _X, tail_flag=> _Y}, 1, _H), cont=> '$CLOSURE'('$fun'(126, 0, 1178201112), '$TUPPLE'(35125253113324))}
c_code local build_ref_495
	ret_reg &ref[495]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,534249472)
	move_ret R(0)
	call_c   Dyam_Closure_Aux(fun126,R(0))
	move_ret R(0)
	call_c   build_ref_724()
	call_c   build_ref_496()
	call_c   Dyam_Create_Binary(&ref[724],&ref[496],R(0))
	move_ret ref[495]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_126
	ret_reg &seed[126]
	call_c   build_ref_59()
	call_c   build_ref_491()
	call_c   Dyam_Seed_Start(&ref[59],&ref[491],I(0),fun15,1)
	call_c   build_ref_494()
	call_c   Dyam_Seed_Add_Comp(&ref[494],&ref[491],0)
	call_c   Dyam_Seed_End()
	move_ret seed[126]
	c_ret

;; TERM 494: '*PROLOG-FIRST*'(seed_install(seed{model=> ('*RITEM*'(_C, _D) :> _E(_F)(_G)), id=> _A1, anchor=> _B1, subs_comp=> _C1, code=> _D1, go=> _E1, tabule=> _F1, schedule=> _G1, subsume=> _H1, model_ref=> _I1, subs_comp_ref=> _J1, c_type=> _K1, c_type_ref=> _L1, out_env=> [_J], appinfo=> _M1, tail_flag=> _N1}, _Z, _I)) :> '$$HOLE$$'
c_code local build_ref_494
	ret_reg &ref[494]
	call_c   build_ref_493()
	call_c   Dyam_Create_Binary(I(9),&ref[493],I(7))
	move_ret ref[494]
	c_ret

;; TERM 493: '*PROLOG-FIRST*'(seed_install(seed{model=> ('*RITEM*'(_C, _D) :> _E(_F)(_G)), id=> _A1, anchor=> _B1, subs_comp=> _C1, code=> _D1, go=> _E1, tabule=> _F1, schedule=> _G1, subsume=> _H1, model_ref=> _I1, subs_comp_ref=> _J1, c_type=> _K1, c_type_ref=> _L1, out_env=> [_J], appinfo=> _M1, tail_flag=> _N1}, _Z, _I))
c_code local build_ref_493
	ret_reg &ref[493]
	call_c   build_ref_5()
	call_c   build_ref_492()
	call_c   Dyam_Create_Unary(&ref[5],&ref[492])
	move_ret ref[493]
	c_ret

;; TERM 492: seed_install(seed{model=> ('*RITEM*'(_C, _D) :> _E(_F)(_G)), id=> _A1, anchor=> _B1, subs_comp=> _C1, code=> _D1, go=> _E1, tabule=> _F1, schedule=> _G1, subsume=> _H1, model_ref=> _I1, subs_comp_ref=> _J1, c_type=> _K1, c_type_ref=> _L1, out_env=> [_J], appinfo=> _M1, tail_flag=> _N1}, _Z, _I)
c_code local build_ref_492
	ret_reg &ref[492]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_0()
	call_c   Dyam_Create_Binary(&ref[0],V(2),V(3))
	move_ret R(0)
	call_c   Dyam_Create_Binary(I(3),V(4),V(5))
	move_ret R(1)
	call_c   Dyam_Create_Binary(I(3),R(1),V(6))
	move_ret R(1)
	call_c   Dyam_Create_Binary(I(9),R(0),R(1))
	move_ret R(1)
	call_c   Dyam_Create_List(V(9),I(0))
	move_ret R(0)
	call_c   build_ref_748()
	call_c   Dyam_Term_Start(&ref[748],16)
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(26))
	call_c   Dyam_Term_Arg(V(27))
	call_c   Dyam_Term_Arg(V(28))
	call_c   Dyam_Term_Arg(V(29))
	call_c   Dyam_Term_Arg(V(30))
	call_c   Dyam_Term_Arg(V(31))
	call_c   Dyam_Term_Arg(V(32))
	call_c   Dyam_Term_Arg(V(33))
	call_c   Dyam_Term_Arg(V(34))
	call_c   Dyam_Term_Arg(V(35))
	call_c   Dyam_Term_Arg(V(36))
	call_c   Dyam_Term_Arg(V(37))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(38))
	call_c   Dyam_Term_Arg(V(39))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_747()
	call_c   Dyam_Term_Start(&ref[747],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(25))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_End()
	move_ret ref[492]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 491: '*PROLOG-ITEM*'{top=> seed_install(seed{model=> ('*RITEM*'(_C, _D) :> _E(_F)(_G)), id=> _A1, anchor=> _B1, subs_comp=> _C1, code=> _D1, go=> _E1, tabule=> _F1, schedule=> _G1, subsume=> _H1, model_ref=> _I1, subs_comp_ref=> _J1, c_type=> _K1, c_type_ref=> _L1, out_env=> [_J], appinfo=> _M1, tail_flag=> _N1}, _Z, _I), cont=> _A}
c_code local build_ref_491
	ret_reg &ref[491]
	call_c   build_ref_724()
	call_c   build_ref_492()
	call_c   Dyam_Create_Binary(&ref[724],&ref[492],V(0))
	move_ret ref[491]
	c_ret

long local pool_fun126[2]=[1,build_seed_126]

pl_code local fun126
	call_c   Dyam_Pool(pool_fun126)
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(1))
	move     N(2), R(2)
	move     0, R(3)
	move     V(25), R(4)
	move     S(5), R(5)
	pl_call  pred_check_tag_3()
	call_c   Dyam_Deallocate()
	pl_jump  fun19(&seed[126],12)

long local pool_fun127[3]=[2,build_ref_490,build_seed_127]

pl_code local fun127
	call_c   Dyam_Pool(pool_fun127)
	call_c   Dyam_Unify_Item(&ref[490])
	fail_ret
	pl_jump  fun19(&seed[127],12)

;; TERM 489: '*PROLOG-FIRST*'(unfold('*NEXT*'{nabla=> _B, call=> _C, ret=> _D, right=> _E}, _F, _G, (_H :> _I), _J)) :> []
c_code local build_ref_489
	ret_reg &ref[489]
	call_c   build_ref_488()
	call_c   Dyam_Create_Binary(I(9),&ref[488],I(0))
	move_ret ref[489]
	c_ret

;; TERM 488: '*PROLOG-FIRST*'(unfold('*NEXT*'{nabla=> _B, call=> _C, ret=> _D, right=> _E}, _F, _G, (_H :> _I), _J))
c_code local build_ref_488
	ret_reg &ref[488]
	call_c   build_ref_5()
	call_c   build_ref_487()
	call_c   Dyam_Create_Unary(&ref[5],&ref[487])
	move_ret ref[488]
	c_ret

c_code local build_seed_35
	ret_reg &seed[35]
	call_c   build_ref_4()
	call_c   build_ref_512()
	call_c   Dyam_Seed_Start(&ref[4],&ref[512],I(0),fun1,1)
	call_c   build_ref_513()
	call_c   Dyam_Seed_Add_Comp(&ref[513],fun133,0)
	call_c   Dyam_Seed_End()
	move_ret seed[35]
	c_ret

;; TERM 513: '*PROLOG-ITEM*'{top=> unfold('*PROLOG*'{call=> _B, right=> _C}, _D, _E, _F, _G), cont=> _A}
c_code local build_ref_513
	ret_reg &ref[513]
	call_c   build_ref_724()
	call_c   build_ref_510()
	call_c   Dyam_Create_Binary(&ref[724],&ref[510],V(0))
	move_ret ref[513]
	c_ret

;; TERM 510: unfold('*PROLOG*'{call=> _B, right=> _C}, _D, _E, _F, _G)
c_code local build_ref_510
	ret_reg &ref[510]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_823()
	call_c   Dyam_Create_Binary(&ref[823],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_725()
	call_c   Dyam_Term_Start(&ref[725],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[510]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 823: '*PROLOG*'!'$ft'
c_code local build_ref_823
	ret_reg &ref[823]
	call_c   build_ref_824()
	call_c   Dyam_Create_Atom_Module("$ft",&ref[824])
	move_ret ref[823]
	c_ret

;; TERM 824: '*PROLOG*'
c_code local build_ref_824
	ret_reg &ref[824]
	call_c   Dyam_Create_Atom("*PROLOG*")
	move_ret ref[824]
	c_ret

c_code local build_seed_131
	ret_reg &seed[131]
	call_c   build_ref_59()
	call_c   build_ref_523()
	call_c   Dyam_Seed_Start(&ref[59],&ref[523],I(0),fun15,1)
	call_c   build_ref_526()
	call_c   Dyam_Seed_Add_Comp(&ref[526],&ref[523],0)
	call_c   Dyam_Seed_End()
	move_ret seed[131]
	c_ret

;; TERM 526: '*PROLOG-FIRST*'(build_closure(_C, _D, _W, _X, _G)) :> '$$HOLE$$'
c_code local build_ref_526
	ret_reg &ref[526]
	call_c   build_ref_525()
	call_c   Dyam_Create_Binary(I(9),&ref[525],I(7))
	move_ret ref[526]
	c_ret

;; TERM 525: '*PROLOG-FIRST*'(build_closure(_C, _D, _W, _X, _G))
c_code local build_ref_525
	ret_reg &ref[525]
	call_c   build_ref_5()
	call_c   build_ref_524()
	call_c   Dyam_Create_Unary(&ref[5],&ref[524])
	move_ret ref[525]
	c_ret

;; TERM 524: build_closure(_C, _D, _W, _X, _G)
c_code local build_ref_524
	ret_reg &ref[524]
	call_c   build_ref_797()
	call_c   Dyam_Term_Start(&ref[797],5)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(22))
	call_c   Dyam_Term_Arg(V(23))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[524]
	c_ret

;; TERM 523: '*PROLOG-ITEM*'{top=> build_closure(_C, _D, _W, _X, _G), cont=> '$CLOSURE'('$fun'(131, 0, 1178260948), '$TUPPLE'(35125253113640))}
c_code local build_ref_523
	ret_reg &ref[523]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,411041824)
	move_ret R(0)
	call_c   Dyam_Closure_Aux(fun131,R(0))
	move_ret R(0)
	call_c   build_ref_724()
	call_c   build_ref_524()
	call_c   Dyam_Create_Binary(&ref[724],&ref[524],R(0))
	move_ret ref[523]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_130
	ret_reg &seed[130]
	call_c   build_ref_59()
	call_c   build_ref_519()
	call_c   Dyam_Seed_Start(&ref[59],&ref[519],I(0),fun15,1)
	call_c   build_ref_522()
	call_c   Dyam_Seed_Add_Comp(&ref[522],&ref[519],0)
	call_c   Dyam_Seed_End()
	move_ret seed[130]
	c_ret

;; TERM 522: '*PROLOG-FIRST*'(seed_install(seed{model=> '*PROLOG-ITEM*'{top=> _B, cont=> _X}, id=> _Y, anchor=> _Z, subs_comp=> _A1, code=> _B1, go=> _C1, tabule=> _D1, schedule=> _E1, subsume=> _F1, model_ref=> _G1, subs_comp_ref=> _H1, c_type=> _I1, c_type_ref=> _J1, out_env=> _K1, appinfo=> _L1, tail_flag=> _M1}, 12, _F)) :> '$$HOLE$$'
c_code local build_ref_522
	ret_reg &ref[522]
	call_c   build_ref_521()
	call_c   Dyam_Create_Binary(I(9),&ref[521],I(7))
	move_ret ref[522]
	c_ret

;; TERM 521: '*PROLOG-FIRST*'(seed_install(seed{model=> '*PROLOG-ITEM*'{top=> _B, cont=> _X}, id=> _Y, anchor=> _Z, subs_comp=> _A1, code=> _B1, go=> _C1, tabule=> _D1, schedule=> _E1, subsume=> _F1, model_ref=> _G1, subs_comp_ref=> _H1, c_type=> _I1, c_type_ref=> _J1, out_env=> _K1, appinfo=> _L1, tail_flag=> _M1}, 12, _F))
c_code local build_ref_521
	ret_reg &ref[521]
	call_c   build_ref_5()
	call_c   build_ref_520()
	call_c   Dyam_Create_Unary(&ref[5],&ref[520])
	move_ret ref[521]
	c_ret

;; TERM 520: seed_install(seed{model=> '*PROLOG-ITEM*'{top=> _B, cont=> _X}, id=> _Y, anchor=> _Z, subs_comp=> _A1, code=> _B1, go=> _C1, tabule=> _D1, schedule=> _E1, subsume=> _F1, model_ref=> _G1, subs_comp_ref=> _H1, c_type=> _I1, c_type_ref=> _J1, out_env=> _K1, appinfo=> _L1, tail_flag=> _M1}, 12, _F)
c_code local build_ref_520
	ret_reg &ref[520]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_724()
	call_c   Dyam_Create_Binary(&ref[724],V(1),V(23))
	move_ret R(0)
	call_c   build_ref_748()
	call_c   Dyam_Term_Start(&ref[748],16)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(24))
	call_c   Dyam_Term_Arg(V(25))
	call_c   Dyam_Term_Arg(V(26))
	call_c   Dyam_Term_Arg(V(27))
	call_c   Dyam_Term_Arg(V(28))
	call_c   Dyam_Term_Arg(V(29))
	call_c   Dyam_Term_Arg(V(30))
	call_c   Dyam_Term_Arg(V(31))
	call_c   Dyam_Term_Arg(V(32))
	call_c   Dyam_Term_Arg(V(33))
	call_c   Dyam_Term_Arg(V(34))
	call_c   Dyam_Term_Arg(V(35))
	call_c   Dyam_Term_Arg(V(36))
	call_c   Dyam_Term_Arg(V(37))
	call_c   Dyam_Term_Arg(V(38))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_747()
	call_c   Dyam_Term_Start(&ref[747],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(N(12))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[520]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 519: '*PROLOG-ITEM*'{top=> seed_install(seed{model=> '*PROLOG-ITEM*'{top=> _B, cont=> _X}, id=> _Y, anchor=> _Z, subs_comp=> _A1, code=> _B1, go=> _C1, tabule=> _D1, schedule=> _E1, subsume=> _F1, model_ref=> _G1, subs_comp_ref=> _H1, c_type=> _I1, c_type_ref=> _J1, out_env=> _K1, appinfo=> _L1, tail_flag=> _M1}, 12, _F), cont=> _A}
c_code local build_ref_519
	ret_reg &ref[519]
	call_c   build_ref_724()
	call_c   build_ref_520()
	call_c   Dyam_Create_Binary(&ref[724],&ref[520],V(0))
	move_ret ref[519]
	c_ret

long local pool_fun131[2]=[1,build_seed_130]

pl_code local fun131
	call_c   Dyam_Pool(pool_fun131)
	pl_jump  fun19(&seed[130],12)

long local pool_fun132[2]=[1,build_seed_131]

pl_code local fun132
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Reg_Load(0,V(1))
	call_c   Dyam_Reg_Load(2,V(4))
	move     V(22), R(4)
	move     S(5), R(5)
	pl_call  pred_extend_tupple_3()
	call_c   Dyam_Deallocate()
	pl_jump  fun19(&seed[131],12)

c_code local build_seed_129
	ret_reg &seed[129]
	call_c   build_ref_59()
	call_c   build_ref_515()
	call_c   Dyam_Seed_Start(&ref[59],&ref[515],I(0),fun15,1)
	call_c   build_ref_518()
	call_c   Dyam_Seed_Add_Comp(&ref[518],&ref[515],0)
	call_c   Dyam_Seed_End()
	move_ret seed[129]
	c_ret

;; TERM 518: '*PROLOG-FIRST*'(seed_install(seed{model=> '*PROLOG-ITEM*'{top=> _B, cont=> _D}, id=> _H, anchor=> _I, subs_comp=> _J, code=> _K, go=> _L, tabule=> _M, schedule=> _N, subsume=> _O, model_ref=> _P, subs_comp_ref=> _Q, c_type=> _R, c_type_ref=> _S, out_env=> _T, appinfo=> _U, tail_flag=> _V}, 12, _F)) :> '$$HOLE$$'
c_code local build_ref_518
	ret_reg &ref[518]
	call_c   build_ref_517()
	call_c   Dyam_Create_Binary(I(9),&ref[517],I(7))
	move_ret ref[518]
	c_ret

;; TERM 517: '*PROLOG-FIRST*'(seed_install(seed{model=> '*PROLOG-ITEM*'{top=> _B, cont=> _D}, id=> _H, anchor=> _I, subs_comp=> _J, code=> _K, go=> _L, tabule=> _M, schedule=> _N, subsume=> _O, model_ref=> _P, subs_comp_ref=> _Q, c_type=> _R, c_type_ref=> _S, out_env=> _T, appinfo=> _U, tail_flag=> _V}, 12, _F))
c_code local build_ref_517
	ret_reg &ref[517]
	call_c   build_ref_5()
	call_c   build_ref_516()
	call_c   Dyam_Create_Unary(&ref[5],&ref[516])
	move_ret ref[517]
	c_ret

;; TERM 516: seed_install(seed{model=> '*PROLOG-ITEM*'{top=> _B, cont=> _D}, id=> _H, anchor=> _I, subs_comp=> _J, code=> _K, go=> _L, tabule=> _M, schedule=> _N, subsume=> _O, model_ref=> _P, subs_comp_ref=> _Q, c_type=> _R, c_type_ref=> _S, out_env=> _T, appinfo=> _U, tail_flag=> _V}, 12, _F)
c_code local build_ref_516
	ret_reg &ref[516]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_724()
	call_c   Dyam_Create_Binary(&ref[724],V(1),V(3))
	move_ret R(0)
	call_c   build_ref_748()
	call_c   Dyam_Term_Start(&ref[748],16)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_Arg(V(21))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_747()
	call_c   Dyam_Term_Start(&ref[747],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(N(12))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_End()
	move_ret ref[516]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 515: '*PROLOG-ITEM*'{top=> seed_install(seed{model=> '*PROLOG-ITEM*'{top=> _B, cont=> _D}, id=> _H, anchor=> _I, subs_comp=> _J, code=> _K, go=> _L, tabule=> _M, schedule=> _N, subsume=> _O, model_ref=> _P, subs_comp_ref=> _Q, c_type=> _R, c_type_ref=> _S, out_env=> _T, appinfo=> _U, tail_flag=> _V}, 12, _F), cont=> '$CLOSURE'('$fun'(130, 0, 1178245492), '$TUPPLE'(35125253113576))}
c_code local build_ref_515
	ret_reg &ref[515]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,306184192)
	move_ret R(0)
	call_c   Dyam_Closure_Aux(fun130,R(0))
	move_ret R(0)
	call_c   build_ref_724()
	call_c   build_ref_516()
	call_c   Dyam_Create_Binary(&ref[724],&ref[516],R(0))
	move_ret ref[515]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

pl_code local fun130
	call_c   Dyam_Unify(V(6),V(3))
	fail_ret
	pl_jump  Follow_Cont(V(0))

long local pool_fun133[5]=[65539,build_ref_513,build_ref_514,build_seed_129,pool_fun132]

pl_code local fun133
	call_c   Dyam_Pool(pool_fun133)
	call_c   Dyam_Unify_Item(&ref[513])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun132)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(2),&ref[514])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Deallocate()
	pl_jump  fun19(&seed[129],12)

;; TERM 512: '*PROLOG-FIRST*'(unfold('*PROLOG*'{call=> _B, right=> _C}, _D, _E, _F, _G)) :> []
c_code local build_ref_512
	ret_reg &ref[512]
	call_c   build_ref_511()
	call_c   Dyam_Create_Binary(I(9),&ref[511],I(0))
	move_ret ref[512]
	c_ret

;; TERM 511: '*PROLOG-FIRST*'(unfold('*PROLOG*'{call=> _B, right=> _C}, _D, _E, _F, _G))
c_code local build_ref_511
	ret_reg &ref[511]
	call_c   build_ref_5()
	call_c   build_ref_510()
	call_c   Dyam_Create_Unary(&ref[5],&ref[510])
	move_ret ref[511]
	c_ret

c_code local build_seed_16
	ret_reg &seed[16]
	call_c   build_ref_4()
	call_c   build_ref_501()
	call_c   Dyam_Seed_Start(&ref[4],&ref[501],I(0),fun1,1)
	call_c   build_ref_502()
	call_c   Dyam_Seed_Add_Comp(&ref[502],fun129,0)
	call_c   Dyam_Seed_End()
	move_ret seed[16]
	c_ret

;; TERM 502: '*PROLOG-ITEM*'{top=> app_model(application{item=> '*GUARD*'(_B), trans=> ('*GUARD*'(_B) :> _C), item_comp=> '*GUARD*'(_B), code=> (item_unify(_D) :> allocate :> _E), item_id=> _F, trans_id=> _G, mode=> _H, restrict=> _I, out_env=> _J, key=> _K}), cont=> _A}
c_code local build_ref_502
	ret_reg &ref[502]
	call_c   build_ref_724()
	call_c   build_ref_499()
	call_c   Dyam_Create_Binary(&ref[724],&ref[499],V(0))
	move_ret ref[502]
	c_ret

;; TERM 499: app_model(application{item=> '*GUARD*'(_B), trans=> ('*GUARD*'(_B) :> _C), item_comp=> '*GUARD*'(_B), code=> (item_unify(_D) :> allocate :> _E), item_id=> _F, trans_id=> _G, mode=> _H, restrict=> _I, out_env=> _J, key=> _K})
c_code local build_ref_499
	ret_reg &ref[499]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_789()
	call_c   Dyam_Create_Unary(&ref[789],V(3))
	move_ret R(0)
	call_c   build_ref_299()
	call_c   Dyam_Create_Binary(I(9),&ref[299],V(4))
	move_ret R(1)
	call_c   Dyam_Create_Binary(I(9),R(0),R(1))
	move_ret R(1)
	call_c   build_ref_801()
	call_c   build_ref_504()
	call_c   build_ref_509()
	call_c   Dyam_Term_Start(&ref[801],10)
	call_c   Dyam_Term_Arg(&ref[504])
	call_c   Dyam_Term_Arg(&ref[509])
	call_c   Dyam_Term_Arg(&ref[504])
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_End()
	move_ret R(1)
	call_c   build_ref_813()
	call_c   Dyam_Create_Unary(&ref[813],R(1))
	move_ret ref[499]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 509: '*GUARD*'(_B) :> _C
c_code local build_ref_509
	ret_reg &ref[509]
	call_c   build_ref_504()
	call_c   Dyam_Create_Binary(I(9),&ref[504],V(2))
	move_ret ref[509]
	c_ret

long local pool_fun128[3]=[2,build_ref_338,build_ref_509]

pl_code local fun128
	call_c   Dyam_Update_Choice(fun94)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(7),&ref[338])
	fail_ret
	call_c   Dyam_Cut()
	move     &ref[509], R(0)
	move     S(5), R(1)
	pl_call  pred_holes_1()
	call_c   Dyam_Unify(V(9),I(0))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_jump  Follow_Cont(V(0))

c_code local build_seed_128
	ret_reg &seed[128]
	call_c   build_ref_59()
	call_c   build_ref_505()
	call_c   Dyam_Seed_Start(&ref[59],&ref[505],I(0),fun15,1)
	call_c   build_ref_508()
	call_c   Dyam_Seed_Add_Comp(&ref[508],&ref[505],0)
	call_c   Dyam_Seed_End()
	move_ret seed[128]
	c_ret

;; TERM 508: '*PROLOG-FIRST*'(unfold((_C :> deallocate :> succeed), [], _L, _E, _J)) :> '$$HOLE$$'
c_code local build_ref_508
	ret_reg &ref[508]
	call_c   build_ref_507()
	call_c   Dyam_Create_Binary(I(9),&ref[507],I(7))
	move_ret ref[508]
	c_ret

;; TERM 507: '*PROLOG-FIRST*'(unfold((_C :> deallocate :> succeed), [], _L, _E, _J))
c_code local build_ref_507
	ret_reg &ref[507]
	call_c   build_ref_5()
	call_c   build_ref_506()
	call_c   Dyam_Create_Unary(&ref[5],&ref[506])
	move_ret ref[507]
	c_ret

;; TERM 506: unfold((_C :> deallocate :> succeed), [], _L, _E, _J)
c_code local build_ref_506
	ret_reg &ref[506]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_302()
	call_c   Dyam_Create_Binary(I(9),V(2),&ref[302])
	move_ret R(0)
	call_c   build_ref_725()
	call_c   Dyam_Term_Start(&ref[725],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(I(0))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_End()
	move_ret ref[506]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 505: '*PROLOG-ITEM*'{top=> unfold((_C :> deallocate :> succeed), [], _L, _E, _J), cont=> _A}
c_code local build_ref_505
	ret_reg &ref[505]
	call_c   build_ref_724()
	call_c   build_ref_506()
	call_c   Dyam_Create_Binary(&ref[724],&ref[506],V(0))
	move_ret ref[505]
	c_ret

long local pool_fun129[7]=[65541,build_ref_502,build_ref_331,build_ref_503,build_ref_504,build_seed_128,pool_fun128]

pl_code local fun129
	call_c   Dyam_Pool(pool_fun129)
	call_c   Dyam_Unify_Item(&ref[502])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun128)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(7),&ref[331])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(8),&ref[503])
	fail_ret
	move     &ref[504], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(3))
	pl_call  pred_label_term_2()
	call_c   Dyam_Reg_Load(0,V(1))
	move     V(11), R(2)
	move     S(5), R(3)
	pl_call  pred_tupple_2()
	call_c   Dyam_Deallocate()
	pl_jump  fun19(&seed[128],12)

;; TERM 501: '*PROLOG-FIRST*'(app_model(application{item=> '*GUARD*'(_B), trans=> ('*GUARD*'(_B) :> _C), item_comp=> '*GUARD*'(_B), code=> (item_unify(_D) :> allocate :> _E), item_id=> _F, trans_id=> _G, mode=> _H, restrict=> _I, out_env=> _J, key=> _K})) :> []
c_code local build_ref_501
	ret_reg &ref[501]
	call_c   build_ref_500()
	call_c   Dyam_Create_Binary(I(9),&ref[500],I(0))
	move_ret ref[501]
	c_ret

;; TERM 500: '*PROLOG-FIRST*'(app_model(application{item=> '*GUARD*'(_B), trans=> ('*GUARD*'(_B) :> _C), item_comp=> '*GUARD*'(_B), code=> (item_unify(_D) :> allocate :> _E), item_id=> _F, trans_id=> _G, mode=> _H, restrict=> _I, out_env=> _J, key=> _K}))
c_code local build_ref_500
	ret_reg &ref[500]
	call_c   build_ref_5()
	call_c   build_ref_499()
	call_c   Dyam_Create_Unary(&ref[5],&ref[499])
	move_ret ref[500]
	c_ret

c_code local build_seed_50
	ret_reg &seed[50]
	call_c   build_ref_4()
	call_c   build_ref_529()
	call_c   Dyam_Seed_Start(&ref[4],&ref[529],I(0),fun1,1)
	call_c   build_ref_530()
	call_c   Dyam_Seed_Add_Comp(&ref[530],fun136,0)
	call_c   Dyam_Seed_End()
	move_ret seed[50]
	c_ret

;; TERM 530: '*PROLOG-ITEM*'{top=> unfold('*LIGHTNEXT*'(_B, _C, _D, _E), _F, _G, _H, _I), cont=> _A}
c_code local build_ref_530
	ret_reg &ref[530]
	call_c   build_ref_724()
	call_c   build_ref_527()
	call_c   Dyam_Create_Binary(&ref[724],&ref[527],V(0))
	move_ret ref[530]
	c_ret

;; TERM 527: unfold('*LIGHTNEXT*'(_B, _C, _D, _E), _F, _G, _H, _I)
c_code local build_ref_527
	ret_reg &ref[527]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_825()
	call_c   Dyam_Term_Start(&ref[825],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_725()
	call_c   Dyam_Term_Start(&ref[725],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_End()
	move_ret ref[527]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 825: '*LIGHTNEXT*'
c_code local build_ref_825
	ret_reg &ref[825]
	call_c   Dyam_Create_Atom("*LIGHTNEXT*")
	move_ret ref[825]
	c_ret

c_code local build_seed_133
	ret_reg &seed[133]
	call_c   build_ref_59()
	call_c   build_ref_536()
	call_c   Dyam_Seed_Start(&ref[59],&ref[536],I(0),fun15,1)
	call_c   build_ref_539()
	call_c   Dyam_Seed_Add_Comp(&ref[539],&ref[536],0)
	call_c   Dyam_Seed_End()
	move_ret seed[133]
	c_ret

;; TERM 539: '*PROLOG-FIRST*'(seed_install(seed{model=> '*CITEM*'(_B, _B), id=> _J, anchor=> _K, subs_comp=> _L, code=> _M, go=> _N, tabule=> _O, schedule=> prolog, subsume=> _P, model_ref=> _Q, subs_comp_ref=> _R, c_type=> _S, c_type_ref=> _T, out_env=> _U, appinfo=> _V, tail_flag=> _W}, 1, _X)) :> '$$HOLE$$'
c_code local build_ref_539
	ret_reg &ref[539]
	call_c   build_ref_538()
	call_c   Dyam_Create_Binary(I(9),&ref[538],I(7))
	move_ret ref[539]
	c_ret

;; TERM 538: '*PROLOG-FIRST*'(seed_install(seed{model=> '*CITEM*'(_B, _B), id=> _J, anchor=> _K, subs_comp=> _L, code=> _M, go=> _N, tabule=> _O, schedule=> prolog, subsume=> _P, model_ref=> _Q, subs_comp_ref=> _R, c_type=> _S, c_type_ref=> _T, out_env=> _U, appinfo=> _V, tail_flag=> _W}, 1, _X))
c_code local build_ref_538
	ret_reg &ref[538]
	call_c   build_ref_5()
	call_c   build_ref_537()
	call_c   Dyam_Create_Unary(&ref[5],&ref[537])
	move_ret ref[538]
	c_ret

;; TERM 537: seed_install(seed{model=> '*CITEM*'(_B, _B), id=> _J, anchor=> _K, subs_comp=> _L, code=> _M, go=> _N, tabule=> _O, schedule=> prolog, subsume=> _P, model_ref=> _Q, subs_comp_ref=> _R, c_type=> _S, c_type_ref=> _T, out_env=> _U, appinfo=> _V, tail_flag=> _W}, 1, _X)
c_code local build_ref_537
	ret_reg &ref[537]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_329()
	call_c   Dyam_Create_Binary(&ref[329],V(1),V(1))
	move_ret R(0)
	call_c   build_ref_748()
	call_c   build_ref_770()
	call_c   Dyam_Term_Start(&ref[748],16)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(&ref[770])
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_Arg(V(21))
	call_c   Dyam_Term_Arg(V(22))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_747()
	call_c   Dyam_Term_Start(&ref[747],3)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(N(1))
	call_c   Dyam_Term_Arg(V(23))
	call_c   Dyam_Term_End()
	move_ret ref[537]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 536: '*PROLOG-ITEM*'{top=> seed_install(seed{model=> '*CITEM*'(_B, _B), id=> _J, anchor=> _K, subs_comp=> _L, code=> _M, go=> _N, tabule=> _O, schedule=> prolog, subsume=> _P, model_ref=> _Q, subs_comp_ref=> _R, c_type=> _S, c_type_ref=> _T, out_env=> _U, appinfo=> _V, tail_flag=> _W}, 1, _X), cont=> '$CLOSURE'('$fun'(135, 0, 1178294620), '$TUPPLE'(35125253113796))}
c_code local build_ref_536
	ret_reg &ref[536]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,535822368)
	move_ret R(0)
	call_c   Dyam_Closure_Aux(fun135,R(0))
	move_ret R(0)
	call_c   build_ref_724()
	call_c   build_ref_537()
	call_c   Dyam_Create_Binary(&ref[724],&ref[537],R(0))
	move_ret ref[536]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_132
	ret_reg &seed[132]
	call_c   build_ref_59()
	call_c   build_ref_532()
	call_c   Dyam_Seed_Start(&ref[59],&ref[532],I(0),fun15,1)
	call_c   build_ref_535()
	call_c   Dyam_Seed_Add_Comp(&ref[535],&ref[532],0)
	call_c   Dyam_Seed_End()
	move_ret seed[132]
	c_ret

;; TERM 535: '*PROLOG-FIRST*'(seed_install(seed{model=> ('*LIGHTRITEM*'(_B, _C) :> _D(_F)(_G)), id=> _Z, anchor=> _A1, subs_comp=> _B1, code=> _C1, go=> _D1, tabule=> light, schedule=> prolog, subsume=> _E1, model_ref=> _F1, subs_comp_ref=> _G1, c_type=> _H1, c_type_ref=> _I1, out_env=> [_I], appinfo=> _J1, tail_flag=> _K1}, _Y, _L1)) :> '$$HOLE$$'
c_code local build_ref_535
	ret_reg &ref[535]
	call_c   build_ref_534()
	call_c   Dyam_Create_Binary(I(9),&ref[534],I(7))
	move_ret ref[535]
	c_ret

;; TERM 534: '*PROLOG-FIRST*'(seed_install(seed{model=> ('*LIGHTRITEM*'(_B, _C) :> _D(_F)(_G)), id=> _Z, anchor=> _A1, subs_comp=> _B1, code=> _C1, go=> _D1, tabule=> light, schedule=> prolog, subsume=> _E1, model_ref=> _F1, subs_comp_ref=> _G1, c_type=> _H1, c_type_ref=> _I1, out_env=> [_I], appinfo=> _J1, tail_flag=> _K1}, _Y, _L1))
c_code local build_ref_534
	ret_reg &ref[534]
	call_c   build_ref_5()
	call_c   build_ref_533()
	call_c   Dyam_Create_Unary(&ref[5],&ref[533])
	move_ret ref[534]
	c_ret

;; TERM 533: seed_install(seed{model=> ('*LIGHTRITEM*'(_B, _C) :> _D(_F)(_G)), id=> _Z, anchor=> _A1, subs_comp=> _B1, code=> _C1, go=> _D1, tabule=> light, schedule=> prolog, subsume=> _E1, model_ref=> _F1, subs_comp_ref=> _G1, c_type=> _H1, c_type_ref=> _I1, out_env=> [_I], appinfo=> _J1, tail_flag=> _K1}, _Y, _L1)
c_code local build_ref_533
	ret_reg &ref[533]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   Dyam_Create_Binary(I(3),V(3),V(5))
	move_ret R(0)
	call_c   Dyam_Create_Binary(I(3),R(0),V(6))
	move_ret R(0)
	call_c   build_ref_398()
	call_c   Dyam_Create_Binary(I(9),&ref[398],R(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(8),I(0))
	move_ret R(1)
	call_c   build_ref_748()
	call_c   build_ref_795()
	call_c   build_ref_770()
	call_c   Dyam_Term_Start(&ref[748],16)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(25))
	call_c   Dyam_Term_Arg(V(26))
	call_c   Dyam_Term_Arg(V(27))
	call_c   Dyam_Term_Arg(V(28))
	call_c   Dyam_Term_Arg(V(29))
	call_c   Dyam_Term_Arg(&ref[795])
	call_c   Dyam_Term_Arg(&ref[770])
	call_c   Dyam_Term_Arg(V(30))
	call_c   Dyam_Term_Arg(V(31))
	call_c   Dyam_Term_Arg(V(32))
	call_c   Dyam_Term_Arg(V(33))
	call_c   Dyam_Term_Arg(V(34))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(35))
	call_c   Dyam_Term_Arg(V(36))
	call_c   Dyam_Term_End()
	move_ret R(1)
	call_c   build_ref_747()
	call_c   Dyam_Term_Start(&ref[747],3)
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(24))
	call_c   Dyam_Term_Arg(V(37))
	call_c   Dyam_Term_End()
	move_ret ref[533]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 532: '*PROLOG-ITEM*'{top=> seed_install(seed{model=> ('*LIGHTRITEM*'(_B, _C) :> _D(_F)(_G)), id=> _Z, anchor=> _A1, subs_comp=> _B1, code=> _C1, go=> _D1, tabule=> light, schedule=> prolog, subsume=> _E1, model_ref=> _F1, subs_comp_ref=> _G1, c_type=> _H1, c_type_ref=> _I1, out_env=> [_I], appinfo=> _J1, tail_flag=> _K1}, _Y, _L1), cont=> '$CLOSURE'('$fun'(134, 0, 1178286752), '$TUPPLE'(35125253113756))}
c_code local build_ref_532
	ret_reg &ref[532]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Start_Tupple(0,270532640)
	call_c   Dyam_Almost_End_Tupple(29,1048576)
	move_ret R(0)
	call_c   Dyam_Closure_Aux(fun134,R(0))
	move_ret R(0)
	call_c   build_ref_724()
	call_c   build_ref_533()
	call_c   Dyam_Create_Binary(&ref[724],&ref[533],R(0))
	move_ret ref[532]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 531: _L1 :> deallocate :> succeed
c_code local build_ref_531
	ret_reg &ref[531]
	call_c   build_ref_302()
	call_c   Dyam_Create_Binary(I(9),V(37),&ref[302])
	move_ret ref[531]
	c_ret

pl_code local fun134
	call_c   build_ref_531()
	call_c   Dyam_Allocate(0)
	move     &ref[531], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(23))
	call_c   Dyam_Reg_Load(4,V(7))
	pl_call  pred_choice_code_3()
	call_c   Dyam_Deallocate()
	pl_jump  Follow_Cont(V(0))

long local pool_fun135[2]=[1,build_seed_132]

pl_code local fun135
	call_c   Dyam_Pool(pool_fun135)
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(4))
	move     N(2), R(2)
	move     0, R(3)
	move     V(24), R(4)
	move     S(5), R(5)
	pl_call  pred_check_tag_3()
	call_c   Dyam_Deallocate()
	pl_jump  fun19(&seed[132],12)

long local pool_fun136[3]=[2,build_ref_530,build_seed_133]

pl_code local fun136
	call_c   Dyam_Pool(pool_fun136)
	call_c   Dyam_Unify_Item(&ref[530])
	fail_ret
	pl_jump  fun19(&seed[133],12)

;; TERM 529: '*PROLOG-FIRST*'(unfold('*LIGHTNEXT*'(_B, _C, _D, _E), _F, _G, _H, _I)) :> []
c_code local build_ref_529
	ret_reg &ref[529]
	call_c   build_ref_528()
	call_c   Dyam_Create_Binary(I(9),&ref[528],I(0))
	move_ret ref[529]
	c_ret

;; TERM 528: '*PROLOG-FIRST*'(unfold('*LIGHTNEXT*'(_B, _C, _D, _E), _F, _G, _H, _I))
c_code local build_ref_528
	ret_reg &ref[528]
	call_c   build_ref_5()
	call_c   build_ref_527()
	call_c   Dyam_Create_Unary(&ref[5],&ref[527])
	move_ret ref[528]
	c_ret

c_code local build_seed_27
	ret_reg &seed[27]
	call_c   build_ref_4()
	call_c   build_ref_542()
	call_c   Dyam_Seed_Start(&ref[4],&ref[542],I(0),fun1,1)
	call_c   build_ref_543()
	call_c   Dyam_Seed_Add_Comp(&ref[543],fun138,0)
	call_c   Dyam_Seed_End()
	move_ret seed[27]
	c_ret

;; TERM 543: '*PROLOG-ITEM*'{top=> unfold('*IL-ALT*'(_B, _C, _D, _E), _F, _G, unify(_B, _H), _I), cont=> _A}
c_code local build_ref_543
	ret_reg &ref[543]
	call_c   build_ref_724()
	call_c   build_ref_540()
	call_c   Dyam_Create_Binary(&ref[724],&ref[540],V(0))
	move_ret ref[543]
	c_ret

;; TERM 540: unfold('*IL-ALT*'(_B, _C, _D, _E), _F, _G, unify(_B, _H), _I)
c_code local build_ref_540
	ret_reg &ref[540]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_826()
	call_c   Dyam_Term_Start(&ref[826],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_741()
	call_c   Dyam_Create_Binary(&ref[741],V(1),V(7))
	move_ret R(1)
	call_c   build_ref_725()
	call_c   Dyam_Term_Start(&ref[725],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_End()
	move_ret ref[540]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 826: '*IL-ALT*'
c_code local build_ref_826
	ret_reg &ref[826]
	call_c   Dyam_Create_Atom("*IL-ALT*")
	move_ret ref[826]
	c_ret

c_code local build_seed_134
	ret_reg &seed[134]
	call_c   build_ref_59()
	call_c   build_ref_546()
	call_c   Dyam_Seed_Start(&ref[59],&ref[546],I(0),fun15,1)
	call_c   build_ref_549()
	call_c   Dyam_Seed_Add_Comp(&ref[549],&ref[546],0)
	call_c   Dyam_Seed_End()
	move_ret seed[134]
	c_ret

;; TERM 549: '*PROLOG-FIRST*'(unfold('*IL-THREAD*'(_C, _D), _F, _G, _O, _I)) :> '$$HOLE$$'
c_code local build_ref_549
	ret_reg &ref[549]
	call_c   build_ref_548()
	call_c   Dyam_Create_Binary(I(9),&ref[548],I(7))
	move_ret ref[549]
	c_ret

;; TERM 548: '*PROLOG-FIRST*'(unfold('*IL-THREAD*'(_C, _D), _F, _G, _O, _I))
c_code local build_ref_548
	ret_reg &ref[548]
	call_c   build_ref_5()
	call_c   build_ref_547()
	call_c   Dyam_Create_Unary(&ref[5],&ref[547])
	move_ret ref[548]
	c_ret

;; TERM 547: unfold('*IL-THREAD*'(_C, _D), _F, _G, _O, _I)
c_code local build_ref_547
	ret_reg &ref[547]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_785()
	call_c   Dyam_Create_Binary(&ref[785],V(2),V(3))
	move_ret R(0)
	call_c   build_ref_725()
	call_c   Dyam_Term_Start(&ref[725],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_End()
	move_ret ref[547]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 546: '*PROLOG-ITEM*'{top=> unfold('*IL-THREAD*'(_C, _D), _F, _G, _O, _I), cont=> '$CLOSURE'('$fun'(137, 0, 1178314292), '$TUPPLE'(35125253113928))}
c_code local build_ref_546
	ret_reg &ref[546]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,270581760)
	move_ret R(0)
	call_c   Dyam_Closure_Aux(fun137,R(0))
	move_ret R(0)
	call_c   build_ref_724()
	call_c   build_ref_547()
	call_c   Dyam_Create_Binary(&ref[724],&ref[547],R(0))
	move_ret ref[546]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 545: '$CLOSURE'(_P, _N)
c_code local build_ref_545
	ret_reg &ref[545]
	call_c   build_ref_544()
	call_c   Dyam_Create_Binary(&ref[544],V(15),V(13))
	move_ret ref[545]
	c_ret

pl_code local fun137
	call_c   build_ref_545()
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(14))
	move     V(15), R(2)
	move     S(5), R(3)
	pl_call  pred_label_code_2()
	call_c   Dyam_Unify(V(16),&ref[545])
	fail_ret
	call_c   Dyam_Reg_Load(0,V(16))
	call_c   Dyam_Reg_Load(2,V(7))
	pl_call  pred_label_term_2()
	call_c   Dyam_Deallocate()
	pl_jump  Follow_Cont(V(0))

long local pool_fun138[3]=[2,build_ref_543,build_seed_134]

pl_code local fun138
	call_c   Dyam_Pool(pool_fun138)
	call_c   Dyam_Unify_Item(&ref[543])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(2))
	move     V(9), R(2)
	move     S(5), R(3)
	pl_call  pred_tupple_2()
	call_c   Dyam_Reg_Load(0,V(3))
	call_c   Dyam_Reg_Load(2,V(9))
	move     V(10), R(4)
	move     S(5), R(5)
	pl_call  pred_extend_tupple_3()
	call_c   Dyam_Reg_Load(0,V(6))
	call_c   Dyam_Reg_Load(2,V(10))
	move     V(11), R(4)
	move     S(5), R(5)
	pl_call  pred_inter_3()
	call_c   Dyam_Reg_Load(0,V(11))
	call_c   Dyam_Reg_Load(2,V(4))
	move     V(12), R(4)
	move     S(5), R(5)
	pl_call  pred_tupple_delete_3()
	call_c   Dyam_Reg_Load(0,V(5))
	call_c   Dyam_Reg_Load(2,V(12))
	move     V(13), R(4)
	move     S(5), R(5)
	pl_call  pred_extend_tupple_3()
	call_c   Dyam_Deallocate()
	pl_jump  fun19(&seed[134],12)

;; TERM 542: '*PROLOG-FIRST*'(unfold('*IL-ALT*'(_B, _C, _D, _E), _F, _G, unify(_B, _H), _I)) :> []
c_code local build_ref_542
	ret_reg &ref[542]
	call_c   build_ref_541()
	call_c   Dyam_Create_Binary(I(9),&ref[541],I(0))
	move_ret ref[542]
	c_ret

;; TERM 541: '*PROLOG-FIRST*'(unfold('*IL-ALT*'(_B, _C, _D, _E), _F, _G, unify(_B, _H), _I))
c_code local build_ref_541
	ret_reg &ref[541]
	call_c   build_ref_5()
	call_c   build_ref_540()
	call_c   Dyam_Create_Unary(&ref[5],&ref[540])
	move_ret ref[541]
	c_ret

c_code local build_seed_49
	ret_reg &seed[49]
	call_c   build_ref_4()
	call_c   build_ref_552()
	call_c   Dyam_Seed_Start(&ref[4],&ref[552],I(0),fun1,1)
	call_c   build_ref_553()
	call_c   Dyam_Seed_Add_Comp(&ref[553],fun141,0)
	call_c   Dyam_Seed_End()
	move_ret seed[49]
	c_ret

;; TERM 553: '*PROLOG-ITEM*'{top=> unfold('*LIGHTNEXTALT*'(_B, _C, _D, _E), _F, _G, _H, _I), cont=> _A}
c_code local build_ref_553
	ret_reg &ref[553]
	call_c   build_ref_724()
	call_c   build_ref_550()
	call_c   Dyam_Create_Binary(&ref[724],&ref[550],V(0))
	move_ret ref[553]
	c_ret

;; TERM 550: unfold('*LIGHTNEXTALT*'(_B, _C, _D, _E), _F, _G, _H, _I)
c_code local build_ref_550
	ret_reg &ref[550]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_827()
	call_c   Dyam_Term_Start(&ref[827],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_725()
	call_c   Dyam_Term_Start(&ref[725],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_End()
	move_ret ref[550]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 827: '*LIGHTNEXTALT*'
c_code local build_ref_827
	ret_reg &ref[827]
	call_c   Dyam_Create_Atom("*LIGHTNEXTALT*")
	move_ret ref[827]
	c_ret

c_code local build_seed_136
	ret_reg &seed[136]
	call_c   build_ref_59()
	call_c   build_ref_560()
	call_c   Dyam_Seed_Start(&ref[59],&ref[560],I(0),fun15,1)
	call_c   build_ref_539()
	call_c   Dyam_Seed_Add_Comp(&ref[539],&ref[560],0)
	call_c   Dyam_Seed_End()
	move_ret seed[136]
	c_ret

;; TERM 560: '*PROLOG-ITEM*'{top=> seed_install(seed{model=> '*CITEM*'(_B, _B), id=> _J, anchor=> _K, subs_comp=> _L, code=> _M, go=> _N, tabule=> _O, schedule=> prolog, subsume=> _P, model_ref=> _Q, subs_comp_ref=> _R, c_type=> _S, c_type_ref=> _T, out_env=> _U, appinfo=> _V, tail_flag=> _W}, 1, _X), cont=> '$CLOSURE'('$fun'(140, 0, 1178342916), '$TUPPLE'(35125253113796))}
c_code local build_ref_560
	ret_reg &ref[560]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,535822368)
	move_ret R(0)
	call_c   Dyam_Closure_Aux(fun140,R(0))
	move_ret R(0)
	call_c   build_ref_724()
	call_c   build_ref_537()
	call_c   Dyam_Create_Binary(&ref[724],&ref[537],R(0))
	move_ret ref[560]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_135
	ret_reg &seed[135]
	call_c   build_ref_59()
	call_c   build_ref_556()
	call_c   Dyam_Seed_Start(&ref[59],&ref[556],I(0),fun15,1)
	call_c   build_ref_559()
	call_c   Dyam_Seed_Add_Comp(&ref[559],&ref[556],0)
	call_c   Dyam_Seed_End()
	move_ret seed[135]
	c_ret

;; TERM 559: '*PROLOG-FIRST*'(seed_install(seed{model=> ('*RITEM*'(_B, _C) :> _D(_F)(_G)), id=> _Z, anchor=> _A1, subs_comp=> _B1, code=> _C1, go=> _D1, tabule=> light, schedule=> prolog, subsume=> _E1, model_ref=> _F1, subs_comp_ref=> _G1, c_type=> _H1, c_type_ref=> _I1, out_env=> [_I], appinfo=> _J1, tail_flag=> _K1}, _Y, _L1)) :> '$$HOLE$$'
c_code local build_ref_559
	ret_reg &ref[559]
	call_c   build_ref_558()
	call_c   Dyam_Create_Binary(I(9),&ref[558],I(7))
	move_ret ref[559]
	c_ret

;; TERM 558: '*PROLOG-FIRST*'(seed_install(seed{model=> ('*RITEM*'(_B, _C) :> _D(_F)(_G)), id=> _Z, anchor=> _A1, subs_comp=> _B1, code=> _C1, go=> _D1, tabule=> light, schedule=> prolog, subsume=> _E1, model_ref=> _F1, subs_comp_ref=> _G1, c_type=> _H1, c_type_ref=> _I1, out_env=> [_I], appinfo=> _J1, tail_flag=> _K1}, _Y, _L1))
c_code local build_ref_558
	ret_reg &ref[558]
	call_c   build_ref_5()
	call_c   build_ref_557()
	call_c   Dyam_Create_Unary(&ref[5],&ref[557])
	move_ret ref[558]
	c_ret

;; TERM 557: seed_install(seed{model=> ('*RITEM*'(_B, _C) :> _D(_F)(_G)), id=> _Z, anchor=> _A1, subs_comp=> _B1, code=> _C1, go=> _D1, tabule=> light, schedule=> prolog, subsume=> _E1, model_ref=> _F1, subs_comp_ref=> _G1, c_type=> _H1, c_type_ref=> _I1, out_env=> [_I], appinfo=> _J1, tail_flag=> _K1}, _Y, _L1)
c_code local build_ref_557
	ret_reg &ref[557]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   Dyam_Create_Binary(I(3),V(3),V(5))
	move_ret R(0)
	call_c   Dyam_Create_Binary(I(3),R(0),V(6))
	move_ret R(0)
	call_c   build_ref_408()
	call_c   Dyam_Create_Binary(I(9),&ref[408],R(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(8),I(0))
	move_ret R(1)
	call_c   build_ref_748()
	call_c   build_ref_795()
	call_c   build_ref_770()
	call_c   Dyam_Term_Start(&ref[748],16)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(25))
	call_c   Dyam_Term_Arg(V(26))
	call_c   Dyam_Term_Arg(V(27))
	call_c   Dyam_Term_Arg(V(28))
	call_c   Dyam_Term_Arg(V(29))
	call_c   Dyam_Term_Arg(&ref[795])
	call_c   Dyam_Term_Arg(&ref[770])
	call_c   Dyam_Term_Arg(V(30))
	call_c   Dyam_Term_Arg(V(31))
	call_c   Dyam_Term_Arg(V(32))
	call_c   Dyam_Term_Arg(V(33))
	call_c   Dyam_Term_Arg(V(34))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(35))
	call_c   Dyam_Term_Arg(V(36))
	call_c   Dyam_Term_End()
	move_ret R(1)
	call_c   build_ref_747()
	call_c   Dyam_Term_Start(&ref[747],3)
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(24))
	call_c   Dyam_Term_Arg(V(37))
	call_c   Dyam_Term_End()
	move_ret ref[557]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 556: '*PROLOG-ITEM*'{top=> seed_install(seed{model=> ('*RITEM*'(_B, _C) :> _D(_F)(_G)), id=> _Z, anchor=> _A1, subs_comp=> _B1, code=> _C1, go=> _D1, tabule=> light, schedule=> prolog, subsume=> _E1, model_ref=> _F1, subs_comp_ref=> _G1, c_type=> _H1, c_type_ref=> _I1, out_env=> [_I], appinfo=> _J1, tail_flag=> _K1}, _Y, _L1), cont=> '$CLOSURE'('$fun'(139, 0, 1178334712), '$TUPPLE'(35125253113756))}
c_code local build_ref_556
	ret_reg &ref[556]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Start_Tupple(0,270532640)
	call_c   Dyam_Almost_End_Tupple(29,1048576)
	move_ret R(0)
	call_c   Dyam_Closure_Aux(fun139,R(0))
	move_ret R(0)
	call_c   build_ref_724()
	call_c   build_ref_557()
	call_c   Dyam_Create_Binary(&ref[724],&ref[557],R(0))
	move_ret ref[556]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 554: _L1 :> deallocate_layer :> deallocate :> succeed
c_code local build_ref_554
	ret_reg &ref[554]
	call_c   build_ref_422()
	call_c   Dyam_Create_Binary(I(9),V(37),&ref[422])
	move_ret ref[554]
	c_ret

;; TERM 555: _X :> fail
c_code local build_ref_555
	ret_reg &ref[555]
	call_c   build_ref_424()
	call_c   Dyam_Create_Binary(I(9),V(23),&ref[424])
	move_ret ref[555]
	c_ret

long local pool_fun139[3]=[2,build_ref_554,build_ref_555]

pl_code local fun139
	call_c   Dyam_Pool(pool_fun139)
	call_c   Dyam_Allocate(0)
	move     &ref[554], R(0)
	move     S(5), R(1)
	move     &ref[555], R(2)
	move     S(5), R(3)
	call_c   Dyam_Reg_Load(4,V(7))
	pl_call  pred_choice_code_3()
	call_c   Dyam_Deallocate()
	pl_jump  Follow_Cont(V(0))

long local pool_fun140[2]=[1,build_seed_135]

pl_code local fun140
	call_c   Dyam_Pool(pool_fun140)
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(4))
	move     N(2), R(2)
	move     0, R(3)
	move     V(24), R(4)
	move     S(5), R(5)
	pl_call  pred_check_tag_3()
	call_c   Dyam_Deallocate()
	pl_jump  fun19(&seed[135],12)

long local pool_fun141[3]=[2,build_ref_553,build_seed_136]

pl_code local fun141
	call_c   Dyam_Pool(pool_fun141)
	call_c   Dyam_Unify_Item(&ref[553])
	fail_ret
	pl_jump  fun19(&seed[136],12)

;; TERM 552: '*PROLOG-FIRST*'(unfold('*LIGHTNEXTALT*'(_B, _C, _D, _E), _F, _G, _H, _I)) :> []
c_code local build_ref_552
	ret_reg &ref[552]
	call_c   build_ref_551()
	call_c   Dyam_Create_Binary(I(9),&ref[551],I(0))
	move_ret ref[552]
	c_ret

;; TERM 551: '*PROLOG-FIRST*'(unfold('*LIGHTNEXTALT*'(_B, _C, _D, _E), _F, _G, _H, _I))
c_code local build_ref_551
	ret_reg &ref[551]
	call_c   build_ref_5()
	call_c   build_ref_550()
	call_c   Dyam_Create_Unary(&ref[5],&ref[550])
	move_ret ref[551]
	c_ret

c_code local build_seed_17
	ret_reg &seed[17]
	call_c   build_ref_4()
	call_c   build_ref_563()
	call_c   Dyam_Seed_Start(&ref[4],&ref[563],I(0),fun1,1)
	call_c   build_ref_564()
	call_c   Dyam_Seed_Add_Comp(&ref[564],fun143,0)
	call_c   Dyam_Seed_End()
	move_ret seed[17]
	c_ret

;; TERM 564: '*PROLOG-ITEM*'{top=> app_model(application{item=> '*PROLOG-ITEM*'{top=> _B, cont=> _C}, trans=> ('*PROLOG-FIRST*'(_B) :> _D), item_comp=> '*PROLOG-ITEM*'{top=> _B, cont=> _C}, code=> (item_unify(_E) :> allocate :> _F), item_id=> _G, trans_id=> _H, mode=> _I, restrict=> _J, out_env=> _K, key=> _L}), cont=> _A}
c_code local build_ref_564
	ret_reg &ref[564]
	call_c   build_ref_724()
	call_c   build_ref_561()
	call_c   Dyam_Create_Binary(&ref[724],&ref[561],V(0))
	move_ret ref[564]
	c_ret

;; TERM 561: app_model(application{item=> '*PROLOG-ITEM*'{top=> _B, cont=> _C}, trans=> ('*PROLOG-FIRST*'(_B) :> _D), item_comp=> '*PROLOG-ITEM*'{top=> _B, cont=> _C}, code=> (item_unify(_E) :> allocate :> _F), item_id=> _G, trans_id=> _H, mode=> _I, restrict=> _J, out_env=> _K, key=> _L})
c_code local build_ref_561
	ret_reg &ref[561]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_789()
	call_c   Dyam_Create_Unary(&ref[789],V(4))
	move_ret R(0)
	call_c   build_ref_299()
	call_c   Dyam_Create_Binary(I(9),&ref[299],V(5))
	move_ret R(1)
	call_c   Dyam_Create_Binary(I(9),R(0),R(1))
	move_ret R(1)
	call_c   build_ref_801()
	call_c   build_ref_565()
	call_c   build_ref_571()
	call_c   Dyam_Term_Start(&ref[801],10)
	call_c   Dyam_Term_Arg(&ref[565])
	call_c   Dyam_Term_Arg(&ref[571])
	call_c   Dyam_Term_Arg(&ref[565])
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_End()
	move_ret R(1)
	call_c   build_ref_813()
	call_c   Dyam_Create_Unary(&ref[813],R(1))
	move_ret ref[561]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 571: '*PROLOG-FIRST*'(_B) :> _D
c_code local build_ref_571
	ret_reg &ref[571]
	call_c   build_ref_570()
	call_c   Dyam_Create_Binary(I(9),&ref[570],V(3))
	move_ret ref[571]
	c_ret

;; TERM 570: '*PROLOG-FIRST*'(_B)
c_code local build_ref_570
	ret_reg &ref[570]
	call_c   build_ref_5()
	call_c   Dyam_Create_Unary(&ref[5],V(1))
	move_ret ref[570]
	c_ret

;; TERM 565: '*PROLOG-ITEM*'{top=> _B, cont=> _C}
c_code local build_ref_565
	ret_reg &ref[565]
	call_c   build_ref_724()
	call_c   Dyam_Create_Binary(&ref[724],V(1),V(2))
	move_ret ref[565]
	c_ret

long local pool_fun142[3]=[2,build_ref_338,build_ref_571]

pl_code local fun142
	call_c   Dyam_Update_Choice(fun94)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(8),&ref[338])
	fail_ret
	call_c   Dyam_Cut()
	move     &ref[571], R(0)
	move     S(5), R(1)
	pl_call  pred_holes_1()
	call_c   Dyam_Unify(V(10),I(0))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_jump  Follow_Cont(V(0))

c_code local build_seed_137
	ret_reg &seed[137]
	call_c   build_ref_59()
	call_c   build_ref_566()
	call_c   Dyam_Seed_Start(&ref[59],&ref[566],I(0),fun15,1)
	call_c   build_ref_569()
	call_c   Dyam_Seed_Add_Comp(&ref[569],&ref[566],0)
	call_c   Dyam_Seed_End()
	move_ret seed[137]
	c_ret

;; TERM 569: '*PROLOG-FIRST*'(unfold((_D :> deallocate :> succeed), _C, _M, _F, _K)) :> '$$HOLE$$'
c_code local build_ref_569
	ret_reg &ref[569]
	call_c   build_ref_568()
	call_c   Dyam_Create_Binary(I(9),&ref[568],I(7))
	move_ret ref[569]
	c_ret

;; TERM 568: '*PROLOG-FIRST*'(unfold((_D :> deallocate :> succeed), _C, _M, _F, _K))
c_code local build_ref_568
	ret_reg &ref[568]
	call_c   build_ref_5()
	call_c   build_ref_567()
	call_c   Dyam_Create_Unary(&ref[5],&ref[567])
	move_ret ref[568]
	c_ret

;; TERM 567: unfold((_D :> deallocate :> succeed), _C, _M, _F, _K)
c_code local build_ref_567
	ret_reg &ref[567]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_302()
	call_c   Dyam_Create_Binary(I(9),V(3),&ref[302])
	move_ret R(0)
	call_c   build_ref_725()
	call_c   Dyam_Term_Start(&ref[725],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_End()
	move_ret ref[567]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 566: '*PROLOG-ITEM*'{top=> unfold((_D :> deallocate :> succeed), _C, _M, _F, _K), cont=> _A}
c_code local build_ref_566
	ret_reg &ref[566]
	call_c   build_ref_724()
	call_c   build_ref_567()
	call_c   Dyam_Create_Binary(&ref[724],&ref[567],V(0))
	move_ret ref[566]
	c_ret

long local pool_fun143[7]=[65541,build_ref_564,build_ref_331,build_ref_503,build_ref_565,build_seed_137,pool_fun142]

pl_code local fun143
	call_c   Dyam_Pool(pool_fun143)
	call_c   Dyam_Unify_Item(&ref[564])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun142)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(8),&ref[331])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(2))
	pl_call  pred_zero_var_1()
	call_c   Dyam_Reg_Load(0,V(1))
	move     V(12), R(2)
	move     S(5), R(3)
	pl_call  pred_tupple_2()
	call_c   Dyam_Unify(V(9),&ref[503])
	fail_ret
	move     &ref[565], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(4))
	pl_call  pred_label_term_2()
	call_c   Dyam_Deallocate()
	pl_jump  fun19(&seed[137],12)

;; TERM 563: '*PROLOG-FIRST*'(app_model(application{item=> '*PROLOG-ITEM*'{top=> _B, cont=> _C}, trans=> ('*PROLOG-FIRST*'(_B) :> _D), item_comp=> '*PROLOG-ITEM*'{top=> _B, cont=> _C}, code=> (item_unify(_E) :> allocate :> _F), item_id=> _G, trans_id=> _H, mode=> _I, restrict=> _J, out_env=> _K, key=> _L})) :> []
c_code local build_ref_563
	ret_reg &ref[563]
	call_c   build_ref_562()
	call_c   Dyam_Create_Binary(I(9),&ref[562],I(0))
	move_ret ref[563]
	c_ret

;; TERM 562: '*PROLOG-FIRST*'(app_model(application{item=> '*PROLOG-ITEM*'{top=> _B, cont=> _C}, trans=> ('*PROLOG-FIRST*'(_B) :> _D), item_comp=> '*PROLOG-ITEM*'{top=> _B, cont=> _C}, code=> (item_unify(_E) :> allocate :> _F), item_id=> _G, trans_id=> _H, mode=> _I, restrict=> _J, out_env=> _K, key=> _L}))
c_code local build_ref_562
	ret_reg &ref[562]
	call_c   build_ref_5()
	call_c   build_ref_561()
	call_c   Dyam_Create_Unary(&ref[5],&ref[561])
	move_ret ref[562]
	c_ret

c_code local build_seed_30
	ret_reg &seed[30]
	call_c   build_ref_4()
	call_c   build_ref_574()
	call_c   Dyam_Seed_Start(&ref[4],&ref[574],I(0),fun1,1)
	call_c   build_ref_575()
	call_c   Dyam_Seed_Add_Comp(&ref[575],fun147,0)
	call_c   Dyam_Seed_End()
	move_ret seed[30]
	c_ret

;; TERM 575: '*PROLOG-ITEM*'{top=> unfold('*KLEENE-LAST*'(_B, _C, _D, _E, _F, _G), _H, _I, _J, _K), cont=> _A}
c_code local build_ref_575
	ret_reg &ref[575]
	call_c   build_ref_724()
	call_c   build_ref_572()
	call_c   Dyam_Create_Binary(&ref[724],&ref[572],V(0))
	move_ret ref[575]
	c_ret

;; TERM 572: unfold('*KLEENE-LAST*'(_B, _C, _D, _E, _F, _G), _H, _I, _J, _K)
c_code local build_ref_572
	ret_reg &ref[572]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_828()
	call_c   Dyam_Term_Start(&ref[828],6)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_725()
	call_c   Dyam_Term_Start(&ref[725],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_End()
	move_ret ref[572]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 828: '*KLEENE-LAST*'
c_code local build_ref_828
	ret_reg &ref[828]
	call_c   Dyam_Create_Atom("*KLEENE-LAST*")
	move_ret ref[828]
	c_ret

c_code local build_seed_138
	ret_reg &seed[138]
	call_c   build_ref_222()
	call_c   build_ref_577()
	call_c   Dyam_Seed_Start(&ref[222],&ref[577],I(0),fun15,1)
	call_c   build_ref_578()
	call_c   Dyam_Seed_Add_Comp(&ref[578],&ref[577],0)
	call_c   Dyam_Seed_End()
	move_ret seed[138]
	c_ret

;; TERM 578: '*GUARD*'(append(_C, _N, _O)) :> '$$HOLE$$'
c_code local build_ref_578
	ret_reg &ref[578]
	call_c   build_ref_577()
	call_c   Dyam_Create_Binary(I(9),&ref[577],I(7))
	move_ret ref[578]
	c_ret

;; TERM 577: '*GUARD*'(append(_C, _N, _O))
c_code local build_ref_577
	ret_reg &ref[577]
	call_c   build_ref_222()
	call_c   build_ref_576()
	call_c   Dyam_Create_Unary(&ref[222],&ref[576])
	move_ret ref[577]
	c_ret

;; TERM 576: append(_C, _N, _O)
c_code local build_ref_576
	ret_reg &ref[576]
	call_c   build_ref_829()
	call_c   Dyam_Term_Start(&ref[829],3)
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_End()
	move_ret ref[576]
	c_ret

;; TERM 829: append
c_code local build_ref_829
	ret_reg &ref[829]
	call_c   Dyam_Create_Atom("append")
	move_ret ref[829]
	c_ret

c_code local build_seed_139
	ret_reg &seed[139]
	call_c   build_ref_222()
	call_c   build_ref_580()
	call_c   Dyam_Seed_Start(&ref[222],&ref[580],I(0),fun15,1)
	call_c   build_ref_581()
	call_c   Dyam_Seed_Add_Comp(&ref[581],&ref[580],0)
	call_c   Dyam_Seed_End()
	move_ret seed[139]
	c_ret

;; TERM 581: '*GUARD*'(append(_D, _N, _P)) :> '$$HOLE$$'
c_code local build_ref_581
	ret_reg &ref[581]
	call_c   build_ref_580()
	call_c   Dyam_Create_Binary(I(9),&ref[580],I(7))
	move_ret ref[581]
	c_ret

;; TERM 580: '*GUARD*'(append(_D, _N, _P))
c_code local build_ref_580
	ret_reg &ref[580]
	call_c   build_ref_222()
	call_c   build_ref_579()
	call_c   Dyam_Create_Unary(&ref[222],&ref[579])
	move_ret ref[580]
	c_ret

;; TERM 579: append(_D, _N, _P)
c_code local build_ref_579
	ret_reg &ref[579]
	call_c   build_ref_829()
	call_c   Dyam_Term_Start(&ref[829],3)
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_End()
	move_ret ref[579]
	c_ret

;; TERM 582: [_R|_O]
c_code local build_ref_582
	ret_reg &ref[582]
	call_c   Dyam_Create_List(V(17),V(14))
	move_ret ref[582]
	c_ret

c_code local build_seed_143
	ret_reg &seed[143]
	call_c   build_ref_59()
	call_c   build_ref_594()
	call_c   Dyam_Seed_Start(&ref[59],&ref[594],I(0),fun15,1)
	call_c   build_ref_597()
	call_c   Dyam_Seed_Add_Comp(&ref[597],&ref[594],0)
	call_c   Dyam_Seed_End()
	move_ret seed[143]
	c_ret

;; TERM 597: '*PROLOG-FIRST*'(unfold('*CONT*'(('*KLEENE-CALL*'(_B, _P) :> _G)), _H, _Q, _U, _K)) :> '$$HOLE$$'
c_code local build_ref_597
	ret_reg &ref[597]
	call_c   build_ref_596()
	call_c   Dyam_Create_Binary(I(9),&ref[596],I(7))
	move_ret ref[597]
	c_ret

;; TERM 596: '*PROLOG-FIRST*'(unfold('*CONT*'(('*KLEENE-CALL*'(_B, _P) :> _G)), _H, _Q, _U, _K))
c_code local build_ref_596
	ret_reg &ref[596]
	call_c   build_ref_5()
	call_c   build_ref_595()
	call_c   Dyam_Create_Unary(&ref[5],&ref[595])
	move_ret ref[596]
	c_ret

;; TERM 595: unfold('*CONT*'(('*KLEENE-CALL*'(_B, _P) :> _G)), _H, _Q, _U, _K)
c_code local build_ref_595
	ret_reg &ref[595]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_734()
	call_c   Dyam_Create_Binary(&ref[734],V(1),V(15))
	move_ret R(0)
	call_c   Dyam_Create_Binary(I(9),R(0),V(6))
	move_ret R(0)
	call_c   build_ref_787()
	call_c   Dyam_Create_Unary(&ref[787],R(0))
	move_ret R(0)
	call_c   build_ref_725()
	call_c   Dyam_Term_Start(&ref[725],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_End()
	move_ret ref[595]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 594: '*PROLOG-ITEM*'{top=> unfold('*CONT*'(('*KLEENE-CALL*'(_B, _P) :> _G)), _H, _Q, _U, _K), cont=> '$CLOSURE'('$fun'(145, 0, 1178423244), '$TUPPLE'(35125252598784))}
c_code local build_ref_594
	ret_reg &ref[594]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,271087360)
	move_ret R(0)
	call_c   Dyam_Closure_Aux(fun145,R(0))
	move_ret R(0)
	call_c   build_ref_724()
	call_c   build_ref_595()
	call_c   Dyam_Create_Binary(&ref[724],&ref[595],R(0))
	move_ret ref[594]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_140
	ret_reg &seed[140]
	call_c   build_ref_222()
	call_c   build_ref_585()
	call_c   Dyam_Seed_Start(&ref[222],&ref[585],I(0),fun15,1)
	call_c   build_ref_586()
	call_c   Dyam_Seed_Add_Comp(&ref[586],&ref[585],0)
	call_c   Dyam_Seed_End()
	move_ret seed[140]
	c_ret

;; TERM 586: '*GUARD*'(std_prolog_unif_args([_H|_P], 0, _V, _U, _T)) :> '$$HOLE$$'
c_code local build_ref_586
	ret_reg &ref[586]
	call_c   build_ref_585()
	call_c   Dyam_Create_Binary(I(9),&ref[585],I(7))
	move_ret ref[586]
	c_ret

;; TERM 585: '*GUARD*'(std_prolog_unif_args([_H|_P], 0, _V, _U, _T))
c_code local build_ref_585
	ret_reg &ref[585]
	call_c   build_ref_222()
	call_c   build_ref_584()
	call_c   Dyam_Create_Unary(&ref[222],&ref[584])
	move_ret ref[585]
	c_ret

;; TERM 584: std_prolog_unif_args([_H|_P], 0, _V, _U, _T)
c_code local build_ref_584
	ret_reg &ref[584]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(7),V(15))
	move_ret R(0)
	call_c   build_ref_786()
	call_c   Dyam_Term_Start(&ref[786],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(N(0))
	call_c   Dyam_Term_Arg(V(21))
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_End()
	move_ret ref[584]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_141
	ret_reg &seed[141]
	call_c   build_ref_222()
	call_c   build_ref_588()
	call_c   Dyam_Seed_Start(&ref[222],&ref[588],I(0),fun15,1)
	call_c   build_ref_589()
	call_c   Dyam_Seed_Add_Comp(&ref[589],&ref[588],0)
	call_c   Dyam_Seed_End()
	move_ret seed[141]
	c_ret

;; TERM 589: '*GUARD*'(std_prolog_load_args([_H|_O], 0, _J, (reg_deallocate(_S) :> allocate_layer :> allocate :> _V :> reg_reset(_S)), _Q)) :> '$$HOLE$$'
c_code local build_ref_589
	ret_reg &ref[589]
	call_c   build_ref_588()
	call_c   Dyam_Create_Binary(I(9),&ref[588],I(7))
	move_ret ref[589]
	c_ret

;; TERM 588: '*GUARD*'(std_prolog_load_args([_H|_O], 0, _J, (reg_deallocate(_S) :> allocate_layer :> allocate :> _V :> reg_reset(_S)), _Q))
c_code local build_ref_588
	ret_reg &ref[588]
	call_c   build_ref_222()
	call_c   build_ref_587()
	call_c   Dyam_Create_Unary(&ref[222],&ref[587])
	move_ret ref[588]
	c_ret

;; TERM 587: std_prolog_load_args([_H|_O], 0, _J, (reg_deallocate(_S) :> allocate_layer :> allocate :> _V :> reg_reset(_S)), _Q)
c_code local build_ref_587
	ret_reg &ref[587]
	call_c   Dyam_Pseudo_Choice(3)
	call_c   Dyam_Create_List(V(7),V(14))
	move_ret R(0)
	call_c   build_ref_830()
	call_c   Dyam_Create_Unary(&ref[830],V(18))
	move_ret R(1)
	call_c   build_ref_733()
	call_c   Dyam_Create_Unary(&ref[733],V(18))
	move_ret R(2)
	call_c   Dyam_Create_Binary(I(9),V(21),R(2))
	move_ret R(2)
	call_c   build_ref_299()
	call_c   Dyam_Create_Binary(I(9),&ref[299],R(2))
	move_ret R(2)
	call_c   build_ref_831()
	call_c   Dyam_Create_Binary(I(9),&ref[831],R(2))
	move_ret R(2)
	call_c   Dyam_Create_Binary(I(9),R(1),R(2))
	move_ret R(2)
	call_c   build_ref_778()
	call_c   Dyam_Term_Start(&ref[778],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(N(0))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(R(2))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_End()
	move_ret ref[587]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 831: allocate_layer
c_code local build_ref_831
	ret_reg &ref[831]
	call_c   Dyam_Create_Atom("allocate_layer")
	move_ret ref[831]
	c_ret

;; TERM 830: reg_deallocate
c_code local build_ref_830
	ret_reg &ref[830]
	call_c   Dyam_Create_Atom("reg_deallocate")
	move_ret ref[830]
	c_ret

long local pool_fun144[3]=[2,build_seed_140,build_seed_141]

pl_code local fun145
	call_c   Dyam_Pool(pool_fun144)
	call_c   Dyam_Allocate(0)
fun144:
	pl_call  fun19(&seed[140],1)
	pl_call  fun19(&seed[141],1)
	call_c   Dyam_Deallocate()
	pl_jump  Follow_Cont(V(0))


long local pool_fun146[2]=[1,build_seed_143]

pl_code local fun146
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Deallocate()
	pl_jump  fun19(&seed[143],12)

;; TERM 583: no_empty_kleene_body
c_code local build_ref_583
	ret_reg &ref[583]
	call_c   Dyam_Create_Atom("no_empty_kleene_body")
	move_ret ref[583]
	c_ret

c_code local build_seed_142
	ret_reg &seed[142]
	call_c   build_ref_59()
	call_c   build_ref_590()
	call_c   Dyam_Seed_Start(&ref[59],&ref[590],I(0),fun15,1)
	call_c   build_ref_593()
	call_c   Dyam_Seed_Add_Comp(&ref[593],&ref[590],0)
	call_c   Dyam_Seed_End()
	move_ret seed[142]
	c_ret

;; TERM 593: '*PROLOG-FIRST*'(unfold(('*KLEENE-CALL*'(_B, _P) :> _G), _H, _Q, _U, _K)) :> '$$HOLE$$'
c_code local build_ref_593
	ret_reg &ref[593]
	call_c   build_ref_592()
	call_c   Dyam_Create_Binary(I(9),&ref[592],I(7))
	move_ret ref[593]
	c_ret

;; TERM 592: '*PROLOG-FIRST*'(unfold(('*KLEENE-CALL*'(_B, _P) :> _G), _H, _Q, _U, _K))
c_code local build_ref_592
	ret_reg &ref[592]
	call_c   build_ref_5()
	call_c   build_ref_591()
	call_c   Dyam_Create_Unary(&ref[5],&ref[591])
	move_ret ref[592]
	c_ret

;; TERM 591: unfold(('*KLEENE-CALL*'(_B, _P) :> _G), _H, _Q, _U, _K)
c_code local build_ref_591
	ret_reg &ref[591]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_734()
	call_c   Dyam_Create_Binary(&ref[734],V(1),V(15))
	move_ret R(0)
	call_c   Dyam_Create_Binary(I(9),R(0),V(6))
	move_ret R(0)
	call_c   build_ref_725()
	call_c   Dyam_Term_Start(&ref[725],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_End()
	move_ret ref[591]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 590: '*PROLOG-ITEM*'{top=> unfold(('*KLEENE-CALL*'(_B, _P) :> _G), _H, _Q, _U, _K), cont=> '$CLOSURE'('$fun'(145, 0, 1178423244), '$TUPPLE'(35125252598784))}
c_code local build_ref_590
	ret_reg &ref[590]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,271087360)
	move_ret R(0)
	call_c   Dyam_Closure_Aux(fun145,R(0))
	move_ret R(0)
	call_c   build_ref_724()
	call_c   build_ref_591()
	call_c   Dyam_Create_Binary(&ref[724],&ref[591],R(0))
	move_ret ref[590]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun147[8]=[65542,build_ref_575,build_seed_138,build_seed_139,build_ref_582,build_ref_583,build_seed_142,pool_fun146]

pl_code local fun147
	call_c   Dyam_Pool(pool_fun147)
	call_c   Dyam_Unify_Item(&ref[575])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(8))
	call_c   Dyam_Reg_Load(2,V(5))
	move     V(11), R(4)
	move     S(5), R(5)
	pl_call  pred_inter_3()
	call_c   Dyam_Reg_Load(0,V(4))
	call_c   Dyam_Reg_Load(2,V(11))
	move     V(12), R(4)
	move     S(5), R(5)
	pl_call  pred_union_3()
	call_c   Dyam_Reg_Load(0,V(12))
	move     V(13), R(2)
	move     S(5), R(3)
	pl_call  pred_oset_tupple_2()
	pl_call  fun19(&seed[138],1)
	pl_call  fun19(&seed[139],1)
	call_c   Dyam_Reg_Load(0,V(7))
	call_c   Dyam_Reg_Load(2,V(8))
	move     V(16), R(4)
	move     S(5), R(5)
	pl_call  pred_extend_tupple_3()
	call_c   DYAM_evpred_length(&ref[582],V(18))
	fail_ret
	move     V(19), R(0)
	move     S(5), R(1)
	pl_call  pred_null_tupple_1()
	call_c   Dyam_Choice(fun146)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[583])
	call_c   Dyam_Cut()
	call_c   Dyam_Deallocate()
	pl_jump  fun19(&seed[142],12)

;; TERM 574: '*PROLOG-FIRST*'(unfold('*KLEENE-LAST*'(_B, _C, _D, _E, _F, _G), _H, _I, _J, _K)) :> []
c_code local build_ref_574
	ret_reg &ref[574]
	call_c   build_ref_573()
	call_c   Dyam_Create_Binary(I(9),&ref[573],I(0))
	move_ret ref[574]
	c_ret

;; TERM 573: '*PROLOG-FIRST*'(unfold('*KLEENE-LAST*'(_B, _C, _D, _E, _F, _G), _H, _I, _J, _K))
c_code local build_ref_573
	ret_reg &ref[573]
	call_c   build_ref_5()
	call_c   build_ref_572()
	call_c   Dyam_Create_Unary(&ref[5],&ref[572])
	move_ret ref[573]
	c_ret

c_code local build_seed_70
	ret_reg &seed[70]
	call_c   build_ref_4()
	call_c   build_ref_600()
	call_c   Dyam_Seed_Start(&ref[4],&ref[600],I(0),fun1,1)
	call_c   build_ref_601()
	call_c   Dyam_Seed_Add_Comp(&ref[601],fun151,0)
	call_c   Dyam_Seed_End()
	move_ret seed[70]
	c_ret

;; TERM 601: '*PROLOG-ITEM*'{top=> build_args_closure(_B, callret(_C, _D), _E, _F, '$CLOSURE'(_G, _H), _I), cont=> _A}
c_code local build_ref_601
	ret_reg &ref[601]
	call_c   build_ref_724()
	call_c   build_ref_598()
	call_c   Dyam_Create_Binary(&ref[724],&ref[598],V(0))
	move_ret ref[601]
	c_ret

;; TERM 598: build_args_closure(_B, callret(_C, _D), _E, _F, '$CLOSURE'(_G, _H), _I)
c_code local build_ref_598
	ret_reg &ref[598]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_833()
	call_c   Dyam_Create_Binary(&ref[833],V(2),V(3))
	move_ret R(0)
	call_c   build_ref_544()
	call_c   Dyam_Create_Binary(&ref[544],V(6),V(7))
	move_ret R(1)
	call_c   build_ref_832()
	call_c   Dyam_Term_Start(&ref[832],6)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_End()
	move_ret ref[598]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 832: build_args_closure
c_code local build_ref_832
	ret_reg &ref[832]
	call_c   Dyam_Create_Atom("build_args_closure")
	move_ret ref[832]
	c_ret

;; TERM 833: callret
c_code local build_ref_833
	ret_reg &ref[833]
	call_c   Dyam_Create_Atom("callret")
	move_ret ref[833]
	c_ret

c_code local build_seed_145
	ret_reg &seed[145]
	call_c   build_ref_59()
	call_c   build_ref_607()
	call_c   Dyam_Seed_Start(&ref[59],&ref[607],I(0),fun15,1)
	call_c   build_ref_610()
	call_c   Dyam_Seed_Add_Comp(&ref[610],&ref[607],0)
	call_c   Dyam_Seed_End()
	move_ret seed[145]
	c_ret

;; TERM 610: '*PROLOG-FIRST*'(unfold(_B, _E, _J, _K, _I)) :> '$$HOLE$$'
c_code local build_ref_610
	ret_reg &ref[610]
	call_c   build_ref_609()
	call_c   Dyam_Create_Binary(I(9),&ref[609],I(7))
	move_ret ref[610]
	c_ret

;; TERM 609: '*PROLOG-FIRST*'(unfold(_B, _E, _J, _K, _I))
c_code local build_ref_609
	ret_reg &ref[609]
	call_c   build_ref_5()
	call_c   build_ref_608()
	call_c   Dyam_Create_Unary(&ref[5],&ref[608])
	move_ret ref[609]
	c_ret

;; TERM 608: unfold(_B, _E, _J, _K, _I)
c_code local build_ref_608
	ret_reg &ref[608]
	call_c   build_ref_725()
	call_c   Dyam_Term_Start(&ref[725],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_End()
	move_ret ref[608]
	c_ret

;; TERM 607: '*PROLOG-ITEM*'{top=> unfold(_B, _E, _J, _K, _I), cont=> '$CLOSURE'('$fun'(150, 0, 1178477180), '$TUPPLE'(35125261762620))}
c_code local build_ref_607
	ret_reg &ref[607]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,535035904)
	move_ret R(0)
	call_c   Dyam_Closure_Aux(fun150,R(0))
	move_ret R(0)
	call_c   build_ref_724()
	call_c   build_ref_608()
	call_c   Dyam_Create_Binary(&ref[724],&ref[608],R(0))
	move_ret ref[607]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_144
	ret_reg &seed[144]
	call_c   build_ref_222()
	call_c   build_ref_605()
	call_c   Dyam_Seed_Start(&ref[222],&ref[605],I(0),fun15,1)
	call_c   build_ref_606()
	call_c   Dyam_Seed_Add_Comp(&ref[606],&ref[605],0)
	call_c   Dyam_Seed_End()
	move_ret seed[144]
	c_ret

;; TERM 606: '*GUARD*'(std_prolog_unif_args_alt(_D, 1, _W, _K, _L, _T)) :> '$$HOLE$$'
c_code local build_ref_606
	ret_reg &ref[606]
	call_c   build_ref_605()
	call_c   Dyam_Create_Binary(I(9),&ref[605],I(7))
	move_ret ref[606]
	c_ret

;; TERM 605: '*GUARD*'(std_prolog_unif_args_alt(_D, 1, _W, _K, _L, _T))
c_code local build_ref_605
	ret_reg &ref[605]
	call_c   build_ref_222()
	call_c   build_ref_604()
	call_c   Dyam_Create_Unary(&ref[222],&ref[604])
	move_ret ref[605]
	c_ret

;; TERM 604: std_prolog_unif_args_alt(_D, 1, _W, _K, _L, _T)
c_code local build_ref_604
	ret_reg &ref[604]
	call_c   build_ref_834()
	call_c   Dyam_Term_Start(&ref[834],6)
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(N(1))
	call_c   Dyam_Term_Arg(V(22))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_End()
	move_ret ref[604]
	c_ret

;; TERM 834: std_prolog_unif_args_alt
c_code local build_ref_834
	ret_reg &ref[834]
	call_c   Dyam_Create_Atom("std_prolog_unif_args_alt")
	move_ret ref[834]
	c_ret

;; TERM 603: allocate :> _W :> deallocate :> succeed
c_code local build_ref_603
	ret_reg &ref[603]
	call_c   build_ref_299()
	call_c   build_ref_602()
	call_c   Dyam_Create_Binary(I(9),&ref[299],&ref[602])
	move_ret ref[603]
	c_ret

;; TERM 602: _W :> deallocate :> succeed
c_code local build_ref_602
	ret_reg &ref[602]
	call_c   build_ref_302()
	call_c   Dyam_Create_Binary(I(9),V(22),&ref[302])
	move_ret ref[602]
	c_ret

long local pool_fun149[3]=[2,build_seed_144,build_ref_603]

pl_code local fun149
	call_c   Dyam_Remove_Choice()
	pl_call  fun19(&seed[144],1)
fun148:
	move     &ref[603], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(6))
	pl_call  pred_label_code_2()
	call_c   Dyam_Reg_Load(0,V(4))
	call_c   Dyam_Reg_Load(2,V(21))
	call_c   Dyam_Reg_Load(4,V(7))
	pl_call  pred_extend_tupple_3()
	call_c   Dyam_Deallocate()
	pl_jump  Follow_Cont(V(0))


long local pool_fun150[3]=[65537,build_ref_603,pool_fun149]

pl_code local fun150
	call_c   Dyam_Pool(pool_fun150)
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(4))
	call_c   Dyam_Reg_Load(2,V(5))
	move     V(11), R(4)
	move     S(5), R(5)
	pl_call  pred_extend_tupple_3()
	call_c   Dyam_Reg_Load(0,V(1))
	move     V(12), R(2)
	move     S(5), R(3)
	pl_call  pred_tupple_2()
	call_c   Dyam_Reg_Load(0,V(12))
	call_c   Dyam_Reg_Load(2,V(11))
	move     V(13), R(4)
	move     S(5), R(5)
	pl_call  pred_union_3()
	call_c   Dyam_Reg_Load(0,V(3))
	move     V(14), R(2)
	move     S(5), R(3)
	pl_call  pred_tupple_2()
	call_c   Dyam_Reg_Load(0,V(13))
	call_c   Dyam_Reg_Load(2,V(14))
	move     V(15), R(4)
	move     S(5), R(5)
	pl_call  pred_inter_3()
	call_c   Dyam_Reg_Load(0,V(3))
	move     V(16), R(2)
	move     S(5), R(3)
	pl_call  pred_duplicate_vars_2()
	call_c   Dyam_Reg_Load(0,V(15))
	call_c   Dyam_Reg_Load(2,V(16))
	move     V(17), R(4)
	move     S(5), R(5)
	pl_call  pred_union_3()
	call_c   Dyam_Reg_Load(0,V(2))
	move     V(18), R(2)
	move     S(5), R(3)
	pl_call  pred_tupple_2()
	call_c   Dyam_Reg_Load(0,V(17))
	call_c   Dyam_Reg_Load(2,V(18))
	move     V(19), R(4)
	move     S(5), R(5)
	pl_call  pred_delete_3()
	call_c   Dyam_Reg_Load(0,V(3))
	call_c   Dyam_Reg_Load(2,V(12))
	move     V(20), R(4)
	move     S(5), R(5)
	pl_call  pred_extend_tupple_3()
	call_c   Dyam_Reg_Load(0,V(5))
	call_c   Dyam_Reg_Load(2,V(20))
	move     V(21), R(4)
	move     S(5), R(5)
	pl_call  pred_inter_3()
	call_c   Dyam_Choice(fun149)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Reg_Load(0,V(19))
	pl_call  pred_null_tupple_1()
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(22),V(23))
	fail_ret
	pl_jump  fun148()

long local pool_fun151[3]=[2,build_ref_601,build_seed_145]

pl_code local fun151
	call_c   Dyam_Pool(pool_fun151)
	call_c   Dyam_Unify_Item(&ref[601])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(3))
	call_c   Dyam_Reg_Load(2,V(5))
	move     V(9), R(4)
	move     S(5), R(5)
	pl_call  pred_extend_tupple_3()
	call_c   Dyam_Deallocate()
	pl_jump  fun19(&seed[145],12)

;; TERM 600: '*PROLOG-FIRST*'(build_args_closure(_B, callret(_C, _D), _E, _F, '$CLOSURE'(_G, _H), _I)) :> []
c_code local build_ref_600
	ret_reg &ref[600]
	call_c   build_ref_599()
	call_c   Dyam_Create_Binary(I(9),&ref[599],I(0))
	move_ret ref[600]
	c_ret

;; TERM 599: '*PROLOG-FIRST*'(build_args_closure(_B, callret(_C, _D), _E, _F, '$CLOSURE'(_G, _H), _I))
c_code local build_ref_599
	ret_reg &ref[599]
	call_c   build_ref_5()
	call_c   build_ref_598()
	call_c   Dyam_Create_Unary(&ref[5],&ref[598])
	move_ret ref[599]
	c_ret

c_code local build_seed_32
	ret_reg &seed[32]
	call_c   build_ref_4()
	call_c   build_ref_613()
	call_c   Dyam_Seed_Start(&ref[4],&ref[613],I(0),fun1,1)
	call_c   build_ref_614()
	call_c   Dyam_Seed_Add_Comp(&ref[614],fun157,0)
	call_c   Dyam_Seed_End()
	move_ret seed[32]
	c_ret

;; TERM 614: '*PROLOG-ITEM*'{top=> unfold('*KLEENE*'(_B, _C, _D), _E, _F, _G, _H), cont=> _A}
c_code local build_ref_614
	ret_reg &ref[614]
	call_c   build_ref_724()
	call_c   build_ref_611()
	call_c   Dyam_Create_Binary(&ref[724],&ref[611],V(0))
	move_ret ref[614]
	c_ret

;; TERM 611: unfold('*KLEENE*'(_B, _C, _D), _E, _F, _G, _H)
c_code local build_ref_611
	ret_reg &ref[611]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_835()
	call_c   Dyam_Term_Start(&ref[835],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_725()
	call_c   Dyam_Term_Start(&ref[725],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[611]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 835: '*KLEENE*'
c_code local build_ref_835
	ret_reg &ref[835]
	call_c   Dyam_Create_Atom("*KLEENE*")
	move_ret ref[835]
	c_ret

c_code local build_seed_147
	ret_reg &seed[147]
	call_c   build_ref_59()
	call_c   build_ref_628()
	call_c   Dyam_Seed_Start(&ref[59],&ref[628],I(0),fun15,1)
	call_c   build_ref_625()
	call_c   Dyam_Seed_Add_Comp(&ref[625],&ref[628],0)
	call_c   Dyam_Seed_End()
	move_ret seed[147]
	c_ret

;; TERM 625: '*PROLOG-FIRST*'(unfold(_D, _E, _J, _Q, _H)) :> '$$HOLE$$'
c_code local build_ref_625
	ret_reg &ref[625]
	call_c   build_ref_624()
	call_c   Dyam_Create_Binary(I(9),&ref[624],I(7))
	move_ret ref[625]
	c_ret

;; TERM 624: '*PROLOG-FIRST*'(unfold(_D, _E, _J, _Q, _H))
c_code local build_ref_624
	ret_reg &ref[624]
	call_c   build_ref_5()
	call_c   build_ref_623()
	call_c   Dyam_Create_Unary(&ref[5],&ref[623])
	move_ret ref[624]
	c_ret

;; TERM 623: unfold(_D, _E, _J, _Q, _H)
c_code local build_ref_623
	ret_reg &ref[623]
	call_c   build_ref_725()
	call_c   Dyam_Term_Start(&ref[725],5)
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[623]
	c_ret

;; TERM 628: '*PROLOG-ITEM*'{top=> unfold(_D, _E, _J, _Q, _H), cont=> '$CLOSURE'('$fun'(155, 0, 1178519864), '$TUPPLE'(35125252332692))}
c_code local build_ref_628
	ret_reg &ref[628]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,426250240)
	move_ret R(0)
	call_c   Dyam_Closure_Aux(fun155,R(0))
	move_ret R(0)
	call_c   build_ref_724()
	call_c   build_ref_623()
	call_c   Dyam_Create_Binary(&ref[724],&ref[623],R(0))
	move_ret ref[628]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 627: allocate :> _Q :> deallocate :> succeed
c_code local build_ref_627
	ret_reg &ref[627]
	call_c   build_ref_299()
	call_c   build_ref_626()
	call_c   Dyam_Create_Binary(I(9),&ref[299],&ref[626])
	move_ret ref[627]
	c_ret

;; TERM 626: _Q :> deallocate :> succeed
c_code local build_ref_626
	ret_reg &ref[626]
	call_c   build_ref_302()
	call_c   Dyam_Create_Binary(I(9),V(16),&ref[302])
	move_ret ref[626]
	c_ret

;; TERM 616: kleene(_B, _J, _E, _H)
c_code local build_ref_616
	ret_reg &ref[616]
	call_c   build_ref_836()
	call_c   Dyam_Term_Start(&ref[836],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[616]
	c_ret

;; TERM 836: kleene
c_code local build_ref_836
	ret_reg &ref[836]
	call_c   Dyam_Create_Atom("kleene")
	move_ret ref[836]
	c_ret

;; TERM 615: named_function(_M, _B, local)
c_code local build_ref_615
	ret_reg &ref[615]
	call_c   build_ref_837()
	call_c   build_ref_838()
	call_c   Dyam_Term_Start(&ref[837],3)
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(&ref[838])
	call_c   Dyam_Term_End()
	move_ret ref[615]
	c_ret

;; TERM 838: local
c_code local build_ref_838
	ret_reg &ref[838]
	call_c   Dyam_Create_Atom("local")
	move_ret ref[838]
	c_ret

;; TERM 837: named_function
c_code local build_ref_837
	ret_reg &ref[837]
	call_c   Dyam_Create_Atom("named_function")
	move_ret ref[837]
	c_ret

long local pool_fun155[5]=[4,build_ref_627,build_ref_616,build_ref_615,build_ref_617]

pl_code local fun155
	call_c   Dyam_Pool(pool_fun155)
	call_c   Dyam_Allocate(0)
	move     &ref[627], R(0)
	move     S(5), R(1)
	move     V(12), R(2)
	move     S(5), R(3)
	pl_call  pred_label_code_2()
	call_c   DYAM_evpred_assert_1(&ref[616])
	call_c   DYAM_evpred_assert_1(&ref[615])
	call_c   Dyam_Unify(V(6),&ref[617])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_jump  Follow_Cont(V(0))

long local pool_fun156[2]=[1,build_seed_147]

pl_code local fun156
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Deallocate()
	pl_jump  fun19(&seed[147],12)

c_code local build_seed_146
	ret_reg &seed[146]
	call_c   build_ref_59()
	call_c   build_ref_622()
	call_c   Dyam_Seed_Start(&ref[59],&ref[622],I(0),fun15,1)
	call_c   build_ref_625()
	call_c   Dyam_Seed_Add_Comp(&ref[625],&ref[622],0)
	call_c   Dyam_Seed_End()
	move_ret seed[146]
	c_ret

;; TERM 622: '*PROLOG-ITEM*'{top=> unfold(_D, _E, _J, _Q, _H), cont=> '$CLOSURE'('$fun'(152, 0, 1178503840), '$TUPPLE'(35125252332612))}
c_code local build_ref_622
	ret_reg &ref[622]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,425721856)
	move_ret R(0)
	call_c   Dyam_Closure_Aux(fun152,R(0))
	move_ret R(0)
	call_c   build_ref_724()
	call_c   build_ref_623()
	call_c   Dyam_Create_Binary(&ref[724],&ref[623],R(0))
	move_ret ref[622]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 621: kleene(_B, _E, _H)
c_code local build_ref_621
	ret_reg &ref[621]
	call_c   build_ref_836()
	call_c   Dyam_Term_Start(&ref[836],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[621]
	c_ret

long local pool_fun152[3]=[2,build_ref_621,build_ref_617]

pl_code local fun152
	call_c   Dyam_Pool(pool_fun152)
	call_c   DYAM_evpred_assert_1(&ref[621])
	call_c   Dyam_Unify(V(6),&ref[617])
	fail_ret
	pl_jump  Follow_Cont(V(0))

long local pool_fun153[2]=[1,build_seed_146]

pl_code local fun153
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Deallocate()
	pl_jump  fun19(&seed[146],12)

;; TERM 618: kleene(_B, _J, _N, _H)
c_code local build_ref_618
	ret_reg &ref[618]
	call_c   build_ref_836()
	call_c   Dyam_Term_Start(&ref[836],4)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[618]
	c_ret

;; TERM 620: unify(_O, _P) :> call(_B, [])
c_code local build_ref_620
	ret_reg &ref[620]
	call_c   build_ref_619()
	call_c   build_ref_617()
	call_c   Dyam_Create_Binary(I(9),&ref[619],&ref[617])
	move_ret ref[620]
	c_ret

;; TERM 619: unify(_O, _P)
c_code local build_ref_619
	ret_reg &ref[619]
	call_c   build_ref_741()
	call_c   Dyam_Create_Binary(&ref[741],V(14),V(15))
	move_ret ref[619]
	c_ret

long local pool_fun154[4]=[65538,build_ref_618,build_ref_620,pool_fun153]

pl_code local fun154
	call_c   Dyam_Update_Choice(fun153)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[618])
	call_c   Dyam_Reg_Load(0,V(4))
	call_c   Dyam_Reg_Load(2,V(13))
	pl_call  pred_check_env_renaming_2()
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(4))
	move     V(14), R(2)
	move     S(5), R(3)
	pl_call  pred_label_term_2()
	call_c   Dyam_Reg_Load(0,V(13))
	move     V(15), R(2)
	move     S(5), R(3)
	pl_call  pred_label_term_2()
	call_c   Dyam_Unify(V(6),&ref[620])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_jump  Follow_Cont(V(0))

long local pool_fun157[7]=[131076,build_ref_614,build_ref_615,build_ref_616,build_ref_617,pool_fun156,pool_fun154]

pl_code local fun157
	call_c   Dyam_Pool(pool_fun157)
	call_c   Dyam_Unify_Item(&ref[614])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(2))
	call_c   Dyam_Reg_Load(2,V(5))
	move     V(8), R(4)
	move     S(5), R(5)
	pl_call  pred_union_3()
	call_c   Dyam_Unify(V(8),V(9))
	fail_ret
	call_c   Dyam_Reg_Load(0,V(2))
	move     V(10), R(2)
	move     S(5), R(3)
	pl_call  pred_oset_tupple_2()
	call_c   Dyam_Reg_Load(0,V(5))
	move     V(11), R(2)
	move     S(5), R(3)
	pl_call  pred_oset_tupple_2()
	call_c   Dyam_Choice(fun156)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[615])
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun154)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[616])
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(6),&ref[617])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_jump  Follow_Cont(V(0))

;; TERM 613: '*PROLOG-FIRST*'(unfold('*KLEENE*'(_B, _C, _D), _E, _F, _G, _H)) :> []
c_code local build_ref_613
	ret_reg &ref[613]
	call_c   build_ref_612()
	call_c   Dyam_Create_Binary(I(9),&ref[612],I(0))
	move_ret ref[613]
	c_ret

;; TERM 612: '*PROLOG-FIRST*'(unfold('*KLEENE*'(_B, _C, _D), _E, _F, _G, _H))
c_code local build_ref_612
	ret_reg &ref[612]
	call_c   build_ref_5()
	call_c   build_ref_611()
	call_c   Dyam_Create_Unary(&ref[5],&ref[611])
	move_ret ref[612]
	c_ret

c_code local build_seed_21
	ret_reg &seed[21]
	call_c   build_ref_4()
	call_c   build_ref_631()
	call_c   Dyam_Seed_Start(&ref[4],&ref[631],I(0),fun1,1)
	call_c   build_ref_632()
	call_c   Dyam_Seed_Add_Comp(&ref[632],fun161,0)
	call_c   Dyam_Seed_End()
	move_ret seed[21]
	c_ret

;; TERM 632: '*PROLOG-ITEM*'{top=> app_model(application{item=> '*CITEM*'(_B, _C), trans=> ('*FIRST*'(_D) :> _E), item_comp=> '*CITEM*'(_D, _F), code=> (item_unify(_G) :> allocate :> _H), item_id=> _I, trans_id=> _J, mode=> _K, restrict=> _L, out_env=> _M, key=> key(select, _N, _O)}), cont=> _A}
c_code local build_ref_632
	ret_reg &ref[632]
	call_c   build_ref_724()
	call_c   build_ref_629()
	call_c   Dyam_Create_Binary(&ref[724],&ref[629],V(0))
	move_ret ref[632]
	c_ret

;; TERM 629: app_model(application{item=> '*CITEM*'(_B, _C), trans=> ('*FIRST*'(_D) :> _E), item_comp=> '*CITEM*'(_D, _F), code=> (item_unify(_G) :> allocate :> _H), item_id=> _I, trans_id=> _J, mode=> _K, restrict=> _L, out_env=> _M, key=> key(select, _N, _O)})
c_code local build_ref_629
	ret_reg &ref[629]
	call_c   Dyam_Pseudo_Choice(3)
	call_c   build_ref_329()
	call_c   Dyam_Create_Binary(&ref[329],V(3),V(5))
	move_ret R(0)
	call_c   build_ref_789()
	call_c   Dyam_Create_Unary(&ref[789],V(6))
	move_ret R(1)
	call_c   build_ref_299()
	call_c   Dyam_Create_Binary(I(9),&ref[299],V(7))
	move_ret R(2)
	call_c   Dyam_Create_Binary(I(9),R(1),R(2))
	move_ret R(2)
	call_c   build_ref_792()
	call_c   build_ref_816()
	call_c   Dyam_Term_Start(&ref[792],3)
	call_c   Dyam_Term_Arg(&ref[816])
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_End()
	move_ret R(1)
	call_c   build_ref_801()
	call_c   build_ref_633()
	call_c   build_ref_639()
	call_c   Dyam_Term_Start(&ref[801],10)
	call_c   Dyam_Term_Arg(&ref[633])
	call_c   Dyam_Term_Arg(&ref[639])
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(R(2))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_End()
	move_ret R(1)
	call_c   build_ref_813()
	call_c   Dyam_Create_Unary(&ref[813],R(1))
	move_ret ref[629]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 639: '*FIRST*'(_D) :> _E
c_code local build_ref_639
	ret_reg &ref[639]
	call_c   build_ref_638()
	call_c   Dyam_Create_Binary(I(9),&ref[638],V(4))
	move_ret ref[639]
	c_ret

;; TERM 638: '*FIRST*'(_D)
c_code local build_ref_638
	ret_reg &ref[638]
	call_c   build_ref_325()
	call_c   Dyam_Create_Unary(&ref[325],V(3))
	move_ret ref[638]
	c_ret

;; TERM 640: [_B,_D]
c_code local build_ref_640
	ret_reg &ref[640]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(3),I(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(1),R(0))
	move_ret ref[640]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 641: '$system'(item_k)
c_code local build_ref_641
	ret_reg &ref[641]
	call_c   build_ref_839()
	call_c   build_ref_840()
	call_c   Dyam_Create_Unary(&ref[839],&ref[840])
	move_ret ref[641]
	c_ret

;; TERM 840: item_k
c_code local build_ref_840
	ret_reg &ref[840]
	call_c   Dyam_Create_Atom("item_k")
	move_ret ref[840]
	c_ret

;; TERM 839: '$system'
c_code local build_ref_839
	ret_reg &ref[839]
	call_c   Dyam_Create_Atom("$system")
	move_ret ref[839]
	c_ret

;; TERM 645: unify(_Q, _T, _R, _T) :> unify(_S, _T, _R, _T) :> succeed
c_code local build_ref_645
	ret_reg &ref[645]
	call_c   build_ref_642()
	call_c   build_ref_644()
	call_c   Dyam_Create_Binary(I(9),&ref[642],&ref[644])
	move_ret ref[645]
	c_ret

;; TERM 644: unify(_S, _T, _R, _T) :> succeed
c_code local build_ref_644
	ret_reg &ref[644]
	call_c   build_ref_643()
	call_c   build_ref_301()
	call_c   Dyam_Create_Binary(I(9),&ref[643],&ref[301])
	move_ret ref[644]
	c_ret

;; TERM 643: unify(_S, _T, _R, _T)
c_code local build_ref_643
	ret_reg &ref[643]
	call_c   build_ref_741()
	call_c   Dyam_Term_Start(&ref[741],4)
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_End()
	move_ret ref[643]
	c_ret

;; TERM 642: unify(_Q, _T, _R, _T)
c_code local build_ref_642
	ret_reg &ref[642]
	call_c   build_ref_741()
	call_c   Dyam_Term_Start(&ref[741],4)
	call_c   Dyam_Term_Arg(V(16))
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_Arg(V(17))
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_End()
	move_ret ref[642]
	c_ret

long local pool_fun159[5]=[4,build_ref_640,build_ref_641,build_ref_645,build_ref_639]

pl_code local fun159
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(13),&ref[640])
	fail_ret
	call_c   Dyam_Unify(V(5),V(3))
	fail_ret
	call_c   Dyam_Reg_Load(0,V(1))
	move     V(16), R(2)
	move     S(5), R(3)
	pl_call  pred_label_term_2()
	call_c   Dyam_Reg_Load(0,V(3))
	move     V(17), R(2)
	move     S(5), R(3)
	pl_call  pred_label_term_2()
	call_c   Dyam_Reg_Load(0,V(5))
	move     V(18), R(2)
	move     S(5), R(3)
	pl_call  pred_label_term_2()
	call_c   Dyam_Unify(V(19),&ref[641])
	fail_ret
	move     &ref[645], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(14))
	pl_call  pred_label_code_2()
fun158:
	move     &ref[639], R(0)
	move     S(5), R(1)
	pl_call  pred_holes_1()
	call_c   Dyam_Unify(V(12),I(0))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_jump  Follow_Cont(V(0))


long local pool_fun160[4]=[65538,build_ref_338,build_ref_639,pool_fun159]

pl_code local fun160
	call_c   Dyam_Update_Choice(fun94)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(10),&ref[338])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun159)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(13),I(0))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(1),V(3))
	fail_ret
	call_c   Dyam_Unify(V(2),V(5))
	fail_ret
	pl_jump  fun158()

c_code local build_seed_148
	ret_reg &seed[148]
	call_c   build_ref_59()
	call_c   build_ref_634()
	call_c   Dyam_Seed_Start(&ref[59],&ref[634],I(0),fun15,1)
	call_c   build_ref_637()
	call_c   Dyam_Seed_Add_Comp(&ref[637],&ref[634],0)
	call_c   Dyam_Seed_End()
	move_ret seed[148]
	c_ret

;; TERM 637: '*PROLOG-FIRST*'(unfold((_E :> deallocate :> succeed), _F, _P, _H, _M)) :> '$$HOLE$$'
c_code local build_ref_637
	ret_reg &ref[637]
	call_c   build_ref_636()
	call_c   Dyam_Create_Binary(I(9),&ref[636],I(7))
	move_ret ref[637]
	c_ret

;; TERM 636: '*PROLOG-FIRST*'(unfold((_E :> deallocate :> succeed), _F, _P, _H, _M))
c_code local build_ref_636
	ret_reg &ref[636]
	call_c   build_ref_5()
	call_c   build_ref_635()
	call_c   Dyam_Create_Unary(&ref[5],&ref[635])
	move_ret ref[636]
	c_ret

;; TERM 635: unfold((_E :> deallocate :> succeed), _F, _P, _H, _M)
c_code local build_ref_635
	ret_reg &ref[635]
	call_c   build_ref_725()
	call_c   build_ref_352()
	call_c   Dyam_Term_Start(&ref[725],5)
	call_c   Dyam_Term_Arg(&ref[352])
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_End()
	move_ret ref[635]
	c_ret

;; TERM 634: '*PROLOG-ITEM*'{top=> unfold((_E :> deallocate :> succeed), _F, _P, _H, _M), cont=> _A}
c_code local build_ref_634
	ret_reg &ref[634]
	call_c   build_ref_724()
	call_c   build_ref_635()
	call_c   Dyam_Create_Binary(&ref[724],&ref[635],V(0))
	move_ret ref[634]
	c_ret

long local pool_fun161[6]=[65540,build_ref_632,build_ref_331,build_ref_633,build_seed_148,pool_fun160]

pl_code local fun161
	call_c   Dyam_Pool(pool_fun161)
	call_c   Dyam_Unify_Item(&ref[632])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Choice(fun160)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(10),&ref[331])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(1),V(3))
	fail_ret
	call_c   Dyam_Unify(V(2),V(5))
	fail_ret
	call_c   Dyam_Reg_Load(0,V(5))
	pl_call  pred_zero_var_1()
	call_c   Dyam_Reg_Load(0,V(3))
	move     V(15), R(2)
	move     S(5), R(3)
	pl_call  pred_tupple_2()
	move     &ref[633], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(6))
	pl_call  pred_label_term_2()
	call_c   Dyam_Deallocate()
	pl_jump  fun19(&seed[148],12)

;; TERM 631: '*PROLOG-FIRST*'(app_model(application{item=> '*CITEM*'(_B, _C), trans=> ('*FIRST*'(_D) :> _E), item_comp=> '*CITEM*'(_D, _F), code=> (item_unify(_G) :> allocate :> _H), item_id=> _I, trans_id=> _J, mode=> _K, restrict=> _L, out_env=> _M, key=> key(select, _N, _O)})) :> []
c_code local build_ref_631
	ret_reg &ref[631]
	call_c   build_ref_630()
	call_c   Dyam_Create_Binary(I(9),&ref[630],I(0))
	move_ret ref[631]
	c_ret

;; TERM 630: '*PROLOG-FIRST*'(app_model(application{item=> '*CITEM*'(_B, _C), trans=> ('*FIRST*'(_D) :> _E), item_comp=> '*CITEM*'(_D, _F), code=> (item_unify(_G) :> allocate :> _H), item_id=> _I, trans_id=> _J, mode=> _K, restrict=> _L, out_env=> _M, key=> key(select, _N, _O)}))
c_code local build_ref_630
	ret_reg &ref[630]
	call_c   build_ref_5()
	call_c   build_ref_629()
	call_c   Dyam_Create_Unary(&ref[5],&ref[629])
	move_ret ref[630]
	c_ret

c_code local build_seed_19
	ret_reg &seed[19]
	call_c   build_ref_4()
	call_c   build_ref_648()
	call_c   Dyam_Seed_Start(&ref[4],&ref[648],I(0),fun1,1)
	call_c   build_ref_649()
	call_c   Dyam_Seed_Add_Comp(&ref[649],fun165,0)
	call_c   Dyam_Seed_End()
	move_ret seed[19]
	c_ret

;; TERM 649: '*PROLOG-ITEM*'{top=> app_model(application{item=> '*LIGHTCITEM*'(_B, _C), trans=> ('*LIGHTFIRST*'(_D) :> _E), item_comp=> '*LIGHTCITEM*'(_D, _F), code=> (item_unify(_G) :> allocate :> _H), item_id=> _I, trans_id=> _J, mode=> _K, restrict=> _L, out_env=> _M, key=> key(select, _N, _O)}), cont=> _A}
c_code local build_ref_649
	ret_reg &ref[649]
	call_c   build_ref_724()
	call_c   build_ref_646()
	call_c   Dyam_Create_Binary(&ref[724],&ref[646],V(0))
	move_ret ref[649]
	c_ret

;; TERM 646: app_model(application{item=> '*LIGHTCITEM*'(_B, _C), trans=> ('*LIGHTFIRST*'(_D) :> _E), item_comp=> '*LIGHTCITEM*'(_D, _F), code=> (item_unify(_G) :> allocate :> _H), item_id=> _I, trans_id=> _J, mode=> _K, restrict=> _L, out_env=> _M, key=> key(select, _N, _O)})
c_code local build_ref_646
	ret_reg &ref[646]
	call_c   Dyam_Pseudo_Choice(3)
	call_c   build_ref_841()
	call_c   Dyam_Create_Binary(&ref[841],V(3),V(5))
	move_ret R(0)
	call_c   build_ref_789()
	call_c   Dyam_Create_Unary(&ref[789],V(6))
	move_ret R(1)
	call_c   build_ref_299()
	call_c   Dyam_Create_Binary(I(9),&ref[299],V(7))
	move_ret R(2)
	call_c   Dyam_Create_Binary(I(9),R(1),R(2))
	move_ret R(2)
	call_c   build_ref_792()
	call_c   build_ref_816()
	call_c   Dyam_Term_Start(&ref[792],3)
	call_c   Dyam_Term_Arg(&ref[816])
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_End()
	move_ret R(1)
	call_c   build_ref_801()
	call_c   build_ref_651()
	call_c   build_ref_653()
	call_c   Dyam_Term_Start(&ref[801],10)
	call_c   Dyam_Term_Arg(&ref[651])
	call_c   Dyam_Term_Arg(&ref[653])
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(R(2))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_End()
	move_ret R(1)
	call_c   build_ref_813()
	call_c   Dyam_Create_Unary(&ref[813],R(1))
	move_ret ref[646]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 653: '*LIGHTFIRST*'(_D) :> _E
c_code local build_ref_653
	ret_reg &ref[653]
	call_c   build_ref_652()
	call_c   Dyam_Create_Binary(I(9),&ref[652],V(4))
	move_ret ref[653]
	c_ret

;; TERM 652: '*LIGHTFIRST*'(_D)
c_code local build_ref_652
	ret_reg &ref[652]
	call_c   build_ref_842()
	call_c   Dyam_Create_Unary(&ref[842],V(3))
	move_ret ref[652]
	c_ret

;; TERM 842: '*LIGHTFIRST*'
c_code local build_ref_842
	ret_reg &ref[842]
	call_c   Dyam_Create_Atom("*LIGHTFIRST*")
	move_ret ref[842]
	c_ret

;; TERM 651: '*LIGHTCITEM*'(_B, _C)
c_code local build_ref_651
	ret_reg &ref[651]
	call_c   build_ref_841()
	call_c   Dyam_Create_Binary(&ref[841],V(1),V(2))
	move_ret ref[651]
	c_ret

;; TERM 841: '*LIGHTCITEM*'
c_code local build_ref_841
	ret_reg &ref[841]
	call_c   Dyam_Create_Atom("*LIGHTCITEM*")
	move_ret ref[841]
	c_ret

;; TERM 650: left_corner
c_code local build_ref_650
	ret_reg &ref[650]
	call_c   Dyam_Create_Atom("left_corner")
	move_ret ref[650]
	c_ret

long local pool_fun163[5]=[4,build_ref_640,build_ref_641,build_ref_645,build_ref_653]

pl_code local fun163
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(13),&ref[640])
	fail_ret
	call_c   Dyam_Unify(V(5),V(3))
	fail_ret
	call_c   Dyam_Reg_Load(0,V(1))
	move     V(16), R(2)
	move     S(5), R(3)
	pl_call  pred_label_term_2()
	call_c   Dyam_Reg_Load(0,V(3))
	move     V(17), R(2)
	move     S(5), R(3)
	pl_call  pred_label_term_2()
	call_c   Dyam_Reg_Load(0,V(5))
	move     V(18), R(2)
	move     S(5), R(3)
	pl_call  pred_label_term_2()
	call_c   Dyam_Unify(V(19),&ref[641])
	fail_ret
	move     &ref[645], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(14))
	pl_call  pred_label_code_2()
fun162:
	move     &ref[653], R(0)
	move     S(5), R(1)
	pl_call  pred_holes_1()
	call_c   Dyam_Unify(V(12),I(0))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_jump  Follow_Cont(V(0))


long local pool_fun164[4]=[65538,build_ref_338,build_ref_653,pool_fun163]

pl_code local fun164
	call_c   Dyam_Update_Choice(fun94)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(10),&ref[338])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun163)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(13),I(0))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(1),V(3))
	fail_ret
	call_c   Dyam_Unify(V(2),V(5))
	fail_ret
	pl_jump  fun162()

long local pool_fun165[7]=[65541,build_ref_649,build_ref_650,build_ref_331,build_ref_651,build_seed_148,pool_fun164]

pl_code local fun165
	call_c   Dyam_Pool(pool_fun165)
	call_c   Dyam_Unify_Item(&ref[649])
	fail_ret
	call_c   Dyam_Allocate(0)
	pl_call  Object_1(&ref[650])
	call_c   Dyam_Choice(fun164)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(10),&ref[331])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(1),V(3))
	fail_ret
	call_c   Dyam_Unify(V(2),V(5))
	fail_ret
	call_c   Dyam_Reg_Load(0,V(5))
	pl_call  pred_zero_var_1()
	call_c   Dyam_Reg_Load(0,V(3))
	move     V(15), R(2)
	move     S(5), R(3)
	pl_call  pred_tupple_2()
	move     &ref[651], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(6))
	pl_call  pred_label_term_2()
	call_c   Dyam_Deallocate()
	pl_jump  fun19(&seed[148],12)

;; TERM 648: '*PROLOG-FIRST*'(app_model(application{item=> '*LIGHTCITEM*'(_B, _C), trans=> ('*LIGHTFIRST*'(_D) :> _E), item_comp=> '*LIGHTCITEM*'(_D, _F), code=> (item_unify(_G) :> allocate :> _H), item_id=> _I, trans_id=> _J, mode=> _K, restrict=> _L, out_env=> _M, key=> key(select, _N, _O)})) :> []
c_code local build_ref_648
	ret_reg &ref[648]
	call_c   build_ref_647()
	call_c   Dyam_Create_Binary(I(9),&ref[647],I(0))
	move_ret ref[648]
	c_ret

;; TERM 647: '*PROLOG-FIRST*'(app_model(application{item=> '*LIGHTCITEM*'(_B, _C), trans=> ('*LIGHTFIRST*'(_D) :> _E), item_comp=> '*LIGHTCITEM*'(_D, _F), code=> (item_unify(_G) :> allocate :> _H), item_id=> _I, trans_id=> _J, mode=> _K, restrict=> _L, out_env=> _M, key=> key(select, _N, _O)}))
c_code local build_ref_647
	ret_reg &ref[647]
	call_c   build_ref_5()
	call_c   build_ref_646()
	call_c   Dyam_Create_Unary(&ref[5],&ref[646])
	move_ret ref[647]
	c_ret

c_code local build_seed_66
	ret_reg &seed[66]
	call_c   build_ref_4()
	call_c   build_ref_656()
	call_c   Dyam_Seed_Start(&ref[4],&ref[656],I(0),fun1,1)
	call_c   build_ref_657()
	call_c   Dyam_Seed_Add_Comp(&ref[657],fun174,0)
	call_c   Dyam_Seed_End()
	move_ret seed[66]
	c_ret

;; TERM 657: '*PROLOG-ITEM*'{top=> unfold('*JUMP*'(_B, _C), _D, _E, (_F :> succeed), _G), cont=> _A}
c_code local build_ref_657
	ret_reg &ref[657]
	call_c   build_ref_724()
	call_c   build_ref_654()
	call_c   Dyam_Create_Binary(&ref[724],&ref[654],V(0))
	move_ret ref[657]
	c_ret

;; TERM 654: unfold('*JUMP*'(_B, _C), _D, _E, (_F :> succeed), _G)
c_code local build_ref_654
	ret_reg &ref[654]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_843()
	call_c   Dyam_Create_Binary(&ref[843],V(1),V(2))
	move_ret R(0)
	call_c   build_ref_301()
	call_c   Dyam_Create_Binary(I(9),V(5),&ref[301])
	move_ret R(1)
	call_c   build_ref_725()
	call_c   Dyam_Term_Start(&ref[725],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[654]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 843: '*JUMP*'
c_code local build_ref_843
	ret_reg &ref[843]
	call_c   Dyam_Create_Atom("*JUMP*")
	move_ret ref[843]
	c_ret

;; TERM 665: ['*JUMP*'(_J)|_L]
c_code local build_ref_665
	ret_reg &ref[665]
	call_c   build_ref_658()
	call_c   Dyam_Create_List(&ref[658],V(11))
	move_ret ref[665]
	c_ret

;; TERM 658: '*JUMP*'(_J)
c_code local build_ref_658
	ret_reg &ref[658]
	call_c   build_ref_843()
	call_c   Dyam_Create_Unary(&ref[843],V(9))
	move_ret ref[658]
	c_ret

c_code local build_seed_150
	ret_reg &seed[150]
	call_c   build_ref_59()
	call_c   build_ref_672()
	call_c   Dyam_Seed_Start(&ref[59],&ref[672],I(0),fun15,1)
	call_c   build_ref_675()
	call_c   Dyam_Seed_Add_Comp(&ref[675],&ref[672],0)
	call_c   Dyam_Seed_End()
	move_ret seed[150]
	c_ret

;; TERM 675: '*PROLOG-FIRST*'(unfold(_J, _D, _S, _K, _G)) :> '$$HOLE$$'
c_code local build_ref_675
	ret_reg &ref[675]
	call_c   build_ref_674()
	call_c   Dyam_Create_Binary(I(9),&ref[674],I(7))
	move_ret ref[675]
	c_ret

;; TERM 674: '*PROLOG-FIRST*'(unfold(_J, _D, _S, _K, _G))
c_code local build_ref_674
	ret_reg &ref[674]
	call_c   build_ref_5()
	call_c   build_ref_673()
	call_c   Dyam_Create_Unary(&ref[5],&ref[673])
	move_ret ref[674]
	c_ret

;; TERM 673: unfold(_J, _D, _S, _K, _G)
c_code local build_ref_673
	ret_reg &ref[673]
	call_c   build_ref_725()
	call_c   Dyam_Term_Start(&ref[725],5)
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[673]
	c_ret

;; TERM 672: '*PROLOG-ITEM*'{top=> unfold(_J, _D, _S, _K, _G), cont=> '$CLOSURE'('$fun'(169, 0, 1178637092), '$TUPPLE'(35125252333408))}
c_code local build_ref_672
	ret_reg &ref[672]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,449709056)
	move_ret R(0)
	call_c   Dyam_Closure_Aux(fun169,R(0))
	move_ret R(0)
	call_c   build_ref_724()
	call_c   build_ref_673()
	call_c   Dyam_Create_Binary(&ref[724],&ref[673],R(0))
	move_ret ref[672]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 659: _K :> deallocate :> succeed
c_code local build_ref_659
	ret_reg &ref[659]
	call_c   build_ref_302()
	call_c   Dyam_Create_Binary(I(9),V(10),&ref[302])
	move_ret ref[659]
	c_ret

;; TERM 671: ['*JUMP*'(_J),saved(_S, _F, _D, _G)|_L]
c_code local build_ref_671
	ret_reg &ref[671]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_844()
	call_c   Dyam_Term_Start(&ref[844],4)
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   Dyam_Create_List(R(0),V(11))
	move_ret R(0)
	call_c   build_ref_658()
	call_c   Dyam_Create_List(&ref[658],R(0))
	move_ret ref[671]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 844: saved
c_code local build_ref_844
	ret_reg &ref[844]
	call_c   Dyam_Create_Atom("saved")
	move_ret ref[844]
	c_ret

long local pool_fun169[3]=[2,build_ref_659,build_ref_671]

pl_code local fun169
	call_c   Dyam_Pool(pool_fun169)
	call_c   Dyam_Allocate(0)
	move     &ref[659], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(5))
	pl_call  pred_jump_code_2()
	call_c   Dyam_Reg_Load_Ptr(2,V(1))
	fail_ret
	call_c   Dyam_Reg_Load(4,&ref[671])
	call_c   DyALog_Mutable_Write(R(2),&R(4),0)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(1))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_jump  Follow_Cont(V(0))

long local pool_fun170[2]=[1,build_seed_150]

pl_code local fun171
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(18),V(8))
	fail_ret
fun170:
	call_c   Dyam_Deallocate()
	pl_jump  fun19(&seed[150],12)


long local pool_fun172[3]=[131072,pool_fun170,pool_fun170]

pl_code local fun172
	call_c   Dyam_Remove_Choice()
	call_c   DYAM_evpred_length(V(11),V(17))
	fail_ret
	call_c   Dyam_Choice(fun171)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_evpred_gt(V(17),N(20))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(18),V(2))
	fail_ret
	pl_jump  fun170()

;; TERM 666: saved(_M, _N, _O, _G)
c_code local build_ref_666
	ret_reg &ref[666]
	call_c   build_ref_844()
	call_c   Dyam_Term_Start(&ref[844],4)
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[666]
	c_ret

;; TERM 669: '~w unfolded\n\twith env ~w\n\tbut used with env ~w\n'
c_code local build_ref_669
	ret_reg &ref[669]
	call_c   Dyam_Create_Atom("~w unfolded\n\twith env ~w\n\tbut used with env ~w\n")
	move_ret ref[669]
	c_ret

;; TERM 670: [_J,_O,_D]
c_code local build_ref_670
	ret_reg &ref[670]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(3),I(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(14),R(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(9),R(0))
	move_ret ref[670]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun167[3]=[2,build_ref_669,build_ref_670]

pl_code local fun167
	call_c   Dyam_Remove_Choice()
	move     &ref[669], R(0)
	move     0, R(1)
	move     &ref[670], R(2)
	move     S(5), R(3)
	pl_call  pred_internal_error_2()
	call_c   Dyam_Deallocate()
	pl_jump  Follow_Cont(V(0))

;; TERM 668: unify(_P, _Q) :> _N
c_code local build_ref_668
	ret_reg &ref[668]
	call_c   build_ref_667()
	call_c   Dyam_Create_Binary(I(9),&ref[667],V(13))
	move_ret ref[668]
	c_ret

;; TERM 667: unify(_P, _Q)
c_code local build_ref_667
	ret_reg &ref[667]
	call_c   build_ref_741()
	call_c   Dyam_Create_Binary(&ref[741],V(15),V(16))
	move_ret ref[667]
	c_ret

long local pool_fun168[3]=[65537,build_ref_668,pool_fun167]

pl_code local fun168
	call_c   Dyam_Update_Choice(fun167)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Reg_Load(0,V(3))
	call_c   Dyam_Reg_Load(2,V(14))
	pl_call  pred_check_env_renaming_2()
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(3))
	move     V(15), R(2)
	move     S(5), R(3)
	pl_call  pred_label_term_2()
	call_c   Dyam_Reg_Load(0,V(14))
	move     V(16), R(2)
	move     S(5), R(3)
	pl_call  pred_label_term_2()
	call_c   Dyam_Unify(V(5),&ref[668])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_jump  Follow_Cont(V(0))

long local pool_fun173[5]=[131074,build_ref_665,build_ref_666,pool_fun172,pool_fun168]

pl_code local fun173
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(7),&ref[665])
	fail_ret
	call_c   Dyam_Choice(fun172)
	call_c   Dyam_Set_Cut()
	pl_call  Domain_2(&ref[666],V(11))
	call_c   Dyam_Reg_Load(0,V(12))
	call_c   Dyam_Reg_Load(2,V(8))
	call_c   Dyam_Reg_Load(4,V(8))
	pl_call  pred_inter_3()
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun168)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(3),V(14))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(5),V(13))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_jump  Follow_Cont(V(0))

c_code local build_seed_149
	ret_reg &seed[149]
	call_c   build_ref_59()
	call_c   build_ref_661()
	call_c   Dyam_Seed_Start(&ref[59],&ref[661],I(0),fun15,1)
	call_c   build_ref_664()
	call_c   Dyam_Seed_Add_Comp(&ref[664],&ref[661],0)
	call_c   Dyam_Seed_End()
	move_ret seed[149]
	c_ret

;; TERM 664: '*PROLOG-FIRST*'(unfold(_J, _D, _I, _K, _G)) :> '$$HOLE$$'
c_code local build_ref_664
	ret_reg &ref[664]
	call_c   build_ref_663()
	call_c   Dyam_Create_Binary(I(9),&ref[663],I(7))
	move_ret ref[664]
	c_ret

;; TERM 663: '*PROLOG-FIRST*'(unfold(_J, _D, _I, _K, _G))
c_code local build_ref_663
	ret_reg &ref[663]
	call_c   build_ref_5()
	call_c   build_ref_662()
	call_c   Dyam_Create_Unary(&ref[5],&ref[662])
	move_ret ref[663]
	c_ret

;; TERM 662: unfold(_J, _D, _I, _K, _G)
c_code local build_ref_662
	ret_reg &ref[662]
	call_c   build_ref_725()
	call_c   Dyam_Term_Start(&ref[725],5)
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret ref[662]
	c_ret

;; TERM 661: '*PROLOG-ITEM*'{top=> unfold(_J, _D, _I, _K, _G), cont=> '$CLOSURE'('$fun'(166, 0, 1178609916), '$TUPPLE'(35125252333212))}
c_code local build_ref_661
	ret_reg &ref[661]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,452198400)
	move_ret R(0)
	call_c   Dyam_Closure_Aux(fun166,R(0))
	move_ret R(0)
	call_c   build_ref_724()
	call_c   build_ref_662()
	call_c   Dyam_Create_Binary(&ref[724],&ref[662],R(0))
	move_ret ref[661]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 660: [_H,saved(_I, _F, _D, _G)]
c_code local build_ref_660
	ret_reg &ref[660]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_844()
	call_c   Dyam_Term_Start(&ref[844],4)
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   Dyam_Create_List(R(0),I(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(7),R(0))
	move_ret ref[660]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

long local pool_fun166[3]=[2,build_ref_659,build_ref_660]

pl_code local fun166
	call_c   Dyam_Pool(pool_fun166)
	call_c   Dyam_Allocate(0)
	move     &ref[659], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(5))
	pl_call  pred_jump_code_2()
	call_c   Dyam_Reg_Load_Ptr(2,V(1))
	fail_ret
	call_c   Dyam_Reg_Load(4,&ref[660])
	call_c   DyALog_Mutable_Write(R(2),&R(4),0)
	move_ret R(0)
	call_c   Dyam_Reg_Unify_C_Ptr(0,V(1))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_jump  Follow_Cont(V(0))

long local pool_fun174[5]=[65539,build_ref_657,build_ref_658,build_seed_149,pool_fun173]

pl_code local fun174
	call_c   Dyam_Pool(pool_fun174)
	call_c   Dyam_Unify_Item(&ref[657])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load_Ptr(2,V(1))
	fail_ret
	move     V(7), R(4)
	move     S(5), R(5)
	call_c   DyALog_Mutable_Read(R(2),&R(4))
	fail_ret
	call_c   Dyam_Reg_Load(0,V(4))
	call_c   Dyam_Reg_Load(2,V(2))
	move     V(8), R(4)
	move     S(5), R(5)
	pl_call  pred_inter_3()
	call_c   Dyam_Choice(fun173)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(7),&ref[658])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Deallocate()
	pl_jump  fun19(&seed[149],12)

;; TERM 656: '*PROLOG-FIRST*'(unfold('*JUMP*'(_B, _C), _D, _E, (_F :> succeed), _G)) :> []
c_code local build_ref_656
	ret_reg &ref[656]
	call_c   build_ref_655()
	call_c   Dyam_Create_Binary(I(9),&ref[655],I(0))
	move_ret ref[656]
	c_ret

;; TERM 655: '*PROLOG-FIRST*'(unfold('*JUMP*'(_B, _C), _D, _E, (_F :> succeed), _G))
c_code local build_ref_655
	ret_reg &ref[655]
	call_c   build_ref_5()
	call_c   build_ref_654()
	call_c   Dyam_Create_Unary(&ref[5],&ref[654])
	move_ret ref[655]
	c_ret

c_code local build_seed_18
	ret_reg &seed[18]
	call_c   build_ref_4()
	call_c   build_ref_678()
	call_c   Dyam_Seed_Start(&ref[4],&ref[678],I(0),fun1,1)
	call_c   build_ref_679()
	call_c   Dyam_Seed_Add_Comp(&ref[679],fun180,0)
	call_c   Dyam_Seed_End()
	move_ret seed[18]
	c_ret

;; TERM 679: '*PROLOG-ITEM*'{top=> app_model(application{item=> '*LIGHTCITEM*'(_B, _C), trans=> ('*LIGHTLCFIRST*'(_D) :> _E), item_comp=> _F, code=> (allocate :> _G :> item_unify(_H) :> _I), item_id=> _J, trans_id=> _K, mode=> _L, restrict=> _M, out_env=> _N, key=> key(lcselect, _O, _P)}), cont=> _A}
c_code local build_ref_679
	ret_reg &ref[679]
	call_c   build_ref_724()
	call_c   build_ref_676()
	call_c   Dyam_Create_Binary(&ref[724],&ref[676],V(0))
	move_ret ref[679]
	c_ret

;; TERM 676: app_model(application{item=> '*LIGHTCITEM*'(_B, _C), trans=> ('*LIGHTLCFIRST*'(_D) :> _E), item_comp=> _F, code=> (allocate :> _G :> item_unify(_H) :> _I), item_id=> _J, trans_id=> _K, mode=> _L, restrict=> _M, out_env=> _N, key=> key(lcselect, _O, _P)})
c_code local build_ref_676
	ret_reg &ref[676]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_789()
	call_c   Dyam_Create_Unary(&ref[789],V(7))
	move_ret R(0)
	call_c   Dyam_Create_Binary(I(9),R(0),V(8))
	move_ret R(0)
	call_c   Dyam_Create_Binary(I(9),V(6),R(0))
	move_ret R(0)
	call_c   build_ref_299()
	call_c   Dyam_Create_Binary(I(9),&ref[299],R(0))
	move_ret R(0)
	call_c   build_ref_792()
	call_c   build_ref_793()
	call_c   Dyam_Term_Start(&ref[792],3)
	call_c   Dyam_Term_Arg(&ref[793])
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_End()
	move_ret R(1)
	call_c   build_ref_801()
	call_c   build_ref_651()
	call_c   build_ref_691()
	call_c   Dyam_Term_Start(&ref[801],10)
	call_c   Dyam_Term_Arg(&ref[651])
	call_c   Dyam_Term_Arg(&ref[691])
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_End()
	move_ret R(1)
	call_c   build_ref_813()
	call_c   Dyam_Create_Unary(&ref[813],R(1))
	move_ret ref[676]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 691: '*LIGHTLCFIRST*'(_D) :> _E
c_code local build_ref_691
	ret_reg &ref[691]
	call_c   build_ref_690()
	call_c   Dyam_Create_Binary(I(9),&ref[690],V(4))
	move_ret ref[691]
	c_ret

;; TERM 690: '*LIGHTLCFIRST*'(_D)
c_code local build_ref_690
	ret_reg &ref[690]
	call_c   build_ref_791()
	call_c   Dyam_Create_Unary(&ref[791],V(3))
	move_ret ref[690]
	c_ret

;; TERM 693: unify(_X, _Y, _S, _Y) :> succeed
c_code local build_ref_693
	ret_reg &ref[693]
	call_c   build_ref_692()
	call_c   build_ref_301()
	call_c   Dyam_Create_Binary(I(9),&ref[692],&ref[301])
	move_ret ref[693]
	c_ret

;; TERM 692: unify(_X, _Y, _S, _Y)
c_code local build_ref_692
	ret_reg &ref[692]
	call_c   build_ref_741()
	call_c   Dyam_Term_Start(&ref[741],4)
	call_c   Dyam_Term_Arg(V(23))
	call_c   Dyam_Term_Arg(V(24))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(24))
	call_c   Dyam_Term_End()
	move_ret ref[692]
	c_ret

long local pool_fun178[5]=[4,build_ref_640,build_ref_641,build_ref_693,build_ref_691]

pl_code local fun178
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(14),&ref[640])
	fail_ret
	call_c   Dyam_Unify(V(5),V(3))
	fail_ret
	call_c   Dyam_Reg_Load(0,V(1))
	move     V(23), R(2)
	move     S(5), R(3)
	pl_call  pred_label_term_2()
	call_c   Dyam_Reg_Load(0,V(5))
	move     V(18), R(2)
	move     S(5), R(3)
	pl_call  pred_label_term_2()
	call_c   Dyam_Unify(V(24),&ref[641])
	fail_ret
	move     &ref[693], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(15))
	pl_call  pred_label_code_2()
fun177:
	move     &ref[691], R(0)
	move     S(5), R(1)
	pl_call  pred_holes_1()
	call_c   Dyam_Unify(V(13),I(0))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_jump  Follow_Cont(V(0))


long local pool_fun179[4]=[65538,build_ref_338,build_ref_691,pool_fun178]

pl_code local fun179
	call_c   Dyam_Update_Choice(fun94)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(11),&ref[338])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun178)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(14),I(0))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(3),V(5))
	fail_ret
	call_c   Dyam_Unify(V(3),V(1))
	fail_ret
	pl_jump  fun177()

;; TERM 686: [_D,_B]
c_code local build_ref_686
	ret_reg &ref[686]
	call_c   build_ref_267()
	call_c   Dyam_Create_List(V(3),&ref[267])
	move_ret ref[686]
	c_ret

;; TERM 687: call(_Q, [])
c_code local build_ref_687
	ret_reg &ref[687]
	call_c   build_ref_735()
	call_c   Dyam_Create_Binary(&ref[735],V(16),I(0))
	move_ret ref[687]
	c_ret

;; TERM 689: unify(_R, _S) :> succeed
c_code local build_ref_689
	ret_reg &ref[689]
	call_c   build_ref_688()
	call_c   build_ref_301()
	call_c   Dyam_Create_Binary(I(9),&ref[688],&ref[301])
	move_ret ref[689]
	c_ret

;; TERM 688: unify(_R, _S)
c_code local build_ref_688
	ret_reg &ref[688]
	call_c   build_ref_741()
	call_c   Dyam_Create_Binary(&ref[741],V(17),V(18))
	move_ret ref[688]
	c_ret

;; TERM 681: _T(_U)(_V)
c_code local build_ref_681
	ret_reg &ref[681]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Binary(I(3),V(19),V(20))
	move_ret R(0)
	call_c   Dyam_Create_Binary(I(3),R(0),V(21))
	move_ret ref[681]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_151
	ret_reg &seed[151]
	call_c   build_ref_59()
	call_c   build_ref_682()
	call_c   Dyam_Seed_Start(&ref[59],&ref[682],I(0),fun15,1)
	call_c   build_ref_685()
	call_c   Dyam_Seed_Add_Comp(&ref[685],&ref[682],0)
	call_c   Dyam_Seed_End()
	move_ret seed[151]
	c_ret

;; TERM 685: '*PROLOG-FIRST*'(unfold((_T :> deallocate :> succeed), _U, _W, _I, _N)) :> '$$HOLE$$'
c_code local build_ref_685
	ret_reg &ref[685]
	call_c   build_ref_684()
	call_c   Dyam_Create_Binary(I(9),&ref[684],I(7))
	move_ret ref[685]
	c_ret

;; TERM 684: '*PROLOG-FIRST*'(unfold((_T :> deallocate :> succeed), _U, _W, _I, _N))
c_code local build_ref_684
	ret_reg &ref[684]
	call_c   build_ref_5()
	call_c   build_ref_683()
	call_c   Dyam_Create_Unary(&ref[5],&ref[683])
	move_ret ref[684]
	c_ret

;; TERM 683: unfold((_T :> deallocate :> succeed), _U, _W, _I, _N)
c_code local build_ref_683
	ret_reg &ref[683]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_302()
	call_c   Dyam_Create_Binary(I(9),V(19),&ref[302])
	move_ret R(0)
	call_c   build_ref_725()
	call_c   Dyam_Term_Start(&ref[725],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_Arg(V(22))
	call_c   Dyam_Term_Arg(V(8))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_End()
	move_ret ref[683]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 682: '*PROLOG-ITEM*'{top=> unfold((_T :> deallocate :> succeed), _U, _W, _I, _N), cont=> _A}
c_code local build_ref_682
	ret_reg &ref[682]
	call_c   build_ref_724()
	call_c   build_ref_683()
	call_c   Dyam_Create_Binary(&ref[724],&ref[683],V(0))
	move_ret ref[682]
	c_ret

long local pool_fun175[4]=[3,build_ref_651,build_ref_681,build_seed_151]

long local pool_fun176[5]=[65539,build_ref_686,build_ref_687,build_ref_689,pool_fun175]

pl_code local fun176
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(14),&ref[686])
	fail_ret
	call_c   Dyam_Unify(V(1),V(5))
	fail_ret
	move     V(16), R(0)
	move     S(5), R(1)
	move     N(0), R(2)
	move     0, R(3)
	pl_call  pred_reg_value_2()
	call_c   Dyam_Unify(V(6),&ref[687])
	fail_ret
	call_c   Dyam_Reg_Load(0,V(3))
	move     V(17), R(2)
	move     S(5), R(3)
	pl_call  pred_label_term_2()
	call_c   Dyam_Reg_Load(0,V(5))
	move     V(18), R(2)
	move     S(5), R(3)
	pl_call  pred_label_term_2()
	move     &ref[689], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(15))
	pl_call  pred_label_code_2()
fun175:
	move     &ref[651], R(0)
	move     S(5), R(1)
	pl_call  pred_holes_1()
	call_c   Dyam_Unify(V(4),&ref[681])
	fail_ret
	call_c   Dyam_Reg_Load(0,V(5))
	call_c   Dyam_Reg_Load(2,V(21))
	move     V(22), R(4)
	move     S(5), R(5)
	pl_call  pred_extend_tupple_3()
	call_c   Dyam_Reg_Load(0,V(5))
	call_c   Dyam_Reg_Load(2,V(7))
	pl_call  pred_label_term_2()
	call_c   Dyam_Deallocate()
	pl_jump  fun19(&seed[151],12)


long local pool_fun180[8]=[196612,build_ref_679,build_ref_650,build_ref_331,build_ref_680,pool_fun179,pool_fun176,pool_fun175]

pl_code local fun180
	call_c   Dyam_Pool(pool_fun180)
	call_c   Dyam_Unify_Item(&ref[679])
	fail_ret
	call_c   Dyam_Allocate(0)
	pl_call  Object_1(&ref[650])
	call_c   Dyam_Choice(fun179)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(11),&ref[331])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun176)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(14),I(0))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(3),V(5))
	fail_ret
	call_c   Dyam_Unify(V(3),V(1))
	fail_ret
	call_c   Dyam_Unify(V(6),&ref[680])
	fail_ret
	pl_jump  fun175()

;; TERM 678: '*PROLOG-FIRST*'(app_model(application{item=> '*LIGHTCITEM*'(_B, _C), trans=> ('*LIGHTLCFIRST*'(_D) :> _E), item_comp=> _F, code=> (allocate :> _G :> item_unify(_H) :> _I), item_id=> _J, trans_id=> _K, mode=> _L, restrict=> _M, out_env=> _N, key=> key(lcselect, _O, _P)})) :> []
c_code local build_ref_678
	ret_reg &ref[678]
	call_c   build_ref_677()
	call_c   Dyam_Create_Binary(I(9),&ref[677],I(0))
	move_ret ref[678]
	c_ret

;; TERM 677: '*PROLOG-FIRST*'(app_model(application{item=> '*LIGHTCITEM*'(_B, _C), trans=> ('*LIGHTLCFIRST*'(_D) :> _E), item_comp=> _F, code=> (allocate :> _G :> item_unify(_H) :> _I), item_id=> _J, trans_id=> _K, mode=> _L, restrict=> _M, out_env=> _N, key=> key(lcselect, _O, _P)}))
c_code local build_ref_677
	ret_reg &ref[677]
	call_c   build_ref_5()
	call_c   build_ref_676()
	call_c   Dyam_Create_Unary(&ref[5],&ref[676])
	move_ret ref[677]
	c_ret

c_code local build_seed_20
	ret_reg &seed[20]
	call_c   build_ref_4()
	call_c   build_ref_696()
	call_c   Dyam_Seed_Start(&ref[4],&ref[696],I(0),fun1,1)
	call_c   build_ref_697()
	call_c   Dyam_Seed_Add_Comp(&ref[697],fun186,0)
	call_c   Dyam_Seed_End()
	move_ret seed[20]
	c_ret

;; TERM 697: '*PROLOG-ITEM*'{top=> app_model(application{item=> '*CITEM*'(_B, _C), trans=> ('*LCFIRST*'(_D) :> _E), item_comp=> _F, code=> (allocate :> _G :> item_unify(_H) :> _I), item_id=> _J, trans_id=> _K, mode=> _L, restrict=> _M, out_env=> _N, key=> key(lcselect, _O, _P)}), cont=> _A}
c_code local build_ref_697
	ret_reg &ref[697]
	call_c   build_ref_724()
	call_c   build_ref_694()
	call_c   Dyam_Create_Binary(&ref[724],&ref[694],V(0))
	move_ret ref[697]
	c_ret

;; TERM 694: app_model(application{item=> '*CITEM*'(_B, _C), trans=> ('*LCFIRST*'(_D) :> _E), item_comp=> _F, code=> (allocate :> _G :> item_unify(_H) :> _I), item_id=> _J, trans_id=> _K, mode=> _L, restrict=> _M, out_env=> _N, key=> key(lcselect, _O, _P)})
c_code local build_ref_694
	ret_reg &ref[694]
	call_c   Dyam_Pseudo_Choice(2)
	call_c   build_ref_789()
	call_c   Dyam_Create_Unary(&ref[789],V(7))
	move_ret R(0)
	call_c   Dyam_Create_Binary(I(9),R(0),V(8))
	move_ret R(0)
	call_c   Dyam_Create_Binary(I(9),V(6),R(0))
	move_ret R(0)
	call_c   build_ref_299()
	call_c   Dyam_Create_Binary(I(9),&ref[299],R(0))
	move_ret R(0)
	call_c   build_ref_792()
	call_c   build_ref_793()
	call_c   Dyam_Term_Start(&ref[792],3)
	call_c   Dyam_Term_Arg(&ref[793])
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_End()
	move_ret R(1)
	call_c   build_ref_801()
	call_c   build_ref_633()
	call_c   build_ref_699()
	call_c   Dyam_Term_Start(&ref[801],10)
	call_c   Dyam_Term_Arg(&ref[633])
	call_c   Dyam_Term_Arg(&ref[699])
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(9))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(11))
	call_c   Dyam_Term_Arg(V(12))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(R(1))
	call_c   Dyam_Term_End()
	move_ret R(1)
	call_c   build_ref_813()
	call_c   Dyam_Create_Unary(&ref[813],R(1))
	move_ret ref[694]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 699: '*LCFIRST*'(_D) :> _E
c_code local build_ref_699
	ret_reg &ref[699]
	call_c   build_ref_698()
	call_c   Dyam_Create_Binary(I(9),&ref[698],V(4))
	move_ret ref[699]
	c_ret

;; TERM 698: '*LCFIRST*'(_D)
c_code local build_ref_698
	ret_reg &ref[698]
	call_c   build_ref_794()
	call_c   Dyam_Create_Unary(&ref[794],V(3))
	move_ret ref[698]
	c_ret

long local pool_fun184[5]=[4,build_ref_640,build_ref_641,build_ref_693,build_ref_699]

pl_code local fun184
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(14),&ref[640])
	fail_ret
	call_c   Dyam_Unify(V(5),V(3))
	fail_ret
	call_c   Dyam_Reg_Load(0,V(1))
	move     V(23), R(2)
	move     S(5), R(3)
	pl_call  pred_label_term_2()
	call_c   Dyam_Reg_Load(0,V(5))
	move     V(18), R(2)
	move     S(5), R(3)
	pl_call  pred_label_term_2()
	call_c   Dyam_Unify(V(24),&ref[641])
	fail_ret
	move     &ref[693], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(15))
	pl_call  pred_label_code_2()
fun183:
	move     &ref[699], R(0)
	move     S(5), R(1)
	pl_call  pred_holes_1()
	call_c   Dyam_Unify(V(13),I(0))
	fail_ret
	call_c   Dyam_Deallocate()
	pl_jump  Follow_Cont(V(0))


long local pool_fun185[4]=[65538,build_ref_338,build_ref_699,pool_fun184]

pl_code local fun185
	call_c   Dyam_Update_Choice(fun94)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(11),&ref[338])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun184)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(14),I(0))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(3),V(5))
	fail_ret
	call_c   Dyam_Unify(V(3),V(1))
	fail_ret
	pl_jump  fun183()

long local pool_fun181[4]=[3,build_ref_633,build_ref_681,build_seed_151]

long local pool_fun182[5]=[65539,build_ref_686,build_ref_687,build_ref_689,pool_fun181]

pl_code local fun182
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(14),&ref[686])
	fail_ret
	call_c   Dyam_Unify(V(1),V(5))
	fail_ret
	move     V(16), R(0)
	move     S(5), R(1)
	move     N(0), R(2)
	move     0, R(3)
	pl_call  pred_reg_value_2()
	call_c   Dyam_Unify(V(6),&ref[687])
	fail_ret
	call_c   Dyam_Reg_Load(0,V(3))
	move     V(17), R(2)
	move     S(5), R(3)
	pl_call  pred_label_term_2()
	call_c   Dyam_Reg_Load(0,V(5))
	move     V(18), R(2)
	move     S(5), R(3)
	pl_call  pred_label_term_2()
	move     &ref[689], R(0)
	move     S(5), R(1)
	call_c   Dyam_Reg_Load(2,V(15))
	pl_call  pred_label_code_2()
fun181:
	move     &ref[633], R(0)
	move     S(5), R(1)
	pl_call  pred_holes_1()
	call_c   Dyam_Unify(V(4),&ref[681])
	fail_ret
	call_c   Dyam_Reg_Load(0,V(5))
	call_c   Dyam_Reg_Load(2,V(21))
	move     V(22), R(4)
	move     S(5), R(5)
	pl_call  pred_extend_tupple_3()
	call_c   Dyam_Reg_Load(0,V(5))
	call_c   Dyam_Reg_Load(2,V(7))
	pl_call  pred_label_term_2()
	call_c   Dyam_Deallocate()
	pl_jump  fun19(&seed[151],12)


long local pool_fun186[8]=[196612,build_ref_697,build_ref_650,build_ref_331,build_ref_680,pool_fun185,pool_fun182,pool_fun181]

pl_code local fun186
	call_c   Dyam_Pool(pool_fun186)
	call_c   Dyam_Unify_Item(&ref[697])
	fail_ret
	call_c   Dyam_Allocate(0)
	pl_call  Object_1(&ref[650])
	call_c   Dyam_Choice(fun185)
	call_c   Dyam_Set_Cut()
	call_c   DYAM_sfol_identical(V(11),&ref[331])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Choice(fun182)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(14),I(0))
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(3),V(5))
	fail_ret
	call_c   Dyam_Unify(V(3),V(1))
	fail_ret
	call_c   Dyam_Unify(V(6),&ref[680])
	fail_ret
	pl_jump  fun181()

;; TERM 696: '*PROLOG-FIRST*'(app_model(application{item=> '*CITEM*'(_B, _C), trans=> ('*LCFIRST*'(_D) :> _E), item_comp=> _F, code=> (allocate :> _G :> item_unify(_H) :> _I), item_id=> _J, trans_id=> _K, mode=> _L, restrict=> _M, out_env=> _N, key=> key(lcselect, _O, _P)})) :> []
c_code local build_ref_696
	ret_reg &ref[696]
	call_c   build_ref_695()
	call_c   Dyam_Create_Binary(I(9),&ref[695],I(0))
	move_ret ref[696]
	c_ret

;; TERM 695: '*PROLOG-FIRST*'(app_model(application{item=> '*CITEM*'(_B, _C), trans=> ('*LCFIRST*'(_D) :> _E), item_comp=> _F, code=> (allocate :> _G :> item_unify(_H) :> _I), item_id=> _J, trans_id=> _K, mode=> _L, restrict=> _M, out_env=> _N, key=> key(lcselect, _O, _P)}))
c_code local build_ref_695
	ret_reg &ref[695]
	call_c   build_ref_5()
	call_c   build_ref_694()
	call_c   Dyam_Create_Unary(&ref[5],&ref[694])
	move_ret ref[695]
	c_ret

c_code local build_seed_31
	ret_reg &seed[31]
	call_c   build_ref_4()
	call_c   build_ref_702()
	call_c   Dyam_Seed_Start(&ref[4],&ref[702],I(0),fun1,1)
	call_c   build_ref_703()
	call_c   Dyam_Seed_Add_Comp(&ref[703],fun194,0)
	call_c   Dyam_Seed_End()
	move_ret seed[31]
	c_ret

;; TERM 703: '*PROLOG-ITEM*'{top=> unfold('*SIMPLE-KLEENE*'(_B, _C, _D), _E, _F, _G, _H), cont=> _A}
c_code local build_ref_703
	ret_reg &ref[703]
	call_c   build_ref_724()
	call_c   build_ref_700()
	call_c   Dyam_Create_Binary(&ref[724],&ref[700],V(0))
	move_ret ref[703]
	c_ret

;; TERM 700: unfold('*SIMPLE-KLEENE*'(_B, _C, _D), _E, _F, _G, _H)
c_code local build_ref_700
	ret_reg &ref[700]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   build_ref_845()
	call_c   Dyam_Term_Start(&ref[845],3)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(2))
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_End()
	move_ret R(0)
	call_c   build_ref_725()
	call_c   Dyam_Term_Start(&ref[725],5)
	call_c   Dyam_Term_Arg(R(0))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(5))
	call_c   Dyam_Term_Arg(V(6))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[700]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 845: '*SIMPLE-KLEENE*'
c_code local build_ref_845
	ret_reg &ref[845]
	call_c   Dyam_Create_Atom("*SIMPLE-KLEENE*")
	move_ret ref[845]
	c_ret

;; TERM 704: 'KLEENE label=~w Env=~w T1=~w T=~w\n'
c_code local build_ref_704
	ret_reg &ref[704]
	call_c   Dyam_Create_Atom("KLEENE label=~w Env=~w T1=~w T=~w\n")
	move_ret ref[704]
	c_ret

;; TERM 705: [_B,_E,_L,_M]
c_code local build_ref_705
	ret_reg &ref[705]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Tupple(11,12,I(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(4),R(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(1),R(0))
	move_ret ref[705]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

c_code local build_seed_153
	ret_reg &seed[153]
	call_c   build_ref_59()
	call_c   build_ref_723()
	call_c   Dyam_Seed_Start(&ref[59],&ref[723],I(0),fun15,1)
	call_c   build_ref_720()
	call_c   Dyam_Seed_Add_Comp(&ref[720],&ref[723],0)
	call_c   Dyam_Seed_End()
	move_ret seed[153]
	c_ret

;; TERM 720: '*PROLOG-FIRST*'(unfold(_D, _E, _K, _W, _H)) :> '$$HOLE$$'
c_code local build_ref_720
	ret_reg &ref[720]
	call_c   build_ref_719()
	call_c   Dyam_Create_Binary(I(9),&ref[719],I(7))
	move_ret ref[720]
	c_ret

;; TERM 719: '*PROLOG-FIRST*'(unfold(_D, _E, _K, _W, _H))
c_code local build_ref_719
	ret_reg &ref[719]
	call_c   build_ref_5()
	call_c   build_ref_718()
	call_c   Dyam_Create_Unary(&ref[5],&ref[718])
	move_ret ref[719]
	c_ret

;; TERM 718: unfold(_D, _E, _K, _W, _H)
c_code local build_ref_718
	ret_reg &ref[718]
	call_c   build_ref_725()
	call_c   Dyam_Term_Start(&ref[725],5)
	call_c   Dyam_Term_Arg(V(3))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(22))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_End()
	move_ret ref[718]
	c_ret

;; TERM 723: '*PROLOG-ITEM*'{top=> unfold(_D, _E, _K, _W, _H), cont=> '$CLOSURE'('$fun'(190, 0, 1176330852), '$TUPPLE'(35125262024956))}
c_code local build_ref_723
	ret_reg &ref[723]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,425984064)
	move_ret R(0)
	call_c   Dyam_Closure_Aux(fun190,R(0))
	move_ret R(0)
	call_c   build_ref_724()
	call_c   build_ref_718()
	call_c   Dyam_Create_Binary(&ref[724],&ref[718],R(0))
	move_ret ref[723]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 721: kleene(_B, _K, _E, _H, _B)
c_code local build_ref_721
	ret_reg &ref[721]
	call_c   build_ref_836()
	call_c   Dyam_Term_Start(&ref[836],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_End()
	move_ret ref[721]
	c_ret

;; TERM 722: named_function(_X, _B, local)
c_code local build_ref_722
	ret_reg &ref[722]
	call_c   build_ref_837()
	call_c   build_ref_838()
	call_c   Dyam_Term_Start(&ref[837],3)
	call_c   Dyam_Term_Arg(V(23))
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(&ref[838])
	call_c   Dyam_Term_End()
	move_ret ref[722]
	c_ret

long local pool_fun190[5]=[4,build_ref_603,build_ref_721,build_ref_722,build_ref_617]

pl_code local fun190
	call_c   Dyam_Pool(pool_fun190)
	call_c   Dyam_Allocate(0)
	move     &ref[603], R(0)
	move     S(5), R(1)
	move     V(23), R(2)
	move     S(5), R(3)
	pl_call  pred_label_code_2()
	call_c   DYAM_evpred_assert_1(&ref[721])
	call_c   DYAM_evpred_assert_1(&ref[722])
	call_c   Dyam_Unify(V(6),&ref[617])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_jump  Follow_Cont(V(0))

long local pool_fun191[2]=[1,build_seed_153]

pl_code local fun191
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Deallocate()
	pl_jump  fun19(&seed[153],12)

;; TERM 711: kleene(_B, _S, _T, _U, _V)
c_code local build_ref_711
	ret_reg &ref[711]
	call_c   build_ref_836()
	call_c   Dyam_Term_Start(&ref[836],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(18))
	call_c   Dyam_Term_Arg(V(19))
	call_c   Dyam_Term_Arg(V(20))
	call_c   Dyam_Term_Arg(V(21))
	call_c   Dyam_Term_End()
	move_ret ref[711]
	c_ret

c_code local build_seed_152
	ret_reg &seed[152]
	call_c   build_ref_59()
	call_c   build_ref_717()
	call_c   Dyam_Seed_Start(&ref[59],&ref[717],I(0),fun15,1)
	call_c   build_ref_720()
	call_c   Dyam_Seed_Add_Comp(&ref[720],&ref[717],0)
	call_c   Dyam_Seed_End()
	move_ret seed[152]
	c_ret

;; TERM 717: '*PROLOG-ITEM*'{top=> unfold(_D, _E, _K, _W, _H), cont=> '$CLOSURE'('$fun'(189, 0, 1176316792), '$TUPPLE'(35125262024956))}
c_code local build_ref_717
	ret_reg &ref[717]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_Simple_Tupple(0,425984064)
	move_ret R(0)
	call_c   Dyam_Closure_Aux(fun189,R(0))
	move_ret R(0)
	call_c   build_ref_724()
	call_c   build_ref_718()
	call_c   Dyam_Create_Binary(&ref[724],&ref[718],R(0))
	move_ret ref[717]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 714: kleene_supp
c_code local build_ref_714
	ret_reg &ref[714]
	call_c   Dyam_Create_Atom("kleene_supp")
	move_ret ref[714]
	c_ret

;; TERM 715: '~w_~w'
c_code local build_ref_715
	ret_reg &ref[715]
	call_c   Dyam_Create_Atom("~w_~w")
	move_ret ref[715]
	c_ret

;; TERM 716: [_B,_Y]
c_code local build_ref_716
	ret_reg &ref[716]
	call_c   Dyam_Pseudo_Choice(1)
	call_c   Dyam_Create_List(V(24),I(0))
	move_ret R(0)
	call_c   Dyam_Create_List(V(1),R(0))
	move_ret ref[716]
	call_c   Dyam_Remove_Pseudo_Choice()
	c_ret

;; TERM 712: named_function(_X, _O, local)
c_code local build_ref_712
	ret_reg &ref[712]
	call_c   build_ref_837()
	call_c   build_ref_838()
	call_c   Dyam_Term_Start(&ref[837],3)
	call_c   Dyam_Term_Arg(V(23))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_Arg(&ref[838])
	call_c   Dyam_Term_End()
	move_ret ref[712]
	c_ret

;; TERM 713: kleene(_B, _K, _E, _H, _O)
c_code local build_ref_713
	ret_reg &ref[713]
	call_c   build_ref_836()
	call_c   Dyam_Term_Start(&ref[836],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(10))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_End()
	move_ret ref[713]
	c_ret

;; TERM 707: call(_O, [])
c_code local build_ref_707
	ret_reg &ref[707]
	call_c   build_ref_735()
	call_c   Dyam_Create_Binary(&ref[735],V(14),I(0))
	move_ret ref[707]
	c_ret

long local pool_fun187[3]=[2,build_ref_713,build_ref_707]

long local pool_fun188[6]=[65540,build_ref_714,build_ref_715,build_ref_716,build_ref_712,pool_fun187]

pl_code local fun188
	call_c   Dyam_Remove_Choice()
	move     &ref[714], R(0)
	move     0, R(1)
	move     V(24), R(2)
	move     S(5), R(3)
	pl_call  pred_update_counter_2()
	move     &ref[715], R(0)
	move     0, R(1)
	move     &ref[716], R(2)
	move     S(5), R(3)
	move     V(14), R(4)
	move     S(5), R(5)
	pl_call  pred_name_builder_3()
	call_c   DYAM_evpred_assert_1(&ref[712])
fun187:
	call_c   DYAM_evpred_assert_1(&ref[713])
	call_c   Dyam_Unify(V(6),&ref[707])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_jump  Follow_Cont(V(0))


long local pool_fun189[5]=[131074,build_ref_603,build_ref_712,pool_fun188,pool_fun187]

pl_code local fun189
	call_c   Dyam_Pool(pool_fun189)
	call_c   Dyam_Allocate(0)
	move     &ref[603], R(0)
	move     S(5), R(1)
	move     V(23), R(2)
	move     S(5), R(3)
	pl_call  pred_label_code_2()
	call_c   Dyam_Choice(fun188)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[712])
	call_c   Dyam_Cut()
	pl_jump  fun187()

long local pool_fun192[4]=[65538,build_ref_711,build_seed_152,pool_fun191]

pl_code local fun192
	call_c   Dyam_Update_Choice(fun191)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[711])
	call_c   Dyam_Cut()
	call_c   Dyam_Deallocate()
	pl_jump  fun19(&seed[152],12)

;; TERM 708: kleene(_B, _N, _P, _H, _O)
c_code local build_ref_708
	ret_reg &ref[708]
	call_c   build_ref_836()
	call_c   Dyam_Term_Start(&ref[836],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(15))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_End()
	move_ret ref[708]
	c_ret

;; TERM 710: unify(_Q, _R) :> call(_O, [])
c_code local build_ref_710
	ret_reg &ref[710]
	call_c   build_ref_709()
	call_c   build_ref_707()
	call_c   Dyam_Create_Binary(I(9),&ref[709],&ref[707])
	move_ret ref[710]
	c_ret

;; TERM 709: unify(_Q, _R)
c_code local build_ref_709
	ret_reg &ref[709]
	call_c   build_ref_741()
	call_c   Dyam_Create_Binary(&ref[741],V(16),V(17))
	move_ret ref[709]
	c_ret

long local pool_fun193[4]=[65538,build_ref_708,build_ref_710,pool_fun192]

pl_code local fun193
	call_c   Dyam_Update_Choice(fun192)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[708])
	call_c   Dyam_Reg_Load(0,V(4))
	call_c   Dyam_Reg_Load(2,V(15))
	pl_call  pred_check_env_renaming_2()
	call_c   Dyam_Reg_Load(0,V(13))
	call_c   Dyam_Reg_Load(2,V(10))
	call_c   Dyam_Reg_Load(4,V(10))
	pl_call  pred_inter_3()
	call_c   Dyam_Cut()
	call_c   Dyam_Reg_Load(0,V(4))
	move     V(16), R(2)
	move     S(5), R(3)
	pl_call  pred_label_term_2()
	call_c   Dyam_Reg_Load(0,V(15))
	move     V(17), R(2)
	move     S(5), R(3)
	pl_call  pred_label_term_2()
	call_c   Dyam_Unify(V(6),&ref[710])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_jump  Follow_Cont(V(0))

;; TERM 706: kleene(_B, _N, _E, _H, _O)
c_code local build_ref_706
	ret_reg &ref[706]
	call_c   build_ref_836()
	call_c   Dyam_Term_Start(&ref[836],5)
	call_c   Dyam_Term_Arg(V(1))
	call_c   Dyam_Term_Arg(V(13))
	call_c   Dyam_Term_Arg(V(4))
	call_c   Dyam_Term_Arg(V(7))
	call_c   Dyam_Term_Arg(V(14))
	call_c   Dyam_Term_End()
	move_ret ref[706]
	c_ret

long local pool_fun194[7]=[65541,build_ref_703,build_ref_704,build_ref_705,build_ref_706,build_ref_707,pool_fun193]

pl_code local fun194
	call_c   Dyam_Pool(pool_fun194)
	call_c   Dyam_Unify_Item(&ref[703])
	fail_ret
	call_c   Dyam_Allocate(0)
	call_c   Dyam_Reg_Load(0,V(3))
	move     V(8), R(2)
	move     S(5), R(3)
	pl_call  pred_tupple_2()
	call_c   Dyam_Reg_Load(0,V(5))
	call_c   Dyam_Reg_Load(2,V(8))
	move     V(9), R(4)
	move     S(5), R(5)
	pl_call  pred_inter_3()
	call_c   Dyam_Reg_Load(0,V(2))
	call_c   Dyam_Reg_Load(2,V(9))
	move     V(10), R(4)
	move     S(5), R(5)
	pl_call  pred_union_3()
	call_c   Dyam_Reg_Load(0,V(2))
	move     V(11), R(2)
	move     S(5), R(3)
	pl_call  pred_oset_tupple_2()
	call_c   Dyam_Reg_Load(0,V(10))
	move     V(12), R(2)
	move     S(5), R(3)
	pl_call  pred_oset_tupple_2()
	move     &ref[704], R(0)
	move     0, R(1)
	move     &ref[705], R(2)
	move     S(5), R(3)
	pl_call  pred_format_2()
	call_c   Dyam_Choice(fun193)
	call_c   Dyam_Set_Cut()
	pl_call  Object_1(&ref[706])
	call_c   Dyam_Reg_Load(0,V(13))
	call_c   Dyam_Reg_Load(2,V(10))
	call_c   Dyam_Reg_Load(4,V(10))
	pl_call  pred_inter_3()
	call_c   Dyam_Cut()
	call_c   Dyam_Unify(V(6),&ref[707])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_jump  Follow_Cont(V(0))

;; TERM 702: '*PROLOG-FIRST*'(unfold('*SIMPLE-KLEENE*'(_B, _C, _D), _E, _F, _G, _H)) :> []
c_code local build_ref_702
	ret_reg &ref[702]
	call_c   build_ref_701()
	call_c   Dyam_Create_Binary(I(9),&ref[701],I(0))
	move_ret ref[702]
	c_ret

;; TERM 701: '*PROLOG-FIRST*'(unfold('*SIMPLE-KLEENE*'(_B, _C, _D), _E, _F, _G, _H))
c_code local build_ref_701
	ret_reg &ref[701]
	call_c   build_ref_5()
	call_c   build_ref_700()
	call_c   Dyam_Create_Unary(&ref[5],&ref[700])
	move_ret ref[701]
	c_ret

;; TERM 1: special_app_model(_A)
c_code local build_ref_1
	ret_reg &ref[1]
	call_c   build_ref_800()
	call_c   Dyam_Create_Unary(&ref[800],V(0))
	move_ret ref[1]
	c_ret

;; TERM 3: '*RITEM*'(special_app_model(_A), voidret)
c_code local build_ref_3
	ret_reg &ref[3]
	call_c   build_ref_0()
	call_c   build_ref_1()
	call_c   build_ref_2()
	call_c   Dyam_Create_Binary(&ref[0],&ref[1],&ref[2])
	move_ret ref[3]
	c_ret

;; TERM 367: _M45247498
c_code local build_ref_367
	ret_reg &ref[367]
	call_c   Dyam_Create_Unary(I(6),V(4))
	move_ret ref[367]
	c_ret

;; TERM 366: _C45247507
c_code local build_ref_366
	ret_reg &ref[366]
	call_c   Dyam_Create_Unary(I(6),V(3))
	move_ret ref[366]
	c_ret

;; TERM 369: [_H|_I]
c_code local build_ref_369
	ret_reg &ref[369]
	call_c   Dyam_Create_List(V(7),V(8))
	move_ret ref[369]
	c_ret

;; TERM 368: [_F|_G]
c_code local build_ref_368
	ret_reg &ref[368]
	call_c   Dyam_Create_List(V(5),V(6))
	move_ret ref[368]
	c_ret

;; TERM 371: [_J|_I]
c_code local build_ref_371
	ret_reg &ref[371]
	call_c   Dyam_Create_List(V(9),V(8))
	move_ret ref[371]
	c_ret

;; TERM 370: [_J|_G]
c_code local build_ref_370
	ret_reg &ref[370]
	call_c   Dyam_Create_List(V(9),V(6))
	move_ret ref[370]
	c_ret

;; TERM 54: [_C,_B]
c_code local build_ref_54
	ret_reg &ref[54]
	call_c   build_ref_267()
	call_c   Dyam_Create_List(V(2),&ref[267])
	move_ret ref[54]
	c_ret

pl_code local fun18
	call_c   Dyam_Backptr(R(1))
	call_c   Dyam_Object(R(0))
	call_c   Dyam_Tabule()
	pl_ret

pl_code local fun85
	call_c   Dyam_Remove_Choice()
	call_c   DYAM_evpred_univ(V(1),&ref[370])
	fail_ret
	call_c   DYAM_evpred_univ(V(2),&ref[371])
	fail_ret
	call_c   Dyam_Reg_Load(0,V(6))
	call_c   Dyam_Reg_Load(2,V(8))
	call_c   Dyam_Reg_Deallocate(2)
	pl_jump  pred_check_env_renaming_2()

pl_code local fun86
	call_c   Dyam_Update_Choice(fun85)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[368])
	fail_ret
	call_c   Dyam_Unify(V(2),&ref[369])
	fail_ret
	call_c   Dyam_Reg_Load(0,V(5))
	call_c   Dyam_Reg_Load(2,V(7))
	pl_call  pred_check_env_renaming_2()
	call_c   Dyam_Reg_Load(0,V(6))
	call_c   Dyam_Reg_Load(2,V(8))
	pl_call  pred_check_env_renaming_2()
	call_c   Dyam_Cut()
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun87
	call_c   Dyam_Update_Choice(fun86)
	call_c   Dyam_Set_Cut()
	call_c   Dyam_Unify(V(1),&ref[366])
	fail_ret
	call_c   Dyam_Unify(V(2),&ref[367])
	fail_ret
	call_c   Dyam_Cut()
	call_c   Dyam_Deallocate()
	pl_ret

pl_code local fun13
	call_c   Dyam_Remove_Choice()
	call_c   Dyam_Unify(V(3),&ref[54])
	fail_ret
	call_c   Dyam_Deallocate()
	pl_ret


;;------------------ INIT POOL ------------

c_code local build_init_pool
	call_c   build_seed_3()
	call_c   build_seed_4()
	call_c   build_seed_7()
	call_c   build_seed_8()
	call_c   build_seed_9()
	call_c   build_seed_10()
	call_c   build_seed_11()
	call_c   build_seed_0()
	call_c   build_seed_6()
	call_c   build_seed_2()
	call_c   build_seed_1()
	call_c   build_seed_5()
	call_c   build_seed_24()
	call_c   build_seed_38()
	call_c   build_seed_28()
	call_c   build_seed_23()
	call_c   build_seed_37()
	call_c   build_seed_33()
	call_c   build_seed_57()
	call_c   build_seed_58()
	call_c   build_seed_59()
	call_c   build_seed_60()
	call_c   build_seed_29()
	call_c   build_seed_67()
	call_c   build_seed_64()
	call_c   build_seed_34()
	call_c   build_seed_54()
	call_c   build_seed_61()
	call_c   build_seed_46()
	call_c   build_seed_47()
	call_c   build_seed_48()
	call_c   build_seed_55()
	call_c   build_seed_56()
	call_c   build_seed_25()
	call_c   build_seed_39()
	call_c   build_seed_36()
	call_c   build_seed_26()
	call_c   build_seed_63()
	call_c   build_seed_22()
	call_c   build_seed_43()
	call_c   build_seed_41()
	call_c   build_seed_62()
	call_c   build_seed_71()
	call_c   build_seed_65()
	call_c   build_seed_40()
	call_c   build_seed_13()
	call_c   build_seed_45()
	call_c   build_seed_69()
	call_c   build_seed_68()
	call_c   build_seed_12()
	call_c   build_seed_14()
	call_c   build_seed_15()
	call_c   build_seed_44()
	call_c   build_seed_42()
	call_c   build_seed_51()
	call_c   build_seed_52()
	call_c   build_seed_53()
	call_c   build_seed_35()
	call_c   build_seed_16()
	call_c   build_seed_50()
	call_c   build_seed_27()
	call_c   build_seed_49()
	call_c   build_seed_17()
	call_c   build_seed_30()
	call_c   build_seed_70()
	call_c   build_seed_32()
	call_c   build_seed_21()
	call_c   build_seed_19()
	call_c   build_seed_66()
	call_c   build_seed_18()
	call_c   build_seed_20()
	call_c   build_seed_31()
	call_c   build_ref_1()
	call_c   build_ref_3()
	call_c   build_ref_367()
	call_c   build_ref_366()
	call_c   build_ref_369()
	call_c   build_ref_368()
	call_c   build_ref_371()
	call_c   build_ref_370()
	call_c   build_ref_54()
	c_ret

long local ref[846]
long local seed[154]

long local _initialization

c_code global initialization_dyalog_application
	call_c   Dyam_Check_And_Update_Initialization(_initialization)
	fail_c_ret
	call_c   initialization_dyalog_format()
	call_c   build_init_pool()
	call_c   build_viewers()
	call_c   load()
	c_ret

