/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 1999 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  bmg.pl -- Tools for BMG
 *
 * ----------------------------------------------------------------
 * Description
 *
 *     Provide some tools for the compilation of Bound Movement Grammars
 *     into LPDAs (see dcg.pl)
 *
 * ----------------------------------------------------------------
 */

:-include 'header.pl'.

%% Build unification sequence for stacks that are not identical

bmg_equations([],[],Rest,Rest).
bmg_equations([A|SA],[B|SB],Rest,Trans) :-
	( A==B ->
	    Trans = Trans2
	;   
	    Trans = unify(A,B) :> Trans2
	),
	bmg_equations(SA,SB,Rest,Trans2)
	.


%% Build a set of  stacks

bmg_get_stacks(Stacks,Id) :-
	bmg_get_stacks(Stacks),
	my_numbervars(Stacks,Id,_)
	.

bmg_get_stacks(Stacks) :-
	bmg_stacks(Names),
	length(Names,L),
	length(Stacks,L)
	.

%% Build outside and inside stacks for a rule head that possibly push NT on Name
%% Name == [] means no pushing ([] shouldn't be the name of a BMG stack)

:-rec_prolog bmg_head_stacks_loop/7.

bmg_head_stacks(Name,NT,Left,Right,Inside_Left,Inside_Right) :-
	bmg_stacks(Names),
	bmg_head_stacks_loop(Names,Name,NT,Left,Right,Inside_Left,Inside_Right)
	.

bmg_head_stacks_loop([],_,_,[],[],[],[]).

bmg_head_stacks_loop([N|SN],N,NT,[L|Left],[[NT|L]|Right],[[]|Inside_Left],[[]|Inside_Right]) :-
	bmg_head_stacks_loop(SN,N,NT,Left,Right,Inside_Left,Inside_Right).

bmg_head_stacks_loop([N|SN],Name,NT,[L|Left],[R|Right],[L|Inside_Left],[R|Inside_Right]) :-
	N \== Name,
	bmg_head_stacks_loop(SN,Name,NT,Left,Right,Inside_Left,Inside_Right)
	.

%% Build inside stacks for a non-terminal wrapped in a set of island cosntraints
%% It also builds a sequence of unification to satisfy on the outside stacks. 

bmg_inside_island(A,AAA,Left,Right,Inside_Left,Inside_Right,Cont,Trans) :-
	(   ( A =.. [Island,AA], bmg_island(Island)) ->
	    bmg_stacks(Names),
	    bmg_inside_island_loop(Names,Island,Left,Right,Inside_Left2,Inside_Right2,Cont,Trans2),
	    bmg_inside_island(AA,AAA,Inside_Left2,Inside_Right2,Inside_Left,Inside_Right,Trans2,Trans )
	;
	    Inside_Left = Left, Inside_Right = Right, Cont=Trans, A=AAA
	)
	.

:-rec_prolog bmg_inside_island_loop/8.

bmg_inside_island_loop([],_,[],[],[],[],Cont,Cont).
bmg_inside_island_loop([N|NS],Island,[L|LS],[R|RS],[IL|ILS],[IR|IRS],Cont,Trans) :-
	( bmg_island(Island,N) ->
	    IL = [], IR=[],
	    ( L == R -> Cont2 = Cont ; Cont2= unify(L,R) :> Cont )
	;
	    IL = L, IR = R, Cont2 = Cont
	),
	bmg_inside_island_loop(NS,Island,LS,RS,ILS,IRS,Cont2,Trans)
	.

%% Identify stacks from which some non-terminal A may be discharged

bmg_pop(A,Left,Right,Pop) :-
	bmg_stacks(Names),
	functor(A,F,N),
	bmg_pop_loop(Names,F/N,Left,Right,Pop)
	.

:-rec_prolog bmg_pop_loop/5.

bmg_pop_loop([],_,[],[],[]).
bmg_pop_loop([N|SN],Spec,[L|LS],[R|RS],Pop) :-
	(   (bmg_pushable(Spec,N), L \== R) ->  
	    Pop = [N|Pop2]
	;
	    Pop2 = Pop
	),
	bmg_pop_loop(SN,Spec,LS,RS,Pop2)
	.

%% Generate transition to discharge A from possible stacks Pop
%% We get a set of alterntive discharge + initial transition without discharge.

bmg_pop_code([P|PL],A,Left,Right,LP,RP,Cont,Trans) :-
	bmg_stacks(Names),
	( PL=[] -> 
	    bmg_pop_code_loop(Names,P,A,Left,Right,Code,Cont),
	    Trans = unify(LP,RP) :> Code
	;   
	    handle_choice(Cont,Trans,Last,unify(LP,RP) :> Code1,Code2),
	    bmg_pop_code_loop(Names,P,A,Left,Right,Code1,Last),
	    bmg_pop_code(PL,A,Left,Right,LP,RP,Last,Code2)
	)
	.

:-rec_prolog bmg_pop_code_loop/7.

bmg_pop_code_loop([],_,_,[],[],Last,Last).
bmg_pop_code_loop([N|NS],P,A,[L|LS],[R|RS],Code,Last) :-
	( N == P ->
	    Code = unify(L,[A|R]) :> Code2
	;   
	    Code = unify(L,R) :> Code2
	),
	bmg_pop_code_loop(NS,P,A,LS,RS,Code2,Last)
	.








