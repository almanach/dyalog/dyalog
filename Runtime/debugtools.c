/************************************************************
 * $Id$
 * Copyright (C) 1996, 2004, 2009, 2011, 2014, 2015 Eric de la Clergerie
 * ------------------------------------------------------------
 *
 *   DebugTools -- Tools for Debugging
 *
 * ------------------------------------------------------------
 * Description
 *   Different printing tools to help the debbuging
 * ------------------------------------------------------------
 */


#include <stdarg.h>
#include <string.h>
#include "hash.h"
#include "libdyalog.h"
#include "builtins.h"
#include "tree-indexing.h"

extern void dyalog_fprintf( StmInf *stream, const char *fmt, ... );

/**********************************************************************
 *  To print <t>_k
 **********************************************************************/
inline
static void
local_sfol_fdisplay(StmInf *stream, fol_t t, fkey_t k)
{
  fol_fdisplay(stream,t);
  if (k) Stream_Printf(stream,"_%d",LSTACK_INDEX(k));
}

/*
inline
static void
local_sfol_display(fol_t t, fkey_t k)
{
  local_sfol_fdisplay(INT_TO_STM(stm_output),t,k);
}
*/

/**********************************************************************
 * To print a box of the Trail Stack
 **********************************************************************/
static void
trail_fprint( StmInf *stream, bind_t b )
{
    switch ( b->type ) {
        case LAYER:
            Stream_Printf( stream, "[LAYER]    %d", LSTACK_INDEX(TSO(b).top));
            break;
        case UBIND:
        case SBIND:
            Stream_Printf( stream,
                           "[%s]%s    next=%3d ",
                           (b->type == UBIND) ? "UNIF" : "SUBS",
                           (TSB(b).keep) ? "+" : "-",
                           TSB(b).next ? LSTACK_INDEX(TSB(b).next->binder_key) : -1 );
            dyalog_fprintf( stream,
                            "%&p --> %&p",
                            TSB(b).binder, TSB(b).binder_key,
                            TSB(b).bindee, TSB(b).bindee_key );
            break;
        case CGBIND:
            dyalog_fprintf( stream,
                            "[CGBIND] %&p --> left=%&p right=%&p",
                            TSCG(b).binder, TSCG(b).k_binder,
                            TSCG(b).left, TSCG(b).k_left,
                            TSCG(b).right, TSCG(b).k_right
                            );
            break;
        case COLLAPSE:
            if (TSC(b).layer)
                Stream_Printf( stream,
                               "[COLLAPSE] %3d %s collect0=%08X shift=%d start=%d next=%d ",
                               LSTACK_INDEX(TSC(b).layer),
                               TSC(b).exited ? "+" : "-",
                               TSC(b).layer->collect0,
                               TSC(b).shift,
                               LSTACK_INDEX(TSC(b).block_start),
                               TSC(b).next ? LSTACK_INDEX(TSC(b).next->layer) : -1
                               );
            else
                Stream_Printf( stream, "[COLLAPSE]-" );
            break;
        case UNBIND: {
            unbind_t p = C_TRAIL_UNBIND;
            rename_t l = TSU(b).rename_list;
            for(; p && (p != (unbind_t) b); p = p->next);
            dyalog_fprintf( stream,
                            "[UNBIND]%s  %&p {",
                            p ? "+" : "-" ,
                            TSU(b).Y, TSU(b).l
                            );
            for(; l; l = l->next)
                dyalog_fprintf( stream, " %&p",l->X,l->k);
            Stream_Printf( stream, " }" ); }
        break;
        case COLLECT:
            dyalog_fprintf( stream, "[COLLECT]" );
            break;
        case RENAME:
            dyalog_fprintf( stream, "[RENAME]   %&p", TSR(b).X, TSR(b).k);
            break;
        case CHOICE:
            dyalog_fprintf( stream, "[CHOICE]%s %d",
                            R_B == (choice_t) b ? "+" : "-",
                            TSCH(b).n );
            break;
        case ENVIRONMENT:
            dyalog_fprintf( stream, "[ENVIRONMENT] %d", TSEV(b).n );
            break;
        case INDEXATION: 
        {
            int depth = ((Resume)b)->depth;
            dyalog_fprintf( stream, "[INDEXATION] %d", depth );
            break; 
        }
        case FORWARD:
            Stream_Printf( stream, "[FORWARD]" );
            break;
        case UPWARD:
            Stream_Printf( stream, "[UPWARD]" );
            break;
        case HASH_SCAN:
            Stream_Printf( stream, "[HASH_SCAN]" );
            break;
        case BLOCK:
            Stream_Printf( stream, "[BLOCK]" );
            break;
        case REGISTERS:
            Stream_Printf( stream, "[REGISTERS]" );
            break;
        default:
            Stream_Printf( stream, "Not a valid trail stack type %d",(int)b->type);
            break;
    }
    Stream_Printf( stream, "\n");
}

void
trail_print( bind_t b)
{
  trail_fprint( INT_TO_STM(stm_output), b );
}

/**********************************************************************
 * To print the Trail Stack
 **********************************************************************/
inline
static void
trail_fprint_stack(StmInf *stream)
{
    TrailWord *p = C_TRAIL_TOP;
    while ((bind_t)(*p)) {
        trail_fprint(stream,(bind_t) *p);
        p = ((TrailWord *) (*p))-1;
    }
}

void
trail_print_stack()
{
  trail_fprint_stack( INT_TO_STM(stm_output) );
}

/**********************************************************************
 * To print a layer
 **********************************************************************/
static void
vca_fdisplay( StmInf *stream, fkey_t k )
{
  unsigned long l = VCA_SAFE_LENGTH( k->vca ) << ENTRY_SHIFT ;
  unsigned long i = 0;
  sbind_t b;

  Stream_Printf( stream, "Layer %d {", LSTACK_INDEX(k) );

  if (k->vca)
    for( ; i < l ; i++ )
      if ((b = vca_ref( k->vca , i))) {
	fkey_t l = k;
	SBINDING_K(l,b);
	Stream_Puts(" ",stream);
	fol_fdisplay( stream, FOLVAR_FROM_INDEX( i ) );
	Stream_Puts("-->",stream);
	local_sfol_fdisplay( stream, b->bindee ,l);
      }
  
  Stream_Printf( stream, " } ");
}

void
vca_display( fkey_t k )
{
  vca_fdisplay( INT_TO_STM(stm_output), k );
}

/**********************************************************************
 * A general printer
 **********************************************************************/
void
dyalog_vfprintf( StmInf *stream, const char *fmt, va_list args )
{
  const char  *p=fmt;
  char *s;
  int i;
  double f;
  fkey_t k;
  fol_t A;
  sfol_t S;
  
  for( ; *p; p++ ){
    
    if (*p != '%' ) {
      Stream_Putc(*p,stream);
      continue;
    }

    switch (*++p) {
        case 'd':
            i = va_arg(args,long);
            Stream_Printf(stream,"%ld", i );
            break;
        case 'c':
            i = va_arg(args,long);
            Stream_Printf(stream,"%lc", i );
            break;
        case 'x':
            i = va_arg(args,long);
            Stream_Printf(stream,"%lx", i );
            break;
        case 'X':
            i = va_arg(args,long);
            Stream_Printf(stream,"%lX", i );
            break;
        case 'o':
            i = va_arg(args,long);
            Stream_Printf(stream,"%lo", i );
            break;
        case 's':
            s = va_arg(args, char *);
            Stream_Printf(stream,"%s",s);
            break;
        case 'g':
            f = va_arg(args, double);
            Stream_Printf(stream,"%g",f);
            break;
        case '&':			/* DyALog specifics */
            switch (*++p) {
                case 's':			/* sfol */
                    A = va_arg(args, fol_t);
                    k = va_arg(args, fkey_t);
                    sfol_fdisplay( stream, A, k );
                    break;
                case 'S':			/* SFOL */
                    S = va_arg(args, sfol_t);
                    sfol_fdisplay( stream, S->t, S->k );
                    break;
                case 'c':			/* call return view */
                    A = va_arg(args,fol_t);
                    k = va_arg(args,fkey_t);
                    callret_fdisplay(stream,A,k);
                    break;
                case 'f':			/* fol */
                    A = va_arg(args, fol_t);
                    fol_fdisplay( stream, A );
                    break;
                case 'p':			/* pair fol_key */
                    A = va_arg(args, fol_t);
                    k = va_arg(args, fkey_t);
                    local_sfol_fdisplay( stream, A, k );
                    break;
                case 'l':			/* layers */
                    for (k = LSTACK_BASE ; k < LSTACK_TOP ; k++)
                        vca_fdisplay( stream, k );
                    break;
                case 'k': 		/* key */
                    k = va_arg(args, fkey_t);
                    Stream_Printf( stream, "%d", k ? LSTACK_INDEX( k ) : 0 );
                    break;
                case 'e':			/* envinfo */
                    Stream_Printf( stream, "Layer top=%d", LSTACK_INDEX( LSTACK_TOP ));
                    break;
                case 't':			/* trail */
                    trail_fprint_stack(stream);
                    break;
                case 'o':
                {
                    tabobj_t o = va_arg(args, tabobj_t);
                    k = Trail_Object(o);
                    A = TABOBJ_MODEL(o);
                    sfol_fdisplay( stream, A, k );
                    Untrail_Object;
                }
                break;

                default:
                    Stream_Putc(*p,stream);
                    break;
            }
            break;
        default:
            Stream_Putc(*p,stream);
            break;
    }
  }

  va_end(args);

}

void
dyalog_printf( const char *fmt, ... )
{
  va_list args;
  va_start( args, fmt );
  dyalog_vfprintf( INT_TO_STM(stm_output) , fmt, args );
}

void
dyalog_fprintf( StmInf *stream, const char *fmt, ... )
{
  va_list args;
  va_start( args, fmt );
  dyalog_vfprintf(stream,fmt,args);
}

void
dyalog_error_printf( const char *fmt, ... )
{
  va_list args;
  va_start( args, fmt );
  dyalog_vfprintf(INT_TO_STM(stm_error),fmt,args);
}

/**********************************************************************
 * Error/1
 **********************************************************************/

static Bool Error_1(Sproto);

Bool              /* DyALog Wrapper */
DYAM_Error_1(fol_t A)
{
    fkey_t k=R_TRANS_KEY;
    return Error_1(A,k);
}

static Bool	  /* DyALog Builtin */
Error_1( SP(A) )
{
  dyalog_error_printf( "{ERROR: %&s}\n", A, Sk(A) );
  Fail;
}

