/** 
 * ----------------------------------------------------------------
 * $Id$
 * Copyright (C) 2013, 2014, 2015, 2016, 2017 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  \file  stat_model.c 
 *  \brief Statistical Models
 *
 * ----------------------------------------------------------------
 * Description
 * Management of statistical models
 * ----------------------------------------------------------------
 */

/**********************************************************************
 *
 * support for statistical model management
 * implemented as an hash trie
 *
 * standard operations are
 *      - loading a model (Mdl_Load)
 *      - saving a model  (Mdl_Save)
 *      - querying a model for weights given keys (Mdl_Query)
 *      - updating a model (Mdl_Update)
 *
 *
 * We allow complex non-deterministic querying operations
 * using variables in queries
 * The variables may be bound during the traversal of the Model
 * and weights are returned for each instanciation
 * 
 **********************************************************************/

#include <string.h>
#include "libdyalog.h"
#include "builtins.h"
#include "hash.h"

extern char *getenv();

#define MDL_TRIM_MIN 0.1
#define MDL_TRIM_N 1
#define MDL_TRIM_BATCH 10000

static unsigned long mdl_trim_n=MDL_TRIM_N;
static double mdl_trim_min=MDL_TRIM_MIN;
static unsigned long mdl_trim_batch=MDL_TRIM_BATCH;

#ifndef HAVE_STRNDUP

size_t
my_strnlen (const char *string, size_t maxlen)
{
  const char *end = memchr (string, '\0', maxlen);
  return end ? (size_t) (end - string) : maxlen;
}

char *
strndup (const char *s, size_t n)
{
  size_t len = my_strnlen (s, n);
  char *new = GC_MALLOC_ATOMIC (sizeof(char) * (len + 1));

  if (new == NULL)
    return NULL;

  new[len] = '\0';
  return (char *) memcpy (new, s, len);
}

#endif

static unsigned long perceptron_count=1;



typedef struct collect_smb 
{
    long key;
    long id;
    long emitted;
} * collect_smb_node_t;

#define COLLECT_SMB_ELEM_SIZE sizeof(struct collect_smb)

static unsigned long smb_collect_ctr=0;

typedef struct mdl_node
{
    long key;
    char *next;
    double weight;
    double acc;
    unsigned long n;            /* update frequency */
    unsigned long f;            /* visit frequency */
} * mdl_node_t;

#define MDL_ELEM_SIZE sizeof(struct mdl_node)
#define MDL_TABLE_SIZE  4

static struct mdl_node mdl_root_node = {(long) FOLNIL,NULL,0.0};
static mdl_node_t mdl_root= &mdl_root_node;

static mdl_node_t
mdl_new_entry(long key, double w, char **table) 
{
    struct mdl_node tmp;
    tmp.key = key;
    tmp.next = NULL;
    tmp.weight = w;
    tmp.acc=0.0;
    tmp.n=0;
    if (!*table)
        *table = (char *) Hash_Alloc_Table(MDL_TABLE_SIZE, MDL_ELEM_SIZE);
    return (mdl_node_t) Hash_Insert(*table,(char *)&tmp,(Bool)0);
}

static void
mdl_collect_smb(char *table, char *smb_table) 
{
    HashScan scan;
    mdl_node_t p = (mdl_node_t) Hash_First(table,&scan);
    for( ; p ; p = (mdl_node_t) Hash_Next(&scan)) {
        if (FOLSMBP(p->key)) {
            long key = p->key;
            if (!Hash_Find(smb_table,key)) {
                struct collect_smb tmp;
                tmp.key = key;
                tmp.id = smb_collect_ctr++;
                tmp.emitted = 0;
                Hash_Insert(smb_table,(char *)&tmp,(Bool)0);
            }
        }
        if (p->next)
            mdl_collect_smb(p->next,smb_table);
    }
}

static collect_smb_node_t
mdl_collect_smb_info(char *smb_table,long key) 
{
    return (collect_smb_node_t) Hash_Find(smb_table,key);
}

static char *
mdl_trim_aux(char *table) 
{
    HashScan scan;
    mdl_node_t p = (mdl_node_t) Hash_First(table,&scan);
    for( ; p ; p = (mdl_node_t) Hash_Next(&scan)) {
        if (p->next)
            p->next = mdl_trim_aux(p->next);
        if (p->weight || !p->next){
            p->weight -= (p->acc / (1.0 * perceptron_count));
            p->acc = 0.0;
        }
        if (!p->next
            && ((fabs(p->weight) < mdl_trim_min)
                || p->n < mdl_trim_n
                )
            ) {
            Hash_Delete(table,p->key);
        }
    }
    return table;
}

static void
mdl_trim() 
{
    mdl_root->next = mdl_trim_aux(mdl_root->next);
}

static void
mdl_save_aux(FILE *model, char *table, unsigned long depth,char *smb_table) 
{
    HashScan scan;
    mdl_node_t p = (mdl_node_t) Hash_First(table,&scan);
    for( ; p ; p = (mdl_node_t) Hash_Next(&scan)) {
        char *next = p->next;
        double weight = 0.0;
        if (p->weight || !next) {
            weight = p->weight;
        }
        if (weight || next) {
            if (FOLSMBP(p->key)) {
                collect_smb_node_t info = mdl_collect_smb_info(smb_table,p->key);
                if (info->emitted) {
                    fprintf(model,"s:%ld:%.2lf:%ld\n",depth,weight,info->id);
                } else if (SymbolName(p->key) == '\0') {
                    fprintf(model,"se:%ld:%.2lf\n",depth,weight);
                } else {
                    info->emitted = 1;
                    fprintf(model,"sw:%ld:%.2lf:%ld:%s\n",depth,weight,info->id,SymbolName(p->key));
                }
            } else {
                fprintf(model,"i:%ld:%.2lf:%ld\n",depth,weight,CFOLINT(p->key));
            }
            if (next)
                mdl_save_aux(model,next,depth+1,smb_table);
        }
    }
}

Bool
Mdl_Save(char *modelfile) 
{
    FILE *model = fopen(modelfile,"w");
    if (!model) {
        dyalog_printf("error opening model file %s\n",modelfile);
        Fail;
    }
    if (mdl_root->next) {
        char * smb_table = (char *) Hash_Alloc_Table(MDL_TABLE_SIZE,COLLECT_SMB_ELEM_SIZE);
        smb_collect_ctr = 1;
        mdl_collect_smb(mdl_root->next,smb_table);
        fprintf(model,"smbtable:%ld\n",smb_collect_ctr);
        mdl_save_aux(model,mdl_root->next,0,smb_table);
    }
    if (fclose(model)) {
        dyalog_printf("error closing model file %s\n",modelfile);
        Fail;
    }
    Succeed;
}

static void
mdl_save_binary_aux(FILE *model, char *table, unsigned long depth,char *smb_table) 
{
    HashScan scan;
    mdl_node_t p = (mdl_node_t) Hash_First(table,&scan);
    for( ; p ; p = (mdl_node_t) Hash_Next(&scan)) {
        char *next = p->next;
        float weight = 0.0;
        if (p->weight || !next) {
            weight = p->weight;
        }
        if (weight || next) {
            unsigned char type;
            unsigned char dinfo = (depth << 3);
            long i;
            long id=0;
            unsigned char l=0;
            char *s;
            if (weight)
                dinfo += 4;
            if (FOLSMBP(p->key)) {
                collect_smb_node_t info = mdl_collect_smb_info(smb_table,p->key);
                if (info->emitted) {
                    type = 2;
                    id = info->id;
                } else if (SymbolName(p->key) == '\0') {
                    type = 2;
                    id=0;
                } else {
                    info->emitted = 1;
                    type = 3;
                    id = info->id;
                    s = SymbolName(p->key);
                    l = strlen(s);
                }
            } else {
                type = 1;
                i = CFOLINT(p->key);
            }
            dinfo += type;
            fwrite(&dinfo,sizeof(char),1,model);
            if (weight)
                fwrite(&weight,sizeof(float),1,model);
//            printf("l %ld:%d:%lf => %d <%x>\n",depth,type,weight,dinfo,dinfo);
            switch (type) {
                case 1:         /* integer */
                    fwrite(&i,sizeof(int),1,model);
                    break;
                case 2: /* initialized symbol or empty string */
                    fwrite(&id,sizeof(unsigned int),1,model);
                    break;
                case 3: /* un-initialized symbol */
//                    printf("save new symbol %ld: %d %s\n",id,l,s);
                    fwrite(&id,sizeof(unsigned int),1,model);
                    fwrite(&l,sizeof(unsigned char),1,model);
                    fwrite(s,sizeof(char),l,model);
                    break;
                default:
                    printf("should not be here!");
                    return;
            }
            if (next)
                mdl_save_binary_aux(model,next,depth+1,smb_table);
        }
    }
}

Bool
Mdl_Save_Binary(char *modelfile) 
{
    FILE *model = fopen(modelfile,"wb");
    if (!model) {
        dyalog_printf("error opening model file %s\n",modelfile);
        Fail;
    }
    if (mdl_root->next) {
        char * smb_table = (char *) Hash_Alloc_Table(MDL_TABLE_SIZE,COLLECT_SMB_ELEM_SIZE);
        smb_collect_ctr = 1;
        mdl_collect_smb(mdl_root->next,smb_table);
        fwrite(&smb_collect_ctr,sizeof(unsigned int),1,model);
        mdl_save_binary_aux(model,mdl_root->next,0,smb_table);
    }
    if (fclose(model)) {
        dyalog_printf("error closing model file %s\n",modelfile);
        Fail;
    }
    Succeed;
}


Bool
Mdl_Load(char *modelfile)
{
        /* reset hash tables */
    mdl_node_t vstack[50];
    char line [256];
    long n=0;
    long i=0;
    double w=0;
    char buff[256];
    fol_t * smbtable;
    long nbsymb=0;
    FILE *model = fopen(modelfile,"r");
    if (!model) {
        dyalog_printf("error opening model file %s\n",modelfile);
        Fail;
    }
    if ( fgets(line,256,model)
         && sscanf(line,"smbtable:%ld",&nbsymb) == 1) {
//        dyalog_printf("allocating smb table with %d entries\n",nbsymb);
    } else {
        dyalog_printf("format problem in model file %s line 1 <smbtable>\n",modelfile);
        Fail;
    }
    smbtable = (fol_t *) alloca(nbsymb * sizeof(fol_t));
//    dyalog_printf("done loading smbtable\n");
    n = 1;
    while (fgets(line,256,model) != NULL) {
        long vdepth=0;
        n++;
        if (sscanf(line,"i:%ld:%lf:%ld",&vdepth,&w,&i) == 3) {
            char **table = (vdepth > 0) ? &(vstack[vdepth-1]->next) : &(mdl_root->next);
            vstack[vdepth] = mdl_new_entry((long) DFOLINT(i),w,table);
        } else if (sscanf(line,"s:%ld:%lf:%ld",&vdepth,&w,&i) == 3) {
            char **table = (vdepth > 0) ? &(vstack[vdepth-1]->next) : &(mdl_root->next);
            vstack[vdepth] = mdl_new_entry((long)smbtable[i],w,table);
        } else if (sscanf(line,"sw:%ld:%lf:%ld:%s",&vdepth,&w,&i,buff) == 4) {
            fol_t s = smbtable[i] = Create_Atom((char *)strndup(buff,256));
            char **table = (vdepth > 0) ? &(vstack[vdepth-1]->next) : &(mdl_root->next);
            vstack[vdepth] = mdl_new_entry((long)s,w,table);
        } else if (sscanf(line,"se:%ld:%lf",&vdepth,&w) == 2) {
            char **table = (vdepth > 0) ? &(vstack[vdepth-1]->next) : &(mdl_root->next);
            vstack[vdepth] = mdl_new_entry((long)Create_Atom((char *)""),w,table);
        } else {
            printf("unexpected cmd: %s\n",line);
        }
    }
    if (fclose(model)) {
        dyalog_printf("error closing model file %s\n",modelfile);
        Fail;
    }
    Succeed;
}


Bool
Mdl_Load_Binary(char *modelfile)
{
        /* reset hash tables */
    mdl_node_t vstack[50];
    long n=0;
    int i=0;
    unsigned long id=0;
    unsigned short sl=0;
    char buff[256];
    fol_t * smbtable;
    unsigned int nbsymb=0;
    FILE *model=fopen(modelfile,"rb");
    if (!model) {
        dyalog_printf("error opening model file %s\n",modelfile);
        Fail;
    }
    fread(&nbsymb,sizeof(unsigned int),1,model);
//    printf("#line smbtable:%ld\n",nbsymb);
    smbtable = (fol_t *) alloca(nbsymb * sizeof(fol_t));
    n = 1;
    while (1) {
        unsigned char vdepth=0;
        unsigned char type = 0;
        float w=0.0;
        char **table;
        fol_t s;
        fread(&vdepth,sizeof(char),1,model);
        type = vdepth & 3;
        if (!type) {
            if (fclose(model)) {
                dyalog_printf("error closing model file %s\n",modelfile);
                Fail;
            }
//            printf("loaded %ld entries\n",n);
            Succeed;
        }
        if (vdepth & 4)
            fread(&w,sizeof(float),1,model);
        vdepth >>= 3;
        n++;
        table = (vdepth > 0) ? &(vstack[vdepth-1]->next) : &(mdl_root->next);
//        printf("entry %ld type %d depth %d weight %f\n",n,type,vdepth,w);
        switch (type) {

            case 1:             /* integer */
                fread(&i,sizeof(int),1,model);
//                printf("#line i:%ld:%.2lf:%d\n",vdepth,w,i);
                vstack[vdepth] = mdl_new_entry((long) DFOLINT(i),w,table);
                break;

            case 2:             /* initialized symbol or empty string */
                fread(&id,sizeof(unsigned int),1,model);
                if (id) {
//                    printf("#line s:%ld:%.2lf:%ld\n",vdepth,w,id-1);
                    vstack[vdepth] = mdl_new_entry((long)smbtable[id-1],w,table);
                } else {
//                    printf("#line se:%ld:%.2lf\n",vdepth,w);
                    vstack[vdepth] = mdl_new_entry((long)Create_Atom((char *)""),w,table);
                }
                break;

            case 3:             /* un-initialized symbol */
                fread(&id,sizeof(unsigned int),1,model);
                fread(&sl,sizeof(unsigned char),1,model);
                fread(buff,sizeof(char),sl,model);
                buff[sl] = 0;
//                printf("#line sw:%ld:%.2lf:%ld:%s\n",vdepth,w,id-1,buff);
//                printf("loaded smb %ld: %d %s\n",id,sl,buff);
                s = smbtable[id-1] = Create_Atom((char *)strndup(buff,256));
                vstack[vdepth] = mdl_new_entry((long)s,w,table);
                break;

            default:
                printf("unexpected cmd %d\n",type);
                Fail;
                
        }
    }
    Fail;
}


static void
mdl_update_aux(mdl_node_t entry, SP(values), double update, double acc_update) 
{
    long key;
    char **table=NULL;
        /* values is a sequence of values acting as a path to a final weight w
           we (possibly) update w and return it
         */
    while (1) {

        Deref(values);
//        dyalog_printf("next loop entry=%x values=%&s\n",entry,values,Sk(values));
        
        if (FOLNILP(values)) {
                /* final step */
//            printf("final update step entry=%p\n",entry);
            entry->weight += update;
            entry->acc += acc_update;
            entry->n++;
            return;
        }
        
        if (FOLPAIRP(values)) {
            fol_t v = FOLPAIR_CAR(values);
            fkey_t Sk(v) = Sk(values);
            Deref(v);
            key = (long)v;

            table = &(entry->next);
        
            if (FOLPAIRP(v)) {
                /* if v may be a list of values (for the same feature),
                   we update following each coponent and return the sum on all results
                 */
                values = FOLPAIR_CDR(values);
                while (FOLPAIRP(v)) {
                    fol_t x = FOLPAIR_CAR(v);
                    fkey_t Sk(x) = Sk(v);
                    Deref(x);
                    key = (long)x;
                    if (!*table || !(entry = (mdl_node_t) Hash_Find(*table,key)))
                        entry = mdl_new_entry(key,0.0,table);
                    mdl_update_aux(entry,values,Sk(values),update,acc_update);
                    v = FOLPAIR_CDR(v);
                    Deref(v);
                }
                return;
            }
        
                //printf("adding table=%p key=%d v=%ld\n",table,key,(long)v);
            if (!*table || !(entry = (mdl_node_t) Hash_Find(*table,key)))
                entry = mdl_new_entry(key,0.0,table);
            values = FOLPAIR_CDR(values);
            continue;
        }

        if (FOLCMPP(values)) {
                /* if values is a compound term (but not a list)
                   then its arguments are subpaths to follow in parallel
                   we update following each coponent and return the sum on all results
                */
            fol_t *arg = &FOLCMP_REF( values, 1 );
            fol_t *stop = arg+FOLCMP_ARITY(values);
            for( ; arg < stop ; ) {
                mdl_update_aux(entry,*arg++,Sk(values),update,acc_update);
            }
            return;
        }

            /* should not reach this point */
//        dyalog_printf("should not be here values=%&s\n",values,Sk(values));
        return;
    }
    return;
}

void
Mdl_Update(sfol_t values,long update) 
{
//    dyalog_printf("entering mdl update\n");
    mdl_update_aux(mdl_root,values->t,values->k,1.0 * update, 1.0 * update * perceptron_count);
}

    /* Search blocks are stored in the trail stack
       and are used to accumulate weights for different chain of bindings
       and also to keep trace of the bindings

       NOTE: it should be possible to use simpler blocks, use backtrack on
       HashScan in mdl_query_aux. However, it involves a rewriting of
       mdl_query_aux to be iterative rather than recursive.
       
     */
typedef struct mdl_block
{
    fol_t var;
    fkey_t k;
    fol_t value;
    double weight;              /* current accumulated weight for a binding chain */
    struct mdl_block *bnext;    /* next chain binding (AND) */
    struct mdl_block *next;     /* next alternative binding (OR) */
    fol_t *filter;
} *mdl_block_t;

inline
static mdl_block_t
mdl_push_block (fol_t X, fkey_t Sk(X), fol_t value,fol_t *filter)
{
    mdl_block_t block = (mdl_block_t) PUSH_TRAIL_BLOCK(sizeof(struct mdl_block));
//    dyalog_printf("block bind %&f %&f\n",X,value);
    *block = (struct mdl_block) {X,Sk(X),value,0.0,NULL,NULL,filter};
    return block;
}

inline
static mdl_block_t
mdl_find_or_push_block(mdl_block_t parent, fol_t X, fkey_t Sk(X), fol_t value) 
{
    if (parent->bnext) {
        mdl_block_t current = parent->bnext;
        while( 1 ) {
            if ( current->var == X
                 && current->k == Sk(X)
                 && current->value == value
                 ) {
                return current;
            } else if (!current->next)
                return current->next = mdl_push_block(X,Sk(X),value,current->filter);
                // printf("advance block\n");
            current = current->next;
        }
    } else
        return parent->bnext = mdl_push_block(X,Sk(X),value,parent->filter);
}


static void
mdl_query_aux(mdl_node_t entry, SP(values), mdl_block_t results) 
{
    char *table=NULL;        
        /* values is a sequence of values acting as a path to a final weight w
           we (possibly) update w and return it
         */
    while (entry) {
        Deref(values);
//        dyalog_printf("next loop values=%&s table=%x\n",values,Sk(values),table);

        
        results->weight += entry->weight;
        
        if (FOLNILP(values)){
//            dyalog_printf("leaf %&f w=%&f accw=%&f\n",(fol_t)entry->key,DFOLFLT(entry->weight),DFOLFLT(results->weight));
            return;                 /* final step */
        }

//        dyalog_printf("current %&f\n",(fol_t)entry->key);
        
        table = entry->next;
        if (!table)
            return;
                
        if (FOLPAIRP(values)) {
            fol_t v = FOLPAIR_CAR(values);
            fkey_t Sk(v) = Sk(values);
            Deref(v);
            values = FOLPAIR_CDR(values);
            
            if (FOLPAIRP(v)) {
                    /* if v may be a list of values (for the same feature),
                       we return the sum on all results
                    */
                while (FOLPAIRP(v)) {
                    fol_t x = FOLPAIR_CAR(v);
                    fkey_t Sk(x) = Sk(v);
                    Deref(x);
                    if ((entry = (mdl_node_t) Hash_Find(table,(long)x)))
                        mdl_query_aux(entry,values,Sk(values),results);
                    v = FOLPAIR_CDR(v);
                    Deref(v);
                }
                return;
            } else if (FOLVARP(v)) {
                fol_t * filter = results->filter;
                if (!filter) {
                    HashScan scan;
                    mdl_node_t entry = (mdl_node_t) Hash_First(table,&scan);
                    for(; entry ; entry = (mdl_node_t) Hash_Next(&scan)) {
                        mdl_query_aux( entry,
                                       values,Sk(values),
                                       mdl_find_or_push_block(results,v,Sk(v),(fol_t) entry->key)
                                       );
                    }
                } else {
                    for(; *filter; filter++) {
                        if ((entry=(mdl_node_t)Hash_Find(table,(long)*filter)))
                            mdl_query_aux( entry,
                                           values,Sk(values),
                                           mdl_find_or_push_block(results,v,Sk(v),*filter)
                                           );
                    }
                }
                return;
            }
            

            if (!(entry=(mdl_node_t) Hash_Find(table,(long) v)))
                return;
            continue;
        }

        if (FOLCMPP(values)) {
                /* if values is a compound term (but not a list)
                   then its arguments are subpaths to follow in parallel
                   we return the sum on all results
                */
            fol_t *arg = &FOLCMP_REF( values, 1 );
            fol_t *stop = arg+FOLCMP_ARITY(values)-1;
            for( ; arg < stop ; ) {
                mdl_query_aux(entry,*arg++,Sk(values),results);
            }
            values = *arg;
            continue;
        }

            /* should not reach this point */
//        dyalog_printf("should not be here values=%&s\n",values,Sk(values));
        return;
        
    }
    
    return;
}

// Declaration of a DyALog wrapper Mdl_Query_Next bound to mdl_query_next
extern void Mdl_Query_Next();
// **DyALog Builtin** Mdl_Query_Next :- mdl_query_next(0,1,2,3)

Bool
mdl_query_next(fol_t res, fkey_t Sk(res), mdl_block_t results, fol_t accweight)
{
    results->weight += CFOLFLT(accweight);
    if (results->next) {
            // missing update
//        results->next->weight += old->weight;
        LVALUE_X(0) = REG_VALUE(res);
        LVALUE_X(1) = REG_VALUE(Sk(res));
        LVALUE_X(2) = REG_VALUE(results->next);
        LVALUE_X(3) = REG_VALUE(accweight);
        update_choice(Mdl_Query_Next,4);
    } else {
        Dyam_Remove_Choice();
    }
    Unify_Or_Fail(results->var,results->k,results->value,Key0);
    for(; results->bnext ;) {
        mdl_block_t old = results;
        results = results->bnext;
        results->weight += old->weight;
        old->bnext = NULL;
        if (results->next) {
            LVALUE_X(0) = REG_VALUE(res);
            LVALUE_X(1) = REG_VALUE(Sk(res));
            LVALUE_X(2) = REG_VALUE(results->next);
            LVALUE_X(3) = REG_VALUE(DFOLFLT(old->weight));
            TRAIL_CHOICE(Mdl_Query_Next,4);
        }
        Unify_Or_Fail(results->var,results->k,results->value,Key0);
    }
    if (!results->weight)
        Fail;
    return Unify(res,Sk(res),DFOLINT(round(results->weight)),Key0);
}

// **DyALog Builtin** Mdl_Query :- mdl_query(0,1,2)
Bool
mdl_query(fol_t values, fol_t res, fol_t filter)
{
    fkey_t Sk(values) = R_TRANS_KEY;
    fkey_t Sk(res) = R_TRANS_KEY;
    fkey_t Sk(filter) = R_TRANS_KEY;
    fol_t *xfilter=NULL;
    mdl_block_t results = mdl_push_block(FOLNIL,NULL,FOLNIL,NULL);
    Deref(filter);
    if (FOLPAIRP(filter)) {
        long l = FOL_LENGTH(filter);
        unsigned long i = 0;
        xfilter = (fol_t *) GC_MALLOC_ATOMIC(sizeof(fol_t) * (l+1));
//        dyalog_printf("loading filter %&f l=%d\n",filter,l);
        for(; FOLPAIRP(filter); filter = FOLPAIR_CDR(filter) ) {
            xfilter[i++] = FOLCMP_REF(FOLPAIR_CAR(filter),1);
        }
        xfilter[i] = NULL;
        results->filter=xfilter;
    }
    mdl_query_aux(mdl_root,values,Sk(values),results);
    for(; results->bnext ;) {
        mdl_block_t old = results;
        results = results->bnext;
        results->weight += old->weight;
        old->bnext = NULL;
        if (results->next) {
            LVALUE_X(0) = REG_VALUE(res);
            LVALUE_X(1) = REG_VALUE(Sk(res));
            LVALUE_X(2) = REG_VALUE(results->next);
            LVALUE_X(3) = REG_VALUE(DFOLFLT(old->weight));
            TRAIL_CHOICE(Mdl_Query_Next,4);
            }
        Unify_Or_Fail(results->var,results->k,results->value,Key0);
    }
    if (!results->weight)
        Fail;
    return Unify(res,Sk(res),DFOLINT(round(results->weight)),Key0);
}

void
mdl_perceptron_reset ()
{
    perceptron_count = 0;
}

void
mdl_perceptron_advance ()
{
    perceptron_count++;
    if (0 && (perceptron_count % mdl_trim_batch == 0)) {
        dyalog_printf("trimming model at %d\n",perceptron_count);
            /* we collapse the model very 10_000 sentences, to save memory */
        mdl_trim();
    }
}

void
mdl_init() 
{
    char *info;
    mdl_perceptron_reset();
    if ((info=getenv("DYALOG_MDL_TRIM_MIN"))) {
        mdl_trim_min = atof(info);
    }
    if ((info=getenv("DYALOG_MDL_TRIM_N"))) {
        mdl_trim_n = atoi(info);
    }
    if ((info=getenv("DYALOG_MDL_TRIM_BATCH"))) {
        mdl_trim_batch = atoi(info);
    }
}

