/* $Id$
 * Copyright (C) 1997, 2006 Eric de la Clergerie
 * ------------------------------------------------------------
 *
 *   Machine -- Machine dependent header file
 *
 * ------------------------------------------------------------
 * Description
 *
 * ------------------------------------------------------------
 */

#ifndef TRUE
typedef int Bool;
#define TRUE    ((Bool) 1)
#define FALSE   ((Bool) 0)
#endif

/*---------------------------------*/
/* Machine Type                    */
/*---------------------------------*/

#ifdef CONFIGURE

#   if defined(sparc)

#      define M_MACHINE            "sparc"
#      define M_sparc

#   elif defined(__mips__) && defined(sony_news)

#      define M_MACHINE            "sony_news"
#      define M_sony_news

#   elif defined(__mips__) && defined(ultrix)

#      define M_MACHINE            "dec_ultrix"
#      define M_dec_ultrix

#   elif defined(__alpha) && defined(__osf__)

#      define M_MACHINE            "dec_alpha"
#      define M_dec_alpha

#   elif defined(__linux__) && !defined(__ELF__)

#      define M_MACHINE            "pc_linux_a_out"
#      define M_pc_linux_a_out

#   elif defined(__linux__) && defined(__ELF__)

#      define M_MACHINE            "pc_linux_elf"
#      define M_pc_linux_elf

#   elif defined(__NeXT__) && defined(mc68000)

#      define M_MACHINE            "NeXT_m68k"
#      define M_NeXT_m68k

#   elif defined(__NeXT__) && defined(__hppa__)

#      define M_MACHINE            "NeXT_hppa"
#      define M_NeXT_hppa

#   else

#      error "Unsupported architecture"

#   endif

#endif

#if defined(M_pc_linux_a_out) || defined(M_pc_linux_elf)

#   define M_pc_linux

#endif


/*---------------------------------*/
/* Stacks Management               */
/*---------------------------------*/

#if defined(M_sparc) || defined(M_pc_linux)

#   define M_USE_MMAP
#   define M_MMAP_HIGH_ADR         0x0ffff000
#   define M_Check_Stacks()

#elif defined(M_dec_ultrix)

#   define M_USE_SHM
#   define M_SHM_HIGH_ADR          0x0ffff000
#   define M_Check_Stacks()

#elif defined(M_dec_alpha)

#   define M_USE_MMAP
#   define M_MMAP_HIGH_ADR         0x3f800000000ULL
#   define M_Check_Stacks()        

#else

#   define M_USE_MALLOC
#   define M_Check_Stacks()        M_Check_Magic_Words()

#endif


#if defined(M_USE_MALLOC)

#   define M_USE_MAGIC_NB_TO_DETECT_STACK_NAME
    void   M_Check_Magic_Words(void);

#endif

#define M_SECURITY_MARGIN          128                      /* in TrailWords */
#define M_MAGIC                    0x12345678


/*---------------------------------*/
/* Function Prototypes             */
/*---------------------------------*/

void      Init_Machine          (void);

void      M_Allocate_Stacks     (void);

int       M_Shell               (char *cmd);

long      M_Cpu_Time            (void);

char     *M_Get_Working_Dir     (void);
Bool      M_Set_Working_Dir     (const char *path);

char     *M_Absolute_File_Name  (const char *src);
