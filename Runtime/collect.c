/************************************************************
 * $Id$
 * Copyright (C) 1996, 2004, 2006, 2008, 2009, 2010, 2015 Eric de la Clergerie
 * ------------------------------------------------------------
 *
 *   Collect -- Collect information for collapsing
 *
 * ------------------------------------------------------------
 * Description
 *   Starting from a term, collect some information
 *   about the current environement to build a collapsed env.
 *   We also modify the current environement to close it.
 *
 *       * closure of traversed binding
 *         ( X_k -> Y_l -> t_n   => X_k -> t_n )
 *       * shifting (when possible) of traversed binding
 *         ( X_k -> t_l et Y_l -> Y_n for all Y in Var(t)
 *                 => X_k -> t_n )
 *       * copying (when possible) of small terms
 *         ( X_k -> t_l et Y_l -> rY_n for all Y in Var(t)
 *                 => X_k -> t[Y/rY]_n )
 *       * collect reachable variables X_k
 *          ( X in Var(t) for some traversed binding Y_l -> t_k )
 *       * collect entered layers k
 *          ( if at least one variable X_k is reached )
 *       * collect exited layers k
 *          ( if k is entered and a binding from k traversed )
 *       * collect renamings X_k -> Y_l
 *         (if X_k reached bound to some unreached variable Y_l)
 *
 * ------------------------------------------------------------
 */

//#define VERBOSE

#include "libdyalog.h"
#include "builtins.h"

// #define V_DISPLAY(fmt, args...)      dyalog_printf( fmt, ## args )

static void collect( Sproto );
static void light_collect( Sproto );
static binding_t find_collectable(fol_t, fkey_t);
static binding_t find_collectable_from_term( binding_t, Sproto, Sproto );

/**********************************************************************
 *      MACROs
 **********************************************************************/

#ifndef COLLECTED_P
#define COLLECTED_P(X,k) (collected_p(X,k))
#define COLLECT(X,k)     (add_collected(X,k))
//#define UN_COLLECT(X,k)  (rem_collected(X,k))
#define ADD_RENAME(X,k,Y,l) (add_rename(X,k,Y,l))
#define REM_RENAME(Y,l)  (rem_rename(Y,l))
#endif

#ifndef COPY_THRESHOLD	
#define COPY_THRESHOLD       200         
#endif

// #define SFOL_CMP(_X,_k,_Y,_l) (((long)_k -(long)_l) || ((long)_X -(long)_Y))

static long
sfol_cmp( SP(X), SP(Y) )
{
    long u=((long) Sk(X)) - ((long)Sk(Y));
    return u ? u : ((long)X-(long)Y);
}


/**********************************************************************
 *
 *                        Tagging functions
 *
 **********************************************************************/

/*
 *  add_collected(X,k)
 */
inline
static void
add_collected( const fol_t t, fkey_t k)
{
    fol_t X = FOL_DEREF_VAR( t );
    unsigned long hi = FOLVAR_HI(X);
    unsigned long lo = FOLVAR_LO_INDEX(X);
    collect_t *l;

      //V_LEVEL_DISPLAY( V_SHARE,"Collected %&f -> hi %d low %d\n",X,hi,lo);
  
    if (hi == 0)
        k->collect0 |= lo;
    else {
        for( l = &(k->collect_list); *l; l = &((*l)->next) )
            if (hi == (*l)->index) {
                (*l)->collect |= lo;
                break;
            } else if (hi < (*l)->index) {
                *l = TRAIL_COLLECT(hi,lo,*l);
                break;
            }
        if (!*l)                  /* reach end of collect list */
            *l=TRAIL_COLLECT(hi,lo,0);
    }
    
      //V_LEVEL_DISPLAY( V_SHARE,"Try Collected %d\n",COLLECTED_P(X,k));
}

/*
 *  rem_collected(X,k)
 */
/*
inline
static void
rem_collected( const fol_t t, fkey_t k)
{
  fol_t X = FOL_DEREF_VAR( t );
  unsigned long hi = FOLVAR_HI(X);
  unsigned long lo = ~FOLVAR_LO_INDEX(X);
  collect_t l;

  if (hi == 0)
      k->collect0 &= (lo | LAYER_LOC_BOUND_MASK);
  else
    for( l = k->collect_list; l; l = l->next )
      if (hi == l->index )
	{l->collect &= lo; break;}
      else if (hi > l->index )
	break;
}
*/

/*
 *  collected_p(X,k)
 */
inline
static Bool
collected_p( const fol_t t, const fkey_t k)
{
  fol_t X = FOL_DEREF_VAR( t );
  unsigned long hi = FOLVAR_HI(X);
  unsigned long lo = FOLVAR_LO_INDEX(X);
  collect_t l;

  if (hi == 0)
    return (k->collect0 & lo);
  else
    for( l = k->collect_list; l && hi >= l->index; l = l->next )
      if ( hi == l->index )
	return (l->collect & lo );
  
  return (Bool) 0;
}

/*
 *  add_rename(Y,l,X,k)
 */
inline
static void
add_rename( const fol_t t, const fkey_t k, const fol_t Y, const fkey_t l)
{
  fol_t X = FOL_DEREF_VAR(t);
  unbind_t *p = (unbind_t *)&LVALUE_C_TRAIL_UNBIND;
  long c = 0;
  rename_t *ptr;

  V_LEVEL_DISPLAY( V_SHARE,"Add rename %&p -> %&p\n",t,k,Y,l);
  
  for(; *p && (c=sfol_cmp(Y,l,(*p)->Y,(*p)->l)) < 0 ; p = &(*p)->next);
  if (!*p || c) TRAIL_UNBIND(Y,l,p);

  ptr= &(*p)->rename_list;
  for(; (*ptr) && (c=sfol_cmp(X,k,(*ptr)->X,(*ptr)->k))<0 ; ptr = &(*ptr)->next);
  if (!*ptr || c) TRAIL_RENAME(X,k,ptr);
}

/*
 *  rem_rename(Y,l)
 */
inline
static void
rem_rename( const fol_t Y, const fkey_t l)
{
    unbind_t *ptr = (unbind_t *)&LVALUE_C_TRAIL_UNBIND;
  long c=0;

  V_LEVEL_DISPLAY( V_SHARE,"Rem rename %&p\n",Y,l);
  
  for(; (*ptr) && (c=sfol_cmp(Y,l,(*ptr)->Y,(*ptr)->l)) < 0 ; ptr = &(*ptr)->next);
  if (*ptr && !c) {
    rename_t rl = (*ptr)->rename_list;
    V_LEVEL_DISPLAY( V_SHARE,"      Found\n");
    for(; rl; rl = rl->next) {
        V_LEVEL_DISPLAY( V_SHARE,"      unset %&p\n",rl->X,rl->k);
        light_collect( rl->X, rl->k);
    }
    (*ptr) = (*ptr)->next;
  }
}

/*
 * add_link k l
 *      add hard link between k and l
 */
#define MIN(_a,_b)    ((_a) < (_b) ? (_a) : (_b))
#define MAX(_a,_b)    ((_a) < (_b) ? (_b) : (_a))

inline
static void
add_link( const fkey_t k , const fkey_t l)
{
    V_LEVEL_DISPLAY(V_LOW,"add link %&k %&k\n%&t\n",k,l);

  if (k != l) {
    fkey_t min_l =  MIN(k,l);
    fkey_t max_l =  MAX(k,l);
    collapse_t c =  min_l->collapse;
    fkey_t block_start;
    
    block_start = c->block_start;

    V_LEVEL_DISPLAY(V_LOW,"1-add link\n");
    /* Nothing to update for min_l */
    /* Up to max_l */
    do {
        V_LEVEL_DISPLAY(V_LOW,"2-add link %&k\n",c->layer);
      c = c->next;
      c->block_start = block_start;
    } while (c->layer < max_l);
    V_LEVEL_DISPLAY(V_LOW,"3-add link\n");
    /* Up to end of bloc enclosing max_l */
    for(c=c->next; c && c->block_start != c->layer; c = c->next )
      c->block_start = block_start;
  }
}

/*
 *  add_keep b
 *      add b as a binding to keep (in the future environement)
 */
inline
static void 
add_keep( binding_t b )
{
    V_LEVEL_DISPLAY(V_LOW,"add_keep %x keep=%x C_TRAIL_BINDING=%x\n\t%&p -> %&p\n",
                    b,b->keep,C_TRAIL_BINDING,
                    b->binder,b->binder_key,
                    b->bindee,b->bindee_key);
    if (!b->keep) {
        b->keep = ( C_TRAIL_BINDING ) ? ( C_TRAIL_BINDING ) : b;
        LVALUE_C_TRAIL_BINDING = b;
    }
}

/*
 *  collect(X,k)
 *      X_k is reachable
 */
static void
collect(const fol_t X, fkey_t k)
{
  fol_t Y = X;
  fkey_t l = k;
  binding_t b = ONCE_UL_DEREF(Y,l);
  
        
  V_LEVEL_DISPLAY(V_LOW,"collect %&p *-> %&p\n",X,k,Y,l);
  
  COLLAPSE_MARK_ENTERED(k);     /* at least one reachable variable on k */
  COLLECT(X,k);                 /* X_k is collected */
  
  if (!b)			/* X_k is a free variable */
    REM_RENAME(X,k);            /* that can no longer be the target of a
                                 * renaming */
  
  if (b){              /* when X_k is bound : */
    if (FOLVARP(Y) && (!l->collapse || !COLLECTED_P(Y,l))) 
      /* to an uncollected variable Y_l => Y_l renaming target */
      ADD_RENAME(X,k,Y,l);

    /* not to a renaming target => k exited with a soft or hard link to l */
    else if ( LAYER_BINDING_P( b ) ) {
      COLLAPSE_MARK_LAYER_EXITED(k);
      if (l) add_link(k,l);     /* hard link when Y is not ground */
    } else {
      COLLAPSE_MARK_EXITED(k);
      add_keep( b );
    }
  }
}

/*
 *  light_collect(X,k)
 *      called to collect a variable previously involved in a renaming
 *      => X_k is bound to a collected variable
 */
static void
light_collect(const fol_t X, fkey_t k)
{
  fol_t Y = X;
  fkey_t l = k;
  binding_t b = ONCE_UL_DEREF(Y,l);
  
  
  COLLAPSE_MARK_ENTERED(k);     /* at least one reachable variable on k */
  COLLECT(X,k);                 /* X_k is collected */

  /* not to a renaming target => k exited with a soft or hard link to l */
  if ( LAYER_BINDING_P( b ) ) {
    COLLAPSE_MARK_LAYER_EXITED(k);
    if (l) {
        COLLAPSE_MARK_ENTERED(l);
        add_link(k,l);     /* hard link when Y is not ground */
    }
  } else {
    COLLAPSE_MARK_EXITED(k);
    add_keep( b );
  }
}

/**********************************************************************
 *
 *                 Traversal Collecting Functions
 *
 **********************************************************************/

typedef enum { keep, shift, copy, nothing } copy_t;

/*
 *  find_collectable_from_term X p r k
 *      X_p is initially bound to a compound term r_k
 *         *Find collectable variables from r_k
 *         *Modify X_k's binding if r_k can be shifted or copied
 *      before return, ASM_LOAD new value of X_k
 */
static binding_t
find_collectable_from_term( binding_t b, fol_t X, fkey_t p, fol_t r, fkey_t k)
{
  obj_t tuple = FOLINFO_TUPLE( r );
  Bool assigned = 0;              /* has a new layer been chosen ? */
  fkey_t new_k = k;
  copy_t flag = keep;               /* kind of copy */
  Bool loop = (FOLCMPP(r) && FOLCMP_FUNCTOR(r) == FOLLOOP);

  if (loop) {
      V_LEVEL_DISPLAY(V_LOW,"Loop in collect %&p\n",r,k);
      if (COLLECTED_P(r,k))
          goto end;
      if (!LOOP_BOUNDP(r)){
          V_LEVEL_DISPLAY(V_LOW,"WARNING: folded loop %&p\n",r,k);
      }
      COLLECT(r,k);
      collect(r,k);
  }
  
  V_LEVEL_DISPLAY(V_LOW,"find_collectable_from_term %&p *-> %&p %d\n",X,k,r,k,PAIRP(tuple));

      /* check if r_k can be shifted or copied */
  for (; PAIRP( tuple ); tuple = CDR( tuple )) {
    fol_t Y = (fol_t) CAR( tuple );
    fkey_t l = k;

    WRAP_DEREF(find_collectable,Y,l);
    
    switch ( flag ) {
    case keep :
      if ( l != k ) flag = shift;
    case shift:
      if ( Y != (fol_t) CAR(tuple) ) flag = copy;
    case copy:
      if (!FOL_GROUNDP( Y )) {
	if (!assigned)
	  { assigned = 1; new_k = l;}
	else if (l != new_k )
	  flag = nothing;
	else if (FOLCMPP(Y) && FOLCMP_DEREFP(Y))
	   /* no copy if Y bound to a deref term, */
	   /* otherwise, pb with unification */
            flag = nothing;
        else if (p==k && (fol_t)CAR(tuple) == X)
                /* no copy if infinite term */
            flag = nothing;
      }
    default:
      break;
    }
  }
  
  /* rebind X_p if possible */
  if (!FOLCMP_DEREFP(r)) {	/* no shift or copy for deref terms */
    switch (flag) {
    case copy:
      if (FOLINFO_COPY_COST( r ) > COPY_THRESHOLD) break;
      
      V_LEVEL_DISPLAY( V_SHARE, "copying %&p\n", r,k );

      r = sfol_copy(r, k);

      V_LEVEL_DISPLAY( V_SHARE, "\t --> %&p\n", r,k );
    
    case shift :
      k = new_k;
      V_LEVEL_DISPLAY( V_SHARE, "\t [shift] %&p --> %&p\n",X,p, r,k );
      b = TRAIL_UBIND( X, p, r, k);
    default:
      break;
    }
  }
  
  /* collect the variable of r_k */
  V_LEVEL_DISPLAY( V_SHARE, "collect vars from %&p -> %&p\n",X,p,r,k);
  for ( tuple=FOLINFO_TUPLE(r); PAIRP(tuple); tuple = CDR(tuple) ){
    fol_t X=(fol_t) CAR(tuple);
    V_LEVEL_DISPLAY( V_SHARE, "trying collect var %&p\n",X,k);
    if (!COLLECTED_P(X,k))  collect(X,k);
  }
  
  V_LEVEL_DISPLAY( V_SHARE, "done collect vars\n");

  end:
  
  ASM_LOAD(r,k,b);
}

/*
 *  find_collectable X k
 *      find collectable variables from X_k
 *      if X_k bound, return the deref value through ASM_LOAD
 */
static binding_t
find_collectable(X,k)
     fol_t  X;
     fkey_t k;
{
  fol_t  Y = X;
  fkey_t l = k;
  binding_t b = ONCE_UL_DEREF(Y,l);

  V_LEVEL_DISPLAY(V_LOW,"find_collectable %&p -> %&s\n",X,k,X,k);
  
  
  if (!b) {               /* X_k free => stop */
          /* if X is a free deref term, do not forget to collect its args */
      if (FOLCMPP(Y) && FOLCMP_DEREFP(Y)) {
              /* we collect if not yet done; checking avoid infinite loops
               * through $LOOP/3 */
          V_LEVEL_DISPLAY(V_LOW,"DEREF TERM FREE A\n");
          return find_collectable_from_term(b,X,k,Y,l);
      }
      ASM_LOAD_NULL;
  }
  
  if (!COLLECTED_P(X,k)){     /* X_k collected => load Y_l and stop */
    if (FOL_DEREFP(Y)) {     /* X_k bound to a var or a deref term => follow
                              * closure */
        
        if ((Bool) WRAP_DEREF(find_collectable,Y,l)){
                /* and rebind X_k if Y_l bound (but not weakly) */
            V_LEVEL_DISPLAY(V_LOW,"DEREF TERM BOUND %&p -> %&p\n",X,k,Y,l);
            b = TRAIL_UBIND(X,k,Y,l);   
        }  else if (FOLCMPP(Y)) { /* Y deref and free => Y
                                                        * compound */
                /* we collect if not yet done; checking avoid infinite loops
                 * through $LOOP/3 */
            V_LEVEL_DISPLAY(V_LOW,"DEREF TERM FREE B\n");
            return find_collectable_from_term(b,X,k,Y,l);
        }
    } else if (FOLCMPP( Y ) && !FOLCMP_GROUNDP( Y ))
            /* Y is a non-ground compound term => recursion */
            /* no need to load Y_l, done by find_collectable_from_term */
        return find_collectable_from_term(b,X,k,Y,l);
  }
  
  ASM_LOAD(Y,l,b);
}

void
wrapped_collect(S(X))
     Sdecl(X);
{
    V_LEVEL_DISPLAY(V_LOW,"Start collect\n");
    find_collectable(X,Sk(X));
    collect(X,Sk(X));
}






