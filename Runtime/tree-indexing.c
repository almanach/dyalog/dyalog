/************************************************************
 * $Id$
 * Copyright (C) 1997, 2003, 2004, 2006, 2008, 2009, 2010, 2011, 2012, 2016, 2018 Eric de la Clergerie
 * ------------------------------------------------------------
 *
 *   TreeIndexing --
 *
 * ------------------------------------------------------------
 * Description
 *      Term Indexing using Decision Trees
 *
 * Note
 *      the **DyALog Builtin** comments (for instance Unif_Retrieve2)
 *      are used by a perl extractor to build builtin.ma (see Makefile)
 * ------------------------------------------------------------
 */

#include <string.h>

//#define VERBOSE

#include "libdyalog.h"
#include "builtins.h"
#include "hash.h"
#include "tree-indexing.h"
#include <stdlib.h>

/* Following definition moved into rt.h */
/* typedef struct obj_cell *cell_t; */

struct obj_cell {
  tabobj_t object;
  cell_t   next;		
};

static unsigned long indexing_max_arity;

#define OBJ_CELL_SIZE             sizeof( struct obj_cell )

typedef enum { TABLE=1, LIST=2, FSETTABLE=4, ATOMIC=8 } entry_kind;

typedef struct table_entry_struct {
    fol_t   key;
    cell_t list;
    struct table_entry_struct *var; /* shortcut to FSet entry */
    struct table_entry_struct *fset; /* shortcut to Var entry */
    struct table_entry_struct *brother;          /* access to the tables of next brother */
    struct table_entry_struct *bnext;
    struct table_entry_struct *bprev;
    struct table_entry_struct *first;
    unsigned short     depth;
    unsigned short     son_depth;
    entry_kind kind;
} table_entry_t, *TableEntry;

static table_entry_t dyalog_entry =  { DFOLINT(0),
                                       (void *) 0,
                                       (TableEntry) 0,
                                       (TableEntry) 0,
                                       (TableEntry)0,
                                       (TableEntry) 0,
                                       (TableEntry)0,
                                       (TableEntry)0,
                                       0,
                                       -1,
                                       TABLE };
static TableEntry dyalog_table = &dyalog_entry;

#define START_TABULATION_SIZE      4
#define TYPE_SIZE                  sizeof( table_entry_t )

#define VarKey FOLVAR_FROM_INDEX(0)
#define FSetKey FOLVAR_FROM_INDEX(1)

#define HOLE_P( A )  ( A == FOLHOLE || FOL_DEREFP(A)  )

static unsigned long
fset_mask( fol_t A ) 
{
    fol_t *arg = &FOLCMP_REF(A,3);
    fol_t *stop = arg + FOLCMP_ARITY(A) - 2;
    unsigned long mask =  UCFOLINT(*arg++);
    for(; arg < stop; mask |= UCFOLINT(*arg++));
    return mask;
}

#define FSet_Find(_entry) (_entry->fset)

#define Var_Find(_entry) (_entry->var)

static
unsigned long Indexing_Arity(unsigned long arity,unsigned long depth) 
{
    long max = (indexing_max_arity-3*depth);
    return  (arity < max) ? arity : ((max > 0) ? max : 1);
}

#define INDEXING_DEREF(A,k_A,key,depth)                 \
    struct ResumeInfo *info = IP->info+depth;           \
         k_A = info->k_deref;                           \
         A = info->deref;                               \
         key = FOLCMPP(A) ? FOLCMP_FUNCTOR(A) : A;

/**********************************************************************
 * Initialization
 **********************************************************************/

DSO_LOCAL void initialization_table()
{
    char *size;
    dyalog_entry.list = (void *) Hash_Alloc_Table( START_TABULATION_SIZE, TYPE_SIZE);
    dyalog_entry.var = 0;
    dyalog_entry.fset = 0;
    dyalog_entry.brother=0;
    dyalog_entry.bnext=0;
    dyalog_entry.bprev=0;
    dyalog_entry.first=0;
    indexing_max_arity = (size = getenv("DYALOG_INDEXING_MAX_ARITY")) ? atoi(size) : INDEXING_MAX_ARITY;
}

TableEntry new_local_table () 
{
    TableEntry table = (TableEntry) GC_MALLOC(sizeof(table_entry_t));
    table->key = DFOLINT(0);
    table->list = (void *)Hash_Alloc_Table( START_TABULATION_SIZE, TYPE_SIZE);
    table->var = (TableEntry)0;
    table->fset = (TableEntry)0;
    table->brother = (TableEntry) 0;
    table->bnext = (TableEntry) 0;
    table->bprev = (TableEntry) 0;
    table->first = (TableEntry) 0;
    table->depth = 0;
    table->son_depth = -1;
    table->kind = TABLE;
    return table;
}

/**********************************************************************
 * Insertion
 **********************************************************************/
static unsigned int indexing_length;

static
table_entry_t *
add( SP(A), int depth, const Bool brother, TableEntry master)
{
    fol_t  key;
    Bool nonterminal = brother ;
    table_entry_t tmp, *entry;
    Bool atomic = 0;
    char *table = (char *) master->list;
    
  indexing_length++;

  Deref(A);
  if (HOLE_P(A)) {
      if (FOLFSETP(A)) {
              /* for fsets, we use 2-levels entries
                 the first one is indexed for FSetKey
                 the second one is indexed by key2=mask
              */
          table_entry_t tmp2, *entry2;
          V_LEVEL_DISPLAY( V_INDEX, "fset_add %&s\n", A, k_A );
//      dyalog_printf("FSET_ADD %&s\n", A, k_A );
          tmp2.key = FSetKey;
          tmp2.kind= FSETTABLE;
          tmp2.depth= depth;
          tmp2.son_depth= depth;
          tmp2.list= (void *) 0;
          tmp2.fset= (TableEntry) 0;
          tmp2.var= (TableEntry) 0;
          tmp2.brother = (TableEntry)0;
          tmp2.bnext = (TableEntry)0;
          tmp2.bprev = (TableEntry)0;
          tmp2.first = (TableEntry)0;
          entry2 = (TableEntry)  Hash_Insert( table, (char *) &tmp2, (Bool) 0);
          if ( (!entry2->list) ) {
                  /* entry is new, void and should hold a table */
              entry2->list = (void *) Hash_Alloc_Table( START_TABULATION_SIZE, TYPE_SIZE);
              master->fset = entry2;
          }
          table = (char *) entry2->list;
          key = UDFOLINT(fset_mask(A));
      } else {
          key = VarKey; 
      }
  } else if (0 && FOLNOINDEXP(A)) {
//      dyalog_printf("noindex on %&f\n",A);
      key = VarKey;
  } else if (FOLCMPP(A)) {
      nonterminal |= (depth < INDEXATION_MAX_DEPTH );
      key = FOLCMP_FUNCTOR(A);
  } else {
      atomic = 1;
      key = A;
  }
  
  V_LEVEL_DISPLAY( V_INDEX, "\tctr=%d, brother=%d, key=%&f\n",depth,brother,key);
  
  tmp.key=key;
  tmp.kind= ( nonterminal ? TABLE : LIST );
  if (atomic && nonterminal) {
      tmp.kind |= ATOMIC;
  }
  tmp.depth= depth;
  tmp.son_depth=depth;
  tmp.list= (void *) 0;
  tmp.fset=(TableEntry) 0;
  tmp.var=(TableEntry) 0;
  tmp.brother = (TableEntry)0;
  tmp.bnext = (TableEntry)0;
  tmp.bprev = (TableEntry)0;
  tmp.first = (TableEntry)0;
  entry = (TableEntry)  Hash_Insert( table, (char *) &tmp, (Bool) 0);
  
  if (!entry->list) {
          /* entry is new, void and should hold a table */
      if (nonterminal)
          entry->list = (void *) Hash_Alloc_Table( START_TABULATION_SIZE, TYPE_SIZE);
      if (key == VarKey && !master->var)
          master->var = entry;
  }

  if ( key != VarKey && depth < INDEXATION_MAX_DEPTH && FOLCMPP(A) && (!FOLCMP_DEREFP(A))) {
    /* Index on arguments of A, with incremented depth  */
    int indepth=depth+1;
    fol_t *arg  = &FOLCMP_REF(A,1);
    unsigned long arity = Indexing_Arity(FOLCMP_ARITY(A),indepth);
    fol_t *stop = arg +  arity - 1;
    TableEntry master2 = entry;
    TableEntry first_brother = 0;

    entry->son_depth = indepth;	            /* depth of entry sons is depth+1  */

    for (;
	 arg < stop ;
	 entry = add( *arg, k_A, indepth, 1 ,entry), arg++ );

    entry = add( *arg, k_A, indepth, brother, entry);
//    entry->son_depth = depth;             /* depth of last entry's sons is old depth */

    if (entry->son_depth > depth) {
        entry->son_depth = depth;             /* depth of last entry's sons is old depth */
        if (1) {
            first_brother = entry;
            for(; first_brother->first && first_brother->first != master2; first_brother = first_brother->first);
            if (!first_brother->first) {
                    /* add entry as a brother for master2
                       more precisely, entry is a table that index brother arguments of A
                       we could avoid traversing the brother list through the use of a timestamp
                    */
                first_brother->first = master2;
                first_brother->bnext = master2->brother;
                if (master2->brother)
                    master2->brother->bprev = first_brother;
                master2->brother = first_brother;
            }
        }
    }
    
 }
  
 

  return entry;

}

void
tabulation_add_aux( const tabobj_t o, TableEntry entry )
{
    fkey_t k_A  = Trail_Object( o );
    fol_t A     = TABOBJ_MODEL( o );
    
    V_LEVEL_DISPLAY( V_INDEX, "inserting %&s\n", A, k_A );
    indexing_length=0;
    {
        TableEntry anchor = (TableEntry)  add(A,k_A,0,0,entry);
        cell_t cell = (cell_t) GC_MALLOC_PRINTF("tabulation add", OBJ_CELL_SIZE );
        V_LEVEL_DISPLAY( V_INDEX, "I am here\n" );
        cell->object = o;
        cell->next   = anchor->list;
        anchor->list = cell;
        o->delete_info = (void *) &(anchor->list);
        if ((cell->next)){
            cell->next->object->delete_info=(void *)&(cell->next);
        }
    }
    
    V_LEVEL_DISPLAY( V_INDEX, "Done insertion\n" );
    Untrail_Object;
}

void
tabulation_add( const tabobj_t o) 
{
    tabulation_add_aux(o,dyalog_table);
}
    
void                                 /* Dyam Instruction */
Dyam_Tabule()                        /* object is built and stored in R_OBJECT */
{
    tabulation_add(R_OBJECT);
}

/**********************************************************************
 * Retrieval Closure mechanism
 **********************************************************************/

extern void C_To_DyALog();
extern void DyALog_Fail();
extern void DyALog_To_C();
extern void Closure_Apply();

DSO_LOCAL Bool
closure_apply(const cell_t list)
// **DyALog Builtin** Closure_Apply :- closure_apply(0)
{
    tabobj_t object = list->object;
    ClosedFn fn = * (ClosedFn *) IP->closure;
    if (list->next) {
        LVALUE_X(0)= REG_VALUE(list->next);
        update_choice(Closure_Apply,1);
    } else {
        Dyam_Remove_Choice();
    }    
    V_LEVEL_DISPLAY( V_INDEX, "closure apply\n" );
    V_LEVEL_DISPLAY( V_INDEX, "\tApplying closure to %&f\n",TABOBJ_MODEL(object));
    return (*fn)( object, IP->closure );
}

static Bool
closure_apply_start(const cell_t list) 
{
    tabobj_t object = list->object;
    ClosedFn fn = * (ClosedFn *) IP->closure;
    if (list->next) {
        LVALUE_X(0)=REG_VALUE(list->next);
        Dyam_Full_Choice(Closure_Apply,1);
    }
    V_LEVEL_DISPLAY( V_INDEX, "closure apply\n" );
    V_LEVEL_DISPLAY( V_INDEX, "\tApplying closure to %&f\n",TABOBJ_MODEL(object));
    return (*fn)( object, IP->closure );
}

/**********************************************************************
 *  Retrieval Resume and Undo mechanism
 **********************************************************************/

static HashScan *
Start_HashScan()
{
    PUSH_TRAIL_BOX(Trailed_HashScan,box);
    box->type = HASH_SCAN;
    return &(box->scan);
}

static void
Delete_Topmost_Trailed_Bloc()
{
    POP_TRAIL_BOX(LVALUE_C_TRAIL_TOP);
}

static void
Start_Retrieval(SP(A), const Closure closure)
{
    PUSH_TRAIL_BOX(Resume,box);
    box->type = INDEXATION;
    box->prev = IP;
    box->closure = closure;
    box->depth = 0;
    Deref(A);
    box->A = A;

        /* push first indexing info box for A */
    box->info[0].arg = &(box->A);
    box->info[0].stop = &(box->A)+1;
    box->info[0].k =  Sk(A) ;
    box->info[0].deref = A;
    box->info[0].k_deref= Sk(A);
    
    LVALUE_IP = REG_VALUE(box);
}

static void
Retrieval_To_DyALog(const fun_t fun, SP(A), const Closure closure, TableEntry entry )
{
    Dyam_Full_Choice(DyALog_To_C,1);
    LVALUE_X(0)=REG_VALUE(entry);
    Start_Retrieval(S(A),closure);
    LVALUE_R_CP= (fun_t) Adjust_Address(DyALog_Fail);
    LVALUE_R_P= (fun_t) Adjust_Address(fun);
    C_To_DyALog();
}

static void
Retrieval_To_DyALog_With_Cut(const fun_t fun, SP(A), const Closure closure, TableEntry entry )
{
    Dyam_Full_Choice(DyALog_To_C,1);
    LVALUE_X(0)=REG_VALUE(entry);
    Start_Retrieval(S(A),closure);
    LVALUE_R_CP=(fun_t) Adjust_Address(DyALog_Fail);
    LVALUE_R_BC=R_B;
    LVALUE_R_P= (fun_t) Adjust_Address(fun);
    V_LEVEL_DISPLAY(V_DYAM,"Start Retrieval_To_DyALog\n");
    C_To_DyALog();
}

inline
static void
resume_forward()	/* Advance in resume and possibly pop */
{
    int pop=IP->depth;
    int d=pop;
    Sdecl(A);
    struct ResumeInfo *tmp = IP->info+d;

    A = tmp->deref;
    Sk(A) = tmp->k_deref;
    
//    V_LEVEL_DISPLAY( V_INDEX, "\tforward ..." );
    while( (++(tmp->arg) == tmp->stop) && (d > 0) ){
        d--;
        tmp--;
    }

    IP->depth=d;
    pop -= d;

    if ((void *)IP < (void *) (R_B->trail)) {
//        V_LEVEL_DISPLAY(V_INDEX,"pushing undo forward box\n");
        PUSH_TRAIL_BOX(UndoForward,undo);        
        undo->type = FORWARD;
        undo->forward = pop;
        undo->deref = A;
        undo->k_deref = Sk(A);
    }

    A=*(tmp->arg);
    Sk(A) = tmp->k;
    Deref(A);
    tmp->deref=A;
    tmp->k_deref=Sk(A);
    

//    V_LEVEL_DISPLAY( V_INDEX, " -%d, depth is now %d\n", pop, IP->depth);

}

inline
static void
resume_push( SP(A) )   /* Push A in resume where A is a compound term */
{
    if ((IP->depth < INDEXATION_MAX_DEPTH) && FOLCMPP(A)) {
        int depth = ++IP->depth;
        int arity = Indexing_Arity(FOLCMP_ARITY(A),depth);        
        struct ResumeInfo *info = IP->info+depth;
        fol_t *oldarg = info->arg;
        
//        V_LEVEL_DISPLAY( V_INDEX, "\tpushing %&s to depth %d\n", A, k_A, depth);
        if ((void *) IP < (void *) (R_B->trail)) {
//            V_LEVEL_DISPLAY(V_INDEX,"pushing undo upward box\n");
            PUSH_TRAIL_BOX(UndoUpward,undo); 
            undo->type = UPWARD;
            undo->stop = info->stop;
            undo->k = info->k;
        }
        
        info->stop = (info->arg=&FOLCMP_REF(A,1))+arity;
        info->k = k_A;
        if (info->arg != oldarg) {
            A=*info->arg;
            Deref(A);
            info->deref = A;
            info->k_deref = k_A;
        }
    } else 
        resume_forward();
}

/**********************************************************************
 * Unification-based Retrieval
 **********************************************************************/

static Bool blind_retrieve3( const TableEntry, const long length, fol_t A, fkey_t k_A );
static Bool blind_retrieve( const char *, const long length, fol_t A, fkey_t k_A );

DSO_LOCAL Bool unif_retrieve2( TableEntry );
DSO_LOCAL Bool unif_retrieve3( TableEntry, Sproto);

extern void Blind_Retrieve2();
extern void Unif_Retrieve3();
extern void Unif_Retrieve2();
extern void Unif_FSet_Retrieve();
extern void Unif_FSet_Cst_Retrieve();

extern void Blind_Brother_Retrieve2();
static Bool blind_brother_retrieve(TableEntry entry);

static TableEntry
find_next_fset_mask( TableEntry entry,
                     HashScan * scan,
                     const unsigned long mask )
{
    for(;entry; entry = (TableEntry)Hash_Next(scan)) {
//        V_LEVEL_DISPLAY(V_INDEX,"next fset mask=%x key=%x key=%&f\n",mask,UCFOLINT(entry->key),entry->key);
        if (UCFOLINT(entry->key) & mask)
            return entry;
    }
    return entry;
}

Bool
unif_fset_retrieve( TableEntry entry,
                    HashScan *scan,
                    const unsigned long mask,
                    const Bool forward)
// **DyALog Builtin** Unif_FSet_Retrieve :- unif_fset_retrieve(0,1,2,3)
{
    TableEntry next = (TableEntry)Hash_Next(scan);
    if (next && mask)
        next = (TableEntry) find_next_fset_mask(next,scan,mask);
    V_LEVEL_DISPLAY( V_INDEX,"in unif_fset_retrieve entry key %&f\n",entry->key);
    if (next) {
        LVALUE_X(0)= REG_VALUE(next);
        update_choice(Unif_FSet_Retrieve,4);
        V_LEVEL_DISPLAY( V_INDEX,"Setting choice point in unif_fset_retrieve %&f\n%&t\n",next->key);
    } else {
        Dyam_Remove_Choice();
        Delete_Topmost_Trailed_Bloc();
    }
    if (forward) 
        resume_forward();
    return unif_retrieve2(entry);
}

static Bool
unif_fset_retrieve_start( TableEntry entry,
                          HashScan *scan,
                          const unsigned long mask )
{
    TableEntry next = (TableEntry)Hash_Next(scan);
    if (next && mask)
        next = (TableEntry) find_next_fset_mask(next,scan,mask);
    V_LEVEL_DISPLAY( V_INDEX,"starting unif_fset_retrieve entry key %&f\n",entry->key);
    if (next) {
        LVALUE_X(0)= REG_VALUE(next);
        LVALUE_X(1)=  REG_VALUE(scan);
        LVALUE_X(2)=  REG_VALUE(mask);
        LVALUE_X(3)=  REG_VALUE(0);
        Dyam_Full_Choice(Unif_FSet_Retrieve,4);
        V_LEVEL_DISPLAY( V_INDEX,"Setting choice point in unif_fset_retrieve %&f\n%&t\n",next->key);
    } else {
        Delete_Topmost_Trailed_Bloc();
    }
    return unif_retrieve2(entry);
}

Bool
unif_fset_cst_retrieve( TableEntry entry,
                        unsigned long value,
                        fol_t *arg,
                        fol_t *tmp_arg,
                        fol_t *stop,
                        fol_t *value_arg,
                        fol_t *value_stop,
                        char *table
                       )
// **DyALog Builtin** Unif_FSet_Cst_Retrieve :- unif_fset_cst_retrieve(0,1,2,3,4,5,6,7)
{
    Bool first = 1;
    TableEntry e = 0;
    
    for(; value_arg < value_stop ;
        value_arg++,arg=tmp_arg+FSET_BIT_PER_WORD) {
        if (!first) {
            value = UCFOLINT(*value_arg);
            tmp_arg = arg;
        }
        for(; value && arg < stop ; arg++, value >>= 1) {
            if (first) {
                first = 0;
                continue;
            }
            if (value & 1) {
                e = (TableEntry) Hash_Find(table,(long)*arg);
                if (e) {
                    LVALUE_X(0) = REG_VALUE(e);
                    LVALUE_X(1)=  REG_VALUE(value);
                    LVALUE_X(2)=  REG_VALUE(arg);
                    LVALUE_X(3)= REG_VALUE(tmp_arg);
                    LVALUE_X(5)= REG_VALUE(value_arg);
                    update_choice(Unif_FSet_Cst_Retrieve,8);
                    break;
                }
            }
        }
    }

    if (!e)
        Dyam_Remove_Choice();
     
    resume_forward();
    return unif_retrieve2(entry);
}

Bool
blind_retrieve2(const TableEntry entry, HashScan *scan,
                long length, fol_t A, fkey_t k_A)
// **DyALog Builtin** Blind_Retrieve2 :- blind_retrieve2(0,1,2,3,4)
{
    TableEntry next = (TableEntry)Hash_Next(scan);
    V_LEVEL_DISPLAY( V_INDEX,"in blind_retrieve2 entry key %&f\n",entry->key);
    if (next) {
        LVALUE_X(0)= REG_VALUE(next);
//        LVALUE_X(1)= REG_VALUE(scan);
        update_choice(Blind_Retrieve2,5);
        V_LEVEL_DISPLAY( V_INDEX,"Setting choice point in blind_retrieve2 %&f\n%&t\n",next->key);
    } else {
        Dyam_Remove_Choice();
        Delete_Topmost_Trailed_Bloc();
    }        
    return blind_retrieve3(entry,length,A,k_A);
}

DSO_LOCAL Bool
blind_retrieve2_start(const TableEntry entry,
                      HashScan *scan,
                      const long length,
                      fol_t A,
                      fkey_t k_A
                      )
{
    TableEntry next = (TableEntry)Hash_Next(scan);
    V_LEVEL_DISPLAY( V_INDEX,"starting blind_retrieve2 entry key %&f\n",entry->key);
    if (next) {
        LVALUE_X(0)= REG_VALUE(next);
        LVALUE_X(1)= REG_VALUE(scan);
        LVALUE_X(2)= REG_VALUE(length);
        LVALUE_X(3)= REG_VALUE(A);
        LVALUE_X(4)= REG_VALUE(k_A);
        Dyam_Full_Choice(Blind_Retrieve2,5);
        V_LEVEL_DISPLAY( V_INDEX,"Setting choice point in blind_retrieve2 %&f\n%&t\n",next->key);
    } else {
        Delete_Topmost_Trailed_Bloc();
    }        
    return blind_retrieve3(entry,length,A,k_A);
}

static Bool
blind_retrieve3(const TableEntry entry,
                const long length, fol_t A,fkey_t k_A)
{
    if (entry->kind & LIST){
        V_LEVEL_DISPLAY( V_INDEX, "blind_retrieve3 LIST\n");
        return entry->list && closure_apply_start(entry->list);
    } else if ((entry->kind & FSETTABLE) && entry->key == FSetKey) {
        V_LEVEL_DISPLAY( V_INDEX, "blind_retrieve3 FSETTABLE\n");
        return blind_retrieve((char *)entry->list,length+1,A,k_A);
    } else if (entry->son_depth > IP->depth) {
        V_LEVEL_DISPLAY( V_INDEX, "blind_retrieve3 TOO DEEP\n");
//        return entry->list && blind_retrieve((char *) entry->list );
        return (1 && entry->brother) ? blind_brother_retrieve(entry->brother) : blind_retrieve((char *) entry->list,length+1,A,k_A);
    } else if (1
               && length==0
               && (entry->kind & ATOMIC)
               && A != FOLHOLE) {
            /* going through a constant */
        V_LEVEL_DISPLAY( V_DYAM, "blind_retrieve at depth %d: binding %&s to %&f\n", IP->depth,A,k_A,entry->key);
        if (FOLVARP(A)) {
            TRAIL_UBIND(A,k_A,entry->key,Key0); 
        } else if (!sfol_unify(A,k_A,entry->key,Key0)) {
                /* for cases where A is dereferencable but not a variable, like
                 * a range
                 * we may have to test whether the binding is valid, through unification
                 */
            Fail;
        }
        return unif_retrieve2(entry);
    } else {
        V_LEVEL_DISPLAY( V_INDEX, "Restarting from blind_retrieve at depth %d\n", IP->depth);
        return unif_retrieve2(entry);
    }
}

static Bool
blind_retrieve( const char *table, const long length, fol_t A, fkey_t k_A )
{
    if (table) {
        HashScan *scan=Start_HashScan();
        TableEntry entry = (TableEntry)Hash_First(table,scan);
        return entry && blind_retrieve2_start(entry,scan,length,A,k_A);
     } else
        Fail;
}



Bool
blind_brother_retrieve2(TableEntry entry)
// **DyALog Builtin** Blind_Brother_Retrieve2 :- blind_brother_retrieve2(0)
{
    if (entry->bnext) {
        LVALUE_X(0)=REG_VALUE(entry->bnext);
        update_choice(Blind_Brother_Retrieve2,1);
    } else {
        Dyam_Remove_Choice();
    }
    return entry->list && blind_retrieve3(entry,1,FOLHOLE,Key0);
}

static Bool
blind_brother_retrieve(TableEntry entry) 
{
    if (entry->bnext) {
        LVALUE_X(0)=REG_VALUE(entry->bnext);
        Dyam_Full_Choice(Blind_Brother_Retrieve2,1);
    }
    return entry->list && blind_retrieve3(entry,1,FOLHOLE,Key0);
}


Bool
unif_retrieve2( TableEntry entry)
// **DyALog Builtin** Unif_Retrieve2 :- unif_retrieve2(0)
{
    while (1) {
        V_LEVEL_DISPLAY( V_INDEX, "Unif retrieve\n" );
            // assert( !entry || entry->depth == depth );
        if (entry->kind & LIST) {
            V_LEVEL_DISPLAY( V_INDEX, "\tentry info key=%&f, depth=%d, kind=%d\n", entry->key ,
                       entry->depth, entry->kind );
            return entry->list && closure_apply_start(entry->list);
//        } else if ( entry->kind & FSETTABLE ) {
//            V_LEVEL_DISPLAY( V_INDEX,"SHOULD NOT BE HERE %&f\n",entry->key);
//            Fail;
        } else if ( IP->depth >= 0) {
            int depth = IP->depth;
            char *table = (char *) entry->list;
            fol_t  key ;
            TableEntry e1,e2;
            TableEntry e3= (TableEntry)0; /* used for fset */
            unsigned long mask = 0;            /* used for fset */
            fol_t A;
            fkey_t k_A;

            INDEXING_DEREF(A,k_A,key,depth);
            
            V_LEVEL_DISPLAY( V_INDEX, "\tentry info key=%&f, depth=%d, kind=%d\n", entry->key , entry->depth, entry->kind );
            V_LEVEL_DISPLAY( V_INDEX, "\tcurrent term is %&f\n", A );
//            dyalog_printf("\tentry info key=%&f, depth=%d, kind=%d ipdepth=%d deref_key=%&f term=%&f\n", entry->key , entry->depth, entry->kind,depth,key,A);
            
            if (FOLFSETP(A)){
                fol_t fset_table = FOLCMP_REF(A,2);
                fol_t *arg = &FOLCMP_REF(fset_table,1);
                fol_t *tmp_arg = arg;
                fol_t *stop = arg+FOLCMP_ARITY(fset_table);
                fol_t *value_arg = &FOLCMP_REF(A,3);
                fol_t *value_stop = value_arg+FOLCMP_ARITY(A)-2;
                V_LEVEL_DISPLAY( V_INDEX, "\t IS_FSET %&f\n",A);
                for(; value_arg < value_stop ;
                    value_arg++, arg=tmp_arg+FSET_BIT_PER_WORD) {
                    unsigned long value = UCFOLINT(*value_arg);
                    tmp_arg = arg;
                    for(; value && arg < stop ; arg++, value >>= 1) {
                        if (value & 1) {
                            TableEntry e = (TableEntry) Hash_Find(table,(long)*arg);
                            if (e) {
                                LVALUE_X(0) = REG_VALUE(e);
                                LVALUE_X(1)=  REG_VALUE(value);
                                LVALUE_X(2)=  REG_VALUE(arg);
                                LVALUE_X(3)= REG_VALUE(tmp_arg);
                                LVALUE_X(4)= REG_VALUE(stop);
                                LVALUE_X(5)= REG_VALUE(value_arg);
                                LVALUE_X(6)= REG_VALUE(value_stop);
                                LVALUE_X(7)= REG_VALUE(table);
                                Dyam_Full_Choice(Unif_FSet_Cst_Retrieve,8);
//                                break;
                                goto after_fset;
                            }
                        }
                    }
                }
              after_fset:
                if ((e3 = (TableEntry) FSet_Find(entry)))
                    mask = fset_mask(A);
            } else if (HOLE_P(A)) {                   /* try recursively all entries of table */
                V_LEVEL_DISPLAY( V_INDEX, "\tblind_retrieve\n");
                resume_forward();
                return blind_retrieve(table,0,A,k_A);
            } else if (FOLSMBP(A)) {
                fsetelt_t fsets = FOLSMB_FSETELT(A);
                if (fsets && (e3 = (TableEntry) FSet_Find(entry))) {
                    mask = fsets->unif_mask;
                }
            } else if (FOL_CSTP(A)) {
                e3 = (TableEntry) FSet_Find(entry);
            }

                /* Try variable entry in table */
            e1 = (TableEntry) Var_Find(entry);
            e2 = (TableEntry) Hash_Find(table,(long) key);

            
//            if (e3) V_LEVEL_DISPLAY( V_INDEX,"proto unif_fset_retrieve in unif_retrieve2 %&f mask=%x e1=%x e2=%x\n",e3->key,mask,e1,e2);
            
            if (e3 && (e1 || e2)) {
                HashScan *scan=Start_HashScan();
                TableEntry e4 = (TableEntry)Hash_First((char *)(e3->list),scan);
//                V_LEVEL_DISPLAY(V_INDEX,"Found1 mask=%x e4=%x\n",mask,e4);
                if (mask && e4) 
                    e4=find_next_fset_mask(e4,scan,mask);
//                V_LEVEL_DISPLAY(V_INDEX,"Found2 mask=%x e4=%x\n",mask,e4);
                if (e4) {
                    V_LEVEL_DISPLAY( V_INDEX,"Setting choice point for fset_retrieve in unif_retrieve2 %&f\n",e3->key);
                    LVALUE_X(0) = REG_VALUE(e4);
                    LVALUE_X(1)=  REG_VALUE(scan);
                    LVALUE_X(2)=  REG_VALUE(mask);
                    LVALUE_X(3)= REG_VALUE(1);
                    Dyam_Full_Choice(Unif_FSet_Retrieve,4);
                } else {
                    Delete_Topmost_Trailed_Bloc();
                }
            }
            
            if (e1) {
                if (e2) {
                    V_LEVEL_DISPLAY( V_INDEX,"Setting choice point in unif_retrieve2 %&f\n",e2->key);
                    LVALUE_X(0) = REG_VALUE(e2);
                    LVALUE_X(1)=  REG_VALUE(A);
                    LVALUE_X(2)=  REG_VALUE(Sk(A));
                    Dyam_Full_Choice(Unif_Retrieve3,3);
                }
//                V_LEVEL_DISPLAY( V_INDEX,"<>E1\n");
                entry= e1;
                resume_forward(); 
            } else if (e2) {
//                V_LEVEL_DISPLAY( V_INDEX,"<>E2\n");
                entry= e2;
                resume_push(S(A));
            } else if (e3) {
                    /* the forward should come before the HashScan
                       to ensure the HashScan trail box is the topmost one
                     */
//                V_LEVEL_DISPLAY( V_INDEX,"<>E3\n");
                resume_forward();
                {
                    HashScan *scan=Start_HashScan();
                    TableEntry e4 = (TableEntry)Hash_First((char *)(e3->list),scan);
                    if (mask && e4)
                        e4=find_next_fset_mask(e4,scan,mask);
                    if (!e4) {
//                        V_LEVEL_DISPLAY( V_INDEX,"Fail\n");
                        Fail;
                    }
                    V_LEVEL_DISPLAY( V_INDEX, "\tunif_fset_retrieve\n");
                    return unif_fset_retrieve_start(e4,scan,mask);
                }
            } else {
//                V_LEVEL_DISPLAY( V_INDEX,"Fail\n");
                Fail;
            }
        }
    }
}

Bool
unif_retrieve3(TableEntry entry, SP(A))
// **DyALog Builtin** Unif_Retrieve3 :- unif_retrieve3(0,1,2)
{
    Dyam_Remove_Choice();
    resume_push(S(A));
    return unif_retrieve2(entry);
}

void
unif_retrieve( SP(A), const Closure closure )
{
    V_LEVEL_DISPLAY( V_INDEX,"UNIF RETRIEVE %&s\n",S(A));
    if (dyalog_table) {
        Retrieval_To_DyALog((fun_t)Unif_Retrieve2,S(A),closure,dyalog_table);
        V_LEVEL_DISPLAY( V_INDEX,"END UNIF RETRIEVE\n");
    }
}

Bool
unif_retrieve_alt( SP(A), const Closure closure )
{
    if (dyalog_table) {
        Start_Retrieval(S(A),closure);
        return unif_retrieve2(dyalog_table);
    } else {
        Fail;
    }
}

Bool
unif_retrieve_general( SP(A), const Closure closure, TableEntry entry)
{
    if (entry) {
        Start_Retrieval(S(A),closure);
        return unif_retrieve2(entry);
    } else {
        Fail;
    }
}

/**********************************************************************
 * Subsumption-based  Retrieval
 **********************************************************************/

DSO_LOCAL Bool subs_retrieve2(TableEntry entry);
DSO_LOCAL Bool subs_retrieve3(TableEntry entry, Sproto);

static TableEntry
find_next_subs_fset_mask( TableEntry entry,
                          HashScan * scan,
                          const unsigned long mask )
{
    for(;entry; entry = (TableEntry)Hash_Next(scan)) {
        if ((UCFOLINT(entry->key) & mask) == mask) {
            return entry;
        }
    }
    return entry;
}


extern void Subs_Retrieve3();
extern void Subs_Retrieve2();
extern void Subs_FSet_Retrieve();

Bool
subs_fset_retrieve( TableEntry entry,
                    HashScan *scan,
                     unsigned long mask )
// **DyALog Builtin** Subs_FSet_Retrieve :- subs_fset_retrieve(0,1,2)
{
    TableEntry next = (TableEntry)Hash_Next(scan);
    if (mask && next)
        next = (TableEntry) find_next_subs_fset_mask(next,scan,mask);
//    V_LEVEL_DISPLAY( V_INDEX,"in subs_fset_retrieve entry key %&f\n",entry->key);
    if (next) {
        LVALUE_X(0)= REG_VALUE(next);
        update_choice(Subs_FSet_Retrieve,3);
//        V_LEVEL_DISPLAY( V_INDEX,"Setting choice point in subs_fset_retrieve %&f\n%&t\n",next->key);
    } else {
        Dyam_Remove_Choice();
        Delete_Topmost_Trailed_Bloc();
    }        
    resume_forward();
    return subs_retrieve2(entry);
}

static Bool
subs_fset_retrieve_start( TableEntry entry,
                          HashScan *scan,
                          unsigned long mask )
{
    TableEntry next = (TableEntry)Hash_Next(scan);
    if (mask && next)
        next = (TableEntry) find_next_subs_fset_mask(next,scan,mask);
//    V_LEVEL_DISPLAY( V_INDEX,"starting blind_retrieve2 entry key %&f\n",entry->key);
    if (next) {
        LVALUE_X(0)= REG_VALUE(next);
        LVALUE_X(1)=  REG_VALUE(scan);
        LVALUE_X(2)=  REG_VALUE(mask);
        Dyam_Full_Choice(Subs_FSet_Retrieve,3);
//        V_LEVEL_DISPLAY( V_INDEX,"Setting choice point in unif_fset_retrieve %&f\n%&t\n",next->key);
    } else {
        Delete_Topmost_Trailed_Bloc();
    }
    resume_forward();
    return subs_retrieve2(entry);
}

Bool
subs_retrieve2(TableEntry entry)
// **DyALog Builtin** Subs_Retrieve2 :- subs_retrieve2(0)
{
    while (1) {
//        V_LEVEL_DISPLAY( V_INDEX, "Subs retrieve\n" );
        
            // assert( !entry || entry->depth == depth );
        
        if (entry->kind & LIST) {
            V_LEVEL_DISPLAY( V_INDEX, "\tentry info key=%&f, depth=%d, kind=%d\n", entry->key ,
                       entry->depth, entry->kind );
            return entry->list && closure_apply_start(entry->list);
        } else if ( IP->depth >= 0 ) {
            int depth=IP->depth;
            fol_t A = *(IP->info[depth].arg);
            fkey_t k_A = IP->info[depth].k;
            char *table = (char *) entry->list;
            TableEntry e1= (TableEntry) 0;
            TableEntry e2= (TableEntry) 0;
            TableEntry e3= (TableEntry) 0; /* used for fset */
            unsigned long mask = 0;            /* used for fset */

            Deref(A);
            
//            V_LEVEL_DISPLAY( V_INDEX, "\tentry info key=%&f, depth=%d, kind=%d\n", entry->key , entry->depth, entry->kind );
//            V_LEVEL_DISPLAY( V_INDEX, "\tcurrent term is %&f\n", A );

            e1 = (TableEntry) Var_Find(entry);

            if (FOLFSETP(A)) {
                if ((e3 = (TableEntry) FSet_Find(entry)))
                    mask = fset_mask(A);
            } else if (!HOLE_P(A)) {
                    
                e2 = (TableEntry) Hash_Find(table,
                                                // the key
                                            (long) (FOLCMPP(A) ? FOLCMP_FUNCTOR(A) : A));

                if (FOLSMBP(A)) {
                    fsetelt_t fsets = FOLSMB_FSETELT(A);
                    if (fsets && (e3 = (TableEntry) FSet_Find(entry))) {
                        mask = fsets->subs_mask;
                    }
                } else if (FOL_CSTP(A)) {
                    /* don't know how to associate a specific finite set mask
                     * for non symbolic constants
                     */
                    e3 = (TableEntry) FSet_Find(entry);
                }

            }
            
            if (e3 && (e1 || e2)) {
                HashScan *scan=Start_HashScan();
                TableEntry e4 = (TableEntry)Hash_First((char *)(e3->list),scan);
                if (e4 && mask) 
                    e4=find_next_fset_mask(e4,scan,mask);
                if (e4) {
//                    V_LEVEL_DISPLAY( V_INDEX,"Setting choice point in subs_fset_retrieve %&f\n",e3->key);
                    LVALUE_X(0) = REG_VALUE(e4);
                    LVALUE_X(1)=  REG_VALUE(scan);
                    LVALUE_X(2)=  REG_VALUE(mask);
                    Dyam_Full_Choice(Subs_FSet_Retrieve,3);
                }
            }
            
                /* Try variable entry in table */
            if (e1) {
                if (e2) {
//                    V_LEVEL_DISPLAY( V_INDEX,"Setting choice point in unif_retrieve2 %&f\n",e2->key);
                    LVALUE_X(0) = REG_VALUE(e2);
                    LVALUE_X(1) = REG_VALUE(A);
                    LVALUE_X(2) = REG_VALUE(Sk(A));
                    Dyam_Full_Choice(Subs_Retrieve3,3);
                }
                entry=e1;
                resume_forward();
            } else if (e2) {
                entry=e2;
                resume_push(S(A));
            } else if (e3) {    /* but neither e1 or e2 */
                HashScan *scan=Start_HashScan();
                TableEntry e4 = (TableEntry)Hash_First((char *)(e3->list),scan);
                if (e4 && mask)
                    e4=find_next_subs_fset_mask(e4,scan,mask);
                if (!e4) {
//                    V_LEVEL_DISPLAY( V_INDEX,"Fail\n");
                    Fail;
                }
//                V_LEVEL_DISPLAY( V_INDEX, "\tsubs_fset_retrieve\n");
                return subs_fset_retrieve_start(e4,scan,mask);
            } else {
                Fail;
            }
        }
    }
}

Bool
subs_retrieve3(TableEntry entry, SP(A))
// **DyALog Builtin** Subs_Retrieve3 :- subs_retrieve3(0,1,2)
{
    Dyam_Remove_Choice();
    resume_push(S(A));
    return subs_retrieve2(entry);
}

void
subs_retrieve( SP(A), const Closure closure )
{
    V_LEVEL_DISPLAY( V_INDEX,"SUBS RETRIEVE %&s\n",S(A));
    if (dyalog_table) {
        Retrieval_To_DyALog((fun_t)Subs_Retrieve2,S(A),closure,dyalog_table);
    }
}

/**********************************************************************
 * Variance-Based Retrieval
 ***********************************************************************/

extern void Variance_Retrieve2();

DSO_LOCAL Bool
variance_retrieve2( TableEntry entry )
        // **DyALog Builtin** Variance_Retrieve2 :- variance_retrieve2(0)
{
    while (entry) {
        
        V_LEVEL_DISPLAY( V_INDEX, "Variance retrieve\n" );
            // assert( !entry || entry->depth == depth );

        if (entry->kind & LIST) {
            V_LEVEL_DISPLAY( V_INDEX, "\tentry info key=%&f, depth=%d, kind=%d\n", entry->key ,
                       entry->depth, entry->kind );
            return entry->list && closure_apply_start(entry->list);
        } else if ( IP->depth >= 0 ) {
            int depth=IP->depth;
            char *table = (char *) entry->list;
            fol_t key;
            fol_t A;
            fkey_t k_A;

            INDEXING_DEREF(A,k_A,key,depth);
 
            V_LEVEL_DISPLAY( V_INDEX, "\tentry info key=%&f, depth=%d, kind=%d\n", entry->key , entry->depth, entry->kind );
            V_LEVEL_DISPLAY( V_INDEX, "\tcurrent term is %&f\n", A );


                /* Try variable entry in table */

            if (FOLFSETP(A)) {
                entry = (TableEntry) FSet_Find(entry);
                if (!entry)
                    Fail;
                entry = (TableEntry) Hash_Find((char *)entry->list,
                                               (long)UDFOLINT(fset_mask(A)));
                resume_forward();
            } else if (HOLE_P(A)) {
                entry = (TableEntry) Var_Find(entry);
                resume_forward();
            } else {
                entry = (TableEntry) Hash_Find(table,(long) key);
                resume_push(S(A));
            }
        }
    }
    Fail;
}


void
variance_retrieve( SP(A), const Closure closure )
{
    V_LEVEL_DISPLAY( V_INDEX,"VARIANCE RETRIEVE %&s\n",S(A));
    if (dyalog_table)
        Retrieval_To_DyALog((fun_t)Variance_Retrieve2,S(A),closure,dyalog_table);
}

/**********************************************************************
 * Standard DyALog Indexation-based functions
 **********************************************************************/

				/* Application  */

typedef struct application_closure {
    void     *fn;
    long      id;
    fol_t     type;
    fun_t     code;
    tabobj_t  transition;
    fkey_t    k;
} application_closure;

Bool treat_item( tabobj_t, long, fol_t, fun_t, tabobj_t, fkey_t );

static Bool
closed_application( tabobj_t obj, struct application_closure *argv )
{
    return treat_item(obj, argv->id, argv->type, argv->code, argv->transition,argv->k);
}


Bool                            /* DYAM INSTRUCTION Dyam_Apply */
Dyam_Apply(int item_comp_id, int trans_comp_id)
{
    tabseed_t seed = R_TRANS->seed;
    void **comptab = ((void **)&seed->application_code+1);
    trans_comp_t comp = ((trans_comp_t) comptab)+trans_comp_id;
    fol_t model = comp->model;
    fun_t code = comp->code;
    fol_t type = seed->type;
    Closure closure = (Closure) PUSH_TRAIL_BLOCK(sizeof(struct application_closure));
    *(struct application_closure *)closure = (struct application_closure)
        { (void *)&closed_application,
              item_comp_id,
              type,
              code,
              R_TRANS,
              R_TRANS_KEY };
    
    STAT_UPDATE(loop, sl_direct_applications);
    V_LEVEL_DISPLAY( V_DYAM,"  [%&e] Apply %&s %d\n",model,R_TRANS_KEY,item_comp_id);
    return unif_retrieve_alt(model,R_TRANS_KEY,closure);
}


				/* Completion */

struct completion_closure {
    void *fn;
    long  id;
    fol_t type;
    tabobj_t item;
    fkey_t item_key;
    fol_t item_comp;
};

Bool treat_transition( tabobj_t, long, fol_t );

static Bool
closed_completion( tabobj_t obj, struct completion_closure *argv )
{
    R_LOAD_ITEM( argv->item, argv->item_key, argv->item_comp);
    return treat_transition( obj, argv->id, argv->type );
}

Bool                            /* DYAM INSTRUCTION Dyam_Complete */
Dyam_Complete(int trans_comp_id, int item_comp_id )
{
    tabseed_t seed = R_TRANS->seed;
    void **comptab = ((void **)&seed->application_code+1);
    item_comp_t comp = ((item_comp_t) comptab)+item_comp_id;
    fol_t model = comp->model;
    fol_t item_comp = comp->item_comp;
    fol_t type = seed->type;
    struct completion_closure *closure =
        (struct completion_closure *)
        PUSH_TRAIL_BLOCK(sizeof(struct completion_closure));
    *closure = (struct completion_closure) { (void *) &closed_completion,
                                             trans_comp_id,
                                             type,
                                             R_TRANS,
                                             R_TRANS_KEY,
                                             item_comp
    };

    STAT_UPDATE(loop, sl_reverse_applications);
    V_LEVEL_DISPLAY( V_DYAM,"  [%&e] Complete %&s %d\n",model,R_TRANS_KEY,trans_comp_id);
    return unif_retrieve_alt( model, R_TRANS_KEY, closure);
}

				/* Subsumption */

struct subsumption_closure {
    void   *fn;
    fol_t   A;
    fkey_t  Sk(A);
};

static Bool subs_result;

Bool treat_generalizer( tabobj_t, fol_t, fkey_t, Bool );

Bool
closed_subsumption( tabobj_t obj, struct subsumption_closure *argv )
{
    V_LEVEL_DISPLAY( V_DYAM, "Calling treat_generalizer %&f wrt %&f\n", TABOBJ_SUBS(obj), argv->A );
    if (treat_generalizer( obj, argv->A,argv->Sk(A), 1 )) {
        subs_result=1;
//        V_LEVEL_DISPLAY( V_DYAM, "\tFast exit!\n");
        LVALUE_R_B= REG_VALUE(R_BC);
        LVALUE_R_BC=REG_VALUE(R_BC->bc);
        V_LEVEL_DISPLAY( V_DYAM, "---------------------\n");
    }
    Succeed;
}

Bool
closed_variance( tabobj_t obj, struct subsumption_closure *argv )
{
    V_LEVEL_DISPLAY( V_DYAM, "Calling treat_variance %&f wrt %&f\n", TABOBJ_SUBS(obj), argv->A );
    if (treat_generalizer( obj, argv->A,argv->Sk(A), 0 )) {
        subs_result=1;
//        V_LEVEL_DISPLAY( V_DYAM, "\tFast exit!\n");
        LVALUE_R_B= REG_VALUE(R_BC);
        LVALUE_R_BC=REG_VALUE(R_BC->bc);
        V_LEVEL_DISPLAY( V_DYAM, "---------------------\n");
    }
    Succeed;
}

Bool                            /* DYAM INSTRUCTION Dyam_Subsume */
Dyam_Subsume( tabseed_t seed )
{
    fol_t A=seed->subs_comp;
    fkey_t Sk(A)=R_TRANS_KEY;
    V_LEVEL_DISPLAY( V_DYAM,"  [%&e] Subsumption %&s\n",A,Sk(A));
    if (dyalog_table) {
        struct subsumption_closure *closure=
            (struct subsumption_closure *)
            PUSH_TRAIL_BLOCK(sizeof(struct subsumption_closure));
        *closure = (struct subsumption_closure)
            { (void *)&closed_subsumption,
                  A,
                  Sk(A)
                  };
        V_LEVEL_DISPLAY( V_INDEX,"SUBS RETRIEVE %&s\n",S(A));
        subs_result=0;
        Retrieval_To_DyALog_With_Cut((fun_t)Subs_Retrieve2, S(A), closure, dyalog_table );
        V_LEVEL_DISPLAY( V_INDEX,"Returned from Subs\n");
        STAT_UPDATE(loop,sl_subsumptions);
        return subs_result;
    } else
        Fail;
}

Bool                            /* DYAM INSTRUCTION Dyam_Variance */
Dyam_Variance( tabseed_t seed )
{
    fol_t A=seed->subs_comp;
    fkey_t Sk(A)=R_TRANS_KEY;
    V_LEVEL_DISPLAY( V_DYAM,"  [%&e] Variance %&s\n",A,Sk(A));
    if (dyalog_table) {
        struct subsumption_closure *closure=
            (struct subsumption_closure *)
            PUSH_TRAIL_BLOCK(sizeof(struct subsumption_closure));
        *closure = (struct subsumption_closure)
            { (void *)&closed_variance,
                  A,
                  Sk(A)
                  };
        V_LEVEL_DISPLAY( V_INDEX,"VARIANCE RETRIEVE %&s\n",S(A));
        subs_result=0;
        Retrieval_To_DyALog_With_Cut((fun_t)Variance_Retrieve2, S(A), closure, dyalog_table );
        V_LEVEL_DISPLAY( V_INDEX,"Returned from Variance\n");
        return subs_result;
    } else
        Fail;
}



/**********************************************************************
 * Delete
 *      Delete every object unifiable with A_kA
 **********************************************************************/

DSO_LOCAL Bool unif_delete2(TableEntry,TableEntry);
DSO_LOCAL Bool unif_delete3(TableEntry, Sproto, TableEntry);
static Bool blind_delete3( TableEntry, TableEntry );
static Bool blind_delete( TableEntry );

extern void Blind_Delete2();
extern void Unif_Delete2();
extern void Unif_Delete3();

extern void Unif_FSet_Delete();

struct delete_closure {
  fol_t   A;
  fkey_t  k;
};

static void
delete_apply( cell_t *list, const Closure closure )
{
    Sdecl(A);

    A = ((struct delete_closure *) closure)->A;
    Sk(A) = ((struct delete_closure *) closure)->k;
    V_LEVEL_DISPLAY( V_INDEX, "entering delete apply %&s\n",A,Sk(A));
    for(;*list;){
        tabobj_t o = (*list)->object;
        fkey_t k = Trail_Object( o );
        fol_t  t = TABOBJ_MODEL( o );
        V_LEVEL_DISPLAY( V_INDEX, "entering delete apply trying\n",t,k);
        if (sfol_unify( A, Sk(A), t, k )) {
            *list = (*list)->next;
        } else
            list = &(*list)->next;
        Untrail_Object;
    }
}

static void
clean_entry_aux(TableEntry entry) 
{
    if (entry->bprev) {
        entry->bprev->bnext = entry->bnext;
    } else if (entry->first) {
        entry->first->brother = entry->bnext;
    }
    if (entry->bnext) 
        entry->bnext->bprev = entry->bprev;
    
    entry->first=0;
    entry->bnext=0;
    entry->brother=0;
    entry->bprev=0;
}

        
static void
clean_entry(TableEntry parent, TableEntry entry)
{
    V_LEVEL_DISPLAY( V_INDEX,"cleaning %x %&f %x %x\n",parent,entry ? entry->key
                     : FOLNIL,entry, entry && entry->list);
        /* TMP: To reactivate as soon as possible */
    
    if (parent && !entry->list){

        clean_entry_aux(entry);
                
        Hash_Delete((char *)parent->list,(long)entry->key);
        if (!Hash_Nb_Elements((char *)parent->list)){
            parent->list=0;
            parent->var=0;
            parent->fset=0;
            clean_entry_aux(parent);
        }
    }

}

static void
clean_entry_alt(TableEntry parent, TableEntry entry)
{
    V_LEVEL_DISPLAY( V_INDEX,"cleaning %x %&f %x %x\n",parent,entry ? entry->key
                     : FOLNIL,entry, entry && entry->list);
        /* TMP: To reactivate as soon as possible */
    if (parent && !entry->list){
        
        clean_entry_aux(entry);
        
        Hash_Delete((char *)parent->list,(long)entry->key);
        if (!Hash_Nb_Elements((char *)parent->list)){
            parent->list=0;
            parent->var=0;
            parent->fset=0;
            clean_entry_aux(parent);
        }
    }
}


Bool
unif_fset_delete( TableEntry entry,
                  HashScan *scan,
                  unsigned long mask,
                  Bool forward,
                  TableEntry parent
                  )
// **DyALog Builtin** Unif_FSet_Delete :- unif_fset_delete(0,1,2,3,4)
{
    TableEntry next = (TableEntry)Hash_Next(scan);
    if (next && mask)
        next = (TableEntry) find_next_fset_mask(next,scan,mask);
    V_LEVEL_DISPLAY( V_INDEX,"in unif_fset_delete entry key %&f\n",entry->key);
    if (next) {
        LVALUE_X(0)= REG_VALUE(next);
        update_choice(Unif_FSet_Delete,5);
        V_LEVEL_DISPLAY( V_INDEX,"Setting choice point in unif_fset_delete %&f\n%&t\n",next->key);
    } else {
        Dyam_Remove_Choice();
        Delete_Topmost_Trailed_Bloc();
    }
    if (forward) 
        resume_forward();
    return unif_delete2(entry,parent);
}

static Bool
unif_fset_delete_start( TableEntry entry,
                        HashScan *scan,
                        unsigned long mask,
                        TableEntry parent )
{
    TableEntry next = (TableEntry)Hash_Next(scan);
    if (next && mask)
        next = (TableEntry) find_next_fset_mask(next,scan,mask);
    V_LEVEL_DISPLAY( V_INDEX,"starting unif_fset_delete entry key %&f\n",entry->key);
    if (next) {
        LVALUE_X(0)= REG_VALUE(next);
        LVALUE_X(1)=  REG_VALUE(scan);
        LVALUE_X(2)=  REG_VALUE(mask);
        LVALUE_X(3)=  REG_VALUE(0);
        LVALUE_X(4)=  REG_VALUE(parent);
        Dyam_Full_Choice(Unif_FSet_Delete,5);
        V_LEVEL_DISPLAY( V_INDEX,"Setting choice point in unif_fset_delete %&f\n%&t\n",next->key);
    } else {
        Delete_Topmost_Trailed_Bloc();
    }
    return unif_delete2(entry,parent);
}


static Bool
blind_delete3(TableEntry entry,TableEntry parent)
{
    if (entry->kind & LIST){
        delete_apply((cell_t *) &(entry->list),IP->closure);
        clean_entry(parent,entry);
    } else if (entry->kind & FSETTABLE) {
        blind_delete(entry);
        clean_entry(parent,entry);
    } else if (entry->son_depth > IP->depth) {
        blind_delete(entry);
        clean_entry(parent,entry);
    } else {
        V_LEVEL_DISPLAY( V_INDEX, "Restarting from blind_retrieve at depth %d\n", IP->depth);
        unif_delete2(entry,parent);
    }
    Succeed;
}

Bool
blind_delete2(TableEntry entry, HashScan *scan, TableEntry parent)
        // **DyALog Builtin** Blind_Delete2 :- blind_delete2(0,1,2)
{
    TableEntry next = (TableEntry)Hash_Next(scan);
    V_LEVEL_DISPLAY( V_INDEX,"in blind_delete2 entry key %&f\n",entry->key);
    if (next) {
        V_LEVEL_DISPLAY( V_INDEX,"Setting choice point in blind_delete2 %&f\n",next->key);
        LVALUE_X(0)=REG_VALUE(next);
//        LVALUE_X(1)=REG_VALUE(scan);
//        LVALUE_X(2)=REG_VALUE(parent);
        update_choice(Blind_Delete2,3);
    } else {
        Dyam_Remove_Choice();
        Delete_Topmost_Trailed_Bloc();
    }        
    return blind_delete3(entry,parent);
}

DSO_LOCAL Bool
blind_delete2_start(TableEntry entry, HashScan *scan,TableEntry parent)
{
    TableEntry next = (TableEntry)Hash_Next(scan);
    V_LEVEL_DISPLAY( V_INDEX,"in blind_delete2_start entry key %&f\n",entry->key);
    if (next) {
        V_LEVEL_DISPLAY( V_INDEX,"Setting choice point in blind_delete2 %&f\n",next->key);
        LVALUE_X(0)=REG_VALUE(next);
        LVALUE_X(1)=REG_VALUE(scan);
        LVALUE_X(2)=REG_VALUE(parent);
        TRAIL_CHOICE(Blind_Delete2,3);
    } else {
        Delete_Topmost_Trailed_Bloc();
    }        
    return blind_delete3(entry,parent);
}

static Bool
blind_delete( TableEntry parent)
{
    if (parent && parent->list) {
        HashScan *scan=Start_HashScan();
        TableEntry entry= (TableEntry) Hash_First((char *)parent->list,scan);
    
        return entry && blind_delete2_start(entry,scan,parent);
    } else
        Fail;
}

Bool
unif_delete2(TableEntry entry,TableEntry parent)
        // **DyALog Builtin** Unif_Delete2 :- unif_delete2(0,1)
{
    V_LEVEL_DISPLAY( V_INDEX, "Unif delete\n" );
        // assert( !entry || entry->depth == depth );
    
    if (entry->kind & LIST) {
        V_LEVEL_DISPLAY( V_INDEX, "\tentry info key=%&f, depth=%d, kind=%d\n", entry->key , entry->depth, entry->kind );
        delete_apply((cell_t *) &(entry->list),IP->closure);
        clean_entry(parent,entry);
        Succeed;
    } else if (entry->kind & FSETTABLE) {
        V_LEVEL_DISPLAY( V_INDEX, "SHOULD NOT BE HERE %&f\n",entry->key);
        Fail;
    } else if ( IP->depth >= 0) {
        int depth=IP->depth;
        char *table = (char *) entry->list;
        fol_t  key ;
        TableEntry e1,e2;
        TableEntry e3= (TableEntry)0; /* used for fset */
        unsigned long mask = 0;            /* used for fset */
        Bool is_fset = 0;             /* used for fset */
        fol_t A;
        fkey_t k_A;
        INDEXING_DEREF(A,k_A,key,depth);

        is_fset = FOLFSETP(A);
        
        V_LEVEL_DISPLAY( V_INDEX, "\tcurrent term is %&f %d %&f\n", A, (long) entry,entry->key );
        V_LEVEL_DISPLAY( V_INDEX, "\tentry info key=%&f, depth=%d, kind=%d\n", entry->key , entry->depth, entry->kind );
        
        
        if (HOLE_P(A) && !is_fset) {                   /* try recursively all entries of table */
            V_LEVEL_DISPLAY( V_INDEX, "\tblind_delete\n");
            resume_forward();
            blind_delete(entry);
            clean_entry(parent,entry);
            Succeed;
        }
        
            /* Try variable entry in table */
        e1 = (TableEntry) Var_Find(entry);
        e2 = (TableEntry) Hash_Find(table,(long) key);

        if (FOLSMBP(A)) {
            fsetelt_t fsets = FOLSMB_FSETELT(A);
            if (fsets && (e3 = (TableEntry) FSet_Find(entry)))
                mask = fsets->unif_mask;
        } else if (FOL_CSTP(A)) {
            e3 = (TableEntry) FSet_Find(entry);
        } else if (is_fset && (e3 = (TableEntry) FSet_Find(entry))) {
            mask = fset_mask(A);
        }
        
        if (e3 && (e1 || e2)) {
            HashScan *scan=Start_HashScan();
            TableEntry e4 = (TableEntry)Hash_First((char *)(e3->list),scan);
            if (mask && e4) 
                e4=find_next_fset_mask(e4,scan,mask);
            if (e4) {
                V_LEVEL_DISPLAY( V_INDEX,"Setting choice point in unif_fset_delete %&f\n",e3->key);
                LVALUE_X(0) = REG_VALUE(e4);
                LVALUE_X(1)=  REG_VALUE(scan);
                LVALUE_X(2)=  REG_VALUE(mask);
                LVALUE_X(3)= REG_VALUE(1);
                LVALUE_X(4)= REG_VALUE(entry);
                Dyam_Full_Choice(Unif_FSet_Delete,5);
            } else {
                Delete_Topmost_Trailed_Bloc();
            }
        }
        
        if (e1) {
            if (e2) {
                V_LEVEL_DISPLAY( V_INDEX,"Setting choice point in unif_retrieve2 %&f\n",e2->key);
                LVALUE_X(0) = REG_VALUE(e2);
                LVALUE_X(1) = REG_VALUE(A);
                LVALUE_X(2) = REG_VALUE(Sk(A));
                LVALUE_X(3)= REG_VALUE(entry);
                Dyam_Full_Choice(Unif_Delete3,4);
            }
            resume_forward();
            unif_delete2(e1,entry);
            clean_entry(parent,entry);
            Succeed;
        } else if (e2) {
            resume_push(S(A));
            unif_delete2(e2,entry);
            clean_entry(parent,entry);
            Succeed;
        } else if (e3) {
                /* the forward should come before the HashScan
                   to ensure the HashScan trail box is the topmost one
                */
            V_LEVEL_DISPLAY( V_INDEX,"<>E3\n");
            resume_forward();
            {
                HashScan *scan=Start_HashScan();
                TableEntry e4 = (TableEntry)Hash_First((char *)(e3->list),scan);
                if (mask && e4)
                    e4=find_next_fset_mask(e4,scan,mask);
                if (!e4) {
                    V_LEVEL_DISPLAY( V_INDEX,"Fail\n");
                    Fail;
                }
                V_LEVEL_DISPLAY( V_INDEX, "\tsubs_fset_retrieve\n");
                unif_fset_delete_start(e4,scan,mask,entry);
                clean_entry(parent,entry);
                Succeed;
            }
        } else {
            Fail;
        }
    } else
        Fail;
        
}

Bool
unif_delete3(TableEntry entry, SP(A),TableEntry parent)
// **DyALog Builtin** Unif_Delete3 :- unif_delete3(0,1,2,3)
{
    Dyam_Remove_Choice();
    resume_push(S(A));
    unif_delete2(entry,parent);
    Succeed;
}

void
unif_delete( SP(A) )
{
    V_LEVEL_DISPLAY( V_INDEX, "UNIF DELETE %&s\n",S(A));
    if (dyalog_table) {
        struct delete_closure tmp = { A, Sk(A) };
        Dyam_Full_Choice(DyALog_To_C,1);
        LVALUE_X(0)= REG_VALUE(dyalog_table);
        LVALUE_X(1)= REG_VALUE(0);
        Start_Retrieval(S(A),(Closure)&tmp);
        LVALUE_R_CP= REG_VALUE((fun_t) Adjust_Address(DyALog_Fail));
        LVALUE_R_P= REG_VALUE((fun_t) Adjust_Address((fun_t)Unif_Delete2));
        C_To_DyALog();
    }
}

Bool
object_delete( tabobj_t o )
{
    cell_t *cell_handle = (cell_t *) (o->delete_info);
    V_LEVEL_DISPLAY( V_DYAM, "OBJ DELETE %x\n", (long) o);
//    dyalog_printf("OBJ DELETE %x\n", (long) o);
    if (!cell_handle || (*cell_handle)->object != o){
        dyalog_error_printf("*WARNING*: trying deleting incorrect object at address %x\n",o);
        Fail;
    }
    if ((*cell_handle)->next) {
        (*cell_handle)->next->object->delete_info=(void *)cell_handle;
    }
    o->delete_info=0;
    o->seed=0;
    o->backptr=0;
    o->env=0;
    *cell_handle=(*cell_handle)->next;
    Succeed;
}

Bool
Abolish(sfol_t symbol, const int arity) 
{
    SFOL_Deref(symbol);
    if (!FOLSMBP(symbol->t))
        Fail;
    Hash_Delete((char *)dyalog_table->list,
                (long)FOLSMB_CONVERT_ARITY(symbol->t,arity));
    Succeed;
}

static
void clean_indexing (TableEntry entry, TableEntry parent)
{
    if (entry->kind & LIST) {
            //      dyalog_printf("clean indexing LIST entry=%x parent=%x key=%&f\n",entry,parent,entry->key);
        clean_entry_alt(parent,entry);
    } else if (entry) {
        HashScan here;
        HashScan *scan= &here;
        TableEntry e;
//        dyalog_printf("clean indexing HASH entry=%x parent=%x key=%&f\n",entry,parent,entry->key);
        for (e=(TableEntry) Hash_First((char *)entry->list,scan);
             e;
             e=(TableEntry) Hash_Next(scan)) {
            clean_indexing(e,entry);
        }
        clean_entry_alt(parent,entry);
    }
}

void
DyALog_Clean_Indexing ()
{
    if (dyalog_table)
        clean_indexing(dyalog_table,0);
}

static unsigned int index_cnt;

static
void
compute_indexingkey(SP(tail)) 
{
    if (FOLCMPP(tail) && !FOL_DEREFP(tail)) {
        unsigned int arity = FOLCMP_ARITY(tail);
        fol_t *arg = &FOLCMP_REF(tail,1);
        fol_t *stop = arg+(arity < 15 ? arity : 15);
        
        if (FOLCMP_FUNCTOR(tail) == FOLCLOSURE) {
            FOLCMP_WRITE(*arg++);
            index_cnt++;
            tail = *arg++;
            Deref(tail);
            if (FOLCMPP(tail) && !FOL_DEREFP(tail)) {
                arity = FOLCMP_ARITY(tail);
                arg = &FOLCMP_REF(tail,1);
                stop = arg+(arity < 5 ? arity : 5);
            } else
                return;
        }
        for (; arg < stop ;) {
                fol_t u = *arg++;
                fkey_t Sk(u) = Sk(tail);
                Deref(u);
                compute_indexingkey(u,Sk(u));
            }
    } else if (FOLINTP(tail)) {
        FOLCMP_WRITE(tail);
        index_cnt++;
    }
}

static
fol_t
compute_indexingkey_wrapper(SP(tail))
{
    Deref(tail);
    index_cnt = 0;
    FOLCMP_WRITE_LIGHT_START;
    compute_indexingkey(tail,Sk(tail));
    if (index_cnt > 0) {
        fol_t v = FOLCMP_WRITE_LIGHT_STOP(FOLSMB_CONVERT_ARITY(FOLCURLYFUN,index_cnt),index_cnt);
        return POINTER_TO_DYALOG(v);
    } else {
        FOLCMP_WRITE_ABORT;
        return FOLNIL;
    }
}


void
Dyam_Compute_IndexingKey(fol_t indexing_key, fol_t tail)
{
    /* build a short indexing key given an object tail with closure
       the idea is to concatenate the names of the closures
       *** No real gain ! deactivated but kept for potential future use ! (06/2011)
     */
    fol_t key = compute_indexingkey_wrapper(tail,R_TRANS_KEY);
    fkey_t Sk(key) = FOL_CSTP(key) ? Key0 : LSTACK_PUSH_VOID;
    TRAIL_UBIND(indexing_key,R_TRANS_KEY,key,Sk(key));
//    TRAIL_UBIND(indexing_key,R_TRANS_KEY,FOLNIL,Key0);
}
