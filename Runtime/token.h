/* $Id$
 * Copyright (C) 1997, 2010 Eric de la Clergerie
 * ------------------------------------------------------------
 *
 *   Token
 *
 * ------------------------------------------------------------
 * Description
 *    Misc macros and types for the lexer and parser
 * ------------------------------------------------------------
 */

#ifndef READ_TOKEN
#define READ_TOKEN

#if __GNUC__ >= 4
#define DSO_LOCAL __attribute__ ((visibility("hidden")))
#define DSO_PUBLIC __attribute__ ((visibility("default")))
#else
#define DSO_LOCAL
#define DSO_PUBLIC
#endif

typedef enum {
    TK_ERROR = -1
    ,TK_EOF=0		/* END of file, not end of clause */
    ,TK_VAR	        /* type is a variable */
    ,TK_FUNC            /* type is atom( */
    ,TK_FEAT_FUNC	/* type is atom{ */
    ,TK_FSET_FUNC	/* type is atom[ */
    ,TK_INT	        /* type is an integer number */
    ,TK_CHAR	        /* type is a char */
    ,TK_ATOM	        /* type is an atom */
    ,TK_EOC		/* END of clause but not of file */
    ,TK_REAL	        /* type is a real number */
    ,TK_STR	        /* type is a char string */
    ,TK_CHAR_LIST	/* type is a char string */
    ,TK_COMA
    ,TK_BAR
    ,TK_LPAR
    ,TK_RPAR
    ,TK_LBRACE
    ,TK_RBRACE
    ,TK_LBRACKET
    ,TK_RBRACKET
    ,TK_DOT
    ,TK_MODULE          /* module part */
} TK_Kind;

DSO_LOCAL extern char *token_value;
DSO_LOCAL extern buffer auxbuf;
void Syntax_Error( const char *s, ... ) __attribute__ ((__noreturn__));
void Syntax_Warning( const char *s, ... );

DSO_LOCAL extern int lexer_mode;
DSO_LOCAL extern char *lexer_port;

TK_Kind lexer();		/* prototype of the lexer */

#define yycopy()        strcpy((char *)GC_MALLOC_ATOMIC( yyleng+1 ),yytext)

#endif /* READ_TOKEN */
