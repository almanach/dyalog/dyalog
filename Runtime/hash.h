/* $Id$
 * Copyright (C) 1997, 2009, 2010, 2011, 2014, 2015, 2016 Eric de la Clergerie
 * ------------------------------------------------------------
 *
 *   Hash --
 *
 * ------------------------------------------------------------
 * Description
 *
 * ------------------------------------------------------------
 */

typedef struct hash_node *HashNode;

struct hash_node {
    HashNode next;              /* hash node are chained in a bucket */
    HashNode linear;            /* hash node are chained fora linear traversal
                                 * Hash_Next */
    HashNode   *linear_prev;       /* to update linear in case of delete */
    long key;                   /* the hash key */
        /* the rest of the elem comes here */
};

typedef struct Hash_Table
{
    unsigned long size;
    unsigned long elem_size;
    unsigned long nb_elem;
    HashNode  first;
    HashNode  *table;         /* ptr to the table, indirection to allow reallocation */
} *HashTable;

typedef struct {
    char *current;
} HashScan;

#define HASH_STATIC_SIZE           sizeof(struct Hash_Table)

#define Tbl_Size(t)                (((HashTable)t)->size)
#define Elem_Size(t)               (((HashTable)t)->elem_size)
#define Nb_Elem(t)                 (((HashTable)t)->nb_elem)
#define Hsh_Table(t)               ((((HashTable)t)->table))
#define Tbl_First(t)               (((HashTable)t)->first)

#define Hash_Function(k,size)      ((unsigned long)(k) % (size))

extern char *Hash_Alloc_Table(const int tbl_size,const int elem_size);
extern void Hash_Free_Table(char *tbl);
extern char *Hash_Insert(const char *tbl,const char *elem,const int replace);
extern void Hash_Delete(const char *tbl, const long key);
extern int Hash_Table_Size(const char *tbl);
extern int Hash_Nb_Elements(const char *tbl);


#define Hash_First(tbl, _hfscan)                          \
    ({                                                  \
        HashScan *__hfscan=(_hfscan);                         \
        __hfscan->current=(char *) (Tbl_First(tbl));      \
        (char *)Hash_Next(__hfscan);                      \
    })

#define Hash_Next(_hnscan)                                \
    ({                                                  \
        HashScan *__hnscan=(_hnscan);                         \
        HashNode  p = (HashNode) (__hnscan->current);     \
        (char *) ((p==NULL) ? NULL : ({ __hnscan->current=(char *) (p->linear); (&(p->key)); })); \
    })

#define X_rotate_hash(_xr_hash)                 \
({                                                              \
    unsigned long __xr_hash = (_xr_hash);                               \
    (unsigned long) (__xr_hash ^ (__xr_hash >>10) ^ (__xr_hash >>20));      \
})

#define X_local_hash_fol(_xlh_a, _xlh_size) \
    (unsigned long) ((X_rotate_hash((unsigned long) (_xlh_a))) & (_xlh_size-1));

#define X_Hash_Locate(_xhl_tbl,_xhl_key)        \
({                                                                      \
    char *__xhl_tbl = _xhl_tbl;                                                   \
    unsigned long __xhl_key = _xhl_key;                                           \
    HashNode *t = Hsh_Table(__xhl_tbl)+X_local_hash_fol(__xhl_key,Tbl_Size(__xhl_tbl));   \
    HashNode p=*t;                                                      \
    for(;p && p->key != __xhl_key;t=&p->next,p=p->next);                      \
    t;                                                      \
})

#define Hash_Find(_hf_tbl,_hf_key)                                              \
({                                                                      \
    HashNode  p= * (X_Hash_Locate((char *)(_hf_tbl),(unsigned long)(_hf_key))); \
    (char *) (p ?  (&(p->key)) : NULL);                         \
})


