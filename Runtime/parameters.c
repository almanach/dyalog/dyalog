/************************************************************
 * $Id$
 * Copyright (C) 1997, 2009, 2010 Eric de la Clergerie
 * ------------------------------------------------------------
 *
 * Parameters  -- DyALog parameter files
 *
 * ------------------------------------------------------------
 * Description
 *
 * ------------------------------------------------------------
 */

#include <string.h>

#include "config.h"
#include "bigloo.h"
#include "fol.h"

/**********************************************************************
 * Some tools
 **********************************************************************/

/**********************************************************************
 * About this software
 **********************************************************************/

const char *_dyalog_package  = __DYALOG_PACKAGE_PATH__;

const char *_dyalog_name     = "DyALog (alpha 5)";
const char *_dyalog_author   = "Eric de la Clergerie";
const char *_dyalog_email    = "Eric.Clergerie@inria.fr";

/**********************************************************************
 * High Level Parameters
 **********************************************************************/

fol_t  _dyalog_argv         = FOLNIL;

/**********************************************************************
 * Low Level Parameters
 **********************************************************************/


int    _dyalog_initialization_stamp = 0;

obj_t  _dyalog_callret_viewer = BNIL;

char *  _dyalog_tfs_file;

// int     verbose_level = 0;



