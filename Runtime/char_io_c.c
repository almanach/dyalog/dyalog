/*-------------------------------------------------------------------------*/
/* Buit-In Predicates                                   Daniel Diaz - 1996 */
/* Character input-output management - C part                              */
/*                                                                         */
/* char_io_c.c                            Copyright (C) 1996, 2004, INRIA France */
/*-------------------------------------------------------------------------*/
#include <stdio.h>

#include "libdyalog.h"
#include "builtins.h"

/*---------------------------------*/
/* Constants                       */
/*---------------------------------*/

#define atom_end_of_file          find_folsmb("end_of_file",0)
#define Is_Valid_Code(c)          ((unsigned) (c)-1 <256-1) /* 1<= c <256 */

/*---------------------------------*/
/* Type Definitions                */
/*---------------------------------*/

/*---------------------------------*/
/* Global Variables                */
/*---------------------------------*/

/*---------------------------------*/
/* Function Prototypes             */
/*---------------------------------*/

/*-------------------------------------------------------------------------*/
/* GET_CHAR_2                                                              */
/*                                                                         */
/*-------------------------------------------------------------------------*/

static Bool Get_Char_2(Sproto,Sproto);

Bool              /* DyALog Wrapper */
DYAM_Get_Char_2(fol_t sora_word,fol_t char_word)
{
    fkey_t k=R_TRANS_KEY;
    return Get_Char_2(sora_word,k,char_word,k);
}

static Bool	  /* DyALog Builtin */
Get_Char_2( S(sora_word), S(char_word) )
     Sdecl(sora_word);
     Sdecl(char_word);
{
 int     stm;
 fol_t   atom;
 int     c;
 Deref(sora_word);

 stm=(sora_word == FOLNIL) 
     ? stm_input
     : Get_Stream_Or_Alias(sora_word,k_sora_word,STREAM_FOR_INPUT);

 last_input_sora=sora_word;
 Check_Stream_Type(stm,STREAM_FOR_INPUT|STREAM_FOR_TEXT);


 Deref(char_word);

 if ( !FOLVARP(char_word) && ( !FOLCHARP(char_word) ||
			       char_word!=atom_end_of_file ))
   Fail;

 c=Stream_Getc(stm_tbl+stm);
 if (c!=EOF && ! Is_Valid_Code(c))
     Fail;

 atom=(c==EOF) ? atom_end_of_file : DFOLCHAR(c);

 return Get_Cst(atom,char_word);
}

/*-------------------------------------------------------------------------*/
/* GET_CHAR_1                                                              */
/*                                                                         */
/*-------------------------------------------------------------------------*/

static Bool Get_Char_1(Sproto);

Bool              /* DyALog Wrapper */
DYAM_Get_Char_1(fol_t char_word)
{
    fkey_t k=R_TRANS_KEY;
    return Get_Char_1(char_word,k);
}

static Bool	  /* DyALog Builtin */
Get_Char_1( S(char_word) )
     Sdecl( char_word );
{
 return Get_Char_2(FOLNIL,Key0,char_word,k_char_word);
}

/*-------------------------------------------------------------------------*/
/* PUT_CHAR_2                                                              */
/*                                                                         */
/*-------------------------------------------------------------------------*/

static Bool Put_Char_2(Sproto,Sproto);

Bool              /* DyALog Wrapper */
DYAM_Put_Char_2(fol_t sora_word,fol_t char_word)
{
    fkey_t k=R_TRANS_KEY;
    return Put_Char_2(sora_word,k,char_word,k);
}

static Bool	  /* DyALog Builtin */
Put_Char_2( S(sora_word), S(char_word) )
     Sdecl(sora_word);
     Sdecl(char_word);
{
 int     stm;

 Deref(sora_word);
 
 stm=(sora_word == FOLNIL) 
     ? stm_output
     : Get_Stream_Or_Alias(sora_word,k_sora_word,STREAM_FOR_OUTPUT);
 
 last_output_sora=sora_word;
 Check_Stream_Type(stm,STREAM_FOR_OUTPUT|STREAM_FOR_TEXT);

 Deref(char_word);

 if (!FOLCHARP(char_word)) Fail;

 Stream_Putc(CFOLCHAR(char_word),stm_tbl+stm);

 return True;
}


void
DyALog_Put_Char( int stm, char c)
{
    Check_Stream_Type(stm,STREAM_FOR_OUTPUT|STREAM_FOR_TEXT);
    Stream_Putc(c,stm_tbl+stm);
}

/*-------------------------------------------------------------------------*/
/* PUT_CHAR_1                                                              */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void
Put_Char_1( S(char_word) )
     Sdecl(char_word);
{
    Put_Char_2(FOLNIL,Key0,char_word,k_char_word);
}


Bool              /* DyALog Wrapper */
DYAM_Put_Char_1(fol_t char_word)
{
    fkey_t k=R_TRANS_KEY;
    Put_Char_1(char_word,k);
    Succeed;
}
