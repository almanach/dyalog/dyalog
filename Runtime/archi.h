/* $Id$
 * Copyright (C) 1997, 2006, 2009, 2010, 2014, 2017 Eric de la Clergerie
 * ------------------------------------------------------------
 *
 *   Archi -- Architecture
 *
 * ------------------------------------------------------------
 * Description
 *   Register Architecture 
 * ------------------------------------------------------------
 */

#ifdef __cplusplus
extern "C" {
#endif
    
#ifndef ARCHI_READ
#define ARCHI_READ

typedef void * TrailWord;

#define WORD_SIZE_IN_BITS sizeof(long)*8

#if   defined(M_sparc_sunos) || defined(M_sparc_solaris)

/* these architectures are obsolete
 * I don't believe they are still working the new versions of DyALog
 */
register TrailWord                *reg_bank asm ("g6");

#define REG(i)         ((TrailWord)reg_bank[i])
#define LVALUE_REG(i)         (reg_bank[i])

#define USE_REGBANK 1

#elif defined(__x86_64) && (defined(M_ix86_darwin) || defined(M_ix86_linux))

/* Mac OS X may be used in 64bit
 * and this is the default mode for Snow Leopard
 */



#ifdef __clang__
#ifdef ARCHI_FILE
TrailWord                *reg_bank;
#else
extern   TrailWord       *reg_bank;
#endif

#define REG(i)         ((TrailWord)trail[i])
#define LVALUE_REG(i)         (trail[i])
#else

register TrailWord                *reg_bank asm ("r15");
#define REG(i)         ((TrailWord)reg_bank[i])
#define LVALUE_REG(i)         (reg_bank[i])

#endif

#define USE_REGBANK 1

// modyfied by Djame 
#elif defined(M_ix86_linux) || defined(M_ix86_darwin) || defined(M_powerpc_linux) || defined(M_powerpc_darwin)

#ifdef ARCHI_FILE
TrailWord                *reg_bank;
#else
extern   TrailWord       *reg_bank;
#endif

#define REG(i)         ((TrailWord)trail[i])
#define LVALUE_REG(i)         (trail[i])

#endif

/*
 * The trail stack is structured as follow
 *
 *        system registers
 *        user registers    X(i)
 *        variables
 *        trail blocks
 *
 */

#define NB_SYSTEM_REGISTERS 32
#define NB_USER_REGISTERS   1024

#define NB_REGISTERS        NB_SYSTEM_REGISTERS + NB_USER_REGISTERS
#define VARIABLE_BASE_N     NB_REGISTERS

/*
 * System Register Indexes
 */

#define I_TOP        0	/* Next available save register */
#define I_LAYER      1	/* layer stack top register */
#define I_TRAIL      2	/* trail stack top register */
#define I_CTL        3  /* control stack top register */
#define I_TRANS      4	/* transition register */
#define I_TRANS_KEY  5	/* transition key register */
#define I_ITEM       6	/* item register */
#define I_ITEM_KEY   7	/* item key register */
#define I_ITEM_COMP  8	/* item component register */
#define I_MODULE     9	/* module register */
#define I_BACKPTR    10	/* backptr register (could use an user register) */
#define I_OBJECT     11	/* object register (could use an user register)  */
#define I_HEAP       12	/* heap top register */
#define I_HEAP_STOP  13	/* Stop address of the term being built */
#define I_LEVEL      14	/* Strate Level */

#define I_CP         15	/* Continuation Program pointer */
#define I_E          16	/* Environnement pointer */
#define I_B          17	/* Backtrack  pointer */
#define I_BC         18	/* Backtrack Cut pointer */
#define I_P          19	/* Program Pointer */

#define I_IP         20	/* Indexation Pointer */

#define I_COLLAPSE   21
#define I_BINDING    22
#define I_UNBIND     23
#define I_CGBINDING  24
#define I_DISPLAY    25	
#define I_DEREF_A    26
#define I_DEREF_K    27	

#define I_MIN_LAYER 28

#define I_OCCUR_CHECK 29

#define I_VERBOSE_LEVEL 30

/*
 * Saving to / restoring from  a save register
 */

#define R_SAVE(top,reg)       *top++    = ((TrailWord) reg)
#define R_RESTORE(top,reg)    reg   = *--top


#define R_TOP        (TrailWord *) REG(I_TOP)
#define LVALUE_R_TOP        LVALUE_REG(I_TOP)

#define R_OCCUR_CHECK (long) REG(I_OCCUR_CHECK)
#define LVALUE_R_OCCUR_CHECK        LVALUE_REG(I_OCCUR_CHECK)

#define R_VERBOSE_LEVEL (unsigned long) REG(I_VERBOSE_LEVEL)
#define LVALUE_R_VERBOSE_LEVEL        LVALUE_REG(I_VERBOSE_LEVEL)

DSO_LOCAL extern void initialization_registers();

/*
 *  User Register
 */

#define X( i )         REG( NB_SYSTEM_REGISTERS + i )
#define LVALUE_X( i )         LVALUE_REG( NB_SYSTEM_REGISTERS + i )

#define REG_VALUE(v)     ((TrailWord) (v))


#endif /* ARCHI_READ */

#ifdef __cplusplus
}
#endif    
