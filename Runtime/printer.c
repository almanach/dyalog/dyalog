/* $Id$
 * Copyright (C) 1997, 2002, 2003, 2004, 2007, 2008, 2009, 2010, 2014, 2015, 2016 Eric de la Clergerie
 * ------------------------------------------------------------
 *
 *   Printer --
 *
 * ------------------------------------------------------------
 * Description
 *        Printer for fol terms and substitutions
 *
 *      11 March 98: modified to handle deref terms:
 *              use of 'true_first_arg' to skip deref arg if needed,
 *              not used for curly and numbervar terms (assumed not
 *              be deref terms)
 * ------------------------------------------------------------
 */

#include <stdio.h>
#include <string.h>
#include <ctype.h>

#include "libdyalog.h"
#include "builtins.h"

extern obj_t current_output_port;

typedef struct {
    unsigned int display:2;
    unsigned int quoted:1;
    unsigned int op:1;
    unsigned int numbervars:1;
    unsigned int feature:1;
    unsigned int hilog:1;
    unsigned int indent:1;
} display_flags;

typedef enum { Display_Std, Display_Right, Display_Left } Display_Kind;

char *newline_start="";

inline
static fol_t
feature_info_feature( fol_t f )
{
  return FOLSMBP(f) ? f : FOLCMP_REF(f,1);
}

inline
static fol_t
feature_info_type( fol_t f )
{
  return FOLSMBP(f) ? Not_A_Fol : FOLCMP_REF(f,2);
}


extern display_t find_var_display( fol_t, fkey_t );
extern display_t set_or_update_var_display( fol_t, fkey_t );

/*
 * Collect all deref variables of feature terms
 * and associate to each of them a display entry in the trail stack
 * this info is used to refine the display in DyALog
 *
 */
static
void assign_display_info( SP(A) )
{
    Deref(A);
    V_LEVEL_DISPLAY(V_LOW,"assign_display_info %&s\n",A,Sk(A));
    if (FOLVARP(A)){
        set_or_update_var_display( A, Sk(A) );
    } else if (FOLCMPP(A)) {
        fol_t *arg = &FOLCMP_REF(A,1);
        fol_t *stop = arg + FOLCMP_ARITY(A);
        int status=0;
        if (FOLCMP_DEREFP(A)) {
            status=(set_or_update_var_display( FOLCMP_DEREF_VAR(A), Sk(A) ))->status;
            arg++;
        }
        if (status == 0) 
            for( ; arg < stop; arg++ )
                assign_display_info( *arg, Sk(A));
    }
}

void
DyALog_Assign_Display_Info( sfol_t A ) 
{
    assign_display_info(A->t,A->k);
}

Bool
DyALog_Assign_Varname(sfol_t SA,char *name)
{
    fol_t A = SA->t;
    fkey_t Sk(A) = SA->k;
    display_t dbox;
    Deref_And_Fail_Unless(A,FOL_DEREFP(A));
    V_LEVEL_DISPLAY(V_LOW,"assign_varname %&s %s\n",A,Sk(A),name);
    if (!(dbox=find_var_display(FOL_DEREF_VAR(A),Sk(A))))
        dbox=TRAIL_DISPLAY(FOL_DEREF_VAR(A),Sk(A));
    dbox->name=name;
    Succeed;
}



/*
 * Check if A is a feature term maximal wrt its type
 *   should be done after assign_display_info to know the number of
 * occurences of each deref variable.
 */
static
Bool check_maximal_tfs( SP(A), fol_t type )
{

    Deref(A);

    if (FOL_ICSTP(A))
        return 0;
    else if (FOLVARP(A)) {
        display_t dbox = find_var_display(A,Sk(A)); 
        return (dbox && (dbox->status == 0));
    } else if (type == Not_A_Fol) {
        return 0;
    } else {
        fol_t features = (fol_t) CDR( folsmb_feature_info(type) );
        fol_t functor = FOL_FUNCTOR(A);
        int arity = FOL_ARITY(features);
        fol_t *arg,*stop,*ff;
        display_t dbox = 0;
        int status;
        
        if ( functor != FOLSMB_MAKE( FOLSMB_INDEX(type), arity) )
            return 0;
        
        if (FOLSMBP(A))
            return 1;
        
         /* A and features are compound terms */
        arg = &FOLCMP_REF(A,1);
        stop = arg+arity;
        ff = &FOLCMP_REF(features,1);
        
        if (FOLCMP_DEREFP(A)) {
            dbox = find_var_display(FOLCMP_DEREF_VAR(A),Sk(A));
            if (!dbox || dbox->status < 0)
                return 0;
            arg++;
            ff++;
        }
        
        for( ; arg < stop ; arg++, ff++ )
            if (!check_maximal_tfs( *arg, Sk(A), feature_info_type(*ff )))
                return 0;

        if (dbox && (status = dbox->status) > 0){
            dbox->status = - status;
            return 0;
        }    
        
        return 1;
        
    }
    
}

/**********************************************************************
 * Internal Printer
 **********************************************************************/

static void sfol_display_with_prec( SP(A), int prec, display_flags mode, StmInf
                                    *port );

static void
sfol_display_variable( SP(A), StmInf *port ) 
{
  unsigned int index = FOLVAR_INDEX(A);
  unsigned int trail = index / 26;
  Stream_Putc( 'A'+ (index % 26), port );
  if (trail > 0) Stream_Printf( port,  "%d", trail );
  if (k_A) Stream_Printf( port,  "__%d" , LSTACK_INDEX( k_A ));
}

static
int check_folcmp_derefp( SP(A), display_flags mode, StmInf *port)
        /* 0 -> not deref
           1 -> single occ of deref var
           2 -> first occ of deref var
           3 -> other occ of mutiple occ deref var
         */
{
    display_t dbox = find_var_display(FOLCMP_DEREF_VAR(A),Sk(A));
    int status;
        
    if (!FOLCMP_DEREFP(A))
        return 0;

    if (dbox)
        status = dbox->status;
    else {
        dbox = TRAIL_DISPLAY(FOLCMP_DEREF_VAR(A),Sk(A));
        status = dbox->status = 1;
    }
        
    if (status == 0) 
            /* single occ of deref var => display nothing */
        return 1;

    if (status > 0) {
            /* first occurrence of a multi-occurence deref var */
//        dbox->status = -status;
        sfol_display_variable( FOLCMP_DEREF_VAR(A), Sk(A), port );
        Stream_Puts( "::", port );
        return 2;
    } 
    
        /* occ. > 1 of a multi-occ deref var */
    sfol_display_with_prec( FOLCMP_DEREF_VAR(A), Sk(A), 1200, mode, port );
    return 3;
//    Stream_Puts( "::", port );
//    return 2;
}

static void
sfol_display_symbol( fol_t s, display_flags mode, StmInf *port, Bool op )
{
    Bool to_quote = FOLSMB(s)->to_quote;
    Bool to_scan = FOLSMB(s)->to_scan;
    char *name = (char *) FOLSMB_NAME(s);
    fol_t module = FOLSMB_MODULE(s);
    char *p,*q;
    char str[32];

    if ((module!=FOLNIL)){
        sfol_display_symbol(module,mode,port,op);
        Stream_Putc('!',port);
    }
        
    if (!mode.quoted || !to_quote) {
        Stream_Puts( name, port );
    } else if (op && s == FOLCOMA ) {
        Stream_Puts( name, port);
    } else {
        Stream_Putc( '\'', port );
        if (!to_scan) {
            Stream_Puts(name,port);
        } else {
            for(p=name;*p;p++) {
                if ((q=strchr(escape_char,*p))){
                    Stream_Putc('\\',port);
                    Stream_Putc(escape_symbol[q-escape_char],port);
                } else if ((*p) =='\'' || (*p) =='\\') {
                        /* display twice */
                    Stream_Putc(*p,port);
                    Stream_Putc(*p,port);
                } else if (!isprint(*p)) {
                    sprintf(str,"\\X%02hhX",(*p) );
                    Stream_Puts(str,port);
                } else
                    Stream_Putc(*p,port);
            }
        }
        Stream_Putc( '\'',port );
    }
}

static void
display_newline(unsigned int line_pos, display_flags mode, StmInf *port)
{
    if (mode.indent) {
        unsigned int i=0;
        Stream_Puts( ",\n" , port);
        Stream_Puts(newline_start,port);
        for( i=0; i<line_pos; i++)
            Stream_Putc( ' ', port);
    } else
        Stream_Puts( ", ", port);
}

static void
display_newline_in_infix(unsigned int line_pos, display_flags mode, StmInf *port)
{
    if (mode.indent) {
        unsigned int i=0;
        Stream_Puts( "\n" , port);
        Stream_Puts(newline_start,port);
        for( i=0; i<line_pos; i++)
            Stream_Putc( ' ', port);
    } else
        Stream_Puts( " ", port);
}

static void
sfol_display_with_prec( SP(A), int prec, display_flags mode, StmInf *port )
{
        //binding_t bA = (Sk(A) && FOL_DEREFP(A)) ? CLOSURE_UL_DEREF(A,Sk(A)) : 0;
        Deref(A);
        
        if (FOLINTP(A)) {
            Stream_Printf( port, "%ld", CFOLINT(A) );
        } else if (FOLCHARP(A)){
            Stream_Puts( "0'", port );
            Stream_Putc( CFOLCHAR(A), port );
        } else if (FOLFLTP(A)) {
            Stream_Printf( port, "%#g",CFOLFLT(A));
        } else if (FOLVARP(A)) {
            display_t dbox = find_var_display(A,Sk(A)); 
            if (dbox && (dbox->status == 0)) /* variables occuring once => anonymous */
                Stream_Putc('_',port);
            else if (dbox && dbox->name) /* variable to be displayed with a given
                                          * name */
                Stream_Puts(dbox->name, port);
            else
                sfol_display_variable(A,Sk(A),port);
        } else if (FOLSMBP(A)) {
            fol_t op;
            Bool enclosed = 0;
            obj_t  feature_info;
            fol_t  features;
            if ( mode.feature &&
                 (feature_info = folsmb_feature_info(A)) != BFALSE &&
                 (features = (fol_t) CDR(feature_info),
                  0 == FOL_ARITY(features))){
                sfol_display_symbol(FOLSMB_MODULE(A),mode,port,0);
                Stream_Puts("{}",port);
            } else {
                if (mode.op && mode.display != Display_Std)
                    switch (mode.display) {
                        case Display_Left:
                            op = FOLSMB_CONVERT_ARITY( A, 1 );                    
                            enclosed = FOLSMB_PREFIX_P(op) && (prec < OPINFO_PREC(FOLSMB_PREFIX(op)));
                            break;
                        case Display_Right:
                            op = FOLSMB_CONVERT_ARITY( A, 1 );                                        
                            enclosed = FOLSMB_POSTFIX_P(op) && (prec <
                                                                OPINFO_PREC(FOLSMB_POSTFIX(op)));
                            if (!enclosed) {
                                op = FOLSMB_CONVERT_ARITY( A, 2 );
                                enclosed = FOLSMB_INFIX_P(op) && (prec <
                                                                  OPINFO_PREC(FOLSMB_INFIX(op)));
                            }                    
                            break;
                        default:
                            break;
                    }
                if (enclosed)
                    Stream_Putc( '(', port );
                sfol_display_symbol(A,mode,port,0);
                if (enclosed)
                    Stream_Putc( ')', port );
            }
        } else if (FOLPAIRP(A)) {
            mode.display = Display_Std;
            Stream_Putc( '[', port );
            sfol_display_with_prec( FOLPAIR_CAR(A), k_A, 900, mode, port );
            A = FOLPAIR_CDR(A);
            if (k_A) {Deref( A );}
            while (FOLPAIRP( A )){
                Stream_Putc( ',' , port);
                sfol_display_with_prec( FOLPAIR_CAR(A), k_A, 900,mode, port );
                A = FOLPAIR_CDR(A);
                if (k_A) {Deref( A );}
            }
            if (!FOLNILP(A)){
                Stream_Putc( '|', port );
                sfol_display_with_prec( A, k_A, 900,mode, port );
            }
            Stream_Putc( ']', port );
        } else if (FOLCMPP(A)) {
            fol_t functor = FOLCMP_FUNCTOR(A);
            int    arity  = FOLCMP_ARITY(A);
            Bool enclosed = 0;
            obj_t  feature_info;
            fol_t  features;
            int    opinfo;
            fol_t  *arg=&FOLCMP_REF(A,1);
            fol_t  *stop=arg+arity;
    
//        if (enclosed) Stream_Putc( '(', port );

//        dyalog_printf("<%x>",(void *)A);

            if (functor == FOLLOOP) {
                switch (check_folcmp_derefp(A,k_A,mode,port)) {
                    case 1:         /* single occ of a deref term */
                    case 2:         /* first occ of  a deref term */
                        arg++;
                        break;
                        
                    case 3:         /* other occ of a deref term */
                        return;
                        
                    default:        /* not a deref term */
                        break;
                }
                sfol_display_with_prec( *arg, k_A, prec, mode, port );
            } else if (functor == FOLRANGE ) {

                switch (check_folcmp_derefp(A,k_A,mode,port)) {
                    case 1:         /* single occ of a deref term */
                    case 2:         /* first occ of  a deref term */
                        arg++;
                        break;
                        
                    case 3:         /* other occ of a deref term */
                        return;
                        
                    default:        /* not a deref term */
                        break;
                }
                mode.display=Display_Std;
                Stream_Puts( "$[" , port );
                sfol_display_with_prec( *arg, k_A, 900, mode, port );
                Stream_Putc( ',' , port );
                sfol_display_with_prec( *(arg+1), k_A, 900, mode, port );
                Stream_Putc( ']' , port );
            } else if (FOLCURLYFUN_P(functor)) {
                mode.display=Display_Std;
                Stream_Putc( '{' , port );
                sfol_display_with_prec( *arg, k_A, 1200, mode, port );
                Stream_Putc( '}' , port );
            } else if ( mode.op && FOLSMB_PREFIX_P(functor) ) {
                opinfo = FOLSMB_PREFIX(functor);
                enclosed = (OPINFO_PREC(opinfo)  > prec );
                if (enclosed) Stream_Putc( '(' , port );
                sfol_display_symbol(functor,mode,port,1);
                Stream_Putc( ' ', port );
                mode.display=(OPINFO_PREC(opinfo)  > prec ) ? Display_Std : Display_Right;
                sfol_display_with_prec( *arg, k_A,
                                        OPINFO_RPREC(opinfo),
                                        mode,
                                        port);
                if (enclosed) Stream_Putc( ')' , port );
            } else if ( mode.op && FOLSMB_POSTFIX_P(functor) ) {
                mode.display=Display_Left;
                opinfo = FOLSMB_POSTFIX(functor);
                enclosed = (OPINFO_PREC(opinfo)  > prec );
                if (enclosed) Stream_Putc( '(' , port );
                sfol_display_with_prec( *arg, k_A,
                                        OPINFO_LPREC(opinfo),mode, port );
                Stream_Putc( ' ', port );
                sfol_display_symbol(functor,mode,port,1);
                if (enclosed) Stream_Putc( ')' , port );
            } else if ( mode.op && FOLSMB_INFIX_P(functor) ) {
                unsigned int line_pos;
                mode.display=Display_Left;
                opinfo = FOLSMB_INFIX(functor);
                enclosed = (OPINFO_PREC(opinfo)  > prec );
                if (enclosed) Stream_Putc( '(' , port );
                line_pos = (port->line_pos);
                sfol_display_with_prec( *arg, k_A,
                                        OPINFO_LPREC(opinfo),mode, port );
                display_newline_in_infix(line_pos,mode,port);
                sfol_display_symbol(functor,mode,port,1);
                Stream_Putc( ' ', port );
                mode.display=(OPINFO_PREC(opinfo)  > prec ) ? Display_Std : Display_Right;
                sfol_display_with_prec( *(arg+1), k_A,
                                        OPINFO_RPREC(opinfo),
                                        mode,
                                        port );
                
                if (enclosed) Stream_Putc( ')' , port );
            } else if ( FOLSMB_CONVERT_ARITY(functor,0) == FOLFSET ) {
                    /* assume args of A do not need dereferencing
                       and are correctly typed
                    */
                fol_t table = FOLCMP_REF(A,2);
                fol_t *table_arg= &FOLCMP_REF(table,1);
                fol_t *tmp_table_arg = table_arg;
                fol_t *table_stop= table_arg + FOLCMP_ARITY(table);
                fol_t *value_arg=&FOLCMP_REF(A,3);
                fol_t *value_stop=&FOLCMP_REF(A,1) + FOLCMP_ARITY(A);
                Bool a_element_displayed=0;
                
                switch (check_folcmp_derefp(A,k_A,mode,port)) {
                    case 1:         /* single occ of a deref term */
                    case 2:         /* first occ of  a deref term */
                        break;
                        
                    case 3:         /* other occ of a deref term */
                        return;
                        
                    default:        /* not a deref term */
                        break;
                }
            
                mode.display=Display_Std;
                sfol_display_symbol(FOLCMP_FUNCTOR(table),mode,port,0);
                    // dyalog_printf("/%d %x %x/",FOLCMP_ARITY(table),mask,value);
                Stream_Puts( "[", port);
                for(; value_arg < value_stop; value_arg++,table_arg=tmp_table_arg+FSET_BIT_PER_WORD) {
                    unsigned long value = UCFOLINT(*value_arg);
                    tmp_table_arg=table_arg;
//                dyalog_printf("([%x] %x)",(unsigned int)*value_arg,value);
                    for( ; value && table_arg < table_stop ; table_arg++, value >>= 1 ) {
                        if (value & 1) {
                            if (a_element_displayed) {
                                Stream_Puts( ", ", port);
                            } else {
                                a_element_displayed = 1;
                            }
                            sfol_display_with_prec( *table_arg, Key0, 0,mode, port );
                        }
                    }
                }
                Stream_Puts( "]", port );
                
            } else if ( (feature_info = folsmb_feature_info(functor)) != BFALSE &&
                        (features = (fol_t) CDR(feature_info),
                         arity == FOL_ARITY(features)) ) {
                fol_t *ff  = &FOLCMP_REF(features,1);
                unsigned int line_pos;
                Bool a_feature_displayed = 0;
                
                switch (check_folcmp_derefp(A,k_A,mode,port)) {
                    case 1:         /* single occ of a deref term */
                    case 2:         /* first occ of  a deref term */
                        arg++;
                        ff++;
                        break;
                        
                    case 3:         /* other occ of a deref term */
                        return;
                        
                    default:        /* not a deref term */
                        break;
                }
                
                mode.display=Display_Std;
                sfol_display_symbol(FOLSMB_MODULE(functor),mode,port,0);
                Stream_Puts( "{", port);
                line_pos = (port->line_pos);
                for ( ; arg < stop ; ff++, arg++) {
                    if (!check_maximal_tfs(*arg,k_A,feature_info_type(*ff))) {
                        if (a_feature_displayed)
                            display_newline(line_pos,mode,port);
                        a_feature_displayed=1;
                        sfol_display_symbol( feature_info_feature(*ff),mode,port, 0);
                        Stream_Puts( "=> ", port );
                        sfol_display_with_prec( *arg, k_A, 0,mode, port );
                    }
                }
                Stream_Puts( "}", port );
            } else if (mode.hilog && FOLSMB_CONVERT_ARITY(functor,0) == FOLHILOG ) {
                mode.display = Display_Std;
                sfol_display_with_prec( *arg, k_A, 0, mode , port );
                if (FOLSMBP(*arg) && ! FOLSMB_HILOG_P(*arg)) Stream_Putc( ' ', port);
                Stream_Putc( '(', port );
                if ( ++arg < stop ) {
                    sfol_display_with_prec( *arg++, k_A, 0, mode, port );
                    for (; arg < stop; ) {
                        Stream_Putc( ',' , port );
                        sfol_display_with_prec( *arg++, k_A, 0, mode , port );
                    }
                }
                Stream_Putc( ')', port );
            } else if (mode.numbervars
                       && (FOLCMP_FUNCTOR(A) == FOLNUMBERVAR)) {
                A = *arg;
                Deref(A);
                {
                    unsigned int index = CINT( A );
                    unsigned int trail = index / 26;
                    Stream_Putc( '_', port);      
                    Stream_Putc( 'A' + (index % 26), port );
                    if (trail > 0) Stream_Printf( port, "%d",trail );
                }
            } else {
                unsigned int line_pos;
                mode.display=Display_Std;
                sfol_display_symbol(functor,mode,port,0);
                Stream_Putc( '(', port );
                line_pos = (port->line_pos);
                sfol_display_with_prec( *arg++, k_A, 0, mode, port );
                for (; arg < stop; ) {
                        // Stream_Putc( ',' , port );
                    display_newline(line_pos,mode,port);
                    sfol_display_with_prec( *arg++, k_A, 0, mode, port );
                }
                Stream_Putc( ')', port );
            }
            
        } else {
            
            assert( 0 && "Not a FOL kind" );
//        Stream_Printf(port,"'<UNKNOW:%x>'",(unsigned int) A);
        }
        
}

/**********************************************************************
 * Wrappers
 **********************************************************************/

void
sfol_display( SP(A) )
{
  StmInf *port = INT_TO_STM( stm_output );
  display_flags mode = {.display=Display_Std,.op=1,.numbervars=1,.feature=1,.hilog=1};
  sfol_display_with_prec( A, k_A, 1200, mode, port );
}

void
sfol_fdisplay( StmInf *stream, SP(A) )
{
  display_flags mode = {.display=Display_Std,.op=1,.numbervars=1,.feature=1,.hilog=1};
  sfol_display_with_prec(A,k_A,1200, mode,stream);
}

void
sfol_print( SP(A) )
{
  StmInf *port = INT_TO_STM( stm_output );
  display_flags mode = {.display=Display_Std,.op=1,.numbervars=1,.feature=1,.hilog=1};
  sfol_display_with_prec( A, k_A, 1200,mode, port );
  Stream_Putc( '\n', port );
}

void
fol_display( fol_t A )
{
    fkey_t Sk(A);
    Trail;
    Sk(A) = LSTACK_PUSH_VOID;
    sfol_display( A, Sk(A));
    Untrail;
}

void
fol_fdisplay( StmInf *stream, fol_t A)
{
    fkey_t Sk(A);
    Trail;
    Sk(A) = LSTACK_PUSH_VOID;
    sfol_fdisplay( stream, A, Sk(A));
    Untrail;
}

void
fol_print( fol_t A )
{
    fkey_t Sk(A);
    Trail;
    Sk(A) = LSTACK_PUSH_VOID;
    sfol_print( A, Sk(A));
    Untrail;
}

/**********************************************************************
 * Substitution
 **********************************************************************/

void
subst_fdisplay( StmInf *stream, SP(subst) )
{
  fol_t s;
  fol_t A;
  fol_t Xname;
  Sdecl(X);
  Bool found = 0;
  Trail;
  LSTACK_PUSH_VOID_STMT;
  assign_display_info(subst,Sk(subst));
  for (s=subst;FOLPAIRP(s); s=FOLPAIR_CDR(s)){
    A = FOLPAIR_CAR(s);
    Xname = FOLCMP_REF(A,1);
    X = FOLCMP_REF(A,2);
    Sk(X) = Sk(subst);
    Deref(X);
    if (FOLVARP(X))
      Bind( Xname, (fkey_t) 0, X);
  }
      /*
        Numbervars_3( subst, Sk(subst), DFOLINT(0), Key0,
        FOLVAR_FROM_INDEX(0),k);
      */  
  for (s=subst;FOLPAIRP(s); s=FOLPAIR_CDR(s)){
    A = FOLPAIR_CAR(s);
    Xname = FOLCMP_REF(A,1);
    X = FOLCMP_REF(A,2);
    Sk(X) = Sk(subst);
    Deref(X);
    if (X != Xname){
      found = 1;
      Stream_Puts("\n    ",stream);
      sfol_fdisplay( stream, A, Sk(subst) );
    }
  }
  if (!found)
    Stream_Puts( "true", stream);
  Stream_Putc('\n',stream);
  Untrail;
}

void
subst_display( SP(subst) )
{
  subst_fdisplay(INT_TO_STM(stm_output), subst, Sk(subst));
}

void
reset_dyalog_stream( int stm )
{
  stm_output = stm;
}

/**********************************************************************
 * Built-In
 **********************************************************************/

static Bool
Write_Term( SP(sora), SP(term), display_flags mode )
{
    int     stm;
    Deref(sora);
    
    stm=(sora == FOLNIL)
        ? stm_output
        : Get_Stream_Or_Alias(sora,Sk(sora),STREAM_NOTHING);
    
    if (stm <= 0)
        Fail;

    sfol_display_with_prec( term, Sk(term), 1200, mode, INT_TO_STM(stm) );
 
    Succeed;
}

/*-------------------------------------------------------------------------*/
/* WRITE_2                                                                 */
/*                                                                         */
/*-------------------------------------------------------------------------*/

static Bool Write_2(Sproto,Sproto);

Bool              /* DyALog Wrapper */
DYAM_Write_2(fol_t sora,fol_t term)
{
    fkey_t k=R_TRANS_KEY;
    return Write_2(sora,k,term,k);
}

static Bool	  /* DyALog Builtin */
Write_2( SP(sora), SP(term) )
{
    display_flags mode = {.display=Display_Std,.op=1,.numbervars=1,.feature=1,.hilog=1};
    return Write_Term( sora, Sk(sora), term, Sk(term), mode );
}

/*-------------------------------------------------------------------------*/
/* Writeq_2                                                                */
/*                                                                         */
/*-------------------------------------------------------------------------*/

static Bool Writeq_2(Sproto,Sproto);

Bool              /* DyALog Wrapper */
DYAM_Writeq_2(fol_t sora,fol_t term)
{
    fkey_t k=R_TRANS_KEY;
    return Writeq_2(sora,k,term,k);
}

static Bool	  /* DyALog Builtin */
Writeq_2( SP(sora), SP(term) )
{
 display_flags mode = {.display=Display_Std,.op=1,.numbervars=1,.feature=1,.hilog=1,.quoted=1,.indent=0};
 return Write_Term( sora, Sk(sora), term, Sk(term), mode );
}

/*-------------------------------------------------------------------------*/
/* Writei_2                                                                */
/*                                                                         */
/*-------------------------------------------------------------------------*/

static Bool Writei_2(Sproto,Sproto);

Bool              /* DyALog Wrapper */
DYAM_Writei_2(fol_t sora,fol_t term)
{
    fkey_t k=R_TRANS_KEY;
    return Writei_2(sora,k,term,k);
}

static Bool	  /* DyALog Builtin */
Writei_2( SP(sora), SP(term) )
{
 display_flags mode = {.display=Display_Std,.op=1,.numbervars=1,.feature=1,.hilog=1,.quoted=1,.indent=1};
 return Write_Term( sora, Sk(sora), term, Sk(term), mode );
}

/*-------------------------------------------------------------------------*/
/* Write_Canonical_2                                                       */
/*                                                                         */
/*-------------------------------------------------------------------------*/

static Bool Write_Canonical_2(Sproto,Sproto);

Bool              /* DyALog Wrapper */
DYAM_Write_Canonical_2(fol_t sora,fol_t term)
{
    fkey_t k=R_TRANS_KEY;
    return Write_Canonical_2(sora,k,term,k);
}

static Bool	  /* DyALog Builtin */
Write_Canonical_2( SP(sora), SP(term) )
{
 display_flags mode = {.display=Display_Std,.op=0,.numbervars=1,.feature=0,.hilog=0,.quoted=1};
 return Write_Term( sora, Sk(sora), term, Sk(term), mode );
}

/*-------------------------------------------------------------------------*/
/* Display_2                                                               */
/*                                                                         */
/*-------------------------------------------------------------------------*/

static Bool Display_2(Sproto,Sproto);

Bool              /* DyALog Wrapper */
DYAM_Display_2(fol_t sora,fol_t term)
{
    fkey_t k=R_TRANS_KEY;
    return Display_2(sora,k,term,k);
}

static Bool	  /* DyALog Builtin */
Display_2( SP(sora), SP(term) )
{
 display_flags mode = {.display=Display_Std,.op=0,.numbervars=1,.feature=0,.hilog=0};
 return Write_Term( sora, Sk(sora), term, Sk(term), mode );
}

/*-------------------------------------------------------------------------*/
/* NL_1                                                                    */
/*                                                                         */
/*-------------------------------------------------------------------------*/

static Bool Nl_1(Sproto);

Bool              /* DyALog Wrapper */
DYAM_Nl_1(fol_t sora_word)
{
    fkey_t k=R_TRANS_KEY;
    return Nl_1(sora_word,k);
}

static Bool	  /* DyALog Builtin */
Nl_1( SP(sora_word) )
{
 int stm;
 Deref(sora_word);
 stm=(sora_word==FOLNIL) 
     ? stm_output
     : Get_Stream_Or_Alias(sora_word,Sk(sora_word),STREAM_FOR_OUTPUT);

 last_output_sora=sora_word;
 Check_Stream_Type(stm,STREAM_FOR_OUTPUT|STREAM_FOR_TEXT);

 Stream_Putc('\n',stm_tbl+stm);
 Succeed;
}

/*-------------------------------------------------------------------------*/
/* NL_0                                                                    */
/*                                                                         */
/*-------------------------------------------------------------------------*/

static Bool Nl_0();

Bool              /* DyALog Wrapper */
DYAM_Nl_0()
{
    return Nl_0();
}

static Bool	  /* DyALog Builtin */
Nl_0()
{
    return Nl_1(FOLNIL,Key0);
}

/*-------------------------------------------------------------------------*/
/* Write_C_Atom_2                                                          */
/*                                                                         */
/*-------------------------------------------------------------------------*/

static void
Display_C_Symbol( char *name, StmInf *port )
{
    char *p,*q;
    char      str[32];
    
    Stream_Putc( '"', port );
    for(p=name;*p;p++)
        switch (*p) {
            case '\0':
                Stream_Putc('\\',port);
                Stream_Putc('0',port);
                break;
                
            case '\\':
                Stream_Putc('\\',port);
                Stream_Putc('\\',port);
                break;

            case '"':
                Stream_Putc('\\',port);
                Stream_Putc('"',port);
                break;

            default:
                if ((q=(char *) strchr(escape_char,*p))){
                    Stream_Putc('\\',port);
                    Stream_Putc(escape_symbol[q-escape_char],port);
                } else if (!isprint(*p)) {
                    sprintf(str,"\\X%2X",(*p) & 0xff );
                    Stream_Puts(str,port);
                } else
                    Stream_Putc(*p,port);
                
                break;
        }
    Stream_Putc( '"',port );
}


static Bool Write_C_Atom_2(Sproto,Sproto);

Bool              /* DyALog Wrapper */
DYAM_Write_C_Atom_2(fol_t sora_word,fol_t atom)
{
    fkey_t k=R_TRANS_KEY;
    return Write_C_Atom_2(sora_word,k,atom,k);
}

static Bool	  /* DyALog Builtin */
Write_C_Atom_2( SP(sora_word), SP(atom) )
{
    int stm;
    Deref(sora_word);
    stm=(sora_word==FOLNIL) 
        ? stm_output
        : Get_Stream_Or_Alias(sora_word,Sk(sora_word),STREAM_FOR_OUTPUT);

    last_output_sora=sora_word;
    Check_Stream_Type(stm,STREAM_FOR_OUTPUT|STREAM_FOR_TEXT);

    Deref_And_Fail_Unless(atom,FOLSMBP(atom));
    Display_C_Symbol(FOLSMB_NAME(atom),INT_TO_STM(stm));
    Succeed;
}

/**********************************************************************
 * Newline_Start/1
 **********************************************************************/

Bool
Newline_Start_1( sfol_t SA )
{
    fol_t A = SA->t;
    fkey_t Sk(A) = SA->k;
    Deref(A);
    if (FOLSMBP(A)) {
        newline_start=FOLSMB_NAME(A);
        Succeed;
    } else if (FOLVARP(A)) {
        Bind(Create_Atom(newline_start),Key0,A);
        Succeed;
    }
    Fail;
}



