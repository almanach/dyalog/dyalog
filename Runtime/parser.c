/*************************************************************
 * $Id$
 * Copyright (C) 1997 - 2006, 2008, 2009, 2010, 2011, 2012, 2015, 2017 Eric de la Clergerie
 * ------------------------------------------------------------
 *
 *   Fol Parser --
 *
 * ------------------------------------------------------------
 * Description
 *
 * ------------------------------------------------------------
 */

#include <string.h>
#include "buffer.h"
#include "token.h"

#include "libdyalog.h"
#include "builtins.h"

char *token_value;
buffer auxbuf;
static Bool lookahead = 0;		/* we need one token of lookahead for - and + */
static TK_Kind lookahead_kind;
static char *lookahead_value;

extern obj_t folsmb_info_find( fol_t, obj_t);
extern Bool Open_3(Sproto,Sproto,Sproto);
extern Bool Close_1(Sproto);

/**********************************************************************
 * Errors
 **********************************************************************/

//const static char errfloat[]       =   "float not implemented";
const static char errdollarquote[] =   "$ident$ not implemented";
const static char erreof[]         =   "unexpected eof";
const static char errpar[]         =   "unexpected `)' ";
const static char errbracket[]     =   "unexpected `]' ";
const static char errbrace[]       =   "unexpected `}' ";
const static char errcoma[]        =   "unexpected `,' ";
const static char errbar[]         =   "unexpected `|' ";
const static char errdot[]         =   "unexpected `.' ";
const static char errtoken[]       =   "unknown token type";
const static char errsequence[]    =   "bad sequence of terms around %&f";
const static char errfeatfun[]     =   "'%&f' not a feature functor";
const static char errfsetfun[]     =   "'%&f' not a finite set functor";
const static char errfeature[]     =   "feature expected instead of %&f";
const static char errmodule[]         =   "unexpected module separator `!'";
//const static char errrange[]     =   "two many arguments for range around %&f";

/**********************************************************************
 * Small Aux
 **********************************************************************/

fol_t folcmp_create_string( char * );

inline
static fol_t
string2list(char *s)
{
  return folcmp_create_string(s);
}

#define ANONYMOUSP(s)  ((s[0]=='_' && !s[1]))

inline
static TK_Kind
peek_token()
{
  if (!lookahead) {
    lookahead=1;
    lookahead_kind=lexer();
    lookahead_value=token_value;
#ifdef DEBUG_PARSE
    print_token(lookahead_kind,lookahead_value);
#endif
  } 
  return lookahead_kind;
}

inline
static char *
get_token_value()		/* to use only after peek_token */
{
  lookahead=0;
  return lookahead_value;
}

/**********************************************************************
 * Variable list
 **********************************************************************/

typedef struct varnode {
  char  *name;
  fol_t  var;
  struct varnode *next;
} *varlist_t;

static varlist_t varlist;
static int       varcount;
static fkey_t    varkey;

static varlist_t
find_variable( const char *name, varlist_t vlp )
{
  for( ; vlp && ( strcmp(vlp->name,name) != 0 ); vlp = vlp->next );
  return vlp;
}

static fol_t
string2variable( char *name )
{
  varlist_t vlp = varlist;
  if (ANONYMOUSP(name) || !(vlp = find_variable(name,vlp))) {
    vlp = (varlist_t) GC_MALLOC( sizeof(struct varnode) );
    vlp->name = name;
    vlp->var = FOLVAR_FROM_INDEX( varcount );
    varcount++;
    vlp->next = varlist;
    varlist = vlp;
  }
  return vlp->var;
}

inline
static void
clean_variables()
{
  varlist = (varlist_t) 0;
  varcount = 0;
}

static fol_t
local_variable_table()
{
  fol_t l = FOLNIL;
  varlist_t vlp = varlist;
  for( ; vlp ; vlp = vlp->next )
    if (!ANONYMOUSP(vlp->name))
      l = folcmp_create_pair(
			       (fol_t) folcmp_create_binary("=",
							      find_folsmb(vlp->name,1),
							      vlp->var),
			       l);
  return l;
}

/**********************************************************************
 * Parse Kinds
 **********************************************************************/

typedef enum { 
  init_state,			/* initial state                   */
  in_args,			/* between 'functor(' and ')'      */
  in_prefix,			/* right arg of prefix             */
  in_infix,			/* right arg of infix              */
  in_feature_args,		/* between 'functor{' and '}'      */
  in_hilog,			/* between 'term (' and ')'        */
  in_list,			/* between '[' and ']'             */
  after_term,			/* after term                      */
  in_par,			/* between '(' and ')'             */
  in_curly,			/* between '{' and '}'             */
  after_coma,			/* after ','                       */
  after_bar,			/* after '|'                       */
  after_feature,		/* after a feature                 */
  after_feature_sep,		/* afer => in a feature term       */
  in_fset_args,                  /* between 'functor[' and ']'      */
  in_module,                     /* when reading a module name      */
  in_range_args                 /* beetwen '$[' and ']' for ranges */
} parse_state;

/**********************************************************************
 * PARSE Stack
 *
 *     * the stack should never be void
 **********************************************************************/

typedef struct psnode {
  fol_t term;
  int   prec;
  parse_state state;
  struct psnode *reducer;
} *pstack_t;

#define PARSE_STACK_SIZE 4096

static pstack_t pstack_realloc (pstack_t reducer);

static struct psnode parse_stack[PARSE_STACK_SIZE];
static pstack_t pstack = &parse_stack[0];
static pstack_t pstack_base = &parse_stack[0];
static pstack_t pstack_stop =  &parse_stack[0] + PARSE_STACK_SIZE;
static unsigned long pstack_size = PARSE_STACK_SIZE;

#ifdef DEBUG_PARSE
static void
show_stack( pstack_t ps, char *s)
{
  printf( "%s\t top=%d\tprec=%d red=%d",
	  s,
	  ps-parse_base,
	  ps->prec,
	  ps->reducer ? (ps->reducer - parse_base) : -1
	  );
  switch (ps->state) {
  case init_state: printf(" state init_state "); break;
  case in_args: printf(" state in_args "); break;
  case in_prefix: printf(" state in_prefix "); break;
  case in_infix: printf(" state in_infix "); break;
  case in_feature_args: printf(" state in_feature_args "); break;
  case in_hilog: printf(" state in_hilog "); break;
  case in_list: printf(" state in_list "); break;
  case after_term: printf(" state after_term "); break;
  case in_par: printf(" state in_par "); break;
  case in_curly: printf(" state in_curly "); break;
  case after_coma: printf(" state after_coma "); break;
  case after_bar: printf(" state after_bar "); break;
  case after_feature: printf(" state after_feature "); break;
  case after_feature_sep: printf(" state after_feature_sep "); break;
  default: printf( " state unknown %d ", ps->state);break;
  }
  if (ps->term) fol_display( ps->term );
  printf( "\n" );
}
#endif

inline
static fol_t
bind_on_derefterm( fol_t value )
{
  if (0 && FOLCMPP(value) && FOLCMP_DEREFP(value)) {
    fol_t X = string2variable("_");
    TRAIL_UBIND(X,varkey,value,varkey);
    return X;
  } else
    return value;
}

inline
static fol_t
string2symbol(const char *s)
{
    fol_t module=FOLNIL;
    
    if (pstack->state == in_module) {
        if (!s)
            Syntax_Error(errmodule);
        module = pstack->term;
        pstack--;
//        dyalog_printf("Module %&f\n",module);
    }

    if (!s)
        return R_MODULE;
    else
        return find_module_folsmb(s,0,module);
}

static pstack_t
shift( fol_t term, int prec, parse_state state, pstack_t reducer )
{
    if (pstack > pstack_stop - 2)
        reducer = pstack_realloc(reducer);
    *(++pstack) = (struct psnode) { term, prec, state, reducer };

#ifdef DEBUG_PARSE
  show_stack(pstack,"SHIFT");
#endif

  return pstack;
}

static void
update( pstack_t head, fol_t term, int prec, parse_state state, pstack_t reducer )
{
  *head = (struct psnode) { term, prec, state, reducer };
  //  term = bind_on_derefterm(term);
  pstack=head;
#ifdef DEBUG_PARSE
  show_stack(head,"REDUCE");
#endif
}

static pstack_t
deref( pstack_t ps )
{
  switch (ps->state) {
  case after_coma:
  case after_feature_sep:
    return ps->reducer;
  default:
    return ps;
  }
}

static void
pstack_init()
{
    pstack = pstack_base;
    pstack_stop = pstack_base + pstack_size;
    *pstack = (struct psnode) { Not_A_Fol, 1400, init_state, (pstack_t) 0};
#ifdef DEBUG_PARSE
    show_stack(pstack, "INIT" );
#endif
}

static pstack_t
pstack_realloc(pstack_t reducer) 
{
    unsigned long new_pstack_size = 2*pstack_size;
    pstack_t new_pstack_base = (pstack_t) malloc(sizeof(struct psnode)*new_pstack_size);
    pstack_t new_pstack = new_pstack_base;
    pstack_t old_pstack = pstack_base;
//    dyalog_printf("Realloc PSTACK %d\n",new_pstack_size);
    for(; old_pstack <= pstack; new_pstack++, old_pstack++) {
        pstack_t reducer = old_pstack->reducer;
        if (reducer)
            reducer = new_pstack_base + (reducer-pstack_base);
        *(new_pstack) = (struct psnode) { old_pstack->term,
                                          old_pstack->prec,
                                          old_pstack->state,
                                          reducer
        };
    }
    if (reducer)
        reducer = new_pstack_base + (reducer-pstack_base);
    pstack_size = new_pstack_size;
    pstack_base = new_pstack_base;
    pstack_stop = pstack_base + pstack_size;
    pstack = new_pstack-1;
//    dyalog_printf("done realloc\n");
    return reducer;
}


/**********************************************************************
 * Input file
 **********************************************************************/

/**********************************************************************
 * Errors
 **********************************************************************/


/**********************************************************************
 * Misc
 **********************************************************************/

inline
static Bool
in_feature_or_fset_args( parse_state state )
{
    return (state == in_feature_args
            || state == in_fset_args
            || state == in_range_args
            );
}

inline
static void
fail_after_term(const fol_t v)
{
    if (pstack->state == after_term)
        Syntax_Error(errsequence,v);
    if ( in_feature_or_fset_args(pstack->state) ||
         (pstack->state == after_coma && in_feature_or_fset_args(pstack->reducer->state)))
        Syntax_Error(errfeature,v);
}


inline
static void
fail_after_icst(const fol_t v)
{
    if (pstack->state == after_term)
        Syntax_Error(errsequence,v);
    if (pstack->state == in_feature_args ||
        (pstack->state == after_coma && pstack->reducer->state == in_feature_args))
        Syntax_Error(errfeature,v);
}

/**********************************************************************
 * Builders
 **********************************************************************/

static void
build_hilog_term( pstack_t head )
        /* h=term h+1=arg1 .. pstack=argN */
{
  pstack_t stop = pstack,arg=head;
  int arity = (stop -arg)+1;
  FOLCMP_WRITE_START(FOLSMB_CONVERT_ARITY(FOLHILOG,arity),arity);
  for ( ; arg <= stop; arg++)
    FOLCMP_WRITE(arg->term);
  update(head,FOLCMP_WRITE_STOP,head->reducer->prec,after_term,head->reducer);
}

static void
build_term( pstack_t head )
        /* head = functor( head+1= arg1 pstack= argN */
{
  pstack_t stop = pstack, arg=head+1;
  int arity = (stop - head) ;

  FOLCMP_WRITE_START(FOLSMB_CONVERT_ARITY(head->term,arity),arity);

  for (; arg <= stop; arg++ )
    FOLCMP_WRITE(arg->term);

  update(head,FOLCMP_WRITE_STOP,head->reducer->prec,after_term,head->reducer);
}

static void
build_list( pstack_t head )
        /* h = [ h+1=arg1 h+2=coma ... pstack = lastarg ] */
{
  fol_t a = FOLNIL;
  pstack_t arg = pstack;
  if (arg->reducer->state == after_bar)
    a = (arg--)->term;
  for( ; arg > head; arg-- ){
    FOLCMP_WRITE_START(FOLCONS,2);
    FOLCMP_WRITE( arg->term );
    FOLCMP_WRITE( a );
    a = FOLCMP_WRITE_STOP;
  }
  update(head,a,head->reducer->prec,after_term,head->reducer);
}

static Bool
check_feature_type_aux( fol_t type, fol_t s)
{
  if (s == type) {
    return 1;
  } else {
    obj_t info = folsmb_info_find(type,INFO_SUBTYPES);
    fol_t subtypes = (info == BFALSE) ? FOLNIL : (fol_t) (CDR(info));
    for( ; FOLPAIRP(subtypes) ; subtypes = FOLPAIR_CDR(subtypes)){
      if (check_feature_type_aux(FOLPAIR_CAR(subtypes),s))
	return 1;
    }
    
    return 0;
    
  }
}

static fol_t fill_feature( fol_t, fol_t, fol_t);

static Bool
check_feature_type( fol_t type, fol_t value )
{
    fkey_t Sk(value) = varkey;
    
    if (!type)
        return 1;

    Deref(value);

    V_LEVEL_DISPLAY(V_DYAM,"Check feature type %&f %&s\n",type,value,Sk(value));

    if (FOLVARP(value)) {
        TRAIL_UBIND(value,Sk(value),fill_feature(type,Not_A_Fol,Not_A_Fol),varkey);
        return 1;
    } else {
        fol_t functor = FOL_FUNCTOR(value);
        obj_t info;
        /* we check if type of value is not an acceptable subtype of
         * type to possibly avoiding an unification + filler building */
        if ( FOLSMBP(functor) && 
                 (info = folsmb_feature_info(functor)) != BFALSE &&
                 FOL_ARITY( ((fol_t) CDR(info)) ) == FOL_ARITY(value) &&
                 check_feature_type_aux(type,FOLSMB_CONVERT_ARITY(functor,0))
             ) {
            return TRUE;
        } else {
            fol_t filler = fill_feature(type,Not_A_Fol,Not_A_Fol);
            Bool res = Unify(value,Sk(value),filler,varkey);
            return res;
        }
        
    }
    
}

static fol_t
check_feature_value(fol_t type, fol_t v) 
{
    if (check_feature_type(type,v)) {
        return v;
    } else if ( FOLSMBP(v)
                && folsmb_feature_info(v) != BFALSE
                && check_feature_type_aux(type,v)
                ) {
            /* if v may be interpreted as a type with missing {}
               we do it
            */
        Syntax_Warning("Promoting symbol %&f to term %&f{}",v,v);
        return fill_feature(v, Not_A_Fol, Not_A_Fol);
    } else
        Syntax_Error( "Conflict for value %&f with expected feature type %&f",v,type);
}

static fol_t
fill_feature( fol_t type, fol_t f, fol_t v )
{
    
    V_LEVEL_DISPLAY(V_DYAM,"------ Fill %&f %&f %&f\n",type ? type : FOLNIL,f ? f : FOLNIL, v ? v :FOLNIL);

  if (!type) 
    return string2variable( "_" ); /* use an anonymous variable */
  else {
    obj_t info = folsmb_feature_info( type );
    fol_t table = (fol_t) CDR( info ); /* WARNING: should check if type has a feature entry */
    int arity = FOL_ARITY(table);
    fol_t functor = FOLSMB_CONVERT_ARITY(type,arity);
    fol_t value;
    int i;

    if (arity == 0)
      value = functor;
    else {
        
      FOLCMP_WRITE_START(functor,arity);
      
      for(i=1; i <= arity; i++){
	fol_t info = FOLCMP_REF(table,i);
	fol_t feature =  FOLCMPP(info) ? FOLCMP_REF(info,1) : info;
	fol_t ftype = FOLCMPP(info) ? FOLCMP_REF(info,2) : Not_A_Fol;
        value = (feature == f)
            ? check_feature_value(ftype,v)
            : fill_feature(ftype, Not_A_Fol, Not_A_Fol);
	FOLCMP_WRITE(value);
      }

      value = bind_on_derefterm(FOLCMP_WRITE_STOP);

    }
    
    V_LEVEL_DISPLAY( V_DYAM, "return %&s\n",value ? value : FOLNIL, varkey);
    
    return value;

  }
}

static void
build_feature( pstack_t head )
        /* h= func{ h+1=feat1 h+2=val1 .., pstack = lastval */
{
  pstack_t stop = pstack;
  fol_t table = (fol_t) CDR( folsmb_feature_info( head->term ) );
  int arity = FOL_ARITY(table);
  fol_t functor = FOLSMB_CONVERT_ARITY(head->term,arity);
  pstack_t arg;
  fol_t value;
  int i=1;

//  dyalog_printf("Feature table %&f: %&f\n", head->term, table);
  
  if (arity == 0) 
    value = functor;
  else {

    FOLCMP_WRITE_START(functor,arity);

    for(i=1; i <= arity; i++){
      fol_t info = FOLCMP_REF(table,i);
      fol_t feature =  FOLCMPP(info) ? FOLCMP_REF(info,1) : info;
      fol_t type = FOLCMPP(info) ? FOLCMP_REF(info,2) : Not_A_Fol;
      fol_t final_value = Not_A_Fol;
      
      for(arg=head+1; arg <= stop; arg += 2){
          if (arg->term == feature ){
              arg->term = Not_A_Fol;
              value = check_feature_value(type,(arg+1)->term);
              if (final_value != Not_A_Fol && !Unify(value,varkey,final_value,varkey))
                  Syntax_Error("Failed immediate unification for duplicate feature `%&f'",feature);
              else
                  final_value = value;
          }
      }

      if (final_value == Not_A_Fol)
          final_value = fill_feature(type,Not_A_Fol,Not_A_Fol);

      FOLCMP_WRITE( final_value );
      
    }

    value = bind_on_derefterm(FOLCMP_WRITE_STOP);
//    dyalog_printf( "----\n%&s\n", value, varkey );
    
    for( arg = head+1; arg <= stop ; arg += 2 )
      if (arg->term != Not_A_Fol){
          Syntax_Error("feature %&f not allowed within feature structure %&f{}",
                       arg->term,
                       FOLSMB_MODULE(head->term));
      }

    V_LEVEL_DISPLAY(V_DYAM,"Feature term %&s\n",value, varkey);
    
    
  }

  update(head,value,head->reducer->prec,after_term,head->reducer);
  
}

static void
build_curly( pstack_t head )
        /* head = { ; head+1 = term ; head+2 = } */
{
  FOLCMP_WRITE_START( FOLCURLYFUN, 1);
  FOLCMP_WRITE( (head+1)->term );
  update(head,FOLCMP_WRITE_STOP,head->reducer->prec,after_term,head->reducer);
}

static void
build_range( pstack_t head )
        /* h=[ h+1=min h+2 = max */
{
    pstack_t stop = pstack;
    unsigned int arity = stop-head;
    if (arity != 2) {
        Syntax_Error("range construction: 2 arguments expected but %d found",arity);
    } else {
        fol_t min = (head+1)->term;
        fol_t max = (head+2)->term;
        if (!(FOLINTP(min) && FOLINTP(max))) {
            Syntax_Error("range construction: number expected %&f and %&f",min,max);
        }
        if (CFOLINT(min) > CFOLINT(max)) {
            Syntax_Error("range construction: min=%&f not smaller than max=%&f",min,max);
        }
        if (CFOLINT(min) == CFOLINT(max)) {
            update(head,min,head->reducer->prec,after_term,head->reducer);
        } else {
            FOLCMP_WRITE_START( FOLRANGE, 3);
            FOLCMP_WRITE( string2variable("_") );
            FOLCMP_WRITE( (head+1)->term );
            FOLCMP_WRITE( (head+2)->term );
            update(head,FOLCMP_WRITE_STOP,head->reducer->prec,after_term,head->reducer);
        }
    }
}


int
FSet_Try_Reduce( fol_t value)
{
    int arity = FOLCMP_ARITY(value);
    fol_t *arg= &FOLCMP_REF(value,3);
    fol_t *stop= arg+(arity-2);
    int k=0;
    int found=0;
    for(; arg < stop; arg++, k += FSET_BIT_PER_WORD) {
        unsigned long n=UCFOLINT(*arg);
        if (n) {
            int i=0;
            if (found)
                return 0;
            for(; ((unsigned long) 1 << i) < n; i++);
            if ( ((unsigned long) 1 << i) == n) {
                found =  k+(i+1);
            } else {
                return 0;
            }
        }
    }
    return found;
}

static Bool
FSet_Negation(fol_t arg, fol_t elem )
{
    if (arg !=Not_A_Fol
        && FOLCMPP(arg)
        && FOLCMP_FUNCTOR(arg) == find_folsmb("~",1)){
            // Entering simple negated element or list of negated elements
        arg=FOLCMP_REF(arg,1);
        for(;
            FOLPAIRP(arg) && (FOLPAIR_CAR(arg) != elem);
            arg=FOLPAIR_CDR(arg)
            );
        if (!FOLPAIRP(arg) && arg != elem)
            return 1;
    }
    return 0;
}


static void
build_finite_set( pstack_t head )
        /* h= func[ h+1=element1 .. pstack = lastelem */
{
    pstack_t stop = pstack;
    fol_t info = (fol_t) CDR( folsmb_fset_info( head->term ) );
    fol_t table = FOLPAIRP(info) ? FOLPAIR_CAR(info) : info;
    int arity = FOLCMP_ARITY(table);
    fol_t fmask = FOLPAIRP(info) ? FOLPAIR_CDR(info) : FOLNIL;
    pstack_t arg;
    int words = FSET_ARITY_TO_WORD(arity);
    int k=0;
    Bool neg=0;
    fol_t *ptr= &FOLCMP_REF(table,1);
    unsigned long filled=0;

        /*  value is just a fol_t being built, not yet a true fol_t */
    fol_t value = (fol_t) FOLCMP_WRITE_START(FOLSMB_CONVERT_ARITY(FOLFSET,2+words), 2+words);
    
    FOLCMP_WRITE( string2variable("_") );
    FOLCMP_WRITE( table );

//    dyalog_printf("try build fset %&f\n",table);
        
    for(k=1; k <= words ; k++) {
        unsigned int larity=(k<words) ? FSET_BIT_PER_WORD : (arity - (k-1)*FSET_BIT_PER_WORD);
        unsigned long mask = FOLNILP(fmask) ? (((unsigned long) 1 << larity) - 1) : UCFOLINT( FOLCMP_REF(fmask,k));
        unsigned long n=0;
        unsigned int i;
//        dyalog_printf("fset %d %d %d %d %x\n",arity,words,k,larity,mask);
        for(i=1; i<=larity; i++, ptr++ ) {
            fol_t elem = *ptr;
//            dyalog_printf("Testing fset elt %&f mask=%x fmask=%&f larity=%d\n",elem,mask,fmask,larity);
            if (mask & ((unsigned long) 1 << (i-1))) {
                for(arg=head+1; arg<=stop ; ++arg ) {
                    if (arg->term == elem) {
//                        dyalog_printf("\tFound fset elt %&f\n",elem);
                        n |= ((unsigned long) 1 << (i-1) );
                        arg->term=Not_A_Fol;
                            /* Do not force break to handle repeated values ! */
                            // break;
                    } else if (FSet_Negation(arg->term,elem)) {
                        n |= ((unsigned long) 1 << (i-1) );
                    }           
                    
                }
            }
        }
            //      dyalog_printf("-->%x [%x]\n",n,(unsigned int)DFOLINT(n));
//        dyalog_printf("add fset block %&f %x max_bit=%d\n",UDFOLINT(n),n,FSET_BIT_PER_WORD);
        FOLCMP_WRITE( UDFOLINT(n) );
        filled |= n;
    }
    
    for( arg=head+1; arg<=stop ; ++arg) {
        fol_t argt = arg->term;
        if (argt != Not_A_Fol){
            if( FOLCMPP(argt)
                && FOLCMP_FUNCTOR(argt) == find_folsmb("~",1)){
                neg=1;
                arg->term=Not_A_Fol;
            } else {
                Syntax_Error("element %&f not allowed whithin finite set %&f[]",
                             argt,
                             FOLSMB_MODULE(head->term));
            }
        }
    }

    if (!filled){
        if (neg) {
            Syntax_Error("No possible values in finite set %&f",
                         FOLSMB_MODULE(head->term));
        } else {
            for(k=1;k<=words ;k++) {
                unsigned int larity=(k<words) ? FSET_BIT_PER_WORD : (arity - (k-1)*FSET_BIT_PER_WORD);
                fol_t mask = FOLNILP(fmask) ? UDFOLINT((((unsigned long) 1 << larity) - 1)) : FOLCMP_REF(fmask,k);
                FOLCMP_SET(value,2+k,mask);
            }
        }
    }
    
    if ((k=FSet_Try_Reduce(value))) {
        FOLCMP_WRITE_ABORT;
        value=FOLCMP_REF(table,k);
    } else {
        value = FOLCMP_WRITE_STOP;
    }

//    dyalog_printf("parser fset %&f table=%&f\n",value,table);
        
    update(head,value,head->reducer->prec,after_term,head->reducer);
   
}



/**********************************************************************
 * Reducers
 **********************************************************************/

static Bool
reduce( int lprec )
     /* Reduce a term between a left and right barrier
	Returns 1 if in mode after_term, 0 otherwise
	Fail if in infix
      */
{
  Bool updated=0;
  pstack_t ps = pstack;
  fol_t  right = (ps->state==after_term) ? (ps--)->term : Not_A_Fol;

  while (1) {
    if (ps->state == in_prefix
	&& (OPINFO_PREC(FOLSMB_PREFIX(ps->term)) <= lprec)) {
      if (right == Not_A_Fol) {
	right = FOLSMB_CONVERT_ARITY((ps--)->term,0); /* transform into constant */
      } else {
	FOLCMP_WRITE_START( (ps--)->term, 1 );
	FOLCMP_WRITE( right );
	right = FOLCMP_WRITE_STOP;
      }
    } else if (ps->state == in_infix
	       && (right != Not_A_Fol && OPINFO_PREC(FOLSMB_INFIX(ps->term)) <= lprec)) {
				
      if (ps->term == find_folsmb("::",2)) { /* WARNING : Experiemental */
          fol_t v = (--ps)->term;
          ps--;
          if (!Unify(v,varkey,right,varkey)) 
              Syntax_Error("Failed immediate unification (::)");
          right=v;
      } else if (ps->term == find_folsmb("=>",2)) { /* WARNING : Experiemental */
	fol_t feature = (--ps)->term;
	obj_t info;
	fol_t intro;
	ps--;
	if (!FOLSMBP(feature))
	  Syntax_Error("Symbol expected as feature on left of => instead of %&f",feature);
	if ((info = folsmb_info_find(feature,INFO_INTRO)) == BFALSE)
	  Syntax_Error("Missing intro information for feature `%&f'",feature);
	intro = (fol_t) CDR(info);
	right=fill_feature(intro,feature,right);
      } else {
	FOLCMP_WRITE_START( (ps--)->term,2);
	FOLCMP_WRITE( (ps--)->term );
	FOLCMP_WRITE( right );
	right = FOLCMP_WRITE_STOP;
      }
    } else
      break;
    updated=1;
  }
  if (right != Not_A_Fol && updated){
    update(ps+1,right,ps->prec,after_term,deref(ps));
  }
  return (Bool) (right != Not_A_Fol);
}

static Bool
reduce_before_op( int mask )	/* Reduce potential left argument of a postfix or infix op */
{
  return reduce(OPINFO_LPREC(mask))
    && (pstack->state == after_term)
    && (pstack->prec >= OPINFO_PREC(mask) );
}

static void
reduce_args()			/* Reduce after a closing ')' */
{
  reduce(1400);
  switch (pstack->state) {
  case in_args:		        /* p() => p */
    update(pstack,pstack->term,pstack->reducer->prec,after_term,pstack->reducer);
    break;
  case in_hilog:		/* t () => apply(t) */
    build_hilog_term(pstack);
    break;
  case after_term:		/* <reducer>(.... ,t) => look reducer */
    switch (pstack->reducer->state) {
    case in_args:
      build_term(pstack->reducer);
      break;
    case in_hilog:
      build_hilog_term(pstack->reducer);
      break;
    case in_par:
      pstack--;
      update(pstack,(pstack+1)->term,pstack->reducer->prec,after_term,pstack->reducer);
      break;
    default:
      Syntax_Error( errpar );
    }
    break;
  default:
    Syntax_Error( errpar );
    break;
  }
}

static void
reduce_list()			/* Reduce after a closing ']' */
{
  reduce(1400);
  switch (pstack->state) {
      case in_fset_args:
          build_finite_set(pstack);
          break;
      case in_range_args:       /* $[min,max] */
          build_range(pstack);
          break;
      case in_list:		/* [] */
          update(pstack,FOLNIL,pstack->reducer->prec,after_term,pstack->reducer);
          break;
      case after_term:
          switch (pstack->reducer->state) {
              case in_fset_args:
                  build_finite_set(pstack->reducer);
                  break;
              case in_range_args: /* $[min,max] */
                  build_range(pstack->reducer);
                  break;
              case in_list:
                  build_list(pstack->reducer);
                  break;
              case after_bar:
                  build_list(pstack->reducer->reducer);
                  break;
              default:
                  Syntax_Error( errbracket );
          }
          break;
      default:
          Syntax_Error( errbracket );
  }
}

static void
reduce_brace()
     /* Reduce after a closing '}'
	    could be closing '{...}' or 'fun{ ... }'
      */
{
    reduce(1400);
    switch (pstack->state) {
        case in_curly:		/* {}  */
            update(pstack,FOLCURLYCST,pstack->reducer->prec,after_term,pstack->reducer);
            break;
        case in_feature_args:		/* f{} with f functor*/
            build_feature(pstack);
            break;
        case in_fset_args:        /* p{} with p FSET */
            build_finite_set(pstack);
            break;
        case in_range_args:     /* $[min,max] */
            build_range(pstack);
            break;
        case after_term:
            switch (pstack->reducer->state) {
                case in_curly:
                    build_curly(pstack->reducer);
                    break;
                case in_feature_args:
                    build_feature(pstack->reducer);
                    break;
                case in_fset_args:
                    build_finite_set(pstack->reducer);
                    break;
                case in_range_args:       /* $[min,max] */
                    build_range(pstack->reducer);
                    break;
                default:
                    Syntax_Error( errbrace );
            }
            break;
        default:
            Syntax_Error( errbrace );
    }
}

static void
reduce_dot()   /* Reduce after a TK_DOT */
{
    reduce(1400);
#ifdef DEBUG_PARSE
    show_stack(pstack,"DOT");
#endif
    if (pstack->state != after_term || (pstack-1)->state != init_state) 
        Syntax_Error( errdot );
}

static void
reduce_eof()   /* Reduce after a TK_EOF */
{
    reduce(1400);
}

/* Does not seem to be used
static void
reduce_modsep()                 / * reduce after a TK_MODSEP * /
{
    if ((pstack->state != after_term || pstack->state != after_feature ||
         pstack->state != in_module) &&
        (!FOLSMBP(pstack->term)))
        Syntax_Error( errmodule );
    update(pstack,pstack->term,pstack->reducer->prec,in_module,pstack->reducer);
}
*/

static void
reduce_postfix( fol_t op )
{
  FOLCMP_WRITE_START( FOLSMB_CONVERT_ARITY(op,1), 1);
  FOLCMP_WRITE( pstack->term );
  update(pstack,FOLCMP_WRITE_STOP,pstack->reducer->prec,after_term,pstack->reducer);
}

/**********************************************************************
 * Parser
 **********************************************************************/

static void
parser( TK_Kind kind, char *value )
{
  fol_t  op;
  int    mask;

  switch (kind) {

				/* Immediate cases */

  case TK_VAR:			/* variable */
      op = string2variable(value);
      fail_after_term(op);
      shift(op,pstack->prec,after_term,deref(pstack));
      break;
  case TK_INT:			/* integer */
      op = DFOLINT((long)value);
      fail_after_icst(op);
      shift(op,pstack->prec,after_term,deref(pstack));
      break;
  case TK_CHAR:			/* char */
      op = DFOLCHAR(*value);
      fail_after_icst(op);
      shift(op,pstack->prec,after_term,deref(pstack));
      break;
  case TK_CHAR_LIST:		/* char list */
      op = string2list(value);
      fail_after_term(op);
      shift(op,pstack->prec,after_term,deref(pstack));
      break;
  case TK_REAL:			/* float */
//      Syntax_Error( errfloat );
      op = (fol_t) value;
      fail_after_icst(op);
      shift(op,pstack->prec,after_term,deref(pstack));
      break;
  case TK_STR:			/* $ident$ */
      Syntax_Error( errdollarquote );
      break;

				/* Starters */

  case TK_FUNC:			/* start a functor-based term or an infix*/
    op = string2symbol(value);
    if ((mask = FOLSMB_INFIX(op)) && reduce_before_op(mask)) {
      shift(FOLSMB_CONVERT_ARITY(op,2),OPINFO_RPREC(mask),in_infix,deref(pstack));
      shift(Not_A_Fol,1200,in_par,deref(pstack));
    } else {
        fail_after_term(op);
        shift(op,999,(FOLSMB(op)->is_hilog ? in_hilog : in_args),deref(pstack));
    }
    break;

      case TK_FEAT_FUNC:		/* start a feature-based term */
//          op=find_module_folsmb("feature",0,string2symbol(value));
          op=string2symbol(value);
          if ((pstack->state == after_feature) && !strcmp(value,"=>")) {
                  /* feature =>{ */
              update(pstack,pstack->term,pstack->reducer->prec,after_feature_sep,pstack->reducer);
              shift(Not_A_Fol,1200,in_curly,deref(pstack));
          } else if ((mask = FOLSMB_INFIX(op)) && reduce_before_op(mask)) {
                  /* priority of infix on feature functor interpretation */
                  /* ex. X ={a,b} */
              shift(FOLSMB_CONVERT_ARITY(op,2),OPINFO_RPREC(mask),in_infix,deref(pstack));
              shift(Not_A_Fol,1200,in_curly,deref(pstack));
          } else {
              fol_t fs_op=find_module_folsmb(FEATURE_SPACE,0,op);
              fail_after_term(fs_op);
              if (FOLSMB(fs_op)->is_feature){
                  shift(fs_op,999,in_feature_args,deref(pstack));
              } else if ((mask = FOLSMB(op)->prefix) && pstack->prec >= OPINFO_PREC(mask)) {
                      /* instead of raising an error, use prefix op */
                  shift(FOLSMB_CONVERT_ARITY(op,1),OPINFO_RPREC(mask),in_prefix,deref(pstack));
                  shift(Not_A_Fol,1200,in_curly,deref(pstack));
              } else {
                  Syntax_Error( errfeatfun, fs_op ); 
              }
          }
          break;
          
      case TK_FSET_FUNC:          /* start a fset term or an infix with a list */
          op=string2symbol(value);
          if ((pstack->state == after_feature) && !strcmp(value,"=>")) {
                  /* feature =>[ */
              update(pstack,pstack->term,pstack->reducer->prec,after_feature_sep,pstack->reducer);
              shift(Not_A_Fol,999,in_list,deref(pstack));
          } else if ((mask = FOLSMB_INFIX(op)) && reduce_before_op(mask)) {
                                /* priority of infix on fset interpretation */
                                /*  ex. X=[a,b,c] */
              shift(FOLSMB_CONVERT_ARITY(op,2),OPINFO_RPREC(mask),in_infix,deref(pstack));
              shift(Not_A_Fol,999,in_list,deref(pstack));
          } else if (!strcmp(value,"$")) {
              shift(Not_A_Fol,999,in_range_args,deref(pstack));
          } else {
              fol_t fset_op=find_module_folsmb(FSET_SPACE,0,op);
              fail_after_term(fset_op);
              if (FOLSMB(fset_op)->is_fset) {
                  shift(fset_op,999,in_fset_args,deref(pstack));
              } else if ((mask = FOLSMB(op)->prefix) && pstack->prec >= OPINFO_PREC(mask)) {
                      /* instead of raising an error, use prefix op */
                  shift(FOLSMB_CONVERT_ARITY(op,1),OPINFO_RPREC(mask),in_prefix,deref(pstack));
                  shift(Not_A_Fol,999,in_list,deref(pstack));
              } else {
                  Syntax_Error( errfsetfun, fset_op ); 
              }
          }
          break;

    
  case TK_ATOM:			/* atoms */
    op = string2symbol(value);
    if ( pstack->state == in_feature_args
	 || (pstack->state == after_coma && pstack->reducer->state == in_feature_args )) {
      shift(op,pstack->prec,after_feature,deref(pstack));
    } else if ((pstack->state == after_feature) && !strcmp(value,"=>")) {
      /* feature => value */
      update(pstack,pstack->term,pstack->reducer->prec,after_feature_sep,pstack->reducer);
    } else if ((mask = FOLSMB(op)->infix_or_postfix) && reduce_before_op(mask)){
      if (mask & OPINFO_INFIX)		/* op used as infix */
	shift(FOLSMB_CONVERT_ARITY(op,2),OPINFO_RPREC(mask),in_infix,deref(pstack));
      else				/* op used as postfix */
	reduce_postfix(op);
    } else if (pstack->state == after_term) {
      Syntax_Error( errsequence, op );
    } else if ( !strcmp(value,"-") && (peek_token() == TK_INT)  ) {
        shift(DFOLINT(-(long)get_token_value()),pstack->prec,after_term,deref(pstack));
    } else if ( !strcmp(value,"-") && (peek_token() == TK_REAL)  ) {
        shift(DFOLFLT(-CFOLFLT((obj_t)get_token_value())),pstack->prec,after_term,deref(pstack));
    } else if ( !strcmp(value,"+") && (peek_token() == TK_INT) ) {
        shift(DFOLINT((long)get_token_value()),pstack->prec,after_term,deref(pstack));
    } else if ( !strcmp(value,"+") && (peek_token() == TK_REAL) ) {
        shift((fol_t)get_token_value(),pstack->prec,after_term,deref(pstack));
    } else if ((mask = FOLSMB(op)->prefix) && pstack->prec >= OPINFO_PREC(mask))
				/* op used as prefix */
      shift(FOLSMB_CONVERT_ARITY(op,1),OPINFO_RPREC(mask),in_prefix,deref(pstack));
    else			/* op used as constant */
      shift(op,pstack->prec,after_term,deref(pstack));
    break;

				/* Punctuation Tokens */

  case TK_COMA:			/* either punctuation or infix op or constant */
    op = FOLCOMA;
    if (reduce_before_op((mask=FOLSMB_INFIX(op))))
      shift(FOLCOMA,OPINFO_RPREC(mask),in_infix,deref(pstack));
    else if (reduce(1400) && (pstack->reducer->state != after_bar))
      update(pstack,pstack->term,pstack->reducer->prec,after_coma,pstack->reducer);
    else
      Syntax_Error( errcoma );	                        /* ERROR */
    break;

  case TK_LPAR:			/* start either an par exp or hilog arg list*/
    if (pstack->state == after_term)
      update(pstack,pstack->term,999,in_hilog,pstack->reducer);
    else
      shift(Not_A_Fol,1200,in_par,deref(pstack));
    break;

  case TK_RPAR:			/* end functor arg list, hilog arg list or par exp */
    reduce_args();
    break;

  case TK_LBRACKET:		/* start list */
    fail_after_term(DFOLCHAR('['));
    shift(Not_A_Fol,999,in_list,deref(pstack));
    break;

  case TK_BAR:			/* explicit cons in a list or alias for ; */
    if (reduce_before_op((mask=FOLSMB_INFIX(FOLSEMICOMA))))
      shift(FOLSEMICOMA,OPINFO_RPREC(mask),in_infix,deref(pstack));
    else if (reduce(1400))
      update(pstack,pstack->term,pstack->reducer->prec,after_bar,pstack->reducer);
    else
      Syntax_Error( errbar );
    break;
    
  case TK_RBRACKET:		/* end list */
    reduce_list();
    break;

  case TK_LBRACE:		/* start curly exp */
    fail_after_term(DFOLCHAR('{'));
    shift(Not_A_Fol,1200,in_curly,deref(pstack));
    break;

  case TK_RBRACE:		/* end curly exp or feature arg list */
    reduce_brace();
    break;

  case TK_DOT:			/* end clause */
    reduce_dot();
    break;
                               
  case TK_EOF:                  /* EOF */
    reduce_eof();
    break;

  case TK_MODULE:               /* Module part */
      op=string2symbol(value);
      shift(op,pstack->prec,in_module,pstack->reducer);
      break;

    
  default:
    Syntax_Error( errtoken );
  }
}

/**********************************************************************
 * Head functions
 **********************************************************************/

#ifdef DEBUG_PARSE
static void
print_token( TK_Kind kind, char *ptr)
{
  switch (kind) {
  case TK_VAR		: printf("TK_VAR: %s\t", ptr); break;
  case TK_FUNC		: printf("TK_FUNC: %s\t", ptr); break;
  case TK_FEAT_FUNC	: printf("TK_FEAT_FUNC: %s\t", ptr); break;
  case TK_INT		: printf("TK_INT: %d\t", (int)ptr); break;
  case TK_CHAR		: printf("TK_CHAR: %c\t", *ptr); break;
  case TK_ATOM		: printf("TK_ATOM: %s\t", ptr); break;
  case TK_DOT		: printf("TK_DOT"); break;
  case TK_EOF		: printf("TK_EOF"); break;
  case TK_STR		: printf("TK_STR: %s\t", ptr); break;
  case TK_CHAR_LIST	: printf("TK_CHAR_LIST: %s\t", ptr); break;
  case TK_BAR           : printf("TK_BAR" ); break;
  case TK_LPAR          : printf( "TK_LPAR" ); break;
  case TK_RPAR          : printf( "TK_RPAR" ); break;
  case TK_LBRACKET      : printf( "TK_LBRACKET" ); break;
  case TK_RBRACKET      : printf( "TK_RBRACKET" ); break;
  case TK_LBRACE        : printf( "TK_LBRACE" ); break;
  case TK_RBRACE        : printf( "TK_RBRACE" ); break;
  case TK_COMA          : printf( "TK_COMA" ); break;
  default               : printf("Punctuation");break;
  }
  printf("\n");
}
#endif

static void
parse_init()
{
  auxbuf = buffer_new(BUFFER_SIZE);
  lookahead=0;
  pstack_init();
  varkey = LSTACK_PUSH_VOID;
}

static void
parse_loop(obj_t handler)
{
  TK_Kind kind;
  char *value;
  parse_init();
  Trail;
  do {
    if (!lookahead) {
      kind = lexer();
      value = token_value;
    } else {
      kind = lookahead_kind;
      value = lookahead_value;
    }
    lookahead=0;
#ifdef DEBUG_PARSE
    print_token(kind, value);
#endif
    parser(kind,value);
    if (kind == TK_DOT){
      PROCEDURE_ENTRY(handler)(handler, (obj_t)(pstack--)->term, (obj_t)(varkey), BEOA);
      clean_variables();
      Untrail;
      Trail;
      varkey = LSTACK_PUSH_VOID;
    }
  } while (kind != TK_EOF);
  Untrail;
  if (pstack->state != init_state)
    Syntax_Error( erreof );
}

void
parse_from_stm(int stm, obj_t handler)
{
  lexer_mode=1;
  lexer_port=(char*)INT_TO_STM(stm);
  parse_loop(handler);  
}

void
parse_from_file(const char *filename, obj_t handler)
{
  fol_t    atom_file_name = Create_Atom( filename );
  fol_t    stm_word = FOLVAR_FROM_INDEX(0);
  fkey_t   Sk(stm_word);
  int      stm;

  Trail;
  Sk(stm_word) = (fkey_t) LSTACK_PUSH_VOID;
  if (!Open_3(atom_file_name,Key0,atom_read,Key0,stm_word,Sk(stm_word)))
    exit(1);
  Deref(stm_word);
  stm = CFOLINT(stm_word);
  Untrail;

  parse_from_stm(stm,handler);
  Close_1( stm_word, Key0 ); 
}

void
parse_from_stdin( obj_t handler)
{
  parse_from_stm(stm_stdin,handler);
}

void
parse_from_string(char *s, obj_t handler)
{
  lexer_mode=2;
  lexer_port=s;
  parse_loop(handler);
}

/**********************************************************************
 *                        BUILT-IN PREDICATES
 **********************************************************************/


static Bool Read_Term_3(Sproto,Sproto,Sproto);

Bool              /* DyALog Wrapper */
DYAM_Read_Term_3(fol_t sora,fol_t term,fol_t vars)
{
    fkey_t k=R_TRANS_KEY;
    return Read_Term_3(sora,k,term,k,vars,k);
}

static Bool	  /* DyALog Builtin */
Read_Term_3( SP(sora), SP(term),SP(vars) )
{
 int     stm;
 char *value;
 TK_Kind kind;
 Sdecl(read_word);
 Sdecl(read_vars);

 Deref(sora);

 V_LEVEL_DISPLAY( V_DYAM, "\tread_term on channel %&s\n", sora, Sk(sora));

 stm= sora == FOLNIL
     ? stm_input
     : Get_Stream_Or_Alias(sora,Sk(sora),STREAM_NOTHING);

 if (stm < 0) Fail;

 lexer_mode=1;
 lexer_port=(char*)INT_TO_STM(stm);

 parse_init();
 do {
   if (!lookahead) {
     kind = lexer();
     value = token_value;
   } else {
     kind = lookahead_kind;
     value = lookahead_value;
   }
   lookahead=0;
#ifdef DEBUG_PARSE
   print_token(kind, value);
#endif
   parser(kind,value);
 } while (kind != TK_DOT && kind != TK_EOF);
 
 if (kind == TK_EOF) {
         /* read eof after reading some unfinished term => error */
     if (pstack->state != init_state) 
         Syntax_Error(erreof);
         /* lexer buffer is automatically disposed of */
     clean_variables();
     return Unify( term, Sk(term), atom_eof, Key0 );
 } else {
         /* we have to explicitely free the lexer buffer for stm */
     lexer_mode = -1;
     lexer();

// Sk(read_vars) = Sk(read_word) = LSTACK_PUSH_VOID;
     Sk(read_vars) = Sk(read_word) = varkey;
     read_word = (pstack--)->term;
     read_vars = local_variable_table();

     clean_variables();
     return
         Unify( term, Sk(term), read_word, Sk(read_word)) &&
         Unify( vars, Sk(vars), read_vars, Sk(read_vars)) ;
 }
 
}







