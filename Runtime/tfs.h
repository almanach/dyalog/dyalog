/* $Id$
 * Copyright (C) 1998 Eric de la Clergerie
 * ------------------------------------------------------------
 *
 *   tfs -- Typed Feature Structure header file
 *
 * ------------------------------------------------------------
 *
 * ------------------------------------------------------------
 */

void   Tfs_Init()   __attribute__ (( weak ));
Bool Tfs_Unif( fol_t, fkey_t, fol_t, fkey_t ) __attribute__ (( weak ));
Bool Tfs_Subs( fol_t, fkey_t, fol_t, fkey_t ) __attribute__ (( weak ));

Bool Tfs_Subtype_Unif( fol_t, fkey_t, fol_t, fkey_t );
Bool Tfs_Simple_Subsume( fol_t, fkey_t, fol_t, fkey_t );



