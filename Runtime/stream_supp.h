/*-------------------------------------------------------------------------*/
/* Buit-In Predicates                                   Daniel Diaz - 1996 */
/* Stream support - header file                                            */
/*                                                                         */
/* stream_supp.h                          Copyright (C) 1996, 2004, 2017, INRIA France */
/*-------------------------------------------------------------------------*/

#ifdef __cplusplus
extern "C" {
#endif    

#include <stdio.h>



/*---------------------------------*/
/* Constants                       */
/*---------------------------------*/



#define STREAM_PB_SIZE             8              /* push back buffer size */
#define TTY_BUFFER_SIZE            256




#define STREAM_MODE_READ           0
#define STREAM_MODE_WRITE          1
#define STREAM_MODE_APPEND         2

#define STREAM_EOF_ACTION_ERROR    0
#define STREAM_EOF_ACTION_EOF_CODE 1
#define STREAM_EOF_ACTION_RESET    2


#define STREAM_EOF_NOT             0
#define STREAM_EOF_AT              1
#define STREAM_EOF_PAST            2



                                        /* masks for stream property tests */
#define STREAM_NOTHING             0
#define STREAM_FOR_INPUT           1
#define STREAM_FOR_OUTPUT          2
#define STREAM_FOR_TEXT            4
#define STREAM_FOR_BINARY          8




#define STREAM_FCT_UNDEFINED       ((StmFct) (-1))     /* for optional fct */




/*---------------------------------*/
/* Type Definitions                */
/*---------------------------------*/

typedef struct                           /* Stream properties              */
    {                                    /* ------------------------------ */
     unsigned mode:2;                    /* see STREAM_MODE_xxx defs       */
     unsigned input:1;                   /* is it am input  stream ?       */
     unsigned output:1;                  /* is it an output stream ?       */
     unsigned text:1;                    /* is it a text stream (or binary)*/
     unsigned reposition:1;              /* can it be repositioned ?       */
     unsigned eof_action:2;              /* see STREAM_EOF_ACTION_xxx defs */
     unsigned tty:1;                     /* is it a tty ?                  */
     unsigned other:8;                   /* other properties (user free)   */
    }StmProp;

typedef struct                           /* TTY stream information         */
    {                                    /* ------------------------------ */
     FILE    *f;                         /* associated file                */
//     char     buff[TTY_BUFFER_SIZE];     /* current buffer (end with '\0') */
        char    *buff;   
     char    *ptr;                       /* current pointer into the buffer*/
    }TTYInf;

typedef struct                           /* Push Back stack                */
    {                                    /* ------------------------------ */
     int      buff[STREAM_PB_SIZE];      /* the buffer                     */
     int     *ptr;                       /* pointer into the buffer        */
     int      nb_elems;                  /* # of elements in the buffer    */
    }PbStk;

typedef int (*StmFct)();                 /* generic type for file functions*/

typedef struct                           /* Stream information             */
    {                                    /* ------------------------------ */
     fol_t    atom_file_name;            /* atom associated to the filename*/
     long     file;                      /* file access(FILE *,TTYInf *)!=0*/
     StmProp  prop;                      /* assoctiated properties         */
                                         /* ----- Basic I/O functions ---- */
     StmFct   fct_getc;                  /* get char function (mandatory)  */
     StmFct   fct_putc;                  /* put char function (mandatory)  */
     StmFct   fct_flush;                 /* flush    function (optional)   */
     StmFct   fct_close;                 /* close    function (optional)   */
     StmFct   fct_tell;                  /* tell     function (optional)   */
     StmFct   fct_seek;                  /* seek     function (optional)   */
     StmFct   fct_clearerr;              /* clearerr function (optional)   */
                                         /* ------ Read information  ----- */
     Bool     eof_reached;               /* eof char has been read ?       */
     PbStk    pb_char;                   /* character push back stack      */
                                         /* ---- Position information  --- */
     int      char_count;                /* character read count           */
     int      line_count;                /* line read count                */
     int      line_pos;                  /* line position                  */
     PbStk    pb_line_pos;               /* line position push back stack  */
    }StmInf;

typedef struct                           /* Alias information              */
    {                                    /* ------------------------------ */
     fol_t    atom;                      /* atom of the alias (the key)    */
     int      stm;                       /* associated stream              */
    }AliasInf;

/*---------------------------------*/
/* Global Variables                */
/*---------------------------------*/

#ifdef STREAM_SUPP_FILE

       StmInf   stm_tbl[MAX_STREAM];
       int      stm_last_used=-1;

       char    *alias_tbl;

       fol_t  last_input_sora;
       fol_t  last_output_sora;

       int      stm_stdin;
       int      stm_stdout;
       int      stm_stderr;

       int      stm_input;
       int      stm_output;
       int      stm_error;

       fol_t    atom_user_input;
       fol_t    atom_user_output;
       fol_t    atom_user_error;

       fol_t      atom_read;
       fol_t      atom_write;
       fol_t      atom_append;

       fol_t      atom_stream_position;

       fol_t      atom_bof;
       fol_t      atom_current;
       fol_t      atom_eof;

#else

extern StmInf   stm_tbl[];
extern int      stm_last_used;

extern char    *alias_tbl;


extern fol_t  last_input_sora;
extern fol_t  last_output_sora;

extern int      stm_stdin;
extern int      stm_stdout;
extern int      stm_stderr;

extern int      stm_input;
extern int      stm_output;
extern int      stm_error;

extern fol_t      atom_user_input;
extern fol_t      atom_user_output;
extern fol_t      atom_user_error;

extern fol_t      atom_read;
extern fol_t      atom_write;
extern fol_t      atom_append;

extern fol_t      atom_stream_position;

extern fol_t      atom_bof;
extern fol_t      atom_current;
extern fol_t      atom_eof;

#endif

#define INT_TO_STM(_n_)  (stm_tbl+_n_)

/*---------------------------------*/
/* Function Prototypes             */
/*---------------------------------*/

void      Stream_Initializer    (void);

int       Add_Stream            (fol_t atom_file_name,long file,StmProp prop, 
                                 StmFct fct_getc,StmFct fct_putc,
                                 StmFct fct_flush,StmFct fct_close,
                                 StmFct fct_tell,StmFct fct_seek,
                                 StmFct fct_clearerr);


void      Remove_Stream         (int stm);

int       Find_Stream_By_Name   (fol_t atom_file_name);

int       Find_Stream_By_Alias  (fol_t atom_alias);

Bool      Add_Alias_To_Stream   (fol_t atom_alias,int stm);

void      Del_Aliases_Of_Stream (int stm);

int       Get_Stream_Or_Alias   (fol_t, fkey_t , int test_mask);

void      Check_Stream_Type     (int smt,int test_mask);

int       Stream_Getc           (StmInf *pstm);

void      Stream_Ungetc         (int c,StmInf *pstm);

int       Stream_Peekc          (StmInf *pstm);

char     *Stream_Gets           (char *str,int size,StmInf *pstm);

void      Stream_Putc           (int c,StmInf *pstm);

int       Stream_Puts           (char *str,StmInf *pstm);

int       Stream_Printf         (StmInf *pstm,char *format,...);

void      Stream_Flush          (StmInf *pstm);

int       Stream_Close          (StmInf *pstm);

int       Stream_End_Of_Stream  (StmInf *pstm);


void      Stream_Get_Position   (StmInf *pstm,int *offset,
                                 int *char_count,int *line_count,
                                 int *line_pos);

int       Stream_Set_Position   (StmInf *pstm,int whence,int offset,
                                 int char_count,int line_count,
                                 int line_pos);

int       Stream_Set_Position_LC(StmInf *pstm,int line_count,int line_pos);

#define PB_Init(pb)                                                         \
    {                                                                       \
     pb.ptr=pb.buff;                                                        \
     pb.nb_elems=0;                                                         \
    }

#define PB_Is_Empty(pb)      (pb.nb_elems==0)

#define PB_Push(pb,elem)                                                    \
    {                                                                       \
     *(pb.ptr)=(elem);                                                      \
     if (pb.ptr!=pb.buff+STREAM_PB_SIZE-1)                                  \
         pb.ptr++;                                                          \
      else                                                                  \
         pb.ptr=pb.buff;                                                    \
     if (pb.nb_elems<STREAM_PB_SIZE)                                        \
         pb.nb_elems++;                                                     \
    }

#define PB_Pop(pb,elem)                                                     \
    {                                                                       \
     if (pb.ptr!=pb.buff)                                                   \
         pb.ptr--;                                                          \
      else                                                                  \
         pb.ptr=pb.buff+STREAM_PB_SIZE-1;                                   \
     (elem)=*pb.ptr;                                                        \
     pb.nb_elems--;                                                         \
    }

#define PB_Top(pb,elem)                                                     \
    {                                                                       \
     if (pb.ptr!=pb.buff)                                                   \
         (elem)=pb.ptr[-1];                                                 \
      else                                                                  \
         (elem)=pb.buff[STREAM_PB_SIZE-1];                                  \
    }

#ifdef __cplusplus
}
#endif



