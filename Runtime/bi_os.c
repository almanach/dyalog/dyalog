/************************************************************
 * $Id$
 * Copyright (C) 1997, 2004, 2006, 2014 Eric de la Clergerie
 * ------------------------------------------------------------
 *
 *   BuiltIns OS -- OS related BuiltIns
 *
 * ------------------------------------------------------------
 * Description
 *    Standard OS interface built-ins predicates
 *    (strongly inspired from Daniel Diaz's code of wamcc)
 * ------------------------------------------------------------
 */


#include <string.h>
#include <stdio.h>
#include <time.h>
#include <unistd.h>

#include "hash.h"

#include "libdyalog.h"
#include "machine.h"
#include "builtins.h"



/**********************************************************************
 * Argument list
 **********************************************************************/

extern fol_t _dyalog_argv;


static Bool Os_Argv_1(Sproto);

Bool              /* DyALog Wrapper */
DYAM_Os_Argv_1(fol_t t)
{
    fkey_t k=R_TRANS_KEY;
    return Os_Argv_1(t,k);
}

static Bool	  /* DyALog Builtin */
Os_Argv_1( SP(t) )
{
  return Unify( t, Sk(t), _dyalog_argv, Key0 );
}

/**********************************************************************
 * Add_Path
 **********************************************************************/

extern void add_path(const char *);


static Bool Add_Load_Path_1(Sproto);

Bool              /* DyALog Wrapper */
DYAM_Add_Load_Path_1(fol_t path)
{
    fkey_t k=R_TRANS_KEY;
    return Add_Load_Path_1(path,k);
}

static Bool	  /* DyALog Builtin */
Add_Load_Path_1( SP(path) )
{
    Deref_And_Fail_Unless(path, FOLSMBP(path));
    add_path( SymbolName(path) );
    Succeed;
}

/**********************************************************************
 * Find_File_2
 **********************************************************************/

extern char * search_file(const char *);


static Bool Find_File_2(Sproto,Sproto);

Bool              /* DyALog Wrapper */
DYAM_Find_File_2(fol_t f1,fol_t f2)
{
    fkey_t k=R_TRANS_KEY;
    return Find_File_2(f1,k,f2,k);
}

static Bool	  /* DyALog Builtin */
Find_File_2( SP(f1), SP(f2) )
{
    char *f2_name;
    
    Deref_And_Fail_Unless(f1, FOLSMBP(f1));
    Deref_And_Fail_Unless(f2, FOLSMBP(f2) || FOLVARP(f2) );

    f2_name = search_file( SymbolName(f1) );

    if (f2_name == (char *) 0)
        Fail;
    
    return Get_Cst( Create_Allocate_Atom(f2_name), f2 );    
}

/**********************************************************************
 * Absolute_File_Name
 **********************************************************************/

static Bool Absolute_File_Name(Sproto,Sproto);

Bool              /* DyALog Wrapper */
DYAM_Absolute_File_Name(fol_t f1,fol_t f2)
{
    fkey_t k=R_TRANS_KEY;
    return Absolute_File_Name(f1,k,f2,k);
}

static Bool	  /* DyALog Builtin */
Absolute_File_Name( SP(f1), SP(f2) )
{
  char *f2_name;

  Deref_And_Fail_Unless(f1, FOLSMBP(f1));
  Deref_And_Fail_Unless(f2, FOLSMBP(f2) || FOLVARP(f2) );

  f2_name = M_Absolute_File_Name(SymbolName(f1));
  
  if (f2_name == (char *) 0)
    Fail;
  
  return Get_Cst( Create_Allocate_Atom(f2_name), f2 );
}

/*-------------------------------------------------------------------------*/
/* DECOMPOSE_FILE_NAME_4                                                   */
/*     (Adapted from Calypso [Diaz])                                       */
/*-------------------------------------------------------------------------*/

static Bool Decompose_File_Name_4(Sproto,Sproto,Sproto,Sproto);

Bool              /* DyALog Wrapper */
DYAM_Decompose_File_Name_4(fol_t path_word,fol_t dir_word,fol_t prefix_word,fol_t suffix_word)
{
    fkey_t k=R_TRANS_KEY;
    return Decompose_File_Name_4(path_word,k,dir_word,k,prefix_word,k,suffix_word,k);
}

static Bool	  /* DyALog Builtin */
Decompose_File_Name_4( SP(path_word), SP(dir_word),SP(prefix_word), SP(suffix_word))
{
    char    *path;
    char    *p;
    fol_t   atom;
    char     c;


    Deref_And_Fail_Unless(path_word,FOLSMBP(path_word));
    p = SymbolName(path_word);
    path= (char *) GC_MALLOC_ATOMIC( strlen(p)+1 );
    strcpy(path,p);
       
    Deref_And_Fail_Unless(dir_word, FOLSMBP(dir_word) || FOLVARP(dir_word));
    Deref_And_Fail_Unless(prefix_word, FOLSMBP(prefix_word) || FOLVARP(prefix_word));
    Deref_And_Fail_Unless(suffix_word, FOLSMBP(suffix_word) || FOLVARP(suffix_word));
    
    p=strrchr(path,'/');
    
#ifdef M_ix86_win32
    if (p==NULL)
        p=strrchr(path,'\\');
#endif

    if (p!=NULL) {
        p++;
        c=*p;
        *p='\0';
        atom=Create_Atom(path);
        *p=c;
        path=p;
    }  else
        atom= Create_Atom( "" );
    
    if (!Get_Cst(atom,dir_word))
        Fail;
     
    p=strrchr(path,'.');
    if (p!=NULL){
        *p='\0';
        atom=Create_Atom(path);
        *p='.';
        path=p;
    } else
        atom=Create_Atom(path);


    
    if (!Get_Cst(atom,prefix_word))
        Fail;
    
    if (p!=NULL)
        atom=Create_Atom(path);
    else
        atom= Create_Atom( "" );
    
    return Get_Cst(atom,suffix_word);
}

/*-------------------------------------------------------------------------*/
/* OS_ACCESS_2                                                             */
/*                                                                         */
/*-------------------------------------------------------------------------*/

static Bool Os_Access_2(Sproto,Sproto);

Bool              /* DyALog Wrapper */
DYAM_Os_Access_2(fol_t path_word,fol_t mode_word)
{
    fkey_t k=R_TRANS_KEY;
    return Os_Access_2(path_word,k,mode_word,k);
}

static Bool	  /* DyALog Builtin */
Os_Access_2( SP(path_word),  SP(mode_word) )
{
 char    *path;

 
 Deref_And_Fail_Unless(path_word,FOLSMBP(path_word));

 path = SymbolName( path_word );

 if ((path=M_Absolute_File_Name(path))==NULL)
   Fail;
 
 Deref_And_Fail_Unless(mode_word,FOLINTP(mode_word));

 return access(path,CFOLINT(mode_word))==0;
}
  
/*-------------------------------------------------------------------------*/
/* OS_CD_1                                                                 */
/*                                                                         */
/*-------------------------------------------------------------------------*/

static Bool Os_Cd_1(Sproto);

Bool              /* DyALog Wrapper */
DYAM_Os_Cd_1(fol_t path)
{
    fkey_t k=R_TRANS_KEY;
    return Os_Cd_1(path,k);
}

static Bool	  /* DyALog Builtin */
Os_Cd_1( SP(path) )
{
 Deref_And_Fail_Unless(path, FOLSMBP(path));
 return M_Set_Working_Dir(SymbolName(path));
}

/*-------------------------------------------------------------------------*/
/* OS_GETWD_1                                                              */
/*                                                                         */
/*-------------------------------------------------------------------------*/

static Bool Os_Getwd_1(Sproto);

Bool              /* DyALog Wrapper */
DYAM_Os_Getwd_1(fol_t path)
{
    fkey_t k=R_TRANS_KEY;
    return Os_Getwd_1(path,k);
}

static Bool	  /* DyALog Builtin */
Os_Getwd_1( SP(path) )
{
 char    *c_path=M_Get_Working_Dir();
 return Get_Cst(Create_Allocate_Atom(c_path),path);
}

/*-------------------------------------------------------------------------*/
/* OS_GETENV_2                                                             */
/*                                                                         */
/*-------------------------------------------------------------------------*/

static Bool Os_Getenv_2(Sproto,Sproto);

Bool              /* DyALog Wrapper */
DYAM_Os_Getenv_2(fol_t var,fol_t value)
{
    fkey_t k=R_TRANS_KEY;
    return Os_Getenv_2(var,k,value,k);
}

static Bool	  /* DyALog Builtin */
Os_Getenv_2( SP(var), SP(value) )
{
 char    *v;

 Deref_And_Fail_Unless(var,FOLSMBP(var));
 
 v=(char *) getenv(SymbolName(var));

 return v && Get_Cst(Create_Allocate_Atom(v),value);
}

/*-------------------------------------------------------------------------*/
/* OS_MKTEMP_2                                                             */
/*                                                                         */
/*-------------------------------------------------------------------------*/

static Bool Os_Mktemp_2(Sproto,Sproto);

Bool              /* DyALog Wrapper */
DYAM_Os_Mktemp_2(fol_t template,fol_t file_name)
{
    fkey_t k=R_TRANS_KEY;
    return Os_Mktemp_2(template,k,file_name,k);
}

static Bool	  /* DyALog Builtin */
Os_Mktemp_2( SP(template), SP(file_name))
{
 char    *c_template;
 char    *c_file_name;

 Deref_And_Fail_Unless(template, FOLSMBP(template));

 c_template = SymbolName(template);

 if ((c_template=M_Absolute_File_Name(c_template))==NULL)
   Fail;

 c_file_name=(char *) mktemp(c_template);

 return c_file_name && Get_Cst(Create_Allocate_Atom(c_file_name),file_name);

}

/*-------------------------------------------------------------------------*/
/* OS_SYSTEM_2                                                             */
/*                                                                         */
/*-------------------------------------------------------------------------*/

static Bool Os_System_2(Sproto,Sproto);

Bool              /* DyALog Wrapper */
DYAM_Os_System_2(fol_t cmd_word,fol_t status_word)
{
    fkey_t k=R_TRANS_KEY;
    return Os_System_2(cmd_word,k,status_word,k);
}

static Bool	  /* DyALog Builtin */
Os_System_2( SP(cmd_word), SP(status_word) )
{
 int      status;

 Deref_And_Fail_Unless(cmd_word, FOLSMBP(cmd_word));
 status=system(SymbolName(cmd_word));

 return Get_Integer(DFOLINT(status),status_word);
}

/*-------------------------------------------------------------------------*/
/* OS_SHELL_2                                                              */
/*                                                                         */
/*-------------------------------------------------------------------------*/

static Bool Os_Shell_2(Sproto,Sproto);

Bool              /* DyALog Wrapper */
DYAM_Os_Shell_2(fol_t cmd_word,fol_t status_word)
{
    fkey_t k=R_TRANS_KEY;
    return Os_Shell_2(cmd_word,k,status_word,k);
}

static Bool	  /* DyALog Builtin */
Os_Shell_2(SP(cmd_word), SP(status_word) )
{
 int      status;

 Deref_And_Fail_Unless(cmd_word, FOLSMBP(cmd_word));
 status=M_Shell(SymbolName(cmd_word));

 return Get_Integer(DFOLINT(status),status_word);
}

/*-------------------------------------------------------------------------*/
/* OS_POPEN_3                                                              */
/*                                                                         */
/*-------------------------------------------------------------------------*/

static Bool Os_Popen_3(Sproto,Sproto,Sproto);

Bool              /* DyALog Wrapper */
DYAM_Os_Popen_3(fol_t cmd_word,fol_t mode_word,fol_t stm_word)
{
    fkey_t k=R_TRANS_KEY;
    return Os_Popen_3(cmd_word,k,mode_word,k,stm_word,k);
}

static Bool	  /* DyALog Builtin */
Os_Popen_3( SP(cmd_word), SP(mode_word), SP(stm_word) )
{
 char    *cmd;
 StmProp  prop;
 int      stm;
 FILE    *f;
 char     open_str[10];

 Deref_And_Fail_Unless(cmd_word, FOLSMBP(cmd_word));
 cmd= SymbolName(cmd_word);

 Deref_And_Fail_Unless(mode_word, FOLSMBP(mode_word));
 if (mode_word==atom_read)
    {
     prop.mode  =STREAM_MODE_READ;
     prop.input =TRUE;
     prop.output=FALSE;
     strcpy(open_str,"r");
    }
  else
     if (mode_word==atom_write)
        {
         prop.mode  =STREAM_MODE_WRITE;
         prop.input =FALSE;
         prop.output=TRUE;
         strcpy(open_str,"w");
        }
      else
         Fail;

 prop.text      =TRUE;
 prop.reposition=FALSE;
 prop.eof_action=STREAM_EOF_ACTION_RESET;
 prop.tty       =FALSE;

 f=popen(cmd,open_str);

 if (f==NULL)
     return FALSE;

 stm=Add_Stream(Create_Allocate_Atom(""),(long) f,prop,
                NULL,NULL,NULL,pclose,NULL,NULL,NULL);

 Get_Integer(DFOLINT(stm),stm_word);
 Succeed;
}

/*-------------------------------------------------------------------------*/
/* OS_UNLINK_1                                                             */
/*                                                                         */
/*-------------------------------------------------------------------------*/

static Bool Os_Unlink_1(Sproto);

Bool              /* DyALog Wrapper */
DYAM_Os_Unlink_1(fol_t file_name_word)
{
    fkey_t k=R_TRANS_KEY;
    return Os_Unlink_1(file_name_word,k);
}

static Bool	  /* DyALog Builtin */
Os_Unlink_1( SP(file_name_word) )
{
 char    *file_name;

 Deref_And_Fail_Unless(file_name_word,
		       FOLSMBP(file_name_word) &&
		       (file_name = M_Absolute_File_Name( SymbolName(file_name_word))) != NULL );

 return unlink(file_name)==0;
}

/*-------------------------------------------------------------------------*/
/* DATE_TIME_TO_PROLOG                                                     */
/*                                                                         */
/*-------------------------------------------------------------------------*/

static
Bool Date_Time_To_Prolog(const time_t *t,SP(year),SP(month),SP(day),SP(hour),SP(minute),SP(second))
{
    struct tm *tm=localtime(t);
    
    Deref_And_Fail_Unless(year,FOLVARP(year) || FOLINTP(year));
    Deref_And_Fail_Unless(month,FOLVARP(month) || FOLINTP(month));
    Deref_And_Fail_Unless(day,FOLVARP(day) || FOLINTP(day));
    Deref_And_Fail_Unless(hour,FOLVARP(hour) || FOLINTP(hour));
    Deref_And_Fail_Unless(minute,FOLVARP(minute) || FOLINTP(minute));
    Deref_And_Fail_Unless(second,FOLVARP(second) || FOLINTP(second));

    return
        Get_Integer(DFOLINT(tm->tm_year+1900),year) && 
        Get_Integer(DFOLINT(tm->tm_mon+1),month)    &&
        Get_Integer(DFOLINT(tm->tm_mday),day)       &&
        Get_Integer(DFOLINT(tm->tm_hour),hour)      &&
        Get_Integer(DFOLINT(tm->tm_min),minute)     &&
        Get_Integer(DFOLINT(tm->tm_sec),second);
}

/*-------------------------------------------------------------------------*/
/* DATE_TIME_1                                                             */
/*                                                                         */
/*-------------------------------------------------------------------------*/

static Bool Date_Time_6(Sproto,Sproto,Sproto,Sproto,Sproto,Sproto);

Bool              /* DyALog Wrapper */
DYAM_Date_Time_6(fol_t year,fol_t month,fol_t day,fol_t hour,fol_t minute,fol_t second)
{
    fkey_t k=R_TRANS_KEY;
    return Date_Time_6(year,k,month,k,day,k,hour,k,minute,k,second,k);
}

static Bool	  /* DyALog Builtin */
Date_Time_6( SP(year),SP(month),SP(day),SP(hour),SP(minute),SP(second) )
{
 time_t t;

 t=time(NULL);

 return Date_Time_To_Prolog(&t,S(year),S(month),S(day),S(hour),S(minute),S(second));
}

/*-------------------------------------------------------------------------*/
/* Exit_1                                                                  */
/*                                                                         */
/*-------------------------------------------------------------------------*/

static Bool Exit_1(Sproto);

Bool             /* DyALog Wrapper */
DYAM_Exit_1( fol_t status )
{
    fkey_t k=R_TRANS_KEY;
    return Exit_1(status,k);
}

static Bool
Exit_1( SP(status) )
{
    Deref_And_Fail_Unless(status,INTEGERP(status));
    FREE_ALLOCATION();
    exit( CFOLINT(status) );
}

    
