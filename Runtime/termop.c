/************************************************************
 * $Id$
 * Copyright (C) 1996, 2003, 2004, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2014, 2016 Eric de la Clergerie
 * ------------------------------------------------------------
 *
 *   TermOp -- Standard Term Operations in presence of structure sharing
 *
 * ------------------------------------------------------------
 * Description
 *     Implementation of the main operations on terms taking
 *     into account structure-sharing
 *
 *      * sfol_unify   r k  t l
 *      * sfol_occur   X k  t l
 *      * sfol_subsume r k  t l
 *      * sfol_equal   r k  t l
 *      * sfol_weight  r k
 *      * sfol_copy    r k
 * ------------------------------------------------------------
 */

#include <string.h>
#include "libdyalog.h"
#include "builtins.h"

#ifdef STAT

STAT_DEF(unif,bind)
STAT_DEF(subs,bind)
STAT_DEF(termop,eq_p,eq,skel_p,skel,functor_p,functor)

static void stat_display() __attribute__ ((destructor));

void stat_display()
{
    printf(
	"----------------------------------------------------------------------\n"
	"STAT <low level unification/subsumption info>\n"
	"\tunification bindings     %6d\n"
	"\tsubsumption bindings     %6d\n"
	"\teq      checks           %6d %6d\n"
	"\tskel    checks           %6d %6d\n"
	"\tfunctor checks           %6d %6d\n"
	"----------------------------------------------------------------------\n",
	
	STAT_USE(unif,bind),
	STAT_USE(subs,bind),
	STAT_USE(termop,eq_p),      STAT_USE(termop,eq),
	STAT_USE(termop,skel_p),    STAT_USE(termop,skel),
	STAT_USE(termop,functor_p), STAT_USE(termop,functor)
	);
}
	

#endif /* STAT */

/**********************************************************************
 *      MACROs
 **********************************************************************/

#define SAFE( _deref, t, k ) \
    if (FOL_DEREFP(t)) _deref(t,k)

#define SAFE_ALT( _deref, t, k ) \
    if (FOL_DEREFP(t)) _deref(t,k)

inline
static Bool
sfol_eq( SP(t), SP(r) )
{
    Bool v = (SFOL_EQ(t,Sk(t),r,Sk(r)));
    STAT_UPDATE(termop,eq_p);
#ifdef STAT
    if (v) STAT_UPDATE(termop,eq);
#endif
    return v;
}

inline
static Bool
folcmp_samefunp(fol_t r, fol_t t)
{
    Bool v = FOLCMP_SAMEFUNP(r,t);
    STAT_UPDATE(termop,functor_p);
#ifdef STAT
    if (v) STAT_UPDATE(termop,functor);
#endif
    return v;
}

inline
static Bool
folcmp_skeleq(fol_t r, fol_t t){
    Bool v = (r==t);
    STAT_UPDATE(termop,skel_p);
#ifdef STAT
    if (v) STAT_UPDATE(termop,skel);
#endif
    return v;
}

#define RECURSE( _op, r, k, t, l)                                               \
  if ( FOLCMP_GROUNDP( r ) && FOLCMP_GROUNDP( t ) ){             		\
         return ( folcmp_skeleq(r,t) );						\
    } else if ( folcmp_skeleq(r,t) ) {						\
      /* r and t are identical but with different keys				\
	 we recurse on their variable tuple */					\
      obj_t tuple = FOLINFO_TUPLE( r );						\
										\
      for (;									\
	   PAIRP( CDR(tuple) );							\
	   tuple = CDR(tuple))							\
        if (! _op((fol_t) CAR( tuple),k,(fol_t)CAR(tuple),l)) return FALSE;	\
										\
      return _op((fol_t) CAR( tuple),k,(fol_t)CAR(tuple),l);			\
										\
    } else if (folcmp_samefunp(r,t)){						\
      /* r and t share the same functor */					\
      fol_t  *stop;								\
      unsigned long  arity = FOLCMP_ARITY( r )-1 ;				\
      long  length = compatible(r,t);						\
      fol_t  *arg1;								\
      fol_t  *arg2;								\
										\
      if (length==0) return FALSE;						\
										\
      for(;									\
	      length;								\
	      length--,								\
	      r = *arg1, t=*arg2)						\
	for (arg1 = &FOLCMP_REF(r,1),						\
	     arg2 = &FOLCMP_REF(t,1),						\
	     stop = arg1 + arity;						\
	     arg1 < stop;							\
	     arg1++, arg2++)							\
	  if (! _op( *arg1,k,*arg2,l)) return FALSE;				\
										\
      return _op( *arg1 , k, *arg2 , l );					\
										\
    } else									\
      return FALSE;


#define SAFE_FOLCMP_DEREFP( o ) (FOLCMPP(o) && FOLCMP_DEREFP(o))

#define UNFOLD( _op, r, k, t, l, _rvar_case, _tvar_case, _df, _dp)		\
{										\
  if ( sfol_eq( r, k, t, l ) )                  /* r and t are identical */	\
    return TRUE;								\
										\
  if ( FOLVARP( r ) )								\
    return _rvar_case;								\
										\
  if ( FOLVARP( t ) )								\
    return _tvar_case;								\
										\
  if ( _df && ( SAFE_FOLCMP_DEREFP(r) || SAFE_FOLCMP_DEREFP(t) ))		\
      return _dp;								\
										\
  if ( FOLCMPP( r ) && FOLCMPP( t ) ){     /* r and t are compound terms */	\
    RECURSE( _op, r, k, t, l);}							\
										\
  /* r and t are constants (with different keys) */				\
  return (Bool) ( r == t );							\
}										\

/**********************************************************************
 *      compatible 
 **********************************************************************/
static long
compatible(const fol_t r, const fol_t t)
{
  const long rl = FOLINFO_LENGTH( r );
  const long tl = FOLINFO_LENGTH( t );
  if ( rl < 0)
    if ( tl < 0 )
      return - (( rl < tl ) ?  tl : rl);
    else
      return ( - rl <= tl ) ? -rl : 0;
  else if ( tl < 0 )
    return (-tl <= rl)  ? -tl : 0;
  else
    return (rl == tl) ? rl : 0;

}

/**********************************************************************
 *      sfol_occur ...
 **********************************************************************/
static Bool
sfol_occur_aux( fol_t X, fkey_t k, fol_t t, fkey_t l)
{
  SAFE_ALT(CLOSURE_UL_DEREF,t,l);
  
  if  (SFOL_EQ( X, k, t, l))
    return TRUE;
  
  if (FOLCMPP( t )) {
    obj_t tuple = FOLINFO_TUPLE( t );

    if (FOLCMP_FUNCTOR(t) == FOLLOOP) {
        if (SFOL_EQ(X,k,FOLCMP_REF(t,1),l))
            return TRUE;
        else {
            TRAIL_UBIND(FOLCMP_REF(t,1),l,FOLNIL,Key0);
            return sfol_occur_aux(X,k,FOLCMP_REF(t,2),l);
        }
    }
                
    for (;
	 PAIRP(tuple);
	 tuple = CDR( tuple ))
      if (sfol_occur_aux(X,k,(fol_t)CAR(tuple),l)) return TRUE;
    
  }

  return FALSE;
  
}

Bool
sfol_occur( fol_t X, fkey_t k, fol_t t, fkey_t l)
{
    TrailWord *top= C_TRAIL_TOP;
    Bool r = sfol_occur_aux(X,k,t,l);
    if (C_TRAIL_TOP > top)
        untrail_alt(top);
    return r;
}

/**********************************************************************
 *      sfol_ubind
 **********************************************************************/

Bool            /* bind X_k to a loop term with a fresh variable */
sfol_loop_bind( SP(X), SP(t) )
{
    static fol_t loop= (fol_t) 0;
    fkey_t k = (fkey_t) LSTACK_PUSH_VOID;
    fol_t B= FOLVAR_FROM_INDEX(1);
    V_LEVEL_DISPLAY(V_TERM,"Loop binding %&s->%&s\n",X,Sk(X),t,Sk(t));
    if (!loop) {
        fol_t A= FOLVAR_FROM_INDEX(2);
        FOLCMP_WRITE_START(FOLLOOP,3);
        FOLCMP_WRITE(A);
        FOLCMP_WRITE(B);
        FOLCMP_WRITE(DFOLINT(1)); /* recursive loop */
        loop=FOLCMP_WRITE_STOP;
    }
    TRAIL_UBIND(X,Sk(X),loop,k);
    TRAIL_UBIND(B,k,t,Sk(t));
    Succeed;
}

inline
static Bool
sfol_ubind( SP(X), SP(t) )
{
    if (FOLFSETP(t) && sfol_eq(X,Sk(X),FOLCMP_REF(t,1),Sk(t))) {
            /* that case should not arise
               but there is a bug somewhere to be tracked !
             */
        V_LEVEL_DISPLAY(V_TERM,"*** warning: fset occur check at %&s %&s\n",X,Sk(X),t,Sk(t));
        Sk(t) = LSTACK_PUSH_VOID;
        TRAIL_UBIND(X,Sk(X),t,Sk(t));
        Succeed;
    } else if (R_OCCUR_CHECK && sfol_occur(X,Sk(X),t,Sk(t))) {
        V_LEVEL_DISPLAY(V_TERM,"occcur check at %&s %&s\n",X,Sk(X),t,Sk(t));
        return sfol_loop_bind(X,Sk(X),t,Sk(t));
    } else if (FOLVARP(t) && Sk(t) > Sk(X)) {
        STAT_UPDATE(unif,bind);
        V_LEVEL_DISPLAY(V_TERM,"Bind reverse %&p<-%&p\n",X,Sk(X),t,Sk(t));
        TRAIL_UBIND(t,Sk(t),X,Sk(X));
        Succeed;
    } else {
        STAT_UPDATE(unif,bind);
        V_LEVEL_DISPLAY(V_TERM,"Bind %&p->%&p\n",X,Sk(X),t,Sk(t));
	TRAIL_UBIND( X, Sk(X), t, Sk(t));
        Succeed;
    }
}

void sfol_unif_bind(SP(X),SP(t))
{                               /* assume that t is not a variable */
    if (R_OCCUR_CHECK && sfol_occur(X,Sk(X),t,Sk(t)))
        sfol_loop_bind(X,Sk(X),t,Sk(t));
    else
        TRAIL_UBIND( X, Sk(X), t, Sk(t));
}

/**********************************************************************
 * Tfs_Try_Unif
 *
 *    Try to unify two terms who at least one is TFS 
 *    (which implies that this term is compound with 1st arg used
 *     for deref)
 *
 *    Mainly, this function calls the true Tfs_Unif function except
 *    for special case where the two terms are built on same functor
 **********************************************************************/

static Bool _sfol_unify( fol_t r, fkey_t k, fol_t t, fkey_t l);

/* the following variable are used for the fresh variables that may be
 * introduced during an unification
 * their scope is a whole unification
 */
static unsigned long unif_newvar;
static fkey_t unif_newvar_k;

static
fkey_t unif_use_fresh_layer ()
{
    if (!unif_newvar_k)
        unif_newvar_k = LSTACK_PUSH_VOID;
    return unif_newvar_k;
}


Bool
Tfs_Subtype_Unif( SP(Left), SP(Right) )
{
    int arity = FOLCMP_ARITY(Left);
    V_LEVEL_DISPLAY(V_TERM,"Subtype Unif %&s vs %&s %d\n",Left,Sk(Left),Right,Sk(Right),arity);
    sfol_unif_bind(FOLCMP_REF(Left,1),Sk(Left),Right,Sk(Right));
    if ( arity > 1 ) {
      /* => Right is FOLCMP */
      fol_t *argL = &FOLCMP_REF(Left,2);
      int start = FOLCMP_DEREFP(Right) ? 2 : 1;
      fol_t *argR = &FOLCMP_REF(Right,start);
      for( arity-- ; arity > 0 ; argL++, argR++, arity-- )
	Unify_Or_Fail(*argL,Sk(Left),*argR,Sk(Right));
    }
    Succeed;
}

int FSet_Try_Reduce( fol_t );

/*
static int
FSet_Simple_Try_Reduce(unsigned n)
{
    int i=0;
    for(; ((unsigned long) 1 << i) < n; i++);
    if ( ((unsigned long) 1 << i) == n) {
        return (i+1);
    };
    return 0;
}
*/

/* Left and Right are assumed to be dereferenced
   Left is assumed to be a compound term built on '$FSET$'/3
 */
static
Bool FSet_Unif( SP(Left), SP(Right) )
{
    V_LEVEL_DISPLAY(V_TERM,"Finite Set Unif %&s vs %&s\n",Left,Sk(Left),Right,Sk(Right));

//    dyalog_printf("Finite Set Unif %&s vs %&s\n",Left,Sk(Left),Right,Sk(Right));

        /* Left is a FOLFSET */
    
    if (FOLFSETP(Right)) {
            /* both Left and Right are FOLFSET*/
        if ( FOLCMP_ARITY(Left) == FOLCMP_ARITY(Right)
             && FOLCMP_REF(Left,2) == FOLCMP_REF(Right,2)
             ) {
            unsigned int arity = FOLCMP_ARITY(Left);
            fol_t table = FOLCMP_REF(Left,2);
            fol_t *left_arg = &FOLCMP_REF(Left,3);
            fol_t *left_stop = left_arg + arity - 2;
            fol_t *right_arg = &FOLCMP_REF(Right,3);
            unsigned int left_eq = 1;
            unsigned int right_eq = 1;
            unsigned long filled=0;
            unsigned int k;
            Sdecl(res);
            
            res = (fol_t) FOLCMP_WRITE_START( FOLCMP_FUNCTOR(Left), arity );
            FOLCMP_WRITE( FOLVAR_FROM_INDEX(unif_newvar++) );
            FOLCMP_WRITE( table );
            
            for(; left_arg < left_stop; left_arg++, right_arg++ ) {
                unsigned long n=0;
                fol_t argL=*left_arg;
                fol_t argR=*right_arg;
                Fail_Unless(argL, FOLINTP(argL));
                Fail_Unless(argR, FOLINTP(argR));
                n = (UCFOLINT(argL) & UCFOLINT(argR));
                filled |= n;
                left_eq &= (n == UCFOLINT(argL));
                right_eq &= (n == UCFOLINT(argR));
                FOLCMP_WRITE( UDFOLINT(n) );
            }
            
            if (!filled) {
                FOLCMP_WRITE_ABORT;
                unif_newvar--;  /* no variable wasting ! */
                Fail;
            }
            
            if (left_eq) { /* Bind Right to Left */
                FOLCMP_WRITE_ABORT;
                unif_newvar--;
                TRAIL_UBIND(FOLCMP_REF(Right,1),Sk(Right), Left,Sk(Left));
                Succeed;
            } else if (right_eq) { /* Bind Left to Right */
                FOLCMP_WRITE_ABORT;
                unif_newvar--;
                TRAIL_UBIND(FOLCMP_REF(Left,1),Sk(Left), Right,Sk(Right));
                Succeed;
            }
            
                /* Bind both Left and Right to a new value */
            
            if ((k=FSet_Try_Reduce(res))) { /* value is a constant */
                FOLCMP_WRITE_ABORT;
                unif_newvar--;
                res = FOLCMP_REF(table,k);
                Sk(res)=Key0;
            } else {                /* value is a new FOLFSET */
                res = FOLCMP_WRITE_STOP;
                Sk(res)= unif_use_fresh_layer();
            }
            
            TRAIL_UBIND(FOLCMP_REF(Left,1),Sk(Left), res,Sk(res));
            TRAIL_UBIND(FOLCMP_REF(Right,1),Sk(Right), res,Sk(res));
            Succeed;
        }
    } else if (FOLSMBP(Right)) {
        fsetelt_t fsets = FOLSMB_FSETELT(Right);
        fol_t table = FOLCMP_REF(Left,2);
//        dyalog_printf("right smb table=%&f %x\n",table,table);
        for(; fsets ; fsets = fsets->next) {
//            dyalog_printf("current fset table %&f %x\n",fsets->fset_table,fsets->fset_table);
            if (fsets->fset_table == table ) {
                unsigned long value = UCFOLINT(FOLCMP_REF(Left,2+fsets->block));
//                dyalog_printf("found table in fsets\n");
                if (value & fsets->mask) {
                    TRAIL_UBIND(FOLCMP_REF(Left,1),Sk(Left),Right,Key0);
//                    dyalog_printf("success\n");
                    Succeed;
                } else {
                    Fail;
                }
            }
        }
    } else if (FOL_CSTP(Right)) {
        fol_t table = FOLCMP_REF(Left,2);
        fol_t *arg = &FOLCMP_REF(table,1);
        fol_t *tmp_arg = arg;
        fol_t *stop = arg + FOLCMP_ARITY(table);
        fol_t *value_arg = &FOLCMP_REF(Left,3);
        fol_t *value_stop = &FOLCMP_REF(Left,1)+FOLCMP_ARITY(Left);
        for(; value_arg < value_stop; value_arg++, arg=tmp_arg+FSET_BIT_PER_WORD) {
            unsigned long value = UCFOLINT(*value_arg);
            tmp_arg = arg;
            for( ; value && arg < stop; arg++, value >>= 1 ) {
                if ( Right == *arg ){
                    if (value & 1) {
                        TRAIL_UBIND(FOLCMP_REF(Left,1),Sk(Left),Right,Key0);
                        Succeed;
                    } else {
                        Fail;
                    }
                }
            }
        }
    }
    Fail;
}

inline
static Bool
Loop_Unif( SP(l), SP(r) )
{                               /* l is a loop-term*/
    V_LEVEL_DISPLAY(V_TERM,"Loop Unif %&s vs %&s %d\n",l,Sk(l),r,Sk(r));
    if (FOLLOOPP(r)) 
        TRAIL_UBIND(FOLCMP_REF(l,1),Sk(l),r,Sk(r));
    else 
        sfol_unif_bind(FOLCMP_REF(l,1),Sk(l),r,Sk(r));
    return _sfol_unify(FOLCMP_REF(l,2),Sk(l),r,Sk(r));
}

inline
static Bool
Range_Unif( SP(l), SP(r) )
{                               /* l is a range-term */
//    V_LEVEL_DISPLAY(V_TERM,"Range Unif %&s vs %&s %d\n",l,Sk(l),r,Sk(r));
    if (FOLRANGEP(r)) {
        long l_min = CFOLINT(FOLCMP_REF(l,2));
        long r_min = CFOLINT(FOLCMP_REF(r,2));
        long l_max = CFOLINT(FOLCMP_REF(l,3));
        long r_max = CFOLINT(FOLCMP_REF(r,3));
        long res_min = (l_min < r_min) ? r_min : l_min;
        long res_max = (l_max < r_max) ? l_max : r_max;
//        dyalog_printf("--> res min=%d max=%d\n",res_min,res_max);
        if (res_min==res_max) {
            fol_t res = DFOLINT(res_min);
            TRAIL_UBIND(FOLCMP_REF(r,1),Sk(r),res,Key0);
            TRAIL_UBIND(FOLCMP_REF(l,1),Sk(l),res,Key0);
            Succeed;
        } else if (l_min == res_min && l_max == res_max) {
            TRAIL_UBIND(FOLCMP_REF(r,1),Sk(r),l,Sk(l));
            Succeed;
        } else if (r_min == res_min && r_max == res_max ) {
            TRAIL_UBIND(FOLCMP_REF(l,1),Sk(l),r,Sk(r));
            Succeed;
        } else if (res_max < res_min) {
            Fail;
        } else {
            Sdecl(res);
            FOLCMP_WRITE_START( FOLRANGE, 3 );
            FOLCMP_WRITE( FOLVAR_FROM_INDEX(unif_newvar++) );
            FOLCMP_WRITE( DFOLINT(res_min) );
            FOLCMP_WRITE( DFOLINT(res_max) );
            res = FOLCMP_WRITE_STOP;
            Sk(res) = unif_use_fresh_layer();
            TRAIL_UBIND(FOLCMP_REF(l,1),Sk(l),res,Sk(res));
            TRAIL_UBIND(FOLCMP_REF(r,1),Sk(r),res,Sk(res));
            Succeed;
        }
    } else if (FOLINTP(r)) {
        long r_v = CFOLINT(r);
        long l_min = CFOLINT(FOLCMP_REF(l,2));
        long l_max = CFOLINT(FOLCMP_REF(l,3));
//        dyalog_printf("Range unif min=%d max=%d r=%d\n",l_min,l_max,r_v);
        if (l_min <= r_v && r_v <= l_max) {
            TRAIL_UBIND(FOLCMP_REF(l,1),Sk(l),r,Key0);
//            dyalog_printf("=>yes\n");
            Succeed;
        }
    } 
    Fail;
}

inline
static Bool
Tfs_Try_Unif( SP(r), SP(t) )
{
    V_LEVEL_DISPLAY(V_TERM,"Tfs try Unif %&s vs %&s\n",r,Sk(r),t,Sk(t));
    if (FOLFSETP(r)) {
        return FSet_Unif(r,Sk(r),t,Sk(t));
    } else if (FOLFSETP(t)) {
        return FSet_Unif(t,Sk(t),r,Sk(r));
    }  else if (FOLLOOPP(r)) {
        return Loop_Unif(r,Sk(r),t,Sk(t));
    } else if (FOLLOOPP(t)) {
        return Loop_Unif(t,Sk(t),r,Sk(r));
    } else if (FOLRANGEP(r)) {
        return Range_Unif(r,Sk(r),t,Sk(t));
    } else if (FOLRANGEP(t)) {
        return Range_Unif(t,Sk(t),r,Sk(r));
    } else if (FOLCMPP(r) && FOLCMPP(t) && folcmp_samefunp(r,t)){
            /* => r_k and t_l have same functor but are not eq */
        return Tfs_Subtype_Unif(r,Sk(r),t,Sk(t));
    }  else
        return Tfs_Unif( r, Sk(r), t, Sk(t) );
}

/**********************************************************************
 *      sfol_unify ...
 **********************************************************************/
static
Bool
_sfol_unify( fol_t r, fkey_t k, fol_t t, fkey_t l)
{
    if (k==l && r==t)
        Succeed;
    
  SAFE_ALT(CLOSURE_UL_DEREF,r,k);
  SAFE_ALT(CLOSURE_UL_DEREF,t,l);

//  dyalog_printf("sfol unif %&s vs %&s : %&f vs %&f\n",r,k,t,l,r,t);
  
  UNFOLD( _sfol_unify, r, k, t, l,
	  sfol_ubind( r,k,t,l ),
	  sfol_ubind( t,l,r,k ),
	  1, Tfs_Try_Unif(r,k,t,l)
	  );
}

Bool
sfol_unify( fol_t r, fkey_t k, fol_t t, fkey_t l)
{
    unif_newvar = 0;
    unif_newvar_k = 0;
    return _sfol_unify(r,k,t,l);
}


Bool              /* DyALog Wrapper */
DYAM_sfol_unify(fol_t r,fol_t t)
{
    fkey_t k=R_TRANS_KEY;
    return sfol_unify(r,k,t,k);
}

/**********************************************************************
 *      sfol_sbind
 **********************************************************************/
inline
static Bool
sfol_sbind( fol_t X, fkey_t k, fol_t t, fkey_t l )
{
        /*
  return ONCE_S_DEREF( X, k )
    ? sfol_identical( X,k,t,l )
    : (  ({STAT_UPDATE(subs,bind);}), TRAIL_SBIND( X, k, t, l), TRUE );
        */
    STAT_UPDATE(subs,bind);
    V_LEVEL_DISPLAY(V_TERM,"Subs binds %&s->%&s\n",X,k,t,l);
    TRAIL_SBIND( X, k, t, l);
    Succeed;
}

/**********************************************************************
 * Tfs_Try_Subs
 *
 *    Try to subs two terms whose at least one of them is TFS
 *    a weak binding, but to the binder of this weak binding
 **********************************************************************/

Bool
Tfs_Simple_Subsume( SP(Left), SP(Right) )
{
    int arity = FOLCMP_ARITY(Left);
    // dyalog_printf("Subtype Subs %&s vs %&s\n",Left,Sk(Left),Right,Sk(Right));
    TRAIL_SBIND(FOLCMP_REF(Left,1),Sk(Left), Right,Sk(Right));
    if ( arity > 1) {
      // => Right is FOLCMP
      fol_t *argL = &FOLCMP_REF(Left,2);
      int start = FOLCMP_DEREFP(Right) ? 2 : 1;
      fol_t *argR = &FOLCMP_REF(Right,start);
      for( arity-- ; arity > 0 ; argL++, argR++, arity-- )
	Subsume_Or_Fail(*argL,Sk(Left),*argR,Sk(Right));
    }
    Succeed;
}


/* Left and Right are assumed to be dereferenced
   Left is assumed to be a compound term built on '$FSET$'/3
 */
static Bool
FSet_Subs( SP(Left), SP(Right) )
{
    V_LEVEL_DISPLAY(V_TERM,"Finite Set Subs %&s vs %&s\n",Left,Sk(Left),Right,Sk(Right));

    if (FOLFSETP(Right)) {
        if ( FOLCMP_ARITY(Left) == FOLCMP_ARITY(Right)
             && FOLCMP_REF(Left,2) == FOLCMP_REF(Right,2)) {
            unsigned int arity = FOLCMP_ARITY(Left);
            fol_t *left_arg = &FOLCMP_REF(Left,3);
            fol_t *left_stop = left_arg + arity - 2;
            fol_t *right_arg = &FOLCMP_REF(Right,3);

            for(; left_arg < left_stop; left_arg++, right_arg++ ) {
                fol_t argL=*left_arg;
                fol_t argR=*right_arg;
                Fail_Unless(argL, FOLINTP(argL));
                Fail_Unless(argR, FOLINTP(argR));
                if ((UCFOLINT(argL) & UCFOLINT(argR)) != UCFOLINT(argR))
                    Fail;
            }
            TRAIL_SBIND(FOLCMP_REF(Left,1),Sk(Left),Right,Sk(Right));
            Succeed;
        }
    } else if (FOLSMBP(Right)) {
        fsetelt_t fsets = FOLSMB_FSETELT(Right);
        fol_t table = FOLCMP_REF(Left,2);
        for(; fsets ; fsets = fsets->next) {
            if (fsets->fset_table == table ) {
                unsigned long value = UCFOLINT(FOLCMP_REF(Left,2+fsets->block));
                if (value & fsets->mask) {
                    TRAIL_SBIND(FOLCMP_REF(Left,1),Sk(Left),Right,Key0);
                    Succeed;
                } else {
                    Fail;
                }
            }
        }
    } else if (FOL_CSTP(Right)) {
        fol_t table = FOLCMP_REF(Left,2);
        fol_t *arg = &FOLCMP_REF(table,1);
        fol_t *tmp_arg = arg;
        fol_t *stop = arg + FOLCMP_ARITY(table);
        fol_t *value_arg = &FOLCMP_REF(Left,3);
        fol_t *value_stop = &FOLCMP_REF(Left,1)+FOLCMP_ARITY(Left);
        for(; value_arg < value_stop; value_arg++, arg=tmp_arg+FSET_BIT_PER_WORD) {
            unsigned long value = UCFOLINT(*value_arg);
            tmp_arg = arg;
            for( ; value && arg < stop; arg++, value >>= 1 ) {
                if ( (value & 1) && Right == *arg){
                    TRAIL_SBIND(FOLCMP_REF(Left,1),Sk(Left),Right,Key0);
                    Succeed;
                }
            }
        }
    } 
    Fail;
}

inline
static Bool
Loop_Subs( SP(l), SP(r) )
{                               /* l is a loop-term*/
    fol_t X = FOLCMP_REF(l,1);
    fkey_t Sk(X) = Sk(l);
    V_LEVEL_DISPLAY(V_TERM,"Loop Subs %&s vs %&s %d\n",l,Sk(l),r,Sk(r));
    if (ONCE_S_DEREF(X,Sk(X)))
        return sfol_identical(X,Sk(X),r,Sk(r));
    else {
        TRAIL_SBIND(X,Sk(X),r,Sk(r));
        return sfol_subsume(FOLCMP_REF(l,2),Sk(l),r,Sk(r));
    }
}

static
Bool sfol_true_loop( SP(t) )
{
    if (!FOLLOOPP(t))
        Fail;
    if (!LOOP_BOUNDP(t)){
            /* we unfold the declaration of a loop into a true one */
        V_LEVEL_DISPLAY(V_TERM,"Loop unfolding %&s\n",t,Sk(t));
        sfol_loop_bind(FOLCMP_REF(t,1),Sk(t),FOLCMP_REF(t,2),Sk(t));
    }
    Succeed;
}

inline
static Bool
Range_Subs( SP(Left), SP(Right))
{
       V_LEVEL_DISPLAY(V_TERM,"Range Subs %&s vs %&s\n",Left,Sk(Left),Right,Sk(Right));
       if (FOLINTP(Right)) {
           long v = CFOLINT(Right);
           long l_min = CFOLINT(FOLCMP_REF(Left,2));
           long l_max = CFOLINT(FOLCMP_REF(Left,3));
           if (l_min <= v && v <= l_max) {
               TRAIL_SBIND(FOLCMP_REF(Left,1),Sk(Left),Right,Sk(Right));
               Succeed;
           } else {
               Fail;
           }
       } else if (!FOLRANGEP(Right)) {
           Fail;
       } else {
           long l_min = CFOLINT(FOLCMP_REF(Left,2));
           long r_min = CFOLINT(FOLCMP_REF(Right,2));
           long l_max = CFOLINT(FOLCMP_REF(Left,3));
           long r_max = CFOLINT(FOLCMP_REF(Right,3));
           if (l_min > r_min || r_max > l_max) {
               Fail;
           } else {
               TRAIL_SBIND(FOLCMP_REF(Left,1),Sk(Left),Right,Sk(Right));
               Succeed;
           }
       } 
}

inline
static Bool
Tfs_Try_Subs( SP(r), SP(t) )
{
    if (FOLFSETP(r)) {
        return FSet_Subs(r,Sk(r),t,Sk(t));
    } else if (FOLFSETP(t)) {
        Fail;
    } else if (FOLRANGEP(r)) {
        return Range_Subs(r,Sk(r),t,Sk(t));
    } else if (FOLRANGEP(t)) {
        Fail;
    }  else if (FOLLOOPP(r)) {
        return Loop_Subs(r,Sk(r),t,Sk(t));
    }  else if (sfol_true_loop(t,Sk(t))) {
        return sfol_subsume(r,Sk(r),FOLCMP_REF(t,2),Sk(t));
    }  else if (FOLCMPP(r) && FOLCMPP(t) && folcmp_samefunp(r,t))
            /* => r_k and t_l have same functor */
        return Tfs_Simple_Subsume( r, Sk(r), t, Sk(t) );
    else
        return Tfs_Subs( r, Sk(r), t, Sk(t) );
}

/**********************************************************************
 *      sfol_subsume ...
 **********************************************************************/
Bool
sfol_subsume( fol_t r, fkey_t k, fol_t t, fkey_t l)
{
  // *TMP* To test TFS_Subs
  // SAFE(ONCE_L_DEREF,r,k);
  V_LEVEL_DISPLAY(V_TERM,"Subs %&s vs %&s %d\n",r,k,t,l);
  
  SAFE_ALT(CLOSURE_UL_DEREF,t,l);
  if (FOL_DEREFP(r)) {
      CLOSURE_UL_DEREF(r,k);
      if (FOL_DEREFP(r) && ONCE_S_DEREF(r,k)) {
          V_LEVEL_DISPLAY(V_TERM,"\tsderef to %&s\n",r,k);
              // the second part of the following check because of ad-hoc cases
              // where a naked variable in a variable tuple may be sbound to
              // a derefp terms, build upon the second component as its naked variable
          return sfol_identical(r,k,t,l)
              || (SAFE_FOLCMP_DEREFP(r) && sfol_identical(FOLCMP_REF(r,1),k,t,l))
              || (SAFE_FOLCMP_DEREFP(t) && sfol_identical(r,k,FOLCMP_REF(t,1),l))
              ;
      }
  }
  
  UNFOLD( sfol_subsume, r,k,t,l,
	  sfol_sbind( r,k,t,l ),
	  FALSE,
	  1, Tfs_Try_Subs(r,k,t,l)
	  );
}

Bool              /* DyALog Wrapper */
DYAM_sfol_subsume(fol_t r,fol_t t)
{
    fkey_t k=R_TRANS_KEY;
    return sfol_subsume(r,k,t,k);
}

/**********************************************************************
 *      sfol_alt_subsume ...
 **********************************************************************/
Bool
sfol_alt_subsume( fol_t r, fkey_t k, fol_t t, fkey_t l)
{
  SAFE_ALT(CLOSURE_UL_DEREF,r,k);
  SAFE_ALT(CLOSURE_UL_DEREF,t,l);
  if (FOL_DEREFP(r) && ONCE_S_DEREF(r,k)) {
      V_LEVEL_DISPLAY(V_TERM,"\tsderef to %&s\n",r,k);
          // the second part of the following check because of ad-hoc cases
          // where a naked variable in a variable tuple may be sbound to
          // a derefp terms, build upon the second component as its naked variable
      return sfol_identical(r,k,t,l)
          || (SAFE_FOLCMP_DEREFP(r) && sfol_identical(FOLCMP_REF(r,1),k,t,l));
  }

  UNFOLD( sfol_alt_subsume, r,k,t,l,
	  sfol_sbind( r,k,t,l ),
	  FALSE,
	  1, Tfs_Try_Subs(r,k,t,l)
	  );
}

Bool              /* DyALog Wrapper */
DYAM_sfol_alt_subsume(fol_t r,fol_t t)
{
    fkey_t k=R_TRANS_KEY;
    return sfol_alt_subsume(r,k,t,k);
}

/**********************************************************************
 * Tfs_Try_Identical
 *
 *    Check identity between two terms whose at least one of them is TFS
 *    a weak binding, but to the binder of this weak binding
 **********************************************************************/

Bool
Tfs_Simple_Identical( SP(Left), SP(Right) )
{
        /* this function looks strange and buggy
         */
    int arity = FOLCMP_ARITY(Left);
    fol_t *argL = &FOLCMP_REF(Left,2);
    int start = FOLCMP_DEREFP(Right) ? 2 : 1;
    fol_t *argR = &FOLCMP_REF(Right,start);
    V_LEVEL_DISPLAY(V_TERM,"Tfs Simple Identical %&s vs %&s %d\n",Left,Sk(Left),Right,Sk(Right));
    if (start==2 && !sfol_identical(FOLCMP_REF(Left,1),Sk(Left),FOLCMP_REF(Right,1),Sk(Right))) {
        Fail;
    }
    for( arity-- ; arity > 0 ; argL++, argR++, arity-- )
	if (!sfol_identical(*argL,Sk(Left),*argR,Sk(Right)))
            Fail;
    Succeed;
}

static Bool
Loop_Identical( SP(l), SP(r) )
{                               /* l is a loop-term*/
    V_LEVEL_DISPLAY(V_TERM,"Loop Identical %&s vs %&s %d\n",l,Sk(l),r,Sk(r));
    sfol_unif_bind(FOLCMP_REF(l,1),Sk(l),r,Sk(r));
    return sfol_identical(r,Sk(r),FOLCMP_REF(l,2),Sk(l));
}

static Bool
Tfs_Try_Identical(SP(r),SP(t))
{
    if (FOLLOOPP(r)) {
        return Loop_Identical(r,Sk(r),t,Sk(t));
    }  else if (sfol_true_loop(t,Sk(t))) {
        return sfol_identical(r,Sk(r),FOLCMP_REF(t,2),Sk(t));
    } else if (FOLCMPP(r) && FOLCMPP(t) && folcmp_samefunp(r,t)) {
        return Tfs_Simple_Identical(r,Sk(r),t,Sk(t));
    } else
        Fail;
}

/**********************************************************************
 *      sfol_identical ...
 **********************************************************************/

Bool
sfol_identical( r, k, t, l)
     fol_t r, t;
     fkey_t k, l;
{
  SAFE_ALT( CLOSURE_UL_DEREF, r, k );
  SAFE_ALT( CLOSURE_UL_DEREF, t, l );

  V_LEVEL_DISPLAY(V_TERM,"Identical %&s vs %&s %d\n",r,k,t,l);
  V_LEVEL_DISPLAY(V_TERM,"\ttest eq r==k => (%d) t==l => (%d) \n", (r==t),(k==l));
  UNFOLD( sfol_identical, r, k, t, l, FALSE, FALSE, 
	  1, Tfs_Try_Identical(r,k,t,l) );
}

Bool              /* DyALog Wrapper */
DYAM_sfol_identical(fol_t r,fol_t t)
{
    fkey_t k=R_TRANS_KEY;
    return sfol_identical(r,k,t,k);
}

/**********************************************************************
 *      sfol_layer_identical ...
 * similar to sfol_identical but uses ONCE_L_DEREF
 * WARNING: To be adapted for loop terms or replaced by sfol_identical
 **********************************************************************/
DSO_LOCAL Bool
sfol_layer_identical( r, k, t, l)
     fol_t r, t;
     fkey_t k, l;
{
  SAFE( ONCE_L_DEREF, r, k );
  SAFE( ONCE_L_DEREF, t, l );
  
  UNFOLD( sfol_layer_identical, r, k, t, l, FALSE, FALSE, 
	  0, FALSE );
}

/**********************************************************************
 *  sfol_weight r k
 *      measures the complexity of r_k
 **********************************************************************/
unsigned long
sfol_weight( fol_t r, fkey_t k)
{
  unsigned long w = 1;

  SAFE_ALT(CLOSURE_UL_DEREF,r,k);

  if (FOLVARP( r ))
    w = 0;
  else if (FOLCMPP( r )) {
    obj_t tuple = FOLINFO_TUPLE( r );
    if (FOLCMP_FUNCTOR(r) == FOLLOOP)
        return 0;
    if (FOLRANGEP(r)) {
        return CFOLINT(FOLCMP_REF(r,2))-CFOLINT(FOLCMP_REF(r,3));
    } 
    for(w =FOLINFO_WEIGHT( r );
	PAIRP(tuple) ;
	w += sfol_weight((fol_t)CAR(tuple),k), tuple = CDR(tuple));
  }
  
  return w;

}

/**********************************************************************
 *  sfol_copy r k
 *      make a copy of r and replace r's variables by their values
 *
 * WARNING: no variable of r should be weakly bound
 **********************************************************************/
fol_t
sfol_copy( fol_t r, fkey_t k )
{
  if (FOL_DEREFP(r)) {
    ONCE_UL_DEREF(r,k);
  } else if (FOLCMPP( r ) && ! FOLCMP_GROUNDP( r )) {

    unsigned long arity = FOLCMP_ARITY( r );
    fol_t *arg = &FOLCMP_REF(r,1);
    fol_t *stop = arg + arity;

    FOLCMP_WRITE_START( FOLCMP_FUNCTOR( r ), arity );

    for (; arg < stop ;)
      FOLCMP_WRITE( sfol_copy( *arg++, k) );
    
    r = FOLCMP_WRITE_STOP;
    
  }

  return r;
}

/**********************************************************************
 *  sfol_copy_from_subs r k
 *      make a copy of r and replace r's variables by their values
 *      using subs bindings
 *
 * WARNING: no variable of r should be weakly bound
 **********************************************************************/
fol_t
sfol_copy_from_subs( fol_t r, fkey_t k )
{
  if (FOL_DEREFP(r)) {
    ONCE_S_DEREF(r,k);
  } else if (FOLCMPP( r ) && ! FOLCMP_GROUNDP( r )) {
    
    unsigned long arity = FOLCMP_ARITY( r );
    fol_t *arg = &FOLCMP_REF(r,1);
    fol_t *stop = arg + arity;

    FOLCMP_WRITE_START( FOLCMP_FUNCTOR( r ), arity );

    for (; arg < stop ;)
      FOLCMP_WRITE( sfol_copy_from_subs( *arg++, k) );
    
    r = FOLCMP_WRITE_STOP;
    
  }

  return r;
}

/**********************************************************************
 * Term Ordering
 **********************************************************************/

#define Compare(_x,_y) ((_x) == (_y) ? 0 : (_x) < (_y) ? -1 : 1 )

static int
smb_compare( fol_t A, fol_t B )
{
    if (A==B)
        return 0;
    else {
        int r = strcmp( FOLSMB_NAME(A), FOLSMB_NAME(B) ); 
        return r ? r  : smb_compare(FOLSMB_MODULE(A), FOLSMB_MODULE(B));
    }
}

static int
sfol_compare( SP(A), SP(B) )
{
  Deref(A);
  Deref(B);

  if (SFOL_EQ(A,Sk(A),B,Sk(B)))
    return 0;

  if (FOLVARP(A))
    return (!FOLVARP(B)) ? -1 : ( Compare( FOLVAR_INDEX(A), FOLVAR_INDEX(B) ) ||
				  Compare( Sk(A) , Sk(B) ) );

  if (FOLVARP(B))
      return 1;
    
  if (FOLCHARP(A))
    return (!FOLCHARP(B)) ? -1 : Compare( CFOLCHAR(A), CFOLCHAR(B) );

  if (FOLCHARP(B))
      return 1;

  if (FOLINTP(A))
    return (!FOLINTP(B)) ? -1 : Compare( CFOLINT(A), CFOLINT(B) );

  if (FOLINTP(B))
      return 1;

  
  if (FOLSMBP(A))
      return (!FOLSMBP(B)) ? -1 : smb_compare(A,B);

  if (FOLSMBP(B))
      return 1;

  if (FOLLOOPP(A)) {
      sfol_unif_bind(FOLCMP_REF(A,1),Sk(A),B,Sk(B));
      return sfol_compare(FOLCMP_REF(A,2),Sk(A),B,Sk(B));
  }
  
  if (sfol_true_loop(B,Sk(B))){
      return sfol_compare(A,Sk(A),FOLCMP_REF(B,2),Sk(B));
  }
      
  /* A and B are both compound terms */
  if (FOLCMP_ARITY(A) != FOLCMP_ARITY(B)) {
    return Compare(FOLCMP_ARITY(A),FOLCMP_ARITY(B));
  } else  {
    /* A and B have same arity */  
    int ctrl = FOLCMP_ARITY(A)+1;
    fol_t *argA = &FOLCMP_FUNCTOR(A);
    fol_t *argB = &FOLCMP_FUNCTOR(B);
    int res = 0;
    for( ; res == 0 && ctrl > 0 ; ctrl-- )
      res = sfol_compare( *argA++, Sk(A), *argB++, Sk(B) );
    return res;
  }

}

static Bool Compare_3(Sproto,Sproto,Sproto);

Bool              /* DyALog Wrapper */
DYAM_Compare_3(fol_t Op_Word,fol_t A,fol_t B)
{
    fkey_t k=R_TRANS_KEY;
    return Compare_3(Op_Word,k,A,k,B,k);
}

static Bool	  /* DyALog Builtin */
Compare_3( SP(Op_Word), SP(A), SP(B) )
{
    TrailWord *top= C_TRAIL_TOP;
    int res = sfol_compare( A, Sk(A), B, Sk(B) );
    char *op = (res < 0) ? "<" : (res > 0) ? ">" : "=";
    fol_t op_atom = Create_Atom(op);
    if (C_TRAIL_TOP > top)
        untrail_alt(top);
    return Unify( op_atom, Key0, Op_Word, Sk(Op_Word) );
}

/**********************************************************************
 * Dyam Instructions
 **********************************************************************/

Bool                            /* Dyam instruction */
Dyam_Unify_Item(fol_t A)
{
    Bool res;
    V_LEVEL_DISPLAY( V_TERM,"\tunify item %&s with %&s\n",
                     R_ITEM_COMP, R_ITEM_KEY,
                     A, R_TRANS_KEY);
    res=sfol_unify(R_ITEM_COMP,R_ITEM_KEY,A,R_TRANS_KEY);
    V_LEVEL_DISPLAY( V_TERM, "\t\t-->%d\n", res);
    return res;
}

Bool                            /* Dyam instruction */
DYAM_Unify_2(fol_t A, fol_t B)    /* ***WARNING*** incoherence in notation */
{
    V_LEVEL_DISPLAY( V_TERM,"\tunify %&s with %&s\n",
                     A, R_TRANS_KEY,
                     B, R_TRANS_KEY);
    return sfol_unify(A,R_TRANS_KEY,B,R_TRANS_KEY);
}

Bool Dyam_Unify(fol_t A, fol_t B)
{
  return DYAM_Unify_2(A,B);
}

