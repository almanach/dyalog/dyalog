/* 
 ******************************************************************
 * $Id$
 * Copyright (C) 1998, 2006, 2007, 2008, 2009, 2010, 2011, 2014, 2015, 2016 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  tree-indexing.h -- Header file for tree-indexing.c
 *
 * ----------------------------------------------------------------
 * Description
 * 
 * ----------------------------------------------------------------
 */

#include <string.h>

typedef void *Closure;
typedef Bool (* ClosedFn) (tabobj_t,Closure);

#define INDEXATION_MAX_DEPTH        5
#define INDEXING_MAX_ARITY         12

typedef struct Trailed_HashScan {
    boxtype type;               /* HASH_SCAN */
    HashScan scan;
} *Trailed_HashScan;

typedef struct Resume {
    boxtype type;               /* INDEXATION */
    struct Resume *prev;
    short depth;
    Closure closure;
    fol_t A;
    struct ResumeInfo {
        fol_t   *arg;
        fol_t   *stop;
        fkey_t  k;
        fol_t deref;
        fkey_t k_deref;
    } info[INDEXATION_MAX_DEPTH+2];
} *Resume;

typedef struct UndoForward {
    boxtype type;
    short forward;
    fol_t deref;
    fkey_t k_deref;    
} *UndoForward;

typedef struct UndoUpward {
    boxtype type;
    fol_t *stop;
    fkey_t k;
} *UndoUpward;

#define IP ((Resume) REG(I_IP))
#define LVALUE_IP (LVALUE_REG(I_IP))

#if (defined(__clang__) || (__GNUC__ >= 5))
#define EXTERN_OR_STATIC static
#else
#define EXTERN_OR_STATIC extern
#endif

inline
EXTERN_OR_STATIC
void
Untrail_Forward(UndoForward undo)
{
    short pop = undo->forward;
    struct ResumeInfo *tmp = IP->info+IP->depth;
    V_LEVEL_DISPLAY( V_INDEX, "\tundo forward %d\n", pop);
    IP->depth += pop;
    for (; pop ; pop--, ((tmp++)->arg)--);
    (tmp->arg)--;
    tmp->deref=undo->deref;
    tmp->k_deref=undo->k_deref;
}

inline
EXTERN_OR_STATIC
void
Untrail_Upward(UndoUpward undo)
{
    short depth = (IP->depth)--;
    struct ResumeInfo *info = &(IP->info[depth]);
    
    V_LEVEL_DISPLAY( V_INDEX, "\tundo pushing : restoring %&s from depth %d\n",
                     DFOLINT(0), undo->k, depth);
    info->arg = info->stop = undo->stop;
    info->k=undo->k;
}

