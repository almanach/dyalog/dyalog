/************************************************************
 * $Id$
 * Copyright (C) 1997, 2004, 2006, 2009, 2014 Eric de la Clergerie
 * ------------------------------------------------------------
 *
 *   Archi -- Architecture
 *
 * ------------------------------------------------------------
 * Description
 *   Register architecture
 * ------------------------------------------------------------
 */

#define ARCHI_FILE

#include "libdyalog.h"

void print_reg_bank()
{
  printf( "register bank is %p\n", (void *)reg_bank);
}

void initialization_registers()
{
    trail[I_TOP]   = (TrailWord) TRAIL_BASE ;
    trail[I_LAYER] = (TrailWord) &lstack[0];
    trail[I_TRAIL] = (TrailWord) TRAIL_BASE;
    trail[I_CTL]   = (TrailWord) &c_ctl[0];
//    trail[I_TRANS] = (TrailWord) BINT(1);
//    trail[I_ITEM]  = (TrailWord) BINT(3);
    trail[I_TRANS] = (TrailWord) 0;
    trail[I_ITEM]  = (TrailWord) 0;
    trail[I_HEAP]  = trail[I_HEAP_STOP]
        = (TrailWord) GC_MALLOC( sizeof(fol_t) * FOLCMP_HEAP_SIZE );
    trail[I_CP]    = (TrailWord) 0;
    trail[I_E]     = (TrailWord) 0;
    trail[I_B]     = (TrailWord) 0;
    trail[I_BC]    = (TrailWord) 0;
    trail[I_IP]    = (TrailWord) 0;
//    trail[I_IP]    = (TrailWord) 0;
    trail[I_MIN_LAYER] = trail[I_LAYER];
    trail[I_MODULE] = (TrailWord) FOLNIL;
    trail[I_OCCUR_CHECK] = (TrailWord) 1;
    trail[I_VERBOSE_LEVEL] = (TrailWord) 0;
    reg_bank = (TrailWord *)trail;
}


