/************************************************************
 * $Id$
 * Copyright (C) 1997, 2002, 2003, 2004, 2005, 2006, 2008, 2009, 2011, 2012, 2014, 2015, 2016 Eric de la Clergerie
 * ------------------------------------------------------------
 *
 *   rt-evalpred -- 
 *
 * ------------------------------------------------------------
 * Description
 *   Run Time specialized versions of some builtins
 * ------------------------------------------------------------
 */

//#define VERBOSE

#include "libdyalog.h"
#include "builtins.h"

/**********************************************************************
 * Database Management
 **********************************************************************/

static Bool evpred_assert(Sproto,Sproto);

Bool              /* DyALog Wrapper */
DYAM_evpred_assert(fol_t data,fol_t addr)
{
    fkey_t k=R_TRANS_KEY;
    return evpred_assert(data,k,addr,k);
}

Bool                            /* DyALog Wrapper */
DYAM_evpred_assert_1(fol_t data) 
{
    fkey_t Sk(data)=R_TRANS_KEY;
    Deref(data);
    Trail;
    rt_install_database(data,Sk(data));
    Untrail;
    Succeed;
}

Bool
evpred_assert( SP(data), SP(addr) )
{
    tabobj_t obj;
    Deref(data);
    Trail;
    obj = (tabobj_t) rt_install_database(data,Sk(data));
    Untrail;
    return Unify(POINTER_TO_DYALOG(obj),Key0,addr,Sk(addr));
}

void unif_delete( Sproto );

static Bool evpred_retract(Sproto);

Bool              /* DyALog Wrapper */
DYAM_evpred_retract(fol_t data)
{
    fkey_t k=R_TRANS_KEY;
    return evpred_retract(data,k);
}

Bool
evpred_retract( SP(data) )
{
  Deref(data);
  unif_delete(data,Sk(data));
  Succeed;
}


/**********************************************************************
 * MultiValued Domain : Unifies var with each values in set
 **********************************************************************/

extern void Domain_FSet();

// **DyALog Builtin** Domain_FSet :- Dyam_Domain_FSet()

typedef struct {
    fol_t *arg;
    fol_t *stop;
    unsigned long value;
    fol_t *value_arg;
    int   rank;
} domain_fset_env, *domain_fset_env_t;

static void
domain_fset_next(const domain_fset_env_t env)
{
    (env->arg)++;
    (env->rank)--;
    if (!env->rank){        /* move to next word in FSet */
        env->value = UCFOLINT(*(env->value_arg));
        (env->value_arg)++;
        env->rank=FSET_BIT_PER_WORD;
    } else {
        (env->value) >>=1;
    }
}

Bool
Dyam_Domain_FSet()
{
    domain_fset_env *env=(domain_fset_env *)X(2);
    fol_t v;

    for ( ; env->arg < env->stop && !(env->value & 1);)
        domain_fset_next(env);
    
    if (env->arg == env->stop) {
        Dyam_Remove_Choice();
        Fail;
    }
    
    v = *(env->arg);

    domain_fset_next(env);
    update_choice(Domain_FSet,3);
    
    return v && Unify((fol_t)X(0),(fkey_t)X(1),v,Key0);
    
}

extern void Domain_Range();

// **DyALog Builtin** Domain_Range :- Dyam_Domain_Range()

typedef struct {
    long v;
    long max;
} domain_range_env, *domain_range_env_t;

Bool
Dyam_Domain_Range()
{
    V_DISPLAY("Dyam_Domain_Range\n%&t");
    domain_range_env *env=(domain_range_env *)X(2);
    fol_t v;

    v = DFOLINT(env->v);
    env->v++;

    
    if (env->v > env->max) {
        Dyam_Remove_Choice();
    //        Fail;
    } else {
        update_choice(Domain_Range,3);
    }
    
    return Unify((fol_t)X(0),(fkey_t)X(1),v,Key0);
}

extern void Domain_Aux_2();

// **DyALog Builtin** Domain_Aux_2 :- Dyam_Domain_Aux_2(2,3)

Bool
Dyam_Domain_Aux_2( SP(list) )
{
        /* we assume list has been deref and is a list */
    fol_t car = FOLPAIR_CAR(list);
    fkey_t Sk(car) = Sk(list);
    
    V_DISPLAY("Dyam_Domain_Aux_4\n%&t");
    list = FOLPAIR_CDR(list);
    Deref(list);
    
    if (!FOLPAIRP(list)) {
        Dyam_Remove_Choice();
    } else {
        LVALUE_X(2)= REG_VALUE(list);
        LVALUE_X(3)= REG_VALUE(Sk(list));
        update_choice(Domain_Aux_2,4);
    }
    
    return Unify((fol_t)X(0),(fkey_t)X(1),car,Sk(car));
    
}

// **DyALog Builtin** Domain_2 :- Dyam_Domain_2(0,1)

Bool
Dyam_Domain_2(fol_t var,fol_t list)
{
    fkey_t Sk(var)=R_TRANS_KEY;
    fkey_t Sk(list) = R_TRANS_KEY;
    
    V_DISPLAY("Dyam_Domain_2\n");
    
    Deref(list);

    if (FOLNILP(list))
        Fail;
    
    if (FOL_CSTP(list))
        return Unify(var,Sk(var),list,Sk(list));
    
    LVALUE_X(0)= REG_VALUE(var);
    LVALUE_X(1)= REG_VALUE(Sk(var));

    if (FOLFSETP(list)) {
        fol_t table = FOLCMP_REF(list,2);
        fol_t *arg = &FOLCMP_REF(table,1);
        fol_t *stop = arg + FOLCMP_ARITY(table);
        unsigned long value = UCFOLINT( FOLCMP_REF(list,3) );
        fol_t *value_arg = &FOLCMP_REF(list,3)+1;
        domain_fset_env *env= (domain_fset_env
                               *)PUSH_TRAIL_BLOCK(sizeof(domain_fset_env));
        *env=(domain_fset_env){arg,stop,value,value_arg,FSET_BIT_PER_WORD};
        LVALUE_X(2)= REG_VALUE(env);
        TRAIL_CHOICE(Domain_FSet,3);
    } else if (FOLRANGEP(list)) {
        long min = CFOLINT(FOLCMP_REF(list,2));
        long max = CFOLINT(FOLCMP_REF(list,3));
        domain_range_env *env = (domain_range_env *) PUSH_TRAIL_BLOCK(sizeof(domain_range_env));
        *env = (domain_range_env){min,max};
        LVALUE_X(2) = REG_VALUE(env);
        TRAIL_CHOICE(Domain_Range,3);
    } else if (FOLPAIRP(list)) {
        LVALUE_X(2) = REG_VALUE(list);
        LVALUE_X(3) = REG_VALUE(Sk(list));
        TRAIL_CHOICE(Domain_Aux_2,4);
    }

    Fail;
}

Bool
DyALog_Domain() 
{
    fol_t car;
    sfol_t l = Get_Choice_Buffer(sfol_t);
    sfol_t var = Get_Choice_Buffer(sfol_t)+1;
    V_LEVEL_DISPLAY(V_DYAM,"DyALog_Domain [%d]\n",(long)l);
    SFOL_Deref(l);
    if (!FOLPAIRP(l->t)){
        No_More_Choice();
        Fail;
    }
    car = FOLPAIR_CAR(l->t);
    l->t= FOLPAIR_CDR(l->t);
    return Unify(var->t,var->k,car,l->k);
}

/**********************************************************************
 * MultiValued Database : retrieve each object unifibale with data
 **********************************************************************/

struct sterm {
  fol_t t;
  fkey_t k;
};

struct database_closure {
  void     *fn;
  tabobj_t  trans;
  fkey_t    k_trans;
  fun_t     f;
  Bool    flag;
  struct  sterm *data;
};

typedef void *Closure;

/**********************************************************************
 * MultiValued Callret : retrieve each callret object unifiable with pred
 **********************************************************************/

extern obj_t _dyalog_callret_viewer;
extern void Callret_Aux();

typedef struct {
    Sdecl(item);
    Sdecl(pred);
    fkey_t k;
} callret_env;

// **DyALog Builtin** Callret_Aux :- Dyam_Callret_Aux(1)

Bool
Dyam_Callret_Aux( obj_t viewers )
{
    callret_env *env=(callret_env *)X(0);

    if (!PAIRP(viewers)){
        Dyam_Remove_Choice();
        Fail;
    }
    
    LVALUE_X(1)= REG_VALUE(CDR(viewers));
    update_choice(Callret_Aux,2);

    return Unify( env->item, env->Sk(item), (fol_t) CAR(CAR(viewers)),env->k)
        && Unify( env->pred, env->Sk(pred), (fol_t) CDR(CAR(viewers)), env->k);
}

// **DyALog Builtin** Callret_2 :- Dyam_Callret_2(0,1)

Bool 
Dyam_Callret_2( fol_t item, fol_t pred )
{
    obj_t viewers = _dyalog_callret_viewer;
    fkey_t k=R_TRANS_KEY;
    fkey_t new_k= LSTACK_PUSH_VOID;
    callret_env *env;
    
    if (NULLP(viewers))
        Fail;
      
    env=(callret_env *)PUSH_TRAIL_BLOCK(sizeof(callret_env));
    *env=(callret_env){item,k,pred,k,new_k};

    V_DISPLAY("Dyam_Callret_2\n");
    
    LVALUE_X(0)= REG_VALUE(env);
    LVALUE_X(1)= REG_VALUE(viewers);
    TRAIL_CHOICE(Callret_Aux,2);
    Fail;
}

/**********************************************************************
 * MultiValued Object_2 : retrieve addresses of objects unifiable with object
 **********************************************************************/

extern Bool unif_retrieve_alt( Sproto, Closure );

typedef struct {
    void *fn;
    Sdecl(term);
    Sdecl(add);
} object_closure;

static Bool
closed_object( tabobj_t o, object_closure *closure )
{
  Sdecl(obj);
  Sk(obj) = LOAD_OBJECT(o);
  obj = TABOBJ_MODEL(o);
//  V_DISPLAY("here\n");
  V_DISPLAY( "\t\tin object closure with %&s\n", obj, Sk(obj));
  return Unify(closure->term,closure->Sk(term),obj,Sk(obj))
      && Unify(closure->add,closure->Sk(add), POINTER_TO_DYALOG(o),Key0);
}

// **DyALog Builtin** Object_2 :- Dyam_Object_2(0,1)

Bool
Dyam_Object_2( fol_t term, fol_t add )
{
    fkey_t Sk(term)=R_TRANS_KEY;
    fkey_t Sk(add)=R_TRANS_KEY;

    V_LEVEL_DISPLAY( V_DYAM, "Find Recorded %&s\n", term, Sk(term));
        
    Deref(add);
    if (FOLINTP(add)) {
        tabobj_t o = (tabobj_t) DYALOG_TO_POINTER(add);
        Sdecl( obj );
        obj = TABOBJ_MODEL(o);
        Sk(obj) = TABOBJ_KIND(o) ? LOAD_OBJECT(o) : LSTACK_PUSH_VOID;
        return Unify(term,Sk(term),obj,Sk(obj));
    } else {
        Closure closure = (Closure) PUSH_TRAIL_BLOCK(sizeof(object_closure));
        *(object_closure *)closure = (object_closure) {(void *)&closed_object,S(term),S(add)};
        return unif_retrieve_alt( term, Sk(term), closure );
    }
}

static Bool
closed_object_1( tabobj_t o, object_closure *closure )
{
  Sdecl(obj);
  Sk(obj) = LOAD_OBJECT(o);
  obj = TABOBJ_MODEL(o);
//  V_LEVEL_DISPLAY(V_DYAM, "here\n");
  V_LEVEL_DISPLAY(V_DYAM, "\t\tin object closure with %&s\n", obj, Sk(obj));
  return Unify(closure->term,closure->Sk(term),obj,Sk(obj));
}

// **DyALog Builtin** Object_1 :- Dyam_Object_1(0)

Bool
Dyam_Object_1( fol_t term )
{
    fkey_t Sk(term)=R_TRANS_KEY;
    Closure closure = (Closure) PUSH_TRAIL_BLOCK(sizeof(object_closure));
    *(object_closure *)closure = (object_closure) {(void *)&closed_object_1,S(term),0,0};
    Bool res; 
    V_LEVEL_DISPLAY( V_DYAM, "Find Recorded %&s\n", term, Sk(term));
//    dyalog_printf("Find Recorded %&s\n", term, Sk(term));
    res = unif_retrieve_alt( term, Sk(term), closure );
//    dyalog_printf( "Found Recorded %&s\n", term, Sk(term));
    return res;
}

extern Bool unif_retrieve_general( Sproto, Closure, void *entry);

// **DyALog Builtin** Local_Object :- Dyam_Local_Object(0,1)

Bool
Dyam_Local_Object( fol_t table, fol_t term)
{
    fkey_t Sk(term)=R_TRANS_KEY;
    fkey_t Sk(table)=R_TRANS_KEY;
    Closure closure = (Closure) PUSH_TRAIL_BLOCK(sizeof(object_closure));
    *(object_closure *)closure = (object_closure) {(void *)&closed_object_1,S(term),0,0};
    Bool res;
    Deref(table);
    V_LEVEL_DISPLAY( V_DYAM, "Find localy Recorded %&s\n", term, Sk(term));
//    dyalog_printf( "Find Recorded %&s\n", term, Sk(term));
    res = unif_retrieve_general( term, Sk(term), closure, DYALOG_TO_POINTER(table));
//    dyalog_printf( "Found Recorded %&s\n", term, Sk(term));
    return res;
}


/**********************************************************************
 * MultiValued Forest : access to object backpointer
 **********************************************************************/

#define B_TYPE( b )  (backptr_t) (PAIRP( b ) ? CINT( CAR( b )) : CINT( b ))
#define B_CAR( b )   (CAR( CDR( b )))
#define B_CDR( b )   (CDR( CDR( b )))

extern void Follow_Backptr();

// **DyALog Builtin** Follow_Backptr :- Dyam_Follow_Backptr_Aux(1,2)

typedef struct {
    Sdecl(T);
    Sdecl(B);
    Sdecl(C);
    Sdecl(L);
    Sdecl(Ind);
} forest_env;

Bool
Dyam_Follow_Backptr( obj_t backptr, long indirect )
{
    fol_t B;
    fol_t C;
    forest_env *env = (forest_env *)X(0);
    int type = B_TYPE( backptr );
    fol_t T = DFOLINT(type);
    obj_t car;
    obj_t cdr;

//    printf("backptr %x type=%d\n",backptr,type);
    
    switch (type) {
        case f_call:
        case f_init:
            return Unify(env->T,env->Sk(T),T,Key0)
                && Unify(env->Ind,env->Sk(Ind),DFOLINT(indirect),Key0);
        case f_or:
            car=B_CAR(backptr);
            cdr= B_CDR(backptr);
            LVALUE_X(1)= REG_VALUE(cdr);
            LVALUE_X(2)= REG_VALUE(indirect);
            TRAIL_CHOICE(Follow_Backptr,3);
            return Dyam_Follow_Backptr( car, indirect );
        case f_and:
            car=B_CAR(backptr);
            cdr= B_CDR(backptr);
            B = POINTER_TO_DYALOG( car );
            C = POINTER_TO_DYALOG( cdr );
            if ( ((tabobj_t)car)
                 && ((tabobj_t)car)->backptr
                 && B_TYPE(((tabobj_t)car)->backptr) == f_trace
                 && ! Unify(env->L,env->Sk(L),(fol_t) B_CAR(((tabobj_t)car)->backptr),Key0)){
                Fail;
            }
            return  Unify(env->T,env->Sk(T),T,Key0)
                && Unify(env->B, env->Sk(B), B, Key0 )
                && Unify(env->C, env->Sk(C), C, Key0 )
                && Unify(env->Ind,env->Sk(Ind),DFOLINT(indirect),Key0);
        case f_trace:
            cdr= B_CDR(backptr);
            return Dyam_Follow_Backptr( cdr, indirect );
        case f_indirect:
            car = B_CAR(backptr);
//            B = POINTER_TO_DYALOG(car);
            return Dyam_Follow_Backptr( car, 1 );

        default:
            Fail;
    }
}

Bool
Dyam_Follow_Backptr_Aux(obj_t backptr, int indirect)
{
    Dyam_Remove_Choice();
    return Dyam_Follow_Backptr(backptr,indirect);
}

// **DyALog Builtin** Forest_6 :- Dyam_Forest_6(0,1,2,3,4,5)


Bool
Dyam_Forest_6( fol_t A, fol_t Type, fol_t B, fol_t C, fol_t L, fol_t Ind)
        /* A should denote the instantiated address of a DyALog object */
{
    fkey_t k=R_TRANS_KEY;
    fkey_t Sk(A)=k;
    forest_env *env;
    obj_t backptr = 0;
    
    Deref(A);

    if (!FOLINTP(A)) Fail;

    env=(forest_env *)PUSH_TRAIL_BLOCK(sizeof(forest_env));
    *env=(forest_env){Type,k,B,k,C,k,L,k,Ind,k};

    LVALUE_X(0)= REG_VALUE(env);
    backptr = ((tabobj_t) DYALOG_TO_POINTER(A))->backptr;
    return backptr && Dyam_Follow_Backptr( backptr , 0 );
    
}


/**********************************************************************
 * DyALog_Count: enumerate all values between a (inclusive)
 * and b (non inclusive)
 * Note: new builtin format (see Compiler/foreign.pl)
 * to use it, write something like

 '$interface'( 'DyALog_Count'(A:int,B:int, Res: -int),
                [ choice_size(0) ] )
 
 **********************************************************************/

Bool                            
DyALog_Count(const int a, const int b, int *res)
{
    int n = a+Get_Choice_Counter();
    if (n>=b) {
        No_More_Choice();
        Fail;
    }
    *res=n;
    Succeed;
}

/**********************************************************************
 * DyALog_Arg: enumerate all args of T
 * Note: new builtin format (see Compiler/foreign.pl)
 * to use it, write something like

 '$interface'( 'DyALog_Arg'(T:term, I:-int, A: term),
                [ choice_size(2) ] )
 
 **********************************************************************/

Bool                            
DyALog_Arg(sfol_t t, int *i, sfol_t a)
{
    int n = Get_Choice_Counter();
    fol_t **ptr = Get_Choice_Buffer(fol_t **);
    fol_t **stop = Get_Choice_Buffer(fol_t **)+1;
//    dyalog_printf("DyALog Arg n=%d",n);
    if (n == 0) {
        SFOL_Deref(t);
//        dyalog_printf("\tt=%&s\n",t->t,t->k);
        if (!FOLCMPP(t->t)) {
            No_More_Choice();
            Fail;
        } else {
            *ptr = &FOLCMP_REF(t->t,1);
            *stop = *ptr + FOLCMP_ARITY(t->t)-1;
        }
    }
//    dyalog_printf("\tptr=%x stop=%x\n",(void *)*ptr,(void *)*stop);
    if (*ptr==*stop){
        No_More_Choice();
    }
    *i=n+1;
    return Unify(*(*ptr)++,t->k,a->t,a->k);
}

/**********************************************************************
 * DyALog_Feature_Arg: enumerate all args of feature term T
 * Note: new builtin format (see Compiler/foreign.pl)
 * to use it, write something like

 '$interface'( 'DyALog_Feature_Arg'(T:term, F:-feature, I:-int, A: term),
                [ choice_size(3) ] )
 
 **********************************************************************/

Bool                            
DyALog_Feature_Arg(sfol_t t, char **f, int *i, sfol_t a)
{
    int n = Get_Choice_Counter();
    fol_t **ptr = Get_Choice_Buffer(fol_t **);
    fol_t **stop = Get_Choice_Buffer(fol_t **)+1;
    fol_t **table = Get_Choice_Buffer(fol_t **)+2;
        // dyalog_printf("DyALog Feature Arg n=%d",n);
    if (n == 0) {
        SFOL_Deref(t);
            // dyalog_printf("\tt=%&s\n",t->t,t->k);
        if (!FOLCMPP(t->t)) {
            No_More_Choice();
            Fail;
        } else {
            fol_t functor = FOLCMP_FUNCTOR(t->t);
            obj_t info;
            fol_t features;
            if ((info = folsmb_feature_info(functor)) == BFALSE) {
                No_More_Choice();
                Fail;
            }
            features = (fol_t) CDR(info);
                //                dyalog_printf("table %&f\n",features);
            *ptr = &FOLCMP_REF(t->t,1);
            *stop = *ptr + FOLCMP_ARITY(t->t)-1;
            *table = &FOLCMP_REF(features,1);
        }
    }
        //  dyalog_printf("\tptr=%x stop=%x\n",(void *)*ptr,(void *)*stop);
    if (*ptr==*stop){
        No_More_Choice();
    }
    *i=n+1;
    *f=FOLSMB_NAME(*(*table)++);
    return Unify(*(*ptr)++,t->k,a->t,a->k);
}


/**********************************************************************
 * DyALog_Repeat: juste an infinite loop
 **********************************************************************/

Bool
DyALog_Repeat()
{
    Succeed;
}
