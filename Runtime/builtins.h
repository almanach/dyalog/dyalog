/* $Id$
 * Copyright (C) 1997, 2004, 2006, 2009, 2010, 2016, 2017 Eric de la Clergerie
 * ------------------------------------------------------------
 *
 *   BuilIns -- 
 *
 * ------------------------------------------------------------
 * Description
 *
 * ------------------------------------------------------------
 */

#ifdef __cplusplus
extern "C" {
#endif

#ifndef READ_BUILTINS
#define READ_BUILTINS

#define Declare_Object_Initializer(f)		\
void (f)();

#define NOT_A_WAM_WORD      (fol_t) 0
#define True                (Bool) 1
#define False               (Bool) 0

#define Sk(n)               k_##n

#define S(n)                n,Sk(n)

#define Sdecl(n)				\
        fol_t     n;				\
        fkey_t    Sk(n)

#define Sinit(_n,_m)				\
        fol_t   _n=_m;				\
	fkey_t  Sk(_n)=Sk(_m)

#define Sproto               fol_t, fkey_t

#define SP(n)               fol_t n, fkey_t Sk(n)
    
#define Sassign(A,v,k)                          \
       A=v;                                     \
       Sk(A)=k

#define Deref( a )\
  if (FOL_DEREFP(a) && closure_ul_deref(a,Sk(a))) {       \
       ASM_UNLOAD(a,Sk(a));                             \
   } 

#define Light_Deref(a)                          \
if (closure_ul_deref(a,Sk(a))) {                \
    ASM_UNLOAD(a,Sk(a));                        \
   }  

// if( FOL_DEREFP( a ) ) CLOSURE_UL_DEREF(a,Sk(a))

#define Loop_Deref( a )   if( FOL_DEREFP( a ) ) LOOP_DEREF(a,Sk(a))

#define Bind( t, l, X )   TRAIL_UBIND(X,Sk(X),t,l)

#define ImmediateBind( v , A) (FOLVARP(A) && ( Bind(v,Key0,A) , True ))

#define Get_Cst( n, A )				\
({						\
  Deref(A);					\
  ( A == n) || ImmediateBind( n, A);		\
})

#define Get_Nil( A )         Get_Cst( FOLNIL, A)
#define Get_Integer( n, A)   Get_Cst( n     , A)
#define Get_Char( c, A )     Get_Cst( c     , A)

#define Unif_Bind(a,ka,b,kb)      sfol_unif_bind(a,ka,b,kb)
#define Subs_Bind(a,ka,b,kb)      TRAIL_SBIND(a,ka,b,kb)

#define Unify(r,k,t,l)   sfol_unify(r,k,t,l)

#define Fail            return False
#define Succeed         return True
#define Key0            ((fkey_t) 0)

#define Trail           TRAIL_LAYER()
#define Untrail         untrail_layer()

#define SymbolName( s )      FOLSMB( s )->name

#define Convert_To_Atom( s )       FOLSMB_MAKE( FOLSMB_INDEX( s ), 0)
#define Create_Allocate_Atom( s )  find_folsmb(s,0)
#define Create_Atom( s )           find_folsmb(s,0)

#define Create_Pair( car, cdr )   folcmp_create_pair( car, cdr )

#define Functor_Arity(n,a)        find_folsmb(n,a)

#define System_Error(s)                         \
    ({                                          \
        perror(s);                              \
        exit(EXIT_FAILURE);                     \
    })

#define Fatal_Error( s, args...  )		\
     ({						\
       fprintf(stderr, s "\n", ## args);	\
       exit(EXIT_FAILURE);					\
     })

#define Deref_And_Fail_Unless(_X,_test)		\
     Deref(_X);					\
     if (!(_test)) Fail;

#define Fail_Unless(_X,_test)		\
     if (!(_test)) Fail;

#define Unify_Or_Fail( r, k, t, l)		\
     if (!sfol_unify(r,k,t,l)) Fail

#define Subsume_Or_Fail( r, k, t, l)		\
     if (!sfol_subsume(r,k,t,l)) Fail

/**********************************************************************
 * For foreigns 
 **********************************************************************/

typedef long ForeignWord;

#ifdef DYAM_FILE
ForeignWord   foreign_bkt_counter;
ForeignWord *foreign_bkt_buffer;
#else
extern ForeignWord   foreign_bkt_counter;
extern ForeignWord *foreign_bkt_buffer;
#endif

#define Get_Choice_Counter()   foreign_bkt_counter
#define Get_Choice_Buffer(t)   ((t) foreign_bkt_buffer)
#define No_More_Choice()                       \
        LVALUE_C_CTL_TOP=REG_VALUE(((TrailWord *) R_B)-1);       \
        LVALUE_R_BC=REG_VALUE(R_B->bc) ;                         \
        LVALUE_R_B= REG_VALUE(R_B->prev)

typedef struct sfol {
    fol_t t;
    fkey_t k;
} * sfol_t;

#define SFOL_Deref( a )        if( FOL_DEREFP( a->t ) ) CLOSURE_UL_DEREF(a->t,a->k)

/**********************************************************************
 * Prototypes
 **********************************************************************/

//extern int    Chars_To_String( char*, Sproto);
//extern Bool String_To_Chars( char*, Sproto);

//extern fol_t  read_fol_number( char* );

#include "stream_supp.h"

extern void sfol_display( Sproto );
extern void sfol_fdisplay( StmInf *, Sproto );
extern void fol_display( fol_t );
extern void fol_fdisplay( StmInf *, fol_t);
extern void fol_print( fol_t );
extern void sfol_print( Sproto );
extern void subst_fdisplay( StmInf *, Sproto );
extern void subst_display( Sproto );
extern void callret_fdisplay( StmInf *, Sproto);

extern void Dyam_Remove_Choice();
extern void Dyam_Full_Choice(const continuation_t fun, const int n);
extern choice_t Dyam_Full_Choice_Light(const continuation_t fun, const int n);

extern void DyALog_Assign_Display_Info( sfol_t );

DSO_LOCAL extern Bool forest_indirect( obj_t );

#endif /* READ_BUILTINS */

#ifdef __cplusplus
}
#endif
