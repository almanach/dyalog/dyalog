/************************************************************
 * $Id$
 * Copyright (C) 1996, 2004, 2006, 2007, 2008, 2009, 2010, 2011 Eric de la Clergerie
 * ------------------------------------------------------------
 *
 *   Trail -- Implementation of the dereferencers
 *
 * ------------------------------------------------------------
 * Description
 *   *Implementation of the two bindings stacks 
 *   *Implementation of the different kinds of dereferencers through
 *   unification, subsumption and/or layer bindings
 * ------------------------------------------------------------
 */

#include "libdyalog.h"
#include "builtins.h"
#include "hash.h"
#include "tree-indexing.h"

#ifdef STAT

STAT_DEF(deref,
	 closure_ul,
	 closure_ul_1,
	 closure_ul_2,
	 closure_ul_3,
	 closure_ul_4,
	 closure_ul_5,
	 closure_ul_6,
	 closure_ul_7
	 )

STAT_DEF(layer,
	 call,
	 void_call,
	 void_push,
	 push
	 )

static void stat_display() __attribute__ ((destructor)); 

void stat_display()  
{
  printf( 

	 "----------------------------------------------------------------------\n"
	 "STAT <env loading>\n"
	 "\tcall       %6d\n"
	 "\tlayer push %6d\n"
	 "\tcall void  %6d\n"
	 "\tpush void  %6d\n"
	 "----------------------------------------------------------------------\n",

	  STAT_USE(layer,call),
	  STAT_USE(layer,push),
	  STAT_USE(layer,void_call),
	  STAT_USE(layer,void_push)
	 
	 );

  printf( 
	 
	 "----------------------------------------------------------------------\n"
	 "STAT <closure ul deref>\n"
	 "\tcall %d\n"
	 "----------------------------------------------------------------------\n",

	 STAT_USE(deref,closure_ul)
	 
	 );

}

#endif

/**********************************************************************
 *   the control stack (forCHOICE and ENVIRONMENT)
 **********************************************************************/

TrailWord c_ctl[CTL_SIZE];

/**********************************************************************
 *   the trail stack
 **********************************************************************/

TrailWord trail[TRAIL_BASE_N+TRAIL_SIZE];

/**********************************************************************
 *   The layer stack (to store layer bindings)
 **********************************************************************/

struct layer lstack[LSTACK_SIZE];

/**********************************************************************
 *   The different dereferencers
 **********************************************************************/

/*
 * one step deref through the unification bindings of X
 * (WARNING: we assume X is a variable)
 */
inline
binding_t
once_u_deref(const fol_t X, const fkey_t k)
{
    if (LAYER_IS_LOC_BOUND(k)) {
        binding_t binding = (binding_t) (FOL_UNIF_REF( X ));
        for (; binding ; binding= binding->next){
            if ( k == binding->binder_key ){
                ASM_LOAD(binding->bindee,binding->bindee_key,binding);
            } else if ( k > binding->binder_key ) {
                ASM_LOAD_NULL;
            }
        }
    }
    ASM_LOAD_NULL;
}

/*
 * one step deref through the subsumption bindings of X
 * (WARNING: we assume X is a variable)
 */
inline
binding_t
once_s_deref(const fol_t X, const fkey_t k)
{
  binding_t binding = (binding_t) (FOL_SUBS_REF( X )) ;

  for (; binding ; binding= binding->next)
    if ( k == binding->binder_key ){
        ASM_LOAD(binding->bindee,binding->bindee_key,binding);
    } else if ( k > binding->binder_key ) {
        ASM_LOAD_NULL;
    }

  ASM_LOAD_NULL;
}

/*
 * one step deref through layer bindings
 * (WARNING : we assume X is a variable
 */
inline
binding_t
once_l_deref(const fol_t X, fkey_t k)
{
    const sbind_t sbinding = (sbind_t) (LAYER_ONCE_DEREF( X, k));
    if (sbinding) {
        SBINDING_K(k,sbinding);
        ASM_LOAD(sbinding->bindee,k,TAG_LAYER_BINDING(sbinding));
    } else
        ASM_LOAD_NULL;
}

/*
 * once step deref through layer and unification bindings
 * we look unification bindings first
 * (WARNING : we assume that X is a variable)
 */
binding_t
once_ul_deref( const fol_t X, fkey_t k)
{
    
    if (LAYER_IS_LOC_BOUND(k)) {
        binding_t binding = (binding_t) FOLVAR_UNIF_REF(FOL_DEREF_VAR(X));
        for (; binding ; binding= binding->next)
            if ( k == binding->binder_key ){
                ASM_LOAD(binding->bindee,binding->bindee_key,binding);
            } else if ( k > binding->binder_key ) {
                break;
            }
    }
    
    {
        const sbind_t sbinding = (sbind_t) LAYER_ONCE_DEREF(X,k);
        if (sbinding) {
            SBINDING_K(k,sbinding);
            ASM_LOAD(sbinding->bindee,k,TAG_LAYER_BINDING(sbinding));
        } else
            ASM_LOAD_NULL;
    }
}

/*
 * closure deref through layer and unification bindings
 * we look unification bindings first
 * (WARNING: we assume X is a variable)
 */
binding_t
closure_ul_deref(fol_t X, fkey_t k)
{
  fol_t _X = X;
  fkey_t _k = k;
  unsigned int n=0;
  
  STAT_UPDATE(deref,closure_ul);

  for (;;) {

      binding_t  binding = 0;

      STAT_UPDATE(deref,closure_ul_1);

    /* we deref into trail bindings from unif */
//    dyalog_printf("Layer loc bound %d\n",LAYER_IS_LOC_BOUND(k));
    if (!LAYER_IS_LOC_BOUND(k)) goto archive;
    
    binding = (binding_t) (FOL_UNIF_REF( X ));

    while ( binding ){
        fkey_t bk = binding->binder_key;
        if (k < bk) {
        	/* we follow the unification list of X */
            
            STAT_UPDATE(deref,closure_ul_4);
            binding= binding->next;
            
        } else if ( k == bk ){

            STAT_UPDATE(deref,closure_ul_2);
            
                /* An unif binding for X_k has been found */
            n++;
            X = binding->bindee;
            k = binding->bindee_key;

                /* if X is bound to a non-deref term => stop deref */
            if (!k || !(FOL_DEREFP( X ))) goto found;   
                /* X is bound to a deref term => restart unif deref with the new variable
                   and note that at least one deref was successul
                */
            if (!LAYER_IS_LOC_BOUND(k)) break;

            binding= (binding_t) (FOL_UNIF_REF( X ));
            
        } else {
            
            STAT_UPDATE(deref,closure_ul_3);
            break;
        }
        
    }

        /* when arriving here, binding should be null */
    archive:
    
    /* we deref into layer binding */
    {
        sbind_t sbinding = (sbind_t) (LAYER_ONCE_DEREF( X, k));
        if ( sbinding ) {
                /* a layer binding for X_k has been found */
            
            STAT_UPDATE(deref,closure_ul_5);
            
            n++;
            X =  sbinding->bindee;
            SBINDING_K(k,sbinding);
                /* if X is bound to a non-deref term => stop deref */
            if (!k || !FOL_DEREFP( X )) goto found;
            
        } else if (!n) {
            
            STAT_UPDATE(deref,closure_ul_7);
            
                /* X_k is not unif- or layer- bound and is the entry variable
                   => the dereferencing is not successul */
                /* ASM_LOAD(X,k); */
            ASM_LOAD_NULL;
            
        } else {
            
            STAT_UPDATE(deref,closure_ul_6);

      /* X_k is not unif- or layer- bound (but was reached by a dereferencing)
	 => we stop and update vX and vk */
            goto found;
        } 
    }
    
    /* X is layer-bound to a variable =>
       we start again the cycle looking the unification bindings
       and note that at least one dereferencing was successul  */
    
  }
  
found:
      /* successul dereferencing => update vX and vk */
  if (n>1) {
          /* we have followed several bindings => we rebind */
      TRAIL_UBIND(_X,_k,X,k);
  }
  ASM_LOAD(X,k,(binding_t) 1);

}

/**********************************************************************
 *  loop_deref
 *
 *  deref loop terms to get the term inside the loop
 *  we assume that A is a derefencable term
 **********************************************************************/

binding_t
loop_deref( SP(A) ) 
{
    binding_t binding = CLOSURE_UL_DEREF(A,Sk(A));
    Bool found= (Bool) binding;
    for(;FOLLOOPP(A); ) {
        found=1;
        A = FOLCMP_REF(A,2);
        Deref(A);
    }
    if (found) {
        ASM_LOAD(A,Sk(A),binding);
    } else {
        ASM_LOAD_NULL;
    }
}

/**********************************************************************
 *  collapse_insert
 *
 *  we try to find the entered layer below box->layer
 *      - by following next field of collapse box (increasing layer)
 *      - by decreasing layers until finding one entered
 **********************************************************************/

void
collapse_insert( collapse_t *ptr, collapse_t box )
{
  fkey_t layer = box->layer;

  for( ;
       *ptr &&  layer >= (*ptr)->layer  && !layer->collapse;
       ptr = &(*ptr)->next , layer--
       );

  if (layer->collapse)	/* decreasing wins */
    ptr = &(layer->collapse->next);

  box->next = *ptr;
  box->block_start = (*ptr && box->layer > (*ptr)->block_start) ? (*ptr)->block_start : box->layer;
  *ptr = box;
}

/**********************************************************************
 *   Untrailing
 **********************************************************************/

inline static void
clean_untrailed( TrailWord *new, TrailWord *old) 
{
    long *p;
    new++;
    old++;
    for(p=(long *)new; p < (long *)old; *p++=0);
//    bzero((void *)new,(old-new) * sizeof(TrailWord));
}

static void
untrail( const boxtype stop )
{
    TrailWord *old_top = C_TRAIL_TOP;
    TrailWord *top = old_top;
    boxtype type;
    Bool not_found_stop=1;
    
    while (not_found_stop && (type=TRAIL_TYPE(top)) >= stop){
        bind_t TrailBox = POP_TRAIL_BOX(top);
        switch (type) {
            case INDEXATION :
                LVALUE_IP = ((Resume) TrailBox)->prev;
                break;
            case LAYER :
                UNTRAIL_LAYER((layer_undo_t) TrailBox);
                break;
            case OVERWRITE :
                UNTRAIL_OVERWRITE((overwrite_t)TrailBox);
                break;
            case UBIND :
            case SBIND :
            case MASK  :
                UNTRAIL_BIND_OR_MASK((binding_t)TrailBox);
                break;
            case CGBIND :
                UNTRAIL_CGBIND((cgbinding_t)TrailBox);
                break;
            case COLLAPSE :
                UNTRAIL_COLLAPSE((collapse_t)TrailBox);
                LVALUE_C_TRAIL_COLLAPSE = (collapse_t) 0;
                break;
            case FORWARD:
                Untrail_Forward((UndoForward)TrailBox);
                break;
            case UPWARD:
                Untrail_Upward((UndoUpward)TrailBox);
                break;
            case REGISTERS:
                UNTRAIL_REGISTERS((struct registers_box *)TrailBox);
                break;
            case UNBIND:
                LVALUE_C_TRAIL_UNBIND   = (unbind_t) 0  ;
                break;
            case DISPLAY:
                UNTRAIL_DISPLAY((display_t)TrailBox);
                break;
            default :			/* RENAME COLLECT */
                break;
        }
        not_found_stop = (type!=stop);
    }
    clean_untrailed(top,old_top);
    LVALUE_C_TRAIL_TOP = top;
}


void
untrail_alt( TrailWord *stop)
{
    TrailWord *old_top = C_TRAIL_TOP;
    TrailWord *top = old_top;
    
    while (((TrailWord *)(*top)) > stop) {
        boxtype type = TRAIL_TYPE(top);
        bind_t TrailBox = POP_TRAIL_BOX(top);
        switch (type) {
            case INDEXATION :
                LVALUE_IP = ((Resume) TrailBox)->prev;
                break;
            case FORWARD:
                Untrail_Forward((UndoForward)TrailBox);
                break;
            case UPWARD:
                Untrail_Upward((UndoUpward)TrailBox);
                break;
            case LAYER :
                UNTRAIL_LAYER((layer_undo_t) TrailBox);
                break;
            case OVERWRITE :
                UNTRAIL_OVERWRITE((overwrite_t)TrailBox);
                break;
            case UBIND :
            case SBIND :
            case MASK  :
                UNTRAIL_BIND_OR_MASK((binding_t)TrailBox);
                break;
            case CGBIND :
                UNTRAIL_CGBIND((cgbinding_t)TrailBox);
                break;
            case COLLAPSE :
                UNTRAIL_COLLAPSE((collapse_t)TrailBox);
                LVALUE_C_TRAIL_COLLAPSE = (collapse_t) 0;
                break;
            case REGISTERS:
                UNTRAIL_REGISTERS((struct registers_box *)TrailBox);
                break;
            case UNBIND:
                LVALUE_C_TRAIL_UNBIND   = (unbind_t) 0  ;
                break;
            case DISPLAY:
                UNTRAIL_DISPLAY((display_t)TrailBox);
                break;
            default :			/* RENAME COLLECT */
                break;
        }
    }
    clean_untrailed(top,old_top);
    LVALUE_C_TRAIL_TOP = top;
}

/*
 * untrail_layer
 *      untrail everything on the trail stack above a layer box
 */
void
untrail_layer()
{
    untrail( LAYER );
}

/*
 * untrail_indexation
 *      untrail everything on the trail stack above an indexation box
 */
void
untrail_indexation()
{
    V_LEVEL_DISPLAY(V_LOW,"UNTRAILING INDEXATION\n");
    untrail( INDEXATION );
}

/**********************************************************************
 *   Untrailing the control stack
 **********************************************************************/

void
untrail_choice()
{
    V_LEVEL_DISPLAY(V_LOW,"Try backtrack %d\n",(long)R_B);
    LVALUE_C_CTL_TOP=((TrailWord *) R_B)-1;
    UNTRAIL_CHOICE(R_B);
}

void
follow_choice()
{
    V_LEVEL_DISPLAY(V_LOW,"Follow backtrack %d\n",(long)R_B);
//    C_CTL_TOP=((TrailWord *) R_B)-1;
//    UNTRAIL_CHOICE(R_B);
    FOLLOW_CHOICE(R_B);
}

/**********************************************************************
 *   Loading layer environements
 **********************************************************************/

DSO_LOCAL long layer_stat_void = 0, layer_stat_unvoid = 0;

/*
 * load_archive entry_key archive
 *      Load a layer archive on top of the current layer stack
 *      return the new entry_key in the stack
 */
fkey_t
push_layer_archive(const unsigned long k, obj_t l)
{
  long i;
  fkey_t top = LSTACK_TOP;
  fkey_t res = top + k;

  STAT_UPDATE(layer,call);

  // TRAIL_LAYER;

  if (NULLP( l ))
    LSTACK_PUSH(top,(vca_t) 0);
  else {
    for(; PAIRP( l ); l=CDR(l))
      if (INTEGERP( CAR( l ) )){

	STAT_UPDATE(layer,void_call);

	for (i=CINT(CAR( l )); i ; --i){
	  STAT_UPDATE(layer,void_push);
	  LSTACK_PUSH(top,(vca_t) 0);
	}
      
      } else {
      STAT_UPDATE(layer,push);
      LSTACK_PUSH(top, (vca_t) CREF( CAR( l )) );
      }}
  
  LVALUE_LSTACK_TOP = top;
  return res;
}

fkey_t
load_layer_archive(const unsigned long k, obj_t l)
{
//    TRAIL_LAYER();
    return push_layer_archive(k,l);
}


/**********************************************************************
 *   Using Variable Display Info
 **********************************************************************/

display_t
find_var_display( SP(X) )
{
    display_t p = C_TRAIL_DISPLAY;
    for( ; p && !(SFOL_EQ(X,Sk(X),p->var,p->key)); p = p->next );
    return p;
}

display_t
set_or_update_var_display( SP(X) )
{
    display_t box = find_var_display( X, Sk(X) );
    if (box) {
        if (box->status >= 0)
            box->status++;
        else
            box->status--;
    } else {
        box=TRAIL_DISPLAY( X, Sk(X) );
    }
    return box;    
}

