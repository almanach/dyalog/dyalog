/*************************************************************
 * $Id$
 * Copyright (C) 1997, 2003, 2008, 2009, 2010, 2011, 2014, 2015, 2016 Eric de la Clergerie
 * ------------------------------------------------------------
 *
 *    Hash -- Hash Table management  
 *
 * ------------------------------------------------------------
 * Description
 *    This file comes from the wamcc distribution
 *    (see following header)
 * ------------------------------------------------------------
 */

/*-------------------------------------------------------------------------*/
/* C Run-Time                                           Daniel Diaz - 1996 */
/* Hash table management                                                   */
/*                                                                         */
/* hash.c                                 Copyright (C) 1996, INRIA France */
/*-------------------------------------------------------------------------*/

#include <stdlib.h>
#include <string.h>

#include "bigloo.h"

#include "fol.h"
#include "hash.h"

/*---------------------------------*/
/* Constants                       */
/*---------------------------------*/

/*---------------------------------*/
/* Type Definitions                */
/*---------------------------------*/
 

/*---------------------------------*/
/* Global Variables                */
/*---------------------------------*/

/*---------------------------------*/
/* Function Prototypes             */
/*---------------------------------*/

/*-------------------------------------------------------------------------*/
/* A hash table consists of a header (tbl_size, elem_size, nb_elem) and a  */
/* table of tbl_size pointers to nodes.                                    */
/* Each node records a pointer to the next node, and a user element whose  */
/* size is elem_size. Each element must begin with the key (a long).       */
/*-------------------------------------------------------------------------*/

// *** WARNING: many functions are now inlined in hash.h, including rotate_hash
// and local_hash_fol
// modifications in this file have to be reflected in hash.h

inline static
unsigned long
rotate_hash(const unsigned long hash)
{
    return (hash ^ (hash >>10) ^ (hash >>20));
}

inline static
unsigned long
local_hash_fol(const fol_t a, const unsigned long size) 
{
    return ((unsigned long)rotate_hash((unsigned long)a)) & (size-1); 
}

inline static unsigned long
Hash_Find_Right_Size(unsigned long size) 
{
    return size;
}

/*-------------------------------------------------------------------------*/
/* HASH_LOCATE                                                             */
/*                                                                         */
/* This function returns the address of the pointer to the node associated */
/* to the key (if the pointer is NULL the key is not in the table).        */
/*-------------------------------------------------------------------------*/
inline static
HashNode *Hash_Locate(const char *tbl,const long key)
{
    HashNode *t = Hsh_Table(tbl)+local_hash_fol((fol_t)key,Tbl_Size(tbl));
    HashNode p;
        
    for(p=*t;p && p->key != key;t=&p->next,p=p->next);
    return t;
}

/*-------------------------------------------------------------------------*/
/* HASH_ALLOC_TABLE                                                        */
/*                                                                         */
/*-------------------------------------------------------------------------*/
char *Hash_Alloc_Table(const int tbl_size, const int elem_size)

{
        /* at least try 4 * buckets */
    unsigned long size = Hash_Find_Right_Size((tbl_size < 4) ? 4 : tbl_size); 
    HashNode *table = (HashNode *)GC_MALLOC_PRINTF( "hash node",size*sizeof(HashNode));
    HashTable tbl= (HashTable) GC_MALLOC_PRINTF( "hash table", HASH_STATIC_SIZE );
    if (tbl==NULL)
        return NULL;
    
    Tbl_Size(tbl) = size;
    Elem_Size(tbl)= elem_size;
    Nb_Elem(tbl)=0;
    Hsh_Table(tbl) = table;
    Tbl_First(tbl)=NULL;
    return (char *)tbl;
}

/*-------------------------------------------------------------------------*/
/* HASH_REALLOC_TABLE                                                      */
/*                                                                         */
/*-------------------------------------------------------------------------*/

inline
static void
Hash_Realloc_Table(const char *tbl)
{
    int       tbl_size=Tbl_Size(tbl);
    HashNode *t=Hsh_Table(tbl);
    HashNode *endt=t+tbl_size;
    HashNode  p,p1;
    HashNode *prev;
    unsigned long new_tbl_size = Hash_Find_Right_Size(tbl_size << 1);
    HashNode *new_t = (HashNode *)GC_MALLOC_PRINTF("hash node realloc", new_tbl_size*sizeof(HashNode));

    if (new_t==NULL) {
        return;                 /* error */
    }

    Hsh_Table(tbl) = new_t;
    Tbl_Size(tbl) = new_tbl_size;

    while (t<endt) {
        p=*t++;
        while(p){
            prev=Hash_Locate(tbl,p->key); 
            p1=p;
            p=p->next;
            *prev=p1;
            p1->next=NULL;
        }
    }
}

/*-------------------------------------------------------------------------*/
/* HASH_FREE_TABLE                                                         */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Hash_Free_Table(char *tbl)

{
 int       tbl_size=Tbl_Size(tbl);
 HashNode *t=Hsh_Table(tbl);
 HashNode *endt=t+tbl_size;
 HashNode  p;

 while(t<endt) {
     p=*t++;
     while(p){
         p=p->next;
     }
 }
}

/*-------------------------------------------------------------------------*/
/* HASH_DELETE_ALL                                                         */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void
Hash_Delete_All(const char *tbl)
{
    int       tbl_size=Tbl_Size(tbl);
    HashNode *t=Hsh_Table(tbl);
    HashNode *endt=t+tbl_size;

    while(t<endt) {
        *t++=NULL;
    }
    Nb_Elem(tbl)=0;
}

/*-------------------------------------------------------------------------*/
/* HASH_INSERT                                                             */
/*                                                                         */
/*-------------------------------------------------------------------------*/
char *
Hash_Insert(const char *tbl,const char *elem,const int replace)
{
    int       elem_size=Elem_Size(tbl);
    long      key= *(long *) elem;
    HashNode *prev;
    HashNode  p;

    prev=Hash_Locate(tbl,key);
    p=*prev;
    
    if (p==NULL)                                    /* the key does not exist */
    {
        p=(HashNode) GC_MALLOC_PRINTF("hash node insert",sizeof(struct hash_node)-sizeof(long)+elem_size);
        if (p==NULL)
            return NULL;
        
        p->next=NULL;
        p->linear=Tbl_First(tbl);
        if (Tbl_First(tbl))
            Tbl_First(tbl)->linear_prev=&(p->linear);
        Tbl_First(tbl)=p;
        p->linear_prev=&(Tbl_First(tbl));
        *prev=p;
        Nb_Elem(tbl)++;
        memcpy((char *) (&(p->key)),elem,elem_size);
        if ( Nb_Elem(tbl)> (Tbl_Size(tbl)/2) ) { /* realloc */
            Hash_Realloc_Table(tbl);
        }
    }
    else if (replace) {
        memcpy((char *) (&(p->key)),elem,elem_size);
    }
 
    return (char *) (&(p->key));
}

/*-------------------------------------------------------------------------*/
/* HASH_FIND                                                               */
/*                                                                         */
/*-------------------------------------------------------------------------*/

/*
char *
Hash_Find(const char *tbl, const long key)
{
 HashNode  p= * (Hash_Locate(tbl,key));
 return (p==NULL) ? NULL : (char *) (&(p->key));
}
*/

/*-------------------------------------------------------------------------*/
/* HASH_DELETE                                                             */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void
Hash_Delete(const char *tbl, const long key)
{
    HashNode *prev;
    HashNode  p;
    
    prev=Hash_Locate(tbl,key);
    p=*prev;
    
    if (p==NULL)                                    /* the key does not exist */
        return;
    
    *prev=p->next;
    if (p->linear)
        p->linear->linear_prev=p->linear_prev;
    *(p->linear_prev)=p->linear;
    Nb_Elem(tbl)--;
}

/*-------------------------------------------------------------------------*/
/* HASH_FIRST                                                              */
/*                                                                         */
/* Hash_First and Hash_Next make it possible to scan a hash table.         */
/* Example of use:                                                         */
/*                                                                         */
/*   HashScan scan;                                                        */
/*   char     *buff_ptr;                                                   */
/*                                                                         */
/*   for(buff_ptr=Hash_First(tbl,&scan);buff_ptr;buff_ptr=Hash_Next(&scan))*/
/*       Display_Element(buff_ptr);                                        */
/*-------------------------------------------------------------------------*/

/*
char *
Hash_First(const char *tbl, HashScan *scan)
{
    scan->current=(char *) (Tbl_First(tbl));
    return Hash_Next(scan);
}
*/

/*-------------------------------------------------------------------------*/
/* HASH_NEXT                                                               */
/*                                                                         */
/*-------------------------------------------------------------------------*/

 /*
char *
Hash_Next(HashScan *scan)
{
    HashNode  p = (HashNode) (scan->current);

    if (p==NULL)
        return NULL;
    else {
        scan->current=(char *) (p->linear);
        return (char *) (&(p->key));
    }
    
}
 */

/*-------------------------------------------------------------------------*/
/* HASH_TABLE_SIZE                                                         */
/*                                                                         */
/*-------------------------------------------------------------------------*/
int Hash_Table_Size(const char *tbl)
{
    return Tbl_Size(tbl);
}

/*-------------------------------------------------------------------------*/
/* HASH_NB_ELEMENTS                                                        */
/*                                                                         */
/*-------------------------------------------------------------------------*/
int Hash_Nb_Elements(const char *tbl)
{
    return Nb_Elem(tbl);
}

