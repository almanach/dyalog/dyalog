/************************************************************
 * $Id$
 * Copyright (C) 1996 Eric de la Clergerie
 * ------------------------------------------------------------
 *
 *   Set -- Tools for sets
 *
 * ------------------------------------------------------------
 * Description
 *
 * ------------------------------------------------------------
 */

#include "bigloo.h"

Bool
set_in(obj_t e, obj_t s)
{
  for (;PAIRP(s); s = CDR(s))
    if (CAR(s) == e) return 1;

  return 0;
}

obj_t
set_add( obj_t s, obj_t e )
{
  return (set_in(e,s)) ? s : MAKE_PAIR(e,s);
}

obj_t
set_union( obj_t s1, obj_t s2 )
{

  if (NULLP(s2)) return s1;

  for (; PAIRP(s1) ; s1 = CDR(s1))
    if (!set_in(CAR(s1),s2))
      s2 = MAKE_PAIR(CAR(s1),s2);

  return s2;
}
