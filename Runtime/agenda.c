/************************************************************
 * $Id$
 * Copyright (C) 1997, 2006, 2010, 2015, 2016 Eric de la Clergerie
 * ------------------------------------------------------------
 *
 *   Agenda -- Agenda
 *
 * ------------------------------------------------------------
 * Description
 *  The agenda is used to schedule the tasks to be done by DyALog
 *  
 * ------------------------------------------------------------
 */

#include "libdyalog.h"

typedef struct agenda_entry {
  tabobj_t obj;
  struct agenda_entry *next;
} *agenda_entry_t;

typedef struct agenda {
  long weight;
  agenda_entry_t entry;
  agenda_entry_t *anchor;
  struct agenda* next;
} *agenda_t;

static agenda_t agenda;
static agenda_entry_t last;

/*
static void
agenda_display()
{
  agenda_t ag = agenda;
  agenda_entry_t entry;

  printf( "**************************************************\n" );
  for( ; ag ; ag = ag->next ) {
    printf( "weight %ld:\n", ag->weight);
    for( entry = ag->entry; entry; entry = entry->next )
        dyalog_printf( "\t%&f\n", TABOBJ_MODEL( entry->obj ) );
  }
  printf( "**************************************************\n" );

}
*/

/**********************************************************************
 * Add
 **********************************************************************/
void
agenda_add( long w, tabobj_t o )
{
  agenda_t *p= &agenda;
  agenda_entry_t entry = (agenda_entry_t) GC_MALLOC_PRINTF("agenda entry", sizeof( struct agenda_entry ));
  
  entry->obj = o;

  for( ; *p && (*p)->weight < w ; p = &(*p)->next );

  if (*p && (*p)->weight == w) {
    *((*p)->anchor) = entry;
    (*p)->anchor = &entry->next;
  } else {
      agenda_t ag = (agenda_t) GC_MALLOC_PRINTF( "agenda", sizeof( struct agenda ) );
    ag->weight = w;
    ag->entry  = entry;
    ag->anchor = &entry->next;
    ag->next   = *p;
    *p = ag;
  }
}

/**********************************************************************
 * Add Last
 **********************************************************************/
void
agenda_add_last( tabobj_t o )
{
    agenda_entry_t entry = (agenda_entry_t) GC_MALLOC_PRINTF( "agenda entry", sizeof( struct agenda_entry ));
  entry->obj = o;
  entry->next = last;
  last = entry;
}

/**********************************************************************
 * next obj in agenda
 *   assume a non-empty agenda
 **********************************************************************/
tabobj_t
agenda_next()
{

  if (agenda) {
    agenda_entry_t entry = agenda->entry;
    tabobj_t o = entry->obj;
    
    if (entry->next)
      agenda->entry = entry->next;
    else
      agenda = agenda->next;
    
    return o;
  
  } else if (last) {
    tabobj_t o = last->obj;
    last = last->next;
    return o;
  } else
    return (tabobj_t) 0;

}
    
/**********************************************************************
 * empty agenda ?
 **********************************************************************/
Bool
agenda_empty_p()
{
  return (agenda == (agenda_t) 0) && (last == (agenda_entry_t) 0);
}

/**********************************************************************
 * Object Scheduling functions (were previously in object.c)
 **********************************************************************/

void
schedule()
{
  tabobj_t o = R_OBJECT;
  unsigned long weight = sfol_weight(o->seed->model,R_TRANS_KEY);
  o->examined = (Bool) 0;
  agenda_add(weight,o);
}

void
schedule_first()
{
  tabobj_t o = R_OBJECT;
  o->examined = (Bool) 0;
  agenda_add(0,o);
}

void
schedule_last()
{
  tabobj_t o = R_OBJECT;
  o->examined = (Bool) 0;
  agenda_add_last(o);
}


void                            /* Dyam Instruction */
Dyam_Schedule()
{
    V_LEVEL_DISPLAY( V_DYAM, "\tschedule object\n" );
    schedule();
}

void                            /* Dyam Instruction */
Dyam_Schedule_Last()
{
    V_LEVEL_DISPLAY( V_DYAM, "\tschedule last object\n" );
    schedule_last();
}

void                            /* Dyam Instruction */
Dyam_Now()
{
    V_LEVEL_DISPLAY( V_DYAM, "\tschedule first object\n" );
    schedule_first();
}

void
DyALog_Agenda_Reschedule(tabobj_t o) 
{
//    dyalog_printf("Rescheduled object %&f\n",o->seed->model);
    o->examined = (Bool) 0;
    agenda_add_last(o);
}

void
DyALog_Agenda_Delete_All()      /* Dyam Instruction */
{
    agenda=NULL;
    last=NULL;
}

