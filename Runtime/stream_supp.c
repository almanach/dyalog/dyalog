/*************************************************************
 * $Id$
 * Copyright (C) 1997, 2003, 2004, 2006, 2008, 2009, 2011, 2016, 2017, 2018 Eric de la Clergerie
 * ------------------------------------------------------------
 *
 *    Stream Support --
 *
 * ------------------------------------------------------------
 * Description
 *   Tools to handle Streams 
 *
 *    This file essentially comes from the wamcc distribution
 *    (see following header)
 * ------------------------------------------------------------
 */

/*-------------------------------------------------------------------------*/
/* Buit-In Predicates                                   Daniel Diaz - 1996 */
/* Stream support                                                          */
/*                                                                         */
/* stream_supp.c                          Copyright (C) 1996, INRIA France */
/*-------------------------------------------------------------------------*/


#include <stdio.h>
#include <stdlib.h>
/*@-skipposixheaders@*/
#include <unistd.h>
/*@=skipposixheaders@*/
#include <stdarg.h>
#include <string.h>

#include "hash.h"

#define STREAM_SUPP_FILE

#include "libdyalog.h"
#include "builtins.h"



// #include "../Linedit/linedit.h"

void fclearerr( FILE *stream )
{
  clearerr( stream );
}

/*---------------------------------*/
/* Constants                       */
/*---------------------------------*/

#define START_ALIAS_TBL_SIZE       128

#define BIG_BUFFER                 10240

          /* Error Messages */

#define ERR_ALLOC_FAULT            "stream_supp: Memory allocation fault"

#define ERR_TOO_MANY_STREAMS       "too many open streams (max:%d)"

#define ERR_TELL_OR_SEEK_UNDEFINED "fct tell or seek undefined\n"

/*---------------------------------*/
/* Type Definitions                */
/*---------------------------------*/

/*---------------------------------*/
/* Global Variables                */
/*---------------------------------*/

static fol_t  stream_1;

static fol_t  word_current_input_stream;
static fol_t  word_current_output_stream;

/*---------------------------------*/
/* Function Prototypes             */
/*---------------------------------*/

Declare_Object_Initializer(Stream_Supp_Initializer)

static
int       TTY_Getc              (TTYInf *tty);
static
void      TTY_Putc              (int c,TTYInf *tty);
static
void      TTY_Flush             (TTYInf *tty);
static
int       TTY_Close             (TTYInf *tty);
static
void      TTY_Clearerr          (TTYInf *tty);

/*-------------------------------------------------------------------------*/
/* STREAM_SUPP_INITIALIZER                                                 */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Stream_Supp_Initializer(void)

{
 StmProp prop;

 alias_tbl=Hash_Alloc_Table(START_ALIAS_TBL_SIZE,sizeof(AliasInf));
 if (alias_tbl==NULL)
     Fatal_Error(ERR_ALLOC_FAULT);

 stream_1=Functor_Arity("$stream",1);

 word_current_input_stream=Create_Atom("current_input_stream");
 word_current_output_stream=Create_Atom("current_output_stream");

                  /* this could be stream_c.c with an initializer but when */
                  /* executed we must be sure that Stream_Supp_Initializer */
                  /* has already been initialized. It is simpler like this */

 atom_user_input =Create_Atom("user_input");
 atom_user_output=Create_Atom("user_output");
 atom_user_error=Create_Atom("user_error");

 atom_read  =Create_Atom("read");
 atom_write =Create_Atom("write");
 atom_append=Create_Atom("append");

 atom_stream_position=Create_Atom("$stream_position");

 atom_bof    =Create_Atom("bof");
 atom_current=Create_Atom("current");
 atom_eof    =Create_Atom("eof");

 prop.mode      =STREAM_MODE_READ;
 prop.input     =TRUE;
 prop.output    =FALSE;
 prop.text      =TRUE;
 prop.reposition=FALSE;
 prop.eof_action=STREAM_EOF_ACTION_RESET;
 prop.tty       =isatty(fileno(stdin));

 stm_stdin=Add_Stream(atom_user_input,(long) stdin,prop,
                      NULL,NULL,NULL,STREAM_FCT_UNDEFINED,NULL,NULL,NULL);
 Add_Alias_To_Stream(atom_user_input,stm_stdin);
 stm_input=stm_stdin;

 prop.mode      =STREAM_MODE_APPEND;
 prop.input     =FALSE;
 prop.output    =TRUE;
 prop.text      =TRUE;
 prop.reposition=FALSE;
 prop.eof_action=STREAM_EOF_ACTION_RESET;
 prop.tty       =isatty(fileno(stdout));

 stm_stdout=Add_Stream(atom_user_output,(long) stdout,prop,
                       NULL,NULL,NULL,STREAM_FCT_UNDEFINED,NULL,NULL,NULL);
 Add_Alias_To_Stream(atom_user_output,stm_stdout);
 stm_output=stm_stdout;

 prop.mode      =STREAM_MODE_APPEND;
 prop.input     =FALSE;
 prop.output    =TRUE;
 prop.text      =TRUE;
 prop.reposition=FALSE;
 prop.eof_action=STREAM_EOF_ACTION_RESET;
 prop.tty       =isatty(fileno(stderr));
 stm_stderr=Add_Stream(atom_user_error,(long) stderr,prop,
                       NULL,NULL,NULL,STREAM_FCT_UNDEFINED,NULL,NULL,NULL);
 Add_Alias_To_Stream(atom_user_error,stm_stderr);
 stm_error=stm_stderr;


}

/*-------------------------------------------------------------------------*/
/* ADD_STREAM                                                              */
/*                                                                         */
/*-------------------------------------------------------------------------*/
int Add_Stream(fol_t atom_file_name,long file,StmProp prop,
               StmFct fct_getc,StmFct fct_putc,
               StmFct fct_flush,StmFct fct_close,
               StmFct fct_tell,StmFct fct_seek,
               StmFct fct_clearerr)

{
 int     stm;
 StmInf *pstm;
 TTYInf *tty;
 int     d;
 static
 StmFct  def[2][7]=
         { { (StmFct) fgetc,       (StmFct) fputc,
             (StmFct) fflush,      (StmFct) fclose,
             (StmFct) ftell,       (StmFct) fseek,
             (StmFct) fclearerr     } ,
           { (StmFct) TTY_Getc,    (StmFct) TTY_Putc,
             (StmFct) TTY_Flush,   (StmFct) TTY_Close,
             STREAM_FCT_UNDEFINED, STREAM_FCT_UNDEFINED,
             (StmFct) TTY_Clearerr,} };


 for(stm=0;stm<MAX_STREAM;stm++)
     if (stm_tbl[stm].file==0)
         break;

 if (stm>=MAX_STREAM)
     Fatal_Error(ERR_TOO_MANY_STREAMS,MAX_STREAM);

 if (stm>stm_last_used)
     stm_last_used=stm;

 if (prop.reposition && (fct_tell==STREAM_FCT_UNDEFINED ||
                         fct_seek==STREAM_FCT_UNDEFINED))
     Fatal_Error(ERR_TELL_OR_SEEK_UNDEFINED);

 pstm=stm_tbl+stm;

 pstm->atom_file_name=atom_file_name;
 if (prop.tty && prop.input)
    {
     if ((tty=(TTYInf *) malloc(sizeof(TTYInf)))==NULL)
         Fatal_Error(ERR_ALLOC_FAULT);

     pstm->file=(long) tty;
     tty->f=(FILE *) file;
//     tty->buff[0]='\0';
     tty->buff=NULL;
     tty->ptr=tty->buff;
     d=1;
    }
  else
    {
     pstm->file=file;
     d=0;
    }

 pstm->prop=prop;

 pstm->fct_getc    =(fct_getc)     ? fct_getc     : def[d][0];
 pstm->fct_putc    =(fct_putc)     ? fct_putc     : def[d][1];
 pstm->fct_flush   =(fct_flush)    ? fct_flush    : def[d][2];
 pstm->fct_close   =(fct_close)    ? fct_close    : def[d][3];
 pstm->fct_tell    =(fct_tell)     ? fct_tell     : def[d][4];
 pstm->fct_seek    =(fct_seek)     ? fct_seek     : def[d][5];
 pstm->fct_clearerr=(fct_clearerr) ? fct_clearerr : def[d][6];

 pstm->eof_reached =FALSE;
 PB_Init(pstm->pb_char)

 pstm->char_count  =0;
 pstm->line_count  =0;
 pstm->line_pos    =0;
 PB_Init(pstm->pb_line_pos)

 return stm;
}

/*-------------------------------------------------------------------------*/
/* REMOVE_STREAM                                                           */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Remove_Stream(int stm)

{
 StmInf  *pstm=stm_tbl+stm;
 StmProp  prop=pstm->prop;


 if (prop.tty && prop.input)
     free((char *)(pstm->file));

 Del_Aliases_Of_Stream(stm);


 stm_tbl[stm].file=0;

 if (stm==stm_last_used)
     stm_last_used--;
}

/*-------------------------------------------------------------------------*/
/* FIND_STREAM_BY_NAME                                                     */
/*                                                                         */
/*-------------------------------------------------------------------------*/
int Find_Stream_By_Name(fol_t atom_file_name)

{
 int stm;

 /*
 if (atom_file_name<0)
     return FALSE;
     */

 for(stm=0;stm<MAX_STREAM;stm++)
     if (stm_tbl[stm].atom_file_name==atom_file_name)
         return stm;

 return -1;
}

/*-------------------------------------------------------------------------*/
/* FIND_STREAM_BY_ALIAS                                                    */
/*                                                                         */
/*-------------------------------------------------------------------------*/
int Find_Stream_By_Alias(fol_t atom_alias)

{
 AliasInf *alias;

 alias=(AliasInf *) Hash_Find(alias_tbl,(long) atom_alias);

 return (alias==NULL) ? -1 : alias->stm;
}

/*-------------------------------------------------------------------------*/
/* ADD_ALIAS_TO_STREAM                                                     */
/*                                                                         */
/*-------------------------------------------------------------------------*/
Bool Add_Alias_To_Stream(fol_t atom_alias, int stm)

{
 AliasInf *alias;
 AliasInf  alias_info;

 alias=(AliasInf *) Hash_Find(alias_tbl, (long) atom_alias);
 if (alias!=NULL)
     return alias->stm==stm;         /* fail if assigned to another stream */

 /* *PB* Extend_Table_If_Needed(&alias_tbl); */

 alias_info.atom=atom_alias;
 alias_info.stm =stm;

 alias=(AliasInf *) Hash_Insert(alias_tbl,(char *) &alias_info,FALSE);
 if (alias==NULL)
     Fatal_Error(ERR_ALLOC_FAULT);

 return TRUE;
}

/*-------------------------------------------------------------------------*/
/* DEL_ALIASES_OF_STREAM                                                   */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Del_Aliases_Of_Stream(int stm)

{
 HashScan  scan;
 AliasInf *alias;
 
 for(alias=(AliasInf *) Hash_First(alias_tbl,&scan); alias;
     alias=(AliasInf *) Hash_Next(&scan))
     if (alias->stm==stm)
         Hash_Delete(alias_tbl, (long) alias->atom);
}

/*-------------------------------------------------------------------------*/
/* GET_STREAM_OR_ALIAS                                                     */
/*                                                                         */
/*-------------------------------------------------------------------------*/
int Get_Stream_Or_Alias( SP(sora_word), int test_mask)
{
 int      smt=0;                                  /* only for the compiler */

 Deref(sora_word);

 if (FOLVARP( sora_word ))
   return -1;

 if (FOLSMBP( sora_word ))
   smt=Find_Stream_By_Alias( sora_word); /* Alias */
 else if ( !FOLINTP( sora_word) ||
	   (smt = CFOLINT( sora_word )) < 0 ||
	   smt >= MAX_STREAM )
   return -1;

 if (smt<0 || stm_tbl[smt].file==0)
     return -1;

 if (test_mask==STREAM_NOTHING)
     goto ok;

 if (test_mask & STREAM_FOR_INPUT)
    {
     if (stm_tbl[smt].prop.input)
         goto ok;

     /* *PB* perm_oper=permission_operation_input; */
    }
  else
    {
     if (stm_tbl[smt].prop.output)
         goto ok;

     /* *PB* perm_oper=permission_operation_output; */

    }

 return -1;

ok:
 return smt;
}

/*-------------------------------------------------------------------------*/
/* CHECK_STREAM_TYPE                                                       */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Check_Stream_Type(int smt, int test_mask)
{
// fol_t   sora_word;

        /* NOTE: very strange function that does nothing ! Should check with
         * the similar function in GNU Prolog
         */
    
 if (test_mask & STREAM_FOR_TEXT)
    {
     if (stm_tbl[smt].prop.text)
         return;
    }
 else if (!stm_tbl[smt].prop.text)  /* STREAM_FOR_BINARY */
      return;
 
                                                 /* here there is an error */
/*
 if (test_mask & STREAM_FOR_INPUT)
    {
     sora_word=(last_input_sora==NOT_A_WAM_WORD)
                     ? word_current_input_stream
                     : last_input_sora;
    }
  else
    {
     sora_word=(last_output_sora==NOT_A_WAM_WORD)
                  ? word_current_output_stream
                  : last_output_sora;
    }
*/

 Fatal_Error("Stream permission");

}

/*-------------------------------------------------------------------------*/
/* The following functions replaces standard fgetc/... when the input is a */
/* TTY. It uses linedit to provide a more comfortable interface.           */
/* These functions should not be used directly but via the common interface*/
/* provided by the Stream_Getc/... functions (see below).                  */
/*-------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------*/
/* TTY_GETC                                                                */
/*                                                                         */
/*-------------------------------------------------------------------------*/

#if defined(HAVE_LIBREADLINE) && defined(HAVE_LIBHISTORY)
#include <readline/readline.h>
#include <readline/history.h>
static
char *my_readline(const char *prompt,FILE *stream)
{
    rl_instream=stream;
    return readline(prompt);
}
#else    /* Basic reading from TTY */
static void
add_history(const char *s) 
{
}
static
char *my_readline(const char *prompt,FILE *stream)
{
    char *buff=(char *)malloc(TTY_BUFFER_SIZE);
    fputs(prompt,stdout);
    return fgets(buff,TTY_BUFFER_SIZE,stream); 
}
#endif /* HAVE_LIBREADLINE */

static int TTY_Getc(TTYInf *tty)
{
 int     c;
 StmInf *pstm;

 if ( tty->buff == NULL){
     
     tty->buff=tty->ptr=my_readline("DyALog> ",tty->f);
     
         /* If the line has any text in it, save it on the history. */
     if (tty->buff && *tty->buff)
         add_history (tty->buff);
     
     if (tty->buff==NULL)
         return EOF;
     
         /* simulate the '\n' on the output */
     pstm=stm_tbl+stm_stdout;
     pstm->char_count++;
     pstm->line_count++;
     pstm->line_pos=0;
     
 }
 c=*(tty->ptr)++;
 if (c!='\0')
     return c;
 else {
     free(tty->buff);
     tty->ptr=tty->buff=(char *)NULL;
     return '\n';
 }
 

}

/*-------------------------------------------------------------------------*/
/* TTY_PUTC                                                                */
/*                                                                         */
/*-------------------------------------------------------------------------*/
static void TTY_Putc(int c,TTYInf *tty)
{
    fputc(c,tty->f);
}

/*-------------------------------------------------------------------------*/
/* TTY_FLUSH                                                               */
/*                                                                         */
/*-------------------------------------------------------------------------*/
static void TTY_Flush(TTYInf *tty)
{
 fflush(tty->f);
}

/*-------------------------------------------------------------------------*/
/* TTY_CLOSE                                                               */
/*                                                                         */
/*-------------------------------------------------------------------------*/
static int TTY_Close(TTYInf *tty)
{
 return fclose(tty->f);
}

/*-------------------------------------------------------------------------*/
/* TTY_CLEARERR                                                            */
/*                                                                         */
/*-------------------------------------------------------------------------*/
static void TTY_Clearerr(TTYInf *tty)
{
 clearerr(tty->f);
}

/*-------------------------------------------------------------------------*/
/* Only the following functions should be used to read/write a stream.     */
/*-------------------------------------------------------------------------*/


#define Before_Reading(pstm,file)						\
 if (pstm->eof_reached)								\
    {										\
     if (pstm->prop.eof_action==STREAM_EOF_ACTION_ERROR)			\
        Fatal_Error( "EOF reached" );						\
										\
     if (pstm->prop.eof_action==STREAM_EOF_ACTION_EOF_CODE)			\
         return EOF;								\
										\
                            /* here: eof_action==STREAM_EOF_ACTION_RESET */	\
     pstm->eof_reached=FALSE;							\
     if (pstm->fct_clearerr!=STREAM_FCT_UNDEFINED)				\
         (*pstm->fct_clearerr)(file);						\
    }

#define Update_Counters(pstm,c)                                             \
 pstm->char_count++;                                                        \
 if (c=='\n')                                                               \
    {                                                                       \
     pstm->line_count++;                                                    \
     pstm->line_pos=0;                                                      \
    }                                                                       \
  else                                                                      \
     pstm->line_pos++;

/*-------------------------------------------------------------------------*/
/* STREAM_GETC                                                             */
/*                                                                         */
/*-------------------------------------------------------------------------*/

int Stream_Getc(StmInf *pstm)
{
 int  c;
 long file=pstm->file;


 Before_Reading(pstm,file)

 if (!PB_Is_Empty(pstm->pb_char))
     PB_Pop(pstm->pb_char,c)
  else
     c=(*pstm->fct_getc)(file);

 if (c==EOF)
     pstm->eof_reached=TRUE;

 if (c=='\n')
     PB_Push(pstm->pb_line_pos,pstm->line_pos)

 Update_Counters(pstm,c)

 return c;
}

/*-------------------------------------------------------------------------*/
/* STREAM_UNGETC                                                           */
/*                                                                         */
/* Several issues should not occur except if more '\n' are unget than read */
/* (when a Stream_Set_Position() is used the number of read '\n' is 0).    */
/*-------------------------------------------------------------------------*/
void Stream_Ungetc(int c, StmInf *pstm)
{
 PB_Push(pstm->pb_char,c)
 pstm->eof_reached=FALSE;

 if (pstm->char_count>0)
     pstm->char_count--;                               /* should not occur */

 if (c=='\n')
    {
     if (pstm->line_count>0)
         pstm->line_count--;                           /* should not occur */

     if (!PB_Is_Empty(pstm->pb_line_pos))
         PB_Pop(pstm->pb_line_pos,pstm->line_pos)
      else
         pstm->line_pos=0;                             /* should not occur */
    }
  else
     if (pstm->line_pos>0)                             /* should not occur */
         pstm->line_pos--;
}

/*-------------------------------------------------------------------------*/
/* STREAM_PEEKC                                                            */
/*                                                                         */
/*-------------------------------------------------------------------------*/

int Stream_Peekc(StmInf *pstm)
{
 int  c;
 long file=pstm->file;


 Before_Reading(pstm,file)

 if (!PB_Is_Empty(pstm->pb_char))
     PB_Top(pstm->pb_char,c)
  else
    {
     c=(*pstm->fct_getc)(file);
     PB_Push(pstm->pb_char,c)
    }

 return c;
}

/*-------------------------------------------------------------------------*/
/* STREAM_GETS                                                             */
/*                                                                         */
/*-------------------------------------------------------------------------*/

char *Stream_Gets(char *str, int size, StmInf *pstm)
{
 int   c='\0';
 char *p=str;

 for(;;)
    {
     if (p-str>=size)
         break;

     c=Stream_Getc(pstm);

     if (c==EOF)
         break;

     *p++=c;

     if (c=='\n')
         break;
    }

 if (c==EOF && p==str)
     return NULL;

 *p='\0';
 return str;
}

/*-------------------------------------------------------------------------*/
/* STREAM_PUTC                                                             */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Stream_Putc(int c, StmInf *pstm)
{
 long file=pstm->file;

 (*pstm->fct_putc)(c,file);

 Update_Counters(pstm,c)
}

/*-------------------------------------------------------------------------*/
/* STREAM_PUTS                                                             */
/*                                                                         */
/*-------------------------------------------------------------------------*/
int Stream_Puts(char *str, StmInf *pstm)
{
 long  file=pstm->file;
 char *p;
 int   c;

 for(p=str;*p;p++)
    {
     c=*p;
     (*pstm->fct_putc)(c,file);                        /* like Stream_Putc */
     Update_Counters(pstm,c)
    }

 return p-str;
}

/*-------------------------------------------------------------------------*/
/* STREAM_PRINTF                                                           */
/*                                                                         */
/*-------------------------------------------------------------------------*/

int Stream_Printf(StmInf *pstm, char *format,...)
{
 long    file=pstm->file;
 va_list arg_ptr;
 static
 char    str[BIG_BUFFER];
 char   *p;
 int     c;


 va_start(arg_ptr,format);
 vsprintf(str,format,arg_ptr);
 va_end(arg_ptr);

 for(p=str;*p;p++)
    {
     c=*p;
     (*pstm->fct_putc)(c,file);                        /* like Stream_Putc */
     Update_Counters(pstm,c)
    }

 return p-str;
}

/*-------------------------------------------------------------------------*/
/* STREAM_FLUSH                                                            */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Stream_Flush(StmInf *pstm)
{
 long file=pstm->file;

 if (pstm->fct_flush && pstm->fct_flush!=STREAM_FCT_UNDEFINED)
     (*pstm->fct_flush)(file);
}

/*-------------------------------------------------------------------------*/
/* STREAM_CLOSE                                                            */
/*                                                                         */
/*-------------------------------------------------------------------------*/

int Stream_Close(StmInf *pstm)
{
 long file=pstm->file;

 if (pstm->fct_close!=STREAM_FCT_UNDEFINED)
     return (*pstm->fct_close)(file);

 return 0;
}

/*-------------------------------------------------------------------------*/
/* STREAM_END_OF_STREAM                                                    */
/*                                                                         */
/*-------------------------------------------------------------------------*/

int Stream_End_Of_Stream(StmInf *pstm)
{
 int c;

 if (pstm->prop.eof_action==STREAM_EOF_ACTION_RESET || !pstm->prop.input)
     return STREAM_EOF_NOT;

 if (pstm->eof_reached)
     return STREAM_EOF_PAST;

 c=Stream_Peekc(pstm);
 if (c==EOF)
     return STREAM_EOF_AT;

 return STREAM_EOF_NOT;
}

/*-------------------------------------------------------------------------*/
/* STREAM_GET_POSITION                                                     */
/*                                                                         */
/*-------------------------------------------------------------------------*/

void Stream_Get_Position(StmInf *pstm,int *offset,int *char_count,
                         int *line_count,int *line_pos)
{
 long file=pstm->file;

 *offset=0;
 if (pstm->fct_tell!=STREAM_FCT_UNDEFINED){
     if ((*offset=(*pstm->fct_tell)(file))<0)
         *offset=0;
      else
        {
         *offset=*offset-pstm->pb_char.nb_elems;
         if (*offset<0)
             *offset=0;
        }
 }
 
 *char_count=pstm->char_count;

 if (pstm->prop.text)
    {
     *line_count=pstm->line_count;
     *line_pos  =pstm->line_pos;
    }
  else
    {
     *line_count=0;
     *line_pos  =0;
    }
}

/*-------------------------------------------------------------------------*/
/* STREAM_SET_POSITION                                                     */
/*                                                                         */
/*-------------------------------------------------------------------------*/

int Stream_Set_Position(StmInf *pstm,int whence,int offset,int char_count,
                        int line_count,int line_pos)
{
 long file=pstm->file;
 int  x;

 x=(*pstm->fct_seek)(file,(long) offset,whence);
 if (x!=0)
     return x;

 pstm->char_count=char_count;

 if (pstm->prop.text)
    {
     pstm->line_count=line_count;
     pstm->line_pos  =line_pos;
    }

 if (pstm->eof_reached)
    {
     pstm->eof_reached=FALSE;
     if (pstm->fct_clearerr!=STREAM_FCT_UNDEFINED)
         (*pstm->fct_clearerr)(file);
    }

 PB_Init(pstm->pb_char)
 PB_Init(pstm->pb_line_pos)

 return 0;
}

/*-------------------------------------------------------------------------*/
/* STREAM_SET_POSITION_LC                                                  */
/*                                                                         */
/* Only the line count and the line position are given.                    */
/*-------------------------------------------------------------------------*/

int Stream_Set_Position_LC(StmInf *pstm,int line_count,int line_pos)
{
 long  file=pstm->file;
 int   x;
 int  *p;
 int   c;
 int   offset;
 Bool  save_eof_reached;
 int   save_char_count,save_line_count,save_line_pos;
 int   save_char_nb_elems;


 offset=(*pstm->fct_tell)(file);
 if (offset<0)
     return offset;

 x=(*pstm->fct_seek)(file,(long) 0,SEEK_SET);
 if (x!=0)
     return x;


 save_eof_reached  =pstm->eof_reached;
 save_char_count   =pstm->char_count;
 save_line_count   =pstm->line_count;
 save_line_pos     =pstm->line_pos;
 save_char_nb_elems=pstm->pb_char.nb_elems;


 pstm->char_count      =0;
 pstm->line_count      =0;
 pstm->line_pos        =0;
 pstm->pb_char.nb_elems=0;

 if (pstm->eof_reached)
    {
     pstm->eof_reached=FALSE;
     if (pstm->fct_clearerr!=STREAM_FCT_UNDEFINED)
         (*pstm->fct_clearerr)(file);
    }

 p=&(pstm->line_count);

 while(*p<line_count)
     if (Stream_Getc(pstm)==EOF)
         goto err;

 p=&(pstm->line_pos);

 while(*p<line_pos)
    {
     if ((c=Stream_Getc(pstm))==EOF)
         goto err;
     if (c=='\n')
         goto err;
    }


 PB_Init(pstm->pb_char)
 PB_Init(pstm->pb_line_pos)
 return 0;

err:

 pstm->eof_reached     =save_eof_reached;
 pstm->char_count      =save_char_count;
 pstm->line_count      =save_line_count;
 pstm->line_pos        =save_line_pos;
 pstm->pb_char.nb_elems=save_char_nb_elems;

 x=(*pstm->fct_seek)(file,(long) offset,SEEK_SET);
 if (x!=0)
     return x;

 return -2;
}

/**********************************************************************
 * Stream from sockets
 **********************************************************************/

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <netinet/in.h>
#include <netdb.h>

int
Sock_Getc( int sock )
{
  char c;
  int n=read(sock,&c,1);
  return (n > 0) ? (int) c : EOF;
}

int
Sock_Putc( char c, int sock )
{
  return write( sock, &c, 1) ? (int) c : 0;
}

void
Sock_Close( int sock )
{
  close(sock);
}

int
Open_Stream_From_Socket( int sock )
{
  StmProp prop;
  
  prop.mode       = STREAM_MODE_READ | STREAM_MODE_WRITE;
  prop.input      = TRUE;
  prop.output     = TRUE;
  prop.text       = TRUE;
  prop.reposition = FALSE;
  prop.eof_action = STREAM_EOF_ACTION_RESET;
  prop.tty        = isatty(sock);
  
  return Add_Stream( (fol_t) 0, (long) sock, prop,
		     (StmFct) Sock_Getc, (StmFct) Sock_Putc,
		     STREAM_FCT_UNDEFINED, (StmFct) Sock_Close,
		     NULL, NULL,
		     NULL );
}

int
server_create()
{  
  int sock;
  struct sockaddr_in adr;
  struct sockaddr * adr_p = (struct sockaddr *) &adr;
  socklen_t lg;
  if ( (sock = socket( AF_INET, SOCK_STREAM, 0 )) == -1 ){
    perror("socket");
    exit(2);
  }
  adr.sin_family = AF_INET;
  adr.sin_addr.s_addr=INADDR_ANY;
  adr.sin_port=3001;
  if ( bind(sock,adr_p,sizeof(adr)) == -1 ) {
    perror("bind");
    exit(2);
  }
  if (getsockname(sock,adr_p,&lg)) {
    perror( "name socket obtention" );
    exit(4);
  }
  fprintf(stderr,"DyALog: Service available on port %d\n",ntohs(adr.sin_port));
  listen(sock,5);
  return sock;
}

char *
in_addr2name( struct in_addr *addr )
{    
  struct hostent *host = gethostbyaddr((char *)addr,sizeof(struct in_addr),AF_INET);
  return host->h_name;
}

int 
server_accept( int server )
{
  int service_sock;
  struct sockaddr_in service_adr;
  struct sockaddr *service_adr_p = (struct sockaddr *) &service_adr;
  socklen_t service_lg;
  int stm;
  service_lg = sizeof( service_adr );
  service_sock = accept( server, service_adr_p, &service_lg );
  dyalog_printf( "New connection accepted for %s\n",  in_addr2name(&service_adr.sin_addr));
  stm = Open_Stream_From_Socket( service_sock );
  return stm;
}

int
stream2sock( int stm )
{
  return (int) ((stm_tbl+stm)->file);
}

void
Stream_Shutdown( int stm )
{
  shutdown(stream2sock(stm),2);
}


/**********************************************************************
 * Stream from Strings
 **********************************************************************/

#define STR_STREAM_WRITE_BLOCK 1024

typedef struct {
    char * ptr;
    char * buff;
    int    size;
} StrSInf;

static int
Str_Getc( StrSInf *str )
{
    int c=*(str->ptr);

    if (c=='\0')
        return EOF;

    (str->ptr)++;

    return (int)c;
}

static int
Str_Putc( char c, StrSInf *str )
{
    int size=str->ptr - str->buff;
    
    if (size >= str->size-1)         /* -1 for last '\0' */
    {
        int new_size=str->size+STR_STREAM_WRITE_BLOCK;

        str->buff=strncpy((char *) GC_MALLOC_ATOMIC(new_size),str->buff,size);
        str->size=new_size;
        str->ptr=str->buff+size;
    }
    *(str->ptr)++ = c;
    return (int)c;
}

int
DyALog_Open_String_Stream(char *buff)
{
    StmProp  prop;
    StrSInf *str = (StrSInf *)GC_MALLOC(sizeof(StrSInf));
    
    if ((buff)) {               /* stream open as input */
        str->buff=buff;
        str->size=0;
        str->ptr= buff;
        prop.mode     =STREAM_MODE_READ;
        prop.input    =TRUE;
        prop.output   =FALSE;
    } else {                    /* stream open as output */
        str->buff= (char *)GC_MALLOC_ATOMIC(STR_STREAM_WRITE_BLOCK);
        str->ptr = str->buff;
        str->size = STR_STREAM_WRITE_BLOCK;
        prop.mode     =STREAM_MODE_WRITE;
        prop.input    =FALSE;
        prop.output   =TRUE;
    }

    prop.text         =TRUE; 
    prop.tty          =FALSE;
    prop.reposition   =FALSE;
//    prop.buffering    =STREAM_BUFFERING_NONE;
    prop.eof_action   =STREAM_EOF_ACTION_EOF_CODE;
    prop.other        =0;

    return Add_Stream( (fol_t)0, (long) str, prop,
                       (StmFct) Str_Getc, (StmFct) Str_Putc,
                       STREAM_FCT_UNDEFINED, STREAM_FCT_UNDEFINED,
                       STREAM_FCT_UNDEFINED, STREAM_FCT_UNDEFINED,
                       STREAM_FCT_UNDEFINED );
}

char *
DyALog_Flush_String_Stream(int stm)
{
    StrSInf *str;
    
    str=(StrSInf *) (stm_tbl[stm].file);
    *(str->ptr)='\0';

    return str->buff;
}




