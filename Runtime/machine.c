/************************************************************
 * $Id$
 * Copyright (C) 1996, 2003, 2006, 2008 Eric de la Clergerie
 * ------------------------------------------------------------
 *
 *   Machine - Machine dependent file
 *
 * ------------------------------------------------------------
 * Description
 *
 * ------------------------------------------------------------
 */

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/param.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <pwd.h>
#include <ctype.h>
#include <string.h>
#include <unistd.h>

#include "config.h"
#include "machine.h"

#define __unix__

#ifdef __unix__

#include <sys/time.h>
#include <sys/resource.h>

#endif

/*---------------------------------*/
/* Global Variables                */
/*---------------------------------*/

static char     cur_work_dir[MAXPATHLEN];

/*-------------------------------------------------------------------------*/
/* INIT_MACHINE                                                            */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Init_Machine(void)

{
 getcwd(cur_work_dir,sizeof(cur_work_dir)-1);
}
/*-------------------------------------------------------------------------*/
/* M_SHELL                                                                 */
/*                                                                         */
/* Invokes a shell (eventually passing a cmd if != NULL)                   */
/*-------------------------------------------------------------------------*/
int M_Shell(char *cmd)

{
 char *env_var_name;
 char *default_path;
 char *before;
 char *option;
 char *after;
 char *shell_path;
 char  buff[1024];

#ifdef __unix__

 env_var_name="SHELL";
 default_path="/bin/sh";
 option="-c '";
 before="exec ";
 after="'";

#elif __dos__

 env_var_name="COMSPEC";
 default_path="command.com";
 option="/c ";
 before="";
 after="";

#else

 printf("Warning: Shell access not available\n");
 return -1;

#endif

 shell_path=getenv(env_var_name);
 if (shell_path==NULL)
     shell_path=default_path;

 if (cmd)
     sprintf(buff,"%s%s %s%s%s",before,shell_path,option,cmd,after);
  else
     sprintf(buff,"%s%s",before,shell_path);

 printf("\n<%s>\n",buff);

 return system(buff);
}

/*-------------------------------------------------------------------------*/
/* M_CPU_TIME                                                              */
/*                                                                         */
/* returns the user time used since the start of the process (in ms).      */
/*-------------------------------------------------------------------------*/
long M_Cpu_Time(void)

{
 long   usr_time;

#ifdef __unix__
 struct rusage rsr_usage;

 getrusage(RUSAGE_SELF,&rsr_usage);

 usr_time=(rsr_usage.ru_utime.tv_sec* 1000) +
          (rsr_usage.ru_utime.tv_usec/1000);

#else

 printf("Warning: User time not available\n");
 return 0;

#endif


 return usr_time;
}

/*-------------------------------------------------------------------------*/
/* M_SET_WORKING_DIR                                                       */
/*                                                                         */
/*-------------------------------------------------------------------------*/
Bool M_Set_Working_Dir(const char *path)

{
 char *new_path=M_Absolute_File_Name(path);

 if (new_path!=NULL && chdir(new_path)==0)
    {
     strcpy(cur_work_dir,new_path);
     return TRUE;
    }

 return FALSE;
}




/*-------------------------------------------------------------------------*/
/* M_GET_WORKING_DIR                                                       */
/*                                                                         */
/*-------------------------------------------------------------------------*/
char *M_Get_Working_Dir(void)

{
 return cur_work_dir;
}

/*-------------------------------------------------------------------------*/
/* M_ABSOLUTE_FILE_NAME                                                    */
/*                                                                         */
/* returns an absolute file name.                                          */
/*-------------------------------------------------------------------------*/
char *M_Absolute_File_Name_Alt( const char *path, const char *csrc)

{
 typedef char PathStr[MAXPATHLEN];

 static 
 PathStr  buff[2];
 int      res=0;
 char    *dst;
 char    *p,*q;
 char    *src = (char *) csrc;
 struct 
 passwd  *pw;
 
 dst=buff[res];
 while((*dst++=*src))                                   /* expand $VARNAME */
    {
     if (*src++=='$')
        {
         p=dst;
         while(isalnum(*src))
             *dst++=*src++;
         *dst='\0';
         q=getenv(p);
         if (q)
            {
             p--;
             strcpy(p,q);
             dst=p+strlen(p);
            }
        }
    }
 *dst='\0';

#ifndef __unix__
 for(src=buff[res];*src;src++)                        /* msdos \ becomes / */
     if (*src=='\\')
         *src='/';
#endif

 if (strcmp(buff[res],"user")==0)            /* prolog special file 'user' */
     return buff[res];


 if (buff[res][0]=='~'){
     if (buff[res][1]=='/')                            /* ~/... read $HOME */
        {
         if ((p=getenv("HOME"))==NULL)
             return NULL;

         sprintf(buff[1-res],"%s/%s",p,buff[res]+1);
         res=1-res;
        }
#ifdef __unix__
      else                                        /* ~user/... read passwd */
        {
         p=buff[res]+1;
         while(*p && *p!='/')
              p++;

         buff[res][0]=*p;
         *p='\0';
         if ((pw=getpwnam(buff[res]+1))==NULL)
             return NULL;

         *p=buff[res][0];

         sprintf(buff[1-res],"%s/%s",pw->pw_dir,p);
         res=1-res;
        }
#endif
 }
 

 if (buff[res][0]!='/')                                /* add path if src is relative */
    {
     sprintf(buff[1-res],"%s/%s",path,buff[res]);
     res=1-res;
    }

 res=1-res;
 src=buff[1-res];
 dst=buff[res];

 while((*dst++=*src))
    {
     if (*src++!='/')
         continue;

     p=dst;

collapse:
     while(*src=='/')                            /* collapse /////... as / */
         src++;
     
     if (*src!='.')
         continue;
     
     if (src[1]=='/' || src[1]=='\0')                       /* /./ removed */
     {
         src++;
         goto collapse;
     }

     if (src[1]!='.' || (src[2]!='/' && src[2]!='\0'))
         continue;
                                                           /* case /../ */
     src+=2;
     p=dst-2;
      while(p>=buff[res] && *p!='/')
          p--;

      if (p<buff[res])
          return NULL;

      dst=p;
    }

 if (dst[-2]=='/')
     dst[-2]='\0';                                        /* remove last / */
 
 return buff[res];
}

char *M_Absolute_File_Name(const char *src)
{
  return M_Absolute_File_Name_Alt( cur_work_dir, src );
}





