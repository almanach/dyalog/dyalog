/* $Id$
 * Copyright (C) 1997 Eric de la Clergerie
 * ------------------------------------------------------------
 *
 *   Buffer --
 *
 * ------------------------------------------------------------
 * Description
 *
 * ------------------------------------------------------------
 */


#ifndef _BUFFER_
#define _BUFFER_

#define BUFFER_SIZE 256

typedef struct buffer {
  unsigned long init;		/* Initial size */
  unsigned long length ;	/* Current maximal length of the buffer */
  unsigned long pos;		/* Current position in the buffer */
  char *buff;			/* the buffer string */
} *buffer;

extern buffer buffer_new(   unsigned long ); /* Create a buffer with an initial size */
extern void   buffer_push(  buffer, char );  /* Push a character at the end of the buffer */
extern char  *buffer_flush( buffer );        /* Return the string buffer and reset the buffer */
extern void   buffer_printf( buffer, const unsigned long, const char*, ... );
#endif /* _BUFFER_ */
