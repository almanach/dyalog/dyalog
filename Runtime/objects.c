/************************************************************
 * $Id$
 * Copyright (C) 1997, 2003, 2004, 2006, 2007, 2009, 2010, 2011, 2012, 2014, 2016, 2018 Eric de la Clergerie
 * ------------------------------------------------------------
 *
 *   DyALog objects --
 *
 * ------------------------------------------------------------
 * Description
 *
 * ------------------------------------------------------------
 */

//#define VERBOSE

#include "libdyalog.h"
#include "builtins.h"

#ifdef STAT

STAT_LIGHT_DEF(loop)		/* List of entries for loop stat in object.h */

#define V( entry )     STAT_USE(loop,entry)

static void stat_loop_nice_display()  __attribute__ ((destructor));

static void stat_loop_nice_display()
{
  printf(
	 
	 "--------------------------------------------------\n"
	  "STAT <execution>\n\n"
	  "Selections    : [all] %4d %4d [Std] %4d [Immediate] %5d [Last]\n"
	  "Applications run for %5d objects [%d direct + %d reverse]\n"
	  "    Delayed %6d and Tried %6d direct  applications\n"
	  "    Delayed %6d and Tried %6d reverse applications\n"
	  "    Tried %6d unifications whose %6d succeeded \n"
	  "Subsumptions run for %5d objects\n"
	  "    Tried %6d tests and kept %d objects\n"
	  "Creations     : %5d [= %5d items + %5d transitions + %5d answers ]\n"
	  "Tabulations   : %5d [= %5d items + %5d transitions + %5d answers ]\n"
	  "--------------------------------------------------\n" ,

				/* Line Selections */
         V( sl_selections ),
	 V( sl_schedule_std ),
	 V( sl_schedule_first ),
	 V( sl_schedule_last ),

				/* Line Applications */
	 V( sl_direct_applications ) + V( sl_reverse_applications ),
	 V( sl_direct_applications ),
	 V( sl_reverse_applications ),
				/* Line Try Direct */
	 V( sl_tried_direct_applications ) - V( sl_tried_direct_applications2 ),
	 V( sl_tried_direct_applications2 ),
				/* Line Try Reverse */
	 V( sl_tried_reverse_applications ) - V( sl_tried_reverse_applications2 ),
	 V( sl_tried_reverse_applications2 ),
				/* Line Try Unification */
	 V( sl_unifications ),
	 V( sl_succeded_unifications ),
				/* Line Subsumptions */
	 V( sl_subsumptions ), 
	 V( sl_tried_subsumptions ),
	 V( sl_subsumptions ) - V( sl_succeded_subsumptions ),
				/* Line Creations */
	 V( sl_items ) + V( sl_transitions ) + V( sl_answers ),
	 V( sl_items ),
	 V( sl_transitions ),
	 V( sl_answers ),
				/* Line Installations */
	 V( sl_installed_items ) + V(sl_installed_transitions ) + V( sl_installed_answers ),
	 V( sl_installed_items ),
	 V( sl_installed_transitions ),
	 V( sl_installed_answers )

	 );
}

#undef V

#endif

void DyALog_Backptr_Watcher( tabobj_t, obj_t);

extern tabobj_t agenda_next();
extern void agenda_add(long, tabobj_t);
extern void tabulation_add( tabobj_t );
extern void tabulation_add_aux( tabobj_t, void *);

DSO_LOCAL obj_t dyalog_solutions = BNIL;

DSO_LOCAL int keep_backptr=1;

/**********************************************************************
 * Constructors
 **********************************************************************/

void
save_object( tabseed_t seed )
{
    unsigned long k;
    obj_t env;
    Collapse_Unwrap(seed->model, R_TRANS_KEY, k, env);
    LVALUE_R_OBJECT = REG_VALUE(ALLOCATE_TABOBJ(seed,k,env,R_BACKPTR,R_LEVEL));
}

void
save_light_object( tabseed_t seed )
{
  LVALUE_R_OBJECT =  REG_VALUE(ALLOCATE_LIGHT_TABOJ(seed,R_BACKPTR,R_LEVEL));
}

tabobj_t
rt_install_database( SP(A) )
{
    fol_t type = find_folsmb("*DATABASE*",0);
    tabseed_t seed = ALLOCATE_TABSEED( -1, type, A, (obj_t) 0, 0);
    unsigned long  k;
    obj_t  env;
    tabobj_t obj;
    
    Collapse_Unwrap( A , Sk(A), k, env );
    obj = ALLOCATE_TABOBJ(seed,k,env,BINT( f_init ),0);
    tabulation_add( obj );
    return obj;
}

void
DyALog_Local_Add( void * table, sfol_t data) 
{
    fol_t type = find_folsmb("*DATABASE*",0);
    tabseed_t seed;
    unsigned long  k;
    obj_t  env;
    tabobj_t obj;
    SFOL_Deref(data);
    seed = ALLOCATE_TABSEED( -1, type, data->t, (obj_t) 0, 0);
    Trail;
    Collapse_Unwrap( data->t , data->k, k, env );
    obj = ALLOCATE_TABOBJ(seed,k,env,BINT( f_init ),0);
    tabulation_add_aux( obj, table );
}


void
Dyam_Forget_Current_Item()
{
    V_LEVEL_DISPLAY( V_DYAM,  "\tforget_current_item\n");
    LVALUE_R_ITEM = REG_VALUE(0);
}

/**********************************************************************
 * Small Run Time functions
 **********************************************************************/

void
build_backptr( unsigned long type, fol_t trace )
{
    obj_t item = (obj_t) R_ITEM;
    if (keep_backptr) {
        switch (type) {
            case 0:                 /* INIT */
                LVALUE_R_BACKPTR = REG_VALUE(BINT( (unsigned long) f_init ));
                break;
            case 1:                 /* CALL */
                LVALUE_R_BACKPTR = REG_VALUE(BINT( (unsigned long) f_call ));
                break;
            case 3: /* JOIN */
                LVALUE_R_BACKPTR = REG_VALUE(MAKE_PAIR(BINT( (unsigned long) f_join ), MAKE_PAIR( (obj_t) R_TRANS, (obj_t) R_ITEM )));
                break;
            case 4: /* REVERSE AND */
                LVALUE_R_BACKPTR = REG_VALUE(MAKE_PAIR(BINT( (unsigned long) f_rand ), MAKE_PAIR((obj_t) R_TRANS, (obj_t) R_ITEM )));
//            R_BACKPTR = MAKE_PAIR(BINT( (unsigned long) f_and ), MAKE_PAIR((obj_t) R_ITEM, (obj_t) R_TRANS ));
//                        R_BACKPTR = MAKE_PAIR(BINT( (unsigned long) f_and ), MAKE_PAIR((obj_t) R_TRANS, (obj_t) R_ITEM ));
                break;
            case 10:             /* indirect cases */
            case 11:
            case 12:
            case 13:
            case 14:
                build_backptr( type-10, (fol_t)0);
                LVALUE_R_BACKPTR = REG_VALUE(MAKE_PAIR(BINT( (unsigned long) f_indirect ),MAKE_PAIR(R_BACKPTR,BNIL)));
                break;
            default: /* 2 AND */
                V_LEVEL_DISPLAY(V_DYAM,"\tBACKPTR AND TRANS=%d ITEM=%d\n",(long)R_TRANS,(long)R_ITEM);
                if (R_TRANS && forest_indirect(R_TRANS->backptr))
                    item = (obj_t)0;
                LVALUE_R_BACKPTR = REG_VALUE(MAKE_PAIR(BINT( (unsigned long) f_and ), MAKE_PAIR( (obj_t) R_TRANS, item )));
        }
        if (trace && trace != FOLNIL) {
            LVALUE_R_BACKPTR= REG_VALUE(MAKE_PAIR( BINT( (unsigned long) f_trace),
                                                   MAKE_PAIR( (obj_t) trace, R_BACKPTR )));
        }
    }
}

/**********************************************************************
 * RT Treatement functions
 **********************************************************************/

extern void DyALog_Fail();

static void
treat_selected( tabobj_t o )
{
  fkey_t k = Trail_Object( o );
  fun_t code = (fun_t) o->seed->application_code;
  tabobj_t saved_trans = R_TRANS;
  fkey_t saved_trans_k = R_TRANS_KEY;

  LVALUE_R_CP= REG_VALUE(DyALog_Fail);
  
  STAT_UPDATE(loop, sl_selections );
  V_LEVEL_DISPLAY( V_DYAM, "--------------------------------------------------\n"
	     "%&e\n%&l\n" 
                   "Selected: <%x> [%&k] %&s\n", (void *) o, k, o->seed->model, k );

  V_LEVEL_DISPLAY( V_DYAM, "--------------------------------------------------\n"
                   "XSelected: <%x> [%&k] %&f\n", (void *) o, k, o->seed->model);

  
//  V_LEVEL_DISPLAY(V_DYAM,"\tR_ITEM=%&f\n",(fol_t) R_ITEM);
    
  o->examined = 2;		/* 2 is code for 'under selection' */

  if (code) {
      R_LOAD_TRANS(o,k);
      Dyam_DyALog(code);
      R_LOAD_TRANS(saved_trans, saved_trans_k);
  }
  
  o->examined = 1;		/* 1 is code for 'treated' */

  Untrail_Object;

  V_LEVEL_DISPLAY( V_DYAM, "Unloading Selected [%&e]\n" );

}

Bool
treat_item( tabobj_t item,
              long id,
              fol_t type,
              fun_t code,
              tabobj_t transition,
              fkey_t transition_key
              )
{
        void **tab = &item->seed->application_code+1;
        fol_t entry = ((item_comp_t) tab + id)->item_comp;

        STAT_UPDATE(loop, sl_tried_direct_applications );
        
        if ( item->examined && ( type == item->seed->type )   &&  entry ) {
            fun_t init_code = ((item_comp_t) tab + id)->init_code;
            fkey_t item_key = LOAD_OBJECT( item );
    
            STAT_UPDATE(loop, sl_tried_direct_applications2 );
            V_LEVEL_DISPLAY( V_DYAM, "     *try with %&s\n", item->seed->model, item_key );

            R_LOAD_ITEM( item, item_key, entry );
            R_LOAD_TRANS( transition, transition_key );
            LVALUE_R_P =  REG_VALUE((fun_t) Adjust_Address(code));
            if ((init_code))
                LVALUE_X(0) = REG_VALUE((fun_t) Adjust_Address(init_code));
            Succeed;
        } else
            Fail;
}

Bool
treat_transition( tabobj_t transition, long id, fol_t type)
{
    void **tab = &transition->seed->application_code+1;
    fun_t entry = ((trans_comp_t) tab + id)->code;
    
    STAT_UPDATE(loop, sl_tried_reverse_applications );
    
    if ( transition->examined  && ( type == transition->seed->type ) && entry ) {
        fkey_t transition_key = LOAD_OBJECT( transition );
        fun_t init_code = ((trans_comp_t) tab + id)->init_code;
        
        STAT_UPDATE(loop, sl_tried_reverse_applications2 );
        V_LEVEL_DISPLAY( V_DYAM, "     *try with %&s\n",
                         transition->seed->model, transition_key );
                
        R_LOAD_TRANS( transition, transition_key );
            
        LVALUE_R_P = REG_VALUE((fun_t) Adjust_Address(entry));            
        if ((init_code))
            LVALUE_X(0) = REG_VALUE((fun_t) Adjust_Address(init_code));
        
        Succeed;
        
    } else
        Fail;
}

#define B_TYPE( b )  (backptr_t) (PAIRP( b ) ? CINT( CAR( b )) : CINT( b ))
#define B_CAR( b )   (CAR( CDR( b )))
#define B_CDR( b )   (CDR( CDR( b )))

static
Bool
same_backptr(fol_t x,fol_t y)
{
    if (x==y)
        Succeed;
    if (!x || !y)
        Fail;
    if (B_TYPE(y) == f_or)
        return same_backptr(x,(fol_t)B_CAR(y))
            || same_backptr(x,(fol_t)B_CDR(y));
    if (B_TYPE(x) != B_TYPE(y))
        Fail;
    switch (B_TYPE(x)) {
        case f_init:
        case f_call:
            Succeed;
            break;
        case f_indirect:
            return same_backptr((fol_t)B_CAR(x),(fol_t)B_CAR(y));
            break;
        case f_trace:
            return B_CAR(x) == B_CAR(y) && same_backptr((fol_t)B_CDR(x),(fol_t)B_CDR(y));
            break;
        default:
            return B_CAR(x)==B_CAR(y) && B_CDR(x) == B_CDR(y);
            break;
    }
    Fail;
}


Bool
treat_generalizer( tabobj_t generalizer, fol_t model, fkey_t k_model, Bool subs_check )
{
  Bool v = 0;
  fkey_t generalizer_key = Trail_Object( generalizer );

  STAT_UPDATE(loop, sl_tried_subsumptions );
  V_LEVEL_DISPLAY( V_DYAM, "     *try generalizer %&s ",
                   generalizer->seed->model, generalizer_key);

  if ( (subs_check && sfol_subsume( generalizer->seed->model, generalizer_key,
                                      model, k_model ))
       ||
       (!subs_check && sfol_identical(generalizer->seed->model, generalizer_key,
                                      model, k_model) )
       ) {

    STAT_UPDATE(loop, sl_succeded_subsumptions );
    V_LEVEL_DISPLAY( V_DYAM, "( *** FOUND *** )\n" );
/*
    dyalog_printf( "found generalizer %&s %&s\n",
                   generalizer->seed->model, generalizer_key,
                   model, k_model
                   );
*/

    if (keep_backptr) {
        switch (B_TYPE(R_BACKPTR)) {
            case f_init:
            case f_call:
                
                    /* do nothing */
                
                break;

            case f_indirect:        /* assume that gen->backptr is also indirect */
                if (! same_backptr((fol_t)B_CAR(R_BACKPTR),(fol_t)B_CAR(generalizer->backptr)))
                  generalizer->backptr =
                      MAKE_PAIR( BINT((unsigned long) f_indirect),
                                     MAKE_PAIR( MAKE_PAIR( BINT( (unsigned long) f_or ),
                                                               MAKE_PAIR(
                                                               B_CAR(R_BACKPTR),
                                                                   B_CAR(generalizer->backptr ))),
                                                    BNIL));
                break;
                
            case f_trace:
                if (B_TYPE( generalizer->backptr ) == f_trace &&
                    B_CAR(R_BACKPTR) == B_CAR( generalizer->backptr )) {
                    if (B_CDR(R_BACKPTR) == R_BACKPTR) {
                        dyalog_printf("*** LOOP \n");
                    }
                    if (!same_backptr((fol_t)B_CDR(R_BACKPTR),(fol_t)B_CDR(generalizer->backptr)))
                        generalizer->backptr =
                            MAKE_PAIR( BINT( (unsigned long) f_trace ),
                                       MAKE_PAIR( B_CAR( R_BACKPTR),
                                                  MAKE_PAIR( BINT( (unsigned long) f_or ),
                                                             MAKE_PAIR(
                                                                 B_CDR(R_BACKPTR),
                                                                 B_CDR(generalizer->backptr
                                                                       )))));
                    break;
                } 

                    /* otherwise use default case */
                
            default:
                if (! same_backptr((fol_t)R_BACKPTR,(fol_t)generalizer->backptr))
                    generalizer->backptr =
                        MAKE_PAIR( BINT( (unsigned long) f_or ),
                                   MAKE_PAIR( R_BACKPTR, generalizer->backptr ));
                break;
        }
    }
    
    if (generalizer->level < R_LEVEL)
      generalizer->level = R_LEVEL;

    if (TABOBJ_WATCHED(generalizer)) {
        DyALog_Backptr_Watcher(generalizer,R_BACKPTR);
    }
    
    v = 1;
  } else {
      V_LEVEL_DISPLAY( V_DYAM, "\n" );
  }
  
  Untrail_Object;
  return v;
}

void
treat_answer()
{
  fol_t tuple  = FOLCMP_REF(R_OBJECT->seed->model,1);

  dyalog_solutions = MAKE_PAIR( (obj_t) R_OBJECT, dyalog_solutions );

  V_LEVEL_DISPLAY( V_DYAM, "----------------------------------------------------------------------\n" );

  dyalog_printf( "Answer:" );
  subst_display( tuple, R_TRANS_KEY );

  V_LEVEL_DISPLAY( V_DYAM, "----------------------------------------------------------------------\n" );
  
}

/**********************************************************************
 * Main DyALog Loop
 **********************************************************************/

DSO_LOCAL extern int should_stop;          /* declared in rt.c */

DSO_LOCAL void
dyalog_loop()
{
  tabobj_t o;
  V_LEVEL_DISPLAY( V_DYAM, "\n"
	     "--------------------------------------------------\n"
	     "Running ...\n"
	     "--------------------------------------------------\n\n"
	     );

  while (!should_stop && (o=agenda_next()))
    treat_selected( o );

}

/**********************************************************************
 * Dyam Instructions
 **********************************************************************/

void                            /* Dyam Instruction */
Dyam_Backptr(long Kind)
{
    V_LEVEL_DISPLAY( V_DYAM, "\tbuild backptr %d\n",Kind);
    build_backptr(Kind, (fol_t) 0);
}

void                            /* Dyam Instruction */
Dyam_Backptr_Trace(long Kind, fol_t Trace)
{
    fkey_t Sk(Trace)=R_TRANS_KEY;
    V_LEVEL_DISPLAY( V_DYAM, "\tbuild backptr with trace %d %&s\n",Kind,Trace,Sk(Trace));
    Deref(Trace);
    build_backptr(Kind,Trace);
}

void                            /* Dyam Instruction */
Dyam_Answer()
{
    V_LEVEL_DISPLAY( V_DYAM, "\tanswer\n");
    treat_answer();
}

void                            /* Dyam Instruction */
Dyam_Object( tabseed_t seed )
{
    V_LEVEL_DISPLAY( V_DYAM,"\tobject %&s\n",seed->model,R_TRANS_KEY);
    save_object(seed);
}

void                            /* Dyam Instruction */
Dyam_Light_Object( tabseed_t seed )
{
    V_LEVEL_DISPLAY( V_DYAM,"\tlight object %&s\n",seed->model,R_TRANS_KEY);
    save_light_object(seed);
}

extern void ItemComplete();

/* create a light item */
void
DyALog_Create_Item_Aux( SP(model), SP(comp) )
{
    tabseed_t seed;
    obj_t env;
    tabobj_t obj;
    unsigned long k;
    static void **app;
//    Deref(model);
//    Deref(comp);
    seed = ALLOCATE_TABSEED(0,0,model,&ItemComplete,1);
    seed->subs_comp=model;
    app=&seed->application_code;
    *++app=(void *)comp;
    *++app=(void *)model;
    *++app=(void *)0;
    Collapse_Unwrap(model,Sk(model),k,env);
    obj = ALLOCATE_TABOBJ(seed,k,env,BINT(f_init),0);
//    obj->examined=1;
    agenda_add(0,obj);
}

void
DyALog_Create_Item( sfol_t model, sfol_t comp )
{
    DyALog_Create_Item_Aux(model->t,model->k,comp->t,comp->k);
}

static fol_t watcher_model = (fol_t) 0;
static fol_t watcher_comp = (fol_t) 0;

/*
 * Var0 = ObjAddr
 * Var1 = BkPtr Type
 * Var2 = BkPtr Car
 * Var3 = BkPtr Cdr
 * Var4 = BkPtr Label
 * Var5 = BkPtr Indirect
 */


static void
backptr_watcher_fill( fkey_t k, obj_t backptr, long indirect )
{
    fol_t B;
    fol_t C;
    int type = B_TYPE( backptr );
    fol_t T = DFOLINT(type);
    obj_t car;
    obj_t cdr;

    switch (type) {
        case f_call:
        case f_init:
            Unify(FOLVAR_FROM_INDEX(1),k,T,Key0);
            Unify(FOLVAR_FROM_INDEX(2), k, DFOLINT(0), Key0 );
            Unify(FOLVAR_FROM_INDEX(3), k, DFOLINT(0), Key0 );
            Unify(FOLVAR_FROM_INDEX(5),k,DFOLINT(indirect),Key0);
            return;
        case f_and:
            car=B_CAR(backptr);
            cdr= B_CDR(backptr);
            B = POINTER_TO_DYALOG( car );
            C = POINTER_TO_DYALOG( cdr );
            if ( ((tabobj_t)car)
                 && ((tabobj_t)car)->backptr
                 && B_TYPE(((tabobj_t)car)->backptr) == f_trace
                 ) {
                Unify(FOLVAR_FROM_INDEX(4),k,(fol_t) B_CAR(((tabobj_t)car)->backptr),Key0);
            } else {
                Unify(FOLVAR_FROM_INDEX(4),k,FOLNIL,Key0);
            }
            Unify(FOLVAR_FROM_INDEX(1),k,T,Key0);
            Unify(FOLVAR_FROM_INDEX(2), k, B, Key0 );
            Unify(FOLVAR_FROM_INDEX(3), k, C, Key0 );
            Unify(FOLVAR_FROM_INDEX(5),k,DFOLINT(indirect),Key0);
            return;
        case f_trace:
            backptr_watcher_fill(k, B_CDR(backptr), indirect );
            return;
        case f_indirect:
            backptr_watcher_fill(k, B_CAR(backptr), 1 );
            return;
        default:
            return;
    }
}

void
DyALog_Backptr_Watcher( tabobj_t o, obj_t backptr) 
{
    if (backptr) {
        if (B_TYPE(backptr) == f_or) {
            DyALog_Backptr_Watcher(o,B_CAR(backptr));
            DyALog_Backptr_Watcher(o,B_CDR(backptr));
        } else {
            fkey_t k = LSTACK_PUSH_VOID;
            Unify(FOLVAR_FROM_INDEX(0),k,POINTER_TO_DYALOG(o),Key0);
            backptr_watcher_fill(k,backptr,0);
//            dyalog_printf("watcher o=%d %&s comp %&s\n",(unsigned long)o,watcher_model,k,watcher_comp,k);
            DyALog_Create_Item_Aux(watcher_model,k,watcher_comp,k);
        }
    }
}

void
DyALog_Assign_Backptr_Watcher( tabobj_t o)
{
    obj_t backptr = o ->backptr;
    if (!watcher_model) { /* initialize */
        fol_t watcher_smb = FOLSMB_CONVERT_ARITY(Create_Atom("$bk_watcher"),6);
        fol_t watcher_comp_smb = FOLSMB_CONVERT_ARITY(Create_Atom(":>"),2);
        fol_t voidret = Create_Atom("voidret");
        fol_t ritem = FOLSMB_CONVERT_ARITY(Create_Atom("*RITEM*"),2);
        FOLCMP_WRITE_START(watcher_smb,6);
        FOLCMP_WRITE(FOLVAR_FROM_INDEX(0));
        FOLCMP_WRITE(FOLVAR_FROM_INDEX(1));
        FOLCMP_WRITE(FOLVAR_FROM_INDEX(2));
        FOLCMP_WRITE(FOLVAR_FROM_INDEX(3));
        FOLCMP_WRITE(FOLVAR_FROM_INDEX(4));
        FOLCMP_WRITE(FOLVAR_FROM_INDEX(5));
        watcher_model = FOLCMP_WRITE_STOP;
        FOLCMP_WRITE_START(ritem,2);
        FOLCMP_WRITE(watcher_model);
        FOLCMP_WRITE(voidret);
        watcher_model = FOLCMP_WRITE_STOP;
        FOLCMP_WRITE_START(watcher_comp_smb,2);
        FOLCMP_WRITE(watcher_model);
        FOLCMP_WRITE(FOLVAR_FROM_INDEX(6));
        watcher_comp = FOLCMP_WRITE_STOP;
//        dyalog_printf("watcher model %&f comp %&f\n",watcher_model,watcher_comp);
    }
    TABOBJ_WATCHED_SET(o);
    DyALog_Backptr_Watcher(o,backptr);
}

    

    




