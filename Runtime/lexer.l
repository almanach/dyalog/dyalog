/*************************************************************
 * $Id$
 * Copyright (C) 1997 - 2002, 2004, 2006, 2008, 2009, 2010, 2014, 2017 Eric de la Clergerie
 * ------------------------------------------------------------
 *
 *   Fol Lexer
 *
 * ------------------------------------------------------------
 * Description
 *   Lexer for a PROLOG-like language
 * ------------------------------------------------------------
 */

%{
 
#include <math.h>
#include <stdarg.h>
#include <string.h>
#include <bigloo.h>
#include "buffer.h"
#include "token.h"
#include "libdyalog.h"
#include "builtins.h"

extern void dyalog_vfprintf( StmInf *stream, const char *, va_list);

#define YY_DECL TK_Kind lexer()	/* redefine the name of the lexer */
int yylex(){ return 0;}				/* dummy yylex */

				/* Set a stack of buffer (cf man of flex) */

#define MAX_INCLUDE_DEPTH 10
YY_BUFFER_STATE include_stack[MAX_INCLUDE_DEPTH];
static int include_stack_ptr = 0;

int lexer_mode = 0;		/* To select a new input port */
char *lexer_port;

//#define DEBUG_PARSE
 
void
Syntax_Error( const char *s, ...)
{
    StmInf *pstm = (StmInf *) yyin;
    char *file;
    va_list args;
    if (!pstm)
        file = "<string>";
    else 
        file = FOLSMB_NAME( pstm->atom_file_name );
        /* I do not why, but yylineno is always 1 less than the real error line
         * number ! */
    dyalog_error_printf( "Line %d of %s:\n\tSyntax Error : ",
                         yylineno+1,
                         file);
    va_start(args,s);
    dyalog_vfprintf(INT_TO_STM(stm_error),s,args);
    dyalog_error_printf( "\n" );
    exit(EXIT_FAILURE);
}

void
Syntax_Warning( const char *s, ...)
{
    StmInf *pstm = (StmInf *) yyin;
    char *file;
    va_list args;
    if (!pstm)
        file = "<string>";
    else 
        file = FOLSMB_NAME( pstm->atom_file_name );
        /* I do not why, but yylineno is always 1 less than the real error line
         * number ! */
    dyalog_error_printf( "Line %d of %s:\n\tSyntax Warning : ",
                         yylineno+1,
                         file);
    va_start(args,s);
    dyalog_vfprintf(INT_TO_STM(stm_error),s,args);
    dyalog_error_printf( "\n" );
}
 

				/* YY_INPUT from a Stream  */

#define YY_INPUT(buf,result,max_size)					\
{									\
  StmInf *pstm= (StmInf *) yyin;					\
  int c = Stream_Getc(pstm);						\
  result = (c == EOF) ? YY_NULL : (buf[0] = c, 1);			\
  yylineno = pstm->line_count;					  	\
}

//#define calloc(n,s) GC_MALLOC_ATOMIC((n)*(s))
//#define free(t) GC_FREE(t)
//#define free(t)
//#define malloc(s) GC_MALLOC_ATOMIC(s)


%}


LAYOUT       [ \n\t\r]
SYMB         [/.*:+\\^<>=~?��#@$���&`-]
SYMBA        [.*:+\\^<>=~?��#$@$���&`-]
SYMBB        [/.:+\\^<>=~?��#@$���&`-]
UNDERLINE    _
LOWACC       [����������������������ߵ�] 
UPACC        [�����������������������]
ALPHA        [[:alnum:]_]|{LOWACC}|{UPACC}
LOWER        [[:lower:]]|{LOWACC}
UPPER        [[:upper:]]|{UPACC}
MODULE       (({LOWER}{ALPHA}*|"/"{SYMBB}{SYMB}*|{SYMBA}{SYMB}*)"!")*


%x comment quoted string escape opfun
%option stack yylineno 8bit
%option noyywrap noreject
%option nounput noyy_top_state
/* %option debug */
%option always-interactive
%%

%{

  if (lexer_mode == -1) {
				/* should be used to explicitely free
				   the current buffer when associated
				   with a Stream */
      if (yyin)  /* at most one char in YY_CURRENT_BUFFER */
          Stream_Ungetc(input(),(StmInf *)yyin);
      
      if ( include_stack_ptr > 0){
          --include_stack_ptr;
          yy_delete_buffer( YY_CURRENT_BUFFER );
          yy_switch_to_buffer( include_stack[include_stack_ptr] );
      }
      yyterminate();
  }

  if (lexer_mode) {
    if ( include_stack_ptr >= MAX_INCLUDE_DEPTH )
      Syntax_Error( "Includes nested too deeply" );
    include_stack[include_stack_ptr++] =YY_CURRENT_BUFFER;
    switch(lexer_mode) {
    case 1:			/* input from Stream */
#ifdef DEBUG_PARSE
      printf("parsing from Stream\n");
#endif
      yyin = (FILE *)lexer_port;
      yy_switch_to_buffer( yy_create_buffer( yyin, YY_BUF_SIZE ) );
      yylineno = 1;
      break;
    case 2:			/* input from string */
#ifdef DEBUG_PARSE
      printf("parsing string %s\n", lexer_port);
#endif
      yy_scan_string(lexer_port);
      yyin = (FILE *) 0;
      yylineno = 1;
      break;
    default:			/* keep previous input */
      break;
    }
    lexer_mode=0;
  }
%}


{LAYOUT}+	/* eat spaces */

"%%%EOF".*$      {
                     if ( include_stack_ptr > 0 ) {
                         --include_stack_ptr;
                         yy_delete_buffer( YY_CURRENT_BUFFER );
                         yy_switch_to_buffer( include_stack[include_stack_ptr] );
                         /* yyterminate returns 0 == TK_EOF */ 
                         yyterminate();
                     } else {
                         yyterminate();
                     }
                  }

[%].*$		/* eat comments */


				/* punctuations */

"("              return TK_LPAR;
")"              return TK_RPAR;
"["              return TK_LBRACKET;
"]"              return TK_RBRACKET;
"{"              return TK_LBRACE;
"}"              return TK_RBRACE;
"|"              return TK_BAR;
","              return TK_COMA;

"."[ \t\r]+      return TK_DOT;
"."$             return TK_DOT;
".%".*$          return TK_DOT;


		/* Idents */

{LOWER}{ALPHA}*               |
[;/]                          |
"/"{SYMBB}{SYMB}*             |
{SYMBA}{SYMB}*         token_value = yycopy(); yy_push_state(opfun);

"!"             token_value = (char *)0; return TK_MODULE;

				/* Variables */

[[:upper:]_]{ALPHA}*   token_value = yycopy(); return TK_VAR;

				/* Chars */

0\'.              token_value = (yytext+2); return TK_CHAR;

				/* Numbers */


[1-9][[:digit:]]*\'[[:alnum:]]+ {
		  char **tail=&yytext;
		  int base = strtol(*tail,tail,0);
		  token_value = (char *) strtol((*tail)+1,tail,base);
		  if (**tail) Syntax_Error( "Uncorrect number" );
		  return TK_INT;
                  }

[[:digit:]]+      token_value = (char *) atol( yytext ); return TK_INT;

[[:digit:]]*\.[[:digit:]]+([eE][+-]?[[:digit:]]+)? |
[[:digit:]]+[eE][+-]?[[:digit:]]+   {
      token_value = (char *) BFLT(atof( yytext )); return TK_REAL; }

				/* multiline comments */

"/*/"     
"/*"              yy_push_state(comment);


<comment>{
      {LAYOUT}+
      [^*\n]+         /* eat anything that's not a '*'  */
      \*+[^*/\n]*   /* eat up '*'s not followed by '/'s */
      \*+"/"        yy_pop_state();
      <<EOF>>       Syntax_Error( "Unexpected End of File in multiline comment" );
}
				/* strings */

\"               yy_push_state(string);

<string>{
   \"{2}         buffer_push(auxbuf,'"');
   \"            yy_pop_state(); token_value = buffer_flush(auxbuf) ; return  TK_CHAR_LIST;
   [^"\\]+	 buffer_printf(auxbuf,yyleng,"%s",yytext);
   <<EOF>>       Syntax_Error( "Unexpected End of File in string" );
}

				/* quoted idents */

\'               yy_push_state(quoted);

<quoted>{
   \'{2}         buffer_push(auxbuf,'\'');
   \'            token_value = buffer_flush(auxbuf) ; BEGIN(opfun);
   [^'\\]+       buffer_printf(auxbuf,yyleng,"%s",yytext);
   <<EOF>>       Syntax_Error( "Unexpected End of File in 'quoted' ident" );
}

				/* escape */

<quoted,string>{
   \\n              buffer_push(auxbuf,'\n');
   \\t              buffer_push(auxbuf,'\t');
   \\r              buffer_push(auxbuf,'\r');
   \\b              buffer_push(auxbuf,'\b');
   \\[0-7]{3}     {                 /* octal escape sequence */
                     int result;
                     (void) sscanf( yytext + 1, "%o", &result );
                     if ( result > 0xff )  Syntax_Error( "char out of bound" );
                     buffer_push(auxbuf,result);
                  }
   \\X[0-9A-F]{2} {		    /* Hexa escape sequence */
                     int result;
                     (void) sscanf( yytext + 2, "%02X", &result );
                     if ( result > 0xff )  Syntax_Error( "char out of bound" );
                     buffer_push(auxbuf,result);
                  }
   \\\\             buffer_push(auxbuf,'\\');
   \\               buffer_push(auxbuf,'\\');
}

				/* op or functor */

<opfun>{
    "("            yy_pop_state(); return TK_FUNC;
    "{"            yy_pop_state(); return TK_FEAT_FUNC;
    "["            yy_pop_state(); return TK_FSET_FUNC;
    "!"            yy_pop_state(); return TK_MODULE;
    .|\n           yy_pop_state(); yyless(yyleng-1);  return TK_ATOM;
}

				/* EOF */


<<EOF>>            {
                     if ( include_stack_ptr > 0 ) {
                         --include_stack_ptr;
                         yy_delete_buffer( YY_CURRENT_BUFFER );
                         yy_switch_to_buffer( include_stack[include_stack_ptr] );
                         /* yyterminate returns 0 == TK_EOF */ 
                         yyterminate();
                     } else {
                         yyterminate();
                     }
                  }

%% 

