;;; $Id$
;;; ----------------------------------------------------------------
;;; Copyright (C) 1999, 2012 by INRIA 
;;; Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
;;; ----------------------------------------------------------------
;;;
;;;  boot.ma -- To go from C to DyALog and conversely
;;;
;;; ----------------------------------------------------------------
;;; Description
;;;	see Ma2Asm/MA_SYNTAX for a description of the mini-assembler
;;; ----------------------------------------------------------------
;;;	

c_code global C_To_DyALog
	pl_jump *

pl_code global DyALog_To_C
	call_c Dyam_Remove_Choice()
	c_ret

pl_code global DyALog_Fail
	pl_fail

pl_code global Schedule_Prolog
	call_c Dyam_Schedule_Prolog()
	pl_jump *

pl_code global Follow_Cont
	call_c	Dyam_Follow_Cont(R(0))
	fail_ret
	pl_jump *

;;; WARNING:	EVDLC Jan 17, 2000
;;; Apply and Complete should not be modified
;;; even if optimizations (inlining and last call) seem possible
;;; 1- Inlining of *_Loop
;;; the different alternatives of *_Loop no longer correctly returns
;;; to after *_Loop in Complete and Apply (instead, they returns to
;;; the position after the call to Complete or Apply)
;;; 2- Last Call: the functions returned by *_Loop should be evaluated
;;; in an env. modified by *_Loop and be in the same env.
	
pl_code local Apply_Loop
	call_c	Dyam_Apply(R(0),R(1))
	fail_ret
	pl_ret

pl_code global Apply
	call_c  Dyam_Allocate(2)
	pl_call Apply_Loop()
	pl_call *
	call_c  Dyam_Deallocate()
	pl_ret

pl_code local Complete_Loop
	call_c	Dyam_Complete(R(0),R(1))
	fail_ret
	pl_ret

pl_code global Complete
	call_c  Dyam_Allocate(2)
	pl_call Complete_Loop()
	pl_call *
	call_c  Dyam_Deallocate()
	pl_ret

pl_code global ItemComplete
	pl_jump Complete(0,0)
	
