/* $Id$
 * Copyright (C) 1997, 2009, 2012 Eric de la Clergerie
 * ------------------------------------------------------------
 *
 *   DyALog objects --
 *
 * ------------------------------------------------------------
 * Description
 *
 * ------------------------------------------------------------
 */

#ifndef OBJECTS_READ
#define OBJECTS_READ

/**********************************************************************
 * DyALog object types
 **********************************************************************/

typedef enum {
  obj_item,
  obj_hitem,
  obj_answer,
  obj_first,
  obj_curnext1,
  obj_database,
  obj_prolog_item,
  obj_prolog_curnext,
  obj_prolog_cont,
  obj_prolog_first,
  obj_wait_cont,
} dyalog_obj_t;

#ifdef STAT

STAT_DECL(loop,
	  sl_items,
	  sl_installed_items,
	  sl_transitions,
	  sl_installed_transitions,
	  sl_answers,
	  sl_installed_answers,
	  sl_unifications,
	  sl_succeded_unifications,
	  sl_subsumptions,
	  sl_tried_subsumptions,
	  sl_succeded_subsumptions,
	  sl_selections,
	  sl_direct_applications,
	  sl_reverse_applications,
	  sl_tried_direct_applications,
	  sl_tried_direct_applications2,
	  sl_tried_reverse_applications,
	  sl_tried_reverse_applications2,
	  sl_schedule_std,
	  sl_schedule_first,
	  sl_schedule_last
	  )

#define STAT_UPDATE_ON_TYPE( stat, type, item_case, trans_case, answer_case )	\
switch ((dyalog_obj_t) type) {							\
case obj_answer:								\
  STAT_UPDATE( stat, answer_case );						\
  break;									\
case obj_item:									\
case obj_hitem:									\
case obj_prolog_item:								\
  STAT_UPDATE( stat, item_case );						\
  break;									\
default:									\
  STAT_UPDATE( stat, trans_case );						\
  break;									\
}

#else

#define STAT_UPDATE_ON_TYPE( stat, type, item_case, trans_case, answer_case )

#endif /* STAT */

/**********************************************************************
 * TabSeed and TabObj Structures
 **********************************************************************/

struct tabseed {
  fol_t type;
  fol_t model;

  long id;

  fol_t subs_comp;

  unsigned long application_length;

  void *application_code;	/* the list of applications follows this entry */

};

typedef struct trans_comp {
    fol_t model;
    fun_t code;
    fun_t init_code;
} *trans_comp_t;

typedef struct item_comp {
    fol_t model;
    fol_t item_comp;
    fun_t init_code;
} *item_comp_t;

#define SIZE_OF_APPCOMP sizeof( struct trans_comp ) 
#define SIZE_OF_TABSEED sizeof( struct tabseed )

struct light_tabobj {
  tabseed_t seed;
  obj_t     backptr;
  int       level;
    int       bindex;
    int     kind;               /* to differentiate light_taboj from tabobj,
                                 * also used for extra info (watchers) */
};

struct tabobj {
  
				/* Light part */
    tabseed_t seed;
    obj_t     backptr;
    int       level;
    int       bindex;
    int     kind;
				/* Specific */
    unsigned long     key;
    obj_t             env;
    Bool              examined;
    void *            delete_info;
};

#define SIZE_OF_TABOBJ        sizeof( struct tabobj )
#define SIZE_OF_LIGHT_TABOBJ  sizeof( struct light_tabobj )

#define TABOBJ_KIND(o)        ((o->kind) & 1)
#define TABOBJ_WATCHED(o)     ((o->kind) & 2)
#define TABOBJ_WATCHED_MASK   2
#define TABOBJ_WATCHED_SET(o)     (o->kind |= TABOBJ_WATCHED_MASK)
#define TABOBJ_WATCHED_RESET(o)     (o->kind &= ~ TABOBJ_WATCHED_MASK)

/**********************************************************************
 *       TabSeeds
 **********************************************************************/

#define ALLOCATE_TABSEED( _id_, _type_, _mdl_, _app_, _napp_ )				\
({											\
    tabseed_t o = (tabseed_t) GC_MALLOC_PRINTF( "tabseed", SIZE_OF_TABSEED + _napp_ * SIZE_OF_APPCOMP ); \
  o->type   = _type_;									\
  o->model  = _mdl_;									\
  o->id     = _id_;									\
  o->subs_comp = _mdl_;									\
  o->application_length = _napp_;							\
  o->application_code = (void *) _app_;							\
  o;											\
})

/**********************************************************************
 *       TabObjects
 **********************************************************************/

#define ALLOCATE_TABOBJ( _seed_, _k_, _env_,_backptr_,  _level_ )       \
     ({                                                                 \
         tabobj_t o = (tabobj_t) GC_MALLOC_PRINTF( "tabobj", SIZE_OF_TABOBJ ); \
       o->seed = _seed_;                                                \
       o->key = _k_;                                                    \
       o->env = _env_;                                                  \
       o->examined = 1 ;                                                \
       o->backptr = _backptr_;                                          \
       o->level = _level_;                                              \
       o->bindex = -1;                                                  \
       o->kind = 1;                                                     \
       o;                                                               \
     })

#define ALLOCATE_LIGHT_TABOJ( _seed_, _backptr_,  _level_ )             \
     ({                                                                 \
         tabobj_t o = (tabobj_t) GC_MALLOC_PRINTF( "light tabobj", SIZE_OF_LIGHT_TABOBJ); \
       o->seed= _seed_;                                                 \
       o->backptr = _backptr_;                                          \
       o->level = _level_;                                              \
       o->bindex = -1;                                                  \
       o->kind = 0;                                                     \
       o;                                                               \
     })
	 
/**********************************************************************
 *       Tools
 **********************************************************************/

#define LOAD_OBJECT( o )   (fkey_t) load_layer_archive( (o)->key, (o)->env)
#define Trail_Object( o )   (Trail, LOAD_OBJECT(o))

#define LOAD_STARTER       load_layer_archive( 0, BNIL )

#define UNLOAD_OBJECT      
#define Untrail_Object      Untrail

#define TABOBJ_MODEL( obj )   (obj)->seed->model
#define TABOBJ_BACKPTR( obj ) (obj)->backptr
#define TABOBJ_SUBS( obj )    (obj)->seed->subs_comp
#define TABOBJ_LEVEL( obj)    (obj)->level

/**********************************************************************
 * Prototypes
 **********************************************************************/

extern tabobj_t rt_install_database( fol_t, fkey_t );

#endif /* OBJECTS_READ */
