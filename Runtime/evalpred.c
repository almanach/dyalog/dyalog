/************************************************************
 * $Id$
 * Copyright (C) 1997, 2002, 2003, 2004, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2017 Eric de la Clergerie
 * ------------------------------------------------------------
 *
 *   EvalPred -- Built-in Predicates
 *
 * ------------------------------------------------------------
 * Description
 *    Implementation of general built-in predicates
 *    Others more specific built-in predicates can be found in
 *    other files ( os in bi_os.c, stream in stream_c.c, ...)
 * ------------------------------------------------------------
 */
#include <string.h>
#include <errno.h>
#include <math.h>
#include "buffer.h"
#include "token.h"
#include "libdyalog.h"
#include "builtins.h"


static char buff[4096];

static Bool String_To_Chars(char *, Sproto );
static int Chars_To_String(char *, Sproto );

/**********************************************************************
 * Macros
 **********************************************************************/

  
#define D_Arg(n)      FOLCMP_REF(t,n), k_t

#define Decl_Wrapper(_wrapper, _installer, _args... )	\
void							\
_wrapper##_Installer_Wrapper( handler, o, k )		\
     obj_t handler;					\
     obj_t o;						\
     obj_t k;						\
{							\
  fol_t t = (fol_t) o;					\
  fkey_t k_t = (fkey_t) k;				\
  _installer( _args );				\
  }

/**********************************************************************
 * evpred_functor
 **********************************************************************/


static Bool evpred_functor(Sproto,Sproto,Sproto);

Bool              /* DyALog Wrapper */
DYAM_evpred_functor(fol_t term,fol_t functor,fol_t arity)
{
    fkey_t k=R_TRANS_KEY;
    return evpred_functor(term,k,functor,k,arity,k);
}

static Bool	  /* DyALog Builtin */
evpred_functor( SP(term), SP(functor), SP(arity) )
{
  long c_arity;
  Sdecl( new );

  
  Deref(term);
  
  if (FOL_CSTP(term))		/* functor(+c,?c,?0) */
    return Get_Cst(term,functor) && Get_Cst( DFOLINT(0), arity );
  
  if (FOLCMPP(term)) {		/* functor(+f(a1..aN),?f,?N) */
    fol_t term_arity = DFOLINT( FOLCMP_ARITY(term) );
    fol_t term_functor  = Convert_To_Atom( FOLCMP_FUNCTOR(term) );
    return Get_Cst(term_functor,functor) && Get_Cst( term_arity, arity);
  } 

  Deref(functor);		/* functor must be a constant */
  if (FOLVARP(functor) || FOLCMPP(functor)) Fail;

  Deref(arity);			/* arity must be a positive integer */
  
  if ( !FOLINTP( arity ) || (c_arity=CFOLINT(arity)) < 0 ) Fail;

  if (FOL_ICSTP( functor ))	/* functor(?c,+c,?0) */
    return c_arity == 0 && ( Bind( functor, Sk(functor) , term ) , True );
    
  new = fol_create_generic_term( functor, c_arity );

  Sk(new) = LSTACK_PUSH_VOID;
 
  Bind( new, Sk(new) , term );
    
  Succeed;

}

/**********************************************************************
 * evpred_arg
 **********************************************************************/


static Bool evpred_arg(Sproto,Sproto,Sproto);

Bool              /* DyALog Wrapper */
DYAM_evpred_arg(fol_t indice,fol_t term,fol_t arg)
{
    fkey_t k=R_TRANS_KEY;
    return evpred_arg(indice,k,term,k,arg,k);
}

static Bool	  /* DyALog Builtin */
evpred_arg( SP(indice), SP(term), SP(arg))
{
  long i;

  Deref( indice );
  
  if (!FOLINTP( indice ) || ((i = CFOLINT( indice )) < 1)) Fail;
  
  Deref( term );
  
  if (!FOLCMPP( term ) || (i > FOLCMP_ARITY( term ))) Fail;
  
  return Unify( arg, k_arg, FOLCMP_REF(term,i), k_term );
}

/**********************************************************************
 * evpred_univ  =..
 **********************************************************************/


static Bool evpred_univ(Sproto,Sproto);

Bool              /* DyALog Wrapper */
DYAM_evpred_univ(fol_t term,fol_t list)
{
    fkey_t k=R_TRANS_KEY;
    return evpred_univ(term,k,list,k);
}

static Bool	  /* DyALog Builtin */
evpred_univ( SP(term), SP(list) )
{
  fol_t *tail = (fol_t *) 0;
  long   length = 0;
  fol_t res = FOLNIL;
  Sdecl( functor );
  long arity;
  long var_index;
  Sdecl( new );

  Deref(term);
  
  if (FOLVARP(term)) goto list_to_term;
  
  if (FOLCMPP(term)) {
    length = FOLCMP_ARITY( term );
    tail   = &FOLCMP_REF( term, 1 );
    term   = Convert_To_Atom( FOLCMP_FUNCTOR( term ));
  }

  for (;;) {
    Deref( list );

    if (FOLVARP( list )) {
      for ( tail += length;
	    length ;
	    res = Create_Pair( *--tail, res ), length-- );
      res = Create_Pair( term, res );
      return Unify( list, k_list, res, k_term );
    }
    
    if (!FOLPAIRP( list ) || !Unify( term, k_term, FOLPAIR_CAR( list ), k_list))
      Fail;

    list = FOLPAIR_CDR( list );
    
    if (length==0)
      return Get_Nil( list );
    
    length--;
    term = *tail++;
  }
  
list_to_term:
  
  Deref( list );

  if (!FOLPAIRP(list)) Fail;
  
  Sassign(functor,FOLPAIR_CAR( list ),k_list);
  Deref(functor);

  if (!FOL_CSTP( functor )) Fail;

  list = FOLPAIR_CDR( list );
  Deref( list );

  if (FOLNILP( list ))
    return Unify( term, k_term, functor, Key0 );

  if (FOL_ICSTP( functor )) Fail;

  k_new = LSTACK_PUSH_VOID;

  FOLCMP_WRITE_LIGHT_START;
  arity = 0;
  var_index = 0;

  for(;!FOLNILP(list);arity++) {
    
    if (!FOLPAIRP( list )) {
      FOLCMP_WRITE_ABORT;
      Fail;
    }

    new = FOLVAR_FROM_INDEX( var_index++ );
    Bind( FOLPAIR_CAR( list ), k_list , new );
    FOLCMP_WRITE( new );

    list = FOLPAIR_CDR( list );
    Deref( list );
  }
  
  new = FOLCMP_WRITE_LIGHT_STOP( FOLSMB_CONVERT_ARITY( functor,arity ), arity );
  return Unify( term, k_term, new, k_new );

}

/**********************************************************************
 * evpred_length
 **********************************************************************/


static Bool evpred_length(Sproto,Sproto);

Bool              /* DyALog Wrapper */
DYAM_evpred_length(fol_t term,fol_t number)
{
    fkey_t k=R_TRANS_KEY;
    return evpred_length(term,k,number,k);
}

static Bool	  /* DyALog Builtin */
evpred_length( SP(term), SP(number) )
{
  Bool flag = 0;
  long    n = 0;
  long   length = 0;
  long    i;
  Sdecl( new );

  Deref(number);

  if (FOLINTP(number)) {
    flag = 1;
    length = CFOLINT( number );
  }

  for (;; term = FOLPAIR_CDR(term) , n++) {
    Deref( term );

    if (FOLNILP(term))
      return Get_Integer( DFOLINT(n), number );

    if (FOLVARP(term) && flag)
      break;

    if (!FOLPAIRP(term) || ( flag && n > length )) Fail;
  }

  /* term is a variable and n <= length */

  for( length -= n, new = FOLNIL, i=0 ;
       length ;
       new=Create_Pair( FOLVAR_FROM_INDEX( i++ ), new ), length-- );

  Sk(new) = LSTACK_PUSH_VOID;

  Bind( new, Sk(new), term );
  
  Succeed;

}

/**********************************************************************
 * Make_List
 **********************************************************************/


static Bool Make_List(Sproto,Sproto,Sproto);

Bool              /* DyALog Wrapper */
DYAM_Make_List(fol_t list,fol_t number,fol_t term)
{
    fkey_t k=R_TRANS_KEY;
    return Make_List(list,k,number,k,term,k);
}

static Bool	  /* DyALog Builtin */
Make_List( SP(list), SP(number), SP(term) )
{
  Bool flag = 0;
  long    n = 0;
  long    length = 0;
  fol_t  new;

  Deref(number);

  if (FOLINTP(number)) {
    flag = 1;
    length = CFOLINT( number );
  }

  for (;; list = FOLPAIR_CDR(list) , n++) {
    Deref( list );

    if (FOLNILP(list))
      return Get_Integer( DFOLINT(n), number );

    if (FOLVARP(list) && flag)
      break;

    if (!FOLPAIRP(list) || ( flag && n > length )) Fail;

    if (! Unify( FOLPAIR_CAR( list ), k_list, term, k_term )) Fail;
    
  }

  /* term is a variable and n <= length */

  for( length -= n, new = FOLNIL ;
       length ;
       new=Create_Pair( term , new ), length-- );

  Bind( new, Sk(term) , list );
  
  Succeed;

}

/**********************************************************************
 * Make_Interval
 **********************************************************************/


static Bool Make_Interval(Sproto,Sproto,Sproto);

Bool              /* DyALog Wrapper */
DYAM_Make_Interval(fol_t list,fol_t start,fol_t end)
{
    fkey_t k=R_TRANS_KEY;
    return Make_Interval(list,k,start,k,end,k);
}

static Bool	  /* DyALog Builtin */
Make_Interval( SP(list), SP(start), SP(end) )
{
  long    s,e;
  fol_t new;
  
  Deref(start);
  Deref(end);

  if ( !FOLINTP(start) || !FOLINTP(end)) Fail;

  s = CFOLINT( start );
  e = CFOLINT( end );
  
  for (;; list = FOLPAIR_CDR(list) , s++) {
    Deref( list );

    if (FOLNILP(list))
      return (s > e);
    
    if (FOLVARP(list))
      break;
    
    if (!FOLPAIRP(list) || ( s > e )) Fail;

    if (! Unify( DFOLINT(s), Key0, FOLPAIR_CAR( list ), k_list )) Fail;
  
  }

  /* term is a variable and s <= e */

  for( new = FOLNIL ;
       s <= e ;
       new=Create_Pair( DFOLINT(e--) , new ));

  Bind( new, Key0 , list );
  
  Succeed;

}

/**********************************************************************
 * evpred_copy_term
 **********************************************************************/


static Bool evpred_copy_term(Sproto,Sproto);

Bool              /* DyALog Wrapper */
DYAM_evpred_copy_term(fol_t term,fol_t copy)
{
    fkey_t k=R_TRANS_KEY;
    return evpred_copy_term(term,k,copy,k);
}

static Bool	  /* DyALog Builtin */
evpred_copy_term( SP(term), SP(copy) )
{
  obj_t env;
  unsigned long k;
  fkey_t Sk(new);

  Collapse_Unwrap( term, Sk(term), k, env);
  Sk(new) = (fkey_t) load_layer_archive(k,env);
  
  return Unify( term, Sk(new), copy, Sk(copy) );
}

/**********************************************************************
 * evpred_atom_chars
 **********************************************************************/


static Bool evpred_atom_to_chars(Sproto,Sproto);

Bool              /* DyALog Wrapper */
DYAM_evpred_atom_to_chars(fol_t atom,fol_t list)
{
    fkey_t k=R_TRANS_KEY;
    return evpred_atom_to_chars(atom,k,list,k);
}

static Bool	  /* DyALog Builtin */
evpred_atom_to_chars( SP(atom), SP(list) )
{
  Deref(atom);
  
  if (FOLSMBP(atom))
    return String_To_Chars( SymbolName( atom ), S(list) );
  
  if (!FOLVARP(atom)) Fail;
  
  if (!Chars_To_String( buff, S(list) )) Fail;

  Bind( Create_Atom(buff), Key0, atom );
  
  Succeed;
}
/**********************************************************************
 * evpred_atom_module
 **********************************************************************/


static Bool evpred_atom_to_module(Sproto,Sproto,Sproto);

Bool              /* DyALog Wrapper */
DYAM_evpred_atom_to_module(fol_t atom_with_module,fol_t module,fol_t atom)
{
    fkey_t k=R_TRANS_KEY;
    return evpred_atom_to_module(atom_with_module,k,module,k,atom,k);
}

static Bool	  /* DyALog Builtin */
evpred_atom_to_module( SP(atom_with_module), SP(module), SP(atom) )
{
  Deref(atom_with_module);
  
  if (FOLSMBP(atom_with_module)) {
      return Unify(module,Sk(module),FOLSMB_MODULE(atom_with_module),Key0)
          && Unify(atom,Sk(atom),Create_Atom(FOLSMB_NAME(atom_with_module)),Key0);
  }

  Deref_And_Fail_Unless(module,FOLSMBP(module));
  Deref_And_Fail_Unless(atom,FOLSMBP(atom));
      /* NOTE: the module part of atom is ignored  */
  return Unify(atom_with_module,Sk(atom_with_module),
               find_module_folsmb(FOLSMB_NAME(atom),0,module),Key0);
    
}

/**********************************************************************
 * Module_Get_1 : get default module
 **********************************************************************/


Bool                            /* DyALog Wrapper */
DYAM_Module_Get_1(fol_t module)
{
    return Unify(module,R_TRANS_KEY,R_MODULE,Key0);
}


Bool
Module_Set_1(SP(module))
{
    Deref_And_Fail_Unless(module,FOLSMBP(module));
    LVALUE_R_MODULE= REG_VALUE(module);
    Succeed;
}

Bool                            /* DyALog Wrapper */
DYAM_Module_Set_1(fol_t module)
{
    return Module_Set_1(module,R_TRANS_KEY);
}

/**********************************************************************
 * evpred_number_chars
 **********************************************************************/

// char *token_value;

/*
static
void clean_lexer() 
{
    lexer_mode = -1;
    lexer();
}
*/

static fol_t

read_fol_number( char * s )
{
    char *tail;
    long val;
    errno=0;
    val = strtol(s,&tail,0);
    if (errno || tail == s || *tail != 0)
        return Not_A_Fol;
    else
        return DFOLINT(val);
}

static Bool evpred_number_to_chars(Sproto,Sproto);

Bool              /* DyALog Wrapper */
DYAM_evpred_number_to_chars(fol_t number,fol_t list)
{
    fkey_t k=R_TRANS_KEY;
    return evpred_number_to_chars(number,k,list,k);
}

static Bool	  /* DyALog Builtin */
evpred_number_to_chars( SP(number), SP(list) )
{
  fol_t n;

  Deref(number);
  
  if (FOLINTP(number)){
    sprintf(buff,"%ld",CFOLINT(number));
    return String_To_Chars(buff,list, k_list);
  }
  
  if (!FOLVARP(number)) Fail;
  
  if (!Chars_To_String( buff, list, k_list )) Fail;

  n = read_fol_number(buff);
  if (n == Not_A_Fol) Fail;

  Bind( n, Key0, number );
  
  Succeed;
}

/**********************************************************************
 * Name_To_Chars
 **********************************************************************/


static Bool Name_To_Chars(Sproto,Sproto);

Bool              /* DyALog Wrapper */
DYAM_Name_To_Chars(fol_t term,fol_t list)
{
    fkey_t k=R_TRANS_KEY;
    return Name_To_Chars(term,k,list,k);
}

static Bool	  /* DyALog Builtin */
Name_To_Chars( SP(term), SP(list) )
{
  fol_t n;

  Deref( term );
  
  if (FOLINTP(term)) {
    sprintf(buff,"%ld",CFOLINT(term));
    return String_To_Chars(buff,list, k_list);
  }

  if (FOLSMBP( term ))
    return String_To_Chars(SymbolName(term),list,k_list);
  
  if (!FOLVARP( term )) Fail;

  if (!Chars_To_String( buff, list, k_list )) Fail;

  n = read_fol_number(buff);
  
  Bind( ((n== Not_A_Fol) ? Create_Atom(buff) : n) , Key0 , term );

  Succeed;

}

/**********************************************************************
 * Atom_To_Number
 **********************************************************************/


static Bool Atom_To_Number(Sproto,Sproto);

Bool              /* DyALog Wrapper */
DYAM_Atom_To_Number(fol_t atom,fol_t number)
{
    fkey_t k=R_TRANS_KEY;
    return Atom_To_Number(atom,k,number,k);
}

static Bool	  /* DyALog Builtin */
Atom_To_Number( SP(atom) , SP(number) )
{
  fol_t n;

  Deref(number);
  
  if (FOLINTP(number)) {
    sprintf(buff,"%ld",CFOLINT(number));
    return Get_Cst( Create_Atom(buff), atom ); 
  }

  if (!FOLVARP(number)) Fail;

  Deref(atom);

  if (!FOLSMBP(atom)) Fail;

  n = read_fol_number( SymbolName(atom) );
  if (n == Not_A_Fol) Fail;
  
  Bind( n, Key0, number );

  Succeed;
}

/**********************************************************************
 * Chars_To_String
 **********************************************************************/

int
Chars_To_String( char *str, SP(list) )
{
  int n = 0;
  Sdecl(car);

  for(;;) {
    Deref(list);
    
    if (FOLNILP(list)) break;

    if (!FOLPAIRP(list)) Fail;

    Sassign(car, FOLPAIR_CAR(list), k_list);
    
    Deref(car);

    if (!FOLCHARP(car)) Fail;

    *str++ = CFOLCHAR(car);
    n++;
    list = FOLPAIR_CDR(list);

  }

  *str='\0';
  return n;
}

/**********************************************************************
 * String_To_Chars
 **********************************************************************/

static Bool
String_To_Chars(char *str, SP(list))
{
  Sdecl(car);
  fol_t tail = FOLNIL;
  char *c;

  for(;*str;str++)
    {
      Deref(list);

      if (FOLVARP(list)) break;
      if (!FOLPAIRP(list)) Fail;
      
      Sassign(car,FOLPAIR_CAR(list),k_list);

      if (!Get_Char( DFOLCHAR( *str ), car)) Fail;

      list = FOLPAIR_CDR(list);
    }

 for(c=strchr(str,'\0'); c-- > str; tail = folcmp_create_pair( DFOLCHAR( *c ), tail ) );

 return Unify( list, k_list, tail, Key0 );

}

/**********************************************************************
 * Atom_Concat_3
 **********************************************************************/

static Bool Atom_Concat_3(Sproto,Sproto,Sproto);

Bool              /* DyALog Wrapper */
DYAM_Atom_Concat_3(fol_t a,fol_t b, fol_t c)
{
    fkey_t k=R_TRANS_KEY;
    return Atom_Concat_3(a,k,b,k,c,k);
}

static Bool	  /* DyALog Builtin */
Atom_Concat_3( SP(a) , SP(b), SP(c) )
{
    Deref_And_Fail_Unless(a,FOLSMBP(a));
    Deref_And_Fail_Unless(b,FOLSMBP(b));
    sprintf(buff,"%s%s",SymbolName(a),SymbolName(b));
    return Get_Cst( Create_Atom(buff), c );
}

char *
DyALog_Atom_Concat( char *a, char *b )
{
    sprintf(buff,"%s%s",a,b);
    return buff;
}

/**********************************************************************
 * Char_Code_2
 **********************************************************************/

static Bool Char_Code_2(Sproto,Sproto);

Bool              /* DyALog Wrapper */
DYAM_Char_Code_2(fol_t c,fol_t code)
{
    fkey_t k=R_TRANS_KEY;
    return Char_Code_2(c,k,code,k);
}

static Bool	  /* DyALog Builtin */
Char_Code_2( SP(c) , SP(code) )
{
    Deref(c);

    if (FOLCHARP(c))
        return Get_Cst( DFOLINT(CFOLCHAR(c)), code );

    if (FOLVARP(c))
        Fail;
    
    Deref_And_Fail_Unless(code,FOLINTP(code)
                          && CFOLINT(code) > -1
                          && CFOLINT(code) < 256
                          );

    return Get_Cst( DFOLCHAR(CFOLINT(code)), c );
    
}

/**********************************************************************
 * The test Builtins
 **********************************************************************/

#define Define_Type_Checker(_name,_test)        \
static Bool                                     \
_name( SP(term) )                               \
{                                               \
  Deref( term );                                \
  return _test( term );                         \
}                                               \
                                                \
Bool                                            \
DYAM_##_name( fol_t term )                      \
{                                               \
    return _name( term, R_TRANS_KEY );          \
}


#define FOL_SIMPLEP( o )  (FOLVARP(o) || FOLSMBP( o ))

Define_Type_Checker( evpred_number,    FOLINTP      )
Define_Type_Checker( evpred_float,     FOLFLTP      )
Define_Type_Checker( evpred_atomic,    FOL_CSTP     )
Define_Type_Checker( evpred_variable,  FOLVARP      )
Define_Type_Checker( evpred_compound,  FOLCMPP      )
Define_Type_Checker( evpred_char,      FOLCHARP     )
Define_Type_Checker( evpred_integer,   FOLINTP      )
Define_Type_Checker( evpred_atom,      FOLSMBP      )
Define_Type_Checker( evpred_simple,    FOL_SIMPLEP  )
Define_Type_Checker( evpred_notvar,    !FOLVARP     )
Define_Type_Checker( evpred_fset,      FOLFSETP      )
Define_Type_Checker( evpred_loop,      FOLLOOPP     )
Define_Type_Checker( evpred_range,     FOLRANGEP    )
//Define_Type_Checker( evpred_ground,    FOL_GROUNDP  )

//  Groundness checking doesn't fit the above general scheme

static Bool Ground_1(Sproto);

Bool              /* DyALog Wrapper */
DYAM_Ground_1(fol_t A)
{
    fkey_t k=R_TRANS_KEY;
    return Ground_1(A,k);
}

static Bool
Ground_Aux( SP(A) )
{
  Deref(A);

  if (FOL_CSTP(A))
      Succeed;

  if (FOLVARP(A))
      Fail;

  if (FOLCMP_FUNCTOR(A) == FOLLOOP) {
      TRAIL_UBIND(FOLCMP_REF(A,1),Sk(A),FOLNIL,Key0);
      return Ground_Aux(FOLCMP_REF(A,2),Sk(A));
  }
  
  if (FOL_DEREFP(A))
    Fail;

  {
          /* At this point A is compound and not ground */
      obj_t tuple = FOLINFO_TUPLE(A);
      for( ; PAIRP(tuple) ; tuple = CDR(tuple) )
          if (! Ground_Aux( (fol_t) CAR(tuple), Sk(A) ))
              Fail;
      Succeed;
  }

}

static Bool	  /* DyALog Builtin */
Ground_1( SP(A) )
{
      TrailWord *top= C_TRAIL_TOP;
      Bool r = Ground_Aux(A,Sk(A));
      if (C_TRAIL_TOP > top)
          untrail_alt(top);
      return r;
}



/**********************************************************************
 * The Arith Builtins
 **********************************************************************/

#define Get_Arith_Value( t,k,i )                                        \
if ( (v##i = Eval_Arith_Expr( FOLCMP_REF(t,i), k )) != Not_A_Fol ) {    \
      if (FOLFLTP(v##i)) {                                              \
          f##i = (CFOLFLT( v##i ));                                     \
          float_case = 1;                                               \
      } else {                                                          \
          a##i = (CFOLINT( v##i ));                                     \
              f##i = (dyalog_float) a##i;                               \
      }                                                                 \
} else return Not_A_Fol;

#define Arith_Try_2( t, k, _op, _expr, _fexpr )         \
if ( FOLCMP_FUNCTOR( t ) == Functor_Arity( _op, 2 ) ) { \
  Get_Arith_Value(t,k,1);                               \
  Get_Arith_Value(t,k,2);                               \
  if (float_case) {                                     \
      return DFOLFLT( _fexpr );                          \
  } else {                                              \
      return DFOLINT( _expr );                         \
  }                                                     \
}

#define Arith_Try_1( t, k, _op, _expr, _fexpr )         \
if ( FOLCMP_FUNCTOR( t ) == Functor_Arity( _op, 1 ) ) { \
  Get_Arith_Value(t,k,1);                               \
  if (float_case) {                                     \
      return DFOLFLT( _fexpr );                          \
  } else {                                              \
      return DFOLINT( _expr );                         \
  }                                                     \
}

#define Arith_Try_Coerce_Int_1( t, k, _op, _expr )         \
if ( FOLCMP_FUNCTOR( t ) == Functor_Arity( _op, 1 ) ) { \
  Get_Arith_Value(t,k,1);                               \
  return DFOLINT( _expr );                              \
}


static fol_t
Eval_Arith_Expr( SP(expr) )
{
  fol_t v1,v2;
  long   a1=0,a2=0;
  dyalog_float f1=0,f2=0;
  int float_case=0;
  
  Deref( expr );

  if (FOL_NUMBERP(expr))    return expr;
  
  if (FOLCHARP(expr))   return DFOLINT( (long) CFOLCHAR(expr) );

  if (!FOLCMPP(expr) || FOLPAIRP(expr))   return Not_A_Fol;

/* Arithmetic Operators */

  Arith_Try_2(expr,k_expr,"+",    (a1+a2), (f1+f2)              );
  Arith_Try_1(expr,k_expr,"+",    (a1), (f1)                 );
  Arith_Try_2(expr,k_expr,"-",    (a1-a2), (f1-f2)              );
  Arith_Try_1(expr,k_expr,"-",    (-a1), (-f1)                );
  Arith_Try_2(expr,k_expr,"*",    (a1*a2), (f1*f2)              );
  Arith_Try_2(expr,k_expr,"/",    (a1/a2), (f1/f2)              );
  Arith_Try_2(expr,k_expr,"//",   (a1/a2),  (f1/f2)             );
  Arith_Try_2(expr,k_expr,"mod",  (a1%a2), (dyalog_float) (((long) f1) % ((long) f2)));

  Arith_Try_1(expr,k_expr,"rand",    (rand() % a1), (dyalog_float) (rand() % ((long) f2)));

/* Bitwise Operators */

  Arith_Try_2(expr,k_expr,"/\\",  (a1 & a2), (dyalog_float) (((long)f1) & ((long)f2)));
  Arith_Try_2(expr,k_expr,"\\/",  (a1 | a2), (dyalog_float) (((long)f1) | ((long)f2)));
  Arith_Try_2(expr,k_expr,"#",    (a1 ^ a2), (dyalog_float) (((long)f1)^((long)f2)));
  Arith_Try_1(expr,k_expr,"\\",   (~a1), (dyalog_float) (~((long)f1)));
  Arith_Try_2(expr,k_expr,"<<",   (a1 << a2), (dyalog_float)(((long)f1) << ((long)f2)));
  Arith_Try_2(expr,k_expr,">>",   (a1 >> a2), (dyalog_float)(((long)f1) >> ((long)f2)));

/* Non Standard Arithmetic Operators */

  Arith_Try_1(expr,k_expr,"abs", ( a1 > 0  ? a1 : -a1),  ( f1 > 0  ? f1 : -f1));
  Arith_Try_2(expr,k_expr,"min", ( a1 < a2 ? a1 : a2), ( f1 < f2 ? f1 : f2)  );
  Arith_Try_2(expr,k_expr,"max", ( a1 > a2 ? a1 : a2), ( f1 > f2 ? f1 : f2)  );

/* Floating Point Functions (Not yet implemented) */

  Arith_Try_Coerce_Int_1(expr,k_expr,"round", lround(f1));
  Arith_Try_Coerce_Int_1(expr,k_expr,"floor",(long)floor(f1));
  Arith_Try_Coerce_Int_1(expr,k_expr,"ceil",(long)ceil(f1));
  
/*  Arith_Try_1(expr,k_expr,"truncate",truncate); */
/*  Arith_Try_1(expr,k_expr,"sin",sin); */
/*  Arith_Try_1(expr,k_expr,"cos",cos); */
/*  Arith_Try_1(expr,k_expr,"tan",tan); */
/*  Arith_Try_1(expr,k_expr,"asin",asin); */
/*  Arith_Try_1(expr,k_expr,"acos",acos); */
/*  Arith_Try_1(expr,k_expr,"atan",atan); */
/*  Arith_Try_1(expr,k_expr,"sqrt",sqrt); */
/*  Arith_Try_1(expr,k_expr,"log",log); */
/*  Arith_Try_1(expr,k_expr,"exp", exp2(a1), (float) exp( (float) f1)); */
/*  Arith_Try_2(expr,k_expr,"expt",expt(a1,a2),(float) expt( (float) f1, (float)
 *  f2)); */
/*  Arith_Try_1(expr,k_expr,"integer",flonum->fixnum); */
/*  Arith_Try_1(expr,k_expr,"float",fixnum->flonum); */

  return Not_A_Fol;

}

#define Define_Arith_Relation( _name, _op )                             \
static Bool                                                             \
_name( SP(expr1), SP(expr2) )                                           \
{                                                                       \
  fol_t v1,v2;                                                          \
  if ( (v1 = Eval_Arith_Expr( expr1, k_expr1 )) == Not_A_Fol ||         \
       (v2 = Eval_Arith_Expr( expr2, k_expr2 )) == Not_A_Fol ) Fail;    \
                                                                        \
  if ( (FOLFLTP(v1)) & !FOLFLTP(v2) ){                                  \
          v2 = DFOLFLT((dyalog_float)CFOLINT(v2));                             \
  }                                                                     \
                                                                        \
  if ( (FOLFLTP(v2)) & !FOLFLTP(v1) ){                                  \
          v1 = DFOLFLT((dyalog_float)CFOLINT(v1));                             \
  }                                                                     \
                                                                        \
if (FOLFLTP(v1)) {                                                      \
    return (CFOLFLT(v1) _op CFOLFLT(v2));                               \
} else {                                                                \
    return (CFOLINT(v1) _op CFOLINT(v2));                               \
}                                                                       \
}                                                                       \
                                                                        \
Bool                                                                    \
DYAM_##_name( fol_t expr1, fol_t expr2)                                 \
{                                                                       \
    fkey_t k=R_TRANS_KEY;                                               \
    return _name(expr1,k,expr2,k);                                      \
}


Define_Arith_Relation(evpred_lt,<)
Define_Arith_Relation(evpred_gt,>)
Define_Arith_Relation(evpred_le,<=)
Define_Arith_Relation(evpred_ge,>=)
Define_Arith_Relation(evpred_number_eq,==)
Define_Arith_Relation(evpred_number_neq,!=)


static Bool evpred_is(Sproto,Sproto);

Bool              /* DyALog Wrapper */
DYAM_evpred_is(fol_t term,fol_t expr)
{
    fkey_t k=R_TRANS_KEY;
    return evpred_is(term,k,expr,k);
}

static Bool	  /* DyALog Builtin */
evpred_is( SP(term), SP(expr) )
{
  fol_t v = Eval_Arith_Expr( expr, k_expr );
  V_LEVEL_DISPLAY(V_DYAM,"IS %&s\n",expr,Sk(expr));
  
  if ( v == Not_A_Fol ) Fail;

  return Unify( v, Key0, term, k_term );
}

/**********************************************************************
 * evpred_callret_view
 **********************************************************************/

extern obj_t _dyalog_callret_viewer;


void
callret_fdisplay( StmInf *stream, SP(A) )
{
  obj_t viewers = _dyalog_callret_viewer;
  Sdecl(t);
  Bool found = 0;

  Trail;

  Sk(t) = LSTACK_PUSH_VOID;

  for( ; PAIRP(viewers) && !found ; viewers = CDR(viewers) ){
    Trail;
    if (Unify( A, Sk(A), (fol_t)CAR(CAR(viewers)), Sk(t))) {
      t = (fol_t) CDR(CAR(viewers));
      sfol_fdisplay(stream,t,Sk(t));
      found = 1;
    }
    Untrail;
  }

  Untrail;

  if (!found)
    sfol_fdisplay( stream, A, Sk(A) );

}

extern obj_t current_output_port;


static Bool View_Callret(Sproto);

Bool              /* DyALog Wrapper */
DYAM_View_Callret(fol_t A)
{
    fkey_t k=R_TRANS_KEY;
    return View_Callret(A,k);
}

static Bool	  /* DyALog Builtin */
View_Callret( SP(A) )
{
  StmInf *port = INT_TO_STM( stm_output );
  callret_fdisplay(port,A,Sk(A));
  return (Bool) 1;
}

/**********************************************************************
 * Gensym_1
 **********************************************************************/


static Bool Gensym_1(Sproto);

Bool              /* DyALog Wrapper */
DYAM_Gensym_1(fol_t A)
{
    fkey_t k=R_TRANS_KEY;
    return Gensym_1(A,k);
}

static Bool	  /* DyALog Builtin */
Gensym_1( SP(A) )
{
  static unsigned long counter = 0;
  return Unify( A, Sk(A), DFOLINT(counter++), Sk(A) );
}

unsigned long
DyALog_Gensym()
{
    static unsigned long counter=0;
    return counter++;
}

/**********************************************************************
 * Subsumes_Chk
 **********************************************************************/


static Bool Subsumes_Chk_2(Sproto,Sproto);

Bool              /* DyALog Wrapper */
DYAM_Subsumes_Chk_2(fol_t Gen,fol_t Spec)
{
    fkey_t k=R_TRANS_KEY;
    return Subsumes_Chk_2(Gen,k,Spec,k);
}

static Bool	  /* DyALog Builtin */
Subsumes_Chk_2( SP(Gen), SP(Spec) )
{
  Bool res=0;
  Trail;
  res = sfol_alt_subsume(Gen, Sk(Gen),Spec,Sk(Spec));
  Untrail;
  return res;
}

/**********************************************************************
 * Numbervars
 **********************************************************************/

typedef struct numbervar {
    long var;
    long build_var;
    long ctr;
    fkey_t key;
    fkey_t build_key;
} *numbervar_t;

static void
local_numbervars( SP(A) , numbervar_t info)
{
  Deref(A);
  
  if (FOL_CSTP(A))
      return;

  if (Sk(A) == info->build_key || Sk(A) == info->key)
      return;
    
  if (FOLVARP(A)) {
      fol_t X = FOLVAR_FROM_INDEX( info->var++ );
      Bind( X , info->key, A );
      return;
  }

  if (FOLCMP_FUNCTOR(A) == FOLLOOP) {
          /* A = '$LOOP'(D,t) where D may be reached by dereferencing variables
           * of t
           * then
           *     D bound to *LOOP*(XX,*VAR*(X,Y))
           *     X bound to Z
           *     Y bound to r=[$LOOP,t]
           */
      fol_t B = FOLCMP_REF(A,2);
      fkey_t Sk(B) = Sk(A);
      Deref(B);
      if (FOL_FUNCTOR(B) ==
          FOLSMB_CONVERT_ARITY( FOLNUMBERVAR, 2)) {
          V_LEVEL_DISPLAY(V_DYAM,"NUMBERVARS Second loop: %s\n",A,Sk(A));
          return;
      } else {
          fol_t functor = FOLSMB_CONVERT_ARITY( FOLNUMBERVAR, 2);
          fol_t t;
          fol_t XX = FOLVAR_FROM_INDEX(info->build_var++);
          fol_t X = FOLVAR_FROM_INDEX(info->build_var++);
          fol_t Y = FOLVAR_FROM_INDEX(info->build_var++);
          fol_t Z = FOLVAR_FROM_INDEX( info->var++ );
          fol_t r= folcmp_create_pair( Convert_To_Atom(FOLCMP_FUNCTOR(A)),
                                       folcmp_create_pair( FOLCMP_REF(A,2) ,
                                                           FOLNIL));
          V_LEVEL_DISPLAY(V_DYAM,"NUMBERVARS %&s\n",A,Sk(A));
          V_LEVEL_DISPLAY(V_DYAM,"NUMBERVARS 2 %&p\n",FOLCMP_REF(A,2),Sk(A));
          V_LEVEL_DISPLAY(V_DYAM,"NUMBERVARS R:%&p is %&s\n",
                          Y,info->build_key,
                          r,Sk(A));
          FOLCMP_WRITE_START( functor, 2);
          FOLCMP_WRITE( X );
          FOLCMP_WRITE( Y );
          t = FOLCMP_WRITE_STOP;
          FOLCMP_WRITE_START( FOLLOOP, 3 );
          FOLCMP_WRITE(XX);
          FOLCMP_WRITE(t);
          FOLCMP_WRITE(FOLCMP_REF(A,3)); /* inherit loop status of A */
          t=FOLCMP_WRITE_STOP;
          V_LEVEL_DISPLAY(V_DYAM,"NUMBERVARS T:%&p is %&s\n",
                          FOLCMP_REF(A,1),Sk(A),
                          t,info->build_key);
          TRAIL_UBIND(X,info->build_key,Z,info->key);
          TRAIL_UBIND(Y,info->build_key,r,Sk(A));
          TRAIL_UBIND(FOLCMP_REF(A,1),Sk(A),t,info->build_key);
              /* numbervar args after numbervar D to avoid loops */
          local_numbervars( FOLCMP_REF(A,2), Sk(A), info );
          V_LEVEL_DISPLAY(V_DYAM,"Done\n");
          return;
      }
  }
  
  if (FOLCMP_DEREFP(A)) {
          /* A is a compound term dereferencable
             => A=f(D,a1,..aN) where D is a free variable
             then:
             D bound to t=*VAR*(X,Y)
             X bound to Z
             Y bound to r=[f,a1,..,aN]
           */
      fol_t functor = FOLSMB_CONVERT_ARITY( FOLNUMBERVAR, 2);
      fol_t t;
      fol_t X = FOLVAR_FROM_INDEX(info->build_var++);
      fol_t Y = FOLVAR_FROM_INDEX(info->build_var++);
      fol_t Z = FOLVAR_FROM_INDEX( info->var++ );
      fol_t *stop = &FOLCMP_REF(A,1);
      fol_t D = *stop;
      fol_t *arg = stop + FOLCMP_ARITY(A) - 1;
      fol_t r=FOLNIL;
      V_LEVEL_DISPLAY(V_DYAM,"NUMBERVARS %&s\n",A,Sk(A));
      FOLCMP_WRITE_START( functor, 2);
      FOLCMP_WRITE( X );
      FOLCMP_WRITE( Y );
      t = FOLCMP_WRITE_STOP;
      V_LEVEL_DISPLAY(V_DYAM,"NUMBERVARS T is %&s\n",t,info->build_key);
      for( ; stop < arg ; arg--) {
          r = folcmp_create_pair( *arg, r);
      }
      r = folcmp_create_pair( Convert_To_Atom(FOLCMP_FUNCTOR(A)), r);
      V_LEVEL_DISPLAY(V_DYAM,"NUMBERVARS R is %&s\n",r,Sk(A));
      TRAIL_UBIND(D,Sk(A),t,info->build_key);
      TRAIL_UBIND(X,info->build_key,Z,info->key);
      TRAIL_UBIND(Y,info->build_key,r,Sk(A));
          /* numbervar args after numbervar D to avoid loops */
      for( arg = stop + FOLCMP_ARITY(A) - 1; stop < arg ; arg--) {
          local_numbervars( *arg, Sk(A), info );
      }
      V_LEVEL_DISPLAY(V_DYAM,"Done\n");
      return;
  }
  
  if (FOLCMP_FUNCTOR(A) == FOLNUMBERVAR) {
      A=FOLCMP_REF(A,1);
      Deref(A);
      if (FOLINTP(A)) {
          if (info->ctr <= CFOLINT(A))
              info->ctr = CFOLINT(A)+1;
          return;
      }
      local_numbervars(A,Sk(A),info);
      return;
  }
  
  {
      fol_t *arg = &FOLCMP_REF(A,1);
      fol_t *stop = arg + FOLCMP_ARITY(A);
      for(;arg < stop;)
          local_numbervars( *arg++, Sk(A), info );
  }

}

static Bool Numbervars_3(Sproto,Sproto,Sproto);

Bool              /* DyALog Wrapper */
DYAM_Numbervars_3(fol_t A,fol_t start,fol_t stop)
{
    fkey_t k=R_TRANS_KEY;
    return Numbervars_3(A,k,start,k,stop,k);
}

static Bool	  /* DyALog Builtin */
Numbervars_3( SP(A), SP(start), SP(stop) )
{
  fkey_t k = LSTACK_PUSH_VOID;
  fkey_t build_key = LSTACK_PUSH_VOID;
  struct numbervar box = { .var= 0, .build_var= 0, .ctr= 0 , .key= k, .build_key= build_key };
  long i;

  Deref(start);
  if (FOLVARP(start))
    Bind( DFOLINT(0), Key0, start);
  else if (FOLINTP( start ))
    box.ctr = CFOLINT( start );
  else
    Fail;
	 
  local_numbervars( A, Sk(A), &box );
  for( i = 0 ; i < box.var ; i++ ) {
    fol_t t;
    FOLCMP_WRITE_START( FOLNUMBERVAR, 1);
    FOLCMP_WRITE( DFOLINT(box.ctr++) );
    t = FOLCMP_WRITE_STOP;
    TRAIL_UBIND( FOLVAR_FROM_INDEX(i), k, t , Key0 );
  }

  return Unify( DFOLINT(box.ctr), Key0, stop, Sk(stop));
}

/**********************************************************************
 *  sfol_copy_numbervar r k
 *      make a copy of r and replace r's numvariables by fresh variables
 *
 *   WARNING: this  function is still unsafe
 *         - `A' may look like a numbervar but not be one
 *         - A variable and a numbervar may collapse to a same fresh variable
 **********************************************************************/

fol_t
sfol_copy_numbervar( SP(A) )
{

    Deref(A);
    
    if (FOLCMPP( A )) {
        
        if (FOLCMP_FUNCTOR(A) == FOLNUMBERVAR) {
            long index;
            A = FOLCMP_REF(A,1);
            Deref(A);
            index = CINT(A);
            A = FOLVAR_FROM_INDEX(index);
        } else {
            unsigned long arity = FOLCMP_ARITY( A );
            fol_t *arg = &FOLCMP_REF(A,1);
            fol_t *stop = arg + arity;
            
            FOLCMP_WRITE_START( FOLCMP_FUNCTOR( A ), arity );
            
            for (; arg < stop ;)
                FOLCMP_WRITE( sfol_copy_numbervar( *arg++, Sk(A) ) );
            
            A = FOLCMP_WRITE_STOP;
        }
    }
    
    return A;
}

static Bool	  /* DyALog Builtin */
Copy_Numbervars_2( SP(A), SP(B) )
{
  fkey_t k = LSTACK_PUSH_VOID;
  return Unify(B,Sk(B),sfol_copy_numbervar(A,Sk(A)),k);
}

Bool              /* DyALog Wrapper */
DYAM_Copy_Numbervars_2(fol_t A,fol_t B)
{
    fkey_t k=R_TRANS_KEY;
    return Copy_Numbervars_2(A,k,B,k);
}

/**********************************************************************
 * Binder_2 : bind each var of term to val
 **********************************************************************/

typedef struct vars 
{
    fol_t t;
    fkey_t k;
    void * info;
    struct vars *previous;
} * vars_t;

inline static
vars_t check_var(SP(t),vars_t l) 
{
    for(;l;l=l->previous) {
        if (l->t==t && l->k==Sk(t))
            break;
    }
    return l;
}

static Bool Binder_2(Sproto,Sproto,vars_t);

Bool              /* DyALog Wrapper */
DYAM_Binder_2(fol_t term,fol_t val)
{
    fkey_t k=R_TRANS_KEY;
    return Binder_2(term,k,val,k,(vars_t)0);
}

static Bool	  /* DyALog Builtin */
Binder_2( SP(term), SP(val), vars_t list )
{
  Deref(term);
  if (FOLLOOPP(term)) {
      vars_t entry=check_var(term,Sk(term),list);
      if (entry)
          Succeed;
      else {
          struct vars v;
          v.t= term;
          v.k= Sk(term);
          v.previous=list;
          list = &v;
          return Binder_2( (fol_t) FOLCMP_REF(term,2), Sk(term), val, Sk(val), list);
      }
  } else if (FOL_DEREFP(term))
    return Unify(term,Sk(term),val,Sk(val));
  else if (FOLCMPP(term)) {
    obj_t tuple = FOLINFO_TUPLE(term);
    for( ; PAIRP(tuple) ; tuple = CDR( tuple ))
      if (!Binder_2( (fol_t) CAR( tuple ), Sk(term), val, Sk(val), list))
	Fail;
    Succeed;
  } else
    Succeed;
}

/**********************************************************************
 * DerefTerm Binding @=
 *   WARNING: to update
 **********************************************************************/


static Bool Derefterm_Bind_2(Sproto,Sproto);

Bool              /* DyALog Wrapper */
DYAM_Derefterm_Bind_2(fol_t term,fol_t val)
{
    fkey_t k=R_TRANS_KEY;
    return Derefterm_Bind_2(term,k,val,k);
}

static Bool	  /* DyALog Builtin */
Derefterm_Bind_2( SP(term), SP(val) )
{
  Deref(term);
  Deref(val);
  
  Bind(val,Sk(val),term);
  Succeed;
}

/**********************************************************************
 * Generic_Install
 **********************************************************************/

void
generic_install( SP(A), void installer(fol_t) )
{
  Deref(A);
  if (FOLNILP(A))
    return;

  if (FOLVARP(A) || FOLINTP(A) || FOLCHARP(A))
    return;			/* should raise an ERROR */

  if (FOLSMBP(A)) {
    installer(A);
  } else if (FOLPAIRP(A)){
    generic_install( FOLPAIR_CAR(A), Sk(A), installer );
    generic_install( FOLPAIR_CDR(A), Sk(A), installer );
  } else if (FOLCMPP(A)
	     && (FOLCMP_FUNCTOR(A) == Functor_Arity("/",2))) {
    Sdecl(s);
    Sdecl(n);

    Sassign(s,FOLCMP_REF(A,1),Sk(A));
    Deref(s);

    Sassign(n,FOLCMP_REF(A,2),Sk(A));
    Deref(n);

    if (!FOLSMBP(s) || !FOLINTP(n)) /* should raise an error */
      return;

    installer( FOLSMB_CONVERT_ARITY(s,CFOLINT(n)) );
    
  } else			/* should raise an error */
    return;
}

static fol_t installer_args[3];

static void
info_installer( fol_t s )
{
  folsmb_info_set(s, (obj_t) (installer_args[0]), (obj_t) (installer_args[1]));
}

static void
generic_info_set( SP(predlist), fol_t type, fol_t value )
{
  installer_args[0] = type;
  installer_args[1] = value;
  generic_install( predlist, Sk(predlist), &info_installer );
}

/**********************************************************************
 * Hilog_1 installer
 **********************************************************************/

static void
folsmb_switch_hilog( fol_t s )
{
  FOLSMB_SET_HILOG( s );
}


static Bool Hilog_1(Sproto);

Bool              /* DyALog Wrapper */
DYAM_Hilog_1(fol_t spec)
{
    fkey_t k=R_TRANS_KEY;
    return Hilog_1(spec,k);
}

static Bool	  /* DyALog Builtin */
Hilog_1( SP(spec) )
{
  generic_install( spec, Sk(spec), &folsmb_switch_hilog );
  Succeed;
}

Decl_Wrapper(Hilog,Hilog_1,D_Arg(1))

/**********************************************************************
 * Feature_2 installer
 **********************************************************************/

static void
feature_installer( fol_t s )
{
  folsmb_feature_set( s, installer_args[0] );
}

static fol_t
build_feature_table( SP(features) )
{
  fol_t model;
  long arity=0;

  FOLCMP_WRITE_LIGHT_START;
  
  for (;; arity++) {
    
    Deref(features);

    if (FOLNILP(features)){
      model = FOLCMP_WRITE_LIGHT_STOP( find_folsmb( "features", arity), arity);
      return model;
    } else if (FOLPAIRP(features)) {
      Sdecl(f);
      Sassign(f,FOLPAIR_CAR(features),Sk(features));
      Deref(f);
      
      if (! (FOLSMBP(f) || (FOLCMPP(f) && FOLCMP_FUNCTOR(f) == find_folsmb(":",2)))) {
	FOLCMP_WRITE_ABORT;
	dyalog_error_printf( "not a valid feature %&f\n", f );
	exit(EXIT_FAILURE);
      }

      FOLCMP_WRITE( f );	/* should check if f is a symbol */
      features = FOLPAIR_CDR( features );
    } else {
      FOLCMP_WRITE_ABORT;
      dyalog_error_printf( "not a valid feature list %&s\n", features, Sk(features) );
      exit(EXIT_FAILURE);
    }
  }

}


static Bool Feature_2(Sproto,Sproto);

Bool              /* DyALog Wrapper */
DYAM_Feature_2(fol_t predlist,fol_t features)
{
    fkey_t k=R_TRANS_KEY;
    return Feature_2(predlist,k,features,k);
}

static Bool	  /* DyALog Builtin */
Feature_2( SP(predlist), SP(features) )
{
  installer_args[0] = build_feature_table( features, Sk(features) );
  generic_install( predlist, Sk(predlist), &feature_installer);
  Succeed;
}

Decl_Wrapper(Feature, Feature_2, D_Arg(1), D_Arg(2))

/**********************************************************************
 * Finite_Set_2 installer
 **********************************************************************/


static fol_t
build_finite_set_table( fol_t s, SP(elements) )
{
    fol_t model;
    long arity=0;

        //dyalog_printf("FINITE SET %&s\n",elements,Sk(elements));
        
    FOLCMP_WRITE_LIGHT_START;
    for (;; arity++) {
        Deref(elements);
        if (FOLNILP(elements)){
            model = FOLCMP_WRITE_LIGHT_STOP( FOLSMB_CONVERT_ARITY(s,arity),
                                             arity);
            if (arity > FSET_MAX_ARITY) {
                dyalog_error_printf( "finite set length should be less than %d: %d\n",FSET_MAX_ARITY+1,arity);
                exit(EXIT_FAILURE);
            }
            return model;
        } else if (FOLPAIRP(elements)) {
            Sdecl(e);
            Sassign(e,FOLPAIR_CAR(elements),Sk(elements));
            Deref(e);
            if (! (FOL_CSTP(e))) {
                FOLCMP_WRITE_ABORT;
                dyalog_error_printf( "not a valid finite set element %&f\n", e );
                exit(EXIT_FAILURE);
            }
            FOLCMP_WRITE( e );	
            elements = FOLPAIR_CDR( elements );
        } else {
            FOLCMP_WRITE_ABORT;
            dyalog_error_printf( "not a valid finite set element list %&s\n",
                                 elements,
                                 Sk(elements) );
            exit(EXIT_FAILURE);
        }
    }
}

static void
finite_set_installer( fol_t s )
{
    fol_t table= build_finite_set_table(s,
                                        (fol_t) installer_args[0],
                                        (fkey_t) installer_args[1]);
    folsmb_fset_set( s, table );
}

static Bool Finite_Set_2(Sproto,Sproto);

Bool                            /* DyALog Wrapper */
DYAM_Finite_Set_2(fol_t predlist,fol_t elements)
{
    fkey_t k=R_TRANS_KEY;
    return Finite_Set_2(predlist,k,elements,k);
}

static Bool	  /* DyALog Builtin */
Finite_Set_2( SP(predlist), SP(elements) )
{
        //    installer_args[0]=build_finite_set_table( elements, Sk(elements)
        //    );
    installer_args[0]=elements;
    installer_args[1]=(fol_t) Sk(elements);
    generic_install( predlist, Sk(predlist), &finite_set_installer);
    Succeed;
}

Decl_Wrapper(Finite_Set,Finite_Set_2,D_Arg(1),D_Arg(2));

/**********************************************************************
 * Subset_2 installer
 **********************************************************************/

static void
subset_installer( fol_t s )
{
    folsmb_fset_set( s, installer_args[0] );
}

static Bool Subset_2(Sproto,Sproto);

Bool                            /* DyALog Wrapper */
DYAM_Subset_2(fol_t predlist,fol_t set)
{
    fkey_t k=R_TRANS_KEY;
    return Subset_2(predlist,k,set,k);
}

static Bool	  /* DyALog Builtin */
Subset_2( SP(predlist), SP(set) )
{
    Deref(set);
    if (!FOLFSETP(set)) {
        dyalog_error_printf( "a finite set is expected %&s\n",
                             set,
                             Sk(set) );
        Fail;
    } else {
        fol_t table = FOLCMP_REF(set,2);
        long words = FOLCMP_ARITY(set)-2;
        fol_t *arg = &FOLCMP_REF(set,3);
        fol_t *stop = arg+words;
        FOLCMP_WRITE_START(FOLSMB_CONVERT_ARITY(FOLCMP_FUNCTOR(table),words),
                           words);
        for(; arg<stop;)
            FOLCMP_WRITE( *arg++);
        installer_args[0]= folcmp_create_pair(FOLCMP_REF(set,2),
                                              FOLCMP_WRITE_STOP);
        generic_install( predlist, Sk(predlist), &subset_installer);
        Succeed;
    }
}

Decl_Wrapper(Subset,Subset_2,D_Arg(1),D_Arg(2))

    
/**********************************************************************
 * Op_3 installer
 **********************************************************************/

static void
op_installer( fol_t s )
{
  install_opmode_wrapper( CFOLINT(installer_args[0]),
			  FOLSMB_NAME(installer_args[1]), 
			  FOLSMB_NAME(s) );
}


static Bool Op_3(Sproto,Sproto,Sproto);

Bool              /* DyALog Wrapper */
DYAM_Op_3(fol_t Prec,fol_t Mode,fol_t Pred)
{
    fkey_t k=R_TRANS_KEY;
    return Op_3(Prec,k,Mode,k,Pred,k);
}

static Bool	  /* DyALog Builtin */
Op_3( SP(Prec), SP(Mode), SP(Pred) )
{
  Deref(Prec);
  Deref(Mode);
  Deref(Pred);
  installer_args[0] =  Prec;
  installer_args[1] = Mode;
  generic_install( Pred, Sk(Pred), &op_installer );
  Succeed;
}

Decl_Wrapper(Op,Op_3,D_Arg(1),D_Arg(2),D_Arg(3))

/**********************************************************************
 * Derefterm_1 installer
 **********************************************************************/


static Bool DerefTermSet_1(Sproto);

Bool              /* DyALog Wrapper */
DYAM_DerefTermSet_1(fol_t spec)
{
    fkey_t k=R_TRANS_KEY;
    return DerefTermSet_1(spec,k);
}

static Bool	  /* DyALog Builtin */
DerefTermSet_1( SP(spec) )
{
  generic_install( spec, Sk(spec), &folsmb_switch_derefterm );
  Succeed;
}

Decl_Wrapper(DerefTermSet,DerefTermSet_1,D_Arg(1))

/**********************************************************************
 * Subtypes_2 Installer
 **********************************************************************/


static Bool Subtypes_2(Sproto,Sproto);

Bool              /* DyALog Wrapper */
DYAM_Subtypes_2(fol_t spec,fol_t subtypes)
{
    fkey_t k=R_TRANS_KEY;
    return Subtypes_2(spec,k,subtypes,k);
}

static Bool	  /* DyALog Builtin */
Subtypes_2( SP(spec), SP(subtypes) ) 
{
  Deref(subtypes);
  generic_info_set( spec, Sk(spec),
		    (fol_t) INFO_SUBTYPES,
		    subtypes);
  Succeed;
}

Decl_Wrapper(Subtypes,Subtypes_2,D_Arg(1),D_Arg(2))

/**********************************************************************
 * Intro_2 Installer
 **********************************************************************/


static Bool Intro_2(Sproto,Sproto);

Bool              /* DyALog Wrapper */
DYAM_Intro_2(fol_t spec,fol_t intro)
{
    fkey_t k=R_TRANS_KEY;
    return Intro_2(spec,k,intro,k);
}

static Bool	  /* DyALog Builtin */
Intro_2( SP(spec), SP(intro) ) 
{
  Deref(intro);
  generic_info_set( spec, Sk(spec), 
		    (fol_t) INFO_INTRO,
		    intro);
  Succeed;
}

Decl_Wrapper(Intro,Intro_2,D_Arg(1),D_Arg(2))

/**********************************************************************
 * Skel_Id_2 
 **********************************************************************/

static Bool Skel_Id_2( Sproto, Sproto);

Bool                            /* DyALog Wrapper */
DYAM_Skel_Id_2( fol_t term, fol_t id )
{
    fkey_t k=R_TRANS_KEY;
    return Skel_Id_2(term,k,id,k);
}

static Bool                     /* DyALog Builtin */
Skel_Id_2( SP(term), SP(id) )
{
    Deref(term);

    if (!FOLCMPP(term))
        return Unify(term,Sk(term),id,Sk(id));
    else {
        fol_t lid = Create_Pair(DFOLINT(((long) term ) >> TAG_SHIFT),FOLNIL);
        return Unify(lid,Key0,id,Sk(id));
    }
}

/**********************************************************************
 * Compiler_Info_1
 **********************************************************************/

static fol_t compiler_info = (fol_t) 0;

extern const char * _dyalog_author;
extern const char * _dyalog_email;

static Bool
Compiler_Info_1( SP(info) )
{
    if (!compiler_info) {
        fol_t op = find_module_folsmb(FEATURE_SPACE,4,find_folsmb("compiler_info",0));
//        FOLCMP_WRITE_START(Functor_Arity("compiler_info",4), 4);
        FOLCMP_WRITE_START(op, 4);
        FOLCMP_WRITE( Create_Atom( PACKAGE ) );       // name
        FOLCMP_WRITE( Create_Atom( VERSION ) );       // version
        FOLCMP_WRITE( Create_Atom( _dyalog_author ) );  // author
        FOLCMP_WRITE( Create_Atom( _dyalog_email ) ); // email
        compiler_info = FOLCMP_WRITE_STOP;
    }
    return Unify(compiler_info,Key0,info,Sk(info));
}

Bool                            /* DyALog Wrapper */
DYAM_Compiler_Info_1( fol_t info )
{
    fkey_t k=R_TRANS_KEY;
    return Compiler_Info_1(info,k);
}

/**********************************************************************
 * DyALog_Create_Mutable
 **********************************************************************/

typedef struct dyalog_mutable {
    fol_t t;
    long   k;
    obj_t env;
    struct dyalog_mutable *next;
}  dyalog_mutable, *dyalog_mutable_t;

dyalog_mutable_t dyalog_mutable_list=0;

    /* Need to register heap mutable to avoid problems with GC
       (but I don't know an easy way to free them automatically)
    */
static dyalog_mutable_t
mutable_register()
{
    dyalog_mutable_t m = (dyalog_mutable_t) GC_MALLOC_PRINTF("mutable", sizeof(dyalog_mutable) );
    m->next = dyalog_mutable_list;
    dyalog_mutable_list=m;
    return m;
}

void
DyALog_Mutable_Reset () 
{
    dyalog_mutable_list = NULL;
}

dyalog_mutable_t
DyALog_Mutable_Write( dyalog_mutable_t m, sfol_t x, long backtrack )
{
    if (!m)  {                   /* allocate mutable if necessary*/
        if ((backtrack)){         /* global mutex on TRAIL STACK */
            m = (dyalog_mutable_t) PUSH_TRAIL_BLOCK( sizeof( dyalog_mutable ));
            m->next = (dyalog_mutable_t) 01;       /* not a pointer */
        } else                    /* scope mutex in heap (do not use GC_MALLOC) */
            m = mutable_register();
    }
    SFOL_Deref(x);
    if (!FOL_GROUNDP(x->t)) {
        Collapse_Unwrap(x->t, x->k, m->k, m->env);
        x->k=(fkey_t) load_layer_archive(m->k,m->env);
        SFOL_Deref(x);
        if (FOL_GROUNDP(x->t)){
            m->k=0;
            m->env=(obj_t)0;
        }
    }
    m->t=x->t;
    return m;
}

Bool
DyALog_Mutable_Read( dyalog_mutable_t m, sfol_t x )
{
    fkey_t k = Key0;
    if (!FOL_GROUNDP(m->t))
        k = (fkey_t) load_layer_archive(m->k,m->env);
    return Unify(x->t,x->k,m->t,k);
}

Bool
DyALog_Mutable_Inc( dyalog_mutable_t m, long *x)
{
    fol_t i = m->t;
    if (!FOLINTP(i))
        Fail;
    *x=CFOLINT(i);
    m->t = DFOLINT((*x)+1);
    Succeed;
}

void
DyALog_Mutable_List_Extend( dyalog_mutable_t m, sfol_t x)
{
    SFOL_Deref(x);
    m->t = Create_Pair(x->t,m->t);
}

void
DyALog_Mutable_Add( dyalog_mutable_t m, long x)
{
    m->t = DFOLINT(CFOLINT(m->t)+x);
}

void
DyALog_Mutable_Min( dyalog_mutable_t m, long x)
{
    if (x < CFOLINT(m->t))
        m->t = DFOLINT(x);
}

void
DyALog_Mutable_Max( dyalog_mutable_t m, long x)
{
    if (x > CFOLINT(m->t))
        m->t = DFOLINT(x);
}

Bool
DyALog_Mutable_Check_Min( dyalog_mutable_t m, long x)
{
    return (x < CFOLINT(m->t));
}

Bool
DyALog_Mutable_Check_Max( dyalog_mutable_t m, long x)
{
    return (x > CFOLINT(m->t));
}

void
DyALog_Mutable_Free( dyalog_mutable_t m )
{
    m->t=(fol_t)0;
    if ((long)(m->next)==01) {            /* stack-allocated mutable */
        return;                 /* nothing to do */
    }
    
        /* registered heap-allocated mutable */

    if (dyalog_mutable_list==m) {
        dyalog_mutable_list=m->next;
    } else {
        dyalog_mutable_t tmp = dyalog_mutable_list;
        for(;tmp->next!=m;tmp=tmp->next);
        tmp->next=m->next;
    }
}

void
DyALog_Mutable_Dump()
{
    dyalog_mutable_t m = dyalog_mutable_list;
    for(; m ; m=m->next) {
        fkey_t k = Key0;
        if (!FOL_GROUNDP(m->t))
            k = (fkey_t) load_layer_archive(m->k,m->env);
        dyalog_printf("Mutable dump %&s\n",m->t,k);
    }
}

/**********************************************************************
 * DyALog_Variable_Rebind
 May be used to change the value of a variable (with no occur check)
 Note that the old value may be reset by backtrack
 **********************************************************************/

Bool
DyALog_Variable_Rebind( sfol_t x, sfol_t t)
{
//    dyalog_printf("try rebind %&f\n",x->t);
    if (!FOLVARP(x->t))
        Fail;
    TRAIL_UBIND(x->t,x->k,t->t,t->k);
    Succeed;
}

/**********************************************************************
 * DyALog_Format
 Helper to write format-like instruction
 **********************************************************************/

char *
DyALog_Format( sfol_t stream,
               char *motif,
               char escape,
               long *x)
{
    long     stm;

    SFOL_Deref(stream);
    
    stm=(stream->t == FOLNIL) 
        ? stm_output
        : Get_Stream_Or_Alias(stream->t,stream->k,STREAM_FOR_OUTPUT);
    
    last_output_sora=stream->t;
    Check_Stream_Type(stm,STREAM_FOR_OUTPUT|STREAM_FOR_TEXT);

    V_LEVEL_DISPLAY(V_DYAM,"DyALog_Format [%x] %s %c\n",(unsigned long)motif,motif,escape);

  loop:
    
    for(;*motif && *motif != escape;){
        Stream_Putc(*motif++,stm_tbl+stm);
    }
    
    if (!*motif)                /* end of string */
        return (char *)NULL;

    motif++;
    
    if (!*motif) {              /* escape at end of string */
        Stream_Putc(escape,stm_tbl+stm);
        return (char *)NULL;
    }

    if (*motif==escape) {       /* double escape => no escape */
        Stream_Putc(*motif++,stm_tbl+stm);
        goto loop;
    }
    
    *x=(long)*motif++;
    return motif;
}

/*
  Handling Range terms
 */

static Bool /* DyALog Builtin */
Term_Range_3( SP(Min), SP(Max), SP(Range) )    
{
    Deref(Min);
    Deref(Max);
    Deref(Range);

    if (FOLINTP(Min) && FOLINTP(Max)) {
        if (CFOLINT(Min) > CFOLINT(Max)){
            Fail;
        } else if ( CFOLINT(Min) == CFOLINT(Max)) {
            return Unify(Min,Key0,Range,Sk(Range));
        } else {
            Sdecl(res);
            res = (fol_t) FOLCMP_WRITE_START( FOLRANGE, 3 );
            FOLCMP_WRITE( FOLVAR_FROM_INDEX(0) );
            FOLCMP_WRITE( Min );
            FOLCMP_WRITE( Max );
            res = FOLCMP_WRITE_STOP;
            Sk(res) = LSTACK_PUSH_VOID;
            return Unify( res, Sk(res), Range, Sk(Range) );
        }
    } else if (FOLINTP(Range)) {
        return Unify(Min,Sk(Min),Range,Sk(Range))
            && Unify(Max,Sk(Max),Range,Sk(Range));
    } else if (FOLRANGEP(Range)) {
        fol_t min = FOLCMP_REF(Range,2);
        fol_t max = FOLCMP_REF(Range,3);
        
        if (FOLINTP(Min)) {
            if (CFOLINT(Min) < CFOLINT(min) || CFOLINT(Min) > CFOLINT(max))
                Fail;
            if (CFOLINT(Min) == CFOLINT(max)) {
                return Unify(Min,Sk(Min),Range,Sk(Range))
                    && Unify(Max,Sk(Max),Min,Sk(Min));
            } else {
                Sdecl(res);
                res = (fol_t) FOLCMP_WRITE_START( FOLRANGE, 3 );
                FOLCMP_WRITE( FOLVAR_FROM_INDEX(0) );
                FOLCMP_WRITE( Min );
                FOLCMP_WRITE( max );
                res = FOLCMP_WRITE_STOP;
                Sk(res) = LSTACK_PUSH_VOID;
                return Unify(res,Sk(res),Range,Sk(Range))
                    && Unify(Max,Sk(Max),max,Sk(Range));
            }
         } else if (FOLINTP(Max)) {
            if (CFOLINT(Max) < CFOLINT(min) || CFOLINT(Max) > CFOLINT(max))
                Fail;
            if (CFOLINT(Max) == CFOLINT(min)) {
                return Unify(Max,Sk(Max),Range,Sk(Range))
                    && Unify(Max,Sk(Max),Min,Sk(Min));
            } else {
                Sdecl(res);
                res = (fol_t) FOLCMP_WRITE_START( FOLRANGE, 3 );
                FOLCMP_WRITE( FOLVAR_FROM_INDEX(0) );
                FOLCMP_WRITE( min );
                FOLCMP_WRITE( Max );
                res = FOLCMP_WRITE_STOP;
                Sk(res) = LSTACK_PUSH_VOID;
                return Unify(res,Sk(res),Range,Sk(Range))
                    && Unify(Min,Sk(Min),min,Sk(Range));
            }
        } else {
            return Unify(Min,Sk(Min),min,Sk(Range))
                && Unify(Max,Sk(Max),max,Sk(Range));
        }
    } else
        Fail;
}

Bool /* DyALog Wrapper */
DYAM_Term_Range_3(fol_t Min, fol_t Max, fol_t Range ) 
{
    fkey_t k=R_TRANS_KEY;
    return Term_Range_3(Min,k,Max,k,Range,k);
}

static Bool /* DyALog Builtin */
Shift_Range_4( SP(Left), SP(Right), SP(Range), SP(Shifted_Range))
{
    Deref(Left);
    Deref(Right);
    Deref(Range);
    if (FOLRANGEP(Range) && FOLINTP(Left) && FOLINTP(Right)) {
        long min = CFOLINT(FOLCMP_REF(Range,2)) + CFOLINT(Left);
        long max = CFOLINT(FOLCMP_REF(Range,3)) + CFOLINT(Right);
        if (min > max) {
            Fail;
        } else {
            Sdecl(res);
            res = (fol_t) FOLCMP_WRITE_START( FOLRANGE, 3 );
            FOLCMP_WRITE( FOLVAR_FROM_INDEX(0) );
            FOLCMP_WRITE( DFOLINT(min) );
            FOLCMP_WRITE( DFOLINT(max) );
            res = FOLCMP_WRITE_STOP;
            Sk(res) = LSTACK_PUSH_VOID;
            return Unify( res, Sk(res), Shifted_Range, Sk(Shifted_Range) );
        }
    } else
        Fail;
}

Bool /* DyALog Wrapper */
DYAM_Shift_Range_4( fol_t Left, fol_t Right, fol_t Range, fol_t Shifted_Range)
{
    fkey_t k=R_TRANS_KEY;
    return Shift_Range_4(Left,k,Right,k,Range,k,Shifted_Range,k);
}

/**********************************************************************
 * DyALog_Persistent - to persist from one iteration to the next one
 **********************************************************************/

dyalog_mutable dyalog_persistent;

void
DyALog_Persistent_Set(sfol_t x) 
{
    dyalog_mutable_t m = &dyalog_persistent;
    SFOL_Deref(x);
    if (!FOL_GROUNDP(x->t)) {
        Collapse_Unwrap(x->t, x->k, m->k, m->env);
        x->k=(fkey_t) load_layer_archive(m->k,m->env);
        SFOL_Deref(x);
        if (FOL_GROUNDP(x->t)){
            m->k=0;
            m->env=(obj_t)0;
        }
    }
    m->t=x->t;
}

Bool
DyALog_Persistent_Read( sfol_t x )
{
    dyalog_mutable_t m=&dyalog_persistent;
    fkey_t k = Key0;
    if (!FOL_GROUNDP(m->t))
        k = (fkey_t) load_layer_archive(m->k,m->env);
    return Unify(x->t,x->k,m->t,k);
}

dyalog_mutable_t dyalog_persistent_list=0;

void
DyALog_Persistent_Add(sfol_t x) 
{
    dyalog_mutable_t cell = (dyalog_mutable_t) GC_MALLOC(sizeof(struct dyalog_mutable));
    SFOL_Deref(x);
    if (!FOL_GROUNDP(x->t)) {
        Collapse_Unwrap(x->t, x->k, cell->k, cell->env);
        x->k=(fkey_t) load_layer_archive(cell->k,cell->env);
        SFOL_Deref(x);
        if (FOL_GROUNDP(x->t)){
            cell->k=0;
            cell->env=(obj_t)0;
        }
    }
    cell->t = x->t;
    cell->next = dyalog_persistent_list;
    dyalog_persistent_list = cell;
}

Bool
DyALog_Persistent_Enumerate( sfol_t x) 
{
    int n = Get_Choice_Counter();
    dyalog_mutable_t *m = Get_Choice_Buffer(dyalog_mutable_t *);
    dyalog_mutable_t cell = n ? *m : dyalog_persistent_list;
    if (cell) {
        fkey_t k = Key0;
        if (!FOL_GROUNDP(cell->t))
            k = (fkey_t) load_layer_archive(cell->k,cell->env);
        *m = cell->next;
        return Unify(x->t,x->k,cell->t,k);
     } else {
        No_More_Choice();
        Fail;
    }
}

extern tabobj_t rt_install_database( fol_t, fkey_t );

void
DyALog_Persistent_Restore() 
{
    dyalog_mutable_t m = dyalog_persistent_list;
    for(; m ; m = m->next) {
        if (FOL_GROUNDP(m->t)) {
            rt_install_database(m->t,Key0);
        } else {
            Trail;
            rt_install_database(m->t,(fkey_t) load_layer_archive(m->k,m->env));
            Untrail;
        }
    }
    dyalog_persistent_list=0;
}

