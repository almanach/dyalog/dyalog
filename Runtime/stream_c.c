/*************************************************************
 * $Id$
 * Copyright (C) 1997, 2004, 2016 Eric de la Clergerie
 * ------------------------------------------------------------
 *
 *    Stream Builtins --
 *
 * ------------------------------------------------------------
 * Description
 *    Prolog builtins for streams. This file is adapted for DyALOg
 *    from afile of the wamcc distribution (see following header)
 * ------------------------------------------------------------
 */

/*-------------------------------------------------------------------------*/
/* Buit-In Predicates                                   Daniel Diaz - 1996 */
/* Stream selection and control management - C part                        */
/*                                                                         */
/* stream_c.c                             Copyright (C) 1996, INRIA France */
/*-------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>

#include "hash.h"

#include "libdyalog.h"
#include "builtins.h"

#define Deref_Stm_Or_Fail( stm, word )			\
({						\
  Deref_And_Fail_Unless( word, FOLINTP(word) ); \
  stm = CFOLINT(word);				\
})


extern char *M_Absolute_File_Name( const char *);

/*---------------------------------*/
/* Constants                       */
/*---------------------------------*/

#define STREAM_WRITE_LIST_BLOCK    1024


          /* Error Messages */

#define ERR_ALLOC_FAULT            "stream_c: Memory allocation fault"

/*---------------------------------*/
/* Global Variables                */
/*---------------------------------*/

/*---------------------------------*/
/* Function Prototypes             */
/*---------------------------------*/

/*-------------------------------------------------------------------------*/
/* CURRENT_INPUT_1                                                         */
/*                                                                         */
/*-------------------------------------------------------------------------*/

static Bool Current_Input_1(Sproto);

Bool              /* DyALog Wrapper */
DYAM_Current_Input_1(fol_t stm)
{
    fkey_t k=R_TRANS_KEY;
    return Current_Input_1(stm,k);
}

static Bool	  /* DyALog Builtin */
Current_Input_1( SP(stm) )
{
 return Get_Integer(DFOLINT(stm_input),stm);
}

/*-------------------------------------------------------------------------*/
/* CURRENT_OUTPUT_1                                                        */
/*                                                                         */
/*-------------------------------------------------------------------------*/

static Bool Current_Output_1(Sproto);

Bool              /* DyALog Wrapper */
DYAM_Current_Output_1(fol_t stm)
{
    fkey_t k=R_TRANS_KEY;
    return Current_Output_1(stm,k);
}

static Bool	  /* DyALog Builtin */
Current_Output_1( SP(stm) )
{
 return Get_Integer(DFOLINT(stm_output),stm);
}

/*-------------------------------------------------------------------------*/
/* SET_INPUT_1                                                             */
/*                                                                         */
/*-------------------------------------------------------------------------*/

static Bool Set_Input_1(Sproto);

Bool              /* DyALog Wrapper */
DYAM_Set_Input_1(fol_t sora)
{
    fkey_t k=R_TRANS_KEY;
    return Set_Input_1(sora,k);
}

static Bool              /* DyALog Builtin */
Set_Input_1( SP(sora) )
{
    stm_input=Get_Stream_Or_Alias( sora, k_sora, STREAM_FOR_INPUT);
    Succeed;
}

/*-------------------------------------------------------------------------*/
/* SET_OUTPUT_1                                                            */
/*                                                                         */
/*-------------------------------------------------------------------------*/

static Bool Set_Output_1(Sproto);

Bool              /* DyALog Wrapper */
DYAM_Set_Output_1(fol_t sora)
{
    fkey_t k=R_TRANS_KEY;
    return Set_Output_1(sora,k);
}

static Bool              /* DyALog Builtin */
Set_Output_1( SP(sora) )
{
    stm_output=Get_Stream_Or_Alias(sora, k_sora, STREAM_FOR_OUTPUT);
    Succeed;
}

/*-------------------------------------------------------------------------*/
/* OPEN_3                                                                  */
/*                                                                         */
/*-------------------------------------------------------------------------*/

Bool Open_3(Sproto,Sproto,Sproto);

Bool              /* DyALog Wrapper */
DYAM_Open_3(fol_t source_sink,fol_t mode,fol_t stm)
{
    fkey_t k=R_TRANS_KEY;
    return Open_3(source_sink,k,mode,k,stm,k);
}

Bool	  /* DyALog Builtin */
Open_3( SP(source_sink_word), SP(mode_word), SP(stm_word) )
{

#define SYS_VAR_OPTION_MASK 1

 StmProp  prop;
 char    *path;
 fol_t    atom_file_name;
 fol_t    atom;
 int      stm;
 char     open_str[10];
 FILE    *f;
 int      mask=SYS_VAR_OPTION_MASK;


 Deref_And_Fail_Unless(source_sink_word,
		       FOLSMBP( source_sink_word ) &&
		       (path = M_Absolute_File_Name( SymbolName( source_sink_word ))) != NULL )

 atom_file_name=Create_Allocate_Atom(path);

 Deref_And_Fail_Unless(mode_word, FOLSMBP(mode_word));
 atom=mode_word;

 if (atom==atom_read) {
   prop.mode  =STREAM_MODE_READ;
   prop.input =TRUE;
   prop.output=FALSE;
   strcpy(open_str,"r");
 } else if (atom==atom_write) {
   prop.mode  =STREAM_MODE_WRITE;
   prop.input =FALSE;
   prop.output=TRUE;
   strcpy(open_str,"w");
 } else if (atom==atom_append) {
   prop.mode  =STREAM_MODE_APPEND;
   prop.input =FALSE;
   prop.output=TRUE;
   strcpy(open_str,"a");
 } else
   Fail;

 prop.text=mask & 1; 
 mask>>=1;

 strcat(open_str,(prop.text) ? "t" : "b");
 
 if ((f=fopen(SymbolName(atom_file_name),open_str))==NULL)
   Fail;

 prop.tty=isatty(fileno(f));

 if ((mask & 2)==0)                      /* not specified - set to default */
   prop.reposition=!(prop.tty);
 else
   prop.reposition=mask & 1;
 mask>>=2;

 if ((mask & 4)==0)                      /* not specified - set to default */
   prop.eof_action=(prop.tty) ? STREAM_EOF_ACTION_RESET : STREAM_EOF_ACTION_EOF_CODE;
 else
   prop.eof_action=mask & 3;

 if (prop.reposition) {
   if (fseek(f,0,SEEK_END)!=0 && errno!=0)
     return FALSE;
   fseek(f,0,SEEK_SET);
 }

 prop.other=0;                                               /* not a list */

 stm=Add_Stream(atom_file_name,(long) f,prop,
                NULL,NULL,NULL,NULL,NULL,NULL,NULL);

 Get_Integer(DFOLINT(stm),stm_word);
 return TRUE;
}

/*-------------------------------------------------------------------------*/
/* TEST_ALIAS_NOT_ASSIGNED_1                                               */
/*                                                                         */
/*-------------------------------------------------------------------------*/

static Bool Test_Alias_Not_Assigned_1(Sproto);

Bool              /* DyALog Wrapper */
DYAM_Test_Alias_Not_Assigned_1(fol_t alias)
{
    fkey_t k=R_TRANS_KEY;
    return Test_Alias_Not_Assigned_1(alias,k);
}

static Bool	  /* DyALog Builtin */
Test_Alias_Not_Assigned_1( SP(alias) )
{
 Deref(alias);
 return Find_Stream_By_Alias(alias)<0;
}

/*-------------------------------------------------------------------------*/
/* FROM_ALIAS_TO_STREAM                                                    */
/*                                                                         */
/*-------------------------------------------------------------------------*/

static Bool From_Alias_To_Stream(Sproto,Sproto);

Bool              /* DyALog Wrapper */
DYAM_From_Alias_To_Stream(fol_t alias,fol_t stm)
{
    fkey_t k=R_TRANS_KEY;
    return From_Alias_To_Stream(alias,k,stm,k);
}

static Bool	  /* DyALog Builtin */
From_Alias_To_Stream( SP(alias_word), SP(stm_word) )
{
 int     stm;

 Deref(alias_word);
 stm=Find_Stream_By_Alias(alias_word);

 return stm>=0 && Get_Integer(DFOLINT(stm),stm_word);
}

/*-------------------------------------------------------------------------*/
/* ADD_STREAM_ALIAS_2                                                      */
/*                                                                         */
/*-------------------------------------------------------------------------*/

static Bool Add_Stream_Alias_2(Sproto,Sproto);

Bool              /* DyALog Wrapper */
DYAM_Add_Stream_Alias_2(fol_t sora,fol_t alias)
{
    fkey_t k=R_TRANS_KEY;
    return Add_Stream_Alias_2(sora,k,alias,k);
}

static Bool	  /* DyALog Builtin */
Add_Stream_Alias_2( SP(sora), SP(alias) )
{
 int       stm;

 stm=Get_Stream_Or_Alias(sora,k_sora,STREAM_NOTHING);

 Deref_And_Fail_Unless(alias, FOLSMBP(alias));
     
 return Add_Alias_To_Stream(alias,stm);
}

/*-------------------------------------------------------------------------*/
/* SET_STREAM_EOF_ACTION_2                                                 */
/*                                                                         */
/*-------------------------------------------------------------------------*/


static Bool Set_Stream_Eof_Action_2(Sproto,Sproto);

Bool              /* DyALog Wrapper */
DYAM_Set_Stream_Eof_Action_2(fol_t sora,fol_t action)
{
    fkey_t k=R_TRANS_KEY;
    return Set_Stream_Eof_Action_2(sora,k,action,k);
}

static Bool	  /* DyALog Builtin */
Set_Stream_Eof_Action_2( SP(sora), SP(action) )
{
 int     stm;
 StmInf  *pstm;

 stm=Get_Stream_Or_Alias(sora,k_sora,STREAM_NOTHING);
 pstm=stm_tbl+stm;

 Deref(action);

 pstm->prop.eof_action=CFOLINT( action );

 Succeed;
 
}

/*-------------------------------------------------------------------------*/
/* CLOSE_1                                                                 */
/*                                                                         */
/*-------------------------------------------------------------------------*/

Bool Close_1(Sproto);

Bool              /* DyALog Wrapper */
DYAM_Close_1(fol_t sora)
{
    fkey_t k=R_TRANS_KEY;
    return Close_1(sora,k);
}

Bool	  /* DyALog Builtin */
Close_1( SP(sora) )
{
 int      stm;
 StmInf  *pstm;
 int      mask=SYS_VAR_OPTION_MASK;

 stm=Get_Stream_Or_Alias(sora,k_sora,STREAM_NOTHING);

 pstm=stm_tbl+stm;
     
 if (stm==stm_stdin || stm==stm_stdout)
     Succeed;
 
 if (stm==stm_input)
     stm_input=stm_stdin;
  else if (stm==stm_output)
    stm_output=stm_stdout;

 if (Stream_Close(pstm)!=0 && (mask & 1)==0)
   Fatal_Error( "how to close" );           /* else should force close (how ?) */

 Remove_Stream(stm);
 
 Succeed;

}

/*-------------------------------------------------------------------------*/
/* FLUSH_OUTPUT_1                                                          */
/*                                                                         */
/*-------------------------------------------------------------------------*/

static Bool Flush_Output_1(Sproto);

Bool              /* DyALog Wrapper */
DYAM_Flush_Output_1(fol_t sora)
{
    fkey_t k=R_TRANS_KEY;
    return Flush_Output_1(sora,k);
}

static Bool	  /* DyALog Builtin */
Flush_Output_1( SP(sora) )
{
 int stm;

 stm=(sora==NOT_A_WAM_WORD || sora == FOLNIL) 
               ? stm_output
               : Get_Stream_Or_Alias(sora,k_sora,STREAM_FOR_OUTPUT);

 last_output_sora=sora;

 Stream_Flush(stm_tbl+stm);
 Succeed;
}

/*-------------------------------------------------------------------------*/
/* FLUSH_OUTPUT_0                                                          */
/*                                                                         */
/*-------------------------------------------------------------------------*/


Bool Flush_Output_0();

Bool              /* DyALog Wrapper */
DYAM_Flush_Output_0()
{
    return Flush_Output_0();
}

Bool	  /* DyALog Builtin */
Flush_Output_0()
{
    return Flush_Output_1(NOT_A_WAM_WORD, Key0);
}

/*-------------------------------------------------------------------------*/
/* STREAM_PROP_FILE_NAME_2                                                 */
/*                                                                         */
/*-------------------------------------------------------------------------*/

static Bool Stream_Prop_File_Name_2(Sproto,Sproto);

Bool              /* DyALog Wrapper */
DYAM_Stream_Prop_File_Name_2(fol_t file_name,fol_t stm)
{
    fkey_t k=R_TRANS_KEY;
    return Stream_Prop_File_Name_2(file_name,k,stm,k);
}

static Bool	  /* DyALog Builtin */
Stream_Prop_File_Name_2( SP(file_name), SP(stm_word) )
{
 int     stm;
 Deref_Stm_Or_Fail(stm,stm_word);
 return Get_Cst(stm_tbl[stm].atom_file_name,file_name);
}

/*-------------------------------------------------------------------------*/
/* STREAM_PROP_MODE_2                                                      */
/*                                                                         */
/*-------------------------------------------------------------------------*/

static Bool Stream_Prop_Mode_2(Sproto,Sproto);

Bool              /* DyALog Wrapper */
DYAM_Stream_Prop_Mode_2(fol_t mode,fol_t stm)
{
    fkey_t k=R_TRANS_KEY;
    return Stream_Prop_Mode_2(mode,k,stm,k);
}

static Bool	  /* DyALog Builtin */
Stream_Prop_Mode_2( SP(mode_word), SP(stm_word) )
{
 int     stm;
 fol_t    atom;

 Deref_Stm_Or_Fail(stm,stm_word);
 switch(stm_tbl[stm].prop.mode)
    {
     case STREAM_MODE_READ:
         atom=atom_read;
         break;

     case STREAM_MODE_WRITE:
         atom=atom_write;
         break;

     case STREAM_MODE_APPEND:
         atom=atom_append;
         break;

        default:
            Fail;
            break;
            
    }

 return Get_Cst(atom,mode_word);
}

/*-------------------------------------------------------------------------*/
/* STREAM_PROP_INPUT_1                                                     */
/*                                                                         */
/*-------------------------------------------------------------------------*/

static Bool Stream_Prop_Input_1(Sproto);

Bool              /* DyALog Wrapper */
DYAM_Stream_Prop_Input_1(fol_t stm_word)
{
    fkey_t k=R_TRANS_KEY;
    return Stream_Prop_Input_1(stm_word,k);
}

static Bool	  /* DyALog Builtin */
Stream_Prop_Input_1( SP(stm_word) )
{
 int     stm;
 Deref_Stm_Or_Fail(stm,stm_word);
 return stm_tbl[stm].prop.input;
}

/*-------------------------------------------------------------------------*/
/* STREAM_PROP_OUTPUT_1                                                    */
/*                                                                         */
/*-------------------------------------------------------------------------*/

static Bool Stream_Prop_Output_1(Sproto);

Bool              /* DyALog Wrapper */
DYAM_Stream_Prop_Output_1(fol_t stm_word)
{
    fkey_t k=R_TRANS_KEY;
    return Stream_Prop_Output_1(stm_word,k);
}

static Bool	  /* DyALog Builtin */
Stream_Prop_Output_1( SP(stm_word) )
{
 int     stm;
 Deref_Stm_Or_Fail(stm, stm_word);
 return stm_tbl[stm].prop.output;
}

/*-------------------------------------------------------------------------*/
/* STREAM_PROP_END_OF_STREAM_2                                             */
/*                                                                         */
/*-------------------------------------------------------------------------*/

static Bool Stream_Prop_End_Of_Stream_2(Sproto,Sproto);

Bool              /* DyALog Wrapper */
DYAM_Stream_Prop_End_Of_Stream_2(fol_t end_of_stream_word,fol_t stm_word)
{
    fkey_t k=R_TRANS_KEY;
    return Stream_Prop_End_Of_Stream_2(end_of_stream_word,k,stm_word,k);
}

static Bool	  /* DyALog Builtin */
Stream_Prop_End_Of_Stream_2( SP(end_of_stream_word), SP(stm_word) )
{
 int     stm;
 Deref_Stm_Or_Fail(stm,stm_word);
 return Get_Integer(DFOLINT(Stream_End_Of_Stream(stm_tbl+stm)),end_of_stream_word);
}

/*-------------------------------------------------------------------------*/
/* STREAM_PROP_EOF_ACTION_2                                                */
/*                                                                         */
/*-------------------------------------------------------------------------*/

static Bool Stream_Prop_Eof_Action_2(Sproto,Sproto);

Bool              /* DyALog Wrapper */
DYAM_Stream_Prop_Eof_Action_2(fol_t eof_action_word,fol_t stm_word)
{
    fkey_t k=R_TRANS_KEY;
    return Stream_Prop_Eof_Action_2(eof_action_word,k,stm_word,k);
}

static Bool	  /* DyALog Builtin */
Stream_Prop_Eof_Action_2( SP(eof_action_word), SP(stm_word) )
{
 int     stm;
 Deref_Stm_Or_Fail(stm,stm_word);
 return Get_Integer(DFOLINT(stm_tbl[stm].prop.eof_action),eof_action_word);
}

/*-------------------------------------------------------------------------*/
/* STREAM_PROP_REPOSITION_2                                                */
/*                                                                         */
/*-------------------------------------------------------------------------*/

static Bool Stream_Prop_Reposition_2(Sproto,Sproto);

Bool              /* DyALog Wrapper */
DYAM_Stream_Prop_Reposition_2(fol_t reposition_word, fol_t stm_word)
{
    fkey_t k=R_TRANS_KEY;
    return Stream_Prop_Reposition_2(reposition_word,k,stm_word,k);
}

static Bool	  /* DyALog Builtin */
Stream_Prop_Reposition_2( SP(reposition_word), SP(stm_word) )
{
 int     stm;
 Deref_Stm_Or_Fail(stm, stm_word);
 return Get_Integer(DFOLINT(stm_tbl[stm].prop.reposition),reposition_word);
}

/*-------------------------------------------------------------------------*/
/* STREAM_PROP_TYPE_2                                                      */
/*                                                                         */
/*-------------------------------------------------------------------------*/

static Bool Stream_Prop_Type_2(Sproto,Sproto);

Bool              /* DyALog Wrapper */
DYAM_Stream_Prop_Type_2(fol_t type_word,fol_t stm_word)
{
    fkey_t k=R_TRANS_KEY;
    return Stream_Prop_Type_2(type_word,k,stm_word,k);
}

static Bool	  /* DyALog Builtin */
Stream_Prop_Type_2(SP(type_word), SP(stm_word))
{
 int     stm;
 Deref_Stm_Or_Fail(stm,stm_word);
 return Get_Integer(DFOLINT(stm_tbl[stm].prop.text),type_word);
}

/*-------------------------------------------------------------------------*/
/* AT_END_OF_STREAM_1                                                      */
/*                                                                         */
/*-------------------------------------------------------------------------*/

static Bool At_End_Of_Stream_1(Sproto);

Bool              /* DyALog Wrapper */
DYAM_At_End_Of_Stream_1(fol_t sora_word)
{
    fkey_t k=R_TRANS_KEY;
    return At_End_Of_Stream_1(sora_word,k);
}

static Bool	  /* DyALog Builtin */
At_End_Of_Stream_1(SP(sora_word))
{
 int stm;

 stm=(sora_word==NOT_A_WAM_WORD || sora_word == FOLNIL) 
               ? stm_input
               : Get_Stream_Or_Alias(sora_word,k_sora_word,STREAM_NOTHING);

 return Stream_End_Of_Stream(stm_tbl+stm)!=STREAM_EOF_NOT;
}

/*-------------------------------------------------------------------------*/
/* AT_END_OF_STREAM_0                                                      */
/*                                                                         */
/*-------------------------------------------------------------------------*/
Bool
DYAM_At_End_Of_Stream_0(void)
{
 return At_End_Of_Stream_1(NOT_A_WAM_WORD,Key0);
}

/*-------------------------------------------------------------------------*/
/* CURRENT_ALIAS_2                                                         */
/*                                                                         */
/*-------------------------------------------------------------------------*/

static Bool Current_Alias_2(Sproto,Sproto);

Bool              /* DyALog Wrapper */
DYAM_Current_Alias_2(fol_t stm_word,fol_t alias_word)
{
    fkey_t k=R_TRANS_KEY;
    return Current_Alias_2(stm_word,k,alias_word,k);
}

static Bool	  /* DyALog Builtin */
Current_Alias_2(SP(stm_word),SP(alias_word))
{
 int       stm;

 Deref_Stm_Or_Fail(stm,stm_word);
 Deref_And_Fail_Unless(alias_word, FOLSMBP(alias_word));
 return Find_Stream_By_Alias(alias_word)==stm;
}

  


