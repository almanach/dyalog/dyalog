#/usr/bin/perl -n

print <<EOF ;
;; Don't touch: Automatically created using $0
;; Used to link DyALog functions to C functions

EOF

while (<>) {
    next unless /\*{2}\s*DyALog\s+Builtin\s*\*{2}\s+(\w+)\s*:-\s*(\w+)\((.*?)\)/og;
    $dname=$1;
    $cname=$2;
    @args=split(/,/,$3);
    @args=map("R($_)",@args);
    $args=join(',',@args);
    print <<EOF ;

;;----------------------------------------------------------------------
;; extracted from $ARGV

pl_code global $dname
    call_c  $cname($args)
    fail_ret
    pl_ret

EOF
}

