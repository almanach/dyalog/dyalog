#ifdef __cplusplus
extern "C" {
#endif
    
/* Runtime/config.h.  Generated from config.h.in by configure.  */
/* Runtime/config.h.in.  Generated from configure.ac by autoheader.  */

/* Define to 1 if you have the <arpa/inet.h> header file. */
#define HAVE_ARPA_INET_H 1

/* Define to 1 if you have the `atexit' function. */
#define HAVE_ATEXIT 1

/* Define to 1 if you have the <dlfcn.h> header file. */
#define HAVE_DLFCN_H 1

/* Define to 1 if you don't have `vprintf' but do have `_doprnt.' */
/* #undef HAVE_DOPRNT */

/* Define to 1 if you have the <fcntl.h> header file. */
#define HAVE_FCNTL_H 1

/* Define to 1 if you have the <gc/gc.h> header file. */
#define HAVE_GC_GC_H 1

/* Define to 1 if you have the `getcwd' function. */
#define HAVE_GETCWD 1

/* Define to 1 if you have the `gethostbyaddr' function. */
#define HAVE_GETHOSTBYADDR 1

/* Define to 1 if you have the `gethostbyname' function. */
#define HAVE_GETHOSTBYNAME 1

/* Define to 1 if you have the `getpagesize' function. */
#define HAVE_GETPAGESIZE 1

/* Define to 1 if you have the `inet_ntoa' function. */
#define HAVE_INET_NTOA 1

/* Define to 1 if you have the <inttypes.h> header file. */
#define HAVE_INTTYPES_H 1

/* Define to 1 if you have the `dl' library (-ldl). */
#define HAVE_LIBDL 1

/* Define to 1 if you have the `gc' library (-lgc). */
#define HAVE_LIBGC 1

/* Define to 1 if you have the `history' library (-lhistory). */
/* #undef HAVE_LIBHISTORY */

/* Define to 1 if you have the <libintl.h> header file. */
/* #undef HAVE_LIBINTL_H */

/* Define to 1 if you have the `m' library (-lm). */
#define HAVE_LIBM 1

/* Define to 1 if you have the `readline' library (-lreadline). */
#define HAVE_LIBREADLINE 1

/* Define to 1 if you have the <malloc.h> header file. */
/* #undef HAVE_MALLOC_H */

/* Define to 1 if you have the <memory.h> header file. */
#define HAVE_MEMORY_H 1

/* Define to 1 if you have a working `mmap' system call. */
#define HAVE_MMAP 1

/* Define to 1 if you have the <netdb.h> header file. */
#define HAVE_NETDB_H 1

/* Define to 1 if you have the <netinet/in.h> header file. */
#define HAVE_NETINET_IN_H 1

/* Define to 1 if you have the `setlocale' function. */
#define HAVE_SETLOCALE 1

/* Define to 1 if you have the `socket' function. */
#define HAVE_SOCKET 1

/* Define to 1 if you have the <stddef.h> header file. */
#define HAVE_STDDEF_H 1

/* Define to 1 if you have the <stdint.h> header file. */
#define HAVE_STDINT_H 1

/* Define to 1 if you have the <stdlib.h> header file. */
#define HAVE_STDLIB_H 1

/* Define to 1 if you have the `strchr' function. */
#define HAVE_STRCHR 1

/* Define to 1 if you have the `strdup' function. */
#define HAVE_STRDUP 1

/* Define to 1 if you have the <strings.h> header file. */
#define HAVE_STRINGS_H 1

/* Define to 1 if you have the <string.h> header file. */
#define HAVE_STRING_H 1

/* Define to 1 if you have the `strndup' function. */
#define HAVE_STRNDUP 1

/* Define to 1 if you have the `strrchr' function. */
#define HAVE_STRRCHR 1

/* Define to 1 if you have the `strtol' function. */
#define HAVE_STRTOL 1

/* Define to 1 if you have the <sys/param.h> header file. */
#define HAVE_SYS_PARAM_H 1

/* Define to 1 if you have the <sys/socket.h> header file. */
#define HAVE_SYS_SOCKET_H 1

/* Define to 1 if you have the <sys/stat.h> header file. */
#define HAVE_SYS_STAT_H 1

/* Define to 1 if you have the <sys/time.h> header file. */
#define HAVE_SYS_TIME_H 1

/* Define to 1 if you have the <sys/types.h> header file. */
#define HAVE_SYS_TYPES_H 1

/* Define to 1 if you have the <unistd.h> header file. */
#define HAVE_UNISTD_H 1

/* Define to 1 if you have the `vprintf' function. */
#define HAVE_VPRINTF 1

/* Define to the sub-directory where libtool stores uninstalled libraries. */
#define LT_OBJDIR ".libs/"

/* Define M_cygwin */
/* #undef M_cygwin */

/* Define M_darwin */
#define M_darwin 1

/* Define M_ix86 */
/* #undef M_ix86 */

/* Define M_ix86_cygwin */
/* #undef M_ix86_cygwin */

/* Define M_ix86_darwin */
#define M_ix86_darwin 1

/* Define M_ix86_linux */
/* #undef M_ix86_linux */

/* Define M_linux */
/* #undef M_linux */

/* Define M_powerpc */
/* #undef M_powerpc */

/* Define M_powerpc_darwin */
/* #undef M_powerpc_darwin */

/* Define M_powerpc_linux */
/* #undef M_powerpc_linux */

/* Define M_solaris */
/* #undef M_solaris */

/* Define M_sparc */
/* #undef M_sparc */

/* Define M_sparc_solaris */
/* #undef M_sparc_solaris */

/* Define M_sparc_sunos */
/* #undef M_sparc_sunos */

/* Define M_sunos */
/* #undef M_sunos */

/* Name of package */
#define PACKAGE "DyALog"

/* Define to the address where bug reports for this package should be sent. */
#define PACKAGE_BUGREPORT "Eric.De_La_Clergerie@inria.fr"

/* Define to the full name of this package. */
#define PACKAGE_NAME "DyALog"

/* Define to the full name and version of this package. */
#define PACKAGE_STRING "DyALog 1.14.0"

/* Define to the one symbol short name of this package. */
#define PACKAGE_TARNAME "DyALog"

/* Define to the home page for this package. */
#define PACKAGE_URL ""

/* Define to the version of this package. */
#define PACKAGE_VERSION "1.14.0"

/* Define as the return type of signal handlers (`int' or `void'). */
#define RETSIGTYPE void


#ifndef STDC_HEADERS
#define EXIT_FAILURE 1
#define EXIT_SUCCESS 0
#endif

/* Define to 1 if your <sys/time.h> declares `struct tm'. */
/* #undef TM_IN_SYS_TIME */

/* Version number of package */
#define VERSION "1.14.0"

/* Define to 1 if `lex' declares `yytext' as a `char *' by default, not a
   `char[]'. */
#define YYTEXT_POINTER 1

/* Define __DYALOG_INCLUDE_PATH__ */
#define __DYALOG_INCLUDE_PATH__ "/usr/local/lib/DyALog"

/* Define __DYALOG_PACKAGE_PATH__ */
#define __DYALOG_PACKAGE_PATH__ "/Users/clergeri/Work/DyALog"

/* Define to empty if `const' does not conform to ANSI C. */
/* #undef const */

/* Define to `__inline__' or `__inline' if that's what the C compiler
   calls it, or to nothing if 'inline' is not supported under any name.  */
#ifndef __cplusplus
/* #undef inline */
#endif

/* Define to `unsigned int' if <sys/types.h> does not define. */
/* #undef size_t */



/*---------------------------------*/
/* Register Definitions            */
/*---------------------------------*/

#if defined(M_sparc_sunos)
#    define MAP_REG_BANK           "g6"
#    define Adjust_Address(p)      ((void *)((unsigned long)(p)-8))
#else
#    define Adjust_Address(p)      (p)
#endif

/*---------------------------------*/
/* Miscellaneous                   */
/*---------------------------------*/

#if defined(M_sparc_sunos)
#define __USE_FIXED_PROTOTYPES__
#endif

#define GCC_VERSION (__GNUC__ * 10000 \
                               + __GNUC_MINOR__ * 100 \
                               + __GNUC_PATCHLEVEL__)


#ifdef __cplusplus
}
#endif
