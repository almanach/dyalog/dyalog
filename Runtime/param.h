/* $Id$
 * Copyright (C) 1996, 2005, 2007, 2009, 2010, 2011, 2014, 2015, 2017 Eric de la Clergerie
 * ------------------------------------------------------------
 *
 *   Parameters -- The various parameters for DyALog
 *
 * ------------------------------------------------------------
 * Description
 *
 * ------------------------------------------------------------
 */

#ifdef __cplusplus
extern "C" {
#endif

#define FOLVAR_TAB_SIZE      32768        /* variable table size */

#define LSTACK_SIZE          16*32768        /* layer stack size    */
#define TRAIL_SIZE           64*32768*8     /* trail stack size    */
#define CTL_SIZE             2*32768*8      /* control stack size  */

#define COPY_THRESHOLD       400         /* threshold for small terms */

#define MAX_STREAM           128

/**********************************************************************
 * Statistics Mechanism
 **********************************************************************/

//#define STAT

#ifdef STAT

#define STAT_UPDATE(name,i) ((name##_stat->i)++)

#define STAT_DECL(name, entries...)		\
extern struct name##_stat {			\
  int entries;					\
} *name##_stat;

#define STAT_LIGHT_DEF(name)				\
struct name##_stat name##_stat_record;			\
struct name##_stat *name##_stat= &name##_stat_record;

#define STAT_DEF(name, entries...)		\
STAT_DECL(name, ## entries )			\
STAT_LIGHT_DEF(name)

#define STAT_USE(name,i)    (name##_stat->i)

#else
#define STAT_UPDATE(name,i)
#define STAT_DEF(name, entries...)
#define STAT_DECL(name, entries...)
#define STAT_USE(name, i)
#endif /* STAT */

/**********************************************************************
 * Verbose Mechanism
 **********************************************************************/

//extern int verbose_level;

extern Bool Flush_Output_0();
extern void dyalog_error_printf(const char*, ... );

#define V_DISPLAY(fmt, args...)      V_LEVEL_DISPLAY(V_LOW,fmt, ## args)

#define V_LEVEL_DISPLAY(level, fmt, args...)                                    \
    if (R_VERBOSE_LEVEL & level) { dyalog_printf(fmt, ## args), Flush_Output_0(); }

#define V_DYAM       1          /* Trace Dyam Instructions */
#define V_INDEX      2          /* Trace Indexation  */
#define V_SHARE      4          /* Trace Sharing (collapse and collect) */
#define V_TERM       8          /* Trace Term maninulations */
#define V_TFS       16          /* Trace Tfs */
#define V_BUILTINS  32          /* Trace Builtins */
#define V_LOW       64          /* Trace Low Level operations */

#if 0
#define V_DISPLAY(fmt, args...)                 \
    dyalog_printf( fmt, ## args );              \
    Flush_Output_0()
#endif

#ifdef __cplusplus
}
#endif
