/* $Id$
 * Copyright (C) 1996, 2004, 2017 Eric de la Clergerie
 * ------------------------------------------------------------
 *
 *   Dyalog -- Header file for DyALog
 *
 * ------------------------------------------------------------
 * Description
 *
 * ------------------------------------------------------------
 */

#ifdef __cplusplus
extern "C" {
#endif
    
#include "config.h"
#include "bigloo.h"
#include "rt.h"

#ifdef __cplusplus
}
#endif
