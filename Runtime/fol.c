/*************************************************************
 * $Id$
 * Copyright (C) 1996, 2002, 2003, 2004, 2008, 2009, 2010, 2011, 2012, 2013, 2015, 2016, 2017 Eric de la Clergerie
 * ------------------------------------------------------------
 *
 *   Init -- Intialization file
 
 * ------------------------------------------------------------
 * Description
 *    *Initialization of the different tables for fol objects
 *    *Different fol-term related operations
 * ------------------------------------------------------------
 */

/*!\file fol.c
  \brief Managing DyALog terms
 */

#include <stdio.h>

#include <stdarg.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <search.h>

#include "libdyalog.h"
#include "set.h"

/**********************************************************************
 *  Table Initializations
 **********************************************************************/

#ifdef USE_REGBANK

folvar_t folvar_tab= FOLVAR_BASE;

#else

struct folvar folvar_tab[FOLVAR_TAB_SIZE];

#endif

folsmb_t *folsmb_tab;
static unsigned long   folsmb_tab_top=0;
static unsigned long   folsmb_tab_max;

int  char_type[256]= {

/*  nul soh stx etx eot enq ack bel bs  ht  nl  vt  np  cr  so  si  */
    LA, LA, LA, LA, LA, LA, LA, LA, LA, LA, LA, LA, LA, LA, LA, LA,

/*  dle dc1 dc2 dc3 dc4 nak syn etb can em sub esc  fs  gs  rs  us  */
    LA, LA, LA, LA, LA, LA, LA, LA, LA, LA, LA, LA, LA, LA, LA, LA,

/*  spc !   "   #   $   %   &   '   (   )   *   +   ,   -   .   /   */
    LA, SC, DQ, GR, GR, CM, GR, QT, PC, PC, GR, GR, SC, GR, GR, GR,

/*  0   1   2   3   4   5   6   7   8   9   :   ;   <   =   >   ?   */
    DI, DI, DI, DI, DI, DI, DI, DI, DI, DI, GR, SC, GR, GR, GR, GR,

/*  @   A   B   C   D   E   F   G   H   I   J   K   L   M   N   O   */
    GR, CL, CL, CL, CL, CL, CL, CL, CL, CL, CL, CL, CL, CL, CL, CL,

/*  P   Q   R   S   T   U   V   W   X   Y   Z   [   \   ]   ^   _   */
    CL, CL, CL, CL, CL, CL, CL, CL, CL, CL, CL, PC, GR, PC, GR, UL,

/*  `   a   b   c   d   e   f   g   h   i   j   k   l   m   n   o    */
    BQ, SL, SL, SL, SL, SL, SL, SL, SL, SL, SL, SL, SL, SL, SL, SL,

/*  p   q   r   s   t   u   v   w   x   y   z   {   |   }   ~   del  */
    SL, SL, SL, SL, SL, SL, SL, SL, SL, SL, SL, PC, PC, PC, GR, LA 

/*  0x80 ... 0xff = EX (set by Init_Atom)"                           */
    };


char escape_symbol[]="abfnrtv";
char escape_char  []="\a\b\f\n\r\t\v";

void * smb_root;               /* root of balanced tree for symbols */
struct folsmb tmp_smb;          /* tmp symbol struct to insert into smb tree */

/**********************************************************************
 *    init_folsmb ...                                                  
 **********************************************************************/

static void initialize_folsmb_table()
{
    folsmb_tab_max = 4096;
//    printf("Initialize folsmb table to size=%d\n",folsmb_tab_max);
    folsmb_tab = (folsmb_t *)
        GC_MALLOC_IGNORE_OFF_PAGE(folsmb_tab_max*sizeof(folsmb_t));
    folsmb_tab[folsmb_tab_top]=&tmp_smb;
}

static
void realloc_folsmb_table()
{
    folsmb_t *old = folsmb_tab;
    long max = folsmb_tab_max * 2;
    folsmb_t *new = (folsmb_t *)
        GC_MALLOC_IGNORE_OFF_PAGE(max*sizeof(folsmb_t));
//    printf("Realloc folsmb table to size=%d\n",max);
    memcpy(new,folsmb_tab,folsmb_tab_max*sizeof(folsmb_t));
    GC_FREE(old);
    folsmb_tab = new;
    folsmb_tab_max = max;
}

/**********************************************************************
 *    fill_folsmb ...                                                  
 **********************************************************************/

//extern  char *LE_Completion_Add_Word(const char *,int);

inline static
unsigned long
smb_key(const char *name) 
{
    const unsigned char *c=(unsigned char *)name;
    unsigned long key=0;
    for(; *c ; ) {
//        key = (key<<4)^(key>>28)^(*c++);
//        key = (key<<3)^(key>>62)^((unsigned int)*c++);
//        key = (key >> 6) + ((unsigned int)*c++ << 24);
        key = (key >> 6) | ((unsigned int)*c++ << 23) ;
    }
    return key;
}

/* static long */
/* reverse_strcmp(const char *s1,const char *s2)  */
/* { */
/*     long n1 = strlen(s1); */
/*     long n2 = strlen(s2); */
/*     long r = n1-n2; */
/*     char *x1; */
/*     char *x2; */
/*     if (r) return r; */
/*         /\* same length *\/ */
/*     for(x1=s1+n1, x2=s2+n2; x1 >= s1 && !(r=(*x1--)-(*x2--));); */
/*     return r; */
/* } */

static
unsigned long
fill_folsmb( const char *name, const fol_t module, long key )
{
  int lg =strlen( name );
  char *p;
  folsmb_t smb=(folsmb_t) GC_MALLOC_PRINTF("folsmb",sizeof( struct folsmb));
  int       c_type;
  Bool identifier;
  Bool graphic;
  
  smb->name=(char *)GC_MALLOC_ATOMIC( lg + 1 );
  strcpy( smb->name, name );
  smb->to_quote=0;
  smb->to_scan=0;
  smb->module=module;
  smb->key = key;
  
  identifier=graphic=(*name!='\0');
      
  for(p=(char *)name;*p;p++){
      c_type=char_type[(unsigned char) *p];
      
      if ((c_type & (UL | CL | SL | DI))==0)
          identifier=0;
      
      if (c_type!=GR)
          graphic=0;

      if ((*p!=' ' && (c_type & (QT | EX | LA))) || *p=='\\')
          smb->to_scan=1;
  }

  if (char_type[(unsigned char) *name]!=SL)                 /* small letter */
      identifier=0;
  
  if (identifier){
      smb->to_quote=0;
  } else if (graphic){
      smb->to_quote=(lg==1 && *name=='.');
  } else if (lg==1 && char_type[(unsigned char) *name]==SC){
      smb->to_quote=(*name==',' || *name == '!');
  } else {
      smb->to_quote=smb->to_scan ||
          !(lg==2 && ((name[0]=='[' &&  name[1]==']')||
                      (name[0]=='{' &&  name[1]=='}')));
  }
  
#if 0
  if ( lg>1 && isalnum(*name) )
      LE_Completion_Add_Word(name,lg);
#endif
      /* Install info in folsmb_tab */
//  printf("Install symbol %d -> %s\n",folsmb_tab_top,smb->name);
  if (folsmb_tab_top > (folsmb_tab_max/2)) {
      realloc_folsmb_table();
  }
  folsmb_tab[folsmb_tab_top] = smb;
  folsmb_tab[folsmb_tab_top+1]=&tmp_smb;
  return folsmb_tab_top++;
}

int Dyalog_Smb_Number () 
{
    int n = folsmb_tab_top - 10;
    if (n < 0)
        n = 0;
    for(; n < folsmb_tab_top; n++) {
        folsmb_t smb = folsmb_tab[n];
        printf("symbol %d %s\n",n,smb->name);
    }
    return folsmb_tab_top;
}

static int
smb_compare(const unsigned long a,const unsigned long b) 
{
    folsmb_t sa = folsmb_tab[a];
    folsmb_t sb = folsmb_tab[b];
    int r=0;

    return (r=(sa->key-sb->key)) ? r
        : (r=strcmp(sa->name,sb->name)) ? r
        : (long) (sa->module) - (long)(sb->module);
        /*
    return (r=(sa->key-sb->key)) ? r
        : (r=reverse_strcmp(sa->name,sb->name)) ? r
        : (long) (sa->module) - (long)(sb->module);
        */
}

/**********************************************************************
 *    find_folsmb ...                                                  
 **********************************************************************/

typedef int (*search_fn)(const void *,const void *);

fol_t
find_module_folsmb( const char *name, const unsigned short arity, const fol_t module )
{
  unsigned long pos=0;
  long key = smb_key((char *)name);
  folsmb_tab[folsmb_tab_top]->name=(char *)name;
  folsmb_tab[folsmb_tab_top]->module=module;
  folsmb_tab[folsmb_tab_top]->key=key;
  
  pos = *(unsigned long *) tsearch((const void*)folsmb_tab_top,&smb_root,(search_fn)&smb_compare);
  
//  printf("Try insert smb %s at %d->%d\n",name,folsmb_tab_top,pos);  
  return FOLSMB_MAKE( (pos < folsmb_tab_top ? pos : fill_folsmb( name,module,key )) , arity );
}

fol_t
find_folsmb(const char *name, const unsigned short arity) 
{
    return find_module_folsmb(name,arity,FOLNIL);
}

/**********************************************************************
 *    is_folsmb ...                                                    
 **********************************************************************/


obj_t
is_module_folsmb( const char *name, const unsigned short arity, const fol_t module )
{
  unsigned long pos=0;
  folsmb_tab[folsmb_tab_top]->name=(char *)name;
  folsmb_tab[folsmb_tab_top]->module=module;
  folsmb_tab[folsmb_tab_top]->key=smb_key((char *)name);

  pos = *(unsigned long *) tfind((const void*)folsmb_tab_top,&smb_root,(search_fn)&smb_compare);
/*
  while (pos < folsmb_tab_top && strcmp(folsmb_tab[pos]->name, name))
     pos++;
*/
  return (pos < folsmb_tab_top ? (obj_t)FOLSMB_MAKE( pos, arity) : BFALSE);
}

obj_t
is_folsmb( const char *name, const unsigned short arity)
{
    return is_module_folsmb(name,arity,FOLNIL);
}


/**********************************************************************
 *    fill_folvar ...                                                  
 **********************************************************************/
inline
static void
fill_folvars()
{
  unsigned long i;
  for(i=0; i < FOLVAR_TAB_SIZE; i++){
    folvar_tab[i].index = i;
    folvar_tab[i].unifbindings =  (void *)0;
    folvar_tab[i].subbindings  =  (void *)0;
  };
}

/**********************************************************************
 *     c_compute_info
 **********************************************************************/
static long
list_length( obj_t l)
{
  long i = 0;
  for (; PAIRP(l); l = CDR(l), i++);
  return i;
}

static inline long
folcmp_compute_length(fol_t functor, fol_t last_arg)
{
  long l = 0;

  if (FOL_CSTP(last_arg))
    return 1;
  else if (FOL_DEREFP(last_arg))
    return -1;
  else if (functor != FOLCMP_FUNCTOR( last_arg ))
    return 1;
  else if ((l = FOLINFO_LENGTH( last_arg )) > 0)
    return l+1;
  else
    return l-1;
}

static fol_t
c_compute_info(fol_t t)
{
  unsigned short arity    = FOLCMP_ARITY( t );
  fol_t functor  = FOLCMP_FUNCTOR( t );
  fol_t last_arg = FOLCMP_REF( t, arity);
  obj_t tuple    = BNIL;
  long  l        = folcmp_compute_length(functor,last_arg);
  unsigned long weight   = 0;
  unsigned long copy     = 0;
  Bool generic = 1;
  unsigned short i        = 0;

  for(; i <= arity; i++){
    fol_t arg       = FOLCMP_REF(t, i);
    obj_t arg_tuple = FOL_TUPLE( arg );
    tuple    = set_union( tuple, arg_tuple);
    weight  += FOL_WEIGHT(arg) + list_length(arg_tuple);
    copy    += FOL_COPY_COST(arg);
    generic &= FOLVARP( arg );
  }

  i = list_length(tuple);
  weight  -=  i;
  generic &=  (arity == i );
  if (copy != 0) copy += arity;
  
  FOLINFO_FILL( t, tuple, weight, copy, l, generic );
  
  return t;
}

/**********************************************************************
 *     folcmp_make
 **********************************************************************/
fol_t
folcmp_make( unsigned short a, fol_t functor, ...)
{
  va_list args;

  FOLCMP_WRITE_START( functor, a );

  va_start( args, functor );
  for (; a ; a--)
    FOLCMP_WRITE( va_arg(args,fol_t) );

  va_end(args);

  return FOLCMP_WRITE_STOP;

}

/**********************************************************************
 *     folcmp table
 **********************************************************************/

//#define HNODE_TAG( o )          POINTER_TO_DYALOG(o)
//#define HNODE_UNTAG( o )        (fol_t)DYALOG_TO_POINTER(o)

//#define HNODE_TAG( o )          ((unsigned long)o|TAG_INT)
//#define HNODE_UNTAG( o )        (fol_t)(((unsigned long)o)-TAG_INT)

#define HNODE_TAG(o)        (o)
#define HNODE_UNTAG(o)   (fol_t)(o)

typedef struct weakptr_s {
    fol_t ptr;
} *weakptr;

typedef struct hash_node {
//    fol_t weakptr;              /* disguised pointer to a fol compound term */
    weakptr weakptr;
    struct hash_node *next;
} *hash_node_t;

typedef struct hash_table {
  unsigned long size;       		/* number of buckets */
  unsigned long nb_elem;	        /* nunber of entries */
  hash_node_t table;		        /* 1st entry in the table */
} *hash_table_t;

#define H_TABLE_SIZE            sizeof( struct hash_table )

#define FOLCMP_TABLE_SIZE       4096
#define INIT_FOLCMP_SIZE        (H_TABLE_SIZE + FOLCMP_TABLE_SIZE * sizeof( fol_t))

static hash_table_t _folcmp_table_;

#ifdef STAT

STAT_DEF(folcmp,requests,collisions,relocations,hits);

static void stat_display() __attribute__ ((destructor));

#define V(entry) STAT_USE(folcmp,entry)
void
stat_display()
{
  printf(
	 
	 "----------------------------------------------------------------------\n"
	 "STAT <folcmp hash>\n"
	 "\trequests    %6d\n"
	 "\tcollisions  %6d\n"
	 "\trelocations %6d\n"
	 "\thits        %6d\n"
	 "----------------------------------------------------------------------\n",
	 
	 V(requests),
	 V(collisions),
	 V(relocations),
	 V(hits)
	 );
}

#undef V

#endif

    /* allocate a hash node holding a disguised weakpointer to o
       when o is collected, the weak pointer is set to 0
       the hash node can then be identified as removable (weakptr=0)
     */
inline static hash_node_t
hnode_allocate( fol_t o, hash_node_t next )
{
    void *base = (void *) GC_base((void *)o);
    hash_node_t node = (hash_node_t) GC_MALLOC_PRINTF("folcmp hash node",sizeof( struct hash_node) );
    weakptr weakptr = (void *) GC_MALLOC_ATOMIC(sizeof(struct weakptr_s));
    node->weakptr = weakptr;
    weakptr->ptr = HNODE_TAG(o);
    node->next = next;
    _folcmp_table_->nb_elem++;
    GC_general_register_disappearing_link((void *)&(weakptr->ptr),base);
    return node;
}

    /* return value of node, removing disguise */
inline static fol_t
hnode_value( hash_node_t node)
{
    return (node->weakptr->ptr) ? HNODE_UNTAG(node->weakptr->ptr) : (fol_t) 0;
}

    /* update table by removing useless nodes */
inline static void
hnode_advance_first( hash_node_t * table )
{
//    hash_node_t node= *table;
//    for(; node && !node->weakptr->ptr;  _folcmp_table_->nb_elem--, *table = node = node->next);
    for(; *table && !(*table)->weakptr->ptr;  _folcmp_table_->nb_elem--, *table = (*table)->next);
}

    /* return first nonempty successor of node, cleaning useless intermediary ones */
inline static void
hnode_advance_next( hash_node_t node )
{
    hnode_advance_first(&(node->next));
}

/**********************************************************************
 *     hash_folcmp
 **********************************************************************/
inline
static unsigned long
hash_folcmp(const unsigned long arity, const fol_t o)
{
  unsigned long hash=0;
  const fol_t *arg=  &FOLCMP_REF( o, 0);
  const fol_t *stop= &FOLCMP_REF( o, arity);
  
  for(; arg <= stop ; ){
          /* hash = 97*hash + ((* (unsigned long *)arg >> 2)); */
      unsigned long v = ((unsigned long) *arg++);
      if (FOLSMBP((fol_t)v)) {
          v = FOLSMB_INDEX((fol_t)v);
      } else if (FOLCMPP((fol_t)v)) {
          v >>= 4;
      } else {
          v >>= 2;
      }
//      hash = 23*hash + v;
      hash = (hash << 3) + hash + v + (hash >> 57);
//      hash = 9*hash + (hash >> 57) + v;
  }
  return hash;
}


/**********************************************************************
 *     folcmp_eq ...
 **********************************************************************/
inline
static Bool
folcmp_eq( const unsigned long arity, const fol_t o1 , const fol_t o2)
{
  fol_t *arg1 = &FOLCMP_REF( o1 , 0);
  fol_t *arg2 = &FOLCMP_REF( o2 , 0);
  fol_t *stop = &FOLCMP_REF( o1 , arity) + 1;

  for(; arg1 < stop && * (long *) arg1 == * (long *) arg2; ++arg1,++arg2) ;
    
  return (arg1 == stop);
}

/**********************************************************************
 *  hash_locate
 **********************************************************************/
inline
static hash_node_t *
hash_locate(hash_node_t *table,  const unsigned long size, const fol_t key)
{
  unsigned long arity = FOLCMP_ARITY(key);

  STAT_UPDATE(folcmp,requests);

  table += ( hash_folcmp(arity,key) % size );
  hnode_advance_first(table);
  
  for( ;
       *table && !folcmp_eq(arity,hnode_value(*table),key);
       table = &((*table)->next) )
  {
      STAT_UPDATE(folcmp,collisions);
//      dyalog_printf("collision %d %&f %&f\n",arity,key,hnode_value(*table));
      hnode_advance_next(*table);
  }
  
  return table;
}

inline
static hash_node_t *
hash_relocate( hash_node_t *table,  const unsigned long size, const fol_t key)
{
        /* we assume table has no useless node */
    table += (hash_folcmp(FOLCMP_ARITY(key),key) % size);
    return table;
}

/**********************************************************************
 *    folcmp_table_clean
 **********************************************************************/

extern int GC_dl_entries;

static void
folcmp_table_clean()
{
    hash_table_t t = _folcmp_table_;
    hash_node_t *table = &(t->table);
    hash_node_t *stop =  table + (t->size);
    hash_node_t p;
//    dyalog_printf("Start GC before table cleaning\n");
//    Flush_Output_0();
    
    while ((GC_collect_a_little()));
    
//    if (t->nb_elem == GC_dl_entries)
//        return;                 /* no garbaged term  */
    
//    dyalog_printf("Cleaning folcmp table elem=%d\n",t->nb_elem);
//    Flush_Output_0();
    for(; table < stop; table++) {
        hnode_advance_first(table);
        for(p=*table; p ; p = p->next)
            hnode_advance_next(p);
    }
//    dyalog_printf("Done Cleaning elem=%d\n",t->nb_elem);
//    Flush_Output_0();

}


/**********************************************************************
 *    folcmp_table_dump
 **********************************************************************/

void
folcmp_table_dump()
{
    hash_table_t t = _folcmp_table_;
    hash_node_t *table = &(t->table);
    hash_node_t *stop =  table + (t->size);
    hash_node_t p;
//    dyalog_printf("Start GC before table cleaning\n");
//    Flush_Output_0();
    
    while ((GC_collect_a_little()));

    GC_gcollect();
    
    
//    if (t->nb_elem == GC_dl_entries)
//        return;                 /* no garbaged term  */
    
    dyalog_printf("Dumping folcmp table elem=%d\n",t->nb_elem);
    Flush_Output_0();
    for(; table < stop; table++) {
        hnode_advance_first(table);
        for(p=*table; p ; p = p->next) {
            dyalog_printf("Folcmp table %&f\n",hnode_value(p));
            hnode_advance_next(p);
        }
    }
    dyalog_printf("Done dumping elem=%d\n",t->nb_elem);
    Flush_Output_0();

}

/**********************************************************************
 *    folcmp_table_realloc
 **********************************************************************/
static void
folcmp_table_realloc()
{
  hash_table_t old_t = _folcmp_table_;
  hash_table_t new_t;
//  unsigned long        size  =  old_t->size << 3;
  unsigned long        size  =  old_t->size << 1;
  hash_node_t *old_table = &(old_t->table);
  hash_node_t *end_old_table =  old_table + (old_t->size);
  hash_node_t *table,*new;
  hash_node_t p,tmp;

  STAT_UPDATE(folcmp,relocations);
  
  new_t = (hash_table_t) GC_MALLOC_IGNORE_OFF_PAGE( H_TABLE_SIZE + size * sizeof( fol_t));
  new_t->size = size;
  new_t->nb_elem = old_t->nb_elem;

  table = &(new_t->table);
  
//  dyalog_printf("Start Realloc new size=%d\n",size);
//  Flush_Output_0();
  
      /* we assume old_table has been cleaned using folcmp_table_clean() */
  
  for ( ; old_table < end_old_table; old_table++ ){
      for( p = *old_table ; p ; p = tmp ){
          tmp= p->next;
          new = hash_relocate( table, size, hnode_value(p) );
          p->next=*new;
          *new = p;
      }
  }
  
  _folcmp_table_ = new_t;

  GC_FREE(old_t);
  
//  dyalog_printf("End Realloc\n");
//  Flush_Output_0();

}
	
/**********************************************************************
 *    hash_find
 *       look_up for a fol compound term A
 *       if found, return the found key
 *       otherwise install and return A
 **********************************************************************/
fol_t
folcmp_find( fol_t key)
{
  hash_table_t t = _folcmp_table_;
  unsigned long size = t->size;
  hash_node_t *table = &(t->table);

//  dyalog_printf("Finding %&f\n",key);
//  Flush_Output_0();
  
  table = hash_locate(table,size,key);

  if (!*table){
    unsigned long arity = FOLCMP_ARITY(key);
    fol_t functor = FOLCMP_FUNCTOR( key );
    fol_t copy_key = ALLOCATE_FOLCMP( functor, arity );

    for(; arity; arity--)
      FOLCMP_SET( copy_key, arity, FOLCMP_REF( key, arity ));
    
    key = c_compute_info(copy_key);
    
				/* Marking derefterms  */

    if (folsmb_derefterm_info(functor)
        || folsmb_derefterm_info(FOLSMB_CONVERT_ARITY(functor,0))) {
      if (!FOLVARP(FOLCMP_REF(key,1))){
	dyalog_error_printf("** ERROR ** : A variable expected as first arg of deref-marked term %&f\n",key);
	exit(EXIT_FAILURE);
      }
      FOLINFO_DEREF(key) = 1;
    }
        
//    dyalog_printf("\tallocating\n");
//    Flush_Output_0();

        // dyalog_printf("alloc [%x] %x\n",(long)table,(long)key);
    *table = hnode_allocate(key,0);

    if (t->nb_elem >= (size/4)) {
            // int n = t->nb_elem;
        folcmp_table_clean();
         if (t->nb_elem >= (size/4)) {
             folcmp_table_realloc();
         }
    }
    
  } else {
    STAT_UPDATE(folcmp,hits);
//    dyalog_printf("\tfound\n");
//    Flush_Output_0();
    key = hnode_value(*table);
  }
  
  return key;
  
}
    
/**********************************************************************
 *    hash_alloc
 **********************************************************************/
inline
static void
folcmp_table_alloc()
{
  _folcmp_table_ = (hash_table_t) GC_MALLOC( INIT_FOLCMP_SIZE );
  _folcmp_table_->size = FOLCMP_TABLE_SIZE;
  _folcmp_table_->nb_elem = 0;
}

unsigned long
hash_fol(const fol_t a) 
{
    if (FOLSMBP(a)) {
        return FOLSMB_INDEX(a);
    } else if (FOLCMPP(a)) {
        return ((unsigned long)a) >> 4;      
    } else {
        return ((unsigned long)a) >> 2;      
    }
}

/**********************************************************************
 *    term constructors
 **********************************************************************/

/*---------------------------------------------------------------------*/
/*    generic make_pair ... (from bigloo)                              */
/*---------------------------------------------------------------------*/
obj_t 
make_pair( obj_t car, obj_t cdr )
{
    obj_t pair = (obj_t) GC_MALLOC_PRINTF("pair",PAIR_SIZE);
    pair->pair_t.car    = car;
    pair->pair_t.cdr    = cdr;
    
    return BPAIR( pair );
}

fol_t
Dyam_Create_Unary( const fol_t functor, const fol_t a)
{
    V_LEVEL_DISPLAY( V_LOW,"Unary: %&f %&f\n",functor,a);
    FOLCMP_WRITE_START( FOLSMB_CONVERT_ARITY(functor,1), 1 );
    FOLCMP_WRITE( a );
    return FOLCMP_WRITE_STOP;
}

fol_t
folcmp_create_unary( const char *name, const fol_t a )
{
  FOLCMP_WRITE_START( find_folsmb(name,1), 1);
  FOLCMP_WRITE( a );
  return FOLCMP_WRITE_STOP;
}

fol_t
Dyam_Create_Binary( const fol_t functor, const fol_t a1, const fol_t a2)
{
    V_LEVEL_DISPLAY( V_LOW,"Binary: %&f %&f %&f\n",functor,a1,a2);
    FOLCMP_WRITE_START( FOLSMB_CONVERT_ARITY(functor,2), 2 );
    FOLCMP_WRITE( a1 );
    FOLCMP_WRITE( a2 );
    return FOLCMP_WRITE_STOP;
}

fol_t
folcmp_create_binary( const char *name , const fol_t a1, const fol_t a2 )
{
  return Dyam_Create_Binary( find_folsmb(name,2), a1, a2 );
}

fol_t
folcmp_create_pair( const fol_t a1 , const fol_t a2 )
{
  return Dyam_Create_Binary( FOLCONS, a1, a2 );
}

fol_t
folcmp_create_string( const char *s )
{
  char *p = strchr(s,'\0');
  fol_t o = FOLNIL;
  for (p--; s <= p ; p--)
    o = folcmp_create_pair((fol_t) BCHAR( *p ), o);
  return o;
}

fol_t
fol_create_generic_term( const fol_t smb, const unsigned long arity)
{
  if (arity == 0)
    return smb;
  else {
    unsigned long i = 1;
    FOLCMP_WRITE_START( FOLSMB_CONVERT_ARITY(smb,arity), arity );
    for( ; i <= arity ; i++ )
      FOLCMP_WRITE( FOLVAR_FROM_INDEX(i) );
    return FOLCMP_WRITE_STOP;
  }
}

fol_t
encode_extern_ptr(const void *p)
{
    FOLCMP_WRITE_START(FOLEXTERN,2);
    FOLCMP_WRITE(DFOLINT((unsigned long)p >> 8));
    FOLCMP_WRITE(DFOLINT((unsigned long)p & ((1 << 8) - 1)));
    return FOLCMP_WRITE_STOP;
}

/**********************************************************************
 *  Readline Completion
 *        - Completion on symbols
 **********************************************************************/

#if defined(HAVE_LIBREADLINE) && defined(HAVE_LIBHISTORY)

#include <readline/readline.h>
#include <readline/history.h>

char *
find_smb_completion(char *text, int state) 
{
    static int list_index,len;
    char *name;

        /* If this is a new word to complete, initialize now.  This includes
           saving the length of TEXT for efficiency, and initializing the index
           variable to 0. */
    
    if (!state)
    {
        list_index = 0;
        len = strlen(text);
    }
    
        /* Return the next name which partially matches from the command list. */
    while (list_index < folsmb_tab_top )
    {
        name=folsmb_tab[list_index]->name;
        list_index++;
        
        if (strncmp(text, name, len) == 0)
            return strdup(name);
        
    }
    
        /* If no names matched, then return NULL. */
    return ((char *)NULL);
}

char **
dyalog_completion (text, start, end)
    char *text;
    int start, end;
{
    char **matches;

    matches = rl_completion_matches(text, (rl_compentry_func_t *)find_smb_completion);
    
    return (matches);
}

static void
initialize_readline()
{
        /* Allow conditional parsing of the ~/.inputrc file. */
    rl_readline_name = "DyALog";
    
        /* Tell the completer that we want a crack first. */
    rl_attempted_completion_function = (CPPFunction *)dyalog_completion;

        /* Tell the completer not to add anything after completion */
    rl_completion_append_character='\0';

        /* Tell the completer what quotes are  */
    rl_completer_quote_characters="\'";

        /* Tell the completer that these chars induce quoting of filenames */
    rl_filename_quote_characters=".";
}

#else

static void
initialize_readline()
{
}

#endif


/**********************************************************************
 *    initialization
 **********************************************************************/

extern void Init_Machine();
extern void initialization_registers();
extern void Stream_Supp_Initializer();
extern void install_std_optable();

fol_t dyalog_true, dyalog_false;

inline static void
fill_folsmb_alt(const char *name)
{
    find_folsmb(name,0);
}

DSO_LOCAL void
fol_init()
{
  Init_Machine();
  initialization_registers();
  fill_folvars();
  initialize_folsmb_table();
  fill_folsmb_alt( "[]" );	        /* symb 0 */
  fill_folsmb_alt( "."  );	        /* symb 1 */
  fill_folsmb_alt( "{}" );	        /* symb 2 */
  fill_folsmb_alt( "apply" );	/* symb 3 */
  fill_folsmb_alt( ","  );		/* symb 4 */
  fill_folsmb_alt( ";"  );		/* symb 5 */
  fill_folsmb_alt( "$VAR" );	/* symb 6 */
  fill_folsmb_alt( "$$HOLE$$");	/* symb 7 */
  fill_folsmb_alt( "$SET$" );       /* symb 8 */
  fill_folsmb_alt( ":>" );          /* symb 9 */
  fill_folsmb_alt( "$TUPPLE" );     /* symb 10 */
  fill_folsmb_alt( "$CLOSURE" );    /* symb 11 */
  fill_folsmb_alt( "$LOOP" );       /* symb 12 */
  fill_folsmb_alt( "$EXTERN" );     /* symb 13 */
  fill_folsmb_alt( "$RANGE" );     /* symb 14 */
  fill_folsmb_alt( "$NOINDEX" );     /* symb 15 */
  folcmp_table_alloc();
  Stream_Supp_Initializer();
  install_std_optable();   /* from symbol_info.c : for declarations */
  initialize_readline();
  dyalog_true = find_folsmb("true",0);
  dyalog_false = find_folsmb("false",0);
}

/**********************************************************************
 *    Dyam Instructions
 **********************************************************************/

fol_t                           /* Dyam instruction */
Dyam_Create_Atom(const char *s)
{
    V_LEVEL_DISPLAY( V_LOW,"Atom: %s\n",s);
    return find_folsmb(s,0);
}

fol_t                           /* Dyam instruction */
Dyam_Create_Atom_Module(const char *s,const fol_t module)
{
    V_LEVEL_DISPLAY( V_LOW,"Atom: %s Module: %&f\n",s,module);
    return find_module_folsmb(s,0,module);
//    return find_folsmb(s,0);
}

void                           /* Dyam instruction */
Dyam_Term_Start( fol_t f, const int n )
{
    f = FOLSMB_MAKE(FOLSMB_INDEX(f),n);
    FOLCMP_WRITE_START(f,n);
}

void                           /* Dyam instruction */
Dyam_Term_Arg( const fol_t a )
{
    FOLCMP_WRITE(a);
    V_LEVEL_DISPLAY( V_LOW,"TermArg: %&f\n",a);
}

fol_t                           /* Dyam instruction */
Dyam_Term_End()
{
    fol_t a = FOLCMP_WRITE_STOP;
    V_LEVEL_DISPLAY( V_LOW,"Term: %&f\n",a);
    return a;
}

fol_t                           /* Dyam instruction */
Dyam_Create_List(const fol_t hd, const fol_t tl)
{
    fol_t a=folcmp_create_pair(hd,tl);
    V_LEVEL_DISPLAY( V_LOW,"Term: %&f\n",a);
    return a;
}

fol_t                           /* Dyam instruction */
Dyam_Create_Tupple(const int a, int b, fol_t l)
{
    V_LEVEL_DISPLAY( V_LOW,"Create Tupple %d %d %&f\n",a,b,l);
    for(;a<=b ; b--)
        l=folcmp_create_pair(FOLVAR_FROM_INDEX(b),l);
    V_LEVEL_DISPLAY( V_LOW,"Term: %&f\n",l);
    return l;
}

fol_t                           /* Dyam instruction */
Dyam_Create_Char_List( const char *s )
{
    return folcmp_create_string( s );
}

fol_t                           /* Dyam instruction */
Dyam_Create_Follow( fol_t hd, fol_t tl )
{
    fol_t follow = FOLSMB_MAKE(9,2);
    return Dyam_Create_Binary( follow, hd, tl );
}

fol_t                           /* Dyam instruction */
Dyam_Function_Reference(const void *fun)
{
    V_LEVEL_DISPLAY( V_LOW,"Create Function Reference %d\n",(unsigned long) fun );
        /*
          if ( (DYALOG_TO_POINTER(POINTER_TO_DYALOG(fun)) != (void *)fun) ||
          ! (FOLINTP(POINTER_TO_DYALOG(fun)))
          ) {
          V_LEVEL_DISPLAY( V_LOW,"Pb fun ref (%d)\n", (unsigned long)
          Adjust_Address(DYALOG_TO_POINTER((POINTER_TO_DYALOG(fun)))) );
          }
        */
    return POINTER_TO_DYALOG(fun);
}

void                           /* Dyam instruction */
Dyam_Set_Not_Copyable(fol_t a)
{
    FOLINFO_COPY_COST( a ) = 60000;
}

void                           /* Dyam instruction */
Dyam_Set_Deref_Functor(fol_t a, const fol_t arity)
{
    a = FOLSMB_CONVERT_ARITY(a,CFOLINT(arity));
    folsmb_switch_derefterm(a);
}


fol_t
Dyam_Closure_Aux(const void *fun, const fol_t env) 
{
    fol_t closure;       
    V_LEVEL_DISPLAY( V_DYAM,"Create Closure %d [%x] %&f\n",(unsigned long) fun,
                     (unsigned long) fun, env );
    FOLCMP_WRITE_START(FOLCLOSURE,2);
    FOLCMP_WRITE(POINTER_TO_DYALOG(fun));
    FOLCMP_WRITE(env);
    closure=FOLCMP_WRITE_STOP;
    Dyam_Set_Not_Copyable(closure);
    return closure;
}


fol_t
Dyam_Closure(fol_t *holder, const void *fun, const fol_t env) 
{
    if (!*holder)
        *holder = Dyam_Closure_Aux(fun,env);
    return *holder;
}

