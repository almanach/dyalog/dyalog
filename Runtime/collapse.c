/************************************************************
 * $Id$
 * Copyright (C) 1996, 2006, 2009, 2010, 2017 Eric de la Clergerie
 * ------------------------------------------------------------
 *
 *   Collapse -- Build a collapsed environement
 *
 * ------------------------------------------------------------
 * Description
 *   Build a collapsed environement from the current environement
 *   using the information of the Collect Pass (see CLib/collect.c)
 *
 *   The collapsed environement is given by a set of layers that
 *   merge the entered layers of the current environement with the local
 *   bindings of the reachable variables.
 *   
 *   A remapping of the layer is done to reduce the size of the
 *   collapsed environement.
 * ------------------------------------------------------------
 */

//#define VERBOSE

#include "libdyalog.h"

// #define V_DISPLAY(fmt, args...)      dyalog_printf( fmt, ## args )


/**********************************************************************
 * c_unmask X k        remove mask on X_k to un-bind X_k
 **********************************************************************/
inline
static void
c_unmask( fol_t X, fkey_t k)
{
  vca_reset(k->collapse->xnew, FOLVAR_INDEX(X));
}

/**********************************************************************
 * c_rebind X_k t_l   bind X_t to t_l in the environement to be created
 **********************************************************************/
static void
c_rebind( fol_t X, fkey_t k, fol_t t, fkey_t l)
{
  collapse_t c = k->collapse;
  unsigned long shifted_k = c->shift;
  unsigned long shifted_l = l ?  l->collapse->shift : 0;
  
  V_LEVEL_DISPLAY( V_SHARE, "%&p -> %&f (%d %d)\n", X, k, t , shifted_k, shifted_l );
    
  if (!c->xnew) c->xnew = VCA_NEW;
  LAYER_REBIND(c->xnew,X,shifted_k,t,shifted_l);
}

/**********************************************************************
 * c_archive        returns an env archive from the new layers
 **********************************************************************/
static obj_t
c_archive(collapse_t c)
{
  obj_t l = BNIL;
  obj_t anchor;
  unsigned long  last = 0;
  anchor = l = MAKE_PAIR( BNIL , BNIL);
  for (; c ; c = c->next) {
      if ( c->exited ) {
	if ( c->shift > last ) {
	  SET_CAR(anchor, BINT(c->shift - last));
	  SET_CDR(anchor, MAKE_PAIR(BNIL,BNIL));
	  anchor = CDR(anchor);
	}
	last = c->shift + 1;
	SET_CAR(anchor,BREF(c->xnew));
	SET_CDR(anchor, (c->next) ? MAKE_PAIR(BNIL,BNIL) : BNIL);
	anchor = CDR(anchor);
	c->xnew = (vca_t) 0;         /* Cleaning */
      } else if (!c->next)
          SET_CAR(anchor,BINT(1 + (c->shift - last)));
  }
  return l;
}

/**********************************************************************
 *  build_collapse entry_key
 *      build a new environement using the collected information
 *      return either
 *          * the empty list for a null environement
 *          * a pair (new_entry_key . env) otherwise
 **********************************************************************/
static obj_t
build_collapse(fkey_t k)
{
  collapse_t c = C_TRAIL_COLLAPSE;
  binding_t lb = C_TRAIL_BINDING;
  binding_t keep = (binding_t) 0;
  unbind_t  lu = C_TRAIL_UNBIND;
  fkey_t prev;
  unsigned long l =  -1;
  obj_t archive = BNIL;
  
  if (c) {
    
    V_LEVEL_DISPLAY( V_SHARE, "Remapping ...\n" );                     
    
    /* compute layer indexes in the collapsed environement */
    for(prev=c->layer ; c; prev=c->layer , c = c->next)
      c->shift = (l += (c->layer == c->block_start) ? 1 : (c->layer - prev));
 
    V_LEVEL_DISPLAY( V_SHARE, "%&t\nInstalling local bindings ...\n");

    /* install the local bindings in the collapsed environement */
    for(; lb ; lb = keep ) {
        V_LEVEL_DISPLAY( V_SHARE, "%lb %x keep %x: ",
                         (void *)lb,(void *)(lb->keep));
        V_LEVEL_DISPLAY( V_SHARE, "%&p -> %&p\n",
                         lb->binder,lb->binder_key,
                         lb->bindee,lb->bindee_key );
      c_rebind(lb->binder,lb->binder_key,lb->bindee,lb->bindee_key);
      keep = lb->keep;
      lb->keep = (binding_t) 0;
      if ( keep == lb ) break;
    }

    LVALUE_C_TRAIL_BINDING = REG_VALUE(0);

    V_LEVEL_DISPLAY( V_SHARE, "Masking unbindings ...\n");
    
    /* install masks in the collapsed environement for variable to unbind */
    for (;lu; lu = lu->next) {
      rename_t lr = lu->rename_list;
      fol_t    X0 = lr->X;
      fkey_t    k0 = lr->k;
      c_rebind(X0,k0,X0,k0);         /* tmp binding */
      for(lr = lr->next ; lr; lr = lr->next){
          COLLAPSE_MARK_LAYER_EXITED(lr->k);
          c_rebind(lr->X,lr->k,X0,k0);
      }
    }
    
    V_LEVEL_DISPLAY( V_SHARE, "Merging with old layers ...\n");
     
    /* merge the layers of the collapsed environement with the old layers */
    for( c = C_TRAIL_COLLAPSE; c; c =  c->next )
      if (COLLAPSE_LAYER_EXITED_P(c))
	c->xnew = vca_merge(c->xnew,c->layer->vca);
    
    V_LEVEL_DISPLAY( V_SHARE, "Unmasking ...\n");

    /* remove masks to unbind variables */
    for(lu = C_TRAIL_UNBIND; lu; lu = lu->next)
      c_unmask(lu->rename_list->X,lu->rename_list->k);

    LVALUE_C_TRAIL_UNBIND = REG_VALUE(0);

    V_LEVEL_DISPLAY( V_SHARE, "Archiving ...\n");

    /* build an archive from the layers of the collapsed environement */
    archive = c_archive(C_TRAIL_COLLAPSE);
    archive = MAKE_PAIR( BINT(k->collapse->shift), archive );

  }

  V_LEVEL_DISPLAY( V_SHARE, "Done\n");

  return archive;
}
  
/**********************************************************************
 *  collapse t_k
 *      Collect from a compound term t_k and Collapse
 **********************************************************************/

extern void untrail_alt(TrailWord *stop);

obj_t
collapse( fol_t t, fkey_t k)
{
  obj_t tuple;
  obj_t r;
  TrailWord *top= C_TRAIL_TOP;
        
  if (FOLCMPP( t ))
    for(tuple= FOLINFO_TUPLE( t ); PAIRP(tuple); tuple=CDR(tuple))
      wrapped_collect((fol_t)CAR(tuple),k);
  else if (FOLVARP( t ))
    wrapped_collect(t,k);
  r= build_collapse(k);

  if (C_TRAIL_TOP > top)
      untrail_alt(top);
  return r;
}




