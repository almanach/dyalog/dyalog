/* 
 ******************************************************************
 * $Id$
 * Copyright (C) 1998, 2002, 2003, 2004, 2006, 2008, 2009, 2010, 2011, 2014, 2016 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  dyam.c -- DyALog Abstract Machine Instructions
 *
 * ----------------------------------------------------------------
 * Description
 * 
 * ----------------------------------------------------------------
 */

//#define VERBOSE

#define DYAM_FILE

#include "libdyalog.h"
#include "builtins.h"

    /* Prototyping */

extern fol_t dyalog_true, dyalog_false;

extern fkey_t  load_layer_archive( unsigned long, obj_t);
extern void    tabulation_add( tabobj_t );

    /* Instruction Dyam_Unify_Item: moved to termop.c */
    /* Instruction DYAM_Unify_2: moved to termop.c */
    /* Instruction Dyam_Backptr: moved to objects.c */
    /* Instruction Dyam_Tabule: moved to tree-indexing.c */
    /* Instruction Dyam_Schedule: moved to agenda.c */
    /* Instruction Dyam_Schedule_Last: moved to agenda.c */
    /* Instruction Dyam_Now: moved to agenda.c */
    /* Instruction Dyam_Answer: moved to objects.c */
    /* Instruction Dyam_Object: moved to objects.c */
    /* Instruction Dyam_Light_Object: moved to objects.c */
    /* Instruction Dyam_Subsume: moved to tree-indexing.c */
    /* Instruction Dyam_Apply: moved to tree-indexing.c */
    /* Instruction Dyam_Complete: moved to tree-indexing.c */

/**********************************************************************
 * Main functions
 **********************************************************************/

extern int  _dyalog_initialization_stamp;

Bool
Dyam_Check_And_Update_Initialization( int *initializer )
{
    V_LEVEL_DISPLAY( V_DYAM,  "Initializing %d %d\n",_dyalog_initialization_stamp,*initializer);
    if (*initializer == _dyalog_initialization_stamp) {
        *initializer = ((*initializer)+1) % 2;
        return 1;
    } else {
        return 0;
    }
}

void
DYAM_Test()
{
    V_LEVEL_DISPLAY( V_DYAM, "In DYAM Test %d\n",(long) R_B);
}

typedef void (*builder_fun_t)();

void
Dyam_Pool( long *initializer)
{
    V_LEVEL_DISPLAY( V_DYAM, "Pool Initializer %x %d\n",(long)initializer,*initializer);
    if (*initializer){          /* un-initialized pool */
        long *p= initializer+1;
        long *stop = p + (*initializer & ((1 << 16)-1));
        *initializer >>= 16;
        for(;p<stop; p++) {
            (*(builder_fun_t)(*p))();
        }
        stop += *initializer;
        for(;p<stop; p++) {
            Dyam_Pool((long *)*p);
        }
        *initializer=0;
    }
}

/**********************************************************************
 * Instructions for Term Creation
 **********************************************************************/


    /* Instruction Dyam_Create_Atom: moved to fol.c */
    /* Instruction Dyam_Term_Start: moved to fol.c */
    /* Instruction Dyam_Term_Arg: moved to fol.c */
    /* Instruction Dyam_Term_End: moved to fol.c */
    /* Instruction Dyam_Create_List: moved to fol.c */
    /* Instruction Dyam_Create_Tupple: moved to fol.c */
    /* Instruction Dyam_Set_Not_Copyable: moved to fol.c */
    /* Instruction Dyam_Set_Deref_Functor: moved to fol.c */

/**********************************************************************
 * Instructions for Seed and Viewer Creation
 **********************************************************************/

static tabseed_t seed;
static void **seed_app;
extern obj_t _dyalog_callret_viewer;

void
Dyam_Seed_Start(const char *type, const fol_t model, const fol_t subs_model, const fun_t fun,const int napp)
{
    seed=ALLOCATE_TABSEED(0,0,model,fun,napp);
    seed->subs_comp=subs_model;
    seed_app=&seed->application_code;
}

void
Dyam_Seed_Add_Comp(void *A, void *B, void *init_code)
{
    *++seed_app=A;
    *++seed_app=B;
    *++seed_app=init_code;
}

tabseed_t
Dyam_Seed_End()
{
    V_LEVEL_DISPLAY( V_DYAM, "Build seed model %&f\n",seed->model);
    return seed;
}

void
Dyam_Load_Viewer(const fol_t A, const fol_t B)
{
    V_LEVEL_DISPLAY( V_DYAM, "Load viewer %&f %&f\n",A,B);
    _dyalog_callret_viewer = MAKE_PAIR( MAKE_PAIR((obj_t)A,(obj_t)B), _dyalog_callret_viewer);
}

void
Dyam_Reset_Viewers()
{
    _dyalog_callret_viewer = BNIL;
}

/**********************************************************************
 * Other Instructions 
 **********************************************************************/

void
Dyam_Allocate(const int arity)
{
    V_LEVEL_DISPLAY( V_DYAM, "\tallocate item=%x\n",R_ITEM);
    TRAIL_ENVIRONMENT(arity);
}

void
Dyam_Deallocate()
{
    V_LEVEL_DISPLAY( V_DYAM, "\tdeallocate ->item=%x\n",R_E->item);
    UNTRAIL_ENVIRONMENT(R_E,0);
}

void
Dyam_Deallocate_Alt()
{
    V_LEVEL_DISPLAY( V_DYAM, "\tdeallocate alt\n");
    UNTRAIL_ENVIRONMENT_ALT(R_E,0);
}

void
Dyam_Choice(const continuation_t fun)
{
    V_LEVEL_DISPLAY( V_DYAM,  "\tset choice point (current B=%d BC=%d)\n",(long)R_B,(long)R_BC );
    TRAIL_CHOICE(fun,0);
}

void
Dyam_Remove_Choice()
{
    V_LEVEL_DISPLAY( V_DYAM,  "\tremove choice point B=%d BC=%d-> B=%d\n",(long)R_B,(long) R_BC, (long) R_B->prev );
    LVALUE_C_CTL_TOP= REG_VALUE(((TrailWord *) R_B)-1);
    LVALUE_R_B = REG_VALUE(R_B->prev);
}

void
Dyam_Update_Choice(const continuation_t fun)
{
    update_choice(fun,0);
}

void
Dyam_Full_Choice(const continuation_t fun, const int n)
{
    V_LEVEL_DISPLAY( V_DYAM,  "\tset full choice point (current B=%d)\n",R_B );
    TRAIL_CHOICE(fun,n);
}

choice_t
Dyam_Full_Choice_Light(const continuation_t fun, const int n)
{
    V_LEVEL_DISPLAY( V_DYAM,  "\tset full choice point light (current B=%d)\n",R_B );
    return TRAIL_CHOICE_LIGHT(fun,n);
}

void
Dyam_Set_Cut()
{
    V_LEVEL_DISPLAY( V_DYAM,  "\tset cut point (cut will give B=%d)\n", (long) R_B->prev);
    LVALUE_R_BC= REG_VALUE(R_B);
}

void
Dyam_Cut()
{
    V_LEVEL_DISPLAY( V_DYAM,  "\tbefore cut (B=%d BC=%d)\n",
                     (long)R_B, (long)R_BC);
    V_LEVEL_DISPLAY( V_DYAM,  "\tcut (now B=%d BC=%d)\n",
                     (long)R_BC->prev, (long)R_BC->bc);
    LVALUE_R_B=  REG_VALUE(R_BC->prev);
    LVALUE_R_BC= REG_VALUE(R_BC->bc);
}

void
Dyam_Pseudo_Choice(const int n)
{
    V_LEVEL_DISPLAY( V_DYAM,  "\tset pseudo choice point\n" );
    TRAIL_PSEUDO_CHOICE(n);
}

void
Dyam_Remove_Pseudo_Choice()
{
    V_LEVEL_DISPLAY( V_DYAM,  "\tremove pseudo choice point\n" );
    LVALUE_C_CTL_TOP= REG_VALUE(((TrailWord *) R_B)-1);
    UNTRAIL_PSEUDO_CHOICE(R_B);
}

Bool
Dyam_Bind(const fol_t X,fol_t t)
{
    V_LEVEL_DISPLAY( V_DYAM, "\tbind %&s to %&s\n",X,R_TRANS_KEY,t,R_TRANS_KEY);
    TRAIL_UBIND(X,R_TRANS_KEY,t,R_TRANS_KEY);
    Succeed;
}

Bool
Dyam_Alt_Bind(const fol_t X,fol_t t)
{
    V_LEVEL_DISPLAY( V_DYAM, "\tbind [alt] %&s to %&s\n",X,R_TRANS_KEY,t,R_E->trans_key);
    TRAIL_UBIND(X,R_TRANS_KEY,t,R_E->trans_key);
    Succeed;
}

Bool
Dyam_Alt_Unify(fol_t A,fol_t B)
{
    V_LEVEL_DISPLAY( V_DYAM,"\tunify [alt] %&s with %&s\n",
                     A, R_TRANS_KEY,
                     B, R_E->trans_key);
    return sfol_unify(A,R_TRANS_KEY,B,R_E->trans_key);
}

Bool
Dyam_Bind_Ptr(const fol_t X, const void *ptr)
{
    TRAIL_UBIND(X,R_TRANS_KEY,POINTER_TO_DYALOG(ptr),Key0);
    Succeed;
}

Bool
Dyam_Unify_Ptr(fol_t A,const void *ptr)
{
    return sfol_unify(POINTER_TO_DYALOG(ptr),Key0,A,R_TRANS_KEY);
}



void
Dyam_Schedule_Prolog()
{
    V_LEVEL_DISPLAY( V_DYAM,  "\tschedule_prolog\n");
    LVALUE_R_TRANS = REG_VALUE(R_OBJECT);
    LVALUE_R_P= REG_VALUE((fun_t) Adjust_Address(R_TRANS->seed->application_code));
}

Bool
Dyam_Follow_Cont(fol_t closure)
{
    fkey_t Sk(closure) = R_TRANS_KEY;
    
    V_LEVEL_DISPLAY( V_DYAM, "\tfollow closure %&p -> %&s\n",closure,Sk(closure),closure,Sk(closure));

        /* closure = '*CLOSURE*'(<code>,<env>) */

        /* We may have to take extra care to be sure we have a closure
           and go through potential cyclic terms
         */
    
    Deref(closure);

        // check if the closure is accessible through a loop term
    Loop_Deref(closure);

    if (!(FOLCMPP(closure) && FOLCMP_FUNCTOR(closure) == FOLCLOSURE)) {
        if (FOLCMPP(closure)) {
            fol_t test = FOLCMP_REF(closure,1);
            fkey_t Sk(test) = Sk(closure);
            Deref(test);
            Loop_Deref(test);
//            dyalog_printf("Using contargs %&f\n",closure);
            if (!(FOLCMPP(test) && FOLCMP_FUNCTOR(test) == FOLCLOSURE)) {
                dyalog_error_printf( "not a valid closure %&f\n", closure );
                exit(EXIT_FAILURE);
            } else {
                closure = test;
                Sk(closure) = Sk(test);
            }
        } else {
            dyalog_error_printf( "not a valid closure %&f\n", closure );
            exit(EXIT_FAILURE);
        }
    }

    LVALUE_R_TRANS_KEY= REG_VALUE(Sk(closure));
    
    closure = FOLCMP_REF(closure,1);
    Deref(closure);
    
    LVALUE_R_P = REG_VALUE((fun_t) Adjust_Address(DYALOG_TO_POINTER( closure )));

    V_LEVEL_DISPLAY( V_DYAM, "\tready for closure R_P=%d (%&f)\n",(long)R_P,POINTER_TO_DYALOG(R_P));
    Succeed;
}

/**********************************************************************
 * Memory Load instructions
 **********************************************************************/

void
Dyam_Mem_Load(int i, fol_t t)
{
    fkey_t Sk(t)=R_TRANS_KEY;
    V_LEVEL_DISPLAY( V_DYAM, "\tmem load [%d] %&s\n",i,t,Sk(t));
    Deref(t);
    foreign_bkt_buffer[i++] = (ForeignWord) t;
    foreign_bkt_buffer[i] = (ForeignWord) Sk(t);
}

    
void                 /* Load First Occurrence of a variable */
Dyam_Mem_Load_Var(int i, const fol_t t)
{
    fkey_t Sk(t)=R_TRANS_KEY;
    V_LEVEL_DISPLAY( V_DYAM, "\tmem load var %&s\n",t,Sk(t));
    foreign_bkt_buffer[i++] = (ForeignWord) t;
    foreign_bkt_buffer[i] = (ForeignWord) Sk(t);
}

    
void                  /* Load Constante */
Dyam_Mem_Load_Cst(int i, const fol_t t)
{
    fkey_t Sk(t)=R_TRANS_KEY;
    V_LEVEL_DISPLAY( V_DYAM, "\tmem load cst %&s\n",t,Sk(t));
    foreign_bkt_buffer[i++] = (ForeignWord) t;
    foreign_bkt_buffer[i] = (ForeignWord) Key0;
}

void
Dyam_Mem_Load_Nil(int i)
{
    Dyam_Mem_Load_Cst(i,FOLNIL);
}


Bool
Dyam_Mem_Load_Number(const int i, fol_t t)
{
    fkey_t Sk(t)=R_TRANS_KEY;
    V_LEVEL_DISPLAY( V_DYAM, "\tmem load number %&s\n",t,R_TRANS_KEY);
    Deref_And_Fail_Unless(t,FOLINTP(t));
    foreign_bkt_buffer[i] = (ForeignWord) CFOLINT(t);
    Succeed;
}

Bool
Dyam_Mem_Load_Float(const int i, fol_t t)
{
    fkey_t Sk(t)=R_TRANS_KEY;
    V_LEVEL_DISPLAY( V_DYAM, "\tmem load number %&s\n",t,R_TRANS_KEY);
    Deref_And_Fail_Unless(t,FOLFLTP(t));
    foreign_bkt_buffer[i] = (ForeignWord) float2long(CFOLFLT(t));
    Succeed;
}


Bool
Dyam_Mem_Load_Ptr(const int i, fol_t t)
{
    fkey_t Sk(t)=R_TRANS_KEY;
    V_LEVEL_DISPLAY( V_DYAM, "\tmem load ptr %&s\n",t,R_TRANS_KEY);
    Deref(t);
    if (FOLVARP(t)) {
        foreign_bkt_buffer[i] = (ForeignWord) 0;
        Succeed;
    } else if (FOL_POINTERP(t)) {
        foreign_bkt_buffer[i] = (ForeignWord) DYALOG_TO_POINTER(t);
        Succeed;
    } else 
        Fail;
}

Bool
Dyam_Mem_Load_Char(const int i, fol_t t)
{
    fkey_t Sk(t)=R_TRANS_KEY;
    V_LEVEL_DISPLAY( V_DYAM, "\tmem load char %&s\n",t,R_TRANS_KEY);
    Deref_And_Fail_Unless(t,FOLCHARP(t));
    foreign_bkt_buffer[i] = (ForeignWord) CFOLCHAR(t);
    Succeed;
}

Bool
Dyam_Mem_Load_Boolean(const int i, fol_t t)
{
    fkey_t Sk(t)=R_TRANS_KEY;
    V_LEVEL_DISPLAY( V_DYAM, "\tmem load boolean %&s\n",t,R_TRANS_KEY);
    Deref(t);
    if (t == dyalog_true) {
        foreign_bkt_buffer[i] = (ForeignWord) 1;
        Succeed;
    } else if (t == dyalog_false) {
        foreign_bkt_buffer[i] = (ForeignWord) 0;
        Succeed;
    } else
        Fail;
}

Bool
Dyam_Mem_Load_String(const int i, fol_t t)
{
    fkey_t Sk(t)=R_TRANS_KEY;
    V_LEVEL_DISPLAY( V_DYAM, "\tmem load string %&s\n",t,R_TRANS_KEY);
    Deref(t);
    if (FOLVARP(t)) {
        foreign_bkt_buffer[i] = (ForeignWord) 0;
    } else if (FOLSMBP(t)) {
        foreign_bkt_buffer[i] = (ForeignWord) SymbolName(t);
    } else
        Fail;
    
    Succeed;
}

Bool
Dyam_Mem_Load_Output(const int i, fol_t t)
{
    fkey_t Sk(t)=R_TRANS_KEY;
    long stm;
    V_LEVEL_DISPLAY( V_DYAM, "\tmem load output %&s\n",t,R_TRANS_KEY);
    Deref(t);
    if (t==FOLNIL){
        stm=stm_output;
    } else if ((FOLINTP(t) || FOLSMBP(t))
               && (stm = Get_Stream_Or_Alias(t,Key0,STREAM_FOR_OUTPUT)) >= 0) {
            /* nothing to do */
    } else
        Fail;
    last_output_sora=t;
    foreign_bkt_buffer[i]=(ForeignWord)stm;
    Succeed;
}

Bool
Dyam_Mem_Load_Input(const int i, fol_t t)
{
    fkey_t Sk(t)=R_TRANS_KEY;
    long stm;
    V_LEVEL_DISPLAY( V_DYAM, "\tmem load input %&s\n",t,R_TRANS_KEY);
    Deref(t);
    if (t==FOLNIL){
        stm=stm_input;
    } else if ((FOLINTP(t) || FOLSMBP(t))
               && (stm = Get_Stream_Or_Alias(t,Key0,STREAM_FOR_INPUT)) >= 0) {
            /* nothing to do */
    } else
        Fail;
    last_input_sora=t;
    foreign_bkt_buffer[i]=(ForeignWord)stm;
    Succeed;
}

/**********************************************************************
 * X-Register Instructions
 **********************************************************************/

void
Dyam_Reg_Load(int reg, fol_t t)
{
    fkey_t Sk(t)=R_TRANS_KEY;
    Deref(t);
    V_LEVEL_DISPLAY( V_DYAM, "\treg load %d %&s (%x %x)\n",reg,t,Sk(t),t,Sk(t));
    LVALUE_X( reg++ ) = REG_VALUE(t);
    LVALUE_X( reg )= REG_VALUE(Sk(t));
}

    /* Load First Occurrence of a variable */
void
Dyam_Reg_Load_Var(int reg, const fol_t t)
{
    fkey_t Sk(t)=R_TRANS_KEY;
    V_LEVEL_DISPLAY( V_DYAM, "\treg load var %d %&s\n",reg,t,Sk(t));
    LVALUE_X( reg++ ) = REG_VALUE(t);
    LVALUE_X( reg )= REG_VALUE(Sk(t));
}

    /* Load Constante */
void
Dyam_Reg_Load_Cst(int reg, const fol_t t)
{
    fkey_t Sk(t)=R_TRANS_KEY;
    V_LEVEL_DISPLAY( V_DYAM, "\treg load cst %d %&s\n",reg,t,Sk(t));
    LVALUE_X( reg++ ) = REG_VALUE(t);
    LVALUE_X( reg )= REG_VALUE(Key0);
}

void
Dyam_Reg_Load_Nil(const int reg)
{
    Dyam_Reg_Load_Cst(reg,FOLNIL);
}

/* Load a void fol, used for useless free variables */
void
Dyam_Reg_Load_Zero(const int reg)
{
    LVALUE_X(reg) = REG_VALUE((fol_t)0);
}


Bool
Dyam_Reg_Load_Number(const int reg, fol_t t)
{
    fkey_t Sk(t)=R_TRANS_KEY;
    V_LEVEL_DISPLAY( V_DYAM, "\treg load integer %d %&s\n",reg,t,R_TRANS_KEY);
    Deref_And_Fail_Unless(t,FOLINTP(t));
    LVALUE_X( reg ) = REG_VALUE(CFOLINT(t));
    Succeed;
}

Bool
Dyam_Reg_Load_Float(int reg, fol_t t)
{
    fkey_t Sk(t)=R_TRANS_KEY;
    double dbl;
    long *p=(long *)&dbl;
    Deref(t);
    if (FOLFLTP(t))
        dbl = (double) CFOLFLT(t);
    else if (FOLINTP(t))
        dbl = (double) CFOLINT(t);
    else
        Fail;
    LVALUE_X( reg++ ) = REG_VALUE(p[0]);
    LVALUE_X( reg ) = REG_VALUE(p[1]);
    V_LEVEL_DISPLAY( V_DYAM, "\treg load float %d %&s %g %g\n",reg,t,R_TRANS_KEY,dbl,*(double *)p);
    Succeed;
}

Bool
Dyam_Reg_Load_Ptr(const int reg, fol_t t)
{
    fkey_t Sk(t)=R_TRANS_KEY;
    V_LEVEL_DISPLAY( V_DYAM, "\treg load ptr %d %&s\n",reg,t,R_TRANS_KEY);
    Deref(t);
    if (FOLVARP(t)) {
        LVALUE_X( reg ) = REG_VALUE(0);
        Succeed;
    } else if (FOLSMBP(t)) {
        LVALUE_X( reg ) = REG_VALUE(SymbolName(t));
        Succeed;
    } else if (FOL_POINTERP(t)) {
        V_LEVEL_DISPLAY( V_DYAM, "\t->reg load ptr %d\n",(unsigned long) DYALOG_TO_POINTER(t));
        LVALUE_X( reg ) = REG_VALUE(DYALOG_TO_POINTER(t));
        Succeed;
    } else
        Fail;
}

Bool
Dyam_Reg_Load_Char(const int reg, fol_t t)
{
    fkey_t Sk(t)=R_TRANS_KEY;
    V_LEVEL_DISPLAY( V_DYAM, "\treg load char %d %&s\n",reg,t,R_TRANS_KEY);
    Deref_And_Fail_Unless(t,FOLCHARP(t));
	// Djamé Message to eric 
	// : warning important lors de la compile : 
	// cast to pointer from integer of different size 
	// C'est normal qu'il y ait deux fois la même ligne ?
    LVALUE_X( reg ) = REG_VALUE((long)CFOLCHAR(t));
    //LVALUE_X( reg ) = REG_VALUE(CFOLCHAR(t));
    Succeed;
}

Bool
Dyam_Reg_Load_Boolean(const int reg, fol_t t)
{
    fkey_t Sk(t)=R_TRANS_KEY;
    V_LEVEL_DISPLAY( V_DYAM, "\treg load boolean %d %&s\n",reg,t,R_TRANS_KEY);
    Deref(t);
    if (t == dyalog_true) {
        LVALUE_X( reg ) = REG_VALUE(1);
        Succeed;
    } else if (t == dyalog_false) {
        LVALUE_X( reg ) = REG_VALUE(0);
        Succeed;
    } else
        Fail;
}

Bool
Dyam_Reg_Load_String(const int reg, fol_t t)
{
    fkey_t Sk(t)=R_TRANS_KEY;
    V_LEVEL_DISPLAY( V_DYAM, "\treg load string %d %&s\n",reg,t,R_TRANS_KEY);
    Deref(t);
    if (FOLVARP(t)) {
        LVALUE_X( reg ) = REG_VALUE(0);
    } else if (FOLSMBP(t)) {
        LVALUE_X( reg ) = REG_VALUE(SymbolName(t));
    } else
        Fail;
    
    Succeed;
}

Bool
Dyam_Reg_Load_Output(const int reg, fol_t t)
{
    fkey_t Sk(t)=R_TRANS_KEY;
    long stm;
    V_LEVEL_DISPLAY( V_DYAM, "\treg load output %d %&s\n",reg,t,R_TRANS_KEY);
    Deref(t);
    if (t==FOLNIL){
        stm=stm_output;
    } else if ((FOLINTP(t) || FOLSMBP(t))
               && (stm = Get_Stream_Or_Alias(t,Key0,STREAM_FOR_OUTPUT)) >= 0) {
            /* nothing to do */
    } else
        Fail;
    last_output_sora=t;
    LVALUE_X(reg)= REG_VALUE(stm);
    Succeed;
}

Bool
Dyam_Reg_Load_Input(const int reg, fol_t t)
{
    fkey_t Sk(t)=R_TRANS_KEY;
    long stm;
    V_LEVEL_DISPLAY( V_DYAM, "\treg load input %d %&s\n",reg,t,R_TRANS_KEY);
    Deref(t);
    if (t==FOLNIL){
        stm=stm_input;
    } else if ((FOLINTP(t) || FOLSMBP(t))
               && (stm = Get_Stream_Or_Alias(t,Key0,STREAM_FOR_INPUT)) >= 0) {
            /* nothing to do */
    } else
        Fail;
    last_input_sora=t;
    LVALUE_X(reg)= REG_VALUE((long) stm);
    Succeed;
}

Bool
Dyam_Reg_Unify(int reg, fol_t t)
{
    fol_t v = (fol_t) X(reg++);
    V_LEVEL_DISPLAY( V_DYAM, "\treg unif %d %&s\n",reg-1,t,R_TRANS_KEY);
    return !v || Unify(t,R_TRANS_KEY,v,(fkey_t)X(reg));
}

void
Dyam_Reg_Bind(int reg, const fol_t t)
{
    fol_t v = (fol_t) X(reg++);
    V_LEVEL_DISPLAY( V_DYAM, "\treg bind %d %&s\n",reg-1,t,R_TRANS_KEY);
    if (v) 
        TRAIL_UBIND(t,R_TRANS_KEY,v,(fkey_t)X(reg));
    V_LEVEL_DISPLAY( V_DYAM, "\tdone reg bind %x %x\n",v,X(reg));
}

void
Dyam_Multi_Reg_Bind(int reg, int vindex, int n)
{
    folvar_t v = (folvar_t) DFOLVAR(&folvar_tab[vindex]);
    V_LEVEL_DISPLAY( V_DYAM, "\tmulti reg bind %d %d %d\n",reg,vindex,n);
    for(; n ; n--,v++,reg++) {
        fol_t value = (fol_t)X(reg++);
        if (value)
            TRAIL_UBIND((fol_t)v,R_TRANS_KEY,value,(fkey_t)X(reg));
    }
}

void
Dyam_Multi_Reg_Bind_Zero(int vindex, int n)
{
    Dyam_Multi_Reg_Bind(0,vindex,n);
}

void
Dyam_Multi_Reg_Bind_ZeroOne(int n)
{
    Dyam_Multi_Reg_Bind(0,1,n);
}

Bool
Dyam_Reg_Unify_Cst(int reg, fol_t t)
{
    fol_t r=(fol_t)X(reg++);
    V_LEVEL_DISPLAY( V_DYAM, "\treg unif cst %d %&s (%&f)\n",reg-1,t,R_TRANS_KEY,r);
    return (t==r) || Unify(t,Key0,r,(fkey_t)X(reg));
}

Bool
Dyam_Reg_Unify_Nil(const int reg)
{
    return Dyam_Reg_Unify_Cst(reg,FOLNIL);
}

Bool
Dyam_Reg_Alt_Unify(const int reg, fol_t t)
{
    V_LEVEL_DISPLAY( V_DYAM, "\treg alt unif %d %&s\n",reg,t,R_TRANS_KEY);
    V_LEVEL_DISPLAY( V_DYAM, "\t\treg=%&s\n",(fol_t)X(reg),R_E->trans_key);
    return Unify(t,R_TRANS_KEY,(fol_t)X(reg),R_E->trans_key);
}

void
Dyam_Reg_Alt_Bind(const int reg, fol_t t)
{
    V_LEVEL_DISPLAY( V_DYAM, "\treg alt bind %d %&s\n",reg,t,R_TRANS_KEY);
    V_LEVEL_DISPLAY( V_DYAM, "\t\treg=%&p\n",(fol_t)X(reg),R_E->trans_key);
    TRAIL_UBIND(t,R_TRANS_KEY,(fol_t)X(reg),R_E->trans_key);
}

Bool
Dyam_Reg_Alt_Unify_Cst(const int reg, const fol_t t)
{
    fol_t r=(fol_t)X(reg);
    V_LEVEL_DISPLAY( V_DYAM, "\treg unif alt cst %d %&s\n",reg,t,R_TRANS_KEY);
    V_LEVEL_DISPLAY( V_DYAM, "\t\treg=%&s\n",(fol_t)X(reg),R_E->trans_key);
//    return (t == r || (FOLVARP(r) && TRAIL_UBIND(r,R_E->trans_key,t,Key0)));
    return (t==r) || Unify(t,Key0,r,R_E->trans_key);
}

Bool
Dyam_Reg_Alt_Unify_Nil(const int reg)
{
    return Dyam_Reg_Alt_Unify_Cst(reg,FOLNIL);
}

Bool
Dyam_Reg_Unify_C_Number(const int reg, const fol_t t)
{
    V_LEVEL_DISPLAY( V_DYAM, "\treg unify integer %d %&s\n",reg,t,R_TRANS_KEY);
    return Unify(t,R_TRANS_KEY, DFOLINT( (long) X(reg)),Key0);
}

Bool
Dyam_Reg_Unify_C_Float(int reg, const fol_t t)
{
    long p[2];
    double a;
    p[0]= (long) X(reg++);
    p[1]= (long) X(reg);
    a = *(double*)p;
    V_LEVEL_DISPLAY( V_DYAM, "\treg unify float %d %&s\n",reg,t,R_TRANS_KEY);
    return Unify(t,R_TRANS_KEY, DFOLFLT( (dyalog_float) a),Key0);
}


Bool
Dyam_Reg_Unify_C_Ptr(const int reg, const fol_t t)
{
    void *ptr=(void *)X(reg);
    V_LEVEL_DISPLAY( V_DYAM, "\treg unify ptr %d %&s\n",reg,t,R_TRANS_KEY);
    if (!ptr)
        Fail;
    return Unify(t,R_TRANS_KEY, POINTER_TO_DYALOG(ptr),Key0);
}

Bool
Dyam_Reg_Unify_C_Char(const int reg, const fol_t t)
{
    V_LEVEL_DISPLAY( V_DYAM, "\treg unify char %d [%&f] %&s\n",
                     reg,DFOLCHAR( (long) X(reg)),
                     t,R_TRANS_KEY);
    return Unify(t,R_TRANS_KEY, DFOLCHAR( (long) X(reg)),Key0);
}

Bool
Dyam_Reg_Unify_C_Boolean(const int reg, const fol_t t)
{
    fol_t v = (Bool) X(reg) ? dyalog_true : dyalog_false;
    V_LEVEL_DISPLAY( V_DYAM, "\treg unify number %d %&s\n",reg,t,R_TRANS_KEY);
    return Unify(t,R_TRANS_KEY,v,Key0);
}

Bool
Dyam_Reg_Unify_C_String(const int reg, const fol_t t)
{
    char * s = (char *) X(reg);
    V_LEVEL_DISPLAY( V_DYAM, "\treg unify string %d %&s\n",reg,t,R_TRANS_KEY);
    if (!s)
        Fail;
    return Unify(t,R_TRANS_KEY, find_folsmb(s,0),Key0);
}

Bool
Dyam_Reg_Unify_C_Output(const int reg, fol_t t)
{
    fkey_t Sk(t)=R_TRANS_KEY;
    long stm = (long) X(reg);
    V_LEVEL_DISPLAY( V_DYAM, "\treg unify output %d %&s\n",reg,t,R_TRANS_KEY);
    Deref(t);
    if (FOLVARP(t)){
        Bind(DFOLINT(stm),Key0,t);
        Succeed;
    } else if (t==FOLNIL) {
        return (stm==stm_output);
    } else if (FOLSMBP(t)) {
        return (stm==Find_Stream_By_Alias(t));
    } else {
        Fail;
    }
}

Bool
Dyam_Reg_Unify_C_Input(const int reg, fol_t t)
{
    fkey_t Sk(t)=R_TRANS_KEY;
    long stm = (long) X(reg);
    V_LEVEL_DISPLAY( V_DYAM, "\treg unify input %d %&s\n",reg,t,R_TRANS_KEY);
    Deref(t);
    if (FOLVARP(t)){
        Bind(DFOLINT(stm),Key0,t);
        Succeed;
    } else if (t==FOLNIL) {
        return (stm==stm_input);
    } else if (FOLSMBP(t)) {
        return (stm==Find_Stream_By_Alias(t));
    } else {
        Fail;
    }
}

void
Dyam_Reg_Allocate_Layer()
{
    V_LEVEL_DISPLAY( V_DYAM, "\tallocate layer\n");
    /* *WARNING* Need to check if the following line can be left commented 1-Dec-00 */
//    R_TRANS= (tabobj_t) 0;
    LVALUE_R_TRANS_KEY= LSTACK_PUSH_VOID;
}

void
Dyam_Reg_Reset()
{
    V_LEVEL_DISPLAY( V_DYAM, "\treset reg\n");
}

void
Dyam_Reg_Deallocate_Layer()
{
    V_LEVEL_DISPLAY( V_DYAM, "\tdeallocate layer\n");
}

void
Dyam_Reg_Deallocate(const int n)
{
    V_LEVEL_DISPLAY( V_DYAM, "\tdeallocate reg %d ->item=%x\n",n,R_E->item);
    UNTRAIL_ENVIRONMENT(R_E,n);
}

void
Dyam_Reg_Deallocate_Alt(const int n)
{
    V_LEVEL_DISPLAY( V_DYAM, "\tdeallocate alt reg %d ->item=%x\n",n,R_E->item);
    UNTRAIL_ENVIRONMENT_ALT(R_E,n);
}


/**********************************************************************
 * Instructions for Initial Object Loading
 **********************************************************************/

void
Dyam_Loading_Set()
{
    Dyam_Allocate(0);
    Trail;
    R_LOAD_TRANS(NULL,load_layer_archive(0,MAKE_PAIR(BINT(1),BNIL)));
    V_LEVEL_DISPLAY( V_DYAM, "In Dyam_Loading_Set\n");
}

void
Dyam_Loading_Reset()
{
    Untrail;
    Dyam_Deallocate();
    V_LEVEL_DISPLAY( V_DYAM, "In Dyam_Loading_Reset\n");
}

/**********************************************************************
 * Calling DyALog from C
 **********************************************************************/

extern void DyALog_Fail();
extern void DyALog_To_C();
extern void C_To_DyALog();

void                            /* call DyALog from C, trying all alternatives */
Dyam_DyALog(const fun_t fun)
{
    V_LEVEL_DISPLAY( V_LOW, "Starting DyALog\n");
    TRAIL_CHOICE(DyALog_To_C,0);
    LVALUE_R_CP= REG_VALUE((fun_t) Adjust_Address(DyALog_Fail));
    LVALUE_R_P= REG_VALUE((fun_t) Adjust_Address(fun));
    C_To_DyALog();
}

Bool                            /* call dyalog once from C, return the success value of fun */
Dyam_DyALog_Once(const fun_t fun)
{
    continuation_t cp = R_CP;
    choice_t b = R_B;
    V_LEVEL_DISPLAY( V_LOW, "Starting DyALog Once\n");
    TRAIL_CHOICE(DyALog_To_C,0);
    Dyam_Set_Cut();
    LVALUE_R_CP= REG_VALUE((fun_t) Adjust_Address(DyALog_To_C));
    LVALUE_R_P= REG_VALUE((fun_t) Adjust_Address(fun));
    C_To_DyALog();
    LVALUE_R_CP= REG_VALUE(cp);
    if (R_B>b) {
        Dyam_Cut();
        Succeed;
    } else {
        Fail;
    }
}

void
Dyam_RP_Move(void **reg) 
{
    *reg = (void *) R_P;
}

void
Dyam_Foreign_Create_Choice( const continuation_t fun,
                            const int arity,
                            const int choice_size)
{
    int _a = 2*(arity+1);
    LVALUE_X( _a++   ) = REG_VALUE(0);
    if (choice_size) {
        LVALUE_X( _a ) = REG_VALUE(PUSH_TRAIL_BLOCK( sizeof(int) * choice_size));
        foreign_bkt_buffer = (long *) X( _a ); /* used to load init
                                                         * arguments */
    } else {
        LVALUE_X( _a ) = REG_VALUE(0);
        foreign_bkt_buffer = (long *) 0;
    }
}

void
Dyam_Foreign_Update_Choice(const fun_t fun,
                           const int arity,
                           const int choice_size)
{
    foreign_bkt_counter = ((long) X(2*(arity+1)));
    LVALUE_X(2*(arity+1)) = REG_VALUE(foreign_bkt_counter+1);
    foreign_bkt_buffer=(long *) X(2*(arity+1)+1 );
    if (foreign_bkt_counter){
        update_choice((continuation_t)fun, 2*arity + 4 );
    } else {
        TRAIL_CHOICE((continuation_t)fun,2*arity+4);
    }
}

