/*************************************************************
 * $Id$
 * Copyright (C) 1997, 2002, 2003, 2004, 2005, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2016 Eric de la Clergerie
 * ------------------------------------------------------------
 *
 * Run Time   --
 *
 * ------------------------------------------------------------
 * Description
 *      DyALog runtime.
 *
 * ------------------------------------------------------------
 */

#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include <sys/time.h>
#include <sys/resource.h>
#include <signal.h>

#include <fcntl.h>
#include <errno.h>
#include <syslog.h>
#include <sys/stat.h>

#include "libdyalog.h"
#include "builtins.h"

DSO_LOCAL extern obj_t dyalog_solutions;
extern fol_t _dyalog_argv;
extern int _dyalog_initialization_stamp;
extern int dyalog_compact_forest;

DSO_LOCAL extern int keep_backptr;

DSO_LOCAL extern void internal_init();
extern void folsmb_exec_declaration( fol_t, fkey_t );
extern tabobj_t rt_install_database( fol_t, fkey_t );
extern void File_Include( const char *, const obj_t);
extern void parse_from_stm(int, obj_t);
DSO_LOCAL extern void dyalog_loop();
extern void forest_display( obj_t );
extern void forest_count( obj_t );
extern Bool Close_1(Sproto);
DSO_LOCAL extern void initialization_table();
extern void DyALog_Persistent_Restore();

extern void Dyam_Reset_Viewers();

extern int server_accept( int );
extern int server_create();

extern void DyALog_Agenda_Delete_All();

typedef enum {Forest_Nothing=0, Forest_Display, Forest_Count} Forest_Action;

static char string_getc(FILE *);
static void usage( char *);
static void file_db( char *);
static void stringlex( FILE*, char port_getc(FILE*), char );

extern void add_path(const char *);

extern long M_Cpu_Time(void);

long utime;

static char *command;
static int server=0;
static int db_clause_flag;
static Forest_Action forest;
static int loop = 0;
static int daemonize=0;

// may register a DyALog function executed at the end of each loop
DSO_LOCAL fun_t dyalog_ender;

DSO_LOCAL int should_stop;

extern void C_To_DyALog();
extern void DyALog_Fail();
extern void DyALog_To_C();

extern void DyALog_Mutable_Reset();

/**********************************************************************
 * file_db
 **********************************************************************/

void
db_clause( obj_t handler, obj_t o, obj_t kk )
{
  fol_t t = (fol_t) o;
  fkey_t Sk(t) = (fkey_t) kk;
  
  if (FOLCMPP(t) && FOLCMP_FUNCTOR(t) == find_folsmb( ":-", 1 ))
      folsmb_exec_declaration( FOLCMP_REF(t,1), Sk(t) );
  else {
      rt_install_database( t, Sk(t) );
      db_clause_flag=1;
  }
}

DEFINE_STATIC_PROCEDURE(db_clause_handler,db_clause_closure,db_clause,0L,2);

void
file_db( char *filename )
{
  File_Include( filename, db_clause_handler );
}

/**********************************************************************
 * Reading from a string
 **********************************************************************/

char
string_getc( FILE *port )
{
  char **handle= (char **) port;
  char c = **handle;
  
  if (c) (*handle)++;

  return c;
}

static unsigned long
decode_verbose_level( const char * level )
{
    if (strcmp(level,"none") == 0)
        return 0;

    if (strcmp(level,"dyam") == 0)
        return V_DYAM;

    if (strcmp(level,"index") == 0)
        return V_INDEX;

    if (strcmp(level,"share") == 0)
        return V_SHARE;

    if (strcmp(level,"term") == 0)
        return V_TERM;

    if (strcmp(level,"tfs") == 0)
        return V_TFS;

    if (strcmp(level,"builtins") == 0)
        return V_BUILTINS;

    if (strcmp(level,"low") == 0)
        return V_LOW;
    
    return V_DYAM + V_INDEX + V_SHARE + V_TFS + V_BUILTINS + V_LOW;
}

void set_verbose_level(unsigned long level)
{
    LVALUE_R_VERBOSE_LEVEL = (TrailWord) level;
}

unsigned long get_verbose_level()
{
    return R_VERBOSE_LEVEL;
}

void exit_on_signal (int sig)
{
        /* Post Processing */
        // dyalog_printf("process signal %d forest=%d\n",sig,forest);
    if (!loop || sig != 14) {
        switch (forest) {
            case Forest_Display:
                forest_display( dyalog_solutions );
                break;
            case Forest_Count:
                forest_count( dyalog_solutions );
                break;
            default:
                break;
        }
        exit(1);
    } else {
        /* othwerwise return to almost normal */
        // dyalog_printf("alarm interrupt\n");
        should_stop=1;
    }
    
}

/**********************************************************************
 * Run
 **********************************************************************/


static void bye() 
{
    if (server) {
        close(server);
        server=0;
    }
    FREE_ALLOCATION();
}

int
main( int argc, char *argv[])
{
  int i=1;
  int utime_flag=0;
  long timeout = 0;
  
  loop = 0;
  server = 0;
  forest = Forest_Nothing ;
  
#ifdef HAVE_ATEXIT  
  atexit(bye);                 /* registering exit function */
#endif

//  signal(2,exit_on_signal);     /* on INT */
//  signal(3,exit_on_signal);    /* on QUIT */
//  signal(9,exit_on_signal);    /* on KILL */
//  signal(15,exit_on_signal);    /* on TERM */
  
//  LVALUE_R_VERBOSE_LEVEL= (TrailWord) 0;
  
//  setlocale(LC_CTYPE, "");        /* Set locale for lexer */
     
  command = argv[0];

  for(i=1; i < argc ;) {
    char *arg = argv[i++];
    if (strcmp(arg,"-daemonize") == 0) {
            /* daemonize the process => imply loop */
        daemonize=1;
        server=1;
        loop = 1;
        continue;
    }
  }

  if (daemonize) {
          /* daemonize  */
          /* need to daemoize very early, before running GC_INIT ! */
      pid_t pid, sid;
//      FILE *fp= NULL;
      printf("going to daemonize!\n");
      /* Fork off the parent process */
      pid = fork();
      if (pid < 0) {
          exit(EXIT_FAILURE);
      }
      if (pid > 0) {
          exit(EXIT_SUCCESS);
      }
      /* Change the file mode mask */
      printf("unmask\n");
      umask(0);
      sid = setsid();
      if (sid < 0) {
              /* Log the failure */
          exit(EXIT_FAILURE);
      }
      printf("change working dir\n");
      /* Change the current working directory */
      if ((chdir("/")) < 0) {
          exit(EXIT_FAILURE);
      }
      printf("closing channels\n");
      /* Close out the standard file descriptors */
      close(STDIN_FILENO); open("/dev/null", O_RDWR);
      close(STDOUT_FILENO); open("/dev/null", O_RDWR); 
      close(STDERR_FILENO); open("/dev/null", O_RDWR);
//      fp = fopen ("/tmp/log.txt", "w+");
//      fprintf(fp, "daemonized ...\n");
//      fflush(fp);
      sleep(2);
  }

  
  /* Internal Initialization : bigloo + DyALog */
  internal_init();

  /* Main Directive Initialization: defined in the generated MA file */
  main_directive_initialization();
  
  /* Parsing command line up to -a or end of line */
  for(i=1; i < argc ;) {
    char *arg = argv[i++];

    if (strcmp(arg,"-h") == 0 || strcmp(arg,"-help") == 0)
      usage( NULL );

    if (strcmp(arg,"-v") == 0  || strcmp(arg,"-verbose") == 0) {
        if (i == argc) {
            usage( arg );
        } else {
            LVALUE_R_VERBOSE_LEVEL = (TrailWord) (R_VERBOSE_LEVEL | decode_verbose_level(argv[i++]));
            continue;
        }
    }
    
    if (strcmp(arg,"-db") == 0){
      if (i == argc)
	usage( arg );
      else {
	file_db( argv[i++] );
	continue;
      }}

    if (strcmp(arg,"-forest") == 0) {
      forest = Forest_Display;
      continue;
    }

    if (strcmp(arg,"-compact") == 0) {
      forest = Forest_Display;
      dyalog_compact_forest=1;
      continue;
    }

    if (strcmp(arg,"-nocompact") == 0) {
      forest = Forest_Display;
      dyalog_compact_forest=0;
      continue;
    }

    if (strcmp(arg,"-fcount") == 0) {
      forest = Forest_Count;
      continue;
    }
      
    if (strcmp(arg,"-slex") == 0){
      if (i == argc) usage( arg );
      else {
	stringlex( (FILE*) &argv[i++], string_getc, '\0');
	continue;
      }}

    if (strcmp(arg,"-flex") == 0) {
      if (i == argc) usage( arg );
      else {
	char *filename = argv[i++];
	if (strcmp(filename,"-") == 0) {
	  stringlex( stdin, (char (*)(FILE*)) fgetc, EOF);
	} else {
	  FILE *file = fopen( filename, "r" );
	  stringlex( file, (char (*)(FILE*)) fgetc, EOF );
	  fclose(file);
	}
	continue;
      }
    }

    if (strcmp(arg,"-I") == 0) {
        if (i == argc)
            usage( arg );
        else {
            add_path( (char *)argv[i++] );
            continue;
        }
    }
    
    if (strcmp(arg,"-server") == 0) {
      /* server mode: to be refined! */
        server = 1;
        loop = 1;
        continue;
    }

    if (strcmp(arg,"-daemonize") == 0) {
            /* daemonize the process => imply loop */
        daemonize=1;
        server=1;
        loop = 1;
        continue;
    }

    if (strcmp(arg,"-loop") == 0) {
      /* loop mode: to be refined! */
        loop = 1;
        continue;
    }

    if (strcmp(arg,"-noforest") == 0) {
            /* don't keep backptrs for forest extraction */
//        keep_backptr=0;
        forest = Forest_Nothing;
        continue;
    }

    if (strcmp(arg,"-nobackptr") == 0) {
            /* don't keep backptrs for forest extraction */
        keep_backptr=0;
        continue;
    }

    if (strcmp(arg,"-utime") == 0) {
            /* display user time */
        utime_flag=1;
        continue;
    }
    
    if (strcmp(arg,"-timeout") == 0) {
        if (i == argc)
            usage( arg );
        else {
            timeout = strtol((char *)argv[i++],NULL,10);
            signal(14,exit_on_signal);    /* on ALRM */
            continue;
        }
    }

    if (strcmp(arg,"-no_occur_check") == 0) {
            /* display user time */
        LVALUE_R_OCCUR_CHECK=(TrailWord) 0;
        continue;
    }
    
    if (strcmp(arg,"-a") == 0 || strcmp(arg,"--") == 0)
        break;                   /* remaining options specific to DyALog application */

    if ( arg[0] == '-' && arg[1] != '\0' ){
//      usage( arg );
        i--;
        break;                  /* remaining options specific to DyALog application */
    }
    
				/*  default */

    file_db( arg );

  }

				/* Build options for DyALog */

  for(_dyalog_argv=FOLNIL ;
      i < argc ;
      _dyalog_argv = folcmp_create_pair( Create_Atom(argv[--argc]), _dyalog_argv )
      );

//  dyalog_printf("size of fol=%d\n",sizeof(union fol));
  

  if (server) {
      server = server_create();
  }
  
  do {
      int save_stdin=0;
      int save_stdout=0;
      
          // long utime;

      should_stop = 0;
          
    /* In server mode : read next database */
    if (server) {
        save_stdin = stm_stdin;
        save_stdout= stm_output;
        stm_output=server_accept( server );
        stm_stdin=stm_output;
    }
    
    if (loop) {
        db_clause_flag=0;
        parse_from_stm(stm_stdin,db_clause_handler);
        if (!db_clause_flag) {
            loop=0;
            break;
        }
        DyALog_Persistent_Restore();
    }

    utime = M_Cpu_Time();

    /* Main Initialization: defined in the generated MA file */
    main_initialization();

    if (timeout) alarm(timeout);
    

    
    /* Loop */
    dyalog_loop();

    if (timeout) alarm(0);
    
    /* Post Processing */
    switch (forest) {
    case Forest_Display:
      forest_display( dyalog_solutions );
      break;
    case Forest_Count:
      forest_count( dyalog_solutions );
      break;
    default:
      break;
    }

        /* execute registered loop ender function, if any*/
    if (dyalog_ender) {
        int should_stop_save=should_stop;
        if (timeout) alarm(timeout);
        should_stop = 0;
        Dyam_DyALog(dyalog_ender);
        if (timeout) alarm(0);
        if (should_stop_save) should_stop = 1;
    }
    
    
    utime=M_Cpu_Time()-utime;

    if (utime_flag) dyalog_printf("Time %dms%s\n",(long) utime,should_stop ? " (stopped) " : "");

    if (loop) {
        dyalog_printf("\n%%ENDLOOP\n");
        Flush_Output_0();
    }
    

    if (server) {
        Close_1( DFOLINT(stm_output), 0 );
        stm_stdin = save_stdin;
        stm_output= save_stdout;
    }

        /* Do some cleaning before next round */
    if (loop) {
        DyALog_Agenda_Delete_All();
        if (Stream_End_Of_Stream(stm_tbl+stm_stdin)==STREAM_EOF_AT) {
            loop=0;
        } else {
            _dyalog_initialization_stamp = (_dyalog_initialization_stamp + 1) % 2;
            dyalog_solutions = BNIL;
            Dyam_Reset_Viewers();
            DyALog_Mutable_Reset();
            LVALUE_LSTACK_TOP=LSTACK_BASE;
            initialization_table();
         }
    }
    
  } while (loop);

  if (server) {
    close(server);
    server=0;
  }
  
                                        /* Exit */
  
   return EXIT_SUCCESS;
}

/**********************************************************************
 * usage
 **********************************************************************/

void
usage( char *error )
{
  if (error) {
    printf( "Error with option %s\n"
	    "------------------------------------------------------------\n",
	    error );
  }
  printf(  "Usage for command %s\n"
	   "\t-h -help            -- this help\n"
           "\t-v <level>          -- trace for <level> in dyam, index, share, or all\n"
           "\t-verbose <level>    -- same as -v\n"
	   "\t-db <filename>      -- load database filename\n"
           "\t-I <path>           -- add <path> to DyALog search path\n"
	   "\t-server             -- enter server mode\n"
           "\t-loop               -- enter loop mode\n"
	   "\t-forest             -- display the shared forest\n"
	   "\t-fcount             -- count number of derivations per answer\n"
           "\t-no_occur_check     -- disable occur checking\n"
	   "\t-slex <string>      -- parse from <string>\n"
	   "\t-flex <filename>    -- parse from <filename>\n"
           "\t-timeout <time>     -- set timeout (in sec)\n"
	   "\t-a <args>           -- all remaining arguments given to DyALog\n"
           "\t-- <args>           -- all remaining arguments given to DyALog\n"
	   , command);
  exit(error ? EXIT_FAILURE : EXIT_SUCCESS);
}

void generic_usage ()
{
    usage(NULL);
}

/**********************************************************************
 * stringlex
 *     read a char file (or string) and install all chars as connect facts
 **********************************************************************/

void
stringlex( FILE *port, char port_getc(FILE *), char port_eof )
{
  int i=0;
  char c;
  fol_t f= find_folsmb("C",3);
  fkey_t k = Key0;
  fol_t t;
  for(; (c=port_getc(port)) != port_eof; )
    if (c != '\n') {
      FOLCMP_WRITE_START( f , 3);
      FOLCMP_WRITE( DFOLINT( i ) );
      FOLCMP_WRITE( DFOLCHAR( c ) );
      FOLCMP_WRITE( DFOLINT( i+1 ));
      t = FOLCMP_WRITE_STOP;
      rt_install_database( t, k );
      i++;
    }
  rt_install_database( folcmp_create_unary( "N", DFOLINT( i )), k);
}
    
/**********************************************************************
 * DyALog_Register_Ender
 *    register an closing function
 ***********************************************************************/

void
DyALog_Register_Ender(fun_t ender) 
{
//    dyalog_printf("registering ender %x\n",ender);
    dyalog_ender = ender;
}

long
DyALog_Utime()
{
    return M_Cpu_Time()-utime;
}

Bool
DyALog_Server_Active()
{
    return server;
}

Bool
DyALog_Set_Verbose_Level (char *level)
{
    unsigned long l = decode_verbose_level(level);
//    dyalog_printf("setting level %s => %d\n",level,l);
    if (l) {
        LVALUE_R_VERBOSE_LEVEL = (TrailWord) (R_VERBOSE_LEVEL | l);
    } else {
        LVALUE_R_VERBOSE_LEVEL = (TrailWord) (0);
    }
    Succeed;
}
