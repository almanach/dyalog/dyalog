/*************************************************************
 * $Id$
 * Copyright (C) 1998, 2006 Eric de la Clergerie
 * ------------------------------------------------------------
 *
 *   Pseudo Typed Feature Structure
 *
 * ------------------------------------------------------------
 * Description
 *   Pseudo implantation of Typed Feature Structures
 *   
 * ------------------------------------------------------------
 */

#include "libdyalog.h"
#include "builtins.h"

#ifdef HAVE_DLFCN_H
#include <dlfcn.h>
#endif

extern char * _dyalog_tfs_file;
static Bool (*_Tfs_Unif)(Sproto,Sproto);
static Bool (*_Tfs_Subs)(Sproto,Sproto);
static void (*_Tfs_Init)(void);
void 
Tfs_Init() 
{
#ifdef HAVE_DLFCN_H
  if (_dyalog_tfs_file) {
    void *handle;
//    V_DISPLAY( "TFS Opening %s\n",_dyalog_tfs_file);
    dyalog_printf( "TFS Opening %s\n",_dyalog_tfs_file);
    handle = dlopen(_dyalog_tfs_file,RTLD_LAZY);
    if (!handle) {
      fputs (dlerror(), stderr);
      exit(EXIT_FAILURE);
    }
    dyalog_printf( "Done TFS Opening\n");
    _Tfs_Unif = dlsym(handle,"Tfs_Unif");
    _Tfs_Subs = dlsym(handle,"Tfs_Subs");
    _Tfs_Init = dlsym(handle,"Tfs_Init");
    
    (*_Tfs_Init)();

  }
#endif
}


Bool
Tfs_Unif( SP(A), SP(B) )
{
#ifdef HAVE_DLFCN_H
  if (_dyalog_tfs_file) {
    if (!_Tfs_Unif)
      Tfs_Init();
    return (*_Tfs_Unif)(A,Sk(A),B,Sk(B));
  }
#endif
  dyalog_error_printf("*WARNING*: TFS unification not loaded : %&s vs %&s\n", 
		      A, Sk(A), B, Sk(B));
  Fail;
}

Bool
Tfs_Subs( SP(A), SP(B) )
{
#ifdef HAVE_DLFCN_H
  if (_dyalog_tfs_file) {
    if (!_Tfs_Subs)
      Tfs_Init();
    return (*_Tfs_Subs)(A,Sk(A),B,Sk(B));
  }
#endif
  dyalog_error_printf("*WARNING*: TFS subsumption not loaded : %&s vs %&s\n", 
		      A, Sk(A), B, Sk(B));
  Fail;
}



/**********************************************************************
 * Tfs_Dlopen_1
 **********************************************************************/

static Bool Tfs_Dlopen_1(Sproto);

Bool              /* DyALog Wrapper */
DYAM_Tfs_Dlopen_1(fol_t file)
{
    fkey_t k=R_TRANS_KEY;
    return Tfs_Dlopen_1(file,k);
}

static Bool	  /* DyALog Builtin */
Tfs_Dlopen_1( SP(file) )
{
    Deref_And_Fail_Unless(file, FOLSMBP(file));
    _dyalog_tfs_file = SymbolName(file);
    Tfs_Init();
    Succeed;
}

