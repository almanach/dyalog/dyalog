/************************************************************
 * $Id$
 * Copyright (C) 1996, 2003, 2006, 2009, 2010 Eric de la Clergerie
 * ------------------------------------------------------------
 *
 *   Init -- DyALog initialization
 *
 * ------------------------------------------------------------
 * Description
 *
 * DyALog internal initialization
 *
 * ------------------------------------------------------------
 */

#include "libdyalog.h"

/*---------------------------------------------------------------------*/
/*    Quelques macros de calcul de taille memoire pour les GCs         */
/*---------------------------------------------------------------------*/
#define MegToByte(x) ((x) * (1024 * 1024))

#define DEFAULT_HEAP_SIZE 4

/*---------------------------------------------------------------------*/
/*    Des recuperations externes                                       */
/*---------------------------------------------------------------------*/
extern char *getenv();

/*---------------------------------------------------------------------*/
/*    Une variable pour memoriser le bas de la pile                    */
/*---------------------------------------------------------------------*/

DSO_LOCAL extern void fol_init();
extern void install_std_optable();
DSO_LOCAL extern void initialization_table();
DSO_LOCAL extern void pathlist_init();
DSO_LOCAL extern void c_oset_initialize();

DSO_LOCAL void
internal_init()
{
   long  heap_size;
   long  mega_size;
   char *env_size;
   
   /* on initialise le tas */
   mega_size = (env_size = getenv( "DYALOG_HEAP" ))
     ? atoi( env_size )
     : DEFAULT_HEAP_SIZE
     ;
   heap_size = MegToByte( mega_size );

   if( !INIT_ALLOCATION( heap_size ) ) {
     char mes[ 600 ];
     sprintf( mes, "%ld Meg wanted", heap_size / (1024 * 1024) );
     fprintf( stderr, "Can't allocate heap: %s", mes);
     exit(EXIT_FAILURE);
   }

#if defined(MPROTECT_VDB) || defined(PROC_VDB)
       GC_enable_incremental();
#endif
   
   /* on initialise les objects pre-alloues (les chars, bool, ...) */
   /* init_objects(); */

   /* DyALog various initializations */
   fol_init();

   if (getenv("DYALOG_NO_OCCUR_CHECK"))
       LVALUE_R_OCCUR_CHECK=(TrailWord) 0;
   
   install_std_optable();
   initialization_table();
   pathlist_init();
   c_oset_initialize();
   Tfs_Init();			/* Typed Feature Structures */
}

