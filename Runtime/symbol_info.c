/************************************************************
 * $Id$
 * Copyright (C) 1997, 2003, 2006, 2008, 2009, 2010, 2011, 2014 Eric de la Clergerie
 * ------------------------------------------------------------
 *
 *    Symbol Info -- Symbol Info Management
 *
 * ------------------------------------------------------------
 * Description
 *
 * ------------------------------------------------------------
 */

#include <stdio.h>
#include <string.h>

#include "libdyalog.h"
#include "hash.h"

typedef struct {
  fol_t key;
  obj_t values;
} symbinfo_t;

static char *symbol_info_tbl;

#define START_SYMBINFO_TBL_SIZE      128
#define SYMBINFO_SIZE                sizeof( symbinfo_t )

static void
symbol_info_initialize()
{
  symbol_info_tbl = Hash_Alloc_Table( START_SYMBINFO_TBL_SIZE, SYMBINFO_SIZE );
}

obj_t
folsmb_info_find( key, type)
     fol_t key;
     obj_t type;
{
  symbinfo_t *info = (symbinfo_t *) Hash_Find( symbol_info_tbl, (long) key );
  obj_t      al;

  if (!info)
    return BFALSE;
  
  for (al = info->values; PAIRP(al); al = CDR(al)) {
    assert( PAIRP( CAR(al) ) );
    if ( CAR( CAR( al )) == type )
      return CAR( al );
  }

  return BFALSE;
}

void
folsmb_info_set( key, type, value)
     fol_t key;
     obj_t type, value;
{
  symbinfo_t tmp_info = {.key=key,  .values=BNIL};
  symbinfo_t *info = (symbinfo_t *) Hash_Insert( symbol_info_tbl, (char *) &tmp_info, (Bool) 0);
  obj_t al = info->values;

  for( ; PAIRP( al ) ; al = CDR( al ) )
    if (CAR(CAR( al )) == type) {
      obj_t entry = CAR( al );
      SET_CDR( entry, value );
      return;
    }

  info->values = MAKE_PAIR( MAKE_PAIR( type, value), info->values );
}

void
folsmb_info_del( key, type )
     fol_t key;
     obj_t type;
{
  symbinfo_t * info= (symbinfo_t *) Hash_Find( symbol_info_tbl, (long) key );
  obj_t *al;
 
  if (info)
    for( al = &info->values; PAIRP( *al ); al = &CDR( *al ) )
      if (CAR(CAR( *al )) == type ) {
	*al = CDR( *al );
	break;
      }
}

/**********************************************************************
 * Info Keys
 **********************************************************************/
  
#define PREFIX  0
#define INFIX   1
#define POSTFIX 2

/**********************************************************************
 * Prefix Infix Postfix info management
 **********************************************************************/

#define BTEST( _test ) ( (_test) != BFALSE )
#define BFAIL( _test ) ( (_test) == BFALSE )

/* precedence mask :=  <prec> <lassoc-bit> <rassoc-bit> */

enum opmode { fx, fy, xf, yf, xfx, yfx, xfy };

void
folsmb_reset_opmode( o )
      fol_t o;
{
  FOLSMB(o)->prefix = FOLSMB(o)->infix_or_postfix = 0;
}

void
folsmb_assign_prefix(o,prec,rassoc)
      fol_t  o;
      int    prec;
      Bool rassoc;
{
  FOLSMB(o)->prefix = OPINFO_MAKE(prec,OPINFO_PREFIX,
				 0 ,
				 rassoc ? OPINFO_RASSOC : 0
				 );
}

void
folsmb_assign_infix(o,prec,lassoc,rassoc)
      fol_t  o;
      int    prec;
      Bool lassoc,rassoc;
{
  FOLSMB(o)->infix_or_postfix = OPINFO_MAKE(prec,OPINFO_INFIX,
					   (lassoc ? OPINFO_LASSOC : 0),
					   (rassoc ? OPINFO_RASSOC : 0)
					   );
}

void
folsmb_assign_postfix(o,prec,lassoc)
      fol_t  o;
      int    prec;
      Bool lassoc;
{
  FOLSMB(o)->infix_or_postfix = OPINFO_MAKE(prec,OPINFO_POSTFIX,
					   lassoc ? OPINFO_LASSOC : 0,
					   0
					   );
}

void
install_opmode( prec, opmode, op )
     int prec;
     enum opmode opmode;
     char *op;
{
  Bool lassoc=0, rassoc=0;
  int  type;

  switch (opmode) {

  case fy:
    rassoc=1;
  case fx:
    type = PREFIX;
    break;
    
  case yf:
    lassoc=1;
  case xf:
    type=POSTFIX;
    break;
    
  case xfy:
    rassoc=1;
  case xfx:
    type=INFIX;
    break;

  case yfx:
    lassoc=1;
    type=INFIX;
    break;

  default:
    assert(!"Bad op mode");
    return;

  }

  switch (type) {
    case PREFIX:
      folsmb_assign_prefix( find_folsmb(op,1), prec, rassoc);
      break;
    case POSTFIX:
      folsmb_assign_postfix( find_folsmb(op,1), prec, lassoc );
      break;
    case INFIX:
      folsmb_assign_infix( find_folsmb(op,2), prec, lassoc, rassoc );
      break;
    default:
      break;
  }

}

void
install_opmode_wrapper( prec, mode, op)
     int prec;
     char *mode;
     char *op;
{
  static const char *tab[]={ "fx", "fy", "xf", "yf", "xfx", "yfx", "xfy" };
  enum opmode opmode = fx;

  for ( ; (opmode <= xfy) && (strcmp(tab[opmode],mode) != 0) ; opmode++);
  install_opmode(prec,opmode,op);

}

Bool
is_ok_prefix_p( s, prec )
      char *s;
      int  prec;
{
  obj_t o = is_folsmb(s,1);
  int_t mask;
  return BTEST(o) && (mask = FOLSMB(o)->prefix) && ( prec >= OPINFO_PREC(mask));
}

Bool
is_ok_infix_p( s, prec, parent_prec )
      char *s;
      int  prec,parent_prec;
{
  obj_t o = is_folsmb(s,2);
  int  mask;
  int  op_prec;
  return ( BTEST(o)
	   && ((mask = FOLSMB(o)->infix_or_postfix) & OPINFO_INFIX)
	   && ( prec >= (op_prec = OPINFO_PREC(mask)))
	   && ( parent_prec <= ((mask & OPINFO_LASSOC) ? op_prec : op_prec-1)));
}

Bool
is_ok_postfix_p( s, prec, parent_prec )
      char *s;
      int  prec,parent_prec;
{
  obj_t o = is_folsmb(s,1);
  int mask;
  int op_prec;
  return ( BTEST(o)
	   && ((mask = FOLSMB(o)->infix_or_postfix) & OPINFO_POSTFIX)
	   && ( prec >= (op_prec = OPINFO_PREC(mask)))
	   && ( parent_prec <= ((mask & OPINFO_LASSOC) ? op_prec : op_prec-1)));
}

/**********************************************************************
 * HiLog
 **********************************************************************/

Bool
is_ok_hilog_p( s )
     char *s;
{
  obj_t o = is_folsmb(s,0);
  return (BTEST(o) && FOLSMB_HILOG_P(o));
}

/**********************************************************************
 * Feature Info Management
 **********************************************************************/

static obj_t
safe_tmp( fol_t oo )
{
  obj_t tmp = folsmb_info_find(oo,INFO_FEATURE);
  return tmp;
}

obj_t
folsmb_feature_info( o )
     fol_t o;
{
  fol_t oo = FOLSMB_CONVERT_ARITY(o,0);
//  dyalog_printf("Feature info %s\n",FOLSMB_NAME(o));
  //  return FOLSMB(oo)->is_feature ? folsmb_info_find(oo,INFO_FEATURE) : BFALSE;
  return FOLSMB(oo)->is_feature ? safe_tmp(oo) : BFALSE;
}

obj_t
is_folsmb_feature_info( s )
     char *s;
{
  obj_t o = is_folsmb(s,0);
  return BTEST(o) ? folsmb_feature_info( (fol_t) o) : BFALSE;
}

void
folsmb_feature_set( o , features )
     fol_t o;                   /* o should be without the FEATURE_SPACE suffix */
     fol_t features;
{
  fol_t oo = find_module_folsmb(FEATURE_SPACE,0,FOLSMB_CONVERT_ARITY(o,0));
  folsmb_info_set(oo, INFO_FEATURE, (obj_t) features);
  FOLSMB( oo )->is_feature = 1;
}

/**********************************************************************
 * Finite Set Info Management
 **********************************************************************/

obj_t
folsmb_fset_info( o )
     fol_t o;
{
  fol_t oo = FOLSMB_CONVERT_ARITY(o,0);
  return FOLSMB_FSET(oo) ? folsmb_info_find(oo,INFO_FSET) : BFALSE;
}

obj_t
is_folsmb_fset_info( s )
     char *s;
{
  obj_t o = is_folsmb(s,0);
  return BTEST(o) ? folsmb_fset_info( (fol_t) o) : BFALSE;
}

static void
set_fsetelt( fol_t elements ) 
{
    unsigned int block = 0;
    unsigned long mask = 1;
    unsigned int n=0;
    unsigned int arity = FOLCMP_ARITY(elements);
    fol_t *arg = &FOLCMP_REF(elements,1);
    fol_t *stop = arg+arity;
//    dyalog_printf("register finite set table %&f x=%x\n",elements,elements);
    for(block=1; ; block++) {
        for(mask = 1, n=0; n < FSET_BIT_PER_WORD; n++, mask <<= 1) {
            if (arg < stop) {
                fol_t v = *arg;
                if (FOLSMBP(v)) {
                    fsetelt_t *fset_list = &FOLSMB_FSETELT(v);
                    fsetelt_t new = (fsetelt_t) GC_MALLOC_PRINTF("fsetelt",sizeof( struct fsetelt ));
                    new->fset_table=elements;
                    new->block = block;
                    new->next = *fset_list;
                    new->mask = mask;
                    new->unif_mask = mask;
                    new->subs_mask = mask;
                    if (*fset_list) {
                        new->unif_mask |= (*fset_list)->unif_mask;
                        if ((*fset_list)->subs_mask != mask)
                            new->subs_mask = 0;
                    }
                    *fset_list=new;
                }
                arg++;
            } else
                return;
        }
    }
}

void
folsmb_fset_set( o , elements )
     fol_t o;                   /* o is assumed without FSET_SPACE suffix */
     fol_t elements;
{
  fol_t oo = find_module_folsmb(FSET_SPACE,0,FOLSMB_CONVERT_ARITY(o,0));
  folsmb_info_set(oo, INFO_FSET, (obj_t) elements);
  if (!FOLSMB_FSET(oo))
      set_fsetelt(elements);
      /* WARNING: EVDLC 04/05/2008
         should add more tests about the fact that a finite set
         may be removed or re-declared
      */
  FOLSMB_SET_FSET( oo );
}


/**********************************************************************
 * Declaration Info Management
 **********************************************************************/

obj_t
folsmb_declaration_info( o )
     fol_t o;
{
  return folsmb_info_find(o,INFO_DECLARATION);
}

void
folsmb_declaration_set(s,n,decl)
     char *s;
     int  n;
     obj_t decl;
{
  fol_t o = find_folsmb(s,n);
  folsmb_info_set(o,INFO_DECLARATION,decl);
  if (n == 1 )
    folsmb_assign_prefix(o,900,0); /* o is fx prec=900 */
}

void
folsmb_exec_declaration(t,k)
     fol_t t;
     fkey_t k;
{
  fol_t f = FOL_FUNCTOR(t);
  obj_t decl;

  if (f == FOLCOMA) {
    folsmb_exec_declaration( FOLCMP_REF(t,1), k);
    folsmb_exec_declaration( FOLCMP_REF(t,2), k);
  } else if ((decl = folsmb_declaration_info(FOL_FUNCTOR(t))) == BFALSE)
    dyalog_error_printf( "WARNING : Not a declaration %&f\n", t );
  else {
    decl = CDR(decl);
    PROCEDURE_ENTRY(decl)(decl,(obj_t)t, (obj_t) k, BEOA);
  }
}

/**********************************************************************
 * DerefTerm Declaration
 **********************************************************************/

Bool
folsmb_derefterm_info( o )
     fol_t o;
{
  return BTEST(folsmb_info_find(o,INFO_DEREFTERM));
}

void
folsmb_switch_derefterm(o)
     fol_t o;
{
  folsmb_info_set(o,INFO_DEREFTERM,BTRUE);
}

/**********************************************************************
 * Standard Installer
 **********************************************************************/

#define INSTALLER(_type,_wrapper)			\
extern void _wrapper( obj_t, obj_t);			\
							\
DEFINE_STATIC_PROCEDURE( _type##_installer_handler,	\
			 _type##_installer_closure,	\
			 _wrapper,			\
			 0L,				\
			 2 );
		 

INSTALLER(hilog, Hilog_Installer_Wrapper);
INSTALLER(feature, Feature_Installer_Wrapper);
INSTALLER(op, Op_Installer_Wrapper);
INSTALLER(derefterm, DerefTermSet_Installer_Wrapper);
INSTALLER(subtypes, Subtypes_Installer_Wrapper);
INSTALLER(intro, Intro_Installer_Wrapper);
INSTALLER(finite_set,Finite_Set_Installer_Wrapper);
INSTALLER(subset,Subset_Installer_Wrapper);
          
static
struct {
  int prec;
  enum opmode mode;
  char *name;
} optable[] = {
    {1200, xfx, ":- -->"},
    {1200, fx, ":- ?-"},
    {1100, xfy, ";"},
    {1050, xfy, "->"},
    {1000, xfy, ","},
    {900, fy, "\\+ spy nospy"},
    {700, xfx, "= is =.. == @< @> @=< @>= \\== =:= =\\= < > =< >= .."},
    {690, fx, "` ``"},
    {600, xfy, ": ::"},
    {590, xfy, "=>"},
    {580, xfy, "&&"},
    {550, fx, "~"},
    {550, xfx, "@"},
    {500, yfx, "- + \\/ /\\"},
    {500, fx, "- +"},
    {400, yfx, "* / // << >> div"},
    {300, xfx, "mod"},
    {200, xfy, "^"},
    
    {900, xfy, "&"},
    {700,  xf, "?"},
    {700, xfx, "isagg"},
    {700,  xf, "@* @+ @?"},
        
    {550, xfy, ":>"},
    
    {-1, xfx, ""}			/* to end the installation */

};

static char *feature_fun_tbl[] = {
//  "*ITEM*	top	bottom",
//  "*HITEM* 	top",
//  "*FIRST* 	call	right",
  "*NEXT* 	nabla	call	ret	right",
//  "*CURNEXT1*	ret 	call	right	x	nabla",
//  "*CURNEXT1/LIGHT*	ret	call	x 	nabla",
//  "*LAST*	last",
  "*ANSWER*	answer",
//  "*EVAL*	eval	rest",
//  "*GUARD*	eval	true	false",
//  "*DATABASE*	data",
//  "*PROLOG-FIRST*	call	right",
  "*PROLOG*	call	right",
//  "*PROLOG-NEXT* call	ret	right",
//  "*PROLOG-CURNEXT* 	ret 	call	right	cont",
  "*PROLOG-ITEM*	top	cont",
  "*WAIT-CONT*	rest	cont",
//  "*UNDO*	rest",
  ""
};

static void
install_feature_from_string( buff )
     char *buff;
{
  char *sep="\t ";
//  fol_t functor = find_module_folsmb("feature",0,find_folsmb( strtok( buff, sep ), 0 ));
  fol_t functor = find_folsmb( strtok( buff, sep), 0 );
  char *feature;
  int arity = 0;
  fol_t table;
  FOLCMP_WRITE_LIGHT_START;
  for( feature = strtok( NULL, sep); feature ; feature = strtok( NULL, sep ), arity++ )
    FOLCMP_WRITE( find_folsmb( feature, 0 ) );
  
  table=FOLCMP_WRITE_LIGHT_STOP( find_folsmb( "features", arity), arity );
  folsmb_feature_set( functor, table );
}

void
install_std_optable()
{
  int i=0;
  char buff[200];
  char *name;
                    
                      /* Initialize Symbol Info mechanism */

  symbol_info_initialize();	


				/* Install Standard Operators */

  for( ; optable[i].prec != -1 ; i++ ){
    int prec = optable[i].prec;
    enum opmode mode = optable[i].mode;
    strcpy(buff,optable[i].name);

    for( name = strtok( buff, " "); name; name = strtok( NULL, " " ) ){
      install_opmode( prec, mode, name );
    }

  }

				/* Install Standard Feature Functors */

  for(i=0 ; *feature_fun_tbl[i] ; i++ ){
    strcpy(buff,feature_fun_tbl[i]);
    install_feature_from_string( buff );
  };
                                /* Install Standard deref_term functors */

  folsmb_switch_derefterm( FOLFSET );
  folsmb_switch_derefterm( FOLLOOP );
  folsmb_switch_derefterm( FOLRANGE );
  
				/* Install declarations */

  folsmb_declaration_set( "hilog", 1, hilog_installer_handler );
  folsmb_declaration_set( "features", 2, feature_installer_handler);
  folsmb_declaration_set( "op",3,op_installer_handler);
  folsmb_declaration_set( "deref_term",1,derefterm_installer_handler);
  folsmb_declaration_set( "subtypes",2,subtypes_installer_handler);
  folsmb_declaration_set( "intro",2,intro_installer_handler);
  folsmb_declaration_set( "finite_set",2,finite_set_installer_handler);
  folsmb_declaration_set( "subset",2,subset_installer_handler);
  
}







