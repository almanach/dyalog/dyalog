/*************************************************************
 * $Id$
 * Copyright (C) 1997, 2003, 2004, 2008, 2010, 2014 Eric de la Clergerie
 * ------------------------------------------------------------
 *
 *   File -- File Managment
 *
 * ------------------------------------------------------------
 * Description
 *
 * ------------------------------------------------------------
 */

#include <stdio.h>
#include <unistd.h>
#include <sys/param.h>
#include <string.h>

#include "libdyalog.h"

extern char *M_Absolute_File_Name_Alt(const char *, const char *);
extern char *M_Absolute_File_Name(const char *);
extern char *M_Get_Working_Dir();

extern void parse_from_file(const char *, obj_t);
extern void parse_from_stdin(obj_t);

typedef struct pathnode *pathlist;
typedef char PathStr[MAXPATHLEN];

struct pathnode {
  PathStr  path;
  pathlist next;
};

#define PATHNODE_SIZE (sizeof( struct pathnode))

static struct pathnode current_path = { "./", 0};
pathlist _dyalog_pathlist= &current_path;

static obj_t _current_handler;
static PathStr tmp;

/**********************************************************************
 * add_path
 **********************************************************************/

void
add_path( const char *path )
{
  pathlist cell = (pathlist) GC_MALLOC( PATHNODE_SIZE );
  strcpy( cell->path, path );
  cell->next = _dyalog_pathlist->next;
  _dyalog_pathlist->next = cell;
}

static
char *
search( const char *path, const char *file )
{
    static PathStr abs_path;
    char *dest;
    sprintf(abs_path,"%s",M_Absolute_File_Name(path));
    dest = M_Absolute_File_Name_Alt( abs_path, file );
    V_LEVEL_DISPLAY(V_DYAM,"File search %s access=%s\n",
                    dest,access(dest,F_OK) ? "NO" : "YES");
    return ( dest && access( dest , F_OK ) == 0 ) ? dest : NULL;
}

char *
search_file( const char *filename )
{
  pathlist list;
  char *dest;

  /* Try to find file using pathes in _dyalog_pathlist
     current path being the first of the list
     */
  
  for(list = _dyalog_pathlist ; list ; list = list->next ){
    if ((dest=search( list->path, filename)))
        return dest;
  }
  
  return NULL;
}

/**********************************************************************
 * c_file_suffix(filename)
 *
 *       suffix /a/b.c is "c"
 *       suffix /a.b/c is ""
 *       suffix /a/.b  is ""
 *       suffix .a     is ""
 **********************************************************************/

char *
c_suffix(const char *filename)
{
  char *suffix=strrchr(filename,'.');
  return (suffix && suffix != filename && *(suffix-1) != '/') ? suffix+1 : NULL;
}

void
dyalog_dirname( PathStr buff, const char *filename )
/* filename must be an abolute path */
{
  char *p = strrchr( filename, '/' );
  if (p)
    strncpy( buff, filename, p-filename );
}

/**********************************************************************
 * add_suffix( filename, suffix )
 *    add .suffix to filename
 **********************************************************************/
char *
add_suffix( const char *filename, const char *suffix )
{
  static PathStr buff;
  sprintf( buff, "%s.%s", filename, suffix );
  return buff;
}

/**********************************************************************
 * Parse_File
 **********************************************************************/
void
Parse_File( const char *filename, const obj_t handler )
        /* filename is an absolute path */
{
  PathStr save;

  strcpy( save, current_path.path );
  dyalog_dirname( current_path.path, filename );

  parse_from_file( filename, handler );
  strcpy( current_path.path, save );

}

/**********************************************************************
 *  File_Include
 **********************************************************************/
void
File_Include( const char *filename, const obj_t handler )
{
  char *absfilename;
  obj_t old_handler=_current_handler;
  
  _current_handler=handler;

  if (filename[0] == '-' && filename[1] == '\0')
    parse_from_stdin(handler);
  else if ((absfilename = search_file( filename )))
    Parse_File(absfilename,handler);
  else if ( (!c_suffix(filename)) &&
	    (absfilename=add_suffix(filename,"pl"),
	     absfilename=search_file(absfilename)))
    Parse_File(absfilename,handler);
  else {
    fprintf( stderr, "No file %s\n", filename );
    exit(EXIT_FAILURE);
  }
  _current_handler=old_handler;
}

void
Subfile_Include( const char *filename )
{
  File_Include( filename, _current_handler );
}

/**********************************************************************
 * Initialization
 **********************************************************************/

extern char *_dyalog_package;

DSO_LOCAL void
pathlist_init()
{
    char *env_dyalog_path = getenv("DYALOG_LOADPATH");
    getcwd( current_path.path, sizeof( current_path.path )-1 );
    strcpy( tmp, __DYALOG_PACKAGE_PATH__ "/Compiler");
    add_path( tmp );
    strcpy( tmp, __DYALOG_INCLUDE_PATH__ );
    add_path( tmp );
    if (env_dyalog_path) {
        char *cp = strdup(env_dyalog_path);
        const char sep[] = ":";
        char *file;
        while ((file=strsep(&cp,sep))) {
            add_path( file );
        }
    }
}

