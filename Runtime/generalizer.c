/************************************************************
 * $Id$
 * Copyright (C) 1997, 2003, 2006, 2009, 2010 Eric de la Clergerie
 * ------------------------------------------------------------
 *
 *   Generalizer --
 *
 * ------------------------------------------------------------
 * Description
 *    Building Generalizers of terms
 * ------------------------------------------------------------
 */

#include "libdyalog.h"
#include "builtins.h"

/**********************************************************************
 * prototypes
 **********************************************************************/

DSO_LOCAL extern Bool sfol_layer_identical( Sproto, Sproto );

/**********************************************************************
 * variables
 **********************************************************************/

static unsigned long anchor_generator, gc_generator;
static fkey_t k_top;

/**********************************************************************
 * Macros
 **********************************************************************/

#define New_Gc_Var()            FOLVAR_FROM_INDEX( gc_generator++ )

#define New_Anchor_Var()        FOLVAR_FROM_INDEX( anchor_generator++ )

#define Sset( u, v )				\
     ({						\
       u = v;					\
       Sk( u ) = Sk( v );			\
     })

#define Sget( u, k )				\
     ({						\
       Sk( u ) = k;				\
       u = *H;					\
     })

#define FOLCMP_WRITE_ADVANCE         (LVALUE_H)++

#define Same_Functor( l, r )    (FOLCMPP( l ) && FOLCMPP( r ) && (FOLCMP_SAMEFUNP(l,r))) 

/**********************************************************************
 * closure_gcul_deref
 *  follow L-bindings, left GC bindings and U-bindings
 **********************************************************************/

static cgbinding_t
Find_GC_Binding( SP(X) )
{
    cgbinding_t binding = C_TRAIL_CGBINDING;
    for( ; binding; binding = binding->next )
        if (SFOL_EQ(X,Sk(X),binding->binder,binding->k_binder))
            break;
    return binding;
}

static binding_t
CG_Deref( SP(X) ) 
{
    Bool free;
        
    for(;;) {
        V_DISPLAY( "GC_Deref %&p\n",X,Sk(X) );
        free=1;
            /*  X is a CG variable => CG bound */
        if (FOL_DEREFP(X) && Sk(X) == k_top) {
            cgbinding_t cgb = Find_GC_Binding(FOL_DEREF_VAR(X),Sk(X));
                /* do not cycle if var self bound */
            V_DISPLAY( "cgb %x left=%&f %&k\n",(long)cgb,cgb->left,cgb->k_left);
            if (cgb->left == FOL_DEREF_VAR(X) && cgb->k_left == Sk(X))
                break;
            free=0;
            X=cgb->left;
            Sk(X)=cgb->k_left;
        }

            /* Follow other bindings */
        if (CLOSURE_UL_DEREF(X,Sk(X)))
            free=0;

            /* break if free or not bound to a deref term */
        if (free || !FOL_DEREFP(X))
            break;
    }

    V_DISPLAY( "-->%&p\n",X,Sk(X) );
    
    ASM_LOAD(X,Sk(X),0);
    
}

#define Special_Deref(X) if( FOL_DEREFP(X) ) WRAP_DEREF(CG_Deref,X,Sk(X))

/**********************************************************************
 * Init_Variable_Generator
 **********************************************************************/
static unsigned long
Init_Variable_Generator( SP(t), unsigned long v )
{
  if (FOLVARP( t )) {
    unsigned long x = FOLVAR_INDEX( t );
    if ( x > v ) v = x;
    if (ONCE_L_DEREF( t, Sk(t) ))
      v = Init_Variable_Generator( S(t), v );
  } else if (FOLCMPP( t )) {
    obj_t tuple = FOLINFO_TUPLE( t );
    for (; PAIRP(tuple); tuple = CDR(tuple))
      v = Init_Variable_Generator( (fol_t) CAR( tuple ), k_t, v );
  }
  return v;
}

/***********************************************************************
 *  Replace cx by A or by a variable bound to A
 **********************************************************************/
static
fkey_t CX_replace( SP(cx), SP(A) ) 
{
    Sdecl(tmp);
    Sset(tmp,cx);
    Deref(tmp);
    
    if (FOLVARP(cx) && SFOL_EQ(tmp,Sk(tmp),A,Sk(A))) {
            /* if cx is bound to A, replace A by cx */
        Sset(A,cx);
    } else if (FOLVARP(cx) && Sk(cx) != k_top && Sk(tmp) != k_top) {
            /* cx is a variable not CG and not bound to CG */
        TRAIL_UBIND(cx,Sk(cx),A,Sk(A));
        Sset(A,cx);
    } else if (Sk(A)==k_top && Sk(cx) && Sk(cx) != k_top) {
            /* if A is a GC variable and cx is not ground and not on top */
        cx = New_Anchor_Var();
        TRAIL_UBIND(cx,Sk(cx),A,Sk(A));
        Sset(A,cx);
    } 

    *H=A;
    return Sk(A);
    
}

/**********************************************************************
 * Find_Or_Bind
 *    - check if an entry for (Sl, Sr) has already been made
 *    and create a new entry if not
 *    - return the binding associated to this entry
 **********************************************************************/
static cgbinding_t
Find_Or_Bind( SP(l), SP(r) )
{
  cgbinding_t binding = C_TRAIL_CGBINDING;
  fol_t X;
  
  V_DISPLAY( "\tfinding index for %&p and %&p\n ", S(l), S(r) );		 

  for( ; binding; binding = binding->next ) {
    if (
	 sfol_layer_identical( l, Sk(l), binding->left, binding->k_left) &&
	 sfol_layer_identical( r, Sk(r), binding->right, binding->k_right) ){
            /* reuse GC-binding */
        V_DISPLAY( "\tfound %&p\n", binding->binder, binding->k_binder);
        return binding;
    }    
  }
  
      /* Creation of a new CG-binding */

  X=New_Gc_Var();
  
  if (FOLVARP(l) && Sk(l) != k_top) {
      l=X;
      Sk(l)=k_top;
  }
  
  binding =  TRAIL_CGBIND( X,
			   k_top,
			   l, Sk(l),
			   r, Sk(r) );
  
  V_DISPLAY( "\t\t not found --> creation %&p\n",binding->binder, binding->k_binder);

  return binding;
  
}
      
/*
 * Misc for finite sets
 */

/* this function should move in another file (fol.c?) */
int                             
FSet_Rank(fol_t a, fol_t table) 
{
    int arity = FOLCMP_ARITY(table);
    fol_t *arg = &FOLCMP_REF(table,1);
    fol_t *stop = arg + arity;
    int rank = 1;
    for( ; arg < stop; arg++, rank++ ) {
        if (a == FOLCMP_REF(table,rank))
            return rank;
    }
    return 0;
}

static fkey_t
FSet_Common_Generalizer(fol_t table, SP(l), SP(r), SP(cx))
{
    cgbinding_t binding=Find_Or_Bind(l,Sk(l),r,Sk(r));
    int arity = FOLCMP_ARITY(l);
    fol_t *arg_l= &FOLCMP_REF(l,3);
    fol_t *stop_l = arg_l+arity-2;
    fol_t *arg_r=  &FOLCMP_REF(r,3);
    
    FOLCMP_WRITE_START( FOLCMP_FUNCTOR(l), arity );
    FOLCMP_WRITE( binding->binder );
    FOLCMP_WRITE( table );

    for(;arg_l < stop_l; )
        FOLCMP_WRITE( UDFOLINT( UCFOLINT(*arg_l++) | UCFOLINT(*arg_r++)));

    return CX_replace(cx,Sk(cx),FOLCMP_WRITE_STOP,binding->k_binder);
}

static fkey_t
FSet_Extend(fol_t table, int rank, SP(l), SP(r), SP(cx))
{                               /* r is a constant with rank 'rank' in table */
    cgbinding_t binding=Find_Or_Bind(l,Sk(l),r,Sk(r));
    int arity = FOLCMP_ARITY(l);
    fol_t *arg_l= &FOLCMP_REF(l,3);
    fol_t *stop_l = arg_l+arity-2;
    
    dyalog_printf("FSet extend rank=%d\n",rank);
        
    FOLCMP_WRITE_START( FOLCMP_FUNCTOR(l), arity );
    FOLCMP_WRITE( binding->binder );
    FOLCMP_WRITE( table );

    for(;arg_l < stop_l;) {
        unsigned long n = UCFOLINT(*arg_l++);
        if (rank > FSET_BIT_PER_WORD)
            rank -= FSET_BIT_PER_WORD;
        else if (rank>0) {
            n |= (1 << (rank - 1));
            rank=0;
        }                 
        FOLCMP_WRITE( UDFOLINT(n) );
    }
    
    return CX_replace(cx,Sk(cx),FOLCMP_WRITE_STOP,binding->k_binder);
}


/**********************************************************************
 * Common_Generalizer Sl Sr
 *   Compute the most instanciated common generalizer of Sl and Sr
 *   keeping when possible bindings and skekeletons of Sl
 *   Assumptions:
 *        - No local unif bindings for Sl
 *        - No common variables between Sl and Sr
 *   Returns:
 *        - a fkey k_res and a term 'res' (at the top of the heap (*H))
 *        - the bindings for res are given by local unif bindings
 *        and possibly old unif bindings of Sl (subsumption bindings are
 *        also used to mark locally bound variables )
 *        - CG-bindings are used to instanciate Sres into Sl or Sr
 *        - CG-bindings are recorded to reuse them when possible
 **********************************************************************/
static fkey_t
Common_Generalizer( SP( l ), SP( r ), fkey_t Sk(cx) )
{
    fol_t cx=l;
    cgbinding_t binding;
    int rank;

    V_LEVEL_DISPLAY(V_DYAM,"Common_Generalizer %&p %&s\n",l,Sk(l),r,Sk(r));
    
    Special_Deref(l);           /* follow left GC-bindings for GC variables */
    Deref(r);

    V_LEVEL_DISPLAY(V_DYAM,"\t%&s %&s\n",l,Sk(l),r,Sk(r));
    
    if (FOL_GROUNDP(l) && l==r) {
            /* replace cx by l or a variable bound to l */
        return CX_replace(cx,Sk(cx),l,Key0);
    }

    if (FOLFSETP(l) && FOLFSETP(r)) {

        if (FOLCMP_ARITY(l) != FOLCMP_ARITY(r))
            goto end;
                
        if (! sfol_identical(FOLCMP_REF(l,2),Sk(l),FOLCMP_REF(r,2),Sk(r)) )
            goto end;
        else {
            return FSet_Common_Generalizer(FOLCMP_REF(l,2),
                                           l,Sk(l),
                                           r,Sk(r),
                                           cx,Sk(cx));
        }
    }

    if (FOLFSETP(l) && FOL_CSTP(r) && (rank=FSet_Rank(r,FOLCMP_REF(l,2)))) {
        return FSet_Extend(FOLCMP_REF(l,2),
                           rank,
                           l,Sk(l),
                           r,Sk(r),
                           cx,Sk(cx));
    }

    if (FOLFSETP(r) && FOL_CSTP(l) && (rank=FSet_Rank(l,FOLCMP_REF(r,2)))) {
        return FSet_Extend(FOLCMP_REF(r,2),
                           rank,
                           l,Sk(l),
                           r,Sk(r),
                           cx,Sk(cx));
    }
    
    
    if (FOLCMPP(l) && l==r) {
            /* same skeleton => recurse on tuple variables */
        obj_t tuple = FOLINFO_TUPLE( l );
        V_LEVEL_DISPLAY(V_DYAM,"\tsame skel\n");
        for (;                                                                    
             PAIRP( CDR(tuple) );                                                 
             tuple = CDR(tuple))
            Common_Generalizer( (fol_t) CAR(tuple), Sk(l),
                                (fol_t) CDR(tuple), Sk(r), Sk(l) );
        
            /* replace cx by l or a variable bound to l */
        return CX_replace(cx,Sk(cx),l,Sk(l));
    }

    if (FOLCMPP(l) && FOLCMPP(r) && Same_Functor(l,r)) {
            /* same fonctor => recurse on args */
        unsigned long arity = FOLCMP_ARITY( l );
        Bool   keep = True;
        fol_t *argl, *argr, *stop;
        Sdecl(res);
        fkey_t Sk(new_cx) = (cx == l) ? Sk(cx) : Sk(l);

        FOLCMP_WRITE_START( FOLCMP_FUNCTOR( l ), arity );
        
            /* Recurse on arguments */
        for( argl = &FOLCMP_REF( l, 1),
                 argr = &FOLCMP_REF( r, 1 ),
                 stop = argl + arity;
             argl < stop ;
             argl++, argr++ )
         {
           Sget( res, Common_Generalizer( *argl, Sk(l), *argr, Sk(r), Sk(new_cx)) );
           keep &= (res == *argl );
           FOLCMP_WRITE_ADVANCE;
         }

            /* Build a new term if one arg has been replaced (by a variable) */
        if (!keep){
            res = folcmp_find( DFOLCMP( (fol_t) H_STOP ) );
                /* if is was ground, new term is not and has all variables on k_top */
            Sk(res) = Sk(l) ? Sk(l) : k_top;
        } else {                 /* otherwise reuse l */
            res = l;
            Sk(res) = Sk(l);
        }
        
        FOLCMP_WRITE_ABORT;
        
            /* Replace cx by res or a variable bound to res */
        return CX_replace(cx,Sk(cx),res,Sk(res));
    }

end:
        /* Replace (or bind) cx by (to) a variable X
           - reuse already CG-bound variable X->{l,r} if present
           or create a new one
           - in case of creation, use cx if it is a variable
           and U-free cx 
        */
    
    binding=Find_Or_Bind(l,Sk(l),r,Sk(r));
    return CX_replace(cx,Sk(cx),binding->binder,binding->k_binder);
}

/**********************************************************************
 * Head function to Common_Generalizer
 **********************************************************************/
fkey_t
Head_Common_Generalizer( SP(l), SP(r) )
{
  anchor_generator = Init_Variable_Generator( S(l), 0 ) + 1;
  gc_generator=0;
  k_top = (fkey_t) LSTACK_PUSH_VOID;
  return Common_Generalizer( S(l), S(r), Sk(l) );
}

/**********************************************************************
 *  copy_term
 **********************************************************************/
static inline
fkey_t
copy_term( SP(t) )
{
  unsigned long  k;
  obj_t  env;

  Collapse_Unwrap( t, Sk(t), k, env);
  return (fkey_t) load_layer_archive( k, env ); 
}
  
/**********************************************************************
 * Built In Common_Generalizer
 **********************************************************************/

static Bool Builtin_Term_Subsumer(Sproto,Sproto,Sproto);

Bool              /* DyALog Wrapper */
DYAM_Builtin_Term_Subsumer(fol_t l,fol_t r,fol_t res)
{
    fkey_t k=R_TRANS_KEY;
    return Builtin_Term_Subsumer(l,k,r,k,res,k);
}

static Bool	  /* DyALog Builtin */
Builtin_Term_Subsumer( SP(l), SP(r), SP(res) )
{
  Sdecl( tmp );

  Sk(l) = copy_term( S(l) );

  Sget( tmp, Head_Common_Generalizer( S(l), S(r) ) );

  V_DISPLAY( "result is %&p -> %&s\n", S(tmp), S(tmp) );

  return Unify( res, k_res, tmp, k_tmp );
}
	
  
