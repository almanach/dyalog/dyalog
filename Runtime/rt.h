/* $Id$
 * Copyright (C) 1996, 2002, 2003, 2004, 2006, 2007, 2008, 2009, 2017 Eric de la Clergerie
 * ------------------------------------------------------------
 *
 *   RunTime --
 *
 * ------------------------------------------------------------
 * Description
 *
 * ------------------------------------------------------------
 */

#ifdef __cplusplus
extern "C" {
#endif

/**********************************************************************
 * STATISTICS
 **********************************************************************/

extern struct loop_stat *loop_stat;

/**********************************************************************
 * Loading .h
 **********************************************************************/

#include <assert.h>
#include <stdarg.h>

#include "param.h"
#include "archi.h"
#include "fol.h"
#include "vca.h"

int main_initialization();
int main_directive_initialization();

typedef Bool (*fun_t)();
typedef enum { f_init, f_call, f_and, f_or, f_join, f_indirect, f_rand, f_trace } backptr_t;

typedef struct tabobj *tabobj_t;
typedef struct tabseed *tabseed_t;

#include "trail.h"
#include "tfs.h"
#include "objects.h"

extern void Dyam_DyALog(fun_t);

extern void untrail_alt(TrailWord *stop);

typedef struct obj_cell *cell_t;

/**********************************************************************
 * Registers
 **********************************************************************/

				/* Registers */

#define R_TRANS	        ((tabobj_t) REG(I_TRANS))
#define R_TRANS_KEY	((fkey_t) REG(I_TRANS_KEY))
#define R_ITEM	        ((tabobj_t) REG(I_ITEM))
#define R_ITEM_KEY	((fkey_t) REG(I_ITEM_KEY))
#define R_ITEM_COMP	((fol_t) REG(I_ITEM_COMP))
#define R_BACKPTR	((obj_t) REG(I_BACKPTR))
#define R_OBJECT	((tabobj_t) REG(I_OBJECT))
#define R_LEVEL         ((long) REG(I_LEVEL))

#define R_INDEX         (long) (R_TOP - reg_bank)
#define R_DEBUG(s)      V_LEVEL_DISPLAY( V_LOW, s "%d\n", R_INDEX );

#define R_MODULE        (fol_t) REG(I_MODULE)



#define LVALUE_R_TRANS	        (LVALUE_REG(I_TRANS))
#define LVALUE_R_TRANS_KEY	(LVALUE_REG(I_TRANS_KEY))
#define LVALUE_R_ITEM	        (LVALUE_REG(I_ITEM))
#define LVALUE_R_ITEM_KEY	(LVALUE_REG(I_ITEM_KEY))
#define LVALUE_R_ITEM_COMP	(LVALUE_REG(I_ITEM_COMP))
#define LVALUE_R_BACKPTR	(LVALUE_REG(I_BACKPTR))
#define LVALUE_R_OBJECT	        (LVALUE_REG(I_OBJECT))
#define LVALUE_R_LEVEL          (LVALUE_REG(I_LEVEL))
#define LVALUE_R_MODULE         (LVALUE_REG(I_MODULE))



				/* Dealing with Item  and Transition */

#define R_LOAD_ITEM(item,item_k,item_comp)      \
R_DEBUG("Loading item");                        \
    TRAIL_REGISTERS();                          \
    LVALUE_R_ITEM = REG_VALUE(item);                              \
    LVALUE_R_ITEM_KEY = REG_VALUE(item_k);                        \
    LVALUE_R_ITEM_COMP = REG_VALUE(item_comp);

#define R_LOAD_TRANS(_trans,_trans_k)		\
R_DEBUG("Loading trans");			\
     LVALUE_R_TRANS = REG_VALUE(_trans);				\
     LVALUE_R_TRANS_KEY = REG_VALUE(_trans_k);

/**********************************************************************
 * CHOICE entries
 *    Materialize a choice point
 **********************************************************************/

#define TSCH( _pos ) ((_pos)->choice)

inline static
void TRAIL_CHOICE( continuation_t alt, int n )
{
    int i;
    PUSH_CTL_VAR_BOX(choice_t,box,n);
    V_LEVEL_DISPLAY(V_LOW,"PUSH CHOICE %d -> %d CP=%d P=%d TRAIL=%d IP=%d\n",(long)R_B,(long)box,(long)R_CP,(long)alt,(long)C_TRAIL_TOP,(long)REG(I_IP));
    box->type = CHOICE;			
    box->alt  = (continuation_t) Adjust_Address(alt);			
    box->cp   = R_CP;			
    box->e    = R_E;				
    box->prev = R_B;				
    box->bc   = R_BC;			
    box->top  = LSTACK_TOP;
    box->trail = C_TRAIL_TOP;
    box->min_layer = R_MIN_LAYER;
    box->trans= R_TRANS;
    box->trans_key = R_TRANS_KEY;
    box->item = R_ITEM;
    box->item_key = R_ITEM_KEY;
    box->n = n;
    for(i=0;i<n;i++) {
        *((TrailWord *)(box+1)+i)=X(i);
    }
    LVALUE_R_B = REG_VALUE(box);
}

inline static
choice_t TRAIL_CHOICE_LIGHT( continuation_t alt, int n )
{
    PUSH_CTL_VAR_BOX(choice_t,box,n);
    V_LEVEL_DISPLAY(V_LOW,"PUSH CHOICE %d -> %d CP=%d P=%d TRAIL=%d IP=%d\n",(long)R_B,(long)box,(long)R_CP,(long)alt,(long)C_TRAIL_TOP,(long)REG(I_IP));
    box->type = CHOICE;			
    box->alt  = (continuation_t) Adjust_Address(alt);			
    box->cp   = R_CP;			
    box->e    = R_E;				
    box->prev = R_B;				
    box->bc   = R_BC;			
    box->top  = LSTACK_TOP;
    box->trail = C_TRAIL_TOP;
    box->min_layer = R_MIN_LAYER;
    box->trans= R_TRANS;
    box->trans_key = R_TRANS_KEY;
    box->item = R_ITEM;
    box->item_key = R_ITEM_KEY;
    box->n = n;
    LVALUE_R_B = REG_VALUE(box);
    return box;
}


inline static
void TRAIL_CHOICE_VARARG( const continuation_t alt, const int n, ... )
{
    int i;
    va_list args;
    PUSH_CTL_VAR_BOX(choice_t,box,n);
    va_start(args, n);
    V_LEVEL_DISPLAY(V_LOW,"PUSH CHOICE %d -> %d CP=%d P=%d TRAIL=%d IP=%d\n",(long)R_B,(long)box,(long)R_CP,(long)alt,(long)C_TRAIL_TOP,(long)REG(I_IP));
    box->type = CHOICE;			
    box->alt  = (continuation_t) Adjust_Address(alt);			
    box->cp   = R_CP;			
    box->e    = R_E;				
    box->prev = R_B;				
    box->bc   = R_BC;			
    box->top  = LSTACK_TOP;
    box->trail = C_TRAIL_TOP;
    box->min_layer = R_MIN_LAYER;
    box->trans= R_TRANS;
    box->trans_key = R_TRANS_KEY;
    box->item = R_ITEM;
    box->item_key = R_ITEM_KEY;
    box->n = n;
    for(i=0;i<n;i++) {
        *((TrailWord *)(box+1)+i)=va_arg(args,TrailWord);
    }
    va_end(args);
    LVALUE_R_B = REG_VALUE(box);
}


#define LVALUE_CHOICE_ARG(box,n)   *((TrailWord *)(box+1)+n)

inline static
void UNTRAIL_CHOICE( choice_t box )
{
    int i;
    int n = box->n;
    untrail_alt(box->trail);
    LVALUE_R_B = REG_VALUE(box->prev);			
    LVALUE_R_CP = REG_VALUE(box->cp);			
    LVALUE_R_E = REG_VALUE(box->e);			
    LVALUE_R_BC = REG_VALUE(box->bc);
    LVALUE_R_P = REG_VALUE(box->alt);
    LVALUE_R_MIN_LAYER=REG_VALUE(box->min_layer);
    LVALUE_R_TRANS = REG_VALUE(box->trans);
    LVALUE_R_TRANS_KEY = REG_VALUE(box->trans_key);
    LVALUE_R_ITEM = REG_VALUE(box->item);
    LVALUE_R_ITEM_KEY = REG_VALUE(box->item_key);
    V_LEVEL_DISPLAY(V_LOW,"POP  CHOICE %d -> %d CP=%d P=%d TRAIL=%d IP=%d\n",(long)box,(long)R_B,(long)R_CP,(long)R_P,(long)C_TRAIL_TOP,(long)REG(I_IP));
    for(i=0;i<n;i++) {
        LVALUE_X(i) = REG_VALUE(*((TrailWord *)(box+1)+i));
    }
    LVALUE_C_CTL_TOP=REG_VALUE((TrailWord *) (box + 1)+n);
    LSTACK_POP( box->top );
        /* cleaning */
    box->prev=0;
    box->cp=0;
    box->e=0;
    box->bc=0;
    box->alt=0;
    box->min_layer=0;
    box->trans=0;
    box->trans_key=0;
    box->item=0;
    box->item_key=0;
    for(i=0;i<n;i++) {
        *((TrailWord *)(box+1)+i) = 0;
    }
}

inline static
void FOLLOW_CHOICE( choice_t box )
{
    int i;
    int n = box->n;
    untrail_alt(box->trail);
    LVALUE_R_CP = REG_VALUE(box->cp);			
    LVALUE_R_E = REG_VALUE(box->e);			
    LVALUE_R_BC = REG_VALUE(box->bc);
    LVALUE_R_P = REG_VALUE(box->alt);
    LVALUE_R_MIN_LAYER= REG_VALUE(box->min_layer);
    LVALUE_R_TRANS = REG_VALUE(box->trans);
    LVALUE_R_TRANS_KEY = REG_VALUE(box->trans_key);
    LVALUE_R_ITEM = REG_VALUE(box->item);
    LVALUE_R_ITEM_KEY = REG_VALUE(box->item_key);
    V_LEVEL_DISPLAY(V_LOW,"FOLLOW CHOICE %d -> %d CP=%d P=%d TRAIL=%d IP=%d\n",(long)box,(long)R_B,(long)R_CP,(long)R_P,(long)C_TRAIL_TOP,(long)REG(I_IP));
    for(i=0;i<n;i++) {
        LVALUE_X(i) = REG_VALUE(*((TrailWord *)(box+1)+i));
    }
    LVALUE_C_CTL_TOP=REG_VALUE((TrailWord *) (box + 1)+n);
    LSTACK_POP( box->top );
}

inline static
void update_choice(continuation_t fun, int n)
{
    int i=0;
    choice_t box = R_B;
    V_LEVEL_DISPLAY( V_DYAM,  "\tupdate choice point and registers %d\n", n);
    for(i=0;i<n;i++) {
        *((TrailWord *)(box+1)+i)=X(i);
    }
    box->alt = fun;
}

/*
  Pseudo Choice (used to save and restore X registers)
  For instance used when building terms
  They are unsafe and should not be confused with true choice points !
 */

inline static
void TRAIL_PSEUDO_CHOICE( int n )
{
    int i;
    PUSH_CTL_VAR_BOX(choice_t,box,n);
    V_LEVEL_DISPLAY(V_LOW,"PUSH PSEUDO CHOICE %d\n",n);
    box->type = CHOICE;			
    box->prev = R_B;				
    box->n = n;
    for(i=0;i<n;i++) {
        *((TrailWord *)(box+1)+i)=X(i);
    }
    LVALUE_R_B = REG_VALUE(box);
}

inline static
void UNTRAIL_PSEUDO_CHOICE( choice_t box )
{
    int i;
    int n = box->n;
    LVALUE_R_B = box->prev;			
    V_LEVEL_DISPLAY(V_LOW,"POP  PSEUDO CHOICE %d\n",n);
    for(i=0;i<n;i++) {
        LVALUE_X(i) = REG_VALUE(*((TrailWord *)(box+1)+i));
    }
        /* cleaning */
    box->prev=0;
    for(i=0;i<n;i++) {
        *((TrailWord *)(box+1)+i)=0;
    }
}

/**********************************************************************
 * ENVIRONEMENT entries
 *    Materialize a new environement (with essentialy a continuation)
 **********************************************************************/

#define TSEV( _pos ) ((_pos)->environment)

inline static
void TRAIL_ENVIRONMENT(int n)
{
    int i;
    PUSH_CTL_VAR_BOX(environment_t,box,n);
    V_LEVEL_DISPLAY(V_LOW,"PUSH ENV %d -> %d CP=%d\n",(long)R_E,(long)box,(long)R_CP);
    V_LEVEL_DISPLAY(V_LOW,"\ttop=%d MIN=%d\n",(long) LSTACK_TOP, (long) R_MIN_LAYER);
    box->type = ENVIRONMENT;			
    box->cp = R_CP;
    box->prev = R_E;
    box->trail = C_TRAIL_TOP;
    box->top = LSTACK_TOP;
    box->min_layer = R_MIN_LAYER;
    LVALUE_R_MIN_LAYER= REG_VALUE(LSTACK_TOP);
    box->trans = R_TRANS;
    box->trans_key = R_TRANS_KEY;
    box->item = R_ITEM;
    box->item_key = R_ITEM_KEY;
    box->n=n;
    for(i=0;i<n;i++) {          /* input argument in X(i) are saved as permanent */
        *((TrailWord *)(box+1)+i)=X(i);
    }
    LVALUE_R_E = REG_VALUE(box);
}

inline static
void UNTRAIL_ENVIRONMENT_ALT( environment_t box, int n)
{
    V_LEVEL_DISPLAY(V_LOW,"POP ENV %d -> %d R_CP=%d\n",(long)
                    box,(long)(box->prev),(long)(box->cp));
    V_LEVEL_DISPLAY(V_LOW,"\ttop=%d min=%d MIN=%d\n",(long) (box->top), (long)
                    (box->min_layer), (long) R_MIN_LAYER);
    LVALUE_R_E = REG_VALUE(box->prev);					
    LVALUE_R_CP = REG_VALUE(box->cp);
    if ((void *)R_B > (void *)box ) {
        /* nothing untrailed if a choice is waiting*/
        LVALUE_C_CTL_TOP = REG_VALUE(((TrailWord *) (R_B+1))+(R_B->n));
    } else {
        int i;
        fkey_t top=box->top;
        for(i=0; i < n ; ++i) {
            if (top <= (fkey_t)X(2*i+1)) {
                top = (fkey_t)X(2*i+1)+1;
            }
        }
        if (top <= (fkey_t) R_TRANS_KEY)
            top = (fkey_t) R_TRANS_KEY +1;
        if (top <= (fkey_t) R_ITEM_KEY)
            top = (fkey_t) R_ITEM_KEY +1;
        LVALUE_C_CTL_TOP = REG_VALUE(((TrailWord *)box)-1);
        if (R_MIN_LAYER >= top) {
            V_LEVEL_DISPLAY(V_LOW,"\tuntrail_alt new_top=%d new_trail->%d\n",
                            (long) top,
                            (long) box->trail);
            untrail_alt( box->trail );
            LSTACK_POP( top );
        }
    }
    if (R_MIN_LAYER > box->min_layer) {
        LVALUE_R_MIN_LAYER = REG_VALUE(box->min_layer);
    }
}

inline static
void UNTRAIL_ENVIRONMENT( environment_t box, int n)
{
    LVALUE_R_TRANS = REG_VALUE(box->trans);
    LVALUE_R_TRANS_KEY = REG_VALUE(box->trans_key);
    LVALUE_R_ITEM = REG_VALUE(box->item);
    LVALUE_R_ITEM_KEY = REG_VALUE(box->item_key);
    UNTRAIL_ENVIRONMENT_ALT( box, n);
}

/**********************************************************************
 * REGISTERS entries
 *    Save additional registers
 **********************************************************************/

#define TSRG( _pos )   ( (_pos)->registers )

    /* TRAIL_REGISTERS used to trail a modification of item-related registers
       no need to trail transition-related registers: they are saved in the choice points
    */

inline static
void TRAIL_REGISTERS()
{                                       
    PUSH_TRAIL_BOX(struct registers_box *,box);         
    box->type = REGISTERS;                   
    box->item = R_ITEM;
    box->k_item = R_ITEM_KEY;
    box->item_comp = R_ITEM_COMP;
}

inline static
void UNTRAIL_REGISTERS(struct registers_box *box)
{                                       
    LVALUE_R_ITEM = REG_VALUE(box->item);
    LVALUE_R_ITEM_KEY = REG_VALUE(box->k_item);
    LVALUE_R_ITEM_COMP = REG_VALUE(box->item_comp);
}

#ifdef __cplusplus
}
#endif
