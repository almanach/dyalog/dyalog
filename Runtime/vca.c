/************************************************************
 * $Id$
 * Copyright (C) 1996, 2006, 2008, 2009, 2011 Eric de la Clergerie
 * ------------------------------------------------------------
 *
 *   VCA -- Implementation of Vitual Copy Arrays
 *
 * ------------------------------------------------------------
 * Description
 *
 *   An efficient way to represent spare vectors as the leaves
 *   of a binary tree.
 *
 *   A VCA of length 2^h is given by a partial binary tree of maximal
 *   depth h. At leaves of depth h is associated an entry table to store
 *   at most 2^ENTRY_SHIFT values
 *
 *   vca_ref( vca, index)
 *           get index's value in vca
 *   vca_set( vca, index, value )
 *           set index's value in vca to value
 *   vca_merge( vca1, vca2 )
 *           merge vca1 with vca2 (keeping vca1's values)
 *
 * ------------------------------------------------------------
 */

#include "bigloo.h"        /* to define GC_MALLOC */

#include "vca.h"

long vca_stat_ref = 0 , vca_stat_in = 0, vca_stat_down = 0 ;

/***********************************************************************
 * vca_ref v n
 *    access the n^th element of vca v (or return NULL)
 ***********************************************************************/

entry_t
vca_ref( const vca_t v, const unsigned long n)
{
  unsigned long h,up;

  if ( v
       && VCA_FLAG_CHECK(v,n)
       && (up = ENTRY_UP( n )) < (h = v->length) ) {

    vcatree_t tree = v->tree;

    /* WARNING: we assume that if h=1 then v->tree is not empty
       this is the case if v is correctly normalized */
    
    do 
      if ((h >>= 1) == 0)   /* leaf with non empty entry table */
	return ENTRYTAB_REF( tree, ENTRY_DOWN( n ));
    while ((tree = VCATREE_SEL(tree, up, h)));
  }

  /* no entry found */
  return 0;

}

/**********************************************************************
 * vca_extend v n
 *    add to v a few top nodes with empty right branches to cover index n
 ***********************************************************************/

inline static void
vca_extend( vca_t v, unsigned long n)
{
  for ( ;							
	v->length <= n ;					
	v->length <<= 1 ){
      v->tree = VCATREE_MAKE( v->tree , VCATREE_NULL );
  }
}

/**********************************************************************
 * vca_set v n e
 *       set the n^th element of v to e ( or reset if e is NULL)
 ***********************************************************************/

void
vca_set( vca_t v, unsigned long n, entry_t e)
{
  unsigned long up = ENTRY_UP( n );
  unsigned long down = ENTRY_DOWN( n );
  
  if ( v->length == 0) {

    /* create a new entry table ee with an entry for e*/
    entrytable_t ee = ENTRYTAB_MAKE;
    ENTRYTAB_REF( ee, down ) = e;
    ENTRYTAB_COUNT( ee ) = 1;
    
    /* wrap ee into a larger tree by following the bits of up */
    for ( v->length = 1, v->tree=(vcatree_t) ee;				
	  up;								
	  up >>= 1 , v->length <<= 1) {
        v->tree=VCATREE_EXTEND( up, v->tree);
    }
  } else {
    unsigned long h;
    vcatree_t *treeptr;
    
    /* extend length of v to cover up */
    vca_extend( v, up);
  
    /* follow path given by up (extending v's tree if necessary) */
    for (treeptr = &(v->tree), h = v->length >> 1;
	 h;
         h >>= 1) {
        if (!*treeptr)
            *treeptr = VCATREE_MAKE_EMPTY;
        if ( VCATREE_L_P( up, h) ) {
            treeptr = &((*treeptr)->left);
        } else {
            treeptr = &((*treeptr)->right);
        }
    }
    
    /* Build a new entrytable if necessary */
    if (!*treeptr) 
      *treeptr = (vcatree_t) ENTRYTAB_MAKE;

    /* set the entry */
    ENTRYTAB_SET( *treeptr, down, e);

  }
  if (e)
      VCA_FLAG_SET(v,n);
}

/**********************************************************************
 * vcatree_reset tree n h
 ***********************************************************************/

static vcatree_t
vcatree_reset( vcatree_t tree, unsigned long n, unsigned long h, unsigned long down)
{
  if ( !tree)    return VCATREE_NULL;
  
  if ( h == 0 ) {
    entrytable_t tab = (entrytable_t) tree;
    if ( tab->table[down] ) {
      tab->table[down] = (entry_t) 0 ;
      if (--tab->count == 0) tree = VCATREE_NULL;
    }
      return tree;
  }

  if VCATREE_L_P( n , h)
    VCATREE_L( tree ) = vcatree_reset( VCATREE_L( tree ), n , h >> 1, down);
  else
    VCATREE_R( tree ) = vcatree_reset( VCATREE_R( tree ), n , h >> 1, down);
    
  return (VCATREE_L( tree ) || VCATREE_R( tree )) ? tree : VCATREE_NULL;

}

/**********************************************************************
 * vca_reset v n
 *       reset the n^th element of v to NULL (and prune v if necessary)
 ***********************************************************************/

void
vca_reset( vca_t v, unsigned long n )
{
  unsigned long h = v->length ;
  unsigned long up = ENTRY_UP( n );

  if ( up < h )  {
    v->tree = vcatree_reset( v->tree, up , h >> 1, ENTRY_DOWN( n ));
    
    /* remove topmost nodes with empty right branches to reduce v's length */
    for ( ;								
	  (v->length > 1) && v->tree && !VCATREE_R( v->tree );	
	  v->tree = VCATREE_L( v->tree), v->length >>= 1 );		
    if (!v->tree) v->length = 0;

  }
}

/**********************************************************************
 * vcatree_merge t1 t2
 *       merge t1 and t2 (with physical modifications of t1)
 *       return the merged VCATREE
 ***********************************************************************/

static vcatree_t
vcatree_merge( vcatree_t t1, vcatree_t t2, unsigned long h)
{
  if ( !t1 ) return t2;
  else if ( !t2 ) return t1;
  else if ( h == 0 ) {
    unsigned long i = 0;
    for (  ; i < ENTRY_MAX ; ++i)
      if ( !ENTRYTAB_REF( t1, i) && ENTRYTAB_REF( t2, i) ) {
	ENTRYTAB_REF( t1, i) = ENTRYTAB_REF( t2, i);
	++ENTRYTAB_COUNT( t1 );
      }
  } else {
    h >>= 1;
  
    VCATREE_L( t1 ) = vcatree_merge( VCATREE_L( t1 ), VCATREE_L( t2 ), h);
    VCATREE_R( t1 ) = vcatree_merge( VCATREE_R( t1 ), VCATREE_R( t2 ), h);
  }
  
  return t1;

}

/**********************************************************************
 * vca_merge v1 v2
 *       merge v1 and v2 (with physical modifications of v1)
 *       return the merged VCA
 ***********************************************************************/

vca_t
vca_merge( vca_t v1, vca_t v2)
{
  
  if ( !v1 || v1->length == 0 ) return v2;
  else if ( !v2 || v2->length == 0 ) return v1;
  else {
    
    unsigned long h1 = v1->length;
    unsigned long h2 = v2->length;
    vcatree_t *treeptr;
    
    h2 >>= 1;  vca_extend( v1, h2);

    /* we follow as deep as possible v1's leftmost branch
     * to find v2's root homolog node
     * (and extend this branch by new nodes if necessary)
     */
    for ( h1 = v1->length, h2 = v2->length, treeptr = &(v1->tree) ;
	  h1 > h2 ;
	  h1 >>= 1 , treeptr = &VCATREE_L( *treeptr ) )
      if (! *treeptr) *treeptr = VCATREE_MAKE_EMPTY;
  
    /* At this point, h1 == h2 */
    *treeptr = vcatree_merge( *treeptr, v2->tree, h1 >> 1);

    v1->flag |= v2->flag;
        
    return v1;
  }
}

