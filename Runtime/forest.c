/************************************************************
 * $Id$
 * Copyright (C) 1997, 2003, 2004, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2014 Eric de la Clergerie
 * ------------------------------------------------------------
 *
 *   Forest --
 *
 * ------------------------------------------------------------
 * Description
 *
 * ------------------------------------------------------------
 */

#include <string.h>
#include "libdyalog.h"
#include "builtins.h"

int dyalog_compact_forest = 1;

static int dyalog_trueforest=0;

static obj_t seen;
static obj_t *last;

static obj_t indirect;

#define B_TAG_SHIFT 4
#define B_TAG_MASK  ((1 << B_TAG_SHIFT) - 1)

#define B_TAG_REDUCED (B_TAG_SHIFT+1)
#define B_TAG_REDUCED_MASK (1 << B_TAG_REDUCED)

#define B_TAG_BEING_REDUCED (B_TAG_SHIFT+2)
#define B_TAG_BEING_REDUCED_MASK (1 << B_TAG_BEING_REDUCED)

#define B_TAG_BEING_PROCESSED (B_TAG_SHIFT+3)
#define B_TAG_BEING_PROCESSED_MASK (1 << B_TAG_BEING_PROCESSED)

#define B_TAG_LOOP (B_TAG_SHIFT+4)
#define B_TAG_LOOP_MASK (1 << B_TAG_LOOP)

#define B_TYPE( b )  (backptr_t) (PAIRP( b ) ? UCINT( CAR( b )) : UCINT( b ))
#define B_SAFE_TYPE( b )  (backptr_t) ((PAIRP( b ) ? UCINT( CAR( b )) : UCINT( b )) & B_TAG_MASK)
#define B_IS_REDUCED( b ) (PAIRP( b ) ? CINT( CAR( b )) : CINT( b )) & B_TAG_REDUCED_MASK
#define B_IS_BEING_REDUCED( b ) (PAIRP( b ) ? CINT( CAR( b )) : CINT( b )) & B_TAG_BEING_REDUCED_MASK

#define B_IS_BEING_PROCESSED( b ) (PAIRP( b ) ? CINT( CAR( b )) : CINT( b )) & B_TAG_BEING_PROCESSED_MASK

#define B_IS_LOOP( b ) (PAIRP( b ) ? CINT( CAR( b )) : CINT( b )) & B_TAG_LOOP_MASK

#define B_CAR( b )   (CAR( CDR( b )))
#define B_CDR( b )   (CDR( CDR( b )))

static obj_t
btr_set_reduced( obj_t b ) 
{
    if (PAIRP(b)) {
        unsigned long v = UCINT( CAR(b));
        v |= B_TAG_REDUCED_MASK;
        SET_CAR(b,BINT(v));
        return b;
    } else {
        unsigned long v = UCINT(b);
        v |= B_TAG_REDUCED_MASK;
        return (obj_t) BINT(v);
    }
}

static obj_t
btr_set_being_reduced( obj_t b ) 
{
    if (PAIRP(b)) {
        unsigned long v = UCINT( CAR(b));
        v |= B_TAG_BEING_REDUCED_MASK;
        SET_CAR(b,BINT(v));
        return b;
    } else {
        unsigned long v = UCINT(b);
        v |= B_TAG_BEING_REDUCED_MASK;
        return (obj_t) BINT(v);
    }
}


static obj_t
btr_reset_being_reduced( obj_t b ) 
{
    if (PAIRP(b)) {
        unsigned long v = UCINT( CAR(b));
        v &= ~B_TAG_BEING_REDUCED_MASK;
        SET_CAR(b,BINT(v));
        return b;
    } else {
        unsigned long v = UCINT(b);
        v &= ~B_TAG_BEING_REDUCED_MASK;
        return (obj_t) BINT(v);
    }
}

static obj_t
btr_set_being_processed( obj_t b ) 
{
    if (PAIRP(b)) {
        unsigned long v = UCINT( CAR(b));
        v |= B_TAG_BEING_PROCESSED_MASK;
        SET_CAR(b,BINT(v));
        return b;
    } else {
        unsigned long v = UCINT(b);
        v |= B_TAG_BEING_PROCESSED_MASK;
        return (obj_t) BINT(v);
    }
}

static obj_t
btr_reset_being_processed( obj_t b ) 
{
    if (PAIRP(b)) {
        unsigned long v = UCINT( CAR(b));
        v &= ~B_TAG_BEING_PROCESSED_MASK;
        SET_CAR(b,BINT(v));
        return b;
    } else {
        unsigned long v = UCINT(b);
        v &= ~B_TAG_BEING_PROCESSED_MASK;
        return (obj_t) BINT(v);
    }
}

static obj_t
btr_set_loop( obj_t b ) 
{
    dyalog_printf("set loop %x\n",b);
    if (PAIRP(b)) {
        unsigned long v = UCINT( CAR(b));
        v |= B_TAG_LOOP_MASK;
        SET_CAR(b,BINT(v));
        return b;
    } else {
        unsigned long v = UCINT(b);
        v |= B_TAG_LOOP_MASK;
        return (obj_t) BINT(v);
    }
}


/**********************************************************************
 *
 **********************************************************************/

static unsigned long bindex = 0;

static long
get_index( tabobj_t o) 
{
    if (o->bindex == -1) {

        *last = MAKE_PAIR( (obj_t) o, BNIL );
        last = &(CDR(*last));
        
        return (o->bindex = bindex++);
    } else 
        return o->bindex;
}

static tabobj_t
starter( tabobj_t o ) 
{
    if (!o)
        return (tabobj_t)0;
    else{
        obj_t backptr = o->backptr;
        if (B_SAFE_TYPE( backptr ) == f_trace) {
            backptr = B_CDR(backptr);
        }
        switch (B_SAFE_TYPE( backptr )) {
            case f_init:
            case f_call:
                return (tabobj_t) 0;
                break;
            default:
                return o;
                break;
        }
    }
}

/* EVDLC -- October 5 2006
   The 2 following functions try to remove duplicate derivations in forest.
   It remains to be checked if the process can loop in case of cyclic forests.
   Also the process could possibly be made more efficient by flagging reduced
   derivations
*/

static obj_t forest_remove_duplicates( obj_t b);

static Bool
forest_check_duplicate( obj_t b1, obj_t b2 )
{
        // assume b1 and b2 have been already reduced
//    dyalog_printf("check duplicates b1=%x b2=%x\n",b1,b2);
    if (b1 == b2) {
//        dyalog_printf("\teq\n");
        return 1;
    } else if (B_SAFE_TYPE( b2 ) == f_or) {
//        dyalog_printf("\tor2\n");
        return forest_check_duplicate(b1,B_CAR(b2))
            && forest_check_duplicate(b1,B_CDR(b2));
    } else if (B_SAFE_TYPE(b1) == f_or) {
//        dyalog_printf("\tor1\n");
        return forest_check_duplicate(B_CAR(b1),b2)
            && forest_check_duplicate(B_CDR(b1),b2);
    } else if (B_SAFE_TYPE( b2 ) != B_SAFE_TYPE(b1)) {
//        dyalog_printf("\ttype diff\n");
        return 0;
    }  else if (B_IS_LOOP(b1) || B_IS_LOOP(b2)) {
        return 0;
    }  else if (B_IS_BEING_REDUCED(b1) || B_IS_BEING_REDUCED(b2)) {
        return 0;
    } else {
//        dyalog_printf("\tsame type %d\n",B_SAFE_TYPE(b1));
        switch (B_SAFE_TYPE( b1 )) {
            case f_init:
            case f_call:
                return 1;
            case f_join:
                return ( starter( (tabobj_t) B_CDR(b1) ) == starter( (tabobj_t) B_CDR(b2) )
                         && starter( (tabobj_t) B_CAR(b1) ) == starter( (tabobj_t) B_CAR(b2) ));
            case f_rand:
            case f_and:
                if ( starter( (tabobj_t) B_CDR(b1) ) != starter( (tabobj_t) B_CDR(b2) ) ) {
                    return 0;
                } else {
                    Bool res=0;
//                    dyalog_printf("\t\tsame type car b1 %x\n",((tabobj_t)B_CAR(b1))->backptr);
                    ((tabobj_t)B_CAR(b1))->backptr = forest_remove_duplicates(((tabobj_t)B_CAR(b1))->backptr);
//                    dyalog_printf("\t\tsame type car b2 %x\n",((tabobj_t)B_CAR(b2))->backptr);
                    ((tabobj_t)B_CAR(b2))->backptr = forest_remove_duplicates(((tabobj_t)B_CAR(b2))->backptr);
//                    dyalog_printf("\t\tsame type check\n");
                    btr_set_being_reduced(b1);
                    res=forest_check_duplicate( ((tabobj_t)B_CAR(b1))->backptr,
                                                   ((tabobj_t)B_CAR(b2))->backptr );
                    btr_reset_being_reduced(b1);
                    return res;
                }
            case f_indirect:
                return forest_check_duplicate(B_CAR(b1),B_CAR(b2));
            case f_trace:
                return B_CAR(b1) == B_CAR(b2) && forest_check_duplicate(B_CDR(b1),B_CDR(b2));
            default:
                return 0;
        }
    }
}

static obj_t
forest_remove_duplicates( obj_t b ) 
{
//    dyalog_printf( "duplicates [%d] %x\n", B_SAFE_TYPE(b),b); /* trace */
    if (dyalog_trueforest) {
        return b;
    } else if (B_IS_BEING_PROCESSED(b)) {
        btr_set_loop(b);
        btr_set_reduced(b);
        return b;
    } else if (B_IS_REDUCED(b)) {
        return b;
    } else if (B_IS_LOOP(b)) {
        btr_set_reduced(b);
        return b;
    } else if (0 && B_IS_BEING_REDUCED(b)) {
            /* there is a loop */
            // dyalog_printf("loop [%x]\n",b);
        btr_set_reduced(b);
        return b;
            /* if a loop is found: we cut it with an empty forest
               this is not absolutely correct
               return BINT((unsigned long) f_init);
            */
    } else if (B_SAFE_TYPE( b ) == f_or) {
            /* b is a disjunction with car simple (not disj)
               remove duplicates from cdr
               and then check if car occur in reduced cdr b2
            */
        obj_t b2;
        obj_t b3;
        obj_t bres;
//        btr_set_being_reduced(b);
        btr_set_being_processed(b);
        b2 = btr_set_reduced(forest_remove_duplicates( B_CDR(b) ));
        b3 = btr_set_reduced(forest_remove_duplicates( B_CAR(b) ));
        btr_reset_being_processed(b);
            /*
        SET_CDR( CDR(b), b3);
        SET_CDR( CDR(b), b2);
        if (forest_check_duplicate(b3,b2)) {
            return b2;
        } else if (b2 == B_CDR(b)) {
            return b;
        } else {
            return MAKE_PAIR( BINT( (unsigned long) f_or ),
                              MAKE_PAIR( b3, b2 ));
        }
            */
        SET_CAR( CDR(b), b3);
        SET_CDR( CDR(b), b2);
//        dyalog_printf("try check duplicates b=%x b2=%x b3=%x\n",b,b2,b3);
        bres=forest_check_duplicate(b3,b2) ? b2 : b;
//        dyalog_printf("done check duplicates b=%x b2=%x b3=%x => bres=%x\n",b,b2,b3,bres);
        return bres;
    } else if (B_SAFE_TYPE(b) == f_trace) {
        obj_t b2;
            //      b=btr_set_reduced(b);
//        btr_set_being_reduced(b);
        btr_set_being_processed(b);
        b2 = btr_set_reduced(forest_remove_duplicates( B_CDR(b)));
        btr_reset_being_processed(b);
        if (b2 == B_CDR(b))
            return b;
        else {
            SET_CDR(CDR(b),b2);
            return b;
        }
    } else {
        return b;
    }
}

#define INDIRECT_FOUND 1
#define INDIRECT_VOID 2
#define INDIRECT_CYCLE 4
#define INDIRECT_TEST_CYCLE 8

static Bool forest_void( obj_t, Bool );

static void indirect_check_cycle( obj_t );


static long
find_or_add_indirect( obj_t o, long *info, long init_flag, obj_t *entry)
{
    long i=0;
    obj_t *l =&indirect;
    
    for( ; PAIRP( *l ) && CAR(CAR((*l))) != o ; i++, l = &CDR((*l)) );

//        dyalog_printf("Registering indirect %x %d\n",o,i); /* trace */
        
    if (NULLP( *l )){
        obj_t car = B_CAR(o);
        *l = MAKE_PAIR(MAKE_PAIR(o,(obj_t)(init_flag |INDIRECT_TEST_CYCLE)), BNIL );
        * info =  forest_void( car, 1) ? INDIRECT_VOID : 0;
        if (B_IS_LOOP(car)) {
            *info |= INDIRECT_CYCLE;
        } else {
            indirect_check_cycle( car );
            if ((long)CDR(CAR(*l)) & INDIRECT_CYCLE) {
                *info |= INDIRECT_CYCLE;
            }
        }
        CDR(CAR(*l)) = (obj_t) (init_flag | (*info));
    } else {
        if (((long) CDR(CAR(*l))) & INDIRECT_TEST_CYCLE) {
                // we have found a cycling indirect backptr
            CDR(CAR(*l)) = (obj_t) ((long)  CDR(CAR(*l)) | INDIRECT_CYCLE);
        }
        * info = (long) CDR(CAR(*l));
    }
    * entry = * l;
    return i;
}

static void
indirect_check_cycle( obj_t backptr ) 
{
    if (!backptr) {
//        return 1;
    } else if (B_IS_LOOP(backptr)) {
//        return 1;
    } else if (B_IS_BEING_PROCESSED(backptr)) {
        btr_set_loop(backptr);
    } else {
//        dyalog_printf( "cycle? [%d] %x\n", B_SAFE_TYPE( backptr ),backptr); /* trace */
        switch (B_SAFE_TYPE( backptr )) {
            case f_or:
                btr_set_being_processed(backptr);
                indirect_check_cycle(B_CAR(backptr));
                indirect_check_cycle(B_CDR(backptr));
                btr_reset_being_processed(backptr);
                break;
            case f_join:
            case f_rand:
                break;
            case f_and:
                btr_set_being_processed(backptr);
                indirect_check_cycle(((tabobj_t)B_CAR(backptr))->backptr);
                btr_reset_being_processed(backptr);
                break;
            case f_trace:
                indirect_check_cycle( B_CDR(backptr));
                break;
            case f_indirect:
            {
                long info=0;
                obj_t entry=0;
                find_or_add_indirect(backptr,&info,0,&entry);
                break;
            }
            default:
                    //              return 0;
                break;
        }
    }
}


static Bool
forest_void( obj_t backptr, Bool indirect )
{
    if (!backptr)
        return 1;
//    dyalog_printf( "void? [%d] %x\n", B_SAFE_TYPE( backptr ), backptr); /* trace */
    if (B_IS_BEING_PROCESSED(backptr)){
//        dyalog_printf("set loop on %x\n",backptr);
        btr_set_loop(backptr);
        return 1;
    }
    switch (B_SAFE_TYPE( backptr )) {
        case f_or:
//            dyalog_printf("\tf_or\n");
            btr_set_being_processed(backptr);
            if (!forest_void( B_CAR( backptr), indirect)) {
                btr_reset_being_processed(backptr);
                return 0;
            }
            if (!forest_void( B_CDR( backptr), indirect)) {
                btr_reset_being_processed(backptr);
                return 0;
            }
            btr_reset_being_processed(backptr);
            return 1;
            break;
        case f_join:
        case f_rand:
            return 0;
            break;
        case f_and:
//            dyalog_printf("FAND %x\n",B_CDR(backptr)); /* trace */
//            dyalog_printf("\tf_and\n");
            if (!indirect)
                return 0;
//            dyalog_printf("\t\tf_and_car %&o\n",((tabobj_t)B_CAR(backptr)));
//            dyalog_printf("\t\tf_and_car\n");
            btr_set_being_processed(backptr);
            if (!forest_void(((tabobj_t)B_CAR(backptr))->backptr,indirect)){
                btr_reset_being_processed(backptr);
                return 0;
            }
//            dyalog_printf("\t\tf_and_cdr\n");
            if ((!B_CDR(backptr) || forest_void(((tabobj_t)B_CDR(backptr))->backptr,0))) {
                btr_reset_being_processed(backptr);
                return 1;
            }
            btr_reset_being_processed(backptr);
            return 0;
            break;
        case f_trace:
//            dyalog_printf("\tf_trace\n");
            return forest_void( B_CDR(backptr),indirect);
            break;
        case f_indirect:
        {
            long info=0;
            obj_t entry=0;
//            dyalog_printf("\tf_indirect\n");
            find_or_add_indirect(backptr,&info,0,&entry);
            return info & INDIRECT_VOID;
            break;
        }
        default:
            return 1;
            break;
    }
}

Bool
forest_indirect( obj_t backptr )
{
//    dyalog_printf( "ind? [%d]\n", B_SAFE_TYPE( backptr )); /* trace */
    
    switch (B_SAFE_TYPE( backptr )) {
        case f_or:
            return forest_indirect( B_CAR( backptr ) ) && forest_indirect( B_CDR( backptr));
            break;
        case f_indirect:
            return 1;
            break;
        case f_trace:
            return forest_indirect( B_CDR( backptr ));
            break;
        default:
            return 0;
            break;
    }
}

static Bool
forest_display_body( obj_t backptr, Bool balance )
{
    Bool forest=0;
    fol_t trace = (fol_t) 0;
    obj_t b;

    backptr = forest_remove_duplicates(backptr);
        
//    dyalog_printf( "body [%d]\n", B_SAFE_TYPE(backptr) ); /* trace */
    switch (B_SAFE_TYPE( backptr )) {
        case f_call:
        case f_init:
            
            return 0;
            break;

        case f_or:
            if (balance) {
//                if (!balance) dyalog_printf( "( " );
                if (!forest_display_body( B_CAR( backptr ), 1)) dyalog_printf( " e " );
                dyalog_printf( " | " );
                if (!forest_display_body( B_CDR( backptr ), 1)) dyalog_printf( " e " );
//                if (!balance) dyalog_printf( " )" );
            } else {
                long info=0;
                obj_t entry=0;
                long i=find_or_add_indirect((obj_t)backptr,&info,INDIRECT_FOUND,&entry);
                if (info & INDIRECT_FOUND) {
                    dyalog_printf("#%d",i);
                } else if (dyalog_compact_forest) {
                    CDR(CAR(entry)) = (obj_t) (((long) CDR(CAR(entry))) | INDIRECT_FOUND);
                    dyalog_printf("#%d",i);
                    dyalog_printf( "{ " );
                    if (!forest_display_body( B_CAR( backptr ), 1)) dyalog_printf( " e " );
                    dyalog_printf( " | " );
                    if (!forest_display_body( B_CDR( backptr ), 1)) dyalog_printf( " e " );
                    dyalog_printf( " }" );
                } else {
                    dyalog_printf( "( " );
                    if (!forest_display_body( B_CAR( backptr ), 1)) dyalog_printf( " e " );
                    dyalog_printf( " | " );
                    if (!forest_display_body( B_CDR( backptr ), 1)) dyalog_printf( " e " );
                    dyalog_printf( " )" );
                }
            }
            return 1;
            break;
    
        case f_join:
            if (! forest_void( ((tabobj_t)B_CAR(backptr))->backptr,0 )){
                dyalog_printf( "%d", get_index( (tabobj_t) B_CAR(backptr) ));
                forest=1;
            } 
            if (! forest_void( ((tabobj_t)B_CDR(backptr))->backptr,0 )){
                if (forest) {dyalog_printf(" ");}
                dyalog_printf( "%d", get_index( (tabobj_t) B_CDR(backptr) ));
                forest |= 1;
            } 
            return forest;
            break;

        case f_and:
//            dyalog_printf("CAT %d\n",(int)B_CAR(backptr)); /* trace */
            b=((tabobj_t)B_CAR(backptr))->backptr;
            if (B_SAFE_TYPE(b) == f_trace){
                trace = (fol_t) B_CAR(b);
                b=B_CDR(b);
            }
            if (forest_display_body(b,0)){
                forest = 1;
                dyalog_printf( " " );
            }
//            dyalog_printf("CDR %d\n",(int)B_CDR(backptr)); /* trace */
            if ((tabobj_t)B_CDR(backptr)){
                if (forest_indirect(((tabobj_t)B_CDR(backptr))->backptr)) {
                    tabobj_t o = (tabobj_t)B_CDR(backptr);
                    if (trace) {
                        dyalog_printf( "[%&f]", trace);
                        forest=1;
                    }
                    forest=forest_display_body( o->backptr,0);
                } else if (! forest_void( ((tabobj_t)B_CDR(backptr))->backptr,0 )){
                    if (trace) {
                        dyalog_printf( "[%&f]", trace);
                        forest=1;
                    }
                    dyalog_printf( "%d", get_index( (tabobj_t) B_CDR(backptr) ));
                    forest=1;
                } else if (trace) {
                        // This case should be an error
//                    dyalog_printf("HERE ");
                }                
            }
            return forest;
            break;

        case f_rand:
            if (! forest_void( ((tabobj_t)B_CAR(backptr))->backptr,0 )){
                dyalog_printf( "%d", get_index( (tabobj_t) B_CAR(backptr) ));
                forest=1;
            } else if (forest_indirect(((tabobj_t)B_CAR(backptr))->backptr)) {
                if (forest_display_body( ((tabobj_t)B_CAR(backptr))->backptr, 0))
                    forest=1;
            }
            if (forest)
                dyalog_printf( " " );

            if (forest_display_body( ((tabobj_t)B_CDR(backptr))->backptr, 0)){
                forest |= 1;
            }
            return forest;
            break;

        case f_indirect: 
        {
            long info=0;
            obj_t entry=0;
            long i=find_or_add_indirect((obj_t)backptr,&info,INDIRECT_FOUND,&entry);
//            dyalog_printf("{%d:%d}",i,info); /* trace */
            if (!(info & INDIRECT_VOID)){
                if (info & INDIRECT_CYCLE) {
                        /* non empty cycling indirect => need to use #x
                         * notation to print the forest */
                    dyalog_printf("#%d",i);
                    if (!(info & INDIRECT_FOUND)){
                        dyalog_printf( "{ " );
                        CDR(CAR(entry)) = (obj_t) (((long) CDR(CAR(entry))) | INDIRECT_FOUND);
                        forest=forest_display_body( B_CAR( backptr ), 1);
                        dyalog_printf( " }" );
                    } else {
                        dyalog_printf(" ");
                    }
                } else if (info & INDIRECT_FOUND) {
                    dyalog_printf("#%d",i);
                } else if (dyalog_compact_forest) {
                    dyalog_printf("#%d",i);
                    dyalog_printf( "{ " );
                    CDR(CAR(entry)) = (obj_t) (((long) CDR(CAR(entry))) | INDIRECT_FOUND);
                    forest=forest_display_body( B_CAR( backptr ), 1);
                    dyalog_printf( " }" );
                } else {
                        /* non empty non cycling indirect => can expand
                         * content and no need for #x (except for compactness) */
                    forest=forest_display_body( B_CAR( backptr ), 0);
                }
            }
        }
        return forest; 
        break;

        default:
            return 0;
            break;
    }
}

void
forest_display( obj_t solutions )
{
  dyalog_printf( "\n"
	  "----------------------------------------------------------------------\n"
	  "Shared Forest\n"
	  );

  seen = solutions; 
  indirect = BNIL;
  dyalog_trueforest = (getenv("DYALOG_TRUEFOREST") != NULL);
  bindex = 0;

  for ( last = &solutions;
        PAIRP(*last);
        last = &(CDR(*last))
        ) {
      ((tabobj_t) (CAR(*last)))->bindex = bindex++;
  }
  
  for( ; PAIRP( solutions ) ; solutions = CDR( solutions ) ){
    tabobj_t o = (tabobj_t) CAR(solutions);
    struct sfol s = { .t=o->seed->model, .k=Trail_Object( o )};
//    fkey_t k = Trail_Object( o );
    long  i = get_index(o);
//           dyalog_printf( "\n%&c\n\t%d <-- ", s.t, s.k, i ); /* trace */
    DyALog_Assign_Display_Info( &s );
    dyalog_printf( "\n%&c\n\t%d <-- ", s.t, s.k, i );
    forest_display_body( o->backptr, 0);
    Untrail_Object;
  }

  dyalog_printf( "\n" );	  

}


/**********************************************************************
 * Counting derivation trees
 **********************************************************************/

typedef struct count {
  tabobj_t key;
  double    count;
  struct count *next;
} *count_t;

static count_t solutions_count;
static long node_counter=0;

static double forest_count_head( tabobj_t );

static double
forest_count_body( obj_t backptr )
{
  
    if (forest_void( backptr,0 ))
        return 1;
    else
        switch (B_SAFE_TYPE( backptr )) {
            case f_or:
                return forest_count_body( B_CAR( backptr )) + forest_count_body( B_CDR( backptr ));
                break;
            case f_join:
                return forest_count_head( (tabobj_t) B_CAR( backptr )) *
                    forest_count_head( (tabobj_t) B_CDR( backptr ));
                break;
            case f_and:
                return forest_count_body( ((tabobj_t) B_CAR( backptr ))->backptr) *
                    forest_count_head( (tabobj_t) B_CDR( backptr ));
                break;
            default:
                return 1.0 ;
                break;
        }
}

static double
forest_count_head( tabobj_t o )
{
  count_t *c = &solutions_count;
  
  for(; *c && (*c)->key != o; c = &(*c)->next );
  
  if (!*c) {
    count_t entry = (count_t) GC_MALLOC( sizeof( struct count ) );
    *c = entry;
    entry->key  = o;
    entry->count = 0.0;
    entry->count = forest_count_body( o->backptr);
    ++node_counter;
  }
  
  return (*c)->count;
}

void
forest_count( obj_t solutions )
{
  double derivation_counter=0.0;
  dyalog_printf( "\n"
		 "----------------------------------------------------------------------\n"
		 "\n" );

  seen = solutions; 

  for( ; PAIRP( solutions ) ; solutions = CDR( solutions ) ){
    tabobj_t o = (tabobj_t) CAR(solutions);
    fkey_t k = Trail_Object( o );
    double nb = forest_count_head(o);
    derivation_counter = derivation_counter + nb;
    dyalog_printf( "%g derivations for %&c\n",nb, o->seed->model, k);
    Untrail_Object;
  }

  dyalog_printf( "Used %d nodes for %g derivations\n\n", node_counter, derivation_counter);
}

/* TMP FUNCTIONS, till merging with DyALOg proba */

void
Forest_Reduce_NBest(long n) 
{
}

tabobj_t
Forest_Partial(tabobj_t v, long n2, long n) 
{
    return v;
}





