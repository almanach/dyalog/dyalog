/* $Id$
 * Copyright (C) 1996 Eric de la Clergerie
 * ------------------------------------------------------------
 *
 *   Set -- Header file for set.c
 *
 * ------------------------------------------------------------
 * Description
 *
 * ------------------------------------------------------------
 */

extern Bool set_in(obj_t, obj_t);
extern obj_t  set_union(obj_t,obj_t);
