/* 
 ******************************************************************
 * $Id$
 * Copyright (C) 2000, 2003, 2004, 2008, 2009, 2010, 2011, 2017 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  oset.c -- ordered set
 *
 * ----------------------------------------------------------------
 * Description
 *   
 * ----------------------------------------------------------------
 */

#include "libdyalog.h"
#include "builtins.h"
#include "hash.h"

#include <search.h>
#include <string.h>

//#define calloc(n,s) GC_MALLOC_ATOMIC((n)*(s))
//#define free(t) GC_FREE(t)
//#define free(t)
//#define malloc(s) GC_MALLOC_ATOMIC(s)

#define OSET_LENGTH 29

typedef struct oset 
{
    unsigned long length;
    unsigned long v;
} * oset_t;


static void *oset_root=NULL;

static int
oset_compare(const oset_t t1, const oset_t t2)
{
    unsigned long l1 = ((oset_t)t1)->length;
    unsigned long l2 = ((oset_t)t2)->length;
    
    if ( l1 < l2) {
        return -1;
    } else if ( l1 > l2) {
        return 1;
    } else {
        unsigned long l= l1;
        unsigned long *p1=&( ((oset_t)t1)->v);
        unsigned long *p2=&( ((oset_t)t2)->v);
        for(; l; l--,p1++,p2++) {
            if (*p1 < *p2 ) return -1;
            if (*p1 > *p2 ) return 1;
        }
        return 0;
    }
}

typedef int compfun(const void *, const void *);

oset_t
oset_register( const oset_t t )
{
    if (!t) {
        return (oset_t) 0;
     } else {
        oset_t res= *(oset_t *)tsearch((void *)t,&oset_root,(compfun *) oset_compare);
        return res;
     }
}

// Check is n belongs to t
Bool
oset_member( unsigned long n, oset_t t) 
{
    if (!t) {
        Fail;
    } else {
        unsigned long base = (n / OSET_LENGTH);
        unsigned long q = (OSET_LENGTH-1)-(n % OSET_LENGTH);
        if (t->length>base) {
            unsigned long *p= &(t->v);
            return p[base] & (1 << q);
        } else
            Fail;
    }
}


// we assume that t may be discarded
oset_t
oset_insert( unsigned long n, oset_t t)
{
    unsigned long base = (n / OSET_LENGTH);
    unsigned long q = (OSET_LENGTH-1)-(n % OSET_LENGTH);
    if (!t) {
        oset_t new = (oset_t) calloc( (base+2), sizeof(long));
        unsigned long *p_new= &(new->v);
        new->length=base+1;
        p_new[base] = 1 << q;
        return new;
    } else if (t->length>base) {
        unsigned long *p= &(t->v);
        p[base] |= 1 << q;
        return t;
    } else {
        oset_t new = (oset_t) calloc( (base+2), sizeof(long));
        unsigned long l= t->length;
        unsigned long *p= &(t->v);
        unsigned long *p_new= &(new->v);
        new->length=base+1;
        p_new[base] = (unsigned long) 1 << q;
        for(; l; l--) {
            *p_new++ = *p++;
        }
        free(t);
        return new;
    }
}


oset_t
oset_void()
{
    return (oset_t) 0;
}

oset_t
number_oset( unsigned long n ) 
{
    unsigned long base = (n / OSET_LENGTH);
    unsigned long q = (OSET_LENGTH-1)-(n % OSET_LENGTH);
    unsigned long length = base+1;
    oset_t t = (oset_t) calloc( (length+1), sizeof(long));
    unsigned long *p= &(t->v);
    t->length=length;
    p[base] = 1 << q;
    return oset_register(t);
}

static oset_t
oset_copy(oset_t t)
{
    if (!t){
        return t;
    } else {
        size_t s = (1+t->length)*sizeof(unsigned long);
        oset_t copy = (oset_t) malloc( s );
        memcpy(copy,t,s);
        return copy;
    }
}


static oset_t
oset_light_union( oset_t t1, oset_t t2 )
{
        /* asymetry: t1 should be preserved, t2 may be modified or discarded */
    if (!t1) {
//        return oset_copy(t2);
        return t2;
    } else if (!t2) {
        return oset_copy(t1);
    } else {
        unsigned long n1 = t1->length;
        unsigned long n2 = t2->length;
        unsigned long min = (n1 < n2) ? n1 : n2;
        unsigned long max = (n1 < n2) ? n2 : n1;
        unsigned long *p_min = (n1 < n2) ? &(t1->v) : &(t2->v);
        unsigned long *p_max = (n1 < n2) ? &(t2->v) : &(t1->v);
        oset_t t = (oset_t) calloc( (max+1), sizeof(unsigned long) );
        unsigned long *p = &(t->v);
        t->length=max;
        for(; min; min--, max-- ) {
//            dyalog_printf("Union %x + %x = %x\n",*p_min,*p_max,*p_min | *p_max);
            *p++ = (*p_min++ | *p_max++);
        }
        for(; max; max--) {
            *p++ = *p_max++;
        }
        free(t2);
        return t;
    }
}

oset_t
oset_union( oset_t t1, oset_t t2 )
{
//    printf("oset union t1=%X t2=%X\n",t1,t2);
    if (!t1) {
        return t2;
    } else if (!t2) {
        return t1;
    } else {
        unsigned long n1 = t1->length;
        unsigned long n2 = t2->length;
        unsigned long min = (n1 < n2) ? n1 : n2;
        unsigned long max = (n1 < n2) ? n2 : n1;
        unsigned long *p_min = (n1 < n2) ? &(t1->v) : &(t2->v);
        unsigned long *p_max = (n1 < n2) ? &(t2->v) : &(t1->v);
        oset_t t = (oset_t) calloc( (max+1), sizeof(unsigned long) );
        unsigned long *p = &(t->v);
        t->length=max;
        for(; min; min--, max-- ) {
            *p++ = (*p_min++ | *p_max++);
        }
        for(; max; max--) {
            *p++ = *p_max++;
        }
        return oset_register(t);
    }
}
        
oset_t
oset_inter( oset_t t1, oset_t t2 )
{
//    printf("oset inter t1=%X t2=%X\n",t1,t2);
    if (!t1 || !t2) {
        return (oset_t) 0;
    } else {
        unsigned long n1 = t1->length;
        unsigned long n2 = t2->length;
        unsigned long min = (n1 < n2) ? n1 : n2;
        unsigned long *p1 = &(t1->v);
        unsigned long *p2 = &(t2->v);
        oset_t t = (oset_t) calloc( (min+1), sizeof(unsigned long) );
        unsigned long *p = &(t->v);
        unsigned long flag = 0;
        t->length=min;
        for(; min; min-- ) {
            flag |= (*p++ = *p1++ & *p2++) ;
        }
        if (flag) {
            return oset_register(t);
        } else {
            free(t);
            return (oset_t) 0;
        }
    }
}

// return t1-t2
oset_t
oset_delete( oset_t t1, oset_t t2) 
{
//    printf("oset delete t1=%X t2=%X\n",t1,t2);
    if (!t1 || !t2) {
        return t1;
    } else {
        unsigned long n1 = t1->length;
        unsigned long n2 = t2->length;
        unsigned long min = (n1 < n2) ? n1 : n2;
        unsigned long *p1 = &(t1->v);
        unsigned long *p2 = &(t2->v);
        oset_t t = (oset_t) calloc( (n1+1), sizeof(unsigned long) );
        unsigned long *p = &(t->v);
        unsigned long flag = 0;
        t->length=n1;
        for(; min; min--, n1-- ) {
            flag |= (*p++ = *p1++ & ~*p2++) ;
        }
        for(; n1 ; n1--) {
            flag |= (*p++ = *p1++);
        }
        if (flag) {
            return oset_register(t);
        } else {
            free(t);
            return (oset_t) 0;
        }
    }
}

// return t1-t2 with t1 not registered
// and not registering the result
// asymmetry: t1 may be modified or discarded; t2 must be preserved
static oset_t
oset_light_delete( oset_t t1, oset_t t2) 
{
    if (!t1 || !t2) {
        return t1;
    } else {
        unsigned long n1 = t1->length;
        unsigned long n2 = t2->length;
        unsigned long min = (n1 < n2) ? n1 : n2;
        unsigned long *p1 = &(t1->v);
        unsigned long *p2 = &(t2->v);
        oset_t t = (oset_t) calloc( (n1+1), sizeof(unsigned long) );
        unsigned long *p = &(t->v);
        unsigned long flag = 0;
        t->length=n1;
        for(; min; min--, n1-- ) {
//            dyalog_printf("Delete %x - %x = %x\n",*p1,*p2,*p1 & ~*p2);
            flag |= (*p++ = *p1++ & ~*p2++) ;
        }
        for(; n1 ; n1--) {
            flag |= (*p++ = *p1++);
        }
        free(t1);
        if (flag) {
            return t;
        } else {
            free(t);
            return (oset_t) 0;
        }
    }
}


fol_t
oset_list( oset_t t ) 
{
    if (!t) {
        return FOLNIL;
    } else {
        unsigned long n = t->length;
        unsigned long *p = &(t->v)+(n-1);
        fol_t f = FOLNIL;
        unsigned long v;
        unsigned long base = n*OSET_LENGTH-1;
        unsigned long local;
        for(; n; n--, base -= OSET_LENGTH ) {
            for(v=*p--, local=base; v; local--, v >>= 1 ) {
                if (v & 1)
                    f = folcmp_create_pair( DFOLINT(local), f );
            }
        }
        return f;
    }
}

fol_t
oset_list_alt( oset_t t ) 
{
    if (!t) {
        return FOLNIL;
    } else {
        unsigned long n = t->length;
        unsigned long *p = &(t->v)+(n-1);
        fol_t f = FOLNIL;
        unsigned long base = (n-1)*OSET_LENGTH;
        for(; n; n--, base -= OSET_LENGTH, p-- ) {
            if (*(unsigned long *)p) {
                f = folcmp_create_pair( UDFOLINT(base),
                                        folcmp_create_pair( UDFOLINT(*(unsigned long *)p), f));
            }
        }
        return f;
    }
}


fol_t
oset_numbervarlist( oset_t t ) 
{
    if (!t) {
        return FOLNIL;
    } else {
        unsigned long n = t->length;
        unsigned long *p = &(t->v)+(n-1);
        fol_t f = FOLNIL;
        unsigned long v;
        unsigned long base = n*OSET_LENGTH-1;
        unsigned long local;
        for(; n; n--, base -= OSET_LENGTH ) {
            for(v=*p--, local=base; v; local--, v >>= 1 ) {
                if (v & 1){
                    fol_t x;
                    FOLCMP_WRITE_START(FOLNUMBERVAR,1);
                    FOLCMP_WRITE( DFOLINT(local) );
                    x=FOLCMP_WRITE_STOP;
                    f = folcmp_create_pair( x , f );
                }
            }
        }
        return f;
    }
}

typedef struct {
  fol_t key;
  oset_t tupple;
} tupple_entry;

static char *tupple_table;

DSO_LOCAL void
c_oset_initialize()
{
    tupple_table=Hash_Alloc_Table( 1024, sizeof(tupple_entry) );
}

oset_t
numbervar_tupple( SP(A), oset_t t) 
{
//    int flag;
//    tupple_entry *entry;

    Deref(A);

    if (FOLVARP(A)) {
        dyalog_printf("error: unexpected variable\n");
        exit(1);
    }

    if (!FOLCMPP(A)) {
        return t;
    }

    if (FOLCMP_FUNCTOR(A) == FOLLOOP) {
        TRAIL_UBIND(FOLCMP_REF(A,1),Sk(A),FOLNIL,Key0);
        return numbervar_tupple(FOLCMP_REF(A,2),Sk(A),t);
    }
    
    if (FOLCMP_FUNCTOR(A) == FOLNUMBERVAR) {
        A = FOLCMP_REF(A,1);
        Deref(A);
        if (FOLINTP(A))
            return oset_insert( UCINT(A), t) ;
        else
            return numbervar_tupple(A,Sk(A),t);
    }

    if (FOLCMP_FUNCTOR(A) == FOLTUPPLE) {
        A = FOLCMP_REF(A,1);
        Deref(A);
        if (FOLINTP(A)) {
            V_LEVEL_DISPLAY(V_DYAM,"Try Tupple union orig=%x c=%x with t=%x\n",(unsigned long)A,UCINT(A),(unsigned long)t );
            return oset_light_union( (oset_t) LIGHT_DYALOG_TO_POINTER(A), t);
        } else
            return numbervar_tupple(A,Sk(A),t);
    }

    if (FOLCMP_FUNCTOR(A) == FOLSMB_CONVERT_ARITY(FOLTUPPLE,2)) {
            // '$TUPPLE'(T,A) : remove T from tupple(A)
        fol_t T = FOLCMP_REF(A,1);
        fkey_t Sk(T) = Sk(A);
        A = FOLCMP_REF(A,2);
        Deref(T);
        V_LEVEL_DISPLAY(V_DYAM,"Try Tupple remove\n" );
        if (FOLINTP(T)){
            oset_t tA = numbervar_tupple(A,Sk(A),(oset_t) 0);
            tA = oset_light_delete(tA,(oset_t)LIGHT_DYALOG_TO_POINTER(T));
            t = oset_light_union(tA,t);
                // tA is a tmp oset that may now be discarded
            if (tA)
                free(tA);
            return t;
        } else {
            t = numbervar_tupple(T,Sk(T),t);
            return numbervar_tupple(A,Sk(A),t);
        }
    }
        
        /*
   flag = FOLCMP_GROUNDP(A) && (FOLINFO_WEIGHT(A) > 100);
    
    if (flag && (entry = (tupple_entry *)Hash_Find(tupple_table,(long)A))){
        return oset_light_union( entry->tupple, t);
        } else
        */

    {
        fol_t *p=&FOLCMP_REF(A,1);
        fol_t *stop=p+FOLCMP_ARITY(A);
        for(; p < stop ; t = numbervar_tupple(*p++,Sk(A),t));
            /*
        if (flag){
            tupple_entry x = {key:A, tupple:t};
            Hash_Insert(tupple_table,(char *)&x,0);
        }
            */
        return t;
    }
}

oset_t
Numbervar_Tupple( sfol_t A )
{
    TrailWord *top= C_TRAIL_TOP;
    oset_t t = numbervar_tupple( A->t, A->k, (oset_t) 0);
//    if (C_TRAIL_TOP > top)
        untrail_alt(top);
    return t;
}

oset_t
Numbervar_Tupple_And_Register( sfol_t A )
{
    TrailWord *top= C_TRAIL_TOP;
    oset_t t = oset_register(numbervar_tupple( A->t, A->k, (oset_t) 0));
    if (C_TRAIL_TOP > top)
        untrail_alt(top);
    return t;
}

oset_t
Tupple_Extend( sfol_t A, oset_t t) 
{
    TrailWord *top= C_TRAIL_TOP;
    oset_t new = oset_copy(t);
    new = oset_register(numbervar_tupple( A->t, A->k, new));
    if (C_TRAIL_TOP > top)
        untrail_alt(top);
    return new;
}

oset_t
Tupple_Delete( sfol_t A, oset_t t) 
{
    TrailWord *top= C_TRAIL_TOP;
    oset_t new = oset_copy(t);
    oset_t tA = numbervar_tupple( A->t, A->k, (oset_t) 0);
    new = oset_register(oset_light_delete(new,tA));
    if (tA)
        free(tA);
    if (C_TRAIL_TOP > top)
        untrail_alt(top);
    return new;
}


fol_t                           /* Dyam instruction */
Dyam_Create_Alt_Tupple_Old(unsigned int base, unsigned int mask, fol_t l)
{
    V_LEVEL_DISPLAY( V_LOW,"Create Alt Tupple %d %d %x %&f\n",base,mask,mask,l);
    base += OSET_LENGTH-1 ;
    for(; mask; base--, mask >>= 1 ) {
        V_LEVEL_DISPLAY( V_LOW,"\tbase=%d mask=%x flag=%d l=%&f\n",base,mask,(mask & 1),l);
        if (mask & 1)
            l = folcmp_create_pair(FOLVAR_FROM_INDEX(base),l);
    }
    return l;
}

fol_t                           /* Dyam instruction */
Dyam_Create_Alt_Tupple(unsigned int base, unsigned int mask, fol_t l)
{
    int n=0;
    V_LEVEL_DISPLAY( V_LOW,"Create Alt Tupple %d %d %x %&f\n",base,mask,mask,l);
    base += OSET_LENGTH-1 ;
    FOLCMP_WRITE_LIGHT_START;
    for(; mask; base--, mask >>= 1 ) {
        V_LEVEL_DISPLAY( V_LOW,"\tbase=%d mask=%x flag=%d l=%&f\n",base,mask,(mask & 1),l);
        if (mask & 1) {
            FOLCMP_WRITE(FOLVAR_FROM_INDEX(base));
            n++;
        }
    }
    if (1
        && FOLCMPP(l)
        && (FOLSMB_CONVERT_ARITY(FOLCMP_FUNCTOR(l),1) == FOLCURLYFUN)
        && (n+FOLCMP_ARITY(l) < 8)
        ) {
        fol_t *arg=&FOLCMP_REF(l,1);
        fol_t *stop=arg+FOLCMP_ARITY(l);
        for(; arg<stop; arg++) {
            FOLCMP_WRITE(*arg);
            n++;
        }
    } else if (!FOLNILP(l)) {
        FOLCMP_WRITE(l);
        n++;
    }
    l = FOLCMP_WRITE_LIGHT_STOP( FOLSMB_CONVERT_ARITY(FOLCURLYFUN,n), n);
    return l;
}

static int tupple_cnt;


void  /* Dyam instruction */
Dyam_Write_Tupple(unsigned int base, unsigned long mask)
{
    unsigned long v = 1 << (OSET_LENGTH-1);
    for(; v ; base++, v >>= 1) {
        V_LEVEL_DISPLAY( V_LOW,"\tbase=%d mask=%x flag=%d\n",base,mask,(mask & 1));
        if (mask & v) {
//            dyalog_printf("\tvariable %d\n",base);
            FOLCMP_WRITE(FOLVAR_FROM_INDEX(base));
            tupple_cnt++;
        }
    }
}


void
Dyam_Write_Tupple_Alt(unsigned int base, unsigned long mask)
{
    base += OSET_LENGTH-1 ;
    for(; mask; base--, mask >>= 1 ) {
        V_LEVEL_DISPLAY( V_LOW,"\tbase=%d mask=%x flag=%d\n",base,mask,(mask & 1));
        if (mask & 1) {
            dyalog_printf("\tvariable %d\n",base);
            FOLCMP_WRITE(FOLVAR_FROM_INDEX(base));
            tupple_cnt++;
        }
    }
}


void  /* Dyam instruction */
Dyam_Start_Tupple(unsigned int base, unsigned long mask)
{
    V_LEVEL_DISPLAY( V_LOW,"Start Tupple\n");
//    dyalog_printf("Start Tupple mask=%x base=%d\n",mask,base);
    tupple_cnt=0;
    FOLCMP_WRITE_LIGHT_START;
    Dyam_Write_Tupple(base,mask);
}

fol_t /* Dyam instruction */
Dyam_End_Tupple() 
{
    int n=tupple_cnt;
    V_LEVEL_DISPLAY( V_LOW,"Create Tupple\n");
    tupple_cnt=0;
    return FOLCMP_WRITE_LIGHT_STOP( FOLSMB_CONVERT_ARITY(FOLCURLYFUN,n), n);
}

fol_t /* Dyam instruction */
Dyam_Almost_End_Tupple(unsigned int base, unsigned long mask) 
{
    Dyam_Write_Tupple(base,mask);
    return Dyam_End_Tupple();
}

fol_t                           /* Dyam instruction */
Dyam_Create_Simple_Tupple(unsigned int base, unsigned long mask)
{
    Dyam_Start_Tupple(base,mask);
    return Dyam_End_Tupple();
}

void  /* Dyam instruction */
Dyam_Write_Fun_With_Tupple(unsigned int base, unsigned long mask)
{
    Dyam_Write_Tupple(base,mask);
}

void  /* Dyam instruction */
Dyam_Start_Fun_With_Tupple(unsigned int base, unsigned long mask)
{
    V_LEVEL_DISPLAY( V_LOW,"Start Fun Tupple\n");
    tupple_cnt=0;
    FOLCMP_WRITE_LIGHT_START;
    Dyam_Write_Fun_With_Tupple(base,mask);
}

fol_t                           /* Dyam instruction */
Dyam_Create_Fun_With_Tupple(const fol_t functor, unsigned int base, unsigned long mask)
{
    Dyam_Start_Fun_With_Tupple(base,mask);
    return FOLCMP_WRITE_LIGHT_STOP( FOLSMB_CONVERT_ARITY(functor,tupple_cnt), tupple_cnt);
}

fol_t /* Dyam instruction */
Dyam_End_Fun_With_Tupple(fol_t functor) 
{
    int n=tupple_cnt;
    fol_t l;
    V_LEVEL_DISPLAY( V_LOW,"Create Fun Tupple %&f %d\n",functor,n);
//    dyalog_printf( "Create Fun Tupple %&f %d\n",functor,n);
    tupple_cnt=0;
    l = FOLCMP_WRITE_LIGHT_STOP( FOLSMB_CONVERT_ARITY(functor,n), n);
        //    dyalog_printf( "=> %&f\n",l);
    return l;
}





