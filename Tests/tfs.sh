#!/bin/sh
OPTIONS=$1  # when ppc is used has to pass option -ffixed-r15 and so on

tfs2lib -dev -libtool 'glibtool' list.def
gcc  -dynamiclib  list.c -current_version 1.0 -compatibility_version 1.0  -o liblist.dylib -I ../Runtime -I ../Tfs/ -I ../Compiler -single_module -L../Runtime/.libs/ -ldyalog $OPTIONS
#-ffixed-r15 -ffixed-r16 -ffixed-r17

../Compiler/dyacc -v -dev -tfs list -libtool '/usr/local/bin/glibtool' tfs_append.pl -o tfs_append
