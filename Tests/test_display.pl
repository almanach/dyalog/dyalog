/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2000 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  test_display.pl -- Test Optimizations on display
 *
 * ----------------------------------------------------------------
 * Description
 *  test
 *       - anonymous variables (single occurrence variable)
 *       - maximal features (bound to a single occurrence variable)
 * ----------------------------------------------------------------
 */

:-require 'format.pl'.

:-include 'test_display.plh'.

:-extensional bar/1.

bar( foo{ a=> p(X,_), b=>X, c=>Y } ).


:-rec_prolog test/2.

test(a,X) :-
	bar(X)
	.

test(b,X) :-
	bar(X),
	format('--> X=~w\n',[X]),
	fail
	.

test(c,X) :-
	bar(X),
	optimized_format( [X]^'--> X=~w\n' ),
	fail
	.

test(d,X) :-
	bar(X),
	optimized_format( [X,X]^'--> X=~w\n--> X=~w\n' ),
	fail
	.

test(e,X) :-
	bar(X),
	optimized_format(
			 (   
			     [X]^'--> X=~w\n',
			     [X]^'--> X=~w\n'
			 )
			),
	fail
	.

test(f,X) :-
	bar(X),
	optimized_format(
			 (   
			     [[X],[X],[p(a)]]^'--> X=~w'^',\n',
			     []^'\n'
			 )
			),
	fail
	.

test(g,X) :-
	bar(X),
	format( '~l\n', [['--> X=~w',',\n'],[[X],[X],[p(a)]]] ),
	fail
	.


?- ( argv(Args),
       ( Args = [] -> Tests = [a,b,c,d,e,f,g] ; Tests = Args ),
       domain(Test,Tests),
       format( 'Test ~w\n',[Test]),
       test(Test,X)
   )
   .


?- argv([NN]),atom_number(NN,N),make_list(L,N,[a]),
   optimized_format( ( []^'Test ',
			 L^'~w'^':',
			 []^'\n' ) ),
   fail
   .
