:-rec_prolog append/3.
:-require 'format.pl'.

append(e_list{}, Y, Y).

append( A::tl=>X,Y::list{}, B::tl=>Z) :-
	A .> hd .= B.> hd,
        append(X,Y,Z).

test :- 
    X=tl=>tl=>e_list{},
    Y=tl=>_,
    append(X,Y,Z),
    format('append (long)\n\tX=~w\n\tY=~w\n\tZ=~w\n',[X,Y,Z]),
    optimized_format([X,Y,Z]^'append (short)\n\tX=~w\n\tY=~w\n\tZ=~w\n\n')
    .

test :-
     X=list{},
    format('list (long)\tX=~w\n',[X]),
    optimized_format([X]^'list (short)\tX=~w\n\n')
    .

test :-
     X=term{},
    format('term (long)\tX=~w\n',[X]),
    optimized_format([X]^'term (short)\tX=~w\n'),
    domain(X,[constant{},compound{}]),
    format('->cst (long)\tX=~w\n',[X]),
    optimized_format([X]^'->cst (short)\tX=~w\n'),
    domain(X,[alpha,0'a,2,f(x)]),
    format('->instantiate (long)\tX=~w\n',[X]),
    optimized_format([X]^'->instantiate (short)\tX=~w\n\n')
    .

?-test.





