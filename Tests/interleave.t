#!/usr/bin/perl

use DyALog;
use strict;
use Test::More tests=>5;

my $test=Test::DyALog->new(workdir => '');

#$test->compile('il_test',input=>['il_test.dcg']);     # 1
#$test->compile('il_test',input=>['append.pl']);     # 1

$test->compile('il_test',input=>['il_test.dcg']);

$test->run(prog=>'il_test');
ok($? == 0 && $test->answers==1008,$test->message('generate '));


$test->compile('ilkleene_test',input=>['ilkleene_test.dcg']);

$test->run(prog=>'ilkleene_test',args=> '-a 3');
ok($? == 0 && $test->answers==62,$test->message('generate n=3'));

$test->run(prog=>'ilkleene_test',args=> '-a 4');
ok($? == 0 && $test->answers==242,$test->message('generate n=4'));

1;

__END__

