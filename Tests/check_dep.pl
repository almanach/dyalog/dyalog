#!/usr/bin/perl

use strict;

my @missing=();

foreach my $dep (@ARGV) {
    push(@missing,$dep) unless (eval "require $dep");
}

if (@missing) {
    print "*** WARNING *** Missing PERL modules @missing. Please install them from CPAN\n";
    exit 1;
}

exit;
