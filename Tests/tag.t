#!/usr/bin/perl

use DyALog;
use strict;
use Test::More tests=>29;

sub Test::DyALog::check_length {
    my $self = shift;
    my $length = shift;
    return $self->answers == 1 && ($self->answers)[0] =~ /L\s*=\s*$length/;
}

my $test=Test::DyALog->new(workdir => '');

$test->compile('tag_parser',input=>['test.tag'],flags=>'-parse -autoload -verbose_tag');     # 1

$test->run(prog=>'tag_parser',parse=>"Pierre mange une pomme", args=>'-');
ok($? == 0 && $test->answers==1,$test->succeed_sentence);

$test->run(prog=>'tag_parser',parse=>"Pierre mange une belle pomme", args=>'-');
ok($? == 0 && $test->answers==1,$test->succeed_sentence);

$test->run(prog=>'tag_parser',parse=>"Pierre mange une belle pomme rouge", args=>'-');
ok($? == 0 && $test->answers==1,$test->succeed_sentence);

$test->run(prog=>'tag_parser',parse=>"Pierre mange une pomme avec Paul", args=>'-');
ok($? == 0 && $test->answers==1,$test->succeed_sentence);

$test->run(prog=>'tag_parser',parse=>"Pierre mange une belle grosse pomme rouge avec Paul", args=>'-');
ok($? == 0 && $test->answers==1,$test->succeed_sentence);

$test->run(prog=>'tag_parser',parse=>"Pierre mange avec Paul", args=>'-');
ok($? == 0 && $test->failure,$test->fail_sentence);

######################################################################
# Test TIG parser

# Using pseudo compilation (actually analyze)
$test->compile('test.tigh',input=>['test.tag'],flags=>'-analyze tag2tig');

$test->compile('tig_parser',input=>['test.tag'],flags=>'-parse -autoload -verbose_tag -res test.tigh');

$test->run(prog=>'tig_parser',parse=>"Pierre mange une pomme", args=>'-');
ok($? == 0 && $test->answers==1,$test->succeed_sentence);

$test->run(prog=>'tig_parser',parse=>"Pierre mange une belle pomme", args=>'-');
ok($? == 0 && $test->answers==1,$test->succeed_sentence);

$test->run(prog=>'tig_parser',parse=>"Pierre mange une belle pomme rouge", args=>'-');
ok($? == 0 && $test->answers==1,$test->succeed_sentence);

$test->run(prog=>'tig_parser',parse=>"Pierre mange une pomme avec Paul", args=>'-');
ok($? == 0 && $test->answers==1,$test->succeed_sentence);

$test->run(prog=>'tig_parser',parse=>"Pierre mange une belle grosse pomme rouge avec Paul", args=>'-');
ok($? == 0 && $test->answers==1,$test->succeed_sentence);

$test->run(prog=>'tig_parser',parse=>"Pierre mange avec Paul", args=>'-');
ok($? == 0 && $test->failure,$test->fail_sentence);

######################################################################
# Test TAG parser with guiding

# Using pseudo compilation (actually analyze)

$test->compile('gtag_parser',input=>['test.tag'],flags=>'-guide strip -parse -autoload -verbose_tag');

$test->run(prog=>'gtag_parser',parse=>"Pierre mange une pomme", args=>'-');
ok($? == 0 && $test->answers==1,$test->succeed_sentence);

$test->run(prog=>'gtag_parser',parse=>"Pierre mange une belle pomme", args=>'-');
ok($? == 0 && $test->answers==1,$test->succeed_sentence);

$test->run(prog=>'gtag_parser',parse=>"Pierre mange une belle pomme rouge", args=>'-');
ok($? == 0 && $test->answers==1,$test->succeed_sentence);

$test->run(prog=>'gtag_parser',parse=>"Pierre mange une pomme avec Paul", args=>'-');
ok($? == 0 && $test->answers==1,$test->succeed_sentence);

$test->run(prog=>'gtag_parser',parse=>"Pierre mange une belle grosse pomme rouge avec Paul", args=>'-');
ok($? == 0 && $test->answers==1,$test->succeed_sentence);

$test->run(prog=>'gtag_parser',parse=>"Pierre mange avec Paul", args=>'-');
ok($? == 0 && $test->failure,$test->fail_sentence);

######################################################################
# Test TIG parser with guiding

# Using pseudo compilation (actually analyze)

$test->compile('gtig_parser',input=>['test.tag'],flags=>'-guide strip -parse -autoload -verbose_tag -res test.tigh');

$test->run(prog=>'gtig_parser',parse=>"Pierre mange une pomme", args=>'-');
ok($? == 0 && $test->answers==1,$test->succeed_sentence);

$test->run(prog=>'gtig_parser',parse=>"Pierre mange une belle pomme", args=>'-');
ok($? == 0 && $test->answers==1,$test->succeed_sentence);

$test->run(prog=>'gtig_parser',parse=>"Pierre mange une belle pomme rouge", args=>'-');
ok($? == 0 && $test->answers==1,$test->succeed_sentence);

$test->run(prog=>'gtig_parser',parse=>"Pierre mange une pomme avec Paul", args=>'-');
ok($? == 0 && $test->answers==1,$test->succeed_sentence);

$test->run(prog=>'gtig_parser',parse=>"Pierre mange une belle grosse pomme rouge avec Paul", args=>'-');
ok($? == 0 && $test->answers==1,$test->succeed_sentence);

$test->run(prog=>'gtig_parser',parse=>"Pierre mange avec Paul", args=>'-');
ok($? == 0 && $test->failure,$test->fail_sentence);
