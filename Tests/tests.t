#!/usr/bin/perl

use DyALog;
use strict;
use Test::More tests=>17;

my $test=Test::DyALog->new(workdir => '');

$test->compile('append');

$test->run(prog => 'append', args=> ' -a a');
ok($? == 0 && $test->answers == 1, $test->message('test a'));

$test->run(prog => 'append', args=> ' -a b');
ok($? == 0 && $test->answers == 1, $test->message('test b'));

$test->run(prog => 'append', args=> ' -a c');
ok($? == 0 && $test->answers == 1, $test->message('test c'));

$test->run(prog => 'append', args=> ' -a d');
ok($? == 0 && $test->answers == 1, $test->message('test d'));

$test->compile('test_display');	# 1

$test->run(prog => 'test_display', args=> "$SRCDIR/test_display.plh -a 10");
ok($? == 0 && grep($_ eq "Test a:a:a:a:a:a:a:a:a:a",$test->lines),$test->message('n=10'));

$test->run(prog => 'test_display', args=> "$SRCDIR/test_display.plh -a a");
ok($? == 0 && $test->answers == 1 && ($test->answers)[0] !~ /c\s*=>/,$test->message('opt display a'));

$test->run(prog => 'test_display', args=> "$SRCDIR/test_display.plh -a b");
ok($? == 0 && $test->lines == 2 && ($test->lines)[1] =~ /c\s*=>/,$test->message('opt display b'));

$test->run(prog => 'test_display', args=> "$SRCDIR/test_display.plh -a c");
ok($? == 0 && $test->lines == 2 && ($test->lines)[1] !~ /c\s*=>/,$test->message('opt display c'));

$test->run(prog => 'test_display', args=> "$SRCDIR/test_display.plh -a d");
ok($? == 0 && $test->lines == 3 && ($test->lines)[1] =~ /c\s*=>/,$test->message('opt display d'));

$test->run(prog => 'test_display', args=> "$SRCDIR/test_display.plh -a e");
ok($? == 0 && $test->lines == 3 && ($test->lines)[1] =~ /c\s*=>/,$test->message('opt display e'));

$test->run(prog => 'test_display', args=> "$SRCDIR/test_display.plh -a f");
ok($? == 0
   && $test->lines == 4 
   && ($test->lines)[1] =~ /,$/ 
   && ($test->lines)[3] !~ /,$/,
   $test->message('opt display f'));

$test->compile('mutable');

$test->compile('comment');

$test->compile('light');
$test->run(prog => 'light');
ok($?==0 && $test->answers == 1 && ($test->answers)[0] =~ /bar!toto/,
   $test->message('test light'));


