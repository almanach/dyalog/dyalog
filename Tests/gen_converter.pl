/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2001 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  gen_converter.pl -- Generic converter
 *
 * ----------------------------------------------------------------
 * Description
 *  Module for performing generic conversions
 *  To use it, use the exported predicates
 *         gen_tranducer/0 : run the conversion
 *  and define predicate
 *         transformer/2 : your own transformation rules
 *         initialize/0  : your own initialization
 * ----------------------------------------------------------------
 */


:-require 'format.pl'.

%% The goal
%% ?-gen_transducer.

%% To be user defined
:-rec_prolog transformer/2.

:-std_prolog gen_initialize/0.
:-prolog gen_transducer/0.

:-extensional verbose/0.

gen_initialize :-
	op(1100, fx, [ extensional,
		       hilog,
		       lco,
		       prolog,
		       rec_prolog,
		       std_prolog,
		       include,
		       require,
		       resource
		     ]),
	op( 550, xfy, [:>] ),
	op( 520, yfx, [<+] ),
	op( 550, xfy, [+>] )
        .   

gen_transducer :-
        gen_initialize,
        argv( Argv ),
        phrase( gen_parse_options, Argv, [] ),
        exit(0)
        .

:-prolog (dcg gen_parse_options/0, gen_parse_option/0).

gen_parse_options --> gen_parse_option, gen_parse_options ; [].
gen_parse_option -->
        ( ['-f',File] -> { record(in_file(File)), gen_reader(File) }
        ;   ['-o',File] -> { record( output_file(File) ) }
        ;   ['-I',Path] -> { add_load_path(Path) }
	;   ['-v'] -> { record( verbose ) }
        ;   [File] -> { record( in_file(File) ), gen_reader(File) }
        ;   { fail }
        )
        .

:-std_prolog gen_reader/1.

gen_reader( File ) :-
        (   find_file(File,AFile) xor error('File not found ~w',[File]) ),
        (   open(AFile,read,S) xor error('Could not open file ~w',[AFile] ) ),
        repeat(
               (   read_term(S,T,_),
                   gen_analyze(T),
                   T == eof
               )
              ),
        close(S)
        .

:-std_prolog gen_analyze/1.

gen_analyze(T) :-
        ( T == eof ->
            true
        ;
	    (\+ verbose xor format('%% ~k.\n',[T])),
	    gen_transform(T,Q),
	    format('~q.\n',[Q])
        )
        .   

:-std_prolog gen_transform/2, gen_transform_args/2.

gen_transform(T,Q) :-
	( var(T) ->  Q=T
	;   number(T) -> Q=T
	;   char(T) -> Q=T
	;   transformer(T,T2) -> gen_transform(T2,Q)
	;   atom(T) -> Q=T
	;   T =.. [F|Args],
	    gen_transform_args(Args,QArgs),
	    Q =.. [F|QArgs]
	)
	.

gen_transform_args(Args,QArgs) :-
	( Args = [] ->
	    QArgs = []
	;   Args = [T|Rest],
	    QArgs = [Q|QRest],
	    gen_transform(T,Q),
	    gen_transform_args(Rest,QRest)
	)
	.

