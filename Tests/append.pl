/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2001 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  append.pl -- append with modulation
 *
 * ----------------------------------------------------------------
 * Description
 * 
 * ----------------------------------------------------------------
 */

:-mode(append/3,+(+,-,+)).
:-lco(append/3).

append([],Y,Y).
append([X|A],Y,[X|B]) :- append(A,Y,B).

:-prolog test/2.

test(a,L) :- append([A],[B,A],L).
test(b,L) :- append([A],[],L).
test(c,L) :- append([A],[A],L).
test(d,L) :- append([A,B],[A,B],L).

?-argv([T]),test(T,L).
