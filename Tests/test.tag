/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2003, 2004, 2006, 2008, 2009, 2011 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  test.tag -- Small Test file for TAGs
 *
 * ----------------------------------------------------------------
 * Description
 * 
 * ----------------------------------------------------------------
 */

?-recorded('N'(N)),tag_phrase(s,0,N).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

tag_tree{ name => tn1,
          family => tn1,
          tree=> tree
	s(
	  np,
	  vp(
	     <>v,
	     np
	    )
	 )
        }.

tag_tree{ name => dn,
          family => np,
          tree=> tree np(d,<>n)
        }.

tag_tree{ name => pn,
          family => np,
          tree=> tree np(<>pn)
        }.

tag_tree{ name => vppn,
          family => vppn,
          tree=> auxtree
        vp(* vp, pp( <>p, np ))
        }.

tag_tree{ name => nppn,
          family => nppn,
          tree=> auxtree
        np(* np, np( <>p, np ))
        }.

% tag_tree{ name => void,
%           family => void,
%           tree=> auxtree
%         np(* np, [])
%         }.

tag_tree{ name => d,
          family => d,
          tree=> tree d(<>d)
        }.

tag_tree{ name => an,
          family => a,
          tree=> auxtree n(<>a,*n)
        }.

tag_tree{ name => na,
          family => a,
          tree=> auxtree n(*n,<>a)
        }.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%:-scanner(check_lexical).
:-parse_mode(token).

:-std_prolog anchor/7.

anchor(_,
       Token,
       Cat,
       Left,
       Right,
       Cat,
       _
      ) :-
        'C'( Left,Token,Right),
	token_cat(Cat,Token)
        .

:-extensional token_cat/2.

token_cat(pn,'Paul').
token_cat(pn,'Pierre').
token_cat(n,pomme).
token_cat(n,poire).
token_cat(d,un).
token_cat(d,une).
token_cat(d,le).
token_cat(d,la).
token_cat(p,avec).
token_cat(v,mange).
token_cat(a,belle).
token_cat(a,rouge).
token_cat(a,grosse).

%% Dummy predicate used to leave a trace in the forest
verbose!struct(_Tree_Name,_Family).

%% Dummy predicate used to leave a trace in the forest
verbose!anchor(_Anchor,_Left,_Right,_Tree_Name,_Top, _Lemma_Lex, _TagAnchor).

%% Dummy predicate used to leave a trace in the forest
verbose!lexical([_Lexical|_],_Left,_Right,lex,_Lexical).

%% Dummy predicate used to leave a trace in the forest
verbose!adj.

:-std_prolog tag_family_load/7.

tag_family_load(Family,Cat,_,_,_,L,R) :- term_range(R,_,L).

