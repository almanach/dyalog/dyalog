/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2001 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  rcg_transducer.pl -- Tranducer from RCG to DyALog
 *
 * ----------------------------------------------------------------
 * Description
 *  A preliminary transducer from RCG syntax to DyALog syntax
 *  Could be written in Perl to be closer from Pierre's syntax (?)
 * ----------------------------------------------------------------
 */

:-require 'format.pl'.

:-prolog initialize/0, transducer/0.

:-op(300,fx, [&] ).

initialize :-
	op(300,xfy,['@']),
	op(300,fx, [&] )
        .   

transducer :-
        initialize,
        argv( Argv ),
        phrase( parse_options, Argv, [] ),
        exit(0)
        .

:-prolog (dcg parse_options/0, parse_option/0).

parse_options --> parse_option, parse_options ; [].
parse_option -->
        ( ['-f',File] -> { record(in_file(File)), reader(File) }
        ;   ['-o',File] -> { record( output_file(File) ) }
	;   ['-I',Path] -> { add_load_path(Path) }
        ;   [File] -> { record( in_file(File) ), reader(File) }
        ;   { fail }
        )
        .

?-transducer. 

:-rec_prolog reader/1.

reader( File ) :-
	(   find_file(File,AFile) xor error('File not found ~w',[File]) ),
	(   open(AFile,read,S) xor error('Could not open file ~w',[AFile] ) ),
        date_time(Year,Month,Day,Hour,Min,Sec),
        format('%% RCG Transducer\n%% File ~w\n%% Date: ~w ~w ~w at ~w:~w:~w\n\n',
	       [File,Month,Day,Year,Hour,Min,Sec]),
	write(':-require ''rcg.pl''.\n\n' ),
        repeat(
               (   read_term(S,T,_),
                   analyze(T),
                   T == eof
               )
              ),
        close(S)
	.

:-rec_prolog analyze/1.

analyze(T) :-
        ( T == eof ->
            true
	;  T = axiom(S) ->
	    emit_axiom(S)
        ;   T =   (Head --> Body) ->
            emit_rule(T)
        ;   
            error('unknown type ~w\n',[T])
        )
        .   

:-rec_prolog emit_axiom/1.

emit_axiom(S) :-
	format('% Axiom ~w\n',[S]),
	transform_elem(S(R),Call,[[true]],_),
	format('?-recorded(''N''(~w)),rcg_range(0,~w,~w),~w.\n',[N,N,R,Call])
	.

:-rec_prolog emit_rule/1.

emit_rule((Head-->Body)) :-
	format('% RCG ~w\n',[(Head-->Body)]),
	transform((Head,Body),(NHead,NBody),[[true]],Code),
	format('~w :-\n\t~l,\n\t~w.\n\n',[NHead,['~w',',\n\t'],Code,NBody])
        .


:-rec_prolog transform/4,
	transform_elem/4,
	transform_args/4,
	transform_arg/4,
	transform_scan/4
	.

transform(U,NU,In,Out) :-
	( U = (A,B) ->
	    transform_elem(A,NA,In,Middle),
	    transform(B,NB,Middle,Out),
	    NU=(NA,NB)
	;   U = true ->
	    NU=true,
	    In=Out
	;   transform_elem(U,NU,In,Out)
	)
	.

transform_elem(A,NA,In,Out) :-
	( A = { X } ->
	    NA = X,
	    In = Out
	;   A = (& X) ->
	    ( rcg_registered_escape(X,RCG_Vars,NA) ->
		transform_args(RCG_Vars,New_Vars,In,Out)
	    ;	
		error('not a RCG escape predicate ~w',[X])
	    )
	;   A =.. [apply,Pred|RCG_Args] ->
	    transform_args(RCG_Args,DyALog_Args,In,Out),
	    NA =.. [apply,Pred|DyALog_Args]
	;
	    error('not a RCG litteral ~w\n',[A])
	)
	.

transform_args([],[],In,In).
transform_args([A|R],[NA|NR],In,Out) :-
	transform_args(R,NR,In,Middle),
	transform_arg(A,NA,Middle,Out)
	.

transform_arg(X,NX,In,Out) :-
	( var(X) ->
	    NX=X,
	    In=Out
	;   X = "" ->
	    Out = [[rcg_empty(NX)]|In]
	;   X = Y@Z ->
	    transform_arg(Y,NY,In,Middle),
	    transform_arg(Z,NZ,Middle,Middle2),
	    Out = [[rcg_decompose(NX,NY,NZ)]|Middle2]
	;
	    transform_scan(X,NX,In,Out)
	)
	.

transform_scan(X,NX,In,Out) :-
	( X=[T] ->
	    Out = [[rcg_scan(NX,T)]|In]
	; X=[T|Y] ->
	    transform_scan(Y,NY,In,Middle),
	    Out=[[rcg_decompose(NX,NT,NY)],
		 [rcg_scan(NT,T)] | Middle
		]
	)
	.



:-extensional rcg_registered_escape/3.

rcg_registered_escape(eqstr(R1,R2),[R1,R2],rcg_eqstr(R1,R2)).
rcg_registered_escape(eqstrlen(R1,R2),[R1,R2],rcg_eqstrlen(R1,R2)).
rcg_registered_escape(eqlen(R,N),[R],rcg_length(R,N)).

rcg_registered_escape(range(R,A,B),[R],rcg_range(A,B,R)).
rcg_registered_escape(empty(R),[R],rcg_empty(R)).
rcg_registered_escape(decompose(XY,X,Y),[XY,X,Y],rcg_decompose(XY,X,Y)).
