#!/usr/bin/perl

use DyALog;
use strict;
use Test::More tests=>16;

sub Test::DyALog::check_length {
    my $self = shift;
    my $length = shift;
    return $self->answers == 1 && ($self->answers)[0] =~ /L\s*=\s*$length/;
}

my $test=Test::DyALog->new(workdir => '');

$test->compile('simple1',input=>['simple.rcg'],flags=>'-rcg');	# 1

$test->run(prog => 'simple1', args=> '-a list ""');
ok($? == 0 && $test->check_length(0),$test->message('succeed on empty'));

$test->run(prog => 'simple1', args=> '-a list abc');
ok($? == 0 && $test->check_length(1),$test->message('succeed on abc'));

$test->run(prog => 'simple1', args=> ' -a list aaabbbccc');
ok($? == 0 && $test->check_length(3),$test->message('succeed on aaabbbccc'));

$test->run(prog => 'simple1', args=> ' -a list abbc');
ok($? == 0 && $test->failure,$test->message('fail on on abbc'));

$test->compile('simple2',input=>['simple.rcg'],flags=>'-parse -rcg');	# 2

$test->run(prog => 'simple2', args=> ' -slex "" -a a');
ok($? == 0 && $test->check_length(0),$test->message('succeed on empty'));

$test->run(prog => 'simple2', args=> ' -slex abc -a a');
ok($? == 0 && $test->check_length(1),$test->message('succeed on abc'));

$test->run(prog => 'simple2', args=> ' -slex aaabbbccc -a a');
ok($? == 0 && $test->check_length(3),$test->message('succeed on aaabbbccc'));

$test->run(prog => 'simple2', args=> ' -slex abbc -a a');
ok($? == 0 && $test->failure,$test->message('fail on on abbc'));

$test->run(prog => 'simple2', args=> ' -slex aaabbbccc -a b');
ok($? == 0 && $test->answers==1,$test->message('checking range instantiation'));

$test->compile('quad',input=>['quad.rcg'],flags=>'-parse -rcg');	# 2

$test->run(prog => 'quad', args=> ' -slex ""');
ok($? == 0 && $test->failure,$test->message('fail on empty'));

$test->run(prog => 'quad', args=> ' -slex a'); 
ok($? == 0 && $test->succeed,$test->message('succeed on a'));

$test->run(prog => 'quad', args=> ' -slex aaa'); 
ok($? == 0 && $test->failure,$test->message('fail on aaa'));

$test->run(prog => 'quad', args=> " -slex "."a"x2048); 
ok($? == 0 && $test->succeed,$test->message('succeed on a^2048'));

1;

__END__
