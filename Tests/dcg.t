#!/usr/bin/perl

use DyALog;
use strict;
use Test::More tests=>12;

sub Test::DyALog::check_length {
    my $self = shift;
    my $length = shift;
    return $self->answers == 1 && ($self->answers)[0] =~ /L\s*=\s*$length/;
}

my $test=Test::DyALog->new(workdir => '');

$test->compile('fset',input=>['fset.dcg'],flags=>'-parse');	# 1

$test->run(prog=>'fset',parse=>"Anne mange", args=>'-');
ok($? == 0 && $test->answers==1,$test->succeed_sentence);

$test->run(prog=>'fset',parse=>"Anne fume le cigare", args=>'-');
ok($? == 0 && $test->answers==1,$test->succeed_sentence);

$test->run(prog=>'fset',parse=>"Anne fume les cigare", args=>'-');
ok($? == 0 && $test->answers==0,$test->fail_sentence);

$test->run(prog=>'fset',parse=>"Anne mange une pomme verte", args=>'-');
ok($? == 0 && $test->answers==1,$test->succeed_sentence);

$test->run(prog=>'fset',parse=>"Anne mange une jolie pomme verte", args=>'-');
ok($? == 0 && $test->answers==1,$test->succeed_sentence);

$test->run(prog=>'fset',parse=>"une pomme mange Anne", args=>'-');
ok($? == 0 && $test->answers==1,$test->fail_sentence);

$test->compile('kleene',input=>['kleene.dcg'],flags=>'-parse');  #2

$test->run(prog=>'kleene',args=>'-slex abeebbeeba');
ok($? == 0 && $test->answers==1,$test->message("succeed on 'abeebbeeba'"));

$test->run(prog=>'kleene',args=>'-slex abeebbfba');
ok($? == 0 && $test->answers==0,$test->message("fail on 'abeebbfba'"));

$test->run(prog=>'kleene',args=>'-slex x');
ok($? == 0 && $test->answers==0,$test->message("fail on 'x'"));

$test->run(prog=>'kleene',args=>'-slex xaa');
ok($? == 0 && $test->answers==1,$test->message("succeed on 'xaa'"));


1;

__END__
