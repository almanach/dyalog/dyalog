#!/usr/bin/perl

use DyALog;
use strict;
use Test::More tests=>13;

my $test=Test::DyALog->new(workdir => '');

$test->compile('rcg_transducer');	# 1

		
              	# 2
$test->run(prog => 'rcg_transducer',
	   args=> " -a -I $SRCDIR g1.rcg"
	   );
ok($? == 0 && $test->lines==34,$test->message('building g1.pl for a^nb^nc^n'));

open(G,">g1.pl") || die "can't open g1.pl";
print G $test->stdout;
close(G) || die "can't close g1";
				
$test->compile('g1','flags'=>'-parse'); # 3

$test->run(prog => 'g1', args=> ' -slex ""'); # 4
ok($? == 0 && $test->succeed,$test->message('succeed on empty'));

				# 5
$test->run(prog => 'g1', args=> ' -slex abc');
ok($? == 0 && $test->succeed,$test->message('succeed on abc'));

				# 6
$test->run(prog => 'g1', args=> ' -slex aabbcc -forest');
ok($? == 0 && $test->succeed && $test->forest == 5,$test->message('succeed on aabbcc'));

				# 7
$test->run(prog => 'g1', args=> ' -slex aabc');
ok($? == 0 && $test->failure,$test->message('fail on aabc'));

$test->run(prog => 'rcg_transducer',
	   args=> " -a -I $SRCDIR g2.rcg"
	   );
ok($? == 0 && $test->lines==20,$test->message('building g2.pl for a^(n^2)'));

open(G,">g2.pl") || die "can't open g2.pl";
print G $test->stdout;
close(G) || die "can't close g2";
				
$test->compile('g2','flags'=>'-parse');

$test->run(prog => 'g2', args=> ' -slex ""');
ok($? == 0 && $test->failure,$test->message('fail on empty'));

$test->run(prog => 'g2', args=> ' -slex a'); 
ok($? == 0 && $test->succeed,$test->message('succeed on a'));

$test->run(prog => 'g2', args=> ' -slex aaa'); 
ok($? == 0 && $test->failure,$test->message('fail on aaa'));

$test->run(prog => 'g2', args=> " -slex "."a"x2048); 
ok($? == 0 && $test->succeed,$test->message('succeed on a^2048'));


