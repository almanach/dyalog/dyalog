#!/usr/bin/perl

use DyALog;
use strict;
use Test::More tests=>4;

my $test=Test::DyALog->new(workdir => '');


my $uname=`uname -v`;
$uname=~ m/(.+?) Kernel Version (.+?):.+RELEASE_(.+?)$/g;
my ($os,$version,$arch)=($1,$2,$3);
my $opt="";

if (0 && $os eq "Darwin"){
   $test->run(prog => '/bin/true');
   ok($?!=0,$test->message());
   $test->run(prog => '/bin/true');  
   ok($?!=0,$test->message()); 
   $opt="-ffixed-r15 -ffixed-r16 -ffixed-r17" if ($arch eq "PPC");
  `sh tfs.sh $opt 2> /dev/null`   
}else{
  $test->tfs_compile('list');	# 1

  $test->compile('tfs_append', flags => '-tfs ./liblist.la'); #2
}


$test->run(prog => 'tfs_append');

ok($?==0,$test->message());

ok($test->in('term (long)	X=C__1::term{}',
	     $test->lines),
   $test->message('(term long)'));
    
    




