#!/usr/bin/perl

use DyALog;
use strict;
use Test::More tests=>5;

sub Test::DyALog::check_length {
    my $self = shift;
    my $length = shift;
    return $self->answers == 1 && ($self->answers)[0] =~ /L\s*=\s*$length/;
}

my $test=Test::DyALog->new(workdir => '');

$test->compile('bmg',input=>['bmg.dcg'],flags=>'-parse');	# 1

$test->run(prog=>'bmg',parse=>"le garcon mange", args=>'-');
ok($? == 0 && $test->answers==1,$test->succeed_sentence());

$test->run(prog=>'bmg',parse=>"le garcon mange le gateau", args=>'-');
ok($? == 0 && $test->answers==1,$test->succeed_sentence());

$test->run(prog=>'bmg',parse=>"qui mange le gateau", args=>'-');
ok($? == 0 && $test->answers==1,$test->succeed_sentence());

$test->run(prog=>'bmg',parse=>"le garcon donne le gateau que la fille mange", args=>'-');
ok($? == 0 && $test->answers==1,$test->succeed_sentence());


1;

__END__
