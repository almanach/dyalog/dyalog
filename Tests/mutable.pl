/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2000 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  mutable.pl -- Test mutable
 *
 * ----------------------------------------------------------------
 * Description
 *   Mutable in DyALog
 * ----------------------------------------------------------------
 */

:-require 'format.pl'.

:-std_prolog test_repeat.

?-test_repeat.

test_repeat :-
%	argv([F]),
	mutable(M,[0]),
	repeat((
		mutable_read(M,L::[I|_]),
		format('Term ~w hello\n',[L]),
		J is I+1,
		mutable(M,[J|L]),
		J == 20
	       ))
	.


