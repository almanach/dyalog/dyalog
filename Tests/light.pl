/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2002 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  light.pl -- Test light tabular
 *
 * ----------------------------------------------------------------
 * Description
 * 
 * ----------------------------------------------------------------
 */

 :-std_prolog foo/2.

foo(Pred1,Pred2) :-
	(   bar(Pred1,Pred2) xor  Pred1=Pred2 )
	.

:-light_tabular bar/2.

bar(Pred1::F1/N,F/N) :-
	atom_module(F,bar,F1)
	.

?-foo(toto/2,P).


