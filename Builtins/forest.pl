/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 1999, 2004, 2005 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  forest.pl -- Forest Brwoser
 *
 * ----------------------------------------------------------------
 * Description
 * 
 * ----------------------------------------------------------------
 */


%% Module Interface

:-module(forest).

:-import{ file=>'format.pl', preds => [format/2,format_hook/4,format/3] }.

:-std_prolog update_counter/2,value_counter/2.
:-std_prolog display_forest/1.

:-std_prolog forest/2.

:-extensional forest_type/2.

:-std_prolog wrapped_forest/1.

:-finite_set(forest,[init,call,and,or,join,indirect,rand,trace,void]).

:-rec_prolog forest_decode/5.

:-light_tabular
	build_forest_aux/3,
	build_forest/3
	.

:-std_prolog
	build_forest_ter/3
	.

:-std_prolog empty/1.

:-mode(build_forest/3,+(+,-,-)).

:-end_require.

%%----------------------------------------------------------------------
%% Module Implementation
%%
%% This part is not read when importing/including this file

forest_type(0,init). 		% for initial objects              bptr = init
forest_type(1,call).            % for objects resulting from call  bptr = call
forest_type(2,and).             % for std objects                  bptr = and(trans,item)
forest_type(3,or).              % for alternatives                 bptr = or(bptr1,bptr2)
forest_type(4,join).            % not used
forest_type(5,indirect).        % for inlined objects             bptr = indirect(item) 
forest_type(6,rand).            % not used (reverse and)
forest_type(7,trace).           % to label objects                 bptr = trace(label,bptr1)

%% The indirect type is used for inligning the forest attached to an object
%% rather than introducing a new entry (clause) in the forest
%% It is used for kleene operator

%% The trace type is strangely implemented. The label is actually not
%% related to the trans but to next object to be joined with trans
%% i.e.   and(trans,item) with bptr(trans) = trace(item_label,bptr1)

forest(Addr,Forest) :-
	( Addr == 0 ->
	    Forest = void
	;   
	    forest(Addr,Type,AX,AY,L,Ind),
	    forest_type(Type,FType),
	    forest_decode(FType,AX,AY,L,_Forest),
	    ( Ind = 1 ->
		Forest = indirect(_Forest)
	    ;
		Forest = _Forest
	    )
	)
	.

forest_decode(X::forest[init,call],_,_,_,X).
forest_decode(and,A,B,L,and(A,BB)) :- (L=[] -> BB=B ; BB = (L:B)).
forest_decode(indirect,A,_,_,indirect(A)).

update_counter(Name,V) :- 
    recorded( counter(Name,V) ) ->
    ( erase( counter(Name,V) ),W is V+1,record( counter(Name,W) ) );
    ( V=0, record( counter(Name,1) ) ).

value_counter(Name,V) :- (recorded( counter(Name,V) ) -> true ; V = 0).

wrapped_forest( X ) :- 
	(   item_term(A,X),
	    recorded(A,Add_A),
	    build_forest(Add_A,_,_),
	    fail
	;   
	    loop(0)
	)
    .

:-std_prolog loop/1.

loop(Id) :- 
	display_forest(Id),
	New_Id is Id + 1,
	loop(New_Id).

build_forest_aux(Add,K,L) :-
	forest(Add,Forest),
	%% format('Forest ~w->~w\n',[Add,Forest]),
	build_forest_ter(Forest,K,L)
	.

build_forest_ter(Forest,K,L) :-
	( Forest = forest[call,init] ->
	    K=L
	;   Forest = and(Add_X,Y) ->
	    (	Y = LY:Add_Y xor Y=Add_Y, LY=[] ),
	    ( empty(Add_Y) ->
		build_forest_aux(Add_X,K,L)
	    ;	
		build_forest_aux(Add_X,K,[LY:Id_Y|L]),
		build_forest(Add_Y,Id_Y,_)
	    )
	;   Forest = indirect(Forest2) ->
	    K=[L_Inline:Body_Inline|L],
	    build_inline_forest(Forest,L_Inline,Body_Inline)
	;   
	    format('not yet implemented ~w\n',[Forest])
	).

:-light_tabular build_inline_forest/3.
:-mode(build_inline_forest/3,+(+,-,-)).

build_inline_forest(indirect(Forest),inline(Id),Body) :-
	update_counter(forest_inline,Id),
	build_forest_ter(Forest,Body,[])
	.

empty(Add) :- forest(Add,forest[call,init,void]).

build_forest(Add,Id,L) :-
	update_counter(forest_node,Id),
	build_forest_aux(Add,L,[])
	.


display_forest(Id) :-
	'$answers'( build_forest(Add,Id,L) ),
	( recorded( forest!displayed(Id) ) ->
	    true
	;
	    record( forest!displayed(Id) ),
	    recorded(A,Add),
	    ( item_term(A,X) ->
		numbervars(X),
		format('~w\n\t~w <-- ~F\n',[X,Id,L])
	    ;	
		true		%% part to skip
	    )
	)
	.

format_hook(0'F,Stream,[L|R],R) :-
	every((domain(Label:Id2,L),
	       ( Label = [] ->
		   format(Stream, ' ~w', [Id2] )
	       ;   Label = inline(Label2) ->
		   format(Stream, '#~w { ~F }', [Label2,Id2] )
	       ;   
		   format(Stream, ' [~w] ~w', [Label,Id2] )
	       )))
	.

