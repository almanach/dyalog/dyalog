/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2001 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  rcg.pl -- a few predicate to handle RCG
 *
 * ----------------------------------------------------------------
 * Description
 *  Range Concatenation Grammar have been introduced by Pierre Boullier
 * ----------------------------------------------------------------
 */

:-op(400,fx, [&] ).

:-extensional rcg_decompose/3.
:-std_prolog  rcg_scan/2.
:-extensional rcg_empty/1.
:-extensional rcg_range/3.
	     
rcg_range(A,B,A:B).
rcg_empty(A:A).
rcg_decompose(L:R,L:J,J:R).
rcg_scan(L:R,T) :- 'C'(L,T,R).

:-std_prolog rcg_eqstr/2.

rcg_eqstr(Range1:: L1:R1, Range2:: L2:R2) :-
	(   L1=R1,
	    L2=R2
	;   
	    'C'(L1,T,M1),
	    'C'(L2,T,M2),
	    rcg_eqstr(M1:R1,M2:R2)
	)
	.

:-std_prolog rcg_length/2.

rcg_length(L:R,Length) :-
	( ground(L:R) ->
	    Length is R-L
	;   ground(Length) ->
	    ( Length == 0 ->
		L=R
	    ;	ground(L) ->
		R is L+Length
	    ;	ground(R) ->
		L is R-Length
	    ;	
		rcg_general_length(L,R,Length)
	    )
	;
	    rcg_general_length(L,R,Length)
	)
	.

:-std_prolog rcg_general_length/3.

rcg_general_length(L,R,Length) :-
	(   L=R, Length=0
	;   
	    'C'(L,_,M),
	    rcg_general_length(M,R,Length2),
	    Length is Length2 + 1
	)
	.

:-std_prolog rcg_eqstrlen/2.

rcg_eqstrlen(R1,R2) :-
	rcg_length(R1,L),rcg_length(R2,L)
	.

