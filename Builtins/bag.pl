:-hilog min,max,sum,succ,sumcount.

max(X,Y,Z) :- 
    X == -infinity -> Z = Y ;
    Y == -infinity -> Z = X ;
    X < Y          -> Z = Y ;
    Z=X.

min(X,Y,Z) :- 
    X == +infinity -> Z = Y ;
    Y == +infinity -> Z = X ;
    X > Y          -> Z = Y ;
    Z=X.

sum(X,Y,Z) :- Z is X+Y.
succ(X,_,Z) :- Z is X+1.
sumcount([S|C],X,[S1|C1]) :- S1 is S+X, C1 is C+1.

bagReduce(SetPred,Arg,Op,Id) :-
    iterate( NewAcc^(NewAcc=Id,V^Acc^(Op(Acc,V,NewAcc))), SetPred(V) ), Arg = NewAcc.

bagMax(SetPred,Arg) :- bagReduce(SetPred,Arg,max,-infinity).
bagMin(SetPred,Arg) :- bagReduce(SetPred,Arg,min,+infinity).
bagSum(SetPred,Arg) :- bagReduce(SetPred,Arg,sum,0).
bagCount(SetPred,Arg) :- bagReduce(SetPred,Arg,succ,0).
bagAvg(SetPred,Avg) :- bagReduce(SetPred,X,sumcount,[0|0]), X = [Sum|Count], Avg is  Sum / Count.

enum(L)(X) :- domain(X,L).
