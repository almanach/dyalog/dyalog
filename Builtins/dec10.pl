/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 1998 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  dec10.pl -- Dec-10 I/0 compatibility library
 *
 * ----------------------------------------------------------------
 * Description
 * (derived from wamcc distribution file dec10io.pl)
 * ----------------------------------------------------------------
 */

% '$use_dec10io'.

:-extensional '$dec10_stream'/3.

% '$dec10_stream'(user,       0,input).

'$dec10_stream'(user_input, 0,input).

% '$dec10_stream'(user,       1,output).

'$dec10_stream'(user_output,1,output).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Input

:-std_prolog see/1,seeing/1,seen/0.

see(File):-
    ( atom(File) ->                  % File is a filename or a predefined stream
      (
	  recorded('$dec10_stream'(File,Stream,input))            % File already open
      xor
          open(File,read,Stream),                                 % File to be open
	  record( '$dec10_stream'(File,Stream,input) )
      )
    ;	
	integer(File),
	Stream = File
    ),
    set_input(Stream).

seeing(File):-
    current_input(Stream),
    recorded( '$dec10_stream'(File,Stream,input) ).

seen:- 
    current_input(Stream),
    (
	Stream = 0
    xor
        close(Stream),
        erase( '$dec10_stream'(_,Stream,input) )
    ).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Output

:-std_prolog tell/1,telling/1,told/0.

tell(File):-
    ( atom(File) ->                  % File is a filename or a predefined stream
      (
	  recorded('$dec10_stream'(File,Stream,output))           % File already open
      xor
	  open(File,write,Stream),                                 % File to be open
	  record( '$dec10_stream'(File,Stream,output) )
      )
    ;	
	integer(File),
	Stream = File
    ),
    set_output(Stream).

telling(File):-
    current_output(Stream),
    recorded( '$dec10_stream'(File,Stream,output) ).

told:- 
    current_output(Stream),
    (	Stream = 1
    xor
        close(Stream),
	erase( '$dec10_stream'(_,Stream,output) )
    ).





