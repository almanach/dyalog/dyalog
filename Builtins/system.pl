/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2000 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  system.pl -- misc. system predicate
 *
 * ----------------------------------------------------------------
 * Description
 *   API to misc. system functions
 * ----------------------------------------------------------------
 */

:-std_prolog system_perror/1.

system_perror(Name) :- '$interface'( 'perror'(Name:string), [return(none)] ).

:-std_prolog system_error/1.
system_error(Name) :- system_perror(Name),exit(1).

