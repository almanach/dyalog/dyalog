/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 1998, 2004, 2005 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  call.pl -- predicate call
 *
 * ----------------------------------------------------------------
 * Description
 *
 * ----------------------------------------------------------------
 */

:-require 'format.pl'.

'$dyalog_module_call'.

:-prolog call!call/1,'$call'/1,'$bmgcall'/5,call!bmgcall/5.

:-rec_prolog call!pcall/1,'$pcall'/1,'$bmgpcall'/5,call!bmgpcall/5.

:-prolog call!tagcall/4,'$tagcall'/4.

call!call( X ) :-
    var( X ) -> error( instanciation_error( call(X), 1 ) );
    X = (Y,Z) -> (call!call(Y) , call!call(Z)) ;
    X = (Y;Z) -> (call!call(Y) ; call!call(Z)) ;
    '$call'(X).

call!pcall( X ) :-
	( var( X ) -> error( instanciation_error( pcall(X), 1 ) );
	    X = (Y,Z) -> (call!pcall(Y) , call!pcall(Z)) ;
	    X = (Y;Z) -> (call!pcall(Y) ; call!pcall(Z)) ;
	    '$pcall'(X)
	    ).

    
call!bmgcall(X,L,R,BMG_L,BMG_R) :-
	( var( X ) -> error( instanciation_error( call(X), 1 ) )
	;   X = (Y,Z) ->
	    length(BMG_L,N),
	    length(BMG_M,N),
	    call!bmgcall(Y,L,M,BMG_L,BMG_M),
	    call!bmgcall(Z,M,R,BMG_M,BMG_R)
	;   X = (Y;Z) ->
	    (	call!bmgcall(Y,L,R,BMG_L,BMG_R)
	    ;	call!bmgcall(Z,L,R,BMG_L,BMG_R)
	    )
	;   X = {Y} ->
	    L=R,BMG_L=BMG_R,call!call(Y)
	;   X = [] ->
	    L=R,BMG_L=BMG_R
	;   '$bmgcall'(X,L,R,BMG_L,BMG_R)
	)
	.


call!bmgpcall(X,L,R,BMG_L,BMG_R) :-
	( var( X ) -> error( instanciation_error( call(X), 1 ) )
	;   X = (Y,Z) ->
	    length(BMG_L,N),
	    length(BMG_M,N),
	    call!bmgpcall(Y,L,M,BMG_L,BMG_M),
	    call!bmgpcall(Z,M,R,BMG_M,BMG_R)
	;   X = (Y;Z) ->
	    (	call!bmgpcall(Y,L,R,BMG_L,BMG_R)
	    ;	call!bmgpcall(Z,L,R,BMG_L,BMG_R)
	    )
	;   X = {Y} ->
	    L=R,BMG_L=BMG_R,call!pcall(Y)
	;   X = [] ->
	    L=R,BMG_L=BMG_R
	;   '$bmgpcall'(X,L,R,BMG_L,BMG_R)
	)
	.
	    
call!tagcall(X,L,R,Info) :-
	( var(X) -> error( instanciation_error( call(X), 1))
	;
	    '$tagcall'(X,L,R,Info)
	)
	.
	
