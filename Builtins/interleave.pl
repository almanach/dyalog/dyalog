/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id: interleave.pl 933 2011-05-19 13:22:32Z clerger $
 * Copyright (C) 2003, 2004, 2011 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  interleave.pl -- support for interleave operator
 *
 * ----------------------------------------------------------------
 * Description
 * 
 * ----------------------------------------------------------------
*/
 

%% Public interface

:-std_prolog interleave_start/4.
:-std_prolog interleave_register/4.
:-std_prolog interleave_choose/4.
:-std_prolog interleave_choose_alt/5.
:-std_prolog interleave_choose_first/6.

:-end_require.

:-require 'format.pl'.

%% No debug
:-xcompiler((il_debug(F,L) :- (true))).

%% Debug Start Code
%% :-xcompiler((il_debug(F,L) :- (optimized_format(L^F),fail ; true))).

:-std_prolog interleave_simplify/2.

format_hook(0'x,Stream,[IL_File|R],R) :-
	interleave_simplify(IL_File,IL_File2),
	format(Stream,'~w',[IL_File2])
	.

interleave_simplify(IL,XIL) :-
	( var(IL) -> error('Unexpected variable as interleave structure: ~w',[IL])
	;   IL = [] -> XIL=[]
	;   IL = [Label,Exit|IL2] ->
	    interleave_simplify_x(Exit,XExit),
	    interleave_simplify_aux(IL2,XIL2),
	    XIL=[Label,XExit|XIL2]
	).

:-rec_prolog interleave_simplify_aux/2.
interleave_simplify_aux([],[]).
interleave_simplify_aux([A|IL],[XA|XIL]) :-
	interleave_simplify_x(A,XA),
	interleave_simplify_aux(IL,XIL)
	.

:-std_prolog interleave_simplify_x/2.

interleave_simplify_x(A,XA) :-
	( A = [_|_] -> interleave_simplify(A,XA)
	; A = R : '$CLOSURE'(_XA,_) ->  XA = (R : _XA)
	; A = '$CLOSURE'(XA,_)
	)
	.

%% Debug end code

%% Private interface

interleave_start(Label,Exit,Left,Right) :-
	( Left == [] ->
	    Right = [Label,Exit]
	;   atom_module(Label,Label1,_),
	    interleave_update(Label1,Left,Right,
			      [Label1,Exit1|Table1],
			      [Label1,Exit1,[Label,Exit]|Table1]
			     )
	),
	il_debug('Interleave start ~x -> ~x\n',[Left,Right]),
	true
	.

interleave_register(Label,Alt,Left,Right) :-
	interleave_update(Label,Left,Right,
			  [Label,Exit|Table],
			  [Label,Exit, (Rank:Alt) |Table]
			 ),
	il_debug('Interleave register ~x -> ~x\n',[Left,Right]),
	true
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

:-std_prolog interleave_update/5.

	     
interleave_update(Label,
		  Left::[L|_],Right,
		  Table_Left::[Label,_|_Tail],
		  Table_Right::[Label,_, (Rank:Alt) |_Tail]
		 ):-
	( L==Label ->
	  Left = Table_Left,
	  Right = Table_Right,
	  length(_Tail,N),
	  Rank is N+1
	; atom_module(Label,Label1,_),
	  interleave_update(Label1,Left,Right,
			    Table1_Left::[Label1,X|T1_Left],
			    Table1_Right::[Label1,X|T1_Right]
			   ),
	  interleave_update_aux(T1_Left,T1_Right,Table_Left,Table_Right)
	)
	.

:-std_prolog interleave_update_aux/4.

interleave_update_aux(Left,Right,Table_Left,Table_Right) :-
	( Left = [Table_Left|Left2] ->
	    Right = [Table_Right|Left2]
	;   
	    Left = [X|Left2],
	    Right = [X|Right2],
	    interleave_update_aux(Left2,Right2,Table_Left,Table_Right)
	)
	.
	    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%! @brief To choose next task in interleave table

%:-std_prolog interleave_choose_x/3.

interleave_choose(Label,Left,Right,Alt) :-
	il_debug('Interleave choose ~w ~x\n',[Label,Left]),
	interleave_choose_x(Left,Right,_Alt),
	(_Alt = (Rank:Alt) xor _Alt = Alt, Rank = 0),
	Alt = '$CLOSURE'(XAlt,_),
	il_debug('Interleave choose ~w ~x -> (~w) ~w : ~x\n',[Label,Left,Rank,XAlt,Right]),
	true
	.

interleave_choose_alt(Label,Left,Right,Alt,Rank) :-
	il_debug('Interleave choose ~w ~x\n',[Label,Left]),
	interleave_choose_x(Left,Right,_Alt),
	(_Alt = (Rank:Alt) xor _Alt = Alt, Rank = 0),
	Alt = '$CLOSURE'(XAlt,_),
	il_debug('Interleave choose ~w ~x -> (~w) ~w : ~x\n',[Label,Left,Rank,XAlt,Right]),
	true
	.

:-xcompiler((

interleave_choose_x(Left,Right,Alt) :-
	( Left = [_,Alt] ->	% Exit case
	    Right = []
	;   Left = [Label,Exit|Left2],
	    Right = [Label,Exit|Right2],
	    interleave_choose_aux(Left2,Right2,Alt)
	)
	
	))
	.

:-std_prolog interleave_choose_aux/3.

interleave_choose_aux(Left::[X|Left2],Right,Alt) :-
	( ( X = [_,Alt] ->
	      Right = Left2
	  ;   X = [LA,EX|LeftX] ->
	      Right = [[LA,EX|RightX]|Left2],
	      interleave_choose_aux(LeftX,RightX,Alt)
	  ;   
	      Alt = X,
	      Right = Left2
	  )
	;   
	    Right=[X|Right2],
	    interleave_choose_aux(Left2,Right2,Alt)
	)
	.


interleave_choose_first(Label,Left,Right,Alt,Rank,PrevRank) :-
	il_debug('Interleave choose first ~w ~x\n',[Label,Left]),
	interleave_choose_first_x(Left,Right,_Alt),
	(_Alt = (Rank:Alt) xor _Alt = Alt, Rank = 0),
	Rank < PrevRank,
	Alt = '$CLOSURE'(XAlt,_),
	il_debug('Interleave choose first ~w ~x -> (~w) ~w : ~x\n',[Label,Left,Rank,XAlt,Right]),
	true
	.

:-xcompiler((

interleave_choose_first_x(Left,Right,Alt) :-
	( Left = [_,Alt] ->	% Exit case
	    Right = []
	;   Left = [Label,Exit,Alt|Left2],
	    Right = [Label,Exit|Left2]
	)
	
	))
	.

