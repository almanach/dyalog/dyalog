/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 1998, 2002, 2008 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  format.pl -- Formatted Printer
 *
 * ----------------------------------------------------------------
 * Description
 * 
 * ----------------------------------------------------------------
 */

%% User hook: one can add its own formatter
%% format_hook(Char,Stream,Args,Args_Trail)
:-rec_prolog format_hook/4.
 
%% Public
:-std_prolog format/2,format/3,error/2,warning/2,sformat/3.
:-std_prolog optimized_format/1, optimized_format/2.

optimized_format( Args ) :-
	current_output(S), optimized_format(S,Args).

optimized_format( Stream, Args ) :-
	(   
	    optimized_format_find_terms(Args),
	    optimized_format_go(Stream,Args),
	    fail
	;
	    true
	)
	.

%% Private
:-std_prolog optimized_format_find_terms/1.

optimized_format_find_terms( T ) :-
	( T = (Args^_) ->
	    '$interface'( 'DyALog_Assign_Display_Info'(Args:term), [return(none)])
	;   T = (T1,T2),
	    optimized_format_find_terms( T1 ),
	    optimized_format_find_terms( T2 )
	)
	.   
	    
%% Private
:-std_prolog optimized_format_go/2.

optimized_format_go( Stream, T ) :-
	( T = (ArgsList^Format^Sep) ->
	    mutable(M,0,false),
	    domain(Args,ArgsList),
	    mutable_inc(M,V),
	    (	V==0 xor  write(Stream,Sep) ), 
	    format(Stream,Format,Args)

	;   T = (Args^Format) ->
	    format(Stream,Format,Args)
	;   T = (T1,T2),
	    (	optimized_format_go( Stream, T1 ) ;
		optimized_format_go( Stream, T2 )
	    )
	)
	.   

%% Private	
:-std_prolog special_dcg_format/4.

error(Msg,Args) :-
        write(2,'ERROR: ' ),
        format(2,Msg,Args),
	nl(2),
        exit(1)
	.

warning(Msg,Args) :-
        write(2,'WARNING: ' ),
        format(2,Msg,Args),
	nl(2)
	.

format( Format, Args ) :-
    current_output(S), format(S,Format,Args).

%% Construction de nouveau symbole � la sprintf
sformat(Format,Args,Name) :-
        string_stream(_,S),
        format(S,Format,Args),
        flush_string_stream(S,Name),
        close(S)
        .

format(Stream,Format,Args) :-
	(   '$interface'( 'DyALog_Format'(Stream:term,
					  Format:ptr,
					  0'~:char,
					 X: -char),
			  [return(Rest:ptr)] )
	->  
	    special_dcg_format(Stream,X,Args,R),
	    format(Stream,Rest,R)
	;   
	    true
	)
	.


special_dcg_format( S, X, Args,R ) :-
	( X == 0'w -> Args=[A|R],write(S,A)   
	%%      {X == 0'q} -> {Args=[A|R],write_c_atom(S,A)} ;
	;   X == 0'q -> Args=[A|R],writeq(S,A)
	;   X == 0'Q -> Args=[A|R],writei(S,A) 
	;   X == 0'k -> Args=[A|R],display(S,A) 
	;   X == 0'c -> Args=[A|R],char(A),put_char(S,A) 
	;   X == 0'i -> Args=[_|R] 
	;   X == 0'n -> Args=R,nl(S) 
	;   X == 0'l -> Args = [[Format,Sep],AA|R], 
	                mutable(M,0,false),
	               ( domain(A,AA),
			   mutable_inc(M,V),
			   (   V == 0 xor write(S,Sep) ),
			   format(S,Format,A),
			   fail
		       ;
			   true
		       )
	; X == 0'~ -> Args=R,put_char(S,0'~)
	;   format_hook(X,S,Args,R) -> true
	; error('format: not a valid escape option ~w',[X])
	)
	.



