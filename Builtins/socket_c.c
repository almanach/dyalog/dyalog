/*************************************************************
 * $Id$
 * Copyright (C) 2000, 2004, 2006, 2014 Eric de la Clergerie
 * ------------------------------------------------------------
 *
 *    socket -- support for sockets in DyALog
 *
 * ------------------------------------------------------------
 * Description
 *   C Compagnion File to socket.pl.
 *   Add support for sockets within DyALog
 *
 * ------------------------------------------------------------
 */

#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>

#include "hash.h"
#include "libdyalog.h"
#include "builtins.h"

#include <sys/types.h>
#include <sys/socket.h>
#ifdef HAVE_SYS_UN_H
#include <sys/un.h>
#endif
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>


/*-------------------------------------------------------------------------*/
/* SOCKET_1                                                                */
/*                                                                         */
/*-------------------------------------------------------------------------*/
int Socket_1( char *domain )
{
    int sock;
    
#ifdef HAVE_SYS_UN_H 
    if (!strcmp(domain ,"UNIX")) {
        sock=socket(AF_UNIX,SOCK_STREAM,0);
        goto end;
    }
#endif

    if (!strcmp(domain ,"INET")) {
        sock=socket(AF_INET,SOCK_STREAM,0);
        goto end;
    }

    Fatal_Error("Socket domain error %s",domain);
    
  end:

    if (sock==-1)
        System_Error(__FUNCTION__);
    
    return sock;
}

/*-------------------------------------------------------------------------*/
/* SOCKET_CLOSE_1                                                          */
/*                                                                         */
/*-------------------------------------------------------------------------*/

void Socket_Close_1( int sock )
{
 if (sock<2) {
     errno=EBADF;
     Fatal_Error("%s",__FUNCTION__);
 }

 if (close(sock))
     Fatal_Error("%s",__FUNCTION__);
 
}

/*-------------------------------------------------------------------------*/
/* SOCKET_BIND_UNIX_2                                                   */
/*                                                                         */
/*-------------------------------------------------------------------------*/
Bool Socket_Bind_Unix(int sock, char *pathname)
{

#ifdef HAVE_SYS_UN_H
    struct sockaddr_un  adr;
    adr.sun_family=AF_UNIX;

        /*
          if ((pathname=M_Absolute_Path_Name(pathname))==NULL)
          Fail;
        */
    strcpy(adr.sun_path,pathname);
    unlink(pathname);
    
    if (bind(sock,(struct sockaddr *) &adr,sizeof(adr))==-1)
        System_Error(__FUNCTION__);
    
    Succeed;
#else
    Fatal_Error("Socket domain error %s","UNIX");
#endif
}

/*-------------------------------------------------------------------------*/
/* SOCKET_CONNECT_UNIX_2                                                   */
/*                                                                         */
/*-------------------------------------------------------------------------*/
Bool Socket_Connect_Unix(int sock, char *pathname)
{
#ifdef HAVE_SYS_UN_H
    struct sockaddr_un  adr;
    adr.sun_family=AF_UNIX;

        /*
          if ((pathname=M_Absolute_Path_Name(pathname))==NULL)
          Fail;
        */
    strcpy(adr.sun_path,pathname);

    if (connect(sock,(struct sockaddr *) &adr,sizeof(adr)))
        System_Error(__FUNCTION__);
    
    Succeed;
#else
    Fatal_Error("Socket domain error %s","UNIX");
#endif
}

/*-------------------------------------------------------------------------*/
/* SOCKET_BIND_INET                                                        */
/*                                                                         */
/*-------------------------------------------------------------------------*/
Bool Socket_Bind_Inet( int sock, const char *hostname, int port, int *port_out)
{
    struct sockaddr_in  adr;

    adr.sin_family=AF_INET;
    adr.sin_port  =htons(port);
    adr.sin_addr.s_addr=INADDR_ANY;

    if ( (bind(sock,(struct sockaddr *) &adr,sizeof(adr))== -1) )
//        || (getsockname(sock,(struct sockaddr *) &adr,sizeof(adr))) )
        System_Error(__FUNCTION__);

    *port_out=ntohs(adr.sin_port);
    Succeed;
}

/*-------------------------------------------------------------------------*/
/* SOCKET_CONNECT_INET_3                                                   */
/*                                                                         */
/*-------------------------------------------------------------------------*/
Bool Socket_Connect_Inet( int sock, const char *hostname, int port)
{
    struct sockaddr_in  adr;
    struct hostent     *host_entry=gethostbyname(hostname);

    if (host_entry==NULL)
        Fail;

    adr.sin_family=AF_INET;
    adr.sin_port  =htons(port);
    memcpy(&adr.sin_addr,host_entry->h_addr_list[0],host_entry->h_length);

    if (connect(sock,(struct sockaddr *) &adr,sizeof(adr)))
        System_Error(__FUNCTION__);

    Succeed;
}

/*-------------------------------------------------------------------------*/
/* SOCKET_ACCEPT_4                                                         */
/*                                                                         */
/*-------------------------------------------------------------------------*/
Bool Socket_Accept( int sock, char **client_address, int *client_sock )
{
 socklen_t                 l;
 struct sockaddr_in  adr_in;

 *client_address="AF_UNIX";

 l=sizeof(adr_in);

 if ((*client_sock=accept(sock,(struct sockaddr *) &adr_in,&l))==-1)
     System_Error(__FUNCTION__);
 
 if (adr_in.sin_family==AF_INET) {
     if ((*client_address=inet_ntoa(adr_in.sin_addr))==NULL) {
         Fail;
     }
 }
 Succeed;
}

/*-------------------------------------------------------------------------*/
/* CREATE_SOCKET_STREAM                                                    */
/*                                                                         */
/*-------------------------------------------------------------------------*/

Bool Assoc_Socket_Stream(int sock,int *stm)
{
 StmProp  prop;
 FILE    *f;
 fol_t    file_name;
 char     stream_name[256];

// dyalog_printf("Assoc_Socket_Stream Socket=%d\n",sock);
 
 sprintf(stream_name,"socket_stream(%d)",sock);
 
 if ((f=fdopen(sock,"r+"))==NULL)
     System_Error(__FUNCTION__);

 file_name=Create_Allocate_Atom((char *)stream_name);

 prop.mode         =STREAM_MODE_READ|STREAM_MODE_WRITE;
 prop.input        =TRUE;
 prop.output       =TRUE;
 prop.text         =TRUE;
 prop.reposition   =FALSE;
 prop.eof_action   =STREAM_EOF_ACTION_RESET;
// prop.buffering    =STREAM_BUFFERING_BLOCK;
 prop.tty          =FALSE;
// prop.special_close=FALSE;
 prop.other        =4;

 *stm=Add_Stream(file_name,(long) f,prop,
                    NULL,NULL,NULL,NULL,NULL,NULL,NULL);
 Succeed;
}



