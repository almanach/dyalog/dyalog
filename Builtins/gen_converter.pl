/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2001, 2002 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  gen_converter.pl -- Generic converter
 *
 * ----------------------------------------------------------------
 * Description
 *  Module for performing generic conversions
 *  To use it, use the exported predicates
 *         gen_tranducer/0 : run the conversion
 *  and define predicate
 *         transformer/2 : your own transformation rules
 *         initialize/0  : your own initialization
 *         user_format/1 : your own formatter
 * ----------------------------------------------------------------
 */


:-require 'format.pl'.

%% The goal
%% ?-gen_transducer.

%% To be user defined
:-rec_prolog transformer/2.

%% To be user defined
:-rec_prolog user_format/1.

:-rec_prolog initialize/0.
:-rec_prolog gen_initialize/0.
:-prolog gen_transducer/0.

:-extensional verbose/0.

gen_initialize :-
	op(1100, fx, [ extensional,
		       hilog,
		       lco,
		       prolog,
		       rec_prolog,
		       std_prolog,
		       include,
		       require,
		       resource
		     ]),
	op( 550, xfy, [:>] ),
	op( 520, yfx, [<+] ),
	op( 550, xfy, [+>] )
        .   

gen_transducer :-
        gen_initialize,
	(initialize xor true),
        argv( Argv ),
        phrase( gen_parse_options, Argv, [] ),
        exit(0)
        .

:-prolog (dcg gen_parse_options/0, gen_parse_option/0).

gen_parse_options --> gen_parse_option, gen_parse_options ; [].
gen_parse_option -->
        ( ['-f',File] -> { record(in_file(File)), gen_reader(File) }
        ;   ['-o',File] -> { record( output_file(File) ) }
        ;   ['-I',Path] -> { add_load_path(Path) }
	;   ['-v'] -> { record( verbose ) }
        ;   [File] -> { record( in_file(File) ), gen_reader(File) }
        ;   { fail }
        )
        .

:-std_prolog gen_reader/1.

gen_reader( File ) :-
        (   find_file(File,AFile) xor error('File not found ~w',[File]) ),
        (   open(AFile,read,S) xor error('Could not open file ~w',[AFile] ) ),
        repeat(
               (   read_term(S,T,V),
                   gen_analyze(T,V),
                   T == eof
               )
              ),
        close(S)
        .

:-xcompiler
	assign_varname(X,Name) :-
		'$interface'( 'DyALog_Assign_Varname'(X:term,Name:string), []).

:-std_prolog assign_varnames/1.

assign_varnames(L) :-
	(   L=[] xor
	L= [X=Y|LL], (	assign_varname(Y,X) xor true), assign_varnames(LL)
	).

:-std_prolog gen_analyze/2.

gen_analyze(T,V) :-
        ( T == eof ->
            true
        ;
	    (\+ verbose xor format('%% ~k.\n',[T])),
	    gen_transform(T,Q),
	    assign_varnames(V),
	    (	user_format(Q) xor optimized_format([Q]^'~Q.\n'))
        )
        .   

:-std_prolog gen_transform/2, gen_transform_args/2.

gen_transform(T,Q) :-
	( var(T) ->  Q=T
	;   number(T) -> Q=T
	;   char(T) -> Q=T
	;   transformer(T,T2) -> gen_transform(T2,Q)
	;   atom(T) -> Q=T
	;   T =.. [F|Args],
	    gen_transform_args(Args,QArgs),
	    Q =.. [F|QArgs]
	)
	.

gen_transform_args(Args,QArgs) :-
	( Args = [] ->
	    QArgs = []
	;   Args = [T|Rest],
	    QArgs = [Q|QRest],
	    gen_transform(T,Q),
	    gen_transform_args(Rest,QRest)
	)
	.

