/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2000, 2004, 2006, 2008, 2009, 2011 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  tag_gneric.pl -- Generic Stuff for TAGs
 *
 * ----------------------------------------------------------------
 * Description
 * 
 * ----------------------------------------------------------------
 */

:-parse_mode(token).

:-features( tag_tree, [ family, name, tree ] ).
:-features( tag_anchor, [name, coanchors, equations ] ).

:-rec_prolog normalized_tag_lemma/5.
%:-rec_prolog anchor/6.
:-std_prolog anchor/7.
:-extensional tag_lexicon/4.

anchor(tag_anchor{ name => Family,
		   coanchors => VarCoanchors,
		   equations => VarEquations
		 },
       Token,
       Label,
       Left,
       Right,
       Top,
       _
      ) :-
	phrase([Token],Left,Right),
	tag_lexicon(Token,Lemma,Label,Top),
	normalized_tag_lemma(Lemma,Label,Family,VarCoanchors,VarEquations)
	.

:-std_prolog coanchor/5.

coanchor(Token,Label,Left,Right,Top) :-
	phrase([Token],Left,Right),
	tag_lexicon(Token,_,Label,Top).

:-std_prolog check_coanchor/3.

check_coanchor(VarLemma,Left,Right) :-
%%	format('check_coanchor ~w ~w ~w\n',[VarLemma,Left,Right]),
	( var(VarLemma) ->
	    VarLemma = check_at_anchor(Left,Right)
	;   
	    check_coanchor_lemma(VarLemma,Left,Right)
	)
	.

:-std_prolog check_coanchor_in_anchor/2.

check_coanchor_in_anchor(VarLemma,Lemma) :-
%%	format('check_anchor_in_anchor ~w ~w\n',[VarLemma,Lemma]),
	( var(VarLemma) ->
	    VarLemma = Lemma
	;   VarLemma = check_at_anchor(Left,Right),
	    check_coanchor_lemma(Lemma,Left,Right)
	)
	.

:-std_prolog check_coanchor_lemma/3.

check_coanchor_lemma(Lemma,Left,Right) :-
	phrase([Token],Left,Right),
%%	format('check_coanchor_lemma ~w ~w ~w -> ~w\n',[Left,Right,Lemma,Token]),
	domain(Token,Lemma),
%%	format('Succeed coanchor lemma\n',[]),
	true
	.


:-std_prolog check_lexical/3.

check_lexical(Token,Left,Right) :-
	phrase( [ Token ], Left, Right )
	.

verbose!struct(_Tree_Name,_HyperTag).

%% The following is now deprecated
%%verbose!struct(_Tree_Name).

%% Dummy predicate used to leave a trace in the forest
verbose!anchor(_Anchor,_Left,_Right,_Tree_Name,_Cat,_Anchor,_Info).

%% Dummy predicate used to leave a trace in the forest
verbose!coanchor(_Coanchor,_Left,_Right,_Cat,_Coanchor).

%% Dummy predicate used to leave a trace in the forest
verbose!lexical([_Lexical|_],_Left,_Right,lex,_Lexical).

%% Dummy predicate used to leave a trace in the forest
verbose!adj.


:-std_prolog tag_family_load/7.

tag_family_load(Family,Cat,Top,Token,Name,Left,Right) :- true.

:-std_prolog tag_check_coanchor/6.

tag_check_coanchor(Label,Top,Token,Left,Right,Tree) :- true.

	
