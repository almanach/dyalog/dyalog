/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2000 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  socket.pl -- simple interface to sockets (client side)
 *
 * ----------------------------------------------------------------
 * Description
 * 
 * ----------------------------------------------------------------
 */

:-require 'system.pl'.

:-std_prolog '_socket_address'/3.

'_socket_address'(Address,Norm_Address,Domain) :-
	( Address = (Hostname : Port) ->
	    Norm_Address = 'INET'(Hostname,Port),
	    Domain = 'INET'
	; atom(Address) ->
	    Norm_Address = 'UNIX'(Address)
	; Norm_Address = Address, functor(Address,Domain,_)
	)
	.

%% Generic Part (for servers and clients)

:-std_prolog socket_create/2, socket_close/1.

socket_create(Domain,Socket) :- '$interface'( 'Socket_1'(Domain: string), [return(Socket: int)] ).
socket_close(Socket) :- '$interface'( 'Socket_Close_1'(Socket: int), [return(none)] ).

%% Client Part

:-std_prolog socket_connect/3.

socket_connect(Socket,Address,Stream) :-
	( Address = 'UNIX'(Name) ->
	    '$interface'( 'Socket_Connect_Unix'(Socket: int,Name: string), [] )
	;   Address = 'INET'(Host,Port),
	    '$interface'( 'Socket_Connect_Inet'(Socket: int,Host: string,Port: int), [] )
	),
	'$interface'( 'Assoc_Socket_Stream'(Socket: int, Stream: -int), [] )
	.

:-std_prolog socket_client/3.

socket_client(Address,Socket,Stream) :-
	'_socket_address'(Address,Norm,Domain),
	socket_create(Domain,Socket),
	socket_connect(Socket,Norm,Stream)
	.

%% Server Part

:-std_prolog socket_bind/2.

socket_bind(Socket,Address) :-
	( Address = 'UNIX'(Name) ->
	    '$interface'( 'Socket_Bind_Unix'(Socket: int, Name: string), [] )
	;   Address = 'INET'(Host,Port),
	    ( var(Port) -> In_Port = 0 ; In_Port = Port ),
	    '$interface'( 'Socket_Bind_Inet'(Socket: int,
					     Host: string,
					     In_Port: int,
					     Out_port: -int),
			  [] )
	)
	.

:-std_prolog socket_listen/2.

socket_listen(Socket,QueueLength) :-
	'$interface'( 'listen'(Socket:int,QueueLength:int), [return(Err:int)]),
	( Err == 0 xor system_error('socket_listen/2'))
	.

:-std_prolog socket_server/3.

socket_server(Address,Socket,QueueLength) :-
	'_socket_address'(Address,Norm,Domain),
	socket_create(Domain,Socket),
	socket_bind(Socket,Norm),
	socket_listen(Socket,QueueLength)
	.

:-std_prolog socket_accept/2.

:-features( socket_client, [name,socket,stream] ).

socket_accept(Socket,
	      socket_client{ name=> Client_Name,
			     socket => Client_Socket,
			     stream => Stream
			   }
	     ) :-
	'$interface'( 'Socket_Accept'(Socket:int, Client_Name: -string, Client_Socket: -int) , [] ),
	'$interface'( 'Assoc_Socket_Stream'(Client_Socket: int, Stream: -int), [] )
	.


