/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2005 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  debug.pl -- DyALog debugging predicates
 *
 * ----------------------------------------------------------------
 * Description
 *  
 * ----------------------------------------------------------------
 */

%% ----------------------------------------------------------------------
%% Module Interface

:-module(debug).

:-import{ file=>'format.pl', preds => [format/2] }.

:-extensional debug_trace_level/2.

:-std_prolog set_trace_level/1.
:-std_prolog analyze_trace_exp/2.

:-end_require.


%%----------------------------------------------------------------------
%% Module Implementation
%%
%% This part is not read when importing/including this file


%% Trace levels (defined in Runtime/param.g=h)
debug_trace_level(1,dyam).
debug_trace_level(2,index).
debug_trace_level(4,share).
debug_trace_level(8,term).
debug_trace_level(16,tfs).
debug_trace_level(32,builtins).
debug_trace_level(64,low).

set_trace_level(Exp) :-
	analyze_trace_exp(Exp,N),
	format('Setting trace level ~w => ~w\n',[Exp,N]),
	'$interface'( set_verbose_level(N:int), [])
	.

analyze_trace_exp(Exp,N) :-
	( number(Exp) -> N = Exp
	;   atomic(Exp) ->
	    debug_trace_level(N,Exp) xor N = 0
	;   Exp = Exp1 + Exp2 ->
	    analyze_trace_exp(Exp1,N1),
	    analyze_trace_exp(Exp2,N2),
	    N is N1 \/ N2
	)
	.

