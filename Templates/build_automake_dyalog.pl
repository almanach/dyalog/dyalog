#!/usr/bin/perl

## Try to build automake-dyalog using some found automake

use strict;
use File::Basename;

my $suff = shift || "";

my $xsuff = $suff ? "-$suff"  : "";

my $automake;

if ($suff) {
  $automake = check_automake($suff);
} else {
  foreach my $v ('', '1.16','1.15','1.14','1.13','1.12','1.11','1.10','1.9') {
    $automake = check_automake($v);
    last if ($automake);
  }
}

sub check_automake {
  my $v = shift;
  my $vv = $v ? "-$v" : "";
  my $automake = `which automake$vv`;
  chomp $automake;
  return unless ($automake);
  my $info = `$automake --version`;
  my ($version) = $info =~ /^automake\s.*\s(1.\d+)/;
  my @versions = $v ? ($v) : qw/1.9 1.10 1.11 1.12 1.13 1.14 1.15/;
  return unless (grep {$version eq $_} @versions);
  return $automake;
}

emit_void_automake() unless (defined $automake);


my $fullname;


open(AM,"<$automake") || die "can't open $automake";
while(<AM>) {
  my $line = $_;
  if ($line =~ /\$_bindir\s*=\s*File::Basename::dirname\(\$0\)/){
    $line =~ s/\$0/'$automake'/;
  };
  if (!$fullname && $line =~ /(automake-1.\d+)/) {
    $fullname = $1;
  }
  if ($line =~ /^use\s+Automake::Config;/) {
    print $line;
    my $base = File::Basename::dirname($automake);
    my $prefix = File::Basename::dirname($base);
    ($prefix eq '/') and $prefix = '/usr/'; # tmp hack when base=/bin/
    ## actually unclear we need to add the following line for recent automake
    print <<EOF;
\$libdir = "$prefix/share/$fullname";
EOF
    next;
  }
  if (($line =~ /'extensions'/) && ($line =~ /\.java/)) {
    print $_;
    print <<EOF;

# DyALog
register_language( 'name' => 'dyalog',
                  'Name' => 'DyALog',
                  'config_vars' => ['DYACC'],
                  'ld' => '\$(DYACC)',
                  'lder' =>'DYACCLD',
                  'linker' => 'DYACCLINK',
                  'link' => '\$(DYACCLD) \${AM_DFLAGS} \${DFLAGS} \${AM_LDFLAGS} \${LDFLAGS} -o \$\@',
                  'flags' => ['DFLAGS','DINCLUDE'],
                  'compile' => '\$(DYACC) \$(DINCLUDES) \$(AM_DFLAGS) \$(DFLAGS)',
                  'compiler' => 'DYACCOMP',
                  'compile_flag' => '-c',
                  'output_flag' => '-o',
                  'pure' => 1,
                  'extensions' => ['.pl','.tag','.rcg','.dcg'],
                  'libtool_tag' => 'CC'
                  );

EOF
    next;
  }
  if ($line =~ /\#\s+Rewrite\s+a\s+single\s+Java\s+file/) {
    print <<EOF;

# Rewrite a single DyALog file.
sub lang_dyalog_rewrite
{
    return &lang_sub_obj;
}

EOF
    print $_;
    next;
  }
  if ($line =~ /GCJLINK\s+CXXLINK\s+F77LINK\s+FCLINK\s+OBJCLINK/) {
    $line =~ s/\)\)/ DYACCLINK))/o;
    print $line;
    next;
  }
  print $line;
}

close(AM);

sub emit_void_automake {
  print <<EOF;
#!/usr/bin/perl

print "Sorry ! This is only a void automake-dyalog. A proper automake$xsuff has to be installed or found on this machine, in order to build automake-dyalog.\\n";

exit 1;

EOF
  exit;
}
