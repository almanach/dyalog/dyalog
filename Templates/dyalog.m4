# Configure paths for DyALg
# Eric de la Clergerie 2002-10-01
# Adapted form LIBXML
# Toshio Kuratomi 2001-04-21
# Adapted from:
# Configure paths for GLIB
# Owen Taylor     97-11-3

dnl AC_PATH_DYALOG([MINIMUM-VERSION, [ACTION-IF-FOUND [, ACTION-IF-NOT-FOUND]]])
dnl Test for DYALOG, and define DYALOG_CFLAGS, DYALOG_LIBS, DYALOG_DFLAGS, DYACC
dnl
AC_DEFUN([AC_PATH_DYALOG],[ 
AC_ARG_WITH(dyalog-prefix,
            [  --with-dyalog-prefix=PFX   Prefix where DyALog is installed (optional)],
            dyalog_config_prefix="$withval", dyalog_config_prefix="")
AC_ARG_WITH(dyalog-exec-prefix,
            [  --with-dyalog-exec-prefix=PFX Exec prefix where DyALog is installed (optional)],
            dyalog_config_exec_prefix="$withval", dyalog_config_exec_prefix="")
AC_ARG_ENABLE(dyalogtest,
              [  --disable-dyalogtest       Do not try to compile and run a test DyALog program],,
              enable_dyalogtest=yes)

  if test x$dyalog_config_exec_prefix != x ; then
     dyalog_config_args="$dyalog_config_args --exec-prefix=$dyalog_config_exec_prefix"
     dyalog_configure="$dyalog_configure --with-dyalog-exec-prefix=$dyalog_config_exec_prefix"
     if test x${DYALOG_CONFIG+set} != xset ; then
        DYALOG_CONFIG=$dyalog_config_exec_prefix/dyalog-config
     fi
  fi
  if test x$dyalog_config_prefix != x ; then
     dyalog_config_args="$dyalog_config_args --prefix=$dyalog_config_prefix"
     dyalog_configure="$dyalog_configure --with-dyalog-prefix=$dyalog_config_prefix"
     if test x${DYALOG_CONFIG+set} != xset ; then
        DYALOG_CONFIG=$dyalog_config_prefix/dyalog-config
     fi
  fi

  if test x$enable_dyalogtest != x ; then
     dyalog_configure="$dyalog_configure --disable-dyalogtest";
  fi

  AC_PATH_PROG(DYALOG_CONFIG, dyalog-config, no)
  min_dyalog_version=ifelse([$1], ,1.10.1,[$1])
  AC_MSG_CHECKING(for DyALog - version >= $min_dyalog_version)
  no_dyalog=""
  if test "$DYALOG_CONFIG" = "no" ; then
    no_dyalog=yes
  else
    DYALOG_CFLAGS=`$DYALOG_CONFIG $dyalog_config_args --cflags`
    DYALOG_DFLAGS=`$DYALOG_CONFIG $dyalog_config_args --dflags`
    DYALOG_LIBS=`$DYALOG_CONFIG $dyalog_config_args --libs`
    DYACC=`$DYALOG_CONFIG $dyalog_config_args --cc`
    dyalog_config_major_version=`$DYALOG_CONFIG $dyalog_config_args --version | \
           sed 's/\([[0-9]]*\).\([[0-9]]*\).\([[0-9]]*\)/\1/'`
    dyalog_config_minor_version=`$DYALOG_CONFIG $dyalog_config_args --version | \
           sed 's/\([[0-9]]*\).\([[0-9]]*\).\([[0-9]]*\)/\2/'`
    dyalog_config_micro_version=`$DYALOG_CONFIG $dyalog_config_args --version | \
           sed 's/\([[0-9]]*\).\([[0-9]]*\).\([[0-9]]*\)/\3/'`
    if test "x$enable_dyalogtest" = "xyes" ; then
      ac_save_CFLAGS="$CFLAGS"
      ac_save_LIBS="$LIBS"
      ac_save_CC="$CC"
dnl      CFLAGS="$DYALOG_DFLAGS"
dnl      LIBS="$DYALOG_LIBS $LIBS"
      CC=`$DYALOG_CONFIG $dyalog_config_args --cc`
dnl
dnl Now check if the installed DyALog is sufficiently new.
dnl (Also sanity checks the results of dyalog-config to some extent)
dnl
      rm -f conf.dyalogtest
      AC_TRY_RUN([
#include <stdlib.h>
#include <stdio.h>
#include <libdyalog.h>

int main_directive_initialization()
{
}

int
main_initialization() /* libdyalog already defines main */
{
  int dyalog_major_version, dyalog_minor_version, dyalog_micro_version;
  int major, minor, micro;
  char *tmp_version;
  char *header_version;

  system("touch conf.dyalogtest");

  /* Capture dyalog-config output via autoconf/configure variables */
  /* HP/UX 9 (%@#!) writes to sscanf strings */
  tmp_version = (char *)strdup("$min_dyalog_version");
  if (sscanf(tmp_version, "%d.%d.%d", &major, &minor, &micro) != 3) {
     printf("%s, bad version string from dyalog-config\n", "$min_dyalog_version");
     exit(1);
   }
   free(tmp_version);

   /* Capture the version information from the header files */
  if (sscanf(PACKAGE_VERSION, "%d.%d.%d", &dyalog_major_version, &dyalog_minor_version, &dyalog_micro_version) != 3) {
     printf("%s, bad version string from DyALog header file\n", PACKAGE_VERSION);
     exit(1);
   }

 /* Compare dyalog-config output to the DyALog headers */
  if ((dyalog_major_version != $dyalog_config_major_version) ||
      (dyalog_minor_version != $dyalog_config_minor_version) ||
      (dyalog_micro_version != $dyalog_config_micro_version))
    {
      printf("*** DyALog header files (version %d.%d.%d) do not match\n",
         dyalog_major_version, dyalog_minor_version, dyalog_micro_version);
      printf("*** dyalog-config (version %d.%d.%d)\n",
         $dyalog_config_major_version, $dyalog_config_minor_version, $dyalog_config_micro_version);
      return 1;
    } 
  /* Compare the headers to the library to make sure we match */
  /* Less than ideal -- doesn't provide us with return value feedback, 
   * only exits if there's a serious mismatch between header and library.
   */

    /* Test that the library is greater than our minimum version */
    if (($dyalog_config_major_version > major) ||
        (($dyalog_config_major_version == major) && ($dyalog_config_minor_version > minor)) ||
        (($dyalog_config_major_version == major) && ($dyalog_config_minor_version == minor) &&
        ($dyalog_config_micro_version >= micro)))
      {
        return 0;
       }
     else
      {
        printf("\n*** An old version of DyALog (%d.%d.%d) was found.\n",
               dyalog_major_version, dyalog_minor_version, dyalog_micro_version);
        printf("*** You need a version of DyALog newer than %d.%d.%d. The latest version of\n",
           major, minor, micro);
        printf("*** DyALog is always available from http://alpage.inria.fr/~clerger.\n");
        printf("***\n");
        printf("*** If you have already installed a sufficiently new version, this error\n");
        printf("*** probably means that the wrong copy of the dyalog-config shell script is\n");
        printf("*** being found. The easiest way to fix this is to remove the old version\n");
        printf("*** of DyALog, but you can also set the DYALOG_CONFIG environment to point to the\n");
        printf("*** correct copy of dyalog-config. (In this case, you will have to\n");
        printf("*** modify your LD_LIBRARY_PATH enviroment variable, or edit /etc/ld.so.conf\n");
        printf("*** so that the correct libraries are found at run-time))\n");
    }
  return 1;
}
],, no_dyalog=yes,[echo $ac_n "cross compiling; assumed OK... $ac_c"])
       CFLAGS="$ac_save_CFLAGS"
       LIBS="$ac_save_LIBS"
       CC="$ac_save_CC"
     fi
  fi

  if test "x$no_dyalog" = x ; then
     AC_MSG_RESULT(yes (version $dyalog_config_major_version.$dyalog_config_minor_version.$dyalog_config_micro_version))
     ifelse([$2], , :, [$2])     
  else
     AC_MSG_RESULT(no)
     if test "$DYALOG_CONFIG" = "no" ; then
       echo "*** The dyalog-config script installed by DYALOG could not be found"
       echo "*** If DyALog was installed in PREFIX, make sure PREFIX/bin is in"
       echo "*** your path, or set the DYALOG_CONFIG environment variable to the"
       echo "*** full path to dyalog-config."
     else
       if test -f conf.dyalogtest ; then
        :
       else
          echo "*** Could not run DyALog test program, checking why..."
dnl          CFLAGS="$CFLAGS $DYALOG_CFLAGS"
dnl          LIBS="$LIBS $DYALOG_LIBS"
          AC_TRY_LINK([
#include <libdyalog.h>
#include <stdio.h>
],      [ return 0;],
        [ echo "*** The test program compiled, but did not run. This usually means"
          echo "*** that the run-time linker is not finding DyALog libraries or finding the wrong"
          echo "*** version of DyALog libraries. Maybe you need to set your"
          echo "*** LD_LIBRARY_PATH environment variable, or edit /etc/ld.so.conf to point"
          echo "*** to the installed location  Also, make sure you have run ldconfig if that"
          echo "*** is required on your system"
          echo "***"
          echo "*** If you have an old version installed, it is best to remove it, although"
          echo "*** you may also be able to get things to work by modifying LD_LIBRARY_PATH" ],
        [ echo "*** The test program failed to compile or link. See the file config.log for the"
          echo "*** exact error that occured. This usually means DyALog was incorrectly installed"
          echo "*** or that you have moved DyALog since it was installed. In the latter case, you"
          echo "*** may want to edit the dyalog-config script: $DYALOG_CONFIG" ])
          CFLAGS="$ac_save_CFLAGS"
          LIBS="$ac_save_LIBS"
       fi
     fi

     DYALOG_CFLAGS=""
     DYALOG_LIBS=""
     ifelse([$3], , :, [$3])
  fi

  dnl Set up compilation variables

  AC_SUBST(DYALOG_CFLAGS)
  AC_SUBST(DYALOG_DFLAGS)
  AC_SUBST(DYALOG_LIBS)
  AC_SUBST(DYACC)
  AC_SUBST(dyalog_configure)

  dnl Set up installation directories

  dnl   dyalogdir -- where to install dyalog headers and libraries.
  AC_CACHE_CHECK([for DyALog installation directory],
    [am_cv_dyalog_dyalogdir],
    [am_cv_dyalog_dyalogdir=`$DYALOG_CONFIG --prefix`/lib/DyALog])
  AC_SUBST([dyalogdir], [$am_cv_dyalog_dyalogdir])

  dnl pkgdyalogdir -- $PACKAGE directory under dyalogdir.	
  AC_SUBST([pkgdyalogdir], [\${dyalogdir}/$PACKAGE])

  rm -f conf.dyalogtest
])


dnl DYALOG_PKG_CHECK_MODULES(GSTUFF, gtk+-2.0 >= 1.3 glib = 1.3.4, action-if, action-not)
dnl defines GSTUFF_LIBS, GSTUFF_CFLAGS, see pkg-config man page
dnl also defines GSTUFF_PKG_ERRORS on error
AC_DEFUN([DYALOG_PKG_CHECK_MODULES], [
  succeeded=no

  if test -z "$DYALOG_PKG_CONFIG"; then
    AC_PATH_PROG(DYALOG_PKG_CONFIG, dyalog-config, no)
  fi

  if test "$DYALOG_PKG_CONFIG" = "no" ; then
     echo "*** The dyalog-config script could not be found. Make sure it is"
     echo "*** in your path"
  else
     AC_MSG_CHECKING(for $2)

     if $DYALOG_PKG_CONFIG --pkg="$2"; then
            AC_MSG_RESULT(yes)
            succeeded=yes

            AC_MSG_CHECKING($1_CFLAGS)
            $1_CFLAGS=`$DYALOG_PKG_CONFIG --pkg="$2" --cflags`
            AC_MSG_RESULT($$1_CFLAGS)

            AC_MSG_CHECKING($1_DFLAGS)
            $1_DFLAGS=`$DYALOG_PKG_CONFIG --pkg="$2" --dflags`
            AC_MSG_RESULT($$1_DFLAGS)

            AC_MSG_CHECKING($1_LIBS)
            $1_LIBS=`$DYALOG_PKG_CONFIG --pkg="$2" --libs`
            AC_MSG_RESULT($$1_LIBS)
     else
            $1_CFLAGS=""
            $1_LIBS=""
	    $1_DFLAGS=""
     fi

     AC_SUBST($1_CFLAGS)
     AC_SUBST($1_DFLAGS)
     AC_SUBST($1_LIBS)
  fi

  if test $succeeded = yes; then
     ifelse([$3], , :, [$3])
  else
     ifelse([$4], , AC_MSG_ERROR([Library requirements ($2) not met; consider adjusting the PKG_CONFIG_PATH environment variable if your libraries are in a nonstandard prefix so pkg-config can find them.]), [$4])
  fi
])


