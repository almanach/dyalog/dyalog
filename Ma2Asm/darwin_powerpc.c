#include <string.h>

/*---------------------------------*/
/* Constants                       */
/*---------------------------------*/

#define STRING_PREFIX              "LC"

#define MAX_C_ARGS_IN_C_CODE       32

#define UN "_"

#define C_ARGS_TOP                    8+MAX_C_ARGS_IN_C_CODE*4

#define R_DATA 1
#define R_FUN  2

/*---------------------------------*/
/* Global Variables                */
/*---------------------------------*/

extern int kind_of_call;        /* cf parser.y */

char  asm_reg_bank[64];
char  asm_reg_e[64];
char  asm_reg_b[64];
char  asm_reg_cp[64];
char  asm_reg_p[64];

#define REG_VAR   0
#define REG_UP    1
#define REG_DOWN  2
#define REG_FLOAT 4

#define REG_KIND(_kind)    (_kind & 3)
#define REG_FLOATP(_kind)  (_kind & 4)

#ifdef M_powerpc_linux
// #error "Argh on est encore en mode powerpc_linux !!!"
// NB : si nom du registre = x au lieu de rx c'est qu'on est en mode
// Popwerpc linux au lieu de darwin, donc run autoheader AVANT autoconf

#define UN
#define R(reg)                     #reg
#define F(reg)                     #reg
#define CR(reg)                    #reg
#define HI(adr)                    #adr "@ha"
#define HI_UN(adr)                 UN #adr "@ha"
#define LO(adr)                    #adr "@l"
#define LO_UN(adr)                 UN #adr "@l"

#else

#define UN                         "_"
#define R(reg)                     "r" #reg
#define F(reg)                     "f" #reg
#define CR(reg)                    "cr" #reg
#define HI(adr)                    "ha16(" #adr ")"
#define HI_UN(adr)                 "ha16(" UN #adr ")"
#define LO(adr)                    "lo16(" #adr ")"
#define LO_UN(adr)                 "lo16(" UN #adr ")"

#endif



#define LITTLE_INT(int_val)     ((unsigned) ((int_val) + 32768) < 65536)

#define IHI(x)                  ((unsigned) ((unsigned long) x >> 16))
#define ILO(x)                  ((unsigned) ((unsigned long) x & 0xFFFF))



struct reg {
    char name;
    char asm_reg[64];
    int  base;
    int  mult;
    int  kind;                  /* 0: variable 1: growing 2: not growing */
}   registers[4];

int   w_label=0;

static int offset;

/* variables for ma_parser.c */

int strings_need_null=0;


/* variables for ma2asm.c */

int call_c_reverse_args=0;


// On garde temporairement le hack du stub tiré de gprolog



#ifdef M_powerpc_darwin
#define STUB_PREFIX		   "L"
#define STUB_POSTFIX		   "$stub"
#define LAZYSTUB_PREFIX		   "L0$"
#define LAZYSTUB_POSTFIX	   "$lazy_ptr"
#define MAX_STUBNAMES		   1024*10

#define STAB_LAZY_PTR          1
//#undef STAB_LAZY_PTR

static char EXTERNAL_SYMBOL_strdup[] = "strdup";

#endif


#ifdef M_powerpc_darwin
static int stubcounter;
static char *stubnames[MAX_STUBNAMES];
#endif

char current_label[255]; // pour générer le déplacement de trail et de folvar_tab

#ifdef M_powerpc_linux

#define STACK_OFFSET(offset)   offset * 4 - 24
#define DBL_RET_WORDS          0

#else

#define STACK_OFFSET(offset)   offset * 4 + 24
#define DBL_RET_WORDS          2

#endif

// was 8 (3+8 =11, donc passage de valeur de r3 ‡ r11 ensuite par la pile
#define MAX_ARGS_IN_REGS 8 

#define BEFORE_ARG				\
{						\
char r[4];					\
if (offset < MAX_ARGS_IN_REGS)		\
sprintf(r, R(%d), offset + 3);		\
else						\
strcpy(r, R(11));




#define AFTER_ARG							\
if (offset >= MAX_ARGS_IN_REGS)					\
Inst_Printf("stw", "%s,%d(" R(1) ")", r, STACK_OFFSET(offset));	\
}


#ifdef M_powerpc_linux

#define AFTER_ARG_DBL						\
}

#else

#define AFTER_ARG_DBL						\
if (offset >= MAX_ARGS_IN_REGS)				\
Inst_Printf("stfd", F(%d) ",%d(" R(1) ")", dbl_reg_no,	\
offset * 4 + 24);				\
}

#endif




/*----------------------------------------------------------------------
 * Handling Relocatable
 *
 *----------------------------------------------------------------------
 */

symbol_t refer_symbol(char * label,long prop);
symbol_t define_symbol(char *label,long prop,ulong size,InitLongList_t init );

static symbol_t smb_trail;
static symbol_t smb_folvar_tab;
static symbol_t smb_follow_choice;

unsigned long rba=0; /* to ensure that reloc_base has been assigned
 * to ebx */
char rbalab[17];
char xrba[22];
unsigned long rta;      /* to ensure that trail base has been assigned
 * to esi */
extern void *symbol_tbl;

static char *
Symbol_R_Data(symbol_t smb)
{
    if (smb->rlabel_data) {
        return smb->rlabel_data;
    } else {
        char *label = smb->label;
        char *rlabel= (char *)malloc(16+strlen(label));
        if (smb->prop & Smb_Defined) {
            sprintf(rlabel,UN "%s",label);
        } else {
            sprintf(rlabel,"L_%s$non_lazy_ptr",label);
        }
        smb->rlabel_data = rlabel;
        smb->prop |= Smb_Long;
        return rlabel;
    }
}

static char *
Symbol_R_Fun(symbol_t smb)
{
    if (smb->rlabel_fun) {
        return smb->rlabel_fun;
    } else {
        char *label = smb->label;
        char *rlabel= (char *)malloc(8+strlen(label));
        if (smb->prop & Smb_Defined) {
            sprintf(rlabel,UN "%s",label);
        } else {
            sprintf(rlabel,"L_%s$stub",label);
        }
        smb->rlabel_fun = rlabel;
        smb->prop |= Smb_C;
        return rlabel;
    }
}

static char *
Symbol_R_Prolog(symbol_t smb)
{
    if (smb->rlabel_fun) {
        return smb->rlabel_fun;
    } else {
        char *label = smb->label;
        char *rlabel= (char *)malloc(8+strlen(label));
        if (smb->prop & Smb_Defined) {
            sprintf(rlabel,UN "%s",label);
        } else {
            sprintf(rlabel,"L_%s$stub",label);
        }
        smb->rlabel_fun = rlabel;
        smb->prop |= Smb_Prolog;
        return rlabel;
    }
}


static
void emit_assign_reloc_base ()
{
    if (pic) {
        Inst_Printf("call","___i686.get_pc_thunk.bx");
        rba++;
        sprintf(rbalab,"\"L%010d$pb\"",(int)rba);
        sprintf(xrba,"%s(%%ebx)",rbalab);
        Label_Printf("%s:",rbalab);
        if (profile) {
            Inst_Printf("call","Lmcount$stub");
        }
    }
}

static
void emit_assign_trail () 
{
	/* assign esi to trail, assume reloc base assigned */
	Inst_Printf("movl","%s-%s,%%esi", Symbol_R_Data(smb_trail),xrba);
}


static
void reloc_symbol_action(const void *nodep,
                         const VISIT which,
                         const int depth
                         ) 
{
    symbol_t info = *((symbol_t *)nodep);
    ulong prop = info->prop;
    switch (which) {
        case postorder:
        case leaf:
            if (prop & Smb_Defined)
                break;
            if (prop & Smb_Long) {
                Inst_Printf(".section","__IMPORT,__pointers,non_lazy_symbol_pointers");
                Label_Printf("%s:", Symbol_R_Data(info));
                Inst_Printf(".indirect_symbol",UN "%s",info->label);
                Inst_Printf(".long","0");
            }
            if (prop & (Smb_Prolog | Smb_C)) {
                Inst_Printf(".section","__IMPORT,__jump_table,symbol_stubs,self_modifying_code+pure_instructions,5");
                Label_Printf("%s:", Symbol_R_Fun(info));
                Inst_Printf(".indirect_symbol",UN "%s",info->label);
                Inst_Printf("hlt ; hlt; hlt; hlt; hlt", "");
            }
            break;
			default:
            break;
    }
}

static
void Relocatable_Symbols (void)
{
    twalk(symbol_tbl,reloc_symbol_action);
}

/*-------------------------------------------------------------------------*/
/* STAB_LINE                                                               */
/*                                                                         */
/*-------------------------------------------------------------------------*/

static struct
{
    char *label;
    int  global;
    long  stab_label;
} fun_info = { 0, 0, 0 };

extern long yylineno;

#define curline (yylineno-5)

static long stab_label=0;

void Stab_Line()
{
    Inst_Printf(".stabn","68,0,%u,.LL%d-%s",curline,stab_label,fun_info.label);
    Label_Printf(".LL%u:",stab_label++);
}


/*-------------------------------------------------------------------------*/
/* STAB_FUNCTION                                                           */
/*                                                                         */
/*-------------------------------------------------------------------------*/

void Stab_Function(char *label,int global)
{
	
    Label_Printf("");
    Inst_Printf(".stabs","\"%s:%c1\",36,0,%u,%s",
                label, global ? 'F' : 'f', curline, label
                );
    fun_info.label=label;
    fun_info.global=global;
    fun_info.stab_label=stab_label;
}

/*-------------------------------------------------------------------------*/
/* SOURCE_LINE                                                             */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Source_Line(int line_no,char *cmt)
{
	Label_Printf("\t# %6d: %s",line_no,cmt);
}

/*-------------------------------------------------------------------------*/
/* FIND_REGISTER_INFO                                                               */
/*                                                                         */
/*-------------------------------------------------------------------------*/
static
struct reg * Find_Register_Info( char name )
{
    struct reg *reg;
    for(reg=registers; reg->name != name ; reg++);
    return reg;
}

extern char *file_name_in, *file_name_out;

/*-------------------------------------------------------------------------*/
/* ASM_START                                                               */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Asm_Start(void) {
    char *trail = "%esi";       /* TBM */
    smb_trail = refer_symbol("trail",Smb_Long);
    smb_folvar_tab = refer_symbol("folvar_tab",Smb_Long);
    smb_follow_choice = refer_symbol("follow_choice",Smb_C);
    sprintf(asm_reg_e,"%d(%s)",I_E*4,trail);
    sprintf(asm_reg_b,"%d(%s)",I_B*4,trail);
    sprintf(asm_reg_cp,"%d(%s)",I_CP*4,trail);
    sprintf(asm_reg_p,"%d(%s)",I_P*4,trail);
	
    registers[0].name = 'Y';
    strcpy(registers[0].asm_reg,asm_reg_e);
    registers[0].base=4;
    registers[0].mult=4;
    registers[0].kind=REG_DOWN;
    
    registers[1].name = 'R';
    strcpy(registers[1].asm_reg,trail);
    registers[1].base=NB_SYSTEM_REGISTERS;
    registers[1].mult=4;
    registers[1].kind=REG_VAR;
	
    registers[2].name = 'S';
    strcpy(registers[2].asm_reg,trail);
    registers[2].base=0;
    registers[2].mult=4;
    registers[2].kind=REG_VAR;
	
    registers[3].name = 'T';
    strcpy(registers[3].asm_reg,trail);
    registers[3].base=NB_SYSTEM_REGISTERS;
    registers[3].mult=4;
    registers[3].kind=REG_VAR | REG_FLOAT;
   
	
	// file_name_out au lieu de file_name_in, fichier assembleur doit contenir
	// le nom .s pas le .ma (aide au déboggage avec gdb)
    Inst_Printf(".stabs","\"%s\",100,0,0,Ltext0",file_name_out);
    Label_Printf(".text");
    Label_Printf("Ltext0:");
    Inst_Printf(".stabs","\"void:t1=15\",128,0,0,0");
	
    Inst_Label("fail");
    Inst_Pl_Fail();
	
}


/*-------------------------------------------------------------------------*/
/* ASM_STOP                                                                */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Asm_Stop(void)
{
    Relocatable_Symbols();
    if (profile) {
        Inst_Printf(".section","__IMPORT,__jump_table,symbol_stubs,self_modifying_code+pure_instructions,5");
        Label_Printf("%s:", "Lmcount$stub");
        Inst_Printf(".indirect_symbol","%s","mcount");
        Inst_Printf("hlt ; hlt; hlt; hlt; hlt", "");
    }
    Inst_Printf(".subsections_via_symbols","");
    if (pic) {
        Inst_Printf(".section","__TEXT,__textcoal_nt,coalesced,pure_instructions");
        Inst_Printf(".weak_definition","___i686.get_pc_thunk.bx");
        Inst_Printf(".private_extern","___i686.get_pc_thunk.bx");
        Label_Printf("___i686.get_pc_thunk.bx:");
        Inst_Printf("movl","(%%esp),%%ebx");
        Inst_Printf("ret","");
    }
}

/*-------------------------------------------------------------------------*/
/* LABEL                                                                   */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void File_Name(char *file_name)
{
    Inst_Printf(".file","\"%s\"",file_name);
}

/*-------------------------------------------------------------------------*/
/* LABEL                                                                   */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Label(symbol_t smb) // ppcized
{
    char * label = Symbol_R_Prolog(smb);
    Inst_Printf(" /* **** Label  ****/","");
	Label_Printf("\n" UN "%s:",label);
	strcpy(current_label,label);
    emit_assign_reloc_base();
}

/*-------------------------------------------------------------------------*/
/* CODE_START                                                              */
/*                                                                         */
/*-------------------------------------------------------------------------*/

void Code_Start(symbol_t smb) // ppcized
{
    long global = smb->prop & Smb_Global;
    long prolog = smb->prop & Smb_Prolog;
    char *label = prolog ? Symbol_R_Prolog(smb) : Symbol_R_Fun(smb);
	
    if (debug) Stab_Function(label,global);
    
    Label_Printf("");
    Inst_Printf(".align","2"); // was 3 for x86 darwin
#ifdef M_powerpc_linux
    Inst_Printf(" .type","%s,@function  /* marche pas sous os X */",label);
#endif
    if (global)
        Inst_Printf(".globl","%s",label);
    
    Label_Printf("\n%s:",label);
	
    if (debug) Stab_Line();
	
    if (!prolog) {
        Inst_Printf("/* ********* Code Start not prolog ********/","");
		Inst_Printf("mr", R(12) "," R(1));
		Inst_Printf("addi", R(1) "," R(1) ",-%d", MAX_C_ARGS_IN_C_CODE * 4);
		Inst_Printf("stw", R(12) ",0(" R(1) ")");
		Inst_Printf("mflr", R(0));
		Inst_Printf("stw", R(0) ",%d(" R(1) ")",(MAX_C_ARGS_IN_C_CODE + 1) * 4);
		
    }
	Load_Label_In_Machine_Register(R(15),"trail",0);
	/*
	 if (profile) {
	 Label_Printf(".data");
	 Inst_Printf(".align","2");
	 Label_Printf(".Lprofile%u:",w_label);
	 Inst_Printf(".long","0");
	 Label_Printf(".text");
	 Inst_Printf("movl","$.Lprofile%u,%%edx",w_label++);
	 Inst_Printf("call","mcount");
	 }
	 */
    emit_assign_reloc_base();
    if (!prolog) {
        emit_assign_trail();
    }
}

/*-------------------------------------------------------------------------*/
/* CODE_STOP                                                               */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Code_Stop(void) // ppcized
{
	Inst_Printf(" /* **** Code Stop  ****/","");
}

/*-------------------------------------------------------------------------*/
/* PL_JUMP                                                                 */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Pl_Jump(symbol_t smb)// to be ppcized
{
	/* we use data access rather than fun access
	 * to avoid pbm with 16-byte stack adjustement
	 * need when going thru a fun stub
	 * in case, smb is locally defined, rlabel_data = UN label = rlabel_fun
	 */
    char *rlabel = Symbol_R_Data(smb);
    if (Defined_P(smb)) {         /* defined label */
        Inst_Printf("jmp","%s",rlabel);
    } else {                    /* undefined label */
        Inst_Printf("movl","%s-%s,%%eax",rlabel,xrba);
        Inst_Printf("jmp","*%%eax");
    }
}

/*-------------------------------------------------------------------------*/
/* JUMP_NOT_ZERO                                                                 */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Jump_Not_Zero_x86(symbol_t smb)
{
    char *rlabel = Symbol_R_Fun(smb);
    Inst_Printf("testl","%%eax,%%eax");
    if (Defined_P(smb)) {         /* defined label */
        Inst_Printf("jne","%s",rlabel);
    } else {                    /* undefined label */
        Inst_Printf("movl","%s-%s,%%eax",rlabel,xrba);
        Inst_Printf("jne","*%%eax");
    }
}


void Jump_Not_Zero(symbol_t smb) /* ppc ized*/ // PAS UTILISE DANS COMPILER
{
	
	char *rlabel = Symbol_R_Fun(smb);
    Inst_Printf(" /* **** Jump_Not_Zero  ****/","");
    /* Inst_Printf("testl","%%eax,%%eax"); 	x86
	 Inst_Printf("jne","%s",label); 		x86 */
	Inst_Printf("cmpwi", CR(1) "," R(3) ",0");  // on compare R3 à zéro
    Inst_Printf("bne", UN "%s",label);		// si !=0 on jump
    
}



/*-------------------------------------------------------------------------*/
/* RET_NOT_ZERO                                                                 */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Ret_Not_Zero_x86(void)
{
    Inst_Printf("testl","%%eax,%%eax");
    Inst_Printf("je","L_cont%u",w_label);
    C_Ret();
    Label_Printf("L_cont%u:",w_label++);
}

/*-------------------------------------------------------------------------*/
/* PL_JUMP_REG                                                             */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Pl_Jump_Reg(char name, long no)
{
    Move_From_Reg(name,no);
    Inst_Printf("jmp","*%s","%eax");
}

/*-------------------------------------------------------------------------*/
/* PL_JUMP_STAR                                                            */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Pl_Jump_Star()
{
    Inst_Printf("jmp","*%s",asm_reg_p);
}

/*-------------------------------------------------------------------------*/
/* PL_FAIL                                                                 */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Pl_Fail(void)
{
	// TBM
	//    Inst_Printf("call","untrail_choice");
    char *label = Symbol_R_Prolog(smb_follow_choice);
    Inst_Printf("call","%s",label);
    Inst_Printf("jmp","*%s",asm_reg_p);    
}

/*-------------------------------------------------------------------------*/
/* PL_RET                                                                  */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Pl_Ret(void)
{
	Inst_Printf("jmp","*%s",asm_reg_cp);
}

/*-------------------------------------------------------------------------*/
/* JUMP                                                                    */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Jump(symbol_t smb)
{
    char *rlabel = Symbol_R_Fun(smb);
    if (Defined_P(smb)) {         /* defined label */
        Inst_Printf("jmp","%s",rlabel);
    } else {                    /* undefined label */
        Inst_Printf("movl","%s-%s,%%eax",rlabel,xrba);
        Inst_Printf("jmp","*%%eax");
    }
}

/*-------------------------------------------------------------------------*/
/* MOVE_FROM_REG                                                         */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Move_From_Reg(char name, long no)
{
    struct reg *reg=Find_Register_Info(name);
    long n = (no+reg->base)*reg->mult;
    int floatp = REG_FLOATP(reg->kind);
    switch (REG_KIND(reg->kind)) {
        case REG_VAR:
            if (floatp)
                Inst_Printf("fldl","%u(%s)",n,reg->asm_reg);
            else
                Inst_Printf("movl","%u(%s),%%eax",n,reg->asm_reg);
            break;
			case REG_UP:
            if (floatp)
                Inst_Printf("fldl","%u(%s)",n,reg->asm_reg);
            else
                Inst_Printf("movl","%u(%s),%%eax",n,reg->asm_reg);            
            break;
			case REG_DOWN:
            if (floatp)
                Inst_Printf("fldl","-%u(%s),%%eax",n,reg->asm_reg);
            else
                Inst_Printf("movl","-%u(%s),%%eax",n,reg->asm_reg);
            break;
    }
}


/*-------------------------------------------------------------------------*/
/* MOVE_TO_REG                                                           */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Move_To_Reg(char name, long no)
{
    struct reg *reg=Find_Register_Info(name);
    long n = (no+reg->base)*reg->mult;
    int floatp = REG_FLOATP(reg->kind);
    switch (REG_KIND(reg->kind)) {
        case REG_VAR:
            if (floatp)
                Inst_Printf("fstpl","%u(%s)",n,reg->asm_reg);
            else
                Inst_Printf("movl","%%eax,%u(%s)",n,reg->asm_reg);
            break;
			case REG_UP:
			if (floatp)
				Inst_Printf("fstpl","%u(%s)",n,reg->asm_reg);
			else
				Inst_Printf("movl","%%eax,%u(%s)",n,reg->asm_reg);            
            break;
			case REG_DOWN:
            if (floatp)
                Inst_Printf("fstpl","-%u(%s)",n,reg->asm_reg);
            else
                Inst_Printf("movl","%%eax,-%u(%s)",n,reg->asm_reg);            
            break;
    }
}

/*-------------------------------------------------------------------------*/
/* MOVE_FLOAT_TO_REG                                                           */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Move_Float_To_Reg(char name, long no)
{
    struct reg *reg=Find_Register_Info(name);
    long n = (no+reg->base)*reg->mult;
    switch (REG_KIND(reg->kind)) {
        case REG_VAR:
            Inst_Printf("fstps","%u(%s)",n,reg->asm_reg);
            break;
        case REG_UP:
            Inst_Printf("fstps","%u(%s)",n,reg->asm_reg);            
            break;
        case REG_DOWN:
            Inst_Printf("fstps","-%u(%s)",n,reg->asm_reg);            
            break;
    }
}

/*-------------------------------------------------------------------------*/
/* MOVE_INT_TO_REG                                                           */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Move_Int_To_Reg(long int_val, char name, long no)
{
    struct reg *reg=Find_Register_Info(name);
    long n = (no+reg->base)*reg->mult;
    switch (REG_KIND(reg->kind)) {
        case REG_VAR:
            Inst_Printf("movl","$%d,%u(%s)",int_val,n,reg->asm_reg);
            break;
        case REG_UP:
            Inst_Printf("movl","$%d,%u(%s)",int_val,n,reg->asm_reg);            
            break;
        case REG_DOWN:
            Inst_Printf("movl","$%d,-%u(%s)",int_val,n,reg->asm_reg);            
            break;
    }
}

/*-------------------------------------------------------------------------*/
/* MOVE_INT_TO_IDENT                                                           */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Move_Int_To_Ident(long int_val, symbol_t smb, long no)
{
	// TBM
    char *ident = Symbol_R_Data(smb);
    if (no==0) {
        Inst_Printf("movl","$%d,%s",int_val,ident); 
    } else {
        Inst_Printf("movl","$%d,%s+%d",int_val,ident,4*no);
    }
}

/*-------------------------------------------------------------------------*/
/* Find Variable                                                           */
/*                                                                         */
/*-------------------------------------------------------------------------*/

void Read_Folvar(int i)
{
    Inst_Printf("movl","%s-%s,%%eax",Symbol_R_Data(smb_folvar_tab),xrba);
    Inst_Printf("leal","%u(%%eax),%%eax",i*sizeof(struct folvar));
    Inst_Printf("orl","$%d,%%eax",TAG_VAR);
}

/*-------------------------------------------------------------------------*/
/* MOVE_CST_TO_REG                                                           */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Move_Cst_To_Reg(char cst_kind, long cst_val, char name, long no)
{
    switch (cst_kind) {
        case 'I':
            Move_Int_To_Reg((int)FOLSMB_MAKE(cst_val,0),name,no);
            break;
        case 'N':
            Move_Int_To_Reg((int)DFOLINT(cst_val),name,no);
            break;
        case 'F':
            Move_Int_To_Reg((int)DFOLFLT(long2float(cst_val)),name,no);
            break;
        case 'C':
            Move_Int_To_Reg((int)DFOLCHAR(cst_val),name,no);
            break;
        case 'V':
            Read_Folvar(cst_val);
            Move_To_Reg(name,no);
            break;
        default:
            fprintf(stderr,"Not a valid Cst constructor %c\n",cst_kind);
            exit(-1);
            break;
    }
}

/*-------------------------------------------------------------------------*/
/* MOVE_CST_TO_IDENT                                                           */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Move_Cst_To_Ident(char cst_kind, long cst_val, symbol_t smb, long no)
{
	/* TBM */
    switch (cst_kind) {
        case 'I':
            Move_Int_To_Ident((long)FOLSMB_MAKE(cst_val,0),smb,no);
            break;
        case 'N':
            Move_Int_To_Ident((long)DFOLINT(cst_val),smb,no);
            break;
        case 'F':
            Move_Int_To_Ident((long)DFOLFLT(long2float(cst_val)),smb,no);
            break;
        case 'C':
            Move_Int_To_Ident((long)DFOLCHAR(cst_val),smb,no);
            break;
        case 'V':
            Read_Folvar(cst_val);
            Move_Ret_To_Ident(smb,no);
            break;
        default:
            fprintf(stderr,"Not a valid Cst constructor %c\n",cst_kind);
            exit(-1);
            break;
    }
}

/*-------------------------------------------------------------------------*/
/* PL_CALL_START                                                           */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Pl_Call_Start(symbol_t smb)
{
    offset=0;
    kind_of_call=1;
}

/*-------------------------------------------------------------------------*/
/* PL_CALL_STOP                                                            */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Pl_Call_Stop(symbol_t smb)
{                               /* TBM */
	//    Inst_Printf("movl","$L_cont%u,%s",w_label,asm_reg_cp);
    Inst_Printf("leal","L_cont%u-%s,%%eax",w_label,xrba);
    Inst_Printf("movl","%%eax,%s",asm_reg_cp);
    Pl_Jump(smb);
    Label_Printf("L_cont%u:",w_label++);
    emit_assign_reloc_base();
}

/*-------------------------------------------------------------------------*/
/* PL_CALL_REG                                                             */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Pl_Call_Reg(char name, long no)
{
	//    Inst_Printf("movl","$L_cont%u,%s",w_label,asm_reg_cp);
    Inst_Printf("leal","L_cont%u-%s,%%eax",w_label,xrba);
    Inst_Printf("movl","%%eax,%s",asm_reg_cp);
    Pl_Jump_Reg(name,no);
    Label_Printf("L_cont%u:",w_label++);
    emit_assign_reloc_base();
}

/*-------------------------------------------------------------------------*/
/* PL_CALL_STAR                                                            */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Pl_Call_Star()
{
	//    Inst_Printf("movl","$L_cont%u,%s",w_label,asm_reg_cp);
    Inst_Printf("leal","L_cont%u-%s,%%eax",w_label,xrba);
    Inst_Printf("movl","%%eax,%s",asm_reg_cp);
    Pl_Jump_Star();
    Label_Printf("L_cont%u:",w_label++);
    emit_assign_reloc_base();
}

/*-------------------------------------------------------------------------*/
/* CALL_C_START                                                            */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Call_C_Start(symbol_t smb)
{
    offset = 0;
    kind_of_call=0;
}

/*-------------------------------------------------------------------------*/
/* CALL_C_ARG_LABEL                                                        */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Call_Arg_Label(symbol_t smb,long no)
{                               /* TBM */
    char *rlabel = Symbol_R_Data(smb);
    if (!Defined_P(smb)) {         /* undefined symbol */
        Inst_Printf("movl","%s-%s,%%eax",rlabel,xrba);
        if (no != 0) {
            Inst_Printf( "addl","%u,%%eax",no*4 );
        }
    } else {                    /* defined symbol */
        Inst_Printf("leal","%s+%u-%s,%%eax",rlabel,no*4,xrba);
    }
    switch (kind_of_call) {
        case 0:                 /* C */
            Inst_Printf("movl","%%eax,%u(%%esp)",offset*4);
            break;
        case 1: {                /* PROLOG */
            struct reg *reg=Find_Register_Info('R');
            int n = (offset+reg->base)*reg->mult;
            Inst_Printf("movl","%%eax,%u(%s)",n,reg->asm_reg);
            break;
        }
    }
    offset++;
}

/*-------------------------------------------------------------------------*/
/* READ_VARIABLE                                                     */
/*                                                                         */
/*-------------------------------------------------------------------------*/

void Read_Variable(symbol_t smb, long no)
{                               /* TBM */
    char *rident = Symbol_R_Data(smb);
    if (!Defined_P(smb)) {         /* undefined symbol */
        Inst_Printf("movl","%s-%s,%%eax",rident,xrba); 
        Inst_Printf("movl","%u(%%eax),%%eax",no*4); 
    } else {                   /* defined symbol */
        Inst_Printf("movl","%s+%u-%s,%%eax",rident,no*4,xrba); 
    }
}

/*-------------------------------------------------------------------------*/
/* CALL_C_ARG_VARIABLE                                                     */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Call_Arg_Variable(symbol_t smb,long no)
{
    Read_Variable(smb,no);
    switch (kind_of_call) {
        case 0:                 /* C */
            Inst_Printf("movl","%%eax,%u(%%esp)",offset*4);
            break;
        case 1:                 /* PROLOG */
            Move_To_Reg('R',offset);
            break;
    }
    offset++;
}

/*-------------------------------------------------------------------------*/
/* CALL_C_ARG_INT                                                          */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Call_Arg_Int(long int_val)
{
    switch (kind_of_call) {
        case 0:                 /* C */
            Inst_Printf("movl","$%d,%u(%%esp)",int_val,offset*4);
            break;
        case 1: {                /* PROLOG */
            struct reg *reg=Find_Register_Info('R');
            int n = (offset+reg->base)*reg->mult;
            Inst_Printf("movl","$%d,%u(%s)",int_val,n,reg->asm_reg);
            break;
        }
    }
    offset++;
}

/*-------------------------------------------------------------------------*/
/* CALL_C_ARG_FLOAT                                                        */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Call_Arg_Float(float flt_val)
{
    double flt_double = (double) flt_val;
    int *p=(int *) &flt_double;
    
    Inst_Printf("movl","$%d,%u(%%esp)",p[0],offset*4);
    Inst_Printf("movl","$%d,%u(%%esp)",p[1],(offset+1)*4);
	
    offset += 2;
}


/*-------------------------------------------------------------------------*/
/* CALL_C_ARG_DOUBLE                                                       */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Call_Arg_Double(double dbl_val)
{
	int *p=(int *) &dbl_val;
	
	Inst_Printf("movl","$%d,%u(%%esp)",p[0],offset*4);
	Inst_Printf("movl","$%d,%u(%%esp)",p[1],(offset+1)*4);
	
	offset += 2;
}

/*-------------------------------------------------------------------------*/
/* CALL_C_ARG_STRING                                                       */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Call_Arg_String(long str_no)
{
    switch (kind_of_call) {
        case 0:                 /* C */
            Inst_Printf("leal","%s%u-%s,%%eax",STRING_PREFIX,str_no,xrba);
            Inst_Printf("movl","%%eax,%u(%%esp)",offset*4);
            break;
        case 1: {                /* PROLOG */
            struct reg *reg=Find_Register_Info('R');
            int n = (offset+reg->base)*reg->mult;
            Inst_Printf("leal","%s%u-%s,%%eax",STRING_PREFIX,str_no,xrba);
            Inst_Printf("movl","%%eax,%u(%s)",n,reg->asm_reg);
            break;
        }
    }
    offset++;
}

/*-------------------------------------------------------------------------*/
/*  CALL_C_ARG_REG                                                         */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Call_Arg_Reg(char name, long no)
{
    struct reg *reg=Find_Register_Info(name);
    int floatp = REG_FLOATP(reg->kind);
    int doffset = floatp ? 2 : 1;
    switch (kind_of_call) {
        case 0:                 /* C */
            Move_From_Reg(name,no);
            if (floatp)
                Inst_Printf("fstpl","%u(%%esp)",offset*4);
            else
                Inst_Printf("movl","%%eax,%u(%%esp)",offset*4);
            break;
			case 1: {                /* PROLOG */
				Move_From_Reg(name,no);
				Move_To_Reg('R',offset);
				break;
			}
    }
    offset += doffset;
}

/*-------------------------------------------------------------------------*/
/* CALL_ARG_REG_VARIABLE                                                 */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Call_Arg_Reg_Variable(char name, long no)
{
    struct reg *reg=Find_Register_Info(name);
    int n = (no+reg->base)*reg->mult;
    int floatp = REG_FLOATP(reg->kind);
    int doffset = floatp ? 2 : 1;
    if (n==0) {
        switch (kind_of_call) {
            case 0:                 /* C */
                if (floatp) {
                    Inst_Printf("fldl","%s",reg->asm_reg);
                    Inst_Printf("fstpl","%u(%%esp)",offset*4);
                } else
                    Inst_Printf("movl","%s,%u(%%esp)",reg->asm_reg,offset*4);
                break;
				case 1: {                /* PROLOG */
					struct reg *reg_R=Find_Register_Info('R');
					int n_R = (offset+reg->base)*reg->mult;
					Inst_Printf("movl","%s,%u(%s)",reg->asm_reg,n_R,reg_R->asm_reg);
					break;
				}
        }
    } else {
        switch (REG_KIND(reg->kind)) {
            case REG_VAR:
                if (floatp)
                    Inst_Printf("fldl","%u(%s)",n,reg->asm_reg);
                else
                    Inst_Printf("leal","%u(%s),%%eax",n,reg->asm_reg);
                break;
				case REG_UP:
                if (floatp)
                    Inst_Printf("fldl","%u(%s)",n,reg->asm_reg);
                else
                    Inst_Printf("movl","%u(%s),%%eax",n,reg->asm_reg);            
                break;
				case REG_DOWN:
                if (floatp)
                    Inst_Printf("fldl","-%u(%s)",n,reg->asm_reg);
                else
                    Inst_Printf("movl","-%u(%s),%%eax",n,reg->asm_reg);            
                break;
        }
        switch (kind_of_call) {
            case 0:                 /* C */
                if (floatp) 
                    Inst_Printf("fstpl","%u(%%esp)",offset*4);
                else
                    Inst_Printf("movl","%%eax,%u(%%esp)",offset*4);
                break;
				case 1: {                /* PROLOG */
					struct reg *reg_R=Find_Register_Info('R');
					int n_R = (offset+reg->base)*reg->mult;
					Inst_Printf("movl","%%eax,%u(%s)",n_R,reg_R->asm_reg);
					break;
				}
        }
        
    }
    offset += doffset;
}

/*-------------------------------------------------------------------------*/
/* CALL_C_ARG_CST                                                          */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Call_Arg_Cst(char name, long no)
{
    fol_t t;
    switch (kind_of_call) {
        case 0:                 /* C */
            switch (name) {
                case 'I':
                    t=FOLSMB_MAKE(no,0);
                    Inst_Printf("movl","$%d,%u(%%esp)",(int)t,offset*4);            
                    break;
                case 'N':
                    t=DFOLINT(no);
                    Inst_Printf("movl","$%d,%u(%%esp)",(int)t,offset*4);            
                    break;
                case 'F':
                    t=DFOLFLT(long2float(no));
                    Inst_Printf("movl","$%d,%u(%%esp)",(int)t,offset*4);            
                    break;
                case 'C':
                    t=DFOLCHAR(no);
                    Inst_Printf("movl","$%d,%u(%%esp)",(int)t,offset*4);            
                    break;
                case 'V':
                    Read_Folvar(no);
                    Inst_Printf("movl","%%eax,%u(%%esp)",offset*4);            
                    break;
                default:
                    fprintf(stderr,"Not a valid Cst constructor %c\n",name);
                    exit(-1);
                    break;
            }
            break;
        case 1: {               /* PROLOG */
            struct reg *reg=Find_Register_Info('R');
            int n = (offset+reg->base)*reg->mult;
            switch (name) {
                case 'I':
                    t=FOLSMB_MAKE(no,0);
                    Inst_Printf("movl","$%d,%u(%s)",(int)t,n,reg->asm_reg);            
                    break;
                case 'N':
                    t=DFOLINT(no);
                    Inst_Printf("movl","$%d,%u(%s)",(int)t,n,reg->asm_reg);            
                    break;
                case 'F':
                    t=DFOLFLT(long2float(no));
                    Inst_Printf("movl","$%d,%u(%s)",(int)t,n,reg->asm_reg);            
                    break;
                case 'C':
                    t=DFOLCHAR(no);
                    Inst_Printf("movl","$%d,%u(%s)",(int)t,n,reg->asm_reg);
                    break;
                case 'V':
                    Read_Folvar(no);
                    Inst_Printf("movl","%%eax,%u(%s)",n,reg->asm_reg);            
                    break;
                default:
                    fprintf(stderr,"Not a valid Cst constructor %c\n",name);
                    exit(-1);
                    break;
            }
            break;
        }
    }
    offset++;
}

/*-------------------------------------------------------------------------*/
/* CALL_C_ARG_PARAM                                                        */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Call_Arg_Param(long no)
{
    Inst_Printf("movl","%u(%%ebp),%%eax",(no+1)*4);
    switch (kind_of_call) {
        case 0:                 /* C */
            Inst_Printf("movl","%%eax,%u(%%esp)",offset*4);
            break;
        case 1:                 /* PROLOG */
            Move_To_Reg('R',offset);
            break;
    }
	
    offset++;
}

/*-------------------------------------------------------------------------*/
/* CALL_C_STOP                                                             */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Call_C_Stop(symbol_t smb)
{                               /* TBM */
    char *tmp_fct_name = Symbol_R_Fun(smb);
    Inst_Printf("call","%s",tmp_fct_name);
}

/*-------------------------------------------------------------------------*/
/* JUMP_RET                                                                */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Jump_Ret(void)
{
    Inst_Printf("jmp","*%%eax");
}

/*-------------------------------------------------------------------------*/
/* FAIL_RET                                                                */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Fail_Ret(void)
{
    Inst_Printf("testl","%%eax,%%eax");
    Inst_Printf("je", UN "%s","fail");
}

/*-------------------------------------------------------------------------*/
/* TRUE_RET                                                                */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void True_Ret(void)
{
    Inst_Printf("testl","%%eax,%%eax");
    Inst_Printf("jne",UN "%s","fail");
}

/*-------------------------------------------------------------------------*/
/* FAIL_C_RET                                                                */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Fail_C_Ret(void)
{
    Inst_Printf("testl","%%eax,%%eax");
    Inst_Printf("jne","L_cont%u",w_label);
    C_Ret();
    Label_Printf("L_cont%u:",w_label++);
}

/*-------------------------------------------------------------------------*/
/* TRUE_C_RET                                                                */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void True_C_Ret(void)
{
    Inst_Printf("testl","%%eax,%%eax");
    Inst_Printf("je","L_cont%u",w_label);
    C_Ret();
    Label_Printf("L_cont%u:",w_label++);
}

/*-------------------------------------------------------------------------*/
/* MOVE_RET_TO_IDENT                                                       */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Move_Ret_To_Ident(symbol_t smb,long no)
{                               /* TBM */
    char *rident = Symbol_R_Data(smb);
    if (!Defined_P(smb)) {         /* undefined symbol */
        Inst_Printf("movl","%s-%s,%%ecx",rident,xrba);
        Inst_Printf("movl","%%eax,%u(%%ecx)",4*no);
    } else {                    /* defined symbol */
        Inst_Printf("movl","%%eax,%d+%s-%s",4*no,rident,xrba);
    }
}

/*-------------------------------------------------------------------------*/
/* MOVE_RET_TO_REG                                                         */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Move_Ret_To_Reg(char name, long no)
{
    Move_To_Reg(name,no);
}

/*-------------------------------------------------------------------------*/
/* C_RET                                                                   */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void C_Ret(void)
{
    Inst_Printf("addl", "$%d,%%esp", 4*MAX_C_ARGS_IN_C_CODE);
    Inst_Printf("popl", "%%esi");
    Inst_Printf("popl", "%%ebx");
    Inst_Printf("popl", "%%ebp");
    Inst_Printf("ret", "");
}

/*-------------------------------------------------------------------------*/
/* DICO_STRING_START                                                       */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Dico_String_Start(long nb_consts)
{
    Label_Printf(".cstring");
}

/*-------------------------------------------------------------------------*/
/* DICO_STRING                                                             */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Dico_String(long str_no,char *asciiz)
{
    Label_Printf("%s%u:",STRING_PREFIX,str_no);
    Inst_Printf(".ascii","\"%s\\0\"",asciiz);
}

/*-------------------------------------------------------------------------*/
/* DICO_STRING_STOP                                                        */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Dico_String_Stop(long nb_consts)
{
}

/*-------------------------------------------------------------------------*/
/* DICO_LONG_START                                                         */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Dico_Long_Start()
{
    Label_Printf(".data");
    Inst_Printf(".align","2");
}

/*-------------------------------------------------------------------------*/
/* DICO_LONG                                                               */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Dico_Long(symbol_t smb)

{
    char *label = smb->label;
    long global = smb->prop & Smb_Global;
    long length = smb->size;
    InitLongList_t value = smb->init;
    if (!length)
        length = 1;
    if (value){
        if (global)
            Inst_Printf(".globl", UN "%s",label);
        Label_Printf(UN "%s:",label);
        for(; value && length ; value = value->next, length--) {
            if (value->type==0) {
                Inst_Printf(".long",UN "%s",(char *)value->value);
            } else {
                Inst_Printf(".long","%d",value->value);
            }
        }
        if (value) {            /* list too long  */
            fprintf(stderr,"Init Long List too long for ident %s\n",label);
            exit(1);            
        }
    } else if (global) {
		//        Inst_Printf(".comm","%s,%lu",label,(unsigned long)(4*length));
        Inst_Printf(".globl",UN "%s",label);
        Inst_Printf(".data","");
        Inst_Printf(".zerofill", "__DATA, __common, _name,%d,2",
                    (unsigned long) (4*length), 2);
    } else {
        Inst_Printf(".lcomm",UN "%s,%lu,2",label,(unsigned long)(4*length)); 
    }
    
}

/*-------------------------------------------------------------------------*/
/* DICO_LONG_STOP                                                          */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Dico_Long_Stop()
{
}



