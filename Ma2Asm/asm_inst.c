#include "libdyalog.h"
#include "builtins.h"

extern unsigned long var;
extern void *label;
long v1[100];
static long v2[100];
extern long v3[100];
long u2[2];
long u3[3];
long u4[4];
long u5[5];

void foo();
void foo1();
void pl_call1();

extern void xfoo();
extern void xbar(long);

// extern long trail[];

// #define X(i) (trail[i]) 
#define YY(i) (trail[i])

#define M_Direct_Goto(label) goto label
#define M_Indirect_Goto(v)   goto *v
//#define R_CP X(0)
//#define LVALUE_R_CP R_CP(0)

typedef void *CodePtr;

long x, y;

extern long foreign_long[];
extern double foreign_double[];

/* to define Asm_Start() */

/* to define Code_Start() (global/not global) and Code_Stop() */

void
TRANS_code_start_global()
{
}

static void
TRANS_code_start_non_global()
{
}

/* to define Pl_Jump() */

void
TRANS_pl_jump()
{
    M_Direct_Goto(foo1);
foo1:;
}

/* to define Pl_Call() */

void
TRANS_pl_call()
{
  LVALUE_R_CP = (CodePtr) pl_call1;
      /*
  M_Direct_Goto(foo);
#ifdef M_ix86_win32
_foo:;
#else
  asm("pl_call1:");
#endif
      */
}

/* to define Pl_Fail() */

void
TRANS_pl_fail()
{
}

/* to define Pl_Ret() */

void
TRANS_pl_ret()
{
    M_Indirect_Goto(R_CP);
}

/* to define Jump() */

void
TRANS_jump()
{
  if (x < 3)
    {
      bar(x);
      goto a;
    }
  x++;
a:;
}

/* to define Move_From/To_Reg_X/Y() */

void
TRANS_move_x_to_x()
{
    LVALUE_X(3) = REG_VALUE(X(5));
}

void
TRANS_move_x_to_y()
{
    YY(10) = REG_VALUE(X(2));
}

void
TRANS_move_y_to_x()
{
  LVALUE_X(0) = YY(3);
}

void
TRANS_move_y_to_y()
{
  YY(2) = YY(4);
}

/* to define Call_C_Start() + Call_C_Arg()... + Call_C_Stop() */

void
TRANS_call_c(void)
{
/*  &label,var,long, double,         string */
  bar(foo, var, 12, 4098, -4095, (double) 1.20e-10, "this is a string",
      "a\14b");

  /* v(index) */
  bar1(v1[2], v1[0], &v1[12], v2[2], v2[0], &v2[4]);
  bar1(v3[4], &v3[2], v3[0]);

  /* regs / &regs */
  bar2(X(0), LVALUE_X(4), YY(0), &YY(12));
}

void
TRANS_call_c_lot_of_args(void)
{
/*  &label,var,long, double,         string */
    vbar(3, 4, 5, 6, 7, 8, 9, 10, foo, var, 12, 4098, -4095, (double) 1.20e-10, (double) 40.1, (float) 25.72, "this is a string", "a\14b");

  /* regs / &regs */
  bar1(0, 0, 0, 0, 0, 0, X(2), LVALUE_X(4), X(0), LVALUE_X(12));
}

void
TRANS_call_c_foreign(void)
{
  bar(foreign_long[0], foreign_long[4], &foreign_long[0], &foreign_long[8]);
  bar(foreign_double[0], foreign_double[4], &foreign_double[0],
      &foreign_double[8]);
}

/* to define Jump_Ret() */

void
TRANS_jump_ret()
{
#if defined(M_ix86_win32)
  register long adr = (long) bar(12, "toto");

  _asm
  {
  jmp adr}
#else
  goto *bar(12, "toto");
#endif
}

  
/* to define Fail_Ret() */

void
TRANS_fail_ret()
{
  if (test(1, 2, 3) == 0)
    goto a;

  x++;
a:;
}



/* to define Move_Ret_To_Mem() */

void
TRANS_move_ret_to_mem()
{
  var = bar(3);
  v1[4096] = bar(15);
}



/* to define Move_Ret_To_Reg_X() */

void
TRANS_move_ret_to_reg_x()
{
    LVALUE_X(4) = REG_VALUE((long)bar(3));
}

/* to define Move_Ret_To_Reg_Y() */

void
TRANS_move_ret_to_reg_y()
{
    YY(2) = REG_VALUE((long)bar(3));
}



/* to define Move_Ret_To_Foreign_L() */

void
TRANS_move_ret_to_foreign_l()
{
  foreign_long[123] = bar(3);
}




/* to define Move_Ret_To_Foreign_D() */

void
TRANS_move_ret_to_foreign_d()
{
  double bard(void);

  foreign_double[123] = bard();
}




/* to define Cmp_Ret_And_Long() */

void
TRANS_cmp_ret_and_long()
{
  if (bar(foo) == 0)		/* case ret = 0 */
    goto a;

  if (bar(foo) == 12345678)	/* case ret !- 0 */
    goto a;

  x++;
a:;
}


/* to define Jump_If_Equal() */

void
TRANS_jump_if_equal()
{
  if (x == y)
    goto a;

  x++;
a:;
}




/* to define Jump_If_Greater() */
/* maybe the C compiler does not generate a jg but a jl, reverse if needed */

void
TRANS_jump_if_greater()
{
  if (x > 12)
    goto a;

  if (y > x)
    goto a;

  x++;
a:
  foo(1);
}


/* to define C_Ret() */

void
TRANS_c_ret()
{
}



/* to define Dico_String_Start() + Dico_String() + Dico_String_Stop() */
/* see definitions of strings in the asm file produced                */

void
TRANS_dico_string()
{
  bar("str1", "str2", "str3", "str\r\tend\n", "str\019toto");
}




/* to define Dico_Long_Start() + Dico_Long() + Dico_Long_Stop()         */
/* see definitions of longs in the asm file produced (global/not global)*/

static long long1;
static long long1b = 0;
static long long2 = 100;
long long3;
long long4 = 128;

static long local_array[128];
long global_array[128];

long global_array2[4] = {1,(long)global_array,(long) &var,0};

    

/* to define Data_Start() + Data_Stop */
/* between obj_chain_start and obj_chain_stop */

static void
initializer_fct()
{
}



# if 0

/* this should not be useful */

long
see_switch()
{
  long y;
  long x = bar();

  if (x == 10)
    y += 11;
  if (x > 10)
    y += 12;
  if (x < 10)
    y += 13;


  if (x == 0)
    y += 1;
  if (x > 0)
    y += 2;
  if (x < 0)
    y += 3;

  return y;
}


#endif

static
long lfoo (long i) 
{
    return i*i;
}


void mytest ()
{
    LVALUE_X(1) = REG_VALUE(xfoo);
    xfoo();
    xbar(1);
    lfoo(1);
}

fol_t TRANS_get_var (long i)
{
    return FOLVAR_FROM_INDEX(i);
}

extern void *Mutable_Write(void *,sfol_t,long);

void TRANS_call_arg_variable ()
{
    Mutable_Write(0,(sfol_t) &(trail[20]),1);
}


