/************************************************************
 * $Id$
 * Copyright (C) 1997, 2004, 2014 Eric de la Clergerie
 * ------------------------------------------------------------
 *
 *   Buffer --
 *
 * ------------------------------------------------------------
 * Description
 *
 * ------------------------------------------------------------
 */



#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <stdlib.h>
#include "./buffer.h"

buffer buffer_new( unsigned long init_size )
{
  buffer b = (buffer) malloc( sizeof( struct buffer ) );
  
  b->length = b->init = init_size;
  b->pos = 0;
  b->buff = (char *)malloc( init_size );
  return b;
}
  
static
void buffer_reallocate( buffer b )
{
  unsigned long new_length = 2*b->length;
  char *s = (char *)malloc( new_length );
  
  b->buff = strncpy( s, b->buff, b->length );
  b->length = new_length;
}

void buffer_push( buffer b, char c )
{
  
  if ( b->pos >= b->length )
    buffer_reallocate( b );

  b->buff[b->pos++] = c;
}

char *buffer_flush( buffer b )
{
  char *s;
  
  buffer_push( b , '\0' );
  s = b->buff;

  b->length = b->init;
  b->pos = 0;
  b->buff = (char *)malloc( b->init );
  
  return s;
}
  
void buffer_printf( buffer b, const unsigned long n, const char *fmt, ... )
{
  va_list args;

  while ( b->pos + n > b->length )
    buffer_reallocate( b );

  va_start( args, fmt );

  vsprintf( b->buff + b->pos, fmt, args );

  b->pos += n;

  va_end( args );
}
