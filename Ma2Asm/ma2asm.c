/* 
 ******************************************************************
 * $Id$
 * Copyright (C) 1998, 2006, 2009, 2010, 2014, 2016 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  dyam2ma.c -- tools for DyAM to Ma conversion
 *
 * ----------------------------------------------------------------
 * Description
 * 
 * ----------------------------------------------------------------
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <search.h>
#include "buffer.h"
#include "ma2asm.h"

extern int yydebug;
extern int yyparse();
extern int yyrestart(FILE *);

typedef struct InfLong {
    char *name;
    int   prop;
    InitLongList_t  value;
    unsigned long  length;
    struct InfLong *next;
}InfLong;

typedef struct InfString {
    char *name;
    struct InfString *next;
}InfString;

buffer auxbuf;
char *file_name_in;
char *file_name_out;
int       keep_source_lines;

FILE     *file_out;

int       work_label=0;

int       profile=0;
int       debug=0;
int       pic=1;

unsigned long       string_n=0;
unsigned long       float_n=0;

InfLong  *long_stack=0;
InfLong  **long_stack_top=&long_stack;
long       long_n=0;


/* bookeeping information about function calls */
inst_t current_inst=0;
unsigned long offset=0;
unsigned long float_offset=0;

void init_function_call (inst_t inst) 
{
    current_inst=inst;
    offset=0;
    float_offset=0;
}

void stop_function_call () 
{
    current_inst=0;
}


void function_add_arg (void) 
{
    if (current_inst) {
        offset++;
        current_inst->arg2++;
    } 
}

void function_add_float_arg (void) 
{
    if (current_inst) {
        float_offset++;
        current_inst->arg3++;
    }
}

    

/*
 * Error and warning management
 */

void
ma2asm_warning( const char * msg, ...)
{
    va_list args;
    va_start(args,msg);
    fprintf(stderr,"warning: ");
    vfprintf(stderr,msg,args);
    va_end(args);
}

void
ma2asm_error( const char * msg, ...)
{
    va_list args;
    va_start(args,msg);
    fprintf(stderr,"warning: ");
    vfprintf(stderr,msg,args);
    va_end(args);
    exit(1);
}

/*
 * Symbol Management
 */

void *symbol_tbl;

static int symbol_compare (const void *a,const void *b)
{
    return strcmp(((symbol_t)a)->label,((symbol_t)b)->label);
}

symbol_t
Find_Or_Add_Symbol (char *label)
{
    symbol_t pos = (symbol_t) malloc( sizeof(struct symbol) );
    pos->label = label;
    pos->prop = 0;
    return *(symbol_t *)tsearch((void *)pos,&symbol_tbl,symbol_compare);
}

symbol_t
define_symbol( char *label,
               long properties,
               ulong size,
               InitLongList_t init ) 
{
    symbol_t smb = Find_Or_Add_Symbol(label);
    if (smb->prop & Smb_Defined) {
        ma2asm_error("symbol multiply defined %s\n",label);
    }
    smb->prop |= Smb_Defined;
    smb->prop |= properties;
    smb->size = size;
    smb->init = init;
//    fprintf(stderr,"defined symbol %s\n",label);
    return smb;
}

symbol_t
refer_symbol( char *label, long properties) 
{
    symbol_t smb = Find_Or_Add_Symbol(label);
    smb->prop |= Smb_Referenced;
    smb->prop |= properties;
    /* Should complete by testing compatibility bewteen
     * properties and possibly existing properties
     */
//    fprintf(stderr,"refered symbol %s\n",label);
    return smb;
}

long
Defined_P (symbol_t smb) 
{
    return smb->prop & Smb_Defined;
}

void Check_C_Function (symbol_t smb)
{
    if (!(smb->prop & Smb_C)) {
        ma2asm_error("symbol %s used but not defined as a C function",smb->label);
    }
}

void Check_Dyalog_Proc (symbol_t smb)
{
    if (!(smb->prop & Smb_Prolog)) {
        ma2asm_error("symbol %s used but not defined as a DyALog procedure",smb->label);
    }
}

void Check_Data (symbol_t smb)
{
    if (!(smb->prop & Smb_Long)) {
        ma2asm_error("symbol %s used but not defined as a data",smb->label);
    }
}

static void
Dico_Long_Action(const void *nodep,
                 const VISIT which,
                 const int depth
                 )
{
    symbol_t smb = *((symbol_t *)nodep);
    ulong prop = smb->prop;
    switch (which) {
        case postorder:
        case leaf:
            if ((prop & (Smb_Defined | Smb_Long)) == (Smb_Defined | Smb_Long)
                && !(prop & Smb_Label)
                )
                Dico_Long(smb);
            break;
        default:
            break;
    }
}


/*
 * Constant Management
 */

static int
constant_compare (const void *a, const void *b) {
    constant_t ca = (constant_t) a;
    constant_t cb = (constant_t) b;
    if (ca->kind < cb->kind) {
        return -1;
    } else if (ca->kind > cb->kind) {
        return 1;
    } else if (ca->kind == Cst_String) {
        return strcmp(ca->value.s,cb->value.s);
    } else if (ca->value.f < cb->value.f) {
        return -1;
    } else if (ca->value.f > cb->value.f) {
        return 1;
    } else {
        return 0;
    }
}

void *constant_tbl;
static constant_t tmp_cst;

constant_t
Find_Or_Add_String( char *string )
{
    constant_t *pos;
    if (!tmp_cst) {
        tmp_cst = (constant_t) malloc( sizeof(struct constant) );
    }
    tmp_cst->kind = Cst_String;
    tmp_cst->value.s = string;
    pos = (constant_t *)tsearch((void *)tmp_cst,&constant_tbl,constant_compare);
    if (tmp_cst == (*pos)) {    // new entry
        tmp_cst->id = string_n++;
        tmp_cst = NULL;
    }
    return *pos;
}

constant_t
Find_Or_Add_Float( float f)
{
    constant_t *pos;
    if (!tmp_cst) {
        tmp_cst = (constant_t) malloc( sizeof(struct constant) );
    }
    tmp_cst->kind = Cst_Float;
    tmp_cst->value.f = f;
    pos = (constant_t *)tsearch((void *)tmp_cst,&constant_tbl,constant_compare);
    if (tmp_cst == (*pos)) {    // new entry
        tmp_cst->id = float_n++;
        tmp_cst = NULL;
    }
    return *pos;
}

static void
Dico_String_Action(const void *nodep,
                     const VISIT which,
                     const int depth
                     )
{
    constant_t cst = *((constant_t *)nodep);
    ulong kind = cst->kind;
    switch (which) {
        case postorder:
        case leaf:
            if (kind == Cst_String)
                Dico_String(cst->id,cst->value.s);
            break;
        default:
            break;
    }
}

static void
Dico_Float_Action(const void *nodep,
                  const VISIT which,
                  const int depth
                  )
{
    constant_t cst = *((constant_t *)nodep);
    ulong kind = cst->kind;
    switch (which) {
        case postorder:
        case leaf:
            if (kind == Cst_Float)
                Dico_Float(cst->id,cst->value.f);
            break;
        default:
            break;
    }
}

/*
 * Generic Instruction Handler
 */


inst_t instructions;
static inst_t *last_instruction=&instructions;

extern int yylineno;

/*
static char *code[] = {
    "not_a_inst",
    "code_start",
    "pl_fail",
    "pl_ret",
    "jump",
    "move_from_reg",
    "move_to_reg",
    "move_int_to_reg",
    "move_int_to_ident",
    "move_cst_to_reg",
    "move_cst_to_ident",
    "read_variable",
    "move_ret_to_ident",
    "jump_ret",
    "fail_ret",
    "true_ret",
    "fail_c_ret",
    "true_c_ret",
    "c_ret",
    "pl_call_start",
    "pl_jump",
    "pl_jump_reg",
    "pl_jump_star",
    "pl_call_stop",
    "pl_call_star",
    "pl_call_reg",
    "call_c_start",
    "call_c_stop",
    "call_arg_int",
    "call_arg_float",
    "call_arg_reg",
    "call_arg_reg_variable",
    "call_arg_param",
    "call_arg_label",
    "call_arg_variable",
    "call_arg_string",
    "call_arg_cst",
    "label",
    "jump_not_zero",
    "ret_not_zero",
    "defined"
};
*/

static inst_t
add_instruction(inst_kind kind,
                inst_handler handler,
                InstArg arg1,
                InstArg arg2,
                InstArg arg3,
                InstArg arg4
                )
{
    inst_t inst = (inst_t) malloc( sizeof( struct inst) );
    *last_instruction = inst;
    last_instruction = &(inst->next);
    inst->kind = kind;
    inst->handler = handler;
    inst->arg1 = arg1;
    inst->arg2 = arg2;
    inst->arg3 = arg3;
    inst->arg4 = arg4;
/*    fprintf(stderr,"Line %d : Add instruction %d %s\n",
            yylineno,(int) kind,code[(int)kind]);
*/
    return inst;
}

void
Inst_Loop() 
{
    inst_t inst;
    for(inst=instructions; inst; inst = inst->next) {
        (*(inst->handler))(inst);
    }
}

#define Add_Inst4(_kind,_handler,_arg1,_arg2,_arg3,_arg4)       \
      add_instruction(_kind,                                    \
                      (inst_handler) _handler,                  \
                      (InstArg) _arg1,                          \
                      (InstArg) _arg2,                          \
                      (InstArg) _arg3,                          \
                      (InstArg) _arg4                           \
                      )

#define Add_Inst3(_kind,_handler,_arg1,_arg2,_arg3)     \
      add_instruction(_kind,                            \
                      (inst_handler) _handler,          \
                      (InstArg) _arg1,                  \
                      (InstArg) _arg2,                  \
                      (InstArg) _arg3,                  \
                      (InstArg) 0                       \
                      )

#define Add_Inst2(_kind,_handler,_arg1,_arg2)   \
      add_instruction(_kind,                    \
                      (inst_handler) _handler,  \
                      (InstArg) _arg1,          \
                      (InstArg) _arg2,          \
                      (InstArg) 0,              \
                      (InstArg) 0               \
                      )

#define Add_Inst1(_kind,_handler,_arg1)         \
      add_instruction(_kind,                    \
                      (inst_handler) _handler,  \
                      (InstArg) _arg1,          \
                      (InstArg) 0,              \
                      (InstArg) 0,              \
                      (InstArg) 0               \
                      )

#define Add_Inst0(_kind,_handler)               \
      add_instruction(_kind,                    \
                      (inst_handler) _handler,  \
                      (InstArg) 0,              \
                      (InstArg) 0,              \
                      (InstArg) 0,              \
                      (InstArg) 0               \
                      )

#define A1(_type) ((_type)inst->arg1)
#define A2(_type) ((_type)inst->arg2)
#define A3(_type) ((_type)inst->arg3)
#define A4(_type) ((_type)inst->arg4)


#define Handler0(_kind,_name)                   \
void _name##_Handler (inst_t inst)              \
{                                               \
    _name();                                    \
}                                               \
                                                \
void Inst_##_name()                             \
{                                               \
    Add_Inst0( _kind, _name##_Handler);         \
}

// Code_Start

void Code_Start_Handler (inst_t inst) 
{
    Code_Start(A1(symbol_t));
}

void Inst_Code_Start (char *fun,int prolog_or_c, int global_or_local) 
{
    long prop = Smb_Defined;
    prop |= global_or_local ? Smb_Global : Smb_Local;
    prop |= prolog_or_c ? Smb_Prolog : Smb_C;
    Add_Inst1( code_start,
               Code_Start_Handler,
               define_symbol(fun,prop,0,0)
               );
}

// Simple Handlers

Handler0(pl_fail,Pl_Fail);
Handler0(pl_ret,Pl_Ret);
Handler0(c_ret,C_Ret);
Handler0(jump_ret,Jump_Ret);
Handler0(fail_ret,Fail_Ret);
Handler0(true_ret,True_Ret);
Handler0(fail_c_ret,Fail_C_Ret);
Handler0(true_c_ret,True_C_Ret);
Handler0(pl_call_star,Pl_Call_Star);
Handler0(ret_not_zero,Ret_Not_Zero);
Handler0(pl_jump_star,Pl_Jump_Star);

// Pl_Jump

void Pl_Jump_Handler (inst_t inst) 
{
    Pl_Jump(A1(symbol_t));
}

void Inst_Pl_Jump (char *ident)
{
    Add_Inst1( pl_jump,
               Pl_Jump_Handler,
               refer_symbol(ident, Smb_Prolog)
               );
}


// Pl_Jump_Reg

void Pl_Jump_Reg_Handler (inst_t inst) 
{
    Pl_Jump_Reg(A1(char),A2(long));
}

void Inst_Pl_Jump_Reg (char r, long no)
{
    Add_Inst2( pl_jump_reg,
               Pl_Jump_Reg_Handler,
               r,
               no
               );
}


// Jump

void Jump_Handler (inst_t inst) 
{
    Jump(A1(symbol_t));
}

void Inst_Jump (char *ident)
{
    Add_Inst1( jump,
               Jump_Handler,
               refer_symbol(ident, Smb_C)
               );
}

// Jump_Not_Zero

void Jump_Not_Zero_Handler (inst_t inst) 
{
    Jump_Not_Zero(A1(symbol_t));
}

void Inst_Jump_Not_Zero (char *ident)
{
    Add_Inst1( jump_not_zero,
               Jump_Not_Zero_Handler,
               refer_symbol(ident, Smb_C)
               );
}


// Label

void Label_Handler (inst_t inst) 
{
    Label(A1(symbol_t));
}

void Inst_Label (char *ident)
{
    Add_Inst1( label,
               Label_Handler,
               define_symbol(ident, Smb_Label | Smb_Local ,0,0)
               );
}

// Pl_Call_Start

void Pl_Call_Start_Handler (inst_t inst) 
{
    Pl_Call_Start(A1(symbol_t));
}

void Inst_Pl_Call_Start (char *ident)
{
    Add_Inst1( pl_call_start,
               Pl_Call_Start_Handler,
               refer_symbol(ident, Smb_Prolog)
               );
}

// Pl_Call_Reg

void Pl_Call_Reg_Handler (inst_t inst) 
{
    Pl_Call_Reg(A1(char),A2(long));
}

void Inst_Pl_Call_Reg (char r, long no)
{
    Add_Inst2( pl_call_reg,
               Pl_Call_Reg_Handler,
               r,
               no
               );
}

// Pl_Call_Stop

void Pl_Call_Stop_Handler (inst_t inst) 
{
    Pl_Call_Stop(A1(symbol_t));
}

void Inst_Pl_Call_Stop (char *ident)
{
    Add_Inst1( pl_call_stop,
               Pl_Call_Stop_Handler,
               refer_symbol(ident, Smb_Prolog)
               );
}


// Call_C_Start

void Call_C_Start_Handler (inst_t inst) 
{
    Call_C_Start(A1(symbol_t),A2(unsigned long),A3(unsigned long));
}

void
Inst_Call_C_Start (char *ident)
{
    inst_t inst =
        Add_Inst3( call_c_start,
                   Call_C_Start_Handler,
                   refer_symbol(ident, Smb_C),
                   0,               /* number of long args, updated by _Arg_* */
                   0                /* number of float args, updated by _Arg_* */
                   );
    init_function_call(inst);
}

// Call_C_Stop

void Call_C_Stop_Handler (inst_t inst) 
{
    Call_C_Stop(A1(symbol_t),A2(unsigned long),A3(unsigned long));
}

void Inst_Call_C_Stop (char *ident)
{
    Add_Inst3( call_c_stop,
               Call_C_Stop_Handler,
               refer_symbol(ident, Smb_C),
               offset,
               float_offset
               );
    stop_function_call();
}

// Move_From_Reg

void Move_From_Reg_Handler (inst_t inst) 
{
    Move_From_Reg(A1(char),A2(long));
}

void Inst_Move_From_Reg (char r,long no)
{
    Add_Inst2( move_from_reg,
               Move_From_Reg_Handler,
               r,
               no );
}

// Move_To_Reg

void Move_To_Reg_Handler (inst_t inst) 
{
    Move_To_Reg(A1(char),A2(long));
}

void Inst_Move_To_Reg (char r, long no)
{
    Add_Inst2( move_to_reg,
               Move_To_Reg_Handler,
               r,
               no );
}

// Move_Int_To_Reg

void Move_Int_To_Reg_Handler (inst_t inst) 
{
    Move_Int_To_Reg(A1(long), A2(char),A3(long));
}

void Inst_Move_Int_To_Reg (long v, char r, long no)
{
    Add_Inst3( move_int_to_reg,
               Move_Int_To_Reg_Handler,
               v,
               r,
               no
               );
}

// Move_Int_To_Ident

void Move_Int_To_Ident_Handler (inst_t inst) 
{
    Move_Int_To_Ident(A1(long), A2(symbol_t),A3(long));
}

void Inst_Move_Int_To_Ident (long v, char *ident,long no)
{
    Add_Inst3( move_int_to_ident,
               Move_Int_To_Ident_Handler,
               v,
               refer_symbol(ident,Smb_Long),
               no
               );
}

// Move_Cst_To_Reg

void Move_Cst_To_Reg_Handler (inst_t inst) 
{
    Move_Cst_To_Reg(A1(char),A2(long), A3(char),A4(long));
}

void Inst_Move_Cst_To_Reg (char cst, long val, char r,long no)
{
    Add_Inst4( move_cst_to_reg,
               Move_Cst_To_Reg_Handler,
               cst,
               val,
               r,
               no
               );
}

// Move_Cst_To_Ident

void Move_Cst_To_Ident_Handler (inst_t inst) 
{
    Move_Cst_To_Ident(A1(char), A2(long), A3(symbol_t),A4(long));
}

void Inst_Move_Cst_To_Ident (char cst, long val,char *ident,long no)
{
    Add_Inst4( move_cst_to_ident,
               Move_Cst_To_Ident_Handler,
               cst,
               cst,
               refer_symbol(ident,Smb_Long),
               no
               );
}

// Read_Variable

void Read_Variable_Handler (inst_t inst) 
{
    Read_Variable(A1(symbol_t), A2(long));
}

void Inst_Read_Variable (char *ident, long trail)
{
    Add_Inst2( read_variable,
               Read_Variable_Handler,
               refer_symbol(ident,Smb_Long),
               trail
               );
}

// Move_Ret_To_Ident

void Move_Ret_To_Ident_Handler (inst_t inst) 
{
    Move_Ret_To_Ident(A1(symbol_t), A2(long));
}

void Inst_Move_Ret_To_Ident (char *ident, long trail)
{
    Add_Inst2( move_ret_to_ident,
               Move_Ret_To_Ident_Handler,
               refer_symbol(ident,Smb_Long),
               trail
               );
}

// Call_Arg_Int

void Call_Arg_Int_Handler (inst_t inst) 
{
    Call_Arg_Int(A1(long));
}

void Inst_Call_Arg_Int (long v) 
{
    function_add_arg();
    Add_Inst1(call_arg_int,
              Call_Arg_Int_Handler,
              v);
}

// Call_Arg_Float

void Call_Arg_Float_Handler (inst_t inst) 
{
    Call_Arg_Float(A1(constant_t));
}

void Inst_Call_Arg_Float (float v) 
{
    function_add_float_arg();
    Add_Inst1(call_arg_float,
              Call_Arg_Float_Handler,
//              v
              Find_Or_Add_Float(v)
              );
}


// Call_Arg_String

void Call_Arg_String_Handler (inst_t inst) 
{
    Call_Arg_String(A1(constant_t));
}

void Inst_Call_Arg_String (char *s) 
{
    function_add_arg();
    Add_Inst1(call_arg_string,
              Call_Arg_String_Handler,
//              Add_String(s)
              Find_Or_Add_String(s)
              );
}

// Call_Arg_Param

void Call_Arg_Param_Handler (inst_t inst) 
{
    Call_Arg_Param(A1(long));
}

void Inst_Call_Arg_Param (long v) 
{
    function_add_arg();
    Add_Inst1(call_arg_param,
              Call_Arg_Param_Handler,
              v);
}

// Call_Arg_Reg

void Call_Arg_Reg_Handler (inst_t inst) 
{
    Call_Arg_Reg(A1(char),A2(long));
}

void Inst_Call_Arg_Reg (char r,long no) 
{
    function_add_arg();
    Add_Inst2(call_arg_reg,
              Call_Arg_Reg_Handler,
              r,
              no);
}

// Call_Arg_Reg_Variable

void Call_Arg_Reg_Variable_Handler (inst_t inst) 
{
    Call_Arg_Reg_Variable(A1(char),A2(long));
}

void Inst_Call_Arg_Reg_Variable (char r,long no) 
{
    function_add_arg();
    Add_Inst2(call_arg_reg_variable,
              Call_Arg_Reg_Variable_Handler,
              r,
              no);
}

// Call_Arg_Cst

void Call_Arg_Cst_Handler (inst_t inst) 
{
    Call_Arg_Cst(A1(char),A2(long));
}

void Inst_Call_Arg_Cst (char cst,long val) 
{
    function_add_arg();
    Add_Inst2(call_arg_cst,
              Call_Arg_Cst_Handler,
              cst,
              val);
}


// Call_Arg_Label

void Call_Arg_Label_Handler (inst_t inst) 
{
    Call_Arg_Label(A1(symbol_t),A2(long));
}

void Inst_Call_Arg_Label (char *label,long no) 
{
    function_add_arg();
    Add_Inst2(call_arg_label,
              Call_Arg_Label_Handler,
              refer_symbol(label,Smb_Long),
              no);
}

// Call_Arg_Variable

void Call_Arg_Variable_Handler (inst_t inst) 
{
    Call_Arg_Variable(A1(symbol_t),A2(long));
}

void Inst_Call_Arg_Variable (char *label,long no) 
{
    function_add_arg();
    Add_Inst2(call_arg_variable,
              Call_Arg_Variable_Handler,
              refer_symbol(label,Smb_Long),
              no);
}

void Cache_Or_Build_Handler (inst_t inst) 
{
    Cache_Or_Build(A1(symbol_t), A2(long), A3(symbol_t));
}

void Inst_Cache_Or_Build (char *ident, long no, char *fct_name)
{
    Add_Inst3( cache_or_build,
               Cache_Or_Build_Handler,
               refer_symbol(ident,Smb_Long),
               no,
               refer_symbol(fct_name,Smb_C)
               );
}

          /* os_cpu.c variables */

extern int   call_c_reverse_args;

/*
 * Main and Argument Parsing
 */

#define L(msg)  fprintf(stderr,"%s\n",msg);

void 
Display_Help(void)
{
 L("Usage: dyam2ma [option...] [-|dyam_file]");
 L("Options:");
 L("   -o [-|ma_file] : name the mini-assembly file");
 L("   -g             : similar to gcc -g debug option");
 L("   -pg            : similar to gcc -pg profile option");
 L("   -help          : show this message and exit");
 L("   -version       : print version number and exit");
 L("   -fpic -fPIC    : similar to gcc -fPIC or -fpic options");
}

#undef L

static void 
Parse_Arguments(int argc,char *argv[])
{
 static
 char str[1024];
 int  i;
 
 file_name_in=file_name_out=NULL;

 for(i=1;i<argc;i++) {
     if (*argv[i]=='-' && argv[i][1]!='\0'){
         if (Check_Arg(i,"-o")){
             if (++i>=argc){
                 fprintf(stderr,"filename missing after -o option\n");
                 exit(1);
             }

             file_name_out=argv[i];
             continue;
         }
         
         if (Check_Arg(i,"-version")){
             fprintf(stderr,"Ma to Asm                               ");
             fprintf(stderr,"           Eric de la Clergerie - 1998\n");
             fprintf(stderr,"ma2asm version %s\n",MA2ASM_VERSION);
             exit(0);
         }

         if (Check_Arg(i,"-debug")) {
             yydebug=1;
             continue;
         }

         if (Check_Arg(i,"-pg")) {
             profile=1;
             continue;
         }

         if (Check_Arg(i,"-g")) {
             debug=1;
             continue;
         }

         if (Check_Arg(i,"-fpic") || Check_Arg(i,"-fPIC")) {
             pic=1;
             continue;
         }

         if (Check_Arg(i,"-help")){
             Display_Help();
             exit(0);
         }

         fprintf(stderr,"unknown option %s\n",argv[i]);
         exit(1);
     }

     if (file_name_in!=NULL){
         fprintf(stderr,"input file already specified (%s)\n",file_name_in);
         exit(1);
     }
     file_name_in=argv[i];
 }
 
 if (file_name_in!=NULL && strcmp(file_name_in,"-")==0)
     file_name_in=NULL;

 if (file_name_out==NULL && file_name_in!=NULL){
     strcpy(str,file_name_in);
     i=strlen(str);
     if (strcmp(str+i-3,".ma")==0)
         strcpy(str+i-3,DEFAULT_OUTPUT_SUFFIX);
     else
         strcpy(str+i,DEFAULT_OUTPUT_SUFFIX);
     file_name_out=str;
 }

 if (file_name_out!=NULL && strcmp(file_name_out,"-")==0)
     file_name_out=NULL;
}

int
main(int argc, char * argv[])
{
    FILE *in=0;
    auxbuf = buffer_new(BUFFER_SIZE);
    Parse_Arguments(argc,argv);

    if (file_name_in){
        if ((in=fopen(file_name_in,"r"))==NULL){
            fprintf(stderr,"cannot open input file %s\n",file_name_in);
            exit(1);
        }
        yyrestart(in);
    }
    
    if (file_name_out)
        if ((file_out=fopen(file_name_out,"w"))==NULL){
            fprintf(stderr,"cannot open output file %s\n",file_name_out);
            exit(1);
        }
    
    Asm_Start();
    
    yyparse();

    Inst_Loop();

    if (string_n>0) {
        Dico_String_Start(string_n);
        twalk(constant_tbl,Dico_String_Action);
        Dico_String_Stop(string_n); 
    }
    
    if (float_n>0) {
        Dico_Float_Start(float_n);
        twalk(constant_tbl,Dico_Float_Action);
        Dico_Float_Stop(float_n); 
    }

    if (long_n>0){
        Dico_Long_Start();
        twalk(symbol_tbl,Dico_Long_Action);
        Dico_Long_Stop();
    }
    
    Asm_Stop();

    if (in)
        fclose(in);
    if (file_out!=stdout)
        fclose(file_out);
 
    exit(0);
}

/**********************************************************************
 *  Emit
 **********************************************************************/

/*-------------------------------------------------------------------------*/
/* DATA_LONG                                                               */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Data_Long(char *name,int global,long length,InitLongList_t value)

{
 long prop = Smb_Defined | Smb_Long;
 prop |= global ? Smb_Global : Smb_Local;
 define_symbol(name,prop,length,value);
 long_n++;
}

/*-------------------------------------------------------------------------*/
/* INIT_LONG_LIST                                                          */
/*                                                                         */
/*-------------------------------------------------------------------------*/

InitLongList_t
Init_Long_Add(short type,long value,InitLongList_t next)
{
    InitLongList_t cell = (InitLongList_t) malloc(sizeof(InitLongList));
    cell->type=type;
    cell->value=value;
    cell->next=next;
    return cell;
}

/*-------------------------------------------------------------------------*/
/* COMMENT_PRINTF                                                          */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Comment_Printf(char *comment,...)
{
    va_list arg_ptr;
    
    va_start(arg_ptr,comment);

    fprintf(file_out,";; ");
    vfprintf(file_out,comment,arg_ptr);

    va_end(arg_ptr);
    fputc('\n',file_out);
}

/*-------------------------------------------------------------------------*/
/* LABEL_PRINTF                                                            */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Label_Printf(char *label,...)
{
    va_list arg_ptr;
    
    va_start(arg_ptr,label);

    vfprintf(file_out,label,arg_ptr);

    va_end(arg_ptr);
    fputc('\n',file_out);
}

/*-------------------------------------------------------------------------*/
/* INST_PRINTF                                                             */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Inst_Printf(char *op,char *operands,...)
{
    va_list arg_ptr;
    
    va_start(arg_ptr,operands);
    
    fprintf(file_out,"\t%s\t",op);
    vfprintf(file_out,operands,arg_ptr);
    
    va_end(arg_ptr);
    fputc('\n',file_out);
}

/*-------------------------------------------------------------------------*/
/* VINST_PRINTF                                                             */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void VInst_Printf(char *op, char *operands, va_list arg_ptr)
{
    fprintf(file_out,"\t%s\t",op);
    vfprintf(file_out,operands,arg_ptr);
    fputc('\n',file_out);
}


/*-------------------------------------------------------------------------*/
/* INST_OUT                                                                */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Inst_Out(char *op,char *operands)
{
    fprintf(file_out,"\t%s\t%s\n",op,operands);
}




