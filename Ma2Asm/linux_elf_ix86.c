#include <string.h>

/*---------------------------------*/
/* Constants                       */
/*---------------------------------*/

#define STRING_PREFIX              ".LC"

#define MAX_C_ARGS_IN_C_CODE       32

#define R_DATA 1
#define R_FUN  2

/*---------------------------------*/
/* Global Variables                */
/*---------------------------------*/

extern int kind_of_call;        /* cf parser.y */

char  asm_reg_bank[16];
char  asm_reg_e[16];
char  asm_reg_b[16];
char  asm_reg_cp[16];
char  asm_reg_p[16];

#define REG_VAR   0
#define REG_UP    1
#define REG_DOWN  2
#define REG_FLOAT 4

#define REG_KIND(_kind)    (_kind & 3)
#define REG_FLOATP(_kind)  (_kind & 4)

struct reg {
    char name;
    char asm_reg[16];
    int  base;
    int  mult;
    int  kind;                  /* 0: variable 1: growing 2: not growing */
}   registers[4];

int   w_label=0;

static int offset;

          /* variables for ma_parser.c */

int strings_need_null=0;


          /* variables for ma2asm.c */

int call_c_reverse_args=0;

/*----------------------------------------------------------------------
 * Handling Symbols
 *
 *----------------------------------------------------------------------
 */

symbol_t refer_symbol(char * label,long prop);
symbol_t define_symbol(char *label,long prop,ulong size,InitLongList_t init );

static symbol_t smb_trail;
static symbol_t smb_folvar_tab;
static symbol_t smb_follow_choice;

extern void *symbol_tbl;

/*-------------------------------------------------------------------------*/
/* STAB_LINE                                                               */
/*                                                                         */
/*-------------------------------------------------------------------------*/

static struct
{
    char *label;
    int  global;
    long  stab_label;
} fun_info = { 0, 0, 0 };
 
extern long yylineno;

#define curline (yylineno-5)

static long stab_label=0;

void Stab_Line()
{
    Inst_Printf(".stabn","68,0,%u,.LL%d-%s",curline,stab_label,fun_info.label);
    Label_Printf(".LL%u:",stab_label++);
}


/*-------------------------------------------------------------------------*/
/* STAB_FUNCTION                                                           */
/*                                                                         */
/*-------------------------------------------------------------------------*/

void Stab_Function(char *label,int global)
{
   
    Label_Printf("");
    Inst_Printf(".stabs","\"%s:%c1\",36,0,%u,%s",
                label, global ? 'F' : 'f', curline, label
                );
    fun_info.label=label;
    fun_info.global=global;
    fun_info.stab_label=stab_label;
}

/*-------------------------------------------------------------------------*/
/* SOURCE_LINE                                                             */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Source_Line(int line_no,char *cmt)
{
 Label_Printf("\t# %6d: %s",line_no,cmt);
}

/*-------------------------------------------------------------------------*/
/* FIND_REGISTER_INFO                                                               */
/*                                                                         */
/*-------------------------------------------------------------------------*/
static
struct reg * Find_Register_Info( char name )
{
    struct reg *reg;
    for(reg=registers; reg->name != name ; reg++);
    return reg;
}

extern char *file_name_in;

/*-------------------------------------------------------------------------*/
/* ASM_START                                                               */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Asm_Start(void) {

    smb_trail = refer_symbol("trail",Smb_Long);
    smb_folvar_tab = refer_symbol("folvar_tab",Smb_Long);
    smb_follow_choice = refer_symbol("follow_choice",Smb_C);
    
    sprintf(asm_reg_e,"trail+%d",I_E*4);
    sprintf(asm_reg_b,"trail+%d",I_B*4);
    sprintf(asm_reg_cp,"trail+%d",I_CP*4);
    sprintf(asm_reg_p,"trail+%d",I_P*4);

    registers[0].name = 'Y';
    strcpy(registers[0].asm_reg,asm_reg_e);
    registers[0].base=4;
    registers[0].mult=4;
    registers[0].kind=REG_DOWN;
    
    registers[1].name = 'R';
    strcpy(registers[1].asm_reg,"trail");
    registers[1].base=NB_SYSTEM_REGISTERS;
    registers[1].mult=4;
    registers[1].kind=REG_VAR;

    registers[2].name = 'S';
    strcpy(registers[2].asm_reg,"trail");
    registers[2].base=0;
    registers[2].mult=4;
    registers[2].kind=REG_VAR;

    registers[3].name = 'T';
    strcpy(registers[3].asm_reg,"trail");
    registers[3].base=NB_SYSTEM_REGISTERS;
    registers[3].mult=4;
    registers[3].kind=REG_VAR | REG_FLOAT;

    Inst_Printf(".stabs","\"%s\",100,0,0,Ltext0",file_name_in);
    Label_Printf(".text");
    Label_Printf("Ltext0:");
    Inst_Printf(".stabs","\"void:t1=15\",128,0,0,0");
    Inst_Label("fail");
    Inst_Pl_Fail();
}

/*-------------------------------------------------------------------------*/
/* ASM_STOP                                                                */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Asm_Stop(void)
{
}

/*-------------------------------------------------------------------------*/
/* LABEL                                                                   */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void File_Name(char *file_name)
{
    Inst_Printf(".file","\"%s\"",file_name);
}

/*-------------------------------------------------------------------------*/
/* LABEL                                                                   */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Label(symbol_t smb)
{
 Label_Printf("\n%s:",smb->label);
}

/*-------------------------------------------------------------------------*/
/* CODE_START                                                              */
/*                                                                         */
/*-------------------------------------------------------------------------*/

void Code_Start(symbol_t smb)
{
    long global = smb->prop & Smb_Global;
    long prolog = smb->prop & Smb_Prolog;
    char *label = smb->label;

    if (debug) Stab_Function(label,global);
   
    Label_Printf("");
    Inst_Printf(".align","16");
    Inst_Printf(".type","%s,@function",label);

    if (global)
        Inst_Printf(".globl","%s",label);

    Label_Printf("\n%s:", label);

    if (debug) Stab_Line();
        
    if (!prolog) {
        Inst_Printf("pushl","%%ebp");
        Inst_Printf("movl","%%esp,%%ebp");
        Inst_Printf("subl","$%u,%%esp",MAX_C_ARGS_IN_C_CODE*4);
    }

    if (profile) {
        Label_Printf(".data");
        Inst_Printf(".align","4");
        Label_Printf(".Lprofile%u:",w_label);
        Inst_Printf(".long","0");
        Label_Printf(".text");
        Inst_Printf("movl","$.Lprofile%u,%%edx",w_label++);
        Inst_Printf("call","mcount");
    }
     
}

/*-------------------------------------------------------------------------*/
/* CODE_STOP                                                               */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Code_Stop(void)
{
}

/*-------------------------------------------------------------------------*/
/* PL_JUMP                                                                 */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Pl_Jump(symbol_t smb)
{
    Inst_Printf("jmp","%s",smb->label);
}

/*-------------------------------------------------------------------------*/
/* JUMP_NOT_ZERO                                                                 */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Jump_Not_Zero(symbol_t smb)
{
    Inst_Printf("testl","%%eax,%%eax");
    Inst_Printf("jne","%s",smb->label);
}

/*-------------------------------------------------------------------------*/
/* RET_NOT_ZERO                                                                 */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Ret_Not_Zero(void)
{
    Inst_Printf("testl","%%eax,%%eax");
    Inst_Printf("je",".Lcont%u",w_label);
    C_Ret();
    Label_Printf(".Lcont%u:",w_label++);
}

/*-------------------------------------------------------------------------*/
/* PL_JUMP_REG                                                             */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Pl_Jump_Reg(char name, long no)
{
    Move_From_Reg(name,no);
    Inst_Printf("jmp","*%s","%eax");
}

/*-------------------------------------------------------------------------*/
/* PL_JUMP_STAR                                                            */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Pl_Jump_Star()
{
    Inst_Printf("jmp","*%s",asm_reg_p);
}

/*-------------------------------------------------------------------------*/
/* PL_FAIL                                                                 */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Pl_Fail(void)
{
//    Inst_Printf("call","untrail_choice");
    Inst_Printf("call","follow_choice");
    Inst_Printf("jmp","*%s",asm_reg_p);    
}

/*-------------------------------------------------------------------------*/
/* PL_RET                                                                  */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Pl_Ret(void)
{
 Inst_Printf("jmp","*%s",asm_reg_cp);
}

/*-------------------------------------------------------------------------*/
/* JUMP                                                                    */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Jump(symbol_t smb)
{
 Inst_Printf("jmp","%s",smb->label);
}

/*-------------------------------------------------------------------------*/
/* MOVE_FROM_REG                                                         */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Move_From_Reg(char name, long no)
{
    struct reg *reg=Find_Register_Info(name);
    int n = (no+reg->base)*reg->mult;
    int floatp = REG_FLOATP(reg->kind);
    switch (REG_KIND(reg->kind)) {
        case REG_VAR:
            if (floatp)
                Inst_Printf("fldl","%s+%u",reg->asm_reg,n);
            else
                Inst_Printf("movl","%s+%u,%%eax",reg->asm_reg,n);
            break;
        case REG_UP:
            if (floatp)
                Inst_Printf("fldl","%u(%s)",n,reg->asm_reg);
            else
                Inst_Printf("movl","%u(%s),%%eax",n,reg->asm_reg);            
            break;
        case REG_DOWN:
            if (floatp)
                Inst_Printf("fldl","-%u(%s),%%eax",n,reg->asm_reg);
            else
                Inst_Printf("movl","-%u(%s),%%eax",n,reg->asm_reg);
            break;
    }
}


/*-------------------------------------------------------------------------*/
/* MOVE_TO_REG                                                           */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Move_To_Reg(char name, long no)
{
    struct reg *reg=Find_Register_Info(name);
    int n = (no+reg->base)*reg->mult;
    int floatp = REG_FLOATP(reg->kind);
    switch (REG_KIND(reg->kind)) {
        case REG_VAR:
            if (floatp)
                Inst_Printf("fstpl","%s+%u",reg->asm_reg,n);
            else
                Inst_Printf("movl","%%eax,%s+%u",reg->asm_reg,n);
            break;
        case REG_UP:
             if (floatp)
                 Inst_Printf("fstpl","%u(%s)",n,reg->asm_reg);
             else
                 Inst_Printf("movl","%%eax,%u(%s)",n,reg->asm_reg);            
            break;
        case REG_DOWN:
            if (floatp)
                Inst_Printf("fstpl","-%u(%s)",n,reg->asm_reg);
            else
                Inst_Printf("movl","%%eax,-%u(%s)",n,reg->asm_reg);            
            break;
    }
}

/*-------------------------------------------------------------------------*/
/* MOVE_FLOAT_TO_REG                                                           */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Move_Float_To_Reg(char name, long no)
{
    struct reg *reg=Find_Register_Info(name);
    int n = (no+reg->base)*reg->mult;
    switch (REG_KIND(reg->kind)) {
        case REG_VAR:
            Inst_Printf("fstps","%s+%u",reg->asm_reg,n);
            break;
        case REG_UP:
            Inst_Printf("fstps","%u(%s)",n,reg->asm_reg);            
            break;
        case REG_DOWN:
            Inst_Printf("fstps","-%u(%s)",n,reg->asm_reg);            
            break;
    }
}

/*-------------------------------------------------------------------------*/
/* MOVE_INT_TO_REG                                                           */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Move_Int_To_Reg(long int_val, char name, long no)
{
    struct reg *reg=Find_Register_Info(name);
    int n = (no+reg->base)*reg->mult;
    switch (REG_KIND(reg->kind)) {
        case REG_VAR:
            Inst_Printf("movl","$%d,%s+%d",int_val,reg->asm_reg,n);
            break;
        case REG_UP:
            Inst_Printf("movl","$%d,%d(%s)",int_val,n,reg->asm_reg);            
            break;
        case REG_DOWN:
            Inst_Printf("movl","$%d,-%d(%s)",int_val,n,reg->asm_reg);            
            break;
    }
}

/*-------------------------------------------------------------------------*/
/* MOVE_INT_TO_IDENT                                                           */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Move_Int_To_Ident(long int_val, symbol_t smb, long no)
{
    char *ident = smb->label;
    if (no==0) {
        Inst_Printf("movl","$%d,%s",int_val,ident); 
    } else {
        Inst_Printf("movl","$%d,%s+%d",int_val,ident,4*no);
    }
}

/*-------------------------------------------------------------------------*/
/* MOVE_CST_TO_REG                                                           */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Move_Cst_To_Reg(char cst_kind, long cst_val, char name, long no)
{
    switch (cst_kind) {
        case 'I':
            Move_Int_To_Reg((int)FOLSMB_MAKE(cst_val,0),name,no);
            break;
        case 'N':
            Move_Int_To_Reg((int)DFOLINT(cst_val),name,no);
            break;
        case 'F':
            Move_Int_To_Reg((int)DFOLFLT(long2float(cst_val)),name,no);
            break;
        case 'C':
            Move_Int_To_Reg((int)DFOLCHAR(cst_val),name,no);
            break;
        case 'V':
            Inst_Printf("movl","$folvar_tab+%d,%%eax",cst_val*sizeof(struct folvar));
            Inst_Printf("orb","$%d,%%al",TAG_VAR);
            Move_To_Reg(name,no);
            break;
        default:
            fprintf(stderr,"Not a valid Cst constructor %c\n",cst_kind);
            exit(-1);
            break;
    }
}

/*-------------------------------------------------------------------------*/
/* MOVE_CST_TO_IDENT                                                           */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Move_Cst_To_Ident(char cst_kind, long cst_val, symbol_t smb, long no)
{
    switch (cst_kind) {
        case 'I':
            Move_Int_To_Ident((int)FOLSMB_MAKE(cst_val,0),smb,no);
            break;
        case 'N':
            Move_Int_To_Ident((int)DFOLINT(cst_val),smb,no);
            break;
        case 'F':
            Move_Int_To_Ident((int)DFOLFLT(long2float(cst_val)),smb,no);
            break;
        case 'C':
            Move_Int_To_Ident((int)DFOLCHAR(cst_val),smb,no);
            break;
        case 'V':
            Inst_Printf("movl","$folvar_tab+%d,%%eax",cst_val*sizeof(struct folvar));
            Inst_Printf("orb","$%d,%%al",TAG_VAR);
            Move_Ret_To_Ident(smb,no);
            break;
        default:
            fprintf(stderr,"Not a valid Cst constructor %c\n",cst_kind);
            exit(-1);
            break;
    }
}

/*-------------------------------------------------------------------------*/
/* PL_CALL_START                                                           */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Pl_Call_Start(symbol_t smb)
{
    offset = 0;
    kind_of_call = 1;
}

/*-------------------------------------------------------------------------*/
/* PL_CALL_STOP                                                            */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Pl_Call_Stop(symbol_t smb)
{
    Inst_Printf("movl","$.Lcont%u,%s",w_label,asm_reg_cp);
    Pl_Jump(smb);
    Label_Printf(".Lcont%u:",w_label++);
}

/*-------------------------------------------------------------------------*/
/* PL_CALL_REG                                                             */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Pl_Call_Reg(char name, long no)
{
    Inst_Printf("movl","$.Lcont%u,%s",w_label,asm_reg_cp);
    Pl_Jump_Reg(name,no);
    Label_Printf(".Lcont%u:",w_label++);
}

/*-------------------------------------------------------------------------*/
/* PL_CALL_STAR                                                            */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Pl_Call_Star()
{
    Inst_Printf("movl","$.Lcont%u,%s",w_label,asm_reg_cp);
    Pl_Jump_Star();
    Label_Printf(".Lcont%u:",w_label++);
}

/*-------------------------------------------------------------------------*/
/* CALL_C_START                                                            */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Call_C_Start(symbol_t smb,
                  unsigned long nb_args,
                  unsigned long nb_float_args)
{
    offset=0;
    kind_of_call=0;
}

/*-------------------------------------------------------------------------*/
/* CALL_C_ARG_LABEL                                                        */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Call_Arg_Label(symbol_t smb,long no)
{
    char *label = smb->label;
    switch (kind_of_call) {
        case 0:                 /* C */
            if (no==0) {
                Inst_Printf("movl","$%s,%u(%%esp)",label,offset*4); 
            } else {
                Inst_Printf("movl","$%s+%u,%u(%%esp)",label,no*4,offset*4); 
            }
            break;
        case 1: {                /* PROLOG */
            struct reg *reg=Find_Register_Info('R');
            int n = (offset+reg->base)*reg->mult;
            Inst_Printf("movl","$%s+%u,%s+%u",label,no*4,reg->asm_reg,n);
            break;
        }
    }
    offset++;
}

/*-------------------------------------------------------------------------*/
/* READ_VARIABLE                                                     */
/*                                                                         */
/*-------------------------------------------------------------------------*/

void Read_Variable(symbol_t smb,long no)
{
    char *ident = smb->label;
    if (no==0) {
        Inst_Printf("movl","%s,%%eax",ident); 
    } else {
        Inst_Printf("movl","%s+%u,%%eax",ident,no*4); 
    }
}

/*-------------------------------------------------------------------------*/
/* CALL_C_ARG_VARIABLE                                                     */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Call_Arg_Variable(symbol_t smb,long no)
{
    Read_Variable(smb,no);
    switch (kind_of_call) {
        case 0:                 /* C */
            Inst_Printf("movl","%%eax,%u(%%esp)",offset*4);
            break;
        case 1:                 /* PROLOG */
            Move_To_Reg('R',offset);
            break;
    }
    offset++;
}

/*-------------------------------------------------------------------------*/
/* CALL_C_ARG_INT                                                          */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Call_Arg_Int(long int_val)
{
    switch (kind_of_call) {
        case 0:                 /* C */
            Inst_Printf("movl","$%d,%u(%%esp)",int_val,offset*4);
            break;
        case 1: {                /* PROLOG */
            struct reg *reg=Find_Register_Info('R');
            int n = (offset+reg->base)*reg->mult;
            Inst_Printf("movl","$%d,%s+%u",int_val,reg->asm_reg,n);
            break;
        }
    }
    offset++;
}

/*-------------------------------------------------------------------------*/
/* CALL_C_ARG_FLOAT                                                        */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Call_Arg_Float(constant_t cst)
{
    float flt_val = cst->value.f;
    double flt_double = (double) flt_val;
    int *p=(int *) &flt_double;
    
    Inst_Printf("movl","$%d,%u(%%esp)",p[0],offset*4);
    Inst_Printf("movl","$%d,%u(%%esp)",p[1],(offset+1)*4);

    offset += 2;
}


/*-------------------------------------------------------------------------*/
/* CALL_C_ARG_DOUBLE                                                       */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Call_Arg_Double(constant_t cst)
{
    double dbl_val=cst->value.f;
    int *p=(int *) &dbl_val;

    Inst_Printf("movl","$%d,%u(%%esp)",p[0],offset*4);
    Inst_Printf("movl","$%d,%u(%%esp)",p[1],(offset+1)*4);
    
    offset += 2;
}

/*-------------------------------------------------------------------------*/
/* CALL_C_ARG_STRING                                                       */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Call_Arg_String(constant_t cst)
{
    long str_no = cst->id;
    switch (kind_of_call) {
        case 0:                 /* C */
            Inst_Printf("movl","$%s%u,%u(%%esp)",STRING_PREFIX,str_no,offset*4);
            break;
        case 1: {                /* PROLOG */
            struct reg *reg=Find_Register_Info('R');
            int n = (offset+reg->base)*reg->mult;
            Inst_Printf("movl","$%s%u,%s+%u",STRING_PREFIX,str_no,reg->asm_reg,n);
            break;
        }
    }
    offset++;
}

/*-------------------------------------------------------------------------*/
/*  CALL_C_ARG_REG                                                         */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Call_Arg_Reg(char name, long no)
{
    struct reg *reg=Find_Register_Info(name);
    int floatp = REG_FLOATP(reg->kind);
    int doffset = floatp ? 2 : 1;
    switch (kind_of_call) {
        case 0:                 /* C */
            Move_From_Reg(name,no);
            if (floatp)
                Inst_Printf("fstpl","%u(%%esp)",offset*4);
            else
                Inst_Printf("movl","%%eax,%u(%%esp)",offset*4);
            break;
        case 1: {                /* PROLOG */
            Move_From_Reg(name,no);
            Move_To_Reg('R',offset);
            break;
        }
    }
    offset += doffset;
}

/*-------------------------------------------------------------------------*/
/* CALL_C_ARG_REG_VARIABLE                                                 */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Call_Arg_Reg_Variable(char name, long no)
{
    struct reg *reg=Find_Register_Info(name);
    int n = (no+reg->base)*reg->mult;
    int floatp = REG_FLOATP(reg->kind);
    int doffset = floatp ? 2 : 1;
    if (n==0) {
        switch (kind_of_call) {
            case 0:                 /* C */
                if (floatp) {
                    Inst_Printf("fldl","%s",reg->asm_reg);
                    Inst_Printf("fstpl","%u(%%esp)",offset*4);
                } else
                    Inst_Printf("movl","%s,%u(%%esp)",reg->asm_reg,offset*4);
                break;
            case 1: {                /* PROLOG */
                struct reg *reg_R=Find_Register_Info('R');
                int n_R = (offset+reg->base)*reg->mult;
                Inst_Printf("movl","%s,%s+%u",reg->asm_reg,reg_R->asm_reg,n_R);
                break;
            }
        }
    } else {
        switch (REG_KIND(reg->kind)) {
            case REG_VAR:
                if (floatp)
                    Inst_Printf("fldl","%s+%u",reg->asm_reg,n);
                else
                    Inst_Printf("leal","%s+%u,%%eax",reg->asm_reg,n);
                break;
            case REG_UP:
                if (floatp)
                    Inst_Printf("fldl","%u(%s)",n,reg->asm_reg);
                else
                    Inst_Printf("leal","%u(%s),%%eax",n,reg->asm_reg);            
                break;
            case REG_DOWN:
                if (floatp)
                    Inst_Printf("fldl","-%u(%s)",n,reg->asm_reg);
                else
                    Inst_Printf("leal","-%u(%s),%%eax",n,reg->asm_reg);            
                break;
        }
        switch (kind_of_call) {
            case 0:                 /* C */
                if (floatp) 
                    Inst_Printf("fstpl","%u(%%esp)",offset*4);
                else
                    Inst_Printf("movl","%%eax,%u(%%esp)",offset*4);
                break;
            case 1: {                /* PROLOG */
                struct reg *reg_R=Find_Register_Info('R');
                int n_R = (offset+reg->base)*reg->mult;
                Inst_Printf("movl","%%eax,%s+%u",reg_R->asm_reg,n_R);
                break;
            }
        }
        
    }
    offset += doffset;
}

/*-------------------------------------------------------------------------*/
/* CALL_C_ARG_CST                                                          */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Call_Arg_Cst(char name, long no)
{
    fol_t t;
    switch (kind_of_call) {
        case 0:                 /* C */
            switch (name) {
                case 'I':
                    t=FOLSMB_MAKE(no,0);
                    Inst_Printf("movl","$%d,%u(%%esp)",(int)t,offset*4);            
                    break;
                case 'N':
                    t=DFOLINT(no);
                    Inst_Printf("movl","$%d,%u(%%esp)",(int)t,offset*4);            
                    break;
                case 'F':
                    t=DFOLFLT(long2float(no));
                    Inst_Printf("movl","$%d,%u(%%esp)",(int)t,offset*4);            
                    break;
                case 'C':
                    t=DFOLCHAR(no);
                    Inst_Printf("movl","$%d,%u(%%esp)",(int)t,offset*4);            
                    break;
                case 'V':
                    Inst_Printf("movl","$folvar_tab+%u,%%eax",no*sizeof(struct folvar));
                    Inst_Printf("orb","$%d,%%al",TAG_VAR);
                    Inst_Printf("movl","%%eax,%u(%%esp)",offset*4);            
                    break;
                default:
                    fprintf(stderr,"Not a valid Cst constructor %c\n",name);
                    exit(-1);
                    break;
            }
            break;
        case 1: {               /* PROLOG */
            struct reg *reg=Find_Register_Info('R');
            int n = (offset+reg->base)*reg->mult;
            switch (name) {
                case 'I':
                    t=FOLSMB_MAKE(no,0);
                    Inst_Printf("movl","$%d,%s+%u",(int)t,reg->asm_reg,n);            
                    break;
                case 'N':
                    t=DFOLINT(no);
                    Inst_Printf("movl","$%d,%s+%u",(int)t,reg->asm_reg,n);            
                    break;
                case 'F':
                    t=DFOLFLT(long2float(no));
                    Inst_Printf("movl","$%d,%s+%u",(int)t,reg->asm_reg,n);            
                    break;
                case 'C':
                    t=DFOLCHAR(no);
                    Inst_Printf("movl","$%d,%s+%u",(int)t,reg->asm_reg,n);
                    break;
                case 'V':
                    Inst_Printf("movl","$folvar_tab+%u,%%eax",no*sizeof(struct folvar));
                    Inst_Printf("orb","$%d,%%al",TAG_VAR);
                    Inst_Printf("movl","%%eax,%s+%u",reg->asm_reg,n);            
                    break;
                default:
                    fprintf(stderr,"Not a valid Cst constructor %c\n",name);
                    exit(-1);
                    break;
            }
            break;
        }
    }
    offset++;
}

/*-------------------------------------------------------------------------*/
/* CALL_C_ARG_PARAM                                                        */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Call_Arg_Param(long no)
{
    Inst_Printf("movl","%u(%%ebp),%%eax",(no+1)*4);
    switch (kind_of_call) {
        case 0:                 /* C */
            Inst_Printf("movl","%%eax,%u(%%esp)",offset*4);
            break;
        case 1:                 /* PROLOG */
            Move_To_Reg('R',offset);
            break;
    }

    offset++;
}

/*-------------------------------------------------------------------------*/
/* CALL_C_STOP                                                             */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Call_C_Stop(symbol_t smb,
                 unsigned long nb_args,
                 unsigned long nb_float_args)
{
    char *fct_name = smb->label;
    Inst_Printf("call","%s",fct_name);
}

/*-------------------------------------------------------------------------*/
/* JUMP_RET                                                                */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Jump_Ret(void)
{
    Inst_Printf("jmp","*%%eax");
}

/*-------------------------------------------------------------------------*/
/* FAIL_RET                                                                */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Fail_Ret(void)
{
 Inst_Printf("testl","%%eax,%%eax");
 Inst_Printf("je","%s","fail");
}

/*-------------------------------------------------------------------------*/
/* TRUE_RET                                                                */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void True_Ret(void)
{
 Inst_Printf("testl","%%eax,%%eax");
 Inst_Printf("jne","%s","fail");
}

/*-------------------------------------------------------------------------*/
/* FAIL_C_RET                                                                */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Fail_C_Ret(void)
{
 Inst_Printf("testl","%%eax,%%eax");
 Inst_Printf("jne",".Lcont%u",w_label);
 C_Ret();
 Label_Printf(".Lcont%u:",w_label++);
}

/*-------------------------------------------------------------------------*/
/* TRUE_C_RET                                                                */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void True_C_Ret(void)
{
 Inst_Printf("testl","%%eax,%%eax");
 Inst_Printf("je",".Lcont%u",w_label);
 C_Ret();
 Label_Printf(".Lcont%u:",w_label++);
}

/*-------------------------------------------------------------------------*/
/* MOVE_RET_TO_IDENT                                                       */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Move_Ret_To_Ident(symbol_t smb,long no)
{
    char *ident = smb->label;
    if (no==0) {
        Inst_Printf("movl","%%eax,%s",ident); 
    } else {
        Inst_Printf("movl","%%eax,%s+%d",ident,4*no);
    }
}

/*-------------------------------------------------------------------------*/
/* MOVE_RET_TO_REG                                                         */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Move_Ret_To_Reg(char name, long no)
{
    Move_To_Reg(name,no);
}

/*-------------------------------------------------------------------------*/
/* C_RET                                                                   */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void C_Ret(void)
{
 Inst_Printf("leave","");
 Inst_Printf("ret","");
}

/*-------------------------------------------------------------------------*/
/* CACHE_OR_BUILD                                                          */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Cache_Or_Build(symbol_t smb,long no,symbol_t fun)
{
        /* not yet fully implemented */
    Inst_Printf("call","%s",fun->label);
}

/*-------------------------------------------------------------------------*/
/* DICO_STRING_START                                                       */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Dico_String_Start(long nb_consts)
{
    Label_Printf(".section\t.rodata");
}

/*-------------------------------------------------------------------------*/
/* DICO_STRING                                                             */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Dico_String(long str_no,char *asciiz)
{
    Label_Printf("%s%u:",STRING_PREFIX,str_no);
    Inst_Printf(".string","\"%s\"",asciiz);
}

/*-------------------------------------------------------------------------*/
/* DICO_STRING_STOP                                                        */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Dico_String_Stop(long nb_consts)
{
}

/*-------------------------------------------------------------------------*/
/* DICO_Float_START                                                       */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Dico_Float_Start(long nb_consts)
{
}

/*-------------------------------------------------------------------------*/
/* DICO_FLOAT                                                             */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Dico_Float(long no,float flt_val)
{
}

/*-------------------------------------------------------------------------*/
/* DICO_FLOAT_STOP                                                        */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Dico_Float_Stop(long nb_consts)
{
}

/*-------------------------------------------------------------------------*/
/* DICO_LONG_START                                                         */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Dico_Long_Start(long nb_longs)
{
    Label_Printf(".data");
    Inst_Printf(".align","4");
}

/*-------------------------------------------------------------------------*/
/* DICO_LONG                                                               */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Dico_Long(symbol_t smb)

{
    char *name = smb->label;
    long global = smb->prop & Smb_Global;
    long length = smb->size;
    InitLongList_t value = smb->init;
    if (!global)
        Inst_Printf(".local","%s",name);
    if (value){
        Inst_Printf(".type","%s,@object",name);
        if (!length)
            length = 1;
        Inst_Printf(".size","%s,%u",name, 4*length);
        Label_Printf("%s:",name);
        for(; value && length ; value = value->next, length--) {
            if (value->type==0) {
                Inst_Printf(".long","%s",(char *)value->value);
            } else {
                Inst_Printf(".long","%d",value->value);
            }
        }
        if (value) {            /* list too long  */
            fprintf(stderr,"Init Long List too long for ident %s\n",name);
            exit(1);            
        }
    } else {
        Inst_Printf(".comm","%s,%lu,4",name,(unsigned long)(length ?  4*length : 4)); 
    }
}

/*-------------------------------------------------------------------------*/
/* DICO_LONG_STOP                                                          */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Dico_Long_Stop(int nb_longs)
{
}



