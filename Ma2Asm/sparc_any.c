/*---------------------------------*/
/* Constants                       */
/*---------------------------------*/

/*---------------------------------*/
/* Constants                       */
/*---------------------------------*/

#define STRING_PREFIX              ".LC"

#ifdef M_sparc_sunos

#define UN                         "_"

#else

#define UN

#endif

#define MAX_C_ARGS_IN_C_CODE       32

/*---------------------------------*/
/* Global Variables                */
/*---------------------------------*/

extern int kind_of_call;        /* cf parser.y */

char  asm_reg_bank[16];
char  asm_reg_e[16];
char  asm_reg_b[16];
char  asm_reg_cp[16];
char  asm_reg_p[16];

struct reg {
    char name;
    char asm_reg[16];
    int  base;
    int  mult;
    int  kind;                  /* 0: variable 1: growing 2: not growing */
}   registers[3];

int   w_label=0;

          /* variables for ma_parser.c */

int strings_need_null=0;


          /* variables for ma2asm.c */

int call_c_reverse_args=0;

char *delay_op;
char  delay_operands[64];

/*---------------------------------*/
/* Function Prototypes             */
/*---------------------------------*/

void      Delay_Printf          (char *op,char *operands,...);
void      Flush_Printf          (char *op,char *operands,...);
void      Immed_Printf          (char *op,char *operands,...);

#define LITTLE_INT(int_val)     ((unsigned) ((int_val)+4096) < 8192)

/*-------------------------------------------------------------------------*/
/* STAB_LINE                                                               */
/*                                                                         */
/*-------------------------------------------------------------------------*/

static struct
{
    char *label;
    int  global;
    long  stab_label;
} fun_info = { 0, 0, 0 };

extern long yylineno;

#define curline (yylineno-5)

static long stab_label=0;

void Stab_Line()
{
    Immed_Printf(".stabn","68,0,%d,.LL%d-" UN "%s",curline,stab_label,fun_info.label);
    Label_Printf(".LL%d:",stab_label++);
}


/*-------------------------------------------------------------------------*/
/* Load_Label_In_Machine_Register                                          */
/*                                                                         */
/*-------------------------------------------------------------------------*/

void Load_Label_In_Machine_Register(char *name,char *label, int no)
{
    Delay_Printf("sethi","%%hi(" UN "%s+%d),%s",label,no,name);
    Delay_Printf("or","%s,%%lo(" UN "%s+%d),%s",name,label,no,name);
}

/*-------------------------------------------------------------------------*/
/* Load_Label_In_Machine_Register                                          */
/*                                                                         */
/*-------------------------------------------------------------------------*/

void Load_Value_In_Machine_Register(char *name,char *label, int no)
{
    Delay_Printf("sethi","%%hi(" UN "%s+%d),%s",label,no,name);
    Delay_Printf("ld","[%s+%%lo(" UN "%s+%d)],%s",name,label,no,name);
}

/*-------------------------------------------------------------------------*/
/* Load_Int_In_Machine_Register                                          */
/*                                                                         */
/*-------------------------------------------------------------------------*/

void Load_Int_In_Machine_Register(char *name,int int_val)
{
    if (LITTLE_INT(int_val))
        Delay_Printf("mov","%d,%s",int_val,name);
    else {
        Delay_Printf("sethi","%%hi(%d),%s",int_val,name);
        Delay_Printf("or","%s,%%lo(%d),%s",name,int_val,name);
    }
}

/*-------------------------------------------------------------------------*/
/* STAB_FUNCTION                                                           */
/*                                                                         */
/*-------------------------------------------------------------------------*/

void Stab_Function(char *label,int global)
{
    Label_Printf("");
    Immed_Printf(".stabs","\"%s:%c1\",36,0,%d," UN "%s",
                label, global ? 'F' : 'f', curline, label
                );
    fun_info.label=label;
    fun_info.global=global;
    fun_info.stab_label=stab_label;
}

/*-------------------------------------------------------------------------*/
/* SOURCE_LINE                                                             */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Source_Line(int line_no,char *cmt)
{
    Label_Printf("\t! %6d: %s",line_no,cmt);
}

/*-------------------------------------------------------------------------*/
/* FIND_REGISTER_INFO                                                               */
/*                                                                         */
/*-------------------------------------------------------------------------*/
static
struct reg * Find_Register_Info( char name )
{
    struct reg *reg;
    for(reg=registers; reg->name != name ; reg++);
    return reg;
}

extern char *file_name_in;

/*-------------------------------------------------------------------------*/
/* ASM_START                                                               */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Asm_Start(void) {

    sprintf(asm_reg_bank,"%%%s",MAP_REG_BANK);

    sprintf(asm_reg_e,"%s+%d",asm_reg_bank,I_E*4);
    sprintf(asm_reg_b,"%s+%d",asm_reg_bank,I_B*4);
    sprintf(asm_reg_cp,"%s+%d",asm_reg_bank,I_CP*4);
    sprintf(asm_reg_p,"%s+%d",asm_reg_bank,I_P*4);

    registers[0].name = 'Y';
    strcpy(registers[0].asm_reg,asm_reg_e);
    registers[0].base=4;
    registers[0].mult=4;
    registers[0].kind=2;
    
    registers[1].name = 'R';
    strcpy(registers[1].asm_reg,asm_reg_bank);
    registers[1].base=NB_SYSTEM_REGISTERS;
    registers[1].mult=4;
    registers[1].kind=0;

    registers[2].name = 'S';
    strcpy(registers[2].asm_reg,asm_reg_bank);
    registers[2].base=0;
    registers[2].mult=4;
    registers[2].kind=0;

    
    Inst_Printf(".stabs","\"%s\",100,0,0,Ltext0",file_name_in);
    Label_Printf(".text");
    Label_Printf("Ltext0:");
    Inst_Printf(".stabs","\"void:t1=15\",128,0,0,0");
    Label("fail");
    Pl_Fail();
}

/*-------------------------------------------------------------------------*/
/* ASM_STOP                                                                */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Asm_Stop(void)
{
}

/*-------------------------------------------------------------------------*/
/* LABEL                                                                   */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void File_Name(char *file_name)
{
    Inst_Printf(".file","\"%s\"",file_name);
}

/*-------------------------------------------------------------------------*/
/* LABEL                                                                   */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Label(char *label)
{
    Label_Printf("\n" UN "%s:",label);
}

/*-------------------------------------------------------------------------*/
/* CODE_START                                                              */
/*                                                                         */
/*-------------------------------------------------------------------------*/

void Code_Start(char *label,int prolog,int global)
{

    if (debug) Stab_Function(label,global);
    
    Label_Printf("");
    Inst_Printf(".align","4");
#ifdef M_sparc_solaris
    Inst_Printf(".type", UN "%s,#function",label);
#endif
    if (global)
      Inst_Printf(".globl",UN "%s",label);
    Inst_Printf(".proc","020");

    Label(label);
    if (debug) Stab_Line();
        
    if (!prolog) {
        Inst_Printf("save","%%sp,-104,%%sp");
    }
}

/*-------------------------------------------------------------------------*/
/* CODE_STOP                                                               */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Code_Stop(void)
{
}

/*-------------------------------------------------------------------------*/
/* PL_JUMP                                                                 */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Pl_Jump(char *label)
{
    Flush_Printf("call",UN "%s,0",label);
}

/*-------------------------------------------------------------------------*/
/* JUMP_NOT_ZERO                                                                 */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Jump_Not_Zero(char *label)
{
    Immed_Printf("cmp","%%o0,0");
    Flush_Printf("bne",UN "%s",label);
}

/*-------------------------------------------------------------------------*/
/* FAIL_C_RET                                                                */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Ret_Not_Zero(void)
{
    Immed_Printf("cmp","%%o0,0");
    Flush_Printf("be",".Lcont%d",w_label);
    C_Ret();
    Label_Printf(".Lcont%d:",w_label++);
}

/*-------------------------------------------------------------------------*/
/* PL_JUMP_REG                                                             */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Pl_Jump_Reg(char name, int no)
{
    Move_From_Reg(name,no);
    Delay_Printf("nop","");
    Flush_Printf("jmp","%%o0+8");
}

/*-------------------------------------------------------------------------*/
/* PL_JUMP_STAR                                                            */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Pl_Jump_Star()
{
  Immed_Printf("ld","[%s],%%o0",asm_reg_p);
  Flush_Printf("jmp","%%o0+8");
}

/*-------------------------------------------------------------------------*/
/* PL_FAIL                                                                 */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Pl_Fail(void)
{
    Flush_Printf("call",UN "untrail_choice,0");
    Pl_Jump_Star();
}

/*-------------------------------------------------------------------------*/
/* PL_RET                                                                  */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Pl_Ret(void)
{
  Immed_Printf("ld","[%s],%%o0",asm_reg_cp);
  Flush_Printf("jmp","%%o0+8");
}

/*-------------------------------------------------------------------------*/
/* JUMP                                                                    */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Jump(char *label)
{
  Flush_Printf("b",UN "%s",label);
}

/*-------------------------------------------------------------------------*/
/* MOVE_FROM_REG_AUX                                                       */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Move_From_Reg_Aux(char name, int no,char *tmp)
{
    struct reg *reg=Find_Register_Info(name);
    int n = (no+reg->base)*reg->mult;
    switch (reg->kind) {
        case 0:
            Delay_Printf("ld","[%s+%d],%s",reg->asm_reg,n,tmp);
            break;
        case 1:
            Delay_Printf("ld","[%s],%s",reg->asm_reg,tmp);
            Delay_Printf("ld","[%s+%d],%s",tmp,n,tmp);            
            break;
        case 2:
            Delay_Printf("ld","[%s],%s",reg->asm_reg,tmp);
            Delay_Printf("ld","[%s-%d],%s",tmp,n,tmp);            
            break;
    }
}

void Move_From_Reg(char name, int no)
{
  Move_From_Reg_Aux(name,no,"%o0");
}

/*-------------------------------------------------------------------------*/
/* MOVE_TO_REG_AUX                                                         */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Move_To_Reg_Aux(char name, int no,char *tmp)
{
    struct reg *reg=Find_Register_Info(name);
    int n = (no+reg->base)*reg->mult;
    switch (reg->kind) {
        case 0:
            Delay_Printf("st","%s,[%s+%d]",tmp,reg->asm_reg,n);
            break;
        case 1:
            Delay_Printf("ld","[%s],%s",reg->asm_reg,tmp);
            Delay_Printf("st","%s,[%s+%d]",tmp,tmp,n);            
            break;
        case 2:
            Delay_Printf("ld","[%s],%s",reg->asm_reg,tmp);
            Delay_Printf("st","%s,[%s-%d]",tmp,tmp,n);            
            break;
    }
}

void Move_To_Reg(char name, int no)
{
  Move_To_Reg_Aux(name,no,"%o0");
}

/*-------------------------------------------------------------------------*/
/* MOVE_INT_TO_REG                                                           */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Move_Int_To_Reg(int int_val, char name, int no)
{
    Load_Int_In_Machine_Register("%o0",int_val);
    Move_To_Reg(name,no);
}

/*-------------------------------------------------------------------------*/
/* MOVE_CST_TO_REG                                                           */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Move_Cst_To_Reg(char cst_kind, int cst_val, char name, int no)
{
    int n;
    switch (cst_kind) {
        case 'I':
            Move_Int_To_Reg((int)FOLSMB_MAKE(cst_val,0),name,no);
            break;
        case 'N':
            Move_Int_To_Reg((int)DFOLINT(cst_val),name,no);
            break;
        case 'C':
            Move_Int_To_Reg((int)DFOLCHAR(cst_val),name,no);
            break;
        case 'V':
            n = cst_val*sizeof(struct folvar) | TAG_VAR;
            Load_Label_In_Machine_Register("%o0","folvar_tab",n);
            Move_To_Reg(name,no);
            break;
        default:
            fprintf(stderr,"Not a valid Cst constructor %c\n",cst_kind);
            exit(-1);
            break;
    }
}


/*-------------------------------------------------------------------------*/
/* PL_CALL_START                                                           */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Pl_Call_Start(char *fct_name,int nb_args)
{
}

/*-------------------------------------------------------------------------*/
/* PL_CALL_STOP                                                            */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Pl_Call_Stop(char *fct_name,int nb_args)
{
    Delay_Printf("st","%%o7,[%s]",asm_reg_cp);
    Flush_Printf("call",UN "%s,0",fct_name);
}

/*-------------------------------------------------------------------------*/
/* PL_CALL_STAR                                                            */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Pl_Call_Star()
{
  Immed_Printf("ld","[%s],%%o0",asm_reg_p);
  Delay_Printf("st","%%o7,[%s]",asm_reg_cp);
  Flush_Printf("call","%%o0+8,0");
}

/*-------------------------------------------------------------------------*/
/* PL_CALL_REG                                                             */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Pl_Call_Reg(char name, int no)
{
    Move_From_Reg(name,no);
    Delay_Printf("st","%%o7,[%s]",asm_reg_cp);
    Flush_Printf("call","%%o0+8,0");
}

/*-------------------------------------------------------------------------*/
/* CALL_C_START                                                            */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Call_C_Start(char *fct_name,int nb_args)
{
}

#define BEFORE_ARG                                                          \
    {                                                                       \
     char r[4];                                                             \
                                                                            \
     if (offset<6)                                                          \
         sprintf(r,"%%o%d",offset);                                         \
      else                                                                  \
         strcpy(r,"%l7");
 
#define AFTER_ARG                                                           \
     if (offset>=6)                                                         \
         Delay_Printf("st","%s,[%%sp+%d]",r,92+(offset-6)*4);               \
    }

/*-------------------------------------------------------------------------*/
/* CALL_ARG_LABEL                                                        */
/*                                                                         */
/*-------------------------------------------------------------------------*/
int Call_Arg_Label(int offset,char *label,int no)
{
    switch (kind_of_call) {
        case 0:                 /* C */
            BEFORE_ARG;
            Load_Label_In_Machine_Register(r,label,no*4);
            AFTER_ARG;
            break;
        case 1: {                /* PROLOG */
            Load_Label_In_Machine_Register("%o0",label,no*4);
            Move_To_Reg('R',offset);
            break;
        }
    }
    return 1;
}

/*-------------------------------------------------------------------------*/
/* READ_VARIABLE                                                     */
/*                                                                         */
/*-------------------------------------------------------------------------*/

void Read_Variable(char *ident,int no)
{
    Load_Value_In_Machine_Register("%o0",ident,no*4);
}

/*-------------------------------------------------------------------------*/
/* CALL_C_ARG_VARIABLE                                                     */
/*                                                                         */
/*-------------------------------------------------------------------------*/
int Call_Arg_Variable(int offset,char *ident,int no)
{
    switch (kind_of_call) {
        case 0:                 /* C */
            BEFORE_ARG;
            Load_Value_In_Machine_Register(r,ident,no*4);
            AFTER_ARG;
            break;
        case 1:                 /* PROLOG */
            Load_Value_In_Machine_Register("%o0",ident,no*4);
            Move_To_Reg('R',offset);
            break;
    }
    return 1;
}

/*-------------------------------------------------------------------------*/
/* CALL_ARG_INT                                                          */
/*                                                                         */
/*-------------------------------------------------------------------------*/
int Call_Arg_Int(int offset,int int_val)
{    
    switch (kind_of_call) {
        case 0:                 /* C */
            BEFORE_ARG;
            Load_Int_In_Machine_Register(r,int_val);
            AFTER_ARG;
            break;
        case 1: {                /* PROLOG */
            Move_Int_To_Reg(int_val,'R',offset);
            break;
        }
    }
    return 1;
}

/*-------------------------------------------------------------------------*/
/* CALL_ARG_DOUBLE                                                       */
/*                                                                         */
/*-------------------------------------------------------------------------*/
int Call_Arg_Double(int offset,double dbl_val)
{
    int *p=(int *) &dbl_val;
    
    BEFORE_ARG;
 
    Delay_Printf("sethi","%%hi(%d),%s",p[0],r);
    Delay_Printf("or","%s,%%lo(%d),%s",r,p[0],r);
    
    AFTER_ARG;
 
    return 2;
}

/*-------------------------------------------------------------------------*/
/* CALL_ARG_STRING                                                       */
/*                                                                         */
/*-------------------------------------------------------------------------*/
int Call_Arg_String(int offset,int str_no)
{
    switch (kind_of_call) {
        case 0:                 /* C */
            BEFORE_ARG;
            Delay_Printf("sethi","%%hi(%s%d),%s",STRING_PREFIX,str_no,r);
            Delay_Printf("or","%s,%%lo(%s%d),%s",r,STRING_PREFIX,str_no,r);
            AFTER_ARG;
            break;
        case 1: {                /* PROLOG */
            Delay_Printf("sethi","%%hi(%s%d),%%o0",STRING_PREFIX,str_no);
            Delay_Printf("or","%%o0,%%lo(%s%d),%%o0",STRING_PREFIX,str_no);
            Move_To_Reg('R',offset);
            break;
        }
    }
    return 1;
}

/*-------------------------------------------------------------------------*/
/* CALL_ARG_REG
/*                                                                         */
/*-------------------------------------------------------------------------*/
int Call_Arg_Reg(int offset,char name, int no)
{
    switch (kind_of_call) {
        case 0:                 /* C */
            BEFORE_ARG;
            Move_From_Reg_Aux(name,no,r);
            AFTER_ARG;
            break;
        case 1: {                /* PROLOG */
            Move_From_Reg(name,no);
            Move_To_Reg('R',offset);
            break;
        }
    }
    return 1;
}

/*-------------------------------------------------------------------------*/
/* CALL_ARG_REG_VARIABLE_AUX                                               */
/*-------------------------------------------------------------------------*/
void
Call_Arg_Reg_Variable_Aux(char *tmp,char name, int no)
{
    struct reg *reg=Find_Register_Info(name);
    int n = (no+reg->base)*reg->mult;
    switch (reg->kind) {
        case 0:
             Delay_Printf("add","%s,%d,%s",reg->asm_reg,n,tmp);
            break;
        case 1:
            Delay_Printf("ld","[%s],%s",reg->asm_reg,tmp);
            Delay_Printf("add","%s,%d,%s",tmp,n,tmp);            
            break;
        case 2:
            Delay_Printf("ld","[%s],%s",reg->asm_reg,tmp);
            Delay_Printf("sub","%s,%d,%s",tmp,n,tmp);            
            break;
    }
}

/*-------------------------------------------------------------------------*/
/* CALL_ARG_REG_VARIABLE                                                   */
/*-------------------------------------------------------------------------*/
int Call_Arg_Reg_Variable(int offset,char name, int no)
{
    switch (kind_of_call) {
        case 0:                 /* C */
            BEFORE_ARG;
            Call_Arg_Reg_Variable_Aux(r,name,no);
            AFTER_ARG;
            break;
        case 1: {                /* PROLOG */
            struct reg *reg=Find_Register_Info('R');
            int n = (offset+reg->base)*reg->mult;
            Call_Arg_Reg_Variable_Aux("%o0",name,no);
            Delay_Printf("movl","%%o0,%s+%d",reg->asm_reg,n);
            break;
        }
    }
    return 1;
}

/*-------------------------------------------------------------------------*/
/* CALL_ARG_CST
/*                                                                         */
/*-------------------------------------------------------------------------*/
int Call_Arg_Cst(int offset,char name, int no)
{
    fol_t t;
    int n;
    switch (name) {
        case 'I':
            t=FOLSMB_MAKE(no,0);
            return Call_Arg_Int(offset,(int) t);
            break;
        case 'N':
            t=DFOLINT(no);
            return Call_Arg_Int(offset,(int) t);
            break;
        case 'C':
            t=DFOLCHAR(no);
            return Call_Arg_Int(offset,(int) t);
            break;
        case 'V':
            n = no*sizeof(struct folvar) | TAG_VAR;
            switch (kind_of_call) {
                case 0:         /* C */
                    BEFORE_ARG;
                    Load_Label_In_Machine_Register(r,"folvar_tab",n);
                    AFTER_ARG;
                    break;
                case 1:     /* PROLOG */
                    Load_Label_In_Machine_Register("%o0","folvar_tab",n);
                    Move_To_Reg('R',offset);
                    break;
            }
	    return 1;
	    break;
        default:
            fprintf(stderr,"Not a valid Cst constructor %c\n",name);
            exit(-1);
            break;
    }
}

/*-------------------------------------------------------------------------*/
/* CALL_C_ARG_PARAM
/*                                                                         */
/*-------------------------------------------------------------------------*/
int Call_Arg_Param(int offset,int no)
{
        /* To be done */
    return 1;
}

/*-------------------------------------------------------------------------*/
/* CALL_C_STOP                                                             */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Call_C_Stop(char *fct_name,int nb_args)
{
    Flush_Printf("call",UN "%s,0",fct_name);
}

/*-------------------------------------------------------------------------*/
/* JUMP_RET                                                                */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Jump_Ret(void)
{
    Flush_Printf("jmp","%%o0");
}

/*-------------------------------------------------------------------------*/
/* FAIL_RET                                                                */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Fail_Ret(void)
{
    Immed_Printf("cmp","%%o0,0");
    Flush_Printf("be", UN "%s","fail");
}

/*-------------------------------------------------------------------------*/
/* FAIL_C_RET                                                                */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Fail_C_Ret(void)
{
    Immed_Printf("cmp","%%o0,0");
    Flush_Printf("bne",".Lcont%d",w_label);
    C_Ret();
    Label_Printf(".Lcont%d:",w_label++);
}

/*-------------------------------------------------------------------------*/
/* MOVE_RET_TO_IDENT                                                       */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Move_Ret_To_Ident(char *ident,int no)
{
     Delay_Printf("sethi","%%hi(" UN "%s+%d),%%o1",ident,no*4);
     Delay_Printf("st","%%o0,[%%o1+%%lo(" UN "%s+%d)]",ident,no*4);
}

/*-------------------------------------------------------------------------*/
/* MOVE_RET_TO_REG
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Move_Ret_To_Reg(char name, int no)
{
    Move_To_Reg(name,no);
}

/*-------------------------------------------------------------------------*/
/* C_RET                                                                   */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void C_Ret(void)
{
    Delay_Printf("restore","");
    Flush_Printf("ret","");
}

/*-------------------------------------------------------------------------*/
/* DICO_STRING_START                                                       */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Dico_String_Start(int nb_consts)
{
}

/*-------------------------------------------------------------------------*/
/* DICO_STRING                                                             */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Dico_String(int str_no,char *asciiz)
{
    Inst_Printf(".align","8");
    Label_Printf("%s%d:",STRING_PREFIX,str_no);
    Inst_Printf(".ascii","\"%s\\0\"",asciiz);
}

/*-------------------------------------------------------------------------*/
/* DICO_STRING_STOP                                                        */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Dico_String_Stop(int nb_consts)
{
}

/*-------------------------------------------------------------------------*/
/* DICO_LONG_START                                                         */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Dico_Long_Start(int nb_longs)
{
#ifdef M_sparc_sunos
    Label_Printf(".data");
#else
    Inst_Printf(".section","\".data\"");
#endif
    Inst_Printf(".align","4");
}

/*-------------------------------------------------------------------------*/
/* DICO_LONG                                                               */
/*   initialization not used                                                                  */
/*-------------------------------------------------------------------------*/
void Dico_Long(char *name,int global,int initialized,InitLongList_t value,long length)

{
    if (length==0)
        length=1;

#ifdef M_sparc_sunos
    if (!global)
        Inst_Printf(".reserve",UN "%s,%u,\"bss\",4",name,length*4);
    else
        Inst_Printf(".common",UN "%s,%u,\"bss\"",name,length*4);
#else
    if (!global)
        Inst_Printf(".local",UN "%s",name);
    Inst_Printf(".common",UN "%s,%u,4",name,length*4);
#endif
}

/*-------------------------------------------------------------------------*/
/* DICO_LONG_STOP                                                          */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Dico_Long_Stop(int nb_longs)
{
}

/*-------------------------------------------------------------------------*/
/* DELAY_PRINTF                                                            */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Delay_Printf(char *op,char *operands,...)
{
    va_list arg_ptr;
    
    if (delay_op)
        Inst_Out(delay_op,delay_operands);
    
    va_start(arg_ptr,operands);
    
    delay_op=op;
    vsprintf(delay_operands,operands,arg_ptr);
}

/*
  Flush_Printf
  To be used for jump instruction (jmp, call, ba, ....)
*/
void Flush_Printf(char *op,char *operands,...)
{
    va_list arg_ptr;
    va_start(arg_ptr,operands);
    VInst_Printf(op,operands,arg_ptr);
    if (delay_op) {
        Inst_Out(delay_op,delay_operands);
        delay_op=NULL;
    } else {
        Inst_Printf("nop","");
    }
    va_end(arg_ptr);
}

/*
  Immed_Printf
  To be used to flush delayed without interversion
*/
void Immed_Printf(char *op,char *operands,...)
{
    va_list arg_ptr;
    if (delay_op) {
        Inst_Out(delay_op,delay_operands);
        delay_op=NULL;
    }
    va_start(arg_ptr,operands);
    VInst_Printf(op,operands,arg_ptr);
    va_end(arg_ptr);
}








