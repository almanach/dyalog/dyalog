/* 
 ******************************************************************
 * $Id$
 * Copyright (C) 1999, 2006, 2009 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  mapper.c -- Mapper File
 *
 * ----------------------------------------------------------------
 * Description
 *    Include machine specific file for the mini assembler to assembler
 * translator
 *    Inspired from Daniel Diaz GNU Prolog
 * ----------------------------------------------------------------
 */

#include <stdio.h>
#include <stdarg.h>
#include <search.h>

#include "config.h"             /* in Runtime */
#include "bigloo.h"             /* in Runtime */
#include "archi.h"              /* in Runtime */
#include "fol.h"                /* in Runtime */

#include "buffer.h"
#include "ma2asm.h"

extern int profile;
extern int debug;
extern int pic;

          /* machined-dependent mapper */

#if   defined(M_ix86_linux) && defined(__x86_64)
#include "linux_elf_x86_64.c"
#elif   defined(M_ix86_linux) || defined(M_ix86_cygwin)
#include "linux_elf_ix86.c"
#elif defined(M_sparc_sunos) || defined(M_sparc_solaris)
#include "sparc_any.c"
#elif defined(M_ix86_darwin) && defined(__x86_64)
#include "darwin_x86_64.c"
#elif defined(M_ix86_darwin)
#include "darwin_ix86.c"
#elif defined(M_powerpc_darwin)
#include "darwin_ix86.c"
#else
#error "Architecture not yet supported"
#endif


