/* 
 ******************************************************************
 * $Id$
 * Copyright (C) 1998, 2002, 2004, 2006, 2009 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  dyam2ma.h -- 
 *
 * ----------------------------------------------------------------
 * Description
 * 
 * ----------------------------------------------------------------
 */

#define Check_Arg(i,str)      (strncmp(argv[i],str,strlen(argv[i]))==0)

#define MA2ASM_VERSION             "1.0"
#define DEFAULT_OUTPUT_SUFFIX      ".s"

#define MASK_LONG_GLOBAL           1
#define MASK_LONG_INITIALIZED      2

extern buffer auxbuf;
extern int    in_ident;

typedef struct InitLongList 
{
    int type;                   /* 0=ident 1=number */
    long value;
    struct InitLongList *next;
} InitLongList, *InitLongList_t;

// Handling instructions


typedef enum {
    not_a_inst = 0,
    code_start,
    pl_fail,
    pl_ret,
    jump,
    move_from_reg,
    move_to_reg,
    move_int_to_reg,
    move_int_to_ident,
    move_cst_to_reg,
    move_cst_to_ident,
    read_variable,
    move_ret_to_ident,
    jump_ret,
    fail_ret,
    true_ret,
    fail_c_ret,
    true_c_ret,
    c_ret,
    pl_call_start,
    pl_jump,
    pl_jump_reg,
    pl_jump_star,
    pl_call_stop,
    pl_call_star,
    pl_call_reg,
    call_c_start,
    call_c_stop,
    call_arg_int,
    call_arg_float,
    call_arg_reg,
    call_arg_reg_variable,
    call_arg_param,
    call_arg_label,
    call_arg_variable,
    call_arg_string,
    call_arg_cst,
    label,
    jump_not_zero,
    ret_not_zero,
    defined,
    cache_or_build
} inst_kind;


typedef long InstArg;

typedef void (*inst_handler) (void *);

typedef struct inst {
    struct inst * next;
    inst_kind kind;
    inst_handler handler;
    InstArg arg1;
    InstArg arg2;
    InstArg arg3;
    InstArg arg4;
} *inst_t;



// handling symbols

#define Smb_Local    1
#define Smb_Global   (1 << 1)

#define Smb_Defined      (1 << 2)
#define Smb_Referenced   (1 << 3)

#define Smb_Prolog   (1 << 4)
#define Smb_C        (1 << 5)
#define Smb_Long     (1 << 6)
#define Smb_Label    (Smb_Prolog | Smb_C)

#define Smb_Local_Prolog   (Smb_Prolog | Smb_Local)
#define Smb_Global_Prolog   (Smb_Prolog | Smb_Global)
#define Smb_Local_Long      (Smb_Long | Smb_Local)
#define Smb_Global_Long      (Smb_Long | Smb_Global)

typedef unsigned long ulong;

typedef struct symbol {
    char *label;
    ulong prop;                  /* a bitmask of Smb_* */
    char *rlabel_data;
    char *rlabel_fun;
    ulong size;
    InitLongList_t init;
} * symbol_t;


long Defined_P(symbol_t);
void Check_C_Function(symbol_t);
void Check_DyALog_Proc(symbol_t);
void Check_Data(symbol_t);

// Handling registered constant
// some data such as strings or floats may need to be declared like symbols


#define Cst_String 0
#define Cst_Float 1

typedef struct constant {
    unsigned long kind;                  /* String or Float */
    union {
        char * s;
        float f;
    } value;
    unsigned long id;
} * constant_t;


/*---------------------------------*/
/* Function Prototypes             */
/*---------------------------------*/

void      Declare_Initializer   (char *initializer_fct);
void      Data_Long             (char *name,int global,long length,InitLongList_t value);

InitLongList_t Init_Long_Add   (short type, long value, InitLongList_t next);


void      Source_Line           (int line_no,char *cmt);

void      File_Name             (char *file_name);

void      Asm_Start             (void);
void      Asm_Stop              (void);

void      Inst_Code_Start       (char *label,int prolog,int global);
void      Code_Start            (symbol_t smb);

void      Inst_Code_Stop        (void);
void      Code_Stop             (void);

void      Inst_Label            (char *label);
void      Label                 (symbol_t smb);

void      Inst_Pl_Jump          (char *label);
void      Pl_Jump               (symbol_t smb);

void      Inst_Pl_Jump_Reg      (char name, long no);
void      Pl_Jump_Reg           (char name, long no);

void      Inst_Pl_Jump_Star     (void);
void      Pl_Jump_Star          (void);

void      Inst_Pl_Fail          (void);
void      Pl_Fail               (void);

void      Inst_Pl_Ret           (void);
void      Pl_Ret                (void);

void      Inst_Jump             (char *label);
void      Jump                  (symbol_t smb);

void      Inst_Jump_Not_Zero    (char *label);
void      Jump_Not_Zero         (symbol_t smb);

void      Inst_Move_From_Reg    (char name, long no);
void      Move_From_Reg         (char name, long no);

void      Inst_Move_To_Reg      (char name, long reg_no);
void      Move_To_Reg           (char name, long reg_no);

void      Inst_Move_Float_To_Reg (char name, long reg_no);
void      Move_Float_To_Reg      (char name, long reg_no);

void      Inst_Move_Int_To_Reg  (long  val_int, char name, long reg_no);
void      Move_Int_To_Reg       (long  val_int, char name, long reg_no);

void      Inst_Move_Cst_To_Reg  (char cst_kind, long cst_val, char name, long reg_no);
void      Move_Cst_To_Reg       (char cst_kind, long cst_val, char name, long reg_no);

void      Inst_Move_Ret_To_Ident (char *ident,long no);
void      Move_Ret_To_Ident      (symbol_t smb,long no);

void      Inst_Move_Int_To_Ident (long  val_int, char *ident, long no);
void      Move_Int_To_Ident      (long  val_int, symbol_t smb, long no);

void      Inst_Move_Cst_To_Ident (char cst_kind, long cst_val, char *ident, long no);
void      Move_Cst_To_Ident      (char cst_kind, long cst_val, symbol_t smb, long no);

void      Inst_Read_Variable    (char *ident,long no);
void      Read_Variable         (symbol_t smb,long no);

void      Inst_Pl_Call_Start    (char *fct_name);
void      Pl_Call_Start         (symbol_t smb);

void      Inst_Pl_Call_Stop     (char *fct_name);
void      Pl_Call_Stop          (symbol_t smb);

void      Inst_Pl_Call_Star     (void);
void      Pl_Call_Star          (void);

void      Inst_Pl_Call_Reg      (char name, long no);
void      Pl_Call_Reg           (char name, long no);

void    Inst_Call_C_Start     (char *fct_name);
void      Call_C_Start          (symbol_t smb,unsigned long offset, unsigned long float_offset);

void      Inst_Call_Arg_Label    (char *ident,long no);
void      Call_Arg_Label        (symbol_t smb,long no);

void      Inst_Call_Arg_Variable (char *ident,long no);
void	  Call_Arg_Variable      (symbol_t smb,long no);

void      Inst_Call_Arg_Int     (long int_val);
void      Call_Arg_Int          (long int_val);

void      Inst_Call_Arg_Float   (float flt_val);
void      Call_Arg_Float        (constant_t);

void       Inst_Call_Arg_Double       (double dbl_val);
void       Call_Arg_Double       (constant_t);

void      Inst_Call_Arg_String   (char *s);
void      Call_Arg_String        (constant_t);

void       Inst_Call_Arg_Reg          (char name,long no);
void       Call_Arg_Reg          (char name,long no);

void       Inst_Call_Arg_Reg_Variable (char name,long no);
void       Call_Arg_Reg_Variable (char name,long no);

void       Inst_Call_Arg_Cst          (char name,long no);
void       Call_Arg_Cst          (char name,long no);

void      Inst_Call_Arg_Param        (long no);
void      Call_Arg_Param        (long no);

void      Inst_Call_C_Stop      (char *fct_name);
void      Call_C_Stop           (symbol_t smb, unsigned long offset, unsigned long float_offset);

void      Call_C_Adjust_Stack   (int nb_pushes);

void      Inst_Jump_Ret              (void);
void      Jump_Ret              (void);

void      Inst_Fail_Ret              (void);
void      Fail_Ret              (void);

void      Inst_Fail_C_Ret            (void);
void      Fail_C_Ret            (void);

void      Inst_Ret_Not_Zero          (void);
void      Ret_Not_Zero          (void);

void      Inst_C_Ret                 (void);
void      C_Ret                 (void);

void      Inst_True_Ret              (void);
void      True_Ret              (void);

void      Inst_True_C_Ret            (void);
void      True_C_Ret            (void);

void      Inst_Cache_Or_Build   (char *ident, long no, char *fct_name);
void      Cache_Or_Build        (symbol_t smb, long no, symbol_t fun);

void      Dico_String_Start     (long nb);
void      Dico_String           (long str_no,char *asciiz);
void      Dico_String_Stop      (long nb);
constant_t Find_Or_Add_String   (char *);

void      Dico_Float_Start     (long nb);
void      Dico_Float           (long str_no,float f);
void      Dico_Float_Stop      (long nb);
constant_t Find_or_Add_Float   (float);


void      Dico_Long_Start       ();
void      Dico_Long             (symbol_t smb);
void      Dico_Long_Stop        ();



// Emitting

void      Label_Printf          (char *label,...);
void      Inst_Printf           (char *op,char *operands,...);
void      VInst_Printf          (char *op,char *operands,va_list arg_ptr);
void      Inst_Out              (char *op,char *operands);


