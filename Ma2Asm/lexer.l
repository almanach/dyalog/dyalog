/*************************************************************
 * $Id$
 * Copyright (C) 1997, 2002, 2003, 2006, 2009, 2014 Eric de la Clergerie
 * ------------------------------------------------------------
 *
 *   Fol Lexer
 *
 * ------------------------------------------------------------
 * Description
 *   Lexer for a PROLOG-like language
 * ------------------------------------------------------------
 */

%{
 
#include <math.h>
#include <string.h>
#include <stdarg.h>
#include "./buffer.h"
#include "./ma2asm.h"
#include "./parser.h"


#define yycopy()        strcpy((char *)malloc( yyleng+1 ),yytext)


void yyerror(char *msg)
{
    fprintf(stderr,"error line %d `%s' : %s\n",yylineno,yytext,msg);
    exit(1);
}

#define load_return(_type) yylval.str = yycopy(); return in_ident ? TK_IDENT : _type

%}


LAYOUT       [ \n\t\r]
ALPHA        [[:alnum:]_]

%x string
%option stack yylineno 8bit
%option noyywrap
%option nounput noyy_top_state
/* %option debug */

%%

%{
%}

{LAYOUT}+	/* eat spaces */
[;].*$		/* eat comments */

				/* punctuations */

"("              return TK_LPAR;
")"              return TK_RPAR;
"["              return TK_LBRA;
"]"              return TK_RBRA;
","              return TK_COMA;
":"              return TK_COLON;
"&"              return TK_AMP;
"$"              return TK_DOLLAR;
"*"              return TK_STAR;
"="              return TK_EQUAL;

    /* MA keywords */

file_name        load_return(FILE_NAME);
pl_code          load_return(PL_CODE);
pl_jump		 load_return(PL_JUMP);
pl_call		 load_return(PL_CALL);
pl_fail		 load_return(PL_FAIL);
pl_ret		 load_return(PL_RET);
jump		 load_return(JUMP);
move		 load_return(MOVE);
call_c		 load_return(CALL_C);
jmp_reg          load_return(JMP_REG);
ret_reg          load_return(RET_REG);
jump_ret	 load_return(JUMP_RET);
fail_ret	 load_return(FAIL_RET);
true_ret	 load_return(TRUE_RET);
fail_c_ret	 load_return(FAIL_C_RET);
true_c_ret	 load_return(TRUE_C_RET);
move_ret	 load_return(MOVE_RET);
c_code		 load_return(C_CODE);
c_ret		 load_return(C_RET);
long		 load_return(LONG);
cache_or_build   load_return(CACHE_OR_BUILD);
local		 load_return(LOCAL);
global		 load_return(GLOBAL);

[XYRST]          load_return(DREG);
[INCVF]          load_return(CST);


				/* Numbers */

-?[0-9]+            yylval.num = atol( yytext ); return TK_INT;

[+-]?[[:digit:]]*\.[[:digit:]]+([eE][+-]?[[:digit:]]+)? {
                    yylval.fnum = atof( yytext ); return TK_REAL;
                    }

                                 /* Idents */

{ALPHA}+          yylval.str = yycopy(); return TK_IDENT;

				/* quoted idents */

\"               yy_push_state(string);

<string>{                       /* the lexer buids a C-compatible string */
   \\\"          buffer_printf(auxbuf,yyleng,"%s",yytext);
   \"            yylval.str = buffer_flush(auxbuf) ; yy_pop_state(); return TK_STRING;
   [^\"\\]+      buffer_printf(auxbuf,yyleng,"%s",yytext);
   \\n           buffer_printf(auxbuf,yyleng,"%s",yytext);
   \\t           buffer_printf(auxbuf,yyleng,"%s",yytext);
   \\r           buffer_printf(auxbuf,yyleng,"%s",yytext);  
   \\b           buffer_printf(auxbuf,yyleng,"%s",yytext);  
   \\[0-7]{3}     {                 /* octal escape sequence */
                     int result;
                     (void) sscanf( yytext + 1, "%o", &result );
                     if ( result > 0xff ) yyerror( "char out of bound" );
                     buffer_printf(auxbuf,1,"%c",(char) result);
                  }
   \\X[0-9A-F]{2} {		    /* Hexa escape sequence */
                     int result;
                     (void) sscanf( yytext + 2, "%02X", &result );
                     if ( result > 0xff )  yyerror( "char out of bound" );
                     buffer_printf(auxbuf,1,"%c",(char) result);
                  }
   \\\\          buffer_printf(auxbuf,2,"%s","\\\\");
   \\            buffer_printf(auxbuf,2,"%s","\\\\");
                               
   <<EOF>>       {
                    yyerror( "end of file in string" );
                    yyterminate();
                  }
}

%% 
