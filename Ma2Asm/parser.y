%{
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include "./buffer.h"
#include "./ma2asm.h"

#include "config.h"             /* in Runtime */
#include "bigloo.h"             /* in Runtime */
#include "archi.h"              /* in Runtime */
#include "fol.h"                /* in Runtime */
    
int kind_of_call;   /* 1: prolog 0: call */

int in_ident;
int init_already_read;
unsigned long offset;

#define YYDEBUG 1

int yylex();
void yyerror(const char *);

%}

%union {
    long   num;
    float fnum;
    char *str;
    InitLongList_t initlonglist;
    struct {
         short  type;
	 long   value;
    }    initlongval;
    struct {
	 char  name;
	 long no;
    }	 reg;
    struct {
        char *ident;
        long trail;
    }    ident;
}

%token  TK_ERROR TK_EOF 
%token  TK_COMA TK_LPAR TK_RPAR TK_AMP TK_COLON
%token  TK_LBRA TK_RBRA
%token  TK_DOLLAR TK_STAR TK_EQUAL
%token  <num> TK_INT
%token  <fnum> TK_REAL
%token  <str> TK_IDENT TK_STRING

	/*  MA Instructions */

%token <str> FILE_NAME
%token <str> PL_CODE
%token <str> PL_JUMP
%token <str> PL_CALL
%token <str> PL_FAIL
%token <str> PL_RET	
%token <str> JUMP	
%token <str> MOVE	
%token <str> CALL_C
%token <str> JMP_REG
%token <str> RET_REG
%token <str> JUMP_RET
%token <str> FAIL_RET
%token <str> TRUE_RET
%token <str> FAIL_C_RET
%token <str> TRUE_C_RET
%token <str> MOVE_RET
%token <str> C_CODE	
%token <str> C_RET	
%token <str> LONG	
%token <str> LOCAL		
%token <str> GLOBAL
%token <str> CACHE_OR_BUILD

%token <str> DREG
%token <str> CST

%type <str> ident call_c pl_call pl_jump
%type <num> local_or_global ident_trail cst
%type <reg> reg
%type <ident> xident
%type <initlonglist> longinit initlonglist
%type <initlongval>  initlongval
%%

statements : /* nothing */
	   |  statements statement
;

    /* DyAM Statements */

statement :
        FILE_NAME  TK_STRING		     {}
     |  PL_CODE local_or_global ident        {Inst_Code_Start($3,1,$2);}
     |  pl_jump				     {}
     |  pl_call				     {}
     |  PL_FAIL				     {Inst_Pl_Fail();}
     |  PL_RET				     {Inst_Pl_Ret();}
     |  JUMP ident			     {Inst_Jump($2);}
     |  MOVE reg TK_COMA reg	             {Inst_Move_From_Reg($2.name,$2.no); 
					      Inst_Move_To_Reg($4.name,$4.no);
					      }
     |  MOVE TK_INT TK_COMA reg              {Inst_Move_Int_To_Reg($2,$4.name,$4.no);} 
     |  MOVE TK_INT TK_COMA xident           {Inst_Move_Int_To_Ident($2,$4.ident,$4.trail);} 
     |  MOVE CST TK_LPAR cst TK_RPAR TK_COMA reg {Inst_Move_Cst_To_Reg($2[0],$4,$7.name,$7.no);} 
     |  MOVE CST TK_LPAR cst TK_RPAR TK_COMA xident {Inst_Move_Cst_To_Ident($2[0],$4,$7.ident,$7.trail);} 
     |  MOVE TK_AMP xident TK_COMA reg       { Inst_Read_Variable($3.ident,$3.trail);
                                              Inst_Move_To_Reg($5.name,$5.no);}
     |  MOVE TK_AMP xident TK_COMA xident    { Inst_Read_Variable($3.ident,$3.trail);
                                               Inst_Move_Ret_To_Ident($5.ident,$5.trail);}
     |  call_c   			     {}
     |  JMP_REG reg TK_COMA ident	     {Inst_Move_From_Reg($2.name,$2.no);
                                              Inst_Jump_Not_Zero($4); }
     |  JMP_REG TK_AMP xident TK_COMA ident  {Inst_Read_Variable($3.ident,$3.trail);
                                              Inst_Jump_Not_Zero($5); }
     |  RET_REG reg	                     { Inst_Move_From_Reg($2.name,$2.no);
                                               Inst_Ret_Not_Zero(); }
     |  RET_REG TK_AMP xident                { Inst_Read_Variable($3.ident,$3.trail);
                                               Inst_Ret_Not_Zero(); }
     |  JUMP_RET			     { Inst_Jump_Ret();}
     |  FAIL_RET			     {  Inst_Fail_Ret();}
     |  TRUE_RET			     {  Inst_True_Ret();}
     |  FAIL_C_RET			     {  Inst_Fail_C_Ret();}
     |  TRUE_C_RET			     { Inst_True_C_Ret();}
     |	MOVE_RET reg			     { Inst_Move_To_Reg($2.name,$2.no);}
     |  MOVE_RET xident                      { Inst_Move_Ret_To_Ident($2.ident,$2.trail);}
     |  C_CODE local_or_global ident         { Inst_Code_Start($3,0,$2);}
     |  C_RET				     { Inst_C_Ret();}
     |  LONG local_or_global xident longinit { Data_Long($3.ident,$2,$3.trail,$4);}
     |  ident TK_COLON			     { Inst_Label($1);}
|  CACHE_OR_BUILD xident TK_COMA ident  { Inst_Cache_Or_Build($2.ident,$2.trail,$4); }// tmp
     |  CACHE_OR_BUILD TK_AMP xident TK_COMA ident  { Inst_Cache_Or_Build($3.ident,$3.trail,$5); }
;

local_or_global :
         LOCAL				{$$=0;}
|        GLOBAL				{$$=1;}
;

reg :
         DREG TK_LPAR TK_INT TK_RPAR { $$.name=$1[0]; $$.no=$3; }
;

pl_jump :
	PL_JUMP ident			{ Inst_Pl_Call_Start($2);  }
	TK_LPAR args TK_RPAR		{ Inst_Pl_Jump($2);}
;

pl_jump :
	PL_JUMP TK_AMP reg		{ Inst_Pl_Jump_Reg($3.name,$3.no); }
;

pl_jump :
	PL_JUMP TK_STAR			{ Inst_Pl_Jump_Star(); }
;

pl_call :
       PL_CALL ident                    { Inst_Pl_Call_Start($2); }
       TK_LPAR args TK_RPAR		{ Inst_Pl_Call_Stop($2); }
;

pl_call :
	PL_CALL TK_AMP reg		{ Inst_Pl_Call_Reg($3.name,$3.no); }
;

pl_call :
        PL_CALL TK_STAR                  { Inst_Pl_Call_Star();}
;

call_c :
       CALL_C ident			{ Inst_Call_C_Start($2); }
       TK_LPAR args TK_RPAR		{ Inst_Call_C_Stop($2); }
;

args : /* nothing */		{}
       | args2		{}
;

args2 :
         arg			{}
       | args2 TK_COMA arg  {}
;

arg :
        TK_INT			   { Inst_Call_Arg_Int($1);}
      | TK_REAL                    { Inst_Call_Arg_Float($1);}
      | TK_STRING		   { Inst_Call_Arg_String($1);}
      | reg			   { Inst_Call_Arg_Reg($1.name,$1.no);}
      | TK_AMP reg		   { Inst_Call_Arg_Reg_Variable($2.name,$2.no);}
      | CST TK_LPAR cst TK_RPAR    { Inst_Call_Arg_Cst($1[0],$3);}
      | TK_DOLLAR TK_INT	   { Inst_Call_Arg_Param($2);}
      | xident	                   { Inst_Call_Arg_Label($1.ident,$1.trail);}
      | TK_AMP xident              { Inst_Call_Arg_Variable($2.ident,$2.trail);}
;

cst :
         TK_INT  { $$=$1; }
      |  TK_REAL { $$=float2long($1); }
;

ident :
        {in_ident=1;} TK_IDENT	{in_ident=0; $$=$2; }
;

ident_trail : /* nothing */           { $$=0;  }
	    | TK_LBRA TK_INT TK_RBRA  { $$=$2; }
;    

xident :  ident ident_trail { $$.ident = $1; $$.trail = $2; }
;

longinit : /* nothing */               { $$=0; }
         |  TK_EQUAL initlongval       { $$=Init_Long_Add($2.type,$2.value,0); }
	 |  TK_EQUAL TK_LBRA initlonglist TK_RBRA { $$=$3; }
;

initlonglist : initlongval             { $$=Init_Long_Add($1.type,$1.value,0); }
             | initlongval TK_COMA initlonglist { $$=Init_Long_Add($1.type,$1.value,$3); }
;

initlongval : TK_INT         { $$.type=1; $$.value=$1; }
           | ident           { $$.type=0; $$.value=(long)$1;  }
;

%%






