#include <string.h>

/*---------------------------------*/
/* Constants                       */
/*---------------------------------*/

#define STRING_PREFIX             ".LC"
#define FLOAT_PREFIX              ".LC"

#define MAX_C_ARGS_IN_C_CODE       32

#define UN ""

#define C_ARGS_TOP                    8+MAX_C_ARGS_IN_C_CODE*8

#define R_DATA 1
#define R_FUN  2

#define FOLVAR_SIZE sizeof(struct folvar)
#define FOLVAR_SIZE_IN_WORD FOLVAR_SIZE / sizeof(long)

#define IS_INTP(n) (((((unsigned long)n) & 0xFFFF) == (unsigned long) n))

/*---------------------------------*/
/* Global Variables                */
/*---------------------------------*/

extern int kind_of_call;        /* cf parser.y */

char  asm_reg_bank[64];
char  asm_reg_e[64];
char  asm_reg_b[64];
char  asm_reg_cp[64];
char  asm_reg_p[64];

#define REG_VAR   0
#define REG_UP    1
#define REG_DOWN  2
#define REG_FLOAT 4

#define REG_KIND(_kind)    (_kind & 3)
#define REG_FLOATP(_kind)  (_kind & 4)

struct reg {
    char name;
    char asm_reg[64];
    int  base;
    int  mult;
    int  kind;                  /* 0: variable 1: growing 2: not growing */
}   registers[4];

unsigned int   w_label=0;

static unsigned int offset;
static unsigned int float_offset;
static unsigned int max_args;
static unsigned int max_float_args;

/* the registers used in order for integer arguments + other */

typedef struct sreg {
    char * name;
    char * name32;
} * sreg_t;

struct sreg rax = { "%rax", "%eax" };
struct sreg rsp = { "%rsp", "%rsp" };
struct sreg rbp = { "%rbp", "%rbp" };
struct sreg trail = { "%r15", "%r15" }; /* use of register %r15 for the base of trail */

struct sreg argregs[6] = {
    {"%rdi","%edi"},
    {"%rsi","%esi"},
    {"%rdx","%edx"},
    {"%rcx","%ecx"},
    {"%r8","%r8d"},
    {"%r9","%e9d"}
};

struct sreg floatregs[8] = {
    {"%xmmd0","%xmmd0"},
    {"%xmmd1","%xmmd1"},
    {"%xmmd2","%xmmd2"},
    {"%xmmd3","%xmmd3"},
    {"%xmmd4","%xmmd4"},
    {"%xmmd5","%xmmd5"},
    {"%xmmd6","%xmmd6"},
    {"%xmmd7","%xmmd7"},
};

          /* variables for ma_parser.c */

unsigned int strings_need_null=0;


          /* variables for ma2asm.c */

unsigned int call_c_reverse_args=0;

/*----------------------------------------------------------------------
 * Handling Relocatable
 *
 *----------------------------------------------------------------------
 */

symbol_t refer_symbol(char * label,long prop);
symbol_t define_symbol(char *label,long prop,ulong size,InitLongList_t init );

static symbol_t smb_folvar_tab;
static symbol_t smb_follow_choice;

extern void *symbol_tbl;

static char *
Symbol_R_Data(symbol_t smb)
{
    if (smb->rlabel_data) {
        return smb->rlabel_data;
    } else {
        char *label = smb->label;
        char *rlabel= (char *)malloc(11+strlen(label));
        if (smb->prop & Smb_Defined) {
            sprintf(rlabel,UN "%s",label);
        } else {
            sprintf(rlabel, UN "%s@GOTPCREL",label);
        }
        smb->rlabel_data = rlabel;
        smb->prop |= Smb_Long;
        return rlabel;
    }
}

static char *
Symbol_R_Fun(symbol_t smb)
{
    if (smb->rlabel_fun) {
        return smb->rlabel_fun;
    } else {
        char *label = smb->label;
        char *rlabel= (char *)malloc(5+strlen(label));
        if (smb->prop & Smb_Local) {
            sprintf(rlabel,UN "%s",label);
        } else {
            sprintf(rlabel, UN "%s@PLT",label);
        }
        smb->rlabel_fun = rlabel;
        smb->prop |= Smb_C;
        return rlabel;
    }
}

static char *
Symbol_R_Prolog(symbol_t smb)
{
    if (smb->rlabel_fun) {
        return smb->rlabel_fun;
    } else {
        char *label = smb->label;
        char *rlabel= (char *)malloc(5+strlen(label));
        if (smb->prop & Smb_Local) {
            sprintf(rlabel,UN "%s",label);
        } else {
            sprintf(rlabel, UN "%s@PLT",label);
        }
        smb->rlabel_fun = rlabel;
        smb->prop |= Smb_Prolog;
        return rlabel;
    }
}

/*-------------------------------------------------------------------------*/
/* STAB_LINE                                                               */
/*                                                                         */
/*-------------------------------------------------------------------------*/

static struct
{
    char *label;
    int  global;
    long  stab_label;
} fun_info = { 0, 0, 0 };
 
extern long yylineno;

#define curline (yylineno-5)

static long stab_label=0;

void Stab_Line()
{
    Inst_Printf(".stabn","68,0,%u,.LL%d-%s",curline,stab_label,fun_info.label);
    Label_Printf(".LL%u:",stab_label++);
}

/*----------------------------------------------------------------------
 * Handling movements
 *----------------------------------------------------------------------
 */

typedef enum {
    imm_long=0,                   /* immediate long, only as src */
    imm_float,                  /* immdiate float, only as src */
    imm_string,                 /* immediate string, only as src */
    imm_label,                  /* immediate local label, only as src */
    folvar,                     /* fol variable, only as src */
    reg,                        /* assembly register */
    stack,                      /* shifted assembly register */
    stackaddr,                   /* addr relative to an assemble register */
    var,                        /* content of a (possibly shifted) variable,
                                   only as src*/
    addr,                        /* address of a (possibly shifted) variable */
    arg                          /* C function arg */
} pos_kind;

typedef union {
    long n;
    char * label;
    symbol_t smb;
    sreg_t reg;
} pos_value;

typedef struct {
    pos_kind  kind;
    pos_value v;
    long offset;
} pos;

static pos rega = {
  kind: reg,
  v: { reg: &rax },
  offset: 0
};

static pos xmm7 = {
  kind: reg,
  v: { reg: &floatregs[7] },
  offset: 0
};

static pos dreg2pos (char name, long no);

#define rname(pos) pos.v.reg->name
#define rname32(pos) pos.v.reg->name32

#define xarg(_offset) ((pos) {kind: arg, offset: _offset})
#define xlong(_n) ((pos) {kind: imm_long, v: {n: (long) _n}})
#define xfolvar(_no) ((pos) {kind: folvar, offset: _no})
#define xstring(_no) ((pos) {kind: imm_string, offset: _no})
#define xlabel(_no) ((pos) {kind: imm_label, offset: _no})
#define xfloat(_no) ((pos) {kind: imm_float, offset: _no})
#define xstack(_reg,_offset) ((pos) {kind: stack, v: {reg: &_reg}, offset: _offset})
#define xstackaddr(_reg,_offset) ((pos) {kind: stackaddr, v: {reg: &_reg}, offset: _offset})
#define xreg(_reg) ((pos) {kind: reg, v: {reg: &_reg}})
#define arg2xreg(_offset) xreg(argregs[_offset])
#define arg2xstack(_offset) xstack(rsp,_offset-6)
#define xvar(_smb,_offset) ((pos) {kind: var, v: {smb: _smb}, offset: _offset})
#define xaddr(_smb,_offset) ((pos) {kind: addr, v: {smb: _smb}, offset: _offset})

#define xfloatreg(_offset) ((pos) {kind: reg, v: {reg: &floatregs[_offset]}})

#define xstack_rax(_offset) xstack(rax,_offset)

#define xprologarg(_offset) dreg2pos('R',_offset)
#define xprologfloatarg(_offset) dreg2pos('R',_offset)
#define xfloatarg(_offset) xreg(floatregs[_offset])

// static pos dyalog_reg_e = xstack(trail,I_E);
// static pos dyalog_reg_b = xstack(trail,I_B);
static pos dyalog_reg_cp = xstack(trail,I_CP);
// static pos dyalog_reg_p = xstack(trail,I_P);

char xsrc_buffer[100];
char xdest_buffer[100];

#define test_dest_is_reg \
    if (dest_kind != reg) { \
        generic_move(src,rega); \
        generic_move(rega,dest); \
        return;\
    } 

static void
generic_move (pos src, pos dest)
{
    char *inst = "movq";
    char *xsrc = xsrc_buffer;
    char *xdest = xdest_buffer;
    pos_kind src_kind = src.kind;
    pos_kind dest_kind = dest.kind;
    Bool need_post_shift = 0;
    Bool is_32 = 0;
        /* to be sure sure the buffers are void */
    xsrc_buffer[0]=0;
    xdest_buffer[0]=0;
    
        /*
         * Not yet correct for move(gvar,gaddr) or move(gaddr,gaddr)
         * because they require the use of two intermediary registers
         * %rax may be one of them
         * but pb to safely choose the second one
         * However, it is possible that this case doesn't arise
         */

        /* C function args
         * the 6 first ones are stored in registers
         * the remaining ones are stored on the stack
         */

    if (src_kind == arg) {
        src = (src.offset < 6) ? arg2xreg(src.offset) : xstack(rbp,src.offset-4);
        src_kind = src.kind;
    }

    if (dest_kind == arg) {
        dest = (dest.offset < 6) ? arg2xreg(dest.offset) : xstack(rsp,dest.offset-6);
        dest_kind = dest.kind;
    }
    
    switch (src_kind) {

        case imm_long:
            sprintf(xsrc_buffer,"$%ld",src.v.n);
            if ((dest_kind == reg) && IS_INTP(src.v.n)) {
                    /* optimization when dest is a register */
                inst = "movl";
                is_32 =1;
            }
            break;
            
        case imm_float:
            inst = "movsd";
            sprintf(xsrc_buffer, STRING_PREFIX "%lu(%%rip)",src.offset);
            break;

        case imm_string:
            test_dest_is_reg;
            sprintf(xsrc_buffer, FLOAT_PREFIX "%lu(%%rip)",src.offset);
            inst = "leaq";
            break;

        case imm_label:
            test_dest_is_reg;
            inst = "leaq";
            sprintf(xsrc_buffer, ".L%lu(%%rip)",src.offset);
            break;

        case folvar:
            test_dest_is_reg;
//            Comment_Printf("folvar %d",src.offset);
            sprintf(xsrc_buffer,"%lu(%s)",
                    (((unsigned long) NB_REGISTERS)*8)+(src.offset*FOLVAR_SIZE)+TAG_VAR,
                    trail.name);
            inst="leaq";
            break;
            
        case reg:
            xsrc = rname(src);
            break;

        case stack:
            test_dest_is_reg;
            if (src.offset==0)
                sprintf(xsrc_buffer,"(%s)",rname(src));
            else
                sprintf(xsrc_buffer,"%lu(%s)",src.offset*8,rname(src));
            break;

        case stackaddr:
            test_dest_is_reg;
            inst="leaq";
            if (src.offset==0)
                sprintf(xsrc_buffer,"(%s)",rname(src));
            else
                sprintf(xsrc_buffer,"%lu(%s)",src.offset*8,rname(src));
            break;

            
        case var: 
        {
            symbol_t smb = src.v.smb;
            test_dest_is_reg;
            if ((smb->prop & Smb_Global) && !(smb->prop & Smb_Defined)) {
                sprintf(xsrc_buffer,"%s(%%rip)",Symbol_R_Data(smb));
                need_post_shift = 1;
            } else if (src.offset == 0) {
                sprintf(xsrc_buffer,"%s(%%rip)",Symbol_R_Data(smb));
            } else {
                sprintf(xsrc_buffer,"%lu+%s(%%rip)",
                        8*src.offset,Symbol_R_Data(smb));
            }
            break;
        }
        
        case addr:
        {
            symbol_t smb = src.v.smb;
            test_dest_is_reg;
            if ((smb->prop & Smb_Global) && !(smb->prop & Smb_Defined)) {
                sprintf(xsrc_buffer,"%s(%%rip)",Symbol_R_Data(smb));
                need_post_shift = 1;
            } else if (src.offset == 0) {
                sprintf(xsrc_buffer,"%s(%%rip)",Symbol_R_Data(smb));
                inst = "leaq";
            } else {
                sprintf(xsrc_buffer,"%lu+%s(%%rip)",
                        8*src.offset,Symbol_R_Data(smb));
                inst = "leaq";
            }
            break;
        }

        
        default:
            fprintf(stderr,"Not a valid mov src component");
            exit(1);
            break;

    }

    switch (dest_kind) {

        case reg:
            xdest = (is_32) ? rname32(dest) : rname(dest);
            break;

        case stack:
            if (dest.offset==0)
                sprintf(xdest_buffer,"(%s)",rname(dest)); 
            else
                sprintf(xdest_buffer,"%lu(%s)",dest.offset*8,rname(dest)); 
            break;

        case addr:
        {
            symbol_t smb = dest.v.smb;
            if ((smb->prop & Smb_Global) && !(smb->prop & Smb_Defined)) {
                pos tmp = xstack_rax(dest.offset);
                dest.offset=0;
                dest.kind=var;
                generic_move( dest, rega );
                generic_move( src, tmp );
                return;
            } else if (dest.offset == 0) 
                sprintf(xdest_buffer,"%s(%%rip)",Symbol_R_Data(smb));
            else {
                sprintf(xdest_buffer,"%lu+%s(%%rip)",8*dest.offset,Symbol_R_Data(smb));
            }
            break;
        }
        
        default:
            fprintf(stderr,"Not a valid dest component");
            exit(1);
            break;

    }

    Inst_Printf(inst,"%s,%s",xsrc,xdest);

    if (need_post_shift) {
            /* imply that dest is a register */
        if (src_kind==var) {
            Inst_Printf("movq","%lu(%s),%s",
                        8*src.offset,rname(dest),rname(dest));
        } else if (src.offset > 0) {
            Inst_Printf("addq","$%lu,%s",8*src.offset,rname(dest));
        }
    }
    
}

static
void load_arg (pos src) 
{
        /* use global variables kind_of_call and offset */
    generic_move(src,(kind_of_call==0) ? xarg(offset) : xprologarg(offset));
    offset++;
}

static
void load_float_arg (pos src) 
{
        /* use global variables kind_of_call and float_offset */
    generic_move(src,(kind_of_call==0) ? xfloatarg(float_offset) : xprologfloatarg(float_offset));
    float_offset++;
}

static
pos dreg2pos (char name, long no) 
{
    switch (name) {
        case 'Y':
                /* EVDLC: not yet implemented
                 * but it seems this kind of dyalog registers is not used
                 */
            fprintf(stderr,"Register 'Y' not yet implemented");
            exit(1);
            break;

        case 'R':
            return xstack(trail,NB_SYSTEM_REGISTERS+no);
            break;

        case 'S':
            return xstack(trail,no);
            break;

        case 'T':
            return xstack(trail,NB_SYSTEM_REGISTERS+no);
            break;

        default:
            fprintf(stderr,"Not a valid register name '%c'",name);
            exit(1);
            break;
    }
}

static pos
dcst2pos (char cst_kind, long cst_val)
{
    switch (cst_kind) {
        case 'I':
            return xlong((long)FOLSMB_MAKE(cst_val,0));
            break;
        case 'N':
            return xlong((long)DFOLINT(cst_val));
            break;
        case 'F':
            return xlong((long)DFOLFLT(long2float(cst_val)));
            break;
        case 'C':
            return xlong((long)DFOLCHAR(cst_val));
            break;
        case 'V':
            return xfolvar(cst_val);
            break;
        default:
            fprintf(stderr,"Not a valid Cst constructor %c\n",cst_kind);
            exit(1);
            break;
    }
}

/*-------------------------------------------------------------------------*/
/* STAB_FUNCTION                                                           */
/*                                                                         */
/*-------------------------------------------------------------------------*/

void Stab_Function(char *label,int global)
{
   
    Label_Printf("");
    Inst_Printf(".stabs","\"%s:%c1\",36,0,%u,%s",
                label, global ? 'F' : 'f', curline, label
                );
    fun_info.label=label;
    fun_info.global=global;
    fun_info.stab_label=stab_label;
}

/*-------------------------------------------------------------------------*/
/* SOURCE_LINE                                                             */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Source_Line(int line_no,char *cmt)
{
 Label_Printf("\t# %6d: %s",line_no,cmt);
}

/*-------------------------------------------------------------------------*/
/* FIND_REGISTER_INFO                                                      */
/*                                                                         */
/*-------------------------------------------------------------------------*/
static
struct reg * Find_Register_Info( char name )
{
    struct reg *reg;
    for(reg=registers; reg->name != name ; reg++);
    return reg;
}

extern char *file_name_in;

/*-------------------------------------------------------------------------*/
/* ASM_START                                                               */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Asm_Start(void) {
    smb_folvar_tab = refer_symbol("folvar_tab",Smb_Long);
    smb_follow_choice = refer_symbol("follow_choice",Smb_C);
    sprintf(asm_reg_e,"%d(%s)",I_E*8,trail.name);
    sprintf(asm_reg_b,"%d(%s)",I_B*8,trail.name);
    sprintf(asm_reg_cp,"%d(%s)",I_CP*8,trail.name);
    sprintf(asm_reg_p,"%d(%s)",I_P*8,trail.name);

    registers[0].name = 'Y';
    strcpy(registers[0].asm_reg,asm_reg_e);
    registers[0].base=4;
    registers[0].mult=8;
    registers[0].kind=REG_DOWN;
    
    registers[1].name = 'R';
    strcpy(registers[1].asm_reg,trail.name);
    registers[1].base=NB_SYSTEM_REGISTERS;
    registers[1].mult=8;
    registers[1].kind=REG_VAR;

    registers[2].name = 'S';
    strcpy(registers[2].asm_reg,trail.name);
    registers[2].base=0;
    registers[2].mult=8;
    registers[2].kind=REG_VAR;

    registers[3].name = 'T';
    strcpy(registers[3].asm_reg,trail.name);
    registers[3].base=NB_SYSTEM_REGISTERS;
    registers[3].mult=8;
    registers[3].kind=REG_VAR | REG_FLOAT;
    
//    Inst_Printf(".stabs","\"%s\",100,0,0,Ltext0",file_name_in);
    Inst_Printf(".text","");
//    Label_Printf("Ltext0:");
//    Inst_Printf(".stabs","\"void:t1=15\",128,0,0,0");

//    Inst_Printf(".align","4,0x90");
    Inst_Printf(".type","%s, @function","fail");
    Inst_Label("fail");
    Inst_Pl_Fail();

}


/*-------------------------------------------------------------------------*/
/* ASM_STOP                                                                */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Asm_Stop(void)
{
//    Inst_Printf(".ident", "\"DyALog: (dyam2asm)\"");
    Inst_Printf(".section",".note.GNU-stack,\"\",@progbits");
}

/*-------------------------------------------------------------------------*/
/* LABEL                                                                   */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void File_Name(char *file_name)
{
    Inst_Printf(".file","\"%s\"",file_name);
}

/*-------------------------------------------------------------------------*/
/* LABEL                                                                   */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Label(symbol_t smb)
{
    char * label = Symbol_R_Prolog(smb);
    Label_Printf("\n%s:", label);
}

/*-------------------------------------------------------------------------*/
/* CODE_START                                                              */
/*                                                                         */
/*-------------------------------------------------------------------------*/

char * current_function;

void Code_Start(symbol_t smb)
{
    long global = smb->prop & Smb_Global;
    long prolog = smb->prop & Smb_Prolog;
//    char *label = prolog ? Symbol_R_Prolog(smb) : Symbol_R_Fun(smb);
    char *label = smb->label;

    if (current_function) {
        Inst_Printf(".size","%s, .-%s",current_function,current_function);
    }
     
    current_function = label;
        
    if (debug) Stab_Function(label,global);
    
    Label_Printf("");
//    Inst_Printf(".align","4,0x90");

    if (global)
        Inst_Printf(".globl","%s",label);

    Inst_Printf(".type","%s, @function",label);
        
    Label_Printf("%s:",label);

    if (debug) Stab_Line();
        
    if (!prolog) {
        Inst_Printf("pushq","%%rbp");
        Inst_Printf("movq","%%rsp,%%rbp");
            /* enough space is reserved on top of stack for function args
             * EVDLC Dec 2009: we tmp keep this old model
             * but it seems possible to define a better one
             */
//        Inst_Printf("subq","$%u,%%rsp",8*MAX_C_ARGS_IN_C_CODE);
        max_args=0;
        max_float_args=0;
    }

/*
    if (profile) {
        Label_Printf(".data");
        Inst_Printf(".align","3");
        Label_Printf(".Lprofile%u:",w_label);
        Inst_Printf(".quad","0");
        Label_Printf(".text");
        Inst_Printf("movq","$.Lprofile%u,%%edx",w_label++);
        Inst_Printf("call","mcount");
    }
*/

}

/*-------------------------------------------------------------------------*/
/* CODE_STOP                                                               */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Code_Stop(void)
{
}

/*-------------------------------------------------------------------------*/
/* PL_JUMP                                                                 */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Pl_Jump(symbol_t smb)
{
    char *rlabel = Symbol_R_Prolog(smb);
    Inst_Printf("jmp","%s",rlabel);
}

/*-------------------------------------------------------------------------*/
/* JUMP_NOT_ZERO                                                                 */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Jump_Not_Zero(symbol_t smb)
{
    char *rlabel = Symbol_R_Fun(smb);
    Inst_Printf("testq","%%rax,%%rax");
    if (Defined_P(smb)) {         /* defined label */
        Inst_Printf("jne","%s",rlabel);
    } else {                    /* undefined label */
        Inst_Printf("movq","%s(%%rip),%%rax",rlabel);
        Inst_Printf("jne","*%%rax");
    }
}

/*-------------------------------------------------------------------------*/
/* RET_NOT_ZERO                                                                 */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Ret_Not_Zero(void)
{
    Inst_Printf("testq","%%rax,%%rax");
    Inst_Printf("je",".L%u",w_label);
    C_Ret();
    Label_Printf(".L%u:",w_label++);
}

/*-------------------------------------------------------------------------*/
/* PL_JUMP_REG                                                             */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Pl_Jump_Reg(char name, long no)
{
    Move_From_Reg(name,no);
    Inst_Printf("jmp","*%s","%rax");
}

/*-------------------------------------------------------------------------*/
/* PL_JUMP_STAR                                                            */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Pl_Jump_Star()
{
    Inst_Printf("jmp","*%s",asm_reg_p);
}

/*-------------------------------------------------------------------------*/
/* PL_FAIL                                                                 */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Pl_Fail(void)
{
        // TBM
//    Inst_Printf("call","untrail_choice");
    char *label = Symbol_R_Prolog(smb_follow_choice);
    Inst_Printf("call","%s",label);
    Inst_Printf("jmp","*%s",asm_reg_p);    
}

/*-------------------------------------------------------------------------*/
/* PL_RET                                                                  */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Pl_Ret(void)
{
 Inst_Printf("jmp","*%s",asm_reg_cp);
}

/*-------------------------------------------------------------------------*/
/* JUMP                                                                    */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Jump(symbol_t smb)
{
    char *rlabel = Symbol_R_Fun(smb);
    if (Defined_P(smb)) {         /* defined label */
        Inst_Printf("jmp","%s",rlabel);
    } else {                    /* undefined label */
        Inst_Printf("movq","%s(%%rip),%%rax",rlabel);
        Inst_Printf("jmp","*%%rax");
    }
}

/*-------------------------------------------------------------------------*/
/* MOVE_FROM_REG                                                         */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Move_From_Reg(char name, long no)
{
    struct reg *reg=Find_Register_Info(name);
    generic_move( dreg2pos(name,no), REG_FLOATP(reg->kind) ? xmm7 : rega );
}

/*-------------------------------------------------------------------------*/
/* MOVE_TO_REG                                                           */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Move_To_Reg(char name, long no)
{
    struct reg *reg=Find_Register_Info(name);
    generic_move( REG_FLOATP(reg->kind) ? xmm7 : rega, dreg2pos(name,no) );
}

/*-------------------------------------------------------------------------*/
/* MOVE_FLOAT_TO_REG                                                           */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Move_Float_To_Reg(char name, long no)
{
    generic_move( xmm7, dreg2pos(name,no) );
}

/*-------------------------------------------------------------------------*/
/* MOVE_INT_TO_REG                                                           */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Move_Int_To_Reg(long int_val, char name, long no)
{
    generic_move(xlong(int_val),dreg2pos(name,no));
}

/*-------------------------------------------------------------------------*/
/* MOVE_INT_TO_IDENT                                                           */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Move_Int_To_Ident(long int_val, symbol_t smb, long no)
{
    generic_move(xlong(int_val),xaddr(smb,no));
}

/*-------------------------------------------------------------------------*/
/* Find Variable                                                           */
/*                                                                         */
/*-------------------------------------------------------------------------*/

void Read_Folvar(int i)
{
    generic_move(xfolvar(i),rega);
}

/*-------------------------------------------------------------------------*/
/* MOVE_CST_TO_REG                                                           */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Move_Cst_To_Reg(char cst_kind, long cst_val, char name, long no)
{
    generic_move(dcst2pos(cst_kind,cst_val),dreg2pos(name,no));
}

/*-------------------------------------------------------------------------*/
/* MOVE_CST_TO_IDENT                                                           */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Move_Cst_To_Ident(char cst_kind, long cst_val, symbol_t smb, long no)
{
    generic_move(dcst2pos(cst_kind,cst_val),xaddr(smb,no));
}

/*-------------------------------------------------------------------------*/
/* PL_CALL_START                                                           */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Pl_Call_Start(symbol_t smb)
{
    offset=0;
    float_offset=0;
    kind_of_call=1;
}

/*-------------------------------------------------------------------------*/
/* PL_CALL_STOP                                                            */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Pl_Call_Stop(symbol_t smb)
{                               /* TBM */
//    Inst_Printf("movl","$L_cont%u,%s",w_label,asm_reg_cp);
    generic_move(xlabel(w_label),dyalog_reg_cp);
    Pl_Jump(smb);
    Label_Printf(".L%u:",w_label++);
}

/*-------------------------------------------------------------------------*/
/* PL_CALL_REG                                                             */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Pl_Call_Reg(char name, long no)
{
//    Inst_Printf("movl","$L_cont%u,%s",w_label,asm_reg_cp);
    generic_move(xlabel(w_label),dyalog_reg_cp);
    Pl_Jump_Reg(name,no);
    Label_Printf(".L%u:",w_label++);
}

/*-------------------------------------------------------------------------*/
/* PL_CALL_STAR                                                            */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Pl_Call_Star()
{
//    Inst_Printf("movl","$L_cont%u,%s",w_label,asm_reg_cp);
    generic_move(xlabel(w_label),dyalog_reg_cp);
    Pl_Jump_Star();
    Label_Printf(".L%u:",w_label++);
}

/*-------------------------------------------------------------------------*/
/* CALL_C_START                                                            */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Call_C_Start(symbol_t smb,
                  unsigned long nb_args,
                  unsigned long nb_float_args)
{
    offset = 0;
    float_offset=0;
    kind_of_call=0;
    if (nb_args>6) {
        int x = nb_args-6;
        if (x % 2==1) x++;      /* align, when prologue */
//        fprintf(stderr,"long call %s %d\n",smb->label,nb_args);
        Inst_Printf("subq","$%u,%%rsp",8*x);
    } 
}

/*-------------------------------------------------------------------------*/
/* CALL_C_ARG_LABEL                                                        */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Call_Arg_Label(symbol_t smb,long no)
{
    load_arg(xaddr(smb,no));
}

/*-------------------------------------------------------------------------*/
/* READ_VARIABLE                                                     */
/*                                                                         */
/*-------------------------------------------------------------------------*/

void Read_Variable(symbol_t smb, long no)
{
    generic_move(xvar(smb,no),rega);
}

/*-------------------------------------------------------------------------*/
/* CALL_C_ARG_VARIABLE                                                     */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Call_Arg_Variable(symbol_t smb,long no)
{
    load_arg(xvar(smb,no));
}

/*-------------------------------------------------------------------------*/
/* CALL_C_ARG_INT                                                          */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Call_Arg_Int(long int_val)
{
    load_arg(xlong(int_val));
}

/*-------------------------------------------------------------------------*/
/* CALL_C_ARG_FLOAT                                                        */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Call_Arg_Float(constant_t cst)
{
    load_float_arg(xfloat(cst->id));
}


/*-------------------------------------------------------------------------*/
/* CALL_C_ARG_DOUBLE                                                       */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Call_Arg_Double(constant_t cst)
{
    load_float_arg(xfloat(cst->id));
}

/*-------------------------------------------------------------------------*/
/* CALL_C_ARG_STRING                                                       */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Call_Arg_String(constant_t cst)
{
    load_arg(xstring(cst->id));
}

/*-------------------------------------------------------------------------*/
/*  CALL_C_ARG_REG                                                         */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Call_Arg_Reg(char name, long no)
{
    struct reg *reg=Find_Register_Info(name);
    pos src = dreg2pos(name,no);
    if (REG_FLOATP(reg->kind))
        load_float_arg(src);
    else
        load_arg(src);
}

/*-------------------------------------------------------------------------*/
/* CALL_ARG_REG_VARIABLE                                                 */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Call_Arg_Reg_Variable(char name, long no)
{
        /* This one seems incorrect
           but does not seems to be used !
        */
    struct reg *reg=Find_Register_Info(name);
    pos src = dreg2pos(name,no);
    src.kind=stackaddr;
    if (REG_FLOATP(reg->kind)) 
        load_float_arg(src);
    else
        load_arg(src);
}

/*-------------------------------------------------------------------------*/
/* CALL_C_ARG_CST                                                          */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Call_Arg_Cst(char name, long no)
{
    load_arg(dcst2pos(name,no));
}

/*-------------------------------------------------------------------------*/
/* CALL_C_ARG_PARAM                                                        */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Call_Arg_Param(long no)
{
    load_arg(xarg(no));
}

/*-------------------------------------------------------------------------*/
/* CALL_C_STOP                                                             */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Call_C_Stop(symbol_t smb,
                 unsigned long nb_args,
                 unsigned long nb_float_args )
{                               /* TBM */
    char *tmp_fct_name = Symbol_R_Fun(smb);
    if (0 // !Defined_P(smb)
        ) {
            /* for extern function, we emit information
             * about the number of floats
             * local functions do not have arguments
             */
        if (nb_float_args > 0)
            generic_move(xlong(nb_float_args),rega);
        else 
            Inst_Printf("xorl","%%eax,%%eax");
    }
    Inst_Printf("call","%s",tmp_fct_name);
    if (nb_args>6) {
        int x=nb_args-6;
        if (x % 2 == 1) x++;    /* align when prologue */
//        fprintf(stderr,"long ret %s %d\n",smb->label,nb_args);
        Inst_Printf("addq","$%u,%%rsp",8*x);
    } 
}

/*-------------------------------------------------------------------------*/
/* JUMP_RET                                                                */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Jump_Ret(void)
{
    Inst_Printf("jmp","*%%rax");
}

/*-------------------------------------------------------------------------*/
/* FAIL_RET                                                                */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Fail_Ret(void)
{
    Inst_Printf("testq","%%rax,%%rax");
    Inst_Printf("je", UN "%s","fail");
}

/*-------------------------------------------------------------------------*/
/* TRUE_RET                                                                */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void True_Ret(void)
{
    Inst_Printf("testq","%%rax,%%rax");
    Inst_Printf("jne",UN "%s","fail");
}

/*-------------------------------------------------------------------------*/
/* FAIL_C_RET                                                                */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Fail_C_Ret(void)
{
    Inst_Printf("testq","%%rax,%%rax");
    Inst_Printf("jne",".L%u",w_label);
    C_Ret();
    Label_Printf(".L%u:",w_label++);
}

/*-------------------------------------------------------------------------*/
/* TRUE_C_RET                                                                */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void True_C_Ret(void)
{
    Inst_Printf("testq","%%rax,%%rax");
    Inst_Printf("je",".L%u",w_label);
    C_Ret();
    Label_Printf(".L%u:",w_label++);
}

/*-------------------------------------------------------------------------*/
/* MOVE_RET_TO_IDENT                                                       */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Move_Ret_To_Ident(symbol_t smb,long no)
{
    generic_move(rega,xaddr(smb,no));
}

/*-------------------------------------------------------------------------*/
/* MOVE_RET_TO_REG                                                         */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Move_Ret_To_Reg(char name, long no)
{
    Move_To_Reg(name,no);
}

/*-------------------------------------------------------------------------*/
/* C_RET                                                                   */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void C_Ret(void)
{
    Inst_Printf("leave", "");
    Inst_Printf("ret", "");
}

/*-------------------------------------------------------------------------*/
/* CACHE_OR_BUILD                                                          */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Cache_Or_Build(symbol_t smb,long no,symbol_t fun)
{
        /* smb and fun should be both local symbols */
    if (smb->prop & Smb_Global) {
        fprintf(stderr,"Bad use of 'cache_or_build' instruction");
        exit(1);
    }
    if (no==0) {
        Inst_Printf("cmpq","$0,%s(%%rip)",Symbol_R_Data(smb));
    } else {
        Inst_Printf("cmpq","$0,%lu+%s(%%rip)",8*no,Symbol_R_Data(smb));
    }
    Inst_Printf("jne",".L%u",w_label);
    Inst_Printf("call","%s",Symbol_R_Fun(fun));
    Label_Printf(".L%u:",w_label++);
}


/*-------------------------------------------------------------------------*/
/* DICO_STRING_START                                                       */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Dico_String_Start(long nb_consts)
{
    Inst_Printf(".section",".rodata.str1.1,\"aMS\",@progbits,1");
}

/*-------------------------------------------------------------------------*/
/* DICO_STRING                                                             */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Dico_String(long str_no,char *asciiz)
{
    Label_Printf("%s%u:",STRING_PREFIX,str_no);
    Inst_Printf(".string","\"%s\"",asciiz);
}

/*-------------------------------------------------------------------------*/
/* DICO_STRING_STOP                                                        */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Dico_String_Stop(long nb_consts)
{
}


/*-------------------------------------------------------------------------*/
/* DICO_STRING_START                                                       */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Dico_Float_Start(long nb_consts)
{
    Inst_Printf(".section",".rodata.cst8,\"aM\",@progbits,8");
}

/*-------------------------------------------------------------------------*/
/* DICO_STRING                                                             */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Dico_Float(long no,float flt_val)
{
    double flt_double = (double) flt_val;
    unsigned int *p=(unsigned int *) &flt_double;

    Inst_Printf(".align","8");
    Label_Printf("%s%u:",FLOAT_PREFIX,no);
    Inst_Printf(".long","%u",p[0]);
    Inst_Printf(".long","%u",p[1]);
}

/*-------------------------------------------------------------------------*/
/* DICO_STRING_STOP                                                        */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Dico_Float_Stop(long nb_consts)
{
}

/*-------------------------------------------------------------------------*/
/* DICO_LONG_START                                                         */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Dico_Long_Start()
{
    Label_Printf(".data");
    Inst_Printf(".align","8");
}

/*-------------------------------------------------------------------------*/
/* DICO_LONG                                                               */
/*                                                                         */
/*-------------------------------------------------------------------------*/

static
unsigned long
length2align(unsigned long length) 
{
    if (length == 1) 
        return 8;
    else if (length < 4)
        return 16;
    else
        return 32;
}


void
Dico_Long(symbol_t smb)
{
    char *label = smb->label;
    long global = smb->prop & Smb_Global;
    long length = smb->size;
    InitLongList_t value = smb->init;
    if (!length)
        length = 1;
    if (value){
        if (global)
            Inst_Printf(".globl", UN "%s",label);
            // some info about aligment to be added here
        Label_Printf(UN "%s:",label);
        for(; value && length ; value = value->next, length--) {
            if (value->type==0) {
                Inst_Printf(".quad",UN "%s",(char *)value->value);
            } else {
                Inst_Printf(".quad","%d",value->value);
            }
        }
        if (value) {            /* list too long  */
            fprintf(stderr,"Init Long List too long for ident %s\n",label);
            exit(1);            
        }
    } else if (global) {
        Inst_Printf(".comm",UN "%s,%lu,%u",
                    label,
                    (unsigned long)(8*length),
                    length2align(length)
                    );
    } else {
        Inst_Printf(".local",UN "%s",label);
        Inst_Printf(".comm",UN "%s,%lu,%u",
                    label,
                    (unsigned long)(8*length),
                    length2align(length)
                    ); 
    }
    
}

/*-------------------------------------------------------------------------*/
/* DICO_LONG_STOP                                                          */
/*                                                                         */
/*-------------------------------------------------------------------------*/
void Dico_Long_Stop()
{
}



